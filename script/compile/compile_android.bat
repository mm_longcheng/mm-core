@echo off

set PLATFORM=android
set ACTION=%*

set index=0

set arr[%index%]=mm                        & set /a index+=1
set arr[%index%]=dish                      & set /a index+=1
set arr[%index%]=data                      & set /a index+=1
set arr[%index%]=nwsi                      & set /a index+=1

:: echo %index%

set _BuildCMD=%MM_HOME%/mm-make/script/bat/compile-core.bat
@echo y | call %_BuildCMD% arr %index% %PLATFORM% %ACTION% 

GOTO :EOF
