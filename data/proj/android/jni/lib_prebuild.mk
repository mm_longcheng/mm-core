LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcurl_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/curl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcurl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcurl_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/curl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcurl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := liblua_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/lua/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/liblua_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := liblua_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/lua/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/liblua_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotobuf_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/protobuf/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libprotobuf_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libprotobuf_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/protobuf/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libprotobuf_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcJSON_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/cJSON/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcJSON_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcJSON_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/cJSON/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcJSON_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openssl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcrypto_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/openssl/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libcrypto_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := librabbitmq_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/rabbitmq/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/librabbitmq_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := librabbitmq_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/rabbitmq/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/librabbitmq_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libhiredis_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/hiredis/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libhiredis_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libhiredis_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/hiredis/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libhiredis_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libzookeeper_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zookeeper/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzookeeper_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libzookeeper_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zookeeper/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libzookeeper_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_net_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_net_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_net_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_net_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_rq_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_rq_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_rq_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_rq_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################