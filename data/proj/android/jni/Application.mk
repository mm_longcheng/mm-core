APP_ABI := arm64-v8a armeabi-v7a

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libmm_data_curl_static
APP_MODULES += libmm_data_curl_shared

APP_MODULES += libmm_data_jwt_static
APP_MODULES += libmm_data_jwt_shared

APP_MODULES += libmm_data_lua_static
APP_MODULES += libmm_data_lua_shared

# APP_MODULES += libmm_data_mysql_static
# APP_MODULES += libmm_data_mysql_shared

APP_MODULES += libmm_data_openssl_static
APP_MODULES += libmm_data_openssl_shared

APP_MODULES += libmm_data_protobuf_static
APP_MODULES += libmm_data_protobuf_shared

APP_MODULES += libmm_data_rabbitmq_static
APP_MODULES += libmm_data_rabbitmq_shared

APP_MODULES += libmm_data_redis_static
APP_MODULES += libmm_data_redis_shared

APP_MODULES += libmm_data_zookeeper_static
APP_MODULES += libmm_data_zookeeper_shared
