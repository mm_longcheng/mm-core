LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libmm_data_curl_shared.mk
include $(LOCAL_PATH)/libmm_data_curl_static.mk

include $(LOCAL_PATH)/libmm_data_jwt_shared.mk
include $(LOCAL_PATH)/libmm_data_jwt_static.mk

include $(LOCAL_PATH)/libmm_data_lua_shared.mk
include $(LOCAL_PATH)/libmm_data_lua_static.mk

# include $(LOCAL_PATH)/libmm_data_mysql_shared.mk
# include $(LOCAL_PATH)/libmm_data_mysql_static.mk

include $(LOCAL_PATH)/libmm_data_openssl_shared.mk
include $(LOCAL_PATH)/libmm_data_openssl_static.mk

include $(LOCAL_PATH)/libmm_data_protobuf_shared.mk
include $(LOCAL_PATH)/libmm_data_protobuf_static.mk

include $(LOCAL_PATH)/libmm_data_rabbitmq_shared.mk
include $(LOCAL_PATH)/libmm_data_rabbitmq_static.mk

include $(LOCAL_PATH)/libmm_data_redis_shared.mk
include $(LOCAL_PATH)/libmm_data_redis_static.mk

include $(LOCAL_PATH)/libmm_data_zookeeper_shared.mk
include $(LOCAL_PATH)/libmm_data_zookeeper_static.mk