LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libmm_data_curl_shared
LOCAL_MODULE_FILENAME := libmm_data_curl_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DMM_SHARED_CURL
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libmm_core_shared
LOCAL_SHARED_LIBRARIES += libmm_dish_shared
LOCAL_SHARED_LIBRARIES += libcurl_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/mm/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/mm/src/os/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/dish/src

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-libx/build/curl/include/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-libx/src/curl/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/curl
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################