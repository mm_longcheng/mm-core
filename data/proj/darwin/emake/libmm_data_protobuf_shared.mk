LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libmm_data_protobuf_shared
LOCAL_MODULE_FILENAME := libmm_data_protobuf_shared
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DMM_SHARED_PROTOBUF

LOCAL_CXXFLAGS += -std=c++11
LOCAL_CXXFLAGS += -frtti
LOCAL_CXXFLAGS += -fexceptions
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libmm_core_shared
LOCAL_SHARED_LIBRARIES += libmm_net_shared
LOCAL_SHARED_LIBRARIES += libmm_rq_shared
LOCAL_SHARED_LIBRARIES += libprotobuf_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/mm/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/mm/src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/dish/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-core/dish/src/os/$(MM_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../../mm-libx/src/protobuf/src
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/protobuf
MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/protobuf/mmProtobuffCString.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################