/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmZookeeperZkwpPath.h"
#include "core/mmLogger.h"
#include "core/mmAtoi.h"
#include "core/mmThread.h"

static void __static_mmZkwpPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);

static void __static_mmZkwpPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwpPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path
static void __static_mmZkwpPath_Awexists(struct mmZkwpPath* p);

static void __static_mmZkwpPath_AcreateStringCompletion(int rc, const char *value, const void *data);
// acreate path
static void __static_mmZkwpPath_Acreate(struct mmZkwpPath* p);

static void __static_mmZkwpPath_ShardAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwpPath_ShardAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path/shard
static void __static_mmZkwpPath_ShardAwexists(struct mmZkwpPath* p);

static void __static_mmZkwpPath_ShardAcreateStringCompletion(int rc, const char *value, const void *data);
// acreate path/shard
static void __static_mmZkwpPath_ShardAcreate(struct mmZkwpPath* p);

static void __static_mmZkwpPath_ShardUniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwpPath_ShardUniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path/shard/unique_id
static void __static_mmZkwpPath_ShardUniqueIdAwexists(struct mmZkwpPath* p);

static void __static_mmZkwpPath_ShardUniqueIdAcreateStringCompletion(int rc, const char *value, const void *data);
// acreate path/shard/unique_id
static void __static_mmZkwpPath_ShardUniqueIdAcreate(struct mmZkwpPath* p);

static void __static_mmZkwpPath_ShardUniqueIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data);
// aset path/shard/unique_id
static void __static_mmZkwpPath_ShardUniqueIdAset(struct mmZkwpPath* p);

static void __static_mmZkwpPath_Event(struct mmZkwpPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkwpPathCallback_Init(struct mmZkwpPathCallback* p)
{
    p->Created = &__static_mmZkwpPath_Event;
    p->Deleted = &__static_mmZkwpPath_Event;
    p->Changed = &__static_mmZkwpPath_Event;
    p->NetConnect = &__static_mmZkwpPath_Event;
    p->NetExpired = &__static_mmZkwpPath_Event;
    p->obj = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkwpPathCallback_Destroy(struct mmZkwpPathCallback* p)
{
    p->Created = &__static_mmZkwpPath_Event;
    p->Deleted = &__static_mmZkwpPath_Event;
    p->Changed = &__static_mmZkwpPath_Event;
    p->NetConnect = &__static_mmZkwpPath_Event;
    p->NetExpired = &__static_mmZkwpPath_Event;
    p->obj = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Init(struct mmZkwpPath* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmString_Init(&p->path);
    mmString_Init(&p->host);
    mmString_Init(&p->value_buffer);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmZkwpPathCallback_Init(&p->callback);
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->shard = 0;
    p->depth = 0;
    p->timeout = MM_ZKWP_PATH_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;

    mmString_Assigns(&p->path, "/zkwp");
    mmString_Assigns(&p->host, "127.0.0.1:2181,");

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Destroy(struct mmZkwpPath* p)
{
    mmZkwpPath_Abandon(p);
    //
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->host);
    mmString_Destroy(&p->value_buffer);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmZkwpPathCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->shard = 0;
    p->depth = 0;
    p->timeout = 0;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Start(struct mmZkwpPath* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmZkwpPath_Watcher(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Interrupt(struct mmZkwpPath* p)
{
    p->state = MM_TS_CLOSED;
    mmZkwpPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Shutdown(struct mmZkwpPath* p)
{
    p->state = MM_TS_FINISH;
    mmZkwpPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Join(struct mmZkwpPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetUniqueId(struct mmZkwpPath* p, mmUInt32_t unique_id)
{
    p->unique_id = unique_id;
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetSlot(struct mmZkwpPath* p, mmUInt32_t shard, mmUInt32_t depth)
{
    p->shard = shard;
    p->depth = depth;
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetPath(struct mmZkwpPath* p, const char* path)
{
    assert(NULL != path && "you can not assign null path.");
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetHost(struct mmZkwpPath* p, const char* host)
{
    assert(NULL != host && "you can not assign null host.");
    mmString_Assigns(&p->host, host);
}

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Watcher(struct mmZkwpPath* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // try abandon first.
    mmZkwpPath_Abandon(p);
    if (0 != mmString_Size(&p->host))
    {
        // init a new zk handle.
        p->zkhandle = zookeeper_init(mmString_CStr(&p->host), __static_mmZkwpPath_Watcher, p->timeout, 0, p, 0);
        if (NULL == p->zkhandle)
        {
            mmLogger_LogE(gLogger, "%s %d init zookeeper servers failure.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d init zookeeper servers success.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d the host is empty.", __FUNCTION__, __LINE__);
    }
}
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Abandon(struct mmZkwpPath* p)
{
    if (NULL != p->zkhandle)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        zookeeper_close(p->zkhandle);
        p->zkhandle = NULL;
        mmLogger_LogI(gLogger, "%s %d close zookeeper servers success.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Lock(struct mmZkwpPath* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Unlock(struct mmZkwpPath* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Commit(struct mmZkwpPath* p)
{
    __static_mmZkwpPath_Awexists(p);
}

static void __static_mmZkwpPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(watcherCtx);
    if (ZOO_SESSION_EVENT == type)
    {
        if (ZOO_CONNECTED_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connected.", __FUNCTION__, __LINE__, path);
            (*(_zkwp_path->callback.NetConnect))(_zkwp_path);
            __static_mmZkwpPath_Awexists(_zkwp_path);
        }
        else if (ZOO_EXPIRED_SESSION_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s session expired.", __FUNCTION__, __LINE__, path);
            (*(_zkwp_path->callback.NetExpired))(_zkwp_path);
            mmZkwpPath_Watcher(_zkwp_path);
        }
        else if (ZOO_CONNECTING_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connecting.", __FUNCTION__, __LINE__, path);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d event happened.type:%d state:%d path:%s.", __FUNCTION__, __LINE__, type, state, path);
        }
    }
}

static void __static_mmZkwpPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkwp_path->callback.Deleted))(_zkwp_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkwp_path->callback.Created))(_zkwp_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkwp_path->callback.Changed))(_zkwp_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwpPath_Awexists(_zkwp_path);
}
static void __static_mmZkwpPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/shard.
        __static_mmZkwpPath_ShardAwexists(_zkwp_path);
    }
    else if (ZNONODE == rc)
    {
        // create path.
        __static_mmZkwpPath_Acreate(_zkwp_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwpPath_Awexists(struct mmZkwpPath* p)
{
    int ret = zoo_awexists(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        __static_mmZkwpPath_AwexistsWatcher, 
        p, 
        __static_mmZkwpPath_AwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwpPath_AcreateStringCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/shard.
        __static_mmZkwpPath_ShardAwexists(_zkwp_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // awexists path/shard.
        __static_mmZkwpPath_ShardAwexists(_zkwp_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// acreate path
static void __static_mmZkwpPath_Acreate(struct mmZkwpPath* p)
{
    // not ZOO_SEQUENCE
    int ret = zoo_acreate(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        "", 
        0, 
        &ZOO_OPEN_ACL_UNSAFE, 
        0, 
        __static_mmZkwpPath_AcreateStringCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwpPath_ShardAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    // struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(watcherCtx);
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwpPath_ShardAwexists(_zkwp_path);
}
static void __static_mmZkwpPath_ShardAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/shard/unique_id
        __static_mmZkwpPath_ShardUniqueIdAwexists(_zkwp_path);
    }
    else if (ZNONODE == rc)
    {
        // acreate path/shard
        __static_mmZkwpPath_ShardAcreate(_zkwp_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// awexists path/shard
static void __static_mmZkwpPath_ShardAwexists(struct mmZkwpPath* p)
{
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWP_FORMAT_1, mmString_CStr(&p->path), p->shard);
    ret = zoo_awexists(
        p->zkhandle, 
        path, 
        __static_mmZkwpPath_ShardAwexistsWatcher, 
        p, 
        __static_mmZkwpPath_ShardAwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwpPath_ShardAcreateStringCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/shard/unique_id
        __static_mmZkwpPath_ShardUniqueIdAwexists(_zkwp_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // awexists path/shard/unique_id
        __static_mmZkwpPath_ShardUniqueIdAwexists(_zkwp_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// acreate path/shard
static void __static_mmZkwpPath_ShardAcreate(struct mmZkwpPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWP_FORMAT_1, mmString_CStr(&p->path), p->shard);
    ret = zoo_acreate(
        p->zkhandle, 
        path, 
        "", 
        0, 
        &ZOO_OPEN_ACL_UNSAFE, 
        0, 
        __static_mmZkwpPath_ShardAcreateStringCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwpPath_ShardUniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    // struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(watcherCtx);
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwpPath_ShardUniqueIdAwexists(_zkwp_path);
}
static void __static_mmZkwpPath_ShardUniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(data);
    if (ZOK == rc)
    {
        // aset path/module_id/unique_id
        __static_mmZkwpPath_ShardUniqueIdAset(_zkwp_path);
    }
    else if (ZNONODE == rc)
    {
        // acreate path/module_id/unique_id
        __static_mmZkwpPath_ShardUniqueIdAcreate(_zkwp_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// awexists path/shard/unique_id
static void __static_mmZkwpPath_ShardUniqueIdAwexists(struct mmZkwpPath* p)
{
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWP_FORMAT_1 "/" MM_ZKWP_FORMAT_2, mmString_CStr(&p->path), p->shard, p->depth);
    ret = zoo_awexists(
        p->zkhandle, 
        path, 
        __static_mmZkwpPath_ShardUniqueIdAwexistsWatcher, 
        p, 
        __static_mmZkwpPath_ShardUniqueIdAwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwpPath_ShardUniqueIdAcreateStringCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwpPath* _zkwp_path = (struct mmZkwpPath*)(data);
    if (ZOK == rc)
    {
        // aset path/shard/unique_id
        __static_mmZkwpPath_ShardUniqueIdAset(_zkwp_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // aset path/shard/unique_id
        __static_mmZkwpPath_ShardUniqueIdAset(_zkwp_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// acreate path/shard/unique_id
static void __static_mmZkwpPath_ShardUniqueIdAcreate(struct mmZkwpPath* p)
{
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWP_FORMAT_1 "/" MM_ZKWP_FORMAT_2, mmString_CStr(&p->path), p->shard, p->depth);
    ret = zoo_acreate(
        p->zkhandle, 
        path, 
        mmString_CStr(&p->value_buffer), 
        (int)mmString_Size(&p->value_buffer), 
        &ZOO_OPEN_ACL_UNSAFE, ZOO_EPHEMERAL, 
        __static_mmZkwpPath_ShardUniqueIdAcreateStringCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwpPath_ShardUniqueIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (ZOK != rc)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// aset path/shard/unique_id
static void __static_mmZkwpPath_ShardUniqueIdAset(struct mmZkwpPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWP_FORMAT_1 "/" MM_ZKWP_FORMAT_2, mmString_CStr(&p->path), p->shard, p->depth);
    ret = zoo_aset(
        p->zkhandle, 
        path, 
        mmString_CStr(&p->value_buffer), 
        (int)mmString_Size(&p->value_buffer), 
        -1, 
        __static_mmZkwpPath_ShardUniqueIdAsetStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

