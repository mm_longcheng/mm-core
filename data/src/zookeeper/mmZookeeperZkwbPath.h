/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmZookeeperZkwbPath_h__
#define __mmZookeeperZkwbPath_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "zookeeper.h"

#include "zookeeper/mmZookeeperExport.h"

#include "core/mmPrefix.h"

#define MM_ZKWB_FORMAT_1 "%u"
// connect invalid timeout.default is 3s(3000ms).
#define MM_ZKWB_PATH_TIMEOUT 3000

// /path
//       ZOO_EPHEMERAL
//       /2002000 buffer

struct mmZkwbPath;

typedef void(*mmZkwbPathEventFunc)(struct mmZkwbPath* p);

struct mmZkwbPathCallback
{
    mmZkwbPathEventFunc Created;
    mmZkwbPathEventFunc Deleted;
    mmZkwbPathEventFunc Changed;
    mmZkwbPathEventFunc NetConnect;
    mmZkwbPathEventFunc NetExpired;
    void* obj;
};

MM_EXPORT_ZOOKEEPER void mmZkwbPathCallback_Init(struct mmZkwbPathCallback* p);
MM_EXPORT_ZOOKEEPER void mmZkwbPathCallback_Destroy(struct mmZkwbPathCallback* p);

struct mmZkwbPath
{
    // zk path for notify.
    struct mmString path;
    // zk cluster "host:port,host:port,"
    struct mmString host;
    // value for buffer.
    struct mmString value_buffer;
    // event for callback.
    struct mmZkwbPathCallback callback;
    mmAtomic_t locker;
    mmUInt32_t unique_id;
    // connect invalid timeout.default is MM_ZKWB_PATH_TIMEOUT.
    mmMSec_t timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // zk handle.
    zhandle_t* zkhandle;
};

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Init(struct mmZkwbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Destroy(struct mmZkwbPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Start(struct mmZkwbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Interrupt(struct mmZkwbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Shutdown(struct mmZkwbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Join(struct mmZkwbPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwbPath_SetUniqueId(struct mmZkwbPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_SetPath(struct mmZkwbPath* p, const char* path);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_SetHost(struct mmZkwbPath* p, const char* host);

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Watcher(struct mmZkwbPath* p);
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Abandon(struct mmZkwbPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Lock(struct mmZkwbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Unlock(struct mmZkwbPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Commit(struct mmZkwbPath* p);

#include "core/mmSuffix.h"

#endif//__mmZookeeperZkwbPath_h__
