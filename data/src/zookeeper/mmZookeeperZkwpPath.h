/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmZookeeperZkwpPath_h__
#define __mmZookeeperZkwpPath_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmRbtreeU32.h"

#include "zookeeper.h"

#include "zookeeper/mmZookeeperExport.h"

#include "core/mmPrefix.h"

#define MM_ZKWP_FORMAT_1 "%u"
#define MM_ZKWP_FORMAT_2 "%u"
// connect invalid timeout.default is 3s(3000ms).
#define MM_ZKWP_PATH_TIMEOUT 3000

// /path
//       /0   /ZOO_EPHEMERAL
//       /1000/1000 buffer
//       /1001/1000 buffer

struct mmZkwpPath;
struct mmZkwpPathElem;

typedef void(*mmZkwpPathEventFunc)(struct mmZkwpPath* p);

struct mmZkwpPathCallback
{
    mmZkwpPathEventFunc Created;
    mmZkwpPathEventFunc Deleted;
    mmZkwpPathEventFunc Changed;
    mmZkwpPathEventFunc NetConnect;
    mmZkwpPathEventFunc NetExpired;
    void* obj;
};

MM_EXPORT_ZOOKEEPER void mmZkwpPathCallback_Init(struct mmZkwpPathCallback* p);
MM_EXPORT_ZOOKEEPER void mmZkwpPathCallback_Destroy(struct mmZkwpPathCallback* p);

struct mmZkwpPath
{
    // zk path for notify.
    struct mmString path;
    // zk cluster "host:port,host:port,"
    struct mmString host;
    // value for buffer.
    struct mmString value_buffer;
    // rb tree for unique_id <--> mmZkwpPathElem.
    struct mmRbtreeU32Vpt rbtree;
    // event for callback.
    struct mmZkwpPathCallback callback;
    mmAtomic_t locker;
    mmUInt32_t unique_id;
    // slot for shard.
    mmUInt32_t shard;
    // slot for depth.
    mmUInt32_t depth;
    // connect invalid timeout.default is MM_ZKWP_PATH_TIMEOUT.
    mmMSec_t timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // zk handle.
    zhandle_t* zkhandle;
};

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Init(struct mmZkwpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Destroy(struct mmZkwpPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Start(struct mmZkwpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Interrupt(struct mmZkwpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Shutdown(struct mmZkwpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Join(struct mmZkwpPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetUniqueId(struct mmZkwpPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetSlot(struct mmZkwpPath* p, mmUInt32_t shard, mmUInt32_t depth);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetPath(struct mmZkwpPath* p, const char* path);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_SetHost(struct mmZkwpPath* p, const char* host);

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Watcher(struct mmZkwpPath* p);
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Abandon(struct mmZkwpPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Lock(struct mmZkwpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwpPath_Unlock(struct mmZkwpPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwpPath_Commit(struct mmZkwpPath* p);

#include "core/mmSuffix.h"

#endif//__mmZookeeperZkwpPath_h__
