/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmZookeeperZkrbPath_h__
#define __mmZookeeperZkrbPath_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmRbtreeU32.h"

#include "zookeeper.h"

#include "zookeeper/mmZookeeperExport.h"

#include "core/mmPrefix.h"

#define MM_ZKRB_FORMAT_1 "%u"
// connect invalid timeout.default is 3s(3000ms).
#define MM_ZKRB_PATH_TIMEOUT 3000

// /path
//       ZOO_EPHEMERAL
//       /2002000 buffer

struct mmZkrbPath;
struct mmZkrbPathElem;

typedef void(*mmZkrbPathElemValueFunc)(struct mmZkrbPath* p, struct mmZkrbPathElem* e, const char* buffer, int offset, int length);
typedef void(*mmZkrbPathElemEventFunc)(struct mmZkrbPath* p, struct mmZkrbPathElem* e);
typedef void(*mmZkrbPathEventFunc)(struct mmZkrbPath* p);

struct mmZkrbPathCallback
{
    mmZkrbPathElemValueFunc PathElemUpdated;
    mmZkrbPathElemEventFunc PathElemCreated;
    mmZkrbPathElemEventFunc PathElemDeleted;
    mmZkrbPathElemEventFunc PathElemChanged;
    mmZkrbPathEventFunc PathCreated;
    mmZkrbPathEventFunc PathDeleted;
    mmZkrbPathEventFunc PathChanged;
    mmZkrbPathEventFunc NetConnect;
    mmZkrbPathEventFunc NetExpired;
    void* obj;
};

MM_EXPORT_ZOOKEEPER void mmZkrbPathCallback_Init(struct mmZkrbPathCallback* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPathCallback_Destroy(struct mmZkrbPathCallback* p);

struct mmZkrbPathElem
{
    // parent node.
    struct mmZkrbPath* zk_path;
    mmUInt32_t unique_id;
    mmAtomic_t locker;
    struct Stat stat;
};

MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Init(struct mmZkrbPathElem* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Destroy(struct mmZkrbPathElem* p);

MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Lock(struct mmZkrbPathElem* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Unlock(struct mmZkrbPathElem* p);

struct mmZkrbPath
{
    // zk path for notify.
    struct mmString path;
    // zk cluster "host:port,host:port,"
    struct mmString host;
    // rb tree for unique_id <--> mmZkrbPathElem.
    struct mmRbtreeU32Vpt rbtree;
    // event for callback.
    struct mmZkrbPathCallback callback;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    // connect invalid timeout.default is MM_ZKRB_PATH_TIMEOUT.
    mmMSec_t timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // zk handle.
    zhandle_t* zkhandle;
};

MM_EXPORT_ZOOKEEPER void mmZkrbPath_Init(struct mmZkrbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Destroy(struct mmZkrbPath* p);

MM_EXPORT_ZOOKEEPER void mmZkrbPath_Start(struct mmZkrbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Interrupt(struct mmZkrbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Shutdown(struct mmZkrbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Join(struct mmZkrbPath* p);

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Watcher(struct mmZkrbPath* p);
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Abandon(struct mmZkrbPath* p);

MM_EXPORT_ZOOKEEPER void mmZkrbPath_Lock(struct mmZkrbPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Unlock(struct mmZkrbPath* p);

// assign net callback.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_SetCallback(struct mmZkrbPath* p, struct mmZkrbPathCallback* zkrb_path_callback);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_SetPath(struct mmZkrbPath* p, const char* path);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_SetHost(struct mmZkrbPath* p, const char* host);

// this function will check and try add watcher when the elem size is zero.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Update(struct mmZkrbPath* p);

MM_EXPORT_ZOOKEEPER struct mmZkrbPathElem* mmZkrbPath_Add(struct mmZkrbPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Rmv(struct mmZkrbPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrbPathElem* mmZkrbPath_Get(struct mmZkrbPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrbPathElem* mmZkrbPath_GetInstance(struct mmZkrbPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Clear(struct mmZkrbPath* p);

#include "core/mmSuffix.h"

#endif//__mmZookeeperZkrbPath_h__
