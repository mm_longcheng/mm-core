/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmZookeeperZkrbPath.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmThread.h"

static void __static_mmZkrbPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);

static void __static_mmZkrbPath_UniqueIdAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data);
static void __static_mmZkrbPath_UniqueIdAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx);
static void __static_mmZkrbPath_UniqueIdAwget(struct mmZkrbPathElem* _elem);

static void __static_mmZkrbPath_UniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrbPath_UniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrbPath_UniqueIdAwexists(struct mmZkrbPathElem* p);

static void __static_mmZkrbPath_AwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data);
static void __static_mmZkrbPath_AwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrbPath_AwgetChildren(struct mmZkrbPath* p);

static void __static_mmZkrbPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrbPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrbPath_Awexists(struct mmZkrbPath* p);

static void __static_mmZkrbPath_ElemValue(struct mmZkrbPath* p, struct mmZkrbPathElem* e, const char* buffer, int offset, int length)
{

}
static void __static_mmZkrbPath_ElemEvent(struct mmZkrbPath* p, struct mmZkrbPathElem* e)
{

}
static void __static_mmZkrbPath_Event(struct mmZkrbPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkrbPathCallback_Init(struct mmZkrbPathCallback* p)
{
    p->PathElemUpdated = &__static_mmZkrbPath_ElemValue;
    p->PathElemCreated = &__static_mmZkrbPath_ElemEvent;
    p->PathElemDeleted = &__static_mmZkrbPath_ElemEvent;
    p->PathElemChanged = &__static_mmZkrbPath_ElemEvent;
    p->PathCreated = &__static_mmZkrbPath_Event;
    p->PathDeleted = &__static_mmZkrbPath_Event;
    p->PathChanged = &__static_mmZkrbPath_Event;
    p->NetConnect = &__static_mmZkrbPath_Event;
    p->NetExpired = &__static_mmZkrbPath_Event;
    p->obj = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkrbPathCallback_Destroy(struct mmZkrbPathCallback* p)
{
    p->PathElemUpdated = &__static_mmZkrbPath_ElemValue;
    p->PathElemCreated = &__static_mmZkrbPath_ElemEvent;
    p->PathElemDeleted = &__static_mmZkrbPath_ElemEvent;
    p->PathElemChanged = &__static_mmZkrbPath_ElemEvent;
    p->PathCreated = &__static_mmZkrbPath_Event;
    p->PathDeleted = &__static_mmZkrbPath_Event;
    p->PathChanged = &__static_mmZkrbPath_Event;
    p->NetConnect = &__static_mmZkrbPath_Event;
    p->NetExpired = &__static_mmZkrbPath_Event;
    p->obj = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Init(struct mmZkrbPathElem* p)
{
    p->zk_path = NULL;
    p->unique_id = 0;
    mmSpinlock_Init(&p->locker, NULL);
    mmMemset(&p->stat, 0, sizeof(struct Stat));
}
MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Destroy(struct mmZkrbPathElem* p)
{
    p->zk_path = NULL;
    p->unique_id = 0;
    mmSpinlock_Destroy(&p->locker);
    mmMemset(&p->stat, 0, sizeof(struct Stat));
}

MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Lock(struct mmZkrbPathElem* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPathElem_Unlock(struct mmZkrbPathElem* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER void mmZkrbPath_Init(struct mmZkrbPath* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmString_Init(&p->path);
    mmString_Init(&p->host);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmZkrbPathCallback_Init(&p->callback);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->timeout = MM_ZKRB_PATH_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;

    mmString_Assigns(&p->path, "/zkrb");
    mmString_Assigns(&p->host, "127.0.0.1:2181,");

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Destroy(struct mmZkrbPath* p)
{
    mmZkrbPath_Clear(p);
    mmZkrbPath_Abandon(p);
    //
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->host);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmZkrbPathCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->timeout = 0;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrbPath_Start(struct mmZkrbPath* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmZkrbPath_Watcher(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Interrupt(struct mmZkrbPath* p)
{
    p->state = MM_TS_CLOSED;
    mmZkrbPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Shutdown(struct mmZkrbPath* p)
{
    p->state = MM_TS_FINISH;
    mmZkrbPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Join(struct mmZkrbPath* p)
{

}

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Watcher(struct mmZkrbPath* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // try abandon first.
    mmZkrbPath_Abandon(p);
    if (0 != mmString_Size(&p->host))
    {
        // init a new zk handle.
        p->zkhandle = zookeeper_init(mmString_CStr(&p->host), __static_mmZkrbPath_Watcher, p->timeout, 0, p, 0);
        if (NULL == p->zkhandle)
        {
            mmLogger_LogE(gLogger, "%s %d init zookeeper servers failure.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d init zookeeper servers success.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d the host is empty.", __FUNCTION__, __LINE__);
    }
}
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Abandon(struct mmZkrbPath* p)
{
    if (NULL != p->zkhandle)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        zookeeper_close(p->zkhandle);
        p->zkhandle = NULL;
        mmLogger_LogI(gLogger, "%s %d close zookeeper servers success.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_ZOOKEEPER void mmZkrbPath_Lock(struct mmZkrbPath* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Unlock(struct mmZkrbPath* p)
{
    mmSpinlock_Unlock(&p->locker);
}

// assign net callback.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_SetCallback(struct mmZkrbPath* p, struct mmZkrbPathCallback* zkrb_path_callback)
{
    assert(NULL != zkrb_path_callback && "you can not assign null zkrb_path_callback.");
    p->callback = *zkrb_path_callback;
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_SetPath(struct mmZkrbPath* p, const char* path)
{
    assert(NULL != path && "you can not assign null path.");
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_SetHost(struct mmZkrbPath* p, const char* host)
{
    assert(NULL != host && "you can not assign null host.");
    mmString_Assigns(&p->host, host);
}

// this function will check and try add watcher when the elem size is zero.
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Update(struct mmZkrbPath* p)
{
    if (0 == p->rbtree.size)
    {
        __static_mmZkrbPath_Awexists(p);
    }
}

MM_EXPORT_ZOOKEEPER struct mmZkrbPathElem* mmZkrbPath_Add(struct mmZkrbPath* p, mmUInt32_t unique_id)
{
    struct mmZkrbPathElem* e = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    e = mmZkrbPath_Get(p, unique_id);
    if (NULL == e)
    {
        e = (struct mmZkrbPathElem*)mmMalloc(sizeof(struct mmZkrbPathElem));
        mmZkrbPathElem_Init(e);
        e->unique_id = unique_id;
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU32Vpt_Set(&p->rbtree, unique_id, e);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Rmv(struct mmZkrbPath* p, mmUInt32_t unique_id)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU32Vpt_GetIterator(&p->rbtree, unique_id);
    if (NULL != it)
    {
        struct mmZkrbPathElem* e = (struct mmZkrbPathElem*)(it->v);
        mmZkrbPathElem_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrbPathElem_Unlock(e);
        mmZkrbPathElem_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_ZOOKEEPER struct mmZkrbPathElem* mmZkrbPath_Get(struct mmZkrbPath* p, mmUInt32_t unique_id)
{
    struct mmZkrbPathElem* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmZkrbPathElem*)mmRbtreeU32Vpt_Get(&p->rbtree, unique_id);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER struct mmZkrbPathElem* mmZkrbPath_GetInstance(struct mmZkrbPath* p, mmUInt32_t unique_id)
{
    struct mmZkrbPathElem* e = mmZkrbPath_Get(p, unique_id);
    if (NULL == e)
    {
        e = mmZkrbPath_Add(p, unique_id);
    }
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrbPath_Clear(struct mmZkrbPath* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmZkrbPathElem* e = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        e = (struct mmZkrbPathElem*)(it->v);
        n = mmRb_Next(n);
        mmZkrbPathElem_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrbPathElem_Unlock(e);
        mmZkrbPathElem_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}

static void __static_mmZkrbPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPath* _zkrb_path = (struct mmZkrbPath*)(watcherCtx);
    if (ZOO_SESSION_EVENT == type)
    {
        if (ZOO_CONNECTED_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connected.", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.NetConnect))(_zkrb_path);
            __static_mmZkrbPath_Awexists(_zkrb_path);
        }
        else if (ZOO_EXPIRED_SESSION_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s session expired.", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.NetExpired))(_zkrb_path);
            mmZkrbPath_Watcher(_zkrb_path);
        }
        else if (ZOO_CONNECTING_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connecting.", __FUNCTION__, __LINE__, path);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d event happened.type:%d state:%d path:%s.", __FUNCTION__, __LINE__, type, state, path);
        }
    }
}

static void __static_mmZkrbPath_UniqueIdAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPathElem* _zkrb_path_elem = (struct mmZkrbPathElem*)(data);
    struct mmZkrbPath* _zkrb_path = _zkrb_path_elem->zk_path;
    if (ZOK == rc)
    {
        if (0 < value_len)
        {
            if (_zkrb_path_elem->stat.mtime != stat->mtime)
            {
                mmMemcpy(&_zkrb_path_elem->stat, stat, sizeof(struct Stat));
                mmLogger_LogV(gLogger, "%s %d value_len:%d.", __FUNCTION__, __LINE__, value_len);
                (*(_zkrb_path->callback.PathElemUpdated))(_zkrb_path, _zkrb_path_elem, value, 0, value_len);
            }
        }
    }
}
static void __static_mmZkrbPath_UniqueIdAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx)
{
    struct mmZkrbPathElem* _zkrb_path_elem = (struct mmZkrbPathElem*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrbPath_UniqueIdAwget(_zkrb_path_elem);
}

static void __static_mmZkrbPath_UniqueIdAwget(struct mmZkrbPathElem* _elem)
{
    struct mmZkrbPath* _zkrb_path = _elem->zk_path;
    int ret = 0;
    char path[128];
    mmSprintf(path, "%s/" MM_ZKRB_FORMAT_1, mmString_CStr(&_zkrb_path->path), _elem->unique_id);
    ret = zoo_awget(_zkrb_path->zkhandle, path, __static_mmZkrbPath_UniqueIdAwgetWatcher, _elem, __static_mmZkrbPath_UniqueIdAwgetDataCompletion, _elem);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrbPath_UniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPathElem* _zkrb_path_elem = (struct mmZkrbPathElem*)(data);
    if (ZOK == rc)
    {
        // awget path/unique_id
        __static_mmZkrbPath_UniqueIdAwget(_zkrb_path_elem);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrbPath_UniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPathElem* _zkrb_path_elem = (struct mmZkrbPathElem*)(watcherCtx);
    struct mmZkrbPath* _zkrb_path = _zkrb_path_elem->zk_path;
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.PathElemDeleted))(_zkrb_path, _zkrb_path_elem);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.PathElemCreated))(_zkrb_path, _zkrb_path_elem);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.PathElemChanged))(_zkrb_path, _zkrb_path_elem);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrbPath_UniqueIdAwexists(_zkrb_path_elem);
}
static void __static_mmZkrbPath_UniqueIdAwexists(struct mmZkrbPathElem* p)
{
    char path[128];
    int ret = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPath* _zkrb_path = p->zk_path;
    mmSprintf(path, "%s/" MM_ZKRB_FORMAT_1, mmString_CStr(&_zkrb_path->path), p->unique_id);
    ret = zoo_awexists(_zkrb_path->zkhandle, path, __static_mmZkrbPath_UniqueIdAwexistsWatcher, p, __static_mmZkrbPath_UniqueIdAwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrbPath_AwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPath* _zkrb_path = (struct mmZkrbPath*)(data);
    if (ZOK == rc)
    {
        if (NULL != strings)
        {
            int32_t i = 0;
            for (i = 0; i < strings->count; ++i)
            {
                mmUInt32_t _unique_id = 0;
                struct mmZkrbPathElem* _zkrb_path_elem = NULL;
                mmSscanf(strings->data[i], MM_ZKRB_FORMAT_1, &_unique_id);
                _zkrb_path_elem = mmZkrbPath_GetInstance(_zkrb_path, _unique_id);
                _zkrb_path_elem->zk_path = _zkrb_path;
                _zkrb_path_elem->unique_id = _unique_id;
                __static_mmZkrbPath_UniqueIdAwexists(_zkrb_path_elem);
            }
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrbPath_AwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmZkrbPath* _zkrb_path = (struct mmZkrbPath*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrbPath_AwgetChildren(_zkrb_path);
}
static void __static_mmZkrbPath_AwgetChildren(struct mmZkrbPath* p)
{
    int ret = zoo_awget_children(p->zkhandle, mmString_CStr(&p->path), __static_mmZkrbPath_AwgetChildrenWatcher, p, __static_mmZkrbPath_AwgetChildrenStringsCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrbPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPath* _zkrb_path = (struct mmZkrbPath*)(data);
    if (ZOK == rc)
    {
        // reset watch again.
        __static_mmZkrbPath_AwgetChildren(_zkrb_path);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrbPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrbPath* _zkrb_path = (struct mmZkrbPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.PathDeleted))(_zkrb_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.PathCreated))(_zkrb_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrb_path->callback.PathChanged))(_zkrb_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrbPath_Awexists(_zkrb_path);
}
static void __static_mmZkrbPath_Awexists(struct mmZkrbPath* p)
{
    int ret = zoo_awexists(p->zkhandle, mmString_CStr(&p->path), __static_mmZkrbPath_AwexistsWatcher, p, __static_mmZkrbPath_AwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

