/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmZookeeperZkwmPath.h"
#include "core/mmLogger.h"
#include "core/mmAtoi.h"
#include "core/mmThread.h"

static void __static_mmZkwmPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);

static void __static_mmZkwmPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwmPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path
static void __static_mmZkwmPath_Awexists(struct mmZkwmPath* p);

static void __static_mmZkwmPath_AcreateStringCompletion(int rc, const char *value, const void *data);
// acreate path
static void __static_mmZkwmPath_Acreate(struct mmZkwmPath* p);

static void __static_mmZkwmPath_NumberIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwmPath_NumberIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path/number_id
static void __static_mmZkwmPath_NumberIdAwexists(struct mmZkwmPath* p);

static void __static_mmZkwmPath_NumberIdAcreateCompletion(int rc, const char *value, const void *data);
// acreate path/number_id
static void __static_mmZkwmPath_NumberIdAcreate(struct mmZkwmPath* p);

static void __static_mmZkwmPath_NumberIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data);
static void __static_mmZkwmPath_NumberIdAset(struct mmZkwmPath* p);

static void __static_mmZkwmPath_NumberIdUniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwmPath_NumberIdUniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path/number_id/unique_id
static void __static_mmZkwmPath_NumberIdUniqueIdAwexists(struct mmZkwmPath* p);

static void __static_mmZkwmPath_NumberIdUniqueIdAcreateCompletion(int rc, const char *value, const void *data);
// acreate path/number_id/unique_id
static void __static_mmZkwmPath_NumberIdUniqueIdAcreate(struct mmZkwmPath* p);

static void __static_mmZkwmPath_NumberIdUniqueIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data);
// aset path/number_id/unique_id
static void __static_mmZkwmPath_NumberIdUniqueIdAset(struct mmZkwmPath* p);

static void __static_mmZkwmPath_Event(struct mmZkwmPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkwmPathCallback_Init(struct mmZkwmPathCallback* p)
{
    p->Created = &__static_mmZkwmPath_Event;
    p->Deleted = &__static_mmZkwmPath_Event;
    p->Changed = &__static_mmZkwmPath_Event;
    p->NetConnect = &__static_mmZkwmPath_Event;
    p->NetExpired = &__static_mmZkwmPath_Event;
    p->obj = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkwmPathCallback_Destroy(struct mmZkwmPathCallback* p)
{
    p->Created = &__static_mmZkwmPath_Event;
    p->Deleted = &__static_mmZkwmPath_Event;
    p->Changed = &__static_mmZkwmPath_Event;
    p->NetConnect = &__static_mmZkwmPath_Event;
    p->NetExpired = &__static_mmZkwmPath_Event;
    p->obj = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Init(struct mmZkwmPath* p)
{
    mmString_Init(&p->path);
    mmString_Init(&p->host);
    mmString_Init(&p->shard_buffer);
    mmString_Init(&p->value_buffer);
    mmZkwmPathCallback_Init(&p->callback);
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->number_id = 0;
    p->timeout = MM_ZKWM_PATH_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;

    mmString_Assigns(&p->path, "/zkwm");
    mmString_Assigns(&p->host, "127.0.0.1:2181,");
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Destroy(struct mmZkwmPath* p)
{
    mmZkwmPath_Abandon(p);
    //
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->host);
    mmString_Destroy(&p->shard_buffer);
    mmString_Destroy(&p->value_buffer);
    mmZkwmPathCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->number_id = 0;
    p->timeout = 0;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Start(struct mmZkwmPath* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmZkwmPath_Watcher(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Interrupt(struct mmZkwmPath* p)
{
    p->state = MM_TS_CLOSED;
    mmZkwmPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Shutdown(struct mmZkwmPath* p)
{
    p->state = MM_TS_FINISH;
    mmZkwmPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Join(struct mmZkwmPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetUniqueId(struct mmZkwmPath* p, mmUInt32_t unique_id)
{
    p->unique_id = unique_id;
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetNumberId(struct mmZkwmPath* p, mmUInt64_t number_id)
{
    p->number_id = number_id;
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetPath(struct mmZkwmPath* p, const char* path)
{
    assert(NULL != path && "you can not assign null path.");
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetHost(struct mmZkwmPath* p, const char* host)
{
    assert(NULL != host && "you can not assign null host.");
    mmString_Assigns(&p->host, host);
}

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Watcher(struct mmZkwmPath* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // try abandon first.
    mmZkwmPath_Abandon(p);
    if (0 != mmString_Size(&p->host))
    {
        // init a new zk handle.
        p->zkhandle = zookeeper_init(mmString_CStr(&p->host), __static_mmZkwmPath_Watcher, p->timeout, 0, p, 0);
        if (NULL == p->zkhandle)
        {
            mmLogger_LogE(gLogger, "%s %d init zookeeper servers failure.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d init zookeeper servers success.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d the host is empty.", __FUNCTION__, __LINE__);
    }
}
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Abandon(struct mmZkwmPath* p)
{
    if (NULL != p->zkhandle)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        zookeeper_close(p->zkhandle);
        p->zkhandle = NULL;
        mmLogger_LogI(gLogger, "%s %d close zookeeper servers success.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Lock(struct mmZkwmPath* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Unlock(struct mmZkwmPath* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Commit(struct mmZkwmPath* p)
{
    __static_mmZkwmPath_Awexists(p);
}

static void __static_mmZkwmPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(watcherCtx);
    if (ZOO_SESSION_EVENT == type)
    {
        if (ZOO_CONNECTED_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connected.", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.NetConnect))(_zkwm_path);
            __static_mmZkwmPath_Awexists(_zkwm_path);
        }
        else if (ZOO_EXPIRED_SESSION_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s session expired.", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.NetExpired))(_zkwm_path);
            mmZkwmPath_Watcher(_zkwm_path);
        }
        else if (ZOO_CONNECTING_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connecting.", __FUNCTION__, __LINE__, path);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d event happened.type:%d state:%d path:%s.", __FUNCTION__, __LINE__, type, state, path);
        }
    }
}

static void __static_mmZkwmPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.Deleted))(_zkwm_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.Created))(_zkwm_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.Changed))(_zkwm_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwmPath_Awexists(_zkwm_path);
}
static void __static_mmZkwmPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/number_id
        __static_mmZkwmPath_NumberIdAwexists(_zkwm_path);
    }
    else if (ZNONODE == rc)
    {
        // create path.
        __static_mmZkwmPath_Acreate(_zkwm_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwmPath_Awexists(struct mmZkwmPath* p)
{
    int ret = zoo_awexists(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        __static_mmZkwmPath_AwexistsWatcher, 
        p, 
        __static_mmZkwmPath_AwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_AcreateStringCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/number_id
        __static_mmZkwmPath_NumberIdAwexists(_zkwm_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // awexists path/number_id
        __static_mmZkwmPath_NumberIdAwexists(_zkwm_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwmPath_Acreate(struct mmZkwmPath* p)
{
    // not ZOO_SEQUENCE
    int ret = zoo_acreate(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        "", 
        0, 
        &ZOO_OPEN_ACL_UNSAFE, 
        0, 
        __static_mmZkwmPath_AcreateStringCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_NumberIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.Deleted))(_zkwm_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.Created))(_zkwm_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkwm_path->callback.Changed))(_zkwm_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwmPath_NumberIdAwexists(_zkwm_path);
}
static void __static_mmZkwmPath_NumberIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAwexists(_zkwm_path);
        // aset path/number_id
        __static_mmZkwmPath_NumberIdAset(_zkwm_path);
    }
    else if (ZNONODE == rc)
    {
        // acreate path/number_id
        __static_mmZkwmPath_NumberIdAcreate(_zkwm_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// awexists path/number_id
static void __static_mmZkwmPath_NumberIdAwexists(struct mmZkwmPath* p)
{
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWM_FORMAT_1, mmString_CStr(&p->path), p->number_id);
    ret = zoo_awexists(
        p->zkhandle, 
        path, 
        __static_mmZkwmPath_NumberIdAwexistsWatcher, 
        p, 
        __static_mmZkwmPath_NumberIdAwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_NumberIdAcreateCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAwexists(_zkwm_path);
        // aset path/number_id
        __static_mmZkwmPath_NumberIdAset(_zkwm_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // awexists path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAwexists(_zkwm_path);
        // aset path/number_id
        __static_mmZkwmPath_NumberIdAset(_zkwm_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// acreate path/number_id
static void __static_mmZkwmPath_NumberIdAcreate(struct mmZkwmPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWM_FORMAT_1, mmString_CStr(&p->path), p->number_id);
    ret = zoo_acreate(
        p->zkhandle, 
        path, 
        mmString_CStr(&p->shard_buffer), 
        (int)mmString_Size(&p->shard_buffer),
        &ZOO_OPEN_ACL_UNSAFE, 
        0, 
        __static_mmZkwmPath_NumberIdAcreateCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_NumberIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (ZOK != rc)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// aset path/number_id
static void __static_mmZkwmPath_NumberIdAset(struct mmZkwmPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWM_FORMAT_1, mmString_CStr(&p->path), p->number_id);
    ret = zoo_aset(
        p->zkhandle, 
        path, 
        mmString_CStr(&p->shard_buffer), 
        (int)mmString_Size(&p->shard_buffer),
        -1, 
        __static_mmZkwmPath_NumberIdAsetStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_NumberIdUniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    // struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(watcherCtx);
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwmPath_NumberIdUniqueIdAwexists(_zkwm_path);
}
static void __static_mmZkwmPath_NumberIdUniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(data);
    if (ZOK == rc)
    {
        // aset path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAset(_zkwm_path);
    }
    else if (ZNONODE == rc)
    {
        // acreate path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAcreate(_zkwm_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// awexists path/number_id/unique_id
static void __static_mmZkwmPath_NumberIdUniqueIdAwexists(struct mmZkwmPath* p)
{
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWM_FORMAT_1 "/" MM_ZKWM_FORMAT_2, mmString_CStr(&p->path), p->number_id, p->unique_id);
    ret = zoo_awexists(
        p->zkhandle, 
        path, 
        __static_mmZkwmPath_NumberIdUniqueIdAwexistsWatcher, 
        p, 
        __static_mmZkwmPath_NumberIdUniqueIdAwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_NumberIdUniqueIdAcreateCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwmPath* _zkwm_path = (struct mmZkwmPath*)(data);
    if (ZOK == rc)
    {
        // aset path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAset(_zkwm_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // aset path/number_id/unique_id
        __static_mmZkwmPath_NumberIdUniqueIdAset(_zkwm_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// acreate path/number_id/unique_id
static void __static_mmZkwmPath_NumberIdUniqueIdAcreate(struct mmZkwmPath* p)
{
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWM_FORMAT_1 "/" MM_ZKWM_FORMAT_2, mmString_CStr(&p->path), p->number_id, p->unique_id);
    ret = zoo_acreate(
        p->zkhandle, 
        path, 
        mmString_CStr(&p->value_buffer), 
        (int)mmString_Size(&p->value_buffer), 
        &ZOO_OPEN_ACL_UNSAFE, ZOO_EPHEMERAL, 
        __static_mmZkwmPath_NumberIdUniqueIdAcreateCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwmPath_NumberIdUniqueIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (ZOK != rc)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
// aset path/number_id/unique_id
static void __static_mmZkwmPath_NumberIdUniqueIdAset(struct mmZkwmPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWM_FORMAT_1 "/" MM_ZKWM_FORMAT_2, mmString_CStr(&p->path), p->number_id, p->unique_id);
    ret = zoo_aset(
        p->zkhandle, 
        path, 
        mmString_CStr(&p->value_buffer), 
        (int)mmString_Size(&p->value_buffer), 
        -1, 
        __static_mmZkwmPath_NumberIdUniqueIdAsetStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

