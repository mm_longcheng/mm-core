/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmZookeeperZkwbPath.h"
#include "core/mmLogger.h"
#include "core/mmAtoi.h"
#include "core/mmThread.h"

static void __static_mmZkwbPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);

static void __static_mmZkwbPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwbPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path
static void __static_mmZkwbPath_Awexists(struct mmZkwbPath* p);

static void __static_mmZkwbPath_AcreateStringCompletion(int rc, const char *value, const void *data);
// acreate path
static void __static_mmZkwbPath_Acreate(struct mmZkwbPath* p);

static void __static_mmZkwbPath_UniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkwbPath_UniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
// awexists path/unique_id
static void __static_mmZkwbPath_UniqueIdAwexists(struct mmZkwbPath* p);

static void __static_mmZkwbPath_UniqueIdAcreateStringCompletion(int rc, const char *value, const void *data);
// acreate path/unique_id
static void __static_mmZkwbPath_UniqueIdAcreate(struct mmZkwbPath* p);

static void __static_mmZkwbPath_UniqueIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data);
// aset path/unique_id
static void __static_mmZkwbPath_UniqueIdAset(struct mmZkwbPath* p);

static void __static_mmZkwbPath_Event(struct mmZkwbPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkwbPathCallback_Init(struct mmZkwbPathCallback* p)
{
    p->Created = &__static_mmZkwbPath_Event;
    p->Deleted = &__static_mmZkwbPath_Event;
    p->Changed = &__static_mmZkwbPath_Event;
    p->NetConnect = &__static_mmZkwbPath_Event;
    p->NetExpired = &__static_mmZkwbPath_Event;
    p->obj = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkwbPathCallback_Destroy(struct mmZkwbPathCallback* p)
{
    p->Created = &__static_mmZkwbPath_Event;
    p->Deleted = &__static_mmZkwbPath_Event;
    p->Changed = &__static_mmZkwbPath_Event;
    p->NetConnect = &__static_mmZkwbPath_Event;
    p->NetExpired = &__static_mmZkwbPath_Event;
    p->obj = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Init(struct mmZkwbPath* p)
{
    mmString_Init(&p->path);
    mmString_Init(&p->host);
    mmString_Init(&p->value_buffer);
    mmZkwbPathCallback_Init(&p->callback);
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->timeout = MM_ZKWB_PATH_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;

    mmString_Assigns(&p->path, "/zkwb");
    mmString_Assigns(&p->host, "127.0.0.1:2181,");
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Destroy(struct mmZkwbPath* p)
{
    mmZkwbPath_Abandon(p);
    //
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->host);
    mmString_Destroy(&p->value_buffer);
    mmZkwbPathCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->timeout = 0;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Start(struct mmZkwbPath* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmZkwbPath_Watcher(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Interrupt(struct mmZkwbPath* p)
{
    p->state = MM_TS_CLOSED;
    mmZkwbPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Shutdown(struct mmZkwbPath* p)
{
    p->state = MM_TS_FINISH;
    mmZkwbPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Join(struct mmZkwbPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkwbPath_SetUniqueId(struct mmZkwbPath* p, mmUInt32_t unique_id)
{
    p->unique_id = unique_id;
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_SetPath(struct mmZkwbPath* p, const char* path)
{
    assert(NULL != path && "you can not assign null path.");
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_SetHost(struct mmZkwbPath* p, const char* host)
{
    assert(NULL != host && "you can not assign null host.");
    mmString_Assigns(&p->host, host);
}

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Watcher(struct mmZkwbPath* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // try abandon first.
    mmZkwbPath_Abandon(p);
    if (0 != mmString_Size(&p->host))
    {
        // init a new zk handle.
        p->zkhandle = zookeeper_init(mmString_CStr(&p->host), __static_mmZkwbPath_Watcher, p->timeout, 0, p, 0);
        if (NULL == p->zkhandle)
        {
            mmLogger_LogE(gLogger, "%s %d init zookeeper servers failure.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d init zookeeper servers success.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d the host is empty.", __FUNCTION__, __LINE__);
    }
}
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Abandon(struct mmZkwbPath* p)
{
    if (NULL != p->zkhandle)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        zookeeper_close(p->zkhandle);
        p->zkhandle = NULL;
        mmLogger_LogI(gLogger, "%s %d close zookeeper servers success.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Lock(struct mmZkwbPath* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkwbPath_Unlock(struct mmZkwbPath* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER void mmZkwbPath_Commit(struct mmZkwbPath* p)
{
    __static_mmZkwbPath_Awexists(p);
}

static void __static_mmZkwbPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(watcherCtx);
    if (ZOO_SESSION_EVENT == type)
    {
        if (ZOO_CONNECTED_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connected.", __FUNCTION__, __LINE__, path);
            (*(_zkwb_path->callback.NetConnect))(_zkwb_path);
            __static_mmZkwbPath_Awexists(_zkwb_path);
        }
        else if (ZOO_EXPIRED_SESSION_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s session expired.", __FUNCTION__, __LINE__, path);
            (*(_zkwb_path->callback.NetExpired))(_zkwb_path);
            mmZkwbPath_Watcher(_zkwb_path);
        }
        else if (ZOO_CONNECTING_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connecting.", __FUNCTION__, __LINE__, path);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d event happened.type:%d state:%d path:%s.", __FUNCTION__, __LINE__, type, state, path);
        }
    }
}

static void __static_mmZkwbPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkwb_path->callback.Deleted))(_zkwb_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkwb_path->callback.Created))(_zkwb_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkwb_path->callback.Changed))(_zkwb_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwbPath_Awexists(_zkwb_path);
}
static void __static_mmZkwbPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(data);
    if (ZOK == rc)
    {
        // awexists path/unique_id.
        __static_mmZkwbPath_UniqueIdAwexists(_zkwb_path);
    }
    else if (ZNONODE == rc)
    {
        // create path.
        __static_mmZkwbPath_Acreate(_zkwb_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwbPath_Awexists(struct mmZkwbPath* p)
{
    int ret = zoo_awexists(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        __static_mmZkwbPath_AwexistsWatcher, 
        p, 
        __static_mmZkwbPath_AwexistsStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwbPath_AcreateStringCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(data);
    if (ZOK == rc)
    {
        // set.
        __static_mmZkwbPath_UniqueIdAset(_zkwb_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // set.
        __static_mmZkwbPath_UniqueIdAset(_zkwb_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwbPath_Acreate(struct mmZkwbPath* p)
{
    // not ZOO_SEQUENCE
    int ret = zoo_acreate(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        "", 
        0, 
        &ZOO_OPEN_ACL_UNSAFE, 
        0, 
        __static_mmZkwbPath_AcreateStringCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwbPath_UniqueIdAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    // struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(watcherCtx);
    // not need watcher children.
    // commit not need reset watcher again.if watcher will case dead loop.
    // __static_mmZkwbPath_UniqueIdAwexists(_zkwb_path);
}
static void __static_mmZkwbPath_UniqueIdAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(data);
    if (ZOK == rc)
    {
        // set.
        __static_mmZkwbPath_UniqueIdAset(_zkwb_path);
    }
    else if (ZNONODE == rc)
    {
        // create path/unique_id.
        __static_mmZkwbPath_UniqueIdAcreate(_zkwb_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwbPath_UniqueIdAwexists(struct mmZkwbPath* p)
{
    char key[128];
    int ret = 0;
    mmSprintf(key, "%s/" MM_ZKWB_FORMAT_1, mmString_CStr(&p->path), p->unique_id);
    ret = zoo_awexists(
        p->zkhandle, 
        mmString_CStr(&p->path), 
        __static_mmZkwbPath_UniqueIdAwexistsWatcher, 
        p, 
        __static_mmZkwbPath_UniqueIdAwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwbPath_UniqueIdAcreateStringCompletion(int rc, const char *value, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(data);
    if (ZOK == rc)
    {
        // set.
        __static_mmZkwbPath_UniqueIdAset(_zkwb_path);
    }
    else if (ZNODEEXISTS == rc)
    {
        // set.
        __static_mmZkwbPath_UniqueIdAset(_zkwb_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwbPath_UniqueIdAcreate(struct mmZkwbPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWB_FORMAT_1, mmString_CStr(&p->path), p->unique_id);
    ret = zoo_acreate(
        p->zkhandle, 
        path, 
        mmString_Data(&p->value_buffer), 
        (int)mmString_Size(&p->value_buffer), 
        &ZOO_OPEN_ACL_UNSAFE, 
        ZOO_EPHEMERAL, 
        __static_mmZkwbPath_UniqueIdAcreateStringCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkwbPath_UniqueIdAsetStatCompletion(int rc, const struct Stat *stat, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkwbPath* _zkwb_path = (struct mmZkwbPath*)(data);
    if (ZOK == rc)
    {
        // success.
    }
    else if (ZNONODE == rc)
    {
        __static_mmZkwbPath_UniqueIdAcreate(_zkwb_path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkwbPath_UniqueIdAset(struct mmZkwbPath* p)
{
    char path[128] = { 0 };
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKWB_FORMAT_1, mmString_CStr(&p->path), p->unique_id);
    ret = zoo_aset(
        p->zkhandle, 
        path, 
        mmString_Data(&p->value_buffer), 
        (int)mmString_Size(&p->value_buffer), 
        -1, 
        __static_mmZkwbPath_UniqueIdAsetStatCompletion, 
        p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

