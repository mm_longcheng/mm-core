/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmZookeeperZkwmPath_h__
#define __mmZookeeperZkwmPath_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "zookeeper.h"

#include "zookeeper/mmZookeeperExport.h"

#include "core/mmPrefix.h"

#define MM_ZKWM_FORMAT_1 "%016" PRIX64
#define MM_ZKWM_FORMAT_2 "%u"
// connect invalid timeout.default is 3s(3000ms).
#define MM_ZKWM_PATH_TIMEOUT 3000

// /path
//       /0               /ZOO_EPHEMERAL
//       /02064000020640FF/2002000 buffer
//       /02065000020650FF/2002000 buffer

struct mmZkwmPath;
struct mmZkwmPathElem;

typedef void(*mmZkwmPathEventFunc)(struct mmZkwmPath* p);

struct mmZkwmPathCallback
{
    mmZkwmPathEventFunc Created;
    mmZkwmPathEventFunc Deleted;
    mmZkwmPathEventFunc Changed;
    mmZkwmPathEventFunc NetConnect;
    mmZkwmPathEventFunc NetExpired;
    void* obj;
};

MM_EXPORT_ZOOKEEPER void mmZkwmPathCallback_Init(struct mmZkwmPathCallback* p);
MM_EXPORT_ZOOKEEPER void mmZkwmPathCallback_Destroy(struct mmZkwmPathCallback* p);

struct mmZkwmPath
{
    // zk path for notify.
    struct mmString path;
    // zk cluster "host:port,host:port,"
    struct mmString host;
    // value for buffer.
    struct mmString shard_buffer;
    // value for buffer.
    struct mmString value_buffer;
    // event for callback.
    struct mmZkwmPathCallback callback;
    mmAtomic_t locker;
    mmUInt32_t unique_id;
    // module shard [mid_r << 32 | mid_l].
    mmUInt64_t number_id;
    // connect invalid timeout.default is MM_ZKWM_PATH_TIMEOUT.
    mmMSec_t timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // zk handle.
    zhandle_t* zkhandle;
};

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Init(struct mmZkwmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Destroy(struct mmZkwmPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Start(struct mmZkwmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Interrupt(struct mmZkwmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Shutdown(struct mmZkwmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Join(struct mmZkwmPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetUniqueId(struct mmZkwmPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetNumberId(struct mmZkwmPath* p, mmUInt64_t number_id);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetPath(struct mmZkwmPath* p, const char* path);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_SetHost(struct mmZkwmPath* p, const char* host);

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Watcher(struct mmZkwmPath* p);
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Abandon(struct mmZkwmPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Lock(struct mmZkwmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkwmPath_Unlock(struct mmZkwmPath* p);

MM_EXPORT_ZOOKEEPER void mmZkwmPath_Commit(struct mmZkwmPath* p);

#include "core/mmSuffix.h"

#endif//__mmZookeeperZkwmPath_h__
