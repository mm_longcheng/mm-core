/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmZookeeperZkrmPath_h__
#define __mmZookeeperZkrmPath_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmRbtreeU32.h"
#include "container/mmRbtreeU64.h"

#include "zookeeper.h"

#include "zookeeper/mmZookeeperExport.h"

#include "core/mmPrefix.h"

#define MM_ZKRM_FORMAT_1 "%016" PRIX64
#define MM_ZKRM_FORMAT_2 "%u"
// connect invalid timeout.default is 3s(3000ms).
#define MM_ZKRM_PATH_TIMEOUT 3000

// /path
//       /0               /ZOO_EPHEMERAL
//       /02064000020640FF/2002000 buffer
//       /02065000020650FF/2002000 buffer

struct mmZkrmPath;
struct mmZkrmPathElem;
struct mmZkrmPathUnit;

typedef void(*mmZkrmPathElemUnitValueFunc)(struct mmZkrmPath* p, struct mmZkrmPathElem* e, struct mmZkrmPathUnit* u, const char* buffer, int offset, int length);
typedef void(*mmZkrmPathElemUnitEventFunc)(struct mmZkrmPath* p, struct mmZkrmPathElem* e, struct mmZkrmPathUnit* u);
typedef void(*mmZkrmPathElemValueFunc)(struct mmZkrmPath* p, struct mmZkrmPathElem* e, const char* buffer, int offset, int length);
typedef void(*mmZkrmPathElemEvent)(struct mmZkrmPath* p, struct mmZkrmPathElem* e);
typedef void(*mmZkrmPathEventFunc)(struct mmZkrmPath* p);

struct mmZkrmPathCallback
{
    mmZkrmPathElemUnitValueFunc PathElemUnitUpdated;
    mmZkrmPathElemUnitEventFunc PathElemUnitCreated;
    mmZkrmPathElemUnitEventFunc PathElemUnitDeleted;
    mmZkrmPathElemUnitEventFunc PathElemUnitChanged;
    mmZkrmPathElemValueFunc PathElemUpdated;
    mmZkrmPathElemEvent PathElemCreated;
    mmZkrmPathElemEvent PathElemDeleted;
    mmZkrmPathElemEvent PathElemChanged;
    mmZkrmPathEventFunc PathCreated;
    mmZkrmPathEventFunc PathDeleted;
    mmZkrmPathEventFunc PathChanged;
    mmZkrmPathEventFunc NetConnect;
    mmZkrmPathEventFunc NetExpired;
    void* obj;
};

MM_EXPORT_ZOOKEEPER void mmZkrmPathCallback_Init(struct mmZkrmPathCallback* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPathCallback_Destroy(struct mmZkrmPathCallback* p);

struct mmZkrmPathUnit
{
    struct Stat stat;
    mmAtomic_t locker;
    mmUInt32_t unique_id;
    // parent node.
    struct mmZkrmPath* zk_path;
    // parent node.
    struct mmZkrmPathElem* zk_path_elem;
};

MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Init(struct mmZkrmPathUnit* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Destroy(struct mmZkrmPathUnit* p);

MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Lock(struct mmZkrmPathUnit* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Unlock(struct mmZkrmPathUnit* p);

struct mmZkrmPathElem
{
    struct Stat stat;
    // rb tree for unique_id <--> mmZkrmPathUnit.
    struct mmRbtreeU32Vpt rbtree;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    mmUInt64_t unique_id;
    // parent node.
    struct mmZkrmPath* zk_path;
};

MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Init(struct mmZkrmPathElem* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Destroy(struct mmZkrmPathElem* p);

MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Lock(struct mmZkrmPathElem* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Unlock(struct mmZkrmPathElem* p);

MM_EXPORT_ZOOKEEPER struct mmZkrmPathUnit* mmZkrmPathElem_Add(struct mmZkrmPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Rmv(struct mmZkrmPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrmPathUnit* mmZkrmPathElem_Get(struct mmZkrmPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrmPathUnit* mmZkrmPathElem_GetInstance(struct mmZkrmPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Clear(struct mmZkrmPathElem* p);

struct mmZkrmPath
{
    // zk path for notify.
    struct mmString path;
    // zk cluster "host:port,host:port,"
    struct mmString host;
    // rb tree for unique_id <--> mmZkrmPathElem.
    struct mmRbtreeU64Vpt rbtree;
    // event for callback.
    struct mmZkrmPathCallback callback;
    // the locker for only mm_mt_contact_get_instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    // connect invalid timeout.default is MM_ZKRM_PATH_TIMEOUT.
    mmMSec_t timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // zk handle.
    zhandle_t* zkhandle;
};

MM_EXPORT_ZOOKEEPER void mmZkrmPath_Init(struct mmZkrmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Destroy(struct mmZkrmPath* p);

MM_EXPORT_ZOOKEEPER void mmZkrmPath_Start(struct mmZkrmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Interrupt(struct mmZkrmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Shutdown(struct mmZkrmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Join(struct mmZkrmPath* p);

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Watcher(struct mmZkrmPath* p);
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Abandon(struct mmZkrmPath* p);

MM_EXPORT_ZOOKEEPER void mmZkrmPath_Lock(struct mmZkrmPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Unlock(struct mmZkrmPath* p);

// assign net callback.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_SetCallback(struct mmZkrmPath* p, struct mmZkrmPathCallback* zkrm_path_callback);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_SetPath(struct mmZkrmPath* p, const char* path);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_SetHost(struct mmZkrmPath* p, const char* host);

// this function will check and try add watcher when the elem size is zero.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Update(struct mmZkrmPath* p);

MM_EXPORT_ZOOKEEPER struct mmZkrmPathElem* mmZkrmPath_Add(struct mmZkrmPath* p, mmUInt64_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Rmv(struct mmZkrmPath* p, mmUInt64_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrmPathElem* mmZkrmPath_Get(struct mmZkrmPath* p, mmUInt64_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrmPathElem* mmZkrmPath_GetInstance(struct mmZkrmPath* p, mmUInt64_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Clear(struct mmZkrmPath* p);

#include "core/mmSuffix.h"

#endif//__mmZookeeperZkrmPath_h__
