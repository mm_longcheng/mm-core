/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmZookeeperZkrpPath.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmThread.h"

static void __static_mmZkrpPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);

static void __static_mmZkrpPath_ElemUnitAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data);
static void __static_mmZkrpPath_ElemUnitAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx);
static void __static_mmZkrpPath_ElemUnitAwget(struct mmZkrpPathUnit* _zkrp_path_unit);

static void __static_mmZkrpPath_ElemUnitAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrpPath_ElemUnitAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrpPath_ElemUnitAwexists(struct mmZkrpPathUnit* p);

static void __static_mmZkrpPath_ElemAdeleteCompletion(int rc, const void *data);
static void __static_mmZkrpPath_ElemAdelete(struct mmZkrpPathElem* p);

static void __static_mmZkrpPath_ElemAwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data);
static void __static_mmZkrpPath_ElemAwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrpPath_ElemAwgetChildren(struct mmZkrpPathElem* p);

static void __static_mmZkrpPath_ElemAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data);
static void __static_mmZkrpPath_ElemAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx);
static void __static_mmZkrpPath_ElemAwget(struct mmZkrpPathElem* _zkrp_path_elem);

static void __static_mmZkrpPath_ElemAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrpPath_ElemAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrpPath_ElemAwexists(struct mmZkrpPathElem* p);

static void __static_mmZkrpPath_AwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data);
static void __static_mmZkrpPath_AwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrpPath_AwgetChildren(struct mmZkrpPath* p);

static void __static_mmZkrpPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrpPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrpPath_Awexists(struct mmZkrpPath* p);

static void __static_mmZkrpPath_ElemUnitValue(struct mmZkrpPath* p, struct mmZkrpPathElem* e, struct mmZkrpPathUnit* u, const char* buffer, int offset, int length)
{

}
static void __static_mmZkrpPath_ElemUnitEvent(struct mmZkrpPath* p, struct mmZkrpPathElem* e, struct mmZkrpPathUnit* u)
{

}
static void __static_mmZkrpPath_ElemValue(struct mmZkrpPath* p, struct mmZkrpPathElem* e, const char* buffer, int offset, int length)
{

}
static void __static_mmZkrpPath_ElemEvent(struct mmZkrpPath* p, struct mmZkrpPathElem* e)
{

}
static void __static_mmZkrpPath_Event(struct mmZkrpPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkrpPathCallback_Init(struct mmZkrpPathCallback* p)
{
    p->PathElemUnitUpdated = &__static_mmZkrpPath_ElemUnitValue;
    p->PathElemUnitCreated = &__static_mmZkrpPath_ElemUnitEvent;
    p->PathElemUnitDeleted = &__static_mmZkrpPath_ElemUnitEvent;
    p->PathElemUnitChanged = &__static_mmZkrpPath_ElemUnitEvent;
    p->PathElemUpdated = &__static_mmZkrpPath_ElemValue;
    p->PathElemCreated = &__static_mmZkrpPath_ElemEvent;
    p->PathElemDeleted = &__static_mmZkrpPath_ElemEvent;
    p->PathElemChanged = &__static_mmZkrpPath_ElemEvent;
    p->PathCreated = &__static_mmZkrpPath_Event;
    p->PathDeleted = &__static_mmZkrpPath_Event;
    p->PathChanged = &__static_mmZkrpPath_Event;
    p->NetConnect = &__static_mmZkrpPath_Event;
    p->NetExpired = &__static_mmZkrpPath_Event;
    p->obj = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathCallback_Destroy(struct mmZkrpPathCallback* p)
{
    p->PathElemUnitUpdated = &__static_mmZkrpPath_ElemUnitValue;
    p->PathElemUnitCreated = &__static_mmZkrpPath_ElemUnitEvent;
    p->PathElemUnitDeleted = &__static_mmZkrpPath_ElemUnitEvent;
    p->PathElemUnitChanged = &__static_mmZkrpPath_ElemUnitEvent;
    p->PathElemUpdated = &__static_mmZkrpPath_ElemValue;
    p->PathElemCreated = &__static_mmZkrpPath_ElemEvent;
    p->PathElemDeleted = &__static_mmZkrpPath_ElemEvent;
    p->PathElemChanged = &__static_mmZkrpPath_ElemEvent;
    p->PathCreated = &__static_mmZkrpPath_Event;
    p->PathDeleted = &__static_mmZkrpPath_Event;
    p->PathChanged = &__static_mmZkrpPath_Event;
    p->NetConnect = &__static_mmZkrpPath_Event;
    p->NetExpired = &__static_mmZkrpPath_Event;
    p->obj = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Init(struct mmZkrpPathUnit* p)
{
    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->zk_path = NULL;
    p->zk_path_elem = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Destroy(struct mmZkrpPathUnit* p)
{
    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->zk_path = NULL;
    p->zk_path_elem = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Lock(struct mmZkrpPathUnit* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Unlock(struct mmZkrpPathUnit* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Init(struct mmZkrpPathElem* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->zk_path = NULL;

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Destroy(struct mmZkrpPathElem* p)
{
    mmZkrpPathElem_Clear(p);
    //
    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->zk_path = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Lock(struct mmZkrpPathElem* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Unlock(struct mmZkrpPathElem* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER struct mmZkrpPathUnit* mmZkrpPathElem_Add(struct mmZkrpPathElem* p, mmUInt32_t unique_id)
{
    struct mmZkrpPathUnit* e = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    e = mmZkrpPathElem_Get(p, unique_id);
    if (NULL == e)
    {
        e = (struct mmZkrpPathUnit*)mmMalloc(sizeof(struct mmZkrpPathUnit));
        mmZkrpPathUnit_Init(e);
        e->unique_id = unique_id;
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU32Vpt_Set(&p->rbtree, unique_id, e);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Rmv(struct mmZkrpPathElem* p, mmUInt32_t unique_id)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU32Vpt_GetIterator(&p->rbtree, unique_id);
    if (NULL != it)
    {
        struct mmZkrpPathUnit* e = (struct mmZkrpPathUnit*)(it->v);
        mmZkrpPathUnit_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrpPathUnit_Unlock(e);
        mmZkrpPathUnit_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_ZOOKEEPER struct mmZkrpPathUnit* mmZkrpPathElem_Get(struct mmZkrpPathElem* p, mmUInt32_t unique_id)
{
    struct mmZkrpPathUnit* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmZkrpPathUnit*)mmRbtreeU32Vpt_Get(&p->rbtree, unique_id);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER struct mmZkrpPathUnit* mmZkrpPathElem_GetInstance(struct mmZkrpPathElem* p, mmUInt32_t unique_id)
{
    struct mmZkrpPathUnit* e = mmZkrpPathElem_Get(p, unique_id);
    if (NULL == e)
    {
        e = mmZkrpPathElem_Add(p, unique_id);
    }
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Clear(struct mmZkrpPathElem* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmZkrpPathUnit* e = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        e = (struct mmZkrpPathUnit*)(it->v);
        n = mmRb_Next(n);
        mmZkrpPathUnit_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrpPathUnit_Unlock(e);
        mmZkrpPathUnit_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}

MM_EXPORT_ZOOKEEPER void mmZkrpPath_Init(struct mmZkrpPath* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmString_Init(&p->path);
    mmString_Init(&p->host);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmZkrpPathCallback_Init(&p->callback);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->timeout = MM_ZKRP_PATH_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;

    mmString_Assigns(&p->path, "/zkrp");
    mmString_Assigns(&p->host, "127.0.0.1:2181,");

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Destroy(struct mmZkrpPath* p)
{
    mmZkrpPath_Clear(p);
    mmZkrpPath_Abandon(p);
    //
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->host);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmZkrpPathCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->timeout = 0;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrpPath_Start(struct mmZkrpPath* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmZkrpPath_Watcher(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Interrupt(struct mmZkrpPath* p)
{
    p->state = MM_TS_CLOSED;
    mmZkrpPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Shutdown(struct mmZkrpPath* p)
{
    p->state = MM_TS_FINISH;
    mmZkrpPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Join(struct mmZkrpPath* p)
{

}

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Watcher(struct mmZkrpPath* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // try abandon first.
    mmZkrpPath_Abandon(p);
    if (0 != mmString_CStr(&p->host))
    {
        // init a new zk handle.
        p->zkhandle = zookeeper_init(mmString_CStr(&p->host), __static_mmZkrpPath_Watcher, p->timeout, 0, p, 0);
        if (NULL == p->zkhandle)
        {
            mmLogger_LogE(gLogger, "%s %d init zookeeper servers failure.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d init zookeeper servers success.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d the host is empty.", __FUNCTION__, __LINE__);
    }
}
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Abandon(struct mmZkrpPath* p)
{
    if (NULL != p->zkhandle)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        zookeeper_close(p->zkhandle);
        p->zkhandle = NULL;
        mmLogger_LogI(gLogger, "%s %d close zookeeper servers success.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_ZOOKEEPER void mmZkrpPath_Lock(struct mmZkrpPath* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Unlock(struct mmZkrpPath* p)
{
    mmSpinlock_Unlock(&p->locker);
}

// assign net callback.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_SetCallback(struct mmZkrpPath* p, struct mmZkrpPathCallback* zkrp_path_callback)
{
    assert(NULL != zkrp_path_callback && "you can not assign null zkrp_path_callback.");
    p->callback = *zkrp_path_callback;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_SetPath(struct mmZkrpPath* p, const char* path)
{
    assert(NULL != path && "you can not assign null path.");
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_SetHost(struct mmZkrpPath* p, const char* host)
{
    assert(NULL != host && "you can not assign null host.");
    mmString_Assigns(&p->host, host);
}

// this function will check and try add watcher when the elem size is zero.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Update(struct mmZkrpPath* p)
{
    if (0 == p->rbtree.size)
    {
        __static_mmZkrpPath_Awexists(p);
    }
}

MM_EXPORT_ZOOKEEPER struct mmZkrpPathElem* mmZkrpPath_Add(struct mmZkrpPath* p, mmUInt32_t unique_id)
{
    struct mmZkrpPathElem* e = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    e = mmZkrpPath_Get(p, unique_id);
    if (NULL == e)
    {
        e = (struct mmZkrpPathElem*)mmMalloc(sizeof(struct mmZkrpPathElem));
        mmZkrpPathElem_Init(e);
        e->unique_id = unique_id;
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU32Vpt_Set(&p->rbtree, unique_id, e);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Rmv(struct mmZkrpPath* p, mmUInt32_t unique_id)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU32Vpt_GetIterator(&p->rbtree, unique_id);
    if (NULL != it)
    {
        struct mmZkrpPathElem* e = (struct mmZkrpPathElem*)(it->v);
        mmZkrpPathElem_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrpPathElem_Unlock(e);
        mmZkrpPathElem_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_ZOOKEEPER struct mmZkrpPathElem* mmZkrpPath_Get(struct mmZkrpPath* p, mmUInt32_t unique_id)
{
    struct mmZkrpPathElem* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmZkrpPathElem*)mmRbtreeU32Vpt_Get(&p->rbtree, unique_id);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER struct mmZkrpPathElem* mmZkrpPath_GetInstance(struct mmZkrpPath* p, mmUInt32_t unique_id)
{
    struct mmZkrpPathElem* e = mmZkrpPath_Get(p, unique_id);
    if (NULL == e)
    {
        e = mmZkrpPath_Add(p, unique_id);
    }
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Clear(struct mmZkrpPath* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmZkrpPathElem* e = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        e = (struct mmZkrpPathElem*)(it->v);
        n = mmRb_Next(n);
        mmZkrpPathElem_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrpPathElem_Unlock(e);
        mmZkrpPathElem_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}

static void __static_mmZkrpPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPath* _zkrp_path = (struct mmZkrpPath*)(watcherCtx);
    if (ZOO_SESSION_EVENT == type)
    {
        if (ZOO_CONNECTED_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connected.", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.NetConnect))(_zkrp_path);
            __static_mmZkrpPath_Awexists(_zkrp_path);
        }
        else if (ZOO_EXPIRED_SESSION_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s session expired.", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.NetExpired))(_zkrp_path);
            mmZkrpPath_Watcher(_zkrp_path);
        }
        else if (ZOO_CONNECTING_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connecting.", __FUNCTION__, __LINE__, path);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d event happened.type:%d state:%d path:%s.", __FUNCTION__, __LINE__, type, state, path);
        }
    }
}

static void __static_mmZkrpPath_ElemUnitAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathUnit* _zkrp_path_unit = (struct mmZkrpPathUnit*)(data);
    struct mmZkrpPathElem* _zkrp_path_elem = _zkrp_path_unit->zk_path_elem;
    struct mmZkrpPath* _zkrp_path = _zkrp_path_unit->zk_path;
    if (ZOK == rc)
    {
        if (0 < value_len)
        {
            if (_zkrp_path_unit->stat.mtime != stat->mtime)
            {
                mmMemcpy(&_zkrp_path_unit->stat, stat, sizeof(struct Stat));
                mmLogger_LogV(gLogger, "%s %d value_len:%d.", __FUNCTION__, __LINE__, value_len);
                (*(_zkrp_path->callback.PathElemUnitUpdated))(_zkrp_path, _zkrp_path_elem, _zkrp_path_unit, value, 0, value_len);
            }
        }
    }
}
static void __static_mmZkrpPath_ElemUnitAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx)
{
    struct mmZkrpPathUnit* _zkrp_path_unit = (struct mmZkrpPathUnit*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrpPath_ElemUnitAwget(_zkrp_path_unit);
}

static void __static_mmZkrpPath_ElemUnitAwget(struct mmZkrpPathUnit* _zkrp_path_unit)
{
    struct mmZkrpPathElem* _zkrp_path_elem = _zkrp_path_unit->zk_path_elem;
    struct mmZkrpPath* _zkrp_path = _zkrp_path_unit->zk_path;
    int ret = 0;
    char path[128];
    mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1 "/" MM_ZKRP_FORMAT_2, mmString_CStr(&_zkrp_path->path), _zkrp_path_elem->unique_id, _zkrp_path_unit->unique_id);
    ret = zoo_awget(_zkrp_path->zkhandle, path, __static_mmZkrpPath_ElemUnitAwgetWatcher, _zkrp_path_unit, __static_mmZkrpPath_ElemUnitAwgetDataCompletion, _zkrp_path_unit);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrpPath_ElemUnitAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathUnit* _zkrp_path_unit = (struct mmZkrpPathUnit*)(data);
    if (ZOK == rc)
    {
        // awget /path/elem/unit
        __static_mmZkrpPath_ElemUnitAwget(_zkrp_path_unit);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrpPath_ElemUnitAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathUnit* _zkrp_path_unit = (struct mmZkrpPathUnit*)(watcherCtx);
    struct mmZkrpPathElem* _zkrp_path_elem = _zkrp_path_unit->zk_path_elem;
    struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathElemUnitDeleted))(_zkrp_path, _zkrp_path_elem, _zkrp_path_unit);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathElemUnitCreated))(_zkrp_path, _zkrp_path_elem, _zkrp_path_unit);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathElemUnitChanged))(_zkrp_path, _zkrp_path_elem, _zkrp_path_unit);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrpPath_ElemUnitAwexists(_zkrp_path_unit);
}
static void __static_mmZkrpPath_ElemUnitAwexists(struct mmZkrpPathUnit* p)
{
    char path[128];
    int ret = 0;
    struct mmZkrpPathElem* _zkrp_path_elem = p->zk_path_elem;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPath* _zkrp_path = p->zk_path;
    mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1 "/" MM_ZKRP_FORMAT_2, mmString_CStr(&_zkrp_path->path), _zkrp_path_elem->unique_id, p->unique_id);
    ret = zoo_awexists(_zkrp_path->zkhandle, path, __static_mmZkrpPath_ElemUnitAwexistsWatcher, p, __static_mmZkrpPath_ElemUnitAwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrpPath_ElemAdeleteCompletion(int rc, const void *data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (ZOK == rc)
    {
        char path[128];
        struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(data);
        struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
        mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1, mmString_CStr(&_zkrp_path->path), _zkrp_path_elem->unique_id);
        // adelete /path/elem success.
        mmLogger_LogI(gLogger, "%s %d adelete %s success.", __FUNCTION__, __LINE__, path);
    }
    else if (ZNOTEMPTY == rc)
    {
        char path[128];
        struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(data);
        struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
        mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1, mmString_CStr(&_zkrp_path->path), _zkrp_path_elem->unique_id);
        // adelete /path/elem precess is not need, because the path have children now.
        mmLogger_LogI(gLogger, "%s %d adelete %s precess is not need, because the path have children now.", __FUNCTION__, __LINE__, path);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrpPath_ElemAdelete(struct mmZkrpPathElem* p)
{
    struct mmZkrpPath* _zkrp_path = p->zk_path;
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1, mmString_CStr(&_zkrp_path->path), p->unique_id);
    ret = zoo_adelete(_zkrp_path->zkhandle, path, 0, __static_mmZkrpPath_ElemAdeleteCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrpPath_ElemAwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(data);
    struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
    if (ZOK == rc)
    {
        if (NULL != strings)
        {
            if (0 != strings->count)
            {
                int32_t i = 0;
                for (i = 0; i < strings->count; ++i)
                {
                    mmUInt32_t _unique_id = 0;
                    struct mmZkrpPathUnit* _zkrp_path_unit = NULL;
                    mmSscanf(strings->data[i], MM_ZKRP_FORMAT_2, &_unique_id);
                    _zkrp_path_unit = mmZkrpPathElem_GetInstance(_zkrp_path_elem, _unique_id);
                    _zkrp_path_unit->zk_path = _zkrp_path;
                    _zkrp_path_unit->zk_path_elem = _zkrp_path_elem;
                    _zkrp_path_unit->unique_id = _unique_id;
                    __static_mmZkrpPath_ElemUnitAwexists(_zkrp_path_unit);
                }
            }
            else
            {
                __static_mmZkrpPath_ElemAdelete(_zkrp_path_elem);
            }
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrpPath_ElemAwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrpPath_ElemAwgetChildren(_zkrp_path_elem);
}
static void __static_mmZkrpPath_ElemAwgetChildren(struct mmZkrpPathElem* p)
{
    struct mmZkrpPath* _zkrp_path = p->zk_path;
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1, mmString_CStr(&_zkrp_path->path), p->unique_id);
    ret = zoo_awget_children(_zkrp_path->zkhandle, path, __static_mmZkrpPath_ElemAwgetChildrenWatcher, p, __static_mmZkrpPath_ElemAwgetChildrenStringsCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrpPath_ElemAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(data);
    struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
    if (ZOK == rc)
    {
        if (0 < value_len)
        {
            if (_zkrp_path_elem->stat.mtime != stat->mtime)
            {
                mmMemcpy(&_zkrp_path_elem->stat, stat, sizeof(struct Stat));
                mmLogger_LogV(gLogger, "%s %d value_len:%d.", __FUNCTION__, __LINE__, value_len);
                (*(_zkrp_path->callback.PathElemUpdated))(_zkrp_path, _zkrp_path_elem, value, 0, value_len);
            }
        }
    }
}
static void __static_mmZkrpPath_ElemAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx)
{
    struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrpPath_ElemAwget(_zkrp_path_elem);
}

static void __static_mmZkrpPath_ElemAwget(struct mmZkrpPathElem* _zkrp_path_elem)
{
    struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
    int ret = 0;
    char path[128];
    mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1, mmString_CStr(&_zkrp_path->path), _zkrp_path_elem->unique_id);
    ret = zoo_awget(_zkrp_path->zkhandle, path, __static_mmZkrpPath_ElemAwgetWatcher, _zkrp_path_elem, __static_mmZkrpPath_ElemAwgetDataCompletion, _zkrp_path_elem);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrpPath_ElemAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(data);
    if (ZOK == rc)
    {
        // awget /path/elem
        __static_mmZkrpPath_ElemAwget(_zkrp_path_elem);
        // awget child.
        __static_mmZkrpPath_ElemAwgetChildren(_zkrp_path_elem);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrpPath_ElemAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPathElem* _zkrp_path_elem = (struct mmZkrpPathElem*)(watcherCtx);
    struct mmZkrpPath* _zkrp_path = _zkrp_path_elem->zk_path;
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathElemDeleted))(_zkrp_path, _zkrp_path_elem);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathElemCreated))(_zkrp_path, _zkrp_path_elem);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathElemChanged))(_zkrp_path, _zkrp_path_elem);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrpPath_ElemAwexists(_zkrp_path_elem);
}
static void __static_mmZkrpPath_ElemAwexists(struct mmZkrpPathElem* p)
{
    char path[128];
    int ret = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPath* _zkrp_path = p->zk_path;
    mmSprintf(path, "%s/" MM_ZKRP_FORMAT_1, mmString_CStr(&_zkrp_path->path), p->unique_id);
    ret = zoo_awexists(_zkrp_path->zkhandle, path, __static_mmZkrpPath_ElemAwexistsWatcher, p, __static_mmZkrpPath_ElemAwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrpPath_AwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPath* _zkrp_path = (struct mmZkrpPath*)(data);
    if (ZOK == rc)
    {
        if (NULL != strings)
        {
            int32_t i = 0;
            for (i = 0; i < strings->count; ++i)
            {
                mmUInt32_t _unique_id = 0;
                struct mmZkrpPathElem* _zkrp_path_elem = NULL;
                mmSscanf(strings->data[i], MM_ZKRP_FORMAT_1, &_unique_id);
                _zkrp_path_elem = mmZkrpPath_GetInstance(_zkrp_path, _unique_id);
                _zkrp_path_elem->zk_path = _zkrp_path;
                _zkrp_path_elem->unique_id = _unique_id;
                __static_mmZkrpPath_ElemAwexists(_zkrp_path_elem);
            }
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrpPath_AwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmZkrpPath* _zkrp_path = (struct mmZkrpPath*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrpPath_AwgetChildren(_zkrp_path);
}
static void __static_mmZkrpPath_AwgetChildren(struct mmZkrpPath* p)
{
    int ret = zoo_awget_children(p->zkhandle, mmString_CStr(&p->path), __static_mmZkrpPath_AwgetChildrenWatcher, p, __static_mmZkrpPath_AwgetChildrenStringsCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrpPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPath* _zkrp_path = (struct mmZkrpPath*)(data);
    if (ZOK == rc)
    {
        // reset watch again.
        __static_mmZkrpPath_AwgetChildren(_zkrp_path);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrpPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrpPath* _zkrp_path = (struct mmZkrpPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathDeleted))(_zkrp_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathCreated))(_zkrp_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrp_path->callback.PathChanged))(_zkrp_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrpPath_Awexists(_zkrp_path);
}
static void __static_mmZkrpPath_Awexists(struct mmZkrpPath* p)
{
    int ret = zoo_awexists(p->zkhandle, mmString_CStr(&p->path), __static_mmZkrpPath_AwexistsWatcher, p, __static_mmZkrpPath_AwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

