/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmZookeeperZkrmPath.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmThread.h"

static void __static_mmZkrmPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);

static void __static_mmZkrmPath_ElemUnitAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data);
static void __static_mmZkrmPath_ElemUnitAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx);
static void __static_mmZkrmPath_ElemUnitAwget(struct mmZkrmPathUnit* _zkrm_path_unit);

static void __static_mmZkrmPath_ElemUnitAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrmPath_ElemUnitAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrmPath_ElemUnitAwexists(struct mmZkrmPathUnit* p);

static void __static_mmZkrmPath_ElemAwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data);
static void __static_mmZkrmPath_ElemAwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrmPath_ElemAwgetChildren(struct mmZkrmPathElem* p);

static void __static_mmZkrmPath_ElemAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data);
static void __static_mmZkrmPath_ElemAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx);
static void __static_mmZkrmPath_ElemAwget(struct mmZkrmPathElem* _zkrm_path_elem);

static void __static_mmZkrmPath_ElemAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrmPath_ElemAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrmPath_ElemAwexists(struct mmZkrmPathElem* p);

static void __static_mmZkrmPath_AwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data);
static void __static_mmZkrmPath_AwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrmPath_AwgetChildren(struct mmZkrmPath* p);

static void __static_mmZkrmPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data);
static void __static_mmZkrmPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx);
static void __static_mmZkrmPath_Awexists(struct mmZkrmPath* p);

static void __static_mmZkrmPath_ElemUnitValue(struct mmZkrmPath* p, struct mmZkrmPathElem* e, struct mmZkrmPathUnit* u, const char* buffer, int offset, int length)
{

}
static void __static_mmZkrmPath_ElemUnitEvent(struct mmZkrmPath* p, struct mmZkrmPathElem* e, struct mmZkrmPathUnit* u)
{

}
static void __static_mmZkrmPath_ElemValue(struct mmZkrmPath* p, struct mmZkrmPathElem* e, const char* buffer, int offset, int length)
{

}
static void __static_mmZkrmPath_ElemEvent(struct mmZkrmPath* p, struct mmZkrmPathElem* e)
{

}
static void __static_mmZkrmPath_Event(struct mmZkrmPath* p)
{

}

MM_EXPORT_ZOOKEEPER void mmZkrmPathCallback_Init(struct mmZkrmPathCallback* p)
{
    p->PathElemUnitUpdated = &__static_mmZkrmPath_ElemUnitValue;
    p->PathElemUnitCreated = &__static_mmZkrmPath_ElemUnitEvent;
    p->PathElemUnitDeleted = &__static_mmZkrmPath_ElemUnitEvent;
    p->PathElemUnitChanged = &__static_mmZkrmPath_ElemUnitEvent;
    p->PathElemUpdated = &__static_mmZkrmPath_ElemValue;
    p->PathElemCreated = &__static_mmZkrmPath_ElemEvent;
    p->PathElemDeleted = &__static_mmZkrmPath_ElemEvent;
    p->PathElemChanged = &__static_mmZkrmPath_ElemEvent;
    p->PathCreated = &__static_mmZkrmPath_Event;
    p->PathDeleted = &__static_mmZkrmPath_Event;
    p->PathChanged = &__static_mmZkrmPath_Event;
    p->NetConnect = &__static_mmZkrmPath_Event;
    p->NetExpired = &__static_mmZkrmPath_Event;
    p->obj = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathCallback_Destroy(struct mmZkrmPathCallback* p)
{
    p->PathElemUnitUpdated = &__static_mmZkrmPath_ElemUnitValue;
    p->PathElemUnitCreated = &__static_mmZkrmPath_ElemUnitEvent;
    p->PathElemUnitDeleted = &__static_mmZkrmPath_ElemUnitEvent;
    p->PathElemUnitChanged = &__static_mmZkrmPath_ElemUnitEvent;
    p->PathElemUpdated = &__static_mmZkrmPath_ElemValue;
    p->PathElemCreated = &__static_mmZkrmPath_ElemEvent;
    p->PathElemDeleted = &__static_mmZkrmPath_ElemEvent;
    p->PathElemChanged = &__static_mmZkrmPath_ElemEvent;
    p->PathCreated = &__static_mmZkrmPath_Event;
    p->PathDeleted = &__static_mmZkrmPath_Event;
    p->PathChanged = &__static_mmZkrmPath_Event;
    p->NetConnect = &__static_mmZkrmPath_Event;
    p->NetExpired = &__static_mmZkrmPath_Event;
    p->obj = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Init(struct mmZkrmPathUnit* p)
{
    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->zk_path = NULL;
    p->zk_path_elem = NULL;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Destroy(struct mmZkrmPathUnit* p)
{
    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->zk_path = NULL;
    p->zk_path_elem = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Lock(struct mmZkrmPathUnit* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathUnit_Unlock(struct mmZkrmPathUnit* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Init(struct mmZkrmPathElem* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->unique_id = 0;
    p->zk_path = NULL;

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Destroy(struct mmZkrmPathElem* p)
{
    mmZkrmPathElem_Clear(p);
    //
    mmMemset(&p->stat, 0, sizeof(struct Stat));
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->unique_id = 0;
    p->zk_path = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Lock(struct mmZkrmPathElem* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Unlock(struct mmZkrmPathElem* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_ZOOKEEPER struct mmZkrmPathUnit* mmZkrmPathElem_Add(struct mmZkrmPathElem* p, mmUInt32_t unique_id)
{
    struct mmZkrmPathUnit* e = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    e = mmZkrmPathElem_Get(p, unique_id);
    if (NULL == e)
    {
        e = (struct mmZkrmPathUnit*)mmMalloc(sizeof(struct mmZkrmPathUnit));
        mmZkrmPathUnit_Init(e);
        e->unique_id = unique_id;
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU32Vpt_Set(&p->rbtree, unique_id, e);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Rmv(struct mmZkrmPathElem* p, mmUInt32_t unique_id)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU32Vpt_GetIterator(&p->rbtree, unique_id);
    if (NULL != it)
    {
        struct mmZkrmPathUnit* e = (struct mmZkrmPathUnit*)(it->v);
        mmZkrmPathUnit_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrmPathUnit_Unlock(e);
        mmZkrmPathUnit_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_ZOOKEEPER struct mmZkrmPathUnit* mmZkrmPathElem_Get(struct mmZkrmPathElem* p, mmUInt32_t unique_id)
{
    struct mmZkrmPathUnit* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmZkrmPathUnit*)mmRbtreeU32Vpt_Get(&p->rbtree, unique_id);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER struct mmZkrmPathUnit* mmZkrmPathElem_GetInstance(struct mmZkrmPathElem* p, mmUInt32_t unique_id)
{
    struct mmZkrmPathUnit* e = mmZkrmPathElem_Get(p, unique_id);
    if (NULL == e)
    {
        e = mmZkrmPathElem_Add(p, unique_id);
    }
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPathElem_Clear(struct mmZkrmPathElem* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmZkrmPathUnit* e = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        e = (struct mmZkrmPathUnit*)(it->v);
        n = mmRb_Next(n);
        mmZkrmPathUnit_Lock(e);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmZkrmPathUnit_Unlock(e);
        mmZkrmPathUnit_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}

MM_EXPORT_ZOOKEEPER void mmZkrmPath_Init(struct mmZkrmPath* p)
{
    struct mmRbtreeU64VptAllocator _U64VptAllocator;

    mmString_Init(&p->path);
    mmString_Init(&p->host);
    mmRbtreeU64Vpt_Init(&p->rbtree);
    mmZkrmPathCallback_Init(&p->callback);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->timeout = MM_ZKRM_PATH_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;

    mmString_Assigns(&p->path, "/zkrm");
    mmString_Assigns(&p->host, "127.0.0.1:2181,");

    _U64VptAllocator.Produce = &mmRbtreeU64Vpt_WeakProduce;
    _U64VptAllocator.Recycle = &mmRbtreeU64Vpt_WeakRecycle;
    mmRbtreeU64Vpt_SetAllocator(&p->rbtree, &_U64VptAllocator);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Destroy(struct mmZkrmPath* p)
{
    mmZkrmPath_Clear(p);
    mmZkrmPath_Abandon(p);
    //
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->host);
    mmRbtreeU64Vpt_Destroy(&p->rbtree);
    mmZkrmPathCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->timeout = 0;
    p->state = MM_TS_CLOSED;
    p->zkhandle = NULL;
}

MM_EXPORT_ZOOKEEPER void mmZkrmPath_Start(struct mmZkrmPath* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmZkrmPath_Watcher(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Interrupt(struct mmZkrmPath* p)
{
    p->state = MM_TS_CLOSED;
    mmZkrmPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Shutdown(struct mmZkrmPath* p)
{
    p->state = MM_TS_FINISH;
    mmZkrmPath_Abandon(p);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Join(struct mmZkrmPath* p)
{

}

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Watcher(struct mmZkrmPath* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // try abandon first.
    mmZkrmPath_Abandon(p);
    if (0 != mmString_Size(&p->host))
    {
        // init a new zk handle.
        p->zkhandle = zookeeper_init(mmString_CStr(&p->host), __static_mmZkrmPath_Watcher, p->timeout, 0, p, 0);
        if (NULL == p->zkhandle)
        {
            mmLogger_LogE(gLogger, "%s %d init zookeeper servers failure.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d init zookeeper servers success.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d the host is empty.", __FUNCTION__, __LINE__);
    }
}
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Abandon(struct mmZkrmPath* p)
{
    if (NULL != p->zkhandle)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        zookeeper_close(p->zkhandle);
        p->zkhandle = NULL;
        mmLogger_LogI(gLogger, "%s %d close zookeeper servers success.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_ZOOKEEPER void mmZkrmPath_Lock(struct mmZkrmPath* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Unlock(struct mmZkrmPath* p)
{
    mmSpinlock_Unlock(&p->locker);
}

// assign net callback.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_SetCallback(struct mmZkrmPath* p, struct mmZkrmPathCallback* zkrm_path_callback)
{
    assert(NULL != zkrm_path_callback && "you can not assign null zkrm_path_callback.");
    p->callback = *zkrm_path_callback;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_SetPath(struct mmZkrmPath* p, const char* path)
{
    assert(NULL != path && "you can not assign null path.");
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_SetHost(struct mmZkrmPath* p, const char* host)
{
    assert(NULL != host && "you can not assign null host.");
    mmString_Assigns(&p->host, host);
}

// this function will check and try add watcher when the elem size is zero.
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Update(struct mmZkrmPath* p)
{
    if (0 == p->rbtree.size)
    {
        __static_mmZkrmPath_Awexists(p);
    }
}

MM_EXPORT_ZOOKEEPER struct mmZkrmPathElem* mmZkrmPath_Add(struct mmZkrmPath* p, mmUInt64_t unique_id)
{
    struct mmZkrmPathElem* e = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    e = mmZkrmPath_Get(p, unique_id);
    if (NULL == e)
    {
        e = (struct mmZkrmPathElem*)mmMalloc(sizeof(struct mmZkrmPathElem));
        mmZkrmPathElem_Init(e);
        e->unique_id = unique_id;
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU64Vpt_Set(&p->rbtree, unique_id, e);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Rmv(struct mmZkrmPath* p, mmUInt64_t unique_id)
{
    struct mmRbtreeU64VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU64Vpt_GetIterator(&p->rbtree, unique_id);
    if (NULL != it)
    {
        struct mmZkrmPathElem* e = (struct mmZkrmPathElem*)(it->v);
        mmZkrmPathElem_Lock(e);
        mmRbtreeU64Vpt_Erase(&p->rbtree, it);
        mmZkrmPathElem_Unlock(e);
        mmZkrmPathElem_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_ZOOKEEPER struct mmZkrmPathElem* mmZkrmPath_Get(struct mmZkrmPath* p, mmUInt64_t unique_id)
{
    struct mmZkrmPathElem* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmZkrmPathElem*)mmRbtreeU64Vpt_Get(&p->rbtree, unique_id);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_ZOOKEEPER struct mmZkrmPathElem* mmZkrmPath_GetInstance(struct mmZkrmPath* p, mmUInt64_t unique_id)
{
    struct mmZkrmPathElem* e = mmZkrmPath_Get(p, unique_id);
    if (NULL == e)
    {
        e = mmZkrmPath_Add(p, unique_id);
    }
    return e;
}
MM_EXPORT_ZOOKEEPER void mmZkrmPath_Clear(struct mmZkrmPath* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    struct mmZkrmPathElem* e = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
        e = (struct mmZkrmPathElem*)(it->v);
        n = mmRb_Next(n);
        mmZkrmPathElem_Lock(e);
        mmRbtreeU64Vpt_Erase(&p->rbtree, it);
        mmZkrmPathElem_Unlock(e);
        mmZkrmPathElem_Destroy(e);
        mmFree(e);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}

static void __static_mmZkrmPath_Watcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPath* _zkrm_path = (struct mmZkrmPath*)(watcherCtx);
    if (ZOO_SESSION_EVENT == type)
    {
        if (ZOO_CONNECTED_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connected.", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.NetConnect))(_zkrm_path);
            __static_mmZkrmPath_Awexists(_zkrm_path);
        }
        else if (ZOO_EXPIRED_SESSION_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s session expired.", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.NetExpired))(_zkrm_path);
            mmZkrmPath_Watcher(_zkrm_path);
        }
        else if (ZOO_CONNECTING_STATE == state)
        {
            mmLogger_LogI(gLogger, "%s %d %s connecting.", __FUNCTION__, __LINE__, path);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d event happened.type:%d state:%d path:%s.", __FUNCTION__, __LINE__, type, state, path);
        }
    }
}

static void __static_mmZkrmPath_ElemUnitAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathUnit* _zkrm_path_unit = (struct mmZkrmPathUnit*)(data);
    struct mmZkrmPathElem* _zkrm_path_elem = _zkrm_path_unit->zk_path_elem;
    struct mmZkrmPath* _zkrm_path = _zkrm_path_unit->zk_path;
    if (ZOK == rc)
    {
        if (0 < value_len)
        {
            if (_zkrm_path_unit->stat.mtime != stat->mtime)
            {
                mmMemcpy(&_zkrm_path_unit->stat, stat, sizeof(struct Stat));
                mmLogger_LogV(gLogger, "%s %d value_len:%d.", __FUNCTION__, __LINE__, value_len);
                (*(_zkrm_path->callback.PathElemUnitUpdated))(_zkrm_path, _zkrm_path_elem, _zkrm_path_unit, value, 0, value_len);
            }
        }
    }
}
static void __static_mmZkrmPath_ElemUnitAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx)
{
    struct mmZkrmPathUnit* _zkrm_path_unit = (struct mmZkrmPathUnit*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrmPath_ElemUnitAwget(_zkrm_path_unit);
}
static void __static_mmZkrmPath_ElemUnitAwget(struct mmZkrmPathUnit* _zkrm_path_unit)
{
    struct mmZkrmPathElem* _zkrm_path_elem = _zkrm_path_unit->zk_path_elem;
    struct mmZkrmPath* _zkrm_path = _zkrm_path_unit->zk_path;
    int ret = 0;
    char path[128];
    mmSprintf(path, "%s/" MM_ZKRM_FORMAT_1 "/" MM_ZKRM_FORMAT_2, mmString_CStr(&_zkrm_path->path), _zkrm_path_elem->unique_id, _zkrm_path_unit->unique_id);
    ret = zoo_awget(_zkrm_path->zkhandle, path, __static_mmZkrmPath_ElemUnitAwgetWatcher, _zkrm_path_unit, __static_mmZkrmPath_ElemUnitAwgetDataCompletion, _zkrm_path_unit);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrmPath_ElemUnitAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathUnit* _zkrm_path_unit = (struct mmZkrmPathUnit*)(data);
    if (ZOK == rc)
    {
        // awget /path/elem/unit
        __static_mmZkrmPath_ElemUnitAwget(_zkrm_path_unit);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrmPath_ElemUnitAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathUnit* _zkrm_path_unit = (struct mmZkrmPathUnit*)(watcherCtx);
    struct mmZkrmPathElem* _zkrm_path_elem = _zkrm_path_unit->zk_path_elem;
    struct mmZkrmPath* _zkrm_path = _zkrm_path_elem->zk_path;
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathElemUnitDeleted))(_zkrm_path, _zkrm_path_elem, _zkrm_path_unit);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathElemUnitCreated))(_zkrm_path, _zkrm_path_elem, _zkrm_path_unit);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathElemUnitChanged))(_zkrm_path, _zkrm_path_elem, _zkrm_path_unit);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrmPath_ElemUnitAwexists(_zkrm_path_unit);
}
static void __static_mmZkrmPath_ElemUnitAwexists(struct mmZkrmPathUnit* p)
{
    char path[128];
    int ret = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathElem* _zkrm_path_elem = p->zk_path_elem;
    struct mmZkrmPath* _zkrm_path = p->zk_path;
    mmSprintf(path, "%s/" MM_ZKRM_FORMAT_1 "/" MM_ZKRM_FORMAT_2, mmString_CStr(&_zkrm_path->path), _zkrm_path_elem->unique_id, p->unique_id);
    ret = zoo_awexists(_zkrm_path->zkhandle, path, __static_mmZkrmPath_ElemUnitAwexistsWatcher, p, __static_mmZkrmPath_ElemUnitAwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrmPath_ElemAwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathElem* _zkrm_path_elem = (struct mmZkrmPathElem*)(data);
    struct mmZkrmPath* _zkrm_path = _zkrm_path_elem->zk_path;
    if (ZOK == rc)
    {
        if (NULL != strings)
        {
            int32_t i = 0;
            for (i = 0; i < strings->count; ++i)
            {
                mmUInt32_t _unique_id = 0;
                struct mmZkrmPathUnit* _zkrm_path_unit = NULL;
                mmSscanf(strings->data[i], MM_ZKRM_FORMAT_2, &_unique_id);
                _zkrm_path_unit = mmZkrmPathElem_GetInstance(_zkrm_path_elem, _unique_id);
                _zkrm_path_unit->zk_path = _zkrm_path;
                _zkrm_path_unit->zk_path_elem = _zkrm_path_elem;
                _zkrm_path_unit->unique_id = _unique_id;
                __static_mmZkrmPath_ElemUnitAwexists(_zkrm_path_unit);
            }
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrmPath_ElemAwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmZkrmPathElem* _zkrm_path_elem = (struct mmZkrmPathElem*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrmPath_ElemAwgetChildren(_zkrm_path_elem);
}
static void __static_mmZkrmPath_ElemAwgetChildren(struct mmZkrmPathElem* p)
{
    struct mmZkrmPath* _zkrm_path = p->zk_path;
    char path[128];
    int ret = 0;
    mmSprintf(path, "%s/" MM_ZKRM_FORMAT_1, mmString_CStr(&_zkrm_path->path), p->unique_id);
    ret = zoo_awget_children(_zkrm_path->zkhandle, path, __static_mmZkrmPath_ElemAwgetChildrenWatcher, p, __static_mmZkrmPath_ElemAwgetChildrenStringsCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrmPath_ElemAwgetDataCompletion(int rc, const char* value, int value_len, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathElem* _zkrm_path_elem = (struct mmZkrmPathElem*)(data);
    struct mmZkrmPath* _zkrm_path = _zkrm_path_elem->zk_path;
    if (ZOK == rc)
    {
        if (0 < value_len)
        {
            if (_zkrm_path_elem->stat.mtime != stat->mtime)
            {
                mmMemcpy(&_zkrm_path_elem->stat, stat, sizeof(struct Stat));
                mmLogger_LogV(gLogger, "%s %d value_len:%d.", __FUNCTION__, __LINE__, value_len);
                (*(_zkrm_path->callback.PathElemUpdated))(_zkrm_path, _zkrm_path_elem, value, 0, value_len);
            }
        }
    }
}
static void __static_mmZkrmPath_ElemAwgetWatcher(zhandle_t *zh, int type, int state, const char *path, void *watcherCtx)
{
    struct mmZkrmPathElem* _zkrm_path_elem = (struct mmZkrmPathElem*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrmPath_ElemAwget(_zkrm_path_elem);
}
static void __static_mmZkrmPath_ElemAwget(struct mmZkrmPathElem* _zkrm_path_elem)
{
    struct mmZkrmPath* _zkrm_path = _zkrm_path_elem->zk_path;
    int ret = 0;
    char path[128];
    mmSprintf(path, "%s/" MM_ZKRM_FORMAT_1, mmString_CStr(&_zkrm_path->path), _zkrm_path_elem->unique_id);
    ret = zoo_awget(_zkrm_path->zkhandle, path, __static_mmZkrmPath_ElemAwgetWatcher, _zkrm_path_elem, __static_mmZkrmPath_ElemAwgetDataCompletion, _zkrm_path_elem);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}
static void __static_mmZkrmPath_ElemAwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathElem* _zkrm_path_elem = (struct mmZkrmPathElem*)(data);
    if (ZOK == rc)
    {
        // awget /path/elem
        __static_mmZkrmPath_ElemAwget(_zkrm_path_elem);
        // awget child.
        __static_mmZkrmPath_ElemAwgetChildren(_zkrm_path_elem);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrmPath_ElemAwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPathElem* _zkrm_path_elem = (struct mmZkrmPathElem*)(watcherCtx);
    struct mmZkrmPath* _zkrm_path = _zkrm_path_elem->zk_path;
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathElemDeleted))(_zkrm_path, _zkrm_path_elem);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathElemCreated))(_zkrm_path, _zkrm_path_elem);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathElemChanged))(_zkrm_path, _zkrm_path_elem);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrmPath_ElemAwexists(_zkrm_path_elem);
}
static void __static_mmZkrmPath_ElemAwexists(struct mmZkrmPathElem* p)
{
    char path[128];
    int ret = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPath* _zkrm_path = p->zk_path;
    mmSprintf(path, "%s/" MM_ZKRM_FORMAT_1, mmString_CStr(&_zkrm_path->path), p->unique_id);
    ret = zoo_awexists(_zkrm_path->zkhandle, path, __static_mmZkrmPath_ElemAwexistsWatcher, p, __static_mmZkrmPath_ElemAwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrmPath_AwgetChildrenStringsCompletion(int rc, const struct String_vector* strings, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPath* _zkrm_path = (struct mmZkrmPath*)(data);
    if (ZOK == rc)
    {
        if (NULL != strings)
        {
            int32_t i = 0;
            for (i = 0; i < strings->count; ++i)
            {
                mmUInt64_t _unique_id = 0;
                struct mmZkrmPathElem* _zkrm_path_elem = NULL;
                mmSscanf(strings->data[i], MM_ZKRM_FORMAT_1, &_unique_id);
                _zkrm_path_elem = mmZkrmPath_GetInstance(_zkrm_path, _unique_id);
                _zkrm_path_elem->zk_path = _zkrm_path;
                _zkrm_path_elem->unique_id = _unique_id;
                __static_mmZkrmPath_ElemAwexists(_zkrm_path_elem);
            }
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrmPath_AwgetChildrenWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmZkrmPath* _zkrm_path = (struct mmZkrmPath*)(watcherCtx);
    // reset watcher again.
    __static_mmZkrmPath_AwgetChildren(_zkrm_path);
}
static void __static_mmZkrmPath_AwgetChildren(struct mmZkrmPath* p)
{
    int ret = zoo_awget_children(p->zkhandle, mmString_CStr(&p->path), __static_mmZkrmPath_AwgetChildrenWatcher, p, __static_mmZkrmPath_AwgetChildrenStringsCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

static void __static_mmZkrmPath_AwexistsStatCompletion(int rc, const struct Stat* stat, const void* data)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPath* _zkrm_path = (struct mmZkrmPath*)(data);
    if (ZOK == rc)
    {
        // reset watch again.
        __static_mmZkrmPath_AwgetChildren(_zkrm_path);
    }
    else if (ZNONODE == rc)
    {
        // here we not need create path.
        // do nothing.
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d zerror:%s", __FUNCTION__, __LINE__, zerror(rc));
    }
}
static void __static_mmZkrmPath_AwexistsWatcher(zhandle_t* zh, int type, int state, const char* path, void* watcherCtx)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmZkrmPath* _zkrm_path = (struct mmZkrmPath*)(watcherCtx);
    if (state == ZOO_CONNECTED_STATE)
    {
        if (type == ZOO_DELETED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s deleted...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathDeleted))(_zkrm_path);
        }
        else if (type == ZOO_CREATED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s created...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathCreated))(_zkrm_path);
        }
        else if (type == ZOO_CHANGED_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s changed...", __FUNCTION__, __LINE__, path);
            (*(_zkrm_path->callback.PathChanged))(_zkrm_path);
        }
        else if (type == ZOO_CHILD_EVENT)
        {
            mmLogger_LogV(gLogger, "%s %d %s children...", __FUNCTION__, __LINE__, path);
        }
    }
    // reset watcher again.
    __static_mmZkrmPath_Awexists(_zkrm_path);
}
static void __static_mmZkrmPath_Awexists(struct mmZkrmPath* p)
{
    int ret = zoo_awexists(p->zkhandle, mmString_CStr(&p->path), __static_mmZkrmPath_AwexistsWatcher, p, __static_mmZkrmPath_AwexistsStatCompletion, p);
    if (ZOK != ret)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d zerror:%s.", __FUNCTION__, __LINE__, zerror(ret));
    }
}

