/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmZookeeperZkrpPath_h__
#define __mmZookeeperZkrpPath_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmRbtreeU32.h"

#include "zookeeper.h"

#include "zookeeper/mmZookeeperExport.h"

#include "core/mmPrefix.h"

#define MM_ZKRP_FORMAT_1 "%u"
#define MM_ZKRP_FORMAT_2 "%u"
// connect invalid timeout.default is 3s(3000ms).
#define MM_ZKRP_PATH_TIMEOUT 3000

// /path
//       /0   /ZOO_EPHEMERAL
//       /1000/1000 buffer
//       /1001/1000 buffer

struct mmZkrpPath;
struct mmZkrpPathElem;
struct mmZkrpPathUnit;

typedef void(*mmZkrpPathElemUnitValueFunc)(struct mmZkrpPath* p, struct mmZkrpPathElem* e, struct mmZkrpPathUnit* u, const char* buffer, int offset, int length);
typedef void(*mmZkrpPathElemUnitEventFunc)(struct mmZkrpPath* p, struct mmZkrpPathElem* e, struct mmZkrpPathUnit* u);
typedef void(*mmZkrpPathElemValueFunc)(struct mmZkrpPath* p, struct mmZkrpPathElem* e, const char* buffer, int offset, int length);
typedef void(*mmZkrpPathElemEventFunc)(struct mmZkrpPath* p, struct mmZkrpPathElem* e);
typedef void(*mmZkrpPathEventFunc)(struct mmZkrpPath* p);

struct mmZkrpPathCallback
{
    mmZkrpPathElemUnitValueFunc PathElemUnitUpdated;
    mmZkrpPathElemUnitEventFunc PathElemUnitCreated;
    mmZkrpPathElemUnitEventFunc PathElemUnitDeleted;
    mmZkrpPathElemUnitEventFunc PathElemUnitChanged;
    mmZkrpPathElemValueFunc PathElemUpdated;
    mmZkrpPathElemEventFunc PathElemCreated;
    mmZkrpPathElemEventFunc PathElemDeleted;
    mmZkrpPathElemEventFunc PathElemChanged;
    mmZkrpPathEventFunc PathCreated;
    mmZkrpPathEventFunc PathDeleted;
    mmZkrpPathEventFunc PathChanged;
    mmZkrpPathEventFunc NetConnect;
    mmZkrpPathEventFunc NetExpired;
    void* obj;
};

MM_EXPORT_ZOOKEEPER void mmZkrpPathCallback_Init(struct mmZkrpPathCallback* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPathCallback_Destroy(struct mmZkrpPathCallback* p);

struct mmZkrpPathUnit
{
    struct Stat stat;
    mmAtomic_t locker;
    mmUInt32_t unique_id;
    // parent node.
    struct mmZkrpPath* zk_path;
    // parent node.
    struct mmZkrpPathElem* zk_path_elem;
};

MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Init(struct mmZkrpPathUnit* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Destroy(struct mmZkrpPathUnit* p);

MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Lock(struct mmZkrpPathUnit* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPathUnit_Unlock(struct mmZkrpPathUnit* p);

struct mmZkrpPathElem
{
    struct Stat stat;
    // rb tree for unique_id <--> mmZkrpPathUnit.
    struct mmRbtreeU32Vpt rbtree;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    mmUInt32_t unique_id;
    // parent node.
    struct mmZkrpPath* zk_path;
};

MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Init(struct mmZkrpPathElem* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Destroy(struct mmZkrpPathElem* p);

MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Lock(struct mmZkrpPathElem* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Unlock(struct mmZkrpPathElem* p);

MM_EXPORT_ZOOKEEPER struct mmZkrpPathUnit* mmZkrpPathElem_Add(struct mmZkrpPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Rmv(struct mmZkrpPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrpPathUnit* mmZkrpPathElem_Get(struct mmZkrpPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrpPathUnit* mmZkrpPathElem_GetInstance(struct mmZkrpPathElem* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrpPathElem_Clear(struct mmZkrpPathElem* p);

// note : /path/elem have no children, will auto adelete /path/elem.
struct mmZkrpPath
{
    // zk path for notify.
    struct mmString path;
    // zk cluster "host:port,host:port,"
    struct mmString host;
    // rb tree for unique_id <--> mmZkrpPathElem.
    struct mmRbtreeU32Vpt rbtree;
    // event for callback.
    struct mmZkrpPathCallback callback;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    // connect invalid timeout.default is MM_ZKRP_PATH_TIMEOUT.
    mmMSec_t timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // zk handle.
    zhandle_t* zkhandle;
};

MM_EXPORT_ZOOKEEPER void mmZkrpPath_Init(struct mmZkrpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Destroy(struct mmZkrpPath* p);

MM_EXPORT_ZOOKEEPER void mmZkrpPath_Start(struct mmZkrpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Interrupt(struct mmZkrpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Shutdown(struct mmZkrpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Join(struct mmZkrpPath* p);

// launch watcher the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Watcher(struct mmZkrpPath* p);
// finish abandon the path and child.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Abandon(struct mmZkrpPath* p);

MM_EXPORT_ZOOKEEPER void mmZkrpPath_Lock(struct mmZkrpPath* p);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Unlock(struct mmZkrpPath* p);

// assign net callback.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_SetCallback(struct mmZkrpPath* p, struct mmZkrpPathCallback* zkrp_path_callback);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_SetPath(struct mmZkrpPath* p, const char* path);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_SetHost(struct mmZkrpPath* p, const char* host);

// this function will check and try add watcher when the elem size is zero.
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Update(struct mmZkrpPath* p);

MM_EXPORT_ZOOKEEPER struct mmZkrpPathElem* mmZkrpPath_Add(struct mmZkrpPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Rmv(struct mmZkrpPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrpPathElem* mmZkrpPath_Get(struct mmZkrpPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER struct mmZkrpPathElem* mmZkrpPath_GetInstance(struct mmZkrpPath* p, mmUInt32_t unique_id);
MM_EXPORT_ZOOKEEPER void mmZkrpPath_Clear(struct mmZkrpPath* p);

#include "core/mmSuffix.h"

#endif//__mmZookeeperZkrpPath_h__
