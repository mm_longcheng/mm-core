/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisCommon_h__
#define __mmRedisCommon_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

struct mmRedisSync;

MM_EXPORT_REDIS long long mmRedis_StrDel(struct mmRedisSync* ctx, const char* key);
MM_EXPORT_REDIS long long mmRedis_StrExpire(struct mmRedisSync* ctx, const char* key, mmUInt32_t second);
MM_EXPORT_REDIS long long mmRedis_StrExpireAt(struct mmRedisSync *ctx, const char *key, mmUInt32_t timestamp);
MM_EXPORT_REDIS long long mmRedis_StrTtl(struct mmRedisSync *ctx, const char *key);

MM_EXPORT_REDIS int mmRedis_U32Incrby(struct mmRedisSync* ctx, const char* key, mmUInt32_t n, mmUInt32_t* v);
MM_EXPORT_REDIS int mmRedis_U64Incrby(struct mmRedisSync* ctx, const char* key, mmUInt32_t n, mmUInt64_t* v);

MM_EXPORT_REDIS int mmRedis_StrSetU32(struct mmRedisSync* ctx, const char* key, mmUInt32_t n);
MM_EXPORT_REDIS int mmRedis_StrSetU64(struct mmRedisSync* ctx, const char* key, mmUInt64_t n);

MM_EXPORT_REDIS int mmRedis_StrGetU32(struct mmRedisSync* ctx, const char* key, mmUInt32_t* n);
MM_EXPORT_REDIS int mmRedis_StrGetU64(struct mmRedisSync* ctx, const char* key, mmUInt64_t* n);

MM_EXPORT_REDIS int mmRedis_StrSetBin(struct mmRedisSync* ctx, const char* key, void* ptr, size_t size);
MM_EXPORT_REDIS int mmRedis_StrGetBin(struct mmRedisSync* ctx, const char* key, struct mmString* val);

MM_EXPORT_REDIS int mmRedis_Auth(struct mmRedisSync* ctx, const char* pwd);

MM_EXPORT_REDIS int mmRedis_ConfigTimeout(struct mmRedisSync* ctx, mmUInt32_t* timeout);

#include "core/mmSuffix.h"

#endif//__mmRedisCommon_h__
