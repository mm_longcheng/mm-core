/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisCommon.h"
#include "mmRedisSync.h"
#include "mmRedisReply.h"

#include "core/mmLogger.h"
#include "core/mmAtoi.h"

#include "hiredis.h"

MM_EXPORT_REDIS long long mmRedis_StrDel(struct mmRedisSync* ctx, const char* key)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "del %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[del %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[del %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedis_StrExpire(struct mmRedisSync* ctx, const char* key, mmUInt32_t second)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "expire %s %u", key, second);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[expire %s %u]. reply_is_null error:(%d)%s.",
                              key, second, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[expire %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, second, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedis_StrExpireAt(struct mmRedisSync *ctx, const char *key, mmUInt32_t timestamp)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "expireat %s %u", key, timestamp);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[expireat %s %u]. reply_is_null error:(%d)%s.",
                              key, timestamp, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[expireat %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, timestamp, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedis_StrTtl(struct mmRedisSync *ctx, const char *key)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "ttl %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[ttl %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[ttl %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedis_U32Incrby(struct mmRedisSync* ctx, const char* key, mmUInt32_t n, mmUInt32_t* v)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "incrby %s %u", key, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[incrby %s %u]. reply_is_null error:(%d)%s.",
                              key, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[incrby %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *v = (mmUInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedis_U64Incrby(struct mmRedisSync* ctx, const char* key, mmUInt32_t n, mmUInt64_t* v)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "incrby %s %u", key, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[incrby %s %u]. reply_is_null error:(%d)%s.",
                              key, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[incrby %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *v = (mmUInt64_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedis_StrSetU32(struct mmRedisSync* ctx, const char* key, mmUInt32_t n)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "set %s %u", key, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[set %s %u]. reply_is_null error:(%d)%s.",
                              key, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[set %s %u]. reply_have_error error:(%d)%s.",
                              key, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STATUS != r->type)
            {
                mmLogger_LogE(gLogger, "command[set %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (0 != strcasecmp(r->str, "OK"))
            {
                mmLogger_LogE(gLogger, "command[set %s %u]. reply_state_error type:%d error:(%d)%s.",
                              key, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedis_StrSetU64(struct mmRedisSync* ctx, const char* key, mmUInt64_t n)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "set %s %llu", key, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STATUS != r->type)
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (0 != strcasecmp(r->str, "OK"))
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIu64 "]. reply_state_error type:%d error:(%d)%s.",
                              key, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedis_StrGetU32(struct mmRedisSync* ctx, const char* key, mmUInt32_t* n)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "get %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_have_error error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *n = (mmUInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedis_StrGetU64(struct mmRedisSync* ctx, const char* key, mmUInt64_t* n)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "get %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_have_error error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *n = r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedis_StrSetBin(struct mmRedisSync* ctx, const char* key, void* ptr, size_t size)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "set %s %b", key, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, size, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIuPTR "]. reply_have_error error:(%d)%s.",
                              key, size, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STATUS != r->type)
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, size, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (0 != strcasecmp(r->str, "OK"))
            {
                mmLogger_LogE(gLogger, "command[set %s %" PRIuPTR "]. reply_state_error type:%d error:(%d)%s.",
                              key, size, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedis_StrGetBin(struct mmRedisSync* ctx, const char* key, struct mmString* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "get %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_have_error error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            mmString_Assignsn(val, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedis_Auth(struct mmRedisSync* ctx, const char* auth)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "auth %s", auth);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[auth %s]. reply_is_null error:(%d)%s.",
                              auth, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[auth %s]. reply_have_error error:(%d)%s.",
                              auth, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[auth %s]. reply_type_error type:%d error:(%d)%s.",
                              auth, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedis_ConfigTimeout(struct mmRedisSync* ctx, mmUInt32_t* timeout)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "config get timeout");
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[config get timeout]. reply_is_null.");
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[config get timeout]. reply_have_error error:%s",
                              r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[config get timeout]. reply_type_error type:%d.",
                              r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            if (2 > r->elements)
            {
                mmLogger_LogE(gLogger, "command[config get timeout]. reply_value_is_nil type:%d.",
                              r->type);
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            // localhost:6379> config get timeout
            // 1) "timeout"
            // 2) "10"
            *timeout = (mmUInt32_t)mmAtoi32(r->element[1]->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

