/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisSync_h__
#define __mmRedisSync_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"

#include "core/mmString.h"
#include "core/mmTime.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

#define MM_REDIS_SYNC_TRYTIMES 3
#define MM_REDIS_SYNC_CONN_TIMEOUT 3000
#define MM_REDIS_SYNC_IO_TIMEOUT -1
#define MM_REDIS_SYNC_TRY_MSLEEP_TIME 200

struct redisContext;

enum mmRedisSyncState_t
{
    MM_REDISSYNC_STATE_BROKEN = 0,
    MM_REDISSYNC_STATE_FINISH = 1,
};

enum mmRedisSyncRestrict_t
{
    MM_REDISSYNC_API_COMPLETE = 0,
    MM_REDISSYNC_API_RESTRICT = 1,
};

struct mmRedisSync
{
    // ip default is ""
    struct mmString auth;
    // ip default is "127.0.0.1"
    struct mmString node;
    // locker.
    mmAtomic_t locker;
    // port default is 6379.
    mmUInt16_t port;
    // connect time out ms.default is MM_REDIS_SYNC_CONN_TIMEOUT
    mmMSec_t conn_timeout;
    // io send recv timeout.default is MM_REDIS_SYNC_IO_TIMEOUT.
    mmMSec_t io_timeout;
    // try bind and listen times.default is MM_REDIS_SYNC_TRYTIMES.
    mmUInt32_t trytimes;
    // timeout for redis.conf.
    mmUInt32_t config_timeout;
    // timecode check select redis fd.
    mmUInt64_t timecode_check_select;
    // flag for connect state. MM_REDISSYNC_STATE_BROKEN is broken MM_REDISSYNC_STATE_FINISH is work.
    mmSInt8_t conn_state;
    // timeout check select redis fd read
    fd_set sr;
    // timeout check select redis fd write
    fd_set sw;
    // strong ref.
    struct redisContext* c;
    // cache error code.
    mmUInt32_t error_code;
    // redis server is a restricted api, can not use such as "config" "keys" command. 
    // default is MM_REDISSYNC_API_COMPLETE.
    mmSInt8_t api_restrict;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_REDIS void mmRedisSync_Init(struct mmRedisSync* p);
MM_EXPORT_REDIS void mmRedisSync_Destroy(struct mmRedisSync* p);

MM_EXPORT_REDIS void mmRedisSync_Lock(struct mmRedisSync* p);
MM_EXPORT_REDIS void mmRedisSync_Unlock(struct mmRedisSync* p);

MM_EXPORT_REDIS void mmRedisSync_SetRemote(struct mmRedisSync* p, const char* node, mmUShort_t port);
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisSync_SetAuth(struct mmRedisSync* p, const char* auth);
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisSync_SetApiRestrict(struct mmRedisSync* p, mmSInt8_t api_restrict);

// -1 alloc error
//  0 success.
//  1 net error.
MM_EXPORT_REDIS int mmRedisSync_Connect(struct mmRedisSync* p);
// -1 alloc error
//  0 success.
//  1 net error.
MM_EXPORT_REDIS int mmRedisSync_Reconnect(struct mmRedisSync* p);
// check and try connect if broken.
// 0 : is broken.if conn is not work, will connect single thread once.
// 1 : is working not broken.
MM_EXPORT_REDIS int mmRedisSync_Check(struct mmRedisSync* p);
// feedback the conn is broken.
MM_EXPORT_REDIS void mmRedisSync_ApplyBroken(struct mmRedisSync* p);
// feedback the conn is finish.
MM_EXPORT_REDIS void mmRedisSync_ApplyFinish(struct mmRedisSync* p);
// get the conn state.
MM_EXPORT_REDIS int mmRedisSync_State(struct mmRedisSync* p);
// synchronize attach to socket.not thread safe.
MM_EXPORT_REDIS void mmRedisSync_AttachSocket(struct mmRedisSync* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_REDIS void mmRedisSync_DetachSocket(struct mmRedisSync* p);
// shutdown_socket for the conn.application interrupt and shutdown process need call this.
MM_EXPORT_REDIS void mmRedisSync_ShutdownSocket(struct mmRedisSync* p);
// timeout for redis recv send;
MM_EXPORT_REDIS void mmRedisSync_SetIOTimeout(struct mmRedisSync* p, mmMSec_t milliseconds);

MM_EXPORT_REDIS void* mmRedisSync_Command(struct mmRedisSync* p, const char *format, ...);

MM_EXPORT_REDIS void mmRedisSync_FreeReply(struct mmRedisSync* p, void* reply);

MM_EXPORT_REDIS void mmRedisSync_Start(struct mmRedisSync* p);
MM_EXPORT_REDIS void mmRedisSync_Interrupt(struct mmRedisSync* p);
MM_EXPORT_REDIS void mmRedisSync_Shutdown(struct mmRedisSync* p);
MM_EXPORT_REDIS void mmRedisSync_Join(struct mmRedisSync* p);

#include "core/mmSuffix.h"

#endif//__mmRedisSync_h__
