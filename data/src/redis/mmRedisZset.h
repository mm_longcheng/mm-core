/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisZset_h__
#define __mmRedisZset_h__

#include "core/mmCore.h"

#include "container/mmRbtsetString.h"
#include "container/mmRbtreeU64.h"
#include "container/mmRbtreeF64.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

struct mmRedisSync;
struct mmString;

// return zset zadd new member size.
MM_EXPORT_REDIS long long mmRedisZset_StrBinZadd(struct mmRedisSync* ctx, const char* key, const char* score, void* ptr, size_t size);
MM_EXPORT_REDIS long long mmRedisZset_U32BinZadd(struct mmRedisSync* ctx, const char* key, mmUInt32_t score, void* ptr, size_t size);
MM_EXPORT_REDIS long long mmRedisZset_U64BinZadd(struct mmRedisSync* ctx, const char* key, mmUInt64_t score, void* ptr, size_t size);

MM_EXPORT_REDIS long long mmRedisZset_U32U64Zadd(struct mmRedisSync* ctx, const char* key, mmUInt32_t score, mmUInt64_t member);
MM_EXPORT_REDIS long long mmRedisZset_U64U64Zadd(struct mmRedisSync* ctx, const char* key, mmUInt64_t score, mmUInt64_t member);
MM_EXPORT_REDIS long long mmRedisZset_U32U64Zrem(struct mmRedisSync* ctx, const char* key, mmUInt64_t member);
// return reply_code
MM_EXPORT_REDIS int mmRedisZset_U32U64Zscore(struct mmRedisSync* ctx, const char* key, mmUInt64_t member, mmUInt32_t* score);
MM_EXPORT_REDIS int mmRedisZset_U64U64Zscore(struct mmRedisSync* ctx, const char* key, mmUInt64_t member, mmUInt64_t* score);

MM_EXPORT_REDIS int mmRedisZset_U32Zcard(struct mmRedisSync* ctx, const char* key, mmUInt32_t* length);
MM_EXPORT_REDIS int mmRedisZset_U64Zcard(struct mmRedisSync* ctx, const char* key, mmUInt64_t* length);

MM_EXPORT_REDIS int mmRedisZset_U32U64Zincrby(struct mmRedisSync *ctx, const char* key, mmUInt32_t score, mmUInt64_t member, mmUInt32_t *rs_score);

MM_EXPORT_REDIS int mmRedisZset_U64U32Zrevrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt32_t *index);
MM_EXPORT_REDIS int mmRedisZset_U64U64Zrevrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt64_t *index);

MM_EXPORT_REDIS int mmRedisZset_U64U32Zrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt32_t *index);
MM_EXPORT_REDIS int mmRedisZset_U64U64Zrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt64_t *index);

// zrange zrevrange zrangebyscore zrevrangebyscore
MM_EXPORT_REDIS int mmRedisZset_CmdBinZrange(struct mmRedisSync *ctx, const char *cmd, const char* key, mmUInt64_t index_l, mmUInt64_t index_r, struct mmRbtsetString* rbtree);
MM_EXPORT_REDIS int mmRedisZset_CmdF64BinZrangeWithscores(struct mmRedisSync *ctx, const char *cmd, const char* key, mmUInt64_t index_l, mmUInt64_t index_r, struct mmRbtreeF64String* rbtree);
MM_EXPORT_REDIS int mmRedisZset_CmdU64BinZrangeWithscores(struct mmRedisSync *ctx, const char *cmd, const char* key, mmUInt64_t index_l, mmUInt64_t index_r, struct mmRbtreeU64String* rbtree);

#include "core/mmSuffix.h"

#endif//__mmRedisZset_h__
