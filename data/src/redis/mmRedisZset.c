/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisZset.h"
#include "mmRedisSync.h"
#include "mmRedisReply.h"

#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmAtoi.h"

#include "hiredis.h"

MM_EXPORT_REDIS long long mmRedisZset_StrBinZadd(struct mmRedisSync* ctx, const char* key, const char* score, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zadd %s %s %b", key, score, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zadd %s %s %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, score, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[zadd %s %s %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, score, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisZset_U32BinZadd(struct mmRedisSync* ctx, const char* key, mmUInt32_t score, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zadd %s %u %b", key, score, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zadd %s %u %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, score, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[zadd %s %u %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, score, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisZset_U64BinZadd(struct mmRedisSync* ctx, const char* key, mmUInt64_t score, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zadd %s %llu %b", key, score, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zadd %s %" PRIu64 " %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, score, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[zadd %s %" PRIu64 " %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, score, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisZset_U32U64Zadd(struct mmRedisSync* ctx, const char* key, mmUInt32_t score, mmUInt64_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zadd %s %u %llu", key, score, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zadd %s %u %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, score, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[zadd %s %u %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, score, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisZset_U64U64Zadd(struct mmRedisSync* ctx, const char* key, mmUInt64_t score, mmUInt64_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zadd %s %llu %llu", key, score, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zadd %s %"PRIu64 " %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, score, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[zadd %s %"PRIu64 " %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, score, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisZset_U32U64Zrem(struct mmRedisSync* ctx, const char* key, mmUInt64_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zrem %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zrem %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[zrem %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_U32U64Zscore(struct mmRedisSync* ctx, const char* key, mmUInt64_t member, mmUInt32_t* score)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(score&&"score is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zscore %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zscore %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zscore %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zscore %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *score = mmAtoi32(r->str);
            ctx->error_code = MM_REPLYCODE_SUCCESS;
            rt = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisZset_U64U64Zscore(struct mmRedisSync* ctx, const char* key, mmUInt64_t member, mmUInt64_t* score)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(score&&"score is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zscore %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zscore %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zscore %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zscore %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *score = mmAtoi64(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisZset_U32Zcard(struct mmRedisSync* ctx, const char* key, mmUInt32_t* length)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(length&&"length is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zcard %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zcard %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zcard %s]. reply_have_error error:(%d)%s",
                              key, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type)
            {
                mmLogger_LogE(gLogger, "command[zcard %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *length = (mmUInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_U64Zcard(struct mmRedisSync* ctx, const char* key, mmUInt64_t* length)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(length&&"length is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zcard %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zcard %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zcard %s]. reply_have_error error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type)
            {
                mmLogger_LogE(gLogger, "command[zcard %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *length = (mmUInt64_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisZset_U32U64Zincrby(struct mmRedisSync *ctx, const char* key, mmUInt32_t score, mmUInt64_t member, mmUInt32_t *rs_score)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(rs_score&&"score is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zincrby %s %u %llu", key, score, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zincrby %s %u %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, score, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zincrby %s %u %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, score, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zincrby %s %u %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, score, member, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *rs_score = mmAtoi32(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisZset_U64U32Zrevrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt32_t *index)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(index&&"index is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zrevrank %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_have_error error:(%d)%s",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *index = (mmUInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_U64U64Zrevrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt64_t *index)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(index&&"index is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zrevrank %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_have_error error:(%d)%s",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *index = (mmUInt64_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_U64U32Zrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt32_t *index)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(index&&"index is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zrank %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_have_error error:(%d)%s",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *index = (mmUInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_U64U64Zrank(struct mmRedisSync *ctx, const char* key, mmUInt64_t member, mmUInt64_t *index)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    assert(index&&"index is a null.");
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "zrank %s %llu", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_have_error error:(%d)%s",
                              key, member, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[zrevrank %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *index = (mmUInt64_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisZset_CmdBinZrange(struct mmRedisSync *ctx, const char *cmd, const char* key, mmUInt64_t index_l, mmUInt64_t index_r, struct mmRbtsetString* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        struct mmString v;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "%s %s %llu %llu", cmd, key, index_l, index_r);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 "]. reply_is_null.",
                              cmd, key, index_l, index_r);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 "]. reply_have_error error:%s",
                              cmd, key, index_l, index_r, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 "]. reply_type_error type:%d.",
                              cmd, key, index_l, index_r, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                mmString_MakeWeaks(&v, r->element[i]->str);
                mmRbtsetString_Add(rbtree, &v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_CmdF64BinZrangeWithscores(struct mmRedisSync *ctx, const char *cmd, const char* key, mmUInt64_t index_l, mmUInt64_t index_r, struct mmRbtreeF64String* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmFloat64_t k = 0;
        struct mmString v;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "%s %s %llu %llu withscores", cmd, key, index_l, index_r);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 " withscores]. reply_is_null.",
                              cmd, key, index_l, index_r);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 " withscores]. reply_have_error error:%s",
                              cmd, key, index_l, index_r, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 " withscores]. reply_type_error type:%d.",
                              cmd, key, index_l, index_r, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmFloat64_t)atof(r->element[i - 1]->str);
                mmString_MakeWeaks(&v, r->element[i]->str);
                mmRbtreeF64String_Set(rbtree, k, &v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisZset_CmdU64BinZrangeWithscores(struct mmRedisSync *ctx, const char *cmd, const char* key, mmUInt64_t index_l, mmUInt64_t index_r, struct mmRbtreeU64String* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt64_t k = 0;
        struct mmString v;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "%s %s %llu %llu withscores", cmd, key, index_l, index_r);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 " withscores]. reply_is_null.",
                              cmd, key, index_l, index_r);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 " withscores]. reply_have_error error:%s",
                              cmd, key, index_l, index_r, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIu64 " %" PRIu64 " withscores]. reply_type_error type:%d.",
                              cmd, key, index_l, index_r, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt64_t)mmAtoi64(r->element[i - 1]->str);
                mmString_MakeWeaks(&v, r->element[i]->str);
                mmRbtreeU64String_Set(rbtree, k, &v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
