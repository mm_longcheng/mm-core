/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisList_h__
#define __mmRedisList_h__

#include "core/mmCore.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

struct mmRedisSync;
struct mmString;

// return list size after push.
MM_EXPORT_REDIS long long mmRedisList_Push(struct mmRedisSync* ctx, const char* cmd, const char* key, void* ptr, size_t size);
MM_EXPORT_REDIS int mmRedisList_Pop(struct mmRedisSync* ctx, const char* cmd, const char* key, struct mmString* val);
MM_EXPORT_REDIS int mmRedisList_Bpop(struct mmRedisSync* ctx, const char* cmd, const char* key, mmUInt32_t second, struct mmString* val);

MM_EXPORT_REDIS long long mmRedisList_Lpush(struct mmRedisSync* ctx, const char* key, void* ptr, size_t size);
MM_EXPORT_REDIS long long mmRedisList_Rpush(struct mmRedisSync* ctx, const char* key, void* ptr, size_t size);

MM_EXPORT_REDIS int mmRedisList_Lpop(struct mmRedisSync* ctx, const char* key, struct mmString* val);
MM_EXPORT_REDIS int mmRedisList_Rpop(struct mmRedisSync* ctx, const char* key, struct mmString* val);

MM_EXPORT_REDIS int mmRedisList_Blpop(struct mmRedisSync* ctx, const char* key, mmUInt32_t second, struct mmString* val);
MM_EXPORT_REDIS int mmRedisList_Brpop(struct mmRedisSync* ctx, const char* key, mmUInt32_t second, struct mmString* val);

MM_EXPORT_REDIS long long mmRedisList_Llen(struct mmRedisSync* ctx, const char* key);

#include "core/mmSuffix.h"

#endif//__mmRedisList_h__
