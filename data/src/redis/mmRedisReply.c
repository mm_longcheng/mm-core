/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisReply.h"
#include "core/mmLogger.h"
#include "hiredis.h"

// check reply type error.will logger if error occur.
MM_EXPORT_REDIS int mmRedisReply_CheckTypeError(void* reply, int type)
{
    redisReply* r = (redisReply*)reply;
    struct mmLogger* gLogger = mmLogger_Instance();
    if (NULL == r)
    {
        mmLogger_LogE(gLogger, "%s %d redis reply is null.", __FUNCTION__, __LINE__);
        return MM_REPLYCODE_REPLY_IS_NULL;
    }
    if (REDIS_REPLY_ERROR == r->type)
    {
        mmLogger_LogE(gLogger, "%s %d redis reply have error:%s", __FUNCTION__, __LINE__, r->str);
        return MM_REPLYCODE_REPLY_HAVE_ERROR;
    }
    if (type != r->type)
    {
        mmLogger_LogE(gLogger, "%s %d redis reply type:%d error.", __FUNCTION__, __LINE__, r->type);
        return MM_REPLYCODE_REPLY_TYPE_ERROR;
    }
    return MM_REPLYCODE_SUCCESS;
}
// check reply error.
MM_EXPORT_REDIS int mmRedisReply_CheckError(void* reply)
{
    redisReply* r = (redisReply*)reply;
    assert(reply && "reply is null.");
    if (REDIS_REPLY_ERROR == r->type)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d redis reply error:%s", __FUNCTION__, __LINE__, r->str);
        return MM_REPLYCODE_REPLY_HAVE_ERROR;
    }
    return MM_REPLYCODE_SUCCESS;
}
// return reply type.
MM_EXPORT_REDIS int mmRedisReply_Type(void* reply)
{
    assert(reply && "reply is null.");
    return ((redisReply*)reply)->type;
}

// return reply elem at index.
MM_EXPORT_REDIS size_t mmRedisReply_ElemSize(void* reply)
{
    assert(reply && "reply is null.");
    return ((redisReply*)reply)->elements;
}
// return reply elem at index.
MM_EXPORT_REDIS void* mmRedisReply_Elem(void* reply, size_t index)
{
    assert(reply && "reply is null.");
    return ((redisReply*)reply)->element[index];
}

MM_EXPORT_REDIS char* mmRedisReply_Str(void* reply)
{
    assert(reply && "reply is null.");
    return ((redisReply*)reply)->str;
}
MM_EXPORT_REDIS size_t mmRedisReply_Len(void* reply)
{
    assert(reply && "reply is null.");
    return ((redisReply*)reply)->len;
}
// reply value cast as buffer.it is weak ref.will check the reply type.
MM_EXPORT_REDIS void mmRedisReply_CastAsBuffer(void* reply, void** buff, size_t* size)
{
    redisReply* r = (redisReply*)reply;
    assert(reply && "reply is null.");
    assert(buff && "buff is null.");
    assert(size && "size is null.");
    *buff = r->str;
    *size = r->len;
}
// reply value cast as integer.will check the reply type.
MM_EXPORT_REDIS long long mmRedisReply_CastAsInteger(void* reply)
{
    assert(reply && "reply is null.");
    return ((redisReply*)reply)->integer;
}
// reply value cast as state.will check the reply type.
MM_EXPORT_REDIS int mmRedisReply_CastAsState(void* reply)
{
    redisReply* r = (redisReply*)reply;
    assert(reply && "reply is null.");
    return
        (REDIS_REPLY_STATUS != r->type || 0 != strcasecmp(r->str, "OK")) ?
        MM_REDIS_REPLY_STATUS_ERR : MM_REDIS_REPLY_STATUS_OK;
}
// reply value cast as string,return "" if reply nil.not check the reply type.
MM_EXPORT_REDIS char* mmRedisReply_CastAsString(void* reply)
{
    redisReply* r = (redisReply*)reply;
    assert(reply && "reply is null.");
    return REDIS_REPLY_NIL == r->type ? "" : r->str;
}

