/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisHash.h"
#include "mmRedisSync.h"
#include "mmRedisReply.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmAtoi.h"
#include "hiredis.h"

MM_EXPORT_REDIS long long mmRedisHash_StrBinHset(struct mmRedisSync* ctx, const char* key, const char* fid, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %s %b", key, fid, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, fid, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %" PRIuPTR "]. reply_have_error error:(%d)%s.",
                              key, fid, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisHash_StrU32Hset(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt32_t val)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %s %u", key, fid, val);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %u]. reply_is_null error:(%d)%s.",
                              key, fid, val, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %u]. reply_have_error error:(%d)%s.",
                              key, fid, val, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, val, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisHash_StrU64Hset(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt64_t val)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %s %llu", key, fid, val);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, val, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, fid, val, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hset %s %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, val, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
// note: you must know what type can memcpy to ptr.
MM_EXPORT_REDIS int mmRedisHash_StrBinHget(struct mmRedisSync* ctx, const char* key, const char* fid, void* ptr, size_t size)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %s", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            if (r->len != size)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_type_error r->len:%" PRIuPTR " size:%" PRIuPTR " error:(%d)%s.",
                              key, fid, r->len, size, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            mmMemcpy(ptr, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_StrU32Hget(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt32_t* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %s", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *val = mmAtoi32(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_StrU64Hget(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt64_t* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %s", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *val = mmAtoi64(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisHash_StrHdel(struct mmRedisSync* ctx, const char* key, const char* fid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hdel %s %s", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hdel %s %s]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[hdel %s %s]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisHash_U32BinHset(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %u %b", key, fid, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %u %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, fid, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[hset %s %u %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
// note: you must know what type can memcpy to ptr.
MM_EXPORT_REDIS int mmRedisHash_U32BinHget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, void* ptr, size_t size)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %u", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            if (r->len != size)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_type_error r->len:%" PRIuPTR " size:%" PRIuPTR " error:(%d)%s.",
                              key, fid, r->len, size, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            mmMemcpy(ptr, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisHash_U64BinHset(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %llu %b", key, fid, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %" PRIu64 " %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              key, fid, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[hset %s %" PRIu64 " %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
// note: you must know what type can memcpy to ptr.
MM_EXPORT_REDIS int mmRedisHash_U64BinHget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, void* ptr, size_t size)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %llu", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            if (r->len != size)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_type_error r->len:%" PRIuPTR
                              " size:%" PRIuPTR " error:(%d)%s.", key, fid, r->len, size, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            mmMemcpy(ptr, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}


MM_EXPORT_REDIS long long mmRedisHash_U32U32Hset(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt32_t vid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %u %u", key, fid, vid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %u %u]. reply_is_null error:(%d)%s.",
                              key, fid, vid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[hset %s %u %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, vid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisHash_U32U64Hset(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt64_t vid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %u %llu", key, fid, vid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %u %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, vid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[hset %s %u %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, vid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U32Hget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt32_t* vid)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %u", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *vid = mmAtoi32(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U64Hget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt64_t* vid)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %u", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *vid = mmAtoi64(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisHash_U32Hdel(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hdel %s %u", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hdel %s %u]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[hdel %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisHash_U64U32Hset(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt32_t vid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %llu %u", key, fid, vid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %" PRIu64 " %u]. reply_is_null error:(%d)%s.",
                              key, fid, vid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[hset %s %" PRIu64 " %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, vid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisHash_U64U64Hset(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt64_t vid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hset %s %llu %llu", key, fid, vid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hset %s %" PRIu64 " %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, vid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[hset %s %" PRIu64 " %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, vid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U32Hget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt32_t* vid)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %llu", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *vid = mmAtoi32(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U64Hget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt64_t* vid)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %llu", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            *vid = mmAtoi64(r->str);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisHash_U64Hdel(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hdel %s %llu", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hdel %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[hdel %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_StrStringHget(struct mmRedisSync* ctx, const char* key, const char* fid, struct mmString* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    mmString_Clear(val);
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %s", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_have_error error:(%d)%s",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %s]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            mmString_Assignsn(val, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32StringHget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, struct mmString* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    mmString_Clear(val);
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %u", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            mmString_Assignsn(val, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64StringHget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, struct mmString* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    mmString_Clear(val);
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hget %s %llu", key, fid);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_have_error error:(%d)%s.",
                              key, fid, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[hget %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            mmString_Assignsn(val, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_U32Hlen(struct mmRedisSync* ctx, const char* key, mmUInt32_t* len)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hlen %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hlen %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hlen %s]. reply_have_error error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type)
            {
                mmLogger_LogE(gLogger, "command[hlen %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *len = (mmUInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_U64Hlen(struct mmRedisSync* ctx, const char* key, mmUInt64_t* len)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hlen %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hlen %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hlen %s]. reply_have_error error:(%d)%s",
                              key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_INTEGER != r->type)
            {
                mmLogger_LogE(gLogger, "command[hlen %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *len = (mmUInt64_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_U32Hincrby(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmSInt32_t n, mmSInt32_t* v)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hincrby %s %u %d", key, fid, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hincrby %s %u %d]. reply_is_null error:(%d)%s.",
                              key, fid, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[hincrby %s %u %d]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *v = (mmSInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_U64Hincrby(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmSInt32_t n, mmSInt32_t* v)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hincrby %s %llu %d", key, fid, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hincrby %s %" PRIu64 " %d]. reply_is_null error:(%d)%s.",
                              key, fid, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[hincrby %s %" PRIu64 " %d]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *v = (mmSInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_StrHincrby(struct mmRedisSync* ctx, const char* key, const char *fid, mmSInt32_t n, mmSInt32_t* v)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hincrby %s %s %d", key, fid, n);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hincrby %s %s %d]. reply_is_null error:(%d)%s.",
                              key, fid, n, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[hincrby %s %s %d]. reply_type_error type:%d error:(%d)%s.",
                              key, fid, n, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            *v = (mmSInt32_t)r->integer;
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS int mmRedisHash_U32U32Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU32U32* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt32_t k = 0;
        mmUInt32_t v = 0;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt32_t)mmAtoi32(r->element[i - 1]->str);
                v = (mmUInt32_t)mmAtoi32(r->element[i    ]->str);
                mmRbtreeU32U32_Set(rbtree, k, v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U64Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU32U64* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt32_t k = 0;
        mmUInt64_t v = 0;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt32_t)mmAtoi32(r->element[i - 1]->str);
                v = (mmUInt64_t)mmAtoi64(r->element[i    ]->str);
                mmRbtreeU32U64_Set(rbtree, k, v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32BinHgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU32String* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt32_t k = 0;
        struct mmString v;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt32_t)mmAtoi32(r->element[i - 1]->str);
                mmString_MakeWeaks(&v, r->element[i]->str);
                mmRbtreeU32String_Set(rbtree, k, &v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U32Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU64U32* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt64_t k = 0;
        mmUInt32_t v = 0;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt64_t)mmAtoi64(r->element[i - 1]->str);
                v = (mmUInt32_t)mmAtoi32(r->element[i    ]->str);
                mmRbtreeU64U32_Set(rbtree, k, v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U64Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU64U64* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt64_t k = 0;
        mmUInt64_t v = 0;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt64_t)mmAtoi64(r->element[i - 1]->str);
                v = (mmUInt64_t)mmAtoi64(r->element[i    ]->str);
                mmRbtreeU64U64_Set(rbtree, k, v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64BinHgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU64String* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        mmUInt64_t k = 0;
        struct mmString v;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                k = (mmUInt64_t)mmAtoi64(r->element[i - 1]->str);
                mmString_MakeWeaks(&v, r->element[i]->str);
                mmRbtreeU64String_Set(rbtree, k, &v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_StringBinHgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeStringString* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        size_t i = 1;
        struct mmString k;
        struct mmString v;
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "hgetall %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_is_null.", key);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_have_error error:%s", key, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hgetall %s]. reply_type_error type:%d.", key, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // elements
            for (i = 1; i < r->elements; i += 2)
            {
                mmString_MakeWeaks(&k, r->element[i - 1]->str);
                mmString_MakeWeaks(&v, r->element[i    ]->str);
                mmRbtreeStringString_Set(rbtree, &k, &v);
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS void mmRedisHash_U32HmgetFormat(struct mmRbtsetU32* rbtset, struct mmString* format)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    size_t sz = rbtset->size + 10 + rbtset->size * MM_INT32_LEN;
    //
    mmString_Clear(format);
    mmString_Reserve(format, sz);
    mmString_Sprintf(format, "hmget %%s");
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mmString_Sprintf(format, " %" PRIu32 "", it->k);
    }
}
MM_EXPORT_REDIS void mmRedisHash_U64HmgetFormat(struct mmRbtsetU64* rbtset, struct mmString* format)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    size_t sz = rbtset->size + 10 + rbtset->size * MM_INT64_LEN;
    //
    mmString_Clear(format);
    mmString_Reserve(format, sz);
    mmString_Sprintf(format, "hmget %%s");
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mmString_Sprintf(format, " %" PRIu64 "", it->k);
    }
}
MM_EXPORT_REDIS void mmRedisHash_StringHmgetFormat(struct mmRbtsetString* rbtset, struct mmString* format)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetStringIterator* it = NULL;
    size_t sz = rbtset->size + 10;
    //
    mmString_Clear(format);
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetStringIterator, n);
        n = mmRb_Next(n);
        sz += mmString_Size(&it->k);
    }
    mmString_Reserve(format, sz);
    mmString_Sprintf(format, "hmget %%s");
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetStringIterator, n);
        n = mmRb_Next(n);
        mmString_Sprintf(format, "%s ", mmString_CStr(&it->k));
    }
}
MM_EXPORT_REDIS int mmRedisHash_U32U32Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U32* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_U32HmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetU32Iterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
                    n = mmRb_Next(n);
                    mmRbtreeU32U32_Set(rbtree, it->k, (mmUInt32_t)mmAtoi32(r->element[i]->str));
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U64Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U64* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_U32HmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetU32Iterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
                    n = mmRb_Next(n);
                    mmRbtreeU32U64_Set(rbtree, it->k, (mmUInt64_t)mmAtoi64(r->element[i]->str));
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32BinHmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, struct mmRbtreeU32String* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_U32HmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                struct mmString v;
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetU32Iterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
                    n = mmRb_Next(n);
                    mmString_MakeWeaks(&v, r->element[i]->str);
                    mmRbtreeU32String_Set(rbtree, it->k, &v);
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U32Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U32* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_U64HmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetU64Iterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
                    n = mmRb_Next(n);
                    mmRbtreeU64U32_Set(rbtree, it->k, (mmUInt32_t)mmAtoi32(r->element[i]->str));
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U64Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U64* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_U64HmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetU64Iterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
                    n = mmRb_Next(n);
                    mmRbtreeU64U64_Set(rbtree, it->k, (mmUInt64_t)mmAtoi64(r->element[i]->str));
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64BinHmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, struct mmRbtreeU64String* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_U64HmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                struct mmString v;
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetU64Iterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
                    n = mmRb_Next(n);
                    mmString_MakeWeaks(&v, r->element[i]->str);
                    mmRbtreeU64String_Set(rbtree, it->k, &v);
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_StringBinHmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetString* rbtset, struct mmRbtreeStringString* rbtree)
{
    int rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmString format;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmString_Init(&format);
        do
        {
            mmRedisHash_StringHmgetFormat(rbtset, &format);
            r = (redisReply*)redisCommand(c, mmString_CStr(&format), key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_is_null.",
                              key, rbtset->size);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_have_error error:%s",
                              key, rbtset->size, r->str);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (REDIS_REPLY_ARRAY != r->type)
            {
                mmLogger_LogE(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. reply_type_error type:%d.",
                              key, rbtset->size, r->type);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            if (r->elements != rbtset->size)
            {
                // we only logger V not need break.
                mmLogger_LogV(gLogger, "command[hmget %s [rbtset.size = %" PRIuPTR "]]. type:%d. Incomplete.",
                              key, rbtset->size, r->type);
            }
            // elements
            {
                struct mmString v;
                size_t i = 0;
                struct mmRbNode* n = NULL;
                struct mmRbtsetStringIterator* it = NULL;
                //
                n = mmRb_First(&rbtset->rbt);
                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtsetStringIterator, n);
                    n = mmRb_Next(n);
                    mmString_MakeWeaks(&v, r->element[i]->str);
                    mmRbtreeStringString_Set(rbtree, &it->k, &v);
                    i++;
                }
            }
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmString_Destroy(&format);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U32HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, size_t page, struct mmRbtreeU32U32* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    //
    struct mmRbtsetU32 rbtset_val;
    mmRbtsetU32_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU32_Add(&rbtset_val, it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_U32U32Hmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetU32_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_U32U32Hmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetU32_Clear(&rbtset_val);
    }
    mmRbtsetU32_Destroy(&rbtset_val);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U64HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, size_t page, struct mmRbtreeU32U64* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    //
    struct mmRbtsetU32 rbtset_val;
    mmRbtsetU32_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU32_Add(&rbtset_val, it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_U32U64Hmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetU32_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_U32U64Hmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetU32_Clear(&rbtset_val);
    }
    mmRbtsetU32_Destroy(&rbtset_val);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32BinHmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, size_t page, struct mmRbtreeU32String* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    //
    struct mmRbtsetU32 rbtset_val;
    mmRbtsetU32_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU32_Add(&rbtset_val, it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_U32BinHmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetU32_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_U32BinHmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetU32_Clear(&rbtset_val);
    }
    mmRbtsetU32_Destroy(&rbtset_val);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U32HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, size_t page, struct mmRbtreeU64U32* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    //
    struct mmRbtsetU64 rbtset_val;
    mmRbtsetU64_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU64_Add(&rbtset_val, it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_U64U32Hmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetU64_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_U64U32Hmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetU64_Clear(&rbtset_val);
    }
    mmRbtsetU64_Destroy(&rbtset_val);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U64HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, size_t page, struct mmRbtreeU64U64* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    //
    struct mmRbtsetU64 rbtset_val;
    mmRbtsetU64_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU64_Add(&rbtset_val, it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_U64U64Hmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetU64_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_U64U64Hmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetU64_Clear(&rbtset_val);
    }
    mmRbtsetU64_Destroy(&rbtset_val);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64BinHmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, size_t page, struct mmRbtreeU64String* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    //
    struct mmRbtsetU64 rbtset_val;
    mmRbtsetU64_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU64_Add(&rbtset_val, it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_U64BinHmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetU64_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_U64BinHmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetU64_Clear(&rbtset_val);
    }
    mmRbtsetU64_Destroy(&rbtset_val);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_StringBinHmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetString* rbtset, size_t page, struct mmRbtreeStringString* rbtree)
{
    int rt = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetStringIterator* it = NULL;
    //
    struct mmRbtsetString rbtset_val;
    mmRbtsetString_Init(&rbtset_val);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetStringIterator, n);
        n = mmRb_Next(n);
        mmRbtsetString_Add(&rbtset_val, &it->k);
        if (rbtset_val.size == page)
        {
            rt = mmRedisHash_StringBinHmget(ctx, key, &rbtset_val, rbtree);
            mmRbtsetString_Clear(&rbtset_val);
            if (0 != rt) { break; }
        }
    }
    if (0 != rbtset_val.size)
    {
        rt = mmRedisHash_StringBinHmget(ctx, key, &rbtset_val, rbtree);
        mmRbtsetString_Clear(&rbtset_val);
    }
    mmRbtsetString_Destroy(&rbtset_val);
    return rt;
}

static void* __static_mmRedisHash_ModRbtreeU32RbtsetU64StrongProduce(struct mmRbtreeU32Vpt* p, mmUInt32_t k)
{
    struct mmRbtsetU64* e = (struct mmRbtsetU64*)mmMalloc(sizeof(struct mmRbtsetU64));
    mmRbtsetU64_Init(e);
    return e;
}
static void* __static_mmRedisHash_ModRbtreeU32RbtsetU64StrongRecycle(struct mmRbtreeU32Vpt* p, mmUInt32_t k, void* v)
{
    struct mmRbtsetU64* e = (struct mmRbtsetU64*)(v);
    mmRbtsetU64_Destroy(e);
    mmFree(e);
    return NULL;
}
static void* __static_mmRedisHash_ModRbtreeU32RbtsetU32StrongProduce(struct mmRbtreeU32Vpt* p, mmUInt32_t k)
{
    struct mmRbtsetU32* e = (struct mmRbtsetU32*)mmMalloc(sizeof(struct mmRbtsetU32));
    mmRbtsetU32_Init(e);
    return e;
}
static void* __static_mmRedisHash_ModRbtreeU32RbtsetU32StrongRecycle(struct mmRbtreeU32Vpt* p, mmUInt32_t k, void* v)
{
    struct mmRbtsetU32* e = (struct mmRbtsetU32*)(v);
    mmRbtsetU32_Destroy(e);
    mmFree(e);
    return NULL;
}

MM_EXPORT_REDIS int mmRedisHash_U32U32HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U32* rbtree)
{
    int rt = 0;
    mmUInt32_t mod_index = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    struct mmRbtsetU32* mod_rbtset = NULL;
    struct mmString pattern_key;
    struct mmRbtreeU32Vpt rbtree_u32_vpt;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    mmString_Init(&pattern_key);
    mmRbtreeU32Vpt_Init(&rbtree_u32_vpt);
    _U32VptAllocator.Produce = &__static_mmRedisHash_ModRbtreeU32RbtsetU32StrongProduce;
    _U32VptAllocator.Recycle = &__static_mmRedisHash_ModRbtreeU32RbtsetU32StrongRecycle;
    mmRbtreeU32Vpt_SetAllocator(&rbtree_u32_vpt, &_U32VptAllocator);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mod_index = it->k % mod_number;
        mod_rbtset = (struct mmRbtsetU32*)mmRbtreeU32Vpt_GetInstance(&rbtree_u32_vpt, mod_index);
        mmRbtsetU32_Add(mod_rbtset, it->k);
    }
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        n = mmRb_First(&rbtree_u32_vpt.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            mod_rbtset = (struct mmRbtsetU32*)(it->v);
            mmString_Clear(&pattern_key);
            mmString_Sprintf(&pattern_key, pattern, it->k);
            rt = mmRedisHash_U32U32HmgetPage(ctx, mmString_CStr(&pattern_key), mod_rbtset, page, rbtree);
            if (0 != rt) { break; }
        }
    }
    mmString_Destroy(&pattern_key);
    mmRbtreeU32Vpt_Destroy(&rbtree_u32_vpt);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32U64HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U64* rbtree)
{
    int rt = 0;
    mmUInt32_t mod_index = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    struct mmRbtsetU32* mod_rbtset = NULL;
    struct mmString pattern_key;
    struct mmRbtreeU32Vpt rbtree_u32_vpt;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    mmString_Init(&pattern_key);
    mmRbtreeU32Vpt_Init(&rbtree_u32_vpt);
    _U32VptAllocator.Produce = &__static_mmRedisHash_ModRbtreeU32RbtsetU32StrongProduce;
    _U32VptAllocator.Recycle = &__static_mmRedisHash_ModRbtreeU32RbtsetU32StrongRecycle;
    mmRbtreeU32Vpt_SetAllocator(&rbtree_u32_vpt, &_U32VptAllocator);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mod_index = it->k % mod_number;
        mod_rbtset = (struct mmRbtsetU32*)mmRbtreeU32Vpt_GetInstance(&rbtree_u32_vpt, mod_index);
        mmRbtsetU32_Add(mod_rbtset, it->k);
    }
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        n = mmRb_First(&rbtree_u32_vpt.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            mod_rbtset = (struct mmRbtsetU32*)(it->v);
            mmString_Clear(&pattern_key);
            mmString_Sprintf(&pattern_key, pattern, it->k);
            rt = mmRedisHash_U32U64HmgetPage(ctx, mmString_CStr(&pattern_key), mod_rbtset, page, rbtree);
            if (0 != rt) { break; }
        }
    }
    mmString_Destroy(&pattern_key);
    mmRbtreeU32Vpt_Destroy(&rbtree_u32_vpt);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U32BinHmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU32* rbtset, struct mmRbtreeU32String* rbtree)
{
    int rt = 0;
    mmUInt32_t mod_index = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    struct mmRbtsetU32* mod_rbtset = NULL;
    struct mmString pattern_key;
    struct mmRbtreeU32Vpt rbtree_u32_vpt;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    mmString_Init(&pattern_key);
    mmRbtreeU32Vpt_Init(&rbtree_u32_vpt);
    _U32VptAllocator.Produce = &__static_mmRedisHash_ModRbtreeU32RbtsetU32StrongProduce;
    _U32VptAllocator.Recycle = &__static_mmRedisHash_ModRbtreeU32RbtsetU32StrongRecycle;
    mmRbtreeU32Vpt_SetAllocator(&rbtree_u32_vpt, &_U32VptAllocator);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        n = mmRb_Next(n);
        mod_index = it->k % mod_number;
        mod_rbtset = (struct mmRbtsetU32*)mmRbtreeU32Vpt_GetInstance(&rbtree_u32_vpt, mod_index);
        mmRbtsetU32_Add(mod_rbtset, it->k);
    }
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        n = mmRb_First(&rbtree_u32_vpt.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            mod_rbtset = (struct mmRbtsetU32*)(it->v);
            mmString_Clear(&pattern_key);
            mmString_Sprintf(&pattern_key, pattern, it->k);
            rt = mmRedisHash_U32BinHmgetPage(ctx, mmString_CStr(&pattern_key), mod_rbtset, page, rbtree);
            if (0 != rt) { break; }
        }
    }
    mmString_Destroy(&pattern_key);
    mmRbtreeU32Vpt_Destroy(&rbtree_u32_vpt);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U32HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U32* rbtree)
{
    int rt = 0;
    mmUInt32_t mod_index = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    struct mmRbtsetU64* mod_rbtset = NULL;
    struct mmString pattern_key;
    struct mmRbtreeU32Vpt rbtree_u32_vpt;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    mmString_Init(&pattern_key);
    mmRbtreeU32Vpt_Init(&rbtree_u32_vpt);
    _U32VptAllocator.Produce = &__static_mmRedisHash_ModRbtreeU32RbtsetU64StrongProduce;
    _U32VptAllocator.Recycle = &__static_mmRedisHash_ModRbtreeU32RbtsetU64StrongRecycle;
    mmRbtreeU32Vpt_SetAllocator(&rbtree_u32_vpt, &_U32VptAllocator);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mod_index = it->k % mod_number;
        mod_rbtset = (struct mmRbtsetU64*)mmRbtreeU32Vpt_GetInstance(&rbtree_u32_vpt, mod_index);
        mmRbtsetU64_Add(mod_rbtset, it->k);
    }
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        n = mmRb_First(&rbtree_u32_vpt.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            mod_rbtset = (struct mmRbtsetU64*)(it->v);
            mmString_Clear(&pattern_key);
            mmString_Sprintf(&pattern_key, pattern, it->k);
            rt = mmRedisHash_U64U32HmgetPage(ctx, mmString_CStr(&pattern_key), mod_rbtset, page, rbtree);
            if (0 != rt) { break; }
        }
    }
    mmString_Destroy(&pattern_key);
    mmRbtreeU32Vpt_Destroy(&rbtree_u32_vpt);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64U64HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U64* rbtree)
{
    int rt = 0;
    mmUInt32_t mod_index = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    struct mmRbtsetU64* mod_rbtset = NULL;
    struct mmString pattern_key;
    struct mmRbtreeU32Vpt rbtree_u32_vpt;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    mmString_Init(&pattern_key);
    mmRbtreeU32Vpt_Init(&rbtree_u32_vpt);
    _U32VptAllocator.Produce = &__static_mmRedisHash_ModRbtreeU32RbtsetU64StrongProduce;
    _U32VptAllocator.Recycle = &__static_mmRedisHash_ModRbtreeU32RbtsetU64StrongRecycle;
    mmRbtreeU32Vpt_SetAllocator(&rbtree_u32_vpt, &_U32VptAllocator);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mod_index = it->k % mod_number;
        mod_rbtset = (struct mmRbtsetU64*)mmRbtreeU32Vpt_GetInstance(&rbtree_u32_vpt, mod_index);
        mmRbtsetU64_Add(mod_rbtset, it->k);
    }
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        n = mmRb_First(&rbtree_u32_vpt.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            mod_rbtset = (struct mmRbtsetU64*)(it->v);
            mmString_Clear(&pattern_key);
            mmString_Sprintf(&pattern_key, pattern, it->k);
            rt = mmRedisHash_U64U64HmgetPage(ctx, mmString_CStr(&pattern_key), mod_rbtset, page, rbtree);
            if (0 != rt) { break; }
        }
    }
    mmString_Destroy(&pattern_key);
    mmRbtreeU32Vpt_Destroy(&rbtree_u32_vpt);
    return rt;
}
MM_EXPORT_REDIS int mmRedisHash_U64BinHmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU64* rbtset, struct mmRbtreeU64String* rbtree)
{
    int rt = 0;
    mmUInt32_t mod_index = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    struct mmRbtsetU64* mod_rbtset = NULL;
    struct mmString pattern_key;
    struct mmRbtreeU32Vpt rbtree_u32_vpt;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    mmString_Init(&pattern_key);
    mmRbtreeU32Vpt_Init(&rbtree_u32_vpt);
    _U32VptAllocator.Produce = &__static_mmRedisHash_ModRbtreeU32RbtsetU64StrongProduce;
    _U32VptAllocator.Recycle = &__static_mmRedisHash_ModRbtreeU32RbtsetU64StrongRecycle;
    mmRbtreeU32Vpt_SetAllocator(&rbtree_u32_vpt, &_U32VptAllocator);
    //
    n = mmRb_First(&rbtset->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mod_index = it->k % mod_number;
        mod_rbtset = (struct mmRbtsetU64*)mmRbtreeU32Vpt_GetInstance(&rbtree_u32_vpt, mod_index);
        mmRbtsetU64_Add(mod_rbtset, it->k);
    }
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        n = mmRb_First(&rbtree_u32_vpt.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            mod_rbtset = (struct mmRbtsetU64*)(it->v);
            mmString_Clear(&pattern_key);
            mmString_Sprintf(&pattern_key, pattern, it->k);
            rt = mmRedisHash_U64BinHmgetPage(ctx, mmString_CStr(&pattern_key), mod_rbtset, page, rbtree);
            if (0 != rt) { break; }
        }
    }
    mmString_Destroy(&pattern_key);
    mmRbtreeU32Vpt_Destroy(&rbtree_u32_vpt);
    return rt;
}
