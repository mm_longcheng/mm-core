/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisPoper_h__
#define __mmRedisPoper_h__

#include "core/mmCore.h"
#include "core/mmThread.h"

#include "redis/mmRedisSync.h"

#include <pthread.h>

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

#define MM_REDIS_POPER_IO_TIMEOUT -1
// second.default is 0 (never).
#define MM_REDIS_POPER_BLOCK_TIMEOUT 0
// millisecond.default is 2000.
#define MM_REDIS_POPER_EMPTY_KEY_SLEEP_TIME 2000
// millisecond.default is 1000.
#define MM_REDIS_POPER_EMPTY_VAL_SLEEP_TIME 1000
// millisecond.default is 3000.
#define MM_REDIS_POPER_ERROR_CTX_SLEEP_TIME 3000

struct mmRedisPoper;

struct mmRedisPoperCallback
{
    void(*Poper)(struct mmRedisPoper* p, void* u, struct mmString* data);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_REDIS void mmRedisPoperCallback_Init(struct mmRedisPoperCallback* p);
MM_EXPORT_REDIS void mmRedisPoperCallback_Destroy(struct mmRedisPoperCallback* p);

struct mmRedisPoper
{
    struct mmRedisSync ctx;
    // default is rpop
    struct mmString cmd;
    // default is queue
    struct mmString key;
    struct mmRedisPoperCallback callback;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t locker;
    // thread.
    pthread_t thread_poper;
    // block or not MM_NONBLOCK MM_BLOCKING, default is MM_BLOCKING.assign cmd will auto assign pop_mode.
    mmSInt8_t pop_mode;
    // io send recv timeout. default is MM_REDIS_POPER_IO_TIMEOUT(invalid).
    mmMSec_t io_timeout;
    // second.default is MM_REDIS_POPER_BLOCK_TIMEOUT.
    // note: block is diffent about send recv timeout.only bpop use it.
    mmMSec_t block_timeout;
    // MM_REDIS_POPER_EMPTY_KEY_SLEEP_TIME
    mmMSec_t sleep_empty_key_timeout;
    // MM_REDIS_POPER_EMPTY_VAL_SLEEP_TIME
    mmMSec_t sleep_empty_val_timeout;
    // MM_REDIS_POPER_ERROR_CTX_SLEEP_TIME
    mmMSec_t sleep_error_ctx_timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_REDIS void mmRedisPoper_Init(struct mmRedisPoper* p);
MM_EXPORT_REDIS void mmRedisPoper_Destroy(struct mmRedisPoper* p);

/* locker order is ctx -> locker. */
MM_EXPORT_REDIS void mmRedisPoper_Lock(struct mmRedisPoper* p);
MM_EXPORT_REDIS void mmRedisPoper_Unlock(struct mmRedisPoper* p);

// lpop rpop blpop brpop
MM_EXPORT_REDIS void mmRedisPoper_SetCmd(struct mmRedisPoper* p, const char* cmd);
MM_EXPORT_REDIS void mmRedisPoper_SetKey(struct mmRedisPoper* p, const char* key);
MM_EXPORT_REDIS void mmRedisPoper_SetSleepTimeout(struct mmRedisPoper* p, mmMSec_t empty_key_timeout, mmMSec_t empty_val_timeout, mmMSec_t error_ctx_timeout);

// set callback.you can not do this at thread runing.
MM_EXPORT_REDIS void mmRedisPoper_SetCallback(struct mmRedisPoper* p, struct mmRedisPoperCallback* poper_callback);
// address
MM_EXPORT_REDIS void mmRedisPoper_SetRemote(struct mmRedisPoper* p, const char* node, mmUShort_t port);
// restart if assign this.
MM_EXPORT_REDIS void mmRedisPoper_SetIOTimeout(struct mmRedisPoper* p, mmMSec_t milliseconds);
// restart if assign this.
MM_EXPORT_REDIS void mmRedisPoper_SetBlockTimeout(struct mmRedisPoper* p, mmUInt32_t seconds);
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisPoper_SetAuth(struct mmRedisPoper* p, const char* auth);
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisPoper_SetApiRestrict(struct mmRedisPoper* p, mmSInt8_t api_restrict);
// assign context handle.
MM_EXPORT_REDIS void mmRedisPoper_SetContext(struct mmRedisPoper* p, void* u);

// pop.
MM_EXPORT_REDIS int mmRedisPoper_Pop(struct mmRedisPoper* p);

// start thread.
MM_EXPORT_REDIS void mmRedisPoper_Start(struct mmRedisPoper* p);
// interrupt thread.
MM_EXPORT_REDIS void mmRedisPoper_Interrupt(struct mmRedisPoper* p);
// shutdown thread.
MM_EXPORT_REDIS void mmRedisPoper_Shutdown(struct mmRedisPoper* p);
// join thread.
MM_EXPORT_REDIS void mmRedisPoper_Join(struct mmRedisPoper* p);

#include "core/mmSuffix.h"

#endif//__mmRedisPoper_h__
