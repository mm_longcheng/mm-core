/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisList.h"
#include "mmRedisSync.h"
#include "mmRedisReply.h"

#include "core/mmLogger.h"
#include "core/mmString.h"

#include "hiredis.h"

MM_EXPORT_REDIS long long mmRedisList_Push(struct mmRedisSync* ctx, const char* cmd, const char* key, void* ptr, size_t size)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "%s %s %b", cmd, key, ptr, size);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIuPTR "]. reply_is_null error:(%d)%s.",
                              cmd, key, size, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                mmLogger_LogE(gLogger, "command[%s %s %" PRIuPTR "]. reply_type_error type:%d error:(%d)%s.",
                              cmd, key, size, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisList_Pop(struct mmRedisSync* ctx, const char* cmd, const char* key, struct mmString* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    mmString_Clear(val);
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "%s %s", cmd, key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[%s %s]. reply_is_null error:(%d)%s.",
                              cmd, key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s]. reply_have_error error:(%d)%s.",
                              cmd, key, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            if (REDIS_REPLY_STRING != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s]. reply_type_error type:%d error:(%d)%s.",
                              cmd, key, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // when get a nil we assign default.
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            //
            mmString_Assignsn(val, r->str, r->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS int mmRedisList_Bpop(struct mmRedisSync* ctx, const char* cmd, const char* key, mmUInt32_t second, struct mmString* val)
{
    int rt = MM_REPLYCODE_UNKNOWN;
    mmString_Clear(val);
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            redisReply* pop_elem = NULL;
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "%s %s %u", cmd, key, second);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[%s %s %u]. reply_is_null error:(%d)%s.",
                              cmd, key, second, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_IS_NULL;
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (REDIS_REPLY_ERROR == r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %u]. reply_have_error error:(%d)%s.",
                              cmd, key, second, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_HAVE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_HAVE_ERROR;
                break;
            }
            // bpop return a array the one is queue name second is elem who be pop.
            // here we ignore the queue name.
            if (REDIS_REPLY_ARRAY != r->type && REDIS_REPLY_NIL != r->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %u]. reply_type_error type:%d error:(%d)%s.",
                              cmd, key, second, r->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // when get a nil we assign default.
            if (REDIS_REPLY_NIL == r->type)
            {
                // when get nil reply value,we not need logger it out.
                rt = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                ctx->error_code = MM_REPLYCODE_REPLY_VALUE_IS_NIL;
                break;
            }
            if (2 != r->elements)
            {
                mmLogger_LogE(gLogger, "command[%s %s %u]. reply_type_error type:%d elements:%u error:(%d)%s.",
                              cmd, key, second, r->type, (unsigned int)r->elements, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            // here we only get the elem who be pop.
            pop_elem = r->element[1];
            // pop_elem is not null generally.
            assert(pop_elem&&"pop_elem is a null.");
            // 
            if (REDIS_REPLY_STRING != pop_elem->type)
            {
                mmLogger_LogE(gLogger, "command[%s %s %u]. reply_type_error pop_elem->type:%d error:(%d)%s.",
                              cmd, key, second, pop_elem->type, c->err, c->errstr);
                rt = MM_REPLYCODE_REPLY_TYPE_ERROR;
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            //
            mmString_Assignsn(val, pop_elem->str, pop_elem->len);
            rt = MM_REPLYCODE_SUCCESS;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisList_Lpush(struct mmRedisSync* ctx, const char* key, void* ptr, size_t size)
{
    return mmRedisList_Push(ctx, "lpush", key, ptr, size);
}
MM_EXPORT_REDIS long long mmRedisList_Rpush(struct mmRedisSync* ctx, const char* key, void* ptr, size_t size)
{
    return mmRedisList_Push(ctx, "rpush", key, ptr, size);
}

MM_EXPORT_REDIS int mmRedisList_Lpop(struct mmRedisSync* ctx, const char* key, struct mmString* val)
{
    return mmRedisList_Pop(ctx, "lpop", key, val);
}
MM_EXPORT_REDIS int mmRedisList_Rpop(struct mmRedisSync* ctx, const char* key, struct mmString* val)
{
    return mmRedisList_Pop(ctx, "rpop", key, val);
}

MM_EXPORT_REDIS int mmRedisList_Blpop(struct mmRedisSync* ctx, const char* key, mmUInt32_t second, struct mmString* val)
{
    return mmRedisList_Bpop(ctx, "blpop", key, second, val);
}
MM_EXPORT_REDIS int mmRedisList_Brpop(struct mmRedisSync* ctx, const char* key, mmUInt32_t second, struct mmString* val)
{
    return mmRedisList_Bpop(ctx, "brpop", key, second, val);
}

MM_EXPORT_REDIS long long mmRedisList_Llen(struct mmRedisSync* ctx, const char* key)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "llen %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[llen %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if (r->type != REDIS_REPLY_INTEGER)
            {
                mmLogger_LogE(gLogger, "command[llen %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
