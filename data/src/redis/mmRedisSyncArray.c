/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisSyncArray.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmThread.h"
#include "core/mmParameters.h"

#include "container/mmListVpt.h"

#include "net/mmSockaddr.h"

MM_EXPORT_REDIS void mmRedisSyncArray_Init(struct mmRedisSyncArray* p)
{
    mmString_Init(&p->auth);
    mmString_Init(&p->node);
    MM_LIST_INIT_HEAD(&p->list);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->list_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->port = 6379;
    p->api_restrict = MM_REDISSYNC_API_COMPLETE;
    p->state = MM_TS_CLOSED;
    pthread_key_create(&p->thread_key, NULL);
}
MM_EXPORT_REDIS void mmRedisSyncArray_Destroy(struct mmRedisSyncArray* p)
{
    mmRedisSyncArray_ShutdownSocket(p);
    mmRedisSyncArray_Clear(p);
    //
    mmString_Destroy(&p->auth);
    mmString_Destroy(&p->node);
    MM_LIST_INIT_HEAD(&p->list);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->list_locker);
    mmSpinlock_Destroy(&p->locker);
    pthread_key_delete(p->thread_key);
    p->port = 6379;
    p->api_restrict = MM_REDISSYNC_API_COMPLETE;
    p->state = MM_TS_CLOSED;
}

MM_EXPORT_REDIS void mmRedisSyncArray_Lock(struct mmRedisSyncArray* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_REDIS void mmRedisSyncArray_Unlock(struct mmRedisSyncArray* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_REDIS void mmRedisSyncArray_SetRemote(struct mmRedisSyncArray* p, const char* node, mmUShort_t port)
{
    struct mmListHead* pos = NULL;

    mmString_Assigns(&p->node, node);
    p->port = port;

    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmRedisSync* conn = NULL;
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(e->v);
        mmRedisSync_Lock(conn);
        mmRedisSync_SetRemote(conn, node, port);
        mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_REDIS void mmRedisSyncArray_SetParameters(struct mmRedisSyncArray* p, const char* parameters)
{
    char node[MM_NODE_NAME_LENGTH] = { 0 };
    mmUShort_t port = 0;
    mmParameters_Client(parameters, node, &port);
    // 
    mmRedisSyncArray_SetRemote(p, node, port);
}
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisSyncArray_SetAuth(struct mmRedisSyncArray* p, const char* auth)
{
    struct mmListHead* pos = NULL;

    mmString_Assigns(&p->auth, auth);

    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmRedisSync* conn = NULL;
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(e->v);
        mmRedisSync_Lock(conn);
        mmRedisSync_SetAuth(conn, auth);
        mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisSyncArray_SetApiRestrict(struct mmRedisSyncArray* p, mmSInt8_t api_restrict)
{
    struct mmListHead* pos = NULL;

    p->api_restrict = api_restrict;

    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmRedisSync* conn = NULL;
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(e->v);
        mmRedisSync_Lock(conn);
        mmRedisSync_SetApiRestrict(conn, p->api_restrict);
        mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// feedback the conn is broken.
MM_EXPORT_REDIS void mmRedisSyncArray_ApplyBroken(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmRedisSync* conn = NULL;
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(e->v);
        mmRedisSync_Lock(conn);
        mmRedisSync_ApplyBroken(conn);
        mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_REDIS struct mmRedisSync* mmRedisSyncArray_ThreadInstance(struct mmRedisSyncArray* p)
{
    struct mmRedisSync* conn = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (NULL == lvp)
    {
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        conn = (struct mmRedisSync*)mmMalloc(sizeof(struct mmRedisSync));
        mmRedisSync_Init(conn);
        mmRedisSync_SetRemote(conn, mmString_CStr(&p->node), p->port);
        mmRedisSync_SetAuth(conn, mmString_CStr(&p->auth));
        mmRedisSync_SetApiRestrict(conn, p->api_restrict);
        lvp->v = conn;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        conn = (struct mmRedisSync*)(lvp->v);
    }
    return conn;
}
MM_EXPORT_REDIS void mmRedisSyncArray_Clear(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmRedisSync* conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(lvp->v);
        mmRedisSync_Lock(conn);
        mmList_Del(curr);
        mmRedisSync_DetachSocket(conn);
        mmRedisSync_Unlock(conn);
        mmRedisSync_Destroy(conn);
        mmFree(conn);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// shutdown socket only,not destroy redis.
MM_EXPORT_REDIS void mmRedisSyncArray_ShutdownSocket(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmRedisSync* conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(lvp->v);
        // shutdown socket is thread safe.lock free here.
        // if lock here, will dead lock when connect at logic level.
        // mmRedisSync_Lock(conn);
        mmRedisSync_ShutdownSocket(conn);
        // mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_REDIS void mmRedisSyncArray_Start(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmRedisSync* conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(lvp->v);
        mmRedisSync_Lock(conn);
        mmRedisSync_Start(conn);
        mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_REDIS void mmRedisSyncArray_Interrupt(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmRedisSync* conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_CLOSED;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(lvp->v);
        // interrupt is thread safe.lock free here.
        // if lock here, will dead lock when connect at logic level.
        // mmRedisSync_Lock(conn);
        mmRedisSync_Interrupt(conn);
        // mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// shutdown, this will stop the connect and reconnect.
MM_EXPORT_REDIS void mmRedisSyncArray_Shutdown(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmRedisSync* conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_FINISH;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(lvp->v);
        // shutdown is thread safe.lock free here.
        // if lock here, will dead lock when connect at logic level.
        // mmRedisSync_Lock(conn);
        mmRedisSync_Shutdown(conn);
        // mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_REDIS void mmRedisSyncArray_Join(struct mmRedisSyncArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmRedisSync* conn = NULL;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_FINISH;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmRedisSync*)(lvp->v);
        mmRedisSync_Lock(conn);
        mmRedisSync_Join(conn);
        mmRedisSync_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
