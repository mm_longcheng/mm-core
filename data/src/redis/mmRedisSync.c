/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisSync.h"
#include "mmRedisCommon.h"
#include "mmRedisReply.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmOSSocket.h"
#include "core/mmThread.h"
#include "core/mmTimeCache.h"

#include <time.h>
#include <stdio.h>
#include "hiredis.h"

MM_EXPORT_REDIS void mmRedisSync_Init(struct mmRedisSync* p)
{
    mmString_Init(&p->auth);
    mmString_Init(&p->node);
    mmString_Assigns(&p->node, "127.0.0.1");
    mmSpinlock_Init(&p->locker, NULL);
    p->port = 6379;
    p->conn_timeout = MM_REDIS_SYNC_CONN_TIMEOUT;
    p->io_timeout = MM_REDIS_SYNC_IO_TIMEOUT;
    p->trytimes = MM_REDIS_SYNC_TRYTIMES;
    p->config_timeout = 0;
    p->timecode_check_select = 0;
    p->conn_state = MM_REDISSYNC_STATE_BROKEN;
    FD_ZERO(&p->sr);
    FD_ZERO(&p->sw);
    p->c = NULL;
    p->error_code = MM_REPLYCODE_SUCCESS;
    p->api_restrict = MM_REDISSYNC_API_COMPLETE;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_REDIS void mmRedisSync_Destroy(struct mmRedisSync* p)
{
    mmRedisSync_DetachSocket(p);
    //
    mmString_Destroy(&p->auth);
    mmString_Destroy(&p->node);
    mmSpinlock_Destroy(&p->locker);
    p->port = 0;
    p->conn_timeout = MM_REDIS_SYNC_CONN_TIMEOUT;
    p->io_timeout = MM_REDIS_SYNC_IO_TIMEOUT;
    p->trytimes = MM_REDIS_SYNC_TRYTIMES;
    p->config_timeout = 0;
    p->timecode_check_select = 0;
    p->conn_state = MM_REDISSYNC_STATE_BROKEN;
    FD_ZERO(&p->sr);
    FD_ZERO(&p->sw);
    p->c = NULL;
    p->error_code = MM_REPLYCODE_SUCCESS;
    p->api_restrict = MM_REDISSYNC_API_COMPLETE;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_REDIS void mmRedisSync_Lock(struct mmRedisSync* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_REDIS void mmRedisSync_Unlock(struct mmRedisSync* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_REDIS void mmRedisSync_SetRemote(struct mmRedisSync* p, const char* node, mmUShort_t port)
{
    mmString_Assigns(&p->node, node);
    p->port = port;
}
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisSync_SetAuth(struct mmRedisSync* p, const char* auth)
{
    mmString_Assigns(&p->auth, auth);
}
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisSync_SetApiRestrict(struct mmRedisSync* p, mmSInt8_t api_restrict)
{
    p->api_restrict = api_restrict;
}
MM_EXPORT_REDIS int mmRedisSync_Connect(struct mmRedisSync* p)
{
    int rt = -1;
    struct mmLogger* gLogger = mmLogger_Instance();
    do
    {
        struct timeval tv;
        tv.tv_sec = (mmTimevalSSec_t)(p->conn_timeout / 1000);
        tv.tv_usec = (mmTimevalUSec_t)((p->conn_timeout % 1000) * 1000);

        mmRedisSync_DetachSocket(p);

        FD_ZERO(&p->sr);

        p->c = redisConnectWithTimeout(mmString_CStr(&p->node), p->port, tv);
        if (NULL == p->c)
        {
            mmLogger_LogI(gLogger, "%s %d connect %s:%d failure.can't allocate redis context.",
                          __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port);
            rt = -1;
            break;
        }
        if (REDIS_OK != p->c->err)
        {
            mmLogger_LogI(gLogger, "%s %d connect %s:%d failure.error:(%d)%s.",
                          __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, p->c->err, p->c->errstr);
            redisFree(p->c);
            p->c = NULL;
            rt = 1;
            break;
        }
        mmRedisSync_ApplyFinish(p);
        //
        rt = 0;
    } while (0);
    return rt;
}
// -1 alloc error
//  0 success.
//  1 net error.
MM_EXPORT_REDIS int mmRedisSync_Reconnect(struct mmRedisSync* p)
{
    int rt = -1;
    struct mmLogger* gLogger = mmLogger_Instance();
    do
    {
        int state = REDIS_ERR;

        FD_ZERO(&p->sr);

        // need check the context.
        if (NULL == p->c)
        {
            mmLogger_LogW(gLogger, "%s %d connect %s:%d failure.the redis context is a null.",
                          __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port);
            rt = 1;
            break;
        }
        state = redisReconnect(p->c);
        if (REDIS_OK != state)
        {
            mmLogger_LogI(gLogger, "%s %d connect %s:%d failure.error:(%d)%s.",
                          __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, p->c->err, p->c->errstr);
            redisFree(p->c);
            p->c = NULL;
            rt = 1;
            break;
        }
        mmRedisSync_ApplyFinish(p);
        //
        rt = 0;
    } while (0);
    return rt;
}
MM_EXPORT_REDIS int mmRedisSync_Check(struct mmRedisSync* p)
{
    int rt = 0;
    struct mmLogger* gLogger = mmLogger_Instance();

    if (0 != p->config_timeout && NULL != p->c && MM_REDISSYNC_STATE_FINISH == p->conn_state)
    {
        // redis timeout check.
        // if config timeout not assign to 0, we need check redis timeout disconnect. 

        // mmUInt64_t msec_current = 0;
        // struct timeval tv;
        // mm_gettimeofday(&tv, NULL);
        // msec_current = ( tv.tv_sec * 1000 ) + ( tv.tv_usec / 1000 );

        // we use the mmTimeCache for quick check timeout.
        struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
        mmUInt64_t msec_current = gTimeCache->msec_current;
        mmUInt64_t timeout_check_select = p->config_timeout * 1000;

        if (msec_current - p->timecode_check_select >= timeout_check_select)
        {
            int fd_state = -1;

            int ready = 0;

            fd_set work_r_fd_set;
            fd_set work_w_fd_set;

            struct timeval  tv;

            tv.tv_sec = 0;
            tv.tv_usec = 0;

            work_r_fd_set = p->sr;
            work_w_fd_set = p->sw;

            ready = select((int)(p->c->fd + 1), &work_r_fd_set, &work_w_fd_set, NULL, &tv);

            if (ready == 0)
            {
                fd_state = 0;
            }
            else if (-1 == ready)
            {
                // not socket .
                mmErr_t _errcode = mmErrno;
                if (_errcode == MM_ECONNABORTED || _errcode == MM_EAGAIN)
                {
                    // An operation on a socket could not be performed because the system 
                    // lacked sufficient buffer space or because a queue was full.
                    fd_state = 0;
                }
                else
                {
                    // error handler.
                    fd_state = -1;
                    mmLogger_LogI(gLogger, "%s %d redis %s:%d timeout:%" PRIu64 " ms need reconnect.",
                                  __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, timeout_check_select);
                }
            }
            else
            {
                fd_state = -1;
                mmLogger_LogI(gLogger, "%s %d redis %s:%d timeout:%" PRIu64 " ms need reconnect.",
                              __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, timeout_check_select);
            }

            p->conn_state = 0 == fd_state ? MM_REDISSYNC_STATE_FINISH : MM_REDISSYNC_STATE_BROKEN;
            
            p->timecode_check_select = msec_current;
        }
    }
    if (MM_REDISSYNC_STATE_BROKEN == p->conn_state)
    {
        if (NULL == p->c)
        {
            mmUInt32_t _try_times = 0;
            rt = -1;
            do
            {
                if (MM_TS_FINISH == p->state)
                {
                    // if thread state is MM_TS_FINISH break immediately.
                    break;
                }
                _try_times++;
                rt = mmRedisSync_Connect(p);
                if (MM_TS_FINISH == p->state)
                {
                    // if thread state is MM_TS_FINISH break immediately.
                    break;
                }
                if (0 != rt)
                {
                    mmLogger_LogI(gLogger, "%s %d connect %s:%d failure.try times:%d",
                                  __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, _try_times);
                    mmMSleep(MM_REDIS_SYNC_TRY_MSLEEP_TIME);
                }
            } while (0 != rt && _try_times < p->trytimes);
        }
        else
        {
            mmUInt32_t _try_times = 0;
            rt = -1;
            do
            {
                if (MM_TS_FINISH == p->state)
                {
                    // if thread state is MM_TS_FINISH break immediately.
                    break;
                }
                _try_times++;
                rt = mmRedisSync_Reconnect(p);
                if (MM_TS_FINISH == p->state)
                {
                    // if thread state is MM_TS_FINISH break immediately.
                    break;
                }
                if (0 != rt)
                {
                    mmLogger_LogI(gLogger, "%s %d connect %s:%d failure.try times:%d",
                                  __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, _try_times);
                    mmMSleep(MM_REDIS_SYNC_TRY_MSLEEP_TIME);
                }
            } while (0 != rt && _try_times < p->trytimes);
        }
    }
    return p->conn_state;
}
// feedback the conn is broken.
MM_EXPORT_REDIS void mmRedisSync_ApplyBroken(struct mmRedisSync* p)
{
    p->conn_state = MM_REDISSYNC_STATE_BROKEN;
}
// feedback the conn is finish.
MM_EXPORT_REDIS void mmRedisSync_ApplyFinish(struct mmRedisSync* p)
{
    struct timeval tv;
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();

    p->conn_state = MM_REDISSYNC_STATE_FINISH;
    FD_SET(p->c->fd, &p->sr);
    // io timeout.
    if (-1 != p->io_timeout)
    {
        tv.tv_sec = (mmTimevalSSec_t)(p->io_timeout / 1000);
        tv.tv_usec = (mmTimevalUSec_t)((p->io_timeout % 1000) * 1000);
        redisSetTimeout(p->c, tv);
    }
    // auth password.
    // the api auth aways available.
    if (0 != mmString_Size(&p->auth))
    {
        mmRedis_Auth(p, mmString_CStr(&p->auth));
    }
    if (MM_REDISSYNC_STATE_FINISH == p->conn_state && MM_REDISSYNC_API_COMPLETE == p->api_restrict)
    {
        // get redis config.
        mmRedis_ConfigTimeout(p, &p->config_timeout);
        p->timecode_check_select = gTimeCache->msec_current;
        p->api_restrict = MM_REPLYCODE_SUCCESS == p->error_code ? MM_REDISSYNC_API_COMPLETE : MM_REDISSYNC_API_RESTRICT;
        // reset the connect state to finish, whether the config_timeout command result.
        p->conn_state = MM_REDISSYNC_STATE_FINISH;
    }
}
// get the conn state.
MM_EXPORT_REDIS int mmRedisSync_State(struct mmRedisSync* p)
{
    return p->conn_state;
}
// synchronize attach to socket.not thread safe.
MM_EXPORT_REDIS void mmRedisSync_AttachSocket(struct mmRedisSync* p)
{
    mmRedisSync_Connect(p);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_REDIS void mmRedisSync_DetachSocket(struct mmRedisSync* p)
{
    if (p->c)
    {
        // make sure interrupt immediately,we shutdown the fd's read and write.
        mmShutdownSocket(p->c->fd, MM_BOTH_SHUTDOWN);
        //
        redisFree(p->c);
        p->c = NULL;
    }
    p->conn_state = MM_REDISSYNC_STATE_BROKEN;
}
// shutdown_socket for the conn.application interrupt and shutdown process need call this.
MM_EXPORT_REDIS void mmRedisSync_ShutdownSocket(struct mmRedisSync* p)
{
    if (p->c)
    {
        // make sure interrupt immediately,we shutdown the fd's read and write.
        mmShutdownSocket(p->c->fd, MM_BOTH_SHUTDOWN);
    }
}
// timeout for redis recv send;
MM_EXPORT_REDIS void mmRedisSync_SetIOTimeout(struct mmRedisSync* p, mmMSec_t milliseconds)
{
    p->io_timeout = milliseconds;
}

MM_EXPORT_REDIS void* mmRedisSync_Command(struct mmRedisSync* p, const char *format, ...)
{
    void* reply = NULL;
    va_list args;
    va_start(args, format);
    reply = redisvCommand(p->c, format, args);
    va_end(args);
    return reply;
}
MM_EXPORT_REDIS void mmRedisSync_FreeReply(struct mmRedisSync* p, void* reply)
{
    redisReply* r = (redisReply*)(reply);
    int reply_type = 0;
    if (NULL != r)
    {
        reply_type = r->type;
        freeReplyObject(r);
    }
    if (NULL != p->c && (REDIS_ERR_IO == p->c->err || REDIS_ERR_EOF == p->c->err || REDIS_REPLY_ERROR == reply_type))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, " %s:%d net failure.", mmString_CStr(&p->node), p->port);
        mmRedisSync_ApplyBroken(p);
    }
}
MM_EXPORT_REDIS void mmRedisSync_Start(struct mmRedisSync* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
}
MM_EXPORT_REDIS void mmRedisSync_Interrupt(struct mmRedisSync* p)
{
    p->state = MM_TS_CLOSED;
    mmRedisSync_ShutdownSocket(p);
}
// shutdown, this will stop the connect and reconnect.
MM_EXPORT_REDIS void mmRedisSync_Shutdown(struct mmRedisSync* p)
{
    p->state = MM_TS_FINISH;
    mmRedisSync_ShutdownSocket(p);
}
MM_EXPORT_REDIS void mmRedisSync_Join(struct mmRedisSync* p)
{

}

