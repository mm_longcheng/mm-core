/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisReply_h__
#define __mmRedisReply_h__

#include "core/mmCore.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"


// hiredis c api some version can not compile at cxx.
// if cxx not compile error,can use hiredis only.
//
// 1.check conn and reconnect if not ready.
// 2.command
// 3.check reply whether NULL.
// 4.check result type
// 5.get reply data. 
// 6.free reply
// 7.check conn state,set to broken if io error occur..

// redis reply type define.it is same from hiredis.
#define MM_REDIS_REPLY_STRING  1
#define MM_REDIS_REPLY_ARRAY   2
#define MM_REDIS_REPLY_INTEGER 3
#define MM_REDIS_REPLY_NIL     4
#define MM_REDIS_REPLY_STATUS  5
#define MM_REDIS_REPLY_ERROR   6

// REDIS_REPLY_STATUS define.
#define MM_REDIS_REPLY_STATUS_ERR -1
#define MM_REDIS_REPLY_STATUS_OK   0

enum mmReplyCode_t
{
    MM_REPLYCODE_UNKNOWN            = -1, // unknown.
    MM_REPLYCODE_SUCCESS            =  0, // success.
    MM_REPLYCODE_REPLY_IS_NULL      =  1, // reply is null.
    MM_REPLYCODE_REPLY_HAVE_ERROR   =  2, // reply have error.
    MM_REPLYCODE_REPLY_TYPE_ERROR   =  3, // reply type error.
    MM_REPLYCODE_REPLY_VALUE_IS_NIL =  4, // reply value is nil.
};

// check reply type error.will logger if error occur.
// return reply_code
MM_EXPORT_REDIS int mmRedisReply_CheckTypeError(void* reply, int type);
// check reply error.will logger if error occur.
// return reply_code
MM_EXPORT_REDIS int mmRedisReply_CheckError(void* reply);
// return reply type.
MM_EXPORT_REDIS int mmRedisReply_Type(void* reply);

// return reply elem size.
MM_EXPORT_REDIS size_t mmRedisReply_ElemSize(void* reply);
// return reply elem at index.
MM_EXPORT_REDIS void* mmRedisReply_Elem(void* reply, size_t index);

MM_EXPORT_REDIS char* mmRedisReply_Str(void* reply);
MM_EXPORT_REDIS size_t mmRedisReply_Len(void* reply);
// reply value cast as buffer.it is weak ref.not check the reply type.
MM_EXPORT_REDIS void mmRedisReply_CastAsBuffer(void* reply, void** buff, size_t* size);
// reply value cast as integer.not the reply type.
MM_EXPORT_REDIS long long mmRedisReply_CastAsInteger(void* reply);
// reply value cast as state.not the reply type.
MM_EXPORT_REDIS int mmRedisReply_CastAsState(void* reply);
// reply value cast as string,return "" if reply nil.not check the reply type.
MM_EXPORT_REDIS char* mmRedisReply_CastAsString(void* reply);

#include "core/mmSuffix.h"

#endif//__mmRedisReply_h__
