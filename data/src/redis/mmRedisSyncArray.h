/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisSyncArray_h__
#define __mmRedisSyncArray_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"
#include "core/mmString.h"
#include "core/mmList.h"

#include <pthread.h>

#include "redis/mmRedisSync.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

struct mmRedisSyncArray
{
    // ip default is ""
    struct mmString auth;
    // ip default is "127.0.0.1"
    struct mmString node;
    // silk strong ref list.
    struct mmListHead list;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t list_locker;
    mmAtomic_t locker;
    // port default is 6379.
    mmUInt16_t port;
    // redis server is a restricted api, can not use such as "config" "keys" command. 
    // default is MM_REDISSYNC_API_COMPLETE.
    mmSInt8_t api_restrict;
    // mm_thread_state_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // thread key.
    pthread_key_t thread_key;
};

MM_EXPORT_REDIS void mmRedisSyncArray_Init(struct mmRedisSyncArray* p);
MM_EXPORT_REDIS void mmRedisSyncArray_Destroy(struct mmRedisSyncArray* p);

MM_EXPORT_REDIS void mmRedisSyncArray_Lock(struct mmRedisSyncArray* p);
MM_EXPORT_REDIS void mmRedisSyncArray_Unlock(struct mmRedisSyncArray* p);

MM_EXPORT_REDIS void mmRedisSyncArray_SetRemote(struct mmRedisSyncArray* p, const char* node, mmUShort_t port);
// 192.168.111.123-65535
MM_EXPORT_REDIS void mmRedisSyncArray_SetParameters(struct mmRedisSyncArray* p, const char* parameters);
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisSyncArray_SetAuth(struct mmRedisSyncArray* p, const char* auth);
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisSyncArray_SetApiRestrict(struct mmRedisSyncArray* p, mmSInt8_t api_restrict);
// feedback the conn is broken.
MM_EXPORT_REDIS void mmRedisSyncArray_ApplyBroken(struct mmRedisSyncArray* p);

// get current thread conn.and connect immediately,if work add to protocol.
MM_EXPORT_REDIS struct mmRedisSync* mmRedisSyncArray_ThreadInstance(struct mmRedisSyncArray* p);

MM_EXPORT_REDIS void mmRedisSyncArray_Clear(struct mmRedisSyncArray* p);
// shutdown socket only,not destroy redis.
MM_EXPORT_REDIS void mmRedisSyncArray_ShutdownSocket(struct mmRedisSyncArray* p);

MM_EXPORT_REDIS void mmRedisSyncArray_Start(struct mmRedisSyncArray* p);
MM_EXPORT_REDIS void mmRedisSyncArray_Interrupt(struct mmRedisSyncArray* p);
MM_EXPORT_REDIS void mmRedisSyncArray_Shutdown(struct mmRedisSyncArray* p);
MM_EXPORT_REDIS void mmRedisSyncArray_Join(struct mmRedisSyncArray* p);

#include "core/mmSuffix.h"

#endif//__mmRedisSyncArray_h__
