/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisPoperArray_h__
#define __mmRedisPoperArray_h__

#include "core/mmCore.h"
#include "core/mmThread.h"

#include "redis/mmRedisPoper.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

struct mmRedisPoperArray
{
    struct mmRedisPoperCallback callback;
    // default is rpop
    struct mmString cmd;
    // default is queue
    struct mmString key;
    // ip default is ""
    struct mmString auth;
    // ip default is "127.0.0.1"
    struct mmString node;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t arrays_locker;
    mmAtomic_t locker;
    // length. default is 0.
    mmUInt32_t length;
    // array pointer.
    struct mmRedisPoper** arrays;
    // port default is 6379.
    mmUInt16_t port;
    // default is MM_REDIS_POPER_IO_TIMEOUT
    mmMSec_t io_timeout;
    // second.default is 0 (never).
    mmUInt32_t block_timeout;
    // MM_REDIS_POPER_EMPTY_KEY_SLEEP_TIME
    mmMSec_t sleep_empty_key_timeout;
    // MM_REDIS_POPER_EMPTY_VAL_SLEEP_TIME
    mmMSec_t sleep_empty_val_timeout;
    // MM_REDIS_POPER_ERROR_CTX_SLEEP_TIME
    mmMSec_t sleep_error_ctx_timeout;
    // redis server is a restricted api, can not use such as "config" "keys" command.default is MM_REDISSYNC_API_COMPLETE.
    mmSInt8_t api_restrict;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_REDIS void mmRedisPoperArray_Init(struct mmRedisPoperArray* p);
MM_EXPORT_REDIS void mmRedisPoperArray_Destroy(struct mmRedisPoperArray* p);

MM_EXPORT_REDIS void mmRedisPoperArray_Lock(struct mmRedisPoperArray* p);
MM_EXPORT_REDIS void mmRedisPoperArray_Unlock(struct mmRedisPoperArray* p);

// must join before set length.
MM_EXPORT_REDIS void mmRedisPoperArray_SetLength(struct mmRedisPoperArray* p, mmUInt32_t length);
MM_EXPORT_REDIS mmUInt32_t mmRedisPoperArray_GetLength(struct mmRedisPoperArray* p);
// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
MM_EXPORT_REDIS void mmRedisPoperArray_SetParameters(struct mmRedisPoperArray* p, const char* parameters);

// set callback.you can not do this at thread runing.
MM_EXPORT_REDIS void mmRedisPoperArray_SetCallback(struct mmRedisPoperArray* p, struct mmRedisPoperCallback* poper_callback);
// address
MM_EXPORT_REDIS void mmRedisPoperArray_SetRemote(struct mmRedisPoperArray* p, const char* node, mmUShort_t port);
// restart if assign this.
MM_EXPORT_REDIS void mmRedisPoperArray_SetBlockTimeout(struct mmRedisPoperArray* p, mmUInt32_t seconds);
MM_EXPORT_REDIS void mmRedisPoperArray_SetIOTimeout(struct mmRedisPoperArray* p, mmMSec_t milliseconds);
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisPoperArray_SetAuth(struct mmRedisPoperArray* p, const char* auth);
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisPoperArray_SetApiRestrict(struct mmRedisPoperArray* p, mmSInt8_t api_restrict);
// assign context handle.
MM_EXPORT_REDIS void mmRedisPoperArray_SetContext(struct mmRedisPoperArray* p, void* u);
// lpop rpop blpop brpop
MM_EXPORT_REDIS void mmRedisPoperArray_SetCmd(struct mmRedisPoperArray* p, const char* cmd);
MM_EXPORT_REDIS void mmRedisPoperArray_SetKey(struct mmRedisPoperArray* p, const char* key);
MM_EXPORT_REDIS void mmRedisPoperArray_SetSleepTimeout(struct mmRedisPoperArray* p, mmMSec_t empty_key_timeout, mmMSec_t empty_val_timeout, mmMSec_t error_ctx_timeout);

// start thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Start(struct mmRedisPoperArray* p);
// interrupt thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Interrupt(struct mmRedisPoperArray* p);
// shutdown thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Shutdown(struct mmRedisPoperArray* p);
// join thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Join(struct mmRedisPoperArray* p);

MM_EXPORT_REDIS void mmRedisPoperArray_Clear(struct mmRedisPoperArray* p);

#include "core/mmSuffix.h"

#endif//__mmRedisPoperArray_h__
