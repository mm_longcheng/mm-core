/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisPoperArray.h"

#include "core/mmAlloc.h"
#include "core/mmParameters.h"

#include "net/mmSockaddr.h"

MM_EXPORT_REDIS void mmRedisPoperArray_Init(struct mmRedisPoperArray* p)
{
    mmRedisPoperCallback_Init(&p->callback);

    mmString_Init(&p->cmd);
    mmString_Init(&p->key);
    mmString_Init(&p->auth);
    mmString_Init(&p->node);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->arrays_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->length = 0;
    p->arrays = NULL;
    p->port = 6379;
    p->io_timeout = MM_REDIS_POPER_IO_TIMEOUT;
    p->block_timeout = 0;// second.default is 0 (never).
    p->api_restrict = MM_REDISSYNC_API_COMPLETE;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    mmString_Assigns(&p->cmd, "brpop");
    mmString_Assigns(&p->key, "queue");
    mmString_Assigns(&p->node, "127.0.0.1");
}
MM_EXPORT_REDIS void mmRedisPoperArray_Destroy(struct mmRedisPoperArray* p)
{
    mmRedisPoperArray_Clear(p);
    //
    mmRedisPoperCallback_Destroy(&p->callback);

    mmString_Destroy(&p->cmd);
    mmString_Destroy(&p->key);
    mmString_Destroy(&p->auth);
    mmString_Destroy(&p->node);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->arrays_locker);
    mmSpinlock_Destroy(&p->locker);
    p->length = 0;
    p->arrays = NULL;
    p->port = 0;
    p->io_timeout = MM_REDIS_POPER_IO_TIMEOUT;
    p->block_timeout = 0;
    p->api_restrict = MM_REDISSYNC_API_COMPLETE;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

MM_EXPORT_REDIS void mmRedisPoperArray_Lock(struct mmRedisPoperArray* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_REDIS void mmRedisPoperArray_Unlock(struct mmRedisPoperArray* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_REDIS void mmRedisPoperArray_SetLength(struct mmRedisPoperArray* p, mmUInt32_t length)
{
    mmSpinlock_Lock(&p->arrays_locker);
    if (length < p->length)
    {
        mmUInt32_t i = 0;
        struct mmRedisPoper* e = NULL;
        for (i = length; i < p->length; ++i)
        {
            e = p->arrays[i];
            mmRedisPoper_Lock(e);
            p->arrays[i] = NULL;
            mmRedisPoper_Unlock(e);
            mmRedisPoper_Destroy(e);
            mmFree(e);
        }
        p->arrays = (struct mmRedisPoper**)mmRealloc(p->arrays, sizeof(struct mmRedisPoper*) * length);
        p->length = length;
    }
    else if (length > p->length)
    {
        mmUInt32_t i = 0;
        p->arrays = (struct mmRedisPoper**)mmRealloc(p->arrays, sizeof(struct mmRedisPoper*) * length);
        for (i = p->length; i < length; ++i)
        {
            p->arrays[i] = (struct mmRedisPoper*)mmMalloc(sizeof(struct mmRedisPoper));
            mmRedisPoper_Init(p->arrays[i]);
            mmRedisPoper_SetCallback(p->arrays[i], &p->callback);
            mmRedisPoper_SetRemote(p->arrays[i], mmString_CStr(&p->node), p->port);
            mmRedisPoper_SetBlockTimeout(p->arrays[i], p->block_timeout);
            mmRedisPoper_SetIOTimeout(p->arrays[i], p->io_timeout);
            mmRedisPoper_SetAuth(p->arrays[i], mmString_CStr(&p->auth));
            mmRedisPoper_SetApiRestrict(p->arrays[i], p->api_restrict);
            mmRedisPoper_SetContext(p->arrays[i], p->u);
            mmRedisPoper_SetCmd(p->arrays[i], mmString_CStr(&p->cmd));
            mmRedisPoper_SetKey(p->arrays[i], mmString_CStr(&p->key));
            p->arrays[i]->state = p->state;
        }
        p->length = length;
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_REDIS mmUInt32_t mmRedisPoperArray_GetLength(struct mmRedisPoperArray* p)
{
    return p->length;
}
// 192.168.111.123-65535(2)
MM_EXPORT_REDIS void mmRedisPoperArray_SetParameters(struct mmRedisPoperArray* p, const char* parameters)
{
    char node[MM_NODE_NAME_LENGTH] = { 0 };
    mmUInt32_t length = 0;
    mmUShort_t port = 0;
    mmParameters_Server(parameters, node, &port, &length);
    // 
    mmRedisPoperArray_SetRemote(p, node, port);
    mmRedisPoperArray_SetLength(p, length);
}
// set callback.you can not do this at thread runing.
MM_EXPORT_REDIS void mmRedisPoperArray_SetCallback(struct mmRedisPoperArray* p, struct mmRedisPoperCallback* poper_callback)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->callback = *poper_callback;
    assert(poper_callback&&"you can not assign null call back.");
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetCallback(p->arrays[i], &p->callback);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// address
MM_EXPORT_REDIS void mmRedisPoperArray_SetRemote(struct mmRedisPoperArray* p, const char* node, mmUShort_t port)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->node, node);
    p->port = port;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetRemote(p->arrays[i], mmString_CStr(&p->node), p->port);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// restart if assign this.
MM_EXPORT_REDIS void mmRedisPoperArray_SetBlockTimeout(struct mmRedisPoperArray* p, mmUInt32_t seconds)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->block_timeout = seconds;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetBlockTimeout(p->arrays[i], p->block_timeout);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_REDIS void mmRedisPoperArray_SetIOTimeout(struct mmRedisPoperArray* p, mmMSec_t milliseconds)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->io_timeout = milliseconds;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetIOTimeout(p->arrays[i], p->io_timeout);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisPoperArray_SetAuth(struct mmRedisPoperArray* p, const char* auth)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->auth, auth);
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetAuth(p->arrays[i], mmString_CStr(&p->auth));
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisPoperArray_SetApiRestrict(struct mmRedisPoperArray* p, mmSInt8_t api_restrict)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->api_restrict = api_restrict;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetApiRestrict(p->arrays[i], p->api_restrict);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// assign context handle.
MM_EXPORT_REDIS void mmRedisPoperArray_SetContext(struct mmRedisPoperArray* p, void* u)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->u = u;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetContext(p->arrays[i], p->u);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_REDIS void mmRedisPoperArray_SetCmd(struct mmRedisPoperArray* p, const char* cmd)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->cmd, cmd);
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetCmd(p->arrays[i], cmd);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_REDIS void mmRedisPoperArray_SetKey(struct mmRedisPoperArray* p, const char* key)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->key, key);
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetKey(p->arrays[i], key);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_REDIS void mmRedisPoperArray_SetSleepTimeout(struct mmRedisPoperArray* p, mmMSec_t empty_key_timeout, mmMSec_t empty_val_timeout, mmMSec_t error_ctx_timeout)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->sleep_empty_key_timeout = empty_key_timeout;
    p->sleep_empty_val_timeout = empty_val_timeout;
    p->sleep_error_ctx_timeout = error_ctx_timeout;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_SetSleepTimeout(p->arrays[i], empty_key_timeout, empty_val_timeout, error_ctx_timeout);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// start thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Start(struct mmRedisPoperArray* p)
{
    mmUInt32_t i = 0;
    assert(0 != p->length && "0 != p->length is invalid.");
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_Start(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// interrupt thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Interrupt(struct mmRedisPoperArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_CLOSED;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_Interrupt(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// shutdown thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Shutdown(struct mmRedisPoperArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH;
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_Shutdown(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// join thread.
MM_EXPORT_REDIS void mmRedisPoperArray_Join(struct mmRedisPoperArray* p)
{
    mmUInt32_t i = 0;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_Join(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_REDIS void mmRedisPoperArray_Clear(struct mmRedisPoperArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        mmRedisPoper_Destroy(p->arrays[i]);
        mmFree(p->arrays[i]);
    }
    mmFree(p->arrays);
    p->arrays = NULL;
    p->length = 0;
    mmSpinlock_Unlock(&p->arrays_locker);
}
