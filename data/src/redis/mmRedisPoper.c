/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisPoper.h"
#include "mmRedisList.h"

#include "core/mmLogger.h"
#include "core/mmTimewait.h"
#include "core/mmOSSocket.h"

#include "hiredis.h"

static void* __static_mmRedisPoper_PopThread(void* arg);

static void __static_mmRedisPoper_PoperDefault(struct mmRedisPoper* p, void* u, struct mmString* data)
{

}
MM_EXPORT_REDIS void mmRedisPoperCallback_Init(struct mmRedisPoperCallback* p)
{
    p->Poper = &__static_mmRedisPoper_PoperDefault;
    p->obj = NULL;
}
MM_EXPORT_REDIS void mmRedisPoperCallback_Destroy(struct mmRedisPoperCallback* p)
{
    p->Poper = &__static_mmRedisPoper_PoperDefault;
    p->obj = NULL;
}

MM_EXPORT_REDIS void mmRedisPoper_Init(struct mmRedisPoper* p)
{
    mmRedisSync_Init(&p->ctx);
    mmString_Init(&p->cmd);
    mmString_Init(&p->key);
    mmRedisPoperCallback_Init(&p->callback);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->pop_mode = MM_BLOCKING;
    p->io_timeout = MM_REDIS_POPER_IO_TIMEOUT;
    p->block_timeout = MM_REDIS_POPER_BLOCK_TIMEOUT;
    p->sleep_empty_key_timeout = MM_REDIS_POPER_EMPTY_KEY_SLEEP_TIME;
    p->sleep_empty_val_timeout = MM_REDIS_POPER_EMPTY_VAL_SLEEP_TIME;
    p->sleep_error_ctx_timeout = MM_REDIS_POPER_ERROR_CTX_SLEEP_TIME;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    mmString_Assigns(&p->cmd, "rpop");
    mmString_Assigns(&p->key, "queue");
}
MM_EXPORT_REDIS void mmRedisPoper_Destroy(struct mmRedisPoper* p)
{
    mmRedisSync_Destroy(&p->ctx);
    mmString_Destroy(&p->cmd);
    mmString_Destroy(&p->key);
    mmRedisPoperCallback_Destroy(&p->callback);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->locker);
    p->pop_mode = MM_BLOCKING;
    p->io_timeout = MM_REDIS_POPER_IO_TIMEOUT;
    p->block_timeout = MM_REDIS_POPER_BLOCK_TIMEOUT;
    p->sleep_empty_key_timeout = MM_REDIS_POPER_EMPTY_KEY_SLEEP_TIME;
    p->sleep_empty_val_timeout = MM_REDIS_POPER_EMPTY_VAL_SLEEP_TIME;
    p->sleep_error_ctx_timeout = MM_REDIS_POPER_ERROR_CTX_SLEEP_TIME;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

MM_EXPORT_REDIS void mmRedisPoper_Lock(struct mmRedisPoper* p)
{
    mmRedisSync_Lock(&p->ctx);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_REDIS void mmRedisPoper_Unlock(struct mmRedisPoper* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmRedisSync_Unlock(&p->ctx);
}
MM_EXPORT_REDIS void mmRedisPoper_SetCmd(struct mmRedisPoper* p, const char* cmd)
{
    size_t cmd_length = mmStrlen(cmd);
    mmString_Assigns(&p->cmd, cmd);
    p->pop_mode = (0 < cmd_length && 'b' == cmd[0]) ? MM_BLOCKING : MM_NONBLOCK;
}
MM_EXPORT_REDIS void mmRedisPoper_SetKey(struct mmRedisPoper* p, const char* key)
{
    mmString_Assigns(&p->key, key);
}
MM_EXPORT_REDIS void mmRedisPoper_SetSleepTimeout(struct mmRedisPoper* p, mmMSec_t empty_key_timeout, mmMSec_t empty_val_timeout, mmMSec_t error_ctx_timeout)
{
    p->sleep_empty_key_timeout = empty_key_timeout;
    p->sleep_empty_val_timeout = empty_val_timeout;
    p->sleep_error_ctx_timeout = error_ctx_timeout;
}

MM_EXPORT_REDIS void mmRedisPoper_SetCallback(struct mmRedisPoper* p, struct mmRedisPoperCallback* poper_callback)
{
    assert(poper_callback && "you can not assign null call back.");
    p->callback = *poper_callback;
}
MM_EXPORT_REDIS void mmRedisPoper_SetRemote(struct mmRedisPoper* p, const char* node, mmUShort_t port)
{
    mmRedisSync_SetRemote(&p->ctx, node, port);
}
MM_EXPORT_REDIS void mmRedisPoper_SetIOTimeout(struct mmRedisPoper* p, mmMSec_t milliseconds)
{
    p->io_timeout = milliseconds;
}
MM_EXPORT_REDIS void mmRedisPoper_SetBlockTimeout(struct mmRedisPoper* p, mmUInt32_t seconds)
{
    p->block_timeout = seconds;
}
// if auth is not empty.will auto commond auth password.
MM_EXPORT_REDIS void mmRedisPoper_SetAuth(struct mmRedisPoper* p, const char* auth)
{
    mmRedisSync_SetAuth(&p->ctx, auth);
}
// the redis api is restricted, can not use such as "config" "keys" command.
MM_EXPORT_REDIS void mmRedisPoper_SetApiRestrict(struct mmRedisPoper* p, mmSInt8_t api_restrict)
{
    mmRedisSync_SetApiRestrict(&p->ctx, api_restrict);
}
// assign context handle.
MM_EXPORT_REDIS void mmRedisPoper_SetContext(struct mmRedisPoper* p, void* u)
{
    p->u = u;
}

// pop.
MM_EXPORT_REDIS int mmRedisPoper_Pop(struct mmRedisPoper* p)
{
    struct timeval  ntime;
    struct timespec otime;
    mmMSec_t _nearby_time = 0;
    struct mmString val;
    mmString_Init(&val);
    assert(p->callback.Poper && "p->callback.Poper is a null.");
    while (MM_TS_MOTION == p->state)
    {
        if (0 == mmString_Size(&p->key))
        {
            _nearby_time = p->sleep_empty_key_timeout;
            // timewait nearby.
            mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
            continue;
        }
        if (MM_NONBLOCK == p->pop_mode)
        {
            mmRedisList_Pop(&p->ctx, mmString_CStr(&p->cmd), mmString_CStr(&p->key), &val);
        }
        else
        {
            mmRedisList_Bpop(&p->ctx, mmString_CStr(&p->cmd), mmString_CStr(&p->key), p->block_timeout, &val);
        }
        if (0 < mmString_Size(&val))
        {
            (*(p->callback.Poper))(p, p->u, &val);
            mmString_Clear(&val);
            _nearby_time = 0;
        }
        else
        {
            _nearby_time = p->sleep_empty_val_timeout;
        }
        if (NULL != p->ctx.c && 0 != p->ctx.c->err)
        {
            _nearby_time = p->sleep_error_ctx_timeout;
        }
        // check the runing state.
        _nearby_time = MM_TS_MOTION == p->state ? _nearby_time : 0;
        // timewait nearby.
        mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
    }
    mmString_Destroy(&val);
    return 0;
}

// start thread.
MM_EXPORT_REDIS void mmRedisPoper_Start(struct mmRedisPoper* p)
{
    mmRedisSync_SetIOTimeout(&p->ctx, p->io_timeout);
    //
    mmRedisSync_Start(&p->ctx);
    //
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->thread_poper, NULL, &__static_mmRedisPoper_PopThread, p);
}
// interrupt thread.
MM_EXPORT_REDIS void mmRedisPoper_Interrupt(struct mmRedisPoper* p)
{
    p->state = MM_TS_CLOSED;
    //
    mmRedisSync_Interrupt(&p->ctx);
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// shutdown thread.
MM_EXPORT_REDIS void mmRedisPoper_Shutdown(struct mmRedisPoper* p)
{
    p->state = MM_TS_FINISH;
    //
    mmRedisSync_Shutdown(&p->ctx);
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// join thread.
MM_EXPORT_REDIS void mmRedisPoper_Join(struct mmRedisPoper* p)
{
    mmRedisSync_Join(&p->ctx);
    pthread_join(p->thread_poper, NULL);
}

static void* __static_mmRedisPoper_PopThread(void* arg)
{
    struct mmRedisPoper* p = (struct mmRedisPoper*)(arg);
    mmRedisPoper_Pop(p);
    return NULL;
}
