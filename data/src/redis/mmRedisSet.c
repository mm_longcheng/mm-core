/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRedisSet.h"
#include "mmRedisSync.h"
#include "mmRedisReply.h"

#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmAtoi.h"

#include "hiredis.h"

MM_EXPORT_REDIS long long mmRedisSet_StrU32Sadd(struct mmRedisSync* ctx, const char* key, mmUInt32_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "sadd %s %u", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[sadd %s %u]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[sadd %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisSet_StrU64Sadd(struct mmRedisSync* ctx, const char* key, mmUInt64_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "sadd %s %u", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[sadd %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[sadd %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisSet_StrU32Srem(struct mmRedisSync* ctx, const char* key, mmUInt32_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "srem %s %u", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[srem %s %u]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[srem %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisSet_StrU64Srem(struct mmRedisSync* ctx, const char* key, mmUInt64_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "srem %s %u", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[srem %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[srem %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisSet_StrU64Scard(struct mmRedisSync* ctx, const char* key)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "scard %s", key);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[scard %s]. reply_is_null error:(%d)%s.",
                              key, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[scard %s]. reply_type_error type:%d error:(%d)%s.",
                              key, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}

MM_EXPORT_REDIS long long mmRedisSet_StrU32Sismember(struct mmRedisSync* ctx, const char* key, mmUInt32_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "sismember %s %u", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[sismember %s %u]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[sismember %s %u]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
MM_EXPORT_REDIS long long mmRedisSet_StrU64Sismember(struct mmRedisSync* ctx, const char* key, mmUInt64_t member)
{
    long long rt = 0;
    if (1 == mmRedisSync_Check(ctx))
    {
        redisContext* c = ctx->c;
        redisReply* r = NULL;
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        do
        {
            // bin data need %b format.
            r = (redisReply*)redisCommand(c, "sismember %s %u", key, member);
            if (NULL == r)
            {
                mmLogger_LogE(gLogger, "command[sismember %s %" PRIu64 "]. reply_is_null error:(%d)%s.",
                              key, member, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_IS_NULL;
                break;
            }
            if ((r->type != REDIS_REPLY_INTEGER) && (r->type != REDIS_REPLY_STATUS || 0 != strcasecmp(r->str, "OK")))
            {
                // here we try print value for a string.
                mmLogger_LogE(gLogger, "command[sismember %s %" PRIu64 "]. reply_type_error type:%d error:(%d)%s.",
                              key, member, r->type, c->err, c->errstr);
                ctx->error_code = MM_REPLYCODE_REPLY_TYPE_ERROR;
                break;
            }
            rt = r->integer;
            ctx->error_code = MM_REPLYCODE_SUCCESS;
        } while (0);
        mmRedisSync_FreeReply(ctx, r);
    }
    return rt;
}
