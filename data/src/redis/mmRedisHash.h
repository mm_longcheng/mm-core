/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRedisHash_h__
#define __mmRedisHash_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"
#include "container/mmRbtreeU64.h"
#include "container/mmRbtreeString.h"

#include "container/mmRbtsetU32.h"
#include "container/mmRbtsetU64.h"
#include "container/mmRbtsetString.h"

#include "redis/mmRedisExport.h"

#include "core/mmPrefix.h"

struct mmRedisSync;
struct mmString;
// note: you must know what type can memcpy to ptr.

MM_EXPORT_REDIS long long mmRedisHash_StrBinHset(struct mmRedisSync* ctx, const char* key, const char* fid, void* ptr, size_t size);
MM_EXPORT_REDIS long long mmRedisHash_StrU32Hset(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt32_t val);
MM_EXPORT_REDIS long long mmRedisHash_StrU64Hset(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt64_t val);
MM_EXPORT_REDIS int mmRedisHash_StrBinHget(struct mmRedisSync* ctx, const char* key, const char* fid, void* ptr, size_t size);
MM_EXPORT_REDIS int mmRedisHash_StrU32Hget(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt32_t* val);
MM_EXPORT_REDIS int mmRedisHash_StrU64Hget(struct mmRedisSync* ctx, const char* key, const char* fid, mmUInt64_t* val);
MM_EXPORT_REDIS long long mmRedisHash_StrHdel(struct mmRedisSync* ctx, const char* key, const char* fid);

MM_EXPORT_REDIS long long mmRedisHash_U32BinHset(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, void* ptr, size_t size);
MM_EXPORT_REDIS int mmRedisHash_U32BinHget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, void* ptr, size_t size);

MM_EXPORT_REDIS long long mmRedisHash_U64BinHset(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, void* ptr, size_t size);
MM_EXPORT_REDIS int mmRedisHash_U64BinHget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, void* ptr, size_t size);

MM_EXPORT_REDIS long long mmRedisHash_U32U32Hset(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt32_t vid);
MM_EXPORT_REDIS long long mmRedisHash_U32U64Hset(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt64_t vid);
MM_EXPORT_REDIS int mmRedisHash_U32U32Hget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt32_t* vid);
MM_EXPORT_REDIS int mmRedisHash_U32U64Hget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmUInt64_t* vid);
MM_EXPORT_REDIS long long mmRedisHash_U32Hdel(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid);

MM_EXPORT_REDIS long long mmRedisHash_U64U32Hset(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt32_t vid);
MM_EXPORT_REDIS long long mmRedisHash_U64U64Hset(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt64_t vid);
MM_EXPORT_REDIS int mmRedisHash_U64U32Hget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt32_t* vid);
MM_EXPORT_REDIS int mmRedisHash_U64U64Hget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmUInt64_t* vid);
MM_EXPORT_REDIS long long mmRedisHash_U64Hdel(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid);

MM_EXPORT_REDIS int mmRedisHash_StrStringHget(struct mmRedisSync* ctx, const char* key, const char* fid, struct mmString* val);
MM_EXPORT_REDIS int mmRedisHash_U32StringHget(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, struct mmString* val);
MM_EXPORT_REDIS int mmRedisHash_U64StringHget(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, struct mmString* val);

MM_EXPORT_REDIS int mmRedisHash_U32Hlen(struct mmRedisSync* ctx, const char* key, mmUInt32_t* len);
MM_EXPORT_REDIS int mmRedisHash_U64Hlen(struct mmRedisSync* ctx, const char* key, mmUInt64_t* len);

MM_EXPORT_REDIS int mmRedisHash_U32Hincrby(struct mmRedisSync* ctx, const char* key, mmUInt32_t fid, mmSInt32_t n, mmSInt32_t* v);
MM_EXPORT_REDIS int mmRedisHash_U64Hincrby(struct mmRedisSync* ctx, const char* key, mmUInt64_t fid, mmSInt32_t n, mmSInt32_t* v);
MM_EXPORT_REDIS int mmRedisHash_StrHincrby(struct mmRedisSync* ctx, const char* key, const char *fid, mmSInt32_t n, mmSInt32_t* v);

MM_EXPORT_REDIS int mmRedisHash_U32U32Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU32U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32U64Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU32U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32BinHgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU32String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U32Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU64U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U64Hgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU64U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64BinHgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeU64String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_StringBinHgetall(struct mmRedisSync* ctx, const char* key, struct mmRbtreeStringString* rbtree);

MM_EXPORT_REDIS void mmRedisHash_U32HmgetFormat(struct mmRbtsetU32* rbtset, struct mmString* format);
MM_EXPORT_REDIS void mmRedisHash_U64HmgetFormat(struct mmRbtsetU64* rbtset, struct mmString* format);
MM_EXPORT_REDIS void mmRedisHash_StringHmgetFormat(struct mmRbtsetString* rbtset, struct mmString* format);
//
MM_EXPORT_REDIS int mmRedisHash_U32U32Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32U64Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32BinHmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, struct mmRbtreeU32String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U32Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U64Hmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64BinHmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, struct mmRbtreeU64String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_StringBinHmget(struct mmRedisSync* ctx, const char* key, struct mmRbtsetString* rbtset, struct mmRbtreeStringString* rbtree);
//
MM_EXPORT_REDIS int mmRedisHash_U32U32HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, size_t page, struct mmRbtreeU32U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32U64HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, size_t page, struct mmRbtreeU32U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32BinHmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU32* rbtset, size_t page, struct mmRbtreeU32String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U32HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, size_t page, struct mmRbtreeU64U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U64HmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, size_t page, struct mmRbtreeU64U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64BinHmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetU64* rbtset, size_t page, struct mmRbtreeU64String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_StringBinHmgetPage(struct mmRedisSync* ctx, const char* key, struct mmRbtsetString* rbtset, size_t page, struct mmRbtreeStringString* rbtree);
//
MM_EXPORT_REDIS int mmRedisHash_U32U32HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32U64HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU32* rbtset, struct mmRbtreeU32U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U32BinHmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU32* rbtset, struct mmRbtreeU32String* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U32HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U32* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64U64HmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU64* rbtset, struct mmRbtreeU64U64* rbtree);
MM_EXPORT_REDIS int mmRedisHash_U64BinHmgetPatternPage(struct mmRedisSync* ctx, const char* pattern, mmUInt32_t mod_number, size_t page, struct mmRbtsetU64* rbtset, struct mmRbtreeU64String* rbtree);

#include "core/mmSuffix.h"

#endif//__mmRedisHash_h__
