/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCurlUnitManager_h__
#define __mmCurlUnitManager_h__

#include "core/mmConfig.h"
#include "core/mmTime.h"

#include "curl/mmCurlUnitNucleus.h"
#include "curl/mmCurlUnitPool.h"

#include "curl/mmCurlExport.h"

#include "core/mmPrefix.h"

struct mmCurlUnitManager
{
    struct mmCurlUnitNucleus curl_unit_nucleus;
    struct mmCurlUnitPool curl_unit_pool;
};

MM_EXPORT_CURL void mmCurlUnitManager_Init(struct mmCurlUnitManager* p);
MM_EXPORT_CURL void mmCurlUnitManager_Destroy(struct mmCurlUnitManager* p);

MM_EXPORT_CURL void mmCurlUnitManager_SetLength(struct mmCurlUnitManager* p, mmUInt32_t length);
MM_EXPORT_CURL mmUInt32_t mmCurlUnitManager_GetLength(struct mmCurlUnitManager* p);
//
MM_EXPORT_CURL void mmCurlUnitManager_SetCurlTimeout(struct mmCurlUnitManager* p, int curl_timeout);
MM_EXPORT_CURL int mmCurlUnitManager_GetCurlTimeout(struct mmCurlUnitManager* p);

MM_EXPORT_CURL struct mmCurlUnit* mmCurlUnitManager_Produce(struct mmCurlUnitManager* p);
MM_EXPORT_CURL void mmCurlUnitManager_Recycle(struct mmCurlUnitManager* p, struct mmCurlUnit* unit);

MM_EXPORT_CURL void mmCurlUnitManager_ProcessHandle(struct mmCurlUnitManager* p, struct mmCurlUnit* unit);

MM_EXPORT_CURL void mmCurlUnitManager_Start(struct mmCurlUnitManager* p);
MM_EXPORT_CURL void mmCurlUnitManager_Interrupt(struct mmCurlUnitManager* p);
MM_EXPORT_CURL void mmCurlUnitManager_Shutdown(struct mmCurlUnitManager* p);
MM_EXPORT_CURL void mmCurlUnitManager_Join(struct mmCurlUnitManager* p);

#include "core/mmSuffix.h"

#endif//__mmCurlUnitManager_h__
