/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCurlUnitManager.h"
#include <assert.h>

static void __static_mmCurlUnitManager_NuclearHandle(void* pbj, struct mmCurlUnit* unit);

MM_EXPORT_CURL void mmCurlUnitManager_Init(struct mmCurlUnitManager* p)
{
    struct mmCurlUnitNuclearCallback _NuclearCallback;

    mmCurlUnitNucleus_Init(&p->curl_unit_nucleus);
    mmCurlUnitPool_Init(&p->curl_unit_pool);

    _NuclearCallback.Handle = &__static_mmCurlUnitManager_NuclearHandle;
    _NuclearCallback.obj = p;
    mmCurlUnitNucleus_SetCallback(&p->curl_unit_nucleus, &_NuclearCallback);
}
MM_EXPORT_CURL void mmCurlUnitManager_Destroy(struct mmCurlUnitManager* p)
{
    mmCurlUnitNucleus_Destroy(&p->curl_unit_nucleus);
    mmCurlUnitPool_Destroy(&p->curl_unit_pool);
}

MM_EXPORT_CURL void mmCurlUnitManager_SetLength(struct mmCurlUnitManager* p, mmUInt32_t length)
{
    mmCurlUnitNucleus_SetLength(&p->curl_unit_nucleus, length);
}
MM_EXPORT_CURL mmUInt32_t mmCurlUnitManager_GetLength(struct mmCurlUnitManager* p)
{
    return mmCurlUnitNucleus_GetLength(&p->curl_unit_nucleus);
}
MM_EXPORT_CURL void mmCurlUnitManager_SetCurlTimeout(struct mmCurlUnitManager* p, int curl_timeout)
{
    mmCurlUnitNucleus_SetCurlTimeout(&p->curl_unit_nucleus, curl_timeout);
}
MM_EXPORT_CURL int mmCurlUnitManager_GetCurlTimeout(struct mmCurlUnitManager* p)
{
    return mmCurlUnitNucleus_GetCurlTimeout(&p->curl_unit_nucleus);
}

MM_EXPORT_CURL struct mmCurlUnit* mmCurlUnitManager_Produce(struct mmCurlUnitManager* p)
{
    return mmCurlUnitPool_Produce(&p->curl_unit_pool);
}
MM_EXPORT_CURL void mmCurlUnitManager_Recycle(struct mmCurlUnitManager* p, struct mmCurlUnit* unit)
{
    mmCurlUnitPool_Recycle(&p->curl_unit_pool, unit);
}
MM_EXPORT_CURL void mmCurlUnitManager_ProcessHandle(struct mmCurlUnitManager* p, struct mmCurlUnit* unit)
{
    mmCurlUnitNucleus_ProcessHandle(&p->curl_unit_nucleus, unit);
}
MM_EXPORT_CURL void mmCurlUnitManager_Start(struct mmCurlUnitManager* p)
{
    assert(0 != p->curl_unit_nucleus.length && "0 != p->curl_unit_nucleus.length is invalid.");
    mmCurlUnitNucleus_Start(&p->curl_unit_nucleus);
}
MM_EXPORT_CURL void mmCurlUnitManager_Interrupt(struct mmCurlUnitManager* p)
{
    mmCurlUnitNucleus_Interrupt(&p->curl_unit_nucleus);
}
MM_EXPORT_CURL void mmCurlUnitManager_Shutdown(struct mmCurlUnitManager* p)
{
    mmCurlUnitNucleus_Shutdown(&p->curl_unit_nucleus);
}
MM_EXPORT_CURL void mmCurlUnitManager_Join(struct mmCurlUnitManager* p)
{
    mmCurlUnitNucleus_Join(&p->curl_unit_nucleus);
}

static void __static_mmCurlUnitManager_NuclearHandle(void* obj, struct mmCurlUnit* unit)
{
    struct mmCurlUnitNuclear* curl_unit_nuclear = (struct mmCurlUnitNuclear*)obj;
    struct mmCurlUnitManager* curl_unit_manager = (struct mmCurlUnitManager*)(curl_unit_nuclear->callback.obj);
    mmCurlUnitManager_Recycle(curl_unit_manager, unit);
}
