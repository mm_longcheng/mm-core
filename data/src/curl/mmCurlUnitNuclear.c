/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCurlUnitNuclear.h"
#include "core/mmThread.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"
#include "core/mmTimewait.h"


static void* __static_mmCurlUnitNuclear_PollWait(void* arg);
static void __static_mmCurlUnitNuclear_CheckDone(struct mmCurlUnitNuclear* p);

static void __static_mmCurlUnitNuclear_HandleDefault(void* pbj, struct mmCurlUnit* unit)
{

}
MM_EXPORT_CURL void mmCurlUnitNuclearCallback_Init(struct mmCurlUnitNuclearCallback* p)
{
    p->Handle = &__static_mmCurlUnitNuclear_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_CURL void mmCurlUnitNuclearCallback_Destroy(struct mmCurlUnitNuclearCallback* p)
{
    p->Handle = &__static_mmCurlUnitNuclear_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_CURL void mmCurlUnitNuclear_Init(struct mmCurlUnitNuclear* p)
{
    mmCurlUnitNuclearCallback_Init(&p->callback);
    mmListVpt_Init(&p->cache_unit);
    mmRbtsetVpt_Init(&p->poll_unit);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->locker_cache, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->curl_multi = curl_multi_init();
    p->empty_timeout = MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT;
    p->nonblock_timeout = MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT;
    p->poll_wait_timeout = MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT;
    p->curl_timeout = MM_CURL_UNIT_MANAGER_CURL_TIMEOUT;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Destroy(struct mmCurlUnitNuclear* p)
{
    curl_multi_cleanup(p->curl_multi);
    //
    mmCurlUnitNuclearCallback_Destroy(&p->callback);
    mmListVpt_Destroy(&p->cache_unit);
    mmRbtsetVpt_Destroy(&p->poll_unit);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->locker_cache);
    mmSpinlock_Destroy(&p->locker);
    p->curl_multi = NULL;
    p->empty_timeout = MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT;
    p->nonblock_timeout = MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT;
    p->poll_wait_timeout = MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT;
    p->curl_timeout = MM_CURL_UNIT_MANAGER_CURL_TIMEOUT;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Lock(struct mmCurlUnitNuclear* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Unlock(struct mmCurlUnitNuclear* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_CURL void mmCurlUnitNuclear_SetCallback(struct mmCurlUnitNuclear* p, struct mmCurlUnitNuclearCallback* callback)
{
    assert(NULL != callback && "callback is a null.");
    p->callback = (*callback);
}
//
MM_EXPORT_CURL void mmCurlUnitNuclear_SetCurlTimeout(struct mmCurlUnitNuclear* p, int curl_timeout)
{
    p->curl_timeout = curl_timeout;
}

MM_EXPORT_CURL void mmCurlUnitNuclear_ProcessHandle(struct mmCurlUnitNuclear* p, struct mmCurlUnit* unit)
{
    mmSpinlock_Lock(&p->locker_cache);
    mmListVpt_AddTail(&p->cache_unit, unit);
    mmSpinlock_Unlock(&p->locker_cache);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}

MM_EXPORT_CURL void mmCurlUnitNuclear_PollWait(struct mmCurlUnitNuclear* p)
{
    int return_code = 0;
    int running_handles = 0;
    size_t poll_number = 0;
    size_t append_number = 0;
    struct timeval  ntime;
    struct timespec otime;
    CURLMcode code = CURLM_LAST;
    struct mmCurlUnit* unit = NULL;
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* it = NULL;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    while (MM_TS_MOTION == p->state)
    {
        if (0 != p->poll_unit.size || 0 != p->cache_unit.size)
        {
            if (p->poll_unit.size < MM_CURL_UNIT_MANAGER_POLL_NUMBER && 0 < p->cache_unit.size)
            {
                poll_number = 0;
                append_number = MM_CURL_UNIT_MANAGER_POLL_NUMBER - p->poll_unit.size;
                mmSpinlock_Lock(&p->locker_cache);
                pos = p->cache_unit.l.next;
                while (pos != &p->cache_unit.l && poll_number < append_number)
                {
                    struct mmListHead* curr = pos;
                    pos = pos->next;
                    it = mmList_Entry(curr, struct mmListVptIterator, n);
                    unit = (struct mmCurlUnit*)(it->v);
                    mmCurlUnit_SetTimeout(unit, p->curl_timeout);
                    curl_multi_add_handle(p->curl_multi, unit->curl);
                    mmRbtsetVpt_Add(&p->poll_unit, unit);
                    mmListVpt_Erase(&p->cache_unit, it);
                    poll_number++;
                }
                mmSpinlock_Unlock(&p->locker_cache);
            }
            //
            code = curl_multi_wait(p->curl_multi, NULL, 0, p->poll_wait_timeout, &return_code);
            if (CURLM_OK == code)
            {
                curl_multi_perform(p->curl_multi, &running_handles);
                __static_mmCurlUnitNuclear_CheckDone(p);
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d errno:(%d) %s", __FUNCTION__, __LINE__, (int)code, curl_multi_strerror(code));
                mmMSleep(p->nonblock_timeout);
            }
        }
        else
        {
            // timewait nearby.
            mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, p->empty_timeout);
        }
    }
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Start(struct mmCurlUnitNuclear* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->poll_thread, NULL, &__static_mmCurlUnitNuclear_PollWait, p);
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Interrupt(struct mmCurlUnitNuclear* p)
{
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Shutdown(struct mmCurlUnitNuclear* p)
{
    p->state = MM_TS_FINISH;
}
MM_EXPORT_CURL void mmCurlUnitNuclear_Join(struct mmCurlUnitNuclear* p)
{
    pthread_join(p->poll_thread, NULL);
}

static void* __static_mmCurlUnitNuclear_PollWait(void* arg)
{
    struct mmCurlUnitNuclear* p = (struct mmCurlUnitNuclear*)(arg);
    mmCurlUnitNuclear_PollWait(p);
    return NULL;
}

static void __static_mmCurlUnitNuclear_CheckDone(struct mmCurlUnitNuclear* p)
{
    CURLMsg* message = NULL;
    int pending = 0;
    CURL* easy_handle = NULL;
    CURLcode code = CURL_LAST;
    void* private_pointer = NULL;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    while ((message = curl_multi_info_read(p->curl_multi, &pending)))
    {
        switch (message->msg)
        {
        case CURLMSG_DONE:
        {
            easy_handle = message->easy_handle;
            code = curl_easy_getinfo(easy_handle, CURLINFO_PRIVATE, &private_pointer);
            if (code != CURLE_OK)
            {
                mmLogger_LogE(gLogger, "%s %d errno:(%d) %s", __FUNCTION__, __LINE__, (int)code, curl_easy_strerror(code));
            }
            else
            {
                struct mmCurlUnit* unit = (struct mmCurlUnit*)(private_pointer);
                mmCurlUnit_FireEvent(unit);
                curl_multi_remove_handle(p->curl_multi, unit->curl);
                mmRbtsetVpt_Rmv(&p->poll_unit, unit);
                (*(p->callback.Handle))(p, unit);
            }
        }
        break;
        default:
        {
            mmLogger_LogW(gLogger, "%s %d CURLMSG default handle.", __FUNCTION__, __LINE__);
        }
        break;
        }
    }
}
