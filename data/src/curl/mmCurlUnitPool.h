/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCurlUnitPool_h__
#define __mmCurlUnitPool_h__

#include "core/mmConfig.h"
#include "core/mmTime.h"

#include "container/mmListVpt.h"
#include "container/mmRbtsetVpt.h"

#include "dish/mmIdGenerater.h"

#include "curl/mmCurlUnit.h"

#include "curl/mmCurlExport.h"

#include "core/mmPrefix.h"

#define MM_PAGE_SIZE_DEFAULT 10
#define MM_FOLD_SIZE_DEFAULT 2
#define MM_POOL_SIZE_DEFAULT 4

struct mmCurlUnitPool
{
    struct mmUInt32IdGenerater id_generater;
    struct mmListVpt pool;
    struct mmRbtsetVpt used;
    mmAtomic_t locker_pool;
    // default is MM_PAGE_SIZE_DEFAULT.
    size_t page_size;
    // default is MM_FOLD_SIZE_DEFAULT.
    size_t fold_size;
    // default is MM_POOL_SIZE_DEFAULT.
    size_t pool_size;
};

MM_EXPORT_CURL void mmCurlUnitPool_Init(struct mmCurlUnitPool* p);
MM_EXPORT_CURL void mmCurlUnitPool_Destroy(struct mmCurlUnitPool* p);

MM_EXPORT_CURL struct mmCurlUnit* mmCurlUnitPool_Produce(struct mmCurlUnitPool* p);
MM_EXPORT_CURL void mmCurlUnitPool_Recycle(struct mmCurlUnitPool* p, struct mmCurlUnit* unit);

MM_EXPORT_CURL void mmCurlUnitPool_Clear(struct mmCurlUnitPool* p);

MM_EXPORT_CURL void mmCurlUnitPool_TryDecrease(struct mmCurlUnitPool* p);
MM_EXPORT_CURL void mmCurlUnitPool_TryIncrease(struct mmCurlUnitPool* p);

#include "core/mmSuffix.h"

#endif//__mmCurlUnitPool_h__
