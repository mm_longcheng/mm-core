/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCurlUnitNuclear_h__
#define __mmCurlUnitNuclear_h__

#include "core/mmConfig.h"
#include "core/mmTime.h"

#include "container/mmListVpt.h"
#include "container/mmRbtsetVpt.h"

#include "curl/mmCurlUnit.h"

#include <pthread.h>

#include "curl/mmCurlExport.h"

#include "core/mmPrefix.h"

// nonblock timeout..
#define MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT 2000
#define MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT 1000
#define MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT 200
#define MM_CURL_UNIT_MANAGER_CURL_TIMEOUT 30
#define MM_CURL_UNIT_MANAGER_POLL_NUMBER 256

struct mmCurlUnitNuclearCallback
{
    void(*Handle)(void* obj, struct mmCurlUnit* unit);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_CURL void mmCurlUnitNuclearCallback_Init(struct mmCurlUnitNuclearCallback* p);
MM_EXPORT_CURL void mmCurlUnitNuclearCallback_Destroy(struct mmCurlUnitNuclearCallback* p);

struct mmCurlUnitNuclear
{
    struct mmCurlUnitNuclearCallback callback;
    struct mmListVpt cache_unit;
    struct mmRbtsetVpt poll_unit;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t locker_cache;
    mmAtomic_t locker;
    CURLM* curl_multi;
    // thread.
    pthread_t poll_thread;
    // empty timeout.default is MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT milliseconds.
    mmMSec_t empty_timeout;
    // nonblock timeout.default is MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;
    // poll wait timeout.default is MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT milliseconds.
    mmMSec_t poll_wait_timeout;
    // curl wait timeout.default is MM_CURL_UNIT_MANAGER_CURL_TIMEOUT milliseconds.
    mmMSec_t curl_timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_CURL void mmCurlUnitNuclear_Init(struct mmCurlUnitNuclear* p);
MM_EXPORT_CURL void mmCurlUnitNuclear_Destroy(struct mmCurlUnitNuclear* p);

MM_EXPORT_CURL void mmCurlUnitNuclear_Lock(struct mmCurlUnitNuclear* p);
MM_EXPORT_CURL void mmCurlUnitNuclear_Unlock(struct mmCurlUnitNuclear* p);

MM_EXPORT_CURL void mmCurlUnitNuclear_SetCallback(struct mmCurlUnitNuclear* p, struct mmCurlUnitNuclearCallback* callback);

MM_EXPORT_CURL void mmCurlUnitNuclear_SetCurlTimeout(struct mmCurlUnitNuclear* p, int curl_timeout);

MM_EXPORT_CURL void mmCurlUnitNuclear_ProcessHandle(struct mmCurlUnitNuclear* p, struct mmCurlUnit* unit);

MM_EXPORT_CURL void mmCurlUnitNuclear_PollWait(struct mmCurlUnitNuclear* p);

MM_EXPORT_CURL void mmCurlUnitNuclear_Start(struct mmCurlUnitNuclear* p);
MM_EXPORT_CURL void mmCurlUnitNuclear_Interrupt(struct mmCurlUnitNuclear* p);
MM_EXPORT_CURL void mmCurlUnitNuclear_Shutdown(struct mmCurlUnitNuclear* p);
MM_EXPORT_CURL void mmCurlUnitNuclear_Join(struct mmCurlUnitNuclear* p);

#include "core/mmSuffix.h"

#endif//__mmCurlUnitNuclear_h__
