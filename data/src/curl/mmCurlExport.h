/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCurlExport_h__
#define __mmCurlExport_h__

#include "core/mmPlatform.h"

// windwos
#if MM_PLATFORM == MM_PLATFORM_WIN32

/* DLLExport define */
#  ifndef MM_EXPORT_CURL
#    ifdef MM_SHARED_CURL
#      ifdef MM_BUILD_CURL
         /* We are building this library */
#        define MM_EXPORT_CURL __declspec(dllexport)
#      else
         /* We are using this library */
#        define MM_EXPORT_CURL __declspec(dllimport)
#      endif
#    else
#      define MM_EXPORT_CURL
#    endif
#  endif

#  ifndef MM_PRIVATE_CURL
#    define MM_PRIVATE_CURL 
#  endif

#  ifndef MM_DEPRECATED_CURL
#    define MM_DEPRECATED_CURL __declspec(deprecated)
#  endif

#  ifndef MM_DEPRECATED_EXPORT_CURL
#    define MM_DEPRECATED_EXPORT_CURL MM_EXPORT_CURL MM_DEPRECATED_CURL
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_CURL
#    define MM_DEPRECATED_PRIVATE_CURL MM_PRIVATE_CURL MM_DEPRECATED_CURL
#  endif

#else// unix

// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).

/* DLLExport define */
#  ifndef MM_EXPORT_CURL
#    ifdef MM_SHARED_CURL
#      ifdef MM_BUILD_CURL
         /* We are building this library */
#        define MM_EXPORT_CURL __attribute__ ((visibility("default")))
#      else
         /* We are using this library */
#        define MM_EXPORT_CURL
#      endif
#    else
#      define MM_EXPORT_CURL
#    endif
#  endif

#  ifndef MM_PRIVATE_CURL
#    define MM_PRIVATE_CURL 
#  endif

#  ifndef MM_DEPRECATED_CURL
#    define MM_DEPRECATED_CURL __attribute__ ((deprecated))
#  endif

#  ifndef MM_DEPRECATED_EXPORT_CURL
#    define MM_DEPRECATED_EXPORT_CURL MM_EXPORT_CURL MM_DEPRECATED_CURL
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_CURL
#    define MM_DEPRECATED_PRIVATE_CURL MM_PRIVATE_CURL MM_DEPRECATED_CURL
#  endif

#endif

#endif//__mmCurlExport_h__
