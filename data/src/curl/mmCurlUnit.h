/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCurlUnit_h__
#define __mmCurlUnit_h__

#include "core/mmConfig.h"
#include "core/mmStreambuf.h"
#include "core/mmAtomic.h"

#include "curl/curl.h"

#include "curl/mmCurlExport.h"

#include "core/mmPrefix.h"

#define MM_CURL_UNIT_INVALID_ID -1

struct mmCurlUnit;

struct mmCurlUnitCallback
{
    void(*Handle)(struct mmCurlUnit* obj);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_CURL void mmCurlUnitCallback_Init(struct mmCurlUnitCallback* p);
MM_EXPORT_CURL void mmCurlUnitCallback_Destroy(struct mmCurlUnitCallback* p);

struct mmCurlUnitPool;

struct mmCurlUnit
{
    struct mmCurlUnitCallback callback;
    
    // strong ref.
    struct mmStreambuf buff_recv;
    // strong ref.
    struct mmStreambuf buff_send;
    
    struct mmCurlUnitPool* pool;
    mmAtomic_t locker;
    CURL* curl;
    mmUInt32_t id;
    void* u;
};

MM_EXPORT_CURL void mmCurlUnit_Init(struct mmCurlUnit* p);
MM_EXPORT_CURL void mmCurlUnit_Destroy(struct mmCurlUnit* p);

MM_EXPORT_CURL void mmCurlUnit_Produce(struct mmCurlUnit* p);
MM_EXPORT_CURL void mmCurlUnit_Recycle(struct mmCurlUnit* p);

MM_EXPORT_CURL void mmCurlUnit_Lock(struct mmCurlUnit* p);
MM_EXPORT_CURL void mmCurlUnit_Unlock(struct mmCurlUnit* p);

MM_EXPORT_CURL void mmCurlUnit_SetContext(struct mmCurlUnit* p, void* u);
MM_EXPORT_CURL void mmCurlUnit_SetCallback(struct mmCurlUnit* p, struct mmCurlUnitCallback* callback);
MM_EXPORT_CURL void mmCurlUnit_SetTimeout(struct mmCurlUnit* p, int seconds);

MM_EXPORT_CURL void mmCurlUnit_FireEvent(struct mmCurlUnit* p);

#include "core/mmSuffix.h"

#endif//__mmCurlUnit_h__
