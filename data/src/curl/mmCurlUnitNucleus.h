/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCurlUnitNucleus_h__
#define __mmCurlUnitNucleus_h__

#include "core/mmConfig.h"
#include "core/mmTime.h"

#include "curl/mmCurlUnitNuclear.h"

#include <pthread.h>

#include "curl/mmCurlExport.h"

#include "core/mmPrefix.h"

struct mmCurlUnitNucleus
{
    struct mmCurlUnitNuclearCallback callback;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t arrays_locker;
    mmAtomic_t locker;
    // length. default is 0.
    mmUInt32_t length;
    // array pointer.
    struct mmCurlUnitNuclear** arrays;

    // empty timeout.default is MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT milliseconds.
    mmMSec_t empty_timeout;
    // nonblock timeout.default is MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;
    // poll wait timeout.default is MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT milliseconds.
    mmMSec_t poll_wait_timeout;
    // curl wait timeout.default is MM_CURL_UNIT_MANAGER_CURL_TIMEOUT milliseconds.
    int curl_timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_CURL void mmCurlUnitNucleus_Init(struct mmCurlUnitNucleus* p);
MM_EXPORT_CURL void mmCurlUnitNucleus_Destroy(struct mmCurlUnitNucleus* p);

MM_EXPORT_CURL void mmCurlUnitNucleus_SetLength(struct mmCurlUnitNucleus* p, mmUInt32_t length);
MM_EXPORT_CURL mmUInt32_t mmCurlUnitNucleus_GetLength(struct mmCurlUnitNucleus* p);

MM_EXPORT_CURL void mmCurlUnitNucleus_SetCurlTimeout(struct mmCurlUnitNucleus* p, int curl_timeout);
MM_EXPORT_CURL int mmCurlUnitNucleus_GetCurlTimeout(struct mmCurlUnitNucleus* p);

MM_EXPORT_CURL void mmCurlUnitNucleus_SetCallback(struct mmCurlUnitNucleus* p, struct mmCurlUnitNuclearCallback* callback);

MM_EXPORT_CURL mmUInt32_t mmCurlUnitNucleus_HashIndex(struct mmCurlUnitNucleus* p, mmUInt32_t id);
MM_EXPORT_CURL void mmCurlUnitNucleus_ProcessHandle(struct mmCurlUnitNucleus* p, struct mmCurlUnit* unit);
MM_EXPORT_CURL void mmCurlUnitNucleus_Clear(struct mmCurlUnitNucleus* p);

MM_EXPORT_CURL void mmCurlUnitNucleus_Start(struct mmCurlUnitNucleus* p);
MM_EXPORT_CURL void mmCurlUnitNucleus_Interrupt(struct mmCurlUnitNucleus* p);
MM_EXPORT_CURL void mmCurlUnitNucleus_Shutdown(struct mmCurlUnitNucleus* p);
MM_EXPORT_CURL void mmCurlUnitNucleus_Join(struct mmCurlUnitNucleus* p);

#include "core/mmSuffix.h"

#endif//__mmCurlUnitNucleus_h__
