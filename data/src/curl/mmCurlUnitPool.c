/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCurlUnitPool.h"
#include "core/mmAlloc.h"
#include "core/mmThread.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"

MM_EXPORT_CURL void mmCurlUnitPool_Init(struct mmCurlUnitPool* p)
{
    mmUInt32IdGenerater_Init(&p->id_generater);
    mmListVpt_Init(&p->pool);
    mmRbtsetVpt_Init(&p->used);
    mmSpinlock_Init(&p->locker_pool, NULL);
    p->page_size = MM_PAGE_SIZE_DEFAULT;
    p->fold_size = MM_FOLD_SIZE_DEFAULT;
    p->pool_size = MM_POOL_SIZE_DEFAULT;
}
MM_EXPORT_CURL void mmCurlUnitPool_Destroy(struct mmCurlUnitPool* p)
{
    mmCurlUnitPool_Clear(p);
    //
    mmUInt32IdGenerater_Destroy(&p->id_generater);
    mmListVpt_Destroy(&p->pool);
    mmRbtsetVpt_Destroy(&p->used);
    mmSpinlock_Destroy(&p->locker_pool);
    p->page_size = MM_PAGE_SIZE_DEFAULT;
    p->fold_size = MM_FOLD_SIZE_DEFAULT;
    p->pool_size = MM_POOL_SIZE_DEFAULT;
}

MM_EXPORT_CURL struct mmCurlUnit* mmCurlUnitPool_Produce(struct mmCurlUnitPool* p)
{
    struct mmCurlUnit* unit = NULL;
    mmCurlUnitPool_TryIncrease(p);
    mmSpinlock_Lock(&p->locker_pool);
    unit = (struct mmCurlUnit*)mmListVpt_PopFont(&p->pool);
    mmRbtsetVpt_Add(&p->used, unit);
    mmSpinlock_Unlock(&p->locker_pool);
    //
    mmCurlUnit_Lock(unit);
    mmCurlUnit_Produce(unit);
    mmCurlUnit_Unlock(unit);
    return unit;
}
MM_EXPORT_CURL void mmCurlUnitPool_Recycle(struct mmCurlUnitPool* p, struct mmCurlUnit* unit)
{
    mmCurlUnit_Lock(unit);
    mmCurlUnit_Recycle(unit);
    mmCurlUnit_Unlock(unit);
    //
    mmSpinlock_Lock(&p->locker_pool);
    mmRbtsetVpt_Rmv(&p->used, unit);
    mmListVpt_AddTail(&p->pool, unit);
    mmSpinlock_Unlock(&p->locker_pool);
    mmCurlUnitPool_TryDecrease(p);
}

MM_EXPORT_CURL void mmCurlUnitPool_Clear(struct mmCurlUnitPool* p)
{
    struct mmCurlUnit* unit = NULL;
    //
    mmSpinlock_Lock(&p->locker_pool);
    {
        struct mmListHead* pos = NULL;
        struct mmListVptIterator* it = NULL;
        pos = p->pool.l.next;
        while (pos != &p->pool.l)
        {
            struct mmListHead* curr = pos;
            pos = pos->next;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            unit = (struct mmCurlUnit*)(it->v);
            mmCurlUnit_Lock(unit);
            mmCurlUnit_Destroy(unit);
            mmListVpt_Erase(&p->pool, it);
            mmUInt32IdGenerater_Recycle(&p->id_generater, unit->id);
            mmCurlUnit_Unlock(unit);
            mmFree(unit);
        }
    }
    //
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        //
        n = mmRb_First(&p->used.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            n = mmRb_Next(n);
            unit = (struct mmCurlUnit*)(it->k);
            mmCurlUnit_Lock(unit);
            mmCurlUnit_Destroy(unit);
            mmRbtsetVpt_Erase(&p->used, it);
            mmUInt32IdGenerater_Recycle(&p->id_generater, unit->id);
            mmCurlUnit_Unlock(unit);
            mmFree(unit);
        }
    }
    mmSpinlock_Unlock(&p->locker_pool);
}

MM_EXPORT_CURL void mmCurlUnitPool_TryDecrease(struct mmCurlUnitPool* p)
{
    if (p->pool.size > p->pool_size * p->page_size)
    {
        size_t i = 0;
        size_t sz = p->fold_size * p->page_size;
        struct mmCurlUnit* unit = NULL;
        mmSpinlock_Lock(&p->locker_pool);
        for (i = 0; i < sz; ++i)
        {
            unit = (struct mmCurlUnit*)mmListVpt_PopFont(&p->pool);
            mmCurlUnit_Lock(unit);
            mmUInt32IdGenerater_Recycle(&p->id_generater, unit->id);
            mmCurlUnit_Destroy(unit);
            mmCurlUnit_Unlock(unit);
            mmFree(unit);
        }
        mmSpinlock_Unlock(&p->locker_pool);
    }
}
MM_EXPORT_CURL void mmCurlUnitPool_TryIncrease(struct mmCurlUnitPool* p)
{
    if (0 == p->pool.size)
    {
        size_t i = 0;
        struct mmCurlUnit* unit = NULL;
        mmSpinlock_Lock(&p->locker_pool);
        for (i = 0; i < p->page_size; ++i)
        {
            unit = (struct mmCurlUnit*)mmMalloc(sizeof(struct mmCurlUnit));
            mmCurlUnit_Init(unit);
            mmCurlUnit_Lock(unit);
            unit->pool = p;
            unit->id = mmUInt32IdGenerater_Produce(&p->id_generater);
            mmListVpt_AddTail(&p->pool, unit);
            mmCurlUnit_Unlock(unit);
        }
        mmSpinlock_Unlock(&p->locker_pool);
    }
}
