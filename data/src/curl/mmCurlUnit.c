/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCurlUnit.h"
#include "core/mmSpinlock.h"

static size_t __static_mmCurlUnit_WriteFunction(char* buffer, size_t size, size_t nitems, void *outstream);

static void __static_mmCurlUnit_HandleDefault(struct mmCurlUnit* obj)
{

}
MM_EXPORT_CURL void mmCurlUnitCallback_Init(struct mmCurlUnitCallback* p)
{
    p->Handle = &__static_mmCurlUnit_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_CURL void mmCurlUnitCallback_Destroy(struct mmCurlUnitCallback* p)
{
    p->Handle = &__static_mmCurlUnit_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_CURL void mmCurlUnit_Init(struct mmCurlUnit* p)
{
    mmCurlUnitCallback_Init(&p->callback);
    mmStreambuf_Init(&p->buff_recv);
    mmStreambuf_Init(&p->buff_send);
    p->pool = NULL;
    mmSpinlock_Init(&p->locker, NULL);
    p->curl = curl_easy_init();
    p->id = MM_CURL_UNIT_INVALID_ID;
    p->u = NULL;
}
MM_EXPORT_CURL void mmCurlUnit_Destroy(struct mmCurlUnit* p)
{
    curl_easy_cleanup(p->curl);
    //
    mmCurlUnitCallback_Destroy(&p->callback);
    mmStreambuf_Destroy(&p->buff_recv);
    mmStreambuf_Destroy(&p->buff_send);
    p->pool = NULL;
    mmSpinlock_Destroy(&p->locker);
    p->curl = NULL;
    p->id = MM_CURL_UNIT_INVALID_ID;
    p->u = NULL;
}
MM_EXPORT_CURL void mmCurlUnit_Produce(struct mmCurlUnit* p)
{
    curl_easy_reset(p->curl);
    mmStreambuf_Reset(&p->buff_recv);
    mmStreambuf_Reset(&p->buff_send);
    //
    p->u = NULL;
    //
    curl_easy_setopt(p->curl, CURLOPT_PRIVATE, p);
    curl_easy_setopt(p->curl, CURLOPT_WRITEFUNCTION, &__static_mmCurlUnit_WriteFunction);
    curl_easy_setopt(p->curl, CURLOPT_WRITEDATA, p);
}

MM_EXPORT_CURL void mmCurlUnit_Recycle(struct mmCurlUnit* p)
{

}

MM_EXPORT_CURL void mmCurlUnit_Lock(struct mmCurlUnit* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_CURL void mmCurlUnit_Unlock(struct mmCurlUnit* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_CURL void mmCurlUnit_SetContext(struct mmCurlUnit* p, void* u)
{
    p->u = u;
}
MM_EXPORT_CURL void mmCurlUnit_SetCallback(struct mmCurlUnit* p, struct mmCurlUnitCallback* callback)
{
    assert(NULL != callback && "callback is a null.");
    p->callback = (*callback);
}
MM_EXPORT_CURL void mmCurlUnit_SetTimeout(struct mmCurlUnit* p, int seconds)
{
    curl_easy_setopt(p->curl, CURLOPT_TIMEOUT, seconds);
}
MM_EXPORT_CURL void mmCurlUnit_FireEvent(struct mmCurlUnit* p)
{
    (*(p->callback.Handle))(p);
}
static size_t __static_mmCurlUnit_WriteFunction(char* buffer, size_t size, size_t nitems, void *outstream)
{
    struct mmCurlUnit* unit = (struct mmCurlUnit*)outstream;
    size_t sz = size * nitems;
    mmStreambuf_AlignedMemory(&unit->buff_recv, sz);
    mmStreambuf_Sputn(&unit->buff_recv, (mmUInt8_t*)buffer, 0, sz);
    return sz;
}
