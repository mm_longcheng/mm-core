/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCurlUnitNucleus.h"
#include "core/mmAlloc.h"
#include "core/mmThread.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"
#include "algorithm/mmWangHash.h"

MM_EXPORT_CURL void mmCurlUnitNucleus_Init(struct mmCurlUnitNucleus* p)
{
    mmCurlUnitNuclearCallback_Init(&p->callback);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->arrays_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->length = 0;
    p->arrays = NULL;
    p->empty_timeout = MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT;
    p->nonblock_timeout = MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT;
    p->poll_wait_timeout = MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT;
    p->curl_timeout = MM_CURL_UNIT_MANAGER_CURL_TIMEOUT;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_CURL void mmCurlUnitNucleus_Destroy(struct mmCurlUnitNucleus* p)
{
    mmCurlUnitNucleus_Clear(p);
    //
    mmCurlUnitNuclearCallback_Destroy(&p->callback);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->arrays_locker);
    mmSpinlock_Destroy(&p->locker);
    p->length = 0;
    p->arrays = NULL;
    p->empty_timeout = MM_CURL_UNIT_MANAGER_EMPTY_TIMEOUT;
    p->nonblock_timeout = MM_CURL_UNIT_MANAGER_NONBLOCK_TIMEOUT;
    p->poll_wait_timeout = MM_CURL_UNIT_MANAGER_POLL_WAIT_TIMEOUT;
    p->curl_timeout = MM_CURL_UNIT_MANAGER_CURL_TIMEOUT;
    p->state = MM_TS_CLOSED;
}

MM_EXPORT_CURL void mmCurlUnitNucleus_SetLength(struct mmCurlUnitNucleus* p, mmUInt32_t length)
{
    mmSpinlock_Lock(&p->arrays_locker);
    if (length < p->length)
    {
        mmUInt32_t i = 0;
        struct mmCurlUnitNuclear* e = NULL;
        struct mmCurlUnitNuclear** v = NULL;
        for (i = length; i < p->length; ++i)
        {
            e = p->arrays[i];
            mmCurlUnitNuclear_Lock(e);
            p->arrays[i] = NULL;
            mmCurlUnitNuclear_Unlock(e);
            mmCurlUnitNuclear_Destroy(e);
            mmFree(e);
        }
        v = (struct mmCurlUnitNuclear**)mmMalloc(sizeof(struct mmCurlUnitNuclear*) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmCurlUnitNuclear*) * length);
        mmFree(p->arrays);
        p->arrays = v;
        p->length = length;
    }
    else if (length > p->length)
    {
        mmUInt32_t i = 0;
        struct mmCurlUnitNuclear* e = NULL;
        struct mmCurlUnitNuclear** v = NULL;
        v = (struct mmCurlUnitNuclear**)mmMalloc(sizeof(struct mmCurlUnitNuclear*) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmCurlUnitNuclear*) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        for (i = p->length; i < length; ++i)
        {
            e = (struct mmCurlUnitNuclear*)mmMalloc(sizeof(struct mmCurlUnitNuclear));
            mmCurlUnitNuclear_Init(e);
            e->empty_timeout = p->empty_timeout;
            e->nonblock_timeout = p->nonblock_timeout;
            e->poll_wait_timeout = p->poll_wait_timeout;
            e->curl_timeout = p->curl_timeout;
            e->state = p->state;
            mmCurlUnitNuclear_SetCallback(e, &p->callback);
            p->arrays[i] = e;
        }
        p->length = length;
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_CURL mmUInt32_t mmCurlUnitNucleus_GetLength(struct mmCurlUnitNucleus* p)
{
    return p->length;
}

MM_EXPORT_CURL void mmCurlUnitNucleus_SetCurlTimeout(struct mmCurlUnitNucleus* p, int curl_timeout)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->curl_timeout = curl_timeout;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        mmCurlUnitNuclear_SetCurlTimeout(e, curl_timeout);
        mmCurlUnitNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_CURL int mmCurlUnitNucleus_GetCurlTimeout(struct mmCurlUnitNucleus* p)
{
    return p->curl_timeout;
}

MM_EXPORT_CURL void mmCurlUnitNucleus_SetCallback(struct mmCurlUnitNucleus* p, struct mmCurlUnitNuclearCallback* callback)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    assert(NULL != callback && "you can not assign null callback.");
    mmSpinlock_Lock(&p->arrays_locker);
    p->callback = (*callback);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        mmCurlUnitNuclear_SetCallback(e, callback);
        mmCurlUnitNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_CURL mmUInt32_t mmCurlUnitNucleus_HashIndex(struct mmCurlUnitNucleus* p, mmUInt32_t id)
{
    return (mmUInt32_t)(mmWangHash64(id) % p->length);
}
MM_EXPORT_CURL void mmCurlUnitNucleus_ProcessHandle(struct mmCurlUnitNucleus* p, struct mmCurlUnit* unit)
{
    mmUInt32_t idx = mmCurlUnitNucleus_HashIndex(p, unit->id);
    assert(idx >= 0 && idx < p->length && "idx is out range.");
    mmCurlUnitNuclear_ProcessHandle(p->arrays[idx], unit);
}
MM_EXPORT_CURL void mmCurlUnitNucleus_Clear(struct mmCurlUnitNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        p->arrays[i] = NULL;
        mmCurlUnitNuclear_Unlock(e);
        mmCurlUnitNuclear_Destroy(e);
        mmFree(e);
    }
    mmFree(p->arrays);
    p->arrays = NULL;
    p->length = 0;
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_CURL void mmCurlUnitNucleus_Start(struct mmCurlUnitNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        mmCurlUnitNuclear_Start(e);
        mmCurlUnitNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_CURL void mmCurlUnitNucleus_Interrupt(struct mmCurlUnitNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_CLOSED;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        mmCurlUnitNuclear_Interrupt(e);
        mmCurlUnitNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_CURL void mmCurlUnitNucleus_Shutdown(struct mmCurlUnitNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        mmCurlUnitNuclear_Shutdown(e);
        mmCurlUnitNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_CURL void mmCurlUnitNucleus_Join(struct mmCurlUnitNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmCurlUnitNuclear* e = NULL;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmCurlUnitNuclear_Lock(e);
        mmCurlUnitNuclear_Join(e);
        mmCurlUnitNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

