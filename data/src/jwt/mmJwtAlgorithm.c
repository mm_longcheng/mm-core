/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmJwtAlgorithm.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static struct mmJwtAlgorithmImpl const_jwt_algorithm_impl[] =
{
    {        NULL, mmString_Make("none"), },

    { &EVP_sha256, mmString_Make("HS256"), },
    { &EVP_sha384, mmString_Make("HS384"), },
    { &EVP_sha512, mmString_Make("HS512"), },

    { &EVP_sha256, mmString_Make("RS256"), },
    { &EVP_sha384, mmString_Make("RS384"), },
    { &EVP_sha512, mmString_Make("RS512"), },

    { &EVP_sha256, mmString_Make("ES256"), },
    { &EVP_sha384, mmString_Make("ES384"), },
    { &EVP_sha512, mmString_Make("ES512"), },

    { &EVP_sha256, mmString_Make("PS256"), },
    { &EVP_sha384, mmString_Make("PS384"), },
    { &EVP_sha512, mmString_Make("PS512"), },
};

MM_EXPORT_JWT const struct mmJwtAlgorithmImpl* mmJwtAlgorithmImpl(int type)
{
    if (JWT_AT_HS256 <= type && type <= JWT_AT_PS512)
    {
        return &const_jwt_algorithm_impl[type];
    }
    else
    {
        return &const_jwt_algorithm_impl[JWT_AT_NONE];
    }
}

static void __static_bn2raw(const BIGNUM* bn, struct mmString* raw)
{
    size_t l = 0;
    unsigned char* d = NULL;
    mmString_Clear(raw);
    mmString_Resize(raw, BN_num_bytes(bn));
    l = mmString_Size(raw);
    d = (unsigned char*)mmString_Data(raw);
    BN_bn2bin(bn, d);
    if (l % 2 == 1 && d[0] == 0x00)
    {
        mmString_Substr(raw, raw, 1, l - 1);
    }
}
static void __static_raw2bn(struct mmString* raw, BIGNUM* bn)
{
    size_t l = 0;
    unsigned char* d = NULL;
    l = mmString_Size(raw);
    d = (unsigned char*)mmString_Data(raw);
    if ((mmUInt8_t)(d[0]) >= 0x80)
    {
        mmString_Resize(raw, l + 1);
        l = mmString_Size(raw);
        d = (unsigned char*)mmString_Data(raw);
        mmMemcpy(d + 1, d, l - 1);
        d[0] = 0x00;
    }
    BN_bin2bn((const unsigned char*)d, (int)l, bn);
}
// unused function now.
//static void __static_raw2der(struct mmString* raw, struct mmString* der)
//{
//  mmString_Resize(der, 4);
//  der->s[0] = 0x30;
//  der->s[1] = 0x00;
//  der->s[2] = 0x02;
//  der->s[3] = 0x00;
//
//  if (raw->s[0] & 0x80)
//  {
//      der->s[3] = (char)(raw->l / 2 + 1);
//      mm_string_putc(der, '\0');
//  }
//  else
//  {
//      der->s[3] = (char)(raw->l / 2);
//  }
//
//  mmString_Resize(der, der->l + (raw->l / 2));
//  mmMemcpy(der->s + der->l - (raw->l / 2), raw->s, (raw->l / 2));
//
//  if (raw->s[raw->l / 2] & 0x80)
//  {
//      mm_string_putc(der, 0x02);
//      mm_string_putc(der, (char)(raw->l / 2 + 1));
//      mm_string_putc(der, '\0');
//  }
//  else
//  {
//      mm_string_putc(der, (char)(raw->l / 2));
//  }
//
//  mmString_Resize(der, der->l + (raw->l / 2));
//  mmMemcpy(der->s + der->l - (raw->l / 2), raw->s + (raw->l / 2), (raw->l / 2));
//  der->s[1] = (char)(der->l - 2);
//}

static void __static_generate_hash(struct mmJwtAlgorithmBase* p, struct mmString* data, struct mmString* hash)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    unsigned int len = 0;

    EVP_MD_CTX* ctx = NULL;

#ifdef MM_JWT_OPENSSL10
    ctx = EVP_MD_CTX_create();
#else
    ctx = EVP_MD_CTX_new();
#endif

    do
    {
        if (EVP_DigestInit(ctx, (*(p->md))()) == 0)
        {
            mmLogger_LogE(gLogger, "%s %d EVP_DigestInit failed.", __FUNCTION__, __LINE__);
            break;
        }
        if (EVP_DigestUpdate(ctx, mmString_Data(data), mmString_Size(data)) == 0)
        {
            mmLogger_LogE(gLogger, "%s %d EVP_DigestUpdate failed.", __FUNCTION__, __LINE__);
            break;
        }

        mmString_Resize(hash, EVP_MD_CTX_size(ctx));

        if (EVP_DigestFinal(ctx, (unsigned char*)mmString_Data(hash), &len) == 0)
        {
            mmLogger_LogE(gLogger, "%s %d EVP_DigestFinal failed.", __FUNCTION__, __LINE__);
            break;
        }

        mmString_Resize(hash, len);
    } while (0);

#ifdef MM_JWT_OPENSSL10
    EVP_MD_CTX_destroy(ctx);
#else
    EVP_MD_CTX_free(ctx);
#endif
}

static void __static_jwt_sign_func_base(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* sign)
{
    mmString_Assigns(sign, "");
}
static int __static_jwt_verify_func_base(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* signature)
{
    return MM_TRUE;
}
MM_EXPORT_JWT void mmJwtAlgorithmBase_Init(struct mmJwtAlgorithmBase* p)
{
    mmString_Init(&p->name);
    p->md = &EVP_sha256;
    p->Sign = &__static_jwt_sign_func_base;
    p->Verify = &__static_jwt_verify_func_base;

    mmJwtAlgorithmBase_SetType(p, JWT_AT_NONE);
}
MM_EXPORT_JWT void mmJwtAlgorithmBase_Destroy(struct mmJwtAlgorithmBase* p)
{
    mmString_Destroy(&p->name);
    p->md = &EVP_sha256;
    p->Sign = &__static_jwt_sign_func_base;
    p->Verify = &__static_jwt_verify_func_base;
}
MM_EXPORT_JWT void mmJwtAlgorithmBase_SetMd(struct mmJwtAlgorithmBase* p, mmJwtEvpMdFunc md)
{
    p->md = md;
}
MM_EXPORT_JWT void mmJwtAlgorithmBase_SetName(struct mmJwtAlgorithmBase* p, const char* name)
{
    mmString_Assigns(&p->name, name);
}
MM_EXPORT_JWT void mmJwtAlgorithmBase_SetType(struct mmJwtAlgorithmBase* p, int type)
{
    const struct mmJwtAlgorithmImpl* jwt_impl = mmJwtAlgorithmImpl(type);
    const char* pName = mmString_CStr(&jwt_impl->n);
    mmJwtAlgorithmBase_SetMd(p, jwt_impl->c);
    mmJwtAlgorithmBase_SetName(p, pName);
}

static void __static_jwt_sign_func_hmacsha(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* sign)
{
    struct mmJwtAlgorithmHmacsha* p = (struct mmJwtAlgorithmHmacsha*)(impl);
    const char* ss; const char* ds; const char* is;
    size_t sl, dl, il;
    unsigned int len;

    mmString_Clear(sign);

    mmString_Resize(sign, EVP_MAX_MD_SIZE);

    ss = mmString_CStr(&p->secret);
    sl = mmString_Size(&p->secret);
    ds = mmString_CStr(data);
    dl = mmString_Size(data);
    is = mmString_CStr(sign);
    il = mmString_Size(sign);

    len = (unsigned int)il;
    if (NULL == HMAC((*(impl->md))(), ss, (int)sl, (const unsigned char*)ds, dl, (unsigned char*)is, &len))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d signature generation failure.", __FUNCTION__, __LINE__);
    }
    mmString_Resize(sign, len);
}
static int __static_jwt_verify_func_hmacsha(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* signature)
{
    struct mmJwtAlgorithmHmacsha* p = (struct mmJwtAlgorithmHmacsha*)(impl);
    const char* ss; const char* is;
    size_t sl, il;

    int matched = MM_TRUE;
    struct mmString sign;

    mmString_Init(&sign);

    (*(p->base.Sign))(impl, data, &sign);

    ss = mmString_CStr(signature);
    sl = mmString_Size(signature);
    is = mmString_CStr(&sign);
    il = mmString_Size(&sign);

    matched = ((il == sl) && (0 == mmMemcmp(is, ss, sl))) ? MM_TRUE : MM_FALSE;

    mmString_Destroy(&sign);

    return matched;
}
MM_EXPORT_JWT void mmJwtAlgorithmHmacsha_Init(struct mmJwtAlgorithmHmacsha* p)
{
    mmJwtAlgorithmBase_Init(&p->base);
    mmString_Init(&p->secret);

    p->base.Sign = &__static_jwt_sign_func_hmacsha;
    p->base.Verify = &__static_jwt_verify_func_hmacsha;
}
MM_EXPORT_JWT void mmJwtAlgorithmHmacsha_Destroy(struct mmJwtAlgorithmHmacsha* p)
{
    mmJwtAlgorithmBase_Destroy(&p->base);
    mmString_Destroy(&p->secret);

    p->base.Sign = &__static_jwt_sign_func_hmacsha;
    p->base.Verify = &__static_jwt_verify_func_hmacsha;
}
MM_EXPORT_JWT void mmJwtAlgorithmHmacsha_SetSecret(struct mmJwtAlgorithmHmacsha* p, struct mmString* secret)
{
    mmString_Assign(&p->secret, secret);
}

static void __static_jwt_sign_func_rsa(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* sign)
{
    struct mmJwtAlgorithmRsa* p = (struct mmJwtAlgorithmRsa*)(impl);
    struct mmLogger* gLogger = mmLogger_Instance();

    unsigned int len = 0;

    EVP_MD_CTX* ctx = NULL;

    mmString_Clear(sign);

#ifdef MM_JWT_OPENSSL10
    ctx = EVP_MD_CTX_create();
#else
    ctx = EVP_MD_CTX_new();
#endif

    do
    {
        if (!ctx)
        {
            mmLogger_LogE(gLogger, "%s %d failed to create signature: could not create context.", __FUNCTION__, __LINE__);
            break;
        }
        if (!EVP_SignInit(ctx, (*(impl->md))()))
        {
            mmLogger_LogE(gLogger, "%s %d failed to create signature: SignInit failed.", __FUNCTION__, __LINE__);
            break;
        }

        mmString_Resize(sign, EVP_PKEY_size(p->pkey));

        if (!EVP_SignUpdate(ctx, mmString_Data(data), mmString_Size(data)))
        {
            mmLogger_LogE(gLogger, "%s %d signature generation failure.", __FUNCTION__, __LINE__);
            break;
        }
        if (!EVP_SignFinal(ctx, (unsigned char*)mmString_Data(sign), &len, p->pkey))
        {
            mmLogger_LogE(gLogger, "%s %d signature generation failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    mmString_Resize(sign, len);

#ifdef MM_JWT_OPENSSL10
    EVP_MD_CTX_destroy(ctx);
#else
    EVP_MD_CTX_free(ctx);
#endif
}
static int __static_jwt_verify_func_rsa(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* signature)
{
    struct mmJwtAlgorithmRsa* p = (struct mmJwtAlgorithmRsa*)(impl);
    struct mmLogger* gLogger = mmLogger_Instance();

    int matched = MM_TRUE;
    EVP_MD_CTX* ctx = NULL;

#ifdef MM_JWT_OPENSSL10
    ctx = EVP_MD_CTX_create();
#else
    ctx = EVP_MD_CTX_new();
#endif

    do
    {
        if (!ctx)
        {
            mmLogger_LogE(gLogger, "%s %d failed to verify signature: could not create context.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
            break;
        }
        if (!EVP_VerifyInit(ctx, (*(impl->md))()))
        {
            mmLogger_LogE(gLogger, "%s %d failed to verify signature: VerifyInit failed.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
            break;
        }
        if (!EVP_VerifyUpdate(ctx, mmString_Data(data), mmString_Size(data)))
        {
            mmLogger_LogE(gLogger, "%s %d failed to verify signature: VerifyUpdate failed.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
            break;
        }
        if (!EVP_VerifyFinal(ctx, (const unsigned char*)mmString_Data(signature), (unsigned int)mmString_Size(signature), p->pkey))
        {
            mmLogger_LogE(gLogger, "%s %d signature generation failure.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
            break;
        }
    } while (0);

#ifdef MM_JWT_OPENSSL10
    EVP_MD_CTX_destroy(ctx);
#else
    EVP_MD_CTX_free(ctx);
#endif

    return matched;
}
MM_EXPORT_JWT void mmJwtAlgorithmRsa_Init(struct mmJwtAlgorithmRsa* p)
{
    mmJwtAlgorithmBase_Init(&p->base);
    p->pkey = NULL;

    p->base.Sign = &__static_jwt_sign_func_rsa;
    p->base.Verify = &__static_jwt_verify_func_rsa;
}
MM_EXPORT_JWT void mmJwtAlgorithmRsa_Destroy(struct mmJwtAlgorithmRsa* p)
{
    if (NULL != p->pkey)
    {
        EVP_PKEY_free(p->pkey);
        p->pkey = NULL;
    }

    mmJwtAlgorithmBase_Destroy(&p->base);
    p->pkey = NULL;

    p->base.Sign = &__static_jwt_sign_func_rsa;
    p->base.Verify = &__static_jwt_verify_func_rsa;
}
MM_EXPORT_JWT void mmJwtAlgorithmRsa_SetKey(struct mmJwtAlgorithmRsa* p, struct mmString* public_key, struct mmString* private_key, struct mmString* public_key_password, struct mmString* private_key_password)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    BIO* pubkey_bio = NULL;
    pubkey_bio = BIO_new(BIO_s_mem());

    do
    {
        const char* kd = mmString_Data(public_key);
        size_t kl = mmString_Size(public_key);
        if ((size_t)BIO_write(pubkey_bio, kd, (int)kl) != kl)
        {
            mmLogger_LogE(gLogger, "%s %d failed to load public key: bio_write failed.", __FUNCTION__, __LINE__);
            break;
        }

        p->pkey = PEM_read_bio_PUBKEY(pubkey_bio, NULL, NULL, (void*)mmString_Data(public_key_password));

        if (!p->pkey)
        {
            mmLogger_LogE(gLogger, "%s %d failed to load public key: PEM_read_bio_PUBKEY failed.", __FUNCTION__, __LINE__);
            break;
        }

        if (!mmString_Empty(private_key))
        {
            RSA* privkey = NULL;

            BIO* privkey_bio = NULL;
            privkey_bio = BIO_new(BIO_s_mem());

            do
            {
                const char* kd = mmString_Data(private_key);
                size_t kl = mmString_Size(private_key);
                if ((size_t)BIO_write(privkey_bio, kd, (int)kl) != kl)
                {
                    mmLogger_LogE(gLogger, "%s %d failed to load private key: bio_write failed.", __FUNCTION__, __LINE__);
                    break;
                }
                privkey = PEM_read_bio_RSAPrivateKey(privkey_bio, NULL, NULL, (void*)mmString_Data(private_key_password));
                if (privkey == NULL)
                {
                    mmLogger_LogE(gLogger, "%s %d failed to load private key: PEM_read_bio_RSAPrivateKey failed.", __FUNCTION__, __LINE__);
                    break;
                }
                if (EVP_PKEY_assign_RSA(p->pkey, privkey) == 0)
                {
                    RSA_free(privkey);
                    mmLogger_LogE(gLogger, "%s %d failed to load private key: EVP_PKEY_assign_RSA failed.", __FUNCTION__, __LINE__);
                    break;
                }
            } while (0);

            BIO_free_all(privkey_bio);
        }
    } while (0);

    BIO_free_all(pubkey_bio);
}
MM_EXPORT_JWT void mmJwtAlgorithmRsa_SetPublicKey(struct mmJwtAlgorithmRsa* p, struct mmString* public_key)
{
    struct mmString empty;
    mmString_MakeWeaks(&empty, "");

    mmJwtAlgorithmRsa_SetKey(p, public_key, &empty, &empty, &empty);
}

static void __static_jwt_sign_func_ecdsa(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* sign)
{
    struct mmJwtAlgorithmEcdsa* p = (struct mmJwtAlgorithmEcdsa*)(impl);

    ECDSA_SIG* sig = NULL;

    const BIGNUM* r = NULL;
    const BIGNUM* s = NULL;

    struct mmString hash;

    struct mmString string_r;
    struct mmString string_s;

    mmString_Clear(sign);

    mmString_Init(&hash);

    mmString_Init(&string_r);
    mmString_Init(&string_s);

    __static_generate_hash(&p->base, data, &hash);

    sig = ECDSA_do_sign((const unsigned char*)mmString_Data(&hash), (int)mmString_Size(&hash), p->pkey);

#ifdef MM_JWT_OPENSSL10
    r = sig->r;
    s = sig->s;
#else
    ECDSA_SIG_get0(sig, &r, &s);
#endif
    __static_bn2raw(r, &string_r);
    __static_bn2raw(s, &string_s);

    mmString_Append(sign, &string_r);
    mmString_Append(sign, &string_s);

    ECDSA_SIG_free(sig);

    mmString_Destroy(&hash);

    mmString_Destroy(&string_r);
    mmString_Destroy(&string_s);
}
static int __static_jwt_verify_func_ecdsa(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* signature)
{
    struct mmJwtAlgorithmEcdsa* p = (struct mmJwtAlgorithmEcdsa*)(impl);
    struct mmLogger* gLogger = mmLogger_Instance();

    int matched = MM_TRUE;

    size_t sl = 0;
    size_t hl = 0;

    BIGNUM* r = BN_new();
    BIGNUM* s = BN_new();

    struct mmString hash;

    struct mmString string_r;
    struct mmString string_s;

    mmString_Init(&hash);

    mmString_Init(&string_r);
    mmString_Init(&string_s);

    __static_generate_hash(&p->base, data, &hash);

    sl = mmString_Size(signature);
    hl = sl / 2;
    mmString_Substr(signature, &string_r,  0, hl);
    mmString_Substr(signature, &string_s, hl, hl);

    __static_raw2bn(&string_r, r);
    __static_raw2bn(&string_s, s);

#ifdef MM_JWT_OPENSSL10
    {
        ECDSA_SIG sig;
        sig.r = r;
        sig.s = s;

        if (ECDSA_do_verify((const unsigned char*)mmString_Data(&hash), mmString_Size(&hash), &sig, p->pkey) != 1)
        {
            mmLogger_LogE(gLogger, "%s %d signature verification failed, Invalid signature.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
        }
    }
#else
    {
        ECDSA_SIG* sig = ECDSA_SIG_new();

        ECDSA_SIG_set0(sig, r, s);

        if (ECDSA_do_verify((const unsigned char*)mmString_Data(&hash), (int)mmString_Size(&hash), sig, p->pkey) != 1)
        {
            mmLogger_LogE(gLogger, "%s %d signature verification failed, Invalid signature.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
        }

        ECDSA_SIG_free(sig);
    }
#endif

    mmString_Destroy(&hash);

    mmString_Destroy(&string_r);
    mmString_Destroy(&string_s);

    BN_free(r);
    BN_free(s);

    return matched;
}
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_Init(struct mmJwtAlgorithmEcdsa* p)
{
    mmJwtAlgorithmBase_Init(&p->base);
    p->pkey = NULL;

    p->base.Sign = &__static_jwt_sign_func_ecdsa;
    p->base.Verify = &__static_jwt_verify_func_ecdsa;
}
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_Destroy(struct mmJwtAlgorithmEcdsa* p)
{
    mmJwtAlgorithmBase_Destroy(&p->base);
    p->pkey = NULL;

    p->base.Sign = &__static_jwt_sign_func_ecdsa;
    p->base.Verify = &__static_jwt_verify_func_ecdsa;
}
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_SetKey(struct mmJwtAlgorithmEcdsa* p, struct mmString* public_key, struct mmString* private_key, struct mmString* public_key_password, struct mmString* private_key_password)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    if (mmString_Empty(private_key))
    {
        BIO* pubkey_bio = NULL;
        pubkey_bio = BIO_new(BIO_s_mem());

        do
        {
            const char* kd = mmString_Data(public_key);
            size_t kl = mmString_Size(public_key);
            if ((size_t)BIO_write(pubkey_bio, kd, (int)kl) != kl)
            {
                mmLogger_LogE(gLogger, "%s %d failed to load public key: bio_write failed.", __FUNCTION__, __LINE__);
                break;
            }

            p->pkey = PEM_read_bio_EC_PUBKEY(pubkey_bio, NULL, NULL, (void*)mmString_Data(public_key_password));

            if (!p->pkey)
            {
                mmLogger_LogE(gLogger, "%s %d failed to load public key: PEM_read_bio_EC_PUBKEY failed.", __FUNCTION__, __LINE__);
                break;
            }
        } while (0);

        BIO_free_all(pubkey_bio);
    }
    else
    {
        BIO* privkey_bio = NULL;
        privkey_bio = BIO_new(BIO_s_mem());

        do
        {
            const char* kd = mmString_Data(private_key);
            size_t kl = mmString_Size(private_key);
            if ((size_t)BIO_write(privkey_bio, kd, (int)kl) != kl)
            {
                mmLogger_LogE(gLogger, "%s %d failed to load private key: bio_write failed.", __FUNCTION__, __LINE__);
                break;
            }

            p->pkey = PEM_read_bio_ECPrivateKey(privkey_bio, NULL, NULL, (void*)mmString_Data(private_key_password));

            if (!p->pkey)
            {
                mmLogger_LogE(gLogger, "%s %d failed to load private key: PEM_read_bio_RSAPrivateKey failed.", __FUNCTION__, __LINE__);
                break;
            }
        } while (0);

        BIO_free_all(privkey_bio);
    }

    if (EC_KEY_check_key(p->pkey) == 0)
    {
        mmLogger_LogE(gLogger, "%s %d failed to load key: key is invalid.", __FUNCTION__, __LINE__);
    }
}
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_SetPublicKey(struct mmJwtAlgorithmEcdsa* p, struct mmString* public_key)
{
    struct mmString empty;
    mmString_MakeWeaks(&empty, "");

    mmJwtAlgorithmEcdsa_SetKey(p, public_key, &empty, &empty, &empty);
}

static void __static_jwt_sign_func_pss(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* sign)
{
    struct mmJwtAlgorithmPss* p = (struct mmJwtAlgorithmPss*)(impl);
    struct mmLogger* gLogger = mmLogger_Instance();

    RSA* key = NULL;
    int size = 0;

    struct mmString hash;
    struct mmString padded;

    mmString_Clear(sign);

    mmString_Init(&hash);
    mmString_Init(&padded);

    __static_generate_hash(&p->base, data, &hash);

    key = EVP_PKEY_get1_RSA(p->pkey);

    do
    {
        unsigned char* pd;
        unsigned char* sd;
        unsigned char* hd;

        size = RSA_size(key);

        mmString_Resize(&padded, size);
        mmMemset((void*)mmString_Data(&padded), 0x00, size);

        pd = (unsigned char*)mmString_Data(&padded);
        hd = (unsigned char*)mmString_Data(&hash);
        if (!RSA_padding_add_PKCS1_PSS_mgf1(key, pd, (const unsigned char*)hd, (*(p->base.md))(), (*(p->base.md))(), -1))
        {
            mmLogger_LogE(gLogger, "%s %d failed to create signature: RSA_padding_add_PKCS1_PSS_mgf1 failed.", __FUNCTION__, __LINE__);
            break;
        }

        mmString_Resize(sign, size);
        mmMemset((void*)mmString_Data(sign), 0x00, size);

        pd = (unsigned char*)mmString_Data(&padded);
        sd = (unsigned char*)mmString_Data(sign);
        if (RSA_private_encrypt(size, (const unsigned char*)pd, sd, key, RSA_NO_PADDING) < 0)
        {
            mmLogger_LogE(gLogger, "%s %d failed to create signature: RSA_private_encrypt failed.", __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    RSA_free(key);

    mmString_Destroy(&hash);
    mmString_Destroy(&padded);
}
static int __static_jwt_verify_func_pss(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* signature)
{
    struct mmJwtAlgorithmPss* p = (struct mmJwtAlgorithmPss*)(impl);
    struct mmLogger* gLogger = mmLogger_Instance();

    int matched = MM_TRUE;

    RSA* key = NULL;
    int size = 0;

    struct mmString hash;
    struct mmString sig;

    mmString_Init(&hash);
    mmString_Init(&sig);

    __static_generate_hash(&p->base, data, &hash);

    key = EVP_PKEY_get1_RSA(p->pkey);

    size = RSA_size(key);

    do
    {
        unsigned char* hd;
        unsigned char* sd;
        unsigned char* id;
        size_t sl;

        mmString_Resize(&sig, size);
        mmMemset((void*)mmString_Data(&sig), 0x00, size);

        sl = mmString_Size(signature);
        sd = (unsigned char*)mmString_Data(signature);
        id = (unsigned char*)mmString_Data(&sig);
        if (!RSA_public_decrypt((int)sl, (const unsigned char*)sd, id, key, RSA_NO_PADDING))
        {
            mmLogger_LogE(gLogger, "%s %d Invalid signature.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
            break;
        }

        hd = (unsigned char*)mmString_Data(&hash);
        id = (unsigned char*)mmString_Data(&sig);
        if (!RSA_verify_PKCS1_PSS_mgf1(key, (const unsigned char*)hd, (*(p->base.md))(), (*(p->base.md))(), (const unsigned char*)id, -1))
        {
            mmLogger_LogE(gLogger, "%s %d Invalid signature.", __FUNCTION__, __LINE__);
            matched = MM_FALSE;
            break;
        }

    } while (0);

    RSA_free(key);

    mmString_Destroy(&hash);
    mmString_Destroy(&sig);

    return matched;
}
MM_EXPORT_JWT void mmJwtAlgorithmPss_Init(struct mmJwtAlgorithmPss* p)
{
    mmJwtAlgorithmBase_Init(&p->base);
    p->pkey = NULL;

    p->base.Sign = &__static_jwt_sign_func_pss;
    p->base.Verify = &__static_jwt_verify_func_pss;
}
MM_EXPORT_JWT void mmJwtAlgorithmPss_Destroy(struct mmJwtAlgorithmPss* p)
{
    mmJwtAlgorithmBase_Destroy(&p->base);
    p->pkey = NULL;

    p->base.Sign = &__static_jwt_sign_func_pss;
    p->base.Verify = &__static_jwt_verify_func_pss;
}
MM_EXPORT_JWT void mmJwtAlgorithmPss_SetKey(struct mmJwtAlgorithmPss* p, struct mmString* public_key, struct mmString* private_key, struct mmString* public_key_password, struct mmString* private_key_password)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    BIO* pubkey_bio = NULL;

    pubkey_bio = BIO_new(BIO_s_mem());

    do
    {
        size_t kl = mmString_Size(public_key);
        const char* kd = mmString_Data(public_key);
        if ((size_t)BIO_write(pubkey_bio, kd, (int)kl) != kl)
        {
            mmLogger_LogE(gLogger, "%s %d failed to load public key: bio_write failed.", __FUNCTION__, __LINE__);
            break;
        }

        p->pkey = PEM_read_bio_PUBKEY(pubkey_bio, NULL, NULL, (void*)mmString_Data(public_key_password));

        if (!p->pkey)
        {
            mmLogger_LogE(gLogger, "%s %d failed to load public key: PEM_read_bio_PUBKEY failed.", __FUNCTION__, __LINE__);
            break;
        }

        if (!mmString_Empty(private_key))
        {
            RSA* privkey = NULL;

            BIO* privkey_bio = NULL;
            privkey_bio = BIO_new(BIO_s_mem());

            do
            {
                size_t kl = mmString_Size(private_key);
                const char* kd = mmString_Data(private_key);
                if ((size_t)BIO_write(privkey_bio, kd, (int)kl) != kl)
                {
                    mmLogger_LogE(gLogger, "%s %d failed to load private key: bio_write failed.", __FUNCTION__, __LINE__);
                    break;
                }
                privkey = PEM_read_bio_RSAPrivateKey(privkey_bio, NULL, NULL, (void*)mmString_Data(private_key_password));
                if (privkey == NULL)
                {
                    mmLogger_LogE(gLogger, "%s %d failed to load private key: PEM_read_bio_RSAPrivateKey failed.", __FUNCTION__, __LINE__);
                    break;
                }
                if (EVP_PKEY_assign_RSA(p->pkey, privkey) == 0)
                {
                    RSA_free(privkey);
                    mmLogger_LogE(gLogger, "%s %d failed to load private key: EVP_PKEY_assign_RSA failed.", __FUNCTION__, __LINE__);
                    break;
                }
            } while (0);

            BIO_free_all(privkey_bio);
        }

    } while (0);

    BIO_free_all(pubkey_bio);
}
MM_EXPORT_JWT void mmJwtAlgorithmPss_SetPublicKey(struct mmJwtAlgorithmPss* p, struct mmString* public_key)
{
    struct mmString empty;
    mmString_MakeWeaks(&empty, "");

    mmJwtAlgorithmPss_SetKey(p, public_key, &empty, &empty, &empty);
}
