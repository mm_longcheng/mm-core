/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmJwt_h__
#define __mmJwt_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "mmJwtAlgorithm.h"
#include "mmJwtExport.h"

#include "cJSON.h"

#include "core/mmPrefix.h"

// mmJwt is simple JSON Web Token (JWT) implement.
//
//
// rfc7515
// 4.  JWT Claims
// 
//    The JWT Claims Set represents a JSON object whose members are the
//    claims conveyed by the JWT.  The Claim Names within a JWT Claims Set
//    MUST be unique; JWT parsers MUST either reject JWTs with duplicate
//    Claim Names or use a JSON parser that returns only the lexically last
//    duplicate member name, as specified in Section 15.12 ("The JSON
//    Object") of ECMAScript 5.1 [ECMAScript].
// 
//    The set of claims that a JWT must contain to be considered valid is
//    context dependent and is outside the scope of this specification.
//    Specific applications of JWTs will require implementations to
//    understand and process some claims in particular ways.  However, in
//    the absence of such requirements, all claims that are not understood
//    by implementations MUST be ignored.
// 
//    There are three classes of JWT Claim Names: Registered Claim Names,
//    Public Claim Names, and Private Claim Names.
// 
//    Claim Name
//       The name portion of a claim representation.  A Claim Name is
//      always a string.
// 
//   Claim Value
//      The value portion of a claim representation.  A Claim Value can be
//      any JSON value.

// rfc7515 pg.10
// 4.1.1.  "alg" (Algorithm) Header Parameter
// 
//    The "alg" (algorithm) Header Parameter identifies the cryptographic
//    algorithm used to secure the JWS.  The JWS Signature value is not
//    valid if the "alg" value does not represent a supported algorithm or
//    if there is not a key for use with that algorithm associated with the
//    party that digitally signed or MACed the content.  "alg" values
//    should either be registered in the IANA "JSON Web Signature and
//    Encryption Algorithms" registry established by [JWA] or be a value
//    that contains a Collision-Resistant Name.  The "alg" value is a case-
//    sensitive ASCII string containing a StringOrURI value.  This Header
//    Parameter MUST be present and MUST be understood and processed by
//    implementations.
// 
//    A list of defined "alg" values for this use can be found in the IANA
//    "JSON Web Signature and Encryption Algorithms" registry established
//    by [JWA]; the initial contents of this registry are the values
//    defined in Section 3.1 of [JWA].
//
// rfc7519 pg.11
// 5.1.  "typ" (Type) Header Parameter
// 
//    The "typ" (type) Header Parameter defined by [JWS] and [JWE] is used
//    by JWT applications to declare the media type [IANA.MediaTypes] of
//    this complete JWT.  This is intended for use by the JWT application
//    when values that are not JWTs could also be present in an application
//    data structure that can contain a JWT object; the application can use
//    this value to disambiguate among the different kinds of objects that
//    might be present.  It will typically not be used by applications when
//    it is already known that the object is a JWT.  This parameter is
//    ignored by JWT implementations; any processing of this parameter is
//    performed by the JWT application.  If present, it is RECOMMENDED that
//    its value be "JWT" to indicate that this object is a JWT.  While
//    media type names are not case sensitive, it is RECOMMENDED that "JWT"
//    always be spelled using uppercase characters for compatibility with
//    legacy implementations.  Use of this Header Parameter is OPTIONAL.
// 
// rfc7519 pg.11
// 5.2.  "cty" (Content Type) Header Parameter
// 
//    The "cty" (content type) Header Parameter defined by [JWS] and [JWE]
//    is used by this specification to convey structural information about
//    the JWT.
// 
//    In the normal case in which nested signing or encryption operations
//    are not employed, the use of this Header Parameter is NOT
//    RECOMMENDED.  In the case that nested signing or encryption is
//    employed, this Header Parameter MUST be present; in this case, the
//    value MUST be "JWT", to indicate that a Nested JWT is carried in this
//    JWT.  While media type names are not case sensitive, it is
//    RECOMMENDED that "JWT" always be spelled using uppercase characters
//    for compatibility with legacy implementations.  See Appendix A.2 for
//    an example of a Nested JWT.
//
// rfc7515 pg.11
// 4.1.4.  "kid" (Key ID) Header Parameter
// 
//    The "kid" (key ID) Header Parameter is a hint indicating which key
//    was used to secure the JWS.  This parameter allows originators to
//    explicitly signal a change of key to recipients.  The structure of
//    the "kid" value is unspecified.  Its value MUST be a case-sensitive
//    string.  Use of this Header Parameter is OPTIONAL.
// 
//    When used with a JWK, the "kid" value is used to match a JWK "kid"
//    parameter value.
//

// rfc7519 pg.9
//
// 4.1.1.  "iss" (Issuer) Claim
// 
//    The "iss" (issuer) claim identifies the principal that issued the
//    JWT.  The processing of this claim is generally application specific.
//    The "iss" value is a case-sensitive string containing a StringOrURI
//    value.  Use of this claim is OPTIONAL.
// 
// 4.1.2.  "sub" (Subject) Claim
// 
//    The "sub" (subject) claim identifies the principal that is the
//    subject of the JWT.  The claims in a JWT are normally statements
//    about the subject.  The subject value MUST either be scoped to be
//    locally unique in the context of the issuer or be globally unique.
//    The processing of this claim is generally application specific.  The
//    "sub" value is a case-sensitive string containing a StringOrURI
//   value.  Use of this claim is OPTIONAL.
// 
// 4.1.3.  "aud" (Audience) Claim
// 
//    The "aud" (audience) claim identifies the recipients that the JWT is
//    intended for.  Each principal intended to process the JWT MUST
//    identify itself with a value in the audience claim.  If the principal
//    processing the claim does not identify itself with a value in the
//    "aud" claim when this claim is present, then the JWT MUST be
//    rejected.  In the general case, the "aud" value is an array of case-
//    sensitive strings, each containing a StringOrURI value.  In the
//    special case when the JWT has one audience, the "aud" value MAY be a
//    single case-sensitive string containing a StringOrURI value.  The
//    interpretation of audience values is generally application specific.
//    Use of this claim is OPTIONAL.
// 
// 4.1.4.  "exp" (Expiration Time) Claim
// 
//    The "exp" (expiration time) claim identifies the expiration time on
//    or after which the JWT MUST NOT be accepted for processing.  The
//    processing of the "exp" claim requires that the current date/time
//    MUST be before the expiration date/time listed in the "exp" claim.
//    Implementers MAY provide for some small leeway, usually no more than
//    a few minutes, to account for clock skew.  Its value MUST be a number
//    containing a NumericDate value.  Use of this claim is OPTIONAL.
// 
// 4.1.5.  "nbf" (Not Before) Claim
// 
//    The "nbf" (not before) claim identifies the time before which the JWT
//    MUST NOT be accepted for processing.  The processing of the "nbf"
//    claim requires that the current date/time MUST be after or equal to
//    the not-before date/time listed in the "nbf" claim.  Implementers MAY
//    provide for some small leeway, usually no more than a few minutes, to
//    account for clock skew.  Its value MUST be a number containing a
//    NumericDate value.  Use of this claim is OPTIONAL.
// 
// 4.1.6.  "iat" (Issued At) Claim
// 
//    The "iat" (issued at) claim identifies the time at which the JWT was
//    issued.  This claim can be used to determine the age of the JWT.  Its
//    value MUST be a number containing a NumericDate value.  Use of this
//    claim is OPTIONAL.
// 
// 4.1.7.  "jti" (JWT ID) Claim
// 
//    The "jti" (JWT ID) claim provides a unique identifier for the JWT.
//    The identifier value MUST be assigned in a manner that ensures that
//    there is a negligible probability that the same value will be
//    accidentally assigned to a different data object; if the application
//    uses multiple issuers, collisions MUST be prevented among values
//    produced by different issuers as well.  The "jti" claim can be used
//    to prevent the JWT from being replayed.  The "jti" value is a case-
//    sensitive string.  Use of this claim is OPTIONAL.
//

// mmJwt Claim now support
//   x is support 
//   o is not support
//================
//   x  object,
//   x  array,
//   o  string,
//   o  number(double),
//   o  boolean(true, false),
//   x  null,
//================

enum mmJwtClaimType_t
{
    MM_JWT_CLAIM_HEADER,
    MM_JWT_CLAIM_PAYLOAD,
    MM_JWT_CLAIM_MAX,
};

MM_EXPORT_JWT extern const char* MM_JWT_TYPE;

struct mmJwt
{
    cJSON* claims[MM_JWT_CLAIM_MAX];
    struct mmString header;
    struct mmString payload;
    struct mmString signature;
    struct mmString header_base64;
    struct mmString payload_base64;
    struct mmString signature_base64;
    struct mmString token_no_signature;
    struct mmString token;
    struct mmJwtAlgorithmBase* algorithm;
};

MM_EXPORT_JWT void mmJwt_Init(struct mmJwt* p);
MM_EXPORT_JWT void mmJwt_Destroy(struct mmJwt* p);

MM_EXPORT_JWT void mmJwt_SetAlgorithm(struct mmJwt* p, struct mmJwtAlgorithmBase* algorithm);
MM_EXPORT_JWT void mmJwt_SetHeader(struct mmJwt* p, const char* header);
MM_EXPORT_JWT void mmJwt_SetPayload(struct mmJwt* p, const char* payload);
MM_EXPORT_JWT void mmJwt_SetToken(struct mmJwt* p, const char* token);

MM_EXPORT_JWT void mmJwt_SetClaimsString(struct mmJwt* p, int t, const char* k, const char* v);
MM_EXPORT_JWT void mmJwt_SetClaimsDouble(struct mmJwt* p, int t, const char* k, double v);
MM_EXPORT_JWT void mmJwt_SetClaimsBoolean(struct mmJwt* p, int t, const char* k, int v);

MM_EXPORT_JWT const char* mmJwt_GetClaimsString(struct mmJwt* p, int t, const char* k);
MM_EXPORT_JWT double mmJwt_GetClaimsDouble(struct mmJwt* p, int t, const char* k);
MM_EXPORT_JWT int mmJwt_GetClaimsBoolean(struct mmJwt* p, int t, const char* k);

MM_EXPORT_JWT void mmJwt_ClaimsClear(struct mmJwt* p);

MM_EXPORT_JWT void mmJwt_Decode(struct mmJwt* p);
MM_EXPORT_JWT void mmJwt_Encode(struct mmJwt* p);

MM_EXPORT_JWT void mmJwt_Sign(struct mmJwt* p);
MM_EXPORT_JWT int mmJwt_Verify(struct mmJwt* p);

#include "core/mmSuffix.h"

#endif//__mmJwt_h__
