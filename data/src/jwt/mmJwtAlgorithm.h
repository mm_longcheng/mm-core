/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmJwtAlgorithm_h__
#define __mmJwtAlgorithm_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/pem.h>
#include <openssl/ec.h>
#include <openssl/err.h>

//If openssl version less than 1.1
#if OPENSSL_VERSION_NUMBER < 269484032
#define MM_JWT_OPENSSL10
#endif

#include "mmJwtExport.h"

#include "core/mmPrefix.h"

enum mmJwtAlgorithmType_t
{
    JWT_AT_NONE = 0,

    JWT_AT_HS256,
    JWT_AT_HS384,
    JWT_AT_HS512,

    JWT_AT_RS256,
    JWT_AT_RS384,
    JWT_AT_RS512,

    JWT_AT_ES256,
    JWT_AT_ES384,
    JWT_AT_ES512,

    JWT_AT_PS256,
    JWT_AT_PS384,
    JWT_AT_PS512,

    JWT_AT_MAX,
};

typedef const EVP_MD*(*mmJwtEvpMdFunc)(void);

struct mmJwtAlgorithmImpl
{
    // func
    mmJwtEvpMdFunc c;
    // name
    struct mmString n;
};

MM_EXPORT_JWT const struct mmJwtAlgorithmImpl* mmJwtAlgorithmImpl(int type);

struct mmJwtAlgorithmBase
{
    struct mmString name;
    mmJwtEvpMdFunc md;

    void(*Sign)(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* sign);
    int(*Verify)(struct mmJwtAlgorithmBase* impl, struct mmString* data, struct mmString* signature);
};
MM_EXPORT_JWT void mmJwtAlgorithmBase_Init(struct mmJwtAlgorithmBase* p);
MM_EXPORT_JWT void mmJwtAlgorithmBase_Destroy(struct mmJwtAlgorithmBase* p);
MM_EXPORT_JWT void mmJwtAlgorithmBase_SetMd(struct mmJwtAlgorithmBase* p, mmJwtEvpMdFunc md);
MM_EXPORT_JWT void mmJwtAlgorithmBase_SetName(struct mmJwtAlgorithmBase* p, const char* name);
// by type mmJwtAlgorithmType_t.
MM_EXPORT_JWT void mmJwtAlgorithmBase_SetType(struct mmJwtAlgorithmBase* p, int type);

struct mmJwtAlgorithmHmacsha
{
    struct mmJwtAlgorithmBase base;
    struct mmString secret;
};
MM_EXPORT_JWT void mmJwtAlgorithmHmacsha_Init(struct mmJwtAlgorithmHmacsha* p);
MM_EXPORT_JWT void mmJwtAlgorithmHmacsha_Destroy(struct mmJwtAlgorithmHmacsha* p);
MM_EXPORT_JWT void mmJwtAlgorithmHmacsha_SetSecret(struct mmJwtAlgorithmHmacsha* p, struct mmString* secret);

struct mmJwtAlgorithmRsa
{
    struct mmJwtAlgorithmBase base;
    EVP_PKEY* pkey;
};
MM_EXPORT_JWT void mmJwtAlgorithmRsa_Init(struct mmJwtAlgorithmRsa* p);
MM_EXPORT_JWT void mmJwtAlgorithmRsa_Destroy(struct mmJwtAlgorithmRsa* p);
MM_EXPORT_JWT void mmJwtAlgorithmRsa_SetKey(struct mmJwtAlgorithmRsa* p, struct mmString* public_key, struct mmString* private_key, struct mmString* public_key_password, struct mmString* private_key_password);
MM_EXPORT_JWT void mmJwtAlgorithmRsa_SetPublicKey(struct mmJwtAlgorithmRsa* p, struct mmString* public_key);

struct mmJwtAlgorithmEcdsa
{
    struct mmJwtAlgorithmBase base;
    EC_KEY* pkey;
};
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_Init(struct mmJwtAlgorithmEcdsa* p);
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_Destroy(struct mmJwtAlgorithmEcdsa* p);
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_SetKey(struct mmJwtAlgorithmEcdsa* p, struct mmString* public_key, struct mmString* private_key, struct mmString* public_key_password, struct mmString* private_key_password);
MM_EXPORT_JWT void mmJwtAlgorithmEcdsa_SetPublicKey(struct mmJwtAlgorithmEcdsa* p, struct mmString* public_key);

struct mmJwtAlgorithmPss
{
    struct mmJwtAlgorithmBase base;
    EVP_PKEY* pkey;
};
MM_EXPORT_JWT void mmJwtAlgorithmPss_Init(struct mmJwtAlgorithmPss* p);
MM_EXPORT_JWT void mmJwtAlgorithmPss_Destroy(struct mmJwtAlgorithmPss* p);
MM_EXPORT_JWT void mmJwtAlgorithmPss_SetKey(struct mmJwtAlgorithmPss* p, struct mmString* public_key, struct mmString* private_key, struct mmString* public_key_password, struct mmString* private_key_password);
MM_EXPORT_JWT void mmJwtAlgorithmPss_SetPublicKey(struct mmJwtAlgorithmPss* p, struct mmString* public_key);

#include "core/mmSuffix.h"

#endif//__mmJwtAlgorithm_h__
