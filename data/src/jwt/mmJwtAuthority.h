/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmJwtAuthority_h__
#define __mmJwtAuthority_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmListVpt.h"

#include "random/mmXoshiro.h"

#include "mmJwtAlgorithm.h"
#include "mmJwt.h"

#include "mmJwtExport.h"

#include <pthread.h>

#include "core/mmPrefix.h"

#define MM_JWT_AUTHORITY_SECRET_LENGTH_DEFAULT 16

// use for simple uid(u64) jwt authority.
// note:
//    header  {"typ":"JWT","alg":"HS256"}
//    payload {"sub":"9008000000005999","iat":"1543657888","jti":"123456"}

struct mmJwtAuthorityData
{
    // user id.
    mmUInt64_t sub;
    // unix timecode ms.
    mmUInt64_t iat;
    // random number.
    mmUInt64_t jti;
};
MM_EXPORT_JWT void mmJwtAuthorityData_Init(struct mmJwtAuthorityData* p);
MM_EXPORT_JWT void mmJwtAuthorityData_Destroy(struct mmJwtAuthorityData* p);
MM_EXPORT_JWT void mmJwtAuthorityData_Reset(struct mmJwtAuthorityData* p);

struct mmJwtAuthority
{
    struct mmXoshiro256starstar token_random;
    struct mmJwtAlgorithmHmacsha jwt_algorithm;
    struct mmString jwt_secret;
    struct mmJwt jwt;
    mmAtomic_t locker;
    mmUInt32_t token_seed;
    mmUInt32_t secret_length;
};
MM_EXPORT_JWT void mmJwtAuthority_Init(struct mmJwtAuthority* p);
MM_EXPORT_JWT void mmJwtAuthority_Destroy(struct mmJwtAuthority* p);

MM_EXPORT_JWT void mmJwtAuthority_Lock(struct mmJwtAuthority* p);
MM_EXPORT_JWT void mmJwtAuthority_Unlock(struct mmJwtAuthority* p);

MM_EXPORT_JWT void mmJwtAuthority_SetTokenSeed(struct mmJwtAuthority* p, mmUInt32_t token_seed);
MM_EXPORT_JWT void mmJwtAuthority_SetSecretLength(struct mmJwtAuthority* p, mmUInt32_t secret_length);

// random secret.
MM_EXPORT_JWT void mmJwtAuthority_GenerateSecret(struct mmJwtAuthority* p);

MM_EXPORT_JWT void mmJwtAuthority_Sign(struct mmJwtAuthority* p, struct mmJwtAuthorityData* data, struct mmString* token);
MM_EXPORT_JWT int mmJwtAuthority_Verify(struct mmJwtAuthority* p, struct mmJwtAuthorityData* data, struct mmString* token);

struct mmJwtAuthorityArray
{
    // strong ref.
    struct mmListHead list;
    // thread key.
    pthread_key_t thread_key;
    mmAtomic_t list_locker;

    mmUInt32_t token_seed;
    mmUInt32_t secret_length;
};

MM_EXPORT_JWT void mmJwtAuthorityArray_Init(struct mmJwtAuthorityArray* p);
MM_EXPORT_JWT void mmJwtAuthorityArray_Destroy(struct mmJwtAuthorityArray* p);

// recommend set length before thread instance.
MM_EXPORT_JWT struct mmJwtAuthority* mmJwtAuthorityArray_ThreadInstance(struct mmJwtAuthorityArray* p);
MM_EXPORT_JWT void mmJwtAuthorityArray_Clear(struct mmJwtAuthorityArray* p);

// recommend call this before thread instance.
MM_EXPORT_JWT void mmJwtAuthorityArray_SetTokenSeed(struct mmJwtAuthorityArray* p, mmUInt32_t token_seed);
MM_EXPORT_JWT void mmJwtAuthorityArray_SetSecretLength(struct mmJwtAuthorityArray* p, mmUInt32_t secret_length);

MM_EXPORT_JWT void mmJwtAuthorityArray_GenerateSecret(struct mmJwtAuthorityArray* p);

MM_EXPORT_JWT void mmJwtAuthorityArray_Sign(struct mmJwtAuthorityArray* p, struct mmJwtAuthorityData* data, struct mmString* token);
MM_EXPORT_JWT int mmJwtAuthorityArray_Verify(struct mmJwtAuthorityArray* p, struct mmJwtAuthorityData* data, struct mmString* token);

#include "core/mmSuffix.h"

#endif//__mmJwtAuthority_h__
