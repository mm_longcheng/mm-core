/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmJwt.h"
#include "mmJwtAlgorithm.h"
#include "mmJwtBase64.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmValueTransform.h"

MM_EXPORT_JWT const char* MM_JWT_TYPE = "JWT";

MM_EXPORT_JWT void mmJwt_Init(struct mmJwt* p)
{
    mmMemset(p->claims, 0, sizeof(cJSON*) * MM_JWT_CLAIM_MAX);
    mmString_Init(&p->header);
    mmString_Init(&p->payload);
    mmString_Init(&p->signature);
    mmString_Init(&p->header_base64);
    mmString_Init(&p->payload_base64);
    mmString_Init(&p->signature_base64);
    mmString_Init(&p->token_no_signature);
    mmString_Init(&p->token);
    p->algorithm = NULL;

    p->claims[MM_JWT_CLAIM_HEADER] = cJSON_CreateObject();
    p->claims[MM_JWT_CLAIM_PAYLOAD] = cJSON_CreateObject();

    mmJwt_SetClaimsString(p, MM_JWT_CLAIM_HEADER, "typ", MM_JWT_TYPE);
}
MM_EXPORT_JWT void mmJwt_Destroy(struct mmJwt* p)
{
    mmJwt_ClaimsClear(p);

    mmMemset(p->claims, 0, sizeof(cJSON*) * MM_JWT_CLAIM_MAX);
    mmString_Destroy(&p->header);
    mmString_Destroy(&p->payload);
    mmString_Destroy(&p->signature);
    mmString_Destroy(&p->header_base64);
    mmString_Destroy(&p->payload_base64);
    mmString_Destroy(&p->signature_base64);
    mmString_Destroy(&p->token_no_signature);
    mmString_Destroy(&p->token);
    p->algorithm = NULL;
}

MM_EXPORT_JWT void mmJwt_SetAlgorithm(struct mmJwt* p, struct mmJwtAlgorithmBase* algorithm)
{
    assert(NULL != algorithm && "algorithm is invalid.");

    p->algorithm = algorithm;

    mmJwt_SetClaimsString(p, MM_JWT_CLAIM_HEADER, "alg", mmString_CStr(&p->algorithm->name));
}
MM_EXPORT_JWT void mmJwt_SetHeader(struct mmJwt* p, const char* header)
{
    mmString_Assigns(&p->header, header);
}
MM_EXPORT_JWT void mmJwt_SetPayload(struct mmJwt* p, const char* payload)
{
    mmString_Assigns(&p->payload, payload);
}
MM_EXPORT_JWT void mmJwt_SetToken(struct mmJwt* p, const char* token)
{
    size_t ln = 0;
    size_t fn = 0;

    mmString_Assigns(&p->token, token);

    ln = mmString_FindLastOf(&p->token, '.');
    fn = mmString_FindFirstOf(&p->token, '.');

    if (mmStringNpos != ln && mmStringNpos != fn)
    {
        if (ln != fn)
        {
            mmString_Substr(&p->token, &p->header_base64, 0, fn);
            mmString_Substr(&p->token, &p->payload_base64, fn + 1, ln - fn - 1);
            mmString_Substr(&p->token, &p->signature_base64, ln + 1, mmString_Size(&p->token) - ln);
        }
        else
        {
            mmString_Substr(&p->token, &p->header_base64, 0, fn);
            mmString_Substr(&p->token, &p->payload_base64, fn + 1, mmString_Size(&p->token) - fn);
            mmString_Clear(&p->signature_base64);
        }
    }
}

MM_EXPORT_JWT void mmJwt_SetClaimsString(struct mmJwt* p, int t, const char* k, const char* v)
{
    cJSON* item = NULL;
    assert(0 <= t && t < MM_JWT_CLAIM_MAX && "claim type is invalid.");
    item = cJSON_GetObjectItem(p->claims[t], k);
    if (NULL == item)
    {
        cJSON_AddItemToObject(p->claims[t], k, cJSON_CreateString(v));
    }
    else
    {
        cJSON_ReplaceItemInObject(p->claims[t], k, cJSON_CreateString(v));
    }
}
MM_EXPORT_JWT void mmJwt_SetClaimsDouble(struct mmJwt* p, int t, const char* k, double v)
{
    cJSON* item = NULL;
    assert(0 <= t && t < MM_JWT_CLAIM_MAX && "claim type is invalid.");
    item = cJSON_GetObjectItem(p->claims[t], k);
    if (NULL == item)
    {
        cJSON_AddItemToObject(p->claims[t], k, cJSON_CreateNumber(v));
    }
    else
    {
        cJSON_ReplaceItemInObject(p->claims[t], k, cJSON_CreateNumber(v));
    }
}

MM_EXPORT_JWT void mmJwt_SetClaimsBoolean(struct mmJwt* p, int t, const char* k, int v)
{
    cJSON* item = NULL;
    assert(0 <= t && t < MM_JWT_CLAIM_MAX && "claim type is invalid.");
    item = cJSON_GetObjectItem(p->claims[t], k);
    if (NULL == item)
    {
        cJSON_AddItemToObject(p->claims[t], k, cJSON_CreateBool(v));
    }
    else
    {
        cJSON_ReplaceItemInObject(p->claims[t], k, cJSON_CreateBool(v));
    }
}
//
MM_EXPORT_JWT const char* mmJwt_GetClaimsString(struct mmJwt* p, int t, const char* k)
{
    cJSON* item = NULL;
    assert(0 <= t && t < MM_JWT_CLAIM_MAX && "claim type is invalid.");
    item = cJSON_GetObjectItem(p->claims[t], k);
    return NULL == item ? "" : item->valuestring;
}
MM_EXPORT_JWT double mmJwt_GetClaimsDouble(struct mmJwt* p, int t, const char* k)
{
    cJSON* item = NULL;
    assert(0 <= t && t < MM_JWT_CLAIM_MAX && "claim type is invalid.");
    item = cJSON_GetObjectItem(p->claims[t], k);
    return NULL == item ? 0 : item->valuedouble;
}

MM_EXPORT_JWT int mmJwt_GetClaimsBoolean(struct mmJwt* p, int t, const char* k)
{
    cJSON* item = NULL;
    assert(0 <= t && t < MM_JWT_CLAIM_MAX && "claim type is invalid.");
    item = cJSON_GetObjectItem(p->claims[t], k);
    return NULL == item ? MM_FALSE : (cJSON_True == item->type ? MM_TRUE : MM_FALSE);
}

MM_EXPORT_JWT void mmJwt_ClaimsClear(struct mmJwt* p)
{
    cJSON_Delete(p->claims[MM_JWT_CLAIM_HEADER]);
    cJSON_Delete(p->claims[MM_JWT_CLAIM_PAYLOAD]);
    mmMemset(p->claims, 0, sizeof(cJSON*) * MM_JWT_CLAIM_MAX);
}

MM_EXPORT_JWT void mmJwt_Decode(struct mmJwt* p)
{
    size_t u_decode_length = 0;
    size_t u_length = 0;
    const char* u_buffer = NULL;

    mmJwt_ClaimsClear(p);
    // base64 json string ==> json string.
    u_length = mmString_Size(&p->header_base64);
    u_buffer = mmString_CStr(&p->header_base64);
    u_decode_length = mmJwtBase64_DecodeLength(u_length);
    mmString_Resize(&p->header, u_decode_length);
    u_decode_length = mmJwtBase64_UDecode((char*)mmString_CStr(&p->header), u_buffer, u_length);
    mmString_Resize(&p->header, u_decode_length);
    //
    u_length = mmString_Size(&p->payload_base64);
    u_buffer = mmString_CStr(&p->payload_base64);
    u_decode_length = mmJwtBase64_DecodeLength(u_length);
    mmString_Resize(&p->payload, u_decode_length);
    u_decode_length = mmJwtBase64_UDecode((char*)mmString_CStr(&p->payload), u_buffer, u_length);
    mmString_Resize(&p->header, u_decode_length);
    //
    u_length = mmString_Size(&p->signature_base64);
    u_buffer = mmString_CStr(&p->signature_base64);
    u_decode_length = mmJwtBase64_DecodeLength(u_length);
    mmString_Resize(&p->signature, u_decode_length);
    u_decode_length = mmJwtBase64_UDecode((char*)mmString_CStr(&p->signature), u_buffer, u_length);
    mmString_Resize(&p->signature, u_decode_length);

    // json string ==> json object.
    p->claims[MM_JWT_CLAIM_HEADER] = cJSON_Parse(mmString_CStr(&p->header));
    p->claims[MM_JWT_CLAIM_PAYLOAD] = cJSON_Parse(mmString_CStr(&p->payload));
}
MM_EXPORT_JWT void mmJwt_Encode(struct mmJwt* p)
{
    size_t u_encode_length = 0;
    size_t u_length = 0;
    const char* u_buffer = NULL;
    // json object ==> json string.
    char* h_string = cJSON_Print(p->claims[MM_JWT_CLAIM_HEADER]);
    char* p_string = cJSON_Print(p->claims[MM_JWT_CLAIM_PAYLOAD]);
    cJSON_Minify(h_string);
    cJSON_Minify(p_string);
    mmString_Assigns(&p->header, h_string);
    mmString_Assigns(&p->payload, p_string);
    mmFree(h_string);
    mmFree(p_string);

    // json string ==> base64 json string.
    u_length = mmString_Size(&p->header);
    u_buffer = mmString_CStr(&p->header);
    u_encode_length = mmJwtBase64_EncodeLength(u_length);
    mmString_Resize(&p->header_base64, u_encode_length);
    mmJwtBase64_UEncode((char*)mmString_CStr(&p->header_base64), u_buffer, u_length);
    //
    u_length = mmString_Size(&p->payload);
    u_buffer = mmString_CStr(&p->payload);
    u_encode_length = mmJwtBase64_EncodeLength(u_length);
    mmString_Resize(&p->payload_base64, u_encode_length);
    mmJwtBase64_UEncode((char*)mmString_CStr(&p->payload_base64), u_buffer, u_length);
}
MM_EXPORT_JWT void mmJwt_Sign(struct mmJwt* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    const char* alg = mmJwt_GetClaimsString(p, MM_JWT_CLAIM_HEADER, "alg");

    assert(NULL != p->algorithm && "NULL != p->algorithm is invalid");

    do
    {
        size_t u_encode_length = 0;
        size_t u_length = 0;
        const char* u_buffer = NULL;

        if (mmString_CompareCStr(&p->algorithm->name, alg))
        {
            mmLogger_LogE(gLogger, "%s %d algorithm is not match.", __FUNCTION__, __LINE__);
            break;
        }
        mmString_Clear(&p->token_no_signature);

        mmString_Append(&p->token_no_signature, &p->header_base64);
        mmString_Appends(&p->token_no_signature, ".");
        mmString_Append(&p->token_no_signature, &p->payload_base64);

        mmString_Clear(&p->token);

        mmString_Append(&p->token, &p->token_no_signature);

        (*(p->algorithm->Sign))(p->algorithm, &p->token_no_signature, &p->signature);

        u_length = mmString_Size(&p->signature);
        u_buffer = mmString_CStr(&p->signature);
        u_encode_length = mmJwtBase64_EncodeLength(u_length);
        mmString_Resize(&p->signature_base64, u_encode_length);
        mmJwtBase64_UEncode((char*)mmString_CStr(&p->signature_base64), u_buffer, u_length);

        mmString_Appends(&p->token, ".");
        mmString_Append(&p->token, &p->signature_base64);
    } while (0);
}
MM_EXPORT_JWT int mmJwt_Verify(struct mmJwt* p)
{
    size_t u_encode_length = 0;
    size_t u_length = 0;
    const char* u_buffer = NULL;

    assert(NULL != p->algorithm && "NULL != p->algorithm is invalid");

    mmString_Clear(&p->token_no_signature);

    mmString_Append(&p->token_no_signature, &p->header_base64);
    mmString_Appends(&p->token_no_signature, ".");
    mmString_Append(&p->token_no_signature, &p->payload_base64);

    u_length = mmString_Size(&p->signature);
    u_buffer = mmString_CStr(&p->signature);
    u_encode_length = mmJwtBase64_EncodeLength(u_length);
    mmString_Resize(&p->signature_base64, u_encode_length);
    mmJwtBase64_UEncode((char*)mmString_CStr(&p->signature_base64), u_buffer, u_length);

    return (*(p->algorithm->Verify))(p->algorithm, &p->token_no_signature, &p->signature);
}
