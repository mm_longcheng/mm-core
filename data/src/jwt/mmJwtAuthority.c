/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmJwtAuthority.h"

#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"

#include "dish/mmXoshiroString.h"

MM_EXPORT_JWT void mmJwtAuthorityData_Init(struct mmJwtAuthorityData* p)
{
    p->sub = 0;
    p->iat = 0;
    p->jti = 0;
}
MM_EXPORT_JWT void mmJwtAuthorityData_Destroy(struct mmJwtAuthorityData* p)
{
    p->sub = 0;
    p->iat = 0;
    p->jti = 0;
}
MM_EXPORT_JWT void mmJwtAuthorityData_Reset(struct mmJwtAuthorityData* p)
{
    p->sub = 0;
    p->iat = 0;
    p->jti = 0;
}

MM_EXPORT_JWT void mmJwtAuthority_Init(struct mmJwtAuthority* p)
{
    mmXoshiro256starstar_Init(&p->token_random);
    mmJwtAlgorithmHmacsha_Init(&p->jwt_algorithm);
    mmString_Init(&p->jwt_secret);
    mmJwt_Init(&p->jwt);
    mmSpinlock_Init(&p->locker, NULL);
    p->token_seed = 0;
    p->secret_length = MM_JWT_AUTHORITY_SECRET_LENGTH_DEFAULT;
    //
    mmJwtAlgorithmBase_SetType(&p->jwt_algorithm.base, JWT_AT_HS256);
    //
    mmJwt_SetAlgorithm(&p->jwt, &p->jwt_algorithm.base);
}
MM_EXPORT_JWT void mmJwtAuthority_Destroy(struct mmJwtAuthority* p)
{
    mmXoshiro256starstar_Destroy(&p->token_random);
    mmJwtAlgorithmHmacsha_Destroy(&p->jwt_algorithm);
    mmString_Destroy(&p->jwt_secret);
    mmJwt_Destroy(&p->jwt);
    mmSpinlock_Destroy(&p->locker);
    p->token_seed = 0;
    p->secret_length = MM_JWT_AUTHORITY_SECRET_LENGTH_DEFAULT;
}

MM_EXPORT_JWT void mmJwtAuthority_Lock(struct mmJwtAuthority* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_JWT void mmJwtAuthority_Unlock(struct mmJwtAuthority* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_JWT void mmJwtAuthority_SetTokenSeed(struct mmJwtAuthority* p, mmUInt32_t token_seed)
{
    p->token_seed = token_seed;
}
MM_EXPORT_JWT void mmJwtAuthority_SetSecretLength(struct mmJwtAuthority* p, mmUInt32_t secret_length)
{
    p->secret_length = secret_length;
}

MM_EXPORT_JWT void mmJwtAuthority_GenerateSecret(struct mmJwtAuthority* p)
{
    // note:
    // secret need readable string, here we need append '\0'.
    // random token buffer.
    char* d = NULL;
    mmString_Resize(&p->jwt_secret, p->secret_length + 1);
    d = (char*)mmString_Data(&p->jwt_secret);
    d[p->secret_length] = '\0';
    //
    mmXoshiro256starstar_Srand(&p->token_random, p->token_seed);
    mmXoshiro256starstar_RandomBuffer(&p->token_random, (unsigned char*)d, 0, (size_t)p->secret_length);
    // jwt
    mmJwtAlgorithmHmacsha_SetSecret(&p->jwt_algorithm, &p->jwt_secret);
}
MM_EXPORT_JWT void mmJwtAuthority_Sign(struct mmJwtAuthority* p, struct mmJwtAuthorityData* data, struct mmString* token)
{
    char sub_string[32] = { 0 };
    char iat_string[32] = { 0 };
    char jti_string[32] = { 0 };

    mmValue_UInt64ToA(&data->sub, sub_string);
    mmValue_UInt64ToA(&data->iat, iat_string);
    mmValue_UInt64ToA(&data->jti, jti_string);
    //
    mmJwt_SetClaimsString(&p->jwt, MM_JWT_CLAIM_PAYLOAD, "sub", sub_string);
    mmJwt_SetClaimsString(&p->jwt, MM_JWT_CLAIM_PAYLOAD, "iat", iat_string);
    mmJwt_SetClaimsString(&p->jwt, MM_JWT_CLAIM_PAYLOAD, "jti", jti_string);
    //
    mmJwt_Encode(&p->jwt);
    mmJwt_Sign(&p->jwt);
    //
    mmString_Assign(token, &p->jwt.token);
}
MM_EXPORT_JWT int mmJwtAuthority_Verify(struct mmJwtAuthority* p, struct mmJwtAuthorityData* data, struct mmString* token)
{
    mmJwt_SetToken(&p->jwt, mmString_CStr(token));

    mmJwt_Decode(&p->jwt);

    mmValue_AToUInt64(mmJwt_GetClaimsString(&p->jwt, MM_JWT_CLAIM_PAYLOAD, "sub"), &data->sub);
    mmValue_AToUInt64(mmJwt_GetClaimsString(&p->jwt, MM_JWT_CLAIM_PAYLOAD, "iat"), &data->iat);
    mmValue_AToUInt64(mmJwt_GetClaimsString(&p->jwt, MM_JWT_CLAIM_PAYLOAD, "jti"), &data->jti);

    return mmJwt_Verify(&p->jwt);
}

MM_EXPORT_JWT void mmJwtAuthorityArray_Init(struct mmJwtAuthorityArray* p)
{
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_create(&p->thread_key, NULL);
    mmSpinlock_Init(&p->list_locker, NULL);

    p->token_seed = 0;
    p->secret_length = MM_JWT_AUTHORITY_SECRET_LENGTH_DEFAULT;
}
MM_EXPORT_JWT void mmJwtAuthorityArray_Destroy(struct mmJwtAuthorityArray* p)
{
    mmJwtAuthorityArray_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_delete(p->thread_key);
    mmSpinlock_Destroy(&p->list_locker);

    p->token_seed = 0;
    p->secret_length = MM_JWT_AUTHORITY_SECRET_LENGTH_DEFAULT;
}

// recommend set length before thread instance.
MM_EXPORT_JWT struct mmJwtAuthority* mmJwtAuthorityArray_ThreadInstance(struct mmJwtAuthorityArray* p)
{
    struct mmJwtAuthority* e = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (!lvp)
    {
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        e = (struct mmJwtAuthority*)mmMalloc(sizeof(struct mmJwtAuthority));
        mmJwtAuthority_Init(e);
        //
        mmJwtAuthority_SetTokenSeed(e, p->token_seed);
        mmJwtAuthority_SetSecretLength(e, p->secret_length);
        //
        mmJwtAuthority_GenerateSecret(e);
        //
        lvp->v = e;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        e = (struct mmJwtAuthority*)(lvp->v);
    }
    return e;
}
MM_EXPORT_JWT void mmJwtAuthorityArray_Clear(struct mmJwtAuthorityArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmJwtAuthority* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmJwtAuthority*)(lvp->v);
        mmJwtAuthority_Lock(e);
        mmList_Del(curr);
        mmJwtAuthority_Unlock(e);
        mmJwtAuthority_Destroy(e);
        mmFree(e);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

// recommend call this before thread instance.
MM_EXPORT_JWT void mmJwtAuthorityArray_SetTokenSeed(struct mmJwtAuthorityArray* p, mmUInt32_t token_seed)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmJwtAuthority* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->token_seed = token_seed;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmJwtAuthority*)(lvp->v);
        mmJwtAuthority_Lock(e);
        mmJwtAuthority_SetTokenSeed(e, p->token_seed);
        mmJwtAuthority_Unlock(e);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_JWT void mmJwtAuthorityArray_SetSecretLength(struct mmJwtAuthorityArray* p, mmUInt32_t secret_length)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmJwtAuthority* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->secret_length = secret_length;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmJwtAuthority*)(lvp->v);
        mmJwtAuthority_Lock(e);
        mmJwtAuthority_SetSecretLength(e, p->secret_length);
        mmJwtAuthority_Unlock(e);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_JWT void mmJwtAuthorityArray_GenerateSecret(struct mmJwtAuthorityArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmJwtAuthority* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmJwtAuthority*)(lvp->v);
        mmJwtAuthority_Lock(e);
        mmJwtAuthority_GenerateSecret(e);
        mmJwtAuthority_Unlock(e);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_JWT void mmJwtAuthorityArray_Sign(struct mmJwtAuthorityArray* p, struct mmJwtAuthorityData* data, struct mmString* token)
{
    struct mmJwtAuthority* e = mmJwtAuthorityArray_ThreadInstance(p);
    mmJwtAuthority_Lock(e);
    mmJwtAuthority_Sign(e, data, token);
    mmJwtAuthority_Unlock(e);
}
MM_EXPORT_JWT int mmJwtAuthorityArray_Verify(struct mmJwtAuthorityArray* p, struct mmJwtAuthorityData* data, struct mmString* token)
{
    int v = MM_FALSE;
    struct mmJwtAuthority* e = mmJwtAuthorityArray_ThreadInstance(p);
    mmJwtAuthority_Lock(e);
    v = mmJwtAuthority_Verify(e, data, token);
    mmJwtAuthority_Unlock(e);
    return v;
}

