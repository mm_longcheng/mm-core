/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDbMysql.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmLogger.h"
#include "core/mmAtoi.h"

#include "container/mmListVpt.h"

MM_EXPORT_MYSQL void mmDbMysqlConfig_Init(struct mmDbMysqlConfig* p)
{
    mmString_Init(&p->node);
    mmString_Init(&p->username);
    mmString_Init(&p->password);
    mmString_Init(&p->basename);
    mmString_Init(&p->charsets);
    p->port = 3306;
    //
    mmString_Assigns(&p->node, "127.0.0.1");
    mmString_Assigns(&p->username, "mm");
    mmString_Assigns(&p->password, "xxxxxx");
    mmString_Assigns(&p->basename, "mm");
    mmString_Assigns(&p->charsets, "utf8");
}
MM_EXPORT_MYSQL void mmDbMysqlConfig_Destroy(struct mmDbMysqlConfig* p)
{
    mmString_Destroy(&p->node);
    mmString_Destroy(&p->username);
    mmString_Destroy(&p->password);
    mmString_Destroy(&p->basename);
    mmString_Destroy(&p->charsets);
    p->port = 0;
}

// 0 is same.
MM_EXPORT_MYSQL int mmDbMysqlConfig_Compare(struct mmDbMysqlConfig* p, struct mmDbMysqlConfig* t)
{
    if (0 == mmString_Compare(&p->node, &t->node) &&
        0 == mmString_Compare(&p->username, &t->username) &&
        0 == mmString_Compare(&p->password, &t->password) &&
        0 == mmString_Compare(&p->basename, &t->basename) &&
        0 == mmString_Compare(&p->charsets, &t->charsets) &&
        p->port == t->port)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}
// assign.
MM_EXPORT_MYSQL void mmDbMysqlConfig_Assign(struct mmDbMysqlConfig* p, struct mmDbMysqlConfig* t)
{
    mmString_Assign(&p->node, &t->node);
    mmString_Assign(&p->username, &t->username);
    mmString_Assign(&p->password, &t->password);
    mmString_Assign(&p->basename, &t->basename);
    mmString_Assign(&p->charsets, &t->charsets);
    p->port = t->port;
}

MM_EXPORT_MYSQL void mmDbMysql_Init(struct mmDbMysql* p)
{
    mmDbMysqlConfig_Init(&p->config);
    mmSpinlock_Init(&p->locker, NULL);
    mysql_init(&p->context);
    p->row = NULL;
    p->result = NULL;
    p->trytimes = MM_DB_MYSQL_TRYTIMES;
    p->context_state = MM_DB_MYSQL_STATE_UNKNOWN;
    p->field_length = NULL;
}
MM_EXPORT_MYSQL void mmDbMysql_Destroy(struct mmDbMysql* p)
{
    mmDbMysql_FreeUnusedResult(p);
    mmDbMysql_FreeCurrentResult(p);
    //
    mmDbMysqlConfig_Destroy(&p->config);
    mmSpinlock_Destroy(&p->locker);
    mysql_close(&p->context);
    p->row = NULL;
    p->result = NULL;
    p->trytimes = MM_DB_MYSQL_TRYTIMES;
    p->context_state = MM_DB_MYSQL_STATE_UNKNOWN;
    p->field_length = NULL;
}

MM_EXPORT_MYSQL void mmDbMysql_Lock(struct mmDbMysql* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_MYSQL void mmDbMysql_Unlock(struct mmDbMysql* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_MYSQL void mmDbMysql_SetConfig(struct mmDbMysql* p, struct mmDbMysqlConfig* config)
{
    assert(NULL != config && "you can not assign null config.");
    mmDbMysqlConfig_Assign(&p->config, config);
}
MM_EXPORT_MYSQL struct mmDbMysqlConfig* mmDbMysql_GetConfig(struct mmDbMysql* p)
{
    return &p->config;
}
// try connect times.default is MM_DB_MYSQL_TRYTIMES.
MM_EXPORT_MYSQL void mmDbMysql_SetTrytimes(struct mmDbMysql* p, int trytimes)
{
    p->trytimes = trytimes;
}
MM_EXPORT_MYSQL int mmDbMysql_GetTrytimes(struct mmDbMysql* p)
{
    return p->trytimes;
}

// get mysql context.
MM_EXPORT_MYSQL MYSQL* mmDbMysql_GetContext(struct mmDbMysql* p)
{
    return &p->context;
}
// context connect state.you must make sure state is success,if not any api for MYSQL will crash.
// default is unknown.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_MYSQL int mmDbMysql_GetContextState(struct mmDbMysql* p)
{
    return p->context_state;
}

// try connect to target.return context_state.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_MYSQL int mmDbMysql_Connect(struct mmDbMysql* p)
{
    p->context_state = MM_DB_MYSQL_STATE_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == mysql_real_connect(
            &p->context,
            mmString_CStr(&p->config.node),
            mmString_CStr(&p->config.username),
            mmString_CStr(&p->config.password),
            mmString_CStr(&p->config.basename),
            p->config.port,
            NULL,
            CLIENT_MULTI_STATEMENTS))
        {
            // handle the errno.
            int error_code = mysql_errno(&p->context);
            mmLogger_LogE(gLogger, "%s %d failure to connect the database:%s-%d.", __FUNCTION__, __LINE__, mmString_CStr(&p->config.node), p->config.port);
            mmLogger_LogE(gLogger, "%s %d error(%d):%s", __FUNCTION__, __LINE__, error_code, mysql_error(&p->context));
            // here we must close and init for make sure MYSQL not crash at next Connect.
            mysql_close(&p->context);
            mysql_init(&p->context);
            p->context_state = MM_DB_MYSQL_STATE_FAILURE;
            break;
        }

        if (0 != mysql_set_character_set(&p->context, mmString_CStr(&p->config.charsets)))
        {
            // handle the errno.
            int error_code = mysql_errno(&p->context);
            mmLogger_LogE(gLogger, "%s %d failure to set character:%s.", __FUNCTION__, __LINE__, mmString_CStr(&p->config.charsets));
            mmLogger_LogE(gLogger, "%s %d error(%d):%s", __FUNCTION__, __LINE__, error_code, mysql_error(&p->context));
            p->context_state = MM_DB_MYSQL_STATE_FAILURE;
            break;
        }

        p->context_state = MM_DB_MYSQL_STATE_SUCCESS;

    } while (0);
    return p->context_state;
}
// check context_state and reconnect if need,and then query result.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_MYSQL int mmDbMysql_RealQuery(struct mmDbMysql* p, const char* sql)
{
    int rt = -1;
    int trys = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    do
    {
        trys++;
        if (MM_DB_MYSQL_STATE_SUCCESS != p->context_state)
        {

            rt = 1;

            mmLogger_LogI(gLogger, "%s %d try to reconnect the database:%s-%d.",
                          __FUNCTION__, __LINE__, mmString_CStr(&p->config.node), p->config.port);
            
            mmDbMysql_Connect(p);

            continue;
        }
        if (0 != mysql_real_query(&p->context, sql, (unsigned long)strlen(sql)))
        {
            int error_code = -1;

            rt = 1;

            error_code = mysql_errno(&p->context);
            mmLogger_LogE(gLogger, "%s %d failure to query:%s", __FUNCTION__, __LINE__, sql);
            mmLogger_LogE(gLogger, "%s %d error(%d):%s",
                          __FUNCTION__, __LINE__, error_code, mysql_error(&p->context));

            //Error: 2014 (CR_COMMANDS_OUT_OF_SYNC)
            //Error: 2006 (CR_SERVER_GONE_ERROR)
            //Error: 2013 (CR_SERVER_LOST)
            //Error: 2000 (CR_UNKNOWN_ERROR)
            if (2006 == error_code || 2013 == error_code)
            {
                mmLogger_LogI(gLogger, "%s %d try to reconnect the database:%s-%d.",
                              __FUNCTION__, __LINE__, mmString_CStr(&p->config.node), p->config.port);
                mmDbMysql_Connect(p);
            }
            else if (2014 == error_code)
            {
                mmLogger_LogI(gLogger, "%s %d try to clear stored results.", __FUNCTION__, __LINE__);
                mmDbMysql_FreeUnusedResult(p);
            }

            continue;
        }

        rt = 0;
        // Query Success!
        mmLogger_LogV(gLogger, "%s %d query success:%s", __FUNCTION__, __LINE__, sql);

    } while (0 != rt && trys < p->trytimes);
    return rt;
}
// ping remote.
// Zero if the connection to the server is active. 
// Nonzero if an error occurred. 
// A nonzero return does not indicate whether the MySQL server itself is down; 
// the connection might be broken for other reasons such as network problems.
MM_EXPORT_MYSQL int mmDbMysql_Ping(struct mmDbMysql* p)
{
    return mysql_ping(&p->context);
}
// set auto commit flag.sets autocommit mode on if mode is 1, off if mode is 0.
// Zero for success. Nonzero if an error occurred.
MM_EXPORT_MYSQL int mmDbMysql_Autocommit(struct mmDbMysql* p, int flag)
{
    return mysql_autocommit(&p->context, flag);
}
// commit immediately.
// Zero for success. Nonzero if an error occurred.
MM_EXPORT_MYSQL int mmDbMysql_Commit(struct mmDbMysql* p)
{
    return mysql_commit(&p->context);
}

// real escape string.
MM_EXPORT_MYSQL unsigned long mmDbMysql_RealEscapeString(struct mmDbMysql* p, char* o_buff, const char* i_buff, size_t i_size)
{
    return mysql_real_escape_string(&p->context, o_buff, i_buff, (unsigned long)i_size);
}

MM_EXPORT_MYSQL void mmDbMysql_FetchRow(struct mmDbMysql* p)
{
    p->row = p->result ? mysql_fetch_row(p->result) : NULL;
    p->field_length = p->result ? mysql_fetch_lengths(p->result) : NULL;
}

// get if has next row for result.
// 1 has row.
// 0 not row.
MM_EXPORT_MYSQL int mmDbMysql_GetHasRow(struct mmDbMysql* p)
{
    return ((NULL != p->row) && (NULL != *p->row)) ? 1 : 0;
}
// get current result field count.you must check before GetField for read value.
MM_EXPORT_MYSQL unsigned int mmDbMysql_GetFieldCount(struct mmDbMysql* p)
{
    return mysql_field_count(&p->context);
}
// get current result affected rows.
MM_EXPORT_MYSQL mmUInt64_t mmDbMysql_GetAffectedRows(struct mmDbMysql* p)
{
    return (mmUInt64_t)mysql_affected_rows(&p->context);
}
// free current result.
MM_EXPORT_MYSQL void mmDbMysql_FreeCurrentResult(struct mmDbMysql* p)
{
    // NULL == p->result is safe.
    mysql_free_result(p->result);
    p->result = NULL;
}
// free unused result.
MM_EXPORT_MYSQL void mmDbMysql_FreeUnusedResult(struct mmDbMysql* p)
{
    MYSQL_RES* result = NULL;
    do
    {
        result = mysql_store_result(&p->context);
        mysql_free_result(result);
    } while (!mysql_next_result(&p->context));
}
// next result for query.if first will only call mysql_store_result.
// note: you must free current result to make sure not memory leak.
MM_EXPORT_MYSQL void mmDbMysql_StoreNextResult(struct mmDbMysql* p)
{
    assert(NULL == p->result && "current must null before store next result.");
    do
    {
        p->result = mysql_store_result(&p->context);
    } while (NULL == p->result && 0 == mysql_next_result(&p->context));
}

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt64(struct mmDbMysql* p, int idx, mmUInt64_t* v)
{
    *v = (mmUInt64_t)(mmAtoi64(p->row[idx]) & 0xFFFFFFFFFFFFFFFF);
}
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt64(struct mmDbMysql* p, int idx, mmSInt64_t* v)
{
    *v = (mmSInt64_t)(mmAtoi64(p->row[idx]) & 0xFFFFFFFFFFFFFFFF);
}

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt32(struct mmDbMysql* p, int idx, mmUInt32_t* v)
{
    *v = (mmUInt32_t)(mmAtoi32(p->row[idx]) & 0xFFFFFFFF);
}
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt32(struct mmDbMysql* p, int idx, mmSInt32_t* v)
{
    *v = (mmSInt32_t)(mmAtoi32(p->row[idx]) & 0xFFFFFFFF);
}

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt16(struct mmDbMysql* p, int idx, mmUInt16_t* v)
{
    *v = (mmUInt16_t)(mmAtoi32(p->row[idx]) & 0xFFFF);
}
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt16(struct mmDbMysql* p, int idx, mmSInt16_t* v)
{
    *v = (mmSInt16_t)(mmAtoi32(p->row[idx]) & 0xFFFF);
}

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt8(struct mmDbMysql* p, int idx, mmUInt8_t* v)
{
    *v = (mmUInt8_t)(mmAtoi32(p->row[idx]) & 0xFF);
}
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt8(struct mmDbMysql* p, int idx, mmSInt8_t* v)
{
    *v = (mmSInt8_t)(mmAtoi32(p->row[idx]) & 0xFF);
}

MM_EXPORT_MYSQL void mmDbMysql_GetFieldFloat(struct mmDbMysql* p, int idx, float* v)
{
    *v = (float)atof(p->row[idx]);
}
MM_EXPORT_MYSQL void mmDbMysql_GetFieldDouble(struct mmDbMysql* p, int idx, double* v)
{
    *v = (double)atof(p->row[idx]);
}

MM_EXPORT_MYSQL void mmDbMysql_GetFieldString(struct mmDbMysql* p, int idx, struct mmString* v)
{
    char* buff = p->row[idx];
    unsigned long size = p->field_length[idx];
    mmString_Resize(v, size);
    mmMemcpy((char*)mmString_Data(v), buff ? buff : "", size);
}

MM_EXPORT_MYSQL void mmDbMysqlSection_Init(struct mmDbMysqlSection* p)
{
    mmDbMysqlConfig_Init(&p->config);
    MM_LIST_INIT_HEAD(&p->list);
    mmSpinlock_Init(&p->list_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    pthread_key_create(&p->thread_key, NULL);
    p->sid = 0;
}
MM_EXPORT_MYSQL void mmDbMysqlSection_Destroy(struct mmDbMysqlSection* p)
{
    mmDbMysqlSection_Clear(p);
    //
    mmDbMysqlConfig_Destroy(&p->config);
    MM_LIST_INIT_HEAD(&p->list);
    mmSpinlock_Destroy(&p->list_locker);
    mmSpinlock_Destroy(&p->locker);
    pthread_key_delete(p->thread_key);
    p->sid = 0;
}

MM_EXPORT_MYSQL void mmDbMysqlSection_Lock(struct mmDbMysqlSection* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_MYSQL void mmDbMysqlSection_Unlock(struct mmDbMysqlSection* p)
{
    mmSpinlock_Unlock(&p->locker);
}

// config for connect.
MM_EXPORT_MYSQL void mmDbMysqlSection_SetConfig(struct mmDbMysqlSection* p, struct mmDbMysqlConfig* config)
{
    struct mmListHead* pos = NULL;
    mmDbMysqlConfig_Assign(&p->config, config);
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmDbMysql* conn = NULL;
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        conn = (struct mmDbMysql*)(e->v);
        mmDbMysql_Lock(conn);
        mmDbMysql_SetConfig(conn, &p->config);
        mmDbMysql_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_MYSQL struct mmDbMysqlConfig* mmDbMysqlSection_GetConfig(struct mmDbMysqlSection* p)
{
    return &p->config;
}
MM_EXPORT_MYSQL void mmDbMysqlSection_SetSid(struct mmDbMysqlSection* p, mmUInt32_t sid)
{
    p->sid = sid;
}
MM_EXPORT_MYSQL mmUInt32_t mmDbMysqlSection_GetSid(struct mmDbMysqlSection* p)
{
    return p->sid;
}

MM_EXPORT_MYSQL void mmDbMysqlSection_Connect(struct mmDbMysqlSection* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmDbMysql* conn = NULL;
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        conn = (struct mmDbMysql*)(e->v);
        mmDbMysql_Lock(conn);
        mmDbMysql_Connect(conn);
        mmDbMysql_Unlock(conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

// get current thread conn.
MM_EXPORT_MYSQL struct mmDbMysql* mmDbMysqlSection_ThreadInstance(struct mmDbMysqlSection* p)
{
    struct mmDbMysql* conn = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (!lvp)
    {
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        conn = (struct mmDbMysql*)mmMalloc(sizeof(struct mmDbMysql));
        mmDbMysql_Init(conn);
        mmDbMysql_SetConfig(conn, &p->config);
        mmDbMysql_Connect(conn);
        lvp->v = conn;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        conn = (struct mmDbMysql*)(lvp->v);
    }
    return conn;
}
// clear all thread conn.
MM_EXPORT_MYSQL void mmDbMysqlSection_Clear(struct mmDbMysqlSection* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmDbMysql* conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        conn = (struct mmDbMysql*)(lvp->v);
        mmDbMysql_Lock(conn);
        mmList_Del(curr);
        mmDbMysql_FreeUnusedResult(conn);
        mmDbMysql_Unlock(conn);
        mmDbMysql_Destroy(conn);
        mmFree(conn);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_MYSQL void mmDbMysqlContact_Init(struct mmDbMysqlContact* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_MYSQL void mmDbMysqlContact_Destroy(struct mmDbMysqlContact* p)
{
    mmDbMysqlContact_Clear(p);
    //
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->locker);
}

// lock to make sure the mt_contact is thread safe.
MM_EXPORT_MYSQL void mmDbMysqlContact_Lock(struct mmDbMysqlContact* p)
{
    mmSpinlock_Lock(&p->locker);
}
// unlock mt_contact.
MM_EXPORT_MYSQL void mmDbMysqlContact_Unlock(struct mmDbMysqlContact* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_MYSQL struct mmDbMysqlSection* mmDbMysqlContact_Add(struct mmDbMysqlContact* p, int sid, struct mmDbMysqlConfig* config)
{
    struct mmDbMysqlSection* mt = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    mt = mmDbMysqlContact_Get(p, sid);
    if (NULL == mt)
    {
        mt = (struct mmDbMysqlSection*)mmMalloc(sizeof(struct mmDbMysqlSection));
        mmDbMysqlSection_Init(mt);

        mmDbMysqlSection_SetConfig(mt, config);
        // assign sid index.
        mt->sid = sid;
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU32Vpt_Set(&p->rbtree, sid, mt);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return mt;
}
MM_EXPORT_MYSQL void mmDbMysqlContact_Rmv(struct mmDbMysqlContact* p, int sid)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU32Vpt_GetIterator(&p->rbtree, sid);
    if (it)
    {
        struct mmDbMysqlSection* mt = (struct mmDbMysqlSection*)(it->v);
        mmDbMysqlSection_Lock(mt);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmDbMysqlSection_Unlock(mt);
        mmDbMysqlSection_Destroy(mt);
        mmFree(mt);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_MYSQL struct mmDbMysqlSection* mmDbMysqlContact_Get(struct mmDbMysqlContact* p, int sid)
{
    struct mmDbMysqlSection* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmDbMysqlSection*)mmRbtreeU32Vpt_Get(&p->rbtree, sid);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_MYSQL struct mmDbMysqlSection* mmDbMysqlContact_GetInstance(struct mmDbMysqlContact* p, int sid, struct mmDbMysqlConfig* config)
{
    struct mmDbMysqlSection* e = mmDbMysqlContact_Get(p, sid);
    if (NULL == e)
    {
        e = mmDbMysqlContact_Add(p, sid, config);
    }
    else
    {
        mmDbMysqlSection_Lock(e);
        if (0 != mmDbMysqlConfig_Compare(&e->config, config))
        {
            mmDbMysqlSection_SetConfig(e, config);
            mmDbMysqlSection_Connect(e);
        }
        mmDbMysqlSection_Unlock(e);
    }
    return e;
}
MM_EXPORT_MYSQL void mmDbMysqlContact_Clear(struct mmDbMysqlContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmDbMysqlSection* mt = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        mt = (struct mmDbMysqlSection*)(it->v);
        n = mmRb_Next(n);
        mmDbMysqlSection_Lock(mt);
        mmRbtreeU32Vpt_Erase(&p->rbtree, it);
        mmDbMysqlSection_Unlock(mt);
        mmDbMysqlSection_Destroy(mt);
        mmFree(mt);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}


