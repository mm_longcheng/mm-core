/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDbMysql_h__
#define __mmDbMysql_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"
#include "core/mmString.h"
#include "core/mmList.h"

#include "container/mmRbtreeU32.h"

#include "pthread.h"
#include <mysql.h>

#include "mysql/mmDbMysqlExport.h"

#include "core/mmPrefix.h"

#define MM_DB_MYSQL_TRYTIMES 3;
// alloc value_size roundup32 * MM_REAL_ESCAPE_STRING_FACTOR size to cache real escape string.
#define MM_REAL_ESCAPE_STRING_FACTOR 2

enum mmDbMysqlState_t
{
    MM_DB_MYSQL_STATE_UNKNOWN = -1,// -1 is unknown.
    MM_DB_MYSQL_STATE_SUCCESS =  0,//  0 is success.
    MM_DB_MYSQL_STATE_FAILURE =  1,//  1 is failure.
};

// config for mmDbMysql.
struct mmDbMysqlConfig
{
    struct mmString node;    // default "127.0.0.1"
    struct mmString username;// default "mm"
    struct mmString password;// default "xxxxxx"
    struct mmString basename;// default "mm"
    struct mmString charsets;// default "utf8"
    mmUShort_t port;         // default "3306"
};

MM_EXPORT_MYSQL void mmDbMysqlConfig_Init(struct mmDbMysqlConfig* p);
MM_EXPORT_MYSQL void mmDbMysqlConfig_Destroy(struct mmDbMysqlConfig* p);

// 0 is same.
MM_EXPORT_MYSQL int mmDbMysqlConfig_Compare(struct mmDbMysqlConfig* p, struct mmDbMysqlConfig* t);
// assign.
MM_EXPORT_MYSQL void mmDbMysqlConfig_Assign(struct mmDbMysqlConfig* p, struct mmDbMysqlConfig* t);

struct mmDbMysql
{
    // mysql config.
    struct mmDbMysqlConfig config;
    // locker for this instance.
    mmAtomic_t locker;
    // mysql context.
    MYSQL context;
    // current row for query.
    MYSQL_ROW row;
    // current result.strong ref.
    MYSQL_RES* result;
    // try connect times.default is MM_DB_MYSQL_TRYTIMES.
    int trytimes;
    // mysql context connect state.mmDbMysqlState_t.
    int context_state;
    // weak ref for MYSQL_RES
    unsigned long* field_length;
};

MM_EXPORT_MYSQL void mmDbMysql_Init(struct mmDbMysql* p);
MM_EXPORT_MYSQL void mmDbMysql_Destroy(struct mmDbMysql* p);

MM_EXPORT_MYSQL void mmDbMysql_Lock(struct mmDbMysql* p);
MM_EXPORT_MYSQL void mmDbMysql_Unlock(struct mmDbMysql* p);

// config for connect.
MM_EXPORT_MYSQL void mmDbMysql_SetConfig(struct mmDbMysql* p, struct mmDbMysqlConfig* config);
MM_EXPORT_MYSQL struct mmDbMysqlConfig* mmDbMysql_GetConfig(struct mmDbMysql* p);
// try connect times.default is MM_DB_MYSQL_TRYTIMES.
MM_EXPORT_MYSQL void mmDbMysql_SetTrytimes(struct mmDbMysql* p, int trytimes);
MM_EXPORT_MYSQL int mmDbMysql_GetTrytimes(struct mmDbMysql* p);

// get mysql context.
MM_EXPORT_MYSQL MYSQL* mmDbMysql_GetContext(struct mmDbMysql* p);
// context connect state.you must make sure state is success,if not any api for MYSQL will crash.
// default is unknown.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_MYSQL int mmDbMysql_GetContextState(struct mmDbMysql* p);

// try connect to target.return context_state.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_MYSQL int mmDbMysql_Connect(struct mmDbMysql* p);
// check context_state and reconnect if need,and then query result.
// -1 is unknown.
//  0 is success.
//  1 is failure.
MM_EXPORT_MYSQL int mmDbMysql_RealQuery(struct mmDbMysql* p, const char* sql);
// ping remote.
// Zero if the connection to the server is active. 
// Nonzero if an error occurred. 
// A nonzero return does not indicate whether the MySQL server itself is down; 
// the connection might be broken for other reasons such as network problems.
MM_EXPORT_MYSQL int mmDbMysql_Ping(struct mmDbMysql* p);
// set auto commit flag.sets autocommit mode on if mode is 1, off if mode is 0.
// Zero for success. Nonzero if an error occurred.
MM_EXPORT_MYSQL int mmDbMysql_Autocommit(struct mmDbMysql* p, int flag);
// commit immediately.
// Zero for success. Nonzero if an error occurred.
MM_EXPORT_MYSQL int mmDbMysql_Commit(struct mmDbMysql* p);

// real escape string.
MM_EXPORT_MYSQL unsigned long mmDbMysql_RealEscapeString(struct mmDbMysql* p, char* o_buff, const char* i_buff, size_t i_size);

// fetch row.
MM_EXPORT_MYSQL void mmDbMysql_FetchRow(struct mmDbMysql* p);

// get if has next row for result.
// 1 has row.
// 0 not row.
MM_EXPORT_MYSQL int mmDbMysql_GetHasRow(struct mmDbMysql* p);
// get current result field count.you must check before GetField for read value.
MM_EXPORT_MYSQL unsigned int mmDbMysql_GetFieldCount(struct mmDbMysql* p);
// get current result affected rows.
MM_EXPORT_MYSQL mmUInt64_t mmDbMysql_GetAffectedRows(struct mmDbMysql* p);
// free current result.
MM_EXPORT_MYSQL void mmDbMysql_FreeCurrentResult(struct mmDbMysql* p);
// free unused result.
MM_EXPORT_MYSQL void mmDbMysql_FreeUnusedResult(struct mmDbMysql* p);
// next result for query.if first will only call mysql_store_result.
// note: you must free current result to make sure not memory leak.
MM_EXPORT_MYSQL void mmDbMysql_StoreNextResult(struct mmDbMysql* p);

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt64(struct mmDbMysql* p, int idx, mmUInt64_t* v);
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt64(struct mmDbMysql* p, int idx, mmSInt64_t* v);

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt32(struct mmDbMysql* p, int idx, mmUInt32_t* v);
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt32(struct mmDbMysql* p, int idx, mmSInt32_t* v);

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt16(struct mmDbMysql* p, int idx, mmUInt16_t* v);
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt16(struct mmDbMysql* p, int idx, mmSInt16_t* v);

MM_EXPORT_MYSQL void mmDbMysql_GetFieldUInt8(struct mmDbMysql* p, int idx, mmUInt8_t* v);
MM_EXPORT_MYSQL void mmDbMysql_GetFieldSInt8(struct mmDbMysql* p, int idx, mmSInt8_t* v);

MM_EXPORT_MYSQL void mmDbMysql_GetFieldFloat(struct mmDbMysql* p, int idx, float* v);
MM_EXPORT_MYSQL void mmDbMysql_GetFieldDouble(struct mmDbMysql* p, int idx, double* v);

MM_EXPORT_MYSQL void mmDbMysql_GetFieldString(struct mmDbMysql* p, int idx, struct mmString* v);

struct mmDbMysqlSection
{
    // mysql config.
    struct mmDbMysqlConfig config;
    // silk strong ref list.
    struct mmListHead list;
    // list locker.
    mmAtomic_t list_locker;
    // locker for this instance.
    mmAtomic_t locker;
    // thread key.
    pthread_key_t thread_key;
    mmUInt32_t sid;
};

MM_EXPORT_MYSQL void mmDbMysqlSection_Init(struct mmDbMysqlSection* p);
MM_EXPORT_MYSQL void mmDbMysqlSection_Destroy(struct mmDbMysqlSection* p);

MM_EXPORT_MYSQL void mmDbMysqlSection_Lock(struct mmDbMysqlSection* p);
MM_EXPORT_MYSQL void mmDbMysqlSection_Unlock(struct mmDbMysqlSection* p);

// config for connect.
MM_EXPORT_MYSQL void mmDbMysqlSection_SetConfig(struct mmDbMysqlSection* p, struct mmDbMysqlConfig* config);
MM_EXPORT_MYSQL struct mmDbMysqlConfig* mmDbMysqlSection_GetConfig(struct mmDbMysqlSection* p);
// sid for connect.
MM_EXPORT_MYSQL void mmDbMysqlSection_SetSid(struct mmDbMysqlSection* p, mmUInt32_t sid);
MM_EXPORT_MYSQL mmUInt32_t mmDbMysqlSection_GetSid(struct mmDbMysqlSection* p);

MM_EXPORT_MYSQL void mmDbMysqlSection_Connect(struct mmDbMysqlSection* p);

// get current thread conn.
MM_EXPORT_MYSQL struct mmDbMysql* mmDbMysqlSection_ThreadInstance(struct mmDbMysqlSection* p);
// clear all thread conn.
MM_EXPORT_MYSQL void mmDbMysqlSection_Clear(struct mmDbMysqlSection* p);

struct mmDbMysqlContact
{
    struct mmRbtreeU32Vpt rbtree;
    mmAtomic_t rbtree_locker;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t locker;
};

MM_EXPORT_MYSQL void mmDbMysqlContact_Init(struct mmDbMysqlContact* p);
MM_EXPORT_MYSQL void mmDbMysqlContact_Destroy(struct mmDbMysqlContact* p);

// lock to make sure the mt_contact is thread safe.
MM_EXPORT_MYSQL void mmDbMysqlContact_Lock(struct mmDbMysqlContact* p);
// unlock mt_contact.
MM_EXPORT_MYSQL void mmDbMysqlContact_Unlock(struct mmDbMysqlContact* p);

MM_EXPORT_MYSQL struct mmDbMysqlSection* mmDbMysqlContact_Add(struct mmDbMysqlContact* p, int sid, struct mmDbMysqlConfig* config);
MM_EXPORT_MYSQL void mmDbMysqlContact_Rmv(struct mmDbMysqlContact* p, int sid);
MM_EXPORT_MYSQL struct mmDbMysqlSection* mmDbMysqlContact_Get(struct mmDbMysqlContact* p, int sid);
MM_EXPORT_MYSQL struct mmDbMysqlSection* mmDbMysqlContact_GetInstance(struct mmDbMysqlContact* p, int sid, struct mmDbMysqlConfig* config);
MM_EXPORT_MYSQL void mmDbMysqlContact_Clear(struct mmDbMysqlContact* p);

#include "core/mmSuffix.h"

#endif//__mmDbMysql_h__
