/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDbMysqlQuery_h__
#define __mmDbMysqlQuery_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "dish/mmPackage.h"

#include "mysql/mmDbMysql.h"

#include <sstream>
#include <string>

#include "mysql/mmDbMysqlExport.h"

// alloc value_size roundup32 * MM_REAL_ESCAPE_STRING_FACTOR size to cache real escape string.
#define MM_REAL_ESCAPE_STRING_FACTOR 2

namespace mm
{
    // use for mysql real query string assemble.
    class MM_EXPORT_MYSQL mmDbMysqlStream
    {
    private:
        std::ostringstream stream_impl;
        MYSQL* context;// weak ref.
    public:
        explicit mmDbMysqlStream(MYSQL* _context);
        ~mmDbMysqlStream();
    public:
        std::ostringstream& GetStreamImpl();
        MYSQL* GetContext();
    public:
        const std::string GetString() const;
    };
    template<typename T>
    mmInline mmDbMysqlStream& operator << (mmDbMysqlStream& o, T& _value)
    {
        std::ostringstream& oss = o.GetStreamImpl();
        oss << _value;
        return o;
    }
    MM_EXPORT_MYSQL mmDbMysqlStream& operator << (mmDbMysqlStream& o, std::string& _value);

    // warp for easy use query process.
    class MM_EXPORT_MYSQL mmDbMysqlQuery
    {
    public:
        struct mmDbMysql* db_mysql;
    public:
        explicit mmDbMysqlQuery(struct mmDbMysql* _db_mysql);
    public:
        /* when use this func,you must check if has row first use get has row.*/
        void GetField(int idx, mmULLong_t& _value);
        void GetField(int idx, mmSLLong_t& _value);

        void GetField(int idx, mmULong_t& _value);
        void GetField(int idx, mmSLong_t& _value);

        void GetField(int idx, mmUInt_t& _value);
        void GetField(int idx, mmSInt_t& _value);

        void GetField(int idx, mmUShort_t& _value);
        void GetField(int idx, mmSShort_t& _value);

        void GetField(int idx, mmUChar_t& _value);
        void GetField(int idx, mmSChar_t& _value);

        void GetField(int idx, float& _value);
        void GetField(int idx, double& _value);
        void GetField(int idx, bool& _value);
        // std::string can be binary or c string.
        void GetField(int idx, std::string& _value);
    };

    class MM_EXPORT_MYSQL mmQueryer : public mmPackage
    {
    public:
        virtual ~mmQueryer() {}
        
        // interface for mysql query.
        virtual int Query(struct mmDbMysql* _sql, mmUInt32_t logger_level) = 0;
    };
}

#endif//__mmDbMysqlQuery_h__
