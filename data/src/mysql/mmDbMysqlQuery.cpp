/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDbMysqlQuery.h"

#include "core/mmAlloc.h"
#include "core/mmAtoi.h"
#include "core/mmBit.h"

namespace mm
{

    mmDbMysqlStream::mmDbMysqlStream(MYSQL* _context)
        : context(_context)
    {

    }

    mmDbMysqlStream::~mmDbMysqlStream()
    {

    }

    std::ostringstream& mmDbMysqlStream::GetStreamImpl()
    {
        return this->stream_impl;
    }

    MYSQL* mmDbMysqlStream::GetContext()
    {
        return this->context;
    }

    const std::string mmDbMysqlStream::GetString() const
    {
        return this->stream_impl.str();
    }


    MM_EXPORT_MYSQL mmDbMysqlStream& operator<<(mmDbMysqlStream& o, std::string& _value)
    {
        std::string binary;

        std::ostringstream& oss = o.GetStreamImpl();
        MYSQL* context = o.GetContext();

        unsigned long binary_real_size = 0;

        size_t value_size = _value.size();
        const char* value_buff = _value.data();
        size_t binary_size = value_size;

        mmUInt64_t pow_size = binary_size;
        mmRoundup64(pow_size);
        binary_size = (size_t)pow_size;

        // here we alloc value_size roundup32 * 2 size to cache real escape string.
        binary_size *= MM_REAL_ESCAPE_STRING_FACTOR;
        // resize for contain all binary data.
        binary.resize(binary_size);
        // real escape string.
        binary_real_size = mysql_real_escape_string(context, (char*)binary.data(), value_buff, (unsigned long)value_size);
        // resize for real binary size for append.
        binary.resize(binary_real_size);

        oss << binary;

        return o;
    }

    mmDbMysqlQuery::mmDbMysqlQuery(struct mmDbMysql* _db_mysql)
        : db_mysql(_db_mysql)
    {

    }

    void mmDbMysqlQuery::GetField(int idx, mmULLong_t& _value)
    {
        _value = (mmULLong_t)(mmAtoi64(db_mysql->row[idx]) & 0xFFFFFFFFFFFFFFFF);
    }
    void mmDbMysqlQuery::GetField(int idx, mmSLLong_t& _value)
    {
        _value = (mmSLLong_t)(mmAtoi64(db_mysql->row[idx]) & 0xFFFFFFFFFFFFFFFF);
    }

    void mmDbMysqlQuery::GetField(int idx, mmULong_t& _value)
    {
        _value = (mmULong_t)(mmAtoi64(db_mysql->row[idx]) & 0xFFFFFFFFFFFFFFFF);
    }
    void mmDbMysqlQuery::GetField(int idx, mmSLong_t& _value)
    {
        _value = (mmSLong_t)(mmAtoi64(db_mysql->row[idx]) & 0xFFFFFFFFFFFFFFFF);
    }

    void mmDbMysqlQuery::GetField(int idx, mmUInt_t& _value)
    {
        _value = (mmUInt_t)(mmAtoi32(db_mysql->row[idx]) & 0xFFFFFFFF);
    }
    void mmDbMysqlQuery::GetField(int idx, mmSInt_t& _value)
    {
        _value = (mmSInt_t)(mmAtoi32(db_mysql->row[idx]) & 0xFFFFFFFF);
    }

    void mmDbMysqlQuery::GetField(int idx, mmUShort_t& _value)
    {
        _value = (mmUShort_t)(mmAtoi32(db_mysql->row[idx]) & 0xFFFF);
    }
    void mmDbMysqlQuery::GetField(int idx, mmSShort_t& _value)
    {
        _value = (mmSShort_t)(mmAtoi32(db_mysql->row[idx]) & 0xFFFF);
    }

    void mmDbMysqlQuery::GetField(int idx, mmUChar_t& _value)
    {
        _value = (mmUChar_t)(mmAtoi32(db_mysql->row[idx]) & 0xFF);
    }
    void mmDbMysqlQuery::GetField(int idx, mmSChar_t& _value)
    {
        _value = (mmSChar_t)(mmAtoi32(db_mysql->row[idx]) & 0xFF);
    }

    void mmDbMysqlQuery::GetField(int idx, float& _value)
    {
        _value = (float)atof(db_mysql->row[idx]);
    }
    void mmDbMysqlQuery::GetField(int idx, double& _value)
    {
        _value = (double)atof(db_mysql->row[idx]);
    }
    void mmDbMysqlQuery::GetField(int idx, bool& _value)
    {
        _value = (0 != (mmAtoi32(db_mysql->row[idx]) & 0xFF));
    }

    void mmDbMysqlQuery::GetField(int idx, std::string& _value)
    {
        char* buff = db_mysql->row[idx];
        unsigned long size = db_mysql->field_length[idx];
        _value.resize(size);
        mmMemcpy((void*)_value.data(), buff ? buff : "", size);
    }
}
