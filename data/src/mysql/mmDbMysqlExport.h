/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDbMysqlExport_h__
#define __mmDbMysqlExport_h__

#include "core/mmPlatform.h"

// windwos
#if MM_PLATFORM == MM_PLATFORM_WIN32

/* DLLExport define */
#  ifndef MM_EXPORT_MYSQL
#    ifdef MM_SHARED_MYSQL
#      ifdef MM_BUILD_MYSQL
         /* We are building this library */
#        define MM_EXPORT_MYSQL __declspec(dllexport)
#      else
         /* We are using this library */
#        define MM_EXPORT_MYSQL __declspec(dllimport)
#      endif
#    else
#      define MM_EXPORT_MYSQL
#    endif
#  endif

#  ifndef MM_PRIVATE_MYSQL
#    define MM_PRIVATE_MYSQL 
#  endif

#  ifndef MM_DEPRECATED_MYSQL
#    define MM_DEPRECATED_MYSQL __declspec(deprecated)
#  endif

#  ifndef MM_DEPRECATED_EXPORT_MYSQL
#    define MM_DEPRECATED_EXPORT_MYSQL MM_EXPORT_MYSQL MM_DEPRECATED_MYSQL
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_MYSQL
#    define MM_DEPRECATED_PRIVATE_MYSQL MM_PRIVATE_MYSQL MM_DEPRECATED_MYSQL
#  endif

#else// unix

// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).

/* DLLExport define */
#  ifndef MM_EXPORT_MYSQL
#    ifdef MM_SHARED_MYSQL
#      ifdef MM_BUILD_MYSQL
         /* We are building this library */
#        define MM_EXPORT_MYSQL __attribute__ ((visibility("default")))
#      else
         /* We are using this library */
#        define MM_EXPORT_MYSQL
#      endif
#    else
#      define MM_EXPORT_MYSQL
#    endif
#  endif

#  ifndef MM_PRIVATE_MYSQL
#    define MM_PRIVATE_MYSQL 
#  endif

#  ifndef MM_DEPRECATED_MYSQL
#    define MM_DEPRECATED_MYSQL __attribute__ ((deprecated))
#  endif

#  ifndef MM_DEPRECATED_EXPORT_MYSQL
#    define MM_DEPRECATED_EXPORT_MYSQL MM_EXPORT_MYSQL MM_DEPRECATED_MYSQL
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_MYSQL
#    define MM_DEPRECATED_PRIVATE_MYSQL MM_PRIVATE_MYSQL MM_DEPRECATED_MYSQL
#  endif

#endif

#endif//__mmDbMysqlExport_h__
