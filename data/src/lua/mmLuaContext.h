/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLuaContext_h__
#define __mmLuaContext_h__

#include "core/mmCore.h"

#include "dish/mmFileContext.h"

#include "core/mmPrefix.h"
// lua is c api.
#include "lua.h" 
#include "lualib.h" 
#include "lauxlib.h" 
#include "core/mmSuffix.h"
//
#include "lua/mmLuaExport.h"

#include "core/mmPrefix.h"

// lua searchers name.
#if ( LUA_VERSION_NUM >= 502 )
#define MM_LUA_SEARCHERS "searchers"
#else
#define MM_LUA_SEARCHERS "loaders"
#endif
#define MM_LUA_FILE_CONTEXT "FileContext"

MM_EXPORT_LUA void mmLuaState_FileNameByModule(const char* module_name, struct mmString* file_name);

MM_EXPORT_LUA void mmLuaState_AddSearchPath(struct lua_State* L, const char* path);
MM_EXPORT_LUA void mmLuaState_AddLuaLoader(struct lua_State* L, lua_CFunction func);

// load buffer not pcall.
MM_EXPORT_LUA int mmLuaState_LoadLuaBufferNotPcall(struct lua_State* L, mmUInt8_t* buffer, size_t offset, size_t length);
// load buffer and pcall.
MM_EXPORT_LUA int mmLuaState_LoadLuaBufferAndPcall(struct lua_State* L, mmUInt8_t* buffer, size_t offset, size_t length);

MM_EXPORT_LUA void mmLuaState_ReloadModule(struct lua_State* L, const char* name);

// load file not pcall.
MM_EXPORT_LUA void mmLuaState_LoadFileNotPcall(struct lua_State* L, const char* file_name);
// load file and pcall.
MM_EXPORT_LUA void mmLuaState_LoadFileAndPcall(struct lua_State* L, const char* file_name);

MM_EXPORT_LUA void mmLuaState_SetGlobalFileContext(struct lua_State* L, struct mmFileContext* context);
MM_EXPORT_LUA struct mmFileContext* mmLuaState_GetGlobalFileContext(struct lua_State* L);

/*
 * Global "FileContext"
 *
 * Module "mm/Logger"
 *     LogU
 *     LogF
 *     LogC
 *     LogE
 *     LogA
 *     LogW
 *     LogN
 *     LogI
 *     LogT
 *     LogD
 *     LogV
 *
 * Module "mm/LuaContext"
 *     RequireModule
 */

struct mmLuaContext
{
    // strong ref.
    struct lua_State* state;
};

MM_EXPORT_LUA void mmLuaContext_Init(struct mmLuaContext* p);
MM_EXPORT_LUA void mmLuaContext_Destroy(struct mmLuaContext* p);

#include "core/mmSuffix.h"

#endif//__mmLuaContext_h__
