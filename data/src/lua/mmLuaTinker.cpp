/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

// lua_tinker.cpp
//
// LuaTinker - Simple and light C++ wrapper for Lua.
//
// Copyright (c) 2005-2007 Kwon-il Lee (zupet@hitel.net)
// 
// please check Licence.txt file for licence and legal issues. 
#include "mmLuaTinker.h"
#include <iostream>

typedef long long lt_llong_t;
typedef unsigned long long lt_ullong_t;
/*---------------------------------------------------------------------------*/
/* init                                                                      */
/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::init(lua_State *L)
{
    init_s64(L);
    init_u64(L);
}

/*---------------------------------------------------------------------------*/
/* __s64                                                                     */
/*---------------------------------------------------------------------------*/
static lt_llong_t _sint64(lua_State *L, int index)
{
    int type = lua_type(L, index);
    lt_llong_t n = 0;
    switch (type)
    {
    case LUA_TNUMBER:
    {
        lua_Number d = lua_tonumber(L, index);
        n = (lt_llong_t)d;
        break;
    }
    case LUA_TSTRING:
    {
        size_t len = 0;
        const unsigned char * str = (const unsigned char *)lua_tolstring(L, index, &len);
        if (len > 8)
        {
            return luaL_error(L, "The string (length = %d) is not an int64 string", len);
        }
        int i = 0;
        lt_ullong_t n64 = 0;
        for (i = 0; i < (int)len; i++)
        {
            n64 |= (lt_ullong_t)str[i] << (i * 8);
        }
        n = (lt_llong_t)n64;
        break;
    }
    case LUA_TLIGHTUSERDATA:
    {
        void * p = lua_touserdata(L, index);
        n = (intptr_t)p;
        break;
    }
    case LUA_TUSERDATA:
    {
        n = *(lt_llong_t*)lua_touserdata(L, index);
        break;
    }
    default:
        return luaL_error(L, "argument %d error type %s", index, lua_typename(L, type));
    }
    return n;
}
static int tostring_s64(lua_State *L)
{
    char temp[64];
    //sprintf(temp, "%I64d", *(long long*)lua_topointer(L, 1));
    sprintf(temp, "%lld", *(long long*)lua_topointer(L, 1));
    lua_pushstring(L, temp);
    return 1;
}

/*---------------------------------------------------------------------------*/
static int eq_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_pushboolean(L, (a == b));
    return 1;
}

/*---------------------------------------------------------------------------*/
static int lt_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_pushboolean(L, (a < b));
    return 1;
}

/*---------------------------------------------------------------------------*/
static int le_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_pushboolean(L, (a <= b));
    return 1;
}
/*---------------------------------------------------------------------------*/
// +
static int add_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_tinker::push(L, a + b);
    return 1;
}
/*---------------------------------------------------------------------------*/
// -
static int sub_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_tinker::push(L, a - b);
    return 1;
}
/*---------------------------------------------------------------------------*/
// *
static int mul_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_tinker::push(L, a * b);
    return 1;
}
/*---------------------------------------------------------------------------*/
// /
static int div_s64(lua_State *L)
{
    lt_llong_t a = _sint64(L, 1);
    lt_llong_t b = _sint64(L, 2);
    lua_tinker::push(L, a / b);
    return 1;
}
/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::init_s64(lua_State *L)
{
    const char* name = "__s64";
    //  lua_pushstring(L, name);
    lua_newtable(L);

    lua_pushstring(L, "__name");
    lua_pushstring(L, name);
    lua_rawset(L, -3);

    lua_pushstring(L, "__tostring");
    lua_pushcclosure(L, tostring_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__eq");
    lua_pushcclosure(L, eq_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__lt");
    lua_pushcclosure(L, lt_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__le");
    lua_pushcclosure(L, le_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__add");
    lua_pushcclosure(L, add_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__sub");
    lua_pushcclosure(L, sub_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__mul");
    lua_pushcclosure(L, mul_s64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__div");
    lua_pushcclosure(L, div_s64, 0);
    lua_rawset(L, -3);

    //  lua_settable(L, LUA_GLOBALSINDEX);
    lua_setglobal(L, name);
}

/*---------------------------------------------------------------------------*/
/* __u64                                                                     */
/*---------------------------------------------------------------------------*/
static lt_ullong_t _uint64(lua_State *L, int index)
{
    int type = lua_type(L, index);
    lt_ullong_t n = 0;
    switch (type)
    {
    case LUA_TNUMBER:
    {
        lua_Number d = lua_tonumber(L, index);
        n = (lt_ullong_t)d;
        break;
    }
    case LUA_TSTRING:
    {
        size_t len = 0;
        const unsigned char * str = (const unsigned char *)lua_tolstring(L, index, &len);
        if (len > 8)
        {
            return luaL_error(L, "The string (length = %d) is not an int64 string", len);
        }
        int i = 0;
        lt_ullong_t n64 = 0;
        for (i = 0; i < (int)len; i++)
        {
            n64 |= (lt_ullong_t)str[i] << (i * 8);
        }
        n = (lt_ullong_t)n64;
        break;
    }
    case LUA_TLIGHTUSERDATA:
    {
        void * p = lua_touserdata(L, index);
        n = (intptr_t)p;
        break;
    }
    case LUA_TUSERDATA:
    {
        n = *(lt_ullong_t*)lua_touserdata(L, index);
        break;
    }
    default:
        return luaL_error(L, "argument %d error type %s", index, lua_typename(L, type));
    }
    return n;
}
static int tostring_u64(lua_State *L)
{
    char temp[64];
    //sprintf(temp, "%I64u", *(unsigned long long*)lua_topointer(L, 1));
    sprintf(temp, "%llu", *(unsigned long long*)lua_topointer(L, 1));
    lua_pushstring(L, temp);
    return 1;
}

/*---------------------------------------------------------------------------*/
static int eq_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_pushboolean(L, (a == b));
    return 1;
}

/*---------------------------------------------------------------------------*/
static int lt_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_pushboolean(L, (a < b));
    return 1;
}

/*---------------------------------------------------------------------------*/
static int le_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_pushboolean(L, (a <= b));
    return 1;
}
/*---------------------------------------------------------------------------*/
// +
static int add_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_tinker::push(L, a + b);
    return 1;
}
/*---------------------------------------------------------------------------*/
// -
static int sub_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_tinker::push(L, a - b);
    return 1;
}
/*---------------------------------------------------------------------------*/
// *
static int mul_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_tinker::push(L, a * b);
    return 1;
}
/*---------------------------------------------------------------------------*/
// /
static int div_u64(lua_State *L)
{
    lt_ullong_t a = _uint64(L, 1);
    lt_ullong_t b = _uint64(L, 2);
    lua_tinker::push(L, a / b);
    return 1;
}
/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::init_u64(lua_State *L)
{
    const char* name = "__u64";
    //  lua_pushstring(L, name);
    lua_newtable(L);

    lua_pushstring(L, "__name");
    lua_pushstring(L, name);
    lua_rawset(L, -3);

    lua_pushstring(L, "__tostring");
    lua_pushcclosure(L, tostring_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__eq");
    lua_pushcclosure(L, eq_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__lt");
    lua_pushcclosure(L, lt_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__le");
    lua_pushcclosure(L, le_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__add");
    lua_pushcclosure(L, add_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__sub");
    lua_pushcclosure(L, sub_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__mul");
    lua_pushcclosure(L, mul_u64, 0);
    lua_rawset(L, -3);

    lua_pushstring(L, "__div");
    lua_pushcclosure(L, div_u64, 0);
    lua_rawset(L, -3);

    //  lua_settable(L, LUA_GLOBALSINDEX);
    lua_setglobal(L, name);
}

/*---------------------------------------------------------------------------*/
/* excution                                                                  */
/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::dofile(lua_State *L, const char *filename)
{
    lua_pushcclosure(L, on_error, 0);
    int errfunc = lua_gettop(L);

    if (luaL_loadfile(L, filename) == 0)
    {
        if (lua_pcall(L, 0, 0, errfunc) != 0)
        {
            lua_pop(L, 1);
        }
    }
    else
    {
        print_error(L, "%s", lua_tostring(L, -1));
        lua_pop(L, 1);
    }

    lua_pop(L, 1);
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::dostring(lua_State *L, const char* buff)
{
    lua_tinker::dobuffer(L, buff, strlen(buff));
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::dobuffer(lua_State *L, const char* buff, size_t len)
{
    lua_pushcclosure(L, on_error, 0);
    int errfunc = lua_gettop(L);

    if (luaL_loadbuffer(L, buff, len, "lua_tinker::dobuffer()") == 0)
    {
        if (lua_pcall(L, 0, 0, errfunc) != 0)
        {
            lua_pop(L, 1);
        }
    }
    else
    {
        print_error(L, "%s", lua_tostring(L, -1));
        lua_pop(L, 1);
    }

    lua_pop(L, 1);
}

/*---------------------------------------------------------------------------*/
/* debug helpers                                                             */
/*---------------------------------------------------------------------------*/
static void call_stack(lua_State* L, int n)
{
    lua_Debug ar;
    if (lua_getstack(L, n, &ar) == 1)
    {
        lua_getinfo(L, "nSlu", &ar);

        const char* indent;
        if (n == 0)
        {
            indent = "->\t";
            lua_tinker::print_error(L, "\t<call stack>");
        }
        else
        {
            indent = "\t";
        }

        if (ar.name)
            lua_tinker::print_error(L, "%s%s() : line %d [%s : line %d]", indent, ar.name, ar.currentline, ar.source, ar.linedefined);
        else
            lua_tinker::print_error(L, "%sunknown : line %d [%s : line %d]", indent, ar.currentline, ar.source, ar.linedefined);

        call_stack(L, n + 1);
    }
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA int lua_tinker::on_error(lua_State *L)
{
    print_error(L, "%s", lua_tostring(L, -1));

    call_stack(L, 0);

    return 0;
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::print_error(lua_State *L, const char* fmt, ...)
{
    char text[4096];

    va_list args;
    va_start(args, fmt);
    vsnprintf(text, sizeof(text), fmt, args);
    va_end(args);

    lua_getglobal(L, "_ALERT");
    //  lua_pushstring(L, "_ALERT");
    //  lua_gettable(L, LUA_GLOBALSINDEX);
    if (lua_isfunction(L, -1))
    {
        lua_pushstring(L, text);
        lua_call(L, 1, 0);
    }
    else
    {
        printf("!!!!! %s\n", text);
        lua_pop(L, 1);
    }
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::enum_stack(lua_State *L)
{
    int top = lua_gettop(L);
    print_error(L, "%s", "----------stack----------");
    print_error(L, "Type:%d", top);
    for (int i = 1; i <= lua_gettop(L); ++i)
    {
        switch (lua_type(L, i))
        {
        case LUA_TNIL:
            print_error(L, "\t%s", lua_typename(L, lua_type(L, i)));
            break;
        case LUA_TBOOLEAN:
            print_error(L, "\t%s    %s", lua_typename(L, lua_type(L, i)), lua_toboolean(L, i) ? "true" : "false");
            break;
        case LUA_TLIGHTUSERDATA:
            print_error(L, "\t%s    0x%08p", lua_typename(L, lua_type(L, i)), lua_topointer(L, i));
            break;
        case LUA_TNUMBER:
            print_error(L, "\t%s    %f", lua_typename(L, lua_type(L, i)), lua_tonumber(L, i));
            break;
        case LUA_TSTRING:
            print_error(L, "\t%s    %s", lua_typename(L, lua_type(L, i)), lua_tostring(L, i));
            break;
        case LUA_TTABLE:
            print_error(L, "\t%s    0x%08p", lua_typename(L, lua_type(L, i)), lua_topointer(L, i));
            break;
        case LUA_TFUNCTION:
            print_error(L, "\t%s()  0x%08p", lua_typename(L, lua_type(L, i)), lua_topointer(L, i));
            break;
        case LUA_TUSERDATA:
            print_error(L, "\t%s    0x%08p", lua_typename(L, lua_type(L, i)), lua_topointer(L, i));
            break;
        case LUA_TTHREAD:
            print_error(L, "\t%s", lua_typename(L, lua_type(L, i)));
            break;
        }
    }
    print_error(L, "%s", "-------------------------");
}

/*---------------------------------------------------------------------------*/
/* read                                                                      */
/*---------------------------------------------------------------------------*/
template<> MM_EXPORT_LUA
char* lua_tinker::read(lua_State *L, int index)
{
    return (char*)lua_tostring(L, index);
}

template<> MM_EXPORT_LUA
char const* lua_tinker::read(lua_State *L, int index)
{
    return (const char*)lua_tostring(L, index);
}

template<> MM_EXPORT_LUA
std::string lua_tinker::read(lua_State *L, int index)
{
    return (const char*)lua_tostring(L, index);
}

template<> MM_EXPORT_LUA
char lua_tinker::read(lua_State *L, int index)
{
    return (char)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
unsigned char lua_tinker::read(lua_State *L, int index)
{
    return (unsigned char)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
short lua_tinker::read(lua_State *L, int index)
{
    return (short)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
unsigned short lua_tinker::read(lua_State *L, int index)
{
    return (unsigned short)lua_tonumber(L, index);
}

// long is different between X86 and X86_64 architecture
#if defined(__X86_64__) || defined(__X86_64) || defined(__amd_64) || defined(__amd_64__)
template<> MM_EXPORT_LUA //64bit
long lua_tinker::read(lua_State *L, int index)
{
    if (lua_isnumber(L, index))
        return (long)lua_tonumber(L, index);
    else
        return *(long*)lua_touserdata(L, index);
}

template<> MM_EXPORT_LUA
unsigned long lua_tinker::read(lua_State *L, int index)
{
    if (lua_isnumber(L, index))
        return (unsigned long)lua_tonumber(L, index);
    else
        return *(unsigned long*)lua_touserdata(L, index);
}
#else   //__X86__  //32bit
template<> MM_EXPORT_LUA
long lua_tinker::read(lua_State *L, int index)
{
    return (long)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
unsigned long lua_tinker::read(lua_State *L, int index)
{
    return (unsigned long)lua_tonumber(L, index);
}
#endif  //__X86__  //32bit

template<> MM_EXPORT_LUA //64bit
long long lua_tinker::read(lua_State *L, int index)
{
    if (lua_isnumber(L, index))
        return (long long)lua_tonumber(L, index);
    else
        return *(long long*)lua_touserdata(L, index);
}
template<> MM_EXPORT_LUA
unsigned long long lua_tinker::read(lua_State *L, int index)
{
    if (lua_isnumber(L, index))
        return (unsigned long long)lua_tonumber(L, index);
    else
        return *(unsigned long long*)lua_touserdata(L, index);
}
#

template<> MM_EXPORT_LUA
int lua_tinker::read(lua_State *L, int index)
{
    return (int)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
unsigned int lua_tinker::read(lua_State *L, int index)
{
    return (unsigned int)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
float lua_tinker::read(lua_State *L, int index)
{
    return (float)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
double lua_tinker::read(lua_State *L, int index)
{
    return (double)lua_tonumber(L, index);
}

template<> MM_EXPORT_LUA
bool lua_tinker::read(lua_State *L, int index)
{
    if (lua_isboolean(L, index))
        return lua_toboolean(L, index) != 0;
    else
        return lua_tonumber(L, index) != 0;
}

template<> MM_EXPORT_LUA
void lua_tinker::read(lua_State *L, int index)
{
    (void)L;
    (void)index;
    return;
}


template<> MM_EXPORT_LUA
lua_tinker::table lua_tinker::read(lua_State *L, int index)
{
    return table(L, index);
}

/*---------------------------------------------------------------------------*/
/* push                                                                      */
/*---------------------------------------------------------------------------*/
template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, char ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, unsigned char ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, short ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, unsigned short ret)
{
    lua_pushnumber(L, ret);
}


template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, int ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, unsigned int ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, float ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, double ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, char* ret)
{
    lua_pushstring(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, const char* ret)
{
    lua_pushstring(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, const std::string& ret)
{
    lua_pushstring(L, ret.c_str());
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, std::string ret)
{
    lua_pushstring(L, ret.c_str());
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, bool ret)
{
    lua_pushboolean(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, lua_value* ret)
{
    if (ret) ret->to_lua(L); else lua_pushnil(L);
}

// long is different between X86 and X86_64 architecture
#if defined(__X86_64__) || defined(__X86_64) || defined(__amd_64) || defined(__amd_64__)
template<> MM_EXPORT_LUA    // 64bit
void lua_tinker::push(lua_State *L, long ret)
{
    *(long *)lua_newuserdata(L, sizeof(long)) = ret;
    lua_gettable(L, "__s64");
    lua_setmetatable(L, -2);
}
template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, unsigned long ret)
{
    *(unsigned long*)lua_newuserdata(L, sizeof(unsigned long)) = ret;
    lua_gettable(L, "__u64");
    lua_setmetatable(L, -2);
}
#else   //__X86__   //32bit
template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, long ret)
{
    lua_pushnumber(L, ret);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, unsigned long ret)
{
    lua_pushnumber(L, ret);
}
#endif  //__X86__   //32bit

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, long long ret)
{
    *(long long*)lua_newuserdata(L, sizeof(long long)) = ret;
    lua_getglobal(L, "__s64");
    //  lua_pushstring(L, "__s64");
    //  lua_gettable(L, LUA_GLOBALSINDEX);
    lua_setmetatable(L, -2);
}
template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, unsigned long long ret)
{
    *(unsigned long long*)lua_newuserdata(L, sizeof(unsigned long long)) = ret;
    lua_getglobal(L, "__u64");
    //  lua_pushstring(L, "__u64");
    //  lua_gettable(L, LUA_GLOBALSINDEX);
    lua_setmetatable(L, -2);
}

template<> MM_EXPORT_LUA
void lua_tinker::push(lua_State *L, lua_tinker::table ret)
{
    lua_pushvalue(L, ret.m_obj->m_index);
}

/*---------------------------------------------------------------------------*/
/* pop                                                                       */
/*---------------------------------------------------------------------------*/
template<> MM_EXPORT_LUA
void lua_tinker::pop(lua_State *L)
{
    lua_pop(L, 1);
}

template<> MM_EXPORT_LUA
lua_tinker::table lua_tinker::pop(lua_State *L)
{
    return table(L, lua_gettop(L));
}

/*---------------------------------------------------------------------------*/
/* Tinker Class Helper                                                       */
/*---------------------------------------------------------------------------*/
static void invoke_parent(lua_State *L)
{
    lua_pushstring(L, "__parent");
    lua_rawget(L, -2);
    if (lua_istable(L, -1))
    {
        lua_pushvalue(L, 2);
        lua_rawget(L, -2);
        if (!lua_isnil(L, -1))
        {
            lua_remove(L, -2);
        }
        else
        {
            lua_remove(L, -1);
            invoke_parent(L);
            lua_remove(L, -2);
        }
    }
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA int lua_tinker::meta_get(lua_State *L)
{
    lua_getmetatable(L, 1);
    lua_pushvalue(L, 2);
    lua_rawget(L, -2);

    if (lua_isuserdata(L, -1))
    {
        user2type<var_base*>::invoke(L, -1)->get(L);
        lua_remove(L, -2);
    }
    else if (lua_isnil(L, -1))
    {
        lua_remove(L, -1);
        invoke_parent(L);
        if (lua_isuserdata(L, -1))
        {
            user2type<var_base*>::invoke(L, -1)->get(L);
            lua_remove(L, -2);
        }
        else if (lua_isnil(L, -1))
        {
            lua_pushfstring(L, "can't find '%s' class variable. (forgot registering class variable ?)", lua_tostring(L, 2));
            lua_error(L);
        }
    }

    lua_remove(L, -2);

    return 1;
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA int lua_tinker::meta_set(lua_State *L)
{
    lua_getmetatable(L, 1);
    lua_pushvalue(L, 2);
    lua_rawget(L, -2);

    if (lua_isuserdata(L, -1))
    {
        user2type<var_base*>::invoke(L, -1)->set(L);
    }
    else if (lua_isnil(L, -1))
    {
        lua_remove(L, -1);
        lua_pushvalue(L, 2);
        lua_pushvalue(L, 4);
        invoke_parent(L);
        if (lua_isuserdata(L, -1))
        {
            user2type<var_base*>::invoke(L, -1)->set(L);
        }
        else if (lua_isnil(L, -1))
        {
            lua_pushfstring(L, "can't find '%s' class variable. (forgot registering class variable ?)", lua_tostring(L, 2));
            lua_error(L);
        }
    }
    lua_settop(L, 3);
    return 0;
}

/*---------------------------------------------------------------------------*/
MM_EXPORT_LUA void lua_tinker::push_meta(lua_State *L, const char* name)
{
    lua_getglobal(L, name);
    //  lua_pushstring(L, name);
    //  lua_gettable(L, LUA_GLOBALSINDEX);
}

/*---------------------------------------------------------------------------*/
/* table object on stack                                                     */
/*---------------------------------------------------------------------------*/
lua_tinker::table_obj::table_obj(lua_State* L, int index)
    :m_L(L)
    , m_index(index)
    , m_ref(0)
{
    if (lua_isnil(m_L, m_index))
    {
        m_pointer = NULL;
        lua_remove(m_L, m_index);
    }
    else
    {
        m_pointer = lua_topointer(m_L, m_index);
    }
}

lua_tinker::table_obj::~table_obj()
{
    if (validate())
    {
        lua_remove(m_L, m_index);
    }
}

void lua_tinker::table_obj::inc_ref()
{
    ++m_ref;
}

void lua_tinker::table_obj::dec_ref()
{
    if (--m_ref == 0)
        delete this;
}

bool lua_tinker::table_obj::validate()
{
    if (m_pointer != NULL)
    {
        if (m_pointer == lua_topointer(m_L, m_index))
        {
            return true;
        }
        else
        {
            int top = lua_gettop(m_L);

            for (int i = 1; i <= top; ++i)
            {
                if (m_pointer == lua_topointer(m_L, i))
                {
                    m_index = i;
                    return true;
                }
            }

            m_pointer = NULL;
            return false;
        }
    }
    else
    {
        return false;
    }
}

/*---------------------------------------------------------------------------*/
/* Table Object Holder                                                       */
/*---------------------------------------------------------------------------*/
lua_tinker::table::table(lua_State* L)
{
    lua_newtable(L);

    m_obj = new table_obj(L, lua_gettop(L));
    m_obj->inc_ref();
    //m_obj->validate();
}

lua_tinker::table::table(lua_State* L, const char* name)
{
    lua_getglobal(L, name);
    //  lua_pushstring(L, name);
    //  lua_gettable(L, LUA_GLOBALSINDEX);

    if (lua_istable(L, -1) == 0)
    {
        lua_pop(L, 1);

        lua_newtable(L);
        //      lua_pushstring(L, name);
        //      lua_pushvalue(L, -2);
        //      lua_settable(L, LUA_GLOBALSINDEX);
        lua_setglobal(L, name);
        lua_getglobal(L, name);
    }

    m_obj = new table_obj(L, lua_gettop(L));
    m_obj->inc_ref();
    //m_obj->validate();
}

lua_tinker::table::table(lua_State* L, int index)
{
    if (index < 0)
    {
        index = lua_gettop(L) + index + 1;
    }

    m_obj = new table_obj(L, index);
    m_obj->inc_ref();
    //m_obj->validate();
}

lua_tinker::table::table(const table& input)
{
    m_obj = input.m_obj;
    m_obj->inc_ref();
    // m_obj->validate();
}

lua_tinker::table::~table()
{
    m_obj->dec_ref();
}

lua_tinker::table& lua_tinker::table::operator=(const table& rhs)
{
    if (&rhs == this)
    {
        return *this;
    }
    m_obj->dec_ref();
    m_obj = rhs.m_obj;
    m_obj->inc_ref();
    // m_obj->validate();
    return *this;
}

/*---------------------------------------------------------------------------*/
