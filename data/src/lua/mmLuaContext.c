/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLuaContext.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static int __static_mmLuaContext_Loader(struct lua_State* L)
{
    const char* modulename = luaL_checkstring(L, 1);
    if (NULL != modulename)
    {
        mmLuaState_ReloadModule(L, modulename);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d can not load a null module.", __FUNCTION__, __LINE__);
    }
    return 1;
}

static int __static_mmLuaContext_LoggerLogU(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogU(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogU failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogF(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogF(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogF failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogC(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogC(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogC failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogE(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogE(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogE failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogA(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogA(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogA failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogW(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogW(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogW failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogN(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogN(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogN failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogI(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogI(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogI failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogT(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogT(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogT failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogD(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogD(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogD failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LoggerLogV(struct lua_State* L)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    if (1 == lua_gettop(L))
    {
        const char* _msg = lua_tostring(L, 1);
        mmLogger_LogV(gLogger, "%s", _msg);
    }
    else
    {
        mmLogger_LogE(gLogger, "mmLogger.LogV failure.");
    }
    return 0;
}
static int __static_mmLuaContext_LuaopenLogger(struct lua_State *L)
{
    static const luaL_Reg lib_funcs[] =
    {
        {"LogU", __static_mmLuaContext_LoggerLogU},
        {"LogF", __static_mmLuaContext_LoggerLogF},
        {"LogC", __static_mmLuaContext_LoggerLogC},
        {"LogE", __static_mmLuaContext_LoggerLogE},
        {"LogA", __static_mmLuaContext_LoggerLogA},
        {"LogW", __static_mmLuaContext_LoggerLogW},
        {"LogN", __static_mmLuaContext_LoggerLogN},
        {"LogI", __static_mmLuaContext_LoggerLogI},
        {"LogT", __static_mmLuaContext_LoggerLogT},
        {"LogD", __static_mmLuaContext_LoggerLogD},
        {"LogV", __static_mmLuaContext_LoggerLogV},
        {NULL, NULL}
    };
    luaL_newlibtable(L, lib_funcs);
    luaL_setfuncs(L, lib_funcs, 0);
    return 1;
}

static int __static_mmLuaContext_RequireModule(struct lua_State* L)
{
    const char* modulename = luaL_checkstring(L, 1);
    if (NULL != modulename)
    {
        mmLuaState_ReloadModule(L, modulename);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d can not load a null module.", __FUNCTION__, __LINE__);
    }
    return 1;
}
static int __static_mmLuaContext_LuaopenLuaContext(struct lua_State *L)
{
    static const luaL_Reg lib_funcs[] =
    {
        {"RequireModule", __static_mmLuaContext_RequireModule},
        {NULL, NULL}
    };
    luaL_newlibtable(L, lib_funcs);
    luaL_setfuncs(L, lib_funcs, 0);
    return 1;
}

MM_EXPORT_LUA void mmLuaState_FileNameByModule(const char* module_name, struct mmString* file_name)
{
    static const char* lua_suffix = ".lua";
    static const size_t lua_suffix_size = 4;
    mmString_Assigns(file_name, module_name);
    if (mmString_Size(file_name) >= lua_suffix_size)
    {
        size_t vl = mmString_Size(file_name);
        char* vd = (char*)mmString_Data(file_name);
        size_t n = vl - lua_suffix_size;
        if (0 == mmMemcmp((vd + n), lua_suffix, lua_suffix_size))
        {
            vd[n] = 0;
            mmString_SetSizeValue(file_name, n);
        }
    }
    mmString_ReplaceChar(file_name, '.', '/');
    mmString_Appends(file_name, lua_suffix);
}
MM_EXPORT_LUA void mmLuaState_AddSearchPath(struct lua_State* L, const char* path)
{
    const char* cur_path = NULL;
    lua_getglobal(L, "package");                          /* stack: package */
    lua_getfield(L, -1, "path");                          /* get package.path, stack: package path */
    cur_path = lua_tostring(L, -1);
    lua_pop(L, 1);                                        /* stack: package */
    lua_pushfstring(L, ";%s\\?.lua%s", path, cur_path);   /* stack: package newpath */
    lua_setfield(L, -2, "path");                          /* package.path = newpath, stack: package */
    lua_pop(L, 1);                                        /* stack: - */
}
MM_EXPORT_LUA void mmLuaState_AddLuaLoader(struct lua_State* L, lua_CFunction func)
{
    size_t _rawLen = 0;
    int i = 0;
    assert(NULL != func && "lua_loader func is a null.");
    // stack content after the invoking of the function
    // get loader table
    lua_getglobal(L, "package");                                  /* L: package */
    lua_getfield(L, -1, MM_LUA_SEARCHERS);                        /* L: package, loaders */
    // insert loader into index 2
    lua_pushcfunction(L, func);                                   /* L: package, loaders, func */
    _rawLen = lua_rawlen(L, -2);
    for (i = (int)(_rawLen + 1); i > 2; --i)
    {
        lua_rawgeti(L, -2, i - 1);                                /* L: package, loaders, func, function */
        // we call lua_rawgeti, so the loader table now is at -3
        lua_rawseti(L, -3, i);                                    /* L: package, loaders, func */
    }
    lua_rawseti(L, -2, 2);                                        /* L: package, loaders */

    // set loaders into package
    lua_setfield(L, -2, MM_LUA_SEARCHERS);                        /* L: package */
    lua_pop(L, 1);
}
// load buffer not pcall.
MM_EXPORT_LUA int mmLuaState_LoadLuaBufferNotPcall(struct lua_State* L, mmUInt8_t* buffer, size_t offset, size_t length)
{
    int code = -1;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        if (NULL == buffer)
        {
            mmLogger_LogE(gLogger, "%s %d can not load a null lua buffer.", __FUNCTION__, __LINE__);
            break;
        }
        if (0 != luaL_loadbuffer(L, (char*)(buffer + offset), length, NULL))
        {
            mmLogger_LogE(gLogger, "%s", lua_tostring(L, -1));
            lua_pop(L, 1);
            break;
        }
        code = 0;
    } while (0);
    return code;
}
// load buffer and pcall.
MM_EXPORT_LUA int mmLuaState_LoadLuaBufferAndPcall(struct lua_State* L, mmUInt8_t* buffer, size_t offset, size_t length)
{
    int code = -1;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        if (NULL == buffer)
        {
            mmLogger_LogE(gLogger, "%s %d can not load a null lua buffer.", __FUNCTION__, __LINE__);
            break;
        }
        if (0 != luaL_loadbuffer(L, (char*)(buffer + offset), length, NULL))
        {
            mmLogger_LogE(gLogger, "%s", lua_tostring(L, -1));
            lua_pop(L, 1);
            break;
        }
        //ok do the buffer now.
        code = lua_pcall(L, 0, LUA_MULTRET, 0);
    } while (0);
    return code;
}
MM_EXPORT_LUA void mmLuaState_ReloadModule(struct lua_State* L, const char* modulename)
{
    struct mmString filename;
    mmString_Init(&filename);
    mmLuaState_FileNameByModule(modulename, &filename);
    mmLuaState_LoadFileNotPcall(L, mmString_CStr(&filename));
    mmString_Destroy(&filename);
}
// load file not pcall.
MM_EXPORT_LUA void mmLuaState_LoadFileNotPcall(struct lua_State* L, const char* file_name)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmFileContext* file_context = mmLuaState_GetGlobalFileContext(L);
    struct mmByteBuffer byte_buffer;
    mmFileContext_AcquireFileByteBuffer(file_context, file_name, &byte_buffer);
    if (0 < byte_buffer.length)
    {
        if (0 == mmLuaState_LoadLuaBufferNotPcall(L, byte_buffer.buffer, byte_buffer.offset, byte_buffer.length))
        {
            mmLogger_LogI(gLogger, "%s %d load script file:%s.", __FUNCTION__, __LINE__, file_name);
        }
        else
        {
            mmLogger_LogE(gLogger, "%s %d error loading module:%s from file:%s error:\n\t%s.",
                          __FUNCTION__,
                          __LINE__,
                          lua_tostring(L, 1),
                          file_name,
                          lua_tostring(L, -1));
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d can not get file data from file:%s.", __FUNCTION__, __LINE__, file_name);
    }
    mmFileContext_ReleaseFileByteBuffer(file_context, &byte_buffer);
}
// load file and pcall.
MM_EXPORT_LUA void mmLuaState_LoadFileAndPcall(struct lua_State* L, const char* file_name)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmFileContext* file_context = mmLuaState_GetGlobalFileContext(L);
    struct mmByteBuffer byte_buffer;
    mmFileContext_AcquireFileByteBuffer(file_context, file_name, &byte_buffer);
    if (0 < byte_buffer.length)
    {
        if (0 == mmLuaState_LoadLuaBufferAndPcall(L, byte_buffer.buffer, byte_buffer.offset, byte_buffer.length))
        {
            mmLogger_LogI(gLogger, "%s %d load script file:%s.", __FUNCTION__, __LINE__, file_name);
        }
        else
        {
            mmLogger_LogE(gLogger, "%s %d error loading module:%s from file:%s error:\n\t%s.",
                          __FUNCTION__,
                          __LINE__,
                          lua_tostring(L, 1),
                          file_name,
                          lua_tostring(L, -1));
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d can not get file data from file:%s.", __FUNCTION__, __LINE__, file_name);
    }
    mmFileContext_ReleaseFileByteBuffer(file_context, &byte_buffer);
}
MM_EXPORT_LUA void mmLuaState_SetGlobalFileContext(struct lua_State* L, struct mmFileContext* context)
{
    lua_pushlightuserdata(L, context);
    lua_setglobal(L, MM_LUA_FILE_CONTEXT);
}
MM_EXPORT_LUA struct mmFileContext* mmLuaState_GetGlobalFileContext(struct lua_State* L)
{
    struct mmFileContext* context = NULL;
    do
    {
        int n_top = lua_gettop(L);
        lua_getglobal(L, MM_LUA_FILE_CONTEXT);
        if (n_top == lua_gettop(L) || !lua_islightuserdata(L, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid global: %s", __FUNCTION__, __LINE__, MM_LUA_FILE_CONTEXT);
            break;
        }
        context = (struct mmFileContext*)lua_touserdata(L, -1);
        lua_pop(L, 1);
    } while (0);
    return context;
}

MM_EXPORT_LUA void mmLuaContext_Init(struct mmLuaContext* p)
{
    p->state = luaL_newstate();
    luaL_openlibs(p->state);
    mmLuaState_AddLuaLoader(p->state, __static_mmLuaContext_Loader);
    luaL_requiref(p->state, "mm/mmLogger", __static_mmLuaContext_LuaopenLogger, 0);
    luaL_requiref(p->state, "mm/mmLuaContext", __static_mmLuaContext_LuaopenLuaContext, 0);
}
MM_EXPORT_LUA void mmLuaContext_Destroy(struct mmLuaContext* p)
{
    if (NULL != p->state)
    {
        lua_close(p->state);
        p->state = NULL;
    }
}
