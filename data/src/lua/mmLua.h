/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef  __mmLua_h__
#define  __mmLua_h__

#include "core/mmPlatform.h"

#include "lua/mmLuaContext.h"

#include <string>

#include "lua/mmLuaExport.h"

namespace mm
{
    class MM_EXPORT_LUA mmLua
    {
    public:
        mmLua(struct lua_State*& L);
        virtual ~mmLua();
    public:
        //
        struct lua_State* State() const;
        //
        void AddSearchPath(const std::string& path);
        bool LoadLuaBuffer(const char* buffer, size_t bufflen);
        bool Require(const std::string& fileName);
        //
        void ClearStack();
        //
        bool GetGlobal(const char* field, bool& val);
        bool GetGlobal(const char* field, int& val);
        bool GetGlobal(const char* field, float& val);
        bool GetGlobal(const char* field, double& val);
        bool GetGlobal(const char* field, std::string& val);

        bool SetGlobal(const char* field, bool val);
        bool SetGlobal(const char* field, int val);
        bool SetGlobal(const char* field, float val);
        bool SetGlobal(const char* field, double val);
        bool SetGlobal(const char* field, const std::string& val);
        bool SetGlobal(const char* field, const char* val);
        //
        bool SetField();
        bool SetField(const char* field, bool val);
        bool SetField(const char* field, int val);
        bool SetField(const char* field, float val);
        bool SetField(const char* field, double val);
        bool SetField(const char* field, const std::string& val);
        bool SetField(const char* field, const char* val);

        bool SetField(int field, bool val);
        bool SetField(int field, int val);
        bool SetField(int field, float val);
        bool SetField(int field, double val);
        bool SetField(int field, const std::string& val);
        bool SetField(int field, const char* val);

        bool GetField(const char* field, bool& val);
        bool GetField(const char* field, int& val);
        bool GetField(const char* field, float& val);
        bool GetField(const char* field, double& val);
        bool GetField(const char* field, std::string& val);

        bool GetField(int field, bool& val);
        bool GetField(int field, int& val);
        bool GetField(int field, float& val);
        bool GetField(int field, double& val);
        bool GetField(int field, std::string& val);
        //
        bool Call(const std::string& code);
        bool Call(const std::string& func, int narg, int nres = 0);
        bool Call(const std::string& ns, const std::string& func, int narg, int nres = 0);
        //
        bool GotoGlobal(const std::string& ns);
        bool GotoTable(const std::string& t);
        bool GotoTable(int t);
        //
        bool Push();
        bool Push(char val);
        bool Push(unsigned char val);
        bool Push(short val);
        bool Push(unsigned short val);
        bool Push(int val);
        bool Push(unsigned int val);
        bool Push(long val);
        bool Push(unsigned long val);
        bool Push(bool val);
        bool Push(float val);
        bool Push(double val);
        bool Push(const std::string& val);
        bool Push(const char* val);
        //
        bool Pop();
        bool Pop(char& val);
        bool Pop(unsigned char& val);
        bool Pop(short& val);
        bool Pop(unsigned short& val);
        bool Pop(int& val);
        bool Pop(unsigned int& val);
        bool Pop(long& val);
        bool Pop(unsigned long& val);
        bool Pop(bool& val);
        bool Pop(float& val);
        bool Pop(double& val);
        bool Pop(std::string& val);
    private:
        void ShowPopError();
        void ShowStackError();
    private:
        struct lua_State*& d_state;//weak ref. 
    };
}
#endif//__mmLua_h__

