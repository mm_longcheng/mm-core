/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLuaPool.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "container/mmListVpt.h"

MM_EXPORT_LUA void mmLuaUnit_Init(struct mmLuaUnit* p)
{
    p->state = NULL;
    mmSpinlock_Init(&p->locker, NULL);
}
MM_EXPORT_LUA void mmLuaUnit_Destroy(struct mmLuaUnit* p)
{
    p->state = NULL;
    mmSpinlock_Destroy(&p->locker);
}
MM_EXPORT_LUA void mmLuaUnit_Lock(struct mmLuaUnit* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_LUA void mmLuaUnit_Unlock(struct mmLuaUnit* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_LUA void mmLuaPool_Init(struct mmLuaPool* p)
{
    p->state = NULL;
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_create(&p->thread_key, NULL);
    pthread_rwlock_init(&p->locker, NULL);
}
MM_EXPORT_LUA void mmLuaPool_Destroy(struct mmLuaPool* p)
{
    mmLuaPool_Clear(p);
    //
    p->state = NULL;
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_delete(p->thread_key);
    pthread_rwlock_destroy(&p->locker);
}
MM_EXPORT_LUA void mmLuaPool_SetMainState(struct mmLuaPool* p, struct lua_State* L)
{
    p->state = L;
}
MM_EXPORT_LUA struct mmLuaUnit* mmLuaPool_ThreadInstance(struct mmLuaPool* p)
{
    // not lock here quick get.
    struct mmLuaUnit* _e = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (NULL == lvp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        _e = (struct mmLuaUnit*)mmMalloc(sizeof(struct mmLuaUnit));
        // not need lock
        _e->state = lua_newthread(p->state);
        lvp->v = _e;
        pthread_rwlock_wrlock(&p->locker);
        mmList_Add(&lvp->n, &p->list);
        pthread_rwlock_unlock(&p->locker);
        mmLogger_LogI(gLogger, "%s %d state:%p  from main state:%p.", __FUNCTION__, __LINE__, _e->state, p->state);
    }
    else
    {
        _e = (struct mmLuaUnit*)(lvp->v);
    }
    return _e;
}
MM_EXPORT_LUA void mmLuaPool_Clear(struct mmLuaPool* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmLuaUnit* _e = NULL;
    pthread_rwlock_wrlock(&p->locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);        
        _e = (struct mmLuaUnit*)(lvp->v);
        mmLuaUnit_Lock(_e);
        // when we delete elem we must lock it and get the handler write permissions.
        mmList_Del(curr);
        mmLuaUnit_Unlock(_e);
        mmLuaUnit_Destroy(_e);
        mmFree(_e);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    pthread_rwlock_unlock(&p->locker);
}
