/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLua.h"
#include "core/mmLogger.h"
#include <stdarg.h>
namespace mm
{
    //static const char* __static_mmLua_GetType(struct lua_State* L, int idx)
    //{
    //    static const char* LUA_TYPE_NAMES[] =
    //    {
    //        "LUA_TNONE",          //        (-1)
    //
    //        "LUA_TNIL",           //        0
    //        "LUA_TBOOLEAN",       //        1
    //        "LUA_TLIGHTUSERDATA", //        2
    //        "LUA_TNUMBER",        //        3
    //        "LUA_TSTRING",        //        4
    //        "LUA_TTABLE",         //        5
    //        "LUA_TFUNCTION",      //        6
    //        "LUA_TUSERDATA",      //        7
    //        "LUA_TTHREAD",        //        8
    //    };
    //
    //    int nType = lua_type(L, idx);
    //    if (nType >= LUA_TNONE &&
    //        nType <= LUA_TTHREAD)
    //    {
    //        return LUA_TYPE_NAMES[nType + 1];
    //    }
    //    else
    //    {
    //        return "LUA_TXXXX";
    //    }
    //}

    mmLua::mmLua(struct lua_State*& L)
        : d_state(L)
    {

    }

    mmLua::~mmLua()
    {

    }

    struct lua_State* mmLua::State()const
    {
        return this->d_state;
    }

    void mmLua::AddSearchPath(const std::string& path)
    {
        mmLuaState_AddSearchPath(this->d_state, path.c_str());
    }

    bool mmLua::LoadLuaBuffer(const char* buffer, size_t length)
    {
        return 0 == mmLuaState_LoadLuaBufferAndPcall(this->d_state, (mmUInt8_t*)buffer, 0, (mmUInt32_t)length);
    }

    bool mmLua::Require(const std::string& fileName)
    {
        lua_pushstring(this->d_state, fileName.c_str());
        lua_getglobal(this->d_state, "require");
        lua_insert(this->d_state, -2);
        if (lua_pcall(this->d_state, 1, 0, 0) != 0)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d filename:%s, error.", __FUNCTION__, __LINE__, fileName.c_str());
            this->ShowStackError();
            return false;
        }
        return true;
    }

    void mmLua::ClearStack()
    {
        lua_settop(this->d_state, 0);
    }

    bool mmLua::Call(const std::string& code)
    {
        if (0 == luaL_loadstring(this->d_state, code.c_str()))
        {
            if (0 == lua_pcall(this->d_state, 0, 0, 0))
            {
                return true;
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "%s %d invalid code, runtime error: %s > %s",
                              __FUNCTION__,
                              __LINE__,
                              code.c_str(),
                              lua_tostring(this->d_state, -1));
                return false;
            }
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid code, syntax error: %s > %s",
                          __FUNCTION__,
                          __LINE__,
                          code.c_str(),
                          lua_tostring(this->d_state, -1));
            return false;
        }
    }

    bool mmLua::Call(const std::string& func, int narg, int nres /* = 0 */)
    {
        if (narg > lua_gettop(this->d_state))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d input arg lost, is: %d", __FUNCTION__, __LINE__, narg);
            return false;
        }
        lua_getglobal(this->d_state, func.c_str());
        if (!lua_isfunction(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid function name : %s", __FUNCTION__, __LINE__, func.c_str());
            return false;
        }

        if (0 < narg)
        {
            lua_insert(this->d_state, -(narg + 1));
        }

        if (0 != lua_pcall(this->d_state, narg, nres, 0))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d pcall error func:%s.", __FUNCTION__, __LINE__, func.c_str());
            this->ShowStackError();
            return false;
        }
        return true;
    }

    bool mmLua::Call(const std::string& ns, const std::string& func, int narg, int nres /* = 0 */)
    {
        if (narg > lua_gettop(this->d_state))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d input arg lost,is: %d", __FUNCTION__, __LINE__, narg);
            return false;
        }

        lua_getglobal(this->d_state, ns.c_str());
        if (!lua_istable(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d null function namespace: %s", __FUNCTION__, __LINE__, ns.c_str());
            return false;
        }

        //
        //  try to get the funtion from ns table
        lua_pushstring(this->d_state, func.c_str());
        lua_gettable(this->d_state, -2);    // table on -2
        if (!lua_isfunction(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid function name : %s", __FUNCTION__, __LINE__, func.c_str());
            return false;
        }

        //
        //  remove the ns table
        lua_remove(this->d_state, -2);

        if (0 < narg)
        {
            lua_insert(this->d_state, -(narg + 1));
        }

        if (0 != lua_pcall(this->d_state, narg, nres, 0))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d pcall error func:%s.", __FUNCTION__, __LINE__, func.c_str());
            this->ShowStackError();
            return false;
        }

        return true;
    }

    bool mmLua::Push()
    {
        lua_pushnil(this->d_state);
        return true;
    }

    bool mmLua::Push(char val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(unsigned char val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(short val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(unsigned short val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(int val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(unsigned int val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(long val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(unsigned long val)
    {
        lua_pushinteger(this->d_state, val);
        return true;
    }

    bool mmLua::Push(bool val)
    {
        lua_pushboolean(this->d_state, val);
        return true;
    }

    bool mmLua::Push(float val)
    {
        lua_pushnumber(this->d_state, val);
        return true;
    }

    bool mmLua::Push(double val)
    {
        lua_pushnumber(this->d_state, val);
        return true;
    }

    bool mmLua::Push(const std::string& val)
    {
        lua_pushstring(this->d_state, val.c_str());
        return true;
    }

    bool mmLua::Push(const char* val)
    {
        lua_pushstring(this->d_state, val);
        return true;
    }

    bool mmLua::Pop()
    {
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::Pop(char& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (char)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(unsigned char& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (unsigned char)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(short& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (short)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(unsigned short& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (unsigned short)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(int& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (int)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(unsigned int& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (unsigned int)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(long& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (long)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(unsigned long& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (unsigned long)lua_tointeger(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(bool& val)
    {
        if (lua_isboolean(this->d_state, -1))
        {
            val = (0 != lua_toboolean(this->d_state, -1));
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(float& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (float)lua_tonumber(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(double& val)
    {
        if (lua_isnumber(this->d_state, -1))
        {
            val = (double)lua_tonumber(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::Pop(std::string& val)
    {
        if (lua_isstring(this->d_state, -1))
        {
            val = lua_tostring(this->d_state, -1);
            lua_pop(this->d_state, 1);
            return true;
        }
        else
        {
            this->ShowPopError();
            return false;
        }
    }

    bool mmLua::GotoGlobal(const std::string& ns)
    {
        lua_getglobal(this->d_state, ns.c_str());
        return true;
    }

    bool mmLua::GotoTable(const std::string& t)
    {
        if (!lua_istable(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d the parent is null table.", __FUNCTION__, __LINE__);
            return false;
        }
        lua_pushstring(this->d_state, t.c_str());
        lua_gettable(this->d_state, -2);    // table on -2
        return true;
    }

    bool mmLua::GotoTable(int t)
    {
        if (!lua_istable(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d the parent is null table.", __FUNCTION__, __LINE__);
            return false;
        }
        lua_pushinteger(this->d_state, t);
        lua_gettable(this->d_state, -2);    // table on -2
        return true;
    }

    bool mmLua::SetField()
    {
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(const char* field, bool val)
    {
        lua_pushstring(this->d_state, field);
        lua_pushboolean(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(const char* field, int val)
    {
        lua_pushstring(this->d_state, field);
        lua_pushinteger(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(const char* field, float val)
    {
        lua_pushstring(this->d_state, field);
        lua_pushnumber(this->d_state, double(val));
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(const char* field, double val)
    {
        lua_pushstring(this->d_state, field);
        lua_pushnumber(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(const char* field, const std::string& val)
    {
        lua_pushstring(this->d_state, field);
        lua_pushstring(this->d_state, val.c_str());
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(const char* field, const char* val)
    {
        lua_pushstring(this->d_state, field);
        lua_pushstring(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(int field, bool val)
    {
        lua_pushinteger(this->d_state, field);
        lua_pushboolean(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(int field, int val)
    {
        lua_pushinteger(this->d_state, field);
        lua_pushinteger(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(int field, float val)
    {
        lua_pushinteger(this->d_state, field);
        lua_pushnumber(this->d_state, double(val));
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(int field, double val)
    {
        lua_pushnumber(this->d_state, field);
        lua_pushnumber(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(int field, const std::string& val)
    {
        lua_pushinteger(this->d_state, field);
        lua_pushstring(this->d_state, val.c_str());
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::SetField(int field, const char* val)
    {
        lua_pushinteger(this->d_state, field);
        lua_pushstring(this->d_state, val);
        lua_settable(this->d_state, -3);
        return true;
    }

    bool mmLua::GetField(const char* field, bool& val)
    {
        if (NULL == field || !lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushstring(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = 0 != lua_toboolean(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(const char* field, int& val)
    {
        if (NULL == field || !lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushstring(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (int)lua_tointeger(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(const char* field, float& val)
    {
        if (NULL == field || !lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushstring(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (float)lua_tonumber(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(const char* field, double& val)
    {
        if (NULL == field || !lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushstring(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (double)lua_tonumber(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(const char* field, std::string& val)
    {
        if (NULL == field || !lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushstring(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isstring(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = lua_tostring(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(int field, bool& val)
    {
        if (!lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushinteger(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %d", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = 0 != lua_toboolean(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(int field, int& val)
    {
        if (!lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushinteger(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %d", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (int)lua_tointeger(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(int field, float& val)
    {
        if (!lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushinteger(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %d", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (float)lua_tonumber(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(int field, double& val)
    {
        if (!lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushinteger(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %d", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (double)lua_tonumber(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetField(int field, std::string& val)
    {
        if (!lua_istable(this->d_state, -1))
        {
            return false;
        }

        lua_pushinteger(this->d_state, field);
        lua_gettable(this->d_state, -2);    // table on -2

        if (!lua_isstring(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid table field: %d", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = lua_tostring(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetGlobal(const char* field, bool& val)
    {
        int nTop = lua_gettop(this->d_state);
        lua_getglobal(this->d_state, field);
        if (nTop == lua_gettop(this->d_state) || !lua_isboolean(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid global: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = 0 != lua_toboolean(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetGlobal(const char* field, int& val)
    {
        int nTop = lua_gettop(this->d_state);
        lua_getglobal(this->d_state, field);
        if (nTop == lua_gettop(this->d_state) || !lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid global: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (int)lua_tointeger(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetGlobal(const char* field, float& val)
    {
        int nTop = lua_gettop(this->d_state);
        lua_getglobal(this->d_state, field);

        if (nTop == lua_gettop(this->d_state) || !lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid global: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (float)lua_tonumber(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetGlobal(const char* field, double& val)
    {
        int nTop = lua_gettop(this->d_state);
        lua_getglobal(this->d_state, field);

        if (nTop == lua_gettop(this->d_state) || !lua_isnumber(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid global: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = (double)lua_tonumber(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::GetGlobal(const char* field, std::string& val)
    {
        int nTop = lua_gettop(this->d_state);
        lua_getglobal(this->d_state, field);

        if (nTop == lua_gettop(this->d_state) || !lua_isstring(this->d_state, -1))
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid global: %s", __FUNCTION__, __LINE__, field);
            return false;
        }

        val = lua_tostring(this->d_state, -1);
        lua_pop(this->d_state, 1);
        return true;
    }

    bool mmLua::SetGlobal(const char* field, bool val)
    {
        lua_pushboolean(this->d_state, val);
        lua_setglobal(this->d_state, field);
        return true;
    }

    bool mmLua::SetGlobal(const char* field, int val)
    {
        lua_pushinteger(this->d_state, val);
        lua_setglobal(this->d_state, field);
        return true;
    }

    bool mmLua::SetGlobal(const char* field, float val)
    {
        lua_pushnumber(this->d_state, double(val));
        lua_setglobal(this->d_state, field);
        return true;
    }

    bool mmLua::SetGlobal(const char* field, double val)
    {
        lua_pushnumber(this->d_state, val);
        lua_setglobal(this->d_state, field);
        return true;
    }

    bool mmLua::SetGlobal(const char* field, const std::string& val)
    {
        lua_pushstring(this->d_state, val.c_str());
        lua_setglobal(this->d_state, field);
        return true;
    }

    bool mmLua::SetGlobal(const char* field, const char* val)
    {
        lua_pushstring(this->d_state, val);
        lua_setglobal(this->d_state, field);
        return true;
    }

    void mmLua::ShowPopError()
    {
        int t = lua_type(this->d_state, -1);
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d pop %s failed.", __FUNCTION__, __LINE__, lua_typename(this->d_state, t));
    }

    void mmLua::ShowStackError()
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s", lua_tostring(this->d_state, -1));
    }

}

