/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLuaPool_h__
#define __mmLuaPool_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"

#include "lua/mmLuaContext.h"

#include <pthread.h>

#include "lua/mmLuaExport.h"

#include "core/mmPrefix.h"

struct mmLuaUnit
{
    struct lua_State* state;
    mmAtomic_t locker;
};
MM_EXPORT_LUA void mmLuaUnit_Init(struct mmLuaUnit* p);
MM_EXPORT_LUA void mmLuaUnit_Destroy(struct mmLuaUnit* p);

MM_EXPORT_LUA void mmLuaUnit_Lock(struct mmLuaUnit* p);
MM_EXPORT_LUA void mmLuaUnit_Unlock(struct mmLuaUnit* p);

struct mmLuaPool
{
    // weak ref.
    struct lua_State* state;
    // silk strong ref list.
    struct mmListHead list;
    // thread key.
    pthread_key_t thread_key;
    // rw locker.
    pthread_rwlock_t locker;
};
MM_EXPORT_LUA void mmLuaPool_Init(struct mmLuaPool* p);
MM_EXPORT_LUA void mmLuaPool_Destroy(struct mmLuaPool* p);

MM_EXPORT_LUA void mmLuaPool_SetMainState(struct mmLuaPool* p, struct lua_State* L);

MM_EXPORT_LUA struct mmLuaUnit* mmLuaPool_ThreadInstance(struct mmLuaPool* p);
MM_EXPORT_LUA void mmLuaPool_Clear(struct mmLuaPool* p);

#include "core/mmSuffix.h"

#endif//__mmLuaPool_h__
