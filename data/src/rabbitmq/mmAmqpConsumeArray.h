/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAmqpConsumeArray_h__
#define __mmAmqpConsumeArray_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"
#include "core/mmList.h"
#include "core/mmTime.h"

#include "net/mmSockaddr.h"
#include "net/mmPacket.h"

#include "container/mmRbtreeU32.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"

#include <pthread.h>

#include "rabbitmq/mmAmqpExchange.h"
#include "rabbitmq/mmAmqpQueue.h"
#include "rabbitmq/mmAmqpConsume.h"

#include "rabbitmq/mmAmqpExport.h"

#include "core/mmPrefix.h"

typedef void(*mmAmqpConsumeArrayHandleFunc)(void* obj, void* u, struct mmPacket* pack);

struct mmAmqpConsumeArrayCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_RABBITMQ void mmAmqpConsumeArrayCallback_Init(struct mmAmqpConsumeArrayCallback* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArrayCallback_Destroy(struct mmAmqpConsumeArrayCallback* p);

struct mmAmqpConsumeArray
{
    struct mmAmqpExchange amqp_exchange;
    struct mmAmqpQueue amqp_queue;
    struct mmString routing_key;
    struct mmString index_key;
    struct mmAmqpConsumeArrayCallback callback;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    mmAtomic_t rbtree_locker;
    mmUInt8_t consume_no_local;
    mmUInt8_t consume_no_ack;
    mmUInt8_t consume_exclusive;
    // MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME
    mmMSec_t sleep_empty_key_timeout;
    // MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME
    mmMSec_t sleep_empty_val_timeout;
    // MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME
    mmMSec_t sleep_error_ctx_timeout;
    // MM_AMQP_CONSUME_ERROR_POP_TIMEOUT
    mmMSec_t pop_timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
    //
    struct mmString vhost;
    struct mmString username;
    struct mmString password;
    struct mmString node;
    mmUShort_t port;
    int channel_max;
    int frame_max;
    int heartbeat;
    // connect time out ms.default is 3s(3000ms)
    mmMSec_t conn_timeout;
    // try bind and listen times.default is MM_AMQP_CONN_TRYTIMES.
    mmUInt32_t trytimes;
    amqp_channel_t channel;

    // length. default is 0.
    mmUInt32_t length;
    // array pointer.
    struct mmAmqpConsume** arrays;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t arrays_locker;
};

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Init(struct mmAmqpConsumeArray* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Destroy(struct mmAmqpConsumeArray* p);

// poll thread number.
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetThreadNumber(struct mmAmqpConsumeArray* p, mmUInt32_t thread_number);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetContext(struct mmAmqpConsumeArray* p, void* u);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetPopTimeout(struct mmAmqpConsumeArray* p, mmMSec_t pop_timeout);

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetSleepTimeout(
    struct mmAmqpConsumeArray* p,
    mmMSec_t empty_key_timeout,
    mmMSec_t empty_val_timeout,
    mmMSec_t error_ctx_timeout);

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetConsumeAttr(
    struct mmAmqpConsumeArray* p,
    mmUInt8_t no_local,
    mmUInt8_t no_ack,
    mmUInt8_t exclusive);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetExchange(struct mmAmqpConsumeArray* p, struct mmAmqpExchange* exchange);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetQueue(struct mmAmqpConsumeArray* p, struct mmAmqpQueue* queue);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetRoutingKey(struct mmAmqpConsumeArray* p, const char* routing_key);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetIndexKey(struct mmAmqpConsumeArray* p, const char* index_key);

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetDefaultCallback(
    struct mmAmqpConsumeArray* p,
    struct mmAmqpConsumeArrayCallback* default_callback);

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetHandle(
    struct mmAmqpConsumeArray* p,
    mmUInt32_t id,
    mmAmqpConsumeArrayHandleFunc callback);

// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetParameters(struct mmAmqpConsumeArray* p, const char* parameters);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetRemote(struct mmAmqpConsumeArray* p, const char* node, mmUShort_t port);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetVhost(struct mmAmqpConsumeArray* p, const char* vhost);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetAccount(struct mmAmqpConsumeArray* p, const char* username, const char* password);

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetConnAttr(
    struct mmAmqpConsumeArray* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel);

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetConnTimeout(
    struct mmAmqpConsumeArray* p,
    mmMSec_t conn_timeout,
    mmUInt32_t trytimes);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_CopyRbtree(struct mmAmqpConsumeArray* p, struct mmAmqpConsume* amqp_consume);

// must join before set length.
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetLength(struct mmAmqpConsumeArray* p, mmUInt32_t length);
MM_EXPORT_RABBITMQ mmUInt32_t mmAmqpConsumeArray_GetLength(struct mmAmqpConsumeArray* p);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Clear(struct mmAmqpConsumeArray* p);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_ApplyBroken(struct mmAmqpConsumeArray* p);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Start(struct mmAmqpConsumeArray* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Interrupt(struct mmAmqpConsumeArray* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Shutdown(struct mmAmqpConsumeArray* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Join(struct mmAmqpConsumeArray* p);

#include "core/mmSuffix.h"

#endif//__mmAmqpConsumeArray_h__
