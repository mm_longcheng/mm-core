/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAmqpPublishArray.h"
#include "mmAmqpConn.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"
#include "core/mmThread.h"
#include "core/mmTimeCache.h"
#include "core/mmTimeval.h"
#include "core/mmParameters.h"
#include "container/mmListVpt.h"

#include "net/mmBufferPacket.h"

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_Init(struct mmAmqpPublishArray* p)
{
    mmAmqpExchange_Init(&p->amqp_exchange);
    mmAmqpPublish_Init(&p->amqp_publish);
    mmString_Init(&p->index_key);
    p->publish_trytimes = MM_AMQP_PUBLISH_ARRAY_TRYTIMES;
    //
    MM_LIST_INIT_HEAD(&p->list);
    mmString_Init(&p->vhost);
    mmString_Init(&p->username);
    mmString_Init(&p->password);
    mmString_Init(&p->node);
    mmSpinlock_Init(&p->list_locker, NULL);
    mmSpinlock_Init(&p->index_key_locker, NULL);
    pthread_key_create(&p->thread_key, NULL);
    p->port = 0;
    p->channel_max = 0;
    p->frame_max = 131072;
    p->heartbeat = 0;
    p->conn_timeout = MM_AMQP_CONN_CONN_TIMEOUT;
    p->trytimes = MM_AMQP_CONN_TRYTIMES;
    p->channel = 1;
    //
    mmString_Assigns(&p->vhost, "/");
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_Destroy(struct mmAmqpPublishArray* p)
{
    mmAmqpPublishArray_Clear(p);
    //
    mmAmqpExchange_Destroy(&p->amqp_exchange);
    mmAmqpPublish_Destroy(&p->amqp_publish);
    mmString_Destroy(&p->index_key);
    p->publish_trytimes = MM_AMQP_PUBLISH_ARRAY_TRYTIMES;
    //
    MM_LIST_INIT_HEAD(&p->list);
    mmString_Destroy(&p->vhost);
    mmString_Destroy(&p->username);
    mmString_Destroy(&p->password);
    mmString_Destroy(&p->node);
    mmSpinlock_Destroy(&p->list_locker);
    mmSpinlock_Destroy(&p->index_key_locker);
    pthread_key_delete(p->thread_key);
    p->port = 0;
    p->channel_max = 0;
    p->frame_max = 131072;
    p->heartbeat = 0;
    p->conn_timeout = MM_AMQP_CONN_CONN_TIMEOUT;
    p->trytimes = MM_AMQP_CONN_TRYTIMES;
    p->channel = 1;
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetExchange(struct mmAmqpPublishArray* p, struct mmAmqpExchange* exchange)
{
    mmString_Assign(&p->amqp_exchange.exchange_name, &exchange->exchange_name);
    mmString_Assign(&p->amqp_exchange.exchange_type, &exchange->exchange_type);
    p->amqp_exchange.exchange_passive = exchange->exchange_passive;
    p->amqp_exchange.exchange_durable = exchange->exchange_durable;
    p->amqp_exchange.exchange_auto_delete = exchange->exchange_auto_delete;
    p->amqp_exchange.exchange_internal = exchange->exchange_internal;
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetPublish(struct mmAmqpPublishArray* p, struct mmAmqpPublish* publish)
{
    mmString_Assign(&p->amqp_publish.publish_routingkey, &publish->publish_routingkey);
    p->amqp_publish.publish_mandatory = publish->publish_mandatory;
    p->amqp_publish.publish_immediate = publish->publish_immediate;
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetPublishTrytimes(struct mmAmqpPublishArray* p, mmUInt32_t publish_trytimes)
{
    p->publish_trytimes = publish_trytimes;
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetParameters(struct mmAmqpPublishArray* p, const char* parameters)
{
    char node[MM_NODE_NAME_LENGTH] = { 0 };
    mmUShort_t port = 0;
    //
    mmParameters_Client(parameters, node, &port);
    //
    mmAmqpPublishArray_SetRemote(p, node, port);
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetRemote(struct mmAmqpPublishArray* p, const char* node, mmUShort_t port)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmString_Assigns(&p->node, node);
    p->port = port;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmAmqpConn* unit = (struct mmAmqpConn*)(e->v);
        mmAmqpConn_Lock(unit);
        mmAmqpConn_SetRemote(unit, mmString_CStr(&p->node), p->port);
        mmAmqpConn_Unlock(unit);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetVhost(struct mmAmqpPublishArray* p, const char* vhost)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmString_Assigns(&p->vhost, vhost);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmAmqpConn* unit = (struct mmAmqpConn*)(e->v);
        mmAmqpConn_Lock(unit);
        mmAmqpConn_SetVhost(unit, mmString_CStr(&p->vhost));
        mmAmqpConn_Unlock(unit);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetAccount(struct mmAmqpPublishArray* p, const char* username, const char* password)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmString_Assigns(&p->username, username);
    mmString_Assigns(&p->password, password);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmAmqpConn* unit = (struct mmAmqpConn*)(e->v);
        mmAmqpConn_Lock(unit);
        mmAmqpConn_SetAccount(unit, mmString_CStr(&p->username), mmString_CStr(&p->password));
        mmAmqpConn_Unlock(unit);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpPublishArray_SetConnAttr(
    struct mmAmqpPublishArray* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->channel_max = channel_max;
    p->frame_max = frame_max;
    p->heartbeat = heartbeat;
    p->channel = channel;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmAmqpConn* unit = (struct mmAmqpConn*)(e->v);
        mmAmqpConn_Lock(unit);
        mmAmqpConn_SetConnAttr(unit, p->channel_max, p->frame_max, p->heartbeat, p->channel);
        mmAmqpConn_Unlock(unit);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpPublishArray_SetConnTimeout(
    struct mmAmqpPublishArray* p,
    mmMSec_t conn_timeout,
    mmUInt32_t trytimes)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->conn_timeout = conn_timeout;
    p->trytimes = trytimes;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmAmqpConn* unit = (struct mmAmqpConn*)(e->v);
        mmAmqpConn_Lock(unit);
        mmAmqpConn_SetConnTimeout(unit, p->conn_timeout, p->trytimes);
        mmAmqpConn_Unlock(unit);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetIndexKey(struct mmAmqpPublishArray* p, const char* index_key)
{
    mmSpinlock_Lock(&p->index_key_locker);
    mmString_Assigns(&p->index_key, index_key);
    mmSpinlock_Unlock(&p->index_key_locker);
}
// get current thread instance.
MM_EXPORT_RABBITMQ struct mmAmqpConn* mmAmqpPublishArray_ThreadInstance(struct mmAmqpPublishArray* p)
{
    struct mmAmqpConn* amqp_conn = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (NULL == lvp)
    {
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        amqp_conn = (struct mmAmqpConn*)mmMalloc(sizeof(struct mmAmqpConn));
        mmAmqpConn_Init(amqp_conn);
        //
        mmAmqpConn_SetRemote(amqp_conn, mmString_CStr(&p->node), p->port);
        mmAmqpConn_SetVhost(amqp_conn, mmString_CStr(&p->vhost));
        mmAmqpConn_SetAccount(amqp_conn, mmString_CStr(&p->username), mmString_CStr(&p->password));
        mmAmqpConn_SetConnAttr(amqp_conn, p->channel_max, p->frame_max, p->heartbeat, p->channel);
        mmAmqpConn_SetConnTimeout(amqp_conn, p->conn_timeout, p->trytimes);
        //
        lvp->v = amqp_conn;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        amqp_conn = (struct mmAmqpConn*)(lvp->v);
    }
    return amqp_conn;
}
// clear all instance.
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_Clear(struct mmAmqpPublishArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmAmqpConn* amqp_conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);        
        amqp_conn = (struct mmAmqpConn*)(lvp->v);
        mmAmqpConn_Lock(amqp_conn);
        mmList_Del(curr);
        mmAmqpConn_Detach(amqp_conn);
        mmAmqpConn_Unlock(amqp_conn);
        mmAmqpConn_Destroy(amqp_conn);
        mmFree(amqp_conn);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_ApplyBroken(struct mmAmqpPublishArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmAmqpConn* amqp_conn = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        amqp_conn = (struct mmAmqpConn*)(lvp->v);
        mmAmqpConn_Lock(amqp_conn);
        mmAmqpConn_ApplyBroken(amqp_conn);
        mmAmqpConn_Unlock(amqp_conn);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_RABBITMQ int
mmAmqpPublishArray_BasicPublish(
    struct mmAmqpPublishArray* p,
    struct mmAmqpConn* conn,
    struct mmString* routingkey,
    mmUInt8_t* buffer,
    size_t offset,
    size_t length)
{
    int rt = -1;
    int status = 0;
    //
    amqp_bytes_t message_bytes;
    message_bytes.len = length;
    message_bytes.bytes = buffer + offset;
    do
    {
        // first check for amqp connect state.
        if (MM_AC_STATE_BROKEN == conn->conn_state)
        {
            // try reconnect.
            if (MM_AC_STATE_FINISH == mmAmqpConn_Check(conn))
            {
                // if success for connect,exchange declare.
                mmAmqpExchange_Declare(&p->amqp_exchange, conn);
            }
            // check result is correct.
            if (MM_AC_STATE_BROKEN == conn->conn_state)
            {
                // can not connect to node.
                rt = -1;
                break;
            }
        }
        //
        status = amqp_basic_publish(conn->conn,
            conn->channel,
            amqp_cstring_bytes(mmString_CStr(&p->amqp_exchange.exchange_name)),
            amqp_cstring_bytes(mmString_CStr(routingkey)),
            p->amqp_publish.publish_mandatory,
            p->amqp_publish.publish_immediate,
            NULL,
            message_bytes);
        //
        if (AMQP_STATUS_OK != status)
        {
            char link_name[MM_LINK_NAME_LENGTH];
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmAmqpConn_ToString(conn, link_name);
            //
            mmLogger_LogE(gLogger, "%s %d publish_message routing_key:%s %s failure.",
                          __FUNCTION__, __LINE__, mmString_CStr(routingkey), link_name);
            //
            mmAmqpConn_ApplyBroken(conn);
        }
        rt = 0;
    } while (0);
    return rt;
}

MM_EXPORT_RABBITMQ int
mmAmqpPublishArray_PublishMessage(
    struct mmAmqpPublishArray* p,
    const char* event_name,
    mmUInt8_t* buffer,
    size_t offset,
    size_t length)
{
    int rt = -1;
    mmUInt32_t _try_times = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmString routingkey;
    struct mmAmqpConn* amqp_conn = NULL;
    const char* publish_routingkey = mmString_CStr(&p->amqp_publish.publish_routingkey);
    const char* index_key = mmString_CStr(&p->index_key);
    mmString_Init(&routingkey);
    amqp_conn = mmAmqpPublishArray_ThreadInstance(p);
    mmSpinlock_Lock(&p->index_key_locker);
    mmString_Sprintf(&routingkey, "%s.%s.%s", publish_routingkey, event_name, index_key);
    mmSpinlock_Unlock(&p->index_key_locker);
    do
    {
        if (NULL == amqp_conn)
        {
            mmLogger_LogE(gLogger, "%s %d system memory not enough.", __FUNCTION__, __LINE__);
            break;
        }
        _try_times++;
        rt = mmAmqpPublishArray_BasicPublish(p, amqp_conn, &routingkey, buffer, offset, length);
        if (0 != rt)
        {
            mmLogger_LogI(gLogger, "%s %d attach %s:%d failure.try times:%d",
                          __FUNCTION__, __LINE__, mmString_CStr(&amqp_conn->node), amqp_conn->port, _try_times);
            mmMSleep(MM_AMQP_CONN_TRY_MSLEEP_TIME);
        }
    } while (0 != rt && _try_times < p->publish_trytimes);
    mmString_Destroy(&routingkey);
    return rt;
}

