/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAmqpConn.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"

MM_EXPORT_RABBITMQ int mmAmqp_RpcReplyError(amqp_rpc_reply_t x, const char* func, int line)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    switch (x.reply_type)
    {
    case AMQP_RESPONSE_NORMAL:
        return 0;

    case AMQP_RESPONSE_NONE:
        mmLogger_LogE(gLogger, "%s %d amqp: missing RPC reply type!", func, line);
        break;

    case AMQP_RESPONSE_LIBRARY_EXCEPTION:
        mmLogger_LogE(gLogger, "%s %d amqp: %s", func, line, amqp_error_string2(x.library_error));
        break;

    case AMQP_RESPONSE_SERVER_EXCEPTION:
        switch (x.reply.id)
        {
        case AMQP_CONNECTION_CLOSE_METHOD:
        {
            amqp_connection_close_t *m = (amqp_connection_close_t *)x.reply.decoded;
            mmLogger_LogE(gLogger, "%s %d amqp: server connection error(%uh):%.*s",
                          func, line, m->reply_code, (int)m->reply_text.len, (char *)m->reply_text.bytes);
            break;
        }
        case AMQP_CHANNEL_CLOSE_METHOD:
        {
            amqp_channel_close_t *m = (amqp_channel_close_t *)x.reply.decoded;
            mmLogger_LogE(gLogger, "%s %d amqp: server channel error(%uh):%.*s",
                          func, line, m->reply_code, (int)m->reply_text.len, (char *)m->reply_text.bytes);
            break;
        }
        default:
            mmLogger_LogE(gLogger, "%s %d amqp: unknown server error, method id 0x%08X", func, line, x.reply.id);
            break;
        }
        break;
    }
    return -1;
}

MM_EXPORT_RABBITMQ void mmAmqpConn_Init(struct mmAmqpConn* p)
{
    mmString_Init(&p->vhost);
    mmString_Init(&p->username);
    mmString_Init(&p->password);
    mmString_Init(&p->node);
    mmSpinlock_Init(&p->locker, NULL);
    p->port = 0;
    p->channel_max = 0;
    p->frame_max = 131072;
    p->heartbeat = 0;
    p->channel = 1;
    //
    p->conn_timeout = MM_AMQP_CONN_CONN_TIMEOUT;
    p->trytimes = MM_AMQP_CONN_TRYTIMES;
    p->conn_state = MM_AC_STATE_BROKEN;
    //
    p->socket = NULL;
    p->conn = NULL;
    p->is_open_channel = 0;
    //
    mmString_Assigns(&p->vhost, "/");
}

MM_EXPORT_RABBITMQ void mmAmqpConn_Destroy(struct mmAmqpConn* p)
{
    mmAmqpConn_Detach(p);
    mmAmqpConn_RecycleConnectionState(p);
    //
    mmString_Destroy(&p->vhost);
    mmString_Destroy(&p->username);
    mmString_Destroy(&p->password);
    mmString_Destroy(&p->node);
    mmSpinlock_Destroy(&p->locker);
    p->port = 0;
    p->channel_max = 0;
    p->frame_max = 131072;
    p->heartbeat = 0;
    p->channel = 1;
    //
    p->conn_timeout = MM_AMQP_CONN_CONN_TIMEOUT;
    p->trytimes = MM_AMQP_CONN_TRYTIMES;
    p->conn_state = MM_AC_STATE_BROKEN;
    //
    p->socket = NULL;
    p->conn = NULL;
    p->is_open_channel = 0;
}

MM_EXPORT_RABBITMQ void mmAmqpConn_Lock(struct mmAmqpConn* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConn_Unlock(struct mmAmqpConn* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_RABBITMQ void mmAmqpConn_SetRemote(struct mmAmqpConn* p, const char* node, mmUShort_t port)
{
    mmString_Assigns(&p->node, node);
    p->port = port;
}
MM_EXPORT_RABBITMQ void mmAmqpConn_SetVhost(struct mmAmqpConn* p, const char* vhost)
{
    mmString_Assigns(&p->vhost, vhost);
}
MM_EXPORT_RABBITMQ void mmAmqpConn_SetAccount(struct mmAmqpConn* p, const char* username, const char* password)
{
    mmString_Assigns(&p->username, username);
    mmString_Assigns(&p->password, password);
}

MM_EXPORT_RABBITMQ void
mmAmqpConn_SetConnAttr(
    struct mmAmqpConn* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel)
{
    p->channel_max = channel_max;
    p->frame_max = frame_max;
    p->heartbeat = heartbeat;
    p->channel = channel;
}

MM_EXPORT_RABBITMQ void mmAmqpConn_SetConnTimeout(struct mmAmqpConn* p, mmMSec_t conn_timeout, mmUInt32_t trytimes)
{
    p->conn_timeout = conn_timeout;
    p->trytimes = trytimes;
}

MM_EXPORT_RABBITMQ void mmAmqpConn_ProduceConnectionState(struct mmAmqpConn* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    assert(NULL == p->conn && "p->conn is not a NULL.");
    do
    {
        p->conn = amqp_new_connection();
        if (NULL == p->conn)
        {
            mmLogger_LogE(gLogger, "%s %d new_connection failure.system memory not enough.", __FUNCTION__, __LINE__);
        }
        else
        {
            mmLogger_LogT(gLogger, "%s %d new_connection:%p success.", __FUNCTION__, __LINE__, p->conn);
        }
    } while (0);
}
MM_EXPORT_RABBITMQ void mmAmqpConn_RecycleConnectionState(struct mmAmqpConn* p)
{
    int status = 0;
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    do
    {
        if (NULL == p->conn)
        {
            // the connect is already closed.
            break;
        }
        //
        mmAmqpConn_ToString(p, link_name);
        //
        status = amqp_destroy_connection(p->conn);
        if (AMQP_STATUS_OK != status)
        {
            mmLogger_LogN(gLogger, "%s %d destroy_connection %s failure.", __FUNCTION__, __LINE__, link_name);
        }
        else
        {
            mmLogger_LogT(gLogger, "%s %d destroy_connection %s success.", __FUNCTION__, __LINE__, link_name);
        }
        p->conn = NULL;
    } while (0);
    p->conn_state = MM_AC_STATE_BROKEN;
}
MM_EXPORT_RABBITMQ void mmAmqpConn_InstanceConnectionState(struct mmAmqpConn* p)
{
    if (NULL == p->conn)
    {
        mmAmqpConn_ProduceConnectionState(p);
    }
}

MM_EXPORT_RABBITMQ int mmAmqpConn_Attach(struct mmAmqpConn* p)
{
    int rt = -1;
    int status = -1;
    amqp_rpc_reply_t reply;
    amqp_channel_open_ok_t* channel_open_ok = NULL;
    struct timeval tv;
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    do
    {
        p->conn_state = MM_AC_STATE_BROKEN;
        //
        mmAmqpConn_InstanceConnectionState(p);
        if (NULL == p->conn)
        {
            mmLogger_LogE(gLogger, "%s %d amqp_conn is a null.system memory not enough.", __FUNCTION__, __LINE__);
            break;
        }
        //
        p->socket = amqp_tcp_socket_new(p->conn);
        if (NULL == p->socket)
        {
            mmLogger_LogE(gLogger, "%s %d socket_new failure.system memory not enough.", __FUNCTION__, __LINE__);
            break;
        }
        //
        mmLogger_LogT(gLogger, "%s %d socket_new:%p success.", __FUNCTION__, __LINE__, p->socket);
        //
        tv.tv_sec = (mmTimevalSSec_t)(p->conn_timeout / 1000);
        tv.tv_usec = (mmTimevalUSec_t)((p->conn_timeout % 1000) * 1000);
        status = amqp_socket_open_noblock(p->socket, mmString_CStr(&p->node), p->port, &tv);
        mmAmqpConn_ToString(p, link_name);
        if (AMQP_STATUS_OK != status)
        {
            mmLogger_LogE(gLogger, "%s %d socket_open %s failure.", __FUNCTION__, __LINE__, link_name);
            break;
        }
        mmLogger_LogT(gLogger, "%s %d socket_open %s success.", __FUNCTION__, __LINE__, link_name);
        //
        reply = amqp_login(
            p->conn,
            mmString_CStr(&p->vhost),
            p->channel_max,
            p->frame_max,
            p->heartbeat,
            AMQP_SASL_METHOD_PLAIN,
            mmString_CStr(&p->username),
            mmString_CStr(&p->password));
        
        if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
        {
            mmLogger_LogE(gLogger, "%s %d login %s failure.", __FUNCTION__, __LINE__, link_name);
            break;
        }
        mmLogger_LogT(gLogger, "%s %d login %s success.", __FUNCTION__, __LINE__, link_name);
        //
        channel_open_ok = amqp_channel_open(p->conn, p->channel);
        if (NULL == channel_open_ok)
        {
            reply = amqp_get_rpc_reply(p->conn);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogE(gLogger, "%s %d fopen_channel:%d %s failure.",
                              __FUNCTION__, __LINE__, p->channel, link_name);
                break;
            }
        }
        mmLogger_LogT(gLogger, "%s %d fopen_channel:%d %s success.",
                      __FUNCTION__, __LINE__, p->channel, link_name);
        
        p->is_open_channel = 1;
        p->conn_state = MM_AC_STATE_FINISH;
        rt = 0;
    } while (0);
    return rt;
}
MM_EXPORT_RABBITMQ int mmAmqpConn_Detach(struct mmAmqpConn* p)
{
    int rt = -1;
    amqp_rpc_reply_t reply;
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    do
    {
        if (NULL == p->conn)
        {
            // the connect is already closed.
            rt = 0;
            break;
        }
        //
        mmAmqpConn_ToString(p, link_name);
        // if the channel is not open, we can not close it.
        if (1 == p->is_open_channel)
        {
            reply = amqp_channel_close(p->conn, p->channel, AMQP_REPLY_SUCCESS);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogE(gLogger, "%s %d close_channel:%d %s failure.",
                              __FUNCTION__, __LINE__, p->channel, link_name);
            }
            else
            {
                mmLogger_LogT(gLogger, "%s %d close_channel:%d %s success.",
                              __FUNCTION__, __LINE__, p->channel, link_name);
            }
            p->is_open_channel = 0;
        }
        //
        mmLogger_LogT(gLogger, "%s %d logout %s success.", __FUNCTION__, __LINE__, link_name);
        //
        if (NULL != p->socket)
        {
            reply = amqp_connection_close(p->conn, AMQP_REPLY_SUCCESS);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogN(gLogger, "%s %d connection_close %s failure.",
                              __FUNCTION__, __LINE__, link_name);
            }
            else
            {
                mmLogger_LogT(gLogger, "%s %d connection_close %s success.",
                              __FUNCTION__, __LINE__, link_name);
            }
            p->socket = NULL;
        }
        //
        rt = 0;
    } while (0);
    p->conn_state = MM_AC_STATE_BROKEN;
    return rt;
}
MM_EXPORT_RABBITMQ void mmAmqpConn_ApplyBroken(struct mmAmqpConn* p)
{
    p->conn_state = MM_AC_STATE_BROKEN;
}

// check and try connect if broken.
// 0 : is broken.if conn is not work, will connect single thread once.
// 1 : is working not broken.
MM_EXPORT_RABBITMQ int mmAmqpConn_Check(struct mmAmqpConn* p)
{
    int rt = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    if (MM_AC_STATE_BROKEN == p->conn_state)
    {
        mmUInt32_t _try_times = 0;
        rt = -1;
        do
        {
            _try_times++;
            do
            {
                rt = mmAmqpConn_Detach(p);
                if (0 != rt)
                {
                    mmLogger_LogI(gLogger, "%s %d detach %s:%d failure.try times:%d",
                                  __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, _try_times);
                    mmMSleep(MM_AMQP_CONN_TRY_MSLEEP_TIME);
                    break;
                }
                rt = mmAmqpConn_Attach(p);
                if (0 != rt)
                {
                    mmLogger_LogI(gLogger, "%s %d attach %s:%d failure.try times:%d",
                                  __FUNCTION__, __LINE__, mmString_CStr(&p->node), p->port, _try_times);
                    mmMSleep(MM_AMQP_CONN_TRY_MSLEEP_TIME);
                    break;
                }
            } while (0);
        } while (0 != rt && _try_times < p->trytimes);
    }
    return p->conn_state;
}
MM_EXPORT_RABBITMQ void mmAmqpConn_ToString(struct mmAmqpConn* p, char link_name[MM_LINK_NAME_LENGTH])
{
    int fd = (NULL != p->socket) ? amqp_socket_get_sockfd(p->socket) : 0;
    mmSprintf(link_name, "(%d)|%s-%d", fd, mmString_CStr(&p->node), p->port);
}
