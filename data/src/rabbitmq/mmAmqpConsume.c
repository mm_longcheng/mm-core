/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAmqpConsume.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"
#include "core/mmThread.h"
#include "core/mmTimewait.h"
#include "core/mmTimeval.h"

static void
__static_mmAmqpConsume_PoperDefault(
    struct mmAmqpConsume* p,
    void* u,
    mmUInt8_t* buffer,
    mmUInt32_t offset,
    mmUInt32_t length)
{

}

static void* __static_mmAmqpConsume_PopThread(void* arg);

MM_EXPORT_RABBITMQ void mmAmqpConsumeCallback_Init(struct mmAmqpConsumeCallback* p)
{
    p->Poper = &__static_mmAmqpConsume_PoperDefault;
    p->obj = NULL;
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeCallback_Destroy(struct mmAmqpConsumeCallback* p)
{
    p->Poper = &__static_mmAmqpConsume_PoperDefault;
    p->obj = NULL;
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeContext_Init(struct mmAmqpConsumeContext* p)
{
    mmTimeval_Init(&p->recv_timeout);
    mmMemset(&p->frame, 0, sizeof(amqp_frame_t));
    mmMemset(&p->reply, 0, sizeof(amqp_rpc_reply_t));
    mmMemset(&p->envelope, 0, sizeof(amqp_envelope_t));
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeContext_Destroy(struct mmAmqpConsumeContext* p)
{
    mmTimeval_Destroy(&p->recv_timeout);
    mmMemset(&p->frame, 0, sizeof(amqp_frame_t));
    mmMemset(&p->reply, 0, sizeof(amqp_rpc_reply_t));
    mmMemset(&p->envelope, 0, sizeof(amqp_envelope_t));
}

MM_EXPORT_RABBITMQ void mmAmqpConsume_Init(struct mmAmqpConsume* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmAmqpConn_Init(&p->amqp_conn);
    mmAmqpConsumeContext_Init(&p->consume_context);
    mmAmqpExchange_Init(&p->amqp_exchange);
    mmAmqpQueue_Init(&p->amqp_queue);
    mmString_Init(&p->routing_key);
    mmString_Init(&p->index_key);
    mmAmqpConsumeCallback_Init(&p->callback);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->consume_no_local = 0;
    p->consume_no_ack = 0;
    p->consume_exclusive = 0;

    p->sleep_empty_key_timeout = MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME;
    p->sleep_empty_val_timeout = MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME;
    p->sleep_error_ctx_timeout = MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME;
    p->pop_timeout = MM_AMQP_CONSUME_ERROR_POP_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);

    mmString_Assigns(&p->index_key, "*");
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_Destroy(struct mmAmqpConsume* p)
{
    mmAmqpConn_Destroy(&p->amqp_conn);
    mmAmqpConsumeContext_Destroy(&p->consume_context);
    mmAmqpExchange_Destroy(&p->amqp_exchange);
    mmAmqpQueue_Destroy(&p->amqp_queue);
    mmString_Destroy(&p->routing_key);
    mmString_Destroy(&p->index_key);
    mmAmqpConsumeCallback_Destroy(&p->callback);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->consume_no_local = 0;
    p->consume_no_ack = 0;
    p->consume_exclusive = 0;

    p->sleep_empty_key_timeout = MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME;
    p->sleep_empty_val_timeout = MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME;
    p->sleep_error_ctx_timeout = MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME;
    p->pop_timeout = MM_AMQP_CONSUME_ERROR_POP_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_Lock(struct mmAmqpConsume* p)
{
    mmAmqpConn_Lock(&p->amqp_conn);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_Unlock(struct mmAmqpConsume* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmAmqpConn_Unlock(&p->amqp_conn);
}
// assign context handle.
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetContext(struct mmAmqpConsume* p, void* u)
{
    p->u = u;
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetPopTimeout(struct mmAmqpConsume* p, mmMSec_t pop_timeout)
{
    struct timeval* recv_timeout = NULL;
    p->pop_timeout = pop_timeout;
    recv_timeout = &p->consume_context.recv_timeout;
    recv_timeout->tv_sec = (mmTimevalSSec_t)(p->pop_timeout / 1000);
    recv_timeout->tv_usec = (mmTimevalUSec_t)((p->pop_timeout % 1000) * 1000);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsume_SetSleepTimeout(
    struct mmAmqpConsume* p,
    mmMSec_t empty_key_timeout,
    mmMSec_t empty_val_timeout,
    mmMSec_t error_ctx_timeout)
{
    p->sleep_empty_key_timeout = empty_key_timeout;
    p->sleep_empty_val_timeout = empty_val_timeout;
    p->sleep_error_ctx_timeout = error_ctx_timeout;
}

MM_EXPORT_RABBITMQ void
mmAmqpConsume_SetConsumeAttr(
    struct mmAmqpConsume* p,
    mmUInt8_t no_local,
    mmUInt8_t no_ack,
    mmUInt8_t exclusive)
{
    p->consume_no_local = no_local;
    p->consume_no_ack = no_ack;
    p->consume_exclusive = exclusive;
}

MM_EXPORT_RABBITMQ void mmAmqpConsume_SetExchange(struct mmAmqpConsume* p, struct mmAmqpExchange* exchange)
{
    mmString_Assign(&p->amqp_exchange.exchange_name, &exchange->exchange_name);
    mmString_Assign(&p->amqp_exchange.exchange_type, &exchange->exchange_type);
    p->amqp_exchange.exchange_passive = exchange->exchange_passive;
    p->amqp_exchange.exchange_durable = exchange->exchange_durable;
    p->amqp_exchange.exchange_auto_delete = exchange->exchange_auto_delete;
    p->amqp_exchange.exchange_internal = exchange->exchange_internal;
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetQueue(struct mmAmqpConsume* p, struct mmAmqpQueue* queue)
{
    mmString_Assign(&p->amqp_queue.queue_name, &queue->queue_name);
    p->amqp_queue.queue_passive = queue->queue_passive;
    p->amqp_queue.queue_durable = queue->queue_durable;
    p->amqp_queue.queue_exclusive = queue->queue_exclusive;
    p->amqp_queue.queue_auto_delete = queue->queue_auto_delete;
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetCallback(struct mmAmqpConsume* p, struct mmAmqpConsumeCallback* callback)
{
    p->callback = *callback;
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetRoutingKey(struct mmAmqpConsume* p, const char* routing_key)
{
    mmString_Assigns(&p->routing_key, routing_key);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetIndexKey(struct mmAmqpConsume* p, const char* index_key)
{
    mmString_Assigns(&p->index_key, index_key);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetEventHandle(struct mmAmqpConsume* p, mmUInt32_t id, void* callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetRemote(struct mmAmqpConsume* p, const char* node, mmUShort_t port)
{
    mmAmqpConn_SetRemote(&p->amqp_conn, node, port);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetVhost(struct mmAmqpConsume* p, const char* vhost)
{
    mmAmqpConn_SetVhost(&p->amqp_conn, vhost);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetAccount(struct mmAmqpConsume* p, const char* username, const char* password)
{
    mmAmqpConn_SetAccount(&p->amqp_conn, username, password);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsume_SetConnAttr(
    struct mmAmqpConsume* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel)
{
    mmAmqpConn_SetConnAttr(&p->amqp_conn, channel_max, frame_max, heartbeat, channel);
}

MM_EXPORT_RABBITMQ void mmAmqpConsume_SetConnTimeout(struct mmAmqpConsume* p, mmMSec_t conn_timeout, mmUInt32_t trytimes)
{
    mmAmqpConn_SetConnTimeout(&p->amqp_conn, conn_timeout, trytimes);
}

MM_EXPORT_RABBITMQ int mmAmqpConsume_BasicMessage(struct mmAmqpConsume* p, struct mmAmqpExchange* exchange)
{
    int rt = -1;
    amqp_connection_state_t state = p->amqp_conn.conn;
    amqp_frame_t* frame = &p->consume_context.frame;
    amqp_rpc_reply_t* reply = &p->consume_context.reply;
    amqp_envelope_t* envelope = &p->consume_context.envelope;
    struct timeval* recv_timeout = &p->consume_context.recv_timeout;
    do
    {
        amqp_maybe_release_buffers(state);
        *reply = amqp_consume_message(state, envelope, recv_timeout, 0);
        if (AMQP_RESPONSE_NORMAL != reply->reply_type)
        {
            if (AMQP_RESPONSE_LIBRARY_EXCEPTION == reply->reply_type && AMQP_STATUS_TIMEOUT == reply->library_error)
            {
                // recv timeout event.
                rt = 0;
                break;
            }
            // logger rpc reply error.
            mmAmqp_RpcReplyError(*reply, __FUNCTION__, __LINE__);
            //
            if (AMQP_RESPONSE_LIBRARY_EXCEPTION == reply->reply_type && AMQP_STATUS_UNEXPECTED_STATE == reply->library_error)
            {
                if (AMQP_STATUS_OK != amqp_simple_wait_frame(state, frame))
                {
                    // can not wait frame.the connect is broken.
                    // this error is not serious.
                    rt = -1;
                    break;
                }

                if (AMQP_FRAME_METHOD == frame->frame_type)
                {
                    switch (frame->payload.method.id)
                    {
                    case AMQP_BASIC_ACK_METHOD:
                        /* if we've turned publisher confirms on, and we've published a message
                        * here is a message being confirmed
                        */
                    {
                        rt = 1;
                    }
                    break;
                    case AMQP_BASIC_RETURN_METHOD:
                        /* if a published message couldn't be routed and the mandatory flag was set
                        * this is what would be returned. The message then needs to be read.
                        */
                    {
                        amqp_message_t message;
                        *reply = amqp_read_message(state, frame->channel, &message, 0);
                        if (AMQP_RESPONSE_NORMAL != reply->reply_type)
                        {
                            rt = -1;
                            break;
                        }
                        amqp_destroy_message(&message);
                        rt = 1;
                    }
                    break;

                    case AMQP_CHANNEL_CLOSE_METHOD:
                        /* a channel.close method happens when a channel exception occurs, this
                        * can happen by publishing to an exchange that doesn't exist for example
                        *
                        * In this case you would need to open another channel redeclare any queues
                        * that were declared auto-delete, and restart any consumers that were attached
                        * to the previous channel
                        */
                    {
                        // the channel is close,the state is broken.
                        rt = -1;
                    }
                    break;

                    case AMQP_CONNECTION_CLOSE_METHOD:
                        /* a connection.close method happens when a connection exception occurs,
                        * this can happen by trying to use a channel that isn't open for example.
                        *
                        * In this case the whole connection must be restarted.
                        */
                    {
                        // the connect is close,the state is broken.
                        rt = -1;
                    }
                    break;

                    default:
                    {
                        struct mmLogger* gLogger = mmLogger_Instance();
                        mmLogger_LogE(gLogger, "%s %d An unexpected method was received %u.",
                                      __FUNCTION__, __LINE__, frame->payload.method.id);
                        // an unexpected method was received,the state is broken.
                        rt = -1;
                    }
                    break;
                    }
                }
            }
            // the other AMQP_RESPONSE_LIBRARY_EXCEPTION library_error is means connection have something error.
            // such as AMQP_STATUS_CONNECTION_CLOSED
            if (AMQP_RESPONSE_LIBRARY_EXCEPTION == reply->reply_type)
            {
                // connection is broken.
                rt = -1;
                break;
            }
        }
        else
        {
            // pop message success.
            rt = 0;
        }
    } while (0);
    return rt;
}
MM_EXPORT_RABBITMQ int mmAmqpConsume_Pop(struct mmAmqpConsume* p)
{
    struct timeval  ntime;
    struct timespec otime;
    mmMSec_t _nearby_time = 0;
    int rt = -1;
    amqp_envelope_t* envelope = &p->consume_context.envelope;
    amqp_channel_t channel = p->amqp_conn.channel;
    amqp_bytes_t* body = &envelope->message.body;
    assert(p->callback.Poper && "p->callback.Poper is a null.");
    while (MM_TS_MOTION == p->state)
    {
        if (0 == mmString_Size(&p->routing_key))
        {
            _nearby_time = p->sleep_empty_key_timeout;
            // timewait nearby.
            mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
            continue;
        }
        body->len = 0;
        if (MM_AC_STATE_BROKEN == p->amqp_conn.conn_state)
        {
            // need reconnect and declare bind.
            // reconnect.
            if (MM_AC_STATE_BROKEN == mmAmqpConn_Check(&p->amqp_conn))
            {
                // can not connect to node.
                _nearby_time = p->sleep_error_ctx_timeout;
                // timewait nearby.
                mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
                continue;
            }
            mmAmqpConsume_AttachQueue(p);
            if (MM_AC_STATE_BROKEN == p->amqp_conn.conn_state)
            {
                // can not connect to node.
                _nearby_time = p->sleep_error_ctx_timeout;
                // timewait nearby.
                mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
                continue;
            }
        }
        rt = mmAmqpConsume_BasicMessage(p, NULL);
        if (0 < body->len)
        {
            (*(p->callback.Poper))(p, p->u, (mmUInt8_t*)(body->bytes), 0, (mmUInt32_t)(body->len));
            // if consume_no_ack is 0,means we must send ack manual.
            if (0 == p->consume_no_ack)
            {
                amqp_basic_ack(p->amqp_conn.conn, channel, envelope->delivery_tag, 0);
            }
        }
        if (-1 == rt)
        {
            mmAmqpConn_ApplyBroken(&p->amqp_conn);
        }
        if (1 == rt || 0 == rt)
        {
            _nearby_time = 0;
        }
        else
        {
            _nearby_time = p->sleep_error_ctx_timeout;
        }
        amqp_destroy_envelope(envelope);
        // check the runing state.
        _nearby_time = MM_TS_MOTION == p->state ? _nearby_time : 0;
        // timewait nearby.
        mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
    }
    return 0;
}
MM_EXPORT_RABBITMQ int mmAmqpConsume_AttachQueue(struct mmAmqpConsume* p)
{
    int rt = -1;
    do
    {
        rt = mmAmqpExchange_Declare(&p->amqp_exchange, &p->amqp_conn);
        if (0 != rt) { break; }
        rt = mmAmqpQueue_Declare(&p->amqp_queue, &p->amqp_conn);
        if (0 != rt) { break; }
        rt = mmAmqpQueue_Bind(&p->amqp_queue, &p->amqp_conn, &p->amqp_exchange, p);
        if (0 != rt) { break; }
        rt = mmAmqpQueue_BasicConsume(&p->amqp_queue, &p->amqp_conn, &p->amqp_exchange, p);
        if (0 != rt) { break; }
        rt = 0;
    } while (0);
    return rt;
}
MM_EXPORT_RABBITMQ int mmAmqpConsume_DetachQueue(struct mmAmqpConsume* p)
{
    return 0;
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_ApplyBroken(struct mmAmqpConsume* p)
{
    mmAmqpConn_ApplyBroken(&p->amqp_conn);
}

MM_EXPORT_RABBITMQ void mmAmqpConsume_Start(struct mmAmqpConsume* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->thread_poper, NULL, &__static_mmAmqpConsume_PopThread, p);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_Interrupt(struct mmAmqpConsume* p)
{
    p->state = MM_TS_CLOSED;
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_Shutdown(struct mmAmqpConsume* p)
{
    p->state = MM_TS_FINISH;
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_RABBITMQ void mmAmqpConsume_Join(struct mmAmqpConsume* p)
{
    pthread_join(p->thread_poper, NULL);
}
static void* __static_mmAmqpConsume_PopThread(void* arg)
{
    struct mmAmqpConsume* p = (struct mmAmqpConsume*)(arg);
    mmAmqpConsume_Pop(p);
    return NULL;
}
