/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAmqpPublish_h__
#define __mmAmqpPublish_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"
#include "core/mmList.h"
#include "core/mmTime.h"
#include "net/mmSockaddr.h"
#include "net/mmPacket.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"

#include <pthread.h>

#include "rabbitmq/mmAmqpExport.h"

#include "core/mmPrefix.h"

struct mmAmqpPublish
{
    struct mmString publish_routingkey;
    mmUInt8_t publish_mandatory;
    mmUInt8_t publish_immediate;
};

MM_EXPORT_RABBITMQ void mmAmqpPublish_Init(struct mmAmqpPublish* p);
MM_EXPORT_RABBITMQ void mmAmqpPublish_Destroy(struct mmAmqpPublish* p);

#include "core/mmSuffix.h"

#endif//__mmAmqpPublish_h__
