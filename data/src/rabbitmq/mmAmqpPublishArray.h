/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAmqpPublishArray_h__
#define __mmAmqpPublishArray_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"
#include "core/mmList.h"
#include "core/mmTime.h"

#include "net/mmSockaddr.h"
#include "net/mmPacket.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"

#include <pthread.h>

#include "rabbitmq/mmAmqpExchange.h"
#include "rabbitmq/mmAmqpPublish.h"

#include "rabbitmq/mmAmqpExport.h"

#include "core/mmPrefix.h"

#define MM_AMQP_PUBLISH_ARRAY_TRYTIMES 3

struct mmAmqpPublishArray
{
    struct mmAmqpExchange amqp_exchange;
    struct mmAmqpPublish amqp_publish;
    struct mmString index_key;
    // default is MM_AMQP_PUBLISH_ARRAY_TRYTIMES.
    mmUInt32_t publish_trytimes;
    // silk strong ref list.
    struct mmListHead list;
    struct mmString vhost;
    struct mmString username;
    struct mmString password;
    struct mmString node;
    mmAtomic_t list_locker;
    mmAtomic_t index_key_locker;
    // thread key.
    pthread_key_t thread_key;
    mmUShort_t port;
    int channel_max;
    int frame_max;
    int heartbeat;
    // connect time out ms.default is 3s(3000ms)
    mmMSec_t conn_timeout;
    // try bind and listen times.default is MM_AMQP_CONN_TRYTIMES.
    mmUInt32_t trytimes;
    amqp_channel_t channel;
};

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_Init(struct mmAmqpPublishArray* p);
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_Destroy(struct mmAmqpPublishArray* p);

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetExchange(struct mmAmqpPublishArray* p, struct mmAmqpExchange* exchange);
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetPublish(struct mmAmqpPublishArray* p, struct mmAmqpPublish* publish);
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetPublishTrytimes(struct mmAmqpPublishArray* p, mmUInt32_t publish_trytimes);

// 192.168.111.123-65535
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetParameters(struct mmAmqpPublishArray* p, const char* parameters);

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetRemote(struct mmAmqpPublishArray* p, const char* node, mmUShort_t port);
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetVhost(struct mmAmqpPublishArray* p, const char* vhost);
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetAccount(struct mmAmqpPublishArray* p, const char* username, const char* password);

MM_EXPORT_RABBITMQ void
mmAmqpPublishArray_SetConnAttr(
    struct mmAmqpPublishArray* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel);

MM_EXPORT_RABBITMQ void
mmAmqpPublishArray_SetConnTimeout(
    struct mmAmqpPublishArray* p,
    mmMSec_t conn_timeout,
    mmUInt32_t trytimes);

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_SetIndexKey(struct mmAmqpPublishArray* p, const char* index_key);

// get current thread instance.
MM_EXPORT_RABBITMQ struct mmAmqpConn* mmAmqpPublishArray_ThreadInstance(struct mmAmqpPublishArray* p);
// clear all instance.
MM_EXPORT_RABBITMQ void mmAmqpPublishArray_Clear(struct mmAmqpPublishArray* p);

MM_EXPORT_RABBITMQ void mmAmqpPublishArray_ApplyBroken(struct mmAmqpPublishArray* p);

MM_EXPORT_RABBITMQ int
mmAmqpPublishArray_BasicPublish(
    struct mmAmqpPublishArray* p,
    struct mmAmqpConn* conn,
    struct mmString* routingkey,
    mmUInt8_t* buffer,
    size_t offset,
    size_t length);

MM_EXPORT_RABBITMQ int
mmAmqpPublishArray_PublishMessage(
    struct mmAmqpPublishArray* p,
    const char* event_name,
    mmUInt8_t* buffer,
    size_t offset,
    size_t length);

#include "core/mmSuffix.h"

#endif//__mmAmqpPublishArray_h__
