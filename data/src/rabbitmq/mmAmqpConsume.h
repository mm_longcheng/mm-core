/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAmqpConsume_h__
#define __mmAmqpConsume_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"
#include "core/mmList.h"
#include "core/mmTime.h"
#include "net/mmSockaddr.h"
#include "net/mmPacket.h"

#include "container/mmRbtreeU32.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"

#include <pthread.h>
#include "rabbitmq/mmAmqpConn.h"
#include "rabbitmq/mmAmqpExchange.h"
#include "rabbitmq/mmAmqpQueue.h"

#include "rabbitmq/mmAmqpExport.h"

#include "core/mmPrefix.h"

struct mmAmqpConsume;

struct mmAmqpConsumeCallback
{
    // return aca_ack_ensure need send ack aca_ack_cancel not send ack.
    void(*Poper)(struct mmAmqpConsume* p, void* u, mmUInt8_t* buffer, mmUInt32_t offset, mmUInt32_t length);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_RABBITMQ void mmAmqpConsumeCallback_Init(struct mmAmqpConsumeCallback* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeCallback_Destroy(struct mmAmqpConsumeCallback* p);

struct mmAmqpConsumeContext
{
    struct timeval recv_timeout;
    amqp_frame_t frame;
    amqp_rpc_reply_t reply;
    amqp_envelope_t envelope;
};
MM_EXPORT_RABBITMQ void mmAmqpConsumeContext_Init(struct mmAmqpConsumeContext* p);
MM_EXPORT_RABBITMQ void mmAmqpConsumeContext_Destroy(struct mmAmqpConsumeContext* p);

// millisecond.default is 2000.
#define MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME 2000
// millisecond.default is 1000.
#define MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME 1000
// millisecond.default is 3000.
#define MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME 3000
// millisecond.default is 300.
#define MM_AMQP_CONSUME_ERROR_POP_TIMEOUT 300

struct mmAmqpConsume
{
    struct mmAmqpConn amqp_conn;
    struct mmAmqpConsumeContext consume_context;
    struct mmAmqpExchange amqp_exchange;
    struct mmAmqpQueue amqp_queue;
    struct mmString routing_key;
    struct mmString index_key;
    struct mmAmqpConsumeCallback callback;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    mmUInt8_t consume_no_local;
    mmUInt8_t consume_no_ack;
    mmUInt8_t consume_exclusive;

    // thread.
    pthread_t thread_poper;
    // MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME
    mmMSec_t sleep_empty_key_timeout;
    // MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME
    mmMSec_t sleep_empty_val_timeout;
    // MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME
    mmMSec_t sleep_error_ctx_timeout;
    // MM_AMQP_CONSUME_ERROR_POP_TIMEOUT
    mmMSec_t pop_timeout;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_RABBITMQ void mmAmqpConsume_Init(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ void mmAmqpConsume_Destroy(struct mmAmqpConsume* p);

/* locker order is amqp_conn -> locker. */
MM_EXPORT_RABBITMQ void mmAmqpConsume_Lock(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ void mmAmqpConsume_Unlock(struct mmAmqpConsume* p);

// assign context handle.
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetContext(struct mmAmqpConsume* p, void* u);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetPopTimeout(struct mmAmqpConsume* p, mmMSec_t pop_timeout);

MM_EXPORT_RABBITMQ void
mmAmqpConsume_SetSleepTimeout(
    struct mmAmqpConsume* p,
    mmMSec_t empty_key_timeout,
    mmMSec_t empty_val_timeout,
    mmMSec_t error_ctx_timeout);

MM_EXPORT_RABBITMQ void
mmAmqpConsume_SetConsumeAttr(
    struct mmAmqpConsume* p,
    mmUInt8_t no_local,
    mmUInt8_t no_ack,
    mmUInt8_t exclusive);

MM_EXPORT_RABBITMQ void mmAmqpConsume_SetExchange(struct mmAmqpConsume* p, struct mmAmqpExchange* exchange);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetQueue(struct mmAmqpConsume* p, struct mmAmqpQueue* queue);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetCallback(struct mmAmqpConsume* p, struct mmAmqpConsumeCallback* callback);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetRoutingKey(struct mmAmqpConsume* p, const char* routing_key);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetIndexKey(struct mmAmqpConsume* p, const char* index_key);

MM_EXPORT_RABBITMQ void mmAmqpConsume_SetEventHandle(struct mmAmqpConsume* p, mmUInt32_t id, void* callback);

MM_EXPORT_RABBITMQ void mmAmqpConsume_SetRemote(struct mmAmqpConsume* p, const char* node, mmUShort_t port);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetVhost(struct mmAmqpConsume* p, const char* vhost);
MM_EXPORT_RABBITMQ void mmAmqpConsume_SetAccount(struct mmAmqpConsume* p, const char* username, const char* password);

MM_EXPORT_RABBITMQ void
mmAmqpConsume_SetConnAttr(
    struct mmAmqpConsume* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel);

MM_EXPORT_RABBITMQ void mmAmqpConsume_SetConnTimeout(struct mmAmqpConsume* p, mmMSec_t conn_timeout, mmUInt32_t trytimes);

MM_EXPORT_RABBITMQ int mmAmqpConsume_BasicMessage(struct mmAmqpConsume* p, struct mmAmqpExchange* exchange);
MM_EXPORT_RABBITMQ int mmAmqpConsume_Pop(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ int mmAmqpConsume_AttachQueue(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ int mmAmqpConsume_DetachQueue(struct mmAmqpConsume* p);

MM_EXPORT_RABBITMQ void mmAmqpConsume_ApplyBroken(struct mmAmqpConsume* p);

MM_EXPORT_RABBITMQ void mmAmqpConsume_Start(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ void mmAmqpConsume_Interrupt(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ void mmAmqpConsume_Shutdown(struct mmAmqpConsume* p);
MM_EXPORT_RABBITMQ void mmAmqpConsume_Join(struct mmAmqpConsume* p);

#include "core/mmSuffix.h"

#endif//__mmAmqpConsume_h__
