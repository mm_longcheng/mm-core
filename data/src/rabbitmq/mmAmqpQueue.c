/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAmqpQueue.h"
#include "mmAmqpConn.h"
#include "mmAmqpExchange.h"
#include "mmAmqpConsume.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"

MM_EXPORT_RABBITMQ void mmAmqpQueue_Init(struct mmAmqpQueue* p)
{
    mmString_Init(&p->queue_name);
    p->queue_passive = 0;
    p->queue_durable = 1;
    p->queue_exclusive = 0;
    p->queue_auto_delete = 0;
}
MM_EXPORT_RABBITMQ void mmAmqpQueue_Destroy(struct mmAmqpQueue* p)
{
    mmString_Destroy(&p->queue_name);
    p->queue_passive = 0;
    p->queue_durable = 1;
    p->queue_exclusive = 0;
    p->queue_auto_delete = 0;
}

MM_EXPORT_RABBITMQ int mmAmqpQueue_Declare(struct mmAmqpQueue* p, struct mmAmqpConn* conn)
{
    int rt = -1;
    amqp_queue_declare_ok_t* queue_declare_ok = NULL;
    amqp_rpc_reply_t reply;
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmAmqpConn_ToString(conn, link_name);
    //
    do
    {
        // queue declare.
        queue_declare_ok = amqp_queue_declare(
            conn->conn,
            conn->channel,
            amqp_cstring_bytes(mmString_CStr(&p->queue_name)),
            p->queue_passive,
            p->queue_durable,
            p->queue_exclusive,
            p->queue_auto_delete,
            amqp_empty_table);
        //
        if (NULL == queue_declare_ok)
        {
            //
            reply = amqp_get_rpc_reply(conn->conn);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogE(gLogger, "%s %d queue_declare queue_name:%s %s failure.",
                              __FUNCTION__, __LINE__, mmString_CStr(&p->queue_name), link_name);
                mmAmqpConn_ApplyBroken(conn);
                rt = -1;
                break;
            }
        }
        mmLogger_LogI(gLogger, "%s %d queue_declare queue_name:%s %s success.",
                      __FUNCTION__, __LINE__, mmString_CStr(&p->queue_name), link_name);
        rt = 0;
    } while (0);
    return rt;
}

MM_EXPORT_RABBITMQ int
mmAmqpQueue_BindRoutingkeyReal(
    struct mmAmqpQueue* p,
    struct mmAmqpConn* conn,
    struct mmAmqpExchange* exchange,
    const char* routingkey_real)
{
    int rt = -1;
    amqp_queue_bind_ok_t* queue_bind_ok = NULL;
    amqp_rpc_reply_t reply;
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmAmqpConn_ToString(conn, link_name);
    //
    do
    {
        // queue bind.
        queue_bind_ok = amqp_queue_bind(
            conn->conn,
            conn->channel,
            amqp_cstring_bytes(mmString_CStr(&p->queue_name)),
            amqp_cstring_bytes(mmString_CStr(&exchange->exchange_name)),
            amqp_cstring_bytes(routingkey_real),
            amqp_empty_table);
        //
        if (NULL == queue_bind_ok)
        {
            reply = amqp_get_rpc_reply(conn->conn);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogE(gLogger, "%s %d queue_bind queue_name:%s routing_key:%s %s failure.",
                              __FUNCTION__, __LINE__, mmString_CStr(&p->queue_name), routingkey_real, link_name);
                mmAmqpConn_ApplyBroken(conn);
                rt = -1;
                break;
            }
        }
        mmLogger_LogI(gLogger, "%s %d queue_bind queue_name:%s routing_key:%s %s success.",
                      __FUNCTION__, __LINE__, mmString_CStr(&p->queue_name), routingkey_real, link_name);
        rt = 0;
    } while (0);
    return rt;
}

MM_EXPORT_RABBITMQ int
mmAmqpQueue_Bind(
    struct mmAmqpQueue* p,
    struct mmAmqpConn* conn,
    struct mmAmqpExchange* exchange,
    struct mmAmqpConsume* consume)
{
    int rt = -1;
    struct mmString routingkey_real;
    //
    mmString_Init(&routingkey_real);
    //
    do
    {
        struct mmRbNode* n = NULL;
        struct mmRbtreeU32VptIterator* it = NULL;
        //
        mmSpinlock_Lock(&consume->rbtree_locker);

        n = mmRb_First(&consume->rbtree.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            //
            mmString_Clear(&routingkey_real);
            mmString_Sprintf(&routingkey_real, "%s." MM_MESSAGE_MID_KEY_FORMAT ".%s",
                mmString_CStr(&consume->routing_key), it->k, mmString_CStr(&consume->index_key));
            mmAmqpQueue_BindRoutingkeyReal(p, conn, exchange, mmString_CStr(&routingkey_real));
        }

        mmSpinlock_Unlock(&consume->rbtree_locker);

        rt = 0;
    } while (0);
    mmString_Destroy(&routingkey_real);
    return rt;
}

MM_EXPORT_RABBITMQ int
mmAmqpQueue_BasicConsume(
    struct mmAmqpQueue* p,
    struct mmAmqpConn* conn,
    struct mmAmqpExchange* exchange,
    struct mmAmqpConsume* consume)
{
    int rt = -1;
    amqp_basic_consume_ok_t* basic_consume_ok = NULL;
    amqp_rpc_reply_t reply;
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmAmqpConn_ToString(conn, link_name);
    do
    {
        // basic consume.
        basic_consume_ok = amqp_basic_consume(
            conn->conn,
            conn->channel,
            amqp_cstring_bytes(mmString_CStr(&p->queue_name)),
            amqp_empty_bytes,
            consume->consume_no_local,
            consume->consume_no_ack,
            consume->consume_exclusive,
            amqp_empty_table);
        //
        if (NULL == basic_consume_ok)
        {
            reply = amqp_get_rpc_reply(conn->conn);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogE(gLogger, "%s %d queue_basic_consume queue_name:%s %s failure.",
                              __FUNCTION__, __LINE__, mmString_CStr(&p->queue_name), link_name);
                mmAmqpConn_ApplyBroken(conn);
                rt = -1;
                break;
            }
        }
        mmLogger_LogI(gLogger, "%s %d queue_basic_consume queue_name:%s %s success.",
                      __FUNCTION__, __LINE__, mmString_CStr(&p->queue_name), link_name);
        rt = 0;
    } while (0);
    return rt;
}

