/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAmqpConsumeArray.h"
#include "mmAmqpConn.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmSpinlock.h"
#include "core/mmThread.h"
#include "core/mmTimeCache.h"
#include "core/mmTimeval.h"
#include "core/mmParameters.h"

#include "net/mmBufferPacket.h"

static void __static_mmAmqpConsumeArray_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{

}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArrayCallback_Init(struct mmAmqpConsumeArrayCallback* p)
{
    p->Handle = &__static_mmAmqpConsumeArray_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArrayCallback_Destroy(struct mmAmqpConsumeArrayCallback* p)
{
    p->Handle = &__static_mmAmqpConsumeArray_HandleDefault;
    p->obj = NULL;
}

static void
__static_AmqpConsumeArray_ConsumePoperCallback(
    struct mmAmqpConsume* p,
    void* u,
    mmUInt8_t* buffer,
    mmUInt32_t offset,
    mmUInt32_t length);

static void __static_AmqpConsumeArray_PacketHandlePoper(void* obj, struct mmPacket* pack);

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Init(struct mmAmqpConsumeArray* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmAmqpExchange_Init(&p->amqp_exchange);
    mmAmqpQueue_Init(&p->amqp_queue);
    mmString_Init(&p->routing_key);
    mmString_Init(&p->index_key);
    mmAmqpConsumeArrayCallback_Init(&p->callback);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    p->consume_no_local = 0;
    p->consume_no_ack = 0;
    p->consume_exclusive = 0;
    p->sleep_empty_key_timeout = MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME;
    p->sleep_empty_val_timeout = MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME;
    p->sleep_error_ctx_timeout = MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME;
    p->pop_timeout = MM_AMQP_CONSUME_ERROR_POP_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
    //
    mmString_Init(&p->vhost);
    mmString_Init(&p->username);
    mmString_Init(&p->password);
    mmString_Init(&p->node);
    p->port = 0;
    p->channel_max = 0;
    p->frame_max = 131072;
    p->heartbeat = 0;
    p->conn_timeout = MM_AMQP_CONN_CONN_TIMEOUT;
    p->trytimes = MM_AMQP_CONN_TRYTIMES;
    p->channel = 1;
    //
    p->length = 0;
    p->arrays = NULL;
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->arrays_locker, NULL);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);

    mmString_Assigns(&p->index_key, "*");
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Destroy(struct mmAmqpConsumeArray* p)
{
    mmAmqpConsumeArray_Clear(p);
    //
    mmAmqpExchange_Destroy(&p->amqp_exchange);
    mmAmqpQueue_Destroy(&p->amqp_queue);
    mmString_Destroy(&p->routing_key);
    mmString_Destroy(&p->index_key);
    mmAmqpConsumeArrayCallback_Destroy(&p->callback);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmSpinlock_Destroy(&p->rbtree_locker);
    p->consume_no_local = 0;
    p->consume_no_ack = 0;
    p->consume_exclusive = 0;
    p->sleep_empty_key_timeout = MM_AMQP_CONSUME_EMPTY_KEY_SLEEP_TIME;
    p->sleep_empty_val_timeout = MM_AMQP_CONSUME_EMPTY_VAL_SLEEP_TIME;
    p->sleep_error_ctx_timeout = MM_AMQP_CONSUME_ERROR_CTX_SLEEP_TIME;
    p->pop_timeout = MM_AMQP_CONSUME_ERROR_POP_TIMEOUT;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
    //
    mmString_Destroy(&p->vhost);
    mmString_Destroy(&p->username);
    mmString_Destroy(&p->password);
    mmString_Destroy(&p->node);
    p->port = 0;
    p->channel_max = 0;
    p->frame_max = 131072;
    p->heartbeat = 0;
    p->conn_timeout = MM_AMQP_CONN_CONN_TIMEOUT;
    p->trytimes = MM_AMQP_CONN_TRYTIMES;
    p->channel = 1;
    //
    p->length = 0;
    p->arrays = NULL;
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetThreadNumber(struct mmAmqpConsumeArray* p, mmUInt32_t thread_number)
{
    mmAmqpConsumeArray_SetLength(p, thread_number);
}

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetContext(struct mmAmqpConsumeArray* p, void* u)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->u = u;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetContext(p->arrays[i], p->u);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetPopTimeout(struct mmAmqpConsumeArray* p, mmMSec_t pop_timeout)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->pop_timeout = pop_timeout;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetPopTimeout(p->arrays[i], p->pop_timeout);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetSleepTimeout(
    struct mmAmqpConsumeArray* p,
    mmMSec_t empty_key_timeout,
    mmMSec_t empty_val_timeout,
    mmMSec_t error_ctx_timeout)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->sleep_empty_key_timeout = empty_key_timeout;
    p->sleep_empty_val_timeout = empty_val_timeout;
    p->sleep_error_ctx_timeout = error_ctx_timeout;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetSleepTimeout(p->arrays[i], p->sleep_empty_key_timeout, p->sleep_empty_val_timeout, p->sleep_error_ctx_timeout);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetConsumeAttr(
    struct mmAmqpConsumeArray* p,
    mmUInt8_t no_local,
    mmUInt8_t no_ack,
    mmUInt8_t exclusive)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->consume_no_local = no_local;
    p->consume_no_ack = no_ack;
    p->consume_exclusive = exclusive;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetConsumeAttr(p->arrays[i], p->consume_no_local, p->consume_no_ack, p->consume_exclusive);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetExchange(struct mmAmqpConsumeArray* p, struct mmAmqpExchange* exchange)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assign(&p->amqp_exchange.exchange_name, &exchange->exchange_name);
    mmString_Assign(&p->amqp_exchange.exchange_type, &exchange->exchange_type);
    p->amqp_exchange.exchange_passive = exchange->exchange_passive;
    p->amqp_exchange.exchange_durable = exchange->exchange_durable;
    p->amqp_exchange.exchange_auto_delete = exchange->exchange_auto_delete;
    p->amqp_exchange.exchange_internal = exchange->exchange_internal;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetExchange(p->arrays[i], &p->amqp_exchange);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetQueue(struct mmAmqpConsumeArray* p, struct mmAmqpQueue* queue)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assign(&p->amqp_queue.queue_name, &queue->queue_name);
    p->amqp_queue.queue_passive = queue->queue_passive;
    p->amqp_queue.queue_durable = queue->queue_durable;
    p->amqp_queue.queue_exclusive = queue->queue_exclusive;
    p->amqp_queue.queue_auto_delete = queue->queue_auto_delete;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetQueue(p->arrays[i], &p->amqp_queue);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetRoutingKey(struct mmAmqpConsumeArray* p, const char* routing_key)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->routing_key, routing_key);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetRoutingKey(p->arrays[i], mmString_CStr(&p->routing_key));
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetIndexKey(struct mmAmqpConsumeArray* p, const char* index_key)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->index_key, index_key);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetIndexKey(p->arrays[i], mmString_CStr(&p->index_key));
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetDefaultCallback(
    struct mmAmqpConsumeArray* p,
    struct mmAmqpConsumeArrayCallback* default_callback)
{
    p->callback = *default_callback;
}

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetHandle(
    struct mmAmqpConsumeArray* p,
    mmUInt32_t id,
    mmAmqpConsumeArrayHandleFunc callback)
{
    mmUInt32_t i = 0;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
    //
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetEventHandle(p->arrays[i], id, callback);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetParameters(struct mmAmqpConsumeArray* p, const char* parameters)
{
    char node[MM_NODE_NAME_LENGTH] = { 0 };
    mmUShort_t port = 0;
    mmUInt32_t length = 0;
    //
    mmParameters_Server(parameters, node, &port, &length);
    //
    mmAmqpConsumeArray_SetRemote(p, node, port);
    mmAmqpConsumeArray_SetThreadNumber(p, length);
}

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetRemote(struct mmAmqpConsumeArray* p, const char* node, mmUShort_t port)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->node, node);
    p->port = port;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetRemote(p->arrays[i], mmString_CStr(&p->node), p->port);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetVhost(struct mmAmqpConsumeArray* p, const char* vhost)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->vhost, vhost);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetVhost(p->arrays[i], mmString_CStr(&p->vhost));
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetAccount(struct mmAmqpConsumeArray* p, const char* username, const char* password)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    mmString_Assigns(&p->username, username);
    mmString_Assigns(&p->password, password);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetAccount(p->arrays[i], mmString_CStr(&p->username), mmString_CStr(&p->password));
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetConnAttr(
    struct mmAmqpConsumeArray* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->channel_max = channel_max;
    p->frame_max = frame_max;
    p->heartbeat = heartbeat;
    p->channel = channel;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetConnAttr(p->arrays[i], p->channel_max, p->frame_max, p->heartbeat, p->channel);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void
mmAmqpConsumeArray_SetConnTimeout(
    struct mmAmqpConsumeArray* p,
    mmMSec_t conn_timeout,
    mmUInt32_t trytimes)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->conn_timeout = conn_timeout;
    p->trytimes = trytimes;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_SetConnTimeout(p->arrays[i], p->conn_timeout, p->trytimes);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_CopyRbtree(struct mmAmqpConsumeArray* p, struct mmAmqpConsume* amqp_consume)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    mmSpinlock_Lock(&p->rbtree_locker);
    mmSpinlock_Lock(&amqp_consume->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeU32Vpt_Set(&amqp_consume->rbtree, it->k, (void*)it->v);
    }
    mmSpinlock_Unlock(&amqp_consume->rbtree_locker);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

// must join before set length.
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_SetLength(struct mmAmqpConsumeArray* p, mmUInt32_t length)
{
    struct mmAmqpConsumeCallback amqp_consume_callback;
    amqp_consume_callback.Poper = &__static_AmqpConsumeArray_ConsumePoperCallback;
    amqp_consume_callback.obj = p;
    //
    mmSpinlock_Lock(&p->arrays_locker);
    if (length < p->length)
    {
        mmUInt32_t i = 0;
        struct mmAmqpConsume* e = NULL;
        for (i = length; i < p->length; ++i)
        {
            e = p->arrays[i];
            mmAmqpConsume_Lock(e);
            p->arrays[i] = NULL;
            mmAmqpConsume_Unlock(e);
            mmAmqpConsume_Destroy(e);
            mmFree(e);
        }
        p->arrays = (struct mmAmqpConsume**)mmRealloc(p->arrays, sizeof(struct mmAmqpConsume*) * length);
        p->length = length;
    }
    else if (length > p->length)
    {
        mmUInt32_t i = 0;
        struct mmAmqpConsume* e = NULL;
        p->arrays = (struct mmAmqpConsume**)mmRealloc(p->arrays, sizeof(struct mmAmqpConsume*) * length);
        for (i = p->length; i < length; ++i)
        {
            e = (struct mmAmqpConsume*)mmMalloc(sizeof(struct mmAmqpConsume));
            p->arrays[i] = e;
            mmAmqpConsume_Init(e);
            //
            mmAmqpConsume_SetContext(e, p->u);
            mmAmqpConsume_SetPopTimeout(e, p->pop_timeout);
            mmAmqpConsume_SetSleepTimeout(e, p->sleep_empty_key_timeout, p->sleep_empty_val_timeout, p->sleep_error_ctx_timeout);
            mmAmqpConsume_SetConsumeAttr(e, p->consume_no_local, p->consume_no_ack, p->consume_exclusive);
            mmAmqpConsume_SetExchange(e, &p->amqp_exchange);
            mmAmqpConsume_SetQueue(e, &p->amqp_queue);
            mmAmqpConsume_SetCallback(e, &amqp_consume_callback);
            mmAmqpConsume_SetRoutingKey(e, mmString_CStr(&p->routing_key));
            mmAmqpConsume_SetIndexKey(e, mmString_CStr(&p->index_key));
            //
            mmAmqpConsume_SetRemote(e, mmString_CStr(&p->node), p->port);
            mmAmqpConsume_SetVhost(e, mmString_CStr(&p->vhost));
            mmAmqpConsume_SetAccount(e, mmString_CStr(&p->username), mmString_CStr(&p->password));
            mmAmqpConsume_SetConnAttr(e, p->channel_max, p->frame_max, p->heartbeat, p->channel);
            mmAmqpConsume_SetConnTimeout(e, p->conn_timeout, p->trytimes);
            //
            mmAmqpConsumeArray_CopyRbtree(p, e);
            //
            e->state = p->state;
        }
        p->length = length;
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ mmUInt32_t mmAmqpConsumeArray_GetLength(struct mmAmqpConsumeArray* p)
{
    return p->length;
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Clear(struct mmAmqpConsumeArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_Destroy(p->arrays[i]);
        mmFree(p->arrays[i]);
    }
    mmFree(p->arrays);
    p->arrays = NULL;
    p->length = 0;
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_ApplyBroken(struct mmAmqpConsumeArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_ApplyBroken(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Start(struct mmAmqpConsumeArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_Start(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Interrupt(struct mmAmqpConsumeArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_CLOSED;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_Interrupt(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Shutdown(struct mmAmqpConsumeArray* p)
{
    mmUInt32_t i = 0;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH;
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_Shutdown(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_RABBITMQ void mmAmqpConsumeArray_Join(struct mmAmqpConsumeArray* p)
{
    mmUInt32_t i = 0;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        mmAmqpConsume_Join(p->arrays[i]);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

static void
__static_AmqpConsumeArray_ConsumePoperCallback(
    struct mmAmqpConsume* p,
    void* u,
    mmUInt8_t* buffer,
    mmUInt32_t offset,
    mmUInt32_t length)
{
    mmBufferPacket_HandleTcp(buffer, offset, length, __static_AmqpConsumeArray_PacketHandlePoper, p);
}

static void __static_AmqpConsumeArray_PacketHandlePoper(void* obj, struct mmPacket* pack)
{
    struct mmAmqpConsume* amqp_consume = (struct mmAmqpConsume*)(obj);
    struct mmAmqpConsumeArray* amqp_consume_array = (struct mmAmqpConsumeArray*)(amqp_consume->callback.obj);
    //
    mmAmqpConsumeArrayHandleFunc handle = NULL;
    mmSpinlock_Lock(&amqp_consume_array->rbtree_locker);
    handle = (mmAmqpConsumeArrayHandleFunc)mmRbtreeU32Vpt_Get(&amqp_consume_array->rbtree, pack->phead.mid);
    mmSpinlock_Unlock(&amqp_consume_array->rbtree_locker);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(amqp_consume, amqp_consume_array->u, pack);
    }
    else
    {
        assert(amqp_consume_array->callback.Handle && "message_consume->Handle is a null.");
        (*(amqp_consume_array->callback.Handle))(amqp_consume, amqp_consume_array->u, pack);
    }
}
