/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAmqpExchange.h"
#include "mmAmqpConn.h"
#include "core/mmLogger.h"

MM_EXPORT_RABBITMQ void mmAmqpExchange_Init(struct mmAmqpExchange* p)
{
    mmString_Init(&p->exchange_name);
    mmString_Init(&p->exchange_type);
    p->exchange_passive = 0;
    p->exchange_durable = 1;
    p->exchange_auto_delete = 0;
    p->exchange_internal = 0;
}
MM_EXPORT_RABBITMQ void mmAmqpExchange_Destroy(struct mmAmqpExchange* p)
{
    mmString_Destroy(&p->exchange_name);
    mmString_Destroy(&p->exchange_type);
    p->exchange_passive = 0;
    p->exchange_durable = 1;
    p->exchange_auto_delete = 0;
    p->exchange_internal = 0;
}

MM_EXPORT_RABBITMQ int mmAmqpExchange_Declare(struct mmAmqpExchange* p, struct mmAmqpConn* conn)
{
    int rt = -1;
    amqp_rpc_reply_t reply;
    amqp_exchange_declare_ok_t* exchange_declare_ok = NULL;
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmAmqpConn_ToString(conn, link_name);
    //
    do
    {
        // exchange declare.
        exchange_declare_ok = amqp_exchange_declare(
            conn->conn,
            conn->channel,
            amqp_cstring_bytes(mmString_CStr(&p->exchange_name)),
            amqp_cstring_bytes(mmString_CStr(&p->exchange_type)),
            p->exchange_passive,
            p->exchange_durable,
            p->exchange_auto_delete,
            p->exchange_internal,
            amqp_empty_table);
        //
        if (NULL == exchange_declare_ok)
        {
            reply = amqp_get_rpc_reply(conn->conn);
            if (0 != mmAmqp_RpcReplyError(reply, __FUNCTION__, __LINE__))
            {
                mmLogger_LogE(gLogger, "%s %d exchange_declare name:%s type:%s %s failure.",
                              __FUNCTION__, __LINE__, mmString_CStr(&p->exchange_name), mmString_CStr(&p->exchange_type), link_name);
                mmAmqpConn_ApplyBroken(conn);
                rt = -1;
                break;
            }
        }
        mmLogger_LogI(gLogger, "%s %d exchange_declare name:%s type:%s %s success.",
                      __FUNCTION__, __LINE__, mmString_CStr(&p->exchange_name), mmString_CStr(&p->exchange_type), link_name);
        rt = 0;
    } while (0);
    return rt;
}

