/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAmqpQueue_h__
#define __mmAmqpQueue_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"
#include "core/mmList.h"
#include "core/mmTime.h"
#include "net/mmSockaddr.h"
#include "net/mmPacket.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"

#include <pthread.h>

#include "rabbitmq/mmAmqpExport.h"

#include "core/mmPrefix.h"

struct mmAmqpConn;
struct mmAmqpConsume;
struct mmAmqpExchange;

struct mmAmqpQueue
{
    struct mmString queue_name;
    mmUInt8_t queue_passive;
    mmUInt8_t queue_durable;
    mmUInt8_t queue_exclusive;
    mmUInt8_t queue_auto_delete;
};

MM_EXPORT_RABBITMQ void mmAmqpQueue_Init(struct mmAmqpQueue* p);
MM_EXPORT_RABBITMQ void mmAmqpQueue_Destroy(struct mmAmqpQueue* p);

MM_EXPORT_RABBITMQ int mmAmqpQueue_Declare(struct mmAmqpQueue* p, struct mmAmqpConn* conn);

MM_EXPORT_RABBITMQ int
mmAmqpQueue_BindRoutingkeyReal(
    struct mmAmqpQueue* p,
    struct mmAmqpConn* conn,
    struct mmAmqpExchange* exchange,
    const char* routingkey_real);

MM_EXPORT_RABBITMQ int
mmAmqpQueue_Bind(
    struct mmAmqpQueue* p,
    struct mmAmqpConn* conn,
    struct mmAmqpExchange* exchange,
    struct mmAmqpConsume* consume);

MM_EXPORT_RABBITMQ int
mmAmqpQueue_BasicConsume(
    struct mmAmqpQueue* p,
    struct mmAmqpConn* conn,
    struct mmAmqpExchange* exchange,
    struct mmAmqpConsume* consume);

#include "core/mmSuffix.h"

#endif//__mmAmqpQueue_h__
