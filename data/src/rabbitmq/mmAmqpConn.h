/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAmqpConn_h__
#define __mmAmqpConn_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmAtomic.h"
#include "core/mmList.h"
#include "core/mmTime.h"
#include "net/mmSockaddr.h"
#include "net/mmPacket.h"

#include "amqp.h"
#include "amqp_tcp_socket.h"

#include <pthread.h>

#include "rabbitmq/mmAmqpExport.h"

#include "core/mmPrefix.h"

#define MM_MESSAGE_MID_KEY_FORMAT "%08X"
#define MM_MESSAGE_MID_KEY_LENGTH 32
#define MM_MESSAGE_IDX_KEY_FORMAT "%04u"
#define MM_MESSAGE_IDX_KEY_LENGTH 32

#define MM_AMQP_CONN_TRYTIMES 3
#define MM_AMQP_CONN_CONN_TIMEOUT 3000
#define MM_AMQP_CONN_TRY_MSLEEP_TIME 200

MM_EXPORT_RABBITMQ int mmAmqp_RpcReplyError(amqp_rpc_reply_t x, const char* func, int line);

enum mmAmqpConnState_t
{
    MM_AC_STATE_BROKEN = 0,
    MM_AC_STATE_FINISH = 1,
};

struct mmAmqpConn
{
    struct mmString vhost;
    struct mmString username;
    struct mmString password;
    struct mmString node;
    mmAtomic_t locker;
    mmUShort_t port;
    int channel_max;
    int frame_max;
    int heartbeat;
    // connect time out ms.default is 3s(3000ms)
    mmMSec_t conn_timeout;
    // try bind and listen times.default is MM_AMQP_CONN_TRYTIMES.
    mmUInt32_t trytimes;
    // flag for connect state.MM_AC_STATE_BROKEN is broken MM_AC_STATE_FINISH is work.
    mmSInt8_t conn_state;
    amqp_channel_t channel;
    amqp_socket_t* socket;
    amqp_connection_state_t conn;
    mmSInt8_t is_open_channel;
};

MM_EXPORT_RABBITMQ void mmAmqpConn_Init(struct mmAmqpConn* p);
MM_EXPORT_RABBITMQ void mmAmqpConn_Destroy(struct mmAmqpConn* p);

MM_EXPORT_RABBITMQ void mmAmqpConn_Lock(struct mmAmqpConn* p);
MM_EXPORT_RABBITMQ void mmAmqpConn_Unlock(struct mmAmqpConn* p);

MM_EXPORT_RABBITMQ void mmAmqpConn_SetRemote(struct mmAmqpConn* p, const char* node, mmUShort_t port);
MM_EXPORT_RABBITMQ void mmAmqpConn_SetVhost(struct mmAmqpConn* p, const char* vhost);
MM_EXPORT_RABBITMQ void mmAmqpConn_SetAccount(struct mmAmqpConn* p, const char* username, const char* password);

MM_EXPORT_RABBITMQ void
mmAmqpConn_SetConnAttr(
    struct mmAmqpConn* p,
    int channel_max,
    int frame_max,
    int heartbeat,
    amqp_channel_t channel);

MM_EXPORT_RABBITMQ void mmAmqpConn_SetConnTimeout(struct mmAmqpConn* p, mmMSec_t conn_timeout, mmUInt32_t trytimes);

MM_EXPORT_RABBITMQ void mmAmqpConn_ProduceConnectionState(struct mmAmqpConn* p);
MM_EXPORT_RABBITMQ void mmAmqpConn_RecycleConnectionState(struct mmAmqpConn* p);
// if amqp_connection_state_t is a null,will mmAmqpConn_ProduceConnectionState if have will do nothing.
MM_EXPORT_RABBITMQ void mmAmqpConn_InstanceConnectionState(struct mmAmqpConn* p);

MM_EXPORT_RABBITMQ int mmAmqpConn_Attach(struct mmAmqpConn* p);
MM_EXPORT_RABBITMQ int mmAmqpConn_Detach(struct mmAmqpConn* p);
MM_EXPORT_RABBITMQ void mmAmqpConn_ApplyBroken(struct mmAmqpConn* p);

// check and try connect if broken.
// 0 : is broken.if conn is not work, will connect single thread once.
// 1 : is working not broken.
MM_EXPORT_RABBITMQ int mmAmqpConn_Check(struct mmAmqpConn* p);

MM_EXPORT_RABBITMQ void mmAmqpConn_ToString(struct mmAmqpConn* p, char link_name[MM_LINK_NAME_LENGTH]);

#include "core/mmSuffix.h"

#endif//__mmAmqpConn_h__
