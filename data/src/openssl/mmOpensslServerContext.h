/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOpensslServerContext_h__
#define __mmOpensslServerContext_h__

#include "core/mmCore.h"
#include "core/mmPoolElement.h"
#include "core/mmString.h"

#include "net/mmTcp.h"

#include "openssl/mmOpensslRc4.h"
#include "openssl/mmOpensslRsa.h"

#include "openssl/mmOpensslExport.h"

#include "core/mmPrefix.h"

#define MM_OPENSSL_SERVER_RSA_SECRET_LENGTH_DEFAULT 1024
#define MM_OPENSSL_SERVER_RC4_SECRET_LENGTH_DEFAULT 16

struct mmOpensslServerContext
{
    struct mmPoolElement pool_element;
    struct mmOpensslRc4Array openssl_rc4_array;
    struct mmOpensslRsaArray openssl_rsa_array;
    struct mmOpensslRsa openssl_rsa;
    mmUInt32_t rsa_secret_length;
    mmUInt32_t rc4_secret_length;
};
MM_EXPORT_OPENSSL void mmOpensslServerContext_Init(struct mmOpensslServerContext* p);
MM_EXPORT_OPENSSL void mmOpensslServerContext_Destroy(struct mmOpensslServerContext* p);

MM_EXPORT_OPENSSL void mmOpensslServerContext_SetRsaSecretLength(struct mmOpensslServerContext* p, mmUInt32_t rsa_secret_length);
MM_EXPORT_OPENSSL void mmOpensslServerContext_SetRc4SecretLength(struct mmOpensslServerContext* p, mmUInt32_t rc4_secret_length);

MM_EXPORT_OPENSSL void mmOpensslServerContext_Srand(struct mmOpensslServerContext* p, mmUInt64_t seed);

MM_EXPORT_OPENSSL struct mmOpensslRc4* mmOpensslServerContext_Rc4ThreadInstance(struct mmOpensslServerContext* p);
MM_EXPORT_OPENSSL struct mmOpensslRsa* mmOpensslServerContext_RsaThreadInstance(struct mmOpensslServerContext* p);

MM_EXPORT_OPENSSL void mmOpensslServerContext_RsaPublicKeyClient(struct mmOpensslServerContext* p, struct mmString* public_key_client);

MM_EXPORT_OPENSSL void mmOpensslServerContext_RsaGenerateServer(struct mmOpensslServerContext* p);
MM_EXPORT_OPENSSL void mmOpensslServerContext_RsaPublicKeyServer(struct mmOpensslServerContext* p, struct mmString* public_key_server);
//
MM_EXPORT_OPENSSL void mmOpensslServerContext_Rc4GenerateKeyR(struct mmOpensslServerContext* p);
MM_EXPORT_OPENSSL void mmOpensslServerContext_Rc4KeyAssembly(struct mmOpensslServerContext* p);
//
MM_EXPORT_OPENSSL void mmOpensslServerContext_DecryptKeyL(struct mmOpensslServerContext* p, struct mmString* encrypt_key_l);
MM_EXPORT_OPENSSL void mmOpensslServerContext_EncryptKeyR(struct mmOpensslServerContext* p, struct mmString* encrypt_key_r);

MM_EXPORT_OPENSSL void mmOpensslServerContext_MailboxEventTcpProduce(void* p, struct mmTcp* tcp);
MM_EXPORT_OPENSSL void mmOpensslServerContext_MailboxEventTcpRecycle(void* p, struct mmTcp* tcp);

#include "core/mmSuffix.h"

#endif//__mmOpensslServerContext_h__
