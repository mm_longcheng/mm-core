/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslTcpContext.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "net/mmTcp.h"

MM_EXPORT_OPENSSL void mmOpensslTcpContext_Init(struct mmOpensslTcpContext* p)
{
    mmMemset(&p->context_send, 0, sizeof(RC4_KEY));
    mmMemset(&p->context_recv, 0, sizeof(RC4_KEY));
    mmSpinlock_Init(&p->locker, NULL);
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->state = MM_CRYPTO_CONTEXT_INACTIVE;
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Destroy(struct mmOpensslTcpContext* p)
{
    mmMemset(&p->context_send, 0, sizeof(RC4_KEY));
    mmMemset(&p->context_recv, 0, sizeof(RC4_KEY));
    mmSpinlock_Destroy(&p->locker);
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->state = MM_CRYPTO_CONTEXT_INACTIVE;
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Reset(struct mmOpensslTcpContext* p)
{
    mmMemset(&p->context_send, 0, sizeof(RC4_KEY));
    mmMemset(&p->context_recv, 0, sizeof(RC4_KEY));
    // spinlock not reset.
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->state = MM_CRYPTO_CONTEXT_INACTIVE;
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Lock(struct mmOpensslTcpContext* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Unlock(struct mmOpensslTcpContext* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetKey(struct mmOpensslTcpContext* p, const mmUInt8_t* buffer, size_t offset, size_t length)
{
    RC4_set_key(&p->context_send, (int)length, (const unsigned char*)(buffer + offset));
    RC4_set_key(&p->context_recv, (int)length, (const unsigned char*)(buffer + offset));
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetState(struct mmOpensslTcpContext* p, mmUInt8_t state)
{
    p->state = state;
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetTimecodeCreate(struct mmOpensslTcpContext* p, mmUInt32_t timecode_create)
{
    p->timecode_create = timecode_create;
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetTimecodeUpdate(struct mmOpensslTcpContext* p, mmUInt32_t timecode_update)
{
    p->timecode_update = timecode_update;
}

MM_EXPORT_OPENSSL void mmOpensslTcpContext_MailboxCryptoEncrypt(void* p, void* e, mmUInt8_t* buffer_i, size_t offset_i, mmUInt8_t* buffer_o, size_t offset_o, size_t length)
{
    // note: at this callback context is only one thread handle tcp, 
    // because when call this function must send recv api, 
    // when send and recv call will lock tcp context outside.
    // 
    // here is tcp lock free.
    struct mmTcp* tcp = (struct mmTcp*)(e);
    struct mmOpensslTcpContext* tcp_context = (struct mmOpensslTcpContext*)mmTcp_GetContext(tcp);
    // if tcp_context is NULL we do nothing.
    if (NULL != tcp_context && MM_CRYPTO_CONTEXT_ACTIVATE == tcp_context->state)
    {
        mmUInt8_t* i_data = buffer_i + offset_i;
        mmUInt8_t* o_data = buffer_o + offset_o;
        RC4(&tcp_context->context_send, (size_t)length, (const unsigned char*)i_data, (unsigned char*)o_data);
    }
}
MM_EXPORT_OPENSSL void mmOpensslTcpContext_MailboxCryptoDecrypt(void* p, void* e, mmUInt8_t* buffer_i, size_t offset_i, mmUInt8_t* buffer_o, size_t offset_o, size_t length)
{
    // note: at this callback context is only one thread handle tcp, 
    // because when call this function must send recv api, 
    // when send and recv call will lock tcp context outside.
    // 
    // here is tcp lock free.
    struct mmTcp* tcp = (struct mmTcp*)(e);
    struct mmOpensslTcpContext* tcp_context = (struct mmOpensslTcpContext*)mmTcp_GetContext(tcp);
    // if tcp_context is NULL we do nothing.
    if (NULL != tcp_context && MM_CRYPTO_CONTEXT_ACTIVATE == tcp_context->state)
    {
        mmUInt8_t* i_data = buffer_i + offset_i;
        mmUInt8_t* o_data = buffer_o + offset_o;
        RC4(&tcp_context->context_recv, (size_t)length, (const unsigned char*)i_data, (unsigned char*)o_data);
    }
}

