/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOpensslRc4_h__
#define __mmOpensslRc4_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmListVpt.h"

#include "random/mmXoshiro.h"

#include "openssl/mmOpensslExport.h"

#include <pthread.h>

#include "core/mmPrefix.h"

struct mmOpensslRc4
{
    struct mmXoshiro256starstar key_random;
    // key l.
    struct mmString key_l;
    // key r.
    struct mmString key_r;
    // key.
    struct mmString key;
    mmAtomic_t locker;
    mmUInt64_t seed;
};

MM_EXPORT_OPENSSL void mmOpensslRc4_Init(struct mmOpensslRc4* p);
MM_EXPORT_OPENSSL void mmOpensslRc4_Destroy(struct mmOpensslRc4* p);

MM_EXPORT_OPENSSL void mmOpensslRc4_Lock(struct mmOpensslRc4* p);
MM_EXPORT_OPENSSL void mmOpensslRc4_Unlock(struct mmOpensslRc4* p);

MM_EXPORT_OPENSSL void mmOpensslRc4_Srand(struct mmOpensslRc4* p, mmUInt64_t seed);
MM_EXPORT_OPENSSL void mmOpensslRc4_RandomBuffer(struct mmOpensslRc4* p, struct mmString* buffer, size_t length);
// note: 
// not append '\0' at end, you need append it youself if printf.
MM_EXPORT_OPENSSL void mmOpensslRc4_Assembly(struct mmOpensslRc4* p);

struct mmOpensslRc4Array
{
    // strong ref.
    struct mmListHead list;
    // thread key.
    pthread_key_t thread_key;
    mmAtomic_t list_locker;

    mmUInt64_t seed;
};

MM_EXPORT_OPENSSL void mmOpensslRc4Array_Init(struct mmOpensslRc4Array* p);
MM_EXPORT_OPENSSL void mmOpensslRc4Array_Destroy(struct mmOpensslRc4Array* p);

// recommend set length before thread instance.
MM_EXPORT_OPENSSL struct mmOpensslRc4* mmOpensslRc4Array_ThreadInstance(struct mmOpensslRc4Array* p);
MM_EXPORT_OPENSSL void mmOpensslRc4Array_Clear(struct mmOpensslRc4Array* p);

MM_EXPORT_OPENSSL void mmOpensslRc4Array_Srand(struct mmOpensslRc4Array* p, mmUInt64_t seed);

#include "core/mmSuffix.h"

#endif//__mmOpensslRc4_h__
