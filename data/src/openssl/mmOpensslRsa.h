/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOpensslRsa_h__
#define __mmOpensslRsa_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"

#include "container/mmListVpt.h"

#include "openssl/rsa.h"

#include "openssl/mmOpensslExport.h"

#include <pthread.h>

#include "core/mmPrefix.h"

#define MM_OPENSSL_RSA_BIO_BUFFER_PAGE 128

struct mmOpensslRsa
{
    // buffer public key
    struct mmStreambuf pri_mem;
    // buffer private key
    struct mmStreambuf pub_mem;
    
    // key password
    struct mmString key_password;
    //
    mmAtomic_t locker;
    //
    RSA* rsa;
    BIGNUM* e;
    
    // bio public key
    BIO* pri_bio;
    // bio private key
    BIO* pub_bio;
};
MM_EXPORT_OPENSSL void mmOpensslRsa_Init(struct mmOpensslRsa* p);
MM_EXPORT_OPENSSL void mmOpensslRsa_Destroy(struct mmOpensslRsa* p);

MM_EXPORT_OPENSSL void mmOpensslRsa_Lock(struct mmOpensslRsa* p);
MM_EXPORT_OPENSSL void mmOpensslRsa_Unlock(struct mmOpensslRsa* p);

MM_EXPORT_OPENSSL void mmOpensslRsa_SetKeyPassword(struct mmOpensslRsa* p, const char* key_password);

MM_EXPORT_OPENSSL void mmOpensslRsa_PriMemSet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length);
MM_EXPORT_OPENSSL void mmOpensslRsa_PubMemSet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length);
//
MM_EXPORT_OPENSSL size_t mmOpensslRsa_PriMemSize(struct mmOpensslRsa* p);
MM_EXPORT_OPENSSL size_t mmOpensslRsa_PubMemSize(struct mmOpensslRsa* p);
//
MM_EXPORT_OPENSSL void mmOpensslRsa_PriMemGet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length);
MM_EXPORT_OPENSSL void mmOpensslRsa_PubMemGet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length);

// RSA_generate_key_ex
// w recommond RSA_3.
MM_EXPORT_OPENSSL void mmOpensslRsa_Generate(struct mmOpensslRsa* p, int bits, BN_ULONG w);

// private
MM_EXPORT_OPENSSL void mmOpensslRsa_CtxToPriMem(struct mmOpensslRsa* p);
MM_EXPORT_OPENSSL void mmOpensslRsa_PriMemToCtx(struct mmOpensslRsa* p);
// public
MM_EXPORT_OPENSSL void mmOpensslRsa_CtxToPubMem(struct mmOpensslRsa* p);
MM_EXPORT_OPENSSL void mmOpensslRsa_PubMemToCtx(struct mmOpensslRsa* p);

// private
MM_EXPORT_OPENSSL int mmOpensslRsa_PriEncrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o);
MM_EXPORT_OPENSSL int mmOpensslRsa_PriDecrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o);
// public
MM_EXPORT_OPENSSL int mmOpensslRsa_PubEncrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o);
MM_EXPORT_OPENSSL int mmOpensslRsa_PubDecrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o);

struct mmOpensslRsaArray
{
    // strong ref.
    struct mmListHead list;
    // thread key.
    pthread_key_t thread_key;
    mmAtomic_t list_locker;
    
    // key password
    struct mmString key_password;
};

MM_EXPORT_OPENSSL void mmOpensslRsaArray_Init(struct mmOpensslRsaArray* p);
MM_EXPORT_OPENSSL void mmOpensslRsaArray_Destroy(struct mmOpensslRsaArray* p);

// recommend set length before thread instance.
MM_EXPORT_OPENSSL struct mmOpensslRsa* mmOpensslRsaArray_ThreadInstance(struct mmOpensslRsaArray* p);
MM_EXPORT_OPENSSL void mmOpensslRsaArray_Clear(struct mmOpensslRsaArray* p);

MM_EXPORT_OPENSSL void mmOpensslRsaArray_SetKeyPassword(struct mmOpensslRsaArray* p, const char* key_password);

#include "core/mmSuffix.h"

#endif//__mmOpensslRsa_h__
