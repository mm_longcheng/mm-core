/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslRsa.h"
#include "openssl/mmOpensslBio.h"
#include "openssl/pem.h"

#include "core/mmAlloc.h"

MM_EXPORT_OPENSSL void mmOpensslRsa_Init(struct mmOpensslRsa* p)
{
    mmStreambuf_Init(&p->pri_mem);
    mmStreambuf_Init(&p->pub_mem);

    mmString_Init(&p->key_password);

    mmSpinlock_Init(&p->locker, NULL);

    p->e = NULL;
    p->rsa = NULL;

    p->pri_bio = NULL;
    p->pub_bio = NULL;

    p->e = BN_new();
    p->rsa = RSA_new();
    p->pri_bio = BIO_new(BIO_s_mem());
    p->pub_bio = BIO_new(BIO_s_mem());

    mmString_Assigns(&p->key_password, "");
}
MM_EXPORT_OPENSSL void mmOpensslRsa_Destroy(struct mmOpensslRsa* p)
{
    BIO_free(p->pri_bio);
    BIO_free(p->pub_bio);
    BN_free(p->e);
    RSA_free(p->rsa);

    mmStreambuf_Destroy(&p->pri_mem);
    mmStreambuf_Destroy(&p->pub_mem);

    mmString_Destroy(&p->key_password);

    mmSpinlock_Destroy(&p->locker);

    p->e = NULL;
    p->rsa = NULL;
}
MM_EXPORT_OPENSSL void mmOpensslRsa_Lock(struct mmOpensslRsa* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_Unlock(struct mmOpensslRsa* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_SetKeyPassword(struct mmOpensslRsa* p, const char* key_password)
{
    mmString_Assigns(&p->key_password, key_password);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_PriMemSet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmStreambuf_Reset(&p->pri_mem);
    mmStreambuf_AlignedMemory(&p->pri_mem, length);
    mmStreambuf_Sputn(&p->pri_mem, buffer, offset, length);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_PubMemSet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmStreambuf_Reset(&p->pub_mem);
    mmStreambuf_AlignedMemory(&p->pub_mem, length);
    mmStreambuf_Sputn(&p->pub_mem, buffer, offset, length);
}
MM_EXPORT_OPENSSL size_t mmOpensslRsa_PriMemSize(struct mmOpensslRsa* p)
{
    return mmStreambuf_Size(&p->pri_mem);
}
MM_EXPORT_OPENSSL size_t mmOpensslRsa_PubMemSize(struct mmOpensslRsa* p)
{
    return mmStreambuf_Size(&p->pub_mem);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_PriMemGet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    size_t sz = mmStreambuf_Size(&p->pri_mem);
    mmMemcpy(buffer + offset, p->pri_mem.buff + p->pri_mem.gptr, sz);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_PubMemGet(struct mmOpensslRsa* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    size_t sz = mmStreambuf_Size(&p->pub_mem);
    mmMemcpy(buffer + offset, p->pub_mem.buff + p->pub_mem.gptr, sz);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_Generate(struct mmOpensslRsa* p, int bits, BN_ULONG w)
{
    // note:
    // we can not find RSA reset api.
    // if we not reset and reuse rsa context will case this error.
    //
    //     rsa_ossl_public_decrypt:padding check failed
    //
    RSA_free(p->rsa);
    p->rsa = RSA_new();

    // RSA_generate_key_ex api is a random generate.
    // do not random generate e.
    BN_set_word(p->e, w);
    // generate key.
    RSA_generate_key_ex(p->rsa, bits, p->e, NULL);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_CtxToPriMem(struct mmOpensslRsa* p)
{
    // reset pri_bio.
    (void)BIO_reset(p->pri_bio);
    // return code true false.
    PEM_write_bio_RSAPrivateKey(p->pri_bio, p->rsa, NULL, NULL, 0, NULL, (void*)mmString_Data(&p->key_password));
    // reset pri_mem.
    mmStreambuf_Reset(&p->pri_mem);
    // recv pri_bio to pri_mem.
    mmOpensslBio_HandleRecv(p->pri_bio, &p->pri_mem);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_PriMemToCtx(struct mmOpensslRsa* p)
{
    size_t gptr = 0;

    // note:
    // we can not find RSA reset api.
    // if we not reset and reuse rsa context will case this error.
    //
    //     rsa_ossl_public_decrypt:padding check failed
    //
    RSA_free(p->rsa);
    p->rsa = RSA_new();

    gptr = p->pri_mem.gptr;
    // reset pri_bio.
    (void)BIO_reset(p->pri_bio);
    // send pri_mem to pri_bio.
    mmOpensslBio_FlushSend(p->pri_bio, &p->pri_mem);
    // reset gptr.
    mmStreambuf_SetGPtr(&p->pri_mem, gptr);
    // return rsa.
    PEM_read_bio_RSAPrivateKey(p->pri_bio, &p->rsa, NULL, (void*)mmString_Data(&p->key_password));
}
// public
MM_EXPORT_OPENSSL void mmOpensslRsa_CtxToPubMem(struct mmOpensslRsa* p)
{
    // reset pub_bio.
    (void)BIO_reset(p->pub_bio);
    // return code true false.
    PEM_write_bio_RSAPublicKey(p->pub_bio, p->rsa);
    // reset pub_mem.
    mmStreambuf_Reset(&p->pub_mem);
    // recv pub_bio.
    mmOpensslBio_HandleRecv(p->pub_bio, &p->pub_mem);
}
MM_EXPORT_OPENSSL void mmOpensslRsa_PubMemToCtx(struct mmOpensslRsa* p)
{
    size_t gptr = 0;

    // note:
    // we can not find RSA reset api.
    // if we not reset and reuse rsa context will case this error.
    //
    //     rsa_ossl_public_decrypt:padding check failed
    //
    RSA_free(p->rsa);
    p->rsa = RSA_new();

    gptr = p->pub_mem.gptr;
    // reset pub_bio.
    (void)BIO_reset(p->pub_bio);
    // send pub_bio to pub_mem.
    mmOpensslBio_FlushSend(p->pub_bio, &p->pub_mem);
    // reset gptr.
    mmStreambuf_SetGPtr(&p->pub_mem, gptr);
    // return rsa.
    PEM_read_bio_RSAPublicKey(p->pub_bio, &p->rsa, NULL, (void*)mmString_Data(&p->key_password));
}
MM_EXPORT_OPENSSL int mmOpensslRsa_PriEncrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o)
{
    int pri_key_encrypt_len = 0;
    size_t il = (size_t)mmString_Size(i);
    const unsigned char* is = (const unsigned char*)mmString_Data(i);
    unsigned char* os = NULL;
    assert(NULL != p->rsa && "NULL != p->rsa is invalid");
    mmString_Resize(o, RSA_size(p->rsa));
    os = (unsigned char*)mmString_Data(o);
    pri_key_encrypt_len = RSA_private_encrypt((int)il, is, os, p->rsa, RSA_PKCS1_PADDING);
    if (0 > pri_key_encrypt_len)
    {
        mmString_Clear(o);
    }
    else
    {
        mmString_Resize(o, pri_key_encrypt_len);
    }
    return pri_key_encrypt_len;
}
MM_EXPORT_OPENSSL int mmOpensslRsa_PriDecrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o)
{
    int pri_key_decrypt_len = 0;
    size_t il = (size_t)mmString_Size(i);
    const unsigned char* is = (const unsigned char*)mmString_Data(i);
    unsigned char* os = NULL;
    assert(NULL != p->rsa && "NULL != p->rsa is invalid");
    mmString_Resize(o, RSA_size(p->rsa));
    os = (unsigned char*)mmString_Data(o);
    pri_key_decrypt_len = RSA_private_decrypt((int)il, is, os, p->rsa, RSA_PKCS1_PADDING);
    if (0 > pri_key_decrypt_len)
    {
        mmString_Clear(o);
    }
    else
    {
        mmString_Resize(o, pri_key_decrypt_len);
    }
    return pri_key_decrypt_len;
}

MM_EXPORT_OPENSSL int mmOpensslRsa_PubEncrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o)
{
    int pri_key_encrypt_len = 0;
    size_t il = (size_t)mmString_Size(i);
    const unsigned char* is = (const unsigned char*)mmString_Data(i);
    unsigned char* os = NULL;
    assert(NULL != p->rsa && "NULL != p->rsa is invalid");
    mmString_Resize(o, RSA_size(p->rsa));
    os = (unsigned char*)mmString_Data(o);
    pri_key_encrypt_len = RSA_public_encrypt((int)il, is, os, p->rsa, RSA_PKCS1_PADDING);
    if (0 > pri_key_encrypt_len)
    {
        mmString_Clear(o);
    }
    else
    {
        mmString_Resize(o, pri_key_encrypt_len);
    }
    return pri_key_encrypt_len;
}
MM_EXPORT_OPENSSL int mmOpensslRsa_PubDecrypt(struct mmOpensslRsa* p, struct mmString* i, struct mmString* o)
{
    int pri_key_decrypt_len = 0;
    size_t il = (size_t)mmString_Size(i);
    const unsigned char* is = (const unsigned char*)mmString_Data(i);
    unsigned char* os = NULL;
    assert(NULL != p->rsa && "NULL != p->rsa is invalid");
    mmString_Resize(o, RSA_size(p->rsa));
    os = (unsigned char*)mmString_Data(o);
    pri_key_decrypt_len = RSA_public_decrypt((int)il, is, os, p->rsa, RSA_PKCS1_PADDING);
    if (0 > pri_key_decrypt_len)
    {
        mmString_Clear(o);
    }
    else
    {
        mmString_Resize(o, pri_key_decrypt_len);
    }
    return pri_key_decrypt_len;
}

MM_EXPORT_OPENSSL void mmOpensslRsaArray_Init(struct mmOpensslRsaArray* p)
{
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_create(&p->thread_key, NULL);
    mmSpinlock_Init(&p->list_locker, NULL);

    mmString_Init(&p->key_password);
}
MM_EXPORT_OPENSSL void mmOpensslRsaArray_Destroy(struct mmOpensslRsaArray* p)
{
    mmOpensslRsaArray_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_delete(p->thread_key);
    mmSpinlock_Destroy(&p->list_locker);

    mmString_Destroy(&p->key_password);
}

// recommend set length before thread instance.
MM_EXPORT_OPENSSL struct mmOpensslRsa* mmOpensslRsaArray_ThreadInstance(struct mmOpensslRsaArray* p)
{
    struct mmOpensslRsa* e = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (!lvp)
    {
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        e = (struct mmOpensslRsa*)mmMalloc(sizeof(struct mmOpensslRsa));
        mmOpensslRsa_Init(e);
        //
        mmOpensslRsa_SetKeyPassword(e, mmString_CStr(&p->key_password));
        //
        lvp->v = e;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        e = (struct mmOpensslRsa*)(lvp->v);
    }
    return e;
}
MM_EXPORT_OPENSSL void mmOpensslRsaArray_Clear(struct mmOpensslRsaArray* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmOpensslRsa* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);        
        e = (struct mmOpensslRsa*)(lvp->v);
        mmOpensslRsa_Lock(e);
        mmList_Del(curr);
        mmOpensslRsa_Unlock(e);
        mmOpensslRsa_Destroy(e);
        mmFree(e);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_OPENSSL void mmOpensslRsaArray_SetKeyPassword(struct mmOpensslRsaArray* p, const char* key_password)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmOpensslRsa* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmString_Assigns(&p->key_password, key_password);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmOpensslRsa*)(lvp->v);
        mmOpensslRsa_Lock(e);
        mmOpensslRsa_SetKeyPassword(e, mmString_CStr(&p->key_password));
        mmOpensslRsa_Unlock(e);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
