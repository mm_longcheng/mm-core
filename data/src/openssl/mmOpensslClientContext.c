/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslClientContext.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

MM_EXPORT_OPENSSL void mmOpensslClientContext_Init(struct mmOpensslClientContext* p)
{
    mmOpensslRsa_Init(&p->openssl_rsa_client);
    mmOpensslRsa_Init(&p->openssl_rsa_server);
    mmOpensslRc4_Init(&p->openssl_rc4);
    mmOpensslTcpContext_Init(&p->openssl_tcp_context);
    p->rsa_secret_length = MM_OPENSSL_CLIENT_RSA_SECRET_LENGTH_DEFAULT;
    p->rc4_secret_length = MM_OPENSSL_CLIENT_RC4_SECRET_LENGTH_DEFAULT;
}
MM_EXPORT_OPENSSL void mmOpensslClientContext_Destroy(struct mmOpensslClientContext* p)
{
    mmOpensslRsa_Destroy(&p->openssl_rsa_client);
    mmOpensslRsa_Destroy(&p->openssl_rsa_server);
    mmOpensslRc4_Destroy(&p->openssl_rc4);
    mmOpensslTcpContext_Destroy(&p->openssl_tcp_context);
    p->rsa_secret_length = MM_OPENSSL_CLIENT_RSA_SECRET_LENGTH_DEFAULT;
    p->rc4_secret_length = MM_OPENSSL_CLIENT_RC4_SECRET_LENGTH_DEFAULT;
}

MM_EXPORT_OPENSSL void mmOpensslClientContext_SetRsaSecretLength(struct mmOpensslClientContext* p, mmUInt32_t rsa_secret_length)
{
    p->rsa_secret_length = rsa_secret_length;
}
MM_EXPORT_OPENSSL void mmOpensslClientContext_SetRc4SecretLength(struct mmOpensslClientContext* p, mmUInt32_t rc4_secret_length)
{
    p->rc4_secret_length = rc4_secret_length;
}

MM_EXPORT_OPENSSL void mmOpensslClientContext_Srand(struct mmOpensslClientContext* p, mmUInt64_t seed)
{
    mmOpensslRc4_Lock(&p->openssl_rc4);

    // srand the rc4 random.
    mmOpensslRc4_Srand(&p->openssl_rc4, seed);

    mmOpensslRc4_Unlock(&p->openssl_rc4);
}

MM_EXPORT_OPENSSL void mmOpensslClientContext_RsaPublicKeyServer(struct mmOpensslClientContext* p, struct mmString* public_key_server)
{
    const char* kd = mmString_Data(public_key_server);
    size_t kl = mmString_Size(public_key_server);

    mmOpensslRsa_Lock(&p->openssl_rsa_server);

    // set public key.
    mmOpensslRsa_PubMemSet(&p->openssl_rsa_server, (mmUInt8_t*)kd, 0, (size_t)kl);
    // public key to ctx.
    mmOpensslRsa_PubMemToCtx(&p->openssl_rsa_server);

    mmOpensslRsa_Unlock(&p->openssl_rsa_server);
}

MM_EXPORT_OPENSSL void mmOpensslClientContext_RsaGenerateClient(struct mmOpensslClientContext* p)
{
    mmOpensslRsa_Lock(&p->openssl_rsa_client);

    // generate client rsa to ctx. but not get the string public key to pub_mem.
    mmOpensslRsa_Generate(&p->openssl_rsa_client, p->rsa_secret_length, RSA_3);
    // generate rsa public key to pub_mem.
    mmOpensslRsa_CtxToPubMem(&p->openssl_rsa_client);

    mmOpensslRsa_Unlock(&p->openssl_rsa_client);
}
MM_EXPORT_OPENSSL void mmOpensslClientContext_RsaPublicKeyClient(struct mmOpensslClientContext* p, struct mmString* public_key_client)
{
    size_t mem_size = 0;
    const char* kd = NULL;

    mmOpensslRsa_Lock(&p->openssl_rsa_client);

    // get client rsa public key size.
    mem_size = mmOpensslRsa_PubMemSize(&p->openssl_rsa_client);
    // resize public_key_client.
    mmString_Resize(public_key_client, mem_size + 1);
    kd = mmString_Data(public_key_client);
    mmMemset((void*)kd, 0, mem_size + 1);
    // get client rsa public key to public_key_client
    mmOpensslRsa_PubMemGet(&p->openssl_rsa_client, (mmUInt8_t*)kd, 0, mem_size);

    mmOpensslRsa_Unlock(&p->openssl_rsa_client);
}
MM_EXPORT_OPENSSL void mmOpensslClientContext_Rc4GenerateKeyL(struct mmOpensslClientContext* p)
{
    // rc4 random buffer to key_l.
    mmOpensslRc4_Lock(&p->openssl_rc4);
    //mm_openssl_rc4_key_lock(&p->openssl_rc4_key);

    mmOpensslRc4_RandomBuffer(&p->openssl_rc4, &p->openssl_rc4.key_l, p->rc4_secret_length);

    //mm_openssl_rc4_key_unlock(&p->openssl_rc4_key);
    mmOpensslRc4_Unlock(&p->openssl_rc4);
}
MM_EXPORT_OPENSSL void mmOpensslClientContext_Rc4KeyAssembly(struct mmOpensslClientContext* p)
{
    mmOpensslRc4_Lock(&p->openssl_rc4);

    // assembly key_l and key_r to rc4 key.
    mmOpensslRc4_Assembly(&p->openssl_rc4);

    mmOpensslRc4_Unlock(&p->openssl_rc4);
}
MM_EXPORT_OPENSSL void mmOpensslClientContext_EncryptKeyL(struct mmOpensslClientContext* p, struct mmString* encrypt_key_l)
{
    // server rsa public encrypt key_l to encrypt_key_l.
    mmOpensslRsa_Lock(&p->openssl_rsa_server);
    mmOpensslRc4_Lock(&p->openssl_rc4);

    mmOpensslRsa_PubEncrypt(&p->openssl_rsa_server, &p->openssl_rc4.key_l, encrypt_key_l);

    mmOpensslRc4_Unlock(&p->openssl_rc4);
    mmOpensslRsa_Unlock(&p->openssl_rsa_server);
}

MM_EXPORT_OPENSSL void mmOpensslClientContext_DecryptKeyR(struct mmOpensslClientContext* p, struct mmString* encrypt_key_r)
{
    mmOpensslRsa_Lock(&p->openssl_rsa_client);
    mmOpensslRc4_Lock(&p->openssl_rc4);

    // use client rsa private decrypt encrypt_key_r to key_r
    mmOpensslRsa_PriDecrypt(&p->openssl_rsa_client, encrypt_key_r, &p->openssl_rc4.key_r);

    mmOpensslRc4_Unlock(&p->openssl_rc4);
    mmOpensslRsa_Unlock(&p->openssl_rsa_client);
}
