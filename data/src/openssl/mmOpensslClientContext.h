/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOpensslClientContext_h__
#define __mmOpensslClientContext_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmPoolElement.h"

#include "openssl/mmOpensslRc4.h"
#include "openssl/mmOpensslRsa.h"
#include "openssl/mmOpensslTcpContext.h"

#include "openssl/mmOpensslExport.h"

#include "core/mmPrefix.h"

#define MM_OPENSSL_CLIENT_RSA_SECRET_LENGTH_DEFAULT 1024
#define MM_OPENSSL_CLIENT_RC4_SECRET_LENGTH_DEFAULT 16

struct mmOpensslClientContext
{
    struct mmOpensslRsa openssl_rsa_client;
    struct mmOpensslRsa openssl_rsa_server;
    struct mmOpensslRc4 openssl_rc4;
    struct mmOpensslTcpContext openssl_tcp_context;
    mmUInt32_t rsa_secret_length;
    mmUInt32_t rc4_secret_length;
};
MM_EXPORT_OPENSSL void mmOpensslClientContext_Init(struct mmOpensslClientContext* p);
MM_EXPORT_OPENSSL void mmOpensslClientContext_Destroy(struct mmOpensslClientContext* p);

MM_EXPORT_OPENSSL void mmOpensslClientContext_SetRsaSecretLength(struct mmOpensslClientContext* p, mmUInt32_t rsa_secret_length);
MM_EXPORT_OPENSSL void mmOpensslClientContext_SetRc4SecretLength(struct mmOpensslClientContext* p, mmUInt32_t rc4_secret_length);

MM_EXPORT_OPENSSL void mmOpensslClientContext_Srand(struct mmOpensslClientContext* p, mmUInt64_t seed);

MM_EXPORT_OPENSSL void mmOpensslClientContext_RsaPublicKeyServer(struct mmOpensslClientContext* p, struct mmString* public_key_server);
//
MM_EXPORT_OPENSSL void mmOpensslClientContext_RsaGenerateClient(struct mmOpensslClientContext* p);
MM_EXPORT_OPENSSL void mmOpensslClientContext_RsaPublicKeyClient(struct mmOpensslClientContext* p, struct mmString* public_key_client);
//
MM_EXPORT_OPENSSL void mmOpensslClientContext_Rc4GenerateKeyL(struct mmOpensslClientContext* p);
MM_EXPORT_OPENSSL void mmOpensslClientContext_Rc4KeyAssembly(struct mmOpensslClientContext* p);
//
MM_EXPORT_OPENSSL void mmOpensslClientContext_EncryptKeyL(struct mmOpensslClientContext* p, struct mmString* encrypt_key_l);
MM_EXPORT_OPENSSL void mmOpensslClientContext_DecryptKeyR(struct mmOpensslClientContext* p, struct mmString* encrypt_key_r);

#include "core/mmSuffix.h"

#endif//__mmOpensslClientContext_h__
