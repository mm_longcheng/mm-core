/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslBn.h"

#include "core/mmAlloc.h"

MM_EXPORT_OPENSSL void mmOpenssl_Bn2raw(BIGNUM* bn, struct mmString* raw)
{
    char* rd = NULL;
    size_t rl = 0;
    mmString_Clear(raw);
    mmString_Resize(raw, BN_num_bytes(bn));
    rd = (char*)mmString_Data(raw);
    rl = mmString_Size(raw);
    BN_bn2bin(bn, (unsigned char*)rd);
    if (rl % 2 == 1 && rd[0] == 0x00)
    {
        mmString_Substr(raw, raw, 1, rl - 1);
    }
}
MM_EXPORT_OPENSSL void mmOpenssl_Raw2bn(struct mmString* raw, BIGNUM* bn)
{
    char* rd = (char*)mmString_Data(raw);
    size_t rl = mmString_Size(raw);
    if ((mmUInt8_t)(rd[0]) >= 0x80)
    {
        mmString_Resize(raw, rl + 1);
        rd = (char*)mmString_Data(raw);
        rl = mmString_Size(raw);
        mmMemmove(rd + 1, rd, rl - 1);
        rd[0] = 0x00;
    }
    BN_bin2bn((const unsigned char*)rd, (int)rl, bn);
}
