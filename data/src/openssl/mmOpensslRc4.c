/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslRc4.h"

#include "core/mmAlloc.h"

#include "dish/mmXoshiroString.h"

MM_EXPORT_OPENSSL void mmOpensslRc4_Init(struct mmOpensslRc4* p)
{
    mmXoshiro256starstar_Init(&p->key_random);
    mmString_Init(&p->key_l);
    mmString_Init(&p->key_r);
    mmString_Init(&p->key);
    mmSpinlock_Init(&p->locker, NULL);
    p->seed = 0;
}
MM_EXPORT_OPENSSL void mmOpensslRc4_Destroy(struct mmOpensslRc4* p)
{
    mmXoshiro256starstar_Destroy(&p->key_random);
    mmString_Destroy(&p->key_l);
    mmString_Destroy(&p->key_r);
    mmString_Destroy(&p->key);
    mmSpinlock_Destroy(&p->locker);
    p->seed = 0;
}
MM_EXPORT_OPENSSL void mmOpensslRc4_Lock(struct mmOpensslRc4* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_OPENSSL void mmOpensslRc4_Unlock(struct mmOpensslRc4* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_OPENSSL void mmOpensslRc4_Srand(struct mmOpensslRc4* p, mmUInt64_t seed)
{
    p->seed = seed;
    mmXoshiro256starstar_Srand(&p->key_random, p->seed);
}
MM_EXPORT_OPENSSL void mmOpensslRc4_RandomBuffer(struct mmOpensslRc4* p, struct mmString* buffer, size_t length)
{
    // random buffer.
    mmString_Resize(buffer, length);
    //
    mmXoshiro256starstar_RandomBuffer(&p->key_random, (unsigned char*)mmString_Data(buffer), 0, (size_t)length);
}
MM_EXPORT_OPENSSL void mmOpensslRc4_Assembly(struct mmOpensslRc4* p)
{
    size_t offset = 0;
    char* kd = NULL;
    size_t ll = mmString_Size(&p->key_l);
    size_t rl = mmString_Size(&p->key_r);
    const char* ld = mmString_Data(&p->key_l);
    const char* rd = mmString_Data(&p->key_r);
    size_t key_length = ll + rl;
    //
    mmString_Resize(&p->key, key_length);
    kd = (char*)mmString_Data(&p->key);
    mmMemcpy(kd + offset, ld, ll);
    offset = ll;
    mmMemcpy(kd + offset, rd, ll);
}

MM_EXPORT_OPENSSL void mmOpensslRc4Array_Init(struct mmOpensslRc4Array* p)
{
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_create(&p->thread_key, NULL);
    mmSpinlock_Init(&p->list_locker, NULL);

    p->seed = 0;
}
MM_EXPORT_OPENSSL void mmOpensslRc4Array_Destroy(struct mmOpensslRc4Array* p)
{
    mmOpensslRc4Array_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->list);
    pthread_key_delete(p->thread_key);
    mmSpinlock_Destroy(&p->list_locker);

    p->seed = 0;
}

// recommend set length before thread instance.
MM_EXPORT_OPENSSL struct mmOpensslRc4* mmOpensslRc4Array_ThreadInstance(struct mmOpensslRc4Array* p)
{
    struct mmOpensslRc4* e = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (!lvp)
    {
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        e = (struct mmOpensslRc4*)mmMalloc(sizeof(struct mmOpensslRc4));
        mmOpensslRc4_Init(e);
        //
        mmOpensslRc4_Srand(e, p->seed);
        //
        lvp->v = e;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        e = (struct mmOpensslRc4*)(lvp->v);
    }
    return e;
}
MM_EXPORT_OPENSSL void mmOpensslRc4Array_Clear(struct mmOpensslRc4Array* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmOpensslRc4* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);        
        e = (struct mmOpensslRc4*)(lvp->v);
        mmOpensslRc4_Lock(e);
        mmList_Del(curr);
        mmOpensslRc4_Unlock(e);
        mmOpensslRc4_Destroy(e);
        mmFree(e);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_OPENSSL void mmOpensslRc4Array_Srand(struct mmOpensslRc4Array* p, mmUInt64_t seed)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmOpensslRc4* e = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->seed = seed;
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmOpensslRc4*)(lvp->v);
        mmOpensslRc4_Lock(e);
        mmOpensslRc4_Srand(e, p->seed);
        mmOpensslRc4_Unlock(e);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
