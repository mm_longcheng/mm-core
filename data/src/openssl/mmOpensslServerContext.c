/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslServerContext.h"
#include "mmOpensslTcpContext.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmTime.h"

#include "net/mmMailbox.h"

MM_EXPORT_OPENSSL void mmOpensslServerContext_Init(struct mmOpensslServerContext* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmPoolElement_Init(&p->pool_element);
    mmOpensslRc4Array_Init(&p->openssl_rc4_array);
    mmOpensslRsaArray_Init(&p->openssl_rsa_array);
    mmOpensslRsa_Init(&p->openssl_rsa);
    p->rsa_secret_length = MM_OPENSSL_SERVER_RSA_SECRET_LENGTH_DEFAULT;
    p->rc4_secret_length = MM_OPENSSL_SERVER_RC4_SECRET_LENGTH_DEFAULT;

    hAllocator.Produce = &mmOpensslTcpContext_Init;
    hAllocator.Recycle = &mmOpensslTcpContext_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmOpensslTcpContext));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_Destroy(struct mmOpensslServerContext* p)
{
    mmPoolElement_Destroy(&p->pool_element);
    mmOpensslRc4Array_Destroy(&p->openssl_rc4_array);
    mmOpensslRsaArray_Destroy(&p->openssl_rsa_array);
    mmOpensslRsa_Destroy(&p->openssl_rsa);
    p->rsa_secret_length = MM_OPENSSL_SERVER_RSA_SECRET_LENGTH_DEFAULT;
    p->rc4_secret_length = MM_OPENSSL_SERVER_RC4_SECRET_LENGTH_DEFAULT;
}

MM_EXPORT_OPENSSL void mmOpensslServerContext_SetRsaSecretLength(struct mmOpensslServerContext* p, mmUInt32_t rsa_secret_length)
{
    p->rsa_secret_length = rsa_secret_length;
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_SetRc4SecretLength(struct mmOpensslServerContext* p, mmUInt32_t rc4_secret_length)
{
    p->rc4_secret_length = rc4_secret_length;
}

MM_EXPORT_OPENSSL void mmOpensslServerContext_Srand(struct mmOpensslServerContext* p, mmUInt64_t seed)
{
    // srand the rc4 random. openssl_rc4_array is lock free.
    mmOpensslRc4Array_Srand(&p->openssl_rc4_array, seed);
}
MM_EXPORT_OPENSSL struct mmOpensslRc4* mmOpensslServerContext_Rc4ThreadInstance(struct mmOpensslServerContext* p)
{
    // thread local cache. mmOpensslRc4Array is lock free.
    return mmOpensslRc4Array_ThreadInstance(&p->openssl_rc4_array);
}
MM_EXPORT_OPENSSL struct mmOpensslRsa* mmOpensslServerContext_RsaThreadInstance(struct mmOpensslServerContext* p)
{
    // thread local cache. openssl_rsa_array is lock free.
    return mmOpensslRsaArray_ThreadInstance(&p->openssl_rsa_array);
}

MM_EXPORT_OPENSSL void mmOpensslServerContext_RsaPublicKeyClient(struct mmOpensslServerContext* p, struct mmString* public_key_client)
{
    // thread local cache. openssl_rsa_array is lock free.
    struct mmOpensslRsa* openssl_rsa = mmOpensslRsaArray_ThreadInstance(&p->openssl_rsa_array);

    const char* kd = mmString_Data(public_key_client);
    size_t kl = mmString_Size(public_key_client);

    // encrypt rc4_key_r.
    // lock openssl_rsa.
    mmOpensslRsa_Lock(openssl_rsa);
    // set public key.
    mmOpensslRsa_PubMemSet(openssl_rsa, (mmUInt8_t*)kd, 0, (size_t)kl);
    // public key to ctx.
    mmOpensslRsa_PubMemToCtx(openssl_rsa);
    // unlock openssl_rsa.
    mmOpensslRsa_Unlock(openssl_rsa);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_RsaGenerateServer(struct mmOpensslServerContext* p)
{
    mmOpensslRsa_Lock(&p->openssl_rsa);

    // openssl new rsa key.
    mmOpensslRsa_Generate(&p->openssl_rsa, p->rsa_secret_length, RSA_3);
    // cache key.
    mmOpensslRsa_CtxToPubMem(&p->openssl_rsa);

    mmOpensslRsa_Unlock(&p->openssl_rsa);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_RsaPublicKeyServer(struct mmOpensslServerContext* p, struct mmString* public_key_server)
{
    size_t mem_size = 0;
    const char* kd = NULL;

    mmOpensslRsa_Lock(&p->openssl_rsa);

    // openssl public key.
    mem_size = mmOpensslRsa_PubMemSize(&p->openssl_rsa);
    mmString_Resize(public_key_server, mem_size + 1);
    kd = mmString_Data(public_key_server);
    mmMemset((void*)kd, 0, mem_size + 1);
    mmOpensslRsa_PubMemGet(&p->openssl_rsa, (mmUInt8_t*)kd, 0, mem_size);

    mmOpensslRsa_Unlock(&p->openssl_rsa);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_Rc4GenerateKeyR(struct mmOpensslServerContext* p)
{
    // thread local cache. mmOpensslRc4Array is lock free.
    struct mmOpensslRc4* openssl_rc4 = mmOpensslRc4Array_ThreadInstance(&p->openssl_rc4_array);

    // openssl_rc4_array is lock free.
    // rc4 key rc4_key_r generate.
    mmOpensslRc4_Lock(openssl_rc4);
    mmOpensslRc4_RandomBuffer(openssl_rc4, &openssl_rc4->key_r, p->rc4_secret_length);
    mmOpensslRc4_Unlock(openssl_rc4);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_Rc4KeyAssembly(struct mmOpensslServerContext* p)
{
    // thread local cache. mmOpensslRc4Array is lock free.
    struct mmOpensslRc4* openssl_rc4 = mmOpensslRc4Array_ThreadInstance(&p->openssl_rc4_array);

    mmOpensslRc4_Lock(openssl_rc4);

    // rc4 key assembly.
    mmOpensslRc4_Assembly(openssl_rc4);

    mmOpensslRc4_Unlock(openssl_rc4);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_DecryptKeyL(struct mmOpensslServerContext* p, struct mmString* encrypt_key_l)
{
    // thread local cache. mmOpensslRc4Array is lock free.
    struct mmOpensslRc4* openssl_rc4 = mmOpensslRc4Array_ThreadInstance(&p->openssl_rc4_array);

    // decode rc4_key_l.
    mmOpensslRsa_Lock(&p->openssl_rsa);
    mmOpensslRc4_Lock(openssl_rc4);

    // private key decrypt rc4_key_l.
    mmOpensslRsa_PriDecrypt(&p->openssl_rsa, encrypt_key_l, &openssl_rc4->key_l);

    mmOpensslRc4_Unlock(openssl_rc4);
    mmOpensslRsa_Unlock(&p->openssl_rsa);
}
MM_EXPORT_OPENSSL void mmOpensslServerContext_EncryptKeyR(struct mmOpensslServerContext* p, struct mmString* encrypt_key_r)
{
    // thread local cache. openssl_rsa_array is lock free.
    struct mmOpensslRsa* openssl_rsa = mmOpensslRsaArray_ThreadInstance(&p->openssl_rsa_array);
    // thread local cache. mmOpensslRc4Array is lock free.
    struct mmOpensslRc4* openssl_rc4 = mmOpensslRc4Array_ThreadInstance(&p->openssl_rc4_array);

    // encrypt rc4_key_r.
    mmOpensslRsa_Lock(openssl_rsa);
    mmOpensslRc4_Lock(openssl_rc4);

    // public encrypt rc4_key_r.
    mmOpensslRsa_PubEncrypt(openssl_rsa, &openssl_rc4->key_r, encrypt_key_r);

    mmOpensslRc4_Unlock(openssl_rc4);
    mmOpensslRsa_Unlock(openssl_rsa);
}

void mmOpensslServerContext_MailboxEventTcpProduce(void* p, struct mmTcp* tcp)
{
    struct mmMailbox* mailbox = (struct mmMailbox*)(p);
    struct mmOpensslServerContext* openssl_server_context = (struct mmOpensslServerContext*)(mailbox->event_tcp_allocator.obj);
    mmUInt64_t msec_current = (mmUInt64_t)(mmTime_CurrentUSec() / MM_MSEC_PER_SEC);

    mmUInt32_t sec_current = (mmUInt32_t)(msec_current / MM_MSEC_PER_SEC);

    struct mmOpensslTcpContext* tcp_context = NULL;

    mmTcp_SLock(tcp);
    tcp_context = (struct mmOpensslTcpContext*)mmTcp_GetContext(tcp);
    mmTcp_SUnlock(tcp);
    assert(NULL == tcp_context && "NULL == tcp_context is not invalid");

    // pool_element produce.
    mmPoolElement_Lock(&openssl_server_context->pool_element);
    tcp_context = (struct mmOpensslTcpContext*)mmPoolElement_Produce(&openssl_server_context->pool_element);
    mmPoolElement_Unlock(&openssl_server_context->pool_element);
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmOpensslTcpContext_Reset(tcp_context);

    mmOpensslTcpContext_SetTimecodeCreate(tcp_context, sec_current);

    mmTcp_SLock(tcp);
    mmTcp_SetContext(tcp, tcp_context);
    mmTcp_SUnlock(tcp);
}
void mmOpensslServerContext_MailboxEventTcpRecycle(void* p, struct mmTcp* tcp)
{
    struct mmMailbox* mailbox = (struct mmMailbox*)(p);
    struct mmOpensslServerContext* openssl_server_context = (struct mmOpensslServerContext*)(mailbox->event_tcp_allocator.obj);

    struct mmOpensslTcpContext* tcp_context = NULL;

    mmTcp_SLock(tcp);
    tcp_context = (struct mmOpensslTcpContext*)mmTcp_GetContext(tcp);
    mmTcp_SetContext(tcp, NULL);
    mmTcp_SUnlock(tcp);

    assert(NULL != tcp_context && "NULL != tcp_context is not invalid");

    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmOpensslTcpContext_Reset(tcp_context);
    // pool_element recycle.
    mmPoolElement_Lock(&openssl_server_context->pool_element);
    mmPoolElement_Recycle(&openssl_server_context->pool_element, tcp_context);
    mmPoolElement_Unlock(&openssl_server_context->pool_element);
}

