/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOpensslOTP_h__
#define __mmOpensslOTP_h__

#include "core/mmCore.h"

#include "openssl/mmOpensslExport.h"

#include "core/mmPrefix.h"

// One-time Password algorithm (OTP)

/* -------- rfc4226 -------- */
// MAIN HOTP function
MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_HOTP(mmUInt8_t* key, size_t kl, mmUInt64_t interval, int digits);
// First step
MM_EXPORT_OPENSSL mmUInt8_t* mmOpensslOTP_Hmac(unsigned char* key, int kl, mmUInt64_t interval);
// Second step
MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_DT(mmUInt8_t* digest);
// dbc
MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_DBC(unsigned char* key, int kl, mmUInt64_t interval);


/* -------- RFC6238 --------
 *
 * TOTP = HOTP(k,T) where
 * K = the supersecret key
 * T = ( Current Unix time - T0) / X
 * where X is the Time Step
 *
 * -------------------------
 */

/* time step in seconds, default value */
#define MM_TOTP_TS 30

MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_TOTP(mmUInt8_t* key, size_t kl, mmUInt64_t time, int digits);
MM_EXPORT_OPENSSL time_t mmOpensslOTP_GetTime(time_t t0, int X);

#include "core/mmSuffix.h"

#endif//__mmOpensslOTP_h__
