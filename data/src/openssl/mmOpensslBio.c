/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslBio.h"

// handle recv for buffer pool and pool max size.
MM_EXPORT_OPENSSL void mmOpensslBio_HandleRecv(BIO* p, struct mmStreambuf* streambuf)
{
    int real_len = 0;
    // mmStreambuf page size MM_STREAMBUF_PAGE_SIZE is equal MM_SOCKET_PAGE_SIZE.
    // reduce the mmStreambuf frequently change max_size, here we use 3 * MM_SOCKET_PAGE_SIZE / 4 for max_length for recv.
    size_t max_length = 3 * MM_STREAMBUF_PAGE_SIZE / 4;
    do
    {
        // if the idle put size is lack, we try remove the get buffer.
        mmStreambuf_AlignedMemory(streambuf, max_length);
        // recv.
        real_len = BIO_read(p, (void*)(streambuf->buff + streambuf->pptr), (int)max_length);
        if (-1 == real_len)
        {
            // No matter what the result, we break this while loop.
            break;
        }
        else if (0 == real_len)
        {
            // No buffer here.
            break;
        }
        // pbump the position.
        // note: the real buffer is 
        //       buffer = p->buff_recv.buff
        //       offset = (mmUInt32_t)p->buff_recv.pptr - real_len
        //       length = real_len
        mmStreambuf_Pbump(streambuf, real_len);
    } while (1);
}
// handle send for buffer pool and pool max size.
MM_EXPORT_OPENSSL int mmOpensslBio_Send(BIO* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    int real_len = 0;
    int send_len = 0;
    //
    do
    {
        if (0 == length)
        {
            // nothing for send break immediately.
            break;
        }
        real_len = BIO_write(p, (void*)(buffer + offset), (int)length);
        if (-1 == real_len)
        {
            // No matter what the result, we break this while loop.
            break;
        }
        else if (real_len == length)
        {
            // complete
            send_len += real_len;
            break;
        }
        else if (0 == real_len)
        {
            // No buffer here.
            break;
        }
        else
        {
            // need cycle send.
            offset += real_len;
            length -= real_len;
            send_len += real_len;
        }
    } while (1);
    return send_len;
}
// handle send data by flush send buffer.
MM_EXPORT_OPENSSL int mmOpensslBio_FlushSend(BIO* p, struct mmStreambuf* streambuf)
{
    int send_sz = 0;
    size_t sz = 0;
    int rt = 0;
    do
    {
        sz = mmStreambuf_Size(streambuf);
        if (0 == sz)
        {
            // streambuf size is empty, break this flush send.
            break;
        }
        // sometimes the streambuf size is so large, we need send multiple times.
        sz = sz < MM_STREAMBUF_PAGE_SIZE ? sz : MM_STREAMBUF_PAGE_SIZE;
        rt = mmOpensslBio_Send(p, streambuf->buff, (mmUInt32_t)streambuf->gptr, (mmUInt32_t)sz);
        if (0 < rt)
        {
            // 0 <  rt,means rt buffer is send,we must gbump rt size.
            // 0 == rt,means the send buffer can be full.
            // 0 >  rt,means the send process can be failure.
            mmStreambuf_Gbump(streambuf, rt);
            send_sz += rt;
        }
        else
        {
            // an error occurred during the sending process.
            break;
        }
    } while (1);
    return 0 < rt ? send_sz : rt;
}
