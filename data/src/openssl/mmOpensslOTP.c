/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOpensslOTP.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "openssl/hmac.h"
#include "openssl/evp.h"

// #define MM_DEBUG_OTP

MM_EXPORT_OPENSSL mmUInt8_t* mmOpensslOTP_Hmac(unsigned char* key, int kl, mmUInt64_t interval)
{
    const EVP_MD* evp_md = EVP_sha1();
    const unsigned char* d = (const unsigned char*)&interval;
    size_t n = sizeof(interval);
    return (mmUInt8_t*)HMAC(evp_md, key, kl, d, n, NULL, 0);
}

MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_DT(mmUInt8_t* digest)
{
    mmUInt32_t offset;
    mmUInt32_t bin_code;
    
#ifdef MM_DEBUG_OTP
    int i = 0;
    char mdString[40];
    for (i = 0; i < 20; i++)
    {
        sprintf(&mdString[i * 2], "%02x", (unsigned int)digest[i]);
    }
    printf("HMAC digest: %s\n", mdString);
#endif

    // dynamically truncates hash
    offset = digest[19] & 0x0f;

    bin_code =
        (digest[offset    ] & 0x7f) << 24 |
        (digest[offset + 1] & 0xff) << 16 |
        (digest[offset + 2] & 0xff) <<  8 |
        (digest[offset + 3] & 0xff);

    // truncates code to 6 digits
#ifdef MM_DEBUG_OTP
    printf("OFFSET: %" PRIu32 "\n", offset);
    printf("\nDBC1: %" PRIu32 "\n", bin_code);
#endif
    return bin_code;
}

MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_DBC(unsigned char* key, int kl, mmUInt64_t interval)
{
    //First Phase, get the digest of the message using the provided key ...
    mmUInt8_t* digest = (mmUInt8_t*)mmOpensslOTP_Hmac(key, (int)kl, interval);
    //digest = (uint8_t *)HMAC(EVP_sha1(), key, kl, (const unsigned char *)&interval, sizeof(interval), NULL, 0);
    //Second Phase, get the dbc from the algorithm
    return mmOpensslOTP_DT(digest);
}

static mmUInt32_t __static_mmOpensslOTP_ModHOTP(mmUInt32_t bin_code, int digits)
{
    int power = (int)pow(10, digits);
    return bin_code % power;
}

MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_HOTP(mmUInt8_t* key, size_t kl, mmUInt64_t interval, int digits)
{
    mmUInt32_t result;
    mmUInt32_t endianness;

#ifdef MM_DEBUG_OTP
    printf("KEY IS: %s\n", key);
    printf("KEY LEN IS: %" PRIuPTR "\n", kl);
    printf("COUNTER IS: %" PRIu64  "\n", interval);
#endif

    endianness = 0xdeadbeef;
    if ((*(const mmUInt8_t*)&endianness) == 0xef)
    {
        interval = ((interval & 0x00000000ffffffff) << 32) | ((interval & 0xffffffff00000000) >> 32);
        interval = ((interval & 0x0000ffff0000ffff) << 16) | ((interval & 0xffff0000ffff0000) >> 16);
        interval = ((interval & 0x00ff00ff00ff00ff) <<  8) | ((interval & 0xff00ff00ff00ff00) >>  8);
    }
    
    mmUInt32_t dbc = mmOpensslOTP_DBC(key, (int)kl, interval);
    //Third Phase: calculate the mod_k of the dbc to get the correct number
    result = __static_mmOpensslOTP_ModHOTP(dbc, digits);

    return result;
}


MM_EXPORT_OPENSSL time_t mmOpensslOTP_GetTime(time_t t0, int X)
{
    double T = (double)((time(NULL) - t0) / X);
    return (time_t)floor(T);
}

MM_EXPORT_OPENSSL mmUInt32_t mmOpensslOTP_TOTP(mmUInt8_t* key, size_t kl, mmUInt64_t time, int digits)
{
    return mmOpensslOTP_HOTP(key, kl, time, digits);;
}
