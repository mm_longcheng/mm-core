/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOpensslTcpContext_h__
#define __mmOpensslTcpContext_h__

#include "core/mmCore.h"
#include "core/mmAtomic.h"

#include "openssl/rc4.h"

#include "openssl/mmOpensslExport.h"

#include "core/mmPrefix.h"

enum mmCryptoContextState_t
{
    MM_CRYPTO_CONTEXT_INACTIVE = 0,
    MM_CRYPTO_CONTEXT_ACTIVATE = 1,
};

struct mmOpensslTcpContext
{
    // crypto context.
    RC4_KEY context_send;
    RC4_KEY context_recv;
    //
    mmAtomic_t locker;
    // seconds, timecode for context create.
    mmUInt32_t timecode_create;
    // seconds, timecode for context update.
    mmUInt32_t timecode_update;

    // default is MM_CRYPTO_CONTEXT_INACTIVE.
    mmUInt8_t state;
};
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Init(struct mmOpensslTcpContext* p);
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Destroy(struct mmOpensslTcpContext* p);

MM_EXPORT_OPENSSL void mmOpensslTcpContext_Reset(struct mmOpensslTcpContext* p);

MM_EXPORT_OPENSSL void mmOpensslTcpContext_Lock(struct mmOpensslTcpContext* p);
MM_EXPORT_OPENSSL void mmOpensslTcpContext_Unlock(struct mmOpensslTcpContext* p);

MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetKey(struct mmOpensslTcpContext* p, const mmUInt8_t* buffer, size_t offset, size_t length);
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetState(struct mmOpensslTcpContext* p, mmUInt8_t state);
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetTimecodeCreate(struct mmOpensslTcpContext* p, mmUInt32_t timecode_create);
MM_EXPORT_OPENSSL void mmOpensslTcpContext_SetTimecodeUpdate(struct mmOpensslTcpContext* p, mmUInt32_t timecode_update);

MM_EXPORT_OPENSSL void mmOpensslTcpContext_MailboxCryptoEncrypt(void* p, void* e, mmUInt8_t* buffer_i, size_t offset_i, mmUInt8_t* buffer_o, size_t offset_o, size_t length);
MM_EXPORT_OPENSSL void mmOpensslTcpContext_MailboxCryptoDecrypt(void* p, void* e, mmUInt8_t* buffer_i, size_t offset_i, mmUInt8_t* buffer_o, size_t offset_o, size_t length);

#include "core/mmSuffix.h"

#endif//__mmOpensslTcpContext_h__
