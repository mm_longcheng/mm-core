/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmProtobuffCxxRq.h"

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqClientUdpFlushUnicastRq(struct mmRqUdpClient* rq_udp_client, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack)
{
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmPacketHead packet_head;
    packet_head.mid = mid;
    packet_head.pid = 0;
    packet_head.sid = 0;
    packet_head.uid = uid;
    return mmRqUdpClient_SendMessageUnicast(rq_udp_client, coder, &packet_head, rq_msg, hlength, rq_pack);
}
// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqClientUdpFlushMulcastRq(struct mmRqUdpClient* rq_udp_client, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack)
{
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmPacketHead packet_head;
    packet_head.mid = mid;
    packet_head.pid = 0;
    packet_head.sid = 0;
    packet_head.uid = uid;
    return mmRqUdpClient_SendMessageMulcast(rq_udp_client, coder, &packet_head, rq_msg, hlength, rq_pack);
}

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqServerUdpFlushUnicastNt(struct mmRqUdpServer* rq_udp_server, mmUInt32_t identity, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* nt_msg, size_t hlength, struct mmPacket* nt_pack)
{
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmUdp* udp = &rq_udp_server->headset.udp;
    struct mmPacketHead packet_head;
    packet_head.mid = mid;
    packet_head.pid = 0;
    packet_head.sid = 0;
    packet_head.uid = uid;
    return mmRqUdpServer_SendMessageUnicast(rq_udp_server, udp, identity, coder, &packet_head, nt_msg, hlength, nt_pack);
}
// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqServerUdpFlushUnicastRs(struct mmRqUdpServer* rq_udp_server, struct mmUdp* udp, mmUInt32_t identity, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, struct mmPacket* rq_pack, struct mmPacket* rs_pack)
{
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmPacketHead packet_head;
    packet_head = rq_pack->phead;
    packet_head.mid = mid;
    return mmRqUdpServer_SendMessageUnicast(rq_udp_server, udp, identity, coder, &packet_head, rq_msg, rq_pack->hbuff.length, rs_pack);
}
// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqServerUdpFlushMulcastNt(struct mmRqUdpServer* rq_udp_server, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* nt_msg, size_t hlength, struct mmPacket* nt_pack)
{
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmUdp* udp = &rq_udp_server->headset.udp;
    struct mmPacketHead packet_head;
    packet_head.mid = mid;
    packet_head.pid = 0;
    packet_head.sid = 0;
    packet_head.uid = uid;
    return mmRqUdpServer_SendMessageMulcast(rq_udp_server, udp, coder, &packet_head, nt_msg, hlength, nt_pack);
}
