/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmProtobuffCxx_h__
#define __mmProtobuffCxx_h__

#include "core/mmCore.h"

#include "net/mmMessageCoder.h"

#include "protobuf/mmProtobuffExport.h"

#include "core/mmPrefix.h"

// max length for cut out.
#define MM_MAX_MESSAGE_LOGGER_LENGTH 1024
// min lenght for not return.
#define MM_MIN_MESSAGE_LOGGER_LENGTH 64

namespace google
{
    namespace protobuf
    {
        class Message;
    };
};


MM_EXPORT_PROTOBUFF void mmProtobufCxx_Init();
MM_EXPORT_PROTOBUFF void mmProtobufCxx_Destroy();

MM_EXPORT_PROTOBUFF void mmProtobufCxx_LoggerAppendPacket(struct mmString* value, struct mmPacket* pack);

MM_EXPORT_PROTOBUFF void mmProtobufCxx_LoggerAppendMessage(struct mmString* value, ::google::protobuf::Message* message);

MM_EXPORT_PROTOBUFF void mmProtobufCxx_LoggerAppendPacketMessage(struct mmString* value, struct mmPacket* pack, ::google::protobuf::Message* message);

// decode message mmPacket --> message.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_DecodeMessage(struct mmPacket* pack, ::google::protobuf::Message* message);
// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_EncodeMessage(struct mmPacket* pack, ::google::protobuf::Message* message);
// decode message mmPacket --> message.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_DecodeMessageByteBuffer(struct mmByteBuffer* byte_buffer, ::google::protobuf::Message* message);
// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_EncodeMessageByteBuffer(struct mmByteBuffer* byte_buffer, ::google::protobuf::Message* message);

// message coder for protobuf.
MM_EXPORT_PROTOBUFF struct mmMessageCoder* mmMessageCoder_ProtobufCxx();

// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_EncodeMessageProtobuf(struct mmStreambuf* streambuf, ::google::protobuf::Message* message, struct mmPacketHead* packet_head, size_t hlength, struct mmPacket* pack);

#include "core/mmSuffix.h"

#endif//__mmProtobuffCxx_h__
