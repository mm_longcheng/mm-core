/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmProtobuffCxxRq_h__
#define __mmProtobuffCxxRq_h__

#include "core/mmCore.h"

#include "rq/mmRqUdpServer.h"
#include "rq/mmRqUdpClient.h"

#include "protobuf/mmProtobuffCxx.h"
#include "protobuf/mmProtobuffExport.h"

#include "core/mmPrefix.h"

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqClientUdpFlushUnicastRq(struct mmRqUdpClient* rq_udp_client, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack);
// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqClientUdpFlushMulcastRq(struct mmRqUdpClient* rq_udp_client, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack);

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqServerUdpFlushUnicastNt(struct mmRqUdpServer* rq_udp_server, mmUInt32_t identity, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* nt_msg, size_t hlength, struct mmPacket* nt_pack);
// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqServerUdpFlushUnicastRs(struct mmRqUdpServer* rq_udp_server, struct mmUdp* udp, mmUInt32_t identity, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, struct mmPacket* rq_pack, struct mmPacket* rs_pack);
// this api will lock udp o locker, do not lock outside.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NRqServerUdpFlushMulcastNt(struct mmRqUdpServer* rq_udp_server, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* nt_msg, size_t hlength, struct mmPacket* nt_pack);

#include "core/mmSuffix.h"

#endif//__mmProtobuffCxxRq_h__
