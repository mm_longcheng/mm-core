/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmProtobuffCxxNet.h"
#include "core/mmString.h"
#include "core/mmLogger.h"
#include "net/mmStreambufPacket.h"

MM_EXPORT_PROTOBUFF int mmProtobufCxx_NTcpAppendRs(struct mmMailbox* mailbox, struct mmTcp* tcp, mmUInt32_t mid, ::google::protobuf::Message* rs_msg, struct mmPacket* rq_pack, struct mmPacket* rs_pack)
{
    int rt = -1;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmStreambuf* streambuf = &tcp->buff_send;
    rt = mmMessageCoder_AppendHeadCopy(&tcp->buff_send, coder, mid, rs_msg, rq_pack, rs_pack);
    mmMailbox_CryptoEncrypt(mailbox, tcp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpAppendRs(struct mmHeadset* headset, struct mmUdp* udp, mmUInt32_t mid, ::google::protobuf::Message* rs_msg, struct mmPacket* rq_pack, struct mmPacket* rs_pack)
{
    int rt = -1;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmStreambuf* streambuf = &udp->buff_send;
    rt = mmMessageCoder_AppendHeadCopy(&udp->buff_send, coder, mid, rs_msg, rq_pack, rs_pack);
    mmHeadset_CryptoEncrypt(headset, udp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QTcpAppendRq(struct mmNetTcp* net_tcp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack)
{
    int rt = -1;
    mmTcp* tcp = &net_tcp->tcp;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmStreambuf* streambuf = &tcp->buff_send;
    rt = mmMessageCoder_AppendHeadZero(&tcp->buff_send, coder, uid, mid, rq_msg, hlength, rq_pack);
    mmNetTcp_CryptoEncrypt(net_tcp, tcp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QUdpAppendRq(struct mmNetUdp* net_udp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack)
{
    int rt = -1;
    mmUdp* udp = &net_udp->udp;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmStreambuf* streambuf = &udp->buff_send;
    rt = mmMessageCoder_AppendHeadZero(&udp->buff_send, coder, uid, mid, rq_msg, hlength, rq_pack);
    mmNetUdp_CryptoEncrypt(net_udp, udp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NTcpAppendNt(struct mmMailbox* mailbox, struct mmTcp* tcp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* nt_msg, size_t hlength, struct mmPacket* nt_pack)
{
    int rt = -1;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    struct mmStreambuf* streambuf = &tcp->buff_send;
    rt = mmMessageCoder_AppendHeadZero(&tcp->buff_send, coder, uid, mid, nt_msg, hlength, nt_pack);
    mmMailbox_CryptoEncrypt(mailbox, tcp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
// proxy append
// net_tcp t_tcp is target.rq_pack rq pack pr_pack proxy pack.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_PTcpAppendRq(struct mmNetTcp* net_tcp, struct mmTcp* t_tcp, mmUInt32_t pid, mmUInt64_t sid, struct mmPacket* rq_pack, struct mmPacket* pr_pack)
{
    int rt = -1;
    struct mmStreambuf* streambuf = &t_tcp->buff_send;
    rt = mmMessageCoder_AppendHeadCopyProxyRq(&t_tcp->buff_send, pid, sid, rq_pack, pr_pack);
    mmNetTcp_CryptoEncrypt(net_tcp, t_tcp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
// proxy append
// mailbox t_tcp is target.rs_pack rs pack pr_pack proxy pack.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_PTcpAppendRs(struct mmMailbox* mailbox, struct mmTcp* t_tcp, struct mmPacket* rs_pack, struct mmPacket* pr_pack)
{
    int rt = -1;
    struct mmStreambuf* streambuf = &t_tcp->buff_send;
    rt = mmMessageCoder_AppendHeadCopyProxyRs(&t_tcp->buff_send, rs_pack, pr_pack);
    mmMailbox_CryptoEncrypt(mailbox, t_tcp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NTcpFlushSend(struct mmTcp* tcp)
{
    int rt = -1;
    rt = mmTcp_FlushSend(tcp);
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpFlushSend(struct mmUdp* udp)
{
    int rt = -1;
    rt = mmUdp_FlushSend(udp, &udp->socket.ss_remote);
    return rt;
}
// lock it outside manual.use for client.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NNetTcpFlushSend(struct mmNetTcp* net_tcp)
{
    int rt = -1;
    rt = mmNetTcp_FlushSend(net_tcp);
    return rt;
}
// lock it outside manual.use for client.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NNetUdpFlushSend(struct mmNetUdp* net_udp)
{
    int rt = -1;
    rt = mmNetUdp_FlushSend(net_udp, &net_udp->udp.socket.ss_remote);
    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpBufferSendReset(struct mmUdp* udp)
{
    mmStreambuf_Reset(&udp->buff_send);
    return 0;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpBufferSendFlush(struct mmUdp* udp, struct mmSockaddr* remote)
{
    int rt = -1;
    rt = mmUdp_FlushSend(udp, remote);
    return rt;
}
// thread safe encode and append message.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientTcpFlushMessageAppend(struct mmClientTcp* client_tcp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack)
{
    int rt = -1;

    struct mmPoolPacketElem* e = NULL;
    mmNetTcp* net_tcp = &client_tcp->net_tcp;
    mmTcp* tcp = &net_tcp->tcp;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    size_t message_length = (*(coder->Length))(rq_msg);

    e = mmPacketQueue_Overdraft(&client_tcp->queue_send, hlength, message_length);

    mmPoolPacketElem_SetRemote(e, &tcp->socket.ss_remote);
    mmPoolPacketElem_SetObject(e, tcp);

    rq_pack->hbuff = e->packet.hbuff;
    rq_pack->bbuff = e->packet.bbuff;

    rt = mmMessageCoder_AppendImplHeadZero(e->streambuf, coder, uid, mid, rq_msg, hlength, rq_pack);

    // note:
    // queue data not need encrypt, we encrypt when need send.

    e->packet.phead = rq_pack->phead;

    mmPacketQueue_Repayment(&client_tcp->queue_send, e);

    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientUdpFlushMessageAppend(struct mmClientUdp* client_udp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack, struct mmSockaddr* remote)
{
    int rt = -1;

    struct mmPoolPacketElem* e = NULL;
    mmNetUdp* net_udp = &client_udp->net_udp;
    mmUdp* udp = &net_udp->udp;
    struct mmMessageCoder* coder = mmMessageCoder_ProtobufCxx();
    size_t message_length = (*(coder->Length))(rq_msg);

    e = mmPacketQueue_Overdraft(&client_udp->queue_send, hlength, message_length);

    mmPoolPacketElem_SetRemote(e, remote);
    mmPoolPacketElem_SetObject(e, udp);

    rq_pack->hbuff = e->packet.hbuff;
    rq_pack->bbuff = e->packet.bbuff;

    rt = mmMessageCoder_AppendImplHeadZero(e->streambuf, coder, uid, mid, rq_msg, hlength, rq_pack);

    // note:
    // queue data not need encrypt, we encrypt when need send.

    e->packet.phead = rq_pack->phead;

    mmPacketQueue_Repayment(&client_udp->queue_send, e);

    return rt;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientTcpFlushSignal(struct mmClientTcp* client_tcp)
{
    mmClientTcp_FlushSignal(client_tcp);
    return 0;
}
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientUdpFlushSignal(struct mmClientUdp* client_udp)
{
    mmClientUdp_FlushSignal(client_udp);
    return 0;
}
