/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmProtobuffCString_h__
#define __mmProtobuffCString_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include <protobuf-c/protobuf-c.h>

#include "protobuf/mmProtobuffExport.h"

#include "core/mmPrefix.h"

struct ProtobufCMessageUnknownField;
struct ProtobufCMessageDescriptor;
struct ProtobufCFieldDescriptor;
struct ProtobufCMessage;

/* === get_packed_size() === */

/**
 * Return the number of bytes required to store the tag for the field. Includes
 * 3 bits for the wire-type, and a single bit that denotes the end-of-tag.
 *
 * \param number
 *      Field tag to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_GetTagSize(
    unsigned                                       number);

/**
 * Return the number of bytes required to store a variable-length unsigned
 * 32-bit integer in base-128 varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_UInt32Size(
    unsigned int                                   v);

/**
 * Return the number of bytes required to store a variable-length signed 32-bit
 * integer in base-128 varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_Int32Size(
    int                                            v);

/**
 * Return the ZigZag-encoded 32-bit unsigned integer form of a 32-bit signed
 * integer.
 *
 * \param v
 *      Value to encode.
 * \return
 *      ZigZag encoded integer.
 */
MM_EXPORT_PROTOBUFF
unsigned int
mmProtobufC_Zigzag32(
    int                                            v);

/**
 * Return the number of bytes required to store a signed 32-bit integer,
 * converted to an unsigned 32-bit integer with ZigZag encoding, using base-128
 * varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_Sint32Size(
    int                                            v);

/**
 * Return the number of bytes required to store a 64-bit unsigned integer in
 * base-128 varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_UInt64Size(
    unsigned long long int                         v);

/**
 * Return the ZigZag-encoded 64-bit unsigned integer form of a 64-bit signed
 * integer.
 *
 * \param v
 *      Value to encode.
 * \return
 *      ZigZag encoded integer.
 */
MM_EXPORT_PROTOBUFF
unsigned long long int
mmProtobufC_Zigzag64(
    long long int                                  v);

/**
 * Return the number of bytes required to store a signed 64-bit integer,
 * converted to an unsigned 64-bit integer with ZigZag encoding, using base-128
 * varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_Sint64Size(
    long long int                                  v);

/* dump the message. */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_MessageToString(
    const struct ProtobufCMessage*                 message,
    struct mmString*                               desc);

#include "core/mmSuffix.h"

#endif//__mmProtobuffCString_h__
