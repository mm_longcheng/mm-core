/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmProtobuffCString.h"

#include <protobuf-c/protobuf-c.h>

#define PROTOBUF_C__ASSERT_NOT_REACHED() assert(0)

/* Assertions for magic numbers. */

#define ASSERT_IS_ENUM_DESCRIPTOR(desc) \
    assert((desc)->magic == PROTOBUF_C__ENUM_DESCRIPTOR_MAGIC)

#define ASSERT_IS_MESSAGE_DESCRIPTOR(desc) \
    assert((desc)->magic == PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC)

#define ASSERT_IS_MESSAGE(message) \
    ASSERT_IS_MESSAGE_DESCRIPTOR((message)->descriptor)

#define ASSERT_IS_SERVICE_DESCRIPTOR(desc) \
    assert((desc)->magic == PROTOBUF_C__SERVICE_DESCRIPTOR_MAGIC)

/* === get_packed_size() === */

/**
 * Return the number of bytes required to store the tag for the field. Includes
 * 3 bits for the wire-type, and a single bit that denotes the end-of-tag.
 *
 * \param number
 *      Field tag to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_GetTagSize(
    unsigned                                       number)
{
    if (number < (1UL << 4)) {
        return 1;
    } else if (number < (1UL << 11)) {
        return 2;
    } else if (number < (1UL << 18)) {
        return 3;
    } else if (number < (1UL << 25)) {
        return 4;
    } else {
        return 5;
    }
}

/**
 * Return the number of bytes required to store a variable-length unsigned
 * 32-bit integer in base-128 varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_UInt32Size(
    unsigned int                                   v)
{
    if (v < (1UL << 7)) {
        return 1;
    } else if (v < (1UL << 14)) {
        return 2;
    } else if (v < (1UL << 21)) {
        return 3;
    } else if (v < (1UL << 28)) {
        return 4;
    } else {
        return 5;
    }
}

/**
 * Return the number of bytes required to store a variable-length signed 32-bit
 * integer in base-128 varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_Int32Size(
    int                                            v)
{
    if (v < 0) {
        return 10;
    } else if (v < (1L << 7)) {
        return 1;
    } else if (v < (1L << 14)) {
        return 2;
    } else if (v < (1L << 21)) {
        return 3;
    } else if (v < (1L << 28)) {
        return 4;
    } else {
        return 5;
    }
}

/**
 * Return the ZigZag-encoded 32-bit unsigned integer form of a 32-bit signed
 * integer.
 *
 * \param v
 *      Value to encode.
 * \return
 *      ZigZag encoded integer.
 */
MM_EXPORT_PROTOBUFF
unsigned int
mmProtobufC_Zigzag32(
    int                                            v)
{
    // Note:  the right-shift must be arithmetic
    // Note:  left shift must be unsigned because of overflow
    return ((uint32_t)(v) << 1) ^ (uint32_t)(v >> 31);
}

/**
 * Return the number of bytes required to store a signed 32-bit integer,
 * converted to an unsigned 32-bit integer with ZigZag encoding, using base-128
 * varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_Sint32Size(
    int                                            v)
{
    return mmProtobufC_UInt32Size(mmProtobufC_Zigzag32(v));
}

/**
 * Return the number of bytes required to store a 64-bit unsigned integer in
 * base-128 varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_UInt64Size(
    unsigned long long int                         v)
{
    uint32_t upper_v = (uint32_t) (v >> 32);

    if (upper_v == 0) {
        return mmProtobufC_UInt32Size((uint32_t) v);
    } else if (upper_v < (1UL << 3)) {
        return 5;
    } else if (upper_v < (1UL << 10)) {
        return 6;
    } else if (upper_v < (1UL << 17)) {
        return 7;
    } else if (upper_v < (1UL << 24)) {
        return 8;
    } else if (upper_v < (1UL << 31)) {
        return 9;
    } else {
        return 10;
    }
}

/**
 * Return the ZigZag-encoded 64-bit unsigned integer form of a 64-bit signed
 * integer.
 *
 * \param v
 *      Value to encode.
 * \return
 *      ZigZag encoded integer.
 */
MM_EXPORT_PROTOBUFF
unsigned long long int
mmProtobufC_Zigzag64(
    long long int                                  v)
{
    // Note:  the right-shift must be arithmetic
    // Note:  left shift must be unsigned because of overflow
    return ((uint64_t)(v) << 1) ^ (uint64_t)(v >> 63);
}

/**
 * Return the number of bytes required to store a signed 64-bit integer,
 * converted to an unsigned 64-bit integer with ZigZag encoding, using base-128
 * varint encoding.
 *
 * \param v
 *      Value to encode.
 * \return
 *      Number of bytes required.
 */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_Sint64Size(
    long long int                                  v)
{
    return mmProtobufC_UInt64Size(mmProtobufC_Zigzag64(v));
}

static
protobuf_c_boolean
mmProtobufC_FieldIsZeroish(
    const ProtobufCFieldDescriptor*                field,
    const void*                                    member)
{
    protobuf_c_boolean ret = FALSE;

    switch (field->type) {
    case PROTOBUF_C_TYPE_BOOL:
        ret = (0 == *(const protobuf_c_boolean *) member);
        break;
    case PROTOBUF_C_TYPE_ENUM:
    case PROTOBUF_C_TYPE_SINT32:
    case PROTOBUF_C_TYPE_INT32:
    case PROTOBUF_C_TYPE_UINT32:
    case PROTOBUF_C_TYPE_SFIXED32:
    case PROTOBUF_C_TYPE_FIXED32:
        ret = (0 == *(const uint32_t *) member);
        break;
    case PROTOBUF_C_TYPE_SINT64:
    case PROTOBUF_C_TYPE_INT64:
    case PROTOBUF_C_TYPE_UINT64:
    case PROTOBUF_C_TYPE_SFIXED64:
    case PROTOBUF_C_TYPE_FIXED64:
        ret = (0 == *(const uint64_t *) member);
        break;
    case PROTOBUF_C_TYPE_FLOAT:
        ret = (0 == *(const float *) member);
        break;
    case PROTOBUF_C_TYPE_DOUBLE:
        ret = (0 == *(const double *) member);
        break;
    case PROTOBUF_C_TYPE_STRING:
        ret = (NULL == *(const char * const *) member) ||
              ('\0' == **(const char * const *) member);
        break;
    case PROTOBUF_C_TYPE_BYTES:
    case PROTOBUF_C_TYPE_MESSAGE:
        ret = (NULL == *(const void * const *) member);
        break;
    default:
        ret = TRUE;
        break;
    }

    return ret;
}

/**
 * Given a field type, return the in-memory size.
 *
 * \todo Implement as a table lookup.
 *
 * \param type
 *      Field type.
 * \return
 *      Size of the field.
 */
static inline
size_t
mmProtobufC_SizeofEltInRepeatedArray(
    ProtobufCType                                  type)
{
    switch (type) {
    case PROTOBUF_C_TYPE_SINT32:
    case PROTOBUF_C_TYPE_INT32:
    case PROTOBUF_C_TYPE_UINT32:
    case PROTOBUF_C_TYPE_SFIXED32:
    case PROTOBUF_C_TYPE_FIXED32:
    case PROTOBUF_C_TYPE_FLOAT:
    case PROTOBUF_C_TYPE_ENUM:
        return 4;
    case PROTOBUF_C_TYPE_SINT64:
    case PROTOBUF_C_TYPE_INT64:
    case PROTOBUF_C_TYPE_UINT64:
    case PROTOBUF_C_TYPE_SFIXED64:
    case PROTOBUF_C_TYPE_FIXED64:
    case PROTOBUF_C_TYPE_DOUBLE:
        return 8;
    case PROTOBUF_C_TYPE_BOOL:
        return sizeof(protobuf_c_boolean);
    case PROTOBUF_C_TYPE_STRING:
    case PROTOBUF_C_TYPE_MESSAGE:
        return sizeof(void *);
    case PROTOBUF_C_TYPE_BYTES:
        return sizeof(ProtobufCBinaryData);
    }
    
    PROTOBUF_C__ASSERT_NOT_REACHED();
    return 0;
}

/**
 * Get the minimum number of bytes required to pack a field value of a
 * particular type.
 *
 * \param type
 *      Field type.
 * \return
 *      Number of bytes.
 */
static
unsigned
mmProtobufC_GetTypeMinSize(
    ProtobufCType                                  type)
{
    if (type == PROTOBUF_C_TYPE_SFIXED32 ||
        type == PROTOBUF_C_TYPE_FIXED32 ||
        type == PROTOBUF_C_TYPE_FLOAT)
    {
        return 4;
    }
    if (type == PROTOBUF_C_TYPE_SFIXED64 ||
        type == PROTOBUF_C_TYPE_FIXED64 ||
        type == PROTOBUF_C_TYPE_DOUBLE)
    {
        return 8;
    }
    return 1;
}

/**
 * Pack a required field and return the number of bytes written.
 *
 * \param field
 *      Field descriptor.
 * \param member
 *      The field member.
 * \param[out] desc
 *      Packed value.
 * \return
 *      Number of bytes written to `out`.
 */
static
size_t
mmProtobufC_RequiredFieldToString(
    const struct ProtobufCFieldDescriptor*         field,
    const void*                                    member,
    struct mmString*                               desc)
{
    size_t rv = mmProtobufC_GetTagSize(field->id);

    switch (field->type) {
    case PROTOBUF_C_TYPE_SINT32:
        mmString_Sprintf(desc, "=%d,", *(const int *)member);
        return rv + mmProtobufC_Sint32Size(*(const int32_t *)member);
    case PROTOBUF_C_TYPE_ENUM:
    case PROTOBUF_C_TYPE_INT32:
        mmString_Sprintf(desc, "=%u,", *(const unsigned int *)member);
        return rv + mmProtobufC_Int32Size(*(const int32_t *)member);
    case PROTOBUF_C_TYPE_UINT32:
        mmString_Sprintf(desc, "=%u,", *(const unsigned int *)member);
        return rv + mmProtobufC_UInt32Size(*(const uint32_t *)member);
    case PROTOBUF_C_TYPE_SINT64:
        mmString_Sprintf(desc, "=%lld,", *(const long long int *)member);
        return rv + mmProtobufC_Sint64Size(*(const int64_t *)member);
    case PROTOBUF_C_TYPE_INT64:
    case PROTOBUF_C_TYPE_UINT64:
        mmString_Sprintf(desc, "=%llu,", *(const unsigned long long int *)member);
        return rv + mmProtobufC_UInt64Size(*(const uint64_t *)member);
    case PROTOBUF_C_TYPE_SFIXED32:
    case PROTOBUF_C_TYPE_FIXED32:
    case PROTOBUF_C_TYPE_FLOAT:
        mmString_Sprintf(desc, "=%d,", *(const int *)member);
        return rv + 4;
    case PROTOBUF_C_TYPE_SFIXED64:
    case PROTOBUF_C_TYPE_FIXED64:
    case PROTOBUF_C_TYPE_DOUBLE:
        mmString_Sprintf(desc, "=%lf,", *(double *)member);
        return rv + 8;
    case PROTOBUF_C_TYPE_BOOL:
        mmString_Sprintf(desc, "=%s,", 0 == (*(const int *)member) ? "true" : "false");
        return rv + 1;
    case PROTOBUF_C_TYPE_STRING:
        {
            const char *str = *(char * const *)member;
            size_t len = str ? strlen (str) : 0;
            mmString_Sprintf(desc, "=\"%s\",", *(char * const *)member);
            return rv + mmProtobufC_UInt32Size ((unsigned int)len) + len;
        }
    case PROTOBUF_C_TYPE_BYTES:
        {
            size_t len = ((const ProtobufCBinaryData*)member)->len;
            mmString_Sprintf(desc, "=(%u),", (unsigned int)len);
            return rv + mmProtobufC_UInt32Size ((unsigned int)len) + len;
        }
    case PROTOBUF_C_TYPE_MESSAGE:
        {
            const ProtobufCMessage *msg = * (ProtobufCMessage * const *)member;
            size_t subrv = 0;
            mmString_Sprintf(desc, "=");
            subrv = msg ? mmProtobufC_MessageToString(msg,desc) : 0;
            return rv + mmProtobufC_UInt32Size ((unsigned int)subrv) + subrv;
        }
    }
    
    PROTOBUF_C__ASSERT_NOT_REACHED();
    return 0;
}

/**
 * Pack a oneof field and return the number of bytes written. Only packs the
 * field that is selected by the case enum.
 *
 * \param field
 *      Field descriptor.
 * \param oneof_case
 *      Enum value that selects the field in the oneof.
 * \param member
 *      The field member.
 * \param[out] desc
 *      Packed value.
 * \return
 *      Number of bytes written to `out`.
 */
static
size_t
mmProtobufC_OneofFieldToString(
    const ProtobufCFieldDescriptor*                field,
    uint32_t                                       oneof_case,
    const void*                                    member,
    struct mmString*                               desc)
{
    if (oneof_case != field->id) {
        return 0;
    }
    if (field->type == PROTOBUF_C_TYPE_MESSAGE ||
        field->type == PROTOBUF_C_TYPE_STRING)
    {
        const void *ptr = *(const void * const *) member;
        if (ptr == NULL || ptr == field->default_value)
            return 0;
    }
    return mmProtobufC_RequiredFieldToString(field, member, desc);
}

/**
 * Pack an optional field and return the number of bytes written.
 *
 * \param field
 *      Field descriptor.
 * \param has
 *      Whether the field is set.
 * \param member
 *      The field member.
 * \param[out] desc
 *      Packed value.
 * \return
 *      Number of bytes written to `out`.
 */
static
size_t
mmProtobufC_OptionalFieldToString(
    const struct ProtobufCFieldDescriptor*         field,
    const protobuf_c_boolean                       has,
    const void*                                    member,
    struct mmString*                               desc)
{
    if (field->type == PROTOBUF_C_TYPE_MESSAGE ||
        field->type == PROTOBUF_C_TYPE_STRING)
    {
        const void *ptr = *(const void * const *) member;
        if (ptr == NULL || ptr == field->default_value)
            return 0;
    } else {
        if (!has)
            return 0;
    }
    return mmProtobufC_RequiredFieldToString(field, member, desc);
}

/**
 * Pack an unlabeled field and return the number of bytes written.
 *
 * \param field
 *      Field descriptor.
 * \param member
 *      The field member.
 * \param[out] desc
 *      Packed value.
 * \return
 *      Number of bytes written to `out`.
 */
static size_t
mmProtobufC_UnlabeledFieldToString(
    const ProtobufCFieldDescriptor*                field,
    const void*                                    member,
    struct mmString*                               desc)
{
    if (mmProtobufC_FieldIsZeroish(field, member))
        return 0;
    return mmProtobufC_RequiredFieldToString(field, member, desc);
}

/**
 * Packs the elements of a repeated field and returns the serialised field and
 * its length.
 *
 * \param field
 *      Field descriptor.
 * \param count
 *      Number of elements in the repeated field array.
 * \param member
 *      Pointer to the elements for this repeated field.
 * \param[out] desc
 *      Serialised representation of the repeated field.
 * \return
 *      Number of bytes serialised to `out`.
 */
static
size_t
mmProtobufC_RepeatedFieldToString(
    const struct ProtobufCFieldDescriptor*         field,
    size_t                                         count,
    const void*                                    member,
    struct mmString*                               desc)
{
    void *array = *(void * const *) member;
    unsigned i;

    mmString_Sprintf(desc, "%s",field->name);
    
    if (0 != (field->flags & PROTOBUF_C_FIELD_FLAG_PACKED)) {
        size_t header_len;
        size_t len_start;
        size_t min_length;
        size_t payload_len;
        size_t length_size_min;
        size_t actual_length_size;

        if (count == 0)
        {
            mmString_Sprintf(desc, "={},");
            return 0;
        }
        header_len = mmProtobufC_GetTagSize(field->id);
        len_start = header_len;
        min_length = mmProtobufC_GetTypeMinSize(field->type) * count;
        length_size_min = mmProtobufC_UInt32Size((unsigned int)min_length);
        header_len += length_size_min;
        payload_len = 0;

        switch (field->type) {
        case PROTOBUF_C_TYPE_SFIXED32:
        case PROTOBUF_C_TYPE_FIXED32:
        case PROTOBUF_C_TYPE_FLOAT:
            {
                const float* arr = (const float*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%f,", arr[i]);
                }
                mmString_Sprintf(desc, "},");
                payload_len += 4 * count;
            }
            break;
        case PROTOBUF_C_TYPE_SFIXED64:
        case PROTOBUF_C_TYPE_FIXED64:
        case PROTOBUF_C_TYPE_DOUBLE:
            {
                const double* arr = (const double*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%lf,", arr[i]);
                }
                mmString_Sprintf(desc, "},");
                payload_len += 8 * count;
            }
            break;
        case PROTOBUF_C_TYPE_ENUM:
        case PROTOBUF_C_TYPE_INT32:
            {
                const unsigned int* arr = (const unsigned int*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%u,", arr[i]);
                    payload_len += mmProtobufC_UInt32Size (arr[i]);
                }
                mmString_Sprintf(desc, "},");
                break;
            }
        case PROTOBUF_C_TYPE_SINT32:
            {
                const int* arr = (const int*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%d,", arr[i]);
                    payload_len += mmProtobufC_Sint32Size (arr[i]);
                }
                mmString_Sprintf(desc, "},");
                break;
            }
        case PROTOBUF_C_TYPE_SINT64:
            {
                const long long int* arr = (const long long int*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%lld,", arr[i]);
                    payload_len += mmProtobufC_Sint64Size (arr[i]);
                }
                mmString_Sprintf(desc, "},");
                break;
            }
        case PROTOBUF_C_TYPE_UINT32:
            {
                const unsigned int* arr = (const unsigned int*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%u,", arr[i]);
                    payload_len += mmProtobufC_UInt32Size (arr[i]);
                }
                mmString_Sprintf(desc, "},");
                break;
            }
        case PROTOBUF_C_TYPE_INT64:
        case PROTOBUF_C_TYPE_UINT64:
            {
                const unsigned long long int* arr = (const unsigned long long int*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%llu,", arr[i]);
                    payload_len += mmProtobufC_UInt64Size (arr[i]);
                }
                mmString_Sprintf(desc, "},");
                break;
            }
        case PROTOBUF_C_TYPE_BOOL:
            {
                const protobuf_c_boolean* arr = (const protobuf_c_boolean*)array;
                mmString_Sprintf(desc, "={");
                for (i = 0; i < count; i++)
                {
                    mmString_Sprintf(desc, "%s,",0 == arr[i] ? "true":"false");
                }
                payload_len += count;
                mmString_Sprintf(desc, "},");
                break;
            }
        default:
            PROTOBUF_C__ASSERT_NOT_REACHED();
        }

        // payload_len = payload_at - (out + header_len);
        actual_length_size = mmProtobufC_UInt32Size((unsigned int)payload_len);
        if (length_size_min != actual_length_size) {
            assert(actual_length_size == length_size_min + 1);
            header_len++;
        }
        return header_len + payload_len;
    } else {
        /* not "packed" cased */
        /* CONSIDER: optimize this case a bit (by putting the loop inside the switch) */
        size_t rv = 0;
        size_t siz = mmProtobufC_SizeofEltInRepeatedArray(field->type);

        for (i = 0; i < count; i++) {
            rv += mmProtobufC_RequiredFieldToString(field, array, desc);
            array = (char *)array + siz;
        }
        return rv;
    }
}

static
size_t
mmProtobufC_UnknownFieldToString(
    const struct ProtobufCMessageUnknownField*     field,
    struct mmString*                               desc)
{
    mmString_Sprintf(desc, "%u=%u,", field->tag, (unsigned int)field->len);
    return mmProtobufC_GetTagSize (field->tag) + field->len;
}

/* dump the message. */
MM_EXPORT_PROTOBUFF
size_t
mmProtobufC_MessageToString(
    const struct ProtobufCMessage*                 message,
    struct mmString*                               desc)
{
    unsigned i;
    size_t rv = 0;

    ASSERT_IS_MESSAGE(message);
    for (i = 0; i < message->descriptor->n_fields; i++) {
        const ProtobufCFieldDescriptor *field =
            message->descriptor->fields + i;
        const void *member = ((const char *) message) + field->offset;

        /*
         * It doesn't hurt to compute qmember (a pointer to the
         * quantifier field of the structure), but the pointer is only
         * valid if the field is:
         *  - a repeated field, or
         *  - a field that is part of a oneof
         *  - an optional field that isn't a pointer type
         * (Meaning: not a message or a string).
         */
        const void *qmember =
            ((const char *) message) + field->quantifier_offset;

        if (field->label == PROTOBUF_C_LABEL_REQUIRED) {
            rv += mmProtobufC_RequiredFieldToString(field, member, desc);
        } else if ((field->label == PROTOBUF_C_LABEL_OPTIONAL ||
                field->label == PROTOBUF_C_LABEL_NONE) &&
               (0 != (field->flags & PROTOBUF_C_FIELD_FLAG_ONEOF))) {
            rv += mmProtobufC_OneofFieldToString(
                field,
                *(const uint32_t *) qmember,
                member,
                desc
            );
        } else if (field->label == PROTOBUF_C_LABEL_OPTIONAL) {
            rv += mmProtobufC_OptionalFieldToString(
                field,
                *(const protobuf_c_boolean *) qmember,
                member,
                                                    desc
            );
        } else if (field->label == PROTOBUF_C_LABEL_NONE) {
            rv += mmProtobufC_UnlabeledFieldToString(field, member, desc);
        } else {
            rv += mmProtobufC_RepeatedFieldToString(field, *(const size_t *) qmember,
                member, desc);
        }
    }
    for (i = 0; i < message->n_unknown_fields; i++)
        rv += mmProtobufC_UnknownFieldToString(&message->unknown_fields[i], desc);
    return rv;
}

