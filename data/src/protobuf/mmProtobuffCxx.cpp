/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmProtobuffCxx.h"
#include "core/mmString.h"
#include "core/mmLogger.h"
#include "net/mmStreambufPacket.h"

#include <google/protobuf/message.h>

MM_EXPORT_PROTOBUFF void mmProtobufCxx_Init()
{

}
MM_EXPORT_PROTOBUFF void mmProtobufCxx_Destroy()
{
    // Optional:  Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();
}

MM_EXPORT_PROTOBUFF void mmProtobufCxx_LoggerAppendPacket(struct mmString* value, struct mmPacket* pack)
{
    char val[128] = { 0 };
    mmSprintf(val, "uid:%" PRIu64 " mid:0x%08X", pack->phead.uid, pack->phead.mid);
    mmString_Appends(value, val);
}

MM_EXPORT_PROTOBUFF void mmProtobufCxx_LoggerAppendMessage(struct mmString* value, ::google::protobuf::Message* message)
{
    char val[64] = { 0 };
    size_t bsz = 0;
    size_t ssz = 0;
    std::string message_string;
    // append the message class name first.
    const google::protobuf::Descriptor* pDescriptor = message->GetDescriptor();
    mmString_Appends(value, pDescriptor->name().c_str());
    //
    message_string = message->ShortDebugString();

    bsz = message->ByteSizeLong();
    ssz = message_string.size();
    // append binary size and string size.
    mmSprintf(val, " [%" PRIuPTR "][%" PRIuPTR "]:= ", bsz, ssz);
    mmString_Appends(value, val);
    // if message string is to long.we cut it.
    if (MM_MAX_MESSAGE_LOGGER_LENGTH < ssz)
    {
        mmString_Appendsn(value, message_string.c_str(), MM_MAX_MESSAGE_LOGGER_LENGTH);
        mmString_Appends(value, "...");
    }
    else
    {
        mmString_Appends(value, message_string.c_str());
    }
}

MM_EXPORT_PROTOBUFF void mmProtobufCxx_LoggerAppendPacketMessage(struct mmString* value, struct mmPacket* pack, ::google::protobuf::Message* message)
{
    mmProtobufCxx_LoggerAppendPacket(value, pack);
    mmString_Appends(value, " ");
    mmProtobufCxx_LoggerAppendMessage(value, message);
}

// decode message form mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_DecodeMessage(struct mmPacket* pack, ::google::protobuf::Message* message)
{
    return mmProtobufCxx_DecodeMessageByteBuffer(&pack->bbuff, message);
}
// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_EncodeMessage(struct mmPacket* pack, ::google::protobuf::Message* message)
{
    return mmProtobufCxx_EncodeMessageByteBuffer(&pack->bbuff, message);
}
// decode message mmPacket --> message.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_DecodeMessageByteBuffer(struct mmByteBuffer* byte_buffer, ::google::protobuf::Message* message)
{
    int rt = -1;
    try
    {
        rt = (true == message->ParseFromArray((const void*)(byte_buffer->buffer + byte_buffer->offset), (int)byte_buffer->length)) ? 0 : -1;
    }
    catch (std::exception& e)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d error:%s", __FUNCTION__, __LINE__, e.what());
    }
    return rt;
}
// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_EncodeMessageByteBuffer(struct mmByteBuffer* byte_buffer, ::google::protobuf::Message* message)
{
    int rt = -1;
    try
    {
        rt = (true == message->SerializeToArray((void*)(byte_buffer->buffer + byte_buffer->offset), (int)byte_buffer->length)) ? 0 : -1;
    }
    catch (std::exception& e)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "%s %d error:%s", __FUNCTION__, __LINE__, e.what());
    }
    return rt;
}

// ::google::protobuf::Message
static size_t __static_MessageCoder_ProtobufCxxLength(void* message)
{
    ::google::protobuf::Message* message_protobuf = (::google::protobuf::Message*)message;
    return (size_t)message_protobuf->ByteSizeLong();
}
static int __static_MessageCoder_ProtobufCxxDecode(void* message, struct mmByteBuffer* byte_buffer)
{
    ::google::protobuf::Message* message_protobuf = (::google::protobuf::Message*)message;
    return mmProtobufCxx_DecodeMessageByteBuffer(byte_buffer, message_protobuf);
}
static int __static_MessageCoder_ProtobufCxxEncode(void* message, struct mmByteBuffer* byte_buffer)
{
    ::google::protobuf::Message* message_protobuf = (::google::protobuf::Message*)message;
    return mmProtobufCxx_EncodeMessageByteBuffer(byte_buffer, message_protobuf);
}
static struct mmMessageCoder g_message_coder_protobuf =
{
    &__static_MessageCoder_ProtobufCxxLength,
    &__static_MessageCoder_ProtobufCxxDecode,
    &__static_MessageCoder_ProtobufCxxEncode,
};

MM_EXPORT_PROTOBUFF struct mmMessageCoder* mmMessageCoder_ProtobufCxx()
{
    return &g_message_coder_protobuf;
}

// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_EncodeMessageProtobuf(struct mmStreambuf* streambuf, ::google::protobuf::Message* message, struct mmPacketHead* packet_head, size_t hlength, struct mmPacket* pack)
{
    return mmMessageCoder_AppendPacketHead(streambuf, &g_message_coder_protobuf, packet_head, message, hlength, pack);
}
