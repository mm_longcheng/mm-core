/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmProtobuffCxxNet_h__
#define __mmProtobuffCxxNet_h__

#include "core/mmCore.h"

#include "net/mmMailbox.h"
#include "net/mmHeadset.h"
#include "net/mmClientTcp.h"
#include "net/mmClientUdp.h"

#include "protobuf/mmProtobuffCxx.h"
#include "protobuf/mmProtobuffExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_PROTOBUFF int mmProtobufCxx_NTcpAppendRs(struct mmMailbox* mailbox, struct mmTcp* tcp, mmUInt32_t mid, ::google::protobuf::Message* rs_msg, struct mmPacket* rq_pack, struct mmPacket* rs_pack);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpAppendRs(struct mmHeadset* headset, struct mmUdp* udp, mmUInt32_t mid, ::google::protobuf::Message* rs_msg, struct mmPacket* rq_pack, struct mmPacket* rs_pack);

MM_EXPORT_PROTOBUFF int mmProtobufCxx_QTcpAppendRq(struct mmNetTcp* net_tcp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QUdpAppendRq(struct mmNetUdp* net_udp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NTcpAppendNt(struct mmMailbox* mailbox, struct mmTcp* tcp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* nt_msg, size_t hlength, struct mmPacket* nt_pack);
// proxy append
// net_tcp t_tcp is target.rq_pack rq pack pr_pack proxy pack.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_PTcpAppendRq(struct mmNetTcp* net_tcp, struct mmTcp* t_tcp, mmUInt32_t pid, mmUInt64_t sid, struct mmPacket* rq_pack, struct mmPacket* pr_pack);
// proxy append
// mailbox t_tcp is target.rs_pack rs pack pr_pack proxy pack.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_PTcpAppendRs(struct mmMailbox* mailbox, struct mmTcp* t_tcp, struct mmPacket* rs_pack, struct mmPacket* pr_pack);
// lock it outside manual.use for only mmMailbox send.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NTcpFlushSend(struct mmTcp* tcp);
// lock it outside manual.use for only mmNetUdp send.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpFlushSend(struct mmUdp* udp);
// lock it outside manual.use for client.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NNetTcpFlushSend(struct mmNetTcp* net_tcp);
// lock it outside manual.use for client.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NNetUdpFlushSend(struct mmNetUdp* net_udp);

MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpBufferSendReset(struct mmUdp* udp);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_NUdpBufferSendFlush(struct mmUdp* udp, struct mmSockaddr* remote);
// thread safe encode and append message.
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientTcpFlushMessageAppend(struct mmClientTcp* client_tcp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientUdpFlushMessageAppend(struct mmClientUdp* client_udp, mmUInt64_t uid, mmUInt32_t mid, ::google::protobuf::Message* rq_msg, size_t hlength, struct mmPacket* rq_pack, struct mmSockaddr* remote);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientTcpFlushSignal(struct mmClientTcp* client_tcp);
MM_EXPORT_PROTOBUFF int mmProtobufCxx_QClientUdpFlushSignal(struct mmClientUdp* client_udp);

#include "core/mmSuffix.h"

#endif//__mmProtobuffCxxNet_h__
