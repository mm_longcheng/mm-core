/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmListU32.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmListU32Iterator_Init(
    struct mmListU32Iterator* p)
{
    p->v = 0;
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListU32Iterator_Destroy(
    struct mmListU32Iterator* p)
{
    p->v = 0;
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListU32_Init(
    struct mmListU32* p)
{
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListU32_Destroy(
    struct mmListU32* p)
{
    mmListU32_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListU32_AddFont(
    struct mmListU32* p,
    mmUInt32_t v)
{
    struct mmListU32Iterator* it; 
    it = (struct mmListU32Iterator*)mmMalloc(sizeof(struct mmListU32Iterator));
    mmListU32Iterator_Init(it);
    it->v = v;
    mmList_Add(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
void
mmListU32_AddTail(
    struct mmListU32* p,
    mmUInt32_t v)
{
    struct mmListU32Iterator* it; 
    it = (struct mmListU32Iterator*)mmMalloc(sizeof(struct mmListU32Iterator));
    mmListU32Iterator_Init(it);
    it->v = v;
    mmList_AddTail(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
mmUInt32_t
mmListU32_PopFont(
    struct mmListU32* p)
{
    mmUInt32_t v = 0;
    struct mmListHead* next = p->l.next;
    struct mmListU32Iterator* it = mmList_Entry(next, struct mmListU32Iterator, n);
    v = it->v;
    mmListU32_Erase(p, it);
    return v;
}

MM_EXPORT_DLL
mmUInt32_t
mmListU32_PopTail(
    struct mmListU32* p)
{
    mmUInt32_t v = 0;
    struct mmListHead* prev = p->l.prev;
    struct mmListU32Iterator* it = mmList_Entry(prev, struct mmListU32Iterator, n);
    v = it->v;
    mmListU32_Erase(p, it);
    return v;
}

MM_EXPORT_DLL
void
mmListU32_Erase(
    struct mmListU32* p,
    struct mmListU32Iterator* it)
{
    mmList_Del(&it->n);
    p->size--;
    mmListU32Iterator_Destroy(it);
    mmFree(it);
}

MM_EXPORT_DLL
void
mmListU32_Clear(
    struct mmListU32* p)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListU32Iterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListU32Iterator, n);
        mmListU32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmListU32_Size(
    const struct mmListU32* p)
{
    return p->size;
}

MM_EXPORT_DLL
void
mmListU32_Remove(
    struct mmListU32* p,
    mmUInt32_t v)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListU32Iterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListU32Iterator, n);
        if (it->v == v)
        {
            mmListU32_Erase(p, it);
        }
    }
}

MM_EXPORT_DLL
void
mmListU32_InsertPrev(
    struct mmListU32* p,
    struct mmListU32Iterator* i,
    mmUInt32_t v)
{
    struct mmListU32Iterator* it;
    it = (struct mmListU32Iterator*)mmMalloc(sizeof(struct mmListU32Iterator));
    mmListU32Iterator_Init(it);
    it->v = v;
    mmList_AddTail(&it->n, &i->n);
    p->size++;
}

MM_EXPORT_DLL
void
mmListU32_InsertNext(
    struct mmListU32* p,
    struct mmListU32Iterator* i,
    mmUInt32_t v)
{
    struct mmListU32Iterator* it; 
    it = (struct mmListU32Iterator*)mmMalloc(sizeof(struct mmListU32Iterator));
    mmListU32Iterator_Init(it);
    it->v = v;
    mmList_Add(&it->n, &i->n);
    p->size++;
}
