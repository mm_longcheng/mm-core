/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVectorU64.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

MM_EXPORT_DLL
void
mmVectorU64_Init(
    struct mmVectorU64* p)
{
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorU64_Destroy(
    struct mmVectorU64* p)
{
    mmVectorU64_Free(p);
    //
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorU64_Assign(
    struct mmVectorU64* p,
    const struct mmVectorU64* q)
{
    mmVectorU64_Realloc(p, q->capacity);
    p->size = q->size;
    mmMemcpy(p->arrays, q->arrays, sizeof(mmUInt64_t) * p->size);
}

MM_EXPORT_DLL
void
mmVectorU64_Clear(
    struct mmVectorU64* p)
{
    p->size = 0;
}

MM_EXPORT_DLL
void
mmVectorU64_Reset(
    struct mmVectorU64* p)
{
    mmVectorU64_Free(p);
}

MM_EXPORT_DLL
void
mmVectorU64_SetIndex(
    struct mmVectorU64* p,
    size_t index,
    mmUInt64_t e)
{
    assert(0 <= index && index < p->size);
    p->arrays[index] = e;
}

MM_EXPORT_DLL
mmUInt64_t
mmVectorU64_GetIndex(
    const struct mmVectorU64* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
mmUInt64_t*
mmVectorU64_GetIndexReference(
    const struct mmVectorU64* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return &p->arrays[index];
}

MM_EXPORT_DLL
void
mmVectorU64_Realloc(
    struct mmVectorU64* p,
    size_t capacity)
{
    if (p->capacity < capacity)
    {
        mmUInt64_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        arrays = (mmUInt64_t*)mmMalloc(sizeof(mmUInt64_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmUInt64_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(mmUInt64_t) * p->capacity, 0, sizeof(mmUInt64_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    else if (capacity < (p->capacity / 4) && 8 < p->capacity)
    {
        mmUInt64_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        mem_size <<= 1;
        arrays = (mmUInt64_t*)mmMalloc(sizeof(mmUInt64_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmUInt64_t) * mem_size);
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorU64_Free(
    struct mmVectorU64* p)
{
    if (0 < p->capacity && NULL != p->arrays)
    {
        mmFree(p->arrays);
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
    else
    {
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
}

MM_EXPORT_DLL
void
mmVectorU64_Resize(
    struct mmVectorU64* p,
    size_t size)
{
    mmVectorU64_Realloc(p, size);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorU64_Reserve(
    struct mmVectorU64* p,
    size_t capacity)
{
    mmVectorU64_Realloc(p, capacity);
}

MM_EXPORT_DLL
void
mmVectorU64_AlignedMemory(
    struct mmVectorU64* p,
    size_t size)
{
    size_t sz = size;
    sz = sz < p->capacity ? p->capacity : sz;
    mmVectorU64_Realloc(p, sz);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorU64_AllocatorMemory(
    struct mmVectorU64* p,
    size_t size)
{
    if (p->capacity < size)
    {
        mmUInt64_t* arrays = NULL;
        size_t mem_size = size;
        arrays = (mmUInt64_t*)mmMalloc(sizeof(mmUInt64_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmUInt64_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(mmUInt64_t) * p->capacity, 0, sizeof(mmUInt64_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorU64_PushBack(
    struct mmVectorU64* p,
    mmUInt64_t e)
{
    size_t new_size = p->size + 1;
    if (new_size > p->capacity)
    {
        // vector push back realloc strategy, *= 1.5
        size_t capacity = p->capacity + p->capacity / 2;
        capacity = capacity < new_size ? new_size : capacity;
        mmVectorU64_Realloc(p, capacity);
    }
    p->arrays[p->size++] = e;
}

MM_EXPORT_DLL
mmUInt64_t
mmVectorU64_PopBack(
    struct mmVectorU64* p)
{
    assert(0 < p->size && "mmVectorU64 0 < p->size");
    return p->arrays[--p->size];
}

MM_EXPORT_DLL
mmUInt64_t*
mmVectorU64_Begin(
    const struct mmVectorU64* p)
{
    return &p->arrays[0];
}

MM_EXPORT_DLL
mmUInt64_t*
mmVectorU64_End(
    const struct mmVectorU64* p)
{
    return &p->arrays[p->size];
}

MM_EXPORT_DLL
mmUInt64_t*
mmVectorU64_Back(
    const struct mmVectorU64* p)
{
    assert(0 < p->size && "mmVectorU64 0 < p->size");
    return &p->arrays[p->size - 1];
}

MM_EXPORT_DLL
mmUInt64_t*
mmVectorU64_Next(
    const struct mmVectorU64* p,
    mmUInt64_t* iter)
{
    return iter + 1;
}

MM_EXPORT_DLL
mmUInt64_t
mmVectorU64_At(
    const struct mmVectorU64* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
size_t
mmVectorU64_Size(
    const struct mmVectorU64* p)
{
    return p->size;
}

MM_EXPORT_DLL
size_t
mmVectorU64_Capacity(
    const struct mmVectorU64* p)
{
    return p->capacity;
}

MM_EXPORT_DLL
void
mmVectorU64_Remove(
    struct mmVectorU64* p,
    size_t index)
{
    assert(0 < p->size && index <= p->size - 1 && "mmVectorU64 0 < p->size && index <= p->size - 1");
    if (index < p->size - 1)
    {
        mmMemmove(p->arrays + index, p->arrays + (index + 1), (p->size - index - 1) * sizeof(mmUInt64_t));
        p->size--;
    }
    else
    {
        p->size--;
    }
}

MM_EXPORT_DLL
mmUInt64_t*
mmVectorU64_Erase(
    struct mmVectorU64* p,
    mmUInt64_t* iter)
{
    mmUInt64_t* last = &p->arrays[p->size - 1];
    assert(0 < p->size && (&p->arrays[0]) <= iter && iter <= last && "0 < p->size && first <= iter && iter <= last");
    if (iter < last)
    {
        mmMemmove(iter, (mmUInt8_t*)iter + sizeof(mmUInt32_t), (mmUInt8_t*)last - (mmUInt8_t*)iter);
        p->size--;
    }
    else
    {
        p->size--;
    }
    return (iter + 1);
}

MM_EXPORT_DLL
int
mmVectorU64_Compare(
    const struct mmVectorU64* p,
    const struct mmVectorU64* q)
{
    if (p->size == q->size)
    {
        return (int)mmMemcmp(p->arrays, q->arrays, sizeof(mmUInt64_t) * p->size);
    }
    else
    {
        return (int)(p->size - q->size);
    }
}

MM_EXPORT_DLL
void
mmVectorU64_CopyFromValue(
    struct mmVectorU64* p,
    const struct mmVectorU64* q)
{
    mmVectorU64_Reset(p);
    mmVectorU64_AllocatorMemory(p, q->size);
    mmMemcpy(p->arrays, q->arrays, sizeof(mmUInt64_t) * p->size);
}