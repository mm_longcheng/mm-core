/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorU8_h__
#define __mmVectorU8_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorU8
{
    size_t size;
    size_t capacity;
    mmUInt8_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorU8_Init(
    struct mmVectorU8* p);

MM_EXPORT_DLL 
void 
mmVectorU8_Destroy(
    struct mmVectorU8* p);

MM_EXPORT_DLL 
void 
mmVectorU8_Assign(
    struct mmVectorU8* p, 
    const struct mmVectorU8* q);

MM_EXPORT_DLL 
void 
mmVectorU8_Clear(
    struct mmVectorU8* p);

MM_EXPORT_DLL 
void 
mmVectorU8_Reset(
    struct mmVectorU8* p);

MM_EXPORT_DLL 
void 
mmVectorU8_SetIndex(
    struct mmVectorU8* p, 
    size_t index, 
    mmUInt8_t e);

MM_EXPORT_DLL 
mmUInt8_t 
mmVectorU8_GetIndex(
    const struct mmVectorU8* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt8_t* 
mmVectorU8_GetIndexReference(
    const struct mmVectorU8* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorU8_Realloc(
    struct mmVectorU8* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU8_Free(
    struct mmVectorU8* p);

MM_EXPORT_DLL 
void 
mmVectorU8_Resize(
    struct mmVectorU8* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU8_Reserve(
    struct mmVectorU8* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU8_AlignedMemory(
    struct mmVectorU8* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU8_AllocatorMemory(
    struct mmVectorU8* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU8_PushBack(
    struct mmVectorU8* p, 
    mmUInt8_t e);

MM_EXPORT_DLL 
mmUInt8_t 
mmVectorU8_PopBack(
    struct mmVectorU8* p);

MM_EXPORT_DLL 
mmUInt8_t* 
mmVectorU8_Begin(
    const struct mmVectorU8* p);

MM_EXPORT_DLL 
mmUInt8_t* 
mmVectorU8_End(
    const struct mmVectorU8* p);

MM_EXPORT_DLL 
mmUInt8_t* 
mmVectorU8_Back(
    const struct mmVectorU8* p);

MM_EXPORT_DLL 
mmUInt8_t* 
mmVectorU8_Next(
    const struct mmVectorU8* p, 
    mmUInt8_t* iter);

MM_EXPORT_DLL 
mmUInt8_t 
mmVectorU8_At(
    const struct mmVectorU8* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorU8_Size(
    const struct mmVectorU8* p);

MM_EXPORT_DLL 
size_t 
mmVectorU8_Capacity(
    const struct mmVectorU8* p);

MM_EXPORT_DLL 
void 
mmVectorU8_Remove(
    struct mmVectorU8* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt8_t* 
mmVectorU8_Erase(
    struct mmVectorU8* p, 
    mmUInt8_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorU8_Compare(
    const struct mmVectorU8* p, 
    const struct mmVectorU8* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorU8_CopyFromValue(
    struct mmVectorU8* p, 
    const struct mmVectorU8* q);

// vector Null.
#define mmVectorU8_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorU8_Make(v)                    \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (mmUInt8_t*)v,         /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorU8_h__
