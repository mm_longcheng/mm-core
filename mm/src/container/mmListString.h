/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmListString_h__
#define __mmListString_h__

#include "core/mmCore.h"

#include "core/mmList.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

struct mmListStringIterator
{
    struct mmString v;
    struct mmListHead n;
};

MM_EXPORT_DLL 
void 
mmListStringIterator_Init(
    struct mmListStringIterator* p);

MM_EXPORT_DLL 
void 
mmListStringIterator_Destroy(
    struct mmListStringIterator* p);

struct mmListString
{
    struct mmListHead l;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmListString_Init(
    struct mmListString* p);

MM_EXPORT_DLL 
void 
mmListString_Destroy(
    struct mmListString* p);

MM_EXPORT_DLL 
void 
mmListString_AddFont(
    struct mmListString* p, 
    const struct mmString* v);

MM_EXPORT_DLL 
void 
mmListString_AddTail(
    struct mmListString* p, 
    const struct mmString* v);

MM_EXPORT_DLL 
void 
mmListString_PopFont(
    struct mmListString* p, 
    struct mmString* v);

MM_EXPORT_DLL 
void 
mmListString_PopTail(
    struct mmListString* p, 
    struct mmString* v);

MM_EXPORT_DLL 
void 
mmListString_Erase(
    struct mmListString* p, 
    struct mmListStringIterator* it);

MM_EXPORT_DLL 
void 
mmListString_Clear(
    struct mmListString* p);

MM_EXPORT_DLL 
size_t 
mmListString_Size(
    const struct mmListString* p);

MM_EXPORT_DLL 
void 
mmListString_Remove(
    struct mmListString* p, 
    const struct mmString* v);

MM_EXPORT_DLL 
void 
mmListString_InsertPrev(
    struct mmListString* p, 
    struct mmListStringIterator* i, 
    const struct mmString* v);

MM_EXPORT_DLL 
void 
mmListString_InsertNext(
    struct mmListString* p, 
    struct mmListStringIterator* i, 
    const struct mmString* v);

#include "core/mmSuffix.h"

#endif//__mmListString_h__
