/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeIntervalUIntptr.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
intptr_t
mmRbtreeIntervalUIntptrCompareFunc(
    uintptr_t ll, uintptr_t lr,
    uintptr_t rl, uintptr_t rr)
{
    if (rl <= ll && lr <= rr)
    {
        return 0;
    }
    else if (ll > rr)
    {
        return ll - rr;
    }
    else
    {
        return lr - rl;
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptrIterator_Init(
    struct mmRbtreeIntervalUIntptrIterator* p)
{
    p->l = 0;
    p->r = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptrIterator_Destroy(
    struct mmRbtreeIntervalUIntptrIterator* p)
{
    p->l = 0;
    p->r = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptrAllocator_Init(
    struct mmRbtreeIntervalUIntptrAllocator* p)
{
    p->Produce = &mmRbtreeIntervalUIntptr_WeakProduce;
    p->Recycle = &mmRbtreeIntervalUIntptr_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptrAllocator_Destroy(
    struct mmRbtreeIntervalUIntptrAllocator* p)
{
    p->Produce = &mmRbtreeIntervalUIntptr_WeakProduce;
    p->Recycle = &mmRbtreeIntervalUIntptr_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptr_Init(
    struct mmRbtreeIntervalUIntptr* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalUIntptrAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptr_Destroy(
    struct mmRbtreeIntervalUIntptr* p)
{
    mmRbtreeIntervalUIntptr_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalUIntptrAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptr_SetAllocator(
    struct mmRbtreeIntervalUIntptr* p,
    struct mmRbtreeIntervalUIntptrAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalUIntptr_Add(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    intptr_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalUIntptrIterator, n);
        result = mmRbtreeIntervalUIntptrCompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeIntervalUIntptrIterator*)mmMalloc(sizeof(struct mmRbtreeIntervalUIntptrIterator));
    mmRbtreeIntervalUIntptrIterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = (*(p->allocator.Produce))(p, l, r);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it->v;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptr_Rmv(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r)
{
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    intptr_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalUIntptrIterator, n);
        result = mmRbtreeIntervalUIntptrCompareFunc(l, r, it->l, it->r);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeIntervalUIntptr_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptr_Erase(
    struct mmRbtreeIntervalUIntptr* p,
    struct mmRbtreeIntervalUIntptrIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->l, it->r, it->v);
    mmRbtreeIntervalUIntptrIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalUIntptr_GetInstance(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r)
{
    void* v = mmRbtreeIntervalUIntptr_Get(p, l, r);
    if (NULL == v)
    {
        v = mmRbtreeIntervalUIntptr_Add(p, l, r);
    }
    return v;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalUIntptr_Get(
    const struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r)
{
    struct mmRbtreeIntervalUIntptrIterator* it = mmRbtreeIntervalUIntptr_GetIterator(p, l, r);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalUIntptrIterator*
mmRbtreeIntervalUIntptr_GetIterator(
    const struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r)
{
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    intptr_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalUIntptrIterator, n);
        result = mmRbtreeIntervalUIntptrCompareFunc(l, r, it->l, it->r);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalUIntptr_Clear(
    struct mmRbtreeIntervalUIntptr* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeIntervalUIntptrIterator, n);
        n = mmRb_Next(n);
        mmRbtreeIntervalUIntptr_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeIntervalUIntptr_Size(
    const struct mmRbtreeIntervalUIntptr* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalUIntptrIterator*
mmRbtreeIntervalUIntptr_Set(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    intptr_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalUIntptrIterator, n);
        result = mmRbtreeIntervalUIntptrCompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeIntervalUIntptrIterator*)mmMalloc(sizeof(struct mmRbtreeIntervalUIntptrIterator));
    mmRbtreeIntervalUIntptrIterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalUIntptrIterator*
mmRbtreeIntervalUIntptr_Insert(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    intptr_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalUIntptrIterator, n);
        result = mmRbtreeIntervalUIntptrCompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            return NULL;
        }
    }
    it = (struct mmRbtreeIntervalUIntptrIterator*)mmMalloc(sizeof(struct mmRbtreeIntervalUIntptrIterator));
    mmRbtreeIntervalUIntptrIterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalUIntptr_WeakProduce(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalUIntptr_WeakRecycle(
    struct mmRbtreeIntervalUIntptr* p,
    uintptr_t l, uintptr_t r,
    void* v)
{
    return v;
}

