/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeVisibleString_h__
#define __mmRbtreeVisibleString_h__

#include "core/mmCore.h"
#include "core/mmAtomic.h"

#include "container/mmRbtreeString.h"
#include "container/mmRbtsetString.h"
#include "container/mmListString.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// obj is struct mmRbtreeVisibleString.
// note:
// rbtree_visible traver callback we lock instance_locker outside, so the v is valid.
// if at traver callback handle trigger other callback, do not lock the v.
// if lock v before trigger other callback, some time will case dead lock at other callback inside.
// although the v is valid but we can lock v at this function, but careful for it.
typedef void(*mmRbtreeVisibleStringHandleFunc)(void* obj, void* u, const struct mmString* k, void* v);

// Suitable for:
//   1.multi-threading add and rmv.
//   2.multi-threading traverse.
//   3.multi-threading get.
//   4.allow traverse and main rbtree have little missing member(cache in two list).
struct mmRbtreeVisibleString
{
    // main rbtree.
    struct mmRbtreeStringVpt rbtree_m;
    // use for traverse
    struct mmRbtsetString rbtset_s;

    // cache for add when map_s is busy.
    struct mmListString l_a;
    // cache for add when map_s is busy.
    struct mmListString l_r;
    // 
    mmAtomic_t locker_m;
    mmAtomic_t locker_s;
    mmAtomic_t locker_a;
    mmAtomic_t locker_r;
};

MM_EXPORT_DLL 
void 
mmRbtreeVisibleString_Init(
    struct mmRbtreeVisibleString* p);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleString_Destroy(
    struct mmRbtreeVisibleString* p);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleString_Add(
    struct mmRbtreeVisibleString* p, 
    const struct mmString* k, 
    void* v);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleString_Rmv(
    struct mmRbtreeVisibleString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void* 
mmRbtreeVisibleString_Get(
    struct mmRbtreeVisibleString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleString_Clear(
    struct mmRbtreeVisibleString* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeVisibleString_Size(
    const struct mmRbtreeVisibleString* p);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleString_Traver(
    struct mmRbtreeVisibleString* p, 
    mmRbtreeVisibleStringHandleFunc handle, 
    void* u);

#include "core/mmSuffix.h"

#endif//__mmRbtreeVisibleString_h__
