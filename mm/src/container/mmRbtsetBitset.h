/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtsetBitset_h__
#define __mmRbtsetBitset_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"

#include "container/mmBitset.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
mmSInt32_t 
mmRbtsetBitsetCompareFunc(
    const struct mmBitset* lhs, 
    const struct mmBitset* rhs);

// mmRbtsetBitset
struct mmRbtsetBitsetIterator
{
    struct mmBitset k;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtsetBitsetIterator_Init(
    struct mmRbtsetBitsetIterator* p);

MM_EXPORT_DLL 
void 
mmRbtsetBitsetIterator_Destroy(
    struct mmRbtsetBitsetIterator* p);

struct mmRbtsetBitset
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtsetBitset_Init(
    struct mmRbtsetBitset* p);

MM_EXPORT_DLL 
void 
mmRbtsetBitset_Destroy(
    struct mmRbtsetBitset* p);

MM_EXPORT_DLL 
struct mmRbtsetBitsetIterator* 
mmRbtsetBitset_Add(
    struct mmRbtsetBitset* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtsetBitset_Rmv(
    struct mmRbtsetBitset* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtsetBitset_Erase(
    struct mmRbtsetBitset* p, 
    struct mmRbtsetBitsetIterator* it);

// return 1 if have member.0 is not member.
MM_EXPORT_DLL 
int 
mmRbtsetBitset_Get(
    const struct mmRbtsetBitset* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
struct mmRbtsetBitsetIterator* 
mmRbtsetBitset_GetIterator(
    const struct mmRbtsetBitset* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtsetBitset_Clear(
    struct mmRbtsetBitset* p);

MM_EXPORT_DLL 
size_t 
mmRbtsetBitset_Size(
    const struct mmRbtsetBitset* p);

MM_EXPORT_DLL 
struct mmRbtsetBitsetIterator* 
mmRbtsetBitset_Insert(
    struct mmRbtsetBitset* p, 
    const struct mmBitset* k);

#include "core/mmSuffix.h"

#endif//__mmRbtsetBitset_h__
