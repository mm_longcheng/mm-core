/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmListU32_h__
#define __mmListU32_h__

#include "core/mmCore.h"

#include "core/mmList.h"

#include "core/mmPrefix.h"

struct mmListU32Iterator
{
    mmUInt32_t v;
    struct mmListHead n;
};

MM_EXPORT_DLL 
void 
mmListU32Iterator_Init(
    struct mmListU32Iterator* p);

MM_EXPORT_DLL 
void 
mmListU32Iterator_Destroy(
    struct mmListU32Iterator* p);

struct mmListU32
{
    struct mmListHead l;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmListU32_Init(
    struct mmListU32* p);

MM_EXPORT_DLL 
void 
mmListU32_Destroy(
    struct mmListU32* p);

MM_EXPORT_DLL 
void 
mmListU32_AddFont(
    struct mmListU32* p, 
    mmUInt32_t v);

MM_EXPORT_DLL 
void 
mmListU32_AddTail(
    struct mmListU32* p, 
    mmUInt32_t v);

MM_EXPORT_DLL 
mmUInt32_t 
mmListU32_PopFont(
    struct mmListU32* p);

MM_EXPORT_DLL 
mmUInt32_t 
mmListU32_PopTail(
    struct mmListU32* p);

MM_EXPORT_DLL 
void 
mmListU32_Erase(
    struct mmListU32* p, 
    struct mmListU32Iterator* it);

MM_EXPORT_DLL 
void 
mmListU32_Clear(
    struct mmListU32* p);

MM_EXPORT_DLL 
size_t 
mmListU32_Size(
    const struct mmListU32* p);

MM_EXPORT_DLL 
void 
mmListU32_Remove(
    struct mmListU32* p, 
    mmUInt32_t v);

MM_EXPORT_DLL 
void 
mmListU32_InsertPrev(
    struct mmListU32* p, 
    struct mmListU32Iterator* i, 
    mmUInt32_t v);

MM_EXPORT_DLL 
void 
mmListU32_InsertNext(
    struct mmListU32* p, 
    struct mmListU32Iterator* i, 
    mmUInt32_t v);

#include "core/mmSuffix.h"

#endif//__mmListU32_h__
