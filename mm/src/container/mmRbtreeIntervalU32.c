/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeIntervalU32.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
mmSInt32_t
mmRbtreeIntervalU32CompareFunc(
    mmUInt32_t ll, mmUInt32_t lr,
    mmUInt32_t rl, mmUInt32_t rr)
{
    if (rl <= ll && lr <= rr)
    {
        return 0;
    }
    else if (ll > rr)
    {
        return ll - rr;
    }
    else
    {
        return lr - rl;
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32Iterator_Init(
    struct mmRbtreeIntervalU32Iterator* p)
{
    p->l = 0;
    p->r = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32Iterator_Destroy(
    struct mmRbtreeIntervalU32Iterator* p)
{
    p->l = 0;
    p->r = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32Allocator_Init(
    struct mmRbtreeIntervalU32Allocator* p)
{
    p->Produce = &mmRbtreeIntervalU32_WeakProduce;
    p->Recycle = &mmRbtreeIntervalU32_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32Allocator_Destroy(
    struct mmRbtreeIntervalU32Allocator* p)
{
    p->Produce = &mmRbtreeIntervalU32_WeakProduce;
    p->Recycle = &mmRbtreeIntervalU32_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32_Init(
    struct mmRbtreeIntervalU32* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalU32Allocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32_Destroy(
    struct mmRbtreeIntervalU32* p)
{
    mmRbtreeIntervalU32_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalU32Allocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32_SetAllocator(
    struct mmRbtreeIntervalU32* p,
    struct mmRbtreeIntervalU32Allocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU32_Add(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalU32Iterator, n);
        result = mmRbtreeIntervalU32CompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeIntervalU32Iterator*)mmMalloc(sizeof(struct mmRbtreeIntervalU32Iterator));
    mmRbtreeIntervalU32Iterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = (*(p->allocator.Produce))(p, l, r);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it->v;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32_Rmv(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r)
{
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalU32Iterator, n);
        result = mmRbtreeIntervalU32CompareFunc(l, r, it->l, it->r);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeIntervalU32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32_Erase(
    struct mmRbtreeIntervalU32* p,
    struct mmRbtreeIntervalU32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->l, it->r, it->v);
    mmRbtreeIntervalU32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU32_GetInstance(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r)
{
    void* v = mmRbtreeIntervalU32_Get(p, l, r);
    if (NULL == v)
    {
        v = mmRbtreeIntervalU32_Add(p, l, r);
    }
    return v;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU32_Get(
    const struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r)
{
    struct mmRbtreeIntervalU32Iterator* it = mmRbtreeIntervalU32_GetIterator(p, l, r);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalU32Iterator*
mmRbtreeIntervalU32_GetIterator(
    const struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r)
{
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalU32Iterator, n);
        result = mmRbtreeIntervalU32CompareFunc(l, r, it->l, it->r);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU32_Clear(
    struct mmRbtreeIntervalU32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeIntervalU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeIntervalU32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeIntervalU32_Size(
    const struct mmRbtreeIntervalU32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalU32Iterator* 
mmRbtreeIntervalU32_Set(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalU32Iterator, n);
        result = mmRbtreeIntervalU32CompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeIntervalU32Iterator*)mmMalloc(sizeof(struct mmRbtreeIntervalU32Iterator));
    mmRbtreeIntervalU32Iterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalU32Iterator*
mmRbtreeIntervalU32_Insert(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalU32Iterator, n);
        result = mmRbtreeIntervalU32CompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            return NULL;
        }
    }
    it = (struct mmRbtreeIntervalU32Iterator*)mmMalloc(sizeof(struct mmRbtreeIntervalU32Iterator));
    mmRbtreeIntervalU32Iterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU32_WeakProduce(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU32_WeakRecycle(
    struct mmRbtreeIntervalU32* p,
    mmUInt32_t l, mmUInt32_t r,
    void* v)
{
    return v;
}

