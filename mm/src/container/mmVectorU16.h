/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorU16_h__
#define __mmVectorU16_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorU16
{
    size_t size;
    size_t capacity;
    mmUInt16_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorU16_Init(
    struct mmVectorU16* p);

MM_EXPORT_DLL 
void 
mmVectorU16_Destroy(
    struct mmVectorU16* p);

MM_EXPORT_DLL 
void 
mmVectorU16_Assign(
    struct mmVectorU16* p, 
    const struct mmVectorU16* q);

MM_EXPORT_DLL 
void 
mmVectorU16_Clear(
    struct mmVectorU16* p);

MM_EXPORT_DLL 
void 
mmVectorU16_Reset(
    struct mmVectorU16* p);

MM_EXPORT_DLL 
void 
mmVectorU16_SetIndex(
    struct mmVectorU16* p, 
    size_t index, 
    mmUInt16_t e);

MM_EXPORT_DLL 
mmUInt16_t 
mmVectorU16_GetIndex(
    const struct mmVectorU16* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt16_t* 
mmVectorU16_GetIndexReference(
    const struct mmVectorU16* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorU16_Realloc(
    struct mmVectorU16* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU16_Free(
    struct mmVectorU16* p);

MM_EXPORT_DLL 
void 
mmVectorU16_Resize(
    struct mmVectorU16* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU16_Reserve(
    struct mmVectorU16* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU16_AlignedMemory(
    struct mmVectorU16* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU16_AllocatorMemory(
    struct mmVectorU16* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU16_PushBack(
    struct mmVectorU16* p, 
    mmUInt16_t e);

MM_EXPORT_DLL 
mmUInt16_t 
mmVectorU16_PopBack(
    struct mmVectorU16* p);

MM_EXPORT_DLL 
mmUInt16_t* 
mmVectorU16_Begin(
    const struct mmVectorU16* p);

MM_EXPORT_DLL 
mmUInt16_t* 
mmVectorU16_End(
    const struct mmVectorU16* p);

MM_EXPORT_DLL 
mmUInt16_t* 
mmVectorU16_Back(
    const struct mmVectorU16* p);

MM_EXPORT_DLL 
mmUInt16_t* 
mmVectorU16_Next(
    const struct mmVectorU16* p, 
    mmUInt16_t* iter);

MM_EXPORT_DLL 
mmUInt16_t 
mmVectorU16_At(
    const struct mmVectorU16* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorU16_Size(
    const struct mmVectorU16* p);

MM_EXPORT_DLL 
size_t 
mmVectorU16_Capacity(
    const struct mmVectorU16* p);

MM_EXPORT_DLL 
void 
mmVectorU16_Remove(
    struct mmVectorU16* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt16_t* 
mmVectorU16_Erase(
    struct mmVectorU16* p, 
    mmUInt16_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorU16_Compare(
    const struct mmVectorU16* p, 
    const struct mmVectorU16* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorU16_CopyFromValue(
    struct mmVectorU16* p, 
    const struct mmVectorU16* q);

// vector Null.
#define mmVectorU16_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorU16_Make(v)                   \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (mmUInt16_t*)v,        /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorU16_h__
