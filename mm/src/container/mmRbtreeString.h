/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeString_h__
#define __mmRbtreeString_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
mmSInt32_t 
mmRbtreeStringCompareFunc(
    const struct mmString* lhs, 
    const struct mmString* rhs);

// mmRbtreeStringU32
struct mmRbtreeStringU32Iterator
{
    struct mmString k;
    mmUInt32_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringU32Iterator_Init(
    struct mmRbtreeStringU32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringU32Iterator_Destroy(
    struct mmRbtreeStringU32Iterator* p);

struct mmRbtreeStringU32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringU32_Init(
    struct mmRbtreeStringU32* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringU32_Destroy(
    struct mmRbtreeStringU32* p);

MM_EXPORT_DLL 
struct mmRbtreeStringU32Iterator* 
mmRbtreeStringU32_Add(
    struct mmRbtreeStringU32* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringU32_Rmv(
    struct mmRbtreeStringU32* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringU32_Erase(
    struct mmRbtreeStringU32* p, 
    struct mmRbtreeStringU32Iterator* it);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeStringU32_GetInstance(
    struct mmRbtreeStringU32* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeStringU32_Get(
    const struct mmRbtreeStringU32* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
struct mmRbtreeStringU32Iterator* 
mmRbtreeStringU32_GetIterator(
    const struct mmRbtreeStringU32* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringU32_Clear(
    struct mmRbtreeStringU32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeStringU32_Size(
    const struct mmRbtreeStringU32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeStringU32Iterator* 
mmRbtreeStringU32_Set(
    struct mmRbtreeStringU32* p, 
    const struct mmString* k, 
    mmUInt32_t v);

MM_EXPORT_DLL 
struct mmRbtreeStringU32Iterator* 
mmRbtreeStringU32_Insert(
    struct mmRbtreeStringU32* p, 
    const struct mmString* k, 
    mmUInt32_t v);

// mmRbtreeStringString
struct mmRbtreeStringU64Iterator
{
    struct mmString k;
    mmUInt64_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringU64Iterator_Init(
    struct mmRbtreeStringU64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringU64Iterator_Destroy(
    struct mmRbtreeStringU64Iterator* p);

struct mmRbtreeStringU64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringU64_Init(
    struct mmRbtreeStringU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringU64_Destroy(
    struct mmRbtreeStringU64* p);

MM_EXPORT_DLL 
struct mmRbtreeStringU64Iterator* 
mmRbtreeStringU64_Add(
    struct mmRbtreeStringU64* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringU64_Rmv(
    struct mmRbtreeStringU64* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringU64_Erase(
    struct mmRbtreeStringU64* p, 
    struct mmRbtreeStringU64Iterator* it);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeStringU64_GetInstance(
    struct mmRbtreeStringU64* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeStringU64_Get(
    const struct mmRbtreeStringU64* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
struct mmRbtreeStringU64Iterator* 
mmRbtreeStringU64_GetIterator(
    const struct mmRbtreeStringU64* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringU64_Clear(
    struct mmRbtreeStringU64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeStringU64_Size(
    const struct mmRbtreeStringU64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeStringU64Iterator* 
mmRbtreeStringU64_Set(
    struct mmRbtreeStringU64* p, 
    const struct mmString* k, 
    mmUInt64_t v);

MM_EXPORT_DLL 
struct mmRbtreeStringU64Iterator* 
mmRbtreeStringU64_Insert(
    struct mmRbtreeStringU64* p, 
    const struct mmString* k, 
    mmUInt64_t v);

// mmRbtreeStringString
struct mmRbtreeStringStringIterator
{
    struct mmString k;
    struct mmString v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringStringIterator_Init(
    struct mmRbtreeStringStringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringStringIterator_Destroy(
    struct mmRbtreeStringStringIterator* p);

struct mmRbtreeStringString
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringString_Init(
    struct mmRbtreeStringString* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringString_Destroy(
    struct mmRbtreeStringString* p);

MM_EXPORT_DLL 
struct mmRbtreeStringStringIterator* 
mmRbtreeStringString_Add(
    struct mmRbtreeStringString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringString_Rmv(
    struct mmRbtreeStringString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringString_Erase(
    struct mmRbtreeStringString* p, 
    struct mmRbtreeStringStringIterator* it);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeStringString_GetInstance(
    struct mmRbtreeStringString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeStringString_Get(
    const struct mmRbtreeStringString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
struct mmRbtreeStringStringIterator* 
mmRbtreeStringString_GetIterator(
    const struct mmRbtreeStringString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringString_Clear(
    struct mmRbtreeStringString* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeStringString_Size(
    const struct mmRbtreeStringString* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeStringStringIterator* 
mmRbtreeStringString_Set(
    struct mmRbtreeStringString* p, 
    const struct mmString* k, 
    const struct mmString* v);

MM_EXPORT_DLL 
struct mmRbtreeStringStringIterator* 
mmRbtreeStringString_Insert(
    struct mmRbtreeStringString* p, 
    const struct mmString* k, 
    const struct mmString* v);

// mmRbtreeStringVpt
struct mmRbtreeStringVptIterator
{
    struct mmString k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringVptIterator_Init(
    struct mmRbtreeStringVptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringVptIterator_Destroy(
    struct mmRbtreeStringVptIterator* p);

struct mmRbtreeStringVpt;

struct mmRbtreeStringVptAllocator
{
    void* (*Produce)(struct mmRbtreeStringVpt* p, const struct mmString* k);
    void* (*Recycle)(struct mmRbtreeStringVpt* p, const struct mmString* k, void* v);
};

MM_EXPORT_DLL 
void 
mmRbtreeStringVptAllocator_Init(
    struct mmRbtreeStringVptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringVptAllocator_Destroy(
    struct mmRbtreeStringVptAllocator* p);

struct mmRbtreeStringVpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeStringVptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeStringVpt_Init(
    struct mmRbtreeStringVpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringVpt_Destroy(
    struct mmRbtreeStringVpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeStringVpt_SetAllocator(
    struct mmRbtreeStringVpt* p, 
    struct mmRbtreeStringVptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeStringVptIterator* 
mmRbtreeStringVpt_Add(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringVpt_Rmv(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringVpt_Erase(
    struct mmRbtreeStringVpt* p, 
    struct mmRbtreeStringVptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeStringVpt_GetInstance(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void* 
mmRbtreeStringVpt_Get(
    const struct mmRbtreeStringVpt* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
struct mmRbtreeStringVptIterator* 
mmRbtreeStringVpt_GetIterator(
    const struct mmRbtreeStringVpt* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtreeStringVpt_Clear(
    struct mmRbtreeStringVpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeStringVpt_Size(
    const struct mmRbtreeStringVpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeStringVptIterator* 
mmRbtreeStringVpt_Set(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeStringVptIterator* 
mmRbtreeStringVpt_Insert(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeStringVpt_WeakProduce(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void* 
mmRbtreeStringVpt_WeakRecycle(
    struct mmRbtreeStringVpt* p, 
    const struct mmString* k, 
    void* v);

// mmRbtreeWeakStringVpt
struct mmRbtreeWeakStringVptIterator
{
    const struct mmString* k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptIterator_Init(
    struct mmRbtreeWeakStringVptIterator* p);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptIterator_Destroy(
    struct mmRbtreeWeakStringVptIterator* p);

struct mmRbtreeWeakStringVpt;

struct mmRbtreeWeakStringVptAllocator
{
    void* (*Produce)(struct mmRbtreeWeakStringVpt* p, const struct mmString* k);
    void* (*Recycle)(struct mmRbtreeWeakStringVpt* p, const struct mmString* k, void* v);
};

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptAllocator_Init(
    struct mmRbtreeWeakStringVptAllocator* p);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptAllocator_Destroy(
    struct mmRbtreeWeakStringVptAllocator* p);

struct mmRbtreeWeakStringVpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeWeakStringVptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Init(
    struct mmRbtreeWeakStringVpt* p);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Destroy(
    struct mmRbtreeWeakStringVpt* p);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_SetAllocator(
    struct mmRbtreeWeakStringVpt* p,
    struct mmRbtreeWeakStringVptAllocator* allocator);

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_Add(
        struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Rmv(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Erase(
    struct mmRbtreeWeakStringVpt* p,
    struct mmRbtreeWeakStringVptIterator* it);

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_GetInstance(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k);

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_Get(
    const struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k);

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_GetIterator(
        const struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k);

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Clear(
    struct mmRbtreeWeakStringVpt* p);

MM_EXPORT_DLL
size_t
mmRbtreeWeakStringVpt_Size(
    const struct mmRbtreeWeakStringVpt* p);

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_Set(
        struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k,
        void* v);

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_Insert(
        struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k,
        void* v);

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_WeakProduce(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k);

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_WeakRecycle(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k,
    void* v);

// mmRbtreeStrStr
struct mmRbtreeStrStrIterator
{
    const char* k;
    const char* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeStrStrIterator_Init(
    struct mmRbtreeStrStrIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStrStrIterator_Destroy(
    struct mmRbtreeStrStrIterator* p);

struct mmRbtreeStrStr
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeStrStr_Init(
    struct mmRbtreeStrStr* p);

MM_EXPORT_DLL 
void 
mmRbtreeStrStr_Destroy(
    struct mmRbtreeStrStr* p);

MM_EXPORT_DLL 
struct mmRbtreeStrStrIterator* 
mmRbtreeStrStr_Add(
    struct mmRbtreeStrStr* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtreeStrStr_Rmv(
    struct mmRbtreeStrStr* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtreeStrStr_Erase(
    struct mmRbtreeStrStr* p, 
    struct mmRbtreeStrStrIterator* it);

MM_EXPORT_DLL 
const char* 
mmRbtreeStrStr_GetInstance(
    struct mmRbtreeStrStr* p, 
    const char* k);

MM_EXPORT_DLL 
const char* 
mmRbtreeStrStr_Get(
    const struct mmRbtreeStrStr* p, 
    const char* k);

MM_EXPORT_DLL 
struct mmRbtreeStrStrIterator* 
mmRbtreeStrStr_GetIterator(
    const struct mmRbtreeStrStr* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtreeStrStr_Clear(
    struct mmRbtreeStrStr* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeStrStr_Size(
    const struct mmRbtreeStrStr* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeStrStrIterator* 
mmRbtreeStrStr_Set(
    struct mmRbtreeStrStr* p, 
    const char* k, 
    const char* v);

MM_EXPORT_DLL 
struct mmRbtreeStrStrIterator* 
mmRbtreeStrStr_Insert(
    struct mmRbtreeStrStr* p, 
    const char* k, 
    const char* v);

// mmRbtreeStrVpt
struct mmRbtreeStrVptIterator
{
    const char* k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeStrVptIterator_Init(
    struct mmRbtreeStrVptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStrVptIterator_Destroy(
    struct mmRbtreeStrVptIterator* p);

struct mmRbtreeStrVpt;

struct mmRbtreeStrVptAllocator
{
    void* (*Produce)(struct mmRbtreeStrVpt* p, const char* k);
    void* (*Recycle)(struct mmRbtreeStrVpt* p, const char* k, void* v);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_DLL 
void 
mmRbtreeStrVptAllocator_Init(
    struct mmRbtreeStrVptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeStrVptAllocator_Destroy(
    struct mmRbtreeStrVptAllocator* p);

struct mmRbtreeStrVpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeStrVptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeStrVpt_Init(
    struct mmRbtreeStrVpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeStrVpt_Destroy(
    struct mmRbtreeStrVpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeStrVpt_SetAllocator(
    struct mmRbtreeStrVpt* p, 
    struct mmRbtreeStrVptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeStrVptIterator* 
mmRbtreeStrVpt_Add(
    struct mmRbtreeStrVpt* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtreeStrVpt_Rmv(
    struct mmRbtreeStrVpt* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtreeStrVpt_Erase(
    struct mmRbtreeStrVpt* p, 
    struct mmRbtreeStrVptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeStrVpt_GetInstance(
    struct mmRbtreeStrVpt* p, 
    const char* k);

MM_EXPORT_DLL 
void* 
mmRbtreeStrVpt_Get(
    const struct mmRbtreeStrVpt* p, 
    const char* k);

MM_EXPORT_DLL 
struct mmRbtreeStrVptIterator* 
mmRbtreeStrVpt_GetIterator(
    const struct mmRbtreeStrVpt* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtreeStrVpt_Clear(
    struct mmRbtreeStrVpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeStrVpt_Size(
    const struct mmRbtreeStrVpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeStrVptIterator* 
mmRbtreeStrVpt_Set(
    struct mmRbtreeStrVpt* p, 
    const char* k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeStrVptIterator* 
mmRbtreeStrVpt_Insert(
    struct mmRbtreeStrVpt* p, 
    const char* k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeStrVpt_WeakProduce(
    struct mmRbtreeStrVpt* p, 
    const char* k);

MM_EXPORT_DLL 
void* 
mmRbtreeStrVpt_WeakRecycle(
    struct mmRbtreeStrVpt* p, 
    const char* k, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeString_h__
