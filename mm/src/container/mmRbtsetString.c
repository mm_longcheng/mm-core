/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtsetString.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
mmSInt32_t
mmRbtsetStringCompareFunc(
    const struct mmString* lhs,
    const struct mmString* rhs)
{
    return (mmSInt32_t)mmString_Compare(lhs, rhs);
}

MM_EXPORT_DLL
void
mmRbtsetStringIterator_Init(
    struct mmRbtsetStringIterator* p)
{
    mmString_Init(&p->k);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetStringIterator_Destroy(
    struct mmRbtsetStringIterator* p)
{
    mmString_Destroy(&p->k);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetString_Init(
    struct mmRbtsetString* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtsetString_Destroy(
    struct mmRbtsetString* p)
{
    mmRbtsetString_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtsetStringIterator*
mmRbtsetString_Add(
    struct mmRbtsetString* p,
    const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetStringIterator, n);
        result = mmRbtsetStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtsetStringIterator*)mmMalloc(sizeof(struct mmRbtsetStringIterator));
    mmRbtsetStringIterator_Init(it);
    mmString_Assign(&it->k, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtsetString_Rmv(
    struct mmRbtsetString* p,
    const struct mmString* k)
{
    struct mmRbtsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetStringIterator, n);
        result = mmRbtsetStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtsetString_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtsetString_Erase(
    struct mmRbtsetString* p,
    struct mmRbtsetStringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtsetStringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

// return 1 if have member.0 is not member.
MM_EXPORT_DLL
int
mmRbtsetString_Get(
    const struct mmRbtsetString* p,
    const struct mmString* k)
{
    struct mmRbtsetStringIterator* it = mmRbtsetString_GetIterator(p, k);
    return (NULL != it) ? 1 : 0;
}

MM_EXPORT_DLL
struct mmRbtsetStringIterator*
mmRbtsetString_GetIterator(
    const struct mmRbtsetString* p,
    const struct mmString* k)
{
    struct mmRbtsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetStringIterator, n);
        result = mmRbtsetStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtsetString_Clear(
    struct mmRbtsetString* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetStringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetStringIterator, n);
        n = mmRb_Next(n);
        mmRbtsetString_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtsetString_Size(
    const struct mmRbtsetString* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtsetStringIterator*
mmRbtsetString_Insert(
    struct mmRbtsetString* p,
    const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetStringIterator, n);
        result = mmRbtsetStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtsetStringIterator*)mmMalloc(sizeof(struct mmRbtsetStringIterator));
    mmRbtsetStringIterator_Init(it);
    mmString_Assign(&it->k, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtsetStrIterator_Init(
    struct mmRbtsetStrIterator* p)
{
    p->k = "";
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetStrIterator_Destroy(
    struct mmRbtsetStrIterator* p)
{
    p->k = "";
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetStr_Init(
    struct mmRbtsetStr* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtsetStr_Destroy(
    struct mmRbtsetStr* p)
{
    mmRbtsetStr_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtsetStrIterator*
mmRbtsetStr_Add(
    struct mmRbtsetStr* p,
    const char* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetStrIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetStrIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtsetStrIterator*)mmMalloc(sizeof(struct mmRbtsetStrIterator));
    mmRbtsetStrIterator_Init(it);
    it->k = k;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtsetStr_Rmv(
    struct mmRbtsetStr* p,
    const char* k)
{
    struct mmRbtsetStrIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetStrIterator, n);
        result = strcmp(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtsetStr_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtsetStr_Erase(
    struct mmRbtsetStr* p,
    struct mmRbtsetStrIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtsetStrIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

// return 1 if have member.0 is not member.
MM_EXPORT_DLL
int
mmRbtsetStr_Get(
    const struct mmRbtsetStr* p,
    const char* k)
{
    struct mmRbtsetStrIterator* it = mmRbtsetStr_GetIterator(p, k);
    return (NULL != it) ? 1 : 0;
}

MM_EXPORT_DLL
struct mmRbtsetStrIterator*
mmRbtsetStr_GetIterator(
    const struct mmRbtsetStr* p,
    const char* k)
{
    struct mmRbtsetStrIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetStrIterator, n);
        result = strcmp(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtsetStr_Clear(
    struct mmRbtsetStr* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetStrIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetStrIterator, n);
        n = mmRb_Next(n);
        mmRbtsetStr_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtsetStr_Size(
    const struct mmRbtsetStr* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtsetStrIterator*
mmRbtsetStr_Insert(
    struct mmRbtsetStr* p,
    const char* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetStrIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetStrIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtsetStrIterator*)mmMalloc(sizeof(struct mmRbtsetStrIterator));
    mmRbtsetStrIterator_Init(it);
    it->k = k;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}
