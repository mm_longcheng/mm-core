/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorF32_h__
#define __mmVectorF32_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorF32
{
    size_t size;
    size_t capacity;
    mmFloat32_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorF32_Init(
    struct mmVectorF32* p);

MM_EXPORT_DLL 
void 
mmVectorF32_Destroy(
    struct mmVectorF32* p);

MM_EXPORT_DLL 
void 
mmVectorF32_Assign(
    struct mmVectorF32* p, 
    const struct mmVectorF32* q);

MM_EXPORT_DLL 
void 
mmVectorF32_Clear(
    struct mmVectorF32* p);

MM_EXPORT_DLL 
void 
mmVectorF32_Reset(
    struct mmVectorF32* p);

MM_EXPORT_DLL 
void 
mmVectorF32_SetIndex(
    struct mmVectorF32* p, 
    size_t index, 
    mmFloat32_t e);

MM_EXPORT_DLL 
mmFloat32_t 
mmVectorF32_GetIndex(
    const struct mmVectorF32* p, 
    size_t index);

MM_EXPORT_DLL 
mmFloat32_t* 
mmVectorF32_GetIndexReference(
    const struct mmVectorF32* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorF32_Realloc(
    struct mmVectorF32* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorF32_Free(
    struct mmVectorF32* p);

MM_EXPORT_DLL 
void 
mmVectorF32_Resize(
    struct mmVectorF32* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorF32_Reserve(
    struct mmVectorF32* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorF32_AlignedMemory(
    struct mmVectorF32* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorF32_AllocatorMemory(
    struct mmVectorF32* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorF32_PushBack(
    struct mmVectorF32* p, 
    mmFloat32_t e);

MM_EXPORT_DLL 
mmFloat32_t 
mmVectorF32_PopBack(
    struct mmVectorF32* p);

MM_EXPORT_DLL 
mmFloat32_t* 
mmVectorF32_Begin(
    const struct mmVectorF32* p);

MM_EXPORT_DLL 
mmFloat32_t* 
mmVectorF32_End(
    const struct mmVectorF32* p);

MM_EXPORT_DLL 
mmFloat32_t* 
mmVectorF32_Back(
    const struct mmVectorF32* p);

MM_EXPORT_DLL 
mmFloat32_t* 
mmVectorF32_Next(
    const struct mmVectorF32* p, 
    mmFloat32_t* iter);

MM_EXPORT_DLL 
mmFloat32_t 
mmVectorF32_At(
    const struct mmVectorF32* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorF32_Size(
    const struct mmVectorF32* p);

MM_EXPORT_DLL 
size_t 
mmVectorF32_Capacity(
    const struct mmVectorF32* p);

MM_EXPORT_DLL 
void 
mmVectorF32_Remove(
    struct mmVectorF32* p, 
    size_t index);

MM_EXPORT_DLL 
mmFloat32_t* 
mmVectorF32_Erase(
    struct mmVectorF32* p, 
    mmFloat32_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorF32_Compare(
    const struct mmVectorF32* p, 
    const struct mmVectorF32* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorF32_CopyFromValue(
    struct mmVectorF32* p, 
    const struct mmVectorF32* q);

// vector Null.
#define mmVectorF32_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorF32_Make(v)                   \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (mmFloat32_t*)v,       /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorF32_h__
