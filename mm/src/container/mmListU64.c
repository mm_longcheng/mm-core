/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmListU64.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmListU64Iterator_Init(
    struct mmListU64Iterator* p)
{
    p->v = 0;
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListU64Iterator_Destroy(
    struct mmListU64Iterator* p)
{
    p->v = 0;
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListU64_Init(
    struct mmListU64* p)
{
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListU64_Destroy(
    struct mmListU64* p)
{
    mmListU64_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListU64_AddFont(
    struct mmListU64* p,
    mmUInt64_t v)
{
    struct mmListU64Iterator* it; 
    it = (struct mmListU64Iterator*)mmMalloc(sizeof(struct mmListU64Iterator));
    mmListU64Iterator_Init(it);
    it->v = v;
    mmList_Add(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
void
mmListU64_AddTail(
    struct mmListU64* p,
    mmUInt64_t v)
{
    struct mmListU64Iterator* it; 
    it = (struct mmListU64Iterator*)mmMalloc(sizeof(struct mmListU64Iterator));
    mmListU64Iterator_Init(it);
    it->v = v;
    mmList_AddTail(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
mmUInt64_t
mmListU64_PopFont(
    struct mmListU64* p)
{
    mmUInt64_t v = 0;
    struct mmListHead* next = p->l.next;
    struct mmListU64Iterator* it = mmList_Entry(next, struct mmListU64Iterator, n);
    v = it->v;
    mmListU64_Erase(p, it);
    return v;
}

MM_EXPORT_DLL
mmUInt64_t
mmListU64_PopTail(
    struct mmListU64* p)
{
    mmUInt64_t v = 0;
    struct mmListHead* prev = p->l.prev;
    struct mmListU64Iterator* it = mmList_Entry(prev, struct mmListU64Iterator, n);
    v = it->v;
    mmListU64_Erase(p, it);
    return v;
}

MM_EXPORT_DLL
void
mmListU64_Erase(
    struct mmListU64* p,
    struct mmListU64Iterator* it)
{
    mmList_Del(&it->n);
    p->size--;
    mmListU64Iterator_Destroy(it);
    mmFree(it);
}

MM_EXPORT_DLL
void
mmListU64_Clear(
    struct mmListU64* p)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListU64Iterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListU64Iterator, n);
        mmListU64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmListU64_Size(
    const struct mmListU64* p)
{
    return p->size;
}

MM_EXPORT_DLL
void
mmListU64_Remove(
    struct mmListU64* p,
    mmUInt64_t v)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListU64Iterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListU64Iterator, n);
        if (it->v == v)
        {
            mmListU64_Erase(p, it);
        }
    }
}

MM_EXPORT_DLL
void
mmListU64_InsertPrev(
    struct mmListU64* p,
    struct mmListU64Iterator* i,
    mmUInt64_t v)
{
    struct mmListU64Iterator* it; 
    it = (struct mmListU64Iterator*)mmMalloc(sizeof(struct mmListU64Iterator));
    mmListU64Iterator_Init(it);
    it->v = v;
    mmList_AddTail(&it->n, &i->n);
    p->size++;
}

MM_EXPORT_DLL
void
mmListU64_InsertNext(
    struct mmListU64* p,
    struct mmListU64Iterator* i,
    mmUInt64_t v)
{
    struct mmListU64Iterator* it;
    it = (struct mmListU64Iterator*)mmMalloc(sizeof(struct mmListU64Iterator));
    mmListU64Iterator_Init(it);
    it->v = v;
    mmList_Add(&it->n, &i->n);
    p->size++;
}
