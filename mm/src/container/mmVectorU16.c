/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVectorU16.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

MM_EXPORT_DLL
void
mmVectorU16_Init(
    struct mmVectorU16* p)
{
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorU16_Destroy(
    struct mmVectorU16* p)
{
    mmVectorU16_Free(p);
    //
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorU16_Assign(
    struct mmVectorU16* p,
    const struct mmVectorU16* q)
{
    mmVectorU16_Realloc(p, q->capacity);
    p->size = q->size;
    mmMemcpy(p->arrays, q->arrays, sizeof(mmUInt16_t) * p->size);
}

MM_EXPORT_DLL
void
mmVectorU16_Clear(
    struct mmVectorU16* p)
{
    p->size = 0;
}

MM_EXPORT_DLL
void
mmVectorU16_Reset(
    struct mmVectorU16* p)
{
    mmVectorU16_Free(p);
}

MM_EXPORT_DLL
void
mmVectorU16_SetIndex(
    struct mmVectorU16* p,
    size_t index,
    mmUInt16_t e)
{
    assert(0 <= index && index < p->size);
    p->arrays[index] = e;
}

MM_EXPORT_DLL
mmUInt16_t
mmVectorU16_GetIndex(
    const struct mmVectorU16* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
mmUInt16_t*
mmVectorU16_GetIndexReference(
    const struct mmVectorU16* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return &p->arrays[index];
}

MM_EXPORT_DLL
void
mmVectorU16_Realloc(
    struct mmVectorU16* p,
    size_t capacity)
{
    if (p->capacity < capacity)
    {
        mmUInt16_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        arrays = (mmUInt16_t*)mmMalloc(sizeof(mmUInt16_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmUInt16_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(mmUInt16_t) * p->capacity, 0, sizeof(mmUInt16_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    else if (capacity < (p->capacity / 4) && 8 < p->capacity)
    {
        mmUInt16_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        mem_size <<= 1;
        arrays = (mmUInt16_t*)mmMalloc(sizeof(mmUInt16_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmUInt16_t) * mem_size);
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorU16_Free(
    struct mmVectorU16* p)
{
    if (0 < p->capacity && NULL != p->arrays)
    {
        mmFree(p->arrays);
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
    else
    {
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
}

MM_EXPORT_DLL
void
mmVectorU16_Resize(
    struct mmVectorU16* p,
    size_t size)
{
    mmVectorU16_Realloc(p, size);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorU16_Reserve(
    struct mmVectorU16* p,
    size_t capacity)
{
    mmVectorU16_Realloc(p, capacity);
}

MM_EXPORT_DLL
void
mmVectorU16_AlignedMemory(
    struct mmVectorU16* p,
    size_t size)
{
    size_t sz = size;
    sz = sz < p->capacity ? p->capacity : sz;
    mmVectorU16_Realloc(p, sz);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorU16_AllocatorMemory(
    struct mmVectorU16* p,
    size_t size)
{
    if (p->capacity < size)
    {
        mmUInt16_t* arrays = NULL;
        size_t mem_size = size;
        arrays = (mmUInt16_t*)mmMalloc(sizeof(mmUInt16_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmUInt16_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(mmUInt16_t) * p->capacity, 0, sizeof(mmUInt16_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorU16_PushBack(
    struct mmVectorU16* p,
    mmUInt16_t e)
{
    size_t new_size = p->size + 1;
    if (new_size > p->capacity)
    {
        // vector push back realloc strategy, *= 1.5
        size_t capacity = p->capacity + p->capacity / 2;
        capacity = capacity < new_size ? new_size : capacity;
        mmVectorU16_Realloc(p, capacity);
    }
    p->arrays[p->size++] = e;
}

MM_EXPORT_DLL
mmUInt16_t
mmVectorU16_PopBack(
    struct mmVectorU16* p)
{
    assert(0 < p->size && "mmVectorU16 0 < p->size");
    return p->arrays[--p->size];
}

MM_EXPORT_DLL
mmUInt16_t*
mmVectorU16_Begin(
    const struct mmVectorU16* p)
{
    return &p->arrays[0];
}

MM_EXPORT_DLL
mmUInt16_t*
mmVectorU16_End(
    const struct mmVectorU16* p)
{
    return &p->arrays[p->size];
}

MM_EXPORT_DLL
mmUInt16_t*
mmVectorU16_Back(
    const struct mmVectorU16* p)
{
    assert(0 < p->size && "mmVectorU16 0 < p->size");
    return &p->arrays[p->size - 1];
}

MM_EXPORT_DLL
mmUInt16_t*
mmVectorU16_Next(
    const struct mmVectorU16* p,
    mmUInt16_t* iter)
{
    return iter + 1;
}

MM_EXPORT_DLL
mmUInt16_t
mmVectorU16_At(
    const struct mmVectorU16* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
size_t
mmVectorU16_Size(
    const struct mmVectorU16* p)
{
    return p->size;
}

MM_EXPORT_DLL
size_t
mmVectorU16_Capacity(
    const struct mmVectorU16* p)
{
    return p->capacity;
}

MM_EXPORT_DLL
void
mmVectorU16_Remove(
    struct mmVectorU16* p,
    size_t index)
{
    assert(0 < p->size && index <= p->size - 1 && "mmVectorU16 0 < p->size && index <= p->size - 1");
    if (index < p->size - 1)
    {
        mmMemmove(p->arrays + index, p->arrays + (index + 1), (p->size - index - 1) * sizeof(mmUInt16_t));
        p->size--;
    }
    else
    {
        p->size--;
    }
}

MM_EXPORT_DLL
mmUInt16_t*
mmVectorU16_Erase(
    struct mmVectorU16* p,
    mmUInt16_t* iter)
{
    mmUInt16_t* last = &p->arrays[p->size - 1];
    assert(0 < p->size && (&p->arrays[0]) <= iter && iter <= last && "0 < p->size && first <= iter && iter <= last");
    if (iter < last)
    {
        mmMemmove(iter, iter + 1, (mmUInt8_t*)last - (mmUInt8_t*)iter);
        p->size--;
    }
    else
    {
        p->size--;
    }
    return (iter + 1);
}

// 0 is same.
MM_EXPORT_DLL
int
mmVectorU16_Compare(
    const struct mmVectorU16* p,
    const struct mmVectorU16* q)
{
    if (p->size == q->size)
    {
        return (int)mmMemcmp(p->arrays, q->arrays, sizeof(mmUInt16_t) * p->size);
    }
    else
    {
        return (int)(p->size - q->size);
    }
}

MM_EXPORT_DLL
void
mmVectorU16_CopyFromValue(
    struct mmVectorU16* p,
    const struct mmVectorU16* q)
{
    mmVectorU16_Reset(p);
    mmVectorU16_AllocatorMemory(p, q->size);
    mmMemcpy(p->arrays, q->arrays, sizeof(mmUInt16_t) * p->size);
}