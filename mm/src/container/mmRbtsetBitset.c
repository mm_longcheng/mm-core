/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtsetBitset.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
mmSInt32_t
mmRbtsetBitsetCompareFunc(
    const struct mmBitset* lhs,
    const struct mmBitset* rhs)
{
    return (mmSInt32_t)mmBitset_Compare(lhs, rhs);
}

MM_EXPORT_DLL
void
mmRbtsetBitsetIterator_Init(
    struct mmRbtsetBitsetIterator* p)
{
    mmBitset_Init(&p->k);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetBitsetIterator_Destroy(
    struct mmRbtsetBitsetIterator* p)
{
    mmBitset_Destroy(&p->k);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetBitset_Init(
    struct mmRbtsetBitset* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtsetBitset_Destroy(
    struct mmRbtsetBitset* p)
{
    mmRbtsetBitset_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtsetBitsetIterator*
mmRbtsetBitset_Add(
    struct mmRbtsetBitset* p,
    const struct mmBitset* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetBitsetIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetBitsetIterator, n);
        result = mmRbtsetBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtsetBitsetIterator*)mmMalloc(sizeof(struct mmRbtsetBitsetIterator));
    mmRbtsetBitsetIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtsetBitset_Rmv(
    struct mmRbtsetBitset* p,
    const struct mmBitset* k)
{
    struct mmRbtsetBitsetIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetBitsetIterator, n);
        result = mmRbtsetBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtsetBitset_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtsetBitset_Erase(
    struct mmRbtsetBitset* p,
    struct mmRbtsetBitsetIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtsetBitsetIterator_Destroy(it);
    mmFree(it);
    p->size--;
}
// return 1 if have member.0 is not member.
MM_EXPORT_DLL
int
mmRbtsetBitset_Get(
    const struct mmRbtsetBitset* p,
    const struct mmBitset* k)
{
    struct mmRbtsetBitsetIterator* it = mmRbtsetBitset_GetIterator(p, k);
    return (NULL != it) ? 1 : 0;
}

MM_EXPORT_DLL
struct mmRbtsetBitsetIterator*
mmRbtsetBitset_GetIterator(
    const struct mmRbtsetBitset* p,
    const struct mmBitset* k)
{
    struct mmRbtsetBitsetIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetBitsetIterator, n);
        result = mmRbtsetBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtsetBitset_Clear(
    struct mmRbtsetBitset* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetBitsetIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetBitsetIterator, n);
        n = mmRb_Next(n);
        mmRbtsetBitset_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtsetBitset_Size(
    const struct mmRbtsetBitset* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtsetBitsetIterator*
mmRbtsetBitset_Insert(
    struct mmRbtsetBitset* p,
    const struct mmBitset* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetBitsetIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetBitsetIterator, n);
        result = mmRbtsetBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtsetBitsetIterator*)mmMalloc(sizeof(struct mmRbtsetBitsetIterator));
    mmRbtsetBitsetIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}
