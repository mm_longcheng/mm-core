/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVectorVpt.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

MM_EXPORT_DLL
void
mmVectorVpt_Init(
    struct mmVectorVpt* p)
{
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorVpt_Destroy(
    struct mmVectorVpt* p)
{
    mmVectorVpt_Free(p);
    //
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorVpt_Assign(
    struct mmVectorVpt* p,
    const struct mmVectorVpt* q)
{
    mmVectorVpt_Realloc(p, q->capacity);
    p->size = q->size;
    mmMemcpy(p->arrays, q->arrays, sizeof(void*) * p->size);
}

MM_EXPORT_DLL
void
mmVectorVpt_Clear(
    struct mmVectorVpt* p)
{
    p->size = 0;
}

MM_EXPORT_DLL
void
mmVectorVpt_Reset(
    struct mmVectorVpt* p)
{
    mmVectorVpt_Free(p);
}

MM_EXPORT_DLL
void
mmVectorVpt_SetIndex(
    struct mmVectorVpt* p,
    size_t index,
    void* e)
{
    assert(0 <= index && index < p->size);
    p->arrays[index] = e;
}

MM_EXPORT_DLL
void*
mmVectorVpt_GetIndex(
    const struct mmVectorVpt* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
void*
mmVectorVpt_GetIndexReference(
    const struct mmVectorVpt* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
void
mmVectorVpt_Realloc(
    struct mmVectorVpt* p,
    size_t capacity)
{
    if (p->capacity < capacity)
    {
        void** arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        arrays = (void**)mmMalloc(sizeof(void*) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(void*) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(void*) * p->capacity, 0, sizeof(void*) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = (size_t)mem_size;
    }
    else if (capacity < (p->capacity / 4) && 8 < p->capacity)
    {
        void** arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        mem_size <<= 1;
        arrays = (void**)mmMalloc(sizeof(void*) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(void*) * mem_size);
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorVpt_Free(
    struct mmVectorVpt* p)
{
    if (0 < p->capacity && NULL != p->arrays)
    {
        mmFree(p->arrays);
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
    else
    {
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
}

MM_EXPORT_DLL
void
mmVectorVpt_Resize(
    struct mmVectorVpt* p,
    size_t size)
{
    mmVectorVpt_Realloc(p, size);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorVpt_Reserve(
    struct mmVectorVpt* p,
    size_t capacity)
{
    mmVectorVpt_Realloc(p, capacity);
}

MM_EXPORT_DLL
void
mmVectorVpt_AlignedMemory(
    struct mmVectorVpt* p,
    size_t size)
{
    size_t sz = size;
    sz = sz < p->capacity ? p->capacity : sz;
    mmVectorVpt_Realloc(p, sz);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorVpt_AllocatorMemory(
    struct mmVectorVpt* p,
    size_t size)
{
    if (p->capacity < size)
    {
        void** arrays = NULL;
        size_t mem_size = size;
        arrays = (void**)mmMalloc(sizeof(void*) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(void*) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(void*) * p->capacity, 0, sizeof(void*) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = (size_t)mem_size;
    }
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorVpt_PushBack(
    struct mmVectorVpt* p,
    void* e)
{
    size_t new_size = p->size + 1;
    if (new_size > p->capacity)
    {
        // vector push back realloc strategy, *= 1.5
        size_t capacity = p->capacity + p->capacity / 2;
        capacity = capacity < new_size ? new_size : capacity;
        mmVectorVpt_Realloc(p, capacity);
    }
    p->arrays[p->size++] = e;
}

MM_EXPORT_DLL
void*
mmVectorVpt_PopBack(
    struct mmVectorVpt* p)
{
    assert(0 < p->size && "mmVectorVpt 0 < p->size");
    return p->arrays[--p->size];
}

MM_EXPORT_DLL
void**
mmVectorVpt_Begin(
    const struct mmVectorVpt* p)
{
    return &p->arrays[0];
}

MM_EXPORT_DLL
void**
mmVectorVpt_End(
    const struct mmVectorVpt* p)
{
    return &p->arrays[p->size];
}

MM_EXPORT_DLL
void**
mmVectorVpt_Back(
    const struct mmVectorVpt* p)
{
    assert(0 < p->size && "mmVectorVpt 0 < p->size");
    return &p->arrays[p->size - 1];
}

MM_EXPORT_DLL
void**
mmVectorVpt_Next(
    const struct mmVectorVpt* p,
    void** iter)
{
    return iter + 1;
}

MM_EXPORT_DLL
void*
mmVectorVpt_At(
    const struct mmVectorVpt* p,
    size_t index)
{
    return mmVectorVpt_GetIndex(p, index);
}

MM_EXPORT_DLL
size_t
mmVectorVpt_Size(
    const struct mmVectorVpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
size_t
mmVectorVpt_Capacity(
    const struct mmVectorVpt* p)
{
    return p->capacity;
}

MM_EXPORT_DLL
void
mmVectorVpt_Remove(
    struct mmVectorVpt* p,
    size_t index)
{
    assert(0 < p->size && index <= p->size - 1 && "mmVectorVpt 0 < p->size && index <= p->size - 1");
    if (index < p->size - 1)
    {
        mmMemmove(p->arrays + index, p->arrays + (index + 1), (p->size - index - 1) * sizeof(void*));
        p->size--;
    }
    else
    {
        p->size--;
    }
}

MM_EXPORT_DLL
void**
mmVectorVpt_Erase(
    struct mmVectorVpt* p,
    void** iter)
{
    void** last = &p->arrays[p->size - 1];
    assert(0 < p->size && (&p->arrays[0]) <= iter && iter <= last && "0 < p->size && first <= iter && iter <= last");
    if (iter < last)
    {
        mmMemmove(iter, (mmUInt8_t*)iter + sizeof(mmUInt32_t), (mmUInt8_t*)last - (mmUInt8_t*)iter);
        p->size--;
    }
    else
    {
        p->size--;
    }
    return (iter + 1);
}

MM_EXPORT_DLL
int
mmVectorVpt_Compare(
    const struct mmVectorVpt* p,
    const struct mmVectorVpt* q)
{
    if (p->size == q->size)
    {
        return (int)mmMemcmp(p->arrays, q->arrays, sizeof(void*) * p->size);
    }
    else
    {
        return (int)(p->size - q->size);
    }
}

MM_EXPORT_DLL
void
mmVectorVpt_CopyFromValue(
    struct mmVectorVpt* p,
    const struct mmVectorVpt* q)
{
    mmVectorVpt_Reset(p);
    mmVectorVpt_AllocatorMemory(p, q->size);
    mmMemcpy(p->arrays, q->arrays, sizeof(void*) * p->size);
}