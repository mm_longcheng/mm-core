/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mm_rbtree_u64_h__
#define __mm_rbtree_u64_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
mmSInt64_t 
mmRbtreeU64CompareFunc(
    mmUInt64_t lhs, 
    mmUInt64_t rhs)
{
    return lhs - rhs;
}

// mmRbtreeU64U32
struct mmRbtreeU64U32Iterator
{
    mmUInt64_t k;
    mmUInt32_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64U32Iterator_Init(
    struct mmRbtreeU64U32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64U32Iterator_Destroy(
    struct mmRbtreeU64U32Iterator* p);

struct mmRbtreeU64U32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64U32_Init(
    struct mmRbtreeU64U32* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64U32_Destroy(
    struct mmRbtreeU64U32* p);

MM_EXPORT_DLL 
struct mmRbtreeU64U32Iterator* 
mmRbtreeU64U32_Add(
    struct mmRbtreeU64U32* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64U32_Rmv(
    struct mmRbtreeU64U32* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64U32_Erase(
    struct mmRbtreeU64U32* p, 
    struct mmRbtreeU64U32Iterator* it);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeU64U32_GetInstance(
    struct mmRbtreeU64U32* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeU64U32_Get(
    const struct mmRbtreeU64U32* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
struct mmRbtreeU64U32Iterator* 
mmRbtreeU64U32_GetIterator(
    const struct mmRbtreeU64U32* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64U32_Clear(
    struct mmRbtreeU64U32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU64U32_Size(
    const struct mmRbtreeU64U32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU64U32Iterator* 
mmRbtreeU64U32_Set(
    struct mmRbtreeU64U32* p, 
    mmUInt64_t k, 
    mmUInt32_t v);

MM_EXPORT_DLL 
struct mmRbtreeU64U32Iterator* 
mmRbtreeU64U32_Insert(
    struct mmRbtreeU64U32* p, 
    mmUInt64_t k, 
    mmUInt32_t v);

// mmRbtreeU64U64
struct mmRbtreeU64U64Iterator
{
    mmUInt64_t k;
    mmUInt64_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64U64Iterator_Init(
    struct mmRbtreeU64U64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64U64Iterator_Destroy(
    struct mmRbtreeU64U64Iterator* p);

struct mmRbtreeU64U64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64U64_Init(
    struct mmRbtreeU64U64* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64U64_Destroy(
    struct mmRbtreeU64U64* p);

MM_EXPORT_DLL 
struct mmRbtreeU64U64Iterator* 
mmRbtreeU64U64_Add(
    struct mmRbtreeU64U64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64U64_Rmv(
    struct mmRbtreeU64U64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64U64_Erase(
    struct mmRbtreeU64U64* p, 
    struct mmRbtreeU64U64Iterator* it);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeU64U64_GetInstance(
    struct mmRbtreeU64U64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeU64U64_Get(
    const struct mmRbtreeU64U64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
struct mmRbtreeU64U64Iterator* 
mmRbtreeU64U64_GetIterator(
    const struct mmRbtreeU64U64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64U64_Clear(
    struct mmRbtreeU64U64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU64U64_Size(
    const struct mmRbtreeU64U64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU64U64Iterator* 
mmRbtreeU64U64_Set(
    struct mmRbtreeU64U64* p, 
    mmUInt64_t k, 
    mmUInt64_t v);

MM_EXPORT_DLL 
struct mmRbtreeU64U64Iterator* 
mmRbtreeU64U64_Insert(
    struct mmRbtreeU64U64* p, 
    mmUInt64_t k, 
    mmUInt64_t v);

// mmRbtreeU64String
struct mmRbtreeU64StringIterator
{
    mmUInt64_t k;
    struct mmString v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64StringIterator_Init(
    struct mmRbtreeU64StringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64StringIterator_Destroy(
    struct mmRbtreeU64StringIterator* p);

struct mmRbtreeU64String
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64String_Init(
    struct mmRbtreeU64String* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64String_Destroy(
    struct mmRbtreeU64String* p);

MM_EXPORT_DLL 
struct mmRbtreeU64StringIterator* 
mmRbtreeU64String_Add(
    struct mmRbtreeU64String* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64String_Rmv(
    struct mmRbtreeU64String* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64String_Erase(
    struct mmRbtreeU64String* p, 
    struct mmRbtreeU64StringIterator* it);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeU64String_GetInstance(
    struct mmRbtreeU64String* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeU64String_Get(
    const struct mmRbtreeU64String* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
struct mmRbtreeU64StringIterator* 
mmRbtreeU64String_GetIterator(
    const struct mmRbtreeU64String* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64String_Clear(
    struct mmRbtreeU64String* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU64String_Size(
    const struct mmRbtreeU64String* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU64StringIterator* 
mmRbtreeU64String_Set(
    struct mmRbtreeU64String* p, 
    mmUInt64_t k, 
    struct mmString* v);

MM_EXPORT_DLL 
struct mmRbtreeU64StringIterator* 
mmRbtreeU64String_Insert(
    struct mmRbtreeU64String* p, 
    mmUInt64_t k, 
    struct mmString* v);

// mmRbtreeU64Vpt
struct mmRbtreeU64VptIterator
{
    mmUInt64_t k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64VptIterator_Init(
    struct mmRbtreeU64VptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64VptIterator_Destroy(
    struct mmRbtreeU64VptIterator* p);

struct mmRbtreeU64Vpt;

struct mmRbtreeU64VptAllocator
{
    void* (*Produce)(struct mmRbtreeU64Vpt* p, mmUInt64_t k);
    void* (*Recycle)(struct mmRbtreeU64Vpt* p, mmUInt64_t k, void* v);
};

MM_EXPORT_DLL 
void 
mmRbtreeU64VptAllocator_Init(
    struct mmRbtreeU64VptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64VptAllocator_Destroy(
    struct mmRbtreeU64VptAllocator* p);

struct mmRbtreeU64Vpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeU64VptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU64Vpt_Init(
    struct mmRbtreeU64Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64Vpt_Destroy(
    struct mmRbtreeU64Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeU64Vpt_SetAllocator(
    struct mmRbtreeU64Vpt* p, 
    struct mmRbtreeU64VptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeU64VptIterator* 
mmRbtreeU64Vpt_Add(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64Vpt_Rmv(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64Vpt_Erase(
    struct mmRbtreeU64Vpt* p, 
    struct mmRbtreeU64VptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeU64Vpt_GetInstance(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeU64Vpt_Get(
    const struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
struct mmRbtreeU64VptIterator* 
mmRbtreeU64Vpt_GetIterator(
    const struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU64Vpt_Clear(
    struct mmRbtreeU64Vpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU64Vpt_Size(
    const struct mmRbtreeU64Vpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU64VptIterator* 
mmRbtreeU64Vpt_Set(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeU64VptIterator* 
mmRbtreeU64Vpt_Insert(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeU64Vpt_WeakProduce(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeU64Vpt_WeakRecycle(
    struct mmRbtreeU64Vpt* p, 
    mmUInt64_t k, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mm_rbtree_u64_h__
