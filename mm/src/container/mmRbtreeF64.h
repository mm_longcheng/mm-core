/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeF64_h__
#define __mmRbtreeF64_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
mmFloat64_t 
mmRbtreeF64CompareFunc(
    mmFloat64_t lhs, 
    mmFloat64_t rhs)
{
    return lhs - rhs;
}

// mmRbtreeF64U32
struct mmRbtreeF64U32Iterator
{
    mmFloat64_t k;
    mmUInt32_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64U32Iterator_Init(
    struct mmRbtreeF64U32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64U32Iterator_Destroy(
    struct mmRbtreeF64U32Iterator* p);

struct mmRbtreeF64U32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64U32_Init(
    struct mmRbtreeF64U32* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64U32_Destroy(
    struct mmRbtreeF64U32* p);

MM_EXPORT_DLL 
struct mmRbtreeF64U32Iterator* 
mmRbtreeF64U32_Add(
    struct mmRbtreeF64U32* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64U32_Rmv(
    struct mmRbtreeF64U32* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64U32_Erase(
    struct mmRbtreeF64U32* p, 
    struct mmRbtreeF64U32Iterator* it);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeF64U32_GetInstance(
    struct mmRbtreeF64U32* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeF64U32_Get(
    const struct mmRbtreeF64U32* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
struct mmRbtreeF64U32Iterator* 
mmRbtreeF64U32_GetIterator(
    const struct mmRbtreeF64U32* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64U32_Clear(
    struct mmRbtreeF64U32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF64U32_Size(
    const struct mmRbtreeF64U32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF64U32Iterator* 
mmRbtreeF64U32_Set(
    struct mmRbtreeF64U32* p, 
    mmFloat64_t k, 
    mmUInt32_t v);

MM_EXPORT_DLL 
struct mmRbtreeF64U32Iterator* 
mmRbtreeF64U32_Insert(
    struct mmRbtreeF64U32* p, 
    mmFloat64_t k, 
    mmUInt32_t v);

// mmRbtreeF64U64
struct mmRbtreeF64U64Iterator
{
    mmFloat64_t k;
    mmUInt64_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64U64Iterator_Init(
    struct mmRbtreeF64U64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64U64Iterator_Destroy(
    struct mmRbtreeF64U64Iterator* p);

struct mmRbtreeF64U64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64U64_Init(
    struct mmRbtreeF64U64* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64U64_Destroy(
    struct mmRbtreeF64U64* p);

MM_EXPORT_DLL 
struct mmRbtreeF64U64Iterator* 
mmRbtreeF64U64_Add(
    struct mmRbtreeF64U64* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64U64_Rmv(
    struct mmRbtreeF64U64* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void
mmRbtreeF64U64_Erase(
    struct mmRbtreeF64U64* p, 
    struct mmRbtreeF64U64Iterator* it);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeF64U64_GetInstance(
    struct mmRbtreeF64U64* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeF64U64_Get(
    const struct mmRbtreeF64U64* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
struct mmRbtreeF64U64Iterator* 
mmRbtreeF64U64_GetIterator(
    const struct mmRbtreeF64U64* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64U64_Clear(
    struct mmRbtreeF64U64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF64U64_Size(
    const struct mmRbtreeF64U64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF64U64Iterator* 
mmRbtreeF64U64_Set(
    struct mmRbtreeF64U64* p, 
    mmFloat64_t k, 
    mmUInt64_t v);

MM_EXPORT_DLL 
struct mmRbtreeF64U64Iterator* 
mmRbtreeF64U64_Insert(
    struct mmRbtreeF64U64* p, 
    mmFloat64_t k, 
    mmUInt64_t v);

// mmRbtreeF64String
struct mmRbtreeF64StringIterator
{
    mmFloat64_t k;
    struct mmString v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64StringIterator_Init(
    struct mmRbtreeF64StringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64StringIterator_Destroy(
    struct mmRbtreeF64StringIterator* p);

struct mmRbtreeF64String
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64String_Init(
    struct mmRbtreeF64String* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64String_Destroy(
    struct mmRbtreeF64String* p);

MM_EXPORT_DLL 
struct mmRbtreeF64StringIterator* 
mmRbtreeF64String_Add(
    struct mmRbtreeF64String* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64String_Rmv(
    struct mmRbtreeF64String* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64String_Erase(
    struct mmRbtreeF64String* p, 
    struct mmRbtreeF64StringIterator* it);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeF64String_GetInstance(
    struct mmRbtreeF64String* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeF64String_Get(
    const struct mmRbtreeF64String* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
struct mmRbtreeF64StringIterator* 
mmRbtreeF64String_GetIterator(
    const struct mmRbtreeF64String* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64String_Clear(
    struct mmRbtreeF64String* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF64String_Size(
    const struct mmRbtreeF64String* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF64StringIterator* 
mmRbtreeF64String_Set(
    struct mmRbtreeF64String* p, 
    mmFloat64_t k, 
    struct mmString* v);

MM_EXPORT_DLL 
struct mmRbtreeF64StringIterator* 
mmRbtreeF64String_Insert(
    struct mmRbtreeF64String* p, 
    mmFloat64_t k, 
    struct mmString* v);

// mmRbtreeF64Vpt
struct mmRbtreeF64VptIterator
{
    mmFloat64_t k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64VptIterator_Init(
    struct mmRbtreeF64VptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64VptIterator_Destroy(
    struct mmRbtreeF64VptIterator* p);

struct mmRbtreeF64Vpt;

struct mmRbtreeF64VptAllocator
{
    void* (*Produce)(struct mmRbtreeF64Vpt* p, mmFloat64_t k);
    void* (*Recycle)(struct mmRbtreeF64Vpt* p, mmFloat64_t k, void* v);
};

MM_EXPORT_DLL 
void 
mmRbtreeF64VptAllocator_Init(
    struct mmRbtreeF64VptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64VptAllocator_Destroy(
    struct mmRbtreeF64VptAllocator* p);

struct mmRbtreeF64Vpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeF64VptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF64Vpt_Init(
    struct mmRbtreeF64Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64Vpt_Destroy(
    struct mmRbtreeF64Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeF64Vpt_SetAllocator(
    struct mmRbtreeF64Vpt* p, 
    struct mmRbtreeF64VptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeF64VptIterator* 
mmRbtreeF64Vpt_Add(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64Vpt_Rmv(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64Vpt_Erase(
    struct mmRbtreeF64Vpt* p, 
    struct mmRbtreeF64VptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeF64Vpt_GetInstance(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeF64Vpt_Get(
    const struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
struct mmRbtreeF64VptIterator* 
mmRbtreeF64Vpt_GetIterator(
    const struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF64Vpt_Clear(
    struct mmRbtreeF64Vpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF64Vpt_Size(
    const struct mmRbtreeF64Vpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF64VptIterator* 
mmRbtreeF64Vpt_Set(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeF64VptIterator* 
mmRbtreeF64Vpt_Insert(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeF64Vpt_WeakProduce(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeF64Vpt_WeakRecycle(
    struct mmRbtreeF64Vpt* p, 
    mmFloat64_t k, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeF64_h__
