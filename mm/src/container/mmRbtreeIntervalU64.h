/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeIntervalU64_h__
#define __mmRbtreeIntervalU64_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
mmSInt64_t 
mmRbtreeIntervalU64CompareFunc(
    mmUInt64_t ll, mmUInt64_t lr, 
    mmUInt64_t rl, mmUInt64_t rr);

// mmRbtreeIntervalU64
struct mmRbtreeIntervalU64Iterator
{
    mmUInt64_t l;
    mmUInt64_t r;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64Iterator_Init(
    struct mmRbtreeIntervalU64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64Iterator_Destroy(
    struct mmRbtreeIntervalU64Iterator* p);

struct mmRbtreeIntervalU64;

struct mmRbtreeIntervalU64Allocator
{
    void* (*Produce)(struct mmRbtreeIntervalU64* p, mmUInt64_t l, mmUInt64_t r);
    void* (*Recycle)(struct mmRbtreeIntervalU64* p, mmUInt64_t l, mmUInt64_t r, void* v);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64Allocator_Init(
    struct mmRbtreeIntervalU64Allocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64Allocator_Destroy(
    struct mmRbtreeIntervalU64Allocator* p);

struct mmRbtreeIntervalU64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeIntervalU64Allocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64_Init(
    struct mmRbtreeIntervalU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64_Destroy(
    struct mmRbtreeIntervalU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64_SetAllocator(
    struct mmRbtreeIntervalU64* p, 
    struct mmRbtreeIntervalU64Allocator* allocator);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU64_Add(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64_Rmv(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64_Erase(
    struct mmRbtreeIntervalU64* p, 
    struct mmRbtreeIntervalU64Iterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU64_GetInstance(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU64_Get(
    const struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r);

MM_EXPORT_DLL 
struct mmRbtreeIntervalU64Iterator* 
mmRbtreeIntervalU64_GetIterator(
    const struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU64_Clear(
    struct mmRbtreeIntervalU64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeIntervalU64_Size(
    const struct mmRbtreeIntervalU64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeIntervalU64Iterator*
mmRbtreeIntervalU64_Set(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeIntervalU64Iterator*
mmRbtreeIntervalU64_Insert(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU64_WeakProduce(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU64_WeakRecycle(
    struct mmRbtreeIntervalU64* p, 
    mmUInt64_t l, mmUInt64_t r, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeIntervalU64_h__
