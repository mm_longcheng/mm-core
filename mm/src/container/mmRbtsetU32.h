/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtsetU32_h__
#define __mmRbtsetU32_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
mmSInt32_t 
mmRbtsetU32CompareFunc(
    mmUInt32_t lhs, 
    mmUInt32_t rhs)
{
    return lhs - rhs;
}

// mmRbtsetU32
struct mmRbtsetU32Iterator
{
    mmUInt32_t k;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtsetU32Iterator_Init(
    struct mmRbtsetU32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtsetU32Iterator_Destroy(
    struct mmRbtsetU32Iterator* p);

struct mmRbtsetU32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtsetU32_Init(
    struct mmRbtsetU32* p);

MM_EXPORT_DLL 
void 
mmRbtsetU32_Destroy(
    struct mmRbtsetU32* p);

MM_EXPORT_DLL 
struct mmRbtsetU32Iterator* 
mmRbtsetU32_Add(
    struct mmRbtsetU32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtsetU32_Rmv(
    struct mmRbtsetU32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtsetU32_Erase(
    struct mmRbtsetU32* p, 
    struct mmRbtsetU32Iterator* it);

// return 1 if have member.0 is not member.
MM_EXPORT_DLL 
int 
mmRbtsetU32_Get(
    const struct mmRbtsetU32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
struct mmRbtsetU32Iterator* 
mmRbtsetU32_GetIterator(
    const struct mmRbtsetU32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtsetU32_Clear(
    struct mmRbtsetU32* p);

MM_EXPORT_DLL 
size_t 
mmRbtsetU32_Size(
    const struct mmRbtsetU32* p);

MM_EXPORT_DLL 
struct mmRbtsetU32Iterator* 
mmRbtsetU32_Insert(
    struct mmRbtsetU32* p, 
    mmUInt32_t k);

#include "core/mmSuffix.h"

#endif//__mmRbtsetU32_h__
