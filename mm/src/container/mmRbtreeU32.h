/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeU32_h__
#define __mmRbtreeU32_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
mmSInt32_t 
mmRbtreeU32CompareFunc(
    mmUInt32_t lhs, 
    mmUInt32_t rhs)
{
    return lhs - rhs;
}

// mmRbtreeU32U32
struct mmRbtreeU32U32Iterator
{
    mmUInt32_t k;
    mmUInt32_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32U32Iterator_Init(
    struct mmRbtreeU32U32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32U32Iterator_Destroy(
    struct mmRbtreeU32U32Iterator* p);

struct mmRbtreeU32U32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32U32_Init(
    struct mmRbtreeU32U32* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32U32_Destroy(
    struct mmRbtreeU32U32* p);

MM_EXPORT_DLL 
struct mmRbtreeU32U32Iterator* 
mmRbtreeU32U32_Add(
    struct mmRbtreeU32U32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32U32_Rmv(
    struct mmRbtreeU32U32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32U32_Erase(
    struct mmRbtreeU32U32* p, 
    struct mmRbtreeU32U32Iterator* it);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeU32U32_GetInstance(
    struct mmRbtreeU32U32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeU32U32_Get(
    const struct mmRbtreeU32U32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
struct mmRbtreeU32U32Iterator* 
mmRbtreeU32U32_GetIterator(
    const struct mmRbtreeU32U32* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32U32_Clear(
    struct mmRbtreeU32U32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU32U32_Size(
    const struct mmRbtreeU32U32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU32U32Iterator* 
mmRbtreeU32U32_Set(
    struct mmRbtreeU32U32* p, 
    mmUInt32_t k, 
    mmUInt32_t v);

MM_EXPORT_DLL 
struct mmRbtreeU32U32Iterator* 
mmRbtreeU32U32_Insert(
    struct mmRbtreeU32U32* p, 
    mmUInt32_t k, 
    mmUInt32_t v);

// mmRbtreeU32U64
struct mmRbtreeU32U64Iterator
{
    mmUInt32_t k;
    mmUInt64_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32U64Iterator_Init(
    struct mmRbtreeU32U64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32U64Iterator_Destroy(
    struct mmRbtreeU32U64Iterator* p);

struct mmRbtreeU32U64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32U64_Init(
    struct mmRbtreeU32U64* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32U64_Destroy(
    struct mmRbtreeU32U64* p);

MM_EXPORT_DLL 
struct mmRbtreeU32U64Iterator* 
mmRbtreeU32U64_Add(
    struct mmRbtreeU32U64* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32U64_Rmv(
    struct mmRbtreeU32U64* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32U64_Erase(
    struct mmRbtreeU32U64* p, 
    struct mmRbtreeU32U64Iterator* it);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeU32U64_GetInstance(
    struct mmRbtreeU32U64* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeU32U64_Get(
    const struct mmRbtreeU32U64* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
struct mmRbtreeU32U64Iterator* 
mmRbtreeU32U64_GetIterator(
    const struct mmRbtreeU32U64* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32U64_Clear(
    struct mmRbtreeU32U64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU32U64_Size(
    const struct mmRbtreeU32U64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU32U64Iterator* 
mmRbtreeU32U64_Set(
    struct mmRbtreeU32U64* p, 
    mmUInt32_t k, 
    mmUInt64_t v);

MM_EXPORT_DLL 
struct mmRbtreeU32U64Iterator* 
mmRbtreeU32U64_Insert(
    struct mmRbtreeU32U64* p, 
    mmUInt32_t k, 
    mmUInt64_t v);

// mmRbtreeU32String
struct mmRbtreeU32StringIterator
{
    mmUInt32_t k;
    struct mmString v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32StringIterator_Init(
    struct mmRbtreeU32StringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32StringIterator_Destroy(
    struct mmRbtreeU32StringIterator* p);

struct mmRbtreeU32String
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32String_Init(
    struct mmRbtreeU32String* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32String_Destroy(
    struct mmRbtreeU32String* p);

MM_EXPORT_DLL 
struct mmRbtreeU32StringIterator* 
mmRbtreeU32String_Add(
    struct mmRbtreeU32String* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32String_Rmv(
    struct mmRbtreeU32String* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32String_Erase(
    struct mmRbtreeU32String* p, 
    struct mmRbtreeU32StringIterator* it);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeU32String_GetInstance(
    struct mmRbtreeU32String* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeU32String_Get(
    const struct mmRbtreeU32String* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
struct mmRbtreeU32StringIterator* 
mmRbtreeU32String_GetIterator(
    const struct mmRbtreeU32String* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32String_Clear(
    struct mmRbtreeU32String* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU32String_Size(
    const struct mmRbtreeU32String* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU32StringIterator* 
mmRbtreeU32String_Set(
    struct mmRbtreeU32String* p, 
    mmUInt32_t k, 
    struct mmString* v);

MM_EXPORT_DLL 
struct mmRbtreeU32StringIterator* 
mmRbtreeU32String_Insert(
    struct mmRbtreeU32String* p, 
    mmUInt32_t k, 
    struct mmString* v);

// mmRbtreeU32Vpt
struct mmRbtreeU32VptIterator
{
    mmUInt32_t k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32VptIterator_Init(
    struct mmRbtreeU32VptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32VptIterator_Destroy(
    struct mmRbtreeU32VptIterator* p);

struct mmRbtreeU32Vpt;

struct mmRbtreeU32VptAllocator
{
    void* (*Produce)(struct mmRbtreeU32Vpt* p, mmUInt32_t k);
    void* (*Recycle)(struct mmRbtreeU32Vpt* p, mmUInt32_t k, void* v);
};

MM_EXPORT_DLL 
void 
mmRbtreeU32VptAllocator_Init(
    struct mmRbtreeU32VptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32VptAllocator_Destroy(
    struct mmRbtreeU32VptAllocator* p);

struct mmRbtreeU32Vpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeU32VptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeU32Vpt_Init(
    struct mmRbtreeU32Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32Vpt_Destroy(
    struct mmRbtreeU32Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeU32Vpt_SetAllocator(
    struct mmRbtreeU32Vpt* p, 
    struct mmRbtreeU32VptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeU32VptIterator* 
mmRbtreeU32Vpt_Add(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32Vpt_Rmv(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32Vpt_Erase(
    struct mmRbtreeU32Vpt* p, 
    struct mmRbtreeU32VptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeU32Vpt_GetInstance(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeU32Vpt_Get(
    const struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
struct mmRbtreeU32VptIterator* 
mmRbtreeU32Vpt_GetIterator(
    const struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeU32Vpt_Clear(
    struct mmRbtreeU32Vpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeU32Vpt_Size(
    const struct mmRbtreeU32Vpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeU32VptIterator* 
mmRbtreeU32Vpt_Set(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeU32VptIterator* 
mmRbtreeU32Vpt_Insert(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeU32Vpt_WeakProduce(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeU32Vpt_WeakRecycle(
    struct mmRbtreeU32Vpt* p, 
    mmUInt32_t k, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeU32_h__
