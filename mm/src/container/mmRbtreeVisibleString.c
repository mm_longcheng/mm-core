/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeVisibleString.h"
#include "core/mmSpinlock.h"

static 
void 
__static_mmRbtreeVisibleString_TryMergeAdd(
    struct mmRbtreeVisibleString* p);

static 
void 
__static_mmRbtreeVisibleString_TryMergeRmv(
    struct mmRbtreeVisibleString* p);

MM_EXPORT_DLL
void
mmRbtreeVisibleString_Init(
    struct mmRbtreeVisibleString* p)
{
    struct mmRbtreeStringVptAllocator _StringVptAllocator;
    //
    mmRbtreeStringVpt_Init(&p->rbtree_m);
    mmRbtsetString_Init(&p->rbtset_s);
    //
    mmListString_Init(&p->l_a);
    mmListString_Init(&p->l_r);
    //
    mmSpinlock_Init(&p->locker_m, NULL);
    mmSpinlock_Init(&p->locker_s, NULL);
    mmSpinlock_Init(&p->locker_a, NULL);
    mmSpinlock_Init(&p->locker_r, NULL);
    //
    _StringVptAllocator.Produce = &mmRbtreeStringVpt_WeakProduce;
    _StringVptAllocator.Recycle = &mmRbtreeStringVpt_WeakRecycle;
    mmRbtreeStringVpt_SetAllocator(&p->rbtree_m, &_StringVptAllocator);
}

MM_EXPORT_DLL
void
mmRbtreeVisibleString_Destroy(
    struct mmRbtreeVisibleString* p)
{
    mmRbtreeVisibleString_Clear(p);
    //
    mmRbtreeStringVpt_Destroy(&p->rbtree_m);
    mmRbtsetString_Destroy(&p->rbtset_s);
    //
    mmListString_Destroy(&p->l_a);
    mmListString_Destroy(&p->l_r);
    //
    mmSpinlock_Destroy(&p->locker_m);
    mmSpinlock_Destroy(&p->locker_s);
    mmSpinlock_Destroy(&p->locker_a);
    mmSpinlock_Destroy(&p->locker_r);
}

MM_EXPORT_DLL
void
mmRbtreeVisibleString_Add(
    struct mmRbtreeVisibleString* p,
    const struct mmString* k, 
    void* v)
{
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeStringVpt_Set(&p->rbtree_m, k, v);
    mmSpinlock_Unlock(&p->locker_m);
    //
    if (mmSpinlock_Trylock(&p->locker_s))
    {
        __static_mmRbtreeVisibleString_TryMergeAdd(p);
        mmRbtsetString_Add(&p->rbtset_s, k);
        mmSpinlock_Unlock(&p->locker_s);
    }
    else
    {
        mmSpinlock_Lock(&p->locker_a);
        mmListString_AddFont(&p->l_a, k);
        mmSpinlock_Unlock(&p->locker_a);
    }
}

MM_EXPORT_DLL
void
mmRbtreeVisibleString_Rmv(
    struct mmRbtreeVisibleString* p,
    const struct mmString* k)
{
    if (mmSpinlock_Trylock(&p->locker_s))
    {
        __static_mmRbtreeVisibleString_TryMergeRmv(p);
        mmRbtsetString_Rmv(&p->rbtset_s, k);
        mmSpinlock_Unlock(&p->locker_s);
    }
    else
    {
        mmSpinlock_Lock(&p->locker_r);
        mmListString_AddFont(&p->l_r, k);
        mmSpinlock_Unlock(&p->locker_r);
    }
    //
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeStringVpt_Rmv(&p->rbtree_m, k);
    mmSpinlock_Unlock(&p->locker_m);
}

MM_EXPORT_DLL
void*
mmRbtreeVisibleString_Get(
    struct mmRbtreeVisibleString* p,
    const struct mmString* k)
{
    void* e = NULL;
    mmSpinlock_Lock(&p->locker_m);
    e = mmRbtreeStringVpt_Get(&p->rbtree_m, k);
    mmSpinlock_Unlock(&p->locker_m);
    return e;
}

MM_EXPORT_DLL
void
mmRbtreeVisibleString_Clear(
    struct mmRbtreeVisibleString* p)
{
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeStringVpt_Clear(&p->rbtree_m);
    mmSpinlock_Unlock(&p->locker_m);
    //
    mmSpinlock_Lock(&p->locker_s);
    mmRbtsetString_Clear(&p->rbtset_s);
    mmSpinlock_Unlock(&p->locker_s);
    //
    mmSpinlock_Lock(&p->locker_a);
    mmListString_Clear(&p->l_a);
    mmSpinlock_Unlock(&p->locker_a);
    //
    mmSpinlock_Lock(&p->locker_r);
    mmListString_Clear(&p->l_r);
    mmSpinlock_Unlock(&p->locker_r);
}

MM_EXPORT_DLL
size_t
mmRbtreeVisibleString_Size(
    const struct mmRbtreeVisibleString* p)
{
    return p->rbtree_m.size;
}

MM_EXPORT_DLL
void
mmRbtreeVisibleString_Traver(
    struct mmRbtreeVisibleString* p,
    mmRbtreeVisibleStringHandleFunc handle,
    void* u)
{
    struct mmString k;
    void* v = NULL;
    
    assert(NULL != handle && "handle is a null.");
    
    // init key.
    mmString_Init(&k);
    mmSpinlock_Lock(&p->locker_s);
    
    __static_mmRbtreeVisibleString_TryMergeAdd(p);
    __static_mmRbtreeVisibleString_TryMergeRmv(p);
    
    if (0 != p->rbtset_s.size)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetStringIterator* it = NULL;
        //
        n = mmRb_First(&p->rbtset_s.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetStringIterator, n);
            n = mmRb_Next(n);
            // copy key.
            mmString_Assign(&k, &it->k);
            v = mmRbtreeVisibleString_Get(p, &k);
            if (NULL != v)
            {
                (*(handle))(p, u, &k, v);
            }
        }
    }
    mmSpinlock_Unlock(&p->locker_s);
    // destroy key.
    mmString_Destroy(&k);
}

static 
void 
__static_mmRbtreeVisibleString_TryMergeAdd(
    struct mmRbtreeVisibleString* p)
{
    if (0 != p->l_a.size)
    {
        if (mmSpinlock_Trylock(&p->locker_a))
        {
            struct mmListHead* pos = NULL;
            struct mmListStringIterator* it = NULL;
            pos = p->l_a.l.next;
            while (pos != &p->l_a.l)
            {
                struct mmListHead* curr = pos;
                pos = pos->next;
                it = mmList_Entry(curr, struct mmListStringIterator, n);
                mmRbtsetString_Add(&p->rbtset_s, &it->v);
                mmListString_Erase(&p->l_a, it);
            }
            mmSpinlock_Unlock(&p->locker_a);
        }
    }
}

static 
void 
__static_mmRbtreeVisibleString_TryMergeRmv(
    struct mmRbtreeVisibleString* p)
{
    if (0 != p->l_r.size)
    {
        if (mmSpinlock_Trylock(&p->locker_r))
        {
            struct mmListHead* pos = NULL;
            struct mmListStringIterator* it = NULL;
            pos = p->l_r.l.next;
            while (pos != &p->l_r.l)
            {
                struct mmListHead* curr = pos;
                pos = pos->next;
                it = mmList_Entry(curr, struct mmListStringIterator, n);
                mmRbtsetString_Rmv(&p->rbtset_s, &it->v);
                mmListString_Erase(&p->l_r, it);
            }
            mmSpinlock_Unlock(&p->locker_r);
        }
    }
}

