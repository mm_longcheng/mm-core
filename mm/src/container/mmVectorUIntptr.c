/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVectorUIntptr.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

MM_EXPORT_DLL
void
mmVectorUIntptr_Init(
    struct mmVectorUIntptr* p)
{
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Destroy(
    struct mmVectorUIntptr* p)
{
    mmVectorUIntptr_Free(p);
    //
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Assign(
    struct mmVectorUIntptr* p,
    const struct mmVectorUIntptr* q)
{
    mmVectorUIntptr_Realloc(p, q->capacity);
    p->size = q->size;
    mmMemcpy(p->arrays, q->arrays, sizeof(uintptr_t) * p->size);
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Clear(
    struct mmVectorUIntptr* p)
{
    p->size = 0;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Reset(
    struct mmVectorUIntptr* p)
{
    mmVectorUIntptr_Free(p);
}

MM_EXPORT_DLL
void
mmVectorUIntptr_SetIndex(
    struct mmVectorUIntptr* p,
    size_t index,
    uintptr_t e)
{
    assert(0 <= index && index < p->size);
    p->arrays[index] = e;
}

MM_EXPORT_DLL
uintptr_t
mmVectorUIntptr_GetIndex(
    const struct mmVectorUIntptr* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
uintptr_t*
mmVectorUIntptr_GetIndexReference(
    const struct mmVectorUIntptr* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return &p->arrays[index];
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Realloc(
    struct mmVectorUIntptr* p,
    size_t capacity)
{
    if (p->capacity < capacity)
    {
        uintptr_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        arrays = (uintptr_t*)mmMalloc(sizeof(uintptr_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(uintptr_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(uintptr_t) * p->capacity, 0, sizeof(uintptr_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    else if (capacity < (p->capacity / 4) && 8 < p->capacity)
    {
        uintptr_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        mem_size <<= 1;
        arrays = (uintptr_t*)mmMalloc(sizeof(uintptr_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(uintptr_t) * mem_size);
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Free(
    struct mmVectorUIntptr* p)
{
    if (0 < p->capacity && NULL != p->arrays)
    {
        mmFree(p->arrays);
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
    else
    {
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Resize(
    struct mmVectorUIntptr* p,
    size_t size)
{
    mmVectorUIntptr_Realloc(p, size);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Reserve(
    struct mmVectorUIntptr* p,
    size_t capacity)
{
    mmVectorUIntptr_Realloc(p, capacity);
}

MM_EXPORT_DLL
void
mmVectorUIntptr_AlignedMemory(
    struct mmVectorUIntptr* p,
    size_t size)
{
    size_t sz = size;
    sz = sz < p->capacity ? p->capacity : sz;
    mmVectorUIntptr_Realloc(p, sz);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_AllocatorMemory(
    struct mmVectorUIntptr* p,
    size_t size)
{
    if (p->capacity < size)
    {
        uintptr_t* arrays = NULL;
        size_t mem_size = size;
        arrays = (uintptr_t*)mmMalloc(sizeof(uintptr_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(uintptr_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(uintptr_t) * p->capacity, 0, sizeof(uintptr_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_PushBack(
    struct mmVectorUIntptr* p,
    uintptr_t e)
{
    size_t new_size = p->size + 1;
    if (new_size > p->capacity)
    {
        // vector push back realloc strategy, *= 1.5
        size_t capacity = p->capacity + p->capacity / 2;
        capacity = capacity < new_size ? new_size : capacity;
        mmVectorUIntptr_Realloc(p, capacity);
    }
    p->arrays[p->size++] = e;
}

MM_EXPORT_DLL
uintptr_t
mmVectorUIntptr_PopBack(
    struct mmVectorUIntptr* p)
{
    assert(0 < p->size && "mmVectorUIntptr 0 < p->size");
    return p->arrays[--p->size];
}

MM_EXPORT_DLL
uintptr_t*
mmVectorUIntptr_Begin(
    const struct mmVectorUIntptr* p)
{
    return &p->arrays[0];
}

MM_EXPORT_DLL
uintptr_t*
mmVectorUIntptr_End(
    const struct mmVectorUIntptr* p)
{
    return &p->arrays[p->size];
}

MM_EXPORT_DLL
uintptr_t*
mmVectorUIntptr_Back(
    const struct mmVectorUIntptr* p)
{
    assert(0 < p->size && "mmVectorUIntptr 0 < p->size");
    return &p->arrays[p->size - 1];
}

MM_EXPORT_DLL
uintptr_t*
mmVectorUIntptr_Next(
    const struct mmVectorUIntptr* p,
    uintptr_t* iter)
{
    return iter + 1;
}

MM_EXPORT_DLL
uintptr_t
mmVectorUIntptr_At(
    const struct mmVectorUIntptr* p,
    size_t index)
{
    return mmVectorUIntptr_GetIndex(p, index);
}

MM_EXPORT_DLL
size_t
mmVectorUIntptr_Size(
    const struct mmVectorUIntptr* p)
{
    return p->size;
}

MM_EXPORT_DLL
size_t
mmVectorUIntptr_Capacity(
    const struct mmVectorUIntptr* p)
{
    return p->capacity;
}

MM_EXPORT_DLL
void
mmVectorUIntptr_Remove(
    struct mmVectorUIntptr* p,
    size_t index)
{
    assert(0 < p->size && index <= p->size - 1 && "mmVectorUIntptr 0 < p->size && index <= p->size - 1");
    if (index < p->size - 1)
    {
        mmMemmove(p->arrays + index, p->arrays + (index + 1), (p->size - index - 1) * sizeof(uintptr_t));
        p->size--;
    }
    else
    {
        p->size--;
    }
}

MM_EXPORT_DLL
uintptr_t*
mmVectorUIntptr_Erase(
    struct mmVectorUIntptr* p,
    uintptr_t* iter)
{
    uintptr_t* last = &p->arrays[p->size - 1];
    assert(0 < p->size && (&p->arrays[0]) <= iter && iter <= last && "0 < p->size && first <= iter && iter <= last");
    if (iter < last)
    {
        mmMemmove(iter, (mmUInt8_t*)iter + sizeof(mmUInt32_t), (mmUInt8_t*)last - (mmUInt8_t*)iter);
        p->size--;
    }
    else
    {
        p->size--;
    }
    return (iter + 1);
}

MM_EXPORT_DLL
int
mmVectorUIntptr_Compare(
    const struct mmVectorUIntptr* p,
    const struct mmVectorUIntptr* q)
{
    if (p->size == q->size)
    {
        return (int)mmMemcmp(p->arrays, q->arrays, sizeof(uintptr_t) * p->size);
    }
    else
    {
        return (int)(p->size - q->size);
    }
}

MM_EXPORT_DLL
void
mmVectorUIntptr_CopyFromValue(
    struct mmVectorUIntptr* p,
    const struct mmVectorUIntptr* q)
{
    mmVectorUIntptr_Reset(p);
    mmVectorUIntptr_AllocatorMemory(p, q->size);
    mmMemcpy(p->arrays, q->arrays, sizeof(uintptr_t) * p->size);
}