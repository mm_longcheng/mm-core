/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeVisibleU32.h"
#include "core/mmSpinlock.h"

static 
void 
__static_mmRbtreeVisibleU32_TryMergeAdd(
    struct mmRbtreeVisibleU32* p);

static 
void 
__static_mmRbtreeVisibleU32_TryMergeRmv(
    struct mmRbtreeVisibleU32* p);

MM_EXPORT_DLL
void
mmRbtreeVisibleU32_Init(
    struct mmRbtreeVisibleU32* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    //
    mmRbtreeU32Vpt_Init(&p->rbtree_m);
    mmRbtsetU32_Init(&p->rbtset_s);
    //
    mmListU32_Init(&p->l_a);
    mmListU32_Init(&p->l_r);
    //
    mmSpinlock_Init(&p->locker_m, NULL);
    mmSpinlock_Init(&p->locker_s, NULL);
    mmSpinlock_Init(&p->locker_a, NULL);
    mmSpinlock_Init(&p->locker_r, NULL);
    //
    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree_m, &_U32VptAllocator);
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU32_Destroy(
    struct mmRbtreeVisibleU32* p)
{
    mmRbtreeVisibleU32_Clear(p);
    //
    mmRbtreeU32Vpt_Destroy(&p->rbtree_m);
    mmRbtsetU32_Destroy(&p->rbtset_s);
    //
    mmListU32_Destroy(&p->l_a);
    mmListU32_Destroy(&p->l_r);
    //
    mmSpinlock_Destroy(&p->locker_m);
    mmSpinlock_Destroy(&p->locker_s);
    mmSpinlock_Destroy(&p->locker_a);
    mmSpinlock_Destroy(&p->locker_r);
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU32_Add(
    struct mmRbtreeVisibleU32* p,
    mmUInt32_t k,
    void* v)
{
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeU32Vpt_Set(&p->rbtree_m, k, v);
    mmSpinlock_Unlock(&p->locker_m);
    //
    if (mmSpinlock_Trylock(&p->locker_s))
    {
        __static_mmRbtreeVisibleU32_TryMergeAdd(p);
        mmRbtsetU32_Add(&p->rbtset_s, k);
        mmSpinlock_Unlock(&p->locker_s);
    }
    else
    {
        mmSpinlock_Lock(&p->locker_a);
        mmListU32_AddFont(&p->l_a, k);
        mmSpinlock_Unlock(&p->locker_a);
    }
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU32_Rmv(
    struct mmRbtreeVisibleU32* p,
    mmUInt32_t k)
{
    if (mmSpinlock_Trylock(&p->locker_s))
    {
        __static_mmRbtreeVisibleU32_TryMergeRmv(p);
        mmRbtsetU32_Rmv(&p->rbtset_s, k);
        mmSpinlock_Unlock(&p->locker_s);
    }
    else
    {
        mmSpinlock_Lock(&p->locker_r);
        mmListU32_AddFont(&p->l_r, k);
        mmSpinlock_Unlock(&p->locker_r);
    }
    //
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeU32Vpt_Rmv(&p->rbtree_m, k);
    mmSpinlock_Unlock(&p->locker_m);
}

MM_EXPORT_DLL
void*
mmRbtreeVisibleU32_Get(
    struct mmRbtreeVisibleU32* p,
    mmUInt32_t k)
{
    void* e = NULL;
    mmSpinlock_Lock(&p->locker_m);
    e = mmRbtreeU32Vpt_Get(&p->rbtree_m, k);
    mmSpinlock_Unlock(&p->locker_m);
    return e;
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU32_Clear(
    struct mmRbtreeVisibleU32* p)
{
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeU32Vpt_Clear(&p->rbtree_m);
    mmSpinlock_Unlock(&p->locker_m);
    //
    mmSpinlock_Lock(&p->locker_s);
    mmRbtsetU32_Clear(&p->rbtset_s);
    mmSpinlock_Unlock(&p->locker_s);
    //
    mmSpinlock_Lock(&p->locker_a);
    mmListU32_Clear(&p->l_a);
    mmSpinlock_Unlock(&p->locker_a);
    //
    mmSpinlock_Lock(&p->locker_r);
    mmListU32_Clear(&p->l_r);
    mmSpinlock_Unlock(&p->locker_r);
}

MM_EXPORT_DLL
size_t
mmRbtreeVisibleU32_Size(
    const struct mmRbtreeVisibleU32* p)
{
    return p->rbtree_m.size;
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU32_Traver(
    struct mmRbtreeVisibleU32* p,
    mmRbtreeVisibleU32HandleFunc handle,
    void* u)
{
    mmUInt32_t k = 0;
    void* v = NULL;
    
    assert(NULL != handle && "handle is a null.");
    
    // init key.
    // k = 0;
    mmSpinlock_Lock(&p->locker_s);
    
    __static_mmRbtreeVisibleU32_TryMergeAdd(p);
    __static_mmRbtreeVisibleU32_TryMergeRmv(p);
    
    if (0 != p->rbtset_s.size)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetU32Iterator* it = NULL;
        //
        n = mmRb_First(&p->rbtset_s.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
            n = mmRb_Next(n);
            // copy key.
            k = it->k;
            v = mmRbtreeVisibleU32_Get(p, k);
            if (NULL != v)
            {
                (*(handle))(p, u, k, v);
            }
        }
    }
    mmSpinlock_Unlock(&p->locker_s);
    // destroy key.
    // k = 0;
}

static 
void 
__static_mmRbtreeVisibleU32_TryMergeAdd(
    struct mmRbtreeVisibleU32* p)
{
    if (0 != p->l_a.size)
    {
        if (mmSpinlock_Trylock(&p->locker_a))
        {
            struct mmListHead* pos = NULL;
            struct mmListU32Iterator* it = NULL;
            pos = p->l_a.l.next;
            while (pos != &p->l_a.l)
            {
                struct mmListHead* curr = pos;
                pos = pos->next;
                it = mmList_Entry(curr, struct mmListU32Iterator, n);
                mmRbtsetU32_Add(&p->rbtset_s, it->v);
                mmListU32_Erase(&p->l_a, it);
            }
            mmSpinlock_Unlock(&p->locker_a);
        }
    }
}
static 
void 
__static_mmRbtreeVisibleU32_TryMergeRmv(
    struct mmRbtreeVisibleU32* p)
{
    if (0 != p->l_r.size)
    {
        if (mmSpinlock_Trylock(&p->locker_r))
        {
            struct mmListHead* pos = NULL;
            struct mmListU32Iterator* it = NULL;
            pos = p->l_r.l.next;
            while (pos != &p->l_r.l)
            {
                struct mmListHead* curr = pos;
                pos = pos->next;
                it = mmList_Entry(curr, struct mmListU32Iterator, n);
                mmRbtsetU32_Rmv(&p->rbtset_s, it->v);
                mmListU32_Erase(&p->l_r, it);
            }
            mmSpinlock_Unlock(&p->locker_r);
        }
    }
}

