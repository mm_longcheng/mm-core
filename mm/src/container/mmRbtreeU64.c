/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeU64.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmRbtreeU64U32Iterator_Init(
    struct mmRbtreeU64U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64U32Iterator_Destroy(
    struct mmRbtreeU64U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64U32_Init(
    struct mmRbtreeU64U32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU64U32_Destroy(
    struct mmRbtreeU64U32* p)
{
    mmRbtreeU64U32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeU64U32Iterator*
mmRbtreeU64U32_Add(
    struct mmRbtreeU64U32* p,
    mmUInt64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64U32Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64U32Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU64U32Iterator*)mmMalloc(sizeof(struct mmRbtreeU64U32Iterator));
    mmRbtreeU64U32Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64U32_Rmv(
    struct mmRbtreeU64U32* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U32Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64U32Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU64U32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU64U32_Erase(
    struct mmRbtreeU64U32* p,
    struct mmRbtreeU64U32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeU64U32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeU64U32_GetInstance(
    struct mmRbtreeU64U32* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U32Iterator* it = mmRbtreeU64U32_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeU64U32_Get(
    const struct mmRbtreeU64U32* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U32Iterator* it = mmRbtreeU64U32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU64U32Iterator*
mmRbtreeU64U32_GetIterator(
    const struct mmRbtreeU64U32* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U32Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64U32Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU64U32_Clear(
    struct mmRbtreeU64U32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64U32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64U32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeU64U32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU64U32_Size(
    const struct mmRbtreeU64U32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeU64U32Iterator*
mmRbtreeU64U32_Set(
    struct mmRbtreeU64U32* p,
    mmUInt64_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64U32Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64U32Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeU64U32Iterator*)mmMalloc(sizeof(struct mmRbtreeU64U32Iterator));
    mmRbtreeU64U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU64U32Iterator*
mmRbtreeU64U32_Insert(
    struct mmRbtreeU64U32* p,
    mmUInt64_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64U32Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64U32Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU64U32Iterator*)mmMalloc(sizeof(struct mmRbtreeU64U32Iterator));
    mmRbtreeU64U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64U64Iterator_Init(
    struct mmRbtreeU64U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64U64Iterator_Destroy(
    struct mmRbtreeU64U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64U64_Init(
    struct mmRbtreeU64U64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU64U64_Destroy(
    struct mmRbtreeU64U64* p)
{
    mmRbtreeU64U64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeU64U64Iterator*
mmRbtreeU64U64_Add(
    struct mmRbtreeU64U64* p,
    mmUInt64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64U64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64U64Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU64U64Iterator*)mmMalloc(sizeof(struct mmRbtreeU64U64Iterator));
    mmRbtreeU64U64Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64U64_Rmv(
    struct mmRbtreeU64U64* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U64Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64U64Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU64U64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU64U64_Erase(
    struct mmRbtreeU64U64* p,
    struct mmRbtreeU64U64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeU64U64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeU64U64_GetInstance(
    struct mmRbtreeU64U64* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U64Iterator* it = mmRbtreeU64U64_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeU64U64_Get(
    const struct mmRbtreeU64U64* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U64Iterator* it = mmRbtreeU64U64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU64U64Iterator*
mmRbtreeU64U64_GetIterator(
    const struct mmRbtreeU64U64* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64U64Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64U64Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU64U64_Clear(
    struct mmRbtreeU64U64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64U64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64U64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeU64U64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU64U64_Size(
    const struct mmRbtreeU64U64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeU64U64Iterator*
mmRbtreeU64U64_Set(
    struct mmRbtreeU64U64* p,
    mmUInt64_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64U64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64U64Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeU64U64Iterator*)mmMalloc(sizeof(struct mmRbtreeU64U64Iterator));
    mmRbtreeU64U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU64U64Iterator*
mmRbtreeU64U64_Insert(
    struct mmRbtreeU64U64* p,
    mmUInt64_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64U64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64U64Iterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU64U64Iterator*)mmMalloc(sizeof(struct mmRbtreeU64U64Iterator));
    mmRbtreeU64U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64StringIterator_Init(
    struct mmRbtreeU64StringIterator* p)
{
    p->k = 0;
    mmString_Init(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64StringIterator_Destroy(
    struct mmRbtreeU64StringIterator* p)
{
    p->k = 0;
    mmString_Destroy(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64String_Init(
    struct mmRbtreeU64String* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU64String_Destroy(
    struct mmRbtreeU64String* p)
{
    mmRbtreeU64String_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeU64StringIterator*
mmRbtreeU64String_Add(
    struct mmRbtreeU64String* p,
    mmUInt64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64StringIterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64StringIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU64StringIterator*)mmMalloc(sizeof(struct mmRbtreeU64StringIterator));
    mmRbtreeU64StringIterator_Init(it);
    it->k = k;
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64String_Rmv(
    struct mmRbtreeU64String* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64StringIterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64StringIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU64String_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU64String_Erase(
    struct mmRbtreeU64String* p,
    struct mmRbtreeU64StringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeU64StringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeU64String_GetInstance(
    struct mmRbtreeU64String* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64StringIterator* it = mmRbtreeU64String_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeU64String_Get(
    const struct mmRbtreeU64String* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64StringIterator* it = mmRbtreeU64String_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU64StringIterator*
mmRbtreeU64String_GetIterator(
    const struct mmRbtreeU64String* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64StringIterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64StringIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU64String_Clear(
    struct mmRbtreeU64String* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64StringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64StringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeU64String_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU64String_Size(
    const struct mmRbtreeU64String* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeU64StringIterator*
mmRbtreeU64String_Set(
    struct mmRbtreeU64String* p,
    mmUInt64_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64StringIterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64StringIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return it;
        }
    }
    it = (struct mmRbtreeU64StringIterator*)mmMalloc(sizeof(struct mmRbtreeU64StringIterator));
    mmRbtreeU64StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU64StringIterator*
mmRbtreeU64String_Insert(
    struct mmRbtreeU64String* p,
    mmUInt64_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64StringIterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64StringIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU64StringIterator*)mmMalloc(sizeof(struct mmRbtreeU64StringIterator));
    mmRbtreeU64StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64VptIterator_Init(
    struct mmRbtreeU64VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64VptIterator_Destroy(
    struct mmRbtreeU64VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU64VptAllocator_Init(
    struct mmRbtreeU64VptAllocator* p)
{
    p->Produce = &mmRbtreeU64Vpt_WeakProduce;
    p->Recycle = &mmRbtreeU64Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeU64VptAllocator_Destroy(
    struct mmRbtreeU64VptAllocator* p)
{
    p->Produce = &mmRbtreeU64Vpt_WeakProduce;
    p->Recycle = &mmRbtreeU64Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeU64Vpt_Init(
    struct mmRbtreeU64Vpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeU64VptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU64Vpt_Destroy(
    struct mmRbtreeU64Vpt* p)
{
    mmRbtreeU64Vpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeU64VptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU64Vpt_SetAllocator(
    struct mmRbtreeU64Vpt* p,
    struct mmRbtreeU64VptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeU64VptIterator*
mmRbtreeU64Vpt_Add(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64VptIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU64VptIterator*)mmMalloc(sizeof(struct mmRbtreeU64VptIterator));
    mmRbtreeU64VptIterator_Init(it);
    it->k = k;
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU64Vpt_Rmv(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64VptIterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64VptIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU64Vpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU64Vpt_Erase(
    struct mmRbtreeU64Vpt* p,
    struct mmRbtreeU64VptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->k, it->v);
    mmRbtreeU64VptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeU64Vpt_GetInstance(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeU64VptIterator* it = mmRbtreeU64Vpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeU64Vpt_Get(
    const struct mmRbtreeU64Vpt* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64VptIterator* it = mmRbtreeU64Vpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU64VptIterator*
mmRbtreeU64Vpt_GetIterator(
    const struct mmRbtreeU64Vpt* p,
    mmUInt64_t k)
{
    struct mmRbtreeU64VptIterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU64VptIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU64Vpt_Clear(
    struct mmRbtreeU64Vpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeU64Vpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU64Vpt_Size(
    const struct mmRbtreeU64Vpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeU64VptIterator*
mmRbtreeU64Vpt_Set(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64VptIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeU64VptIterator*)mmMalloc(sizeof(struct mmRbtreeU64VptIterator));
    mmRbtreeU64VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU64VptIterator*
mmRbtreeU64Vpt_Insert(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU64VptIterator, n);
        result = mmRbtreeU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU64VptIterator*)mmMalloc(sizeof(struct mmRbtreeU64VptIterator));
    mmRbtreeU64VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeU64Vpt_WeakProduce(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeU64Vpt_WeakRecycle(
    struct mmRbtreeU64Vpt* p,
    mmUInt64_t k,
    void* v)
{
    return v;
}

