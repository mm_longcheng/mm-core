/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtsetU64.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmRbtsetU64Iterator_Init(
    struct mmRbtsetU64Iterator* p)
{
    p->k = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetU64Iterator_Destroy(
    struct mmRbtsetU64Iterator* p)
{
    p->k = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtsetU64_Init(
    struct mmRbtsetU64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtsetU64_Destroy(
    struct mmRbtsetU64* p)
{
    mmRbtsetU64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtsetU64Iterator*
mmRbtsetU64_Add(
    struct mmRbtsetU64* p,
    mmUInt64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetU64Iterator, n);
        result = mmRbtsetU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtsetU64Iterator*)mmMalloc(sizeof(struct mmRbtsetU64Iterator));
    mmRbtsetU64Iterator_Init(it);
    it->k = k;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtsetU64_Rmv(
    struct mmRbtsetU64* p,
    mmUInt64_t k)
{
    struct mmRbtsetU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetU64Iterator, n);
        result = mmRbtsetU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtsetU64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtsetU64_Erase(
    struct mmRbtsetU64* p,
    struct mmRbtsetU64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtsetU64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

// return 1 if have member.0 is not member.
MM_EXPORT_DLL
int
mmRbtsetU64_Get(
    const struct mmRbtsetU64* p,
    mmUInt64_t k)
{
    struct mmRbtsetU64Iterator* it = mmRbtsetU64_GetIterator(p, k);
    return (NULL != it) ? 1 : 0;
}

MM_EXPORT_DLL
struct mmRbtsetU64Iterator*
mmRbtsetU64_GetIterator(
    const struct mmRbtsetU64* p,
    mmUInt64_t k)
{
    struct mmRbtsetU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtsetU64Iterator, n);
        result = mmRbtsetU64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtsetU64_Clear(
    struct mmRbtsetU64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtsetU64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtsetU64_Size(
    const struct mmRbtsetU64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtsetU64Iterator*
mmRbtsetU64_Insert(
    struct mmRbtsetU64* p,
    mmUInt64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtsetU64Iterator, n);
        result = mmRbtsetU64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtsetU64Iterator*)mmMalloc(sizeof(struct mmRbtsetU64Iterator));
    mmRbtsetU64Iterator_Init(it);
    it->k = k;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}
