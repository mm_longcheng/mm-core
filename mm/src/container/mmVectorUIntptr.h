/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorUIntptr_h__
#define __mmVectorUIntptr_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorUIntptr
{
    size_t size;
    size_t capacity;
    uintptr_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Init(
    struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Destroy(
    struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Assign(
    struct mmVectorUIntptr* p, 
    const struct mmVectorUIntptr* q);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Clear(
    struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Reset(
    struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_SetIndex(
    struct mmVectorUIntptr* p, 
    size_t index, 
    uintptr_t e);

MM_EXPORT_DLL 
uintptr_t 
mmVectorUIntptr_GetIndex(
    const struct mmVectorUIntptr* p, 
    size_t index);

MM_EXPORT_DLL 
uintptr_t* 
mmVectorUIntptr_GetIndexReference(
    const struct mmVectorUIntptr* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Realloc(
    struct mmVectorUIntptr* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Free(
    struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Resize(
    struct mmVectorUIntptr* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Reserve(
    struct mmVectorUIntptr* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_AlignedMemory(
    struct mmVectorUIntptr* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_AllocatorMemory(
    struct mmVectorUIntptr* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_PushBack(
    struct mmVectorUIntptr* p, 
    uintptr_t e);

MM_EXPORT_DLL 
uintptr_t 
mmVectorUIntptr_PopBack(
    struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
uintptr_t* 
mmVectorUIntptr_Begin(
    const struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
uintptr_t* 
mmVectorUIntptr_End(
    const struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
uintptr_t* 
mmVectorUIntptr_Back(
    const struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
uintptr_t* 
mmVectorUIntptr_Next(
    const struct mmVectorUIntptr* p, 
    uintptr_t* iter);

MM_EXPORT_DLL 
uintptr_t 
mmVectorUIntptr_At(
    const struct mmVectorUIntptr* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorUIntptr_Size(
    const struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
size_t 
mmVectorUIntptr_Capacity(
    const struct mmVectorUIntptr* p);

MM_EXPORT_DLL 
void 
mmVectorUIntptr_Remove(
    struct mmVectorUIntptr* p, 
    size_t index);

MM_EXPORT_DLL 
uintptr_t* 
mmVectorUIntptr_Erase(
    struct mmVectorUIntptr* p, 
    uintptr_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorUIntptr_Compare(
    const struct mmVectorUIntptr* p, 
    const struct mmVectorUIntptr* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorUIntptr_CopyFromValue(
    struct mmVectorUIntptr* p, 
    const struct mmVectorUIntptr* q);

// vector Null.
#define mmVectorUIntptr_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorUIntptr_Make(v)               \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .capacity  = */ \
    (uintptr_t*)v,         /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorUIntptr_h__
