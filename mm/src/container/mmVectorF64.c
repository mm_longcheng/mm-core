/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVectorF64.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

MM_EXPORT_DLL
void
mmVectorF64_Init(
    struct mmVectorF64* p)
{
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorF64_Destroy(
    struct mmVectorF64* p)
{
    mmVectorF64_Free(p);
    //
    p->size = 0;
    p->capacity = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorF64_Assign(
    struct mmVectorF64* p,
    const struct mmVectorF64* q)
{
    mmVectorF64_Realloc(p, q->capacity);
    p->size = q->size;
    mmMemcpy(p->arrays, q->arrays, sizeof(mmFloat64_t) * p->size);
}

MM_EXPORT_DLL
void
mmVectorF64_Clear(
    struct mmVectorF64* p)
{
    p->size = 0;
}

MM_EXPORT_DLL
void
mmVectorF64_Reset(
    struct mmVectorF64* p)
{
    mmVectorF64_Free(p);
}

MM_EXPORT_DLL
void
mmVectorF64_SetIndex(
    struct mmVectorF64* p,
    size_t index,
    mmFloat64_t e)
{
    assert(0 <= index && index < p->size);
    p->arrays[index] = e;
}

MM_EXPORT_DLL
mmFloat64_t
mmVectorF64_GetIndex(
    const struct mmVectorF64* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays[index];
}

MM_EXPORT_DLL
mmFloat64_t*
mmVectorF64_GetIndexReference(
    const struct mmVectorF64* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return &p->arrays[index];
}

MM_EXPORT_DLL
void
mmVectorF64_Realloc(
    struct mmVectorF64* p,
    size_t capacity)
{
    if (p->capacity < capacity)
    {
        mmFloat64_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        arrays = (mmFloat64_t*)mmMalloc(sizeof(mmFloat64_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmFloat64_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(mmFloat64_t) * p->capacity, 0, sizeof(mmFloat64_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
    else if (capacity < (p->capacity / 4) && 8 < p->capacity)
    {
        mmFloat64_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        mem_size <<= 1;
        arrays = (mmFloat64_t*)mmMalloc(sizeof(mmFloat64_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmFloat64_t) * mem_size);
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorF64_Free(
    struct mmVectorF64* p)
{
    if (0 < p->capacity && NULL != p->arrays)
    {
        mmFree(p->arrays);
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
    else
    {
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
}

MM_EXPORT_DLL
void
mmVectorF64_Resize(
    struct mmVectorF64* p,
    size_t size)
{
    mmVectorF64_Realloc(p, size);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorF64_Reserve(
    struct mmVectorF64* p,
    size_t capacity)
{
    mmVectorF64_Realloc(p, capacity);
}

MM_EXPORT_DLL
void
mmVectorF64_AlignedMemory(
    struct mmVectorF64* p,
    size_t size)
{
    size_t sz = size;
    sz = sz < p->capacity ? p->capacity : sz;
    mmVectorF64_Realloc(p, sz);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorF64_AllocatorMemory(
    struct mmVectorF64* p,
    size_t size)
{
    if (p->capacity < size)
    {
        mmFloat64_t* arrays = NULL;
        size_t mem_size = size;
        arrays = (mmFloat64_t*)mmMalloc(sizeof(mmFloat64_t) * mem_size);
        mmMemcpy(arrays, p->arrays, sizeof(mmFloat64_t) * p->capacity);
        mmMemset((mmUInt8_t*)arrays + sizeof(mmFloat64_t) * p->capacity, 0, sizeof(mmFloat64_t) * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorF64_PushBack(
    struct mmVectorF64* p,
    mmFloat64_t e)
{
    size_t new_size = p->size + 1;
    if (new_size > p->capacity)
    {
        // vector push back realloc strategy, *= 1.5
        size_t capacity = p->capacity + p->capacity / 2;
        capacity = capacity < new_size ? new_size : capacity;
        mmVectorF64_Realloc(p, capacity);
    }
    p->arrays[p->size++] = e;
}

MM_EXPORT_DLL
mmFloat64_t
mmVectorF64_PopBack(
    struct mmVectorF64* p)
{
    assert(0 < p->size && "mmVectorF64 0 < p->size");
    return p->arrays[--p->size];
}

MM_EXPORT_DLL
mmFloat64_t*
mmVectorF64_Begin(
    const struct mmVectorF64* p)
{
    return &p->arrays[0];
}

MM_EXPORT_DLL
mmFloat64_t*
mmVectorF64_End(
    const struct mmVectorF64* p)
{
    return &p->arrays[p->size];
}

MM_EXPORT_DLL
mmFloat64_t*
mmVectorF64_Back(
    const struct mmVectorF64* p)
{
    assert(0 < p->size && "mmVectorF64 0 < p->size");
    return &p->arrays[p->size - 1];
}

MM_EXPORT_DLL
mmFloat64_t*
mmVectorF64_Next(
    const struct mmVectorF64* p,
    mmFloat64_t* iter)
{
    return iter + 1;
}

MM_EXPORT_DLL
mmFloat64_t
mmVectorF64_At(
    const struct mmVectorF64* p,
    size_t index)
{
    return mmVectorF64_GetIndex(p, index);
}

MM_EXPORT_DLL
size_t
mmVectorF64_Size(
    const struct mmVectorF64* p)
{
    return p->size;
}

MM_EXPORT_DLL
size_t
mmVectorF64_Capacity(
    const struct mmVectorF64* p)
{
    return p->capacity;
}

MM_EXPORT_DLL
void
mmVectorF64_Remove(
    struct mmVectorF64* p,
    size_t index)
{
    assert(0 < p->size && index <= p->size - 1 && "mmVectorF64 0 < p->size && index <= p->size - 1");
    if (index < p->size - 1)
    {
        mmMemmove(p->arrays + index, p->arrays + (index + 1), (p->size - index - 1) * sizeof(mmFloat64_t));
        p->size--;
    }
    else
    {
        p->size--;
    }
}

MM_EXPORT_DLL
mmFloat64_t*
mmVectorF64_Erase(
    struct mmVectorF64* p,
    mmFloat64_t* iter)
{
    mmFloat64_t* last = &p->arrays[p->size - 1];
    assert(0 < p->size && (&p->arrays[0]) <= iter && iter <= last && "0 < p->size && first <= iter && iter <= last");
    if (iter < last)
    {
        mmMemmove(iter, (mmUInt8_t*)iter + sizeof(mmUInt32_t), (mmUInt8_t*)last - (mmUInt8_t*)iter);
        p->size--;
    }
    else
    {
        p->size--;
    }
    return (iter + 1);
}

MM_EXPORT_DLL
int
mmVectorF64_Compare(
    const struct mmVectorF64* p,
    const struct mmVectorF64* q)
{
    if (p->size == q->size)
    {
        return (int)mmMemcmp(p->arrays, q->arrays, sizeof(mmFloat64_t) * p->size);
    }
    else
    {
        return (int)(p->size - q->size);
    }
}

MM_EXPORT_DLL
void
mmVectorF64_CopyFromValue(
    struct mmVectorF64* p,
    const struct mmVectorF64* q)
{
    mmVectorF64_Reset(p);
    mmVectorF64_AllocatorMemory(p, q->size);
    mmMemcpy(p->arrays, q->arrays, sizeof(mmFloat64_t) * p->size);
}