/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeF32.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmRbtreeF32U32Iterator_Init(
    struct mmRbtreeF32U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32U32Iterator_Destroy(
    struct mmRbtreeF32U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32U32_Init(
    struct mmRbtreeF32U32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF32U32_Destroy(
    struct mmRbtreeF32U32* p)
{
    mmRbtreeF32U32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeF32U32Iterator*
mmRbtreeF32U32_Add(
    struct mmRbtreeF32U32* p,
    mmFloat32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32U32Iterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32U32Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF32U32Iterator*)mmMalloc(sizeof(struct mmRbtreeF32U32Iterator));
    mmRbtreeF32U32Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32U32_Rmv(
    struct mmRbtreeF32U32* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U32Iterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32U32Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF32U32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF32U32_Erase(
    struct mmRbtreeF32U32* p,
    struct mmRbtreeF32U32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeF32U32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeF32U32_GetInstance(
    struct mmRbtreeF32U32* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U32Iterator* it = mmRbtreeF32U32_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeF32U32_Get(
    const struct mmRbtreeF32U32* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U32Iterator* it = mmRbtreeF32U32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF32U32Iterator*
mmRbtreeF32U32_GetIterator(
    const struct mmRbtreeF32U32* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U32Iterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32U32Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF32U32_Clear(
    struct mmRbtreeF32U32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF32U32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF32U32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeF32U32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF32U32_Size(
    const struct mmRbtreeF32U32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeF32U32Iterator*
mmRbtreeF32U32_Set(
    struct mmRbtreeF32U32* p,
    mmFloat32_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32U32Iterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32U32Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeF32U32Iterator*)mmMalloc(sizeof(struct mmRbtreeF32U32Iterator));
    mmRbtreeF32U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF32U32Iterator*
mmRbtreeF32U32_Insert(
    struct mmRbtreeF32U32* p,
    mmFloat32_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32U32Iterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32U32Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF32U32Iterator*)mmMalloc(sizeof(struct mmRbtreeF32U32Iterator));
    mmRbtreeF32U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32U64Iterator_Init(
    struct mmRbtreeF32U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32U64Iterator_Destroy(
    struct mmRbtreeF32U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32U64_Init(
    struct mmRbtreeF32U64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF32U64_Destroy(
    struct mmRbtreeF32U64* p)
{
    mmRbtreeF32U64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeF32U64Iterator*
mmRbtreeF32U64_Add(
    struct mmRbtreeF32U64* p,
    mmFloat32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32U64Iterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32U64Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF32U64Iterator*)mmMalloc(sizeof(struct mmRbtreeF32U64Iterator));
    mmRbtreeF32U64Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32U64_Rmv(
    struct mmRbtreeF32U64* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U64Iterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32U64Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF32U64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF32U64_Erase(
    struct mmRbtreeF32U64* p,
    struct mmRbtreeF32U64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeF32U64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL mmUInt64_t*
mmRbtreeF32U64_GetInstance(
    struct mmRbtreeF32U64* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U64Iterator* it = mmRbtreeF32U64_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeF32U64_Get(
    const struct mmRbtreeF32U64* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U64Iterator* it = mmRbtreeF32U64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF32U64Iterator*
mmRbtreeF32U64_GetIterator(
    const struct mmRbtreeF32U64* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32U64Iterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32U64Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF32U64_Clear(
    struct mmRbtreeF32U64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF32U64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF32U64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeF32U64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF32U64_Size(
    const struct mmRbtreeF32U64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeF32U64Iterator*
mmRbtreeF32U64_Set(
    struct mmRbtreeF32U64* p,
    mmFloat32_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32U64Iterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32U64Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeF32U64Iterator*)mmMalloc(sizeof(struct mmRbtreeF32U64Iterator));
    mmRbtreeF32U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF32U64Iterator*
mmRbtreeF32U64_Insert(
    struct mmRbtreeF32U64* p,
    mmFloat32_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32U64Iterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32U64Iterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF32U64Iterator*)mmMalloc(sizeof(struct mmRbtreeF32U64Iterator));
    mmRbtreeF32U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32StringIterator_Init(
    struct mmRbtreeF32StringIterator* p)
{
    p->k = 0;
    mmString_Init(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32StringIterator_Destroy(
    struct mmRbtreeF32StringIterator* p)
{
    p->k = 0;
    mmString_Destroy(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32String_Init(
    struct mmRbtreeF32String* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF32String_Destroy(
    struct mmRbtreeF32String* p)
{
    mmRbtreeF32String_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeF32StringIterator*
mmRbtreeF32String_Add(
    struct mmRbtreeF32String* p,
    mmFloat32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32StringIterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32StringIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF32StringIterator*)mmMalloc(sizeof(struct mmRbtreeF32StringIterator));
    mmRbtreeF32StringIterator_Init(it);
    it->k = k;
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32String_Rmv(
    struct mmRbtreeF32String* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32StringIterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32StringIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF32String_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF32String_Erase(
    struct mmRbtreeF32String* p,
    struct mmRbtreeF32StringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeF32StringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeF32String_GetInstance(
    struct mmRbtreeF32String* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32StringIterator* it = mmRbtreeF32String_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeF32String_Get(
    const struct mmRbtreeF32String* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32StringIterator* it = mmRbtreeF32String_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF32StringIterator*
mmRbtreeF32String_GetIterator(
    const struct mmRbtreeF32String* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32StringIterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32StringIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF32String_Clear(
    struct mmRbtreeF32String* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF32StringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF32StringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeF32String_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF32String_Size(
    const struct mmRbtreeF32String* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeF32StringIterator*
mmRbtreeF32String_Set(
    struct mmRbtreeF32String* p,
    mmFloat32_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32StringIterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32StringIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return it;
        }
    }
    it = (struct mmRbtreeF32StringIterator*)mmMalloc(sizeof(struct mmRbtreeF32StringIterator));
    mmRbtreeF32StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF32StringIterator*
mmRbtreeF32String_Insert(
    struct mmRbtreeF32String* p,
    mmFloat32_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32StringIterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32StringIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF32StringIterator*)mmMalloc(sizeof(struct mmRbtreeF32StringIterator));
    mmRbtreeF32StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32VptIterator_Init(
    struct mmRbtreeF32VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32VptIterator_Destroy(
    struct mmRbtreeF32VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF32VptAllocator_Init(
    struct mmRbtreeF32VptAllocator* p)
{
    p->Produce = &mmRbtreeF32Vpt_WeakProduce;
    p->Recycle = &mmRbtreeF32Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeF32VptAllocator_Destroy(
    struct mmRbtreeF32VptAllocator* p)
{
    p->Produce = &mmRbtreeF32Vpt_WeakProduce;
    p->Recycle = &mmRbtreeF32Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeF32Vpt_Init(
    struct mmRbtreeF32Vpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeF32VptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF32Vpt_Destroy(
    struct mmRbtreeF32Vpt* p)
{
    mmRbtreeF32Vpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeF32VptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF32Vpt_SetAllocator(
    struct mmRbtreeF32Vpt* p,
    struct mmRbtreeF32VptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeF32VptIterator*
mmRbtreeF32Vpt_Add(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32VptIterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32VptIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF32VptIterator*)mmMalloc(sizeof(struct mmRbtreeF32VptIterator));
    mmRbtreeF32VptIterator_Init(it);
    it->k = k;
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF32Vpt_Rmv(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32VptIterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32VptIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF32Vpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF32Vpt_Erase(
    struct mmRbtreeF32Vpt* p,
    struct mmRbtreeF32VptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->k, it->v);
    mmRbtreeF32VptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeF32Vpt_GetInstance(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeF32VptIterator* it = mmRbtreeF32Vpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeF32Vpt_Get(
    const struct mmRbtreeF32Vpt* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32VptIterator* it = mmRbtreeF32Vpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF32VptIterator*
mmRbtreeF32Vpt_GetIterator(
    const struct mmRbtreeF32Vpt* p,
    mmFloat32_t k)
{
    struct mmRbtreeF32VptIterator* it = NULL;
    mmFloat32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF32VptIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF32Vpt_Clear(
    struct mmRbtreeF32Vpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF32VptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF32VptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeF32Vpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF32Vpt_Size(
    const struct mmRbtreeF32Vpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeF32VptIterator*
mmRbtreeF32Vpt_Set(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32VptIterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32VptIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeF32VptIterator*)mmMalloc(sizeof(struct mmRbtreeF32VptIterator));
    mmRbtreeF32VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF32VptIterator*
mmRbtreeF32Vpt_Insert(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF32VptIterator* it = NULL;
    mmFloat32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF32VptIterator, n);
        result = mmRbtreeF32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF32VptIterator*)mmMalloc(sizeof(struct mmRbtreeF32VptIterator));
    mmRbtreeF32VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeF32Vpt_WeakProduce(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeF32Vpt_WeakRecycle(
    struct mmRbtreeF32Vpt* p,
    mmFloat32_t k,
    void* v)
{
    return v;
}

