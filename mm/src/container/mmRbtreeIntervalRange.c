/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeIntervalRange.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
intptr_t
mmRbtreeIntervalRangeCompareFunc(
    size_t lo, size_t ll,
    size_t ro, size_t rl)
{
    // [l, r)
    size_t lr = lo + ll;
    size_t rr = ro + rl;
    if (ro <= lo && lr <= rr)
    {
        return 0;
    }
    else if (lo >= rr)
    {
        return (lo == rr) ? +1 : (lo - rr);
    }
    else if (ro >= lr)
    {
        return (lr == ro) ? -1 : (lr - ro);
    }
    else
    {
        return lo - ro;
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRangeIterator_Init(
    struct mmRbtreeIntervalRangeIterator* p)
{
    p->o = 0;
    p->l = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRangeIterator_Destroy(
    struct mmRbtreeIntervalRangeIterator* p)
{
    p->o = 0;
    p->l = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRangeAllocator_Init(
    struct mmRbtreeIntervalRangeAllocator* p)
{
    p->Produce = &mmRbtreeIntervalRange_WeakProduce;
    p->Recycle = &mmRbtreeIntervalRange_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRangeAllocator_Destroy(
    struct mmRbtreeIntervalRangeAllocator* p)
{
    p->Produce = &mmRbtreeIntervalRange_WeakProduce;
    p->Recycle = &mmRbtreeIntervalRange_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRange_Init(
    struct mmRbtreeIntervalRange* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalRangeAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRange_Destroy(
    struct mmRbtreeIntervalRange* p)
{
    mmRbtreeIntervalRange_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalRangeAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRange_SetAllocator(
    struct mmRbtreeIntervalRange* p,
    struct mmRbtreeIntervalRangeAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalRange_Add(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalRangeIterator* it = NULL;
    intptr_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalRangeIterator, n);
        result = mmRbtreeIntervalRangeCompareFunc(o, l, it->o, it->l);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeIntervalRangeIterator*)mmMalloc(sizeof(struct mmRbtreeIntervalRangeIterator));
    mmRbtreeIntervalRangeIterator_Init(it);
    it->o = o;
    it->l = l;
    it->v = (*(p->allocator.Produce))(p, o, l);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it->v;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRange_Rmv(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l)
{
    struct mmRbtreeIntervalRangeIterator* it = NULL;
    intptr_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalRangeIterator, n);
        result = mmRbtreeIntervalRangeCompareFunc(o, l, it->o, it->l);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeIntervalRange_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRange_Erase(
    struct mmRbtreeIntervalRange* p,
    struct mmRbtreeIntervalRangeIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->o, it->l, it->v);
    mmRbtreeIntervalRangeIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalRange_GetInstance(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l)
{
    void* v = mmRbtreeIntervalRange_Get(p, o, l);
    if (NULL == v)
    {
        v = mmRbtreeIntervalRange_Add(p, o, l);
    }
    return v;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalRange_Get(
    const struct mmRbtreeIntervalRange* p,
    size_t o, size_t l)
{
    struct mmRbtreeIntervalRangeIterator* it = mmRbtreeIntervalRange_GetIterator(p, o, l);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalRangeIterator*
mmRbtreeIntervalRange_GetIterator(
    const struct mmRbtreeIntervalRange* p,
    size_t o, size_t l)
{
    struct mmRbtreeIntervalRangeIterator* it = NULL;
    intptr_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalRangeIterator, n);
        result = mmRbtreeIntervalRangeCompareFunc(o, l, it->o, it->l);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalRange_Clear(
    struct mmRbtreeIntervalRange* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeIntervalRangeIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeIntervalRangeIterator, n);
        n = mmRb_Next(n);
        mmRbtreeIntervalRange_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeIntervalRange_Size(
    const struct mmRbtreeIntervalRange* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalRangeIterator*
mmRbtreeIntervalRange_Set(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalRangeIterator* it = NULL;
    intptr_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalRangeIterator, n);
        result = mmRbtreeIntervalRangeCompareFunc(o, l, it->o, it->l);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeIntervalRangeIterator*)mmMalloc(sizeof(struct mmRbtreeIntervalRangeIterator));
    mmRbtreeIntervalRangeIterator_Init(it);
    it->o = o;
    it->l = l;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalRangeIterator*
mmRbtreeIntervalRange_Insert(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalRangeIterator* it = NULL;
    intptr_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalRangeIterator, n);
        result = mmRbtreeIntervalRangeCompareFunc(o, l, it->o, it->l);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            return NULL;
        }
    }
    it = (struct mmRbtreeIntervalRangeIterator*)mmMalloc(sizeof(struct mmRbtreeIntervalRangeIterator));
    mmRbtreeIntervalRangeIterator_Init(it);
    it->o = o;
    it->l = l;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalRange_WeakProduce(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalRange_WeakRecycle(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l,
    void* v)
{
    return v;
}

