/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmListU64_h__
#define __mmListU64_h__

#include "core/mmCore.h"

#include "core/mmList.h"

#include "core/mmPrefix.h"

struct mmListU64Iterator
{
    mmUInt64_t v;
    struct mmListHead n;
};

MM_EXPORT_DLL 
void 
mmListU64Iterator_Init(
    struct mmListU64Iterator* p);

MM_EXPORT_DLL 
void 
mmListU64Iterator_Destroy(
    struct mmListU64Iterator* p);

struct mmListU64
{
    struct mmListHead l;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmListU64_Init(
    struct mmListU64* p);

MM_EXPORT_DLL 
void 
mmListU64_Destroy(
    struct mmListU64* p);

MM_EXPORT_DLL 
void 
mmListU64_AddFont(
    struct mmListU64* p, 
    mmUInt64_t v);

MM_EXPORT_DLL 
void 
mmListU64_AddTail(
    struct mmListU64* p, 
    mmUInt64_t v);

MM_EXPORT_DLL 
mmUInt64_t 
mmListU64_PopFont(
    struct mmListU64* p);

MM_EXPORT_DLL 
mmUInt64_t 
mmListU64_PopTail(
    struct mmListU64* p);

MM_EXPORT_DLL 
void 
mmListU64_Erase(
    struct mmListU64* p, 
    struct mmListU64Iterator* it);

MM_EXPORT_DLL 
void 
mmListU64_Clear(
    struct mmListU64* p);

MM_EXPORT_DLL 
size_t 
mmListU64_Size(
    const struct mmListU64* p);

MM_EXPORT_DLL 
void 
mmListU64_Remove(
    struct mmListU64* p, 
    mmUInt64_t v);

MM_EXPORT_DLL 
void 
mmListU64_InsertPrev(
    struct mmListU64* p, 
    struct mmListU64Iterator* i, 
    mmUInt64_t v);

MM_EXPORT_DLL 
void 
mmListU64_InsertNext(
    struct mmListU64* p, 
    struct mmListU64Iterator* i, 
    mmUInt64_t v);

#include "core/mmSuffix.h"

#endif//__mmListU64_h__
