/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorVpt_h__
#define __mmVectorVpt_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorVpt
{
    size_t size;
    size_t capacity;
    void** arrays;
};

MM_EXPORT_DLL 
void 
mmVectorVpt_Init(
    struct mmVectorVpt* p);

MM_EXPORT_DLL 
void 
mmVectorVpt_Destroy(
    struct mmVectorVpt* p);

MM_EXPORT_DLL 
void 
mmVectorVpt_Assign(
    struct mmVectorVpt* p, 
    const struct mmVectorVpt* q);

MM_EXPORT_DLL 
void 
mmVectorVpt_Clear(
    struct mmVectorVpt* p);

MM_EXPORT_DLL 
void 
mmVectorVpt_Reset(
    struct mmVectorVpt* p);

MM_EXPORT_DLL 
void 
mmVectorVpt_SetIndex(
    struct mmVectorVpt* p, 
    size_t index, 
    void* e);

MM_EXPORT_DLL 
void* 
mmVectorVpt_GetIndex(
    const struct mmVectorVpt* p, 
    size_t index);

MM_EXPORT_DLL 
void* 
mmVectorVpt_GetIndexReference(
    const struct mmVectorVpt* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorVpt_Realloc(
    struct mmVectorVpt* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorVpt_Free(
    struct mmVectorVpt* p);

MM_EXPORT_DLL 
void 
mmVectorVpt_Resize(
    struct mmVectorVpt* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorVpt_Reserve(
    struct mmVectorVpt* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorVpt_AlignedMemory(
    struct mmVectorVpt* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorVpt_AllocatorMemory(
    struct mmVectorVpt* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorVpt_PushBack(
    struct mmVectorVpt* p, 
    void* e);

MM_EXPORT_DLL 
void* 
mmVectorVpt_PopBack(
    struct mmVectorVpt* p);

MM_EXPORT_DLL 
void** 
mmVectorVpt_Begin(
    const struct mmVectorVpt* p);

MM_EXPORT_DLL 
void** 
mmVectorVpt_End(
    const struct mmVectorVpt* p);

MM_EXPORT_DLL 
void** 
mmVectorVpt_Back(
    const struct mmVectorVpt* p);

MM_EXPORT_DLL 
void** 
mmVectorVpt_Next(
    const struct mmVectorVpt* p, 
    void** iter);

MM_EXPORT_DLL 
void* 
mmVectorVpt_At(
    const struct mmVectorVpt* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorVpt_Size(
    const struct mmVectorVpt* p);

MM_EXPORT_DLL 
size_t 
mmVectorVpt_Capacity(
    const struct mmVectorVpt* p);

MM_EXPORT_DLL 
void 
mmVectorVpt_Remove(
    struct mmVectorVpt* p, 
    size_t index);

MM_EXPORT_DLL 
void** 
mmVectorVpt_Erase(
    struct mmVectorVpt* p, 
    void** iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorVpt_Compare(
    const struct mmVectorVpt* p, 
    const struct mmVectorVpt* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorVpt_CopyFromValue(
    struct mmVectorVpt* p, 
    const struct mmVectorVpt* q);

// vector Null.
#define mmVectorVpt_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorVpt_Make(v)                   \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (void**)v,             /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorVpt_h__
