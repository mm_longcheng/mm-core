/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeIntervalUIntptr_h__
#define __mmRbtreeIntervalUIntptr_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
intptr_t 
mmRbtreeIntervalUIntptrCompareFunc(
    uintptr_t ll, uintptr_t lr, 
    uintptr_t rl, uintptr_t rr);

// mmRbtreeIntervalUIntptr
struct mmRbtreeIntervalUIntptrIterator
{
    uintptr_t l;
    uintptr_t r;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptrIterator_Init(
    struct mmRbtreeIntervalUIntptrIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptrIterator_Destroy(
    struct mmRbtreeIntervalUIntptrIterator* p);

struct mmRbtreeIntervalUIntptr;

struct mmRbtreeIntervalUIntptrAllocator
{
    void* (*Produce)(struct mmRbtreeIntervalUIntptr* p, uintptr_t l, uintptr_t r);
    void* (*Recycle)(struct mmRbtreeIntervalUIntptr* p, uintptr_t l, uintptr_t r, void* v);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptrAllocator_Init(
    struct mmRbtreeIntervalUIntptrAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptrAllocator_Destroy(
    struct mmRbtreeIntervalUIntptrAllocator* p);

struct mmRbtreeIntervalUIntptr
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeIntervalUIntptrAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptr_Init(
    struct mmRbtreeIntervalUIntptr* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptr_Destroy(
    struct mmRbtreeIntervalUIntptr* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptr_SetAllocator(
    struct mmRbtreeIntervalUIntptr* p, 
    struct mmRbtreeIntervalUIntptrAllocator* allocator);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalUIntptr_Add(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptr_Rmv(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptr_Erase(
    struct mmRbtreeIntervalUIntptr* p, 
    struct mmRbtreeIntervalUIntptrIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalUIntptr_GetInstance(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalUIntptr_Get(
    const struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r);

MM_EXPORT_DLL 
struct mmRbtreeIntervalUIntptrIterator* 
mmRbtreeIntervalUIntptr_GetIterator(
    const struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalUIntptr_Clear(
    struct mmRbtreeIntervalUIntptr* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeIntervalUIntptr_Size(
    const struct mmRbtreeIntervalUIntptr* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeIntervalUIntptrIterator*
mmRbtreeIntervalUIntptr_Set(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeIntervalUIntptrIterator*
mmRbtreeIntervalUIntptr_Insert(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalUIntptr_WeakProduce(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalUIntptr_WeakRecycle(
    struct mmRbtreeIntervalUIntptr* p, 
    uintptr_t l, uintptr_t r, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeIntervalUIntptr_h__
