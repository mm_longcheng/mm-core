/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtsetU64_h__
#define __mmRbtsetU64_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
mmSInt64_t 
mmRbtsetU64CompareFunc(
    mmUInt64_t lhs, 
    mmUInt64_t rhs)
{
    return lhs - rhs;
}

// mmRbtsetU64
struct mmRbtsetU64Iterator
{
    mmUInt64_t k;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtsetU64Iterator_Init(
    struct mmRbtsetU64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtsetU64Iterator_Destroy(
    struct mmRbtsetU64Iterator* p);

struct mmRbtsetU64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtsetU64_Init(
    struct mmRbtsetU64* p);

MM_EXPORT_DLL 
void 
mmRbtsetU64_Destroy(
    struct mmRbtsetU64* p);

MM_EXPORT_DLL 
struct mmRbtsetU64Iterator* 
mmRbtsetU64_Add(
    struct mmRbtsetU64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtsetU64_Rmv(
    struct mmRbtsetU64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtsetU64_Erase(
    struct mmRbtsetU64* p, 
    struct mmRbtsetU64Iterator* it);

// return 1 if have member.0 is not member.
MM_EXPORT_DLL 
int 
mmRbtsetU64_Get(
    const struct mmRbtsetU64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
struct mmRbtsetU64Iterator* 
mmRbtsetU64_GetIterator(
    const struct mmRbtsetU64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtsetU64_Clear(
    struct mmRbtsetU64* p);

MM_EXPORT_DLL 
size_t 
mmRbtsetU64_Size(
    const struct mmRbtsetU64* p);

MM_EXPORT_DLL 
struct mmRbtsetU64Iterator* 
mmRbtsetU64_Insert(
    struct mmRbtsetU64* p, 
    mmUInt64_t k);

#include "core/mmSuffix.h"

#endif//__mmRbtsetU64_h__
