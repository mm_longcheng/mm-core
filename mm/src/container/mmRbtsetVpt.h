/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtsetVpt_h__
#define __mmRbtsetVpt_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
intptr_t 
mmRbtsetVptCompareFunc(
    void* lhs, 
    void* rhs)
{
    return (intptr_t)lhs - (intptr_t)rhs;
}

// mmRbtsetVpt
struct mmRbtsetVptIterator
{
    void* k;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtsetVptIterator_Init(
    struct mmRbtsetVptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtsetVptIterator_Destroy(
    struct mmRbtsetVptIterator* p);

struct mmRbtsetVpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtsetVpt_Init(
    struct mmRbtsetVpt* p);

MM_EXPORT_DLL 
void 
mmRbtsetVpt_Destroy(
    struct mmRbtsetVpt* p);

MM_EXPORT_DLL 
struct mmRbtsetVptIterator* 
mmRbtsetVpt_Add(
    struct mmRbtsetVpt* p, 
    void* k);

MM_EXPORT_DLL 
void 
mmRbtsetVpt_Rmv(
    struct mmRbtsetVpt* p, 
    void* k);

MM_EXPORT_DLL 
void 
mmRbtsetVpt_Erase(
    struct mmRbtsetVpt* p, 
    struct mmRbtsetVptIterator* it);

// return 1 if have member.0 is not member.
MM_EXPORT_DLL 
int 
mmRbtsetVpt_Get(
    const struct mmRbtsetVpt* p, 
    void* k);

MM_EXPORT_DLL 
struct mmRbtsetVptIterator* 
mmRbtsetVpt_GetIterator(
    const struct mmRbtsetVpt* p, 
    void* k);

MM_EXPORT_DLL 
void 
mmRbtsetVpt_Clear(
    struct mmRbtsetVpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtsetVpt_Size(
    const struct mmRbtsetVpt* p);

MM_EXPORT_DLL 
struct mmRbtsetVptIterator* 
mmRbtsetVpt_Insert(
    struct mmRbtsetVpt* p, 
    void* k);

#include "core/mmSuffix.h"

#endif//__mmRbtsetVpt_h__
