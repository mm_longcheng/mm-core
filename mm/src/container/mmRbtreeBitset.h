/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeBitset_h__
#define __mmRbtreeBitset_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"

#include "container/mmBitset.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
mmSInt32_t 
mmRbtreeBitsetCompareFunc(
    const struct mmBitset* lhs, 
    const struct mmBitset* rhs);

// mmRbtreeBitsetU32
struct mmRbtreeBitsetU32Iterator
{
    struct mmBitset k;
    mmUInt32_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32Iterator_Init(
    struct mmRbtreeBitsetU32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32Iterator_Destroy(
    struct mmRbtreeBitsetU32Iterator* p);

struct mmRbtreeBitsetU32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32_Init(
    struct mmRbtreeBitsetU32* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32_Destroy(
    struct mmRbtreeBitsetU32* p);

MM_EXPORT_DLL 
struct mmRbtreeBitsetU32Iterator* 
mmRbtreeBitsetU32_Add(
    struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32_Rmv(
    struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32_Erase(
    struct mmRbtreeBitsetU32* p, 
    struct mmRbtreeBitsetU32Iterator* it);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeBitsetU32_GetInstance(
    struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeBitsetU32_Get(
    const struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
struct mmRbtreeBitsetU32Iterator* 
mmRbtreeBitsetU32_GetIterator(
    const struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU32_Clear(
    struct mmRbtreeBitsetU32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeBitsetU32_Size(
    const struct mmRbtreeBitsetU32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeBitsetU32Iterator* 
mmRbtreeBitsetU32_Set(
    struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k, 
    mmUInt32_t v);

MM_EXPORT_DLL 
struct mmRbtreeBitsetU32Iterator* 
mmRbtreeBitsetU32_Insert(
    struct mmRbtreeBitsetU32* p, 
    const struct mmBitset* k, 
    mmUInt32_t v);

// mmRbtreeBitsetString
struct mmRbtreeBitsetU64Iterator
{
    struct mmBitset k;
    mmUInt64_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64Iterator_Init(
    struct mmRbtreeBitsetU64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64Iterator_Destroy(
    struct mmRbtreeBitsetU64Iterator* p);

struct mmRbtreeBitsetU64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64_Init(
    struct mmRbtreeBitsetU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64_Destroy(
    struct mmRbtreeBitsetU64* p);

MM_EXPORT_DLL 
struct mmRbtreeBitsetU64Iterator* 
mmRbtreeBitsetU64_Add(
    struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64_Rmv(
    struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64_Erase(
    struct mmRbtreeBitsetU64* p, 
    struct mmRbtreeBitsetU64Iterator* it);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeBitsetU64_GetInstance(
    struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeBitsetU64_Get(
    const struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
struct mmRbtreeBitsetU64Iterator* 
mmRbtreeBitsetU64_GetIterator(
    const struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetU64_Clear(
    struct mmRbtreeBitsetU64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeBitsetU64_Size(
    const struct mmRbtreeBitsetU64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeBitsetU64Iterator* 
mmRbtreeBitsetU64_Set(
    struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k, 
    mmUInt64_t v);

MM_EXPORT_DLL 
struct mmRbtreeBitsetU64Iterator* 
mmRbtreeBitsetU64_Insert(
    struct mmRbtreeBitsetU64* p, 
    const struct mmBitset* k, 
    mmUInt64_t v);

// mmRbtreeBitsetString
struct mmRbtreeBitsetStringIterator
{
    struct mmBitset k;
    struct mmString v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetStringIterator_Init(
    struct mmRbtreeBitsetStringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetStringIterator_Destroy(
    struct mmRbtreeBitsetStringIterator* p);

struct mmRbtreeBitsetString
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetString_Init(
    struct mmRbtreeBitsetString* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetString_Destroy(
    struct mmRbtreeBitsetString* p);

MM_EXPORT_DLL 
struct mmRbtreeBitsetStringIterator* 
mmRbtreeBitsetString_Add(
    struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetString_Rmv(
    struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetString_Erase(
    struct mmRbtreeBitsetString* p, 
    struct mmRbtreeBitsetStringIterator* it);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeBitsetString_GetInstance(
    struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeBitsetString_Get(
    const struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
struct mmRbtreeBitsetStringIterator* 
mmRbtreeBitsetString_GetIterator(
    const struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetString_Clear(
    struct mmRbtreeBitsetString* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeBitsetString_Size(
    const struct mmRbtreeBitsetString* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeBitsetStringIterator* 
mmRbtreeBitsetString_Set(
    struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k, 
    const struct mmString* v);

MM_EXPORT_DLL 
struct mmRbtreeBitsetStringIterator* 
mmRbtreeBitsetString_Insert(
    struct mmRbtreeBitsetString* p, 
    const struct mmBitset* k, 
    const struct mmString* v);

// mmRbtreeBitsetVpt
struct mmRbtreeBitsetVptIterator
{
    struct mmBitset k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVptIterator_Init(
    struct mmRbtreeBitsetVptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVptIterator_Destroy(
    struct mmRbtreeBitsetVptIterator* p);

struct mmRbtreeBitsetVpt;

struct mmRbtreeBitsetVptAllocator
{
    void* (*Produce)(struct mmRbtreeBitsetVpt* p, const struct mmBitset* k);
    void* (*Recycle)(struct mmRbtreeBitsetVpt* p, const struct mmBitset* k, void* v);
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVptAllocator_Init(
    struct mmRbtreeBitsetVptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVptAllocator_Destroy(
    struct mmRbtreeBitsetVptAllocator* p);

struct mmRbtreeBitsetVpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeBitsetVptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVpt_Init(
    struct mmRbtreeBitsetVpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVpt_Destroy(
    struct mmRbtreeBitsetVpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVpt_SetAllocator(
    struct mmRbtreeBitsetVpt* p, 
    struct mmRbtreeBitsetVptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeBitsetVptIterator* 
mmRbtreeBitsetVpt_Add(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVpt_Rmv(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVpt_Erase(
    struct mmRbtreeBitsetVpt* p, 
    struct mmRbtreeBitsetVptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeBitsetVpt_GetInstance(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void* 
mmRbtreeBitsetVpt_Get(
    const struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
struct mmRbtreeBitsetVptIterator* 
mmRbtreeBitsetVpt_GetIterator(
    const struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void 
mmRbtreeBitsetVpt_Clear(
    struct mmRbtreeBitsetVpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeBitsetVpt_Size(
    const struct mmRbtreeBitsetVpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeBitsetVptIterator* 
mmRbtreeBitsetVpt_Set(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeBitsetVptIterator* 
mmRbtreeBitsetVpt_Insert(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeBitsetVpt_WeakProduce(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k);

MM_EXPORT_DLL 
void* 
mmRbtreeBitsetVpt_WeakRecycle(
    struct mmRbtreeBitsetVpt* p, 
    const struct mmBitset* k, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeBitset_h__
