/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmListVpt.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmListVptIterator_Init(
    struct mmListVptIterator* p)
{
    p->v = 0;
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListVptIterator_Destroy(
    struct mmListVptIterator* p)
{
    p->v = 0;
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListVpt_Init(
    struct mmListVpt* p)
{
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListVpt_Destroy(
    struct mmListVpt* p)
{
    mmListVpt_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListVpt_AddFont(
    struct mmListVpt* p,
    void* v)
{
    struct mmListVptIterator* it; 
    it = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
    mmListVptIterator_Init(it);
    it->v = v;
    mmList_Add(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
void
mmListVpt_AddTail(
    struct mmListVpt* p,
    void* v)
{
    struct mmListVptIterator* it; 
    it = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
    mmListVptIterator_Init(it);
    it->v = v;
    mmList_AddTail(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
void*
mmListVpt_PopFont(
    struct mmListVpt* p)
{
    void* v = 0;
    struct mmListHead* next = p->l.next;
    struct mmListVptIterator* it = mmList_Entry(next, struct mmListVptIterator, n);
    v = it->v;
    mmListVpt_Erase(p, it);
    return v;
}

MM_EXPORT_DLL
void*
mmListVpt_PopTail(
    struct mmListVpt* p)
{
    void* v = 0;
    struct mmListHead* prev = p->l.prev;
    struct mmListVptIterator* it = mmList_Entry(prev, struct mmListVptIterator, n);
    v = it->v;
    mmListVpt_Erase(p, it);
    return v;
}

MM_EXPORT_DLL
void
mmListVpt_Erase(
    struct mmListVpt* p,
    struct mmListVptIterator* it)
{
    mmList_Del(&it->n);
    p->size--;
    mmListVptIterator_Destroy(it);
    mmFree(it);
}

MM_EXPORT_DLL
void
mmListVpt_Clear(
    struct mmListVpt* p)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        mmListVpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmListVpt_Size(
    const struct mmListVpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
void
mmListVpt_Remove(
    struct mmListVpt* p,
    void* v)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        if (it->v == v)
        {
            mmListVpt_Erase(p, it);
        }
    }
}

MM_EXPORT_DLL
void
mmListVpt_InsertPrev(
    struct mmListVpt* p,
    struct mmListVptIterator* i,
    void* v)
{
    struct mmListVptIterator* it;
    it = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
    mmListVptIterator_Init(it);
    it->v = v;
    mmList_AddTail(&it->n, &i->n);
    p->size++;
}

MM_EXPORT_DLL
void
mmListVpt_InsertNext(
    struct mmListVpt* p,
    struct mmListVptIterator* i,
    void* v)
{
    struct mmListVptIterator* it; 
    it = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
    mmListVptIterator_Init(it);
    it->v = v;
    mmList_Add(&it->n, &i->n);
    p->size++;
}
