/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeF32_h__
#define __mmRbtreeF32_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

static 
mmInline 
mmFloat32_t 
mmRbtreeF32CompareFunc(
    mmFloat32_t lhs, 
    mmFloat32_t rhs)
{
    return lhs - rhs;
}

// mmRbtreeF32U32
struct mmRbtreeF32U32Iterator
{
    mmFloat32_t k;
    mmUInt32_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32U32Iterator_Init(
    struct mmRbtreeF32U32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32U32Iterator_Destroy(
    struct mmRbtreeF32U32Iterator* p);

struct mmRbtreeF32U32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32U32_Init(
    struct mmRbtreeF32U32* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32U32_Destroy(
    struct mmRbtreeF32U32* p);

MM_EXPORT_DLL 
struct mmRbtreeF32U32Iterator* 
mmRbtreeF32U32_Add(
    struct mmRbtreeF32U32* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32U32_Rmv(
    struct mmRbtreeF32U32* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32U32_Erase(
    struct mmRbtreeF32U32* p, 
    struct mmRbtreeF32U32Iterator* it);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeF32U32_GetInstance(
    struct mmRbtreeF32U32* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
mmUInt32_t* 
mmRbtreeF32U32_Get(
    const struct mmRbtreeF32U32* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
struct mmRbtreeF32U32Iterator* 
mmRbtreeF32U32_GetIterator(
    const struct mmRbtreeF32U32* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32U32_Clear(
    struct mmRbtreeF32U32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF32U32_Size(
    const struct mmRbtreeF32U32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF32U32Iterator* 
mmRbtreeF32U32_Set(
    struct mmRbtreeF32U32* p, 
    mmFloat32_t k, 
    mmUInt32_t v);

MM_EXPORT_DLL 
struct mmRbtreeF32U32Iterator* 
mmRbtreeF32U32_Insert(
    struct mmRbtreeF32U32* p, 
    mmFloat32_t k, 
    mmUInt32_t v);

// mmRbtreeF32U64
struct mmRbtreeF32U64Iterator
{
    mmFloat32_t k;
    mmUInt64_t v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32U64Iterator_Init(
    struct mmRbtreeF32U64Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32U64Iterator_Destroy(
    struct mmRbtreeF32U64Iterator* p);

struct mmRbtreeF32U64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32U64_Init(
    struct mmRbtreeF32U64* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32U64_Destroy(
    struct mmRbtreeF32U64* p);

MM_EXPORT_DLL 
struct mmRbtreeF32U64Iterator* 
mmRbtreeF32U64_Add(
    struct mmRbtreeF32U64* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32U64_Rmv(
    struct mmRbtreeF32U64* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32U64_Erase(
    struct mmRbtreeF32U64* p, 
    struct mmRbtreeF32U64Iterator* it);

MM_EXPORT_DLL mmUInt64_t* 
mmRbtreeF32U64_GetInstance(
    struct mmRbtreeF32U64* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
mmUInt64_t* 
mmRbtreeF32U64_Get(
    const struct mmRbtreeF32U64* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
struct mmRbtreeF32U64Iterator* 
mmRbtreeF32U64_GetIterator(
    const struct mmRbtreeF32U64* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32U64_Clear(
    struct mmRbtreeF32U64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF32U64_Size(
    const struct mmRbtreeF32U64* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF32U64Iterator* 
mmRbtreeF32U64_Set(
    struct mmRbtreeF32U64* p, 
    mmFloat32_t k, 
    mmUInt64_t v);

MM_EXPORT_DLL 
struct mmRbtreeF32U64Iterator* 
mmRbtreeF32U64_Insert(
    struct mmRbtreeF32U64* p, 
    mmFloat32_t k, 
    mmUInt64_t v);

// mmRbtreeF32String
struct mmRbtreeF32StringIterator
{
    mmFloat32_t k;
    struct mmString v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32StringIterator_Init(
    struct mmRbtreeF32StringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32StringIterator_Destroy(
    struct mmRbtreeF32StringIterator* p);

struct mmRbtreeF32String
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32String_Init(
    struct mmRbtreeF32String* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32String_Destroy(
    struct mmRbtreeF32String* p);

MM_EXPORT_DLL 
struct mmRbtreeF32StringIterator* 
mmRbtreeF32String_Add(
    struct mmRbtreeF32String* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32String_Rmv(
    struct mmRbtreeF32String* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32String_Erase(
    struct mmRbtreeF32String* p, 
    struct mmRbtreeF32StringIterator* it);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeF32String_GetInstance(
    struct mmRbtreeF32String* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
struct mmString* 
mmRbtreeF32String_Get(
    const struct mmRbtreeF32String* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
struct mmRbtreeF32StringIterator* 
mmRbtreeF32String_GetIterator(
    const struct mmRbtreeF32String* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32String_Clear(
    struct mmRbtreeF32String* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF32String_Size(
    const struct mmRbtreeF32String* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF32StringIterator* 
mmRbtreeF32String_Set(
    struct mmRbtreeF32String* p, 
    mmFloat32_t k, 
    struct mmString* v);

MM_EXPORT_DLL 
struct mmRbtreeF32StringIterator* 
mmRbtreeF32String_Insert(
    struct mmRbtreeF32String* p, 
    mmFloat32_t k, 
    struct mmString* v);

// mmRbtreeF32Vpt
struct mmRbtreeF32VptIterator
{
    mmFloat32_t k;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32VptIterator_Init(
    struct mmRbtreeF32VptIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32VptIterator_Destroy(
    struct mmRbtreeF32VptIterator* p);

struct mmRbtreeF32Vpt;

struct mmRbtreeF32VptAllocator
{
    void* (*Produce)(struct mmRbtreeF32Vpt* p, mmFloat32_t k);
    void* (*Recycle)(struct mmRbtreeF32Vpt* p, mmFloat32_t k, void* v);
};

MM_EXPORT_DLL 
void 
mmRbtreeF32VptAllocator_Init(
    struct mmRbtreeF32VptAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32VptAllocator_Destroy(
    struct mmRbtreeF32VptAllocator* p);

struct mmRbtreeF32Vpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeF32VptAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeF32Vpt_Init(
    struct mmRbtreeF32Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32Vpt_Destroy(
    struct mmRbtreeF32Vpt* p);

MM_EXPORT_DLL 
void 
mmRbtreeF32Vpt_SetAllocator(
    struct mmRbtreeF32Vpt* p, 
    struct mmRbtreeF32VptAllocator* allocator);

MM_EXPORT_DLL 
struct mmRbtreeF32VptIterator* 
mmRbtreeF32Vpt_Add(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32Vpt_Rmv(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32Vpt_Erase(
    struct mmRbtreeF32Vpt* p, 
    struct mmRbtreeF32VptIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeF32Vpt_GetInstance(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeF32Vpt_Get(
    const struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
struct mmRbtreeF32VptIterator* 
mmRbtreeF32Vpt_GetIterator(
    const struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void 
mmRbtreeF32Vpt_Clear(
    struct mmRbtreeF32Vpt* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeF32Vpt_Size(
    const struct mmRbtreeF32Vpt* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeF32VptIterator* 
mmRbtreeF32Vpt_Set(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeF32VptIterator* 
mmRbtreeF32Vpt_Insert(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeF32Vpt_WeakProduce(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeF32Vpt_WeakRecycle(
    struct mmRbtreeF32Vpt* p, 
    mmFloat32_t k, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeF32_h__
