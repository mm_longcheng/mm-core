/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorValue_h__
#define __mmVectorValue_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

// We have object Init Destroy for simple assembly Type Allocator.
struct mmVectorValueAllocator
{
    // typedef void(*ProduceFuncType)(void* e);
    void* Produce;

    // typedef void(*RecycleFuncType)(void* e);
    void* Recycle;
};

MM_EXPORT_DLL 
void 
mmVectorValueAllocator_Init(
    struct mmVectorValueAllocator* p);

MM_EXPORT_DLL 
void 
mmVectorValueAllocator_Destroy(
    struct mmVectorValueAllocator* p);

// This object can simple memset 0 for quick initialization.
struct mmVectorValue
{
    struct mmVectorValueAllocator allocator;
    size_t size;
    size_t capacity;
    size_t element;
    mmUInt8_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorValue_Init(
    struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_Destroy(
    struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_Assign(
    struct mmVectorValue* p, 
    const struct mmVectorValue* q);

MM_EXPORT_DLL 
void 
mmVectorValue_SetElement(
    struct mmVectorValue* p, 
    size_t element);

MM_EXPORT_DLL 
void 
mmVectorValue_SetAllocator(
    struct mmVectorValue* p, 
    struct mmVectorValueAllocator* allocator);

MM_EXPORT_DLL 
size_t 
mmVectorValue_GetElement(
    const struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_Clear(
    struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_Reset(
    struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_SetIndex(
    struct mmVectorValue* p, 
    size_t index, 
    void* e);

MM_EXPORT_DLL 
void* 
mmVectorValue_GetIndex(
    const struct mmVectorValue* p, 
    size_t index);

MM_EXPORT_DLL 
void* 
mmVectorValue_GetIndexReference(
    const struct mmVectorValue* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorValue_Realloc(
    struct mmVectorValue* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorValue_Free(
    struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_Resize(
    struct mmVectorValue* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorValue_Reserve(
    struct mmVectorValue* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorValue_AlignedMemory(
    struct mmVectorValue* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorValue_AllocatorMemory(
    struct mmVectorValue* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorValue_PushBack(
    struct mmVectorValue* p, 
    void* e);

MM_EXPORT_DLL 
void* 
mmVectorValue_PopBack(
    struct mmVectorValue* p);

MM_EXPORT_DLL 
void* 
mmVectorValue_Begin(
    const struct mmVectorValue* p);

MM_EXPORT_DLL 
void* 
mmVectorValue_End(
    const struct mmVectorValue* p);

MM_EXPORT_DLL 
void* 
mmVectorValue_Back(
    const struct mmVectorValue* p);

MM_EXPORT_DLL 
void* 
mmVectorValue_Next(
    const struct mmVectorValue* p, 
    void* iter);

MM_EXPORT_DLL 
void* 
mmVectorValue_At(
    const struct mmVectorValue* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorValue_Size(
    const struct mmVectorValue* p);

MM_EXPORT_DLL 
size_t 
mmVectorValue_Capacity(
    const struct mmVectorValue* p);

MM_EXPORT_DLL 
void 
mmVectorValue_Remove(
    struct mmVectorValue* p, 
    size_t index);

MM_EXPORT_DLL 
void* 
mmVectorValue_Erase(
    struct mmVectorValue* p, 
    void* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorValue_Compare(
    const struct mmVectorValue* p, 
    const struct mmVectorValue* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorValue_CopyFromValue(
    struct mmVectorValue* p, 
    const struct mmVectorValue* q);

// copy from p <-> q, use func(void* src, void* dst).
MM_EXPORT_DLL 
void 
mmVectorValue_CopyFromType(
    struct mmVectorValue* p, 
    const struct mmVectorValue* q, 
    void* func);

// vector Null.
#define mmVectorValue_Null { { NULL, NULL, }, 0, 0, 0, NULL, }

// make a const weak vector.
#define mmVectorValue_Make(v, t)              \
{                                             \
    { NULL, NULL, },       /* .allocator = */ \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .capacity  = */ \
    sizeof(t),             /* .element   = */ \
    (mmUInt8_t*)v,         /* .arrays    = */ \
}

// make a const weak null vector for type.
#define mmVectorValue_MakeNull(t) { { NULL, NULL, }, 0, 0, sizeof(t), NULL, }

#include "core/mmSuffix.h"

#endif//__mmVectorValue_h__
