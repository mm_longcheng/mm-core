/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeIntervalU32_h__
#define __mmRbtreeIntervalU32_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
mmSInt32_t 
mmRbtreeIntervalU32CompareFunc(
    mmUInt32_t ll, mmUInt32_t lr, 
    mmUInt32_t rl, mmUInt32_t rr);

// mmRbtreeIntervalU32
struct mmRbtreeIntervalU32Iterator
{
    mmUInt32_t l;
    mmUInt32_t r;
    void* v;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32Iterator_Init(
    struct mmRbtreeIntervalU32Iterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32Iterator_Destroy(
    struct mmRbtreeIntervalU32Iterator* p);

struct mmRbtreeIntervalU32;

struct mmRbtreeIntervalU32Allocator
{
    void* (*Produce)(struct mmRbtreeIntervalU32* p, mmUInt32_t l, mmUInt32_t r);
    void* (*Recycle)(struct mmRbtreeIntervalU32* p, mmUInt32_t l, mmUInt32_t r, void* v);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32Allocator_Init(
    struct mmRbtreeIntervalU32Allocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32Allocator_Destroy(
    struct mmRbtreeIntervalU32Allocator* p);

struct mmRbtreeIntervalU32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeIntervalU32Allocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32_Init(
    struct mmRbtreeIntervalU32* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32_Destroy(
    struct mmRbtreeIntervalU32* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32_SetAllocator(
    struct mmRbtreeIntervalU32* p, 
    struct mmRbtreeIntervalU32Allocator* allocator);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU32_Add(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32_Rmv(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32_Erase(
    struct mmRbtreeIntervalU32* p, 
    struct mmRbtreeIntervalU32Iterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU32_GetInstance(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU32_Get(
    const struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r);

MM_EXPORT_DLL 
struct mmRbtreeIntervalU32Iterator* 
mmRbtreeIntervalU32_GetIterator(
    const struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalU32_Clear(
    struct mmRbtreeIntervalU32* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeIntervalU32_Size(
    const struct mmRbtreeIntervalU32* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeIntervalU32Iterator*
mmRbtreeIntervalU32_Set(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r, 
    void* v);

MM_EXPORT_DLL 
struct mmRbtreeIntervalU32Iterator*
mmRbtreeIntervalU32_Insert(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r, 
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU32_WeakProduce(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalU32_WeakRecycle(
    struct mmRbtreeIntervalU32* p, 
    mmUInt32_t l, mmUInt32_t r, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeIntervalU32_h__
