/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorU32_h__
#define __mmVectorU32_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorU32
{
    size_t size;
    size_t capacity;
    mmUInt32_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorU32_Init(
    struct mmVectorU32* p);

MM_EXPORT_DLL 
void 
mmVectorU32_Destroy(
    struct mmVectorU32* p);

MM_EXPORT_DLL 
void 
mmVectorU32_Assign(
    struct mmVectorU32* p, 
    const struct mmVectorU32* q);

MM_EXPORT_DLL 
void 
mmVectorU32_Clear(
    struct mmVectorU32* p);

MM_EXPORT_DLL 
void 
mmVectorU32_Reset(
    struct mmVectorU32* p);

MM_EXPORT_DLL 
void 
mmVectorU32_SetIndex(
    struct mmVectorU32* p, 
    size_t index, 
    mmUInt32_t e);

MM_EXPORT_DLL 
mmUInt32_t 
mmVectorU32_GetIndex(
    const struct mmVectorU32* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt32_t* 
mmVectorU32_GetIndexReference(
    const struct mmVectorU32* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorU32_Realloc(
    struct mmVectorU32* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU32_Free(
    struct mmVectorU32* p);

MM_EXPORT_DLL 
void 
mmVectorU32_Resize(
    struct mmVectorU32* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU32_Reserve(
    struct mmVectorU32* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU32_AlignedMemory(
    struct mmVectorU32* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU32_AllocatorMemory(
    struct mmVectorU32* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU32_PushBack(
    struct mmVectorU32* p, 
    mmUInt32_t e);

MM_EXPORT_DLL 
mmUInt32_t 
mmVectorU32_PopBack(
    struct mmVectorU32* p);

MM_EXPORT_DLL 
mmUInt32_t* 
mmVectorU32_Begin(
    const struct mmVectorU32* p);

MM_EXPORT_DLL 
mmUInt32_t* 
mmVectorU32_End(
    const struct mmVectorU32* p);

MM_EXPORT_DLL 
mmUInt32_t* 
mmVectorU32_Back(
    const struct mmVectorU32* p);

MM_EXPORT_DLL 
mmUInt32_t* 
mmVectorU32_Next(
    const struct mmVectorU32* p, 
    mmUInt32_t* iter);

MM_EXPORT_DLL 
mmUInt32_t 
mmVectorU32_At(
    const struct mmVectorU32* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorU32_Size(
    const struct mmVectorU32* p);

MM_EXPORT_DLL 
size_t 
mmVectorU32_Capacity(
    const struct mmVectorU32* p);

MM_EXPORT_DLL 
void 
mmVectorU32_Remove(
    struct mmVectorU32* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt32_t* 
mmVectorU32_Erase(
    struct mmVectorU32* p, 
    mmUInt32_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorU32_Compare(
    const struct mmVectorU32* p, 
    const struct mmVectorU32* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorU32_CopyFromValue(
    struct mmVectorU32* p, 
    const struct mmVectorU32* q);

// vector Null.
#define mmVectorU32_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorU32_Make(v)                   \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (mmUInt32_t*)v,        /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorU32_h__
