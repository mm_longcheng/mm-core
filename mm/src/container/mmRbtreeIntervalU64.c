/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeIntervalU64.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
mmSInt64_t
mmRbtreeIntervalU64CompareFunc(
    mmUInt64_t ll, mmUInt64_t lr,
    mmUInt64_t rl, mmUInt64_t rr)
{
    if (rl <= ll && lr <= rr)
    {
        return 0;
    }
    else if (ll > rr)
    {
        return ll - rr;
    }
    else
    {
        return lr - rl;
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64Iterator_Init(
    struct mmRbtreeIntervalU64Iterator* p)
{
    p->l = 0;
    p->r = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64Iterator_Destroy(
    struct mmRbtreeIntervalU64Iterator* p)
{
    p->l = 0;
    p->r = 0;
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64Allocator_Init(
    struct mmRbtreeIntervalU64Allocator* p)
{
    p->Produce = &mmRbtreeIntervalU64_WeakProduce;
    p->Recycle = &mmRbtreeIntervalU64_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64Allocator_Destroy(
    struct mmRbtreeIntervalU64Allocator* p)
{
    p->Produce = &mmRbtreeIntervalU64_WeakProduce;
    p->Recycle = &mmRbtreeIntervalU64_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64_Init(
    struct mmRbtreeIntervalU64* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalU64Allocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64_Destroy(
    struct mmRbtreeIntervalU64* p)
{
    mmRbtreeIntervalU64_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeIntervalU64Allocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64_SetAllocator(
    struct mmRbtreeIntervalU64* p,
    struct mmRbtreeIntervalU64Allocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU64_Add(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalU64Iterator, n);
        result = mmRbtreeIntervalU64CompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeIntervalU64Iterator*)mmMalloc(sizeof(struct mmRbtreeIntervalU64Iterator));
    mmRbtreeIntervalU64Iterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = (*(p->allocator.Produce))(p, l, r);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it->v;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64_Rmv(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r)
{
    struct mmRbtreeIntervalU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalU64Iterator, n);
        result = mmRbtreeIntervalU64CompareFunc(l, r, it->l, it->r);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeIntervalU64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64_Erase(
    struct mmRbtreeIntervalU64* p,
    struct mmRbtreeIntervalU64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->l, it->r, it->v);
    mmRbtreeIntervalU64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU64_GetInstance(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r)
{
    void* v = mmRbtreeIntervalU64_Get(p, l, r);
    if (NULL == v)
    {
        v = mmRbtreeIntervalU64_Add(p, l, r);
    }
    return v;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU64_Get(
    const struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r)
{
    struct mmRbtreeIntervalU64Iterator* it = mmRbtreeIntervalU64_GetIterator(p, l, r);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalU64Iterator*
mmRbtreeIntervalU64_GetIterator(
    const struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r)
{
    struct mmRbtreeIntervalU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeIntervalU64Iterator, n);
        result = mmRbtreeIntervalU64CompareFunc(l, r, it->l, it->r);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeIntervalU64_Clear(
    struct mmRbtreeIntervalU64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeIntervalU64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeIntervalU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeIntervalU64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeIntervalU64_Size(
    const struct mmRbtreeIntervalU64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalU64Iterator*
mmRbtreeIntervalU64_Set(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalU64Iterator, n);
        result = mmRbtreeIntervalU64CompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeIntervalU64Iterator*)mmMalloc(sizeof(struct mmRbtreeIntervalU64Iterator));
    mmRbtreeIntervalU64Iterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeIntervalU64Iterator*
mmRbtreeIntervalU64_Insert(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeIntervalU64Iterator* it = NULL;
    mmSInt64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeIntervalU64Iterator, n);
        result = mmRbtreeIntervalU64CompareFunc(l, r, it->l, it->r);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            return NULL;
        }
    }
    it = (struct mmRbtreeIntervalU64Iterator*)mmMalloc(sizeof(struct mmRbtreeIntervalU64Iterator));
    mmRbtreeIntervalU64Iterator_Init(it);
    it->l = l;
    it->r = r;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    it->v = v;
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU64_WeakProduce(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeIntervalU64_WeakRecycle(
    struct mmRbtreeIntervalU64* p,
    mmUInt64_t l, mmUInt64_t r,
    void* v)
{
    return v;
}

