/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBitset_h__
#define __mmBitset_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmUtf32String.h"

#include "core/mmPrefix.h"


/*
*  come from boost::dynamic_bitset.
*  https://www.boost.org
*  less depend.
*
*  I change is_subset_of is_proper_subset_of api. not assert the size.
*  mm_dynamic_bitset<mmUInt32_t> x0(32, 0xFF);
*  mm_dynamic_bitset<mmUInt32_t> x1(16, 0x0F);
*  bool value = x1.is_subset_of(x0);// value = true.
*
*  we use mmUtf32String for bit block.
*/

// block bit 32.
#define MM_BITSET_BLOCK 32
#define MM_BITSET_NPOS -1

struct mmBitset
{
    struct mmUtf32String v;
    size_t n;
};

MM_EXPORT_DLL 
void 
mmBitset_Init(
    struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Destroy(
    struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Assign(
    struct mmBitset* p, 
    const struct mmBitset* q);

MM_EXPORT_DLL 
void 
mmBitset_Clear(
    struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Reset(
    struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Resize(
    struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
size_t 
mmBitset_Size(
    const struct mmBitset* p);

MM_EXPORT_DLL 
size_t 
mmBitset_BlockNumber(
    const struct mmBitset* p);

MM_EXPORT_DLL 
int 
mmBitset_Empty(
    const struct mmBitset* p);

MM_EXPORT_DLL 
size_t 
mmBitset_Capacity(
    const struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Reserve(
    struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
void 
mmBitset_Set(
    struct mmBitset* p, 
    size_t n, 
    int v);

MM_EXPORT_DLL 
int 
mmBitset_Get(
    const struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
void 
mmBitset_BitReset(
    struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Flip(
    struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
void 
mmBitset_FlipAll(
    struct mmBitset* p);

MM_EXPORT_DLL 
int 
mmBitset_All(
    const struct mmBitset* p);

MM_EXPORT_DLL 
int 
mmBitset_Any(
    const struct mmBitset* p);

MM_EXPORT_DLL 
int 
mmBitset_None(
    const struct mmBitset* p);

MM_EXPORT_DLL 
size_t 
mmBitset_Count(
    const struct mmBitset* p);

MM_EXPORT_DLL 
void 
mmBitset_Append(
    struct mmBitset* p, 
    mmUInt32_t value);

// MM_TRUE or MM_FALSE.
MM_EXPORT_DLL 
int 
mmBitset_IsSubsetOf(
    const struct mmBitset* p, 
    const struct mmBitset* q);

MM_EXPORT_DLL 
int 
mmBitset_IsProperSubsetOf(
    const struct mmBitset* p, 
    const struct mmBitset* q);

MM_EXPORT_DLL 
int 
mmBitset_Intersects(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// lookup
MM_EXPORT_DLL 
size_t 
mmBitset_FindFirst(
    const struct mmBitset* p);

MM_EXPORT_DLL 
size_t 
mmBitset_FindNext(
    const struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
void 
mmBitset_UpdateSize(
    struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
void 
mmBitset_ZeroUnusedBits(
    struct mmBitset* p);

MM_EXPORT_DLL 
mmUInt32_t* 
mmBitset_HighestBlock(
    const struct mmBitset* p);

MM_EXPORT_DLL 
size_t 
mmBitset_CountExtraBits(
    const struct mmBitset* p);

// MM_TRUE is valid.MM_FALSE is invalid.
MM_EXPORT_DLL 
int 
mmBitset_CheckInvariants(
    const struct mmBitset* p);

MM_EXPORT_DLL 
size_t 
mmBitset_DoFindFrom(
    const struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
size_t 
mmBitset_DoCount(
    const struct mmBitset* p, 
    mmUInt32_t* first, 
    size_t length);

MM_EXPORT_DLL 
size_t 
mmBitset_Count0(
    const struct mmBitset* p, 
    size_t until);

MM_EXPORT_DLL 
size_t 
mmBitset_Count1(
    const struct mmBitset* p, 
    size_t until);

// comparison
// compare
MM_EXPORT_DLL 
int 
mmBitset_Compare(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// ==
MM_EXPORT_DLL 
int 
mmBitset_OperatorEqual(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// !=
MM_EXPORT_DLL 
int 
mmBitset_OperatorNotEqual(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// <
MM_EXPORT_DLL 
int 
mmBitset_OperatorLess(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// <=
MM_EXPORT_DLL 
int 
mmBitset_OperatorLessEqual(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// >
MM_EXPORT_DLL 
int 
mmBitset_OperatorGreater(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// >=
MM_EXPORT_DLL 
int 
mmBitset_OperatorGreaterEqual(
    const struct mmBitset* p, 
    const struct mmBitset* q);

// to_string
MM_EXPORT_DLL 
size_t 
mmBitset_ToString(
    const struct mmBitset* p, 
    struct mmString* s);

// bit &
MM_EXPORT_DLL 
void 
mmBitset_BitAnd(
    struct mmBitset* p, 
    const struct mmBitset* q);

// bit |
MM_EXPORT_DLL 
void 
mmBitset_BitOr(
    struct mmBitset* p, 
    const struct mmBitset* q);

// bit ^
MM_EXPORT_DLL 
void 
mmBitset_BitNot(
    struct mmBitset* p, 
    const struct mmBitset* q);

// bit -
MM_EXPORT_DLL 
void 
mmBitset_BitSub(
    struct mmBitset* p, 
    const struct mmBitset* q);

// bit +
MM_EXPORT_DLL 
void 
mmBitset_BitAdd(
    struct mmBitset* p, 
    const struct mmBitset* q);

// << Left shift
MM_EXPORT_DLL 
void 
mmBitset_BitLShift(
    struct mmBitset* p, 
    size_t n);

// >> Right shift
MM_EXPORT_DLL 
void 
mmBitset_BitRShift(
    struct mmBitset* p, 
    size_t n);

MM_EXPORT_DLL 
size_t 
mmBitset_CalculateBlockNumber(
    size_t bit_number);

MM_EXPORT_DLL 
size_t 
mmBitset_CalculateBlockIndex(
    size_t n);

MM_EXPORT_DLL 
size_t 
mmBitset_CalculateBitIndex(
    size_t n);

MM_EXPORT_DLL 
mmUInt32_t 
mmBitset_CalculateBitMask(
    size_t n);

#include "core/mmSuffix.h"

#endif//__mmBitset_h__
