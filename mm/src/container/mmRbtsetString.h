/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtsetString_h__
#define __mmRbtsetString_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
mmSInt32_t 
mmRbtsetStringCompareFunc(
    const struct mmString* lhs, 
    const struct mmString* rhs);

// mmRbtsetString
struct mmRbtsetStringIterator
{
    struct mmString k;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtsetStringIterator_Init(
    struct mmRbtsetStringIterator* p);

MM_EXPORT_DLL 
void 
mmRbtsetStringIterator_Destroy(
    struct mmRbtsetStringIterator* p);

struct mmRbtsetString
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtsetString_Init(
    struct mmRbtsetString* p);

MM_EXPORT_DLL 
void 
mmRbtsetString_Destroy(
    struct mmRbtsetString* p);

MM_EXPORT_DLL 
struct mmRbtsetStringIterator* 
mmRbtsetString_Add(
    struct mmRbtsetString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtsetString_Rmv(
    struct mmRbtsetString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtsetString_Erase(
    struct mmRbtsetString* p, 
    struct mmRbtsetStringIterator* it);

// return 1 if have member.0 is not member.
MM_EXPORT_DLL 
int 
mmRbtsetString_Get(
    const struct mmRbtsetString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
struct mmRbtsetStringIterator* 
mmRbtsetString_GetIterator(
    const struct mmRbtsetString* p, 
    const struct mmString* k);

MM_EXPORT_DLL 
void 
mmRbtsetString_Clear(
    struct mmRbtsetString* p);

MM_EXPORT_DLL 
size_t 
mmRbtsetString_Size(
    const struct mmRbtsetString* p);

MM_EXPORT_DLL 
struct mmRbtsetStringIterator* 
mmRbtsetString_Insert(
    struct mmRbtsetString* p, 
    const struct mmString* k);

// mmRbtsetStr
struct mmRbtsetStrIterator
{
    const char* k;
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtsetStrIterator_Init(
    struct mmRbtsetStrIterator* p);

MM_EXPORT_DLL 
void 
mmRbtsetStrIterator_Destroy(
    struct mmRbtsetStrIterator* p);

struct mmRbtsetStr
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtsetStr_Init(
    struct mmRbtsetStr* p);

MM_EXPORT_DLL 
void 
mmRbtsetStr_Destroy(
    struct mmRbtsetStr* p);

MM_EXPORT_DLL 
struct mmRbtsetStrIterator* 
mmRbtsetStr_Add(
    struct mmRbtsetStr* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtsetStr_Rmv(
    struct mmRbtsetStr* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtsetStr_Erase(
    struct mmRbtsetStr* p, 
    struct mmRbtsetStrIterator* it);

// return 1 if have member.0 is not member.
MM_EXPORT_DLL 
int 
mmRbtsetStr_Get(
    const struct mmRbtsetStr* p, 
    const char* k);

MM_EXPORT_DLL 
struct mmRbtsetStrIterator* 
mmRbtsetStr_GetIterator(
    const struct mmRbtsetStr* p, 
    const char* k);

MM_EXPORT_DLL 
void 
mmRbtsetStr_Clear(
    struct mmRbtsetStr* p);

MM_EXPORT_DLL 
size_t 
mmRbtsetStr_Size(
    const struct mmRbtsetStr* p);

MM_EXPORT_DLL 
struct mmRbtsetStrIterator* 
mmRbtsetStr_Insert(
    struct mmRbtsetStr* p, 
    const char* k);

#include "core/mmSuffix.h"

#endif//__mmRbtsetString_h__
