/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeString.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
mmSInt32_t
mmRbtreeStringCompareFunc(
    const struct mmString* lhs,
    const struct mmString* rhs)
{
    return (mmSInt32_t)mmString_Compare(lhs, rhs);
}

MM_EXPORT_DLL
void
mmRbtreeStringU32Iterator_Init(
    struct mmRbtreeStringU32Iterator* p)
{
    mmString_Init(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringU32Iterator_Destroy(
    struct mmRbtreeStringU32Iterator* p)
{
    mmString_Destroy(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringU32_Init(
    struct mmRbtreeStringU32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStringU32_Destroy(
    struct mmRbtreeStringU32* p)
{
    mmRbtreeStringU32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeStringU32Iterator*
mmRbtreeStringU32_Add(
    struct mmRbtreeStringU32* p,
    const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringU32Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeStringU32Iterator*)mmMalloc(sizeof(struct mmRbtreeStringU32Iterator));
    mmRbtreeStringU32Iterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringU32_Rmv(
    struct mmRbtreeStringU32* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringU32Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeStringU32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeStringU32_Erase(
    struct mmRbtreeStringU32* p,
    struct mmRbtreeStringU32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeStringU32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeStringU32_GetInstance(
    struct mmRbtreeStringU32* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU32Iterator* it = mmRbtreeStringU32_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeStringU32_Get(
    const struct mmRbtreeStringU32* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU32Iterator* it = mmRbtreeStringU32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeStringU32Iterator*
mmRbtreeStringU32_GetIterator(
    const struct mmRbtreeStringU32* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringU32Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStringU32_Clear(
    struct mmRbtreeStringU32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringU32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeStringU32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeStringU32_Size(
    const struct mmRbtreeStringU32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeStringU32Iterator*
mmRbtreeStringU32_Set(
    struct mmRbtreeStringU32* p,
    const struct mmString* k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringU32Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeStringU32Iterator*)mmMalloc(sizeof(struct mmRbtreeStringU32Iterator));
    mmRbtreeStringU32Iterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeStringU32Iterator*
mmRbtreeStringU32_Insert(
    struct mmRbtreeStringU32* p,
    const struct mmString* k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringU32Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeStringU32Iterator*)mmMalloc(sizeof(struct mmRbtreeStringU32Iterator));
    mmRbtreeStringU32Iterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringU64Iterator_Init(
    struct mmRbtreeStringU64Iterator* p)
{
    mmString_Init(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringU64Iterator_Destroy(
    struct mmRbtreeStringU64Iterator* p)
{
    mmString_Destroy(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringU64_Init(
    struct mmRbtreeStringU64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStringU64_Destroy(
    struct mmRbtreeStringU64* p)
{
    mmRbtreeStringU64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeStringU64Iterator*
mmRbtreeStringU64_Add(
    struct mmRbtreeStringU64* p,
    const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringU64Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeStringU64Iterator*)mmMalloc(sizeof(struct mmRbtreeStringU64Iterator));
    mmRbtreeStringU64Iterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringU64_Rmv(
    struct mmRbtreeStringU64* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringU64Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeStringU64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeStringU64_Erase(
    struct mmRbtreeStringU64* p,
    struct mmRbtreeStringU64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeStringU64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeStringU64_GetInstance(
    struct mmRbtreeStringU64* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU64Iterator* it = mmRbtreeStringU64_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeStringU64_Get(
    const struct mmRbtreeStringU64* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU64Iterator* it = mmRbtreeStringU64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeStringU64Iterator*
mmRbtreeStringU64_GetIterator(
    const struct mmRbtreeStringU64* p,
    const struct mmString* k)
{
    struct mmRbtreeStringU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringU64Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStringU64_Clear(
    struct mmRbtreeStringU64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringU64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeStringU64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeStringU64_Size(
    const struct mmRbtreeStringU64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeStringU64Iterator*
mmRbtreeStringU64_Set(
    struct mmRbtreeStringU64* p,
    const struct mmString* k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringU64Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeStringU64Iterator*)mmMalloc(sizeof(struct mmRbtreeStringU64Iterator));
    mmRbtreeStringU64Iterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeStringU64Iterator*
mmRbtreeStringU64_Insert(
    struct mmRbtreeStringU64* p,
    const struct mmString* k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringU64Iterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeStringU64Iterator*)mmMalloc(sizeof(struct mmRbtreeStringU64Iterator));
    mmRbtreeStringU64Iterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringStringIterator_Init(
    struct mmRbtreeStringStringIterator* p)
{
    mmString_Init(&p->k);
    mmString_Init(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringStringIterator_Destroy(
    struct mmRbtreeStringStringIterator* p)
{
    mmString_Destroy(&p->k);
    mmString_Destroy(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringString_Init(
    struct mmRbtreeStringString* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStringString_Destroy(
    struct mmRbtreeStringString* p)
{
    mmRbtreeStringString_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeStringStringIterator*
mmRbtreeStringString_Add(
    struct mmRbtreeStringString* p,
    const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringStringIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeStringStringIterator*)mmMalloc(sizeof(struct mmRbtreeStringStringIterator));
    mmRbtreeStringStringIterator_Init(it);
    mmString_Assign(&it->k, k);
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringString_Rmv(
    struct mmRbtreeStringString* p,
    const struct mmString* k)
{
    struct mmRbtreeStringStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringStringIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeStringString_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeStringString_Erase(
    struct mmRbtreeStringString* p,
    struct mmRbtreeStringStringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeStringStringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeStringString_GetInstance(
    struct mmRbtreeStringString* p,
    const struct mmString* k)
{
    struct mmRbtreeStringStringIterator* it = mmRbtreeStringString_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeStringString_Get(
    const struct mmRbtreeStringString* p,
    const struct mmString* k)
{
    struct mmRbtreeStringStringIterator* it = mmRbtreeStringString_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeStringStringIterator*
mmRbtreeStringString_GetIterator(
    const struct mmRbtreeStringString* p,
    const struct mmString* k)
{
    struct mmRbtreeStringStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringStringIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStringString_Clear(
    struct mmRbtreeStringString* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringStringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringStringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeStringString_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeStringString_Size(
    const struct mmRbtreeStringString* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeStringStringIterator*
mmRbtreeStringString_Set(
    struct mmRbtreeStringString* p,
    const struct mmString* k,
    const struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringStringIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return it;
        }
    }
    it = (struct mmRbtreeStringStringIterator*)mmMalloc(sizeof(struct mmRbtreeStringStringIterator));
    mmRbtreeStringStringIterator_Init(it);
    mmString_Assign(&it->k, k);
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeStringStringIterator*
mmRbtreeStringString_Insert(
    struct mmRbtreeStringString* p,
    const struct mmString* k,
    const struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringStringIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeStringStringIterator*)mmMalloc(sizeof(struct mmRbtreeStringStringIterator));
    mmRbtreeStringStringIterator_Init(it);
    mmString_Assign(&it->k, k);
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringVptIterator_Init(
    struct mmRbtreeStringVptIterator* p)
{
    mmString_Init(&p->k);
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringVptIterator_Destroy(
    struct mmRbtreeStringVptIterator* p)
{
    mmString_Destroy(&p->k);
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStringVptAllocator_Init(
    struct mmRbtreeStringVptAllocator* p)
{
    p->Produce = &mmRbtreeStringVpt_WeakProduce;
    p->Recycle = &mmRbtreeStringVpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeStringVptAllocator_Destroy(
    struct mmRbtreeStringVptAllocator* p)
{
    p->Produce = &mmRbtreeStringVpt_WeakProduce;
    p->Recycle = &mmRbtreeStringVpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeStringVpt_Init(
    struct mmRbtreeStringVpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeStringVptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStringVpt_Destroy(
    struct mmRbtreeStringVpt* p)
{
    mmRbtreeStringVpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeStringVptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStringVpt_SetAllocator(
    struct mmRbtreeStringVpt* p,
    struct mmRbtreeStringVptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeStringVptIterator*
mmRbtreeStringVpt_Add(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeStringVptIterator*)mmMalloc(sizeof(struct mmRbtreeStringVptIterator));
    mmRbtreeStringVptIterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStringVpt_Rmv(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeStringVpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeStringVpt_Erase(
    struct mmRbtreeStringVpt* p,
    struct mmRbtreeStringVptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, &it->k, it->v);
    mmRbtreeStringVptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeStringVpt_GetInstance(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeStringVptIterator* it = mmRbtreeStringVpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeStringVpt_Get(
    const struct mmRbtreeStringVpt* p,
    const struct mmString* k)
{
    struct mmRbtreeStringVptIterator* it = mmRbtreeStringVpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeStringVptIterator*
mmRbtreeStringVpt_GetIterator(
    const struct mmRbtreeStringVpt* p,
    const struct mmString* k)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStringVpt_Clear(
    struct mmRbtreeStringVpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeStringVpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeStringVpt_Size(
    const struct mmRbtreeStringVpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeStringVptIterator*
mmRbtreeStringVpt_Set(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeStringVptIterator*)mmMalloc(sizeof(struct mmRbtreeStringVptIterator));
    mmRbtreeStringVptIterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeStringVptIterator*
mmRbtreeStringVpt_Insert(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeStringVptIterator*)mmMalloc(sizeof(struct mmRbtreeStringVptIterator));
    mmRbtreeStringVptIterator_Init(it);
    mmString_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeStringVpt_WeakProduce(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeStringVpt_WeakRecycle(
    struct mmRbtreeStringVpt* p,
    const struct mmString* k,
    void* v)
{
    return v;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptIterator_Init(
    struct mmRbtreeWeakStringVptIterator* p)
{
    p->k = NULL;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptIterator_Destroy(
    struct mmRbtreeWeakStringVptIterator* p)
{
    p->k = NULL;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptAllocator_Init(
    struct mmRbtreeWeakStringVptAllocator* p)
{
    p->Produce = &mmRbtreeWeakStringVpt_WeakProduce;
    p->Recycle = &mmRbtreeWeakStringVpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVptAllocator_Destroy(
    struct mmRbtreeWeakStringVptAllocator* p)
{
    p->Produce = &mmRbtreeWeakStringVpt_WeakProduce;
    p->Recycle = &mmRbtreeWeakStringVpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Init(
    struct mmRbtreeWeakStringVpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeWeakStringVptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Destroy(
    struct mmRbtreeWeakStringVpt* p)
{
    mmRbtreeWeakStringVpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeWeakStringVptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_SetAllocator(
    struct mmRbtreeWeakStringVpt* p,
    struct mmRbtreeWeakStringVptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_Add(
        struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeWeakStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeWeakStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeWeakStringVptIterator*)mmMalloc(sizeof(struct mmRbtreeWeakStringVptIterator));
    mmRbtreeWeakStringVptIterator_Init(it);
    it->k = k;
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Rmv(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k)
{
    struct mmRbtreeWeakStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeWeakStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeWeakStringVpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Erase(
    struct mmRbtreeWeakStringVpt* p,
    struct mmRbtreeWeakStringVptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->k, it->v);
    mmRbtreeWeakStringVptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_GetInstance(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeWeakStringVptIterator* it = mmRbtreeWeakStringVpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_Get(
    const struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k)
{
    struct mmRbtreeWeakStringVptIterator* it = mmRbtreeWeakStringVpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_GetIterator(
        const struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k)
{
    struct mmRbtreeWeakStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeWeakStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeWeakStringVpt_Clear(
    struct mmRbtreeWeakStringVpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeWeakStringVptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeWeakStringVptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeWeakStringVpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeWeakStringVpt_Size(
    const struct mmRbtreeWeakStringVpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_Set(
        struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k,
        void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeWeakStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeWeakStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeWeakStringVptIterator*)mmMalloc(sizeof(struct mmRbtreeWeakStringVptIterator));
    mmRbtreeWeakStringVptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeWeakStringVptIterator*
    mmRbtreeWeakStringVpt_Insert(
        struct mmRbtreeWeakStringVpt* p,
        const struct mmString* k,
        void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeWeakStringVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeWeakStringVptIterator, n);
        result = mmRbtreeStringCompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeWeakStringVptIterator*)mmMalloc(sizeof(struct mmRbtreeWeakStringVptIterator));
    mmRbtreeWeakStringVptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_WeakProduce(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeWeakStringVpt_WeakRecycle(
    struct mmRbtreeWeakStringVpt* p,
    const struct mmString* k,
    void* v)
{
    return v;
}

MM_EXPORT_DLL
void
mmRbtreeStrStrIterator_Init(
    struct mmRbtreeStrStrIterator* p)
{
    p->k = "";
    p->v = "";
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStrStrIterator_Destroy(
    struct mmRbtreeStrStrIterator* p)
{
    p->k = "";
    p->v = "";
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStrStr_Init(
    struct mmRbtreeStrStr* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStrStr_Destroy(
    struct mmRbtreeStrStr* p)
{
    mmRbtreeStrStr_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeStrStrIterator*
mmRbtreeStrStr_Add(
    struct mmRbtreeStrStr* p,
    const char* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStrStrIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStrStrIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeStrStrIterator*)mmMalloc(sizeof(struct mmRbtreeStrStrIterator));
    mmRbtreeStrStrIterator_Init(it);
    it->k = k;
    it->v = "";
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStrStr_Rmv(
    struct mmRbtreeStrStr* p,
    const char* k)
{
    struct mmRbtreeStrStrIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStrStrIterator, n);
        result = strcmp(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeStrStr_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeStrStr_Erase(
    struct mmRbtreeStrStr* p,
    struct mmRbtreeStrStrIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeStrStrIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
const char*
mmRbtreeStrStr_GetInstance(
    struct mmRbtreeStrStr* p,
    const char* k)
{
    struct mmRbtreeStrStrIterator* it = mmRbtreeStrStr_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
const char*
mmRbtreeStrStr_Get(
    const struct mmRbtreeStrStr* p,
    const char* k)
{
    struct mmRbtreeStrStrIterator* it = mmRbtreeStrStr_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeStrStrIterator*
mmRbtreeStrStr_GetIterator(
    const struct mmRbtreeStrStr* p,
    const char* k)
{
    struct mmRbtreeStrStrIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStrStrIterator, n);
        result = strcmp(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStrStr_Clear(
    struct mmRbtreeStrStr* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStrStrIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStrStrIterator, n);
        n = mmRb_Next(n);
        mmRbtreeStrStr_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeStrStr_Size(
    const struct mmRbtreeStrStr* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeStrStrIterator*
mmRbtreeStrStr_Set(
    struct mmRbtreeStrStr* p,
    const char* k,
    const char* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStrStrIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStrStrIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeStrStrIterator*)mmMalloc(sizeof(struct mmRbtreeStrStrIterator));
    mmRbtreeStrStrIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeStrStrIterator*
mmRbtreeStrStr_Insert(
    struct mmRbtreeStrStr* p,
    const char* k,
    const char* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStrStrIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStrStrIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeStrStrIterator*)mmMalloc(sizeof(struct mmRbtreeStrStrIterator));
    mmRbtreeStrStrIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStrVptIterator_Init(
    struct mmRbtreeStrVptIterator* p)
{
    p->k = "";
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStrVptIterator_Destroy(
    struct mmRbtreeStrVptIterator* p)
{
    p->k = "";
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeStrVptAllocator_Init(
    struct mmRbtreeStrVptAllocator* p)
{
    p->Produce = &mmRbtreeStrVpt_WeakProduce;
    p->Recycle = &mmRbtreeStrVpt_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStrVptAllocator_Destroy(
    struct mmRbtreeStrVptAllocator* p)
{
    p->Produce = &mmRbtreeStrVpt_WeakProduce;
    p->Recycle = &mmRbtreeStrVpt_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStrVpt_Init(
    struct mmRbtreeStrVpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeStrVptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStrVpt_Destroy(
    struct mmRbtreeStrVpt* p)
{
    mmRbtreeStrVpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeStrVptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeStrVpt_SetAllocator(
    struct mmRbtreeStrVpt* p,
    struct mmRbtreeStrVptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeStrVptIterator*
mmRbtreeStrVpt_Add(
    struct mmRbtreeStrVpt* p,
    const char* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStrVptIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeStrVptIterator*)mmMalloc(sizeof(struct mmRbtreeStrVptIterator));
    mmRbtreeStrVptIterator_Init(it);
    it->k = k;
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeStrVpt_Rmv(
    struct mmRbtreeStrVpt* p,
    const char* k)
{
    struct mmRbtreeStrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStrVptIterator, n);
        result = strcmp(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeStrVpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeStrVpt_Erase(
    struct mmRbtreeStrVpt* p,
    struct mmRbtreeStrVptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->k, it->v);
    mmRbtreeStrVptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeStrVpt_GetInstance(
    struct mmRbtreeStrVpt* p,
    const char* k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeStrVptIterator* it = mmRbtreeStrVpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeStrVpt_Get(
    const struct mmRbtreeStrVpt* p,
    const char* k)
{
    struct mmRbtreeStrVptIterator* it = mmRbtreeStrVpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeStrVptIterator*
mmRbtreeStrVpt_GetIterator(
    const struct mmRbtreeStrVpt* p,
    const char* k)
{
    struct mmRbtreeStrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeStrVptIterator, n);
        result = strcmp(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeStrVpt_Clear(
    struct mmRbtreeStrVpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStrVptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeStrVpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeStrVpt_Size(
    const struct mmRbtreeStrVpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeStrVptIterator*
mmRbtreeStrVpt_Set(
    struct mmRbtreeStrVpt* p,
    const char* k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStrVptIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeStrVptIterator*)mmMalloc(sizeof(struct mmRbtreeStrVptIterator));
    mmRbtreeStrVptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeStrVptIterator*
mmRbtreeStrVpt_Insert(
    struct mmRbtreeStrVpt* p,
    const char* k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeStrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeStrVptIterator, n);
        result = strcmp(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeStrVptIterator*)mmMalloc(sizeof(struct mmRbtreeStrVptIterator));
    mmRbtreeStrVptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeStrVpt_WeakProduce(
    struct mmRbtreeStrVpt* p,
    const char* k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeStrVpt_WeakRecycle(
    struct mmRbtreeStrVpt* p,
    const char* k,
    void* v)
{
    return v;
}
