/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmConcurrentQueue.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmTwoLockQuque_Init(
    struct mmTwoLockQuque* p)
{
    struct mmTwoLockQuqueNode* node = NULL;

    p->Head = NULL;
    p->Tail = NULL;
    // Locks are initially free
    mmSpinlock_Init(&p->H_lock, NULL);
    mmSpinlock_Init(&p->T_lock, NULL);

    // Allocate a free node
    node = (struct mmTwoLockQuqueNode*)mmMalloc(sizeof(struct mmTwoLockQuqueNode));
    node->value = 0;
    // Make it the only node in the linked list
    node->next = NULL;

    // Both Head and Tail point to it
    p->Head = node;
    p->Tail = node;
}

MM_EXPORT_DLL
void
mmTwoLockQuque_Destroy(
    struct mmTwoLockQuque* p)
{
    struct mmTwoLockQuqueNode* node = NULL;

    // clear quque.
    mmTwoLockQuque_Clear(p);

    // the last node.
    node = p->Head;
    mmFree(node);

    p->Head = NULL;
    p->Tail = NULL;
    mmSpinlock_Destroy(&p->H_lock);
    mmSpinlock_Destroy(&p->T_lock);
}

MM_EXPORT_DLL
void
mmTwoLockQuque_Enqueue(
    struct mmTwoLockQuque* p,
    uintptr_t value)
{
    struct mmTwoLockQuqueNode* node = NULL;

    // Allocate a new node from the free list
    node = (struct mmTwoLockQuqueNode*)mmMalloc(sizeof(struct mmTwoLockQuqueNode));
    // Copy enqueued value into node
    node->value = value;
    // Set next pointer of node to NULL
    node->next = NULL;
    // Acquire T_lock in order to access Tail
    mmSpinlock_Lock(&p->T_lock);
    // Link node at the end of the linked list
    p->Tail->next = node;
    // Swing Tail to node
    p->Tail = node;
    // Release T_lock
    mmSpinlock_Unlock(&p->T_lock);
}

MM_EXPORT_DLL
int
mmTwoLockQuque_Dequeue(
    struct mmTwoLockQuque* p,
    uintptr_t* pvalue)
{
    struct mmTwoLockQuqueNode* node = NULL;
    struct mmTwoLockQuqueNode* new_head = NULL;

    // Acquire h_lock in order to access Head
    mmSpinlock_Lock(&p->H_lock);
    // Read Head
    node = p->Head;
    // Read next pointer
    new_head = node->next;
    // Is queue empty?
    if (new_head == NULL)
    {
        // Release h_lock before return
        mmSpinlock_Unlock(&p->H_lock);
        // Queue was empty
        return MM_FALSE;
    }
    else
    {
        // Queue not empty, read value
        *pvalue = new_head->value;
        // Swing Head to next node
        p->Head = new_head;
        // Release h_lock
        mmSpinlock_Unlock(&p->H_lock);
        // Free node
        mmFree(node);
        // Queue was not empty, dequeue succeeded
        return MM_TRUE;
    }
}

MM_EXPORT_DLL
int
mmTwoLockQuque_IsEmpty(
    struct mmTwoLockQuque* p)
{
    struct mmTwoLockQuqueNode* node = NULL;
    struct mmTwoLockQuqueNode* new_head = NULL;

    // Acquire h_lock in order to access Head
    mmSpinlock_Lock(&p->H_lock);
    // Read Head
    node = p->Head;
    // Read next pointer
    new_head = node->next;
    mmSpinlock_Unlock(&p->H_lock);
    // Is queue empty?
    return new_head == NULL;
}

MM_EXPORT_DLL
void
mmTwoLockQuque_Clear(
    struct mmTwoLockQuque* p)
{
    uintptr_t v = 0;
    while (MM_TRUE == mmTwoLockQuque_Dequeue(p, &v))
    {
        // do nothing here.
    }
}

