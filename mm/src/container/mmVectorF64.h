/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorF64_h__
#define __mmVectorF64_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorF64
{
    size_t size;
    size_t capacity;
    mmFloat64_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorF64_Init(
    struct mmVectorF64* p);

MM_EXPORT_DLL 
void 
mmVectorF64_Destroy(
    struct mmVectorF64* p);

MM_EXPORT_DLL 
void 
mmVectorF64_Assign(
    struct mmVectorF64* p, 
    const struct mmVectorF64* q);

MM_EXPORT_DLL 
void 
mmVectorF64_Clear(
    struct mmVectorF64* p);

MM_EXPORT_DLL 
void 
mmVectorF64_Reset(
    struct mmVectorF64* p);

MM_EXPORT_DLL 
void 
mmVectorF64_SetIndex(
    struct mmVectorF64* p, 
    size_t index, 
    mmFloat64_t e);

MM_EXPORT_DLL 
mmFloat64_t 
mmVectorF64_GetIndex(
    const struct mmVectorF64* p, 
    size_t index);

MM_EXPORT_DLL 
mmFloat64_t* 
mmVectorF64_GetIndexReference(
    const struct mmVectorF64* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorF64_Realloc(
    struct mmVectorF64* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorF64_Free(
    struct mmVectorF64* p);

MM_EXPORT_DLL 
void 
mmVectorF64_Resize(
    struct mmVectorF64* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorF64_Reserve(
    struct mmVectorF64* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorF64_AlignedMemory(
    struct mmVectorF64* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorF64_AllocatorMemory(
    struct mmVectorF64* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorF64_PushBack(
    struct mmVectorF64* p, 
    mmFloat64_t e);

MM_EXPORT_DLL 
mmFloat64_t 
mmVectorF64_PopBack(
    struct mmVectorF64* p);

MM_EXPORT_DLL 
mmFloat64_t* 
mmVectorF64_Begin(
    const struct mmVectorF64* p);

MM_EXPORT_DLL 
mmFloat64_t* 
mmVectorF64_End(
    const struct mmVectorF64* p);

MM_EXPORT_DLL 
mmFloat64_t* 
mmVectorF64_Back(
    const struct mmVectorF64* p);

MM_EXPORT_DLL 
mmFloat64_t* 
mmVectorF64_Next(
    const struct mmVectorF64* p, 
    mmFloat64_t* iter);

MM_EXPORT_DLL 
mmFloat64_t 
mmVectorF64_At(
    const struct mmVectorF64* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorF64_Size(
    const struct mmVectorF64* p);

MM_EXPORT_DLL 
size_t 
mmVectorF64_Capacity(
    const struct mmVectorF64* p);

MM_EXPORT_DLL 
void 
mmVectorF64_Remove(
    struct mmVectorF64* p, 
    size_t index);

MM_EXPORT_DLL 
mmFloat64_t* 
mmVectorF64_Erase(
    struct mmVectorF64* p, 
    mmFloat64_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorF64_Compare(
    const struct mmVectorF64* p, 
    const struct mmVectorF64* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorF64_CopyFromValue(
    struct mmVectorF64* p, 
    const struct mmVectorF64* q);

// vector Null.
#define mmVectorF64_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorF64_Make(v)                   \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (mmFloat64_t*)v,       /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorF64_h__
