/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeVisibleU64.h"
#include "core/mmSpinlock.h"

static 
void 
__static_mmRbtreeVisibleU64_TryMergeAdd(
    struct mmRbtreeVisibleU64* p);

static 
void
__static_mmRbtreeVisibleU64_TryMergeRmv(
    struct mmRbtreeVisibleU64* p);

MM_EXPORT_DLL
void
mmRbtreeVisibleU64_Init(
    struct mmRbtreeVisibleU64* p)
{
    struct mmRbtreeU64VptAllocator _U64VptAllocator;
    //
    mmRbtreeU64Vpt_Init(&p->rbtree_m);
    mmRbtsetU64_Init(&p->rbtset_s);
    //
    mmListU64_Init(&p->l_a);
    mmListU64_Init(&p->l_r);
    //
    mmSpinlock_Init(&p->locker_m, NULL);
    mmSpinlock_Init(&p->locker_s, NULL);
    mmSpinlock_Init(&p->locker_a, NULL);
    mmSpinlock_Init(&p->locker_r, NULL);
    //
    _U64VptAllocator.Produce = &mmRbtreeU64Vpt_WeakProduce;
    _U64VptAllocator.Recycle = &mmRbtreeU64Vpt_WeakRecycle;
    mmRbtreeU64Vpt_SetAllocator(&p->rbtree_m, &_U64VptAllocator);
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU64_Destroy(
    struct mmRbtreeVisibleU64* p)
{
    mmRbtreeVisibleU64_Clear(p);
    //
    mmRbtreeU64Vpt_Destroy(&p->rbtree_m);
    mmRbtsetU64_Destroy(&p->rbtset_s);
    //
    mmListU64_Destroy(&p->l_a);
    mmListU64_Destroy(&p->l_r);
    //
    mmSpinlock_Destroy(&p->locker_m);
    mmSpinlock_Destroy(&p->locker_s);
    mmSpinlock_Destroy(&p->locker_a);
    mmSpinlock_Destroy(&p->locker_r);
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU64_Add(
    struct mmRbtreeVisibleU64* p,
    mmUInt64_t k,
    void* v)
{
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeU64Vpt_Set(&p->rbtree_m, k, v);
    mmSpinlock_Unlock(&p->locker_m);
    //
    if (mmSpinlock_Trylock(&p->locker_s))
    {
        __static_mmRbtreeVisibleU64_TryMergeAdd(p);
        mmRbtsetU64_Add(&p->rbtset_s, k);
        mmSpinlock_Unlock(&p->locker_s);
    }
    else
    {
        mmSpinlock_Lock(&p->locker_a);
        mmListU64_AddFont(&p->l_a, k);
        mmSpinlock_Unlock(&p->locker_a);
    }
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU64_Rmv(
    struct mmRbtreeVisibleU64* p,
    mmUInt64_t k)
{
    if (mmSpinlock_Trylock(&p->locker_s))
    {
        __static_mmRbtreeVisibleU64_TryMergeRmv(p);
        mmRbtsetU64_Rmv(&p->rbtset_s, k);
        mmSpinlock_Unlock(&p->locker_s);
    }
    else
    {
        mmSpinlock_Lock(&p->locker_r);
        mmListU64_AddFont(&p->l_r, k);
        mmSpinlock_Unlock(&p->locker_r);
    }
    //
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeU64Vpt_Rmv(&p->rbtree_m, k);
    mmSpinlock_Unlock(&p->locker_m);
}

MM_EXPORT_DLL
void*
mmRbtreeVisibleU64_Get(
    struct mmRbtreeVisibleU64* p,
    mmUInt64_t k)
{
    void* e = NULL;
    mmSpinlock_Lock(&p->locker_m);
    e = mmRbtreeU64Vpt_Get(&p->rbtree_m, k);
    mmSpinlock_Unlock(&p->locker_m);
    return e;
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU64_Clear(
    struct mmRbtreeVisibleU64* p)
{
    mmSpinlock_Lock(&p->locker_m);
    mmRbtreeU64Vpt_Clear(&p->rbtree_m);
    mmSpinlock_Unlock(&p->locker_m);
    //
    mmSpinlock_Lock(&p->locker_s);
    mmRbtsetU64_Clear(&p->rbtset_s);
    mmSpinlock_Unlock(&p->locker_s);
    //
    mmSpinlock_Lock(&p->locker_a);
    mmListU64_Clear(&p->l_a);
    mmSpinlock_Unlock(&p->locker_a);
    //
    mmSpinlock_Lock(&p->locker_r);
    mmListU64_Clear(&p->l_r);
    mmSpinlock_Unlock(&p->locker_r);
}

MM_EXPORT_DLL
size_t
mmRbtreeVisibleU64_Size(
    const struct mmRbtreeVisibleU64* p)
{
    return p->rbtree_m.size;
}

MM_EXPORT_DLL
void
mmRbtreeVisibleU64_Traver(
    struct mmRbtreeVisibleU64* p,
    mmRbtreeVisibleU64HandleFunc handle,
    void* u)
{
    mmUInt64_t k = 0;
    void* v = NULL;
    
    assert(NULL != handle && "handle is a null.");
    
    // init key.
    // k = 0;
    mmSpinlock_Lock(&p->locker_s);
    
    __static_mmRbtreeVisibleU64_TryMergeAdd(p);
    __static_mmRbtreeVisibleU64_TryMergeRmv(p);
    
    if (0 != p->rbtset_s.size)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetU64Iterator* it = NULL;
        //
        n = mmRb_First(&p->rbtset_s.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
            n = mmRb_Next(n);
            // copy key.
            k = it->k;
            v = mmRbtreeVisibleU64_Get(p, k);
            if (NULL != v)
            {
                (*(handle))(p, u, k, v);
            }
        }
    }
    mmSpinlock_Unlock(&p->locker_s);
    // destroy key.
    // k = 0;
}

static 
void 
__static_mmRbtreeVisibleU64_TryMergeAdd(
    struct mmRbtreeVisibleU64* p)
{
    if (0 != p->l_a.size)
    {
        if (mmSpinlock_Trylock(&p->locker_a))
        {
            struct mmListHead* pos = NULL;
            struct mmListU64Iterator* it = NULL;
            pos = p->l_a.l.next;
            while (pos != &p->l_a.l)
            {
                struct mmListHead* curr = pos;
                pos = pos->next;
                it = mmList_Entry(curr, struct mmListU64Iterator, n);
                mmRbtsetU64_Add(&p->rbtset_s, it->v);
                mmListU64_Erase(&p->l_a, it);
            }
            mmSpinlock_Unlock(&p->locker_a);
        }
    }
}

static 
void 
__static_mmRbtreeVisibleU64_TryMergeRmv(
    struct mmRbtreeVisibleU64* p)
{
    if (0 != p->l_r.size)
    {
        if (mmSpinlock_Trylock(&p->locker_r))
        {
            struct mmListHead* pos = NULL;
            struct mmListU64Iterator* it = NULL;
            pos = p->l_r.l.next;
            while (pos != &p->l_r.l)
            {
                struct mmListHead* curr = pos;
                pos = pos->next;
                it = mmList_Entry(curr, struct mmListU64Iterator, n);
                mmRbtsetU64_Rmv(&p->rbtset_s, it->v);
                mmListU64_Erase(&p->l_r, it);
            }
            mmSpinlock_Unlock(&p->locker_r);
        }
    }
}

