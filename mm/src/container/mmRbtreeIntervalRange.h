/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeIntervalRange_h__
#define __mmRbtreeIntervalRange_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL 
intptr_t 
mmRbtreeIntervalRangeCompareFunc(
    size_t lo, size_t ll, 
    size_t ro, size_t rl);

// mmRbtreeIntervalRange
struct mmRbtreeIntervalRangeIterator
{
    // offset
    size_t o;
    // length
    size_t l;
    // value
    void* v;
    // Node
    struct mmRbNode n;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRangeIterator_Init(
    struct mmRbtreeIntervalRangeIterator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRangeIterator_Destroy(
    struct mmRbtreeIntervalRangeIterator* p);

struct mmRbtreeIntervalRange;

struct mmRbtreeIntervalRangeAllocator
{
    void* (*Produce)(struct mmRbtreeIntervalRange* p, size_t o, size_t l);
    void* (*Recycle)(struct mmRbtreeIntervalRange* p, size_t o, size_t l, void* v);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRangeAllocator_Init(
    struct mmRbtreeIntervalRangeAllocator* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRangeAllocator_Destroy(
    struct mmRbtreeIntervalRangeAllocator* p);

struct mmRbtreeIntervalRange
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeIntervalRangeAllocator allocator;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRange_Init(
    struct mmRbtreeIntervalRange* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRange_Destroy(
    struct mmRbtreeIntervalRange* p);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRange_SetAllocator(
    struct mmRbtreeIntervalRange* p, 
    struct mmRbtreeIntervalRangeAllocator* allocator);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalRange_Add(
    struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRange_Rmv(
    struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRange_Erase(
    struct mmRbtreeIntervalRange* p, 
    struct mmRbtreeIntervalRangeIterator* it);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalRange_GetInstance(
    struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalRange_Get(
    const struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l);

MM_EXPORT_DLL 
struct mmRbtreeIntervalRangeIterator* 
mmRbtreeIntervalRange_GetIterator(
    const struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l);

MM_EXPORT_DLL 
void 
mmRbtreeIntervalRange_Clear(
    struct mmRbtreeIntervalRange* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeIntervalRange_Size(
    const struct mmRbtreeIntervalRange* p);

// weak ref can use this.
MM_EXPORT_DLL 
struct mmRbtreeIntervalRangeIterator*
mmRbtreeIntervalRange_Set(
    struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l, 
    void* v);

MM_EXPORT_DLL
struct mmRbtreeIntervalRangeIterator*
mmRbtreeIntervalRange_Insert(
    struct mmRbtreeIntervalRange* p,
    size_t o, size_t l,
    void* v);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalRange_WeakProduce(
    struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l);

MM_EXPORT_DLL 
void* 
mmRbtreeIntervalRange_WeakRecycle(
    struct mmRbtreeIntervalRange* p, 
    size_t o, size_t l,
    void* v);

#include "core/mmSuffix.h"

#endif//__mmRbtreeIntervalRange_h__
