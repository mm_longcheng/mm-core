/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeU32.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmRbtreeU32U32Iterator_Init(
    struct mmRbtreeU32U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32U32Iterator_Destroy(
    struct mmRbtreeU32U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32U32_Init(
    struct mmRbtreeU32U32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU32U32_Destroy(
    struct mmRbtreeU32U32* p)
{
    mmRbtreeU32U32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeU32U32Iterator*
mmRbtreeU32U32_Add(
    struct mmRbtreeU32U32* p,
    mmUInt32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32U32Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU32U32Iterator*)mmMalloc(sizeof(struct mmRbtreeU32U32Iterator));
    mmRbtreeU32U32Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32U32_Rmv(
    struct mmRbtreeU32U32* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32U32Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU32U32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU32U32_Erase(
    struct mmRbtreeU32U32* p,
    struct mmRbtreeU32U32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeU32U32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeU32U32_GetInstance(
    struct mmRbtreeU32U32* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U32Iterator* it = mmRbtreeU32U32_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeU32U32_Get(
    const struct mmRbtreeU32U32* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U32Iterator* it = mmRbtreeU32U32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU32U32Iterator*
mmRbtreeU32U32_GetIterator(
    const struct mmRbtreeU32U32* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32U32Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU32U32_Clear(
    struct mmRbtreeU32U32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32U32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeU32U32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU32U32_Size(
    const struct mmRbtreeU32U32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeU32U32Iterator*
mmRbtreeU32U32_Set(
    struct mmRbtreeU32U32* p,
    mmUInt32_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32U32Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeU32U32Iterator*)mmMalloc(sizeof(struct mmRbtreeU32U32Iterator));
    mmRbtreeU32U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU32U32Iterator*
mmRbtreeU32U32_Insert(
    struct mmRbtreeU32U32* p,
    mmUInt32_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32U32Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU32U32Iterator*)mmMalloc(sizeof(struct mmRbtreeU32U32Iterator));
    mmRbtreeU32U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32U64Iterator_Init(
    struct mmRbtreeU32U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32U64Iterator_Destroy(
    struct mmRbtreeU32U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32U64_Init(
    struct mmRbtreeU32U64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU32U64_Destroy(
    struct mmRbtreeU32U64* p)
{
    mmRbtreeU32U64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeU32U64Iterator*
mmRbtreeU32U64_Add(
    struct mmRbtreeU32U64* p,
    mmUInt32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32U64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32U64Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU32U64Iterator*)mmMalloc(sizeof(struct mmRbtreeU32U64Iterator));
    mmRbtreeU32U64Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32U64_Rmv(
    struct mmRbtreeU32U64* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32U64Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU32U64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU32U64_Erase(
    struct mmRbtreeU32U64* p,
    struct mmRbtreeU32U64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeU32U64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeU32U64_GetInstance(
    struct mmRbtreeU32U64* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U64Iterator* it = mmRbtreeU32U64_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeU32U64_Get(
    const struct mmRbtreeU32U64* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U64Iterator* it = mmRbtreeU32U64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU32U64Iterator*
mmRbtreeU32U64_GetIterator(
    const struct mmRbtreeU32U64* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32U64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32U64Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU32U64_Clear(
    struct mmRbtreeU32U64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32U64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32U64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeU32U64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU32U64_Size(
    const struct mmRbtreeU32U64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeU32U64Iterator*
mmRbtreeU32U64_Set(
    struct mmRbtreeU32U64* p,
    mmUInt32_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32U64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32U64Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeU32U64Iterator*)mmMalloc(sizeof(struct mmRbtreeU32U64Iterator));
    mmRbtreeU32U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU32U64Iterator*
mmRbtreeU32U64_Insert(
    struct mmRbtreeU32U64* p,
    mmUInt32_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32U64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32U64Iterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU32U64Iterator*)mmMalloc(sizeof(struct mmRbtreeU32U64Iterator));
    mmRbtreeU32U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32StringIterator_Init(
    struct mmRbtreeU32StringIterator* p)
{
    p->k = 0;
    mmString_Init(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32StringIterator_Destroy(
    struct mmRbtreeU32StringIterator* p)
{
    p->k = 0;
    mmString_Destroy(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32String_Init(
    struct mmRbtreeU32String* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU32String_Destroy(
    struct mmRbtreeU32String* p)
{
    mmRbtreeU32String_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeU32StringIterator*
mmRbtreeU32String_Add(
    struct mmRbtreeU32String* p,
    mmUInt32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32StringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32StringIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU32StringIterator*)mmMalloc(sizeof(struct mmRbtreeU32StringIterator));
    mmRbtreeU32StringIterator_Init(it);
    it->k = k;
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32String_Rmv(
    struct mmRbtreeU32String* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32StringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32StringIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU32String_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU32String_Erase(
    struct mmRbtreeU32String* p,
    struct mmRbtreeU32StringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeU32StringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeU32String_GetInstance(
    struct mmRbtreeU32String* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32StringIterator* it = mmRbtreeU32String_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeU32String_Get(
    const struct mmRbtreeU32String* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32StringIterator* it = mmRbtreeU32String_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU32StringIterator*
mmRbtreeU32String_GetIterator(
    const struct mmRbtreeU32String* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32StringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32StringIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU32String_Clear(
    struct mmRbtreeU32String* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32StringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32StringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeU32String_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU32String_Size(
    const struct mmRbtreeU32String* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeU32StringIterator*
mmRbtreeU32String_Set(
    struct mmRbtreeU32String* p,
    mmUInt32_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32StringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32StringIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return it;
        }
    }
    it = (struct mmRbtreeU32StringIterator*)mmMalloc(sizeof(struct mmRbtreeU32StringIterator));
    mmRbtreeU32StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU32StringIterator*
mmRbtreeU32String_Insert(
    struct mmRbtreeU32String* p,
    mmUInt32_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32StringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32StringIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU32StringIterator*)mmMalloc(sizeof(struct mmRbtreeU32StringIterator));
    mmRbtreeU32StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32VptIterator_Init(
    struct mmRbtreeU32VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32VptIterator_Destroy(
    struct mmRbtreeU32VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeU32VptAllocator_Init(
    struct mmRbtreeU32VptAllocator* p)
{
    p->Produce = &mmRbtreeU32Vpt_WeakProduce;
    p->Recycle = &mmRbtreeU32Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeU32VptAllocator_Destroy(
    struct mmRbtreeU32VptAllocator* p)
{
    p->Produce = &mmRbtreeU32Vpt_WeakProduce;
    p->Recycle = &mmRbtreeU32Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeU32Vpt_Init(
    struct mmRbtreeU32Vpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeU32VptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU32Vpt_Destroy(
    struct mmRbtreeU32Vpt* p)
{
    mmRbtreeU32Vpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeU32VptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeU32Vpt_SetAllocator(
    struct mmRbtreeU32Vpt* p,
    struct mmRbtreeU32VptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeU32VptIterator*
mmRbtreeU32Vpt_Add(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32VptIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeU32VptIterator*)mmMalloc(sizeof(struct mmRbtreeU32VptIterator));
    mmRbtreeU32VptIterator_Init(it);
    it->k = k;
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeU32Vpt_Rmv(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32VptIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeU32Vpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeU32Vpt_Erase(
    struct mmRbtreeU32Vpt* p,
    struct mmRbtreeU32VptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->k, it->v);
    mmRbtreeU32VptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeU32Vpt_GetInstance(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeU32VptIterator* it = mmRbtreeU32Vpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeU32Vpt_Get(
    const struct mmRbtreeU32Vpt* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32VptIterator* it = mmRbtreeU32Vpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeU32VptIterator*
mmRbtreeU32Vpt_GetIterator(
    const struct mmRbtreeU32Vpt* p,
    mmUInt32_t k)
{
    struct mmRbtreeU32VptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeU32VptIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeU32Vpt_Clear(
    struct mmRbtreeU32Vpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeU32Vpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeU32Vpt_Size(
    const struct mmRbtreeU32Vpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeU32VptIterator*
mmRbtreeU32Vpt_Set(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32VptIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeU32VptIterator*)mmMalloc(sizeof(struct mmRbtreeU32VptIterator));
    mmRbtreeU32VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeU32VptIterator*
mmRbtreeU32Vpt_Insert(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeU32VptIterator, n);
        result = mmRbtreeU32CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeU32VptIterator*)mmMalloc(sizeof(struct mmRbtreeU32VptIterator));
    mmRbtreeU32VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeU32Vpt_WeakProduce(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeU32Vpt_WeakRecycle(
    struct mmRbtreeU32Vpt* p,
    mmUInt32_t k,
    void* v)
{
    return v;
}

