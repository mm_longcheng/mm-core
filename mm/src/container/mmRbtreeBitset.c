/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeBitset.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
mmSInt32_t
mmRbtreeBitsetCompareFunc(
    const struct mmBitset* lhs,
    const struct mmBitset* rhs)
{
    return (mmSInt32_t)mmBitset_Compare(lhs, rhs);
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32Iterator_Init(
    struct mmRbtreeBitsetU32Iterator* p)
{
    mmBitset_Init(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32Iterator_Destroy(
    struct mmRbtreeBitsetU32Iterator* p)
{
    mmBitset_Destroy(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32_Init(
    struct mmRbtreeBitsetU32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32_Destroy(
    struct mmRbtreeBitsetU32* p)
{
    mmRbtreeBitsetU32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU32Iterator*
mmRbtreeBitsetU32_Add(
    struct mmRbtreeBitsetU32* p,
    const struct mmBitset* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetU32Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeBitsetU32Iterator*)mmMalloc(sizeof(struct mmRbtreeBitsetU32Iterator));
    mmRbtreeBitsetU32Iterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32_Rmv(
    struct mmRbtreeBitsetU32* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetU32Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeBitsetU32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32_Erase(
    struct mmRbtreeBitsetU32* p,
    struct mmRbtreeBitsetU32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeBitsetU32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeBitsetU32_GetInstance(
    struct mmRbtreeBitsetU32* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU32Iterator* it = mmRbtreeBitsetU32_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeBitsetU32_Get(
    const struct mmRbtreeBitsetU32* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU32Iterator* it = mmRbtreeBitsetU32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU32Iterator*
    mmRbtreeBitsetU32_GetIterator(
        const struct mmRbtreeBitsetU32* p,
        const struct mmBitset* k)
{
    struct mmRbtreeBitsetU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetU32Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU32_Clear(
    struct mmRbtreeBitsetU32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeBitsetU32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeBitsetU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeBitsetU32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeBitsetU32_Size(
    const struct mmRbtreeBitsetU32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU32Iterator*
mmRbtreeBitsetU32_Set(
    struct mmRbtreeBitsetU32* p,
    const struct mmBitset* k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetU32Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeBitsetU32Iterator*)mmMalloc(sizeof(struct mmRbtreeBitsetU32Iterator));
    mmRbtreeBitsetU32Iterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU32Iterator*
mmRbtreeBitsetU32_Insert(
    struct mmRbtreeBitsetU32* p,
    const struct mmBitset* k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetU32Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeBitsetU32Iterator*)mmMalloc(sizeof(struct mmRbtreeBitsetU32Iterator));
    mmRbtreeBitsetU32Iterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64Iterator_Init(
    struct mmRbtreeBitsetU64Iterator* p)
{
    mmBitset_Init(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64Iterator_Destroy(
    struct mmRbtreeBitsetU64Iterator* p)
{
    mmBitset_Destroy(&p->k);
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64_Init(
    struct mmRbtreeBitsetU64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64_Destroy(
    struct mmRbtreeBitsetU64* p)
{
    mmRbtreeBitsetU64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU64Iterator*
mmRbtreeBitsetU64_Add(
    struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetU64Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeBitsetU64Iterator*)mmMalloc(sizeof(struct mmRbtreeBitsetU64Iterator));
    mmRbtreeBitsetU64Iterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64_Rmv(
    struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetU64Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeBitsetU64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64_Erase(
    struct mmRbtreeBitsetU64* p,
    struct mmRbtreeBitsetU64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeBitsetU64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeBitsetU64_GetInstance(
    struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU64Iterator* it = mmRbtreeBitsetU64_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeBitsetU64_Get(
    const struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU64Iterator* it = mmRbtreeBitsetU64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU64Iterator*
mmRbtreeBitsetU64_GetIterator(
    const struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetU64Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetU64_Clear(
    struct mmRbtreeBitsetU64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeBitsetU64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeBitsetU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeBitsetU64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeBitsetU64_Size(
    const struct mmRbtreeBitsetU64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU64Iterator*
mmRbtreeBitsetU64_Set(
    struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetU64Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeBitsetU64Iterator*)mmMalloc(sizeof(struct mmRbtreeBitsetU64Iterator));
    mmRbtreeBitsetU64Iterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetU64Iterator*
mmRbtreeBitsetU64_Insert(
    struct mmRbtreeBitsetU64* p,
    const struct mmBitset* k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetU64Iterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeBitsetU64Iterator*)mmMalloc(sizeof(struct mmRbtreeBitsetU64Iterator));
    mmRbtreeBitsetU64Iterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetStringIterator_Init(
    struct mmRbtreeBitsetStringIterator* p)
{
    mmBitset_Init(&p->k);
    mmString_Init(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetStringIterator_Destroy(
    struct mmRbtreeBitsetStringIterator* p)
{
    mmBitset_Destroy(&p->k);
    mmString_Destroy(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetString_Init(
    struct mmRbtreeBitsetString* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetString_Destroy(
    struct mmRbtreeBitsetString* p)
{
    mmRbtreeBitsetString_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetStringIterator*
mmRbtreeBitsetString_Add(
    struct mmRbtreeBitsetString* p,
    const struct mmBitset* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetStringIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeBitsetStringIterator*)mmMalloc(sizeof(struct mmRbtreeBitsetStringIterator));
    mmRbtreeBitsetStringIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetString_Rmv(
    struct mmRbtreeBitsetString* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetStringIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeBitsetString_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeBitsetString_Erase(
    struct mmRbtreeBitsetString* p,
    struct mmRbtreeBitsetStringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeBitsetStringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeBitsetString_GetInstance(
    struct mmRbtreeBitsetString* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetStringIterator* it = mmRbtreeBitsetString_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeBitsetString_Get(
    const struct mmRbtreeBitsetString* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetStringIterator* it = mmRbtreeBitsetString_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetStringIterator*
mmRbtreeBitsetString_GetIterator(
    const struct mmRbtreeBitsetString* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetStringIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetString_Clear(
    struct mmRbtreeBitsetString* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeBitsetStringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeBitsetStringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeBitsetString_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeBitsetString_Size(
    const struct mmRbtreeBitsetString* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeBitsetStringIterator*
mmRbtreeBitsetString_Set(
    struct mmRbtreeBitsetString* p,
    const struct mmBitset* k,
    const struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetStringIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return it;
        }
    }
    it = (struct mmRbtreeBitsetStringIterator*)mmMalloc(sizeof(struct mmRbtreeBitsetStringIterator));
    mmRbtreeBitsetStringIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetStringIterator*
mmRbtreeBitsetString_Insert(
    struct mmRbtreeBitsetString* p,
    const struct mmBitset* k,
    const struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetStringIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeBitsetStringIterator*)mmMalloc(sizeof(struct mmRbtreeBitsetStringIterator));
    mmRbtreeBitsetStringIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVptIterator_Init(
    struct mmRbtreeBitsetVptIterator* p)
{
    mmBitset_Init(&p->k);
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVptIterator_Destroy(
    struct mmRbtreeBitsetVptIterator* p)
{
    mmBitset_Destroy(&p->k);
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVptAllocator_Init(
    struct mmRbtreeBitsetVptAllocator* p)
{
    p->Produce = &mmRbtreeBitsetVpt_WeakProduce;
    p->Recycle = &mmRbtreeBitsetVpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVptAllocator_Destroy(
    struct mmRbtreeBitsetVptAllocator* p)
{
    p->Produce = &mmRbtreeBitsetVpt_WeakProduce;
    p->Recycle = &mmRbtreeBitsetVpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVpt_Init(
    struct mmRbtreeBitsetVpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeBitsetVptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVpt_Destroy(
    struct mmRbtreeBitsetVpt* p)
{
    mmRbtreeBitsetVpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeBitsetVptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVpt_SetAllocator(
    struct mmRbtreeBitsetVpt* p,
    struct mmRbtreeBitsetVptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetVptIterator*
mmRbtreeBitsetVpt_Add(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetVptIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeBitsetVptIterator*)mmMalloc(sizeof(struct mmRbtreeBitsetVptIterator));
    mmRbtreeBitsetVptIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVpt_Rmv(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetVptIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeBitsetVpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVpt_Erase(
    struct mmRbtreeBitsetVpt* p,
    struct mmRbtreeBitsetVptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, &it->k, it->v);
    mmRbtreeBitsetVptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeBitsetVpt_GetInstance(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeBitsetVptIterator* it = mmRbtreeBitsetVpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeBitsetVpt_Get(
    const struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetVptIterator* it = mmRbtreeBitsetVpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetVptIterator*
mmRbtreeBitsetVpt_GetIterator(
    const struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k)
{
    struct mmRbtreeBitsetVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeBitsetVptIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeBitsetVpt_Clear(
    struct mmRbtreeBitsetVpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeBitsetVptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeBitsetVptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeBitsetVpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeBitsetVpt_Size(
    const struct mmRbtreeBitsetVpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetVptIterator*
mmRbtreeBitsetVpt_Set(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetVptIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeBitsetVptIterator*)mmMalloc(sizeof(struct mmRbtreeBitsetVptIterator));
    mmRbtreeBitsetVptIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeBitsetVptIterator*
mmRbtreeBitsetVpt_Insert(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeBitsetVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeBitsetVptIterator, n);
        result = mmRbtreeBitsetCompareFunc(k, &it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeBitsetVptIterator*)mmMalloc(sizeof(struct mmRbtreeBitsetVptIterator));
    mmRbtreeBitsetVptIterator_Init(it);
    mmBitset_Assign(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeBitsetVpt_WeakProduce(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeBitsetVpt_WeakRecycle(
    struct mmRbtreeBitsetVpt* p,
    const struct mmBitset* k,
    void* v)
{
    return v;
}

