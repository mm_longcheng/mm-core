/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmListVpt_h__
#define __mmListVpt_h__

#include "core/mmCore.h"

#include "core/mmList.h"

#include "core/mmPrefix.h"

struct mmListVptIterator
{
    void* v;
    struct mmListHead n;
};

MM_EXPORT_DLL 
void 
mmListVptIterator_Init(
    struct mmListVptIterator* p);

MM_EXPORT_DLL 
void 
mmListVptIterator_Destroy(
    struct mmListVptIterator* p);

struct mmListVpt
{
    struct mmListHead l;
    size_t size;
};

MM_EXPORT_DLL 
void 
mmListVpt_Init(
    struct mmListVpt* p);

MM_EXPORT_DLL 
void 
mmListVpt_Destroy(
    struct mmListVpt* p);

MM_EXPORT_DLL 
void 
mmListVpt_AddFont(
    struct mmListVpt* p, 
    void* v);

MM_EXPORT_DLL 
void 
mmListVpt_AddTail(
    struct mmListVpt* p, 
    void* v);

MM_EXPORT_DLL 
void* 
mmListVpt_PopFont(
    struct mmListVpt* p);

MM_EXPORT_DLL 
void* 
mmListVpt_PopTail(
    struct mmListVpt* p);

MM_EXPORT_DLL 
void 
mmListVpt_Erase(
    struct mmListVpt* p, 
    struct mmListVptIterator* it);

MM_EXPORT_DLL 
void 
mmListVpt_Clear(
    struct mmListVpt* p);

MM_EXPORT_DLL 
size_t 
mmListVpt_Size(
    const struct mmListVpt* p);

MM_EXPORT_DLL 
void 
mmListVpt_Remove(
    struct mmListVpt* p, 
    void* v);

MM_EXPORT_DLL 
void 
mmListVpt_InsertPrev(
    struct mmListVpt* p, 
    struct mmListVptIterator* i, 
    void* v);

MM_EXPORT_DLL 
void 
mmListVpt_InsertNext(
    struct mmListVpt* p, 
    struct mmListVptIterator* i, 
    void* v);

#include "core/mmSuffix.h"

#endif//__mmListVpt_h__
