/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeF64.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmRbtreeF64U32Iterator_Init(
    struct mmRbtreeF64U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64U32Iterator_Destroy(
    struct mmRbtreeF64U32Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64U32_Init(
    struct mmRbtreeF64U32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF64U32_Destroy(
    struct mmRbtreeF64U32* p)
{
    mmRbtreeF64U32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeF64U32Iterator*
mmRbtreeF64U32_Add(
    struct mmRbtreeF64U32* p,
    mmFloat64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64U32Iterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64U32Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF64U32Iterator*)mmMalloc(sizeof(struct mmRbtreeF64U32Iterator));
    mmRbtreeF64U32Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64U32_Rmv(
    struct mmRbtreeF64U32* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U32Iterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64U32Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF64U32_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF64U32_Erase(
    struct mmRbtreeF64U32* p,
    struct mmRbtreeF64U32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeF64U32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeF64U32_GetInstance(
    struct mmRbtreeF64U32* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U32Iterator* it = mmRbtreeF64U32_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt32_t*
mmRbtreeF64U32_Get(
    const struct mmRbtreeF64U32* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U32Iterator* it = mmRbtreeF64U32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF64U32Iterator*
mmRbtreeF64U32_GetIterator(
    const struct mmRbtreeF64U32* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U32Iterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64U32Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF64U32_Clear(
    struct mmRbtreeF64U32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF64U32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF64U32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeF64U32_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF64U32_Size(
    const struct mmRbtreeF64U32* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeF64U32Iterator*
mmRbtreeF64U32_Set(
    struct mmRbtreeF64U32* p,
    mmFloat64_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64U32Iterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64U32Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeF64U32Iterator*)mmMalloc(sizeof(struct mmRbtreeF64U32Iterator));
    mmRbtreeF64U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF64U32Iterator*
mmRbtreeF64U32_Insert(
    struct mmRbtreeF64U32* p,
    mmFloat64_t k,
    mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64U32Iterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64U32Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF64U32Iterator*)mmMalloc(sizeof(struct mmRbtreeF64U32Iterator));
    mmRbtreeF64U32Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64U64Iterator_Init(
    struct mmRbtreeF64U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64U64Iterator_Destroy(
    struct mmRbtreeF64U64Iterator* p)
{
    p->k = 0;
    p->v = 0;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64U64_Init(
    struct mmRbtreeF64U64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF64U64_Destroy(
    struct mmRbtreeF64U64* p)
{
    mmRbtreeF64U64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeF64U64Iterator*
mmRbtreeF64U64_Add(
    struct mmRbtreeF64U64* p,
    mmFloat64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64U64Iterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64U64Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF64U64Iterator*)mmMalloc(sizeof(struct mmRbtreeF64U64Iterator));
    mmRbtreeF64U64Iterator_Init(it);
    it->k = k;
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64U64_Rmv(
    struct mmRbtreeF64U64* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U64Iterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64U64Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF64U64_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF64U64_Erase(
    struct mmRbtreeF64U64* p,
    struct mmRbtreeF64U64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeF64U64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeF64U64_GetInstance(
    struct mmRbtreeF64U64* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U64Iterator* it = mmRbtreeF64U64_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
mmUInt64_t*
mmRbtreeF64U64_Get(
    const struct mmRbtreeF64U64* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U64Iterator* it = mmRbtreeF64U64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF64U64Iterator*
mmRbtreeF64U64_GetIterator(
    const struct mmRbtreeF64U64* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64U64Iterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64U64Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF64U64_Clear(
    struct mmRbtreeF64U64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF64U64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF64U64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeF64U64_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF64U64_Size(
    const struct mmRbtreeF64U64* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeF64U64Iterator*
mmRbtreeF64U64_Set(
    struct mmRbtreeF64U64* p,
    mmFloat64_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64U64Iterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64U64Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeF64U64Iterator*)mmMalloc(sizeof(struct mmRbtreeF64U64Iterator));
    mmRbtreeF64U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF64U64Iterator*
mmRbtreeF64U64_Insert(
    struct mmRbtreeF64U64* p,
    mmFloat64_t k,
    mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64U64Iterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64U64Iterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF64U64Iterator*)mmMalloc(sizeof(struct mmRbtreeF64U64Iterator));
    mmRbtreeF64U64Iterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64StringIterator_Init(
    struct mmRbtreeF64StringIterator* p)
{
    p->k = 0;
    mmString_Init(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64StringIterator_Destroy(
    struct mmRbtreeF64StringIterator* p)
{
    p->k = 0;
    mmString_Destroy(&p->v);
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64String_Init(
    struct mmRbtreeF64String* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF64String_Destroy(
    struct mmRbtreeF64String* p)
{
    mmRbtreeF64String_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_DLL
struct mmRbtreeF64StringIterator*
mmRbtreeF64String_Add(
    struct mmRbtreeF64String* p,
    mmFloat64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64StringIterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64StringIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF64StringIterator*)mmMalloc(sizeof(struct mmRbtreeF64StringIterator));
    mmRbtreeF64StringIterator_Init(it);
    it->k = k;
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64String_Rmv(
    struct mmRbtreeF64String* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64StringIterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64StringIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF64String_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF64String_Erase(
    struct mmRbtreeF64String* p,
    struct mmRbtreeF64StringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeF64StringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeF64String_GetInstance(
    struct mmRbtreeF64String* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64StringIterator* it = mmRbtreeF64String_Add(p, k);
    return &it->v;
}

MM_EXPORT_DLL
struct mmString*
mmRbtreeF64String_Get(
    const struct mmRbtreeF64String* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64StringIterator* it = mmRbtreeF64String_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF64StringIterator*
mmRbtreeF64String_GetIterator(
    const struct mmRbtreeF64String* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64StringIterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64StringIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF64String_Clear(
    struct mmRbtreeF64String* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF64StringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF64StringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeF64String_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF64String_Size(
    const struct mmRbtreeF64String* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_DLL
struct mmRbtreeF64StringIterator*
mmRbtreeF64String_Set(
    struct mmRbtreeF64String* p,
    mmFloat64_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64StringIterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64StringIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return it;
        }
    }
    it = (struct mmRbtreeF64StringIterator*)mmMalloc(sizeof(struct mmRbtreeF64StringIterator));
    mmRbtreeF64StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF64StringIterator*
mmRbtreeF64String_Insert(
    struct mmRbtreeF64String* p,
    mmFloat64_t k,
    struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64StringIterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64StringIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF64StringIterator*)mmMalloc(sizeof(struct mmRbtreeF64StringIterator));
    mmRbtreeF64StringIterator_Init(it);
    it->k = k;
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64VptIterator_Init(
    struct mmRbtreeF64VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64VptIterator_Destroy(
    struct mmRbtreeF64VptIterator* p)
{
    p->k = 0;
    p->v = NULL;
    mmMemset(&p->n, 0, sizeof(struct mmRbNode));
}

MM_EXPORT_DLL
void
mmRbtreeF64VptAllocator_Init(
    struct mmRbtreeF64VptAllocator* p)
{
    p->Produce = &mmRbtreeF64Vpt_WeakProduce;
    p->Recycle = &mmRbtreeF64Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeF64VptAllocator_Destroy(
    struct mmRbtreeF64VptAllocator* p)
{
    p->Produce = &mmRbtreeF64Vpt_WeakProduce;
    p->Recycle = &mmRbtreeF64Vpt_WeakRecycle;
}

MM_EXPORT_DLL
void
mmRbtreeF64Vpt_Init(
    struct mmRbtreeF64Vpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeF64VptAllocator_Init(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF64Vpt_Destroy(
    struct mmRbtreeF64Vpt* p)
{
    mmRbtreeF64Vpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeF64VptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmRbtreeF64Vpt_SetAllocator(
    struct mmRbtreeF64Vpt* p,
    struct mmRbtreeF64VptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL
struct mmRbtreeF64VptIterator*
mmRbtreeF64Vpt_Add(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64VptIterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64VptIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return it;
    }
    it = (struct mmRbtreeF64VptIterator*)mmMalloc(sizeof(struct mmRbtreeF64VptIterator));
    mmRbtreeF64VptIterator_Init(it);
    it->k = k;
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmRbtreeF64Vpt_Rmv(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64VptIterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64VptIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeF64Vpt_Erase(p, it);
            // break.
            return;
        }
    }
}

MM_EXPORT_DLL
void
mmRbtreeF64Vpt_Erase(
    struct mmRbtreeF64Vpt* p,
    struct mmRbtreeF64VptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, it->k, it->v);
    mmRbtreeF64VptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}

MM_EXPORT_DLL
void*
mmRbtreeF64Vpt_GetInstance(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k)
{
    // Vpt Instance is direct pointer not &it->v.
    struct mmRbtreeF64VptIterator* it = mmRbtreeF64Vpt_Add(p, k);
    return it->v;
}

MM_EXPORT_DLL
void*
mmRbtreeF64Vpt_Get(
    const struct mmRbtreeF64Vpt* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64VptIterator* it = mmRbtreeF64Vpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}

MM_EXPORT_DLL
struct mmRbtreeF64VptIterator*
mmRbtreeF64Vpt_GetIterator(
    const struct mmRbtreeF64Vpt* p,
    mmFloat64_t k)
{
    struct mmRbtreeF64VptIterator* it = NULL;
    mmFloat64_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeF64VptIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}

MM_EXPORT_DLL
void
mmRbtreeF64Vpt_Clear(
    struct mmRbtreeF64Vpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeF64VptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeF64VptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeF64Vpt_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmRbtreeF64Vpt_Size(
    const struct mmRbtreeF64Vpt* p)
{
    return p->size;
}

MM_EXPORT_DLL
struct mmRbtreeF64VptIterator*
mmRbtreeF64Vpt_Set(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64VptIterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64VptIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return it;
        }
    }
    it = (struct mmRbtreeF64VptIterator*)mmMalloc(sizeof(struct mmRbtreeF64VptIterator));
    mmRbtreeF64VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
struct mmRbtreeF64VptIterator*
mmRbtreeF64Vpt_Insert(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k,
    void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeF64VptIterator* it = NULL;
    mmFloat64_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeF64VptIterator, n);
        result = mmRbtreeF64CompareFunc(k, it->k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeF64VptIterator*)mmMalloc(sizeof(struct mmRbtreeF64VptIterator));
    mmRbtreeF64VptIterator_Init(it);
    it->k = k;
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void*
mmRbtreeF64Vpt_WeakProduce(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k)
{
    return NULL;
}

MM_EXPORT_DLL
void*
mmRbtreeF64Vpt_WeakRecycle(
    struct mmRbtreeF64Vpt* p,
    mmFloat64_t k,
    void* v)
{
    return v;
}

