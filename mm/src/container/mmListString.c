/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmListString.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmListStringIterator_Init(
    struct mmListStringIterator* p)
{
    mmString_Init(&p->v);
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListStringIterator_Destroy(
    struct mmListStringIterator* p)
{
    mmString_Destroy(&p->v);
    MM_LIST_INIT_HEAD(&p->n);
}

MM_EXPORT_DLL
void
mmListString_Init(
    struct mmListString* p)
{
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListString_Destroy(
    struct mmListString* p)
{
    mmListString_Clear(p);
    //
    MM_LIST_INIT_HEAD(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmListString_AddFont(
    struct mmListString* p,
    const struct mmString* v)
{
    struct mmListStringIterator* it;
    it = (struct mmListStringIterator*)mmMalloc(sizeof(struct mmListStringIterator));
    mmListStringIterator_Init(it);
    mmString_Assign(&it->v, v);
    mmList_Add(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
void
mmListString_AddTail(
    struct mmListString* p,
    const struct mmString* v)
{
    struct mmListStringIterator* it;
    it = (struct mmListStringIterator*)mmMalloc(sizeof(struct mmListStringIterator));
    mmListStringIterator_Init(it);
    mmString_Assign(&it->v, v);
    mmList_AddTail(&it->n, &p->l);
    p->size++;
}

MM_EXPORT_DLL
void
mmListString_PopFont(
    struct mmListString* p,
    struct mmString* v)
{
    struct mmListHead* next = p->l.next;
    struct mmListStringIterator* it = mmList_Entry(next, struct mmListStringIterator, n);
    mmString_Assign(v, &it->v);
    mmListString_Erase(p, it);
}

MM_EXPORT_DLL
void
mmListString_PopTail(
    struct mmListString* p,
    struct mmString* v)
{
    struct mmListHead* prev = p->l.prev;
    struct mmListStringIterator* it = mmList_Entry(prev, struct mmListStringIterator, n);
    mmString_Assign(v, &it->v);
    mmListString_Erase(p, it);
}

MM_EXPORT_DLL
void
mmListString_Erase(
    struct mmListString* p,
    struct mmListStringIterator* it)
{
    mmList_Del(&it->n);
    p->size--;
    mmListStringIterator_Destroy(it);
    mmFree(it);
}

MM_EXPORT_DLL
void
mmListString_Clear(
    struct mmListString* p)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListStringIterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListStringIterator, n);
        mmListString_Erase(p, it);
    }
}

MM_EXPORT_DLL
size_t
mmListString_Size(
    const struct mmListString* p)
{
    return p->size;
}

MM_EXPORT_DLL
void
mmListString_Remove(
    struct mmListString* p,
    const struct mmString* v)
{
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListStringIterator* it = NULL;
    next = p->l.next;
    while (next != &p->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListStringIterator, n);
        if (0 == mmString_Compare(&it->v, v))
        {
            mmListString_Erase(p, it);
        }
    }
}

MM_EXPORT_DLL
void
mmListString_InsertPrev(
    struct mmListString* p,
    struct mmListStringIterator* i,
    const struct mmString* v)
{
    struct mmListStringIterator* it;
    it = (struct mmListStringIterator*)mmMalloc(sizeof(struct mmListStringIterator));
    mmListStringIterator_Init(it);
    mmString_Assign(&it->v, v);
    mmList_AddTail(&it->n, &i->n);
    p->size++;
}

MM_EXPORT_DLL
void
mmListString_InsertNext(
    struct mmListString* p,
    struct mmListStringIterator* i,
    const struct mmString* v)
{
    struct mmListStringIterator* it;
    it = (struct mmListStringIterator*)mmMalloc(sizeof(struct mmListStringIterator));
    mmListStringIterator_Init(it);
    mmString_Assign(&it->v, v);
    mmList_Add(&it->n, &i->n);
    p->size++;
}
