/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVectorValue.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

static 
void 
__static_mmVectorValue_EventProduceRange(
    struct mmVectorValue* p, 
    size_t b, 
    size_t e)
{
    if (NULL != p->allocator.Produce)
    {
        typedef void(*ProduceFuncType)(void* e);
        size_t i = 0;
        ProduceFuncType Produce = (ProduceFuncType)p->allocator.Produce;
        for (i = b; i < e; i++)
        {
            (*(Produce))(p->arrays + i * p->element);
        }
    }
}

static 
void 
__static_mmVectorValue_EventRecycleRange(
    struct mmVectorValue* p, 
    size_t b, 
    size_t e)
{
    if (NULL != p->allocator.Recycle)
    {
        typedef void(*RecycleFuncType)(void* e);
        size_t i = 0;
        RecycleFuncType Recycle = (RecycleFuncType)p->allocator.Recycle;
        for (i = b; i < e; i++)
        {
            (*(Recycle))(p->arrays + i * p->element);
        }
    }
}

MM_EXPORT_DLL
void
mmVectorValueAllocator_Init(
    struct mmVectorValueAllocator* p)
{
    p->Produce = NULL;
    p->Recycle = NULL;
}

MM_EXPORT_DLL
void
mmVectorValueAllocator_Destroy(
    struct mmVectorValueAllocator* p)
{
    p->Produce = NULL;
    p->Recycle = NULL;
}

MM_EXPORT_DLL
void
mmVectorValue_Init(
    struct mmVectorValue* p)
{
    mmVectorValueAllocator_Init(&p->allocator);
    p->size = 0;
    p->capacity = 0;
    p->element = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorValue_Destroy(
    struct mmVectorValue* p)
{
    mmVectorValue_Free(p);
    //
    mmVectorValueAllocator_Destroy(&p->allocator);
    p->size = 0;
    p->capacity = 0;
    p->element = 0;
    p->arrays = NULL;
}

MM_EXPORT_DLL
void
mmVectorValue_Assign(
    struct mmVectorValue* p,
    const struct mmVectorValue* q)
{
    mmVectorValue_Realloc(p, q->capacity);
    p->size = q->size;
    p->element = q->element;
    p->allocator = q->allocator;
    mmMemcpy(p->arrays, q->arrays, p->size * p->element);
}

MM_EXPORT_DLL
void
mmVectorValue_SetElement(
    struct mmVectorValue* p,
    size_t element)
{
    p->element = element;
}

MM_EXPORT_DLL
void
mmVectorValue_SetAllocator(
    struct mmVectorValue* p,
    struct mmVectorValueAllocator* allocator)
{
    p->allocator = *allocator;
}

MM_EXPORT_DLL
size_t
mmVectorValue_GetElement(
    const struct mmVectorValue* p)
{
    return p->element;
}

MM_EXPORT_DLL
void
mmVectorValue_Clear(
    struct mmVectorValue* p)
{
    p->size = 0;
}

MM_EXPORT_DLL
void
mmVectorValue_Reset(
    struct mmVectorValue* p)
{
    mmVectorValue_Free(p);
}

MM_EXPORT_DLL
void
mmVectorValue_SetIndex(
    struct mmVectorValue* p,
    size_t index,
    void* e)
{
    assert(0 <= index && index < p->size);
    mmMemcpy(p->arrays + index * p->element, e, p->element);
}

MM_EXPORT_DLL
void*
mmVectorValue_GetIndex(
    const struct mmVectorValue* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays + index * p->element;
}

MM_EXPORT_DLL
void*
mmVectorValue_GetIndexReference(
    const struct mmVectorValue* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays + index * p->element;
}

MM_EXPORT_DLL
void
mmVectorValue_Realloc(
    struct mmVectorValue* p,
    size_t capacity)
{
    if (p->capacity < capacity)
    {
        mmUInt8_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        arrays = (mmUInt8_t*)mmMalloc(p->element * mem_size);
        mmMemcpy(arrays, p->arrays, p->element * p->capacity);
        mmMemset((mmUInt8_t*)arrays + p->element * p->capacity, 0, p->element * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        __static_mmVectorValue_EventProduceRange(p, p->capacity, mem_size);
        p->capacity = mem_size;
    }
    else if (capacity < (p->capacity / 4) && 8 < p->capacity)
    {
        mmUInt8_t* arrays = NULL;
        size_t mem_size = mmSizeAlign(capacity);
        mem_size <<= 1;
        arrays = (mmUInt8_t*)mmMalloc(p->element * mem_size);
        mmMemcpy(arrays, p->arrays, p->element * mem_size);
        __static_mmVectorValue_EventRecycleRange(p, mem_size, p->capacity);
        mmFree(p->arrays);
        p->arrays = arrays;
        p->capacity = mem_size;
    }
}

MM_EXPORT_DLL
void
mmVectorValue_Free(
    struct mmVectorValue* p)
{
    if (0 < p->capacity && NULL != p->arrays)
    {
        __static_mmVectorValue_EventRecycleRange(p, 0, p->capacity);
        mmFree(p->arrays);
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
    else
    {
        p->arrays = NULL;
        p->size = 0;
        p->capacity = 0;
    }
}

MM_EXPORT_DLL
void
mmVectorValue_Resize(
    struct mmVectorValue* p,
    size_t size)
{
    mmVectorValue_Realloc(p, size);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorValue_Reserve(
    struct mmVectorValue* p,
    size_t capacity)
{
    mmVectorValue_Realloc(p, capacity);
}

MM_EXPORT_DLL
void
mmVectorValue_AlignedMemory(
    struct mmVectorValue* p,
    size_t size)
{
    size_t sz = size;
    sz = sz < p->capacity ? p->capacity : sz;
    mmVectorValue_Realloc(p, sz);
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorValue_AllocatorMemory(
    struct mmVectorValue* p,
    size_t size)
{
    if (p->capacity < size)
    {
        mmUInt8_t* arrays = NULL;
        size_t mem_size = size;
        arrays = (mmUInt8_t*)mmMalloc(p->element * mem_size);
        mmMemcpy(arrays, p->arrays, p->element * p->capacity);
        mmMemset((mmUInt8_t*)arrays + p->element * p->capacity, 0, p->element * (mem_size - p->capacity));
        mmFree(p->arrays);
        p->arrays = arrays;
        __static_mmVectorValue_EventProduceRange(p, p->capacity, mem_size);
        p->capacity = mem_size;
    }
    p->size = size;
}

MM_EXPORT_DLL
void
mmVectorValue_PushBack(
    struct mmVectorValue* p,
    void* e)
{
    size_t new_size = p->size + 1;
    if (new_size > p->capacity)
    {
        // vector push back realloc strategy, *= 1.5
        size_t capacity = p->capacity + p->capacity / 2;
        capacity = capacity < new_size ? new_size : capacity;
        mmVectorValue_Realloc(p, capacity);
    }
    mmMemcpy(p->arrays + p->size++ * p->element, e, p->element);
}

MM_EXPORT_DLL
void*
mmVectorValue_PopBack(
    struct mmVectorValue* p)
{
    assert(0 < p->size && "mmVectorValue 0 < p->size");
    return p->arrays + (--p->size * p->element);
}

MM_EXPORT_DLL
void*
mmVectorValue_Begin(
    const struct mmVectorValue* p)
{
    return (p->arrays);
}

MM_EXPORT_DLL
void*
mmVectorValue_End(
    const struct mmVectorValue* p)
{
    return (p->arrays + p->size * p->element);
}

MM_EXPORT_DLL
void*
mmVectorValue_Back(
    const struct mmVectorValue* p)
{
    assert(0 < p->size && "mmVectorValue 0 < p->size");
    return (p->arrays + (p->size - 1) * p->element);
}

MM_EXPORT_DLL
void*
mmVectorValue_Next(
    const struct mmVectorValue* p,
    void* iter)
{
    return (mmUInt8_t*)iter + p->element;
}

MM_EXPORT_DLL
void*
mmVectorValue_At(
    const struct mmVectorValue* p,
    size_t index)
{
    assert(0 <= index && index < p->size);
    return p->arrays + index * p->element;
}

MM_EXPORT_DLL
size_t
mmVectorValue_Size(
    const struct mmVectorValue* p)
{
    return p->size;
}

MM_EXPORT_DLL
size_t
mmVectorValue_Capacity(
    const struct mmVectorValue* p)
{
    return p->capacity;
}

MM_EXPORT_DLL
void
mmVectorValue_Remove(
    struct mmVectorValue* p,
    size_t index)
{
    assert(0 < p->size && index <= p->size - 1 && "mmVectorValue 0 < p->size && index <= p->size - 1");
    if (index < p->size - 1)
    {
        mmMemmove(p->arrays + index * p->element, p->arrays + (index + 1) * p->element, (p->size - index - 1) * p->element);
        p->size--;
    }
    else
    {
        p->size--;
    }
}

MM_EXPORT_DLL
void*
mmVectorValue_Erase(
    struct mmVectorValue* p,
    void* iter)
{
    void* last = p->arrays + (p->size - 1);
    assert(0 < p->size && (p->arrays <= (mmUInt8_t*)iter) && iter <= last && "0 < p->size && first <= iter && iter <= last");
    if (iter < last)
    {
        mmMemmove(iter, (mmUInt8_t*)iter + p->element, (mmUInt8_t*)last - (mmUInt8_t*)iter);
        p->size--;
    }
    else
    {
        p->size--;
    }
    return ((mmUInt8_t*)iter + 1);
}

MM_EXPORT_DLL
int
mmVectorValue_Compare(
    const struct mmVectorValue* p,
    const struct mmVectorValue* q)
{
    if (p->size == q->size)
    {
        return (int)mmMemcmp(p->arrays, q->arrays, p->element * p->size);
    }
    else
    {
        return (int)(p->size - q->size);
    }
}

MM_EXPORT_DLL
void
mmVectorValue_CopyFromValue(
    struct mmVectorValue* p,
    const struct mmVectorValue* q)
{
    size_t i = 0;

    void* src = NULL;
    void* dst = NULL;

    assert(p->element == q->element && "p->element == q->element invalid.");

    mmVectorValue_Reset(p);

    mmVectorValue_AllocatorMemory(p, q->size);
    for (i = 0; i < q->size; i++)
    {
        src = mmVectorValue_At(q, i);
        dst = mmVectorValue_At(p, i);
        mmMemcpy(dst, src, q->element);
    }
}

MM_EXPORT_DLL
void
mmVectorValue_CopyFromType(
    struct mmVectorValue* p,
    const struct mmVectorValue* q,
    void* func)
{
    typedef void(*CopyFromFuncType)(void* src, void* dst);
    size_t i = 0;

    void* src = NULL;
    void* dst = NULL;

    CopyFromFuncType CopyFrom = (CopyFromFuncType)func;

    assert(p->element == q->element && "p->element == q->element invalid.");

    mmVectorValue_Reset(p);

    mmVectorValue_AllocatorMemory(p, q->size);
    for (i = 0; i < q->size; i++)
    {
        src = mmVectorValue_At(q, i);
        dst = mmVectorValue_At(p, i);
        (*(CopyFrom))(dst, src);
    }
}
