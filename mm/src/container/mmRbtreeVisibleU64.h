/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRbtreeVisibleU64_h__
#define __mmRbtreeVisibleU64_h__

#include "core/mmCore.h"
#include "core/mmAtomic.h"

#include "container/mmRbtreeU64.h"
#include "container/mmRbtsetU64.h"
#include "container/mmListU64.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// obj is struct mmRbtreeVisibleU64.
// note:
// rbtree_visible traver callback we lock instance_locker outside, so the v is valid.
// if at traver callback handle trigger other callback, do not lock the v.
// if lock v before trigger other callback, some time will case dead lock at other callback inside.
// although the v is valid but we can lock v at this function, but careful for it.
typedef void(*mmRbtreeVisibleU64HandleFunc)(void* obj, void* u, mmUInt64_t k, void* v);

// Suitable for:
//   1.multi-threading add and rmv.
//   2.multi-threading traverse.
//   3.multi-threading get.
//   4.allow traverse and main rbtree have little missing member(cache in two list).
struct mmRbtreeVisibleU64
{
    // main rbtree.
    struct mmRbtreeU64Vpt rbtree_m;
    // use for traverse
    struct mmRbtsetU64 rbtset_s;

    // cache for add when map_s is busy.
    struct mmListU64 l_a;
    // cache for add when map_s is busy.
    struct mmListU64 l_r;
    // 
    mmAtomic_t locker_m;
    mmAtomic_t locker_s;
    mmAtomic_t locker_a;
    mmAtomic_t locker_r;
};

MM_EXPORT_DLL 
void 
mmRbtreeVisibleU64_Init(
    struct mmRbtreeVisibleU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleU64_Destroy(
    struct mmRbtreeVisibleU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleU64_Add(
    struct mmRbtreeVisibleU64* p, 
    mmUInt64_t k, 
    void* v);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleU64_Rmv(
    struct mmRbtreeVisibleU64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void* 
mmRbtreeVisibleU64_Get(
    struct mmRbtreeVisibleU64* p, 
    mmUInt64_t k);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleU64_Clear(
    struct mmRbtreeVisibleU64* p);

MM_EXPORT_DLL 
size_t 
mmRbtreeVisibleU64_Size(
    const struct mmRbtreeVisibleU64* p);

MM_EXPORT_DLL 
void 
mmRbtreeVisibleU64_Traver(
    struct mmRbtreeVisibleU64* p, 
    mmRbtreeVisibleU64HandleFunc handle, 
    void* u);

#include "core/mmSuffix.h"

#endif//__mmRbtreeVisibleU64_h__
