/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVectorU64_h__
#define __mmVectorU64_h__

#include "core/mmCore.h"

#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

struct mmVectorU64
{
    size_t size;
    size_t capacity;
    mmUInt64_t* arrays;
};

MM_EXPORT_DLL 
void 
mmVectorU64_Init(
    struct mmVectorU64* p);

MM_EXPORT_DLL 
void 
mmVectorU64_Destroy(
    struct mmVectorU64* p);

MM_EXPORT_DLL 
void 
mmVectorU64_Assign(
    struct mmVectorU64* p, 
    const struct mmVectorU64* q);

MM_EXPORT_DLL 
void 
mmVectorU64_Clear(
    struct mmVectorU64* p);

MM_EXPORT_DLL 
void 
mmVectorU64_Reset(
    struct mmVectorU64* p);

MM_EXPORT_DLL 
void 
mmVectorU64_SetIndex(
    struct mmVectorU64* p, 
    size_t index, 
    mmUInt64_t e);

MM_EXPORT_DLL 
mmUInt64_t 
mmVectorU64_GetIndex(
    const struct mmVectorU64* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt64_t* 
mmVectorU64_GetIndexReference(
    const struct mmVectorU64* p, 
    size_t index);

MM_EXPORT_DLL 
void 
mmVectorU64_Realloc(
    struct mmVectorU64* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU64_Free(
    struct mmVectorU64* p);

MM_EXPORT_DLL 
void 
mmVectorU64_Resize(
    struct mmVectorU64* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU64_Reserve(
    struct mmVectorU64* p, 
    size_t capacity);

MM_EXPORT_DLL 
void 
mmVectorU64_AlignedMemory(
    struct mmVectorU64* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU64_AllocatorMemory(
    struct mmVectorU64* p, 
    size_t size);

MM_EXPORT_DLL 
void 
mmVectorU64_PushBack(
    struct mmVectorU64* p, 
    mmUInt64_t e);

MM_EXPORT_DLL 
mmUInt64_t 
mmVectorU64_PopBack(
    struct mmVectorU64* p);

MM_EXPORT_DLL 
mmUInt64_t* 
mmVectorU64_Begin(
    const struct mmVectorU64* p);

MM_EXPORT_DLL 
mmUInt64_t* 
mmVectorU64_End(
    const struct mmVectorU64* p);

MM_EXPORT_DLL 
mmUInt64_t* 
mmVectorU64_Back(
    const struct mmVectorU64* p);

MM_EXPORT_DLL 
mmUInt64_t* 
mmVectorU64_Next(
    const struct mmVectorU64* p, 
    mmUInt64_t* iter);

MM_EXPORT_DLL 
mmUInt64_t 
mmVectorU64_At(
    const struct mmVectorU64* p, 
    size_t index);

MM_EXPORT_DLL 
size_t 
mmVectorU64_Size(
    const struct mmVectorU64* p);

MM_EXPORT_DLL 
size_t 
mmVectorU64_Capacity(
    const struct mmVectorU64* p);

MM_EXPORT_DLL 
void 
mmVectorU64_Remove(
    struct mmVectorU64* p, 
    size_t index);

MM_EXPORT_DLL 
mmUInt64_t* 
mmVectorU64_Erase(
    struct mmVectorU64* p, 
    mmUInt64_t* iter);

// 0 is same.
MM_EXPORT_DLL 
int 
mmVectorU64_Compare(
    const struct mmVectorU64* p, 
    const struct mmVectorU64* q);

// copy from p <-> q, use memcpy.
MM_EXPORT_DLL 
void 
mmVectorU64_CopyFromValue(
    struct mmVectorU64* p, 
    const struct mmVectorU64* q);

// vector Null.
#define mmVectorU64_Null { 0, 0, NULL, }

// make a const weak vector.
#define mmVectorU64_Make(v)                   \
{                                             \
    MM_ARRAY_SIZE(v),      /* .size      = */ \
    MM_ARRAY_SIZE(v),      /* .max_size  = */ \
    (mmUInt64_t*)v,        /* .arrays    = */ \
}

#include "core/mmSuffix.h"

#endif//__mmVectorU64_h__
