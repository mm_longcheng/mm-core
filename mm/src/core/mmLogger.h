/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLogger_h__
#define __mmLogger_h__

#include "core/mmCore.h"

#include "core/mmString.h"
#include "core/mmErrno.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// [1992/01/26 09:13:14-520 8 V ]

enum mmLoggerLevel_t
{
    MM_LOG_UNKNOWN =  0,
    MM_LOG_FATAL   =  1,
    MM_LOG_CRIT    =  2,
    MM_LOG_ERROR   =  3,
    MM_LOG_ALERT   =  4,
    MM_LOG_WARNING =  5,
    MM_LOG_NOTICE  =  6,
    MM_LOG_INFO    =  7,
    MM_LOG_TRACE   =  8,
    MM_LOG_DEBUG   =  9,
    MM_LOG_VERBOSE = 10,
};

struct mmLevelMark
{
    // mark
    struct mmString m;
    // name
    struct mmString n;
};

MM_EXPORT_DLL const struct mmLevelMark* mmLoggerLevelMark(int lvl);

struct mmLogger;

typedef void(*mmLoggerFunc)(struct mmLogger* p, mmUInt32_t lvl, const char* message);

struct mmLogger
{
    mmLoggerFunc LoggerMessage;
    //weak ref for obj.
    void* obj;
};
// this length is recommend size, actually not limit here.
#define MM_MAX_LOGGER_LENGTH 1024
#define MM_MAX_LOGGER_FILE_NAME_LENGTH 512

MM_EXPORT_DLL extern struct mmLogger* mmLogger_Instance(void);

MM_EXPORT_DLL void mmLogger_Init(struct mmLogger* p);
MM_EXPORT_DLL void mmLogger_Destroy(struct mmLogger* p);
// assign callback.
MM_EXPORT_DLL void mmLogger_SetCallback(struct mmLogger* p, mmLoggerFunc callback, void* obj);

MM_EXPORT_DLL void mmLogger_Message(struct mmLogger* p, mmUInt32_t lvl, const char* fmt, ...) MM_STRING_ATTR_PRINTF(3, 4);
// default callback.
MM_EXPORT_DLL void mmLogger_LoggerMessagePrintf(struct mmLogger* p, mmUInt32_t lvl, const char* message);
//
#define mmLogger_LogU(p,...) mmLogger_Message(p,MM_LOG_UNKNOWN , __VA_ARGS__)
#define mmLogger_LogF(p,...) mmLogger_Message(p,MM_LOG_FATAL   , __VA_ARGS__)
#define mmLogger_LogC(p,...) mmLogger_Message(p,MM_LOG_CRIT    , __VA_ARGS__)
#define mmLogger_LogE(p,...) mmLogger_Message(p,MM_LOG_ERROR   , __VA_ARGS__)
#define mmLogger_LogA(p,...) mmLogger_Message(p,MM_LOG_ALERT   , __VA_ARGS__)
#define mmLogger_LogW(p,...) mmLogger_Message(p,MM_LOG_WARNING , __VA_ARGS__)
#define mmLogger_LogN(p,...) mmLogger_Message(p,MM_LOG_NOTICE  , __VA_ARGS__)
#define mmLogger_LogI(p,...) mmLogger_Message(p,MM_LOG_INFO    , __VA_ARGS__)
#define mmLogger_LogT(p,...) mmLogger_Message(p,MM_LOG_TRACE   , __VA_ARGS__)
#define mmLogger_LogD(p,...) mmLogger_Message(p,MM_LOG_DEBUG   , __VA_ARGS__)
#define mmLogger_LogV(p,...) mmLogger_Message(p,MM_LOG_VERBOSE , __VA_ARGS__)

#define mmAssert assert

// init the mmLogger_Instance before use this global macro.
#define mmLog_Init() mmLogger_Init(mmLogger_Instance())
#define mmLog_Destroy() mmLogger_Destroy(mmLogger_Instance())
#define mmLog_U(...) mmLogger_LogU(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_F(...) mmLogger_LogF(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_C(...) mmLogger_LogC(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_E(...) mmLogger_LogE(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_A(...) mmLogger_LogA(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_W(...) mmLogger_LogW(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_N(...) mmLogger_LogN(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_I(...) mmLogger_LogI(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_T(...) mmLogger_LogT(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_D(...) mmLogger_LogD(mmLogger_Instance(), __VA_ARGS__)
#define mmLog_V(...) mmLogger_LogV(mmLogger_Instance(), __VA_ARGS__)

#define SOCKET_ERROR_BUFFER_SIZE 1024
//
MM_EXPORT_DLL void mmLogger_SocketError(struct mmLogger* p, mmUInt32_t lvl, mmErr_t errcode);

#include "core/mmSuffix.h"

#endif//__mmLogger_h__
