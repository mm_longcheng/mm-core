/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUtf32String.h"
#include "core/mmAlloc.h"

static mmInline mmUInt32_t* mmUtf32_Allocate(size_t n)
{
    return (mmUInt32_t*)mmMalloc(n * sizeof(mmUInt32_t));
}
static mmInline void mmUtf32_Deallocate(mmUInt32_t* d, size_t n)
{
    mmFree(d);
}
static mmInline void mmUtf32_Copy(mmUInt32_t* t, const mmUInt32_t* s, size_t n)
{
    mmMemcpy(t, s, n * sizeof(mmUInt32_t));
}
static mmInline void mmUtf32_Move(mmUInt32_t* t, const mmUInt32_t* s, size_t n)
{
    mmMemmove(t, s, n * sizeof(mmUInt32_t));
}
static mmInline int mmUtf32_compare(const mmUInt32_t* l, const mmUInt32_t* r, size_t n)
{
    return mmMemcmp(l, r, n * sizeof(mmUInt32_t));
}
static mmInline void mmUtf32_Assignn(mmUInt32_t* d, size_t n, mmUInt32_t c)
{
    mmUInt32_t* r = d;
    for (; n; --n, ++r)
    {
        *r = c;
    }
}
static mmInline void mmUtf32_Assignc(mmUInt32_t* d, mmUInt32_t c)
{
    *d = c;
}

MM_EXPORT_DLL size_t mmUtf32Strlen(const mmUInt32_t* s)
{
    const mmUInt32_t* t = s;
    while (*t != 0)
    {
        ++t;
    }
    return (size_t)(t - s);
}

MM_EXPORT_DLL const size_t mmUtf32StringTinyMask = mmUtf32StringTinyMaskMacro;
MM_EXPORT_DLL const size_t mmUtf32StringLongMask = mmUtf32StringLongMaskMacro;
MM_EXPORT_DLL const size_t mmUtf32StringNpos = -1;

enum { mmUtf32StringAlignment = 16 };

static mmInline size_t mmUtf32String_MaxSize(void)
{
    static const size_t __m = SIZE_MAX;
#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
    return (__m <= ~mmUtf32StringLongMask ? __m : __m / 2) - mmUtf32StringAlignment;
#else
    return __m - mmUtf32StringAlignment;
#endif
}

static mmInline size_t mmUtf32String_AlignIt(size_t __s)
{
    // template <size_type __a>
    // return (__s + (__a-1)) & ~(__a-1);
    return (__s + (mmUtf32StringAlignment - 1)) & ~(mmUtf32StringAlignment - 1);
}

static mmInline size_t mmUtf32String_Recommend(size_t __s)
{
    if (__s < mmUtf32StringTinyCapacity) return mmUtf32StringTinyCapacity - 1;
    // <sizeof(value_type) < mmUtf32StringAlignment ? mmUtf32StringAlignment / sizeof(value_type) : 1>
    size_t __guess = mmUtf32String_AlignIt(__s + 1) - 1;
    return (__guess == mmUtf32StringTinyCapacity) ? ++__guess : __guess;
}

static mmInline mmBool_t mmUtf32String_IsLong(const struct mmUtf32String* p)
{
    return p->s.size & mmUtf32StringTinyMask;
}

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// bigger endian
static mmInline void mmUtf32String_SetTinySize(struct mmUtf32String* p, size_t size)
{
    p->s_size = (unsigned char)(size);
}
static mmInline size_t mmUtf32String_GetTinySize(const struct mmUtf32String* p)
{
    return p->s_size;
}

#else
// little endian
static mmInline void mmUtf32String_SetTinySize(struct mmUtf32String* p, size_t size)
{
    p->s.size = (unsigned char)(size << 1);
}

static mmInline size_t mmUtf32String_GetTinySize(const struct mmUtf32String* p)
{
    return p->s.size >> 1;
}

#endif

static mmInline void mmUtf32String_SetLongSize(struct mmUtf32String* p, size_t size)
{
    p->l.size = size;
}
static mmInline size_t mmUtf32String_GetLongSize(const struct mmUtf32String* p)
{
    return p->l.size;
}
static mmInline void mmUtf32String_SetSize(struct mmUtf32String* p, size_t size)
{
    if (mmUtf32String_IsLong(p))
    {
        mmUtf32String_SetLongSize(p, size);
    }
    else
    {
        mmUtf32String_SetTinySize(p, size);
    }
}

static mmInline void mmUtf32String_SetLongCapacity(struct mmUtf32String* p, size_t capacity)
{
    p->l.capacity = mmUtf32StringLongMask | capacity;
}
static mmInline size_t mmUtf32String_GetLongCapacity(const struct mmUtf32String* p)
{
    return p->l.capacity & (size_t)(~mmUtf32StringLongMask);
}


static mmInline void mmUtf32String_SetLongPointer(struct mmUtf32String* p, mmUtf32_t* data)
{
    p->l.data = data;
}
static mmInline mmUtf32_t* mmUtf32String_GetLongPointer(const struct mmUtf32String* p)
{
    return p->l.data;
}
static mmInline mmUtf32_t* mmUtf32String_GetTinyPointer(const struct mmUtf32String* p)
{
    return (mmUtf32_t*)&p->s.data[0];
}
static mmInline mmUtf32_t* mmUtf32String_GetPointer(const struct mmUtf32String* p)
{
    return mmUtf32String_IsLong(p) ? mmUtf32String_GetLongPointer(p) : mmUtf32String_GetTinyPointer(p);
}

static void mmUtf32String_EraseToEnd(struct mmUtf32String* p, size_t __pos)
{
    if (mmUtf32String_IsLong(p))
    {
        *(mmUtf32String_GetLongPointer(p) + __pos) = 0;
        mmUtf32String_SetLongSize(p, __pos);
    }
    else
    {
        *(mmUtf32String_GetTinyPointer(p) + __pos) = 0;
        mmUtf32String_SetTinySize(p, __pos);
    }
    // __invalidate_iterators_past(__pos);
}

static int mmUtf32String_GrowBy(struct mmUtf32String* p,
    size_t __old_cap, size_t __delta_cap, size_t __old_sz,
    size_t __n_copy, size_t __n_del, size_t __n_add)
{
    mmUtf32_t* __old_p; mmUtf32_t* __p;
    size_t __n1, __n2, __nm, __cap, __sec_cp_sz;

    size_t __ms = mmUtf32String_MaxSize();
    if (__delta_cap > __ms - __old_cap)
    {
        assert(0 && "mmUtf32String_GrowBy max size length error.");
        return EOF;
    }
    __old_p = mmUtf32String_GetPointer(p);
    __n1 = __old_cap + __delta_cap;
    __n2 = 2 * __old_cap;
    __nm = __n1 > __n2 ? __n1 : __n2;
    __cap = __old_cap < __ms / 2 - mmUtf32StringAlignment ? mmUtf32String_Recommend(__nm) : __ms - 1;
    __p = (mmUtf32_t*)mmUtf32_Allocate(__cap + 1);
    // __invalidate_all_iterators();
    if (__n_copy != 0)
    {
        mmUtf32_Move(__p, __old_p, __n_copy);
    }

    __sec_cp_sz = __old_sz - __n_del - __n_copy;
    if (__sec_cp_sz != 0)
    {
        mmUtf32_Move(__p + __n_copy + __n_add, __old_p + __n_copy + __n_del, __sec_cp_sz);
    }
    if (__old_cap + 1 != mmUtf32StringTinyCapacity)
    {
        // __alloc_traits::deallocate(__alloc(), __old_p, __old_cap + 1);
        mmUtf32_Deallocate(__old_p, __old_cap + 1);
    }
    mmUtf32String_SetLongPointer(p, __p);
    mmUtf32String_SetLongCapacity(p, __cap + 1);
    return 0;
}

static int mmUtf32String_GrowByAndReplace(struct mmUtf32String* p,
    size_t __old_cap, size_t __delta_cap, size_t __old_sz,
    size_t __n_copy, size_t __n_del, size_t __n_add, const mmUtf32_t* __p_new_stuff)
{
    mmUtf32_t* __old_p; mmUtf32_t* __p;
    size_t __b1, __b2, __bn, __cap, __sec_cp_sz;

    size_t __ms = mmUtf32String_MaxSize();
    if (__delta_cap > __ms - __old_cap - 1)
    {
        assert(0 && "mmUtf32String_GrowByAndReplace max size length error.");
        return EOF;
    }
    __old_p = mmUtf32String_GetPointer(p);
    __b1 = __old_cap + __delta_cap;
    __b2 = 2 * __old_cap;
    __bn = __b1 > __b2 ? __b1 : __b2;
    __cap = __old_cap < __ms / 2 - mmUtf32StringAlignment ? mmUtf32String_Recommend(__bn) : __ms - 1;
    __p = (mmUtf32_t*)mmUtf32_Allocate(__cap + 1);
    // __invalidate_all_iterators();
    if (__n_copy != 0)
    {
        mmUtf32_Copy(__p, __old_p, __n_copy);
    }
    if (__n_add != 0)
    {
        mmUtf32_Copy(__p + __n_copy, __p_new_stuff, __n_add);
    }
    __sec_cp_sz = __old_sz - __n_del - __n_copy;
    if (__sec_cp_sz != 0)
    {
        mmUtf32_Copy(__p + __n_copy + __n_add, __old_p + __n_copy + __n_del, __sec_cp_sz);
    }
    if (__old_cap + 1 != mmUtf32StringTinyCapacity)
    {
        // __alloc_traits::deallocate(__alloc(), __old_p, __old_cap + 1);
        mmUtf32_Deallocate(__old_p, __old_cap + 1);
    }
    mmUtf32String_SetLongPointer(p, __p);
    mmUtf32String_SetLongCapacity(p, __cap + 1);
    __old_sz = __n_copy + __n_add + __sec_cp_sz;
    mmUtf32String_SetLongSize(p, __old_sz);
    // traits_type::assign(__p[__old_sz], value_type());
    // __p[__old_sz] = 0;
    mmUtf32_Assignc(&__p[__old_sz], 0);
    return 0;
}

static int mmUtf32String_AppendChar(struct mmUtf32String* p, size_t __n, mmUtf32_t __c)
{
    int r = 0;
    if (__n)
    {
        mmUtf32_t* __p;
        size_t __cap = mmUtf32String_Capacity(p);
        size_t __sz = mmUtf32String_Size(p);
        if (__cap - __sz < __n)
        {
            r = mmUtf32String_GrowBy(p, __cap, __sz + __n - __cap, __sz, __sz, 0, 0);
        }
        __p = mmUtf32String_GetPointer(p);
        mmUtf32_Assignn(__p + __sz, __n, __c);
        __sz += __n;
        mmUtf32String_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf32_Assignc(&__p[__sz], 0);
    }
    return r;
}

static void mmUtf32String_Deallocate(struct mmUtf32String* p)
{
    size_t __cap = mmUtf32String_GetLongCapacity(p);
    if (mmUtf32String_IsLong(p) && 0 != __cap)
    {
        // long mode and long capacity equal 0 is weak or const reference.
        mmUtf32_t* __p = mmUtf32String_GetLongPointer(p);
        // __alloc_traits::deallocate(__alloc(), __p, __cap);
        mmUtf32_Deallocate(__p, __cap);
    }
}

MM_EXPORT_DLL void mmUtf32String_Init(struct mmUtf32String* p)
{
    mmMemset(p, 0, sizeof(struct mmUtf32String));
}
MM_EXPORT_DLL void mmUtf32String_Destroy(struct mmUtf32String* p)
{
    mmUtf32String_Deallocate(p);
    mmMemset(p, 0, sizeof(struct mmUtf32String));
}

MM_EXPORT_DLL int mmUtf32String_Assign(struct mmUtf32String* p, const struct mmUtf32String* __str)
{
    const mmUtf32_t* __p = mmUtf32String_GetPointer(__str);
    size_t __sz = mmUtf32String_Size(__str);
    return mmUtf32String_Assignsn(p, __p, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Assigns(struct mmUtf32String* p, const mmUtf32_t* __s)
{
    size_t __sz = mmUtf32Strlen(__s);
    return mmUtf32String_Assignsn(p, __s, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Assignsn(struct mmUtf32String* p, const mmUtf32_t* __s, size_t __n)
{
    size_t __cap = 0;
    assert((__n == 0 || __s != NULL) && "mmUtf32String_Assignsn received nullptr");
    __cap = mmUtf32String_Capacity(p);
    if (__cap >= __n)
    {
        mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        mmUtf32_Move(__p, __s, __n);
        // traits_type::assign(__p[__n], value_type());
        // __p[__n] = 0;
        mmUtf32_Assignc(&__p[__n], 0);
        mmUtf32String_SetSize(p, __n);
        // __invalidate_iterators_past(__n);
        return 0;
    }
    else
    {
        size_t __sz = mmUtf32String_Size(p);
        return mmUtf32String_GrowByAndReplace(p, __cap, __n - __cap, __sz, 0, __sz, __n, __s);
    }
}
MM_EXPORT_DLL int mmUtf32String_Assignnc(struct mmUtf32String* p, size_t __n, char __c)
{
    int r = 0;
    mmUtf32_t* __p = NULL;
    size_t __cap = mmUtf32String_Capacity(p);
    if (__cap < __n)
    {
        size_t __sz = mmUtf32String_Size(p);
        r = mmUtf32String_GrowBy(p, __cap, __n - __cap, __sz, 0, __sz, 0);
    }
    else
    {
        // __invalidate_iterators_past(__n);
    }
    __p = mmUtf32String_GetPointer(p);
    // traits_type::assign(__p, __n, __c);
    mmUtf32_Assignn(__p, __n, __c);
    // traits_type::assign(__p[__n], value_type());
    // __p[__n] = 0;
    mmUtf32_Assignc(&__p[__n], 0);
    mmUtf32String_SetSize(p, __n);
    return r;
}
MM_EXPORT_DLL int mmUtf32String_Insert(struct mmUtf32String* p, size_t __pos, const struct mmUtf32String* __str)
{
    const mmUtf32_t* __p = mmUtf32String_GetPointer(__str);
    size_t __sz = mmUtf32String_Size(__str);
    return mmUtf32String_Insertsn(p, __pos, __p, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Inserts(struct mmUtf32String* p, size_t __pos, const mmUtf32_t* __s)
{
    size_t __sz = mmUtf32Strlen(__s);
    return mmUtf32String_Insertsn(p, __pos, __s, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Insertsn(struct mmUtf32String* p, size_t __pos, const mmUtf32_t* __s, size_t __n)
{
    int r = 0;
    size_t __sz, __cap;
    assert((__n == 0 || __s != NULL) && "mmUtf32String_Insertsn received nullptr");
    __sz = mmUtf32String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf32String_Insertsn out of range error.");
        return EOF;
    }
    __cap = mmUtf32String_Capacity(p);
    if (__cap - __sz >= __n)
    {
        if (__n)
        {
            mmUtf32_t* __p = mmUtf32String_GetPointer(p);
            size_t __n_move = __sz - __pos;
            if (__n_move != 0)
            {
                if (__p + __pos <= __s && __s < __p + __sz)
                {
                    __s += __n;
                }
                mmUtf32_Move(__p + __pos + __n, __p + __pos, __n_move);
            }
            mmUtf32_Move(__p + __pos, __s, __n);
            __sz += __n;
            mmUtf32String_SetSize(p, __sz);
            // traits_type::assign(__p[__sz], value_type());
            // __p[__sz] = 0;
            mmUtf32_Assignc(&__p[__sz], 0);
        }
    }
    else
    {
        r = mmUtf32String_GrowByAndReplace(p, __cap, __sz + __n - __cap, __sz, __pos, 0, __n, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf32String_Insertnc(struct mmUtf32String* p, size_t __pos, size_t __n, mmUtf32_t __c)
{
    int r = 0;
    size_t __sz = mmUtf32String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf32String_Insertnc out of range error.");
        return EOF;
    }
    if (__n)
    {
        size_t __cap = mmUtf32String_Capacity(p);
        mmUtf32_t* __p;
        if (__cap - __sz >= __n)
        {
            size_t __n_move = __sz - __pos;
            __p = mmUtf32String_GetPointer(p);
            if (__n_move != 0)
            {
                mmUtf32_Move(__p + __pos + __n, __p + __pos, __n_move);
            }
        }
        else
        {
            r = mmUtf32String_GrowBy(p, __cap, __sz + __n - __cap, __sz, __pos, 0, __n);
            __p = mmUtf32String_GetLongPointer(p);
        }
        // traits_type::assign(__p + __pos, __n, __c);
        mmUtf32_Assignn(__p + __pos, __n, __c);
        __sz += __n;
        mmUtf32String_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf32_Assignc(&__p[__sz], 0);
    }
    return r;
}

MM_EXPORT_DLL int mmUtf32String_Erase(struct mmUtf32String* p, size_t __pos, size_t __n)
{
    size_t __sz = mmUtf32String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf32String_Erase out of range error.");
        return EOF;
    }
    if (__n)
    {
        mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        size_t __b1 = __n;
        size_t __b2 = __sz - __pos;
        size_t __bn = __b1 < __b2 ? __b1 : __b2;
        size_t __n_move;
        __n = __bn;
        __n_move = __sz - __pos - __n;
        if (__n_move != 0)
        {
            mmUtf32_Move(__p + __pos, __p + __pos + __n, __n_move);
        }
        __sz -= __n;
        mmUtf32String_SetSize(p, __sz);
        // __invalidate_iterators_past(__sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf32_Assignc(&__p[__sz], 0);
    }
    return 0;
}

MM_EXPORT_DLL size_t mmUtf32String_Size(const struct mmUtf32String* p)
{
    return mmUtf32String_IsLong(p) ? mmUtf32String_GetLongSize(p) : mmUtf32String_GetTinySize(p);
}
MM_EXPORT_DLL int mmUtf32String_Resize(struct mmUtf32String* p, size_t __n)
{
    return mmUtf32String_ResizeChar(p, __n, 0);
}
MM_EXPORT_DLL int mmUtf32String_ResizeChar(struct mmUtf32String* p, size_t __n, mmUtf32_t __c)
{
    int r = 0;
    size_t __sz = mmUtf32String_Size(p);
    if (__n > __sz)
    {
        r = mmUtf32String_AppendChar(p, __n - __sz, __c);
    }
    else
    {
        mmUtf32String_EraseToEnd(p, __n);
    }
    return r;
}
MM_EXPORT_DLL size_t mmUtf32String_Capacity(const struct mmUtf32String* p)
{
    return (mmUtf32String_IsLong(p) ? mmUtf32String_GetLongCapacity(p) : (size_t)(mmUtf32StringTinyCapacity)) - 1;
}
MM_EXPORT_DLL int mmUtf32String_Reserve(struct mmUtf32String* p, size_t __res_arg)
{
    size_t __cap, __sz;
    if (__res_arg > mmUtf32String_MaxSize())
    {
        assert(0 && "mmUtf32String_Reserve max size length error.");
        return EOF;
    }
    __cap = mmUtf32String_Capacity(p);
    __sz = mmUtf32String_Size(p);
    // max
    __res_arg = __res_arg > __sz ? __res_arg : __sz;
    __res_arg = mmUtf32String_Recommend(__res_arg);
    if (__res_arg != __cap)
    {
        mmUtf32_t* __new_data; mmUtf32_t* __p;
        mmBool_t __was_long, __now_long;
        if (__res_arg == mmUtf32StringTinyCapacity - 1)
        {
            __was_long = MM_TRUE;
            __now_long = MM_FALSE;
            __new_data = mmUtf32String_GetTinyPointer(p);
            __p = mmUtf32String_GetLongPointer(p);
        }
        else
        {
            __new_data = (mmUtf32_t*)mmUtf32_Allocate(__res_arg + 1);
            if (__new_data == NULL)
            {
                return EOF;
            }
            __now_long = MM_TRUE;
            __was_long = mmUtf32String_IsLong(p);
            __p = mmUtf32String_GetPointer(p);
        }
        mmUtf32_Copy(__new_data, __p, mmUtf32String_Size(p) + 1);
        if (__was_long)
        {
            // __alloc_traits::deallocate(__alloc(), __p, __cap + 1);
            mmUtf32_Deallocate(__p, __cap + 1);
        }
        if (__now_long)
        {
            mmUtf32String_SetLongCapacity(p, __res_arg + 1);
            mmUtf32String_SetLongSize(p, __sz);
            mmUtf32String_SetLongPointer(p, __new_data);
        }
        else
        {
            mmUtf32String_SetTinySize(p, __sz);
        }
        // __invalidate_all_iterators();
    }
    return 0;
}
MM_EXPORT_DLL void mmUtf32String_Clear(struct mmUtf32String* p)
{
    // __invalidate_all_iterators();
    if (mmUtf32String_IsLong(p))
    {
        *mmUtf32String_GetLongPointer(p) = 0;
        mmUtf32String_SetLongSize(p, 0);
    }
    else
    {
        *mmUtf32String_GetTinyPointer(p) = 0;
        mmUtf32String_SetTinySize(p, 0);
    }
}
MM_EXPORT_DLL mmBool_t mmUtf32String_Empty(const struct mmUtf32String* p)
{
    return (0 == mmUtf32String_Size(p));
}
MM_EXPORT_DLL int mmUtf32String_ShrinkToFit(struct mmUtf32String* p)
{
    return mmUtf32String_Reserve(p, 0);
}

MM_EXPORT_DLL mmUtf32_t mmUtf32String_At(const struct mmUtf32String* p, size_t __n)
{
    if (__n >= mmUtf32String_Size(p))
    {
        assert(0 && "mmUtf32String_At out of range error.");
        return 0;
    }
    else
    {
        return *(mmUtf32String_GetPointer(p) + __n);
    }
}

MM_EXPORT_DLL int mmUtf32String_Append(struct mmUtf32String* p, const struct mmUtf32String* __str)
{
    const mmUtf32_t* __p = mmUtf32String_GetPointer(__str);
    size_t __sz = mmUtf32String_Size(__str);
    return mmUtf32String_Appendsn(p, __p, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Appends(struct mmUtf32String* p, const mmUtf32_t* __s)
{
    size_t __sz = mmUtf32Strlen(__s);
    return mmUtf32String_Appendsn(p, __s, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Appendsn(struct mmUtf32String* p, const mmUtf32_t* __s, size_t __n)
{
    int r = 0;
    size_t __cap, __sz;
    assert((__n == 0 || __s != NULL) && "mmUtf32String_Appendsn received nullptr");
    __cap = mmUtf32String_Capacity(p);
    __sz = mmUtf32String_Size(p);
    if (__cap - __sz >= __n)
    {
        if (__n)
        {
            mmUtf32_t* __p = mmUtf32String_GetPointer(p);
            mmUtf32_Copy(__p + __sz, __s, __n);
            __sz += __n;
            mmUtf32String_SetSize(p, __sz);
            // traits_type::assign(__p[__sz], value_type());
            // __p[__sz] = 0;
            mmUtf32_Assignc(&__p[__sz], 0);
        }
    }
    else
    {
        r = mmUtf32String_GrowByAndReplace(p, __cap, __sz + __n - __cap, __sz, __sz, 0, __n, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf32String_Appendnc(struct mmUtf32String* p, size_t __n, mmUtf32_t __c)
{
    int r = 0;
    if (__n)
    {
        mmUtf32_t* __p = NULL;
        size_t __cap = mmUtf32String_Capacity(p);
        size_t __sz = mmUtf32String_Size(p);
        if (__cap - __sz < __n)
        {
            r = mmUtf32String_GrowBy(p, __cap, __sz + __n - __cap, __sz, __sz, 0, 0);
        }
        __p = mmUtf32String_GetPointer(p);
        // traits_type::assign(_VSTD::__to_raw_pointer(__p) + __sz, __n, __c);
        mmUtf32_Assignn(__p + __sz, __n, __c);
        __sz += __n;
        mmUtf32String_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        mmUtf32_Assignc(__p + __sz, 0);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf32String_Appendc(struct mmUtf32String* p, mmUtf32_t __c)
{
    return mmUtf32String_PushBack(p, __c);
}
MM_EXPORT_DLL int mmUtf32String_PushBack(struct mmUtf32String* p, mmUtf32_t __c)
{
    int r = 0;
    mmBool_t __is_short = !mmUtf32String_IsLong(p);
    size_t __cap;
    size_t __sz;
    mmUtf32_t* __p;
    if (__is_short)
    {
        __cap = mmUtf32StringTinyCapacity - 1;
        __sz = mmUtf32String_GetTinySize(p);
    }
    else
    {
        __cap = mmUtf32String_GetLongCapacity(p) - 1;
        __sz = mmUtf32String_GetLongSize(p);
    }
    if (__sz == __cap)
    {
        r = mmUtf32String_GrowBy(p, __cap, 1, __sz, __sz, 0, 0);
        __is_short = !mmUtf32String_IsLong(p);
    }

    if (__is_short)
    {
        __p = mmUtf32String_GetTinyPointer(p) + __sz;
        mmUtf32String_SetTinySize(p, __sz + 1);
    }
    else
    {
        __p = mmUtf32String_GetLongPointer(p) + __sz;
        mmUtf32String_SetLongSize(p, __sz + 1);
    }
    (*__p) = __c;
    (*++__p) = 0;
    return r;
}
MM_EXPORT_DLL void mmUtf32String_PopBack(struct mmUtf32String* p)
{
    size_t __sz;
    assert(!mmUtf32String_Empty(p) && "mmUtf32String_PopBack string is already empty");
    if (mmUtf32String_IsLong(p))
    {
        __sz = mmUtf32String_GetLongSize(p) - 1;
        mmUtf32String_SetLongSize(p, __sz);
        *(mmUtf32String_GetLongPointer(p) + __sz) = 0;
    }
    else
    {
        __sz = mmUtf32String_GetTinySize(p) - 1;
        mmUtf32String_SetTinySize(p, __sz);
        *(mmUtf32String_GetTinyPointer(p) + __sz) = 0;
    }
    // __invalidate_iterators_past(__sz);
}

MM_EXPORT_DLL int mmUtf32String_Replace(struct mmUtf32String* p, size_t __pos, size_t __n1, const struct mmUtf32String* __str)
{
    const mmUtf32_t* __p = mmUtf32String_GetPointer(__str);
    size_t __sz = mmUtf32String_Size(__str);
    return mmUtf32String_Replacesn(p, __pos, __n1, __p, __sz);
}
MM_EXPORT_DLL int mmUtf32String_Replaces(struct mmUtf32String* p, size_t __pos, size_t __n1, const mmUtf32_t* __s)
{
    size_t __sz = mmUtf32Strlen(__s);
    return mmUtf32String_Replacesn(p, __pos, __n1, __s, __sz);
}

MM_EXPORT_DLL int mmUtf32String_Replacesn(struct mmUtf32String* p, size_t __pos, size_t __n1, const mmUtf32_t* __s, size_t __n2)
{
    int r = 0;
    size_t __sz, __b1, __b2, __bn, __cap;
    assert((__n2 == 0 || __s != NULL) && "mmUtf32String_Replace received nullptr");
    __sz = mmUtf32String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf32String_Replacesn out of range error.");
        return EOF;
    }
    __b1 = __n1;
    __b2 = __sz - __pos;
    __bn = __b1 < __b2 ? __b1 : __b2;
    __n1 = __bn;
    __cap = mmUtf32String_Capacity(p);
    if (__cap - __sz + __n1 >= __n2)
    {
        mmUtf32_t* __p = mmUtf32String_GetPointer(p);

        do
        {
            if (__n1 != __n2)
            {
                size_t __n_move = __sz - __pos - __n1;
                if (__n_move != 0)
                {
                    if (__n1 > __n2)
                    {
                        mmUtf32_Move(__p + __pos, __s, __n2);
                        mmUtf32_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
                        break;
                    }
                    if (__p + __pos < __s && __s < __p + __sz)
                    {
                        if (__p + __pos + __n1 <= __s)
                        {
                            __s += __n2 - __n1;
                        }
                        else // __p + __pos < __s < __p + __pos + __n1
                        {
                            mmUtf32_Move(__p + __pos, __s, __n1);
                            __pos += __n1;
                            __s += __n2;
                            __n2 -= __n1;
                            __n1 = 0;
                        }
                    }
                    mmUtf32_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
                }
            }
            mmUtf32_Move(__p + __pos, __s, __n2);
        } while (0);

        // __sz += __n2 - __n1; in this and the below function below can cause unsigned integer overflow,
        // but this is a safe operation, so we disable the check.
        __sz += __n2 - __n1;
        mmUtf32String_SetSize(p, __sz);
        // __invalidate_iterators_past(__sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf32_Assignc(&__p[__sz], 0);
    }
    else
    {
        r = mmUtf32String_GrowByAndReplace(p, __cap, __sz - __n1 + __n2 - __cap, __sz, __pos, __n1, __n2, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf32String_Replacenc(struct mmUtf32String* p, size_t __pos, size_t __n1, size_t __n2, mmUtf32_t __c)
{
    int r = 0;
    size_t __sz, __b1, __b2, __bn, __cap;
    mmUtf32_t* __p;
    __sz = mmUtf32String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Replacenc out of range error.");
        return EOF;
    }
    __b1 = __n1;
    __b2 = __sz - __pos;
    __bn = __b1 < __b2 ? __b1 : __b2;
    __n1 = __bn;
    __cap = mmUtf32String_Capacity(p);
    if (__cap - __sz + __n1 >= __n2)
    {
        __p = mmUtf32String_GetPointer(p);
        if (__n1 != __n2)
        {
            size_t __n_move = __sz - __pos - __n1;
            if (__n_move != 0)
            {
                mmUtf32_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
            }
        }
    }
    else
    {
        r = mmUtf32String_GrowBy(p, __cap, __sz - __n1 + __n2 - __cap, __sz, __pos, __n1, __n2);
        __p = mmUtf32String_GetLongPointer(p);
    }
    // traits_type::assign(__p + __pos, __n2, __c);
    mmUtf32_Assignn(__p + __pos, __n2, __c);
    __sz += __n2 - __n1;
    mmUtf32String_SetSize(p, __sz);
    // __invalidate_iterators_past(__sz);
    // traits_type::assign(__p[__sz], value_type());
    // __p[__sz] = 0;
    mmUtf32_Assignc(&__p[__sz], 0);
    return r;
}

MM_EXPORT_DLL size_t mmUtf32String_ReplaceChar(struct mmUtf32String* p, mmUtf32_t s, mmUtf32_t t)
{
    size_t n = 0;
    size_t __sz = mmUtf32String_Size(p);
    if (0 < __sz)
    {
        size_t index = 0;
        mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        for (index = 0; index < __sz; ++index)
        {
            if (__p[index] == s)
            {
                __p[index] = t;
                n++;
            }
        }
    }
    return n;
}

MM_EXPORT_DLL const mmUtf32_t* mmUtf32String_CStr(const struct mmUtf32String* p)
{
    return mmUtf32String_GetPointer(p);
}
MM_EXPORT_DLL const mmUtf32_t* mmUtf32String_Data(const struct mmUtf32String* p)
{
    return mmUtf32String_GetPointer(p);
}

MM_EXPORT_DLL size_t mmUtf32String_FindLastOf(const struct mmUtf32String* p, mmUtf32_t c)
{
    size_t index = mmUtf32StringNpos;
    size_t __sz = mmUtf32String_Size(p);
    if (0 < __sz)
    {
        const mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        size_t idx = 0;
        for (idx = __sz - 1; mmUtf32StringNpos != idx; --idx)
        {
            if (__p[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmUtf32String_FindFirstOf(const struct mmUtf32String* p, mmUtf32_t c)
{
    size_t index = mmUtf32StringNpos;
    size_t __sz = mmUtf32String_Size(p);
    if (0 < __sz)
    {
        const mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        size_t idx = 0;
        for (idx = 0; idx < __sz; ++idx)
        {
            if (__p[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmUtf32String_FindLastNotOf(const struct mmUtf32String* p, mmUtf32_t c)
{
    size_t index = mmUtf32StringNpos;
    size_t __sz = mmUtf32String_Size(p);
    if (0 < __sz)
    {
        const mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        size_t idx = 0;
        for (idx = __sz - 1; mmUtf32StringNpos != idx; --idx)
        {
            if (__p[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmUtf32String_FindFirstNotOf(const struct mmUtf32String* p, mmUtf32_t c)
{
    size_t index = mmUtf32StringNpos;
    size_t __sz = mmUtf32String_Size(p);
    if (0 < __sz)
    {
        const mmUtf32_t* __p = mmUtf32String_GetPointer(p);
        size_t idx = 0;
        for (idx = 0; idx < __sz; ++idx)
        {
            if (__p[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL void mmUtf32String_Substr(const struct mmUtf32String* p, struct mmUtf32String* s, size_t o, size_t l)
{
    size_t __sz = mmUtf32String_Size(p);
    const mmUtf32_t* __p = mmUtf32String_GetPointer(p);
    size_t h = __sz - o;
    size_t r = (l > h) ? h : l;
    mmUtf32String_Assignsn(s, &(__p[o]), r);
}

// return 0 is equal.
MM_EXPORT_DLL int mmUtf32String_Compare(const struct mmUtf32String* p, const struct mmUtf32String* __str)
{
    size_t __lhs_sz = mmUtf32String_Size(p);
    const mmUtf32_t* __lhs_pt = mmUtf32String_GetPointer(p);
    size_t __rhs_sz = mmUtf32String_Size(__str);
    const mmUtf32_t* __rhs_pt = mmUtf32String_GetPointer(__str);
    size_t __nm = __lhs_sz < __rhs_sz ? __lhs_sz : __rhs_sz;
    int __result = mmUtf32_compare(__lhs_pt, __rhs_pt, __nm);
    if (__result != 0) { return __result; }
    if (__lhs_sz < __rhs_sz) { return -1; }
    if (__lhs_sz > __rhs_sz) { return 1; }
    return 0;
}
// return 0 is equal.
MM_EXPORT_DLL int mmUtf32String_CompareCStr(const struct mmUtf32String* p, const mmUtf32_t* __s)
{
    struct mmUtf32String __str;
    mmUtf32String_MakeWeaks(&__str, __s);
    return mmUtf32String_Compare(p, &__str);
}

// empty string.
static mmUtf32_t gUtf16StringEmpty[1] = { 0 };
MM_EXPORT_DLL const mmUtf32_t* mmUtf32StringEmpty = gUtf16StringEmpty;

MM_EXPORT_DLL void mmUtf32String_MakeWeak(struct mmUtf32String* p, const struct mmUtf32String* __str)
{
    const mmUtf32_t* __s = mmUtf32String_CStr(__str);
    size_t __n = mmUtf32String_Size(__str);
    mmUtf32String_MakeWeaksn(p, __s, __n);
}

MM_EXPORT_DLL void mmUtf32String_MakeWeaks(struct mmUtf32String* p, const mmUtf32_t* __s)
{
    size_t __n = mmUtf32Strlen(__s);
    mmUtf32String_MakeWeaksn(p, __s, __n);
}

MM_EXPORT_DLL void mmUtf32String_MakeWeaksn(struct mmUtf32String* p, const mmUtf32_t* __s, size_t __n)
{
    // const reference long capacity is 0.
    p->l.capacity = mmUtf32StringLongMask;
    p->l.size = __n;
    p->l.data = (mmUtf32_t*)__s;
}

MM_EXPORT_DLL void mmUtf32String_SetSizeValue(struct mmUtf32String* p, size_t size)
{
    mmUtf32String_SetSize(p, size);
}

MM_EXPORT_DLL void mmUtf32String_Reset(struct mmUtf32String* p)
{
    mmUtf32String_Deallocate(p);
    mmMemset(p, 0, sizeof(struct mmUtf32String));
}
