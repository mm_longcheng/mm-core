/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmString.h"
#include "core/mmAlloc.h"

static mmInline char* mmUtf8_Allocate(size_t n)
{
    return (char*)mmMalloc(n);
}
static mmInline void mmUtf8_Deallocate(char* d, size_t n)
{
    mmFree(d);
}
static mmInline void mmUtf8_Copy(char* t, const char* s, size_t n)
{
    mmMemcpy(t, s, n);
}
static mmInline void mmUtf8_Move(char* t, const char* s, size_t n)
{
    mmMemmove(t, s, n);
}
static mmInline int mmUtf8_compare(const char* l, const char* r, size_t n)
{
    return mmMemcmp(l, r, n);
}
static mmInline void mmUtf8_Assignn(char* d, size_t n, char c)
{
    mmMemset(d, c, n);
}
static mmInline void mmUtf8_Assignc(char* d, char c)
{
    *d = c;
}

MM_EXPORT_DLL size_t mmUtf8Strlen(const mmUtf8_t* s)
{
    return strlen(s);
}

MM_EXPORT_DLL const size_t mmStringTinyMask = mmStringTinyMaskMacro;
MM_EXPORT_DLL const size_t mmStringLongMask = mmStringLongMaskMacro;
MM_EXPORT_DLL const size_t mmStringNpos = -1;

enum { mmStringAlignment = 16 };

static mmInline size_t mmString_MaxSize(void)
{
    static const size_t __m = SIZE_MAX;
#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
    return (__m <= ~mmStringLongMask ? __m : __m / 2) - mmStringAlignment;
#else
    return __m - mmStringAlignment;
#endif
}

static mmInline size_t mmString_AlignIt(size_t __s)
{
    // template <size_type __a>
    // return (__s + (__a-1)) & ~(__a-1);
    return (__s + (mmStringAlignment - 1)) & ~(mmStringAlignment - 1);
}

static mmInline size_t mmString_Recommend(size_t __s)
{
    if (__s < mmStringTinyCapacity) return mmStringTinyCapacity - 1;
    // <sizeof(value_type) < mmStringAlignment ? mmStringAlignment / sizeof(value_type) : 1>
    size_t __guess = mmString_AlignIt(__s + 1) - 1;
    return (__guess == mmStringTinyCapacity) ? ++__guess : __guess;
}

static mmInline mmBool_t mmString_IsLong(const struct mmString* p)
{
    return p->s.size & mmStringTinyMask;
}

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// bigger endian
static mmInline void mmString_SetTinySize(struct mmString* p, size_t size)
{
    p->s_size = (unsigned char)(size);
}
static mmInline size_t mmString_GetTinySize(const struct mmString* p)
{
    return p->s_size;
}

#else
// little endian
static mmInline void mmString_SetTinySize(struct mmString* p, size_t size)
{
    p->s.size = (unsigned char)(size << 1);
}

static mmInline size_t mmString_GetTinySize(const struct mmString* p)
{
    return p->s.size >> 1;
}

#endif

static mmInline void mmString_SetLongSize(struct mmString* p, size_t size)
{
    p->l.size = size;
}
static mmInline size_t mmString_GetLongSize(const struct mmString* p)
{
    return p->l.size;
}
static mmInline void mmString_SetSize(struct mmString* p, size_t size)
{
    if (mmString_IsLong(p))
    {
        mmString_SetLongSize(p, size);
    }
    else
    {
        mmString_SetTinySize(p, size);
    }
}

static mmInline void mmString_SetLongCapacity(struct mmString* p, size_t capacity)
{
    p->l.capacity = mmStringLongMask | capacity;
}
static mmInline size_t mmString_GetLongCapacity(const struct mmString* p)
{
    return p->l.capacity & (size_t)(~mmStringLongMask);
}


static mmInline void mmString_SetLongPointer(struct mmString* p, char* data)
{
    p->l.data = data;
}
static mmInline char* mmString_GetLongPointer(const struct mmString* p)
{
    return p->l.data;
}
static mmInline char* mmString_GetTinyPointer(const struct mmString* p)
{
    return (char*)&p->s.data[0];
}
static mmInline char* mmString_GetPointer(const struct mmString* p)
{
    return mmString_IsLong(p) ? mmString_GetLongPointer(p) : mmString_GetTinyPointer(p);
}

static void mmString_EraseToEnd(struct mmString* p, size_t __pos)
{
    if (mmString_IsLong(p))
    {
        *(mmString_GetLongPointer(p) + __pos) = 0;
        mmString_SetLongSize(p, __pos);
    }
    else
    {
        *(mmString_GetTinyPointer(p) + __pos) = 0;
        mmString_SetTinySize(p, __pos);
    }
    // __invalidate_iterators_past(__pos);
}

static int mmString_GrowBy(struct mmString* p, 
    size_t __old_cap, size_t __delta_cap, size_t __old_sz,
    size_t __n_copy, size_t __n_del, size_t __n_add)
{
    char* __old_p; char* __p;
    size_t __n1, __n2, __nm, __cap, __sec_cp_sz;

    size_t __ms = mmString_MaxSize();
    if (__delta_cap > __ms - __old_cap)
    {
        assert(0 && "mmString_GrowBy max size length error.");
        return EOF;
    }
    __old_p = mmString_GetPointer(p);
    __n1 = __old_cap + __delta_cap;
    __n2 = 2 * __old_cap;
    __nm = __n1 > __n2 ? __n1 : __n2;
    __cap = __old_cap < __ms / 2 - mmStringAlignment ? mmString_Recommend(__nm) : __ms - 1;
    __p = (char*)mmUtf8_Allocate(__cap + 1);
    // __invalidate_all_iterators();
    if (__n_copy != 0)
    {
        mmUtf8_Move(__p, __old_p, __n_copy);
    }

    __sec_cp_sz = __old_sz - __n_del - __n_copy;
    if (__sec_cp_sz != 0)
    {
        mmUtf8_Move(__p + __n_copy + __n_add, __old_p + __n_copy + __n_del, __sec_cp_sz);
    }
    if (__old_cap + 1 != mmStringTinyCapacity)
    {
        // __alloc_traits::deallocate(__alloc(), __old_p, __old_cap + 1);
        mmUtf8_Deallocate(__old_p, __old_cap + 1);
    }
    mmString_SetLongPointer(p, __p);
    mmString_SetLongCapacity(p, __cap + 1);
    return 0;
}

static int mmString_GrowByAndReplace(struct mmString* p,
    size_t __old_cap, size_t __delta_cap, size_t __old_sz,
    size_t __n_copy, size_t __n_del, size_t __n_add, const char* __p_new_stuff)
{
    char* __old_p; char* __p;
    size_t __b1, __b2, __bn, __cap, __sec_cp_sz;

    size_t __ms = mmString_MaxSize();
    if (__delta_cap > __ms - __old_cap - 1)
    {
        assert(0 && "mmString_GrowByAndReplace max size length error.");
        return EOF;
    }
    __old_p = mmString_GetPointer(p);
    __b1 = __old_cap + __delta_cap;
    __b2 = 2 * __old_cap;
    __bn = __b1 > __b2 ? __b1 : __b2;
    __cap = __old_cap < __ms / 2 - mmStringAlignment ? mmString_Recommend(__bn) : __ms - 1;
    __p = (char*)mmUtf8_Allocate(__cap + 1);
    // __invalidate_all_iterators();
    if (__n_copy != 0)
    {
        mmUtf8_Copy(__p, __old_p, __n_copy);
    }
    if (__n_add != 0)
    {
        mmUtf8_Copy(__p + __n_copy, __p_new_stuff, __n_add);
    }
    __sec_cp_sz = __old_sz - __n_del - __n_copy;
    if (__sec_cp_sz != 0)
    {
        mmUtf8_Copy(__p + __n_copy + __n_add, __old_p + __n_copy + __n_del, __sec_cp_sz);
    }
    if (__old_cap + 1 != mmStringTinyCapacity)
    {
        // __alloc_traits::deallocate(__alloc(), __old_p, __old_cap + 1);
        mmUtf8_Deallocate(__old_p, __old_cap + 1);
    }
    mmString_SetLongPointer(p, __p);
    mmString_SetLongCapacity(p, __cap + 1);
    __old_sz = __n_copy + __n_add + __sec_cp_sz;
    mmString_SetLongSize(p, __old_sz);
    // traits_type::assign(__p[__old_sz], value_type());
    // __p[__old_sz] = 0;
    mmUtf8_Assignc(&__p[__old_sz], 0);
    return 0;
}

static int mmString_AppendChar(struct mmString* p, size_t __n, char __c)
{
    int r = 0;
    if (__n)
    {
        char* __p;
        size_t __cap = mmString_Capacity(p);
        size_t __sz = mmString_Size(p);
        if (__cap - __sz < __n)
        {
            r = mmString_GrowBy(p, __cap, __sz + __n - __cap, __sz, __sz, 0, 0);
        }
        __p = mmString_GetPointer(p);
        mmUtf8_Assignn(__p + __sz, __n, __c);
        __sz += __n;
        mmString_SetSize(p, __sz);
        // traits_type::assign(__p[_sz], value_type());
        // __p[__sz] = 0;
        mmUtf8_Assignc(&__p[__sz], 0);
    }
    return r;
}

static void mmString_Deallocate(struct mmString* p)
{
    size_t __cap = mmString_GetLongCapacity(p);
    if (mmString_IsLong(p) && 0 != __cap)
    {
        // long mode and long capacity equal 0 is weak or const reference.
        char* __p = mmString_GetLongPointer(p);
        // __alloc_traits::deallocate(__alloc(), __p, __cap);
        mmUtf8_Deallocate(__p, __cap);
    }
}

MM_EXPORT_DLL void mmString_Init(struct mmString* p)
{
    mmMemset(p, 0, sizeof(struct mmString));
}
MM_EXPORT_DLL void mmString_Destroy(struct mmString* p)
{
    mmString_Deallocate(p);
    mmMemset(p, 0, sizeof(struct mmString));
}

MM_EXPORT_DLL int mmString_Assign(struct mmString* p,const struct mmString* __str)
{
    const char* __p = mmString_GetPointer(__str);
    size_t __sz = mmString_Size(__str);
    return mmString_Assignsn(p, __p, __sz);
}
MM_EXPORT_DLL int mmString_Assigns(struct mmString* p, const char* __s)
{
    size_t __sz = strlen(__s);
    return mmString_Assignsn(p, __s, __sz);
}
MM_EXPORT_DLL int mmString_Assignsn(struct mmString* p, const char* __s, size_t __n)
{
    size_t __cap = 0;
    assert((__n == 0 || __s != NULL) && "mmString_Assignsn received nullptr");
    __cap = mmString_Capacity(p);
    if (__cap >= __n)
    {
        char* __p = mmString_GetPointer(p);
        mmUtf8_Move(__p, __s, __n);
        // traits_type::assign(__p[__n], value_type());
        // __p[__n] = 0;
        mmUtf8_Assignc(&__p[__n], 0);
        mmString_SetSize(p, __n);
        // __invalidate_iterators_past(__n);
        return 0;
    }
    else
    {
        size_t __sz = mmString_Size(p);
        return mmString_GrowByAndReplace(p, __cap, __n - __cap, __sz, 0, __sz, __n, __s);
    }
}

MM_EXPORT_DLL int mmString_Assignnc(struct mmString* p, size_t __n, char __c)
{
    int r = 0;
    char* __p = NULL;
    size_t __cap = mmString_Capacity(p);
    if (__cap < __n)
    {
        size_t __sz = mmString_Size(p);
        r = mmString_GrowBy(p, __cap, __n - __cap, __sz, 0, __sz, 0);
    }
    else
    {
        // __invalidate_iterators_past(__n);
    }
    __p = mmString_GetPointer(p);
    // traits_type::assign(__p, __n, __c);
    mmUtf8_Assignn(__p, __n, __c);
    // traits_type::assign(__p[__n], value_type());
    // __p[__n] = 0;
    mmUtf8_Assignc(&__p[__n], 0);
    mmString_SetSize(p, __n);
    return r;
}

MM_EXPORT_DLL int mmString_Insert(struct mmString* p, size_t __pos, const struct mmString* __str)
{
    const char* __p = mmString_GetPointer(__str);
    size_t __sz = mmString_Size(__str);
    return mmString_Insertsn(p, __pos, __p, __sz);
}
MM_EXPORT_DLL int mmString_Inserts(struct mmString* p, size_t __pos, const char* __s)
{
    size_t __sz = mmUtf8Strlen(__s);
    return mmString_Insertsn(p, __pos, __s, __sz);
}
MM_EXPORT_DLL int mmString_Insertsn(struct mmString* p, size_t __pos, const char* __s, size_t __n)
{
    int r = 0;
    size_t __sz, __cap;
    assert((__n == 0 || __s != NULL) && "mmString_Insertsn received nullptr");
    __sz = mmString_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Insertsn out of range error.");
        return EOF;
    }
    __cap = mmString_Capacity(p);
    if (__cap - __sz >= __n)
    {
        if (__n)
        {
            char* __p = mmString_GetPointer(p);
            size_t __n_move = __sz - __pos;
            if (__n_move != 0)
            {
                if (__p + __pos <= __s && __s < __p + __sz)
                {
                    __s += __n;
                }
                mmUtf8_Move(__p + __pos + __n, __p + __pos, __n_move);
            }
            mmUtf8_Move(__p + __pos, __s, __n);
            __sz += __n;
            mmString_SetSize(p, __sz);
            // traits_type::assign(__p[__sz], value_type());
            // __p[__sz] = 0;
            mmUtf8_Assignc(&__p[__sz], 0);
        }
    }
    else
    {
        r = mmString_GrowByAndReplace(p, __cap, __sz + __n - __cap, __sz, __pos, 0, __n, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmString_Insertnc(struct mmString* p, size_t __pos, size_t __n, char __c)
{
    int r = 0;
    size_t __sz = mmString_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Insertnc out of range error.");
        return EOF;
    }
    if (__n)
    {
        size_t __cap = mmString_Capacity(p);
        char* __p;
        if (__cap - __sz >= __n)
        {
            size_t __n_move = __sz - __pos;
            __p = mmString_GetPointer(p);
            if (__n_move != 0)
            {
                mmUtf8_Move(__p + __pos + __n, __p + __pos, __n_move);
            }
        }
        else
        {
            r = mmString_GrowBy(p, __cap, __sz + __n - __cap, __sz, __pos, 0, __n);
            __p = mmString_GetLongPointer(p);
        }
        // traits_type::assign(__p + __pos, __n, __c);
        mmUtf8_Assignn(__p + __pos, __n, __c);
        __sz += __n;
        mmString_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf8_Assignc(&__p[__sz], 0);
    }
    return r;
}

MM_EXPORT_DLL int mmString_Erase(struct mmString* p, size_t __pos, size_t __n)
{
    size_t __sz = mmString_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Erase out of range error.");
        return EOF;
    }
    if (__n)
    {
        char* __p = mmString_GetPointer(p);
        size_t __b1 = __n;
        size_t __b2 = __sz - __pos;
        size_t __bn = __b1 < __b2 ? __b1 : __b2;
        size_t __n_move;
        __n = __bn;
        __n_move = __sz - __pos - __n;
        if (__n_move != 0)
        {
            mmUtf8_Move(__p + __pos, __p + __pos + __n, __n_move);
        }
        __sz -= __n;
        mmString_SetSize(p, __sz);
        // __invalidate_iterators_past(__sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf8_Assignc(&__p[__sz], 0);
    }
    return 0;
}

MM_EXPORT_DLL size_t mmString_Size(const struct mmString* p)
{
    return mmString_IsLong(p) ? mmString_GetLongSize(p) : mmString_GetTinySize(p);
}
MM_EXPORT_DLL int mmString_Resize(struct mmString* p, size_t __n)
{
    return mmString_ResizeChar(p, __n, 0);
}
MM_EXPORT_DLL int mmString_ResizeChar(struct mmString* p, size_t __n, char __c)
{
    int r = 0;
    size_t __sz = mmString_Size(p);
    if (__n > __sz)
    {
        r = mmString_AppendChar(p, __n - __sz, __c);
    }
    else
    {
        mmString_EraseToEnd(p, __n);
    }
    return r;
}
MM_EXPORT_DLL size_t mmString_Capacity(const struct mmString* p)
{
    return (mmString_IsLong(p) ? mmString_GetLongCapacity(p) : (size_t)(mmStringTinyCapacity)) - 1;
}
MM_EXPORT_DLL int mmString_Reserve(struct mmString* p, size_t __res_arg)
{
    size_t __cap, __sz;
    if (__res_arg > mmString_MaxSize())
    {
        assert(0 && "mmString_Reserve max size length error.");
        return EOF;
    }
    __cap = mmString_Capacity(p);
    __sz = mmString_Size(p);
    // max
    __res_arg = __res_arg > __sz ? __res_arg : __sz;
    __res_arg = mmString_Recommend(__res_arg);
    if (__res_arg != __cap)
    {
        char* __new_data; char* __p;
        mmBool_t __was_long, __now_long;
        if (__res_arg == mmStringTinyCapacity - 1)
        {
            __was_long = MM_TRUE;
            __now_long = MM_FALSE;
            __new_data = mmString_GetTinyPointer(p);
            __p = mmString_GetLongPointer(p);
        }
        else
        {
            __new_data = (char*)mmUtf8_Allocate(__res_arg + 1);
            if (__new_data == NULL)
            {
                return EOF;
            }
            __now_long = MM_TRUE;
            __was_long = mmString_IsLong(p);
            __p = mmString_GetPointer(p);
        }
        mmUtf8_Copy(__new_data, __p, mmString_Size(p) + 1);
        if (__was_long)
        {
            // __alloc_traits::deallocate(__alloc(), __p, __cap + 1);
            mmUtf8_Deallocate(__p, __cap + 1);
        }
        if (__now_long)
        {
            mmString_SetLongCapacity(p, __res_arg + 1);
            mmString_SetLongSize(p, __sz);
            mmString_SetLongPointer(p, __new_data);
        }
        else
        {
            mmString_SetTinySize(p, __sz);
        }
        // __invalidate_all_iterators();
    }
    return 0;
}
MM_EXPORT_DLL void mmString_Clear(struct mmString* p)
{
    // __invalidate_all_iterators();
    if (mmString_IsLong(p))
    {
        *mmString_GetLongPointer(p) = 0;
        mmString_SetLongSize(p, 0);
    }
    else
    {
        *mmString_GetTinyPointer(p) = 0;
        mmString_SetTinySize(p, 0);
    }
}
MM_EXPORT_DLL mmBool_t mmString_Empty(const struct mmString* p)
{
    return (0 == mmString_Size(p));
}
MM_EXPORT_DLL int mmString_ShrinkToFit(struct mmString* p)
{
    return mmString_Reserve(p, 0);
}

MM_EXPORT_DLL char mmString_At(const struct mmString* p, size_t __n)
{
    if (__n >= mmString_Size(p))
    {
        assert(0 && "mmString_At out of range error.");
        return 0;
    }
    else
    {
        return *(mmString_GetPointer(p) + __n);
    }
}

MM_EXPORT_DLL int mmString_Append(struct mmString* p, const struct mmString* __str)
{
    const char* __p = mmString_GetPointer(__str);
    size_t __sz = mmString_Size(__str);
    return mmString_Appendsn(p, __p, __sz);
}
MM_EXPORT_DLL int mmString_Appends(struct mmString* p, const char* __s)
{
    size_t __sz = strlen(__s);
    return mmString_Appendsn(p, __s, __sz);
}
MM_EXPORT_DLL int mmString_Appendsn(struct mmString* p, const char* __s, size_t __n)
{
    int r = 0;
    size_t __cap, __sz;
    assert((__n == 0 || __s != NULL) && "mmString_Appendsn received nullptr");
    __cap = mmString_Capacity(p);
    __sz = mmString_Size(p);
    if (__cap - __sz >= __n)
    {
        if (__n)
        {
            char* __p = mmString_GetPointer(p);
            mmUtf8_Copy(__p + __sz, __s, __n);
            __sz += __n;
            mmString_SetSize(p, __sz);
            // traits_type::assign(__p[__sz], value_type());
            // __p[__sz] = 0;
            mmUtf8_Assignc(&__p[__sz], 0);
        }
    }
    else
    {
        r = mmString_GrowByAndReplace(p, __cap, __sz + __n - __cap, __sz, __sz, 0, __n, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmString_Appendnc(struct mmString* p, size_t __n, char __c)
{
    int r = 0;
    if (__n)
    {
        char* __p = NULL;
        size_t __cap = mmString_Capacity(p);
        size_t __sz = mmString_Size(p);
        if (__cap - __sz < __n)
        {
            r = mmString_GrowBy(p, __cap, __sz + __n - __cap, __sz, __sz, 0, 0);
        }
        __p = mmString_GetPointer(p);
        // traits_type::assign(_VSTD::__to_raw_pointer(__p) + __sz, __n, __c);
        mmUtf8_Assignn(__p + __sz, __n, __c);
        __sz += __n;
        mmString_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        mmUtf8_Assignc(__p + __sz, 0);
    }
    return r;
}
MM_EXPORT_DLL int mmString_Appendc(struct mmString* p, char __c)
{
    return mmString_PushBack(p, __c);
}
MM_EXPORT_DLL int mmString_PushBack(struct mmString* p, char __c)
{
    int r = 0;
    mmBool_t __is_short = !mmString_IsLong(p);
    size_t __cap;
    size_t __sz;
    char* __p;
    if (__is_short)
    {
        __cap = mmStringTinyCapacity - 1;
        __sz = mmString_GetTinySize(p);
    }
    else
    {
        __cap = mmString_GetLongCapacity(p) - 1;
        __sz = mmString_GetLongSize(p);
    }
    if (__sz == __cap)
    {
        r = mmString_GrowBy(p, __cap, 1, __sz, __sz, 0, 0);
        __is_short = !mmString_IsLong(p);
    }

    if (__is_short)
    {
        __p = mmString_GetTinyPointer(p) + __sz;
        mmString_SetTinySize(p, __sz + 1);
    }
    else
    {
        __p = mmString_GetLongPointer(p) + __sz;
        mmString_SetLongSize(p, __sz + 1);
    }
    (*__p) = __c;
    (*++__p) = 0;
    return r;
}
MM_EXPORT_DLL void mmString_PopBack(struct mmString* p)
{
    size_t __sz;
    assert(!mmString_Empty(p) && "mmString_PopBack string is already empty");
    if (mmString_IsLong(p))
    {
        __sz = mmString_GetLongSize(p) - 1;
        mmString_SetLongSize(p, __sz);
        *(mmString_GetLongPointer(p) + __sz) = 0;
    }
    else
    {
        __sz = mmString_GetTinySize(p) - 1;
        mmString_SetTinySize(p, __sz);
        *(mmString_GetTinyPointer(p) + __sz) = 0;
    }
    // __invalidate_iterators_past(__sz);
}

MM_EXPORT_DLL int mmString_Replace(struct mmString* p, size_t __pos, size_t __n1, const struct mmString* __str)
{
    const char* __p = mmString_GetPointer(__str);
    size_t __sz = mmString_Size(__str);
    return mmString_Replacesn(p, __pos, __n1, __p, __sz);
}
MM_EXPORT_DLL int mmString_Replaces(struct mmString* p, size_t __pos, size_t __n1, const char* __s)
{
    size_t __sz = mmUtf8Strlen(__s);
    return mmString_Replacesn(p, __pos, __n1, __s, __sz);
}

MM_EXPORT_DLL int mmString_Replacesn(struct mmString* p, size_t __pos, size_t __n1, const char* __s, size_t __n2)
{
    int r = 0;
    size_t __sz, __b1, __b2, __bn, __cap;
    assert((__n2 == 0 || __s != NULL) && "mmString_Replace received nullptr");
    __sz = mmString_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Replacesn out of range error.");
        return EOF;
    }
    __b1 = __n1;
    __b2 = __sz - __pos;
    __bn = __b1 < __b2 ? __b1 : __b2;
    __n1 = __bn;
    __cap = mmString_Capacity(p);
    if (__cap - __sz + __n1 >= __n2)
    {
        char* __p = mmString_GetPointer(p);

        do
        {
            if (__n1 != __n2)
            {
                size_t __n_move = __sz - __pos - __n1;
                if (__n_move != 0)
                {
                    if (__n1 > __n2)
                    {
                        mmUtf8_Move(__p + __pos, __s, __n2);
                        mmUtf8_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
                        break;
                    }
                    if (__p + __pos < __s && __s < __p + __sz)
                    {
                        if (__p + __pos + __n1 <= __s)
                        {
                            __s += __n2 - __n1;
                        }
                        else // __p + __pos < __s < __p + __pos + __n1
                        {
                            mmUtf8_Move(__p + __pos, __s, __n1);
                            __pos += __n1;
                            __s += __n2;
                            __n2 -= __n1;
                            __n1 = 0;
                        }
                    }
                    mmUtf8_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
                }
            }
            mmUtf8_Move(__p + __pos, __s, __n2);
        } while (0);

        // __sz += __n2 - __n1; in this and the below function below can cause unsigned integer overflow,
        // but this is a safe operation, so we disable the check.
        __sz += __n2 - __n1;
        mmString_SetSize(p, __sz);
        // __invalidate_iterators_past(__sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf8_Assignc(&__p[__sz], 0);
    }
    else
    {
        r = mmString_GrowByAndReplace(p, __cap, __sz - __n1 + __n2 - __cap, __sz, __pos, __n1, __n2, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmString_Replacenc(struct mmString* p, size_t __pos, size_t __n1, size_t __n2, char __c)
{
    int r = 0;
    size_t __sz, __b1, __b2, __bn, __cap;
    char* __p;
    __sz = mmString_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Replacenc out of range error.");
        return EOF;
    }
    __b1 = __n1;
    __b2 = __sz - __pos;
    __bn = __b1 < __b2 ? __b1 : __b2;
    __n1 = __bn;
    __cap = mmString_Capacity(p);
    if (__cap - __sz + __n1 >= __n2)
    {
        __p = mmString_GetPointer(p);
        if (__n1 != __n2)
        {
            size_t __n_move = __sz - __pos - __n1;
            if (__n_move != 0)
            {
                mmUtf8_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
            }
        }
    }
    else
    {
        r = mmString_GrowBy(p, __cap, __sz - __n1 + __n2 - __cap, __sz, __pos, __n1, __n2);
        __p = mmString_GetLongPointer(p);
    }
    // traits_type::assign(__p + __pos, __n2, __c);
    mmUtf8_Assignn(__p + __pos, __n2, __c);
    __sz += __n2 - __n1;
    mmString_SetSize(p, __sz);
    // __invalidate_iterators_past(__sz);
    // traits_type::assign(__p[__sz], value_type());
    // __p[__sz] = 0;
    mmUtf8_Assignc(&__p[__sz], 0);
    return r;
}
MM_EXPORT_DLL size_t mmString_ReplaceChar(struct mmString* p, char s, char t)
{
    size_t n = 0;
    size_t __sz = mmString_Size(p);
    if (0 < __sz)
    {
        size_t index = 0;
        char* __p = mmString_GetPointer(p);
        for (index = 0; index < __sz; ++index)
        {
            if (__p[index] == s)
            {
                __p[index] = t;
                n++;
            }
        }
    }
    return n;
}

MM_EXPORT_DLL const char* mmString_CStr(const struct mmString* p)
{
    return mmString_GetPointer(p);
}
MM_EXPORT_DLL const char* mmString_Data(const struct mmString* p)
{
    return mmString_GetPointer(p);
}

MM_EXPORT_DLL size_t mmString_FindLastOf(const struct mmString* p, char c)
{
    size_t index = mmStringNpos;
    size_t __sz = mmString_Size(p);
    if (0 < __sz)
    {
        const char* __p = mmString_GetPointer(p);
        size_t idx = 0;
        for (idx = __sz - 1; mmStringNpos != idx; --idx)
        {
            if (__p[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmString_FindFirstOf(const struct mmString* p, char c)
{
    size_t index = mmStringNpos;
    size_t __sz = mmString_Size(p);
    if (0 < __sz)
    {
        const char* __p = mmString_GetPointer(p);
        size_t idx = 0;
        for (idx = 0; idx < __sz; ++idx)
        {
            if (__p[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmString_FindLastNotOf(const struct mmString* p, char c)
{
    size_t index = mmStringNpos;
    size_t __sz = mmString_Size(p);
    if (0 < __sz)
    {
        const char* __p = mmString_GetPointer(p);
        size_t idx = 0;
        for (idx = __sz - 1; mmStringNpos != idx; --idx)
        {
            if (__p[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmString_FindFirstNotOf(const struct mmString* p, char c)
{
    size_t index = mmStringNpos;
    size_t __sz = mmString_Size(p);
    if (0 < __sz)
    {
        const char* __p = mmString_GetPointer(p);
        size_t idx = 0;
        for (idx = 0; idx < __sz; ++idx)
        {
            if (__p[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL void mmString_Substr(const struct mmString* p, struct mmString* s, size_t o, size_t l)
{
    size_t __sz = mmString_Size(p);
    const char* __p = mmString_GetPointer(p);
    size_t h = __sz - o;
    size_t r = (l > h) ? h : l;
    mmString_Assignsn(s, &(__p[o]), r);
}

// return 0 is equal.
MM_EXPORT_DLL int mmString_Compare(const struct mmString* p, const struct mmString* __str)
{
    size_t __lhs_sz = mmString_Size(p);
    const char* __lhs_pt = mmString_GetPointer(p);
    size_t __rhs_sz = mmString_Size(__str);
    const char* __rhs_pt = mmString_GetPointer(__str);
    size_t __nm = __lhs_sz < __rhs_sz ? __lhs_sz : __rhs_sz;
    int __result = mmUtf8_compare(__lhs_pt, __rhs_pt, __nm);
    if (__result != 0) { return __result; }
    if (__lhs_sz < __rhs_sz) { return -1; }
    if (__lhs_sz > __rhs_sz) { return 1; }
    return 0;
}
// return 0 is equal.
MM_EXPORT_DLL int mmString_CompareCStr(const struct mmString* p, const char* __s)
{
    struct mmString __str;
    mmString_MakeWeaks(&__str, __s);
    return mmString_Compare(p, &__str);
}

// empty string.
static char gStringEmpty[1] = { 0 };
MM_EXPORT_DLL const char* mmStringEmpty = gStringEmpty;

MM_EXPORT_DLL void mmString_MakeWeak(struct mmString* p, const struct mmString* __str)
{
    const char* __s = mmString_CStr(__str);
    size_t __n = mmString_Size(__str);
    mmString_MakeWeaksn(p, __s, __n);
}

MM_EXPORT_DLL void mmString_MakeWeaks(struct mmString* p, const char* __s)
{
    size_t __n = mmUtf8Strlen(__s);
    mmString_MakeWeaksn(p, __s, __n);
}
MM_EXPORT_DLL void mmString_MakeWeaksn(struct mmString* p, const char* __s, size_t __n)
{
    // const reference long capacity is 0.
    p->l.capacity = mmStringLongMask;
    p->l.size = __n;
    p->l.data = (char*)__s;
}
MM_EXPORT_DLL void mmString_SetSizeValue(struct mmString* p, size_t size)
{
    mmString_SetSize(p, size);
}

MM_EXPORT_DLL void mmString_Reset(struct mmString* p)
{
    mmString_Deallocate(p);
    mmMemset(p, 0, sizeof(struct mmString));
}

#ifndef va_copy 
#   ifdef __va_copy 
#       define va_copy(DEST,SRC) __va_copy((DEST),(SRC)) 
#   else 
#       define va_copy(DEST, SRC) memcpy((&DEST), (&SRC), sizeof(va_list)) 
#   endif 
#endif

MM_EXPORT_DLL int mmString_Vsprintf(struct mmString* p, const char* fmt, va_list ap)
{
    va_list args;
    int l;
    char* ps = NULL;
    size_t pm = 0;
    size_t pl = 0;
    do
    {
        // This line does not work with glibc 2.0. See `man snprintf'.
        // if buffer size is 0.l will a real len.
        ps = mmString_GetPointer(p);
        pm = mmString_Capacity(p);
        pl = mmString_Size(p);
        va_copy(args, ap);
        l = mmVsnprintf(ps + pl, pm - pl, fmt, args);
        va_end(args);
        if (0 < l && (size_t)(l + 1) > pm - pl)
        {
            if (0 != mmString_Reserve(p, pl + l + 2))
            {
                mmString_Clear(p);
                mmString_ShrinkToFit(p);
                return EOF;
            }
            // write again.
            ps = mmString_GetPointer(p);
            pm = mmString_Capacity(p);
            pl = mmString_Size(p);
            va_copy(args, ap);
            l = mmVsnprintf(ps + pl, pm - pl, fmt, args);
            va_end(args);
        }
        if (0 > l)
        {
            // if -1 == l,we realloc m * 2 buffer size. and retry.
            if (0 != mmString_Reserve(p, pm * 2))
            {
                mmString_Clear(p);
                mmString_ShrinkToFit(p);
                return EOF;
            }
        }
    } while (0 > l);
    mmString_SetSize(p, pl + l);
    return l;
}

MM_EXPORT_DLL int mmString_Sprintf(struct mmString* p, const char* fmt, ...)
{
    va_list ap;
    int l;
    va_start(ap, fmt);
    l = mmString_Vsprintf(p, fmt, ap);
    va_end(ap);
    return l;
}
