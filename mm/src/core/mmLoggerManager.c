/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLoggerManager.h"
#include "core/mmOSContext.h"
#include "core/mmTimeCache.h"
#include "core/mmTimewait.h"
#include "core/mmSpinlock.h"

static void __static_LoggerManager_LoggerCallback(struct mmLogger* p, mmUInt32_t lvl, const char* message);
static void* __static_LoggerManager_Thread(void* arg);

MM_EXPORT_DLL void mmLoggerManagerCallback_Init(struct mmLoggerManagerCallback* p)
{
    p->ConsolePrintf = &mmOSConsoleLogger;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmLoggerManagerCallback_Destroy(struct mmLoggerManagerCallback* p)
{
    p->ConsolePrintf = &mmOSConsoleLogger;
    p->obj = NULL;
}

static struct mmLoggerManager gLoggerManager;
MM_EXPORT_DLL struct mmLoggerManager* mmLoggerManager_Instance(void)
{
    return &gLoggerManager;
}

MM_EXPORT_DLL void mmLoggerManager_Init(struct mmLoggerManager* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    p->logger_impl = gLogger;
    p->stream = NULL;
    mmString_Init(&p->logger_section);
    mmString_Init(&p->logger_path);
    mmString_Init(&p->file_name);
    mmString_Init(&p->file_fopen_mode);
    mmString_Init(&p->current_file_prefix);
    mmString_Init(&p->current_file_name);
    mmString_Init(&p->logger_file_name);
    mmLoggerManagerCallback_Init(&p->callback);

    mmSpinlock_Init(&p->locker, NULL);
    mmSpinlock_Init(&p->locker_stream, NULL);

    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);

    p->rolling_index = 100;
    p->current_index = 0;
    p->file_size = 300 * 1024 * 1024;
    //
    p->logger_level = MM_LOG_VERBOSE;
    p->check_file_size_time = 5000;
    //
    p->is_rolling = 1;
    p->is_console = 0;
    p->is_immediately = 0;
    p->state = MM_TS_CLOSED;
    //
    mmString_Assigns(&p->logger_section, "mm");
    mmString_Assigns(&p->logger_path, "log");
    mmString_Assigns(&p->file_name, "mm");
    mmString_Assigns(&p->file_fopen_mode, MM_LOGGER_MANAGER_FILE_FOPEN_MODE_DEFAULT);
    //
    mmLogger_SetCallback(p->logger_impl, &__static_LoggerManager_LoggerCallback, p);
}
MM_EXPORT_DLL void mmLoggerManager_Destroy(struct mmLoggerManager* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmLogger_SetCallback(p->logger_impl, &mmLogger_LoggerMessagePrintf, p);
    //
    mmLoggerManager_Flush(p);
    mmLoggerManager_Close(p);
    //
    p->logger_impl = gLogger;
    p->stream = NULL;
    mmString_Destroy(&p->logger_section);
    mmString_Destroy(&p->logger_path);
    mmString_Destroy(&p->file_name);
    mmString_Destroy(&p->file_fopen_mode);
    mmString_Destroy(&p->current_file_prefix);
    mmString_Destroy(&p->current_file_name);
    mmString_Destroy(&p->logger_file_name);
    mmLoggerManagerCallback_Destroy(&p->callback);

    mmSpinlock_Destroy(&p->locker);
    mmSpinlock_Destroy(&p->locker_stream);

    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    //
    p->rolling_index = 100;
    p->current_index = 0;
    p->file_size = 300 * 1024 * 1024;
    //
    p->logger_level = MM_LOG_VERBOSE;
    p->check_file_size_time = 5000;
    //
    p->is_rolling = 1;
    p->is_console = 0;
    p->is_immediately = 0;
    p->state = MM_TS_CLOSED;
}

MM_EXPORT_DLL void mmLoggerManager_Lock(struct mmLoggerManager* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmLoggerManager_Unlock(struct mmLoggerManager* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_DLL void mmLoggerManager_SetFileName(struct mmLoggerManager* p, const char* file_name)
{
    mmString_Assigns(&p->file_name, file_name);
}
MM_EXPORT_DLL void mmLoggerManager_SetFileFopenMode(struct mmLoggerManager* p, const char* file_fopen_mode)
{
    mmString_Assigns(&p->file_fopen_mode, file_fopen_mode);
}
MM_EXPORT_DLL void mmLoggerManager_SetLoggerPath(struct mmLoggerManager* p, const char* logger_path)
{
    mmString_Assigns(&p->logger_path, logger_path);
    // directory none suffix.
    if (!mmString_Empty(&p->logger_path))
    {
        const char* ps = mmString_Data(&p->logger_path);
        size_t pl = mmString_Size(&p->logger_path);
        char c = ps[pl - 1];
        if ('/' == c || '\\' == c)
        {
            mmString_PopBack(&p->logger_path);
        }
    }
}
MM_EXPORT_DLL void mmLoggerManager_SetFileSize(struct mmLoggerManager* p, size_t file_size)
{
    p->file_size = file_size;
}
MM_EXPORT_DLL void mmLoggerManager_SetLoggerLevel(struct mmLoggerManager* p, mmUInt32_t logger_level)
{
    p->logger_level = logger_level;
}
MM_EXPORT_DLL void mmLoggerManager_SetCheckFileSizeTime(struct mmLoggerManager* p, mmMSec_t check_file_size_time)
{
    p->check_file_size_time = check_file_size_time;
}
MM_EXPORT_DLL void mmLoggerManager_SetRollingIndex(struct mmLoggerManager* p, size_t rolling_index)
{
    p->rolling_index = rolling_index;
}
MM_EXPORT_DLL void mmLoggerManager_SetIsRolling(struct mmLoggerManager* p, mmSInt8_t is_rolling)
{
    p->is_rolling = is_rolling;
}
MM_EXPORT_DLL void mmLoggerManager_SetIsConsole(struct mmLoggerManager* p, mmSInt8_t is_console)
{
    p->is_console = is_console;
}
MM_EXPORT_DLL void mmLoggerManager_SetIsImmediately(struct mmLoggerManager* p, mmSInt8_t is_immediately)
{
    p->is_immediately = is_immediately;
}
// assign callback.
MM_EXPORT_DLL void mmLoggerManager_SetCallback(struct mmLoggerManager* p, const struct mmLoggerManagerCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = *callback;
}
// assign logger instance.default is mmLogger_Instance.
MM_EXPORT_DLL void mmLoggerManager_SetLogger(struct mmLoggerManager* p, struct mmLogger* logger)
{
    assert(NULL != logger && "logger is a null.");
    if (NULL != p->logger_impl)
    {
        mmLogger_SetCallback(p->logger_impl, &mmLogger_LoggerMessagePrintf, p);
    }
    p->logger_impl = logger;
    if (NULL != p->logger_impl)
    {
        mmLogger_SetCallback(p->logger_impl, &__static_LoggerManager_LoggerCallback, p);
    }
}

MM_EXPORT_DLL void mmLoggerManager_FOpen(struct mmLoggerManager* p)
{
    mmSpinlock_Lock(&p->locker_stream);
    p->current_index = 0;
    mmLoggerManager_GenerateCurrentFilePrefix(p);
    mmLoggerManager_GenerateCurrentFileName(p);
    mmLoggerManager_GenerateLoggerFileName(p);
    p->stream = fopen(mmString_CStr(&p->logger_file_name), mmString_CStr(&p->file_fopen_mode));
    mmSpinlock_Unlock(&p->locker_stream);
}
MM_EXPORT_DLL void mmLoggerManager_Close(struct mmLoggerManager* p)
{
    mmSpinlock_Lock(&p->locker_stream);
    if (NULL != p->stream)
    {
        fclose(p->stream);
        p->stream = NULL;
    }
    mmSpinlock_Unlock(&p->locker_stream);
}
MM_EXPORT_DLL void mmLoggerManager_Flush(struct mmLoggerManager* p)
{
    mmSpinlock_Lock(&p->locker_stream);
    if (NULL != p->stream)
    {
        fflush(p->stream);
    }
    mmSpinlock_Unlock(&p->locker_stream);
}
MM_EXPORT_DLL void mmLoggerManager_Start(struct mmLoggerManager* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->file_size_thread, NULL, &__static_LoggerManager_Thread, p);
}
MM_EXPORT_DLL void mmLoggerManager_Interrupt(struct mmLoggerManager* p)
{
    p->state = MM_TS_CLOSED;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmLoggerManager_Shutdown(struct mmLoggerManager* p)
{
    p->state = MM_TS_FINISH;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmLoggerManager_Join(struct mmLoggerManager* p)
{
    pthread_join(p->file_size_thread, NULL);
}
MM_EXPORT_DLL void mmLoggerManager_CheckFileSize(struct mmLoggerManager* p)
{
    mmSpinlock_Lock(&p->locker_stream);
    if (NULL != p->stream)
    {
        if ((size_t)ftell(p->stream) >= p->file_size)
        {
            p->current_index++;
            p->current_index = p->current_index > p->rolling_index ? 0 : p->current_index;
            //
            fflush(p->stream);
            fclose(p->stream);
            p->stream = NULL;

            mmLoggerManager_GenerateCurrentFileName(p);

            rename(mmString_CStr(&p->logger_file_name), mmString_CStr(&p->current_file_name));

            p->stream = fopen(mmString_CStr(&p->logger_file_name), mmString_CStr(&p->file_fopen_mode));
        }
    }
    mmSpinlock_Unlock(&p->locker_stream);
}
MM_EXPORT_DLL void mmLoggerManager_GenerateCurrentFileName(struct mmLoggerManager* p)
{
    char file_name[MM_MAX_LOGGER_FILE_NAME_LENGTH] = { 0 };
    char fmt[64] = { 0 };
    const char* prefix = mmString_CStr(&p->current_file_prefix);
    unsigned int w = 0;
    size_t n = p->rolling_index;
    while (n != 0) { n = n / 10; w++; }
    mmSprintf(fmt, "%%s_%%0%dd.log", w);
    // log/mm_20160217_113233_105_000.log
    mmSnprintf(file_name, MM_MAX_LOGGER_FILE_NAME_LENGTH - 1, fmt, prefix, p->current_index);
    mmString_Assigns(&p->current_file_name, file_name);
}
MM_EXPORT_DLL void mmLoggerManager_GenerateCurrentFilePrefix(struct mmLoggerManager* p)
{
    char file_name[MM_MAX_LOGGER_FILE_NAME_LENGTH] = { 0 };
    const char* path = mmString_CStr(&p->logger_path);
    const char* name = mmString_CStr(&p->file_name);
    // log/mm_20160217_113233_105
    // [19920126_091314_520]
    char time_path[64] = { 0 };
    struct tm tm;
    struct timeval tv;
    struct mmTimeInfo tp;
    mmGettimeofday(&tv, NULL);
    tp.sec = tv.tv_sec;
    tp.msec = tv.tv_usec / 1000;
    mmTime_Tm(&tp, &tm);
    mmSprintf(time_path, "%4d%02d%02d_%02d%02d%02d_%03d",
        tm.mmTm_year, tm.mmTm_mon,
        tm.mmTm_mday, tm.mmTm_hour,
        tm.mmTm_min, tm.mmTm_sec, tp.msec);
    mmSnprintf(file_name, MM_MAX_LOGGER_FILE_NAME_LENGTH - 1, "%s/%s_%s", path, name, time_path);
    mmString_Assigns(&p->current_file_prefix, file_name);
}
MM_EXPORT_DLL void mmLoggerManager_GenerateLoggerFileName(struct mmLoggerManager* p)
{
    char file_name[MM_MAX_LOGGER_FILE_NAME_LENGTH] = { 0 };
    const char* path = mmString_CStr(&p->logger_path);
    const char* name = mmString_CStr(&p->file_name);
    // log/mm.log
    mmSnprintf(file_name, MM_MAX_LOGGER_FILE_NAME_LENGTH - 1, "%s/%s.log", path, name);
    mmString_Assigns(&p->logger_file_name, file_name);
}
MM_EXPORT_DLL void mmLoggerManager_OnFinishLaunching(struct mmLoggerManager* p)
{
    mmLoggerManager_FOpen(p);
}
MM_EXPORT_DLL void mmLoggerManager_OnBeforeTerminate(struct mmLoggerManager* p)
{
    mmLoggerManager_Flush(p);
    mmLoggerManager_Close(p);
}
MM_EXPORT_DLL void mmLoggerManager_LoggerSection(struct mmLogger* p, const char* section, mmUInt32_t lvl, const char* message)
{
    struct timeval tv;
    struct mmTimeInfo tp;
    char timeStamp[MM_TIME_STRING_LENGTH] = { 0 };
    struct mmLoggerManager* lm = (struct mmLoggerManager*)(p->obj);
    if (NULL == lm || lvl > lm->logger_level)
    {
        // logger filter.
        return;
    }
    //
    mmGettimeofday(&tv, NULL);
    tp.sec = tv.tv_sec;
    tp.msec = tv.tv_usec / 1000;
    mmTime_String(&tp, timeStamp);
    // logger to file.
    mmSpinlock_Lock(&lm->locker_stream);
    if (lm->stream)
    {
        char log_head_suffix[8] = { 0 };
        // [1992/01/26 09:13:14-520 8 V ]
        const struct mmLevelMark* mark = mmLoggerLevelMark((enum mmLoggerLevel_t)lvl);
        const char* m = mmString_CStr(&mark->m);
        // [1992/01/26 09:13:14-520]
        fputs(timeStamp, lm->stream);
        // [ 8 V ]
        mmSprintf(log_head_suffix, " %d %s ", lvl, m);
        fputs(log_head_suffix, lm->stream);
        fputs(section, lm->stream);
        fputc(' ', lm->stream);
        fputs(message, lm->stream);
        fputc('\n', lm->stream);
    }
    mmSpinlock_Unlock(&lm->locker_stream);
    if (1 == lm->is_console)
    {
        // logger to console.
        (*(lm->callback.ConsolePrintf))(section, timeStamp, lvl, message);
    }
    if (1 == lm->is_immediately)
    {
        mmLoggerManager_Flush(lm);
    }
}
static void __static_LoggerManager_LoggerCallback(struct mmLogger* p, mmUInt32_t lvl, const char* message)
{
    struct mmLoggerManager* lm = (struct mmLoggerManager*)(p->obj);
    mmLoggerManager_LoggerSection(p, mmString_CStr(&lm->logger_section), lvl, message);
}
static void* __static_LoggerManager_Thread(void* arg)
{
    struct timeval  ntime;
    struct timespec otime;
    mmMSec_t _nearby_time = 0;
    //
    struct mmLoggerManager* p = (struct mmLoggerManager*)(arg);
    //
    while (MM_TS_MOTION == p->state)
    {
        mmLoggerManager_CheckFileSize(p);
        mmLoggerManager_Flush(p);
        //
        _nearby_time = p->check_file_size_time;
        // timewait nearby.
        mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
    }
    return NULL;
}
