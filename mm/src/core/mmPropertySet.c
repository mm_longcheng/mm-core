/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPropertySet.h"
#include "core/mmLogger.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmProperty_SetValueMember(
    const struct mmProperty*                       p,
    void*                                          obj,
    const void*                                    pValue)
{
    void* pMemberPointer = (void*)((char*)obj + p->hBaseOffset + p->hMemberOffset);
    assert(obj && "obj is null.");
    assert(0 != p->hMemberLength && "API only Property can not set by Member.");
    mmMemcpy(pMemberPointer , pValue, p->hMemberLength);
}

MM_EXPORT_DLL
void
mmProperty_GetValueMember(
    const struct mmProperty*                       p,
    const void*                                    obj,
    void*                                          pValue)
{
    void* pMemberPointer = (void*)((char*)obj + p->hBaseOffset + p->hMemberOffset);
    assert(obj && "obj is null.");
    assert(0 != p->hMemberLength && "API only Property can not get by Member.");
    mmMemcpy(pValue, pMemberPointer, p->hMemberLength);
}

MM_EXPORT_DLL
void
mmProperty_SetValueDirect(
    const struct mmProperty*                       p,
    void*                                          obj,
    const void*                                    pValue)
{
    if (NULL != p->pSetterFunc)
    {
        typedef void(*SetterFuncType)(void* obj, const void* value);
        SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
        (*(hFunc))((char*)obj + p->hBaseOffset, pValue);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmProperty_GetValueDirect(
    const struct mmProperty*                       p,
    const void*                                    obj,
    void*                                          pValue)
{
    if (NULL != p->pGetterFunc)
    {
        typedef void(*GetterFuncType)(const void* obj, void* value);
        GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
        (*(hFunc))((char*)obj + p->hBaseOffset, pValue);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmProperty_SetValueString(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         pValue)
{
    if (NULL != p->hHelper.SetValue)
    {
        (*(p->hHelper.SetValue))(p, obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmProperty_GetValueString(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               pValue)
{
    if (NULL != p->hHelper.GetValue)
    {
        (*(p->hHelper.GetValue))(p, obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmProperty_SetPointerDirect(
    const struct mmProperty*                       p,
    void*                                          obj,
    const void*                                    ptr)
{
    mmProperty_SetValueDirect(p, obj, &ptr);
}

MM_EXPORT_DLL
void*
mmProperty_GetPointerDirect(
    const struct mmProperty*                       p,
    const void*                                    obj)
{
    void* ptr = NULL;
    mmProperty_GetValueDirect(p, obj, &ptr);
    return ptr;
}

MM_EXPORT_DLL
void
mmPropertySet_Init(
    struct mmPropertySet*                          p)
{
    mmRbtreeWeakStringVpt_Init(&p->rbtree);
    p->obj = NULL;
}

MM_EXPORT_DLL
void
mmPropertySet_Destroy(
    struct mmPropertySet*                          p)
{
    p->obj = NULL;
    mmRbtreeWeakStringVpt_Destroy(&p->rbtree);
}

MM_EXPORT_DLL
void
mmPropertySet_SetObject(
    struct mmPropertySet*                          p,
    void*                                          obj)
{
    p->obj = obj;
}

MM_EXPORT_DLL
void
mmPropertySet_AddProperty(
    struct mmPropertySet*                          p,
    const struct mmProperty*                       pProperty)
{
    mmRbtreeWeakStringVpt_Set(&p->rbtree, &pProperty->hName, (void*)pProperty);
}

MM_EXPORT_DLL
void
mmPropertySet_RmvProperty(
    struct mmPropertySet*                          p,
    const struct mmProperty*                       pProperty)
{
    mmRbtreeWeakStringVpt_Rmv(&p->rbtree, &pProperty->hName);
}

MM_EXPORT_DLL
void
mmPropertySet_RmvPropertyByName(
    struct mmPropertySet*                          p,
    const char*                                    pName)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, pName);
    mmRbtreeWeakStringVpt_Rmv(&p->rbtree, &hWeakKey);
}

MM_EXPORT_DLL
void
mmPropertySet_ClearProperty(
    struct mmPropertySet*                          p)
{
    mmRbtreeWeakStringVpt_Clear(&p->rbtree);
}

MM_EXPORT_DLL
const struct mmProperty*
mmPropertySet_GetProperty(
    const struct mmPropertySet*                    p,
    const char*                                    pName)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, pName);
    return (const struct mmProperty*)mmRbtreeWeakStringVpt_Get(&p->rbtree, &hWeakKey);
}

MM_EXPORT_DLL
int
mmPropertySet_GetIsPropertyExists(
    const struct mmPropertySet*                    p,
    const char*                                    pName)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, pName);
    return NULL != mmRbtreeWeakStringVpt_GetIterator(&p->rbtree, &hWeakKey);
}

MM_EXPORT_DLL
void
mmPropertySet_SetValueMember(
    struct mmPropertySet*                          p,
    const char*                                    pName,
    const void*                                    pValue)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_SetValueMember(pProperty, p->obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_GetValueMember(
    const struct mmPropertySet*                    p,
    const char*                                    pName,
    void*                                          pValue)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_GetValueMember(pProperty, p->obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_SetValueDirect(
    struct mmPropertySet*                          p,
    const char*                                    pName,
    const void*                                    pValue)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_SetValueDirect(pProperty, p->obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_GetValueDirect(
    const struct mmPropertySet*                    p,
    const char*                                    pName,
    void*                                          pValue)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_GetValueDirect(pProperty, p->obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_SetValueString(
    struct mmPropertySet*                          p,
    const char*                                    pName,
    const struct mmString*                         pValue)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_SetValueString(pProperty, p->obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_GetValueString(
    const struct mmPropertySet*                    p,
    const char*                                    pName,
    struct mmString*                               pValue)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_GetValueString(pProperty, p->obj, pValue);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_SetPointerDirect(
    const struct mmPropertySet*                    p,
    const char*                                    pName,
    const void*                                    ptr)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
    }
    else
    {
        mmProperty_SetPointerDirect(pProperty, p->obj, ptr);
    }
}

MM_EXPORT_DLL
void*
mmPropertySet_GetPointerDirect(
    const struct mmPropertySet*                    p,
    const char*                                    pName)
{
    const struct mmProperty* pProperty = mmPropertySet_GetProperty(p, pName);
    if (NULL == pProperty)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d There is no Property pName: %s", __FUNCTION__, __LINE__, pName);
        return NULL;
    }
    else
    {
        return mmProperty_GetPointerDirect(pProperty, p->obj);
    }
}

MM_EXPORT_DLL
void
mmPropertySet_SetValueCStr(
    struct mmPropertySet*                          p,
    const char*                                    pName,
    const char*                                    pValue)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, pValue);
    mmPropertySet_SetValueString(p, pName, &hWeakKey);
}
