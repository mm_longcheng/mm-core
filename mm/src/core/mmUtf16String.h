/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUtf16String_h__
#define __mmUtf16String_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// clang generally mode simplify string.
// https://github.com/llvm-mirror/libcxx/blob/master/include/string
// https://joellaity.com/2020/01/31/string.html

typedef mmUInt16_t mmUtf16_t;

MM_EXPORT_DLL size_t mmUtf16Strlen(const mmUInt16_t* s);

struct mmUtf16StringLong
{
    // capacity
    size_t capacity;
    // size
    size_t size;
    // data pointer
    mmUtf16_t* data;
};

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// bigger endian
#define mmUtf16StringTinyMaskMacro 0x80
#define mmUtf16StringLongMaskMacro ~((size_t)(~((size_t)(0))) >> 1)
#else
// little endian
#define mmUtf16StringTinyMaskMacro 0x01
#define mmUtf16StringLongMaskMacro 0x1ul
#endif

MM_EXPORT_DLL extern const size_t mmUtf16StringTinyMask;
MM_EXPORT_DLL extern const size_t mmUtf16StringLongMask;
MM_EXPORT_DLL extern const size_t mmUtf16StringNpos;

enum
{
    mmUtf16StringLongCapacity = (sizeof(struct mmUtf16StringLong) - 1) / sizeof(mmUtf16_t),
    mmUtf16StringTinyCapacity = mmUtf16StringLongCapacity > 2 ? mmUtf16StringLongCapacity : 2,
};

struct mmUtf16StringTiny
{
    union
    {
        // size
        unsigned char size;
        // lx
        mmUtf16_t lx;
    };
    // data buffer
    mmUtf16_t data[mmUtf16StringTinyCapacity];
};

union mmUtf16StringUlxx { struct mmUtf16StringLong __lxx; struct mmUtf16StringTiny __txx; };

enum { mmUtf16StringNWords = sizeof(union mmUtf16StringUlxx) / sizeof(size_t) };

struct mmUtf16StringRaws
{
    // raw words buffer
    size_t words[mmUtf16StringNWords];
};

struct mmUtf16StringRep
{
    union
    {
        struct mmUtf16StringLong l;
        struct mmUtf16StringTiny s;
        struct mmUtf16StringRaws r;
    };
};

struct mmUtf16String
{
    union
    {
        // long
        struct mmUtf16StringLong l;
        // tiny
        struct mmUtf16StringTiny s;
        // raws
        struct mmUtf16StringRaws r;
    };
};

MM_EXPORT_DLL void mmUtf16String_Init(struct mmUtf16String* p);
MM_EXPORT_DLL void mmUtf16String_Destroy(struct mmUtf16String* p);

MM_EXPORT_DLL int mmUtf16String_Assign(struct mmUtf16String* p, const struct mmUtf16String* __str);
MM_EXPORT_DLL int mmUtf16String_Assigns(struct mmUtf16String* p, const mmUtf16_t* __s);
MM_EXPORT_DLL int mmUtf16String_Assignsn(struct mmUtf16String* p, const mmUtf16_t* __s, size_t __n);
MM_EXPORT_DLL int mmUtf16String_Assignnc(struct mmUtf16String* p, size_t __n, char __c);

MM_EXPORT_DLL int mmUtf16String_Insert(struct mmUtf16String* p, size_t __pos, const struct mmUtf16String* __str);
MM_EXPORT_DLL int mmUtf16String_Inserts(struct mmUtf16String* p, size_t __pos, const mmUtf16_t* __s);
MM_EXPORT_DLL int mmUtf16String_Insertsn(struct mmUtf16String* p, size_t __pos, const mmUtf16_t* __s, size_t __n);
MM_EXPORT_DLL int mmUtf16String_Insertnc(struct mmUtf16String* p, size_t __pos, size_t __n, mmUtf16_t __c);

MM_EXPORT_DLL int mmUtf16String_Erase(struct mmUtf16String* p, size_t __pos, size_t __n);

MM_EXPORT_DLL size_t mmUtf16String_Size(const struct mmUtf16String* p);
MM_EXPORT_DLL int mmUtf16String_Resize(struct mmUtf16String* p, size_t __n);
MM_EXPORT_DLL int mmUtf16String_ResizeChar(struct mmUtf16String* p, size_t __n, mmUtf16_t __c);
MM_EXPORT_DLL size_t mmUtf16String_Capacity(const struct mmUtf16String* p);
MM_EXPORT_DLL int mmUtf16String_Reserve(struct mmUtf16String* p, size_t __res_arg);
MM_EXPORT_DLL void mmUtf16String_Clear(struct mmUtf16String* p);
MM_EXPORT_DLL mmBool_t mmUtf16String_Empty(const struct mmUtf16String* p);
MM_EXPORT_DLL int mmUtf16String_ShrinkToFit(struct mmUtf16String* p);

MM_EXPORT_DLL mmUtf16_t mmUtf16String_At(const struct mmUtf16String* p, size_t __n);

MM_EXPORT_DLL int mmUtf16String_Append(struct mmUtf16String* p, const struct mmUtf16String* __str);
MM_EXPORT_DLL int mmUtf16String_Appends(struct mmUtf16String* p, const mmUtf16_t* __s);
MM_EXPORT_DLL int mmUtf16String_Appendsn(struct mmUtf16String* p, const mmUtf16_t* __s, size_t __n);
MM_EXPORT_DLL int mmUtf16String_Appendnc(struct mmUtf16String* p, size_t __n, mmUtf16_t __c);
MM_EXPORT_DLL int mmUtf16String_Appendc(struct mmUtf16String* p, mmUtf16_t __c);
MM_EXPORT_DLL int mmUtf16String_PushBack(struct mmUtf16String* p, mmUtf16_t __c);
MM_EXPORT_DLL void mmUtf16String_PopBack(struct mmUtf16String* p);

MM_EXPORT_DLL int mmUtf16String_Replace(struct mmUtf16String* p, size_t __pos, size_t __n1, const struct mmUtf16String* __str);
MM_EXPORT_DLL int mmUtf16String_Replaces(struct mmUtf16String* p, size_t __pos, size_t __n1, const mmUtf16_t* __s);
MM_EXPORT_DLL int mmUtf16String_Replacesn(struct mmUtf16String* p, size_t __pos, size_t __n1, const mmUtf16_t* __s, size_t __n2);
MM_EXPORT_DLL int mmUtf16String_Replacenc(struct mmUtf16String* p, size_t __pos, size_t __n1, size_t __n2, mmUtf16_t __c);
// s ==> t
MM_EXPORT_DLL size_t mmUtf16String_ReplaceChar(struct mmUtf16String* p, mmUtf16_t s, mmUtf16_t t);

MM_EXPORT_DLL const mmUtf16_t* mmUtf16String_CStr(const struct mmUtf16String* p);
MM_EXPORT_DLL const mmUtf16_t* mmUtf16String_Data(const struct mmUtf16String* p);

MM_EXPORT_DLL size_t mmUtf16String_FindLastOf(const struct mmUtf16String* p, mmUtf16_t c);
MM_EXPORT_DLL size_t mmUtf16String_FindFirstOf(const struct mmUtf16String* p, mmUtf16_t c);
MM_EXPORT_DLL size_t mmUtf16String_FindLastNotOf(const struct mmUtf16String* p, mmUtf16_t c);
MM_EXPORT_DLL size_t mmUtf16String_FindFirstNotOf(const struct mmUtf16String* p, mmUtf16_t c);

MM_EXPORT_DLL void mmUtf16String_Substr(const struct mmUtf16String* p, struct mmUtf16String* s, size_t o, size_t l);

// return 0 is equal.
MM_EXPORT_DLL int mmUtf16String_Compare(const struct mmUtf16String* p, const struct mmUtf16String* __str);
// return 0 is equal.
MM_EXPORT_DLL int mmUtf16String_CompareCStr(const struct mmUtf16String* p, const mmUtf16_t* __s);

/* Expansion module */
// const char* empty string
MM_EXPORT_DLL extern const mmUtf16_t* mmUtf16StringEmpty;
// make a weak string.
MM_EXPORT_DLL void mmUtf16String_MakeWeak(struct mmUtf16String* p, const struct mmUtf16String* __str);
MM_EXPORT_DLL void mmUtf16String_MakeWeaks(struct mmUtf16String* p, const mmUtf16_t* __s);
MM_EXPORT_DLL void mmUtf16String_MakeWeaksn(struct mmUtf16String* p, const mmUtf16_t* __s, size_t __n);
// set size value.
MM_EXPORT_DLL void mmUtf16String_SetSizeValue(struct mmUtf16String* p, size_t size);
// reset weak or strong string. Mix reference must reset before reassign.
MM_EXPORT_DLL void mmUtf16String_Reset(struct mmUtf16String* p);

// string Null.
#define mmUtf16String_Null      { { { 0, 0, NULL, }, }, }
// make a const weak string.
#define mmUtf16String_Make(str) { { { mmUtf16StringLongMaskMacro, (sizeof(str) / sizeof(mmUtf16_t) - 1), (mmUtf16_t*)str, }, }, }

#include "core/mmSuffix.h"

#endif//__mmUtf16String_h__
