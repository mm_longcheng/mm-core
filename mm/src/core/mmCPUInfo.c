/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCPUInfo.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmTypes.h"
#include "core/mmLogger.h"

#if MM_COMPILER == MM_COMPILER_MSVC
#   include <excpt.h>      // For SEH values
#   if _MSC_VER >= 1400
#       include <intrin.h>
#   endif
#elif (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && MM_PLATFORM != MM_PLATFORM_NACL
#   include <signal.h>
#   include <setjmp.h>

#   if MM_PLATFORM == MM_PLATFORM_ANDROID
#       include <cpu-features.h>
#   elif MM_CPU == MM_CPU_ARM 
#       include <sys/sysctl.h>
#       if __MACH__
#           include <mach/machine.h>
#           ifndef CPU_SUBTYPE_ARM64_V8
#               define CPU_SUBTYPE_ARM64_V8 ((cpu_subtype_t) 1)
#           endif
#           ifndef CPU_SUBTYPE_ARM_V8
#               define CPU_SUBTYPE_ARM_V8 ((cpu_subtype_t) 13)
#           endif
#       endif
#   endif
#endif

//---------------------------------------------------------------------
// Compiler-dependent routines
//---------------------------------------------------------------------
#if MM_COMPILER == MM_COMPILER_MSVC
#pragma warning(push)
#pragma warning(disable: 4035)  // no return value
#endif

#if MM_CPU == MM_CPU_X86

//---------------------------------------------------------------------
// Detect whether CPU supports CPUID instruction, returns non-zero if supported.
MM_EXPORT_DLL mmBool_t mmCPU_SupportCPUId(void)
{
#if MM_COMPILER == MM_COMPILER_MSVC
    // Visual Studio 2005 & 64-bit compilers always supports __cpuid intrinsic
    // note that even though this is a build rather than runtime setting, all
    // 64-bit CPUs support this so since binary is 64-bit only we're ok
#if _MSC_VER >= 1400 && defined(_M_X64)
    return MM_TRUE;
#else
    // If we can modify flag register bit 21, the cpu is supports CPUID instruction
    __asm
    {
        // Read EFLAG
        pushfd
        pop     eax
        mov     ecx, eax

        // Modify bit 21
        xor     eax, 0x200000
        push    eax
        popfd

        // Read back EFLAG
        pushfd
        pop     eax

        // Restore EFLAG
        push    ecx
        popfd

        // Check bit 21 modifiable
        xor     eax, ecx
        neg     eax
        sbb     eax, eax

        // Return values in eax, no return statement requirement here for VC.
    }
#endif
#elif (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && \
       MM_PLATFORM != MM_PLATFORM_NACL && MM_PLATFORM != MM_PLATFORM_EMSCRIPTEN
#if MM_ARCH_TYPE == MM_ARCHITECTURE_64
    
#if defined(__arm__) || defined(__arm64__)
    // arm64 is architecture 64 but not cpuid.
    return MM_FALSE;
#else
    return MM_TRUE;
#endif
    
#else
    unsigned oldFlags, newFlags;
    __asm__
    (
        "pushfl         \n\t"
        "pop    %0      \n\t"
        "mov    %0, %1  \n\t"
        "xor    %2, %0  \n\t"
        "push   %0      \n\t"
        "popfl          \n\t"
        "pushfl         \n\t"
        "pop    %0      \n\t"
        "push   %1      \n\t"
        "popfl          \n\t"
        : "=r" (oldFlags), "=r" (newFlags)
        : "n" (0x200000)
    );
    return oldFlags != newFlags;
#endif // 64
#else
    // TODO: Supports other compiler
    return MM_FALSE;
#endif
}
//---------------------------------------------------------------------
// Performs CPUID instruction with 'query', fill the results, and return value of eax.
MM_EXPORT_DLL mmUInt_t mmPerformCPUId(struct mmCPUIdInfo* p, mmUInt_t query)
{
#if MM_COMPILER == MM_COMPILER_MSVC
#if _MSC_VER >= 1400 
    int CPUInfo[4];
    __cpuid(CPUInfo, query);
    p->_eax = CPUInfo[0];
    p->_ebx = CPUInfo[1];
    p->_ecx = CPUInfo[2];
    p->_edx = CPUInfo[3];
    return p->_eax;
#else
    __asm
    {
        mov     edi, p
        mov     eax, query
        cpuid
        mov[edi]->_eax, eax
        mov[edi]->_ebx, ebx
        mov[edi]->_edx, edx
        mov[edi]->_ecx, ecx
        // Return values in eax, no return statement requirement here for VC.
    }
#endif
#elif (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && \
       MM_PLATFORM != MM_PLATFORM_NACL && MM_PLATFORM != MM_PLATFORM_EMSCRIPTEN
#if MM_ARCH_TYPE == MM_ARCHITECTURE_64
    
#if defined(__arm__) || defined(__arm64__)
    // arm64 is architecture 64 but not cpuid.
    return 0;
#else
    __asm__
    (
        "cpuid": "=a" (p->_eax), "=b" (p->_ebx), "=c" (p->_ecx), "=d" (p->_edx) : "a" (query)
    );
#endif

#else
    __asm__
    (
        "pushl  %%ebx           \n\t"
        "cpuid                  \n\t"
        "movl   %%ebx, %%edi    \n\t"
        "popl   %%ebx           \n\t"
        : "=a" (p->_eax), "=D" (p->_ebx), "=c" (p->_ecx), "=d" (p->_edx)
        : "a" (query)
    );
#endif // MM_ARCHITECTURE_64
    return p->_eax;

#else
    // TODO: Supports other compiler
    return 0;
#endif
}

#else

MM_EXPORT_DLL mmBool_t mmCPU_SupportCPUId(void)
{
    return MM_FALSE;
}
MM_EXPORT_DLL mmUInt_t mmPerformCPUId(struct mmCPUIdInfo* p, mmUInt_t query)
{
    p->_eax = 0;
    p->_ebx = 0;
    p->_edx = 0;
    p->_ecx = 0;

    return 0;
}

#endif


#if MM_COMPILER == MM_COMPILER_MSVC
#pragma warning(pop)
#endif

#if MM_CPU == MM_CPU_X86
    //---------------------------------------------------------------------
    // Detect whether or not os support Streaming SIMD Extension.
#if (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && MM_PLATFORM != MM_PLATFORM_NACL
#if MM_ARCH_TYPE == MM_ARCHITECTURE_32 && MM_CPU == MM_CPU_X86
static jmp_buf sIllegalJmpBuf;
static void __static_IllegalHandler(int x)
{
    (void)(x); // Unused
    longjmp(sIllegalJmpBuf, 1);
}
#endif
#endif
static mmBool_t __static_CheckOperatingSystemSupportSSE(void)
{
#if MM_COMPILER == MM_COMPILER_MSVC
    /*
        The FP part of SSE introduces a new architectural state and therefore
        requires support from the operating system. So even if CPUID indicates
        support for SSE FP, the application might not be able to use it. If
        CPUID indicates support for SSE FP, check here whether it is also
        supported by the OS, and turn off the SSE FP feature bit if there
        is no OS support for SSE FP.

        Operating systems that do not support SSE FP return an illegal
        instruction exception if execution of an SSE FP instruction is performed.
        Here, a sample SSE FP instruction is executed, and is checked for an
        exception using the (non-standard) __try/__except mechanism
        of Microsoft Visual C/C++.
    */
    // Visual Studio 2005, Both AMD and Intel x64 support SSE
    // note that even though this is a build rather than runtime setting, all
    // 64-bit CPUs support this so since binary is 64-bit only we're ok
#if _MSC_VER >= 1400 && defined(_M_X64)
    return MM_TRUE;
#else
    __try
    {
        __asm orps  xmm0, xmm0
        return MM_TRUE;
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
        return MM_FALSE;
    }
#endif
#elif (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && \
       MM_PLATFORM != MM_PLATFORM_NACL && MM_PLATFORM != MM_PLATFORM_EMSCRIPTEN
#if MM_ARCH_TYPE == MM_ARCHITECTURE_64 
    return MM_TRUE;
#else
    // Does gcc have __try/__except similar mechanism?
    // Use signal, setjmp/longjmp instead.
    void(*oldHandler)(int);
    oldHandler = signal(SIGILL, __static_IllegalHandler);

    if (setjmp(sIllegalJmpBuf))
    {
        signal(SIGILL, oldHandler);
        return MM_FALSE;
    }
    else
    {
        __asm__ __volatile__("orps %xmm0, %xmm0");
        signal(SIGILL, oldHandler);
        return MM_TRUE;
    }
#endif
#else
    // TODO: Supports other compiler, assumed is supported by default
    return MM_TRUE;
#endif
}

//---------------------------------------------------------------------
// Compiler-independent routines
//---------------------------------------------------------------------

static mmUInt_t __static_QueryCPUFeatures(void)
{
#define CPUID_FUNC_VENDOR_ID                 0x0
#define CPUID_FUNC_STANDARD_FEATURES         0x1
#define CPUID_FUNC_EXTENSION_QUERY           0x80000000
#define CPUID_FUNC_EXTENDED_FEATURES         0x80000001
#define CPUID_FUNC_ADVANCED_POWER_MANAGEMENT 0x80000007

#define CPUID_STD_FPU               (1<<0)
#define CPUID_STD_TSC               (1<<4)
#define CPUID_STD_CMOV              (1<<15)
#define CPUID_STD_MMX               (1<<23)
#define CPUID_STD_SSE               (1<<25)
#define CPUID_STD_SSE2              (1<<26)
#define CPUID_STD_HTT               (1<<28)     // EDX[28] - Bit 28 set indicates  Hyper-Threading Technology is supported in hardware.

#define CPUID_STD_SSE3              (1<<0)      // ECX[0]  - Bit 0 of standard function 1 indicate SSE3 supported
#define CPUID_STD_SSE41             (1<<19)     // ECX[19] - Bit 0 of standard function 1 indicate SSE41 supported
#define CPUID_STD_SSE42             (1<<20)     // ECX[20] - Bit 0 of standard function 1 indicate SSE42 supported

#define CPUID_FAMILY_ID_MASK        0x0F00      // EAX[11:8] - Bit 11 thru 8 contains family  processor id
#define CPUID_EXT_FAMILY_ID_MASK    0x0F00000   // EAX[23:20] - Bit 23 thru 20 contains extended family processor id
#define CPUID_PENTIUM4_ID           0x0F00      // Pentium 4 family processor id

#define CPUID_EXT_3DNOW             (1<<31)
#define CPUID_EXT_AMD_3DNOWEXT      (1<<30)
#define CPUID_EXT_AMD_MMXEXT        (1<<22)


#define CPUID_APM_INVARIANT_TSC     (1<<8)      // EDX[8] - Bit 8 of function 0x80000007 indicates support for invariant TSC.

    mmUInt_t features = 0;

    // Supports CPUID instruction ?
    if (mmCPU_SupportCPUId())
    {
        struct mmCPUIdInfo result;

        // Has standard feature ?
        if (mmPerformCPUId(&result, CPUID_FUNC_VENDOR_ID))
        {
            // Check vendor strings
            if (memcmp(&result._ebx, "GenuineIntel", 12) == 0)
            {
                mmUInt_t maxExtensionFunctionSupport = 0;

                if (result._eax > 2)
                    features |= MM_CPU_FEATURE_PRO;

                // Check standard feature
                mmPerformCPUId(&result, CPUID_FUNC_STANDARD_FEATURES);

                if (result._edx & CPUID_STD_FPU)
                    features |= MM_CPU_FEATURE_FPU;
                if (result._edx & CPUID_STD_TSC)
                    features |= MM_CPU_FEATURE_TSC;
                if (result._edx & CPUID_STD_CMOV)
                    features |= MM_CPU_FEATURE_CMOV;
                if (result._edx & CPUID_STD_MMX)
                    features |= MM_CPU_FEATURE_MMX;
                if (result._edx & CPUID_STD_SSE)
                    features |= MM_CPU_FEATURE_MMXEXT | MM_CPU_FEATURE_SSE;
                if (result._edx & CPUID_STD_SSE2)
                    features |= MM_CPU_FEATURE_SSE2;
                if (result._ecx & CPUID_STD_SSE3)
                    features |= MM_CPU_FEATURE_SSE3;
                if (result._ecx & CPUID_STD_SSE41)
                    features |= MM_CPU_FEATURE_SSE41;
                if (result._ecx & CPUID_STD_SSE42)
                    features |= MM_CPU_FEATURE_SSE42;

                // Check to see if this is a Pentium 4 or later processor
                if ((result._eax & CPUID_EXT_FAMILY_ID_MASK) ||
                    (result._eax & CPUID_FAMILY_ID_MASK) == CPUID_PENTIUM4_ID)
                {
                    // Check hyper-threading technology
                    if (result._edx & CPUID_STD_HTT)
                        features |= MM_CPU_FEATURE_HTT;
                }


                maxExtensionFunctionSupport = mmPerformCPUId(&result, CPUID_FUNC_EXTENSION_QUERY);
                if (maxExtensionFunctionSupport >= CPUID_FUNC_ADVANCED_POWER_MANAGEMENT)
                {
                    mmPerformCPUId(&result, CPUID_FUNC_ADVANCED_POWER_MANAGEMENT);

                    if (result._edx & CPUID_APM_INVARIANT_TSC)
                        features |= MM_CPU_FEATURE_INVARIANT_TSC;
                }
            }
            else if (memcmp(&result._ebx, "AuthenticAMD", 12) == 0)
            {
                mmUInt_t maxExtensionFunctionSupport = 0;

                features |= MM_CPU_FEATURE_PRO;

                // Check standard feature
                mmPerformCPUId(&result, CPUID_FUNC_STANDARD_FEATURES);

                if (result._edx & CPUID_STD_FPU)
                    features |= MM_CPU_FEATURE_FPU;
                if (result._edx & CPUID_STD_TSC)
                    features |= MM_CPU_FEATURE_TSC;
                if (result._edx & CPUID_STD_CMOV)
                    features |= MM_CPU_FEATURE_CMOV;
                if (result._edx & CPUID_STD_MMX)
                    features |= MM_CPU_FEATURE_MMX;
                if (result._edx & CPUID_STD_SSE)
                    features |= MM_CPU_FEATURE_SSE;
                if (result._edx & CPUID_STD_SSE2)
                    features |= MM_CPU_FEATURE_SSE2;

                if (result._ecx & CPUID_STD_SSE3)
                    features |= MM_CPU_FEATURE_SSE3;

                // Has extended feature ?
                maxExtensionFunctionSupport = mmPerformCPUId(&result, CPUID_FUNC_EXTENSION_QUERY);
                if (maxExtensionFunctionSupport >= CPUID_FUNC_EXTENDED_FEATURES)
                {
                    // Check extended feature
                    mmPerformCPUId(&result, CPUID_FUNC_EXTENDED_FEATURES);

                    if (result._edx & CPUID_EXT_3DNOW)
                        features |= MM_CPU_FEATURE_3DNOW;
                    if (result._edx & CPUID_EXT_AMD_3DNOWEXT)
                        features |= MM_CPU_FEATURE_3DNOWEXT;
                    if (result._edx & CPUID_EXT_AMD_MMXEXT)
                        features |= MM_CPU_FEATURE_MMXEXT;
                }


                if (maxExtensionFunctionSupport >= CPUID_FUNC_ADVANCED_POWER_MANAGEMENT)
                {
                    mmPerformCPUId(&result, CPUID_FUNC_ADVANCED_POWER_MANAGEMENT);

                    if (result._edx & CPUID_APM_INVARIANT_TSC)
                        features |= MM_CPU_FEATURE_INVARIANT_TSC;
                }
            }
        }
    }

    return features;
}
//---------------------------------------------------------------------
static mmUInt_t __static_DetectCPUFeatures(void)
{
    mmUInt_t features = __static_QueryCPUFeatures();

    const mmUInt_t sse_features = 0
        | MM_CPU_FEATURE_SSE
        | MM_CPU_FEATURE_SSE2
        | MM_CPU_FEATURE_SSE3
        | MM_CPU_FEATURE_SSE41
        | MM_CPU_FEATURE_SSE42;

    if ((features & sse_features) && !__static_CheckOperatingSystemSupportSSE())
    {
        features &= ~sse_features;
    }

    return features;
}
//---------------------------------------------------------------------
static void __static_DetectCPUIdentifier(struct mmString* identifier)
{
    mmString_Clear(identifier);
    // Supports CPUID instruction ?
    if (mmCPU_SupportCPUId())
    {
        struct mmCPUIdInfo result;
        mmUInt_t nExIds;
        char CPUString[0x20];
        char CPUBrandString[0x40];

        //StringStream detailedIdentStr;

        // Has standard feature ?
        if (mmPerformCPUId(&result, 0))
        {
            mmUInt_t i = 0;

            memset(CPUString, 0, sizeof(CPUString));
            memset(CPUBrandString, 0, sizeof(CPUBrandString));

            //*((int*)CPUString) = result._ebx;
            //*((int*)(CPUString+4)) = result._edx;
            //*((int*)(CPUString+8)) = result._ecx;
            memcpy(CPUString, &result._ebx, sizeof(int));
            memcpy(CPUString + 4, &result._edx, sizeof(int));
            memcpy(CPUString + 8, &result._ecx, sizeof(int));

            mmString_Appends(identifier, CPUString);

            // Calling _performCpuid with 0x80000000 as the query argument
            // gets the number of valid extended IDs.
            nExIds = mmPerformCPUId(&result, 0x80000000);

            for (i = 0x80000000; i <= nExIds; ++i)
            {
                mmPerformCPUId(&result, i);

                // Interpret CPU brand string and cache information.
                if (i == 0x80000002)
                {
                    memcpy(CPUBrandString + 0, &result._eax, sizeof(result._eax));
                    memcpy(CPUBrandString + 4, &result._ebx, sizeof(result._ebx));
                    memcpy(CPUBrandString + 8, &result._ecx, sizeof(result._ecx));
                    memcpy(CPUBrandString + 12, &result._edx, sizeof(result._edx));
                }
                else if (i == 0x80000003)
                {
                    memcpy(CPUBrandString + 16 + 0, &result._eax, sizeof(result._eax));
                    memcpy(CPUBrandString + 16 + 4, &result._ebx, sizeof(result._ebx));
                    memcpy(CPUBrandString + 16 + 8, &result._ecx, sizeof(result._ecx));
                    memcpy(CPUBrandString + 16 + 12, &result._edx, sizeof(result._edx));
                }
                else if (i == 0x80000004)
                {
                    memcpy(CPUBrandString + 32 + 0, &result._eax, sizeof(result._eax));
                    memcpy(CPUBrandString + 32 + 4, &result._ebx, sizeof(result._ebx));
                    memcpy(CPUBrandString + 32 + 8, &result._ecx, sizeof(result._ecx));
                    memcpy(CPUBrandString + 32 + 12, &result._edx, sizeof(result._edx));
                }
            }

            mmString_Appends(identifier, " : ");
            mmString_Appends(identifier, CPUBrandString);
        }
    }

    mmString_Assigns(identifier, "X86");
}

#elif MM_PLATFORM == MM_PLATFORM_ANDROID
static mmUInt_t __static_DetectCPUFeatures(void)
{
    mmUInt_t features = 0;
#if MM_CPU == MM_CPU_ARM
    mmUInt64_t cpufeatures = android_getCpuFeatures();
    if (cpufeatures & ANDROID_CPU_ARM_FEATURE_NEON)
    {
        features |= MM_CPU_FEATURE_NEON;
    }

    if (cpufeatures & ANDROID_CPU_ARM_FEATURE_VFPv3)
    {
        features |= MM_CPU_FEATURE_VFP;
    }
#elif MM_CPU == MM_CPU_X86
    // see https://developer.android.com/ndk/guides/abis.html
    features |= MM_CPU_FEATURE_SSE;
    features |= MM_CPU_FEATURE_SSE2;
    features |= MM_CPU_FEATURE_SSE3;
#endif
    return features;
}
//---------------------------------------------------------------------
static void __static_DetectCPUIdentifier(struct mmString* identifier)
{
    mmString_Clear(identifier);

    AndroidCpuFamily cpuInfo = android_getCpuFamily();

    switch (cpuInfo)
    {
    case ANDROID_CPU_FAMILY_ARM64:
        mmString_Assigns(identifier, "ARM64");
        break;
    case ANDROID_CPU_FAMILY_ARM:
    {
        if (android_getCpuFeatures() & ANDROID_CPU_ARM_FEATURE_ARMv7)
        {
            mmString_Assigns(identifier, "ARMv7");
        }
        else
        {
            mmString_Assigns(identifier, "Unknown ARM");
        }
    }
    break;
    case ANDROID_CPU_FAMILY_X86:
        mmString_Assigns(identifier, "Unknown X86");
        break;
    default:
        mmString_Assigns(identifier, "Unknown");
        break;
    }
}

#elif MM_CPU == MM_CPU_ARM  // MM_CPU == MM_CPU_ARM

    //---------------------------------------------------------------------
static mmUInt_t __static_DetectCPUFeatures(void)
{
    // Use preprocessor definitions to determine architecture and CPU features
    mmUInt_t features = 0;
#if defined(__ARM_NEON__)
#if MM_PLATFORM == MM_PLATFORM_APPLE_IOS
    int hasNEON;
    size_t len = sizeof(size_t);
    sysctlbyname("hw.optional.neon", &hasNEON, &len, NULL, 0);

    if (hasNEON)
#endif
        features |= MM_CPU_FEATURE_NEON;
#elif defined(__VFP_FP__)
    features |= MM_CPU_FEATURE_VFP;
#endif
    return features;
}
//---------------------------------------------------------------------
static void __static_DetectCPUIdentifier(struct mmString* identifier)
{
    mmString_Clear(identifier);

#if MM_PLATFORM == MM_PLATFORM_APPLE_IOS
    // Get the size of the CPU subtype struct
    size_t size;
    sysctlbyname("hw.cpusubtype", NULL, &size, NULL, 0);

    // Get the ARM CPU subtype
    cpu_subtype_t cpusubtype = 0;
    sysctlbyname("hw.cpusubtype", &cpusubtype, &size, NULL, 0);

    switch (cpusubtype)
    {
    case CPU_SUBTYPE_ARM_V6:
        mmString_Assigns(identifier, "ARMv6");
        break;
    case CPU_SUBTYPE_ARM_V7:
        mmString_Assigns(identifier, "ARMv7");
        break;
    case CPU_SUBTYPE_ARM_V7F:
        mmString_Assigns(identifier, "ARM Cortex-A9");
        break;
    case CPU_SUBTYPE_ARM_V7S:
        mmString_Assigns(identifier, "ARM Swift");
        break;
    case CPU_SUBTYPE_ARM_V8:
        mmString_Assigns(identifier, "ARMv8");
        break;
    case CPU_SUBTYPE_ARM64_V8:
        mmString_Assigns(identifier, "ARM64v8");
        break;
    default:
        mmString_Assigns(identifier, "Unknown ARM");
        break;
    }
#endif
}

#elif MM_CPU == MM_CPU_MIPS  // MM_CPU == MM_CPU_ARM

    //---------------------------------------------------------------------
static mmUInt_t __static_DetectCPUFeatures(void)
{
    // Use preprocessor definitions to determine architecture and CPU features
    mmUInt_t features = 0;
#if defined(__mips_msa)
    features |= MM_CPU_FEATURE_MSA;
#endif
    return features;
}
//---------------------------------------------------------------------
static void __static_DetectCPUIdentifier(struct mmString* identifier)
{
    mmString_Clear(identifier);

    mmString_Assigns(identifier, "MIPS");
}

#else   // MM_CPU == MM_CPU_MIPS

    //---------------------------------------------------------------------
static mmUInt_t __static_DetectCPUFeatures(void)
{
    return 0;
}
//---------------------------------------------------------------------
static void __static_DetectCPUIdentifier(struct mmString* identifier)
{
    mmString_Clear(identifier);

    mmString_Assigns(identifier, "Unknown");
}

#endif  // MM_CPU

//---------------------------------------------------------------------
MM_EXPORT_DLL const char* mmCPU_Identifier(void)
{
    static mmBool_t _update = MM_FALSE;
    static char sIdentifier[128] = { 0 };

    if (MM_FALSE == _update)
    {
        struct mmString identifier;

        _update = MM_TRUE;

        mmString_Init(&identifier);

        __static_DetectCPUIdentifier(&identifier);

        mmMemcpy(sIdentifier, mmString_CStr(&identifier), mmString_Size(&identifier));

        mmString_Destroy(&identifier);
    }

    return sIdentifier;
}
MM_EXPORT_DLL mmUInt_t mmCPU_Features(void)
{
    static mmBool_t _update = MM_FALSE;
    static mmUInt_t sFeatures = -1;

    if (MM_FALSE == _update)
    {
        _update = MM_TRUE;

        sFeatures = __static_DetectCPUFeatures();
    }

    return sFeatures;
}
MM_EXPORT_DLL mmBool_t mmCPU_HasFeature(mmUInt_t feature)
{
    return (mmCPU_Features() & feature) != 0;
}

static const char* __static_BoolToStringYesNo(mmBool_t v)
{
    return v ? "yes" : "no";
}
static const char* __static_FeatureToString(mmUInt_t v)
{
    return __static_BoolToStringYesNo(mmCPU_HasFeature(v));
}
MM_EXPORT_DLL void mmCPU_LoggerInformation(struct mmLogger* logger)
{
    mmLogger_LogI(logger, "CPU Identifier & Features");
    mmLogger_LogI(logger, "-------------------------");

    mmLogger_LogI(logger, " *   CPU ID: %s", mmCPU_Identifier());

#if MM_CPU == MM_CPU_X86
    if (mmCPU_SupportCPUId())
    {
        mmLogger_LogI(logger, " *          SSE: %s", __static_FeatureToString(MM_CPU_FEATURE_SSE));
        mmLogger_LogI(logger, " *         SSE2: %s", __static_FeatureToString(MM_CPU_FEATURE_SSE2));
        mmLogger_LogI(logger, " *         SSE3: %s", __static_FeatureToString(MM_CPU_FEATURE_SSE3));
        mmLogger_LogI(logger, " *        SSE41: %s", __static_FeatureToString(MM_CPU_FEATURE_SSE41));
        mmLogger_LogI(logger, " *        SSE42: %s", __static_FeatureToString(MM_CPU_FEATURE_SSE42));
        mmLogger_LogI(logger, " *          MMX: %s", __static_FeatureToString(MM_CPU_FEATURE_MMX));
        mmLogger_LogI(logger, " *       MMXEXT: %s", __static_FeatureToString(MM_CPU_FEATURE_MMXEXT));
        mmLogger_LogI(logger, " *        3DNOW: %s", __static_FeatureToString(MM_CPU_FEATURE_3DNOW));
        mmLogger_LogI(logger, " *     3DNOWEXT: %s", __static_FeatureToString(MM_CPU_FEATURE_3DNOWEXT));
        mmLogger_LogI(logger, " *         CMOV: %s", __static_FeatureToString(MM_CPU_FEATURE_CMOV));
        mmLogger_LogI(logger, " *INVARIANT TSC: %s", __static_FeatureToString(MM_CPU_FEATURE_INVARIANT_TSC));
        mmLogger_LogI(logger, " *          FPU: %s", __static_FeatureToString(MM_CPU_FEATURE_FPU));
        mmLogger_LogI(logger, " *          PRO: %s", __static_FeatureToString(MM_CPU_FEATURE_PRO));
        mmLogger_LogI(logger, " *           HT: %s", __static_FeatureToString(MM_CPU_FEATURE_HTT));
    }
#elif MM_CPU == MM_CPU_ARM || MM_PLATFORM == MM_PLATFORM_ANDROID
    mmLogger_LogI(logger, " *          VFP: %s", __static_FeatureToString(MM_CPU_FEATURE_VFP));
    mmLogger_LogI(logger, " *         NEON: %s", __static_FeatureToString(MM_CPU_FEATURE_NEON));
#elif MM_CPU == MM_CPU_MIPS
    mmLogger_LogI(logger, " *          MSA: %s", __static_FeatureToString(MM_CPU_FEATURE_MSA));
#endif
    mmLogger_LogI(logger, "-------------------------");
}

static void mmCPUInfo_Vendor(struct mmCPUInfo* p)
{
    struct mmCPUIdInfo cpuid_info;
    char vendor[13] = { 0 };
    mmPerformCPUId(&cpuid_info, 0);
    mmMemcpy(vendor, &cpuid_info._ebx, 12);
    mmString_Assigns(&p->vendor, vendor);
}
static void mmCPUInfo_Brand(struct mmCPUInfo* p)
{
    mmUInt32_t i = 0;
    struct mmCPUIdInfo cpuid_info;
    // 0x80000002--0x80000004
    const mmUInt32_t BRANDID = 0x80000002;
    char name[49] = { 0 }; // 48 char
    for (i = 0; i < 3; i++)
    {
        mmPerformCPUId(&cpuid_info, BRANDID + i);
        mmMemcpy(name + i * 16, &cpuid_info, 16);
    }
    mmString_Assigns(&p->brand, name);
}
static void mmCPUInfo_Flags(struct mmCPUInfo* p)
{
    struct mmCPUIdInfo cpuid_info;
    mmPerformCPUId(&cpuid_info, 1);
    mmMemcpy(&p->level, &cpuid_info._eax, 8);
    mmMemcpy(&p->flags, &cpuid_info._edx, 8);
}

MM_EXPORT_DLL void mmCPUInfo_Init(struct mmCPUInfo* p)
{
    mmString_Init(&p->vendor);
    mmString_Init(&p->brand);
    p->level = 0;
    p->flags = 0;
}
MM_EXPORT_DLL void mmCPUInfo_Destroy(struct mmCPUInfo* p)
{
    mmString_Destroy(&p->vendor);
    mmString_Destroy(&p->brand);
}
MM_EXPORT_DLL void mmCPUInfo_Perform(struct mmCPUInfo* p)
{
    mmCPUInfo_Vendor(p);
    mmCPUInfo_Brand(p);
    mmCPUInfo_Flags(p);
}

