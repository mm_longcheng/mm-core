/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTimeUtils_h__
#define __mmTimeUtils_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL void mmGMTime(time_t t, struct tm *tp);

// _offset second is dep for 1970/1/1 8:0:0 - 8 hour is 1970/1/1 0:0:0
// _timecode second is time code now 
MM_EXPORT_DLL time_t mmGetDurationFromDawnByOffset(time_t _timecode, time_t _offset);
// timecode is unix time.
MM_EXPORT_DLL time_t mmGetDurationFromWeek(time_t _timecode);
// _offset second is dep for 1970/1/1 8:0:0 - 8 hour is 1970/1/1 0:0:0
// _timecode second is time code now 
MM_EXPORT_DLL int mmGetDayFlip(time_t _timecode, time_t _offset);

// Normalize time year month.
MM_EXPORT_DLL void mmTimeYMNormalize(int y, int m, int* ny, int* nm);

#include "core/mmSuffix.h"

#endif//__mmTimeUtils_h__
