/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTimer_h__
#define __mmTimer_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmClock.h"
#include "core/mmThread.h"
#include "core/mmAtomic.h"
#include "core/mmTimerTask.h"
#include "core/mmPoolElement.h"

#include "container/mmVectorU32.h"
#include "container/mmVectorVpt.h"

#include <pthread.h>

#include "core/mmPrefix.h"

#define MM_HEAP_P(X)    (X == 0 ? 0 : (((X) - 1) / 2))
#define MM_HEAP_L(X)    (((X)+(X))+1)
#define MM_HEAP_R(X)    (((X)+(X))+2)

#define MM_TIMER_ID_INVALID -1
#define MM_TIMER_HEAP_MAX_POLL_TIMEOUT 25
#define MM_TIMER_HEAP_PAGE 64
#define MM_TIMER_TASK_NEARBY_MSEC 20000
#define MM_TIMER_HEAP_CHUNK_SIZE_DEFAULT 128

// timer for time schedule.

/**
 * The type for internal timer ID.
 */
typedef int mmTimerId_t;

/**
 * The type for timer heap.
 */
struct mmTimerHeap;

/**
 * Forward declaration for pj_timer_entry.
 */
struct mmTimerEntry;

/**
 * The type of callback function to be called by timer scheduler when a timer
 * has expired.
 *
 * @param timer_heap    The timer heap.
 * @param entry         Timer entry which timer's has expired.
 */
typedef void(*mmTimerEntryHandleFunc)(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
struct mmTimerEntryCallback
{
    mmTimerEntryHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmTimerEntryCallback_Init(struct mmTimerEntryCallback* p);
MM_EXPORT_DLL void mmTimerEntryCallback_Destroy(struct mmTimerEntryCallback* p);
MM_EXPORT_DLL void mmTimerEntryCallback_Reset(struct mmTimerEntryCallback* p);

/**
 * This structure represents an entry to the timer.
 */
struct mmTimerEntry
{
    /**
     * Callback to be called when the timer expires.
     */
    struct mmTimerEntryCallback callback;

    /**
     * The future time when the timer expires, which the value is updated
     * by timer heap when the timer is scheduled.
     */
    struct timeval timer_value;

    /**
     * The future time when the timer expires, will repeat.
     * if 0 == period will only fire one time after delay.
     * default is 0.
     */
    mmMSec_t period;

    /**
     * Internal: the lock used by this entry.
     */
    mmAtomic_t locker;

    /**
     * Arbitrary ID assigned by the user/owner of this entry.
     * Applications can use this ID to distinguish multiple
     * timer entries that share the same callback and user_data.
     */
    mmUInt64_t id;

    /**
     * Internal unique timer ID, which is assigned by the timer heap.
     * Application should not touch this ID.
     * default is MM_TIMER_ID_INVALID.
     */
    mmTimerId_t timer_id;
};
MM_EXPORT_DLL void mmTimerEntry_Init(struct mmTimerEntry* p);
MM_EXPORT_DLL void mmTimerEntry_Destroy(struct mmTimerEntry* p);
MM_EXPORT_DLL void mmTimerEntry_Reset(struct mmTimerEntry* p);
/* locker order is
*     locker
*/
MM_EXPORT_DLL void mmTimerEntry_Lock(struct mmTimerEntry* p);
MM_EXPORT_DLL void mmTimerEntry_Unlock(struct mmTimerEntry* p);
// compare p q.less return none zero.
MM_EXPORT_DLL int mmTimerEntry_Compare(const struct mmTimerEntry* p, const struct mmTimerEntry* q);

struct mmTimerHeapCallback
{
    void(*Produce)(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
    void(*Recycle)(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmTimerHeapCallback_Init(struct mmTimerHeapCallback* p);
MM_EXPORT_DLL void mmTimerHeapCallback_Destroy(struct mmTimerHeapCallback* p);

/**
 * The implementation of timer heap. Low accuracy.
 */
struct mmTimerHeap
{
    /** Callback to be called when a timer expires. */
    struct mmTimerHeapCallback callback;

    /**
     * mmTimerEntry storage.
    */
    //struct mmTimerEntry* heap_entry;
    struct mmPoolElement heap_entry;

    /**
     * Current contents of the Heap, which is organized as a "heap" of
     * mmTimerEntry's.  In this context, a heap is a "partially
     * ordered, almost complete" binary tree, which is stored in an
     * array.
    */
    struct mmVectorVpt heap;

    /**
     * An array of "pointers" that allows each mmTimerEntry in the
     * "heap_" to be located in O(1) time.  Basically, "timer_id_[i]"
     * contains the slot in the "heap_" array where an pj_timer_entry
     * with timer id "i" resides.  Thus, the timer id passed back from
     * "schedule_entry" is really an slot into the "timer_ids" array.  The
     * "timer_ids_" array serves two purposes: negative values are
     * treated as "pointers" for the "freelist_", whereas positive
     * values are treated as "pointers" into the "heap_" array.
    */
    struct mmVectorU32 timer_ids;

    /** Time clock. */
    struct mmClock clock;

    /** Lock heap. */
    mmAtomic_t locker_heap;

    /** Lock object. */
    mmAtomic_t locker;

    /** Maximum size of the heap.default is MM_TIMER_HEAP_POLL_PAGE */
    size_t max_size;

    /** Current size of the heap. */
    size_t cur_size;

    /** Max timed out entries to process per poll.
     * default is MM_TIMER_HEAP_MAX_POLL_TIMEOUT.
     */
    mmUInt32_t max_entries_per_poll;

    /**
     * "Pointer" to the first element in the freelist contained within
     * the <timer_ids_> array, which is organized as a stack.
     */
    mmTimerId_t timer_ids_freelist;

    /** Next time wait per poll.
     * default is 0, microseconds.
     */
    double wait_usec;

    /** Frame cost per poll.
     * default is 0, microseconds.
     */
    double frame_cost_usec;
};

MM_EXPORT_DLL void mmTimerHeap_Init(struct mmTimerHeap* p);
MM_EXPORT_DLL void mmTimerHeap_Destroy(struct mmTimerHeap* p);

/* locker order is
*     heap_entry
*     locker
*/
MM_EXPORT_DLL void mmTimerHeap_Lock(struct mmTimerHeap* p);
MM_EXPORT_DLL void mmTimerHeap_Unlock(struct mmTimerHeap* p);

MM_EXPORT_DLL void mmTimerHeap_SetLength(struct mmTimerHeap* p, size_t length);
MM_EXPORT_DLL void mmTimerHeap_SetChunkSize(struct mmTimerHeap* p, mmUInt32_t chunk_size);

MM_EXPORT_DLL int mmTimerHeap_ScheduleEntry(struct mmTimerHeap* p, struct mmTimerEntry* entry, struct timeval* delay);

MM_EXPORT_DLL struct mmTimerEntry*
mmTimerHeap_ScheduleTimeval(
    struct mmTimerHeap* p,
    struct timeval* delay,
    mmTimerEntryHandleFunc handle);

// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mmTimerEntry*
mmTimerHeap_Schedule(
    struct mmTimerHeap* p,
    mmMSec_t delay,
    mmMSec_t period,
    mmTimerEntryHandleFunc handle,
    void* u);

MM_EXPORT_DLL int mmTimerHeap_Cancel(struct mmTimerHeap* p, struct mmTimerEntry* entry);

MM_EXPORT_DLL mmUInt32_t mmTimerHeap_Poll(struct mmTimerHeap* p, struct timeval* next_delay);
MM_EXPORT_DLL void mmTimerHeap_EarliestTime(struct mmTimerHeap* p, struct timeval* next_delay);

struct mmTimer
{
    struct mmTimerHeap timer_heap;
    struct mmTimerTask timer_task;
};

MM_EXPORT_DLL void mmTimer_Init(struct mmTimer* p);
MM_EXPORT_DLL void mmTimer_Destroy(struct mmTimer* p);

/* locker order is
*     timer_heap
*/
MM_EXPORT_DLL void mmTimer_Lock(struct mmTimer* p);
MM_EXPORT_DLL void mmTimer_Unlock(struct mmTimer* p);

// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mmTimerEntry*
mmTimer_Schedule(
    struct mmTimer* p,
    mmMSec_t delay,
    mmMSec_t period,
    mmTimerEntryHandleFunc handle,
    void* u);

MM_EXPORT_DLL void mmTimer_Cancel(struct mmTimer* p, struct mmTimerEntry* entry);

MM_EXPORT_DLL void mmTimer_Start(struct mmTimer* p);
MM_EXPORT_DLL void mmTimer_Interrupt(struct mmTimer* p);
MM_EXPORT_DLL void mmTimer_Shutdown(struct mmTimer* p);
MM_EXPORT_DLL void mmTimer_Join(struct mmTimer* p);

#include "core/mmSuffix.h"

#endif//__mmTimer_h__
