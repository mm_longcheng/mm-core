/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmByteOrder_h__
#define __mmByteOrder_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmByteBuffer;
struct mmStreambuf;
struct mmStream;

// net order in most cases is bigger endian.

MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeBin(struct mmByteBuffer* b, size_t* o, const mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU08(struct mmByteBuffer* b, size_t* o, const mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU16(struct mmByteBuffer* b, size_t* o, const mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU32(struct mmByteBuffer* b, size_t* o, const mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU64(struct mmByteBuffer* b, size_t* o, const mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeBin(const struct mmByteBuffer* b, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU08(const struct mmByteBuffer* b, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU16(const struct mmByteBuffer* b, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU32(const struct mmByteBuffer* b, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU64(const struct mmByteBuffer* b, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeBin(struct mmByteBuffer* b, size_t* o, const mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU08(struct mmByteBuffer* b, size_t* o, const mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU16(struct mmByteBuffer* b, size_t* o, const mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU32(struct mmByteBuffer* b, size_t* o, const mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU64(struct mmByteBuffer* b, size_t* o, const mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeBin(const struct mmByteBuffer* b, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU08(const struct mmByteBuffer* b, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU16(const struct mmByteBuffer* b, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU32(const struct mmByteBuffer* b, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU64(const struct mmByteBuffer* b, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeBin(struct mmByteBuffer* b, size_t* o, const mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU08(struct mmByteBuffer* b, size_t* o, const mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU16(struct mmByteBuffer* b, size_t* o, const mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU32(struct mmByteBuffer* b, size_t* o, const mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU64(struct mmByteBuffer* b, size_t* o, const mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeBin(const struct mmByteBuffer* b, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU08(const struct mmByteBuffer* b, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU16(const struct mmByteBuffer* b, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU32(const struct mmByteBuffer* b, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU64(const struct mmByteBuffer* b, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU08(FILE* f, long* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU16(FILE* f, long* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU32(FILE* f, long* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU64(FILE* f, long* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU08(FILE* f, long* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU16(FILE* f, long* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU32(FILE* f, long* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU64(FILE* f, long* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU08(FILE* f, long* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU16(FILE* f, long* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU32(FILE* f, long* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU64(FILE* f, long* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU08(FILE* f, long* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU16(FILE* f, long* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU32(FILE* f, long* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU64(FILE* f, long* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v);

#include "core/mmSuffix.h"

#endif//__mmByteOrder_h__
