/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSignalTask_h__
#define __mmSignalTask_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmThread.h"
#include "core/mmSpinlock.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// signal task default success nearby time milliseconds.
#define MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC 0
// signal task default failure nearby time milliseconds.
#define MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC 200

struct mmSignalTaskCallback
{
    // factor for condition wait. return code for 0 is fire event -1 for condition wait.
    int(*Factor)(void* obj);
    // handle for fire event. return code result 0 for success -1 for failure.
    int(*Handle)(void* obj);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmSignalTaskCallback_Init(struct mmSignalTaskCallback* p);
MM_EXPORT_DLL void mmSignalTaskCallback_Destroy(struct mmSignalTaskCallback* p);

struct mmSignalTask
{
    struct mmSignalTaskCallback callback;
    pthread_t signal_thread;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t locker;
    // nearby_time milliseconds for each time wait. default is MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC ms.
    mmMSec_t msec_success_nearby;
    // nearby_time milliseconds for each time wait. default is MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC ms.
    mmMSec_t msec_failure_nearby;
    // delay milliseconds before first callback. default is 0 ms.
    mmMSec_t msec_delay;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_DLL void mmSignalTask_Init(struct mmSignalTask* p);
MM_EXPORT_DLL void mmSignalTask_Destroy(struct mmSignalTask* p);

/* locker order is
*     locker
*/
MM_EXPORT_DLL void mmSignalTask_Lock(struct mmSignalTask* p);
MM_EXPORT_DLL void mmSignalTask_Unlock(struct mmSignalTask* p);

MM_EXPORT_DLL void mmSignalTask_SetCallback(struct mmSignalTask* p, const struct mmSignalTaskCallback* cb);
MM_EXPORT_DLL void mmSignalTask_SetSuccessNearby(struct mmSignalTask* p, mmMSec_t nearby);
MM_EXPORT_DLL void mmSignalTask_SetFailureNearby(struct mmSignalTask* p, mmMSec_t nearby);

// wait.
MM_EXPORT_DLL void mmSignalTask_SignalWait(struct mmSignalTask* p);
// signal.
MM_EXPORT_DLL void mmSignalTask_Signal(struct mmSignalTask* p);

// start wait thread.
MM_EXPORT_DLL void mmSignalTask_Start(struct mmSignalTask* p);
// interrupt wait thread.
MM_EXPORT_DLL void mmSignalTask_Interrupt(struct mmSignalTask* p);
// shutdown wait thread.
MM_EXPORT_DLL void mmSignalTask_Shutdown(struct mmSignalTask* p);
// join wait thread.
MM_EXPORT_DLL void mmSignalTask_Join(struct mmSignalTask* p);


#include "core/mmSuffix.h"

#endif//__mmSignalTask_h__
