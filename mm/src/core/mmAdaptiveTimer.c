/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAdaptiveTimer.h"
#include "core/mmSpinlock.h"
#include "core/mmTimespec.h"
#include "core/mmTimewait.h"
#include "core/mmLogger.h"

static void __static_mmAdaptiveTimer_VisibleHandleUpdate(void* obj, void* u, const struct mmString* k, void* v);
static void __static_mmAdaptiveTimer_VisibleHandleLogger(void* obj, void* u, const struct mmString* k, void* v);

MM_EXPORT_DLL void mmAdaptiveTimerArgument_Init(struct mmAdaptiveTimerArgument* p)
{
    p->name = "";
    p->frequency = 60;
    p->background = MM_FRAME_TIMER_BACKGROUND_RUNING;
    p->handle = &mmFrameTimerCallback_UpdateDefault;
    p->u = NULL;
}
MM_EXPORT_DLL void mmAdaptiveTimerArgument_Destroy(struct mmAdaptiveTimerArgument* p)
{
    p->name = "";
    p->frequency = 60;
    p->background = MM_FRAME_TIMER_BACKGROUND_RUNING;
    p->handle = &mmFrameTimerCallback_UpdateDefault;
    p->u = NULL;
}
MM_EXPORT_DLL void mmAdaptiveTimerArgument_Reset(struct mmAdaptiveTimerArgument* p)
{
    p->name = "";
    p->frequency = 60;
    p->background = MM_FRAME_TIMER_BACKGROUND_RUNING;
    p->handle = &mmFrameTimerCallback_UpdateDefault;
    p->u = NULL;
}

MM_EXPORT_DLL void mmAdaptiveTimer_Init(struct mmAdaptiveTimer* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmRbtreeVisibleString_Init(&p->rbtree_visible);
    mmFrameStats_Init(&p->status);
    mmPoolElement_Init(&p->pool_element);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmTimeval_Init(&p->ntime);
    mmTimespec_Init(&p->otime);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->sleep_usec = 0;
    p->nearby_time = 0;
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
    p->state = MM_TS_CLOSED;// mmThreadState_t,default is MM_TS_CLOSED(0)
    p->u = NULL;

    hAllocator.Produce = &mmFrameTimer_Init;
    hAllocator.Recycle = &mmFrameTimer_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmFrameTimer));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);

    mmAdaptiveTimer_SetChunkSize(p, MM_ADAPTIVE_TIMER_CHUNK_SIZE_DEFAULT);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Destroy(struct mmAdaptiveTimer* p)
{
    mmAdaptiveTimer_Clear(p);
    //
    mmRbtreeVisibleString_Destroy(&p->rbtree_visible);
    mmFrameStats_Destroy(&p->status);
    mmPoolElement_Destroy(&p->pool_element);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmTimeval_Destroy(&p->ntime);
    mmTimespec_Destroy(&p->otime);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->locker);
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->sleep_usec = 0;
    p->nearby_time = 0;
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
    p->state = MM_TS_CLOSED;// mmThreadState_t,default is MM_TS_CLOSED(0)
    p->u = NULL;
}
MM_EXPORT_DLL void mmAdaptiveTimer_Lock(struct mmAdaptiveTimer* p)
{
    mmPoolElement_Lock(&p->pool_element);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Unlock(struct mmAdaptiveTimer* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmPoolElement_Unlock(&p->pool_element);
}
MM_EXPORT_DLL void mmAdaptiveTimer_SetContext(struct mmAdaptiveTimer* p, void* u)
{
    p->u = u;
}
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_Add(struct mmAdaptiveTimer* p, const struct mmString* name)
{
    struct mmFrameTimer* unit = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    unit = mmAdaptiveTimer_Get(p, name);
    if (NULL == unit)
    {
        // pool_element produce.
        mmPoolElement_Lock(&p->pool_element);
        unit = (struct mmFrameTimer*)mmPoolElement_Produce(&p->pool_element);
        mmPoolElement_Unlock(&p->pool_element);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmFrameTimer_Reset(unit);
        //
        mmFrameTimer_SetName(unit, mmString_CStr(name));
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleString_Add(&p->rbtree_visible, name, unit);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return unit;
}
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_Get(struct mmAdaptiveTimer* p, const struct mmString* name)
{
    struct mmFrameTimer* unit = NULL;
    // note:
    // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
    unit = (struct mmFrameTimer*)mmRbtreeVisibleString_Get(&p->rbtree_visible, name);
    //
    return unit;
}
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_GetInstance(struct mmAdaptiveTimer* p, const struct mmString* name)
{
    struct mmFrameTimer* unit = mmAdaptiveTimer_Get(p, name);
    if (NULL == unit)
    {
        unit = mmAdaptiveTimer_Add(p, name);
    }
    return unit;
}
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_GetByCString(struct mmAdaptiveTimer* p, const char* name)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    return mmAdaptiveTimer_Get(p, &hWeakKey);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Rmv(struct mmAdaptiveTimer* p, const struct mmString* name)
{
    struct mmFrameTimer* unit = mmAdaptiveTimer_Get(p, name);
    if (NULL != unit)
    {
        mmFrameTimer_Lock(unit);
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleString_Rmv(&p->rbtree_visible, name);
        mmFrameTimer_Unlock(unit);

        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmFrameTimer_Reset(unit);
        // pool_element recycle.
        mmPoolElement_Lock(&p->pool_element);
        mmPoolElement_Recycle(&p->pool_element, unit);
        mmPoolElement_Unlock(&p->pool_element);
    }
}
MM_EXPORT_DLL void mmAdaptiveTimer_Clear(struct mmAdaptiveTimer* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFrameTimer* unit = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);

    // rbtree_m locker is lock inside at mmNuclear_FdRmvPoll.
    // here we only need lock the nuclear->cond_locker.
    // mmSpinlock_Lock(&p->rbtree_visible.locker_m);
    n = mmRb_First(&p->rbtree_visible.rbtree_m.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        unit = (struct mmFrameTimer*)(it->v);
        mmFrameTimer_Lock(unit);
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleString_Rmv(&p->rbtree_visible, &it->k);
        mmFrameTimer_Unlock(unit);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmFrameTimer_Reset(unit);
        // pool_element recycle.
        mmPoolElement_Lock(&p->pool_element);
        mmPoolElement_Recycle(&p->pool_element, unit);
        mmPoolElement_Unlock(&p->pool_element);
    }
    // mmSpinlock_Unlock(&p->rbtree_visible.locker_m);

    mmSpinlock_Unlock(&p->instance_locker);
}

MM_EXPORT_DLL struct mmFrameTimer*
mmAdaptiveTimer_Schedule(
    struct mmAdaptiveTimer* p,
    const char* name,
    double frequency,
    mmUInt8_t background,
    mmFrameTimerUpdateFunc handle,
    void* u)
{
    struct mmFrameTimer* unit = NULL;
    struct mmAdaptiveTimerArgument argument;

    mmAdaptiveTimerArgument_Init(&argument);

    argument.name = name;
    argument.frequency = frequency;
    argument.background = background;
    argument.handle = handle;
    argument.u = u;

    unit = mmAdaptiveTimer_ScheduleArgument(p, &argument);

    mmAdaptiveTimerArgument_Destroy(&argument);

    return unit;
}

MM_EXPORT_DLL struct mmFrameTimer*
mmAdaptiveTimer_ScheduleArgument(
    struct mmAdaptiveTimer* p,
    struct mmAdaptiveTimerArgument* argument)
{
    struct mmFrameTimerCallback callback;
    struct mmFrameTimer* unit = NULL;
    struct mmString v;
    mmString_MakeWeaks(&v, argument->name);
    unit = mmAdaptiveTimer_GetInstance(p, &v);
    //
    callback.Update = argument->handle;
    callback.obj = argument->u;
    //
    mmFrameTimer_Lock(unit);

    mmFrameTimer_SetFrequency(unit, argument->frequency);
    mmFrameTimer_SetCallback(unit, &callback);
    mmFrameTimer_SetBackground(unit, argument->background);

    mmFrameTimer_Unlock(unit);
    //
    return unit;
}
MM_EXPORT_DLL void mmAdaptiveTimer_Cancel(struct mmAdaptiveTimer* p, struct mmFrameTimer* unit)
{
    mmAdaptiveTimer_Rmv(p, &unit->name);
}

MM_EXPORT_DLL void mmAdaptiveTimer_Update(struct mmAdaptiveTimer* p)
{
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->sleep_usec = 0;
    // note: 
    // do lock instance_locker here, not need, and will case dead lock some time.
    mmFrameStats_Update(&p->status);
    //
    mmRbtreeVisibleString_Traver(&p->rbtree_visible, &__static_mmAdaptiveTimer_VisibleHandleUpdate, p);
    //
    p->sleep_usec = (int)(p->max_idle_usec);
    p->sleep_usec = (0 > p->sleep_usec) ? 0 : p->sleep_usec;
    // [0, sleep_usec - 1] for little quick timewake.
    p->sleep_usec = (0 < p->sleep_usec) ? p->sleep_usec - 1 : p->sleep_usec;
}
MM_EXPORT_DLL void mmAdaptiveTimer_Timewait(struct mmAdaptiveTimer* p)
{
    p->nearby_time = p->sleep_usec;
    // timewait nearby.
    mmTimewait_USecNearby(&p->signal_cond, &p->signal_mutex, &p->ntime, &p->otime, p->nearby_time);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Timewake(struct mmAdaptiveTimer* p)
{
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmAdaptiveTimer_SetActive(struct mmAdaptiveTimer* p, const char* name, mmUInt8_t active)
{
    struct mmString v;
    mmString_MakeWeaks(&v, name);

    struct mmFrameTimer* unit = mmAdaptiveTimer_Get(p, &v);
    if (NULL != unit)
    {
        mmFrameTimer_Lock(unit);
        mmFrameTimer_SetActive(unit, active);
        mmFrameTimer_Unlock(unit);
    }
}
MM_EXPORT_DLL void mmAdaptiveTimer_SetChunkSize(struct mmAdaptiveTimer* p, size_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool_element, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Start(struct mmAdaptiveTimer* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
}
MM_EXPORT_DLL void mmAdaptiveTimer_Interrupt(struct mmAdaptiveTimer* p)
{
    p->state = MM_TS_CLOSED;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Shutdown(struct mmAdaptiveTimer* p)
{
    p->state = MM_TS_FINISH;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmAdaptiveTimer_Join(struct mmAdaptiveTimer* p)
{
    // do nothing here, mmAdaptiveTimer not thread context, just api full here.
}
MM_EXPORT_DLL void mmAdaptiveTimer_EnterBackground(struct mmAdaptiveTimer* p)
{
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_BACKGROUND;
}
MM_EXPORT_DLL void mmAdaptiveTimer_EnterForeground(struct mmAdaptiveTimer* p)
{
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
}
MM_EXPORT_DLL void mmAdaptiveTimer_LoggerFrameStatus(struct mmAdaptiveTimer* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // note: 
    // do lock instance_locker here, not need, and will case dead lock some time.
    //
    mmLogger_LogI(gLogger, "adaptive: status number: %" PRIu64 " average: %lf", p->status.number, p->status.average);
    //
    mmRbtreeVisibleString_Traver(&p->rbtree_visible, &__static_mmAdaptiveTimer_VisibleHandleLogger, p);
}

static void __static_mmAdaptiveTimer_VisibleHandleUpdate(void* obj, void* u, const struct mmString* k, void* v)
{
    struct mmAdaptiveTimer* p = (struct mmAdaptiveTimer*)(u);
    struct mmFrameTimer* unit = (struct mmFrameTimer*)(v);
    // note:
    // rbtree_visible traver callback we lock instance_locker outside, so the v is valid.
    // if at traver callback handle trigger other callback, do not lock the v.
    // if lock v before trigger other callback, some time will case dead lock at other callback inside.
    // although the v is valid but we can lock v at this function, but careful for it.
    mmFrameTimer_UpdateMotionStatus(unit, p->motion_status, &p->update_cost_time);
    // note:
    // although the v is valid but we can lock v at this function.
    mmFrameTimer_Lock(unit);
    p->max_idle_usec = (p->max_idle_usec > unit->wait_usec) ? unit->wait_usec : p->max_idle_usec;
    mmFrameTimer_Unlock(unit);
}
static void __static_mmAdaptiveTimer_VisibleHandleLogger(void* obj, void* u, const struct mmString* k, void* v)
{
    struct mmFrameTimer* unit = (struct mmFrameTimer*)(v);
    struct mmFrameStats* status = &unit->status;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    // note:
    // rbtree_visible traver callback we lock instance_locker outside, so the v is valid.
    // if at traver callback handle trigger other callback, do not lock the v.
    // if lock v before trigger other callback, some time will case dead lock at other callback inside.
    // although the v is valid but we can lock v at this function, but careful for it.
    mmFrameTimer_Lock(unit);
    mmLogger_LogI(gLogger, "adaptive: average: %lf number: %" PRIu64 " %s", status->average, status->number, mmString_CStr(&unit->name));
    mmFrameTimer_Unlock(unit);
}

