/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMetadata.h"

MM_EXPORT_DLL
void
mmMetadataHandleArgs0(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         f)
{
    do
    {
        typedef void(*FuncType)(void* p);
        void* metadata;
        void* handler;
        void* func;
        if (NULL == p)
        {
            // object is NULL, do nothing.
            break;
        }
        metadata = (void*)(*((mmUIntptr_t*)((char*)p + m)));
        assert(NULL != metadata && "metadata is invalid.");
        handler = (void*)(*((mmUIntptr_t*)((char*)metadata + h)));
        if (NULL == handler)
        {
            // handler is NULL, do nothing.
            break;
        }
        func = (void*)(*((mmUIntptr_t*)((char*)handler + f)));
        assert(NULL != func && "func is invalid.");
        (*((FuncType)(func)))(p);
    } while (0);
}

MM_EXPORT_DLL
void
mmMetadataHandleArgs1(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         f,
    void*                                          a)
{
    do
    {
        typedef void(*FuncType)(void* p, void* a);
        void* metadata;
        void* handler;
        void* func;
        if (NULL == p)
        {
            // object is NULL, do nothing.
            break;
        }
        metadata = (void*)(*((mmUIntptr_t*)((char*)p + m)));
        assert(NULL != metadata && "metadata is invalid.");
        handler = (void*)(*((mmUIntptr_t*)((char*)metadata + h)));
        if (NULL == handler)
        {
            // handler is NULL, do nothing.
            break;
        }
        func = (void*)(*((mmUIntptr_t*)((char*)handler + f)));
        assert(NULL != func && "func is invalid.");
        (*((FuncType)(func)))(p, a);
    } while (0);
}

MM_EXPORT_DLL
void
mmMetadataHandleGroupArgs0(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         g,
    size_t                                         f)
{
    do
    {
        typedef void(*FuncType)(void* p);
        void* metadata;
        void* handler;
        void* group;
        void* func;
        if (NULL == p)
        {
            // object is NULL, do nothing.
            break;
        }
        metadata = (void*)(*((mmUIntptr_t*)((char*)p + m)));
        assert(NULL != metadata && "metadata is invalid.");
        handler = (void*)(*((mmUIntptr_t*)((char*)metadata + h)));
        if (NULL == handler)
        {
            // handler is NULL, do nothing.
            break;
        }
        group = (void*)(*((mmUIntptr_t*)((char*)handler + g)));
        if (NULL == group)
        {
            // group is NULL, do nothing.
            break;
        }
        func = (void*)(*((mmUIntptr_t*)((char*)group + f)));
        assert(NULL != func && "func is invalid.");
        (*((FuncType)(func)))(p);
    } while (0);
}

MM_EXPORT_DLL
void
mmMetadataHandleGroupArgs1(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         g,
    size_t                                         f,
    void*                                          a)
{
    do
    {
        typedef void(*FuncType)(void* p, void* a);
        void* metadata;
        void* handler;
        void* group;
        void* func;
        if (NULL == p)
        {
            // object is NULL, do nothing.
            break;
        }
        metadata = (void*)(*((mmUIntptr_t*)((char*)p + m)));
        assert(NULL != metadata && "metadata is invalid.");
        handler = (void*)(*((mmUIntptr_t*)((char*)metadata + h)));
        if (NULL == handler)
        {
            // handler is NULL, do nothing.
            break;
        }
        group = (void*)(*((mmUIntptr_t*)((char*)handler + g)));
        if (NULL == group)
        {
            // group is NULL, do nothing.
            break;
        }
        func = (void*)(*((mmUIntptr_t*)((char*)group + f)));
        assert(NULL != func && "func is invalid.");
        (*((FuncType)(func)))(p, a);
    } while (0);
}

MM_EXPORT_DLL
void
mmMetadataDefaultArgs0(
    void*                                          p)
{
    // do nothing here, interface api.
}

MM_EXPORT_DLL
void
mmMetadataDefaultArgs1(
    void*                                          p,
    void*                                          a)
{
    // do nothing here, interface api.
}

MM_EXPORT_DLL
void
mmMetadataDefaultArgs1Int(
    void*                                          p,
    int                                            a)
{
    // do nothing here, interface api.
}
