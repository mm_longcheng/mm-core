/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAdaptiveTimer_h__
#define __mmAdaptiveTimer_h__

#include "core/mmCore.h"
#include "core/mmThread.h"
#include "core/mmString.h"
#include "core/mmTime.h"
#include "core/mmTimeval.h"
#include "core/mmTimeCache.h"
#include "core/mmFrameStats.h"
#include "core/mmPoolElement.h"
#include "core/mmFrameTimer.h"

#include "container/mmRbtreeString.h"
#include "container/mmRbtreeVisibleString.h"

#include <pthread.h>

#include "core/mmPrefix.h"

#define MM_ADAPTIVE_TIMER_CHUNK_SIZE_DEFAULT 64

struct mmAdaptiveTimerArgument
{
    const char* name;
    double frequency;
    mmUInt8_t background;
    mmFrameTimerUpdateFunc handle;
    void* u;
};
MM_EXPORT_DLL void mmAdaptiveTimerArgument_Init(struct mmAdaptiveTimerArgument* p);
MM_EXPORT_DLL void mmAdaptiveTimerArgument_Destroy(struct mmAdaptiveTimerArgument* p);
MM_EXPORT_DLL void mmAdaptiveTimerArgument_Reset(struct mmAdaptiveTimerArgument* p);

struct mmAdaptiveTimer
{
    struct mmRbtreeVisibleString rbtree_visible;
    struct mmFrameStats status;
    struct mmPoolElement pool_element;

    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;

    struct timeval ntime;
    struct timespec otime;

    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t locker;

    double update_cost_time;
    double max_idle_usec;
    int sleep_usec;

    mmULong_t nearby_time;

    // mmFrameTimerMotionStatus_t, default is MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND(0)
    mmSInt8_t motion_status;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;

    // user data.
    void* u;
};

MM_EXPORT_DLL void mmAdaptiveTimer_Init(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Destroy(struct mmAdaptiveTimer* p);

/* locker order is
*     pool_element
*     locker
*/
MM_EXPORT_DLL void mmAdaptiveTimer_Lock(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Unlock(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_SetContext(struct mmAdaptiveTimer* p, void* u);
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_Add(struct mmAdaptiveTimer* p, const struct mmString* name);
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_Get(struct mmAdaptiveTimer* p, const struct mmString* name);
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_GetInstance(struct mmAdaptiveTimer* p, const struct mmString* name);
MM_EXPORT_DLL struct mmFrameTimer* mmAdaptiveTimer_GetByCString(struct mmAdaptiveTimer* p, const char* name);
MM_EXPORT_DLL void mmAdaptiveTimer_Rmv(struct mmAdaptiveTimer* p, const struct mmString* name);
MM_EXPORT_DLL void mmAdaptiveTimer_Clear(struct mmAdaptiveTimer* p);

MM_EXPORT_DLL struct mmFrameTimer*
mmAdaptiveTimer_Schedule(
    struct mmAdaptiveTimer* p,
    const char* name,
    double frequency,
    mmUInt8_t background,
    mmFrameTimerUpdateFunc handle,
    void* u);

MM_EXPORT_DLL struct mmFrameTimer*
mmAdaptiveTimer_ScheduleArgument(
    struct mmAdaptiveTimer* p,
    struct mmAdaptiveTimerArgument* argument);

MM_EXPORT_DLL void mmAdaptiveTimer_Cancel(struct mmAdaptiveTimer* p, struct mmFrameTimer* unit);
MM_EXPORT_DLL void mmAdaptiveTimer_Update(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Timewait(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Timewake(struct mmAdaptiveTimer* p);

MM_EXPORT_DLL void mmAdaptiveTimer_SetActive(struct mmAdaptiveTimer* p, const char* name, mmUInt8_t active);
MM_EXPORT_DLL void mmAdaptiveTimer_SetChunkSize(struct mmAdaptiveTimer* p, size_t chunk_size);

MM_EXPORT_DLL void mmAdaptiveTimer_Start(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Interrupt(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Shutdown(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_Join(struct mmAdaptiveTimer* p);

MM_EXPORT_DLL void mmAdaptiveTimer_EnterBackground(struct mmAdaptiveTimer* p);
MM_EXPORT_DLL void mmAdaptiveTimer_EnterForeground(struct mmAdaptiveTimer* p);

MM_EXPORT_DLL void mmAdaptiveTimer_LoggerFrameStatus(struct mmAdaptiveTimer* p);

#include "core/mmSuffix.h"

#endif//__mmAdaptiveTimer_h__
