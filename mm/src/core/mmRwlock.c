/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
 * Copyright (C) Ruslan Ermilov
 * Copyright (C) Nginx, Inc.
 */


#include "mmRwlock.h"
#include "core/mmOSContext.h"
#include "core/mmProcess.h"

#if (MM_HAVE_ATOMIC_OPS)


#define MM_RWLOCK_SPIN   2048
#define MM_RWLOCK_WLOCK  ((mmAtomicUInt_t) -1)


MM_EXPORT_DLL void mmRwlock_Wlock(mmAtomic_t *lock)
{
    mmUInt_t  i, n;

    mmUInt32_t cores = mmOSContextCPUCoresNumber();

    for (;;)
    {

        if (*lock == 0 && mmAtomicCmpSet(lock, 0, MM_RWLOCK_WLOCK))
        {
            return;
        }

        if (cores > 1)
        {

            for (n = 1; n < MM_RWLOCK_SPIN; n <<= 1)
            {

                for (i = 0; i < n; i++)
                {
                    mmCpuPause();
                }

                if (*lock == 0 && mmAtomicCmpSet(lock, 0, MM_RWLOCK_WLOCK))
                {
                    return;
                }
            }
        }

        mmSchedYield();
    }
}


MM_EXPORT_DLL void mmRwlock_Rlock(mmAtomic_t *lock)
{
    mmUInt_t         i, n;
    mmAtomicUInt_t  readers;

    mmUInt32_t cores = mmOSContextCPUCoresNumber();

    for (;;)
    {
        readers = *lock;

        if (readers != MM_RWLOCK_WLOCK && mmAtomicCmpSet(lock, readers, readers + 1))
        {
            return;
        }

        if (cores > 1)
        {

            for (n = 1; n < MM_RWLOCK_SPIN; n <<= 1)
            {

                for (i = 0; i < n; i++)
                {
                    mmCpuPause();
                }

                readers = *lock;

                if (readers != MM_RWLOCK_WLOCK && mmAtomicCmpSet(lock, readers, readers + 1))
                {
                    return;
                }
            }
        }

        mmSchedYield();
    }
}


MM_EXPORT_DLL void mmRwlock_Unlock(mmAtomic_t *lock)
{
    mmAtomicUInt_t  readers;

    readers = *lock;

    if (readers == MM_RWLOCK_WLOCK)
    {
        (void)mmAtomicCmpSet(lock, MM_RWLOCK_WLOCK, 0);
        return;
    }

    for (;; )
    {

        if (mmAtomicCmpSet(lock, readers, readers - 1))
        {
            return;
        }

        readers = *lock;
    }
}


MM_EXPORT_DLL void mmRwlock_Downgrade(mmAtomic_t *lock)
{
    if (*lock == MM_RWLOCK_WLOCK)
    {
        *lock = 1;
    }
}


#else

#if (MM_HTTP_UPSTREAM_ZONE || MM_STREAM_UPSTREAM_ZONE)

#error mmAtomicCmpSet() is not defined!

#endif

#endif
