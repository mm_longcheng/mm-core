/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBase128Varints.h"
#include "core/mmString.h"

/* Return the number of bytes required to store the
tag for the field (which includes 3 bits for
the wire-type, and a single bit that denotes the end-of-tag. */
MM_EXPORT_DLL size_t mmBase128_GetTagSize(unsigned number)
{
    if (number < (1 << 4))
        return 1;
    else if (number < (1 << 11))
        return 2;
    else if (number < (1 << 18))
        return 3;
    else if (number < (1 << 25))
        return 4;
    else
        return 5;
}

/* Return the number of bytes required to store
a variable-length unsigned integer that fits in 32-bit uint
in base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_UInt32Size(unsigned int v)
{
    if (v < (1 << 7))
        return 1;
    else if (v < (1 << 14))
        return 2;
    else if (v < (1 << 21))
        return 3;
    else if (v < (1 << 28))
        return 4;
    else
        return 5;
}

/* Return the number of bytes required to store
a variable-length signed integer that fits in 32-bit int
in base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_Int32Size(int v)
{
    if (v < 0)
        return 10;
    else if (v < (1 << 7))
        return 1;
    else if (v < (1 << 14))
        return 2;
    else if (v < (1 << 21))
        return 3;
    else if (v < (1 << 28))
        return 4;
    else
        return 5;
}

/* return the zigzag-encoded 32-bit unsigned int from a 32-bit signed int */
MM_EXPORT_DLL unsigned int mmBase128_Zigzag32(int v)
{
    if (v < 0)
        return ((unsigned int)(-v)) * 2 - 1;
    else
        return v * 2;
}

/* Return the number of bytes required to store
a variable-length signed integer that fits in 32-bit int,
converted to unsigned via the zig-zag algorithm,
then packed using base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_SInt32Size(int v)
{
    return mmBase128_UInt32Size(mmBase128_Zigzag32(v));
}

/* Return the number of bytes required to store
a variable-length unsigned integer that fits in 64-bit uint
in base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_UInt64Size(unsigned long long int v)
{
    unsigned int upper_v = (unsigned int)(v >> 32);
    if (upper_v == 0)
        return mmBase128_UInt32Size((unsigned int)v);
    else if (upper_v < (1 << 3))
        return 5;
    else if (upper_v < (1 << 10))
        return 6;
    else if (upper_v < (1 << 17))
        return 7;
    else if (upper_v < (1 << 24))
        return 8;
    else if (upper_v < (1U << 31))
        return 9;
    else
        return 10;
}

/* return the zigzag-encoded 64-bit unsigned int from a 64-bit signed int */
MM_EXPORT_DLL unsigned long long int mmBase128_Zigzag64(long long int v)
{
    if (v < 0)
        return ((unsigned long long int)(-v)) * 2 - 1;
    else
        return v * 2;
}

/* Return the number of bytes required to store
a variable-length signed integer that fits in 64-bit int,
converted to unsigned via the zig-zag algorithm,
then packed using base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_SInt64Size(long long int v)
{
    return mmBase128_UInt64Size(mmBase128_Zigzag64(v));
}

/* === pack() === */
/* Pack an unsigned 32-bit integer in base-128 encoding, and return the number of bytes needed:
this will be 5 or less. */
MM_EXPORT_DLL size_t mmBase128_UInt32Pack(unsigned int value, unsigned char *out)
{
    unsigned rv = 0;
    if (value >= 0x80)
    {
        out[rv++] = value | 0x80;
        value >>= 7;
        if (value >= 0x80)
        {
            out[rv++] = value | 0x80;
            value >>= 7;
            if (value >= 0x80)
            {
                out[rv++] = value | 0x80;
                value >>= 7;
                if (value >= 0x80)
                {
                    out[rv++] = value | 0x80;
                    value >>= 7;
                }
            }
        }
    }
    /* assert: value<128 */
    out[rv++] = value;
    return rv;
}

/* Pack a 32-bit signed integer, returning the number of bytes needed.
Negative numbers are packed as twos-complement 64-bit integers. */
MM_EXPORT_DLL size_t mmBase128_Int32Pack(int value, unsigned char *out)
{
    if (value < 0)
    {
        out[0] = value | 0x80;
        out[1] = (value >> 7) | 0x80;
        out[2] = (value >> 14) | 0x80;
        out[3] = (value >> 21) | 0x80;
        out[4] = (value >> 28) | 0x80;
        out[5] = out[6] = out[7] = out[8] = 0xff;
        out[9] = 0x01;
        return 10;
    }
    else
        return mmBase128_UInt32Pack(value, out);
}

/* Pack a 32-bit integer in zigwag encoding. */
MM_EXPORT_DLL size_t mmBase128_SInt32Pack(int value, unsigned char *out)
{
    return mmBase128_UInt32Pack(mmBase128_Zigzag32(value), out);
}

/* Pack a 64-bit unsigned integer that fits in a 64-bit uint,
using base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_UInt64Pack(unsigned long long int value, unsigned char *out)
{
    unsigned int hi = (unsigned int)(value >> 32);
    unsigned int lo = (unsigned int)value;
    unsigned rv;
    if (hi == 0)
        return mmBase128_UInt32Pack((unsigned int)lo, out);
    out[0] = (lo) | 0x80;
    out[1] = (lo >> 7) | 0x80;
    out[2] = (lo >> 14) | 0x80;
    out[3] = (lo >> 21) | 0x80;
    if (hi < 8)
    {
        out[4] = (hi << 4) | (lo >> 28);
        return 5;
    }
    else
    {
        out[4] = ((hi & 7) << 4) | (lo >> 28) | 0x80;
        hi >>= 3;
    }
    rv = 5;
    while (hi >= 128)
    {
        out[rv++] = hi | 0x80;
        hi >>= 7;
    }
    out[rv++] = hi;
    return rv;
}

/* Pack a 64-bit signed integer in zigzan encoding,
return the size of the packed output.
(Max returned value is 10) */
MM_EXPORT_DLL size_t mmBase128_SInt64Pack(long long int value, unsigned char *out)
{
    return mmBase128_UInt64Pack(mmBase128_Zigzag64(value), out);
}

/* Pack a boolean as 0 or 1, even though the int
can really assume any integer value. */
/* XXX: perhaps on some platforms "*out = !!value" would be
a better impl, b/c that is idiotmatic c++ in some stl impls. */
MM_EXPORT_DLL size_t mmBase128_BooleanPack(int value, unsigned char *out)
{
    *out = value ? 1 : 0;
    return 1;
}

/* wire-type will be added in required_field_pack() */
/* XXX: just call uint64_pack on 64-bit platforms. */
MM_EXPORT_DLL size_t mmBase128_TagPack(unsigned int id, unsigned char *out)
{
    if (id < (1 << (32 - 3)))
        return mmBase128_UInt32Pack(id << 3, out);
    else
        return mmBase128_UInt64Pack(((unsigned long long int)id) << 3, out);
}

/* max base-128_numbers */
MM_EXPORT_DLL size_t mmBase128_MaxB128Numbers(size_t len, const unsigned char *data)
{
    size_t rv = 0;
    while (len--)
        if ((*data++ & 0x80) == 0)
            ++rv;
    return rv;
}

/* base-128 varint buffer size */
MM_EXPORT_DLL unsigned mmBase128_B128VarintBufferSize(size_t length, const unsigned char *data)
{
    unsigned len = 0;

    unsigned max_len = (unsigned)(length < 10 ? length : 10);
    unsigned i;

    for (i = 0; i < max_len; i++)
    {
        if ((data[i] & 0x80) == 0)
        {
            // base-128 varint last byte.
            break;
        }
    }

    assert(i != max_len && "unterminated varint buffer.");

    len = i + 1;

    return len;
}
/* Unpack a 32-bit unsigned zigzag */
MM_EXPORT_DLL int mmBase128_Unzigzag32(unsigned int v)
{
    if (v & 1)
        // warning 4146
        // return -((v >> 1) - 1;
        return -((int)(v >> 1)) - 1;
    else
        return v >> 1;
}
/* Unpack a 64-bit unsigned zigzag */
MM_EXPORT_DLL long long int mmBase128_Unzigzag64(unsigned long long int v)
{
    if (v & 1)
        // warning 4146
        // return -((v >> 1) - 1;
        return -((int)(v >> 1)) - 1;
    else
        return v >> 1;
}

/* Unpack a 32-bit unsigned integer */
MM_EXPORT_DLL unsigned int mmBase128_ParseUInt32(unsigned len, const unsigned char *data)
{
    unsigned rv = data[0] & 0x7f;
    if (len > 1)
    {
        rv |= ((data[1] & 0x7f) << 7);
        if (len > 2)
        {
            rv |= ((data[2] & 0x7f) << 14);
            if (len > 3)
            {
                rv |= ((data[3] & 0x7f) << 21);
                if (len > 4)
                    rv |= (data[4] << 28);
            }
        }
    }
    return rv;
}
/* Unpack a 32-bit signed integer */
MM_EXPORT_DLL int mmBase128_ParseSInt32(unsigned len, const unsigned char *data)
{
    return mmBase128_Unzigzag32(mmBase128_ParseUInt32(len, data));
}
/* Unpack a 32-bit signed integer */
MM_EXPORT_DLL unsigned int mmBase128_ParseInt32(unsigned len, const unsigned char *data)
{
    return mmBase128_ParseUInt32(len, data);
}
/* Unpack a 64-bit unsigned integer */
MM_EXPORT_DLL unsigned long long int mmBase128_ParseUInt64(unsigned len, const unsigned char *data)
{
    unsigned shift, i;
    unsigned long long int rv;
    if (len < 5)
        return mmBase128_ParseUInt32(len, data);
    rv = ((data[0] & 0x7f))
        | ((data[1] & 0x7f) << 7)
        | ((data[2] & 0x7f) << 14)
        | ((data[3] & 0x7f) << 21);
    shift = 28;
    for (i = 4; i < len; i++)
    {
        rv |= (((unsigned long long int)(data[i] & 0x7f)) << shift);
        shift += 7;
    }
    return rv;
}
/* Unpack a 64-bit signed integer */
MM_EXPORT_DLL long long int mmBase128_ParseSInt64(unsigned len, const unsigned char *data)
{
    return mmBase128_Unzigzag64(mmBase128_ParseUInt64(len, data));
}

/* Unpack a boolean */
MM_EXPORT_DLL int mmBase128_ParseBoolean(unsigned len, const unsigned char *data)
{
    unsigned i;
    for (i = 0; i < len; i++)
        if (data[i] & 0x7f)
            return 1;
    return 0;
}

/* Scan varint */
MM_EXPORT_DLL unsigned mmBase128_ScanVarint(unsigned len, const unsigned char *data)
{
    unsigned i;
    if (len > 10)
        len = 10;
    for (i = 0; i < len; i++)
        if ((data[i] & 0x80) == 0)
            break;
    if (i == len)
        return 0;
    return i + 1;
}

/* === encode() === */

/* uint32 encode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt32Encode(mmUInt32_t* v, struct mmByteBuffer* byte_buffer, size_t* offset)
{
    *offset += mmBase128_UInt32Pack(*v, (unsigned char*)(byte_buffer->buffer + byte_buffer->offset + (*offset)));
}

/* uint64 encode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt64Encode(mmUInt64_t* v, struct mmByteBuffer* byte_buffer, size_t* offset)
{
    *offset += mmBase128_UInt64Pack(*v, (unsigned char*)(byte_buffer->buffer + byte_buffer->offset + (*offset)));
}

/* === decode() === */

/* uint32 decode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt32Decode(mmUInt32_t* v, struct mmByteBuffer* byte_buffer, size_t* offset)
{
    unsigned len = 0;
    const unsigned char* data = (const unsigned char*)(byte_buffer->buffer + byte_buffer->offset + (*offset));
    len = mmBase128_B128VarintBufferSize(byte_buffer->length - (*offset), data);
    *v = mmBase128_ParseUInt32(len, data);
    (*offset) += len;
}

/* uint64 decode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt64Decode(mmUInt64_t* v, struct mmByteBuffer* byte_buffer, size_t* offset)
{
    unsigned len = 0;
    const unsigned char* data = (const unsigned char*)(byte_buffer->buffer + byte_buffer->offset + (*offset));
    len = mmBase128_B128VarintBufferSize(byte_buffer->length - (*offset), data);
    *v = mmBase128_ParseUInt64(len, data);
    (*offset) += len;
}
