/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimewait.h"
#include "core/mmTime.h"

MM_EXPORT_DLL int
mmTimewait_MSecNearby(
    pthread_cond_t* signal_cond,
    pthread_mutex_t* signal_mutex,
    struct timeval* ntime,
    struct timespec* otime,
    mmULong_t _nearby_time)
{
    int rt = 0;
    // some os pthread timedwait impl precision is low.
    // MM_MSEC_PER_SEC check can avoid some precision problem
    // but will extend the application shutdown time.
    if (0 == _nearby_time)
    {
        // next loop immediately.
        rt = 0;
    }
    else if (MM_MSEC_PER_SEC < _nearby_time)
    {
        // timedwait a while.
        mmULong_t _a = _nearby_time / MM_MSEC_PER_SEC;
        mmULong_t _b = _nearby_time % MM_MSEC_PER_SEC;
        mmGettimeofday(ntime, NULL);
        otime->tv_sec = (ntime->tv_sec + _a);
        otime->tv_nsec = (ntime->tv_usec + _b * MM_MSEC_PER_SEC) * MM_MSEC_PER_SEC;
        otime->tv_sec += (otime->tv_nsec / MM_NSEC_PER_SEC);
        otime->tv_nsec = (otime->tv_nsec % MM_NSEC_PER_SEC);
        //
        pthread_mutex_lock(signal_mutex);
        rt = pthread_cond_timedwait(signal_cond, signal_mutex, otime);
        pthread_mutex_unlock(signal_mutex);
    }
    else
    {
        // msleep a while.
        mmMSleep(_nearby_time);
        rt = 0;
    }
    return rt;
}

MM_EXPORT_DLL int
mmTimewait_USecNearby(
    pthread_cond_t* signal_cond,
    pthread_mutex_t* signal_mutex,
    struct timeval* ntime,
    struct timespec* otime,
    mmULong_t _nearby_time)
{
    int rt = 0;
    // some os pthread timedwait impl precision is low.
    // MM_MSEC_PER_SEC check can avoid some precision problem
    // but will extend the application shutdown time.
    if (0 == _nearby_time)
    {
        // next loop immediately.
        rt = 0;
    }
    else if (MM_USEC_PER_SEC < _nearby_time)
    {
        // timedwait a while.
        mmULong_t _a = _nearby_time / MM_USEC_PER_SEC;
        mmULong_t _b = _nearby_time % MM_USEC_PER_SEC;
        mmGettimeofday(ntime, NULL);
        otime->tv_sec = (ntime->tv_sec + _a);
        otime->tv_nsec = (ntime->tv_usec + _b) * MM_MSEC_PER_SEC;
        otime->tv_sec += (otime->tv_nsec / MM_NSEC_PER_SEC);
        otime->tv_nsec = (otime->tv_nsec % MM_NSEC_PER_SEC);
        //
        pthread_mutex_lock(signal_mutex);
        rt = pthread_cond_timedwait(signal_cond, signal_mutex, otime);
        pthread_mutex_unlock(signal_mutex);
    }
    else
    {
        // usleep a while.
        mmUSleep(_nearby_time);
        rt = 0;
    }
    return rt;
}

