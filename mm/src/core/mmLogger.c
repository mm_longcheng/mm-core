/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLogger.h"
#include "core/mmAlloc.h"

static struct mmLevelMark const_level_mark[] =
{
    { mmString_Make("U"), mmString_Make("unknown"), },
    { mmString_Make("F"), mmString_Make("fatal"  ), },
    { mmString_Make("C"), mmString_Make("crit"   ), },
    { mmString_Make("E"), mmString_Make("error"  ), },
    { mmString_Make("A"), mmString_Make("alert"  ), },
    { mmString_Make("W"), mmString_Make("warning"), },
    { mmString_Make("N"), mmString_Make("notice" ), },
    { mmString_Make("I"), mmString_Make("info"   ), },
    { mmString_Make("T"), mmString_Make("trace"  ), },
    { mmString_Make("D"), mmString_Make("debug"  ), },
    { mmString_Make("V"), mmString_Make("verbose"), },
};

MM_EXPORT_DLL const struct mmLevelMark* mmLoggerLevelMark(int lvl)
{
    if (MM_LOG_FATAL <= lvl && lvl <= MM_LOG_VERBOSE)
    {
        return &const_level_mark[lvl];
    }
    else
    {
        return &const_level_mark[MM_LOG_UNKNOWN];
    }
}
static struct mmLogger gLogger = { &mmLogger_LoggerMessagePrintf, NULL, };
MM_EXPORT_DLL struct mmLogger* mmLogger_Instance(void)
{
    return &gLogger;
}

MM_EXPORT_DLL void mmLogger_Init(struct mmLogger* p)
{
    p->LoggerMessage = &mmLogger_LoggerMessagePrintf;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmLogger_Destroy(struct mmLogger* p)
{
    p->LoggerMessage = &mmLogger_LoggerMessagePrintf;
    p->obj = NULL;
}
// assign callback.
MM_EXPORT_DLL void mmLogger_SetCallback(struct mmLogger* p, mmLoggerFunc callback, void* obj)
{
    p->LoggerMessage = callback;
    p->obj = obj;
}
MM_EXPORT_DLL void mmLogger_Message(struct mmLogger* p, mmUInt32_t lvl, const char* fmt, ...)
{
    struct mmString message_buffer;
    va_list ap;
    mmString_Init(&message_buffer);
    va_start(ap, fmt);
    mmString_Vsprintf(&message_buffer, fmt, ap);
    va_end(ap);
    if (NULL != p && NULL != p->LoggerMessage)
    {
        (*(p->LoggerMessage))(p, lvl, mmString_CStr(&message_buffer));
    }
    else
    {
        mmLogger_LoggerMessagePrintf(p, lvl, mmString_CStr(&message_buffer));
    }
    mmString_Destroy(&message_buffer);
}
// default callback.
MM_EXPORT_DLL void mmLogger_LoggerMessagePrintf(struct mmLogger* p, mmUInt32_t lvl, const char* message)
{
    mmPrintf("%s\n", message);
}
MM_EXPORT_DLL void mmLogger_SocketError(struct mmLogger* p, mmUInt32_t lvl, mmErr_t errcode)
{
    if (0 != errcode)
    {
        unsigned char error_info[SOCKET_ERROR_BUFFER_SIZE] = { 0 };
        mmStrError(errcode, error_info, SOCKET_ERROR_BUFFER_SIZE);
        mmLogger_Message(p, lvl, "errno:(%d) %s", errcode, error_info);
    }
}
