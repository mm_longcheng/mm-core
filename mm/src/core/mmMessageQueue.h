/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMessageQueue_h__
#define __mmMessageQueue_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmThread.h"
#include "core/mmPoolMessage.h"

#include "container/mmConcurrentQueue.h"
#include "container/mmRbtreeU32.h"

#include "core/mmPrefix.h"

// max poper number at one loop. the default main thread timer frequency is 1/20 = 50 ms.
// 2ms one message handle is suitable 50/2 = 25.
//
// draw frequency is 60, the net sampling rate some time is 60.
// we must pop all message at this case, so we need twice rate 120.
// when net need repaird packet the sampling rate some time is 120, twice rate 240.
//
// value -1, pop message until size is 0.
// 
// we use 240 default. 50 / 240 = 0.20833 ms = 208.33 us.
#define MM_MESSAGE_QUEUE_MAX_POPER_NUMBER 240

typedef void(*mmMessageQueueHandleFunc)(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
struct mmMessageQueueCallback
{
    mmMessageQueueHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmMessageQueueCallback_Init(struct mmMessageQueueCallback* p);
MM_EXPORT_DLL void mmMessageQueueCallback_Destroy(struct mmMessageQueueCallback* p);

// message queue.
struct mmMessageQueue
{
    struct mmPoolMessage pool_message;
    struct mmTwoLockQuque lock_queue;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    // value ref. queue callback.
    struct mmMessageQueueCallback callback;
    mmAtomic_t rbtree_locker;
    mmAtomic_t pool_locker;
    mmAtomic_t locker;
    // max pop to make sure current not wait long times.
    // default is MM_MESSAGE_QUEUE_MAX_POPER_NUMBER.
    size_t max_pop;
    // user data.
    void* u;
};

MM_EXPORT_DLL void mmMessageQueue_Init(struct mmMessageQueue* p);
MM_EXPORT_DLL void mmMessageQueue_Destroy(struct mmMessageQueue* p);

/* locker order is
*     pool_packet
*     locker
*/
MM_EXPORT_DLL void mmMessageQueue_Lock(struct mmMessageQueue* p);
MM_EXPORT_DLL void mmMessageQueue_Unlock(struct mmMessageQueue* p);

MM_EXPORT_DLL void mmMessageQueue_SetHandle(struct mmMessageQueue* p, mmUInt32_t id, mmMessageQueueHandleFunc callback);
MM_EXPORT_DLL void mmMessageQueue_SetQueueCallback(struct mmMessageQueue* p, const struct mmMessageQueueCallback* queue_callback);
MM_EXPORT_DLL void mmMessageQueue_SetMaxPoperNumber(struct mmMessageQueue* p, size_t max_pop);
// assign context handle.
MM_EXPORT_DLL void mmMessageQueue_SetContext(struct mmMessageQueue* p, void* u);

MM_EXPORT_DLL void mmMessageQueue_ClearHandleRbtree(struct mmMessageQueue* p);

MM_EXPORT_DLL int mmMessageQueue_IsEmpty(struct mmMessageQueue* p);

// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_DLL void mmMessageQueue_ThreadHandle(struct mmMessageQueue* p);
// delete all not handle pack at queue.
MM_EXPORT_DLL void mmMessageQueue_Dispose(struct mmMessageQueue* p);
// push a pack and context tp queue.
MM_EXPORT_DLL void mmMessageQueue_Push(struct mmMessageQueue* p, void* obj, mmUInt32_t mid, struct mmByteBuffer* byte_buffer);
// pop and trigger handle number.
MM_EXPORT_DLL void mmMessageQueue_PopNumber(struct mmMessageQueue* p, size_t number);
// pop and trigger handle entire.
MM_EXPORT_DLL void mmMessageQueue_PopEntire(struct mmMessageQueue* p);
// pop and trigger handle single.
MM_EXPORT_DLL void mmMessageQueue_PopSingle(struct mmMessageQueue* p);

MM_EXPORT_DLL struct mmPoolMessageElem* mmMessageQueue_Overdraft(struct mmMessageQueue* p, size_t length);
MM_EXPORT_DLL void mmMessageQueue_Repayment(struct mmMessageQueue* p, struct mmPoolMessageElem* message_elem);

#include "core/mmSuffix.h"

#endif//__mmMessageQueue_h__
