/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmParameters.h"
#include "core/mmLogger.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL void mmParametersOption_Init(struct mmParametersOption* p)
{
    mmString_Init(&p->name);
    mmString_Init(&p->snap);
    mmString_Init(&p->data);
}
MM_EXPORT_DLL void mmParametersOption_Destroy(struct mmParametersOption* p)
{
    mmString_Destroy(&p->name);
    mmString_Destroy(&p->snap);
    mmString_Destroy(&p->data);
}
MM_EXPORT_DLL void* mmParametersOption_RbtreeStringVptStrongAlloc(struct mmRbtreeStringVpt* p, const struct mmString* k)
{
    struct mmParametersOption* e = (struct mmParametersOption*)mmMalloc(sizeof(struct mmParametersOption));
    mmParametersOption_Init(e);
    return e;
}
MM_EXPORT_DLL void* mmParametersOption_RbtreeStringVptStrongRelax(struct mmRbtreeStringVpt* p, const struct mmString* k, void* v)
{
    struct mmParametersOption* e = (struct mmParametersOption*)(v);
    mmParametersOption_Destroy(e);
    mmFree(e);
    return NULL;
}

MM_EXPORT_DLL void mmParameters_Init(struct mmParameters* p)
{
    struct mmRbtreeStringVptAllocator _StringVptAllocator;
    mmRbtreeStringVpt_Init(&p->name_options);
    mmRbtreeStringVpt_Init(&p->snap_options);

    _StringVptAllocator.Produce = &mmParametersOption_RbtreeStringVptStrongAlloc;
    _StringVptAllocator.Recycle = &mmParametersOption_RbtreeStringVptStrongRelax;
    mmRbtreeStringVpt_SetAllocator(&p->name_options, &_StringVptAllocator);

    _StringVptAllocator.Produce = &mmRbtreeStringVpt_WeakProduce;
    _StringVptAllocator.Recycle = &mmRbtreeStringVpt_WeakRecycle;
    mmRbtreeStringVpt_SetAllocator(&p->snap_options, &_StringVptAllocator);
}
MM_EXPORT_DLL void mmParameters_Destroy(struct mmParameters* p)
{
    mmRbtreeStringVpt_Destroy(&p->name_options);
    mmRbtreeStringVpt_Destroy(&p->snap_options);
}

MM_EXPORT_DLL void mmParameters_SetNameSnap(struct mmParameters* p, const char* name, const char* snap)
{
    struct mmParametersOption* option = NULL;
    struct mmString name_str;
    struct mmString snap_str;
    mmString_MakeWeaks(&name_str, name);
    mmString_MakeWeaks(&snap_str, snap);
    option = (struct mmParametersOption*)mmRbtreeStringVpt_GetInstance(&p->name_options, &name_str);
    mmRbtreeStringVpt_Set(&p->snap_options, &snap_str, option);
    mmString_Assigns(&option->name, name);
    mmString_Assigns(&option->snap, snap);
}

// --name=mm -n=mm
MM_EXPORT_DLL void mmParameters_SetOption(struct mmParameters* p, const char* option)
{
    // 128 buffer for memory alignment.
    static const size_t acl = 128;
    char buffer[128] = { 0 };
    size_t len = mmStrlen(option);
    size_t min_len = len < acl ? len : acl;
    size_t i = 0;
    mmMemcpy(buffer, option, min_len);
    //
    if (0 < min_len)
    {
        // --
        if (2 < min_len && '-' == buffer[0])
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            struct mmParametersOption* parameters_option = NULL;
            //
            if ('-' == buffer[1])
            {
                // --name=mm
                struct mmString name;
                mmString_Init(&name);
                for (i = 2; i < min_len; ++i)
                {
                    if ('=' == buffer[i])
                    {
                        mmString_Assignsn(&name, buffer + 2, i - 2);
                        parameters_option = (struct mmParametersOption*)mmRbtreeStringVpt_Get(&p->name_options, &name);
                        if (NULL != parameters_option)
                        {
                            mmString_Assignsn(&parameters_option->data, buffer + i + 1, min_len - i);
                        }
                        else
                        {
                            mmLogger_LogW(gLogger, "%s %d unknown option name:%s", __FUNCTION__, __LINE__, mmString_CStr(&name));
                        }
                    }
                }
                mmString_Destroy(&name);
            }
            else
            {
                // -n=mm
                struct mmString snap;
                mmString_Init(&snap);
                for (i = 1; i < min_len; ++i)
                {
                    if ('=' == buffer[i])
                    {
                        mmString_Assignsn(&snap, buffer + 1, i - 1);
                        parameters_option = (struct mmParametersOption*)mmRbtreeStringVpt_Get(&p->snap_options, &snap);
                        if (NULL != parameters_option)
                        {
                            mmString_Assignsn(&parameters_option->data, buffer + i + 1, min_len - i);
                        }
                        else
                        {
                            mmLogger_LogW(gLogger, "%s %d unknown option snap:%s", __FUNCTION__, __LINE__, mmString_CStr(&snap));
                        }
                    }
                }
                mmString_Destroy(&snap);
            }
        }
    }
}
MM_EXPORT_DLL void mmParameters_SetArgument(struct mmParameters* p, const struct mmArgument* argument)
{
    int i = 0;
    for (i = 0; i < argument->argc; ++i)
    {
        mmParameters_SetOption(p, argument->argv[i]);
    }
}

MM_EXPORT_DLL struct mmParametersOption* mmParameters_GetInstanceOptionByName(struct mmParameters* p, const char* name)
{
    struct mmString name_str;
    mmString_MakeWeaks(&name_str, name);
    return (struct mmParametersOption*)mmRbtreeStringVpt_Get(&p->name_options, &name_str);
}
MM_EXPORT_DLL struct mmParametersOption* mmParameters_GetInstanceOptionBySnap(struct mmParameters* p, const char* snap)
{
    struct mmString snap_str;
    mmString_MakeWeaks(&snap_str, snap);
    return (struct mmParametersOption*)mmRbtreeStringVpt_Get(&p->snap_options, &snap_str);
}

// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
MM_EXPORT_DLL void
mmParameters_Server(
    const char* parameters,
    char node[MM_PARAMETERS_NODE_NAME_LENGTH],
    mmUShort_t* port,
    mmUInt32_t* length)
{
    // MM_PARAMETERS_NODE_NAME_LENGTH + 3 + 2 * MM_INT32_LEN = 89;
    // 128 buffer for memory alignment.
    static const size_t acl = 128;
    char buffer[128] = { 0 };
    size_t len = mmStrlen(parameters);
    size_t min_len = len < acl ? len : acl;
    size_t i = 0;
    mmMemcpy(buffer, parameters, min_len);
    if (0 < min_len)
    {
        for (i = min_len - 1; 0 < i; --i)
        {
            if ('(' == buffer[i] || ')' == buffer[i] || '[' == buffer[i] || ']' == buffer[i])
            {
                buffer[i] = ' ';
            }
            if ('-' == buffer[i])
            {
                buffer[i] = ' ';
                break;
            }
        }
    }
    mmSscanf(buffer, "%s %" SCNu16 " %u", node, port, length);
}
// 192.168.111.123-65535
MM_EXPORT_DLL void
mmParameters_Client(
    const char* parameters,
    char node[MM_PARAMETERS_NODE_NAME_LENGTH],
    mmUShort_t* port)
{
    // MM_PARAMETERS_NODE_NAME_LENGTH + 1 + MM_INT32_LEN = 76;
    // 128 buffer for memory alignment.
    static const size_t acl = 128;
    char buffer[128] = { 0 };
    size_t len = mmStrlen(parameters);
    size_t min_len = len < acl ? len : acl;
    size_t i = 0;
    mmMemcpy(buffer, parameters, min_len);
    if (0 < min_len)
    {
        for (i = min_len - 1; 0 < i; --i)
        {
            if ('-' == buffer[i])
            {
                buffer[i] = ' ';
                break;
            }
        }
    }
    mmSscanf(buffer, "%s %" SCNu16 "", node, port);
}
