/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPropertySet_h__
#define __mmPropertySet_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtreeString.h"

#include "core/mmPrefix.h"

struct mmProperty;

struct mmPropertyHelper
{
    void(*SetValue)(
        const struct mmProperty*                   p, 
        void*                                      obj, 
        const struct mmString*                     pValue);

    void(*GetValue)(
        const struct mmProperty*                   p, 
        const void*                                obj, 
        struct mmString*                           pValue);
};

struct mmProperty
{
    const struct mmString hName;
    const struct mmString hHelp;
    const struct mmString hDefault;
    const struct mmString hOrigin;
    const struct mmPropertyHelper hHelper;
    size_t hBaseOffset;
    size_t hMemberOffset;
    size_t hMemberLength;
    void* pSetterFunc;
    void* pGetterFunc;
};

MM_EXPORT_DLL 
void 
mmProperty_SetValueMember(
    const struct mmProperty*                       p, 
    void*                                          obj, 
    const void*                                    pValue);

MM_EXPORT_DLL 
void 
mmProperty_GetValueMember(
    const struct mmProperty*                       p, 
    const void*                                    obj, 
    void*                                          pValue);

MM_EXPORT_DLL 
void 
mmProperty_SetValueDirect(
    const struct mmProperty*                       p, 
    void*                                          obj, 
    const void*                                    pValue);

MM_EXPORT_DLL 
void 
mmProperty_GetValueDirect(
    const struct mmProperty*                       p, 
    const void*                                    obj, 
    void*                                          pValue);

MM_EXPORT_DLL 
void 
mmProperty_SetValueString(
    const struct mmProperty*                       p, 
    void*                                          obj, 
    const struct mmString*                         pValue);

MM_EXPORT_DLL 
void 
mmProperty_GetValueString(
    const struct mmProperty*                       p, 
    const void*                                    obj, 
    struct mmString*                               pValue);

MM_EXPORT_DLL
void
mmProperty_SetPointerDirect(
    const struct mmProperty*                       p,
    void*                                          obj,
    const void*                                    ptr);

MM_EXPORT_DLL
void*
mmProperty_GetPointerDirect(
    const struct mmProperty*                       p,
    const void*                                    obj);

MM_EXPORT_DLL extern const struct mmProperty mmPropertyDefault;

struct mmPropertySet
{
    struct mmRbtreeWeakStringVpt rbtree;
    void* obj;
};

MM_EXPORT_DLL 
void 
mmPropertySet_Init(
    struct mmPropertySet*                          p);

MM_EXPORT_DLL 
void 
mmPropertySet_Destroy(
    struct mmPropertySet*                          p);

MM_EXPORT_DLL 
void 
mmPropertySet_SetObject(
    struct mmPropertySet*                          p, 
    void*                                          obj);

MM_EXPORT_DLL 
void 
mmPropertySet_AddProperty(
    struct mmPropertySet*                          p, 
    const struct mmProperty*                       pProperty);

MM_EXPORT_DLL 
void 
mmPropertySet_RmvProperty(
    struct mmPropertySet*                          p, 
    const struct mmProperty*                       pProperty);

MM_EXPORT_DLL 
void 
mmPropertySet_RmvPropertyByName(
    struct mmPropertySet*                          p, 
    const char*                                    pName);

MM_EXPORT_DLL 
void 
mmPropertySet_ClearProperty(
    struct mmPropertySet*                          p);

MM_EXPORT_DLL 
const struct mmProperty* 
mmPropertySet_GetProperty(
    const struct mmPropertySet*                    p, 
    const char*                                    pName);

MM_EXPORT_DLL 
int 
mmPropertySet_GetIsPropertyExists(
    const struct mmPropertySet*                    p, 
    const char*                                    pName);

MM_EXPORT_DLL 
void 
mmPropertySet_SetValueMember(
    struct mmPropertySet*                          p, 
    const char*                                    pName, 
    const void*                                    pValue);

MM_EXPORT_DLL 
void 
mmPropertySet_GetValueMember(
    const struct mmPropertySet*                    p, 
    const char*                                    pName, 
    void*                                          pValue);

MM_EXPORT_DLL 
void 
mmPropertySet_SetValueDirect(
    struct mmPropertySet*                          p, 
    const char*                                    pName, 
    const void*                                    pValue);

MM_EXPORT_DLL 
void 
mmPropertySet_GetValueDirect(
    const struct mmPropertySet*                    p, 
    const char*                                    pName, 
    void*                                          pValue);

MM_EXPORT_DLL 
void 
mmPropertySet_SetValueString(
    struct mmPropertySet*                          p, 
    const char*                                    pName, 
    const struct mmString*                         pValue);

MM_EXPORT_DLL 
void 
mmPropertySet_GetValueString(
    const struct mmPropertySet*                    p, 
    const char*                                    pName, 
    struct mmString*                               pValue);

MM_EXPORT_DLL
void
mmPropertySet_SetPointerDirect(
    const struct mmPropertySet*                    p,
    const char*                                    pName,
    const void*                                    ptr);

MM_EXPORT_DLL
void*
mmPropertySet_GetPointerDirect(
    const struct mmPropertySet*                    p,
    const char*                                    pName);

MM_EXPORT_DLL 
void 
mmPropertySet_SetValueCStr(
    struct mmPropertySet*                          p, 
    const char*                                    pName, 
    const char*                                    pValue);

/*
 * @brief Make a PropertyHelper by type.
 *
 * @param HelperType The value transform helper type for property.
 */
#define mmPropertyHelperMake(HelperType) \
{                                        \
    &mmPropertyHelper_Set##HelperType,   \
    &mmPropertyHelper_Get##HelperType,   \
}

 /*
  * @brief Use for property define by offset, property is mutable.
  *
  * @param p          Type struct mmPropertySet*
  * @param Name       The name for property.
  * @param Offset     The base offset for property.
  * @param Type       The receiver type for property.
  * @param Member     The receiver member name for property.
  * @param HelperType The value transform helper type for property.
  * @param Default    The default value for property.
  * @param Setter     The function for 'Set'.
  * @param Getter     The function for 'Get'.
  * @param Origin     The property origin used for property grouping.
  * @param Help       Describe property help information.
  */
#define mmPropertyDefineMutable(p,             \
    Name, Offset, Type,                        \
    Member, HelperType, Default,               \
    Setter,                                    \
    Getter,                                    \
    Origin,                                    \
    Help)                                      \
{                                              \
    static struct mmProperty sProperty =       \
    {                                          \
        mmString_Make(Name),                   \
        mmString_Make(Help),                   \
        mmString_Make(Default),                \
        mmString_Make(Origin),                 \
        mmPropertyHelperMake(HelperType),      \
        0,                                     \
        mmOffsetof(Type, Member),              \
        mmMemberSizeof(Type, Member),          \
        (void*)Setter,                         \
        (void*)Getter,                         \
    };                                         \
    sProperty.hBaseOffset = Offset;            \
    mmPropertySet_AddProperty(p, &sProperty);  \
}

 /*
  * @brief Use for property define API by offset, property is mutable.
  *
  * @param p          Type struct mmPropertySet*
  * @param Name       The name for property.
  * @param Offset     The base offset for property.
  * @param HelperType The value transform helper type for property.
  * @param Default    The default value for property.
  * @param Setter     The function for 'Set'.
  * @param Getter     The function for 'Get'.
  * @param Origin     The property origin used for property grouping.
  * @param Help       Describe property help information.
  */
#define mmPropertyDefineMutableAPI(p,          \
    Name, Offset,                              \
    HelperType, Default,                       \
    Setter,                                    \
    Getter,                                    \
    Origin,                                    \
    Help)                                      \
{                                              \
    static struct mmProperty sProperty =       \
    {                                          \
        mmString_Make(Name),                   \
        mmString_Make(Help),                   \
        mmString_Make(Default),                \
        mmString_Make(Origin),                 \
        mmPropertyHelperMake(HelperType),      \
        0,                                     \
        0,                                     \
        0,                                     \
        (void*)Setter,                         \
        (void*)Getter,                         \
    };                                         \
    sProperty.hBaseOffset = Offset;            \
    mmPropertySet_AddProperty(p, &sProperty);  \
}

  /*
   * @brief Use for property define, property is const.
   *
   * @param p          Type struct mmPropertySet*
   * @param Name       The name for property.
   * @param Offset     The base offset for property.
   * @param Type       The receiver type for property.
   * @param Member     The receiver member name for property.
   * @param HelperType The value transform helper type for property.
   * @param Default    The default value for property.
   * @param Setter     The function for 'Set'.
   * @param Getter     The function for 'Get'.
   * @param Origin     The property origin used for property grouping.
   * @param Help       Describe property help information.
   */
#define mmPropertyDefine(p,                    \
    Name, Offset, Type,                        \
    Member, HelperType, Default,               \
    Setter,                                    \
    Getter,                                    \
    Origin,                                    \
    Help)                                      \
{                                              \
    static const struct mmProperty sProperty = \
    {                                          \
        mmString_Make(Name),                   \
        mmString_Make(Help),                   \
        mmString_Make(Default),                \
        mmString_Make(Origin),                 \
        mmPropertyHelperMake(HelperType),      \
        Offset,                                \
        mmOffsetof(Type, Member),              \
        mmMemberSizeof(Type, Member),          \
        (void*)Setter,                         \
        (void*)Getter,                         \
    };                                         \
    mmPropertySet_AddProperty(p, &sProperty);  \
}


   /*
    * @brief Use for property define API, property is const.
    *
    * @param p          Type struct mmPropertySet*
    * @param Name       The name for property.
    * @param Offset     The base offset for property.
    * @param HelperType The value transform helper type for property.
    * @param Default    The default value for property.
    * @param Setter     The function for 'Set'.
    * @param Getter     The function for 'Get'.
    * @param Origin     The property origin used for property grouping.
    * @param Help       Describe property help information.
    */
#define mmPropertyDefineAPI(p,                 \
    Name, Offset,                              \
    HelperType, Default,                       \
    Setter,                                    \
    Getter,                                    \
    Origin,                                    \
    Help)                                      \
{                                              \
    static const struct mmProperty sProperty = \
    {                                          \
        mmString_Make(Name),                   \
        mmString_Make(Help),                   \
        mmString_Make(Default),                \
        mmString_Make(Origin),                 \
        mmPropertyHelperMake(HelperType),      \
        Offset,                                \
        0,                                     \
        0,                                     \
        (void*)Setter,                         \
        (void*)Getter,                         \
    };                                         \
    mmPropertySet_AddProperty(p, &sProperty);  \
}

/*
 * @brief Use for property remove.
 *
 * @param p          Type struct mmPropertySet*
 * @param Name       The name for property.
 */
#define mmPropertyRemove(p, Name) mmPropertySet_RmvPropertyByName(p, Name)

#include "core/mmSuffix.h"

#endif//__mmPropertySet_h__
