/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBinarySerach.h"

// (arr[idx] <= v && v < arr[idx + 1])
MM_EXPORT_DLL
size_t
mmBinarySerachArrayStepIndexFloat(
    const float*                                   arr,
    size_t                                         o,
    size_t                                         l,
    float                                          v)
{
    size_t idx = (size_t)(-1);

    if (0 != l)
    {
        size_t mid = 0;
        size_t lid = o;
        size_t rid = o + l - 1;

        while (lid <= rid && (size_t)(-1) != rid)
        {
            // Avoid overflow problems
            mid = lid + (rid - lid) / 2;

            if (v < arr[mid])
            {
                rid = mid - 1;
            }
            else if (v >= arr[mid + 1])
            {
                lid = mid + 1;
            }
            else
            {
                //(arr[idx] <= v && v < arr[idx + 1])
                idx = mid;
                break;
            }
        }
    }

    return idx;
}

// (arr[idx] <= v && v < arr[idx + 1])
MM_EXPORT_DLL
size_t
mmBinarySerachArrayStepIndexInt(
    const int*                                     arr,
    size_t                                         o,
    size_t                                         l,
    int                                            v)
{
    size_t idx = (size_t)(-1);

    if (0 != l)
    {
        size_t mid = 0;
        size_t lid = o;
        size_t rid = o + l - 1;

        while (lid <= rid && (size_t)(-1) != rid)
        {
            // Avoid overflow problems
            mid = lid + (rid - lid) / 2;

            if (v < arr[mid])
            {
                rid = mid - 1;
            }
            else if (v >= arr[mid + 1])
            {
                lid = mid + 1;
            }
            else
            {
                //(arr[idx] <= v && v < arr[idx + 1])
                idx = mid;
                break;
            }
        }
    }

    return idx;
}

MM_EXPORT_DLL
size_t 
mmBinarySerachArrayFindEqual(
    const void*                                    key, 
    const void*                                    base,
    size_t                                         size, 
    size_t                                         nitems,
    int(*compare)(const void* item, const void* key))
{
    if (0 == nitems)
    {
        return (size_t)-1;
    }
    else
    {
        size_t mid = 0;
        size_t lid = 0;
        size_t rid = nitems - 1;
        int result = 0;

        while (lid <= rid && (size_t)(-1) != rid)
        {
            // Avoid overflow problems
            mid = lid + (rid - lid) / 2;

            result = (*(compare))(((uint8_t*)base) + size * mid, key);

            if (result > 0)
            {
                rid = mid - 1;
            }
            else if (result < 0)
            {
                lid = mid + 1;
            }
            else
            {
                return mid;
            }
        }

        return (size_t)-1;
    }
}

// base[idx] <= key
MM_EXPORT_DLL
size_t
mmBinarySerachArrayFindFirstEqualLarger(
    const void*                                    key,
    const void*                                    base,
    size_t                                         size,
    size_t                                         nitems,
    int(*compare)(const void* item, const void* key))
{
    if (0 == nitems)
    {
        return (size_t)-1;
    }
    else
    {
        size_t mid = 0;
        size_t lid = 0;
        size_t rid = nitems - 1;
        int result = 0;

        while (lid <= rid && (size_t)(-1) != rid)
        {
            // Avoid overflow problems
            mid = lid + (rid - lid) / 2;

            result = (*(compare))(((uint8_t*)base) + size * mid, key);

            if (result >= 0)
            {
                rid = mid - 1;
            }
            else
            {
                lid = mid + 1;
            }
        }

        return lid;
    }
}
