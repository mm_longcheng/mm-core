/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmContext.h"
#include "core/mmOSContext.h"
#include "core/mmLoggerManager.h"

static struct mmContext gContext;
MM_EXPORT_DLL struct mmContext* mmContext_Instance(void)
{
    return &gContext;
}

MM_EXPORT_DLL void mmContext_Init(struct mmContext* p)
{
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    struct mmOSContext* gOSContext = mmOSContext_Instance();
    mmLoggerManager_Init(gLoggerManager);
    mmOSContext_Init(gOSContext);
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DLL void mmContext_Destroy(struct mmContext* p)
{
    struct mmOSContext* gOSContext = mmOSContext_Instance();
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    mmOSContext_Destroy(gOSContext);
    mmLoggerManager_Destroy(gLoggerManager);
    p->state = MM_TS_CLOSED;
}
//
MM_EXPORT_DLL void mmContext_Start(struct mmContext* p)
{
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    struct mmOSContext* gOSContext = mmOSContext_Instance();
    mmLoggerManager_Start(gLoggerManager);
    mmOSContext_Perform(gOSContext);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
}
MM_EXPORT_DLL void mmContext_Interrupt(struct mmContext* p)
{
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    mmLoggerManager_Interrupt(gLoggerManager);
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DLL void mmContext_Shutdown(struct mmContext* p)
{
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    mmLoggerManager_Shutdown(gLoggerManager);
    p->state = MM_TS_FINISH;
}
MM_EXPORT_DLL void mmContext_Join(struct mmContext* p)
{
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    mmLoggerManager_Join(gLoggerManager);
}

