/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimer.h"
#include "core/mmTimeval.h"
#include "core/mmSpinlock.h"
#include "core/mmAlloc.h"
#include "core/mmLimit.h"

static void __static_mmTimerEntryHandleDefault(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{

}

MM_EXPORT_DLL void mmTimerEntryCallback_Init(struct mmTimerEntryCallback* p)
{
    p->Handle = &__static_mmTimerEntryHandleDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmTimerEntryCallback_Destroy(struct mmTimerEntryCallback* p)
{
    p->Handle = &__static_mmTimerEntryHandleDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmTimerEntryCallback_Reset(struct mmTimerEntryCallback* p)
{
    p->Handle = &__static_mmTimerEntryHandleDefault;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmTimerEntry_Init(struct mmTimerEntry* p)
{
    mmTimerEntryCallback_Init(&p->callback);
    mmTimeval_Init(&p->timer_value);
    mmSpinlock_Init(&p->locker, NULL);
    p->period = 0;
    p->id = 0;
    p->timer_id = MM_TIMER_ID_INVALID;
}
MM_EXPORT_DLL void mmTimerEntry_Destroy(struct mmTimerEntry* p)
{
    mmTimerEntryCallback_Destroy(&p->callback);
    mmTimeval_Destroy(&p->timer_value);
    mmSpinlock_Destroy(&p->locker);
    p->period = 0;
    p->id = 0;
    p->timer_id = 0;
}
MM_EXPORT_DLL void mmTimerEntry_Reset(struct mmTimerEntry* p)
{
    mmTimerEntryCallback_Reset(&p->callback);
    mmTimeval_Reset(&p->timer_value);
    // spinlock not reset
    p->period = 0;
    p->id = 0;
    p->timer_id = 0;
}
MM_EXPORT_DLL void mmTimerEntry_Lock(struct mmTimerEntry* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmTimerEntry_Unlock(struct mmTimerEntry* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_DLL int mmTimerEntry_Compare(const struct mmTimerEntry* p, const struct mmTimerEntry* q)
{
    return mmTimeval_Compare(&p->timer_value, &q->timer_value);
}

static void __static_mmTimerHeapCallback_Produce(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{

}

static void __static_mmTimerHeapCallback_Recycle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{

}

MM_EXPORT_DLL void mmTimerHeapCallback_Init(struct mmTimerHeapCallback* p)
{
    p->Produce = &__static_mmTimerHeapCallback_Produce;
    p->Recycle = &__static_mmTimerHeapCallback_Recycle;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmTimerHeapCallback_Destroy(struct mmTimerHeapCallback* p)
{
    p->Produce = &__static_mmTimerHeapCallback_Produce;
    p->Recycle = &__static_mmTimerHeapCallback_Recycle;
    p->obj = NULL;
}

static void __static_mmTimerHeap_CopyNode(struct mmTimerHeap* p, size_t slot, struct mmTimerEntry* moved_node)
{
    // Insert <moved_node> into its new location in the heap.
    mmVectorVpt_SetIndex(&p->heap, slot, moved_node);

    // Update the corresponding slot in the parallel <ids_> array.
    mmVectorU32_SetIndex(&p->timer_ids, moved_node->timer_id, (int)slot);
}

static int __static_mmTimerHeap_PopFreelist(struct mmTimerHeap* p)
{
    // We need to truncate this to <int> for backwards compatibility.
    int new_id = p->timer_ids_freelist;

    // The freelist values in the <timer_ids_> are negative, so we need
    // to negate them to get the next freelist "pointer."
    p->timer_ids_freelist = -((mmTimerId_t)mmVectorU32_GetIndex(&p->timer_ids, p->timer_ids_freelist));

    return new_id;
}

static void __static_mmTimerHeap_PushFreelist(struct mmTimerHeap* p, int old_id)
{
    // The freelist values in the <timer_ids_> are negative, so we need
    // to negate them to get the next freelist "pointer."
    mmVectorU32_SetIndex(&p->timer_ids, old_id, -p->timer_ids_freelist);
    p->timer_ids_freelist = old_id;
}

static void __static_mmTimerHeap_ReheapDown(struct mmTimerHeap* p, struct mmTimerEntry* moved_node, size_t slot, size_t child)
{
    struct mmTimerEntry* u1 = NULL;
    struct mmTimerEntry* u2 = NULL;
    // Restore the heap property after a deletion.    
    while (child < p->cur_size)
    {
        u1 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, child + 1);
        u2 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, child);

        // Choose the smaller of the two children.
        if (child + 1 < p->cur_size && 0 > mmTimerEntry_Compare(u1, u2))
        {
            child++;
        }

        u2 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, child);

        // Perform a <copy> if the child has a larger timeout value than
        // the <moved_node>.
        if (0 > mmTimerEntry_Compare(u2, moved_node))
        {
            __static_mmTimerHeap_CopyNode(p, slot, u2);
            slot = child;
            child = MM_HEAP_L(child);
        }
        else
        {
            // We've found our location in the heap.
            break;
        }
    }
    __static_mmTimerHeap_CopyNode(p, slot, moved_node);
}

static void __static_mmTimerHeap_ReheapUp(struct mmTimerHeap* p, struct mmTimerEntry* moved_node, size_t slot, size_t parent)
{
    struct mmTimerEntry* u_parent = NULL;
    // Restore the heap property after an insertion.    
    while (slot > 0)
    {
        u_parent = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, parent);

        // If the parent node is greater than the <moved_node> we need
        // to copy it down.
        if (0 > mmTimerEntry_Compare(moved_node, u_parent))
        {
            __static_mmTimerHeap_CopyNode(p, slot, u_parent);
            slot = parent;
            parent = MM_HEAP_P(slot);
        }
        else
        {
            break;
        }
    }

    // Insert the new node into its proper resting place in the heap and
    // update the corresponding slot in the parallel <timer_ids> array.
    __static_mmTimerHeap_CopyNode(p, slot, moved_node);
}

static void __static_mmTimerHeap_TimerIdxAssign(struct mmTimerHeap* p, size_t b, size_t e)
{
    size_t i = 0;
    for (i = b; i < e; ++i)
    {
        mmVectorU32_SetIndex(&p->timer_ids, i, -((mmTimerId_t)(i + 1)));
    }
}

static void __static_mmTimerHeap_GrowHeap(struct mmTimerHeap* p)
{
    // All the containers will double in size from max_size.
    size_t new_size = p->max_size + MM_TIMER_HEAP_PAGE;

    size_t curr_size = p->max_size;

    mmTimerHeap_SetLength(p, new_size);

    // And add the new elements to the end of the "freelist".
    __static_mmTimerHeap_TimerIdxAssign(p, curr_size, new_size);
}

static struct mmTimerEntry* __static_mmTimerHeap_RemoveNode(struct mmTimerHeap* p, size_t slot)
{
    struct mmTimerEntry* removed_node = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, slot);

    // Return this timer id to the freelist.
    __static_mmTimerHeap_PushFreelist(p, removed_node->timer_id);

    // Decrement the size of the heap by one since we're removing the
    // "slot"th node.
    p->cur_size--;

    // Set the ID
    removed_node->timer_id = MM_TIMER_ID_INVALID;

    // Only try to reheapify if we're not deleting the last entry.    
    if (slot < p->cur_size)
    {
        struct mmTimerEntry* u_parent = NULL;
        size_t parent;
        struct mmTimerEntry* moved_node = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, p->cur_size);

        // Move the end node to the location being removed and update
        // the corresponding slot in the parallel <timer_ids> array.
        __static_mmTimerHeap_CopyNode(p, slot, moved_node);

        // If the <moved_node->time_value_> is great than or equal its
        // parent it needs be moved down the heap.
        parent = MM_HEAP_P(slot);

        u_parent = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, parent);
        if (0 == mmTimerEntry_Compare(moved_node, u_parent))
        {
            __static_mmTimerHeap_ReheapDown(p, moved_node, slot, MM_HEAP_L(slot));
        }
        else
        {
            __static_mmTimerHeap_ReheapUp(p, moved_node, slot, parent);
        }
    }

    return removed_node;
}

static void __static_mmTimerHeap_InsertNode(struct mmTimerHeap* p, struct mmTimerEntry* new_node)
{
    if (p->cur_size >= p->max_size)
    {
        __static_mmTimerHeap_GrowHeap(p);
    }
    __static_mmTimerHeap_ReheapUp(p, new_node, p->cur_size, MM_HEAP_P(p->cur_size));
    p->cur_size++;
}

static struct mmTimerEntry* __static_mmTimerHeap_ProduceEntry(struct mmTimerHeap* p)
{
    struct mmTimerEntry* entry = NULL;

    // pool_element produce.
    mmPoolElement_Lock(&p->heap_entry);
    entry = (struct mmTimerEntry*)mmPoolElement_Produce(&p->heap_entry);
    mmPoolElement_Unlock(&p->heap_entry);
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmTimerEntry_Reset(entry);

    // Call the produce event hook.
    (*(p->callback.Produce))(p, entry);

    return entry;
}

static void __static_mmTimerHeap_RecycleEntry(struct mmTimerHeap* p, struct mmTimerEntry* entry)
{
    // Call the recycle event hook.
    (*(p->callback.Recycle))(p, entry);

    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmTimerEntry_Reset(entry);
    // pool_element recycle.
    mmPoolElement_Lock(&p->heap_entry);
    mmPoolElement_Recycle(&p->heap_entry, entry);
    mmPoolElement_Unlock(&p->heap_entry);
}
MM_EXPORT_DLL void mmTimerHeap_Init(struct mmTimerHeap* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmTimerHeapCallback_Init(&p->callback);
    mmPoolElement_Init(&p->heap_entry);
    mmVectorVpt_Init(&p->heap);
    mmVectorU32_Init(&p->timer_ids);
    mmClock_Init(&p->clock);
    mmSpinlock_Init(&p->locker_heap, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->max_size = 0;
    p->cur_size = 0;
    p->max_entries_per_poll = MM_TIMER_HEAP_MAX_POLL_TIMEOUT;
    p->timer_ids_freelist = 0;
    p->wait_usec = 0.0;
    p->frame_cost_usec = 0.0;

    hAllocator.Produce = &mmTimerEntry_Init;
    hAllocator.Recycle = &mmTimerEntry_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->heap_entry, sizeof(struct mmTimerEntry));
    mmPoolElement_SetAllocator(&p->heap_entry, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->heap_entry);

    mmTimerHeap_SetChunkSize(p, MM_TIMER_HEAP_CHUNK_SIZE_DEFAULT);
    mmTimerHeap_SetLength(p, MM_TIMER_HEAP_PAGE);
    //
    __static_mmTimerHeap_TimerIdxAssign(p, 0, p->max_size);
}
MM_EXPORT_DLL void mmTimerHeap_Destroy(struct mmTimerHeap* p)
{
    __static_mmTimerHeap_TimerIdxAssign(p, 0, p->max_size);
    //
    mmTimerHeapCallback_Destroy(&p->callback);
    mmPoolElement_Destroy(&p->heap_entry);
    mmVectorVpt_Destroy(&p->heap);
    mmVectorU32_Destroy(&p->timer_ids);
    mmClock_Destroy(&p->clock);
    mmSpinlock_Destroy(&p->locker_heap);
    mmSpinlock_Destroy(&p->locker);
    p->max_size = 0;
    p->cur_size = 0;
    p->max_entries_per_poll = 0;
    p->timer_ids_freelist = 0;
    p->wait_usec = 0.0;
    p->frame_cost_usec = 0.0;
}
MM_EXPORT_DLL void mmTimerHeap_Lock(struct mmTimerHeap* p)
{
    mmPoolElement_Lock(&p->heap_entry);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmTimerHeap_Unlock(struct mmTimerHeap* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmPoolElement_Unlock(&p->heap_entry);
}

MM_EXPORT_DLL void mmTimerHeap_SetLength(struct mmTimerHeap* p, size_t length)
{
    p->max_size = length;
    mmVectorVpt_AlignedMemory(&p->heap, p->max_size);
    mmVectorU32_AlignedMemory(&p->timer_ids, p->max_size);
}
MM_EXPORT_DLL void mmTimerHeap_SetChunkSize(struct mmTimerHeap* p, mmUInt32_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->heap_entry, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->heap_entry);
}

MM_EXPORT_DLL int mmTimerHeap_ScheduleEntry(struct mmTimerHeap* p, struct mmTimerEntry* entry, struct timeval* delay)
{
    struct timeval expires;
    mmTimerId_t timer_id = 0;

    mmGettimeofday(&expires, NULL);
    mmTimeval_Add(&expires, delay);

    mmSpinlock_Lock(&p->locker_heap);

    if (p->cur_size >= p->max_size)
    {
        __static_mmTimerHeap_GrowHeap(p);
    }
    timer_id = __static_mmTimerHeap_PopFreelist(p);

    mmSpinlock_Unlock(&p->locker_heap);

    if (0 <= timer_id)
    {
        // Obtain the next unique sequence number.
        // Set the entry
        entry->timer_id = timer_id;
        entry->timer_value = expires;

        mmSpinlock_Lock(&p->locker_heap);
        __static_mmTimerHeap_InsertNode(p, entry);
        mmSpinlock_Unlock(&p->locker_heap);
        return 1;
    }
    else
    {
        return 0;
    }
}

MM_EXPORT_DLL struct mmTimerEntry*
mmTimerHeap_ScheduleTimeval(
    struct mmTimerHeap* p,
    struct timeval* delay,
    mmTimerEntryHandleFunc handle)
{
    struct mmTimerEntry* entry = __static_mmTimerHeap_ProduceEntry(p);
    int status = mmTimerHeap_ScheduleEntry(p, entry, delay);
    if (0 == status)
    {
        __static_mmTimerHeap_RecycleEntry(p, entry);
        entry = NULL;
    }
    return entry;
}

// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mmTimerEntry*
mmTimerHeap_Schedule(
    struct mmTimerHeap* p,
    mmMSec_t delay,
    mmMSec_t period,
    mmTimerEntryHandleFunc handle,
    void* u)
{
    struct mmTimerEntry* entry = NULL;
    struct timeval tv_period;
    tv_period.tv_sec = (delay / MM_MSEC_PER_SEC);
    tv_period.tv_usec = (delay % MM_MSEC_PER_SEC) * 1000;
    // schedule timeval is lock free.
    entry = mmTimerHeap_ScheduleTimeval(p, &tv_period, handle);
    entry->callback.Handle = handle;
    entry->callback.obj = u;
    entry->period = period;
    return entry;
}
MM_EXPORT_DLL int mmTimerHeap_Cancel(struct mmTimerHeap* p, struct mmTimerEntry* entry)
{
    int num = 0;
    struct mmTimerEntry* u_slot = NULL;
    mmTimerId_t timer_node_slot = 0;

    do
    {
        // Check to see if the timer_id is out of range
        if (entry->timer_id < 0 || (size_t)entry->timer_id > p->max_size)
        {
            entry->timer_id = MM_TIMER_ID_INVALID;
            break;
        }

        mmSpinlock_Lock(&p->locker_heap);
        timer_node_slot = (mmTimerId_t)mmVectorU32_GetIndex(&p->timer_ids, entry->timer_id);
        mmSpinlock_Unlock(&p->locker_heap);

        if (timer_node_slot < 0)
        {
            // Check to see if timer_id is still valid.
            entry->timer_id = MM_TIMER_ID_INVALID;
            break;
        }

        mmSpinlock_Lock(&p->locker_heap);
        u_slot = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, timer_node_slot);
        mmSpinlock_Unlock(&p->locker_heap);

        if (entry != u_slot)
        {
            entry->timer_id = MM_TIMER_ID_INVALID;
            break;
        }

        mmSpinlock_Lock(&p->locker_heap);
        __static_mmTimerHeap_RemoveNode(p, timer_node_slot);
        mmSpinlock_Unlock(&p->locker_heap);

        __static_mmTimerHeap_RecycleEntry(p, entry);

        num = 1;
    } while (0);

    return num;
}

MM_EXPORT_DLL mmUInt32_t mmTimerHeap_Poll(struct mmTimerHeap* p, struct timeval* next_delay)
{
    struct timeval now_tv;
    struct timeval delay;
    mmUInt32_t count = 0;
    mmUInt64_t wait_usec_i64 = 0;

    mmClock_Reset(&p->clock);

    do
    {
        struct mmTimerEntry* u_slot_0 = NULL;
        struct mmTimerEntry* node = NULL;
        int status = 0;

        if (0 == p->cur_size && NULL != next_delay)
        {
            next_delay->tv_sec = next_delay->tv_usec = MM_UINT32_MAX;
            break;
        }

        count = 0;
        mmGettimeofday(&now_tv, NULL);

        mmSpinlock_Lock(&p->locker_heap);
        u_slot_0 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, 0);
        delay = u_slot_0->timer_value;
        mmSpinlock_Unlock(&p->locker_heap);

        while (0 != p->cur_size && 0 != mmTimeval_LessOrEqual(&delay, &now_tv) && count < p->max_entries_per_poll)
        {
            mmSpinlock_Lock(&p->locker_heap);
            node = __static_mmTimerHeap_RemoveNode(p, 0);
            mmSpinlock_Unlock(&p->locker_heap);

            ++count;

            (*(node->callback.Handle))(p, node);

            if (0 != node->period)
            {
                // schedule is lock free.
                struct timeval tv_period;
                mmMSec_t delay = node->period;
                tv_period.tv_sec  = (delay / MM_MSEC_PER_SEC);
                tv_period.tv_usec = (delay % MM_MSEC_PER_SEC) * 1000;
                status = mmTimerHeap_ScheduleEntry(p, node, &tv_period);
            }
            else
            {
                status = 0;
            }

            if (0 == status)
            {
                // Recycle timeout Entry.
                __static_mmTimerHeap_RecycleEntry(p, node);
                node = NULL;
            }

            mmSpinlock_Lock(&p->locker_heap);
            u_slot_0 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, 0);
            delay = u_slot_0->timer_value;
            mmSpinlock_Unlock(&p->locker_heap);
        }
        if (0 != p->cur_size && NULL != next_delay)
        {
            mmSpinlock_Lock(&p->locker_heap);
            u_slot_0 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, 0);
            *next_delay = u_slot_0->timer_value;
            mmSpinlock_Unlock(&p->locker_heap);

            // we get a new current timecode.
            mmGettimeofday(&now_tv, NULL);

            // next_delay - now
            mmTimeval_Sub(next_delay, &now_tv);

            // if the next delay time less then 0. we assign it to 0.
            //
            // note:
            // we mut be careful for time check, here we can not use 
            //
            //     next_delay->tv_sec * MM_USEC_PER_SEC + next_delay->tv_usec < 0
            //
            // the tv_sec is only long, will overflow at big tv_sec.
            // 
            // use timeval normalize.
            // make sure the 0 < tv_usec, if p->tv_sec * MM_USEC_PER_SEC + p->tv_usec > 0;
            mmTimeval_Normalize(next_delay);

            // now we only need check next_delay->tv_sec < 0.
            if (next_delay->tv_sec < 0)
            {
                next_delay->tv_sec = next_delay->tv_usec = 0;
            }
        }
        else if (NULL != next_delay)
        {
            next_delay->tv_sec = next_delay->tv_usec = MM_UINT32_MAX;
        }
    } while (0);

    if (NULL != next_delay)
    {
        // cache the wait usec.
        mmTimeval_Usec(next_delay, &wait_usec_i64);
        p->wait_usec = (double)wait_usec_i64;

        // cache the frame cost usec.
        p->frame_cost_usec = (double)(mmClock_Microseconds(&p->clock));
    }
    return count;
}
MM_EXPORT_DLL void mmTimerHeap_EarliestTime(struct mmTimerHeap* p, struct timeval* next_delay)
{
    if (p->cur_size == 0)
    {
        next_delay->tv_sec = MM_UINT32_MAX;
        next_delay->tv_usec = MM_UINT32_MAX;
    }
    else
    {
        struct mmTimerEntry* u_slot_0 = NULL;
        struct timeval now;

        mmGettimeofday(&now, NULL);

        mmSpinlock_Lock(&p->locker_heap);
        u_slot_0 = (struct mmTimerEntry*)mmVectorVpt_GetIndex(&p->heap, 0);
        *next_delay = u_slot_0->timer_value;
        mmSpinlock_Unlock(&p->locker_heap);

        mmTimeval_Sub(next_delay, &now);
    }
}

static void __static_timer_task_timer_handle(struct mmTimerTask* p)
{
    struct mmTimer* timer = (struct mmTimer*)(p->callback.obj);
    struct timeval next_delay;
    int nearby_time = 0;
    mmTimerHeap_Poll(&timer->timer_heap, &next_delay);
    nearby_time = (int)(next_delay.tv_sec * MM_MSEC_PER_SEC + next_delay.tv_usec / 1000);
    // [0, MM_TIME_TASK_NEARBY_MSEC]
    nearby_time = 0 > nearby_time ? 0 : nearby_time;
    nearby_time = MM_TIMER_TASK_NEARBY_MSEC < nearby_time ? MM_TIMER_TASK_NEARBY_MSEC : nearby_time;
    mmTimerTask_SetNearbyTime(&timer->timer_task, nearby_time);
}
MM_EXPORT_DLL void mmTimer_Init(struct mmTimer* p)
{
    struct mmTimerTaskCallback callback;

    mmTimerHeap_Init(&p->timer_heap);
    mmTimerTask_Init(&p->timer_task);

    callback.handle = &__static_timer_task_timer_handle;
    callback.obj = p;
    mmTimerTask_SetCallback(&p->timer_task, &callback);
}
MM_EXPORT_DLL void mmTimer_Destroy(struct mmTimer* p)
{
    mmTimerHeap_Destroy(&p->timer_heap);
    mmTimerTask_Destroy(&p->timer_task);
}

MM_EXPORT_DLL void mmTimer_Lock(struct mmTimer* p)
{
    mmTimerHeap_Lock(&p->timer_heap);
}
MM_EXPORT_DLL void mmTimer_Unlock(struct mmTimer* p)
{
    mmTimerHeap_Unlock(&p->timer_heap);
}

MM_EXPORT_DLL struct mmTimerEntry*
mmTimer_Schedule(
    struct mmTimer* p,
    mmMSec_t delay,
    mmMSec_t period,
    mmTimerEntryHandleFunc handle,
    void* u)
{
    struct timeval next_delay;
    int nearby_time = 0;
    struct mmTimerEntry* entry = mmTimerHeap_Schedule(&p->timer_heap, delay, period, handle, u);
    mmTimerHeap_EarliestTime(&p->timer_heap, &next_delay);
    nearby_time = (int)(next_delay.tv_sec * MM_MSEC_PER_SEC + next_delay.tv_usec / 1000);
    // [0, MM_TIME_TASK_NEARBY_MSEC]
    nearby_time = 0 > nearby_time ? 0 : nearby_time;
    nearby_time = MM_TIMER_TASK_NEARBY_MSEC < nearby_time ? MM_TIMER_TASK_NEARBY_MSEC : nearby_time;
    mmTimerTask_SetNearbyTime(&p->timer_task, nearby_time);
    return entry;
}

MM_EXPORT_DLL void mmTimer_Cancel(struct mmTimer* p, struct mmTimerEntry* entry)
{
    mmTimerHeap_Cancel(&p->timer_heap, entry);
}

MM_EXPORT_DLL void mmTimer_Start(struct mmTimer* p)
{
    mmTimerTask_Start(&p->timer_task);
}
MM_EXPORT_DLL void mmTimer_Interrupt(struct mmTimer* p)
{
    mmTimerTask_Interrupt(&p->timer_task);
}
MM_EXPORT_DLL void mmTimer_Shutdown(struct mmTimer* p)
{
    mmTimerTask_Shutdown(&p->timer_task);
}
MM_EXPORT_DLL void mmTimer_Join(struct mmTimer* p)
{
    mmTimerTask_Join(&p->timer_task);
}
