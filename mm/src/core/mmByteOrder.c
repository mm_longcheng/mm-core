/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmByteOrder.h"

#include "core/mmAlloc.h"
#include "core/mmByteswap.h"

#include "core/mmByte.h"
#include "core/mmStreambuf.h"
#include "core/mmStream.h"

MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeBin(struct mmByteBuffer* b, size_t* o, const mmUInt8_t* v, size_t l)
{
    mmMemcpy(b->buffer + b->offset + (*o), v, l);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU08(struct mmByteBuffer* b, size_t* o, const mmUInt8_t*  v)
{
    b->buffer[b->offset + (*o)] = *v;
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU16(struct mmByteBuffer* b, size_t* o, const mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoB16(*v);
    mmMemcpy(b->buffer + b->offset + (*o), &v_, sizeof(mmUInt16_t));
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU32(struct mmByteBuffer* b, size_t* o, const mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoB32(*v);
    mmMemcpy(b->buffer + b->offset + (*o), &v_, sizeof(mmUInt32_t));
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerEncodeU64(struct mmByteBuffer* b, size_t* o, const mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoB64(*v);
    mmMemcpy(b->buffer + b->offset + (*o), &v_, sizeof(mmUInt64_t));
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeBin(const struct mmByteBuffer* b, size_t* o, mmUInt8_t* v, size_t l)
{
    mmMemcpy(v, b->buffer + b->offset + (*o), l);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU08(const struct mmByteBuffer* b, size_t* o, mmUInt8_t*  v)
{
    *v = b->buffer[b->offset + (*o)];
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU16(const struct mmByteBuffer* b, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    mmMemcpy(&v_, b->buffer + b->offset + (*o), sizeof(mmUInt16_t));
    *v = mmBtoH16(v_);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU32(const struct mmByteBuffer* b, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    mmMemcpy(&v_, b->buffer + b->offset + (*o), sizeof(mmUInt32_t));
    *v = mmBtoH32(v_);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteBiggerDecodeU64(const struct mmByteBuffer* b, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    mmMemcpy(&v_, b->buffer + b->offset + (*o), sizeof(mmUInt64_t));
    *v = mmBtoH64(v_);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeBin(struct mmByteBuffer* b, size_t* o, const mmUInt8_t* v, size_t l)
{
    mmMemcpy(b->buffer + b->offset + (*o), v, l);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU08(struct mmByteBuffer* b, size_t* o, const mmUInt8_t*  v)
{
    b->buffer[b->offset + (*o)] = *v;
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU16(struct mmByteBuffer* b, size_t* o, const mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoL16(*v);
    mmMemcpy(b->buffer + b->offset + (*o), &v_, sizeof(mmUInt16_t));
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU32(struct mmByteBuffer* b, size_t* o, const mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoL32(*v);
    mmMemcpy(b->buffer + b->offset + (*o), &v_, sizeof(mmUInt32_t));
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleEncodeU64(struct mmByteBuffer* b, size_t* o, const mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoL64(*v);
    mmMemcpy(b->buffer + b->offset + (*o), &v_, sizeof(mmUInt64_t));
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeBin(const struct mmByteBuffer* b, size_t* o, mmUInt8_t* v, size_t l)
{
    mmMemcpy(v, b->buffer + b->offset + (*o), l);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU08(const struct mmByteBuffer* b, size_t* o, mmUInt8_t*  v)
{
    *v = b->buffer[b->offset + (*o)];
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU16(const struct mmByteBuffer* b, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    mmMemcpy(&v_, b->buffer + b->offset + (*o), sizeof(mmUInt16_t));
    *v = mmLtoH16(v_);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU32(const struct mmByteBuffer* b, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    mmMemcpy(&v_, b->buffer + b->offset + (*o), sizeof(mmUInt32_t));
    *v = mmLtoH32(v_);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteLittleDecodeU64(const struct mmByteBuffer* b, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    mmMemcpy(&v_, b->buffer + b->offset + (*o), sizeof(mmUInt64_t));
    *v = mmLtoH64(v_);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeBin(struct mmByteBuffer* b, size_t* o, const mmUInt8_t* v, size_t l)
{
    mmMemcpy(b->buffer + b->offset + (*o), v, l);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU08(struct mmByteBuffer* b, size_t* o, const mmUInt8_t*  v)
{
    b->buffer[b->offset + (*o)] = *v;
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU16(struct mmByteBuffer* b, size_t* o, const mmUInt16_t* v)
{
    mmMemcpy(b->buffer + b->offset + (*o), v, sizeof(mmUInt16_t));
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU32(struct mmByteBuffer* b, size_t* o, const mmUInt32_t* v)
{
    mmMemcpy(b->buffer + b->offset + (*o), v, sizeof(mmUInt32_t));
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteHostEncodeU64(struct mmByteBuffer* b, size_t* o, const mmUInt64_t* v)
{
    mmMemcpy(b->buffer + b->offset + (*o), v, sizeof(mmUInt64_t));
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeBin(const struct mmByteBuffer* b, size_t* o, mmUInt8_t* v, size_t l)
{
    mmMemcpy(v, b->buffer + b->offset + (*o), l);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU08(const struct mmByteBuffer* b, size_t* o, mmUInt8_t*  v)
{
    *v = b->buffer[b->offset + (*o)];
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU16(const struct mmByteBuffer* b, size_t* o, mmUInt16_t* v)
{
    mmMemcpy(v, b->buffer + b->offset + (*o), sizeof(mmUInt16_t));
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU32(const struct mmByteBuffer* b, size_t* o, mmUInt32_t* v)
{
    mmMemcpy(v, b->buffer + b->offset + (*o), sizeof(mmUInt32_t));
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_ByteHostDecodeU64(const struct mmByteBuffer* b, size_t* o, mmUInt64_t* v)
{
    mmMemcpy(v, b->buffer + b->offset + (*o), sizeof(mmUInt64_t));
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l)
{
    fwrite(v, l, 1, f);
    (*o) += (long)l;
}
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU08(FILE* f, long* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    v_ = mmHtoL08(*v);
    fwrite(&v_, 1, 1, f);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU16(FILE* f, long* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoL16(*v);
    fwrite(&v_, sizeof(mmUInt16_t), 1, f);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU32(FILE* f, long* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoL32(*v);
    fwrite(&v_, sizeof(mmUInt32_t), 1, f);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_FileLittleEncodeU64(FILE* f, long* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoL64(*v);
    fwrite(&v_, sizeof(mmUInt64_t), 1, f);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l)
{
    fread(v, l, 1, f);
    (*o) += (long)l;
}
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU08(FILE* f, long* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    fread(&v_, 1, 1, f);
    *v = mmLtoH08(v_);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU16(FILE* f, long* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    fread(&v_, sizeof(mmUInt16_t), 1, f);
    *v = mmLtoH16(v_);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU32(FILE* f, long* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    fread(&v_, sizeof(mmUInt32_t), 1, f);
    *v = mmLtoH32(v_);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_FileLittleDecodeU64(FILE* f, long* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    fread(&v_, sizeof(mmUInt64_t), 1, f);
    *v = mmLtoH64(v_);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l)
{
    fwrite(v, l, 1, f);
    (*o) += (long)l;
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU08(FILE* f, long* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    v_ = mmHtoB08(*v);
    fwrite(&v_, 1, 1, f);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU16(FILE* f, long* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoB16(*v);
    fwrite(&v_, sizeof(mmUInt16_t), 1, f);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU32(FILE* f, long* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoB32(*v);
    fwrite(&v_, sizeof(mmUInt32_t), 1, f);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerEncodeU64(FILE* f, long* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoB64(*v);
    fwrite(&v_, sizeof(mmUInt64_t), 1, f);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeBin(FILE* f, long* o, mmUInt8_t* v, size_t l)
{
    fread(v, l, 1, f);
    (*o) += (long)l;
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU08(FILE* f, long* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    fread(&v_, 1, 1, f);
    *v = mmBtoH08(v_);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU16(FILE* f, long* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    fread(&v_, sizeof(mmUInt16_t), 1, f);
    *v = mmBtoH16(v_);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU32(FILE* f, long* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    fread(&v_, sizeof(mmUInt32_t), 1, f);
    *v = mmBtoH32(v_);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_FileBiggerDecodeU64(FILE* f, long* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    fread(&v_, sizeof(mmUInt64_t), 1, f);
    *v = mmBtoH64(v_);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l)
{
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)v, 0, l);
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v)
{
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)v, 0, sizeof(mmUInt8_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoL16(*v);
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt16_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoL32(*v);
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt32_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleEncodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoL64(*v);
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt64_t));
}

MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l)
{
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)v, 0, l);
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v)
{
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)v, 0, sizeof(mmUInt8_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt16_t));
    (*v) = mmLtoH16(v_);
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt32_t));
    (*v) = mmLtoH32(v_);
}
MM_EXPORT_DLL void mmByteOrder_StreambufLittleDecodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt64_t));
    (*v) = mmLtoH64(v_);
}

MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l)
{
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)v, 0, l);
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v)
{
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)v, 0, sizeof(mmUInt8_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoB16(*v);
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt16_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoB32(*v);
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt32_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerEncodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoB64(*v);
    (*o) += mmStreambuf_Sputn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt64_t));
}

MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeBin(struct mmStreambuf* s, size_t* o, mmUInt8_t* v, size_t l)
{
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)v, 0, l);
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU08(struct mmStreambuf* s, size_t* o, mmUInt8_t*  v)
{
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)v, 0, sizeof(mmUInt8_t));
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU16(struct mmStreambuf* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt16_t));
    (*v) = mmBtoH16(v_);
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU32(struct mmStreambuf* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt32_t));
    (*v) = mmBtoH32(v_);
}
MM_EXPORT_DLL void mmByteOrder_StreambufBiggerDecodeU64(struct mmStreambuf* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    (*o) += mmStreambuf_Sgetn(s, (mmUInt8_t*)&v_, 0, sizeof(mmUInt64_t));
    (*v) = mmBtoH64(v_);
}

MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l)
{
    mmStreamWrite(v, l, 1, s);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    v_ = mmHtoL08(*v);
    mmStreamWrite(&v_, 1, 1, s);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoL16(*v);
    mmStreamWrite(&v_, sizeof(mmUInt16_t), 1, s);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoL32(*v);
    mmStreamWrite(&v_, sizeof(mmUInt32_t), 1, s);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleEncodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoL64(*v);
    mmStreamWrite(&v_, sizeof(mmUInt64_t), 1, s);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l)
{
    mmStreamRead(v, l, 1, s);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    mmStreamRead(&v_, 1, 1, s);
    *v = mmLtoH08(v_);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    mmStreamRead(&v_, sizeof(mmUInt16_t), 1, s);
    *v = mmLtoH16(v_);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    mmStreamRead(&v_, sizeof(mmUInt32_t), 1, s);
    *v = mmLtoH32(v_);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamLittleDecodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    mmStreamRead(&v_, sizeof(mmUInt64_t), 1, s);
    *v = mmLtoH64(v_);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l)
{
    mmStreamWrite(v, l, 1, s);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    v_ = mmHtoB08(*v);
    mmStreamWrite(&v_, 1, 1, s);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    v_ = mmHtoB16(*v);
    mmStreamWrite(&v_, sizeof(mmUInt16_t), 1, s);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    v_ = mmHtoB32(*v);
    mmStreamWrite(&v_, sizeof(mmUInt32_t), 1, s);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerEncodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    v_ = mmHtoB64(*v);
    mmStreamWrite(&v_, sizeof(mmUInt64_t), 1, s);
    (*o) += sizeof(mmUInt64_t);
}

MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeBin(struct mmStream* s, size_t* o, mmUInt8_t* v, size_t l)
{
    mmStreamRead(v, l, 1, s);
    (*o) += l;
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU08(struct mmStream* s, size_t* o, mmUInt8_t*  v)
{
    mmUInt8_t v_;
    mmStreamRead(&v_, 1, 1, s);
    *v = mmBtoH08(v_);
    (*o) += sizeof(mmUInt8_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU16(struct mmStream* s, size_t* o, mmUInt16_t* v)
{
    mmUInt16_t v_;
    mmStreamRead(&v_, sizeof(mmUInt16_t), 1, s);
    *v = mmBtoH16(v_);
    (*o) += sizeof(mmUInt16_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU32(struct mmStream* s, size_t* o, mmUInt32_t* v)
{
    mmUInt32_t v_;
    mmStreamRead(&v_, sizeof(mmUInt32_t), 1, s);
    *v = mmBtoH32(v_);
    (*o) += sizeof(mmUInt32_t);
}
MM_EXPORT_DLL void mmByteOrder_StreamBiggerDecodeU64(struct mmStream* s, size_t* o, mmUInt64_t* v)
{
    mmUInt64_t v_;
    mmStreamRead(&v_, sizeof(mmUInt64_t), 1, s);
    *v = mmBtoH64(v_);
    (*o) += sizeof(mmUInt64_t);
}
