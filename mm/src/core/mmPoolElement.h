/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPoolElement_h__
#define __mmPoolElement_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"

#include "container/mmListVpt.h"
#include "container/mmRbtsetVpt.h"
#include "container/mmRbtreeIntervalUIntptr.h"

#include <pthread.h>

#include "core/mmPrefix.h"

struct mmPoolElementChunk
{
    mmUInt32_t index_produce;
    mmUInt32_t index_recycle;
    mmUInt8_t* arrays;
    struct mmPoolElement* pool;
};

MM_EXPORT_DLL void mmPoolElementChunk_Init(struct mmPoolElementChunk* p);
MM_EXPORT_DLL void mmPoolElementChunk_Destroy(struct mmPoolElementChunk* p);

MM_EXPORT_DLL void mmPoolElementChunk_SetPool(struct mmPoolElementChunk* p, struct mmPoolElement* pool);

MM_EXPORT_DLL void mmPoolElementChunk_AcquireBuffer(struct mmPoolElementChunk* p);
MM_EXPORT_DLL void mmPoolElementChunk_ReleaseBuffer(struct mmPoolElementChunk* p);

MM_EXPORT_DLL void* mmPoolElementChunk_Produce(struct mmPoolElementChunk* p);
MM_EXPORT_DLL void mmPoolElementChunk_Recycle(struct mmPoolElementChunk* p, void* v);

MM_EXPORT_DLL int mmPoolElementChunk_ProduceComplete(struct mmPoolElementChunk* p);
MM_EXPORT_DLL int mmPoolElementChunk_RecycleComplete(struct mmPoolElementChunk* p);

MM_EXPORT_DLL void mmPoolElementChunk_ResetIndex(struct mmPoolElementChunk* p);

MM_EXPORT_DLL void* mmPoolElementChunk_Buffer(struct mmPoolElementChunk* p, mmUInt32_t index);

#define MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT 512
#define MM_POOL_ELEMENT_RATE_TRIM_BORDER_DEFAULT 0.75
#define MM_POOL_ELEMENT_RATE_TRIM_NUMBER_DEFAULT 0.25

// obj is struct mmPoolElement*
// u   is user data for callback
struct mmPoolElementAllocator
{
    // typedef void(*ProduceFuncType)(void* e);
    void* Produce;

    // typedef void(*RecycleFuncType)(void* e);
    void* Recycle;

    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmPoolElementAllocator_Init(struct mmPoolElementAllocator* p);
MM_EXPORT_DLL void mmPoolElementAllocator_Destroy(struct mmPoolElementAllocator* p);

struct mmPoolElement
{
    struct mmPoolElementAllocator allocator;
    // strong ref.
    struct mmRbtreeIntervalUIntptr rbtree;
    // weak ref.
    struct mmRbtsetVpt used;
    // weak ref.
    struct mmRbtsetVpt idle;
    // weak ref.
    struct mmPoolElementChunk* current;
    
    mmAtomic_t locker;
    
    size_t element_size;
    size_t chunk_size;
    size_t index_size;
    size_t buffer_size;
    
    float rate_trim_border;
    float rate_trim_number;
};

MM_EXPORT_DLL void mmPoolElement_Init(struct mmPoolElement* p);
MM_EXPORT_DLL void mmPoolElement_Destroy(struct mmPoolElement* p);

/* locker order is
*     locker
*/
MM_EXPORT_DLL void mmPoolElement_Lock(struct mmPoolElement* p);
MM_EXPORT_DLL void mmPoolElement_Unlock(struct mmPoolElement* p);

// can not change at pool using.
MM_EXPORT_DLL void mmPoolElement_SetElementSize(struct mmPoolElement* p, size_t element_size);
// can not change at pool using.
MM_EXPORT_DLL void mmPoolElement_SetChunkSize(struct mmPoolElement* p, size_t chunk_size);
// can not change at pool using.
MM_EXPORT_DLL void mmPoolElement_SetRateTrimBorder(struct mmPoolElement* p, float rate_trim_border);
// can not change at pool using.
MM_EXPORT_DLL void mmPoolElement_SetRateTrimNumber(struct mmPoolElement* p, float rate_trim_number);
// can not change at pool using.
MM_EXPORT_DLL void mmPoolElement_SetAllocator(struct mmPoolElement* p, const struct mmPoolElementAllocator* allocator);

MM_EXPORT_DLL void mmPoolElement_CalculationBucketSize(struct mmPoolElement* p);

MM_EXPORT_DLL struct mmPoolElementChunk* mmPoolElement_AddChunk(struct mmPoolElement* p);
MM_EXPORT_DLL struct mmPoolElementChunk* mmPoolElement_GetChunk(struct mmPoolElement* p, uintptr_t n);
MM_EXPORT_DLL void mmPoolElement_RmvChunk(struct mmPoolElement* p, uintptr_t n);
MM_EXPORT_DLL void mmPoolElement_ClearChunk(struct mmPoolElement* p);

MM_EXPORT_DLL size_t mmPoolElement_Capacity(struct mmPoolElement* p);

MM_EXPORT_DLL void mmPoolElement_EventProduce(struct mmPoolElement* p, struct mmPoolElementChunk* e);
MM_EXPORT_DLL void mmPoolElement_EventRecycle(struct mmPoolElement* p, struct mmPoolElementChunk* e);

MM_EXPORT_DLL void* mmPoolElement_Produce(struct mmPoolElement* p);
MM_EXPORT_DLL void mmPoolElement_Recycle(struct mmPoolElement* p, void* v);

#include "core/mmSuffix.h"

#endif//__mmPoolElement_h__
