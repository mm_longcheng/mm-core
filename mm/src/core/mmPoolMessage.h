/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPoolMessage_h__
#define __mmPoolMessage_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmStreambuf.h"
#include "core/mmPoolElement.h"

#include "container/mmRbtsetVpt.h"

#include "core/mmPrefix.h"

#define MM_POOL_MESSAGE_RATE_TRIM_BORDER_DEFAULT 0.75
#define MM_POOL_MESSAGE_RATE_TRIM_NUMBER_DEFAULT 0.25

#define MM_POOL_MESSAGE_TRIM_PAGE 4

// overdraft byte_buffer for encode.
// will pbump streambuf.
MM_EXPORT_DLL mmUInt32_t mmPoolMessage_StreambufByteBufferOverdraft(struct mmStreambuf* p, struct mmByteBuffer* byte_buffer);

// repayment byte_buffer for encode.
// will gbump streambuf.
MM_EXPORT_DLL mmUInt32_t mmPoolMessage_StreambufByteBufferRepayment(struct mmStreambuf* p, struct mmByteBuffer* byte_buffer);

struct mmPoolMessageElem;

MM_EXPORT_DLL void mmPoolMessage_ChunkProduce(struct mmStreambuf* p, struct mmPoolMessageElem* v, size_t length);
MM_EXPORT_DLL void mmPoolMessage_ChunkRecycle(struct mmStreambuf* p, struct mmPoolMessageElem* v);
MM_EXPORT_DLL int mmPoolMessage_ChunkProduceComplete(struct mmStreambuf* p);
MM_EXPORT_DLL int mmPoolMessage_ChunkRecycleComplete(struct mmStreambuf* p);

struct mmPoolMessageElem
{
    // message id
    mmUInt32_t mid;
    // buffer reference.
    struct mmByteBuffer byte_buffer;
    // weak ref for streambuf.
    struct mmStreambuf* streambuf;
    // weak ref for handle callback.
    void* obj;
};
MM_EXPORT_DLL void mmPoolMessageElem_Init(struct mmPoolMessageElem* p);
MM_EXPORT_DLL void mmPoolMessageElem_Destroy(struct mmPoolMessageElem* p);
MM_EXPORT_DLL void mmPoolMessageElem_Reset(struct mmPoolMessageElem* p);
MM_EXPORT_DLL void mmPoolMessageElem_SetMid(struct mmPoolMessageElem* p, mmUInt32_t mid);
MM_EXPORT_DLL void mmPoolMessageElem_SetObject(struct mmPoolMessageElem* p, void* obj);
MM_EXPORT_DLL void mmPoolMessageElem_SetBuffer(struct mmPoolMessageElem* p, struct mmByteBuffer* byte_buffer);

// default message pool chunk size is 64.
#define MM_POOL_MESSAGE_CHUNK_SIZE_DEFAULT 64

struct mmPoolMessage
{
    struct mmPoolElement pool_element;
    // strong ref.
    struct mmRbtsetVpt pool;
    // weak ref.
    struct mmRbtsetVpt used;
    // weak ref.
    struct mmRbtsetVpt idle;
    // weak ref for current streambuf.
    struct mmStreambuf* current;
    
    float rate_trim_border;
    float rate_trim_number;
};

MM_EXPORT_DLL void mmPoolMessage_Init(struct mmPoolMessage* p);
MM_EXPORT_DLL void mmPoolMessage_Destroy(struct mmPoolMessage* p);

/* locker order is
*     pool_element
*/
MM_EXPORT_DLL void mmPoolMessage_Lock(struct mmPoolMessage* p);
MM_EXPORT_DLL void mmPoolMessage_Unlock(struct mmPoolMessage* p);

// can not change at pool using.
MM_EXPORT_DLL void mmPoolMessage_SetChunkSize(struct mmPoolMessage* p, size_t chunk_size);
// can not change at pool using.
MM_EXPORT_DLL void mmPoolMessage_SetRateTrimBorder(struct mmPoolMessage* p, float rate_trim_border);
// can not change at pool using.
MM_EXPORT_DLL void mmPoolMessage_SetRateTrimNumber(struct mmPoolMessage* p, float rate_trim_number);

MM_EXPORT_DLL struct mmStreambuf* mmPoolMessage_AddChunk(struct mmPoolMessage* p);
MM_EXPORT_DLL void mmPoolMessage_RmvChunk(struct mmPoolMessage* p, struct mmStreambuf* e);
MM_EXPORT_DLL void mmPoolMessage_ClearChunk(struct mmPoolMessage* p);

MM_EXPORT_DLL struct mmPoolMessageElem* mmPoolMessage_Produce(struct mmPoolMessage* p, size_t length);
MM_EXPORT_DLL void mmPoolMessage_Recycle(struct mmPoolMessage* p, struct mmPoolMessageElem* v);

#include "core/mmSuffix.h"

#endif//__mmPoolMessage_h__
