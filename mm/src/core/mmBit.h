/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBit_h__
#define __mmBit_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

static mmInline mmUInt64_t mmPopcount64(mmUInt64_t y) // standard popcount; from wikipedia
{
    y -= ((y >> 1) & 0x5555555555555555ull);
    y = (y & 0x3333333333333333ull) + (y >> 2 & 0x3333333333333333ull);
    return ((y + (y >> 4)) & 0xf0f0f0f0f0f0f0full) * 0x101010101010101ull >> 56;
}

static mmInline mmUInt64_t mmDNAcount64(mmUInt64_t y, int c) // count #A/C/G/T from a 2-bit encoded integer; from BWA
{
    // reduce nucleotide counting to bits counting
    y = ((c & 2) ? y : ~y) >> 1 & ((c & 1) ? y : ~y) & 0x5555555555555555ull;
    // count the number of 1s in y
    y = (y & 0x3333333333333333ull) + (y >> 2 & 0x3333333333333333ull);
    return ((y + (y >> 4)) & 0xf0f0f0f0f0f0f0full) * 0x101010101010101ull >> 56;
}

#ifndef mmRoundup32 // round a 32-bit integer to the next closet integer; from "bit twiddling hacks"
#define mmRoundup32(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
#endif

#ifndef mmRoundup64 // round a 64-bit integer to the next closet integer; from "bit twiddling hacks"
#define mmRoundup64(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, (x)|=(x)>>32, ++(x))
#endif

#ifndef mmSwap
#define mmSwap(a, b) (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b))) // from "bit twiddling hacks"
#endif

static mmInline int mmIsPowerOf2(unsigned int n)
{
    return (n != 0 && ((n & (n - 1)) == 0));
}

// helper to find the maximum power of two
// less than p (more involved than necessary, to avoid PTS)
MM_EXPORT_DLL int mmMaxPow2Less(int p, int n);

MM_EXPORT_DLL int mmU08IntegerLog2Impl(mmUInt8_t x, int n);
MM_EXPORT_DLL int mmU16IntegerLog2Impl(mmUInt16_t x, int n);
MM_EXPORT_DLL int mmU32IntegerLog2Impl(mmUInt32_t x, int n);
MM_EXPORT_DLL int mmU64IntegerLog2Impl(mmUInt64_t x, int n);
MM_EXPORT_DLL int mmUIntptrIntegerLog2Impl(uintptr_t x, int n);

MM_EXPORT_DLL int mmUIntptrIntegerLog2(uintptr_t x);

MM_EXPORT_DLL int mmUIntptrLowestBit(uintptr_t x);

// sign for float32.
#define mmSignOfFloat32(f) (((*(mmUInt32_t*)&f) >> 31) & 0x1)
// sign for float64.
#define mmSignOfFloat64(f) (((*(mmUInt64_t*)&f) >> 63) & 0x1)

enum { mmByteAlignment = 16 };

static mmInline size_t mmSizeAlignment(size_t n, size_t a)
{
    return (n + (a - 1)) & ~(a - 1);
}

static mmInline size_t mmSizeAlign(size_t n)
{
    return mmSizeAlignment(n, mmByteAlignment);
}

// m min align size.
// n number for calculate.
MM_EXPORT_DLL size_t mmHalfAccumulateMinGreater(size_t m, size_t n);

// n number for calculate.
MM_EXPORT_DLL size_t mmFibonacciMinGreater(size_t n);
// n fibonacci round.
MM_EXPORT_DLL size_t mmFibonacciValue(size_t n);

#include "core/mmSuffix.h"

#endif//__mmBit_h__
