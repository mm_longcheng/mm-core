/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmStream_h__
#define __mmStream_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// interface of stream.
struct mmIStream
{
    size_t
    (*Write)(
        const void*         ptr,
        size_t              size,
        size_t              nmemb,
        void*               stream);
    
    size_t
    (*Read)(
        void*               ptr,
        size_t              size,
        size_t              nmemb,
        void*               stream);
    
    int
    (*Seek)(
        void*               stream,
        long                offset,
        int                 whence);
    
    int
    (*Seeko)(
        void*               stream,
        off_t               offset,
        int                 whence);
    
    int
    (*Seeko64)(
        void*               stream,
        int64_t             offset,
        int                 whence);

    long
    (*Tell)(
        void*               stream);
    
    off_t
    (*Tello)(
        void*               stream);
    
    int64_t
    (*Tello64)(
        void*               stream);
};

MM_EXPORT_DLL extern const struct mmIStream mmStreamFILE;
MM_EXPORT_DLL extern const struct mmIStream mmStreamStreambuf;
MM_EXPORT_DLL extern const struct mmIStream mmStreamByteBuffer;

struct mmStream
{
    // interface for stream.
    const struct mmIStream* i;
    
    // stream pointer.
    void* s;
    
    // type for implement. -1 is none.
    int t;
};

MM_EXPORT_DLL
void
mmStream_Init(
    struct mmStream*                               p);

MM_EXPORT_DLL
void
mmStream_Destroy(
    struct mmStream*                               p);

MM_EXPORT_DLL
int
mmStream_Invalid(
    const struct mmStream*                         p);

MM_EXPORT_DLL
size_t
mmStreamWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmStreamRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
int
mmStreamSeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmStreamSeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmStreamSeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence);

MM_EXPORT_DLL
long
mmStreamTell(
    void*                                          stream);

MM_EXPORT_DLL
off_t
mmStreamTello(
    void*                                          stream);

MM_EXPORT_DLL
int64_t
mmStreamTello64(
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmFILEWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmFILERead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
int
mmFILESeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmFILESeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmFILESeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence);

MM_EXPORT_DLL
long
mmFILETell(
    void*                                          stream);

MM_EXPORT_DLL
off_t
mmFILETello(
    void*                                          stream);

MM_EXPORT_DLL
int64_t
mmFILETello64(
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmStreambufWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmStreambufRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
int
mmStreambufSeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmStreambufSeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmStreambufSeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence);

MM_EXPORT_DLL
long
mmStreambufTell(
    void*                                          stream);

MM_EXPORT_DLL
off_t
mmStreambufTello(
    void*                                          stream);

MM_EXPORT_DLL
int64_t
mmStreambufTello64(
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmByteBufferWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
size_t
mmByteBufferRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream);

MM_EXPORT_DLL
int
mmByteBufferSeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmByteBufferSeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence);

MM_EXPORT_DLL
int
mmByteBufferSeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence);

MM_EXPORT_DLL
long
mmByteBufferTell(
    void*                                          stream);

MM_EXPORT_DLL
off_t
mmByteBufferTello(
    void*                                          stream);

MM_EXPORT_DLL
int64_t
mmByteBufferTello64(
    void*                                          stream);

#include "core/mmSuffix.h"

#endif//__mmStream_h__
