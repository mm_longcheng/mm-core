/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimespec.h"
#include "core/mmTime.h"

MM_EXPORT_DLL void mmTimespec_Init(struct timespec* p)
{
    p->tv_sec = 0;
    p->tv_nsec = 0;
}

MM_EXPORT_DLL void mmTimespec_Destroy(struct timespec* p)
{
    p->tv_sec = 0;
    p->tv_nsec = 0;
}
MM_EXPORT_DLL void mmTimespec_Reset(struct timespec* p)
{
    p->tv_sec = 0;
    p->tv_nsec = 0;
}

MM_EXPORT_DLL void mmTimespec_Normalize(struct timespec* p)
{
    p->tv_sec += (p->tv_nsec / MM_NSEC_PER_SEC);
    p->tv_nsec = (p->tv_nsec % MM_NSEC_PER_SEC);
    // make sure the 0 < tv_nsec, if p->tv_sec * MM_NSEC_PER_SEC + p->tv_nsec > 0;
    if (0 < p->tv_sec && 0 > p->tv_nsec)
    {
        p->tv_sec--;
        p->tv_nsec += MM_NSEC_PER_SEC;
    }
}

