/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBit.h"

MM_EXPORT_DLL int mmMaxPow2Less(int p, int n)
{
    if (0 == p && n == p)
    {
        return 0;
    }
    else
    {
        int c = 2 * n < p;
        return c ? mmMaxPow2Less(c * p, 2 * c * n) : n;
    }
}

MM_EXPORT_DLL int mmU08IntegerLog2Impl(mmUInt8_t x, int n)
{
    int result = 0;

    while (x != 1)
    {
        const mmUInt8_t t = (mmUInt8_t)(x >> n);
        if (t)
        {
            result += n;
            x = t;
        }
        n /= 2;
    }
    return result;
}
MM_EXPORT_DLL int mmU16IntegerLog2Impl(mmUInt16_t x, int n)
{
    int result = 0;

    while (x != 1)
    {
        const mmUInt16_t t = (mmUInt16_t)(x >> n);
        if (t)
        {
            result += n;
            x = t;
        }
        n /= 2;
    }
    return result;
}

MM_EXPORT_DLL int mmU32IntegerLog2Impl(mmUInt32_t x, int n)
{
    int result = 0;

    while (x != 1)
    {
        const mmUInt32_t t = (mmUInt32_t)(x >> n);
        if (t)
        {
            result += n;
            x = t;
        }
        n /= 2;
    }
    return result;
}
MM_EXPORT_DLL int mmU64IntegerLog2Impl(mmUInt64_t x, int n)
{
    int result = 0;

    while (x != 1)
    {
        const mmUInt32_t t = (mmUInt32_t)(x >> n);
        if (t)
        {
            result += n;
            x = t;
        }
        n /= 2;
    }
    return result;
}

MM_EXPORT_DLL int mmUIntptrIntegerLog2Impl(uintptr_t x, int n)
{
    int result = 0;

    while (x != 1)
    {
        const uintptr_t t = (uintptr_t)(x >> n);
        if (t)
        {
            result += n;
            x = t;
        }
        n /= 2;
    }
    return result;
}

MM_EXPORT_DLL int mmUIntptrIntegerLog2(uintptr_t x)
{
    assert(x > 0);

    const int n = mmMaxPow2Less(MM_WORDSIZE, 4);

    return mmUIntptrIntegerLog2Impl(x, n);
}

MM_EXPORT_DLL int mmUIntptrLowestBit(uintptr_t x)
{
    assert(x >= 1); // PRE

                    // clear all bits on except the rightmost one,
                    // then calculate the logarithm base 2
                    //
    return mmUIntptrIntegerLog2Impl(x - (x & (x - 1)), MM_WORDSIZE / 2);
}

// m min align size.
// n number for calculate.
MM_EXPORT_DLL size_t mmHalfAccumulateMinGreater(size_t m, size_t n)
{
    static const size_t mmMaxValveSize = (SIZE_MAX / 3) * 2;

    size_t v = m;

    if (n >= mmMaxValveSize)
    {
        v = SIZE_MAX;
    }
    else
    {
        do
        {
            v = v + (v >> 1);
        } while (v < n);
    }
    return v;
}

// n number for calculate.
MM_EXPORT_DLL size_t mmFibonacciMinGreater(size_t n)
{
    static const size_t mmMaxValveSize = (size_t)((double)SIZE_MAX * 0.618);

    size_t v = 0;

    if (n >= mmMaxValveSize)
    {
        v = SIZE_MAX;
    }
    else
    {
        size_t v0 = 0;
        size_t v1 = 1;
        size_t v2 = 0;
        while (v0 < n)
        {
            v2 = v1 + v0;
            v0 = v1;
            v1 = v2;
        }

        v = v0;
    }

    return v;
}
// n fibonacci round.
MM_EXPORT_DLL size_t mmFibonacciValue(size_t n)
{
    size_t i = 0;
    size_t v0 = 0;
    size_t v1 = 1;
    size_t v2 = 0;
    for (i = 0; i < n; ++i)
    {
        v2 = v0 + v1;
        v0 = v1;
        v1 = v2;
    }
    return v0;
}

