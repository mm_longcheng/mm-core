/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoolElement.h"
#include "core/mmStreambuf.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

#include <assert.h>

MM_EXPORT_DLL void mmPoolElementChunk_Init(struct mmPoolElementChunk* p)
{
    p->index_produce = 0;
    p->index_recycle = 0;
    p->arrays = NULL;
    p->pool = NULL;
}
MM_EXPORT_DLL void mmPoolElementChunk_Destroy(struct mmPoolElementChunk* p)
{
    mmPoolElementChunk_ReleaseBuffer(p);
    //
    p->index_produce = 0;
    p->index_recycle = 0;
    p->arrays = NULL;
    p->pool = NULL;
}
MM_EXPORT_DLL void mmPoolElementChunk_SetPool(struct mmPoolElementChunk* p, struct mmPoolElement* pool)
{
    p->pool = pool;
}
MM_EXPORT_DLL void mmPoolElementChunk_AcquireBuffer(struct mmPoolElementChunk* p)
{
    assert(NULL == p->arrays && "NULL == p->arrays is invalid.");
    p->arrays = (mmUInt8_t*)mmMalloc(p->pool->buffer_size);
}
MM_EXPORT_DLL void mmPoolElementChunk_ReleaseBuffer(struct mmPoolElementChunk* p)
{
    if (NULL != p->arrays)
    {
        mmFree(p->arrays);
        p->arrays = NULL;
    }
}

MM_EXPORT_DLL void* mmPoolElementChunk_Produce(struct mmPoolElementChunk* p)
{
    return mmPoolElementChunk_Buffer(p, p->index_produce++);
}
MM_EXPORT_DLL void mmPoolElementChunk_Recycle(struct mmPoolElementChunk* p, void* v)
{
    p->index_recycle++;
}

MM_EXPORT_DLL int mmPoolElementChunk_ProduceComplete(struct mmPoolElementChunk* p)
{
    return p->index_produce == p->pool->index_size;
}
MM_EXPORT_DLL int mmPoolElementChunk_RecycleComplete(struct mmPoolElementChunk* p)
{
    return p->index_recycle == p->pool->index_size;
}
MM_EXPORT_DLL void mmPoolElementChunk_ResetIndex(struct mmPoolElementChunk* p)
{
    p->index_produce = 0;
    p->index_recycle = 0;
}
MM_EXPORT_DLL void* mmPoolElementChunk_Buffer(struct mmPoolElementChunk* p, mmUInt32_t index)
{
    return p->arrays + index * p->pool->element_size;
}

MM_EXPORT_DLL void mmPoolElementAllocator_Init(struct mmPoolElementAllocator* p)
{
    p->Produce = NULL;
    p->Recycle = NULL;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmPoolElementAllocator_Destroy(struct mmPoolElementAllocator* p)
{
    p->Produce = NULL;
    p->Recycle = NULL;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmPoolElement_Init(struct mmPoolElement* p)
{
    mmPoolElementAllocator_Init(&p->allocator);
    mmRbtreeIntervalUIntptr_Init(&p->rbtree);
    mmRbtsetVpt_Init(&p->used);
    mmRbtsetVpt_Init(&p->idle);
    mmSpinlock_Init(&p->locker, NULL);
    p->current = NULL;
    p->element_size = 0;
    p->index_size = 0;
    p->buffer_size = 0;
    p->chunk_size = MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT;
    p->rate_trim_border = MM_POOL_ELEMENT_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_POOL_ELEMENT_RATE_TRIM_NUMBER_DEFAULT;
}
MM_EXPORT_DLL void mmPoolElement_Destroy(struct mmPoolElement* p)
{
    mmPoolElement_ClearChunk(p);
    //
    mmPoolElementAllocator_Destroy(&p->allocator);
    mmRbtreeIntervalUIntptr_Destroy(&p->rbtree);
    mmRbtsetVpt_Destroy(&p->used);
    mmRbtsetVpt_Destroy(&p->idle);
    mmSpinlock_Destroy(&p->locker);
    p->current = NULL;
    p->element_size = 0;
    p->index_size = 0;
    p->buffer_size = 0;
    p->chunk_size = MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT;
    p->rate_trim_border = MM_POOL_ELEMENT_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_POOL_ELEMENT_RATE_TRIM_NUMBER_DEFAULT;
}

MM_EXPORT_DLL void mmPoolElement_Lock(struct mmPoolElement* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmPoolElement_Unlock(struct mmPoolElement* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_DLL void mmPoolElement_SetElementSize(struct mmPoolElement* p, size_t element_size)
{
    p->element_size = element_size;
}
MM_EXPORT_DLL void mmPoolElement_SetChunkSize(struct mmPoolElement* p, size_t chunk_size)
{
    p->chunk_size = chunk_size;
}
MM_EXPORT_DLL void mmPoolElement_SetRateTrimBorder(struct mmPoolElement* p, float rate_trim_border)
{
    p->rate_trim_border = rate_trim_border;
}
MM_EXPORT_DLL void mmPoolElement_SetRateTrimNumber(struct mmPoolElement* p, float rate_trim_number)
{
    p->rate_trim_number = rate_trim_number;
}
MM_EXPORT_DLL void mmPoolElement_SetAllocator(struct mmPoolElement* p, const struct mmPoolElementAllocator* allocator)
{
    assert(NULL != allocator && "you can not assign null Allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_DLL void mmPoolElement_CalculationBucketSize(struct mmPoolElement* p)
{
    size_t need_size = p->chunk_size * p->element_size;
    size_t va = need_size / MM_STREAMBUF_PAGE_SIZE;
    size_t vb = need_size % MM_STREAMBUF_PAGE_SIZE;
    size_t page_size = va + (0 == vb ? 0 : 1);
    p->buffer_size = page_size * MM_STREAMBUF_PAGE_SIZE;
    p->index_size = p->buffer_size / p->element_size;
}

MM_EXPORT_DLL struct mmPoolElementChunk* mmPoolElement_AddChunk(struct mmPoolElement* p)
{
    struct mmPoolElementChunk* e = NULL;
    uintptr_t l = 0;
    uintptr_t r = 0;
    e = (struct mmPoolElementChunk*)mmMalloc(sizeof(struct mmPoolElementChunk));
    mmPoolElementChunk_Init(e);
    mmPoolElementChunk_SetPool(e, p);
    mmPoolElementChunk_AcquireBuffer(e);
    l = (uintptr_t)(e->arrays);
    r = (uintptr_t)(e->arrays + p->chunk_size * p->element_size);
    mmRbtreeIntervalUIntptr_Set(&p->rbtree, l, r, e);
    mmPoolElement_EventProduce(p, e);
    return e;
}
MM_EXPORT_DLL struct mmPoolElementChunk* mmPoolElement_GetChunk(struct mmPoolElement* p, uintptr_t n)
{
    return (struct mmPoolElementChunk*)mmRbtreeIntervalUIntptr_Get(&p->rbtree, n, n);
}
MM_EXPORT_DLL void mmPoolElement_RmvChunk(struct mmPoolElement* p, uintptr_t n)
{
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    struct mmPoolElementChunk* e = NULL;
    it = mmRbtreeIntervalUIntptr_GetIterator(&p->rbtree, n, n);
    if (NULL != it)
    {
        e = (struct mmPoolElementChunk*)(it->v);
        mmPoolElement_EventRecycle(p, e);
        mmRbtreeIntervalUIntptr_Erase(&p->rbtree, it);
        mmPoolElementChunk_ReleaseBuffer(e);
        mmPoolElementChunk_Destroy(e);
        mmFree(e);
    }
}
MM_EXPORT_DLL void mmPoolElement_ClearChunk(struct mmPoolElement* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeIntervalUIntptrIterator* it = NULL;
    struct mmPoolElementChunk* e = NULL;
    //
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeIntervalUIntptrIterator, n);
        e = (struct mmPoolElementChunk*)(it->v);
        mmPoolElement_EventRecycle(p, e);
        n = mmRb_Next(n);
        mmRbtreeIntervalUIntptr_Erase(&p->rbtree, it);
        mmPoolElementChunk_ReleaseBuffer(e);
        mmPoolElementChunk_Destroy(e);
        mmFree(e);
    }
}
MM_EXPORT_DLL size_t mmPoolElement_Capacity(struct mmPoolElement* p)
{
    return p->index_size * p->rbtree.size;
}

MM_EXPORT_DLL void mmPoolElement_EventProduce(struct mmPoolElement* p, struct mmPoolElementChunk* e)
{
    if (NULL != p->allocator.Produce)
    {
        typedef void(*ProduceFuncType)(void* e);
        void* v = NULL;
        mmUInt32_t i = 0;
        ProduceFuncType Produce = (ProduceFuncType)p->allocator.Produce;
        for (i = 0; i < p->chunk_size; i++)
        {
            v = mmPoolElementChunk_Buffer(e, i);
            (*(Produce))(v);
        }
    }
}
MM_EXPORT_DLL void mmPoolElement_EventRecycle(struct mmPoolElement* p, struct mmPoolElementChunk* e)
{
    if (NULL != p->allocator.Recycle)
    {
        typedef void(*RecycleFuncType)(void* e);
        void* v = NULL;
        mmUInt32_t i = 0;
        RecycleFuncType Recycle = (RecycleFuncType)p->allocator.Recycle;
        for (i = 0; i < p->chunk_size; i++)
        {
            v = mmPoolElementChunk_Buffer(e, i);
            (*(Recycle))(v);
        }
    }
}

MM_EXPORT_DLL void* mmPoolElement_Produce(struct mmPoolElement* p)
{
    void* v = NULL;
    struct mmPoolElementChunk* e = p->current;
    if (NULL == e)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // min.
        n = mmRb_First(&p->idle.rbt);
        if (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            e = (struct mmPoolElementChunk*)it->k;
            // rmv chunk to idle set.
            mmRbtsetVpt_Rmv(&p->idle, e);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        if (NULL == e)
        {
            // add new chunk.
            e = mmPoolElement_AddChunk(p);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        // cache the current chunk.
        p->current = e;
    }
    v = mmPoolElementChunk_Produce(e);
    if (mmPoolElementChunk_ProduceComplete(e))
    {
        p->current = NULL;
    }
    return v;
}
MM_EXPORT_DLL void mmPoolElement_Recycle(struct mmPoolElement* p, void* v)
{
    struct mmPoolElementChunk* e = mmPoolElement_GetChunk(p, (uintptr_t)v);
    assert(NULL != e && "NULL != e is a invalid.");
    mmPoolElementChunk_Recycle(e, v);
    if (mmPoolElementChunk_RecycleComplete(e))
    {
        float idle_sz = 0;
        float memb_sz = 0;
        int trim_number = 0;

        // reset the index for reused.
        mmPoolElementChunk_ResetIndex(e);
        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, e);
        // add chunk to idle set.
        mmRbtsetVpt_Add(&p->idle, e);
        // 
        idle_sz = (float)p->idle.size;
        memb_sz = (float)p->rbtree.size;
        trim_number = (int)(memb_sz * p->rate_trim_number);

        if (idle_sz / memb_sz > p->rate_trim_border)
        {
            int i = 0;
            // 
            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            // max
            n = mmRb_Last(&p->idle.rbt);
            while (NULL != n && i < trim_number)
            {
                it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                e = (struct mmPoolElementChunk*)it->k;
                n = mmRb_Prev(n);
                mmRbtsetVpt_Erase(&p->idle, it);
                mmPoolElement_RmvChunk(p, (uintptr_t)e->arrays);
                i++;
            }
        }
    }
}
