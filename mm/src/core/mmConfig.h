/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConfig_h__
#define __mmConfig_h__

#include "core/mmConfigPlatform.h"
#include "core/mmPlatform.h"
#include "core/mmTypes.h"

// export shared setting.
#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

// #define MM_WIN32 1
// #define MM_LINUX 2
// #define MM_APPLE 3
// #define MM_APPLE_IOS 4
// #define MM_ANDROID 5
// #define MM_NACL 6
// #define MM_WINRT 7
// #define MM_FLASHCC 8
// #define MM_FREEBSD 9

#ifndef MM_HAVE_SO_SNDLOWAT
#define MM_HAVE_SO_SNDLOWAT     1
#endif

#if !(MM_WIN32)

#define mmSignalHelper(n)     SIG##n
#define mmSignalValue(n)      mmSignalHelper(n)

#define mmRandom               random

/* TODO: #ifndef */
#define MM_SHUTDOWN_SIGNAL      QUIT
#define MM_TERMINATE_SIGNAL     TERM
#define MM_NOACCEPT_SIGNAL      WINCH
#define MM_RECONFIGURE_SIGNAL   HUP

#if (MM_LINUXTHREADS)
#define MM_REOPEN_SIGNAL        INFO
#define MM_CHANGEBIN_SIGNAL     XCPU
#else
#define MM_REOPEN_SIGNAL        USR1
#define MM_CHANGEBIN_SIGNAL     USR2
#endif

#define mmCDECL
#define mmLibcCDECL

#endif

#define MM_DEBUG MM_DEBUG_MODE

#define MM_INT32_LEN   (sizeof("-2147483648") - 1)
#define MM_INT64_LEN   (sizeof("-9223372036854775808") - 1)

#ifndef MM_ALIGNMENT
#define MM_ALIGNMENT   sizeof(unsigned long)    /* platform word */
#endif

#if (MM_PTR_SIZE == 4)
#define MM_INT_T_LEN        MM_INT32_LEN
#define MM_MAX_INT_T_VALUE  2147483647

#else
#define MM_INT_T_LEN        MM_INT64_LEN
#define MM_MAX_INT_T_VALUE  9223372036854775807
#endif

#define mmAlign(d, a)     (((d) + (a - 1)) & ~(a - 1))
#define mmAlignPtr(p, a)  (mmUChar_t*) (((uintptr_t) (p) + ((uintptr_t) a - 1)) & ~((uintptr_t) a - 1))

#define mmAbort       abort

#define  MM_SUCCESS     0
#define  MM_FAILURE     1
#define  MM_UNKNOWN    -1

#define  MM_OK          0
#define  MM_ERROR      -1
#define  MM_AGAIN      -2
#define  MM_BUSY       -3
#define  MM_DONE       -4
#define  MM_DECLINED   -5
#define  MM_ABORT      -6

#define  MM_TRUE        1
#define  MM_FALSE       0

#define MM_LF     (mmUChar_t) '\n'
#define MM_CR     (mmUChar_t) '\r'
#define MM_CRLF   "\r\n"

/* TODO: platform specific: array[MM_INVALID_ARRAY_INDEX] must cause SIGSEGV */
#define MM_INVALID_ARRAY_INDEX 0x80000000

/* TODO: auto_conf: ngx_inline   inline __inline __inline__ */
#ifndef mmInline
#define mmInline      inline
#endif

#ifndef MM_INADDR_NONE  /* Solaris */
#define MM_INADDR_NONE  ((unsigned int) -1)
#endif

#ifdef MAXHOSTNAMELEN
#define MM_MAXHOSTNAMELEN  MAXHOSTNAMELEN
#else
#define MM_MAXHOSTNAMELEN  256
#endif


#if ((__GNU__ == 2) && (__GNUC_MINOR__ < 8))
#define MM_MAX_UINT32_VALUE  (mmUInt32_t) 0xffffffffLL
#else
#define MM_MAX_UINT32_VALUE  (mmUInt32_t) 0xffffffff
#endif

#define MM_MAX_INT32_VALUE   (mmUInt32_t) 0x7fffffff

typedef pid_t mmPid_t;
//
#ifndef mmUnused
#if (defined __clang__ && __clang_major__ >= 3) || (defined __GNUC__ && __GNUC__ >= 3)
#define mmUnused __attribute__ ((__unused__))
#else
#define mmUnused
#endif
#endif /* mmUnused */

static const char MM_EOF_VALUE = EOF;

#include "core/mmSuffix.h"

#endif//__mmConfig_h__
