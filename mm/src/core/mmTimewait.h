/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTimewait_h__
#define __mmTimewait_h__

#include "core/mmCore.h"

#include <pthread.h>

#include "core/mmPrefix.h"

MM_EXPORT_DLL int
mmTimewait_MSecNearby(
    pthread_cond_t* signal_cond,
    pthread_mutex_t* signal_mutex,
    struct timeval* ntime,
    struct timespec* otime,
    mmULong_t _nearby_time);

MM_EXPORT_DLL int
mmTimewait_USecNearby(
    pthread_cond_t* signal_cond,
    pthread_mutex_t* signal_mutex,
    struct timeval* ntime,
    struct timespec* otime,
    mmULong_t _nearby_time);

#include "core/mmSuffix.h"

#endif//__mmTimewait_h__
