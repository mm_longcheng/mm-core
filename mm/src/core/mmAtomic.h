/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAtomic_h__
#define __mmAtomic_h__

#include "core/mmPrefix.h"

#include "core/mmCore.h"

#if (MM_HAVE_LIBATOMIC)

#define AO_REQUIRE_CAS
#include <atomic_ops.h>

#define MM_HAVE_ATOMIC_OPS  1

typedef long                        mmAtomicInt_t;
typedef AO_t                        mmAtomicUInt_t;
typedef volatile mmAtomicUInt_t     mmAtomic_t;

#if (MM_PTR_SIZE == 8)
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)
#else
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)
#endif

#define mmAtomicCmpSet(lock, old, new)                                    \
    AO_compare_and_swap(lock, old, new)
#define mmAtomicFetchAdd(value, add)                                      \
    AO_fetch_and_add(value, add)
#define mmMemoryBarrier()        AO_nop()
#define mmCpuPause()


#elif (MM_DARWIN_ATOMIC)

#if defined(__MAC_OS_X_VERSION_MIN_REQUIRED) && __MAC_OS_X_VERSION_MIN_REQUIRED < 101200
/*
 * use Darwin 8 atomic(3) and barrier(3) operations
 * optimized at run-time for UP and SMP
 */

#include <libkern/OSAtomic.h>

/* "bool" conflicts with perl's CORE/handy.h */
#if 0
#undef bool
#endif


#define MM_HAVE_ATOMIC_OPS  1

#if (MM_PTR_SIZE == 8)

typedef mmSInt64_t                  mmAtomicInt_t;
typedef mmUInt64_t                  mmAtomicUInt_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)

#define mmAtomicCmpSet(lock, old, new)                                    \
    OSAtomicCompareAndSwap64Barrier(old, new, (mmSInt64_t *) lock)

#define mmAtomicFetchAdd(value, add)                                      \
    (OSAtomicAdd64(add, (mmSInt64_t *) value) - add)

#else

typedef mmSInt32_t                  mmAtomicInt_t;
typedef mmUInt32_t                  mmAtomicUInt_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)

#define mmAtomicCmpSet(lock, old, new)                                    \
    OSAtomicCompareAndSwap32Barrier(old, new, (mmSInt32_t *) lock)

#define mmAtomicFetchAdd(value, add)                                      \
    (OSAtomicAdd32(add, (mmSInt32_t *) value) - add)

#endif

#define mmMemoryBarrier()        OSMemoryBarrier()

#define mmCpuPause()

typedef volatile mmAtomicUInt_t     mmAtomic_t;

#else
/* GCC 4.1 builtin atomic operations */

#define MM_HAVE_ATOMIC_OPS  1

typedef long                        mmAtomicInt_t;
typedef unsigned long               mmAtomicUInt_t;

#if (MM_PTR_SIZE == 8)
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)
#else
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)
#endif

typedef volatile mmAtomicUInt_t     mmAtomic_t;


#define mmAtomicCmpSet(lock, old, set)                                    \
    __sync_bool_compare_and_swap(lock, old, set)

#define mmAtomicFetchAdd(value, add)                                      \
    __sync_fetch_and_add(value, add)

#define mmMemoryBarrier()        __sync_synchronize()

#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define mmCpuPause()             __asm__ ("pause")
#else
#define mmCpuPause()
#endif

#endif

#elif (MM_HAVE_GCC_ATOMIC)

/* GCC 4.1 builtin atomic operations */

#define MM_HAVE_ATOMIC_OPS  1

typedef long                        mmAtomicInt_t;
typedef unsigned long               mmAtomicUInt_t;

#if (MM_PTR_SIZE == 8)
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)
#else
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)
#endif

typedef volatile mmAtomicUInt_t     mmAtomic_t;


#define mmAtomicCmpSet(lock, old, set)                                    \
    __sync_bool_compare_and_swap(lock, old, set)

#define mmAtomicFetchAdd(value, add)                                      \
    __sync_fetch_and_add(value, add)

#define mmMemoryBarrier()        __sync_synchronize()

#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define mmCpuPause()             __asm__ ("pause")
#else
#define mmCpuPause()
#endif


#elif ( __i386__ || __i386 )

typedef mmSInt32_t                  mmAtomicInt_t;
typedef mmUInt32_t                  mmAtomicUInt_t;
typedef volatile mmAtomicUInt_t     mmAtomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)


#if ( __SUNPRO_C )

#define MM_HAVE_ATOMIC_OPS  1

mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set);

mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add);

/*
 * Sun Studio 12 exits with segmentation fault on '__asm ("pause")',
 * so mmCpuPause is declared in src/os/unix/mmSunproX86.il
 */

void
mmCpuPause(void);

/* the code in src/os/unix/mmSunproX86.il */

#define mmMemoryBarrier()        __asm (".volatile"); __asm (".nonvolatile")


#else /* ( __GNUC__ || __INTEL_COMPILER ) */

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mmGCCAtomicX86.h"

#endif


#elif ( __amd64__ || __amd64 )

typedef mmSInt64_t                  mmAtomicInt_t;
typedef mmUInt64_t                  mmAtomicUInt_t;
typedef volatile mmAtomicUInt_t     mmAtomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)


#if ( __SUNPRO_C )

#define MM_HAVE_ATOMIC_OPS  1

mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set);

mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add);

/*
 * Sun Studio 12 exits with segmentation fault on '__asm ("pause")',
 * so mmCpuPause is declared in src/os/unix/mmSunproAmd64.il
 */

void
mmCpuPause(void);

/* the code in src/os/unix/mmSunproAmd64.il */

#define mmMemoryBarrier()        __asm (".volatile"); __asm (".nonvolatile")


#else /* ( __GNUC__ || __INTEL_COMPILER ) */

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mmGCCAtomicAmd64.h"

#endif


#elif ( __sparc__ || __sparc || __sparcv9 )

#if (MM_PTR_SIZE == 8)

typedef mmSInt64_t                  mmAtomicInt_t;
typedef mmUInt64_t                  mmAtomicUInt_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)

#else

typedef mmSInt32_t                  mmAtomicInt_t;
typedef mmUInt32_t                  mmAtomicUInt_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)

#endif

typedef volatile mmAtomicUInt_t     mmAtomic_t;


#if ( __SUNPRO_C )

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mmSunproAtomicSparc64.h"


#else /* ( __GNUC__ || __INTEL_COMPILER ) */

#define MM_HAVE_ATOMIC_OPS  1

#include "atomic/mmGCCAtomicSparc64.h"

#endif


#elif ( __powerpc__ || __POWERPC__ )

#define MM_HAVE_ATOMIC_OPS  1

#if (MM_PTR_SIZE == 8)

typedef mmSInt64_t                  mmAtomicInt_t;
typedef mmUInt64_t                  mmAtomicUInt_t;
#define MM_ATOMIC_T_LEN            (sizeof("-9223372036854775808") - 1)

#else

typedef mmSInt32_t                  mmAtomicInt_t;
typedef mmUInt32_t                  mmAtomicUInt_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)

#endif

typedef volatile mmAtomicUInt_t     mmAtomic_t;


#include "atomic/mmGCCAtomicPPC.h"

#elif _MSC_VER

#include "atomic/mmMSVSAtomic.h"

#endif


#if !(MM_HAVE_ATOMIC_OPS)

#define MM_HAVE_ATOMIC_OPS  0

typedef mmSInt32_t                  mmAtomicInt_t;
typedef mmUInt32_t                  mmAtomicUInt_t;
typedef volatile mmAtomicUInt_t     mmAtomic_t;
#define MM_ATOMIC_T_LEN            (sizeof("-2147483648") - 1)


static mmInline mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set)
{
     if (*lock == old)
     {
         *lock = set;
         return 1;
     }

     return 0;
}


static mmInline mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add)
{
     mmAtomicInt_t  old;

     old = *value;
     *value += add;

     return old;
}

#define mmMemoryBarrier()
#define mmCpuPause()

#endif

#include "core/mmSuffix.h"

#endif//__mmAtomic_h__
