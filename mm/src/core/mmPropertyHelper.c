/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPropertyHelper.h"

#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"
#include "core/mmUTFConvert.h"

MM_EXPORT_DLL
void
mmPropertyHelper_SetValue(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v,
    void*                                          func)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    mmAToTypeFunc AToType = (mmAToTypeFunc)(func);
    char hArgs[8] = { 0 };
    const char* pString = mmString_CStr(v);
    (*(AToType))(pString, hArgs);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetValue(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v,
    void*                                          func)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    mmTypeToAFunc TypeToA = (mmTypeToAFunc)(func);
    char hArgs[8] = { 0 };
    char pString[64] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    (*(TypeToA))(hArgs, pString);
    mmString_Assigns(v, pString);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetPointer(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    // void* => uintptr_t
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUIntptr);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetPointer(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    // uintptr_t => void*
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UIntptrToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUChar(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUChar);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUChar(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UCharToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSChar(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSChar);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSChar(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SCharToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUShort(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUShort);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUShort(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UShortToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSShort(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSShort);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSShort(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SShortToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUInt);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UIntToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSInt);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SIntToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetULong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToULong);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetULong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_ULongToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSLong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSLong);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSLong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SLongToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetULLong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToULLong);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetULLong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_ULLongToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSLLong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSLLong);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSLLong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SLLongToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt8(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUInt8);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt8(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UInt8ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt8(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSInt8);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt8(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UInt8ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt16(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUInt16);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt16(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UInt16ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt16(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSInt16);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt16(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SInt16ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt32(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUInt32);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt32(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UInt32ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt32(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSInt32);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt32(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SInt32ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt64(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUInt64);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt64(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UInt64ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt64(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSInt64);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt64(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SInt64ToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSize(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSize);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSize(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SizeToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetSIntptr(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToSIntptr);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetSIntptr(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_SIntptrToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUIntptr(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToUIntptr);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUIntptr(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_UIntptrToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetFloat(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToFloat);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetFloat(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_FloatToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetDouble(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    mmPropertyHelper_SetValue(p, obj, v, &mmValue_AToDouble);
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetDouble(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    mmPropertyHelper_GetValue(p, obj, v, &mmValue_DoubleToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetString(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, v);
    }
    else
    {
        struct mmString* pMemberPointer = (struct mmString*)
        (
            (char*)obj + p->hBaseOffset + p->hMemberOffset
        );
        mmString_Assign(pMemberPointer, v);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetString(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, v);
    }
    else
    {
        struct mmString* pMemberPointer = (struct mmString*)
        (
            (char*)obj + p->hBaseOffset + p->hMemberOffset
        );
        mmString_Assign(v, pMemberPointer);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetUtf16String(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, v);
    }
    else
    {
        // All attribute parameters are is utf8 format string.
        struct mmUtf16String* pMemberPointer = (struct mmUtf16String*)
        (
            (char*)obj + p->hBaseOffset + p->hMemberOffset
        );
        mmUTF_8to16(v, pMemberPointer);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetUtf16String(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, v);
    }
    else
    {
        // All attribute parameters are is utf8 format string.
        struct mmUtf16String* pMemberPointer = (struct mmUtf16String*)
        (
            (char*)obj + p->hBaseOffset + p->hMemberOffset
        );
        mmUTF_16to8(pMemberPointer, v);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetVec2(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    float hArgs[2] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(float), "(,)", 2, &mmValue_AToFloat);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetVec2(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    float hArgs[2] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(float), v, "(,)", 2, &mmValue_FloatToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetVec3(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    float hArgs[3] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(float), "(,,)", 3, &mmValue_AToFloat);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetVec3(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    float hArgs[3] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(float), v, "(,,)", 3, &mmValue_FloatToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetVec4(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    float hArgs[4] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(float), "(,,,)", 4, &mmValue_AToFloat);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetVec4(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    float hArgs[4] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(float), v, "(,,,)", 4, &mmValue_FloatToA);
}

MM_EXPORT_DLL
void
mmPropertyHelper_SetQuat(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v)
{
    typedef void(*SetterFuncType)(void* obj, const void* value);
    SetterFuncType hFunc = (SetterFuncType)(p->pSetterFunc);
    float hArgs[4] = { 0 };
    mmValueStringToTypeArray(v, hArgs, sizeof(float), "(,,,)", 4, &mmValue_AToFloat);
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_SetValueMember(p, obj, hArgs);
    }
}

MM_EXPORT_DLL
void
mmPropertyHelper_GetQuat(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v)
{
    typedef void(*GetterFuncType)(const void* obj, void* value);
    GetterFuncType hFunc = (GetterFuncType)(p->pGetterFunc);
    float hArgs[4] = { 0 };
    if (NULL != hFunc)
    {
        (*(hFunc))((char*)obj + p->hBaseOffset, hArgs);
    }
    else
    {
        mmProperty_GetValueMember(p, obj, hArgs);
    }
    mmValueTypeArrayToString(hArgs, sizeof(float), v, "(,,,)", 4, &mmValue_FloatToA);
}
