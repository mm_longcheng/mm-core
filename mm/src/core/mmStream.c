/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmStream.h"

#include "core/mmAlloc.h"
#include "core/mmByte.h"
#include "core/mmStreambuf.h"
#include "core/mmFileSystem.h"

MM_EXPORT_DLL const struct mmIStream mmStreamFILE =
{
    &mmFILEWrite,
    &mmFILERead,
    &mmFILESeek,
    &mmFILESeeko,
    &mmFILESeeko64,
    &mmFILETell,
    &mmFILETello,
    &mmFILETello64,
};

MM_EXPORT_DLL const struct mmIStream mmStreamStreambuf =
{
    &mmStreambufWrite,
    &mmStreambufRead,
    &mmStreambufSeek,
    &mmStreambufSeeko,
    &mmStreambufSeeko64,
    &mmStreambufTell,
    &mmStreambufTello,
    &mmStreambufTello64,
};

MM_EXPORT_DLL const struct mmIStream mmStreamByteBuffer =
{
    &mmByteBufferWrite,
    &mmByteBufferRead,
    &mmByteBufferSeek,
    &mmByteBufferSeeko,
    &mmByteBufferSeeko64,
    &mmByteBufferTell,
    &mmByteBufferTello,
    &mmByteBufferTello64,
};

MM_EXPORT_DLL
void
mmStream_Init(
    struct mmStream*                               p)
{
    p->i = NULL;
    p->s = NULL;
    p->t = -1;
}

MM_EXPORT_DLL
void
mmStream_Destroy(
    struct mmStream*                               p)
{
    assert(NULL == p->s && "assets is not destroy complete.");
    p->t = -1;
    p->s = NULL;
    p->i = NULL;
}

MM_EXPORT_DLL
int
mmStream_Invalid(
    const struct mmStream*                         p)
{
    return (NULL == p->i) || (NULL == p->s) || (-1 == p->t);
}

MM_EXPORT_DLL
size_t
mmStreamWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Write))(ptr, size, nmemb, s->s);
}

MM_EXPORT_DLL
size_t
mmStreamRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Read))(ptr, size, nmemb, s->s);
}

MM_EXPORT_DLL
int
mmStreamSeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Seek))(s->s, offset, whence);
}

MM_EXPORT_DLL
int
mmStreamSeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Seeko))(s->s, offset, whence);
}

MM_EXPORT_DLL
int
mmStreamSeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Seeko64))(s->s, offset, whence);
}

MM_EXPORT_DLL
long
mmStreamTell(
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Tell))(s->s);
}

MM_EXPORT_DLL
off_t
mmStreamTello(
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Tello))(s->s);
}

MM_EXPORT_DLL
int64_t
mmStreamTello64(
    void*                                          stream)
{
    struct mmStream* s = (struct mmStream*)(stream);
    return (*(s->i->Tello64))(s->s);
}

MM_EXPORT_DLL
size_t
mmFILEWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    FILE* s = (FILE*)(stream);
    return fwrite(ptr, size, nmemb, s);
}

MM_EXPORT_DLL
size_t
mmFILERead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    FILE* s = (FILE*)(stream);
    return fread(ptr, size, nmemb, s);
}

MM_EXPORT_DLL
int
mmFILESeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence)
{
    FILE* s = (FILE*)(stream);
    return fseek(s, offset, whence);
}

MM_EXPORT_DLL
int
mmFILESeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence)
{
    FILE* s = (FILE*)(stream);
    return fseeko(s, offset, whence);
}

MM_EXPORT_DLL
int
mmFILESeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence)
{
    FILE* s = (FILE*)(stream);
    return fseeko64(s, offset, whence);
}

MM_EXPORT_DLL
long
mmFILETell(
    void*                                          stream)
{
    FILE* s = (FILE*)(stream);
    return ftell(s);
}

MM_EXPORT_DLL
off_t
mmFILETello(
    void*                                          stream)
{
    FILE* s = (FILE*)(stream);
    return ftello(s);
}

MM_EXPORT_DLL
int64_t
mmFILETello64(
    void*                                          stream)
{
    FILE* s = (FILE*)(stream);
    return ftello64(s);
}

MM_EXPORT_DLL
size_t
mmStreambufWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    size_t count = size * nmemb;
    size_t remaining = s->pptr - s->gptr;
    // Read over end of memory.
    size_t cnt = (count > remaining) ? remaining : count;
    
    if (0 == cnt)
    {
        return 0;
    }
    else
    {
        assert(cnt <= count);

        mmMemcpy(s->buff + s->gptr, ptr, cnt);
        
        s->gptr += cnt;
        return cnt;
    }
}

MM_EXPORT_DLL
size_t
mmStreambufRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    size_t count = size * nmemb;
    size_t remaining = s->pptr - s->gptr;
    // Read over end of memory.
    size_t cnt = (count > remaining) ? remaining : count;
    
    if (0 == cnt)
    {
        return 0;
    }
    else
    {
        assert(cnt <= count);

        mmMemcpy(ptr, s->buff + s->gptr, cnt);
        
        s->gptr += cnt;
        return cnt;
    }
}

MM_EXPORT_DLL
int
mmStreambufSeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    size_t gptr = s->gptr;
    size_t pptr = s->pptr;
    
    switch(whence)
    {
    case SEEK_SET:
        gptr = offset;
        break;
    case SEEK_END:
        gptr = pptr + offset;
        break;
    case SEEK_CUR:
        gptr = gptr + offset;
        break;
    default:
        return -1;
    }
    
    if (0 <= gptr && gptr <= pptr)
    {
        s->gptr = gptr;
        return 0;
    }
    else
    {
        return -1;
    }
}

MM_EXPORT_DLL
int
mmStreambufSeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    size_t gptr = s->gptr;
    size_t pptr = s->pptr;
    
    switch(whence)
    {
    case SEEK_SET:
        gptr = offset;
        break;
    case SEEK_END:
        gptr = pptr + offset;
        break;
    case SEEK_CUR:
        gptr = gptr + offset;
        break;
    default:
        return -1;
    }
    
    if (0 <= gptr && gptr <= pptr)
    {
        s->gptr = gptr;
        return 0;
    }
    else
    {
        return -1;
    }
}

MM_EXPORT_DLL
int
mmStreambufSeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    size_t gptr = s->gptr;
    size_t pptr = s->pptr;
    
    switch(whence)
    {
    case SEEK_SET:
        gptr = offset;
        break;
    case SEEK_END:
        gptr = pptr + offset;
        break;
    case SEEK_CUR:
        gptr = gptr + offset;
        break;
    default:
        return -1;
    }
    
    if (0 <= gptr && gptr <= pptr)
    {
        s->gptr = gptr;
        return 0;
    }
    else
    {
        return -1;
    }
}

MM_EXPORT_DLL
long
mmStreambufTell(
    void*                                          stream)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    return (long)(s->gptr);
}

MM_EXPORT_DLL
off_t
mmStreambufTello(
    void*                                          stream)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    return (off_t)(s->gptr);
}

MM_EXPORT_DLL
int64_t
mmStreambufTello64(
    void*                                          stream)
{
    struct mmStreambuf* s = (struct mmStreambuf*)(stream);
    
    return (int64_t)(s->gptr);
}

MM_EXPORT_DLL
size_t
mmByteBufferWrite(
    const void*                                    ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    size_t count = size * nmemb;
    size_t remaining = s->length - s->offset;
    // Read over end of memory.
    size_t cnt = (count > remaining) ? remaining : count;
    
    if (0 == cnt)
    {
        return 0;
    }
    else
    {
        assert(cnt <= count);

        memcpy(s->buffer + s->offset, ptr, cnt);
        
        s->offset += cnt;
        return cnt;
    }
}

MM_EXPORT_DLL
size_t
mmByteBufferRead(
    void*                                          ptr,
    size_t                                         size,
    size_t                                         nmemb,
    void*                                          stream)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    size_t count = size * nmemb;
    size_t remaining = s->length - s->offset;
    // Read over end of memory.
    size_t cnt = (count > remaining) ? remaining : count;
    
    if (0 == cnt)
    {
        return 0;
    }
    else
    {
        assert(cnt <= count);

        memcpy(ptr, s->buffer + s->offset, cnt);
        
        s->offset += cnt;
        return cnt;
    }
}

MM_EXPORT_DLL
int
mmByteBufferSeek(
    void*                                          stream,
    long                                           offset,
    int                                            whence)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    size_t gptr = s->offset;
    size_t pptr = s->length;
    
    switch(whence)
    {
    case SEEK_SET:
        gptr = offset;
        break;
    case SEEK_END:
        gptr = pptr + offset;
        break;
    case SEEK_CUR:
        gptr = gptr + offset;
        break;
    default:
        return -1;
    }
    
    if (0 <= gptr && gptr <= pptr)
    {
        s->offset = gptr;
        return 0;
    }
    else
    {
        return -1;
    }
}

MM_EXPORT_DLL
int
mmByteBufferSeeko(
    void*                                          stream,
    off_t                                          offset,
    int                                            whence)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    size_t gptr = s->offset;
    size_t pptr = s->length;
    
    switch(whence)
    {
    case SEEK_SET:
        gptr = offset;
        break;
    case SEEK_END:
        gptr = pptr + offset;
        break;
    case SEEK_CUR:
        gptr = gptr + offset;
        break;
    default:
        return -1;
    }
    
    if (0 <= gptr && gptr <= pptr)
    {
        s->offset = gptr;
        return 0;
    }
    else
    {
        return -1;
    }
}

MM_EXPORT_DLL
int
mmByteBufferSeeko64(
    void*                                          stream,
    int64_t                                        offset,
    int                                            whence)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    size_t gptr = s->offset;
    size_t pptr = s->length;
    
    switch(whence)
    {
    case SEEK_SET:
        gptr = offset;
        break;
    case SEEK_END:
        gptr = pptr + offset;
        break;
    case SEEK_CUR:
        gptr = gptr + offset;
        break;
    default:
        return -1;
    }
    
    if (0 <= gptr && gptr <= pptr)
    {
        s->offset = gptr;
        return 0;
    }
    else
    {
        return -1;
    }
}

MM_EXPORT_DLL
long
mmByteBufferTell(
    void*                                          stream)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    return (long)(s->offset);
}

MM_EXPORT_DLL
off_t
mmByteBufferTello(
    void*                                          stream)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    return (off_t)(s->offset);
}

MM_EXPORT_DLL
int64_t
mmByteBufferTello64(
    void*                                          stream)
{
    struct mmByteBuffer* s = (struct mmByteBuffer*)(stream);
    
    return (int64_t)(s->offset);
}
