/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmStringUtils_h__
#define __mmStringUtils_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

// " abc " ==> 1
MM_EXPORT_DLL
size_t
mmString_FindRSidePrint(
    const struct mmString*                         p);

// " abc " ==> 1
MM_EXPORT_DLL
size_t
mmString_FindLSidePrint(
    const struct mmString*                         p);

// " abc " ==> "abc"
MM_EXPORT_DLL
void
mmString_RemoveBothSidesSpace(
    struct mmString*                               s);

MM_EXPORT_DLL
size_t
mmString_ReplaceCStr(
    struct mmString*                               p, 
    const char*                                    s, 
    const char*                                    t);

// ("abc", 5) ==> "abc  "
MM_EXPORT_DLL
void
mmString_LAlignAppends(
    struct mmString*                               p,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length);

// ("abc", 5) ==> " abc "
// ("abc", 6) ==> " abc  "
MM_EXPORT_DLL
void
mmString_MAlignAppends(
    struct mmString*                               p,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length);

// ("abc", 5) ==> "  abc"
MM_EXPORT_DLL
void
mmString_RAlignAppends(
    struct mmString*                               p,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length);

/*
 * This function transform string to type[].
 *
 * @param v "(1,2,3,4)".
 * @param a type a[4].
 * @param z sizeof(type).
 * @param s "(,,,)".
 * @param d 4.
 * @param f void(*mmAToTypeFunc)(const char*, type*).
 */
MM_EXPORT_DLL
void
mmValueStringToTypeArray(
    const struct mmString*                         v,
    void*                                          a,
    size_t                                         z,
    const char*                                    s,
    int                                            d,
    void*                                          f);

/*
 * This function transform type[] to string.
 *
 * @param a type a[4].
 * @param z sizeof(type).
 * @param v "(1,2,3,4)".
 * @param s "(,,,)".
 * @param d 4.
 * @param f void(*mmTypeToAFunc)(const void*, char*).
 */
MM_EXPORT_DLL
void
mmValueTypeArrayToString(
    const void*                                    a,
    size_t                                         z,
    struct mmString*                               v,
    const char*                                    s,
    int                                            d,
    void*                                          f);

/*
 * This function transform string format to type[].
 *
 * @param v "((1,2),(3,4))".
 * @param a type a[4].
 * @param z sizeof(type).
 * @param s "((,),(,))".
 * @param x int x[] = { 1, 2, 4, 5 }.
 * @param d 4.
 * @param f void(*mmAToTypeFunc)(const char*, type*).
 */
MM_EXPORT_DLL
void
mmValueStringFormatToTypeArray(
    const struct mmString*                         v,
    void*                                          a,
    size_t                                         z,
    const char*                                    s,
    const int*                                     x,
    int                                            d,
    void*                                          f);

/*
 * This function transform type[] to string format.
 *
 * @param a type a[4].
 * @param z sizeof(type).
 * @param v "((1,2),(3,4))".
 * @param s "((,),(,))".
 * @param x int x[] = { 1, 2, 4, 5 }.
 * @param d 4.
 * @param f void(*mmTypeToAFunc)(const void*, char*).
 */
MM_EXPORT_DLL
void
mmValueTypeArrayToStringFormat(
    const void*                                    a,
    size_t                                         z,
    struct mmString*                               v,
    const char*                                    s,
    const int*                                     x,
    int                                            d,
    void*                                          f);

/*
 * This function transform string to type[].
 *
 * @param v " 1 2 3 4 ".
 * @param a type a[4].
 * @param z sizeof(type).
 * @param s ' '.
 * @param d 4.
 * @param f void(*mmAToTypeFunc)(const char*, type*).
 */
MM_EXPORT_DLL
void
mmValueSplitStringToTypeArray(
    const struct mmString*                         v,
    void*                                          a,
    size_t                                         z,
    char                                           s,
    int                                            d,
    void*                                          f);

/*
 * This function transform type[] to string.
 *
 * @param a type a[4].
 * @param z sizeof(type).
 * @param v " 1 2 3 4 ".
 * @param s ' '.
 * @param d 4.
 * @param f void(*mmTypeToAFunc)(const void*, char*).
 */
MM_EXPORT_DLL
void
mmValueTypeArrayToSplitString(
    const void*                                    a,
    size_t                                         z,
    struct mmString*                               v,
    char                                           s,
    int                                            d,
    void*                                          f);

/*
 * This function transform float[2] size to string format.
 * Use for debug infomation.
 *
 * @param hSize   float[2] (w, h).
 * @param hString out put string "%.02f".
 */
MM_EXPORT_DLL
void
mmFloatSizeToString(
    float                                          hSize[2],
    char                                           hString[128]);

/*
 * This function transform float[4] rect to string format.
 * Use for debug infomation.
 *
 * @param hSize   float[4] (x, y, w, h).
 * @param hString out put string "%.02f".
 */
MM_EXPORT_DLL
void
mmFloatRectToString(
    float                                          hRect[4], 
    char                                           hString[128]);

/*
 * This function transform double[2] size to string format.
 * Use for debug infomation.
 *
 * @param hSize   double[2] (w, h).
 * @param hString out put string "%.02lf".
 */
MM_EXPORT_DLL
void
mmDoubleSizeToString(
    double                                         hSize[2], 
    char                                           hString[128]);

/*
 * This function transform double[4] rect to string format.
 * Use for debug infomation.
 *
 * @param hSize   double[4] (x, y, w, h).
 * @param hString out put string "%.02lf".
 */
MM_EXPORT_DLL
void
mmDoubleRectToString(
    double                                         hRect[4], 
    char                                           hString[128]);

// "abcbd" find 'b' ==> 1
MM_EXPORT_DLL
size_t
mmCStringFindNextOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max);

// "abcbd" find 'b' ==> 3
MM_EXPORT_DLL
size_t
mmCStringFindPrevOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max);

// "aacba" find ot 'a' ==> 2
MM_EXPORT_DLL
size_t
mmCStringFindNextNotOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max);

// "aacba" find ot 'a' ==> 3
MM_EXPORT_DLL
size_t
mmCStringFindPrevNotOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max);

// "abbcbbd" find "bb" ==> 1
MM_EXPORT_DLL
size_t
mmCStringFindNextStringOf(
    const char*                                    s,
    const char*                                    c,
    size_t                                         i,
    size_t                                         max);

// "abbcbbd" find "bb" ==> 4
MM_EXPORT_DLL
size_t
mmCStringFindPrevStringOf(
    const char*                                    s,
    const char*                                    c,
    size_t                                         i,
    size_t                                         max);

MM_EXPORT_DLL
size_t
mmCStringTolower(
    const char*                                    s,
    size_t                                         z,
    char*                                          t,
    size_t                                         m);

MM_EXPORT_DLL
size_t
mmCStringToupper(
    const char*                                    s,
    size_t                                         z,
    char*                                          t,
    size_t                                         m);

// ("abc", 5) ==> "abc  "
MM_EXPORT_DLL
void
mmCStrLAlignAppends(
    char*                                          str,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length);

// ("abc", 5) ==> " abc "
// ("abc", 6) ==> " abc  "
MM_EXPORT_DLL
void
mmCStrMAlignAppends(
    char*                                          str,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length);

// ("abc", 5) ==> "  abc"
MM_EXPORT_DLL
void
mmCStrRAlignAppends(
    char*                                          str,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length);

// " abc " ==> 1
MM_EXPORT_DLL
size_t
mmCStringFindRSidePrint(
    const char*                                    s,
    size_t                                         l);

// " abc " ==> 1
MM_EXPORT_DLL
size_t
mmCStringFindLSidePrint(
    const char*                                    s,
    size_t                                         l);

// " abc " ==> "abc"
MM_EXPORT_DLL
void
mmCStringRemoveBothSidesSpace(
    const char*                                    s,
    size_t                                         l,
    size_t*                                        idx,
    size_t*                                        size);

MM_EXPORT_DLL
char
mmCStringLSidePrint(
    const char*                                    s,
    size_t                                         l);

MM_EXPORT_DLL
char
mmCStringRSidePrint(
    const char*                                    s,
    size_t                                         l);

#include "core/mmSuffix.h"

#endif//__mmStringUtils_h__
