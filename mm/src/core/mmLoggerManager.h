/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmLoggerManager_h__
#define __mmLoggerManager_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmLogger.h"
#include "core/mmThread.h"
#include "core/mmAtomic.h"
#include <stdlib.h>
#include <pthread.h>

#include "core/mmPrefix.h"

struct mmLoggerManagerCallback
{
    void(*ConsolePrintf)(const char* section, const char* timestamp, int lvl, const char* message);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmLoggerManagerCallback_Init(struct mmLoggerManagerCallback* p);
MM_EXPORT_DLL void mmLoggerManagerCallback_Destroy(struct mmLoggerManagerCallback* p);

#define MM_LOGGER_MANAGER_FILE_FOPEN_MODE_DEFAULT "ab+"

struct mmLoggerManager
{
    // default is mmLogger_Instance.
    struct mmLogger* logger_impl;
    // strong ref.
    FILE* stream;
    // default "mm"
    struct mmString logger_section;
    // default "log"
    struct mmString logger_path;
    // default "mm"
    struct mmString file_name;
    // default "ab+"
    struct mmString file_fopen_mode;
    struct mmString current_file_prefix;
    struct mmString current_file_name;
    struct mmString logger_file_name;
    // default is os console printf.
    struct mmLoggerManagerCallback callback;
    //
    mmAtomic_t locker;
    mmAtomic_t locker_stream;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    // thread for check file size.
    pthread_t file_size_thread;

    // default 100.
    size_t rolling_index;
    // default 0.
    size_t current_index;
    // default 300 * 1024 * 1024 (300M).
    size_t file_size;

    // default MM_LOG_VERBOSE.
    mmUInt32_t logger_level;
    // default 5000ms.
    mmMSec_t check_file_size_time;
    // we not use byte field, macosx compile different.
    // default is rolling(1)
    mmSInt8_t is_rolling;
    // default is not console(0)
    mmSInt8_t is_console;
    // default is not(0)
    mmSInt8_t is_immediately;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_DLL extern struct mmLoggerManager* mmLoggerManager_Instance(void);

MM_EXPORT_DLL void mmLoggerManager_Init(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Destroy(struct mmLoggerManager* p);

/* locker order is
*     locker
*/
MM_EXPORT_DLL void mmLoggerManager_Lock(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Unlock(struct mmLoggerManager* p);

MM_EXPORT_DLL void mmLoggerManager_SetFileName(struct mmLoggerManager* p, const char* file_name);
MM_EXPORT_DLL void mmLoggerManager_SetFileFopenMode(struct mmLoggerManager* p, const char* file_fopen_mode);
MM_EXPORT_DLL void mmLoggerManager_SetLoggerPath(struct mmLoggerManager* p, const char* logger_path);
MM_EXPORT_DLL void mmLoggerManager_SetFileSize(struct mmLoggerManager* p, size_t file_size);
MM_EXPORT_DLL void mmLoggerManager_SetLoggerLevel(struct mmLoggerManager* p, mmUInt32_t logger_level);
MM_EXPORT_DLL void mmLoggerManager_SetCheckFileSizeTime(struct mmLoggerManager* p, mmMSec_t check_file_size_time);
MM_EXPORT_DLL void mmLoggerManager_SetRollingIndex(struct mmLoggerManager* p, size_t rolling_index);
MM_EXPORT_DLL void mmLoggerManager_SetIsRolling(struct mmLoggerManager* p, mmSInt8_t is_rolling);
MM_EXPORT_DLL void mmLoggerManager_SetIsConsole(struct mmLoggerManager* p, mmSInt8_t is_console);
MM_EXPORT_DLL void mmLoggerManager_SetIsImmediately(struct mmLoggerManager* p, mmSInt8_t is_immediately);
// assign callback.
MM_EXPORT_DLL void mmLoggerManager_SetCallback(struct mmLoggerManager* p, const struct mmLoggerManagerCallback* callback);
// assign logger instance.default is mmLogger_Instance.
MM_EXPORT_DLL void mmLoggerManager_SetLogger(struct mmLoggerManager* p, struct mmLogger* logger);

MM_EXPORT_DLL void mmLoggerManager_FOpen(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Close(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Flush(struct mmLoggerManager* p);

MM_EXPORT_DLL void mmLoggerManager_CheckFileSize(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_GenerateCurrentFileName(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_GenerateCurrentFilePrefix(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_GenerateLoggerFileName(struct mmLoggerManager* p);

MM_EXPORT_DLL void mmLoggerManager_OnFinishLaunching(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_OnBeforeTerminate(struct mmLoggerManager* p);

MM_EXPORT_DLL void mmLoggerManager_Start(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Interrupt(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Shutdown(struct mmLoggerManager* p);
MM_EXPORT_DLL void mmLoggerManager_Join(struct mmLoggerManager* p);

MM_EXPORT_DLL void mmLoggerManager_LoggerSection(struct mmLogger* p, const char* section, mmUInt32_t lvl, const char* message);


#include "core/mmSuffix.h"

#endif//__mmLoggerManager_h__
