/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTimeval_h__
#define __mmTimeval_h__
//
#include "core/mmCore.h"

#include "core/mmPrefix.h"
//

/**
 This function will init timeval.

 @param p The time value to normalize.
 */
MM_EXPORT_DLL void mmTimeval_Init(struct timeval* p);

/**
 This function will destroy timeval.

 @param p The time value to normalize.
 */
MM_EXPORT_DLL void mmTimeval_Destroy(struct timeval* p);

/**
 This function will reset timeval.

 @param p The time value to normalize.
 */
MM_EXPORT_DLL void mmTimeval_Reset(struct timeval* p);

/**
 This function will normalize timeval.

 @param p The time value to normalize.
 */
MM_EXPORT_DLL void mmTimeval_Normalize(struct timeval* p);

/**
 This function will check if \a t1 is equal to \a t2.

 @param p The first time value to compare.
 @param q The second time value to compare.
 @return Non-zero if both time values are equal.
 */
static mmInline int mmTimeval_Equal(const struct timeval* p, const struct timeval* q)
{
    return (p->tv_sec == q->tv_sec && p->tv_usec == q->tv_usec);
}

/**
 This macro will check if \a t1 is greater than \a t2

 @param p The first time value to compare.
 @param q The second time value to compare.
 @return Non-zero if t1 is greater than t2.
 */
static mmInline int mmTimeval_Greater(const struct timeval* p, const struct timeval* q)
{
    return (p->tv_sec > q->tv_sec || (p->tv_sec == q->tv_sec && p->tv_usec > q->tv_usec));
}

/**
 This macro will check if \a t1 is greater than or equal to \a t2

 @param p The first time value to compare.
 @param q The second time value to compare.
 @return Non-zero if t1 is greater than or equal to t2.
 */
static mmInline int mmTimeval_GreaterOrEqual(const struct timeval* p, const struct timeval* q)
{
    return mmTimeval_Greater(p, q) || mmTimeval_Equal(p, q);
}

/**
 This macro will check if \a t1 is less than \a t2

 @param p The first time value to compare.
 @param q The second time value to compare.
 @return Non-zero if t1 is less than t2.
 */
static mmInline int mmTimeval_Less(const struct timeval* p, const struct timeval* q)
{
    return !mmTimeval_GreaterOrEqual(p, q);
}

/**
 This macro will check if \a t1 is less than or equal to \a t2.

 @param p The first time value to compare.
 @param q The second time value to compare.
 @return Non-zero if t1 is less than or equal to t2.
 */
static mmInline int mmTimeval_LessOrEqual(const struct timeval* p, const struct timeval* q)
{
    return !mmTimeval_Greater(p, q);
}

/**
This function will compare \a t1 \a t2.

@param p The first time value to compare.
@param q The second time value to compare.
@return compare value.
*/
MM_EXPORT_DLL int mmTimeval_Compare(const struct timeval* p, const struct timeval* q);

/**
 Add \a t2 to \a t1 and store the result in \a t1. Effectively
 this macro will expand as: (\a t1 += \a t2).

 @param p The time value to add.
 @param q The time value to be added to \a t1.
 */
static mmInline void mmTimeval_Add(struct timeval* p, const struct timeval* q)
{
    p->tv_sec += q->tv_sec;
    p->tv_usec += q->tv_usec;
    mmTimeval_Normalize(p);
}

/**
 Substract \a t2 from \a t1 and store the result in \a t1. Effectively
 this macro will expand as (\a t1 -= \a t2).

 @param p The time value to subsctract.
 @param q The time value to be substracted from \a t1.
 */
static mmInline void mmTimeval_Sub(struct timeval* p, const struct timeval* q)
{
    p->tv_sec -= q->tv_sec;
    p->tv_usec -= q->tv_usec;
    mmTimeval_Normalize(p);
}

/**
This function will return timeval usec to uint64.

@param p The time value to normalize.
@param timecode The time usec out.
*/
MM_EXPORT_DLL void mmTimeval_Usec(const struct timeval* p, mmUInt64_t* timecode);

/**
This function will return timeval usec to uint64.

@param p The time value to normalize.
@return timecode The time usec out.
*/
MM_EXPORT_DLL mmUInt64_t mmTimeval_ToUSec(const struct timeval* p);
//
#include "core/mmSuffix.h"

#endif//__mmTimeval_h__
