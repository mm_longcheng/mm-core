/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmValueTransform_h__
#define __mmValueTransform_h__

#include "core/mmConfig.h"
#include "core/mmTypes.h"

#include "core/mmPrefix.h"

// _CVTBUFSIZE is the maximum size for the per-thread conversion buffer.  It
// should be at least as long as the number of digits in the largest double
// precision value (?.?e308 in IEEE arithmetic).  We will use the same size
// buffer as is used in the printf support routines.
//
// (This value actually allows 40 additional decimal places; even though there
// are only 16 digits of accuracy in a double precision IEEE number, the user may
// ask for more to effect zero padding.)
//
// # of digits in max. dp value + slop
#define MM_CVTBUFSIZE (309 + 40) 

// double to ascii.
// precision value (?.?e308 in IEEE arithmetic).
MM_EXPORT_DLL char* mmEcvt(double arg, int ndigits, int* decpt, int* sign);
MM_EXPORT_DLL char* mmFcvt(double arg, int ndigits, int* decpt, int* sign);
MM_EXPORT_DLL char* mmGcvt(double number, int ndigit, char* buf);

// uint32 uint64 to decimal string.
MM_EXPORT_DLL int mmUI32ToDecA(uint32_t value, char* string);
MM_EXPORT_DLL int mmUI64ToDecA(uint64_t value, char* string);

// IEEE754 version gcvt. It is high precision.
MM_EXPORT_DLL char* mmIEEE754GcvtF(float number, int ndigit, char* buf);
MM_EXPORT_DLL char* mmIEEE754GcvtD(double number, int ndigit, char* buf);

// integer to ascii, high performance version. The basic api.
MM_EXPORT_DLL char* mmIToA(int value, char* string, int radix);
MM_EXPORT_DLL char* mmUIToA(unsigned int value, char* string, int radix);
MM_EXPORT_DLL char* mmLToA(long value, char* string, int radix);
MM_EXPORT_DLL char* mmULToA(unsigned long value, char* string, int radix);
MM_EXPORT_DLL char* mmLLToA(long long value, char* string, int radix);
MM_EXPORT_DLL char* mmULLToA(unsigned long long value, char* string, int radix);

// ascii to integer, high performance version. The basic api.
MM_EXPORT_DLL int mmAToI(const char* string);
MM_EXPORT_DLL unsigned int mmAToUI(const char* string);
MM_EXPORT_DLL long mmAToL(const char* string);
MM_EXPORT_DLL unsigned long mmAToUL(const char* string);
MM_EXPORT_DLL long long mmAToLL(const char* string);
MM_EXPORT_DLL unsigned long long mmAToULL(const char* string);

// ascii hex digit
MM_EXPORT_DLL extern const char* mmHexDigit;
MM_EXPORT_DLL int mmHexCStrToDigit(unsigned int* n, const char* s);
MM_EXPORT_DLL int mmHexBuffToDigit(unsigned int* n, const char* s, int l);

typedef void(*mmTypeToAFunc)(const void*, char*);
typedef void(*mmAToTypeFunc)(const char*, void*);

// integer to ascii, high performance version.
MM_EXPORT_DLL void mmValue_SCharToA(const mmSChar_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UCharToA(const mmUChar_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SShortToA(const mmSShort_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UShortToA(const mmUShort_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SIntToA(const mmSInt_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_UIntToA(const mmUInt_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_SLongToA(const mmSLong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_ULongToA(const mmULong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SLLongToA(const mmSLLong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_ULLongToA(const mmULLong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SInt8ToA(const mmSInt8_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UInt8ToA(const mmUInt8_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SInt16ToA(const mmSInt16_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UInt16ToA(const mmUInt16_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SInt32ToA(const mmSInt32_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_UInt32ToA(const mmUInt32_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_SInt64ToA(const mmSInt64_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_UInt64ToA(const mmUInt64_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SizeToA(const mmSize_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SIntptrToA(const mmSIntptr_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_UIntptrToA(const mmUIntptr_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_FloatToA(const float* v, char s[32]);
MM_EXPORT_DLL void mmValue_DoubleToA(const double* v, char s[64]);
MM_EXPORT_DLL void mmValue_BoolToA(const int* v, char s[8]);

// ascii to integer, high performance version.
MM_EXPORT_DLL void mmValue_AToSChar(const char* s, mmSChar_t* v);
MM_EXPORT_DLL void mmValue_AToUChar(const char* s, mmUChar_t* v);
MM_EXPORT_DLL void mmValue_AToSShort(const char* s, mmSShort_t* v);
MM_EXPORT_DLL void mmValue_AToUShort(const char* s, mmUShort_t* v);
MM_EXPORT_DLL void mmValue_AToSInt(const char* s, mmSInt_t* v);
MM_EXPORT_DLL void mmValue_AToUInt(const char* s, mmUInt_t* v);
MM_EXPORT_DLL void mmValue_AToSLong(const char* s, mmSLong_t* v);
MM_EXPORT_DLL void mmValue_AToULong(const char* s, mmULong_t* v);
MM_EXPORT_DLL void mmValue_AToSLLong(const char* s, mmSLLong_t* v);
MM_EXPORT_DLL void mmValue_AToULLong(const char* s, mmULLong_t* v);
MM_EXPORT_DLL void mmValue_AToSInt8(const char* s, mmSInt8_t* v);
MM_EXPORT_DLL void mmValue_AToUInt8(const char* s, mmUInt8_t* v);
MM_EXPORT_DLL void mmValue_AToSInt16(const char* s, mmSInt16_t* v);
MM_EXPORT_DLL void mmValue_AToUInt16(const char* s, mmUInt16_t* v);
MM_EXPORT_DLL void mmValue_AToSInt32(const char* s, mmSInt32_t* v);
MM_EXPORT_DLL void mmValue_AToUInt32(const char* s, mmUInt32_t* v);
MM_EXPORT_DLL void mmValue_AToSInt64(const char* s, mmSInt64_t* v);
MM_EXPORT_DLL void mmValue_AToUInt64(const char* s, mmUInt64_t* v);
MM_EXPORT_DLL void mmValue_AToSize(const char* s, mmSize_t* v);
MM_EXPORT_DLL void mmValue_AToSIntptr(const char* s, mmSIntptr_t* v);
MM_EXPORT_DLL void mmValue_AToUIntptr(const char* s, mmUIntptr_t* v);
MM_EXPORT_DLL void mmValue_AToFloat(const char* s, float* v);
MM_EXPORT_DLL void mmValue_AToDouble(const char* s, double* v);
MM_EXPORT_DLL void mmValue_AToBool(const char* s, mmBool_t* v);

// String to Value. low performance.
MM_EXPORT_DLL void mmValue_StringToSChar(const char* s, mmSChar_t* v);
MM_EXPORT_DLL void mmValue_StringToUChar(const char* s, mmUChar_t* v);
MM_EXPORT_DLL void mmValue_StringToSShort(const char* s, mmSShort_t* v);
MM_EXPORT_DLL void mmValue_StringToUShort(const char* s, mmUShort_t* v);
MM_EXPORT_DLL void mmValue_StringToSInt(const char* s, mmSInt_t* v);
MM_EXPORT_DLL void mmValue_StringToUInt(const char* s, mmUInt_t* v);
MM_EXPORT_DLL void mmValue_StringToSLong(const char* s, mmSLong_t* v);
MM_EXPORT_DLL void mmValue_StringToULong(const char* s, mmULong_t* v);
MM_EXPORT_DLL void mmValue_StringToSLLong(const char* s, mmSLLong_t* v);
MM_EXPORT_DLL void mmValue_StringToULLong(const char* s, mmULLong_t* v);
MM_EXPORT_DLL void mmValue_StringToSInt8(const char* s, mmSInt8_t* v);
MM_EXPORT_DLL void mmValue_StringToUInt8(const char* s, mmUInt8_t* v);
MM_EXPORT_DLL void mmValue_StringToSInt16(const char* s, mmSInt16_t* v);
MM_EXPORT_DLL void mmValue_StringToUInt16(const char* s, mmUInt16_t* v);
MM_EXPORT_DLL void mmValue_StringToSInt32(const char* s, mmSInt32_t* v);
MM_EXPORT_DLL void mmValue_StringToUInt32(const char* s, mmUInt32_t* v);
MM_EXPORT_DLL void mmValue_StringToSInt64(const char* s, mmSInt64_t* v);
MM_EXPORT_DLL void mmValue_StringToUInt64(const char* s, mmUInt64_t* v);
MM_EXPORT_DLL void mmValue_StringToSize(const char* s, mmSize_t* v);
MM_EXPORT_DLL void mmValue_StringToSIntptr(const char* s, mmSIntptr_t* v);
MM_EXPORT_DLL void mmValue_StringToUIntptr(const char* s, mmUIntptr_t* v);
MM_EXPORT_DLL void mmValue_StringToFloat(const char* s, float* v);
MM_EXPORT_DLL void mmValue_StringToDouble(const char* s, double* v);
MM_EXPORT_DLL void mmValue_StringToBool(const char* s, int* v);
// Value to String.
MM_EXPORT_DLL void mmValue_SCharToString(const mmSChar_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UCharToString(const mmUChar_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SShortToString(const mmSShort_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UShortToString(const mmUShort_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SIntToString(const mmSInt_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_UIntToString(const mmUInt_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_SLongToString(const mmSLong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_ULongToString(const mmULong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SLLongToString(const mmSLLong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_ULLongToString(const mmULLong_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SInt8ToString(const mmSInt8_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UInt8ToString(const mmUInt8_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SInt16ToString(const mmSInt16_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_UInt16ToString(const mmUInt16_t* v, char s[8]);
MM_EXPORT_DLL void mmValue_SInt32ToString(const mmSInt32_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_UInt32ToString(const mmUInt32_t* v, char s[16]);
MM_EXPORT_DLL void mmValue_SInt64ToString(const mmSInt64_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_UInt64ToString(const mmUInt64_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SizeToString(const mmSize_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_SIntptrToString(const mmSIntptr_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_UIntptrToString(const mmUIntptr_t* v, char s[32]);
MM_EXPORT_DLL void mmValue_FloatToString(const float* v, char s[32]);
MM_EXPORT_DLL void mmValue_DoubleToString(const double* v, char s[64]);
MM_EXPORT_DLL void mmValue_BoolToString(const int* v, char s[8]);

#include "core/mmSuffix.h"

#endif//__mmValueTransform_h__
