/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimeval.h"
#include "core/mmTime.h"
//
MM_EXPORT_DLL void mmTimeval_Init(struct timeval* p)
{
    p->tv_sec = 0;
    p->tv_usec = 0;
}
MM_EXPORT_DLL void mmTimeval_Destroy(struct timeval* p)
{
    p->tv_sec = 0;
    p->tv_usec = 0;
}
MM_EXPORT_DLL void mmTimeval_Reset(struct timeval* p)
{
    p->tv_sec = 0;
    p->tv_usec = 0;
}
//
MM_EXPORT_DLL void mmTimeval_Normalize(struct timeval* p)
{
    p->tv_sec += (p->tv_usec / MM_USEC_PER_SEC);
    p->tv_usec = (p->tv_usec % MM_USEC_PER_SEC);
    // make sure the 0 < tv_usec, if p->tv_sec * MM_USEC_PER_SEC + p->tv_usec > 0;
    if (0 < p->tv_sec && 0 > p->tv_usec)
    {
        p->tv_sec--;
        p->tv_usec += MM_USEC_PER_SEC;
    }
}
MM_EXPORT_DLL int mmTimeval_Compare(const struct timeval* p, const struct timeval* q)
{
    if (p->tv_sec == q->tv_sec)
    {
        return (int)p->tv_usec - q->tv_usec;
    }
    else
    {
        return (int)(p->tv_sec - q->tv_sec);
    }
}
MM_EXPORT_DLL void mmTimeval_Usec(const struct timeval* p, mmUInt64_t* timecode)
{
    // note: 
    //   we must cast tv_sec tv_usec to mmUInt64_t, make sure not overflow.
    *timecode = ((mmUInt64_t)p->tv_sec * MM_USEC_PER_SEC) + ((mmUInt64_t)p->tv_usec);
}
MM_EXPORT_DLL mmUInt64_t mmTimeval_ToUSec(const struct timeval* p)
{
    mmUInt64_t usec_timecode = 0;
    mmTimeval_Usec(p, &usec_timecode);
    return usec_timecode;
}

