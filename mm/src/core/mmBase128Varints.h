/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBase128Varints_h__
#define __mmBase128Varints_h__

#include "core/mmCore.h"
#include "core/mmByte.h"

#include "core/mmPrefix.h"

// come from protobuf-c lib.

/* Return the number of bytes required to store the
tag for the field (which includes 3 bits for
the wire-type, and a single bit that denotes the end-of-tag. */
MM_EXPORT_DLL size_t mmBase128_GetTagSize(unsigned number);

/* Return the number of bytes required to store
a variable-length unsigned integer that fits in 32-bit uint
in base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_UInt32Size(unsigned int v);

/* Return the number of bytes required to store
a variable-length signed integer that fits in 32-bit int
in base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_Int32Size(int v);

/* return the zigzag-encoded 32-bit unsigned int from a 32-bit signed int */
MM_EXPORT_DLL unsigned int mmBase128_Zigzag32(int v);

/* Return the number of bytes required to store
a variable-length signed integer that fits in 32-bit int,
converted to unsigned via the zig-zag algorithm,
then packed using base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_SInt32Size(int v);

/* Return the number of bytes required to store
a variable-length unsigned integer that fits in 64-bit uint
in base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_UInt64Size(unsigned long long int v);

/* return the zigzag-encoded 64-bit unsigned int from a 64-bit signed int */
MM_EXPORT_DLL unsigned long long int mmBase128_Zigzag64(long long int v);

/* Return the number of bytes required to store
a variable-length signed integer that fits in 64-bit int,
converted to unsigned via the zig-zag algorithm,
then packed using base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_SInt64Size(long long int v);

/* === pack() === */

/* Pack an unsigned 32-bit integer in base-128 encoding, and return the number of bytes needed:
this will be 5 or less. */
MM_EXPORT_DLL size_t mmBase128_UInt32Pack(unsigned int value, unsigned char *out);

/* Pack a 32-bit signed integer, returning the number of bytes needed.
Negative numbers are packed as twos-complement 64-bit integers. */
MM_EXPORT_DLL size_t mmBase128_Int32Pack(int value, unsigned char *out);

/* Pack a 32-bit integer in zigwag encoding. */
MM_EXPORT_DLL size_t mmBase128_SInt32Pack(int value, unsigned char *out);

/* Pack a 64-bit unsigned integer that fits in a 64-bit uint,
using base-128 encoding. */
MM_EXPORT_DLL size_t mmBase128_UInt64Pack(unsigned long long int value, unsigned char *out);

/* Pack a 64-bit signed integer in zigzan encoding,
return the size of the packed output.
(Max returned value is 10) */
MM_EXPORT_DLL size_t mmBase128_SInt64Pack(long long int value, unsigned char *out);

/* Pack a boolean as 0 or 1, even though the int
can really assume any integer value. */
/* XXX: perhaps on some platforms "*out = !!value" would be
a better impl, b/c that is idiotmatic c++ in some stl impls. */
MM_EXPORT_DLL size_t mmBase128_BooleanPack(int value, unsigned char *out);

/* wire-type will be added in required_field_pack() */
/* XXX: just call uint64_pack on 64-bit platforms. */
MM_EXPORT_DLL size_t mmBase128_TagPack(unsigned int id, unsigned char *out);

/* === Unpack() === */

/* max base-128_numbers */
MM_EXPORT_DLL size_t mmBase128_MaxB128Numbers(size_t len, const unsigned char *data);

/* base-128 varint buffer size */
MM_EXPORT_DLL unsigned mmBase128_B128VarintBufferSize(size_t length, const unsigned char *data);

/* Unpack a 32-bit unsigned zigzag */
MM_EXPORT_DLL int mmBase128_Unzigzag32(unsigned int v);

/* Unpack a 64-bit unsigned zigzag */
MM_EXPORT_DLL long long int mmBase128_Unzigzag64(unsigned long long int v);

/* Unpack a 32-bit unsigned integer */
MM_EXPORT_DLL unsigned int mmBase128_ParseUInt32(unsigned len, const unsigned char *data);

/* Unpack a 32-bit signed integer */
MM_EXPORT_DLL int mmBase128_ParseSInt32(unsigned len, const unsigned char *data);

/* Unpack a 32-bit signed integer */
MM_EXPORT_DLL unsigned int mmBase128_ParseInt32(unsigned len, const unsigned char *data);

/* Unpack a 64-bit unsigned integer */
MM_EXPORT_DLL unsigned long long int mmBase128_ParseUInt64(unsigned len, const unsigned char *data);

/* Unpack a 64-bit signed integer */
MM_EXPORT_DLL long long int mmBase128_ParseSInt64(unsigned len, const unsigned char *data);

/* Unpack a boolean */
MM_EXPORT_DLL int mmBase128_ParseBoolean(unsigned len, const unsigned char *data);

/* Scan varint */
MM_EXPORT_DLL unsigned mmBase128_ScanVarint(unsigned len, const unsigned char *data);

/* === encode() === */

/* uint32 encode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt32Encode(mmUInt32_t* v, struct mmByteBuffer* byte_buffer, size_t* offset);

/* uint64 encode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt64Encode(mmUInt64_t* v, struct mmByteBuffer* byte_buffer, size_t* offset);

/* === decode() === */

/* uint32 decode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt32Decode(mmUInt32_t* v, struct mmByteBuffer* byte_buffer, size_t* offset);

/* uint64 decode to byte_buffer */
MM_EXPORT_DLL void mmBase128_UInt64Decode(mmUInt64_t* v, struct mmByteBuffer* byte_buffer, size_t* offset);

#include "core/mmSuffix.h"

#endif//__mmBase128Varints_h__
