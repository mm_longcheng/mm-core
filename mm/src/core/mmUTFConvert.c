/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
 * Copyright 2001-2004 Unicode, Inc.
 *
 * Disclaimer
 *
 * This source code is provided as is by Unicode, Inc. No claims are
 * made as to fitness for any particular purpose. No warranties of any
 * kind are expressed or implied. The recipient agrees to determine
 * applicability of information provided. If this file has been
 * purchased on magnetic or optical media from Unicode, Inc., the
 * sole remedy for any claim will be exchange of defective media
 * within 90 days of receipt.
 *
 * Limitations on Rights to Redistribute This Code
 *
 * Unicode, Inc. hereby grants the right to freely use the information
 * supplied in this file in the creation of products supporting the
 * Unicode Standard, and to make copies of this file in any form
 * for internal or external distribution as long as this notice
 * remains attached.
 */

/* ---------------------------------------------------------------------

    Conversions between UTF32, UTF-16, and UTF-8. Source code file.
    Author: Mark E. Davis, 1994.
    Rev History: Rick McGowan, fixes & updates May 2001.
    Sept 2001: fixed const & error conditions per
        mods suggested by S. Parent & A. Lillich.
    June 2002: Tim Dodd added detection and handling of incomplete
        source sequences, enhanced error detection, added casts
        to eliminate compiler warnings.
    July 2003: slight mods to back out aggressive FFFE detection.
    Jan 2004: updated switches in from-UTF8 conversions.
    Oct 2004: updated to use MM_UTF_UNI_MAX_LEGAL_UTF32 in UTF-32 conversions.

    See the header file "ConvertUTF.h" for complete documentation.

------------------------------------------------------------------------ */

#include "mmUTFConvert.h"

#include <string.h>

static const int halfShift  = 10; /* used for shifting by 10 bits */

static const mmUTF32 halfBase = 0x0010000UL;
static const mmUTF32 halfMask = 0x3FFUL;

#define MM_UTF_UNI_SUR_HIGH_START  (mmUTF32)0xD800
#define MM_UTF_UNI_SUR_HIGH_END    (mmUTF32)0xDBFF
#define MM_UTF_UNI_SUR_LOW_START   (mmUTF32)0xDC00
#define MM_UTF_UNI_SUR_LOW_END     (mmUTF32)0xDFFF
#define MM_UTF_FALSE      0
#define MM_UTF_TRUE       1

/* --------------------------------------------------------------------- */

/*
 * Index into the table below with the first byte of a UTF-8 sequence to
 * get the number of trailing bytes that are supposed to follow it.
 * Note that *legal* UTF-8 values can't have 4 or 5-bytes. The table is
 * left as-is for anyone who may want to do such conversion, which was
 * allowed in earlier algorithms.
 */
static const char trailingBytesForUTF8[256] =
{
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,3,3,3,4,4,4,4,5,5,5,5,
};

/*
 * Magic values subtracted from a buffer value during UTF8 conversion.
 * This table contains as many values as there might be trailing bytes
 * in a UTF-8 sequence.
 */
static const mmUTF32 offsetsFromUTF8[6] =
{
    0x00000000UL, 0x00003080UL, 0x000E2080UL,
    0x03C82080UL, 0xFA082080UL, 0x82082080UL,
};

/*
 * Once the bits are split out into bytes of UTF-8, this is a mask OR-ed
 * into the first byte, depending on how many bytes follow.  There are
 * as many entries in this table as there are UTF-8 sequence types.
 * (I.e., one byte sequence, two byte... etc.). Remember that sequencs
 * for *legal* UTF-8 will be 4 or fewer bytes total.
 */
static const mmUTF8 firstByteMark[7] = { 0x00, 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, };

/* --------------------------------------------------------------------- */

/* The interface converts a whole buffer to avoid function-call overhead.
 * Constants have been gathered. Loops & conditionals have been removed as
 * much as possible for efficiency, in favor of drop-through switches.
 * (See "Note A" at the bottom of the file for equivalent code.)
 * If your compiler supports it, the "isLegalUTF8" call can be turned
 * into an inline function.
 */


/* --------------------------------------------------------------------- */

MM_EXPORT_DLL
int 
mmUTF_ConvertUTF32toUTF16(
    const mmUTF32**                                li, 
    const mmUTF32*                                 ri, 
    mmUTF16**                                      lo, 
    mmUTF16*                                       ro, 
    int                                            flags)
{
    int result = MM_UTF_CONVERSION_OK;
    const mmUTF32* source = *li;
    mmUTF16* target = *lo;
    while (source < ri)
    {
        mmUTF32 ch;
        if (target >= ro)
        {
            result = MM_UTF_TARGET_EXHAUSTED; break;
        }
        ch = *source++;
        if (ch <= MM_UTF_UNI_MAX_BMP)
        {
            /* Target is a character <= 0xFFFF */
            /* UTF-16 surrogate values are illegal in UTF-32; 0xffff or 0xfffe are both reserved values */
            if (ch >= MM_UTF_UNI_SUR_HIGH_START && ch <= MM_UTF_UNI_SUR_LOW_END)
            {
                if (flags == MM_UTF_STRICT_CONVERSION)
                {
                    --source; /* return to the illegal value itself */
                    result = MM_UTF_SOURCE_ILLEGAL;
                    break;
                }
                else
                {
                    *target++ = MM_UTF_UNI_REPLACEMENT_CHAR;
                }
            }
            else
            {
                *target++ = (mmUTF16)ch; /* normal case */
            }
        }
        else if (ch > MM_UTF_UNI_MAX_LEGAL_UTF32)
        {
            if (flags == MM_UTF_STRICT_CONVERSION)
            {
                result = MM_UTF_SOURCE_ILLEGAL;
            }
            else
            {
                *target++ = MM_UTF_UNI_REPLACEMENT_CHAR;
            }
        }
        else
        {
            /* target is a character in range 0xFFFF - 0x10FFFF. */
            if (target + 1 >= ro)
            {
                --source; /* Back up source pointer! */
                result = MM_UTF_TARGET_EXHAUSTED; break;
            }
            ch -= halfBase;
            *target++ = (mmUTF16)((ch >> halfShift) + MM_UTF_UNI_SUR_HIGH_START);
            *target++ = (mmUTF16)((ch & halfMask) + MM_UTF_UNI_SUR_LOW_START);
        }
    }
    *li = source;
    *lo = target;
    return result;
}

/* --------------------------------------------------------------------- */

MM_EXPORT_DLL
int 
mmUTF_ConvertUTF16toUTF32(
    const mmUTF16**                                li, 
    const mmUTF16*                                 ri, 
    mmUTF32**                                      lo, 
    mmUTF32*                                       ro, 
    int                                            flags)
{
    int result = MM_UTF_CONVERSION_OK;
    const mmUTF16* source = *li;
    mmUTF32* target = *lo;
    mmUTF32 ch, ch2;
    while (source < ri)
    {
        const mmUTF16* oldSource = source; /*  In case we have to back up because of target overflow. */
        ch = *source++;
        /* If we have a surrogate pair, convert to UTF32 first. */
        if (ch >= MM_UTF_UNI_SUR_HIGH_START && ch <= MM_UTF_UNI_SUR_HIGH_END)
        {
            /* If the 16 bits following the high surrogate are in the source buffer... */
            if (source < ri)
            {
                ch2 = *source;
                /* If it's a low surrogate, convert to UTF32. */
                if (ch2 >= MM_UTF_UNI_SUR_LOW_START && ch2 <= MM_UTF_UNI_SUR_LOW_END)
                {
                    ch = ((ch - MM_UTF_UNI_SUR_HIGH_START) << halfShift)
                        + (ch2 - MM_UTF_UNI_SUR_LOW_START) + halfBase;
                    ++source;
                }
                else if (flags == MM_UTF_STRICT_CONVERSION)
                {
                    /* it's an unpaired high surrogate */
                    --source; /* return to the illegal value itself */
                    result = MM_UTF_SOURCE_ILLEGAL;
                    break;
                }
            }
            else
            {
                /* We don't have the 16 bits following the high surrogate. */
                --source; /* return to the high surrogate */
                result = MM_UTF_SOURCE_EXHAUSTED;
                break;
            }
        }
        else if (flags == MM_UTF_STRICT_CONVERSION)
        {
            /* UTF-16 surrogate values are illegal in UTF-32 */
            if (ch >= MM_UTF_UNI_SUR_LOW_START && ch <= MM_UTF_UNI_SUR_LOW_END)
            {
                --source; /* return to the illegal value itself */
                result = MM_UTF_SOURCE_ILLEGAL;
                break;
            }
        }
        if (target >= ro)
        {
            source = oldSource; /* Back up source pointer! */
            result = MM_UTF_TARGET_EXHAUSTED; break;
        }
        *target++ = ch;
    }
    *li = source;
    *lo = target;
#ifdef CVTUTF_DEBUG
    if (result == MM_UTF_SOURCE_ILLEGAL)
    {
        fprintf(stderr, "ConvertUTF16toUTF32 illegal seq 0x%04x,%04x\n", ch, ch2);
        fflush(stderr);
    }
#endif
    return result;
}

MM_EXPORT_DLL
int 
mmUTF_ConvertUTF16toUTF8(
    const mmUTF16**                                li, 
    const mmUTF16*                                 ri, 
    mmUTF8**                                       lo, 
    mmUTF8*                                        ro, 
    int                                            flags)
{
    int result = MM_UTF_CONVERSION_OK;
    const mmUTF16* source = *li;
    mmUTF8* target = *lo;
    while (source < ri)
    {
        mmUTF32 ch;
        unsigned short bytesToWrite = 0;
        const mmUTF32 byteMask = 0xBF;
        const mmUTF32 byteMark = 0x80;
        const mmUTF16* oldSource = source; /* In case we have to back up because of target overflow. */
        ch = *source++;
        /* If we have a surrogate pair, convert to UTF32 first. */
        if (ch >= MM_UTF_UNI_SUR_HIGH_START && ch <= MM_UTF_UNI_SUR_HIGH_END)
        {
            /* If the 16 bits following the high surrogate are in the source buffer... */
            if (source < ri)
            {
                mmUTF32 ch2 = *source;
                /* If it's a low surrogate, convert to UTF32. */
                if (ch2 >= MM_UTF_UNI_SUR_LOW_START && ch2 <= MM_UTF_UNI_SUR_LOW_END)
                {
                    ch = ((ch - MM_UTF_UNI_SUR_HIGH_START) << halfShift)
                        + (ch2 - MM_UTF_UNI_SUR_LOW_START) + halfBase;
                    ++source;
                }
                else if (flags == MM_UTF_STRICT_CONVERSION)
                {
                    /* it's an unpaired high surrogate */
                    --source; /* return to the illegal value itself */
                    result = MM_UTF_SOURCE_ILLEGAL;
                    break;
                }
            }
            else
            {
                /* We don't have the 16 bits following the high surrogate. */
                --source; /* return to the high surrogate */
                result = MM_UTF_SOURCE_EXHAUSTED;
                break;
            }
        }
        else if (flags == MM_UTF_STRICT_CONVERSION)
        {
            /* UTF-16 surrogate values are illegal in UTF-32 */
            if (ch >= MM_UTF_UNI_SUR_LOW_START && ch <= MM_UTF_UNI_SUR_LOW_END)
            {
                --source; /* return to the illegal value itself */
                result = MM_UTF_SOURCE_ILLEGAL;
                break;
            }
        }
        /* Figure out how many bytes the result will require */
        if (ch < (mmUTF32)0x80)
        {
            bytesToWrite = 1;
        }
        else if (ch < (mmUTF32)0x800)
        {
            bytesToWrite = 2;
        }
        else if (ch < (mmUTF32)0x10000)
        {
            bytesToWrite = 3;
        }
        else if (ch < (mmUTF32)0x110000)
        {
            bytesToWrite = 4;
        }
        else
        {
            bytesToWrite = 3;
            ch = MM_UTF_UNI_REPLACEMENT_CHAR;
        }

        target += bytesToWrite;
        if (target > ro)
        {
            source = oldSource; /* Back up source pointer! */
            target -= bytesToWrite; result = MM_UTF_TARGET_EXHAUSTED; break;
        }
        switch (bytesToWrite)
        {
            /* note: everything falls through. */
            case 4: *--target = (mmUTF8)((ch | byteMark) & byteMask); ch >>= 6;
            case 3: *--target = (mmUTF8)((ch | byteMark) & byteMask); ch >>= 6;
            case 2: *--target = (mmUTF8)((ch | byteMark) & byteMask); ch >>= 6;
            case 1: *--target = (mmUTF8) (ch | firstByteMark[bytesToWrite]);
        }
        target += bytesToWrite;
    }
    *li = source;
    *lo = target;
    return result;
}

/* --------------------------------------------------------------------- */

MM_EXPORT_DLL
int 
mmUTF_ConvertUTF32toUTF8(
    const mmUTF32**                                li, 
    const mmUTF32*                                 ri, 
    mmUTF8**                                       lo, 
    mmUTF8*                                        ro, 
    int                                            flags)
{
    int result = MM_UTF_CONVERSION_OK;
    const mmUTF32* source = *li;
    mmUTF8* target = *lo;
    while (source < ri)
    {
        mmUTF32 ch;
        unsigned short bytesToWrite = 0;
        const mmUTF32 byteMask = 0xBF;
        const mmUTF32 byteMark = 0x80;
        ch = *source++;
        if (flags == MM_UTF_STRICT_CONVERSION )
        {
            /* UTF-16 surrogate values are illegal in UTF-32 */
            if (ch >= MM_UTF_UNI_SUR_HIGH_START && ch <= MM_UTF_UNI_SUR_LOW_END)
            {
                --source; /* return to the illegal value itself */
                result = MM_UTF_SOURCE_ILLEGAL;
                break;
            }
        }
        /*
         * Figure out how many bytes the result will require. Turn any
         * illegally large UTF32 things (> Plane 17) into replacement chars.
         */
        if (ch < (mmUTF32)0x80)
        {
            bytesToWrite = 1;
        }
        else if (ch < (mmUTF32)0x800)
        {
            bytesToWrite = 2;
        }
        else if (ch < (mmUTF32)0x10000)
        {
            bytesToWrite = 3;
        }
        else if (ch <= MM_UTF_UNI_MAX_LEGAL_UTF32)
        {
            bytesToWrite = 4;
        }
        else
        {
            bytesToWrite = 3;
            ch = MM_UTF_UNI_REPLACEMENT_CHAR;
            result = MM_UTF_SOURCE_ILLEGAL;
        }
        
        target += bytesToWrite;
        if (target > ro)
        {
            --source; /* Back up source pointer! */
            target -= bytesToWrite; result = MM_UTF_TARGET_EXHAUSTED; break;
        }
        switch (bytesToWrite)
        {
            /* note: everything falls through. */
            case 4: *--target = (mmUTF8)((ch | byteMark) & byteMask); ch >>= 6;
            case 3: *--target = (mmUTF8)((ch | byteMark) & byteMask); ch >>= 6;
            case 2: *--target = (mmUTF8)((ch | byteMark) & byteMask); ch >>= 6;
            case 1: *--target = (mmUTF8) (ch | firstByteMark[bytesToWrite]);
        }
        target += bytesToWrite;
    }
    *li = source;
    *lo = target;
    return result;
}

/* --------------------------------------------------------------------- */

/*
 * Utility routine to tell whether a sequence of bytes is legal UTF-8.
 * This must be called with the length pre-determined by the first byte.
 * If not calling this from ConvertUTF8to*, then the length can be set by:
 *  length = trailingBytesForUTF8[*source]+1;
 * and the sequence is illegal right away if there aren't that many bytes
 * available.
 * If presented with a length > 4, this returns false.  The Unicode
 * definition of UTF-8 goes up to 4-byte sequences.
 */

static 
mmUTFBoolean 
isLegalUTF8(
    const mmUTF8*                                  source, 
    int                                            length)
{
    mmUTF8 a;
    const mmUTF8 *srcptr = source + length;
    switch (length)
    {
    default: return MM_UTF_FALSE;
        /* Everything else falls through when "true"... */
    case 4: if ((a = (*--srcptr)) < 0x80 || a > 0xBF) return MM_UTF_FALSE;
    case 3: if ((a = (*--srcptr)) < 0x80 || a > 0xBF) return MM_UTF_FALSE;
    case 2: if ((a = (*--srcptr)) < 0x80 || a > 0xBF) return MM_UTF_FALSE;

        switch (*source)
        {
            /* no fall-through in this inner switch */
            case 0xE0: if (a < 0xA0) return MM_UTF_FALSE; break;
            case 0xED: if (a > 0x9F) return MM_UTF_FALSE; break;
            case 0xF0: if (a < 0x90) return MM_UTF_FALSE; break;
            case 0xF4: if (a > 0x8F) return MM_UTF_FALSE; break;
            default:   if (a < 0x80) return MM_UTF_FALSE;
        }

    case 1: if (*source >= 0x80 && *source < 0xC2) return MM_UTF_FALSE;
    }
    if (*source > 0xF4) return MM_UTF_FALSE;
    return MM_UTF_TRUE;
}

/* --------------------------------------------------------------------- */

/*
 * Exported function to return whether a UTF-8 sequence is legal or not.
 * This is not used here; it's just exported.
 */
MM_EXPORT_DLL
mmUTFBoolean 
mmUTF_IsLegalUTF8Sequence(
    const mmUTF8*                                  source, 
    const mmUTF8*                                  sourceEnd)
{
    int length = trailingBytesForUTF8[*source] + 1;
    if (length > sourceEnd - source)
    {
        return MM_UTF_FALSE;
    }
    return isLegalUTF8(source, length);
}

/* --------------------------------------------------------------------- */

/*
 * Exported function to return the total number of bytes in a codepoint
 * represented in UTF-8, given the value of the first byte.
 */
MM_EXPORT_DLL
unsigned 
mmUTF_GetNumBytesForUTF8(
    mmUTF8                                         first)
{
    return trailingBytesForUTF8[first] + 1;
}

MM_EXPORT_DLL
int 
mmUTF_GetUTF8StringLength(
    const mmUTF8*                                  utf8)
{
    const mmUTF8** source = &utf8;
    const mmUTF8* ri = utf8 + strlen((const char*)utf8);
    int ret = 0;
    while (*source != ri)
    {
        int length = trailingBytesForUTF8[**source] + 1;
        if (length > ri - *source || !isLegalUTF8(*source, length))
        {
            return 0;
        }
        *source += length;
        ++ret;
    }
    return ret;
}


/* --------------------------------------------------------------------- */

/*
 * Exported function to return whether a UTF-8 string is legal or not.
 * This is not used here; it's just exported.
 */
MM_EXPORT_DLL
mmUTFBoolean 
mmUTF_IsLegalUTF8String(
    const mmUTF8**                                 source, 
    const mmUTF8*                                  sourceEnd)
{
    while (*source != sourceEnd)
    {
        int length = trailingBytesForUTF8[**source] + 1;
        if (length > sourceEnd - *source || !isLegalUTF8(*source, length))
        {
            return MM_UTF_FALSE;
        }
        *source += length;
    }
    return MM_UTF_TRUE;
}

/* --------------------------------------------------------------------- */

MM_EXPORT_DLL
int 
mmUTF_ConvertUTF8toUTF16(
    const mmUTF8**                                    li, 
    const mmUTF8*                                     ri, 
    mmUTF16**                                         lo, 
    mmUTF16*                                          ro, 
    int                                               flags)
{
    int result = MM_UTF_CONVERSION_OK;
    const mmUTF8* source = *li;
    mmUTF16* target = *lo;
    while (source < ri)
    {
        mmUTF32 ch = 0;
        unsigned short extraBytesToRead = trailingBytesForUTF8[*source];
        if (extraBytesToRead >= ri - source)
        {
            result = MM_UTF_SOURCE_EXHAUSTED; break;
        }
        /* Do this check whether lenient or strict */
        if (!isLegalUTF8(source, extraBytesToRead+1))
        {
            result = MM_UTF_SOURCE_ILLEGAL;
            break;
        }
        /*
         * The cases all fall through. See "Note A" below.
         */
        switch (extraBytesToRead)
        {
            case 5: ch += *source++; ch <<= 6; /* remember, illegal UTF-8 */
            case 4: ch += *source++; ch <<= 6; /* remember, illegal UTF-8 */
            case 3: ch += *source++; ch <<= 6;
            case 2: ch += *source++; ch <<= 6;
            case 1: ch += *source++; ch <<= 6;
            case 0: ch += *source++;
        }
        ch -= offsetsFromUTF8[extraBytesToRead];

        if (target >= ro)
        {
            source -= (extraBytesToRead+1); /* Back up source pointer! */
            result = MM_UTF_TARGET_EXHAUSTED; break;
        }
        if (ch <= MM_UTF_UNI_MAX_BMP)
        {
            /* Target is a character <= 0xFFFF */
            /* UTF-16 surrogate values are illegal in UTF-32 */
            if (ch >= MM_UTF_UNI_SUR_HIGH_START && ch <= MM_UTF_UNI_SUR_LOW_END)
            {
                if (flags == MM_UTF_STRICT_CONVERSION)
                {
                    source -= (extraBytesToRead+1); /* return to the illegal value itself */
                    result = MM_UTF_SOURCE_ILLEGAL;
                    break;
                }
                else
                {
                    *target++ = MM_UTF_UNI_REPLACEMENT_CHAR;
                }
            }
            else
            {
                *target++ = (mmUTF16)ch; /* normal case */
            }
        }
        else if (ch > MM_UTF_UNI_MAX_UTF16)
        {
            if (flags == MM_UTF_STRICT_CONVERSION)
            {
                result = MM_UTF_SOURCE_ILLEGAL;
                source -= (extraBytesToRead+1); /* return to the start */
                break; /* Bail out; shouldn't continue */
            }
            else
            {
                *target++ = MM_UTF_UNI_REPLACEMENT_CHAR;
            }
        }
        else
        {
            /* target is a character in range 0xFFFF - 0x10FFFF. */
            if (target + 1 >= ro)
            {
                source -= (extraBytesToRead+1); /* Back up source pointer! */
                result = MM_UTF_TARGET_EXHAUSTED; break;
            }
            ch -= halfBase;
            *target++ = (mmUTF16)((ch >> halfShift) + MM_UTF_UNI_SUR_HIGH_START);
            *target++ = (mmUTF16)((ch & halfMask) + MM_UTF_UNI_SUR_LOW_START);
        }
    }
    *li = source;
    *lo = target;
    return result;
}

/* --------------------------------------------------------------------- */

MM_EXPORT_DLL
int 
mmUTF_ConvertUTF8toUTF32(
    const mmUTF8**                                 li, 
    const mmUTF8*                                  ri, 
    mmUTF32**                                      lo, 
    mmUTF32*                                       ro, 
    int                                            flags)
{
    int result = MM_UTF_CONVERSION_OK;
    const mmUTF8* source = *li;
    mmUTF32* target = *lo;
    while (source < ri)
    {
        mmUTF32 ch = 0;
        unsigned short extraBytesToRead = trailingBytesForUTF8[*source];
        if (extraBytesToRead >= ri - source)
        {
            result = MM_UTF_SOURCE_EXHAUSTED; break;
        }
        /* Do this check whether lenient or strict */
        if (!isLegalUTF8(source, extraBytesToRead+1))
        {
            result = MM_UTF_SOURCE_ILLEGAL;
            break;
        }
        /*
         * The cases all fall through. See "Note A" below.
         */
        switch (extraBytesToRead)
        {
            case 5: ch += *source++; ch <<= 6;
            case 4: ch += *source++; ch <<= 6;
            case 3: ch += *source++; ch <<= 6;
            case 2: ch += *source++; ch <<= 6;
            case 1: ch += *source++; ch <<= 6;
            case 0: ch += *source++;
        }
        ch -= offsetsFromUTF8[extraBytesToRead];

        if (target >= ro)
        {
            source -= (extraBytesToRead+1); /* Back up the source pointer! */
            result = MM_UTF_TARGET_EXHAUSTED; break;
        }
        if (ch <= MM_UTF_UNI_MAX_LEGAL_UTF32)
        {
            /*
             * UTF-16 surrogate values are illegal in UTF-32, and anything
             * over Plane 17 (> 0x10FFFF) is illegal.
             */
            if (ch >= MM_UTF_UNI_SUR_HIGH_START && ch <= MM_UTF_UNI_SUR_LOW_END)
            {
                if (flags == MM_UTF_STRICT_CONVERSION)
                {
                    source -= (extraBytesToRead+1); /* return to the illegal value itself */
                    result = MM_UTF_SOURCE_ILLEGAL;
                    break;
                }
                else
                {
                    *target++ = MM_UTF_UNI_REPLACEMENT_CHAR;
                }
            }
            else
            {
                *target++ = ch;
            }
        }
        else
        {
            /* i.e., ch > MM_UTF_UNI_MAX_LEGAL_UTF32 */
            result = MM_UTF_SOURCE_ILLEGAL;
            *target++ = MM_UTF_UNI_REPLACEMENT_CHAR;
        }
    }
    *li = source;
    *lo = target;
    return result;
}

/* ---------------------------------------------------------------------

    Note A.
    The fall-through switches in UTF-8 reading code save a
    temp variable, some decrements & conditionals.  The switches
    are equivalent to the following loop:
        {
            int tmpBytesToRead = extraBytesToRead+1;
            do {
                ch += *source++;
                --tmpBytesToRead;
                if (tmpBytesToRead) ch <<= 6;
            } while (tmpBytesToRead > 0);
        }
    In UTF-8 writing code, the switches on "bytesToWrite" are
    similarly unrolled loops.

   --------------------------------------------------------------------- */

MM_EXPORT_DLL
void
mmUTF_8to32(
    const struct mmString*                         i,
    struct mmUtf32String*                          o)
{
    const mmUTF8* li = NULL;
    const mmUTF8* ri = NULL;
    mmUTF32* bo = NULL;
    mmUTF32* lo = NULL;
    mmUTF32* ro = NULL;

    int code = 0;

    const size_t maxNumberOfChars = mmString_Size(i);
    // all UTFs at most one element represents one character.
    const size_t numberOfOut = maxNumberOfChars * MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT / sizeof(mmUTF32);

    mmUtf32String_Resize(o, numberOfOut);

    li = (const mmUTF8*)mmString_Data(i);
    ri = (const mmUTF8*)(li + maxNumberOfChars);
    bo = (mmUTF32*)mmUtf32String_Data(o);
    lo = (mmUTF32*)bo;
    ro = (mmUTF32*)(lo + numberOfOut);

    code = mmUTF_ConvertUTF8toUTF32(&li, ri, &lo, ro, MM_UTF_STRICT_CONVERSION);
    if(MM_UTF_CONVERSION_OK != code)
    {
        mmUtf32String_Clear(o);
    }
    else
    {
        mmUtf32String_Resize(o, (mmUTF32*)(lo) - (mmUTF32*)bo);
    }
}

MM_EXPORT_DLL
void
mmUTF_32to8(
    const struct mmUtf32String*                    i,
    struct mmString*                               o)
{
    const mmUTF32* li = NULL;
    const mmUTF32* ri = NULL;
    mmUTF8* bo = NULL;
    mmUTF8* lo = NULL;
    mmUTF8* ro = NULL;

    int code = 0;

    const size_t maxNumberOfChars = mmUtf32String_Size(i);
    // all UTFs at most one element represents one character.
    const size_t numberOfOut = maxNumberOfChars * MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT / sizeof(mmUTF8);

    mmString_Resize(o, numberOfOut);

    li = (const mmUTF32*)mmUtf32String_Data(i);
    ri = (const mmUTF32*)(li + maxNumberOfChars);
    bo = (mmUTF8*)mmString_Data(o);
    lo = (mmUTF8*)bo;
    ro = (mmUTF8*)(lo + numberOfOut);

    code = mmUTF_ConvertUTF32toUTF8(&li, ri, &lo, ro, MM_UTF_STRICT_CONVERSION);
    if(MM_UTF_CONVERSION_OK != code)
    {
        mmString_Clear(o);
    }
    else
    {
        mmString_Resize(o, (mmUTF8*)(lo) - (mmUTF8*)bo);
    }
}

MM_EXPORT_DLL
void
mmUTF_8to16(
    const struct mmString*                         i,
    struct mmUtf16String*                          o)
{
    const mmUTF8* li = NULL;
    const mmUTF8* ri = NULL;
    mmUTF16* bo = NULL;
    mmUTF16* lo = NULL;
    mmUTF16* ro = NULL;

    int code = 0;

    const size_t maxNumberOfChars = mmString_Size(i);
    // all UTFs at most one element represents one character.
    const size_t numberOfOut = maxNumberOfChars * MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT / sizeof(mmUTF16);

    mmUtf16String_Resize(o, numberOfOut);

    li = (const mmUTF8*)mmString_Data(i);
    ri = (const mmUTF8*)(li + maxNumberOfChars);
    bo = (mmUTF16*)mmUtf16String_Data(o);
    lo = (mmUTF16*)bo;
    ro = (mmUTF16*)(lo + numberOfOut);

    code = mmUTF_ConvertUTF8toUTF16(&li, ri, &lo, ro, MM_UTF_STRICT_CONVERSION);
    if(MM_UTF_CONVERSION_OK != code)
    {
        mmUtf16String_Clear(o);
    }
    else
    {
        mmUtf16String_Resize(o, (mmUTF16*)(lo) - (mmUTF16*)bo);
    }
}

MM_EXPORT_DLL
void
mmUTF_16to8(
    const struct mmUtf16String*                    i,
    struct mmString*                               o)
{
    const mmUTF16* li = NULL;
    const mmUTF16* ri = NULL;
    mmUTF8* bo = NULL;
    mmUTF8* lo = NULL;
    mmUTF8* ro = NULL;

    int code = 0;

    const size_t maxNumberOfChars = mmUtf16String_Size(i);
    // all UTFs at most one element represents one character.
    const size_t numberOfOut = maxNumberOfChars * MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT / sizeof(mmUTF8);

    mmString_Resize(o, numberOfOut);

    li = (const mmUTF16*)mmUtf16String_Data(i);
    ri = (const mmUTF16*)(li + maxNumberOfChars);
    bo = (mmUTF8*)mmString_Data(o);
    lo = (mmUTF8*)bo;
    ro = (mmUTF8*)(lo + numberOfOut);

    code = mmUTF_ConvertUTF16toUTF8(&li, ri, &lo, ro, MM_UTF_STRICT_CONVERSION);
    if(MM_UTF_CONVERSION_OK != code)
    {
        mmString_Clear(o);
    }
    else
    {
        mmString_Resize(o, (mmUTF8*)(lo) - (mmUTF8*)bo);
    }
}

MM_EXPORT_DLL
void
mmUTF_16to32(
    const struct mmUtf16String*                    i,
    struct mmUtf32String*                          o)
{
    const mmUTF16* li = NULL;
    const mmUTF16* ri = NULL;
    mmUTF32* bo = NULL;
    mmUTF32* lo = NULL;
    mmUTF32* ro = NULL;

    int code = 0;

    const size_t maxNumberOfChars = mmUtf16String_Size(i);
    // all UTFs at most one element represents one character.
    const size_t numberOfOut = maxNumberOfChars * MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT / sizeof(mmUTF32);

    mmUtf32String_Resize(o, numberOfOut);

    li = (const mmUTF16*)mmUtf16String_Data(i);
    ri = (const mmUTF16*)(li + maxNumberOfChars);
    bo = (mmUTF32*)mmUtf32String_Data(o);
    lo = (mmUTF32*)bo;
    ro = (mmUTF32*)(lo + numberOfOut);

    code = mmUTF_ConvertUTF16toUTF32(&li, ri, &lo, ro, MM_UTF_STRICT_CONVERSION);
    if(MM_UTF_CONVERSION_OK != code)
    {
        mmUtf32String_Clear(o);
    }
    else
    {
        mmUtf32String_Resize(o, (mmUTF32*)(lo) - (mmUTF32*)bo);
    }
}

MM_EXPORT_DLL
void
mmUTF_32to16(
    const struct mmUtf32String*                    i,
    struct mmUtf16String*                          o)
{
    const mmUTF32* li = NULL;
    const mmUTF32* ri = NULL;
    mmUTF16* bo = NULL;
    mmUTF16* lo = NULL;
    mmUTF16* ro = NULL;

    int code = 0;

    const size_t maxNumberOfChars = mmUtf32String_Size(i);
    // all UTFs at most one element represents one character.
    const size_t numberOfOut = maxNumberOfChars * MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT / sizeof(mmUTF16);

    mmUtf16String_Resize(o, numberOfOut);

    li = (const mmUTF32*)mmUtf32String_Data(i);
    ri = (const mmUTF32*)(li + maxNumberOfChars);
    bo = (mmUTF16*)mmUtf16String_Data(o);
    lo = (mmUTF16*)bo;
    ro = (mmUTF16*)(lo + numberOfOut);

    code = mmUTF_ConvertUTF32toUTF16(&li, ri, &lo, ro, MM_UTF_STRICT_CONVERSION);
    if(MM_UTF_CONVERSION_OK != code)
    {
        mmUtf16String_Clear(o);
    }
    else
    {
        mmUtf16String_Resize(o, (mmUTF16*)(lo) - (mmUTF16*)bo);
    }
}
