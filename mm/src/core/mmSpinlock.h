/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSpinlock_h__
#define __mmSpinlock_h__

#include "core/mmCore.h"
#include "core/mmAtomic.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL void mmSpinlock_LockImpl(mmAtomic_t *lock, mmAtomicInt_t value, mmUInt_t spin);

#define mmSpinlock_Init(lock, spin) (*lock = 0)
#define mmSpinlock_Destroy(lock) (*lock = 0)
#define mmSpinlock_Lock(lock) mmSpinlock_LockImpl(lock, 1, 1000)
#define mmSpinlock_Trylock(lock) (*(lock) == 0 && mmAtomicCmpSet(lock, 0, 1))
#define mmSpinlock_Unlock(lock) (*(lock) = 0)

// Internal lock : Used inside the module function body.
// External lock : Used outside the module function body.
// 
// We avoid deadlock by following the following principles.
//
// 1. The internal lock cannot be used for the outside, the external lock cannot be used for the internal.
// 2. Internal and external lock permissions are different, you can overlap permissions.
// 3. The external lock interface should contain all non-overlapping data locks.
// 4. When multiple external locks are used, the order of obtaining lock permissions must be the same.

#include "core/mmSuffix.h"

#endif//__mmSpinlock_h__