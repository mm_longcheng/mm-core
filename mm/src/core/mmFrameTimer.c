/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFrameTimer.h"
#include "core/mmSpinlock.h"
#include "core/mmTimespec.h"
#include "core/mmTimewait.h"
#include "core/mmLogger.h"

MM_EXPORT_DLL void mmFrameTimerCallback_Init(struct mmFrameTimerCallback* p)
{
    p->Update = &mmFrameTimerCallback_UpdateDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmFrameTimerCallback_Destroy(struct mmFrameTimerCallback* p)
{
    p->Update = &mmFrameTimerCallback_UpdateDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmFrameTimerCallback_Reset(struct mmFrameTimerCallback* p)
{
    p->Update = &mmFrameTimerCallback_UpdateDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmFrameTimerCallback_UpdateDefault(void* obj, double interval)
{

}

MM_EXPORT_DLL void mmFrameTimer_Init(struct mmFrameTimer* p)
{
    mmString_Init(&p->name);
    mmClock_Init(&p->clock);
    mmFrameStats_Init(&p->status);
    mmFrameTimerCallback_Init(&p->callback);
    //
    mmSpinlock_Init(&p->locker, NULL);

    p->background = MM_FRAME_TIMER_BACKGROUND_RUNING;
    p->active = 1;

    p->frequency_factor = MM_FRAME_TIMER_FREQUENCY_FACTOR_DEFAULT;

    p->frequency = MM_FRAME_TIMER_FREQUENCY_DEFAULT;
    p->frequency_dynamic = p->frequency;
    p->frequency_l = p->frequency * (1 - p->frequency_factor);
    p->frequency_r = p->frequency * (1 + p->frequency_factor);
    p->frame_interval_usec = (1.0 / p->frequency_dynamic) * (double)(MM_USEC_PER_SEC);

    p->wait_usec = 0;
    p->frame_cost_usec = 0;

    mmString_Assigns(&p->name, "name");

    mmClock_Reset(&p->clock);
}
MM_EXPORT_DLL void mmFrameTimer_Destroy(struct mmFrameTimer* p)
{
    mmString_Destroy(&p->name);
    mmClock_Destroy(&p->clock);
    mmFrameStats_Destroy(&p->status);
    mmFrameTimerCallback_Destroy(&p->callback);
    //
    mmSpinlock_Destroy(&p->locker);

    p->background = MM_FRAME_TIMER_BACKGROUND_RUNING;
    p->active = 1;

    p->frequency_factor = MM_FRAME_TIMER_FREQUENCY_FACTOR_DEFAULT;

    p->frequency = MM_FRAME_TIMER_FREQUENCY_DEFAULT;
    p->frequency_dynamic = p->frequency;
    p->frequency_l = p->frequency * (1 - p->frequency_factor);
    p->frequency_r = p->frequency * (1 + p->frequency_factor);
    p->frame_interval_usec = (1.0 / p->frequency_dynamic) * (double)(MM_USEC_PER_SEC);

    p->wait_usec = 0;
    p->frame_cost_usec = 0;
}
MM_EXPORT_DLL void mmFrameTimer_Reset(struct mmFrameTimer* p)
{
    mmString_Clear(&p->name);
    mmClock_Reset(&p->clock);
    mmFrameStats_Reset(&p->status);
    mmFrameTimerCallback_Reset(&p->callback);
    //
    // spinlock not need reset.

    p->background = MM_FRAME_TIMER_BACKGROUND_RUNING;
    p->active = 1;

    p->frequency_factor = MM_FRAME_TIMER_FREQUENCY_FACTOR_DEFAULT;

    p->frequency = MM_FRAME_TIMER_FREQUENCY_DEFAULT;
    p->frequency_dynamic = p->frequency;
    p->frequency_l = p->frequency * (1 - p->frequency_factor);
    p->frequency_r = p->frequency * (1 + p->frequency_factor);
    p->frame_interval_usec = (1.0 / p->frequency_dynamic) * (double)(MM_USEC_PER_SEC);

    p->wait_usec = 0;
    p->frame_cost_usec = 0;

    mmString_Assigns(&p->name, "name");

    mmClock_Reset(&p->clock);
}
MM_EXPORT_DLL void mmFrameTimer_Lock(struct mmFrameTimer* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmFrameTimer_Unlock(struct mmFrameTimer* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_DLL void mmFrameTimer_SetName(struct mmFrameTimer* p, const char* name)
{
    mmString_Assigns(&p->name, name);
}
MM_EXPORT_DLL void mmFrameTimer_SetFrequencyFactor(struct mmFrameTimer* p, double frequency_factor)
{
    assert(0 < frequency_factor && frequency_factor < 1 && "frequency_factor must (0, 1).");
    //
    p->frequency_factor = frequency_factor;
    //
    p->frequency_l = p->frequency * (1 - p->frequency_factor);
    p->frequency_r = p->frequency * (1 + p->frequency_factor);
}
MM_EXPORT_DLL void mmFrameTimer_SetFrequency(struct mmFrameTimer* p, double frequency)
{
    p->frequency = frequency;
    //
    p->frequency_dynamic = p->frequency;
    p->frame_interval_usec = (1.0 / p->frequency_dynamic) * (double)(MM_USEC_PER_SEC);
    //
    p->wait_usec = p->frame_interval_usec;
    //
    p->frequency_l = p->frequency * (1 - p->frequency_factor);
    p->frequency_r = p->frequency * (1 + p->frequency_factor);
    //
    mmFrameStats_SetAverage(&p->status, p->frequency);
}
MM_EXPORT_DLL void mmFrameTimer_SetBackground(struct mmFrameTimer* p, mmUInt8_t background)
{
    p->background = background;
}
MM_EXPORT_DLL void mmFrameTimer_SetActive(struct mmFrameTimer* p, mmUInt8_t active)
{
    p->active = active;
}
MM_EXPORT_DLL void mmFrameTimer_SetCallback(struct mmFrameTimer* p, const struct mmFrameTimerCallback* callback)
{
    assert(NULL != callback && "callback is a null.");
    p->callback = *callback;
}
MM_EXPORT_DLL void mmFrameTimer_Update(struct mmFrameTimer* p, double* update_cost_time)
{
    if (1 == p->active)
    {
        double interval = 0;

        double wait_usec = 0;

        interval = (double)(mmClock_Microseconds(&p->clock));

        wait_usec = (p->wait_usec - interval);

        // wait the timer is timeout (-inf, 1000) usec.
        // most of system msleep(1) not enough precision.
        if (1000 >= wait_usec)
        {
            double wait_usec_mod = 0;
            double current_interval = 0;

            mmFrameStats_Update(&p->status);

            (*(p->callback.Update))(p, (p->status.interval) / (double)(MM_USEC_PER_SEC));

            // frequency_dynamic update. dynamic frequency modulation.
            p->frequency_dynamic = (p->frequency * p->frequency) / p->status.average;
            // frequency_dynamic = clamp(frequency_dynamic, frequency_l, frequency_r)
            p->frequency_dynamic = p->frequency_dynamic > p->frequency_l ? p->frequency_dynamic : p->frequency_l;
            p->frequency_dynamic = p->frequency_dynamic < p->frequency_r ? p->frequency_dynamic : p->frequency_r;
            // frequency_dynamic_usec update.
            p->frame_interval_usec = (1.0 / p->frequency_dynamic) * (double)(MM_USEC_PER_SEC);

            current_interval = (double)(mmClock_Microseconds(&p->clock));
            mmClock_Reset(&p->clock);
            p->frame_cost_usec = current_interval - interval;

            wait_usec_mod = fmod(wait_usec, p->frame_interval_usec);
            p->wait_usec = p->frame_interval_usec - p->frame_cost_usec + wait_usec_mod;

            (*update_cost_time) += p->frame_cost_usec;
        }
    }
    else
    {
        p->wait_usec = p->frame_interval_usec;
    }
}
MM_EXPORT_DLL void mmFrameTimer_UpdateImmediately(struct mmFrameTimer* p)
{
    mmFrameStats_Update(&p->status);
    (*(p->callback.Update))(p, (p->status.interval) / (double)(MM_USEC_PER_SEC));
}
MM_EXPORT_DLL void mmFrameTimer_UpdateMotionStatus(struct mmFrameTimer* p, mmSInt8_t motion_status, double* update_cost_time)
{
    if (MM_FRAME_TIMER_MOTION_STATUS_BACKGROUND != motion_status || 0 != p->background)
    {
        mmFrameTimer_Update(p, update_cost_time);
    }
    else
    {
        p->wait_usec = (1.0 / p->frequency) * (double)(MM_USEC_PER_SEC);
    }
}

MM_EXPORT_DLL void mmFrameTask_Init(struct mmFrameTask* p)
{
    mmFrameTimer_Init(&p->frame_timer);
    mmClock_Init(&p->clock);
    mmTimeval_Init(&p->ntime);
    mmTimespec_Init(&p->otime);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->frame_cost_usec = 0.0;
    p->sleep_usec = 0;
    p->smooth_mode = 0;
    p->nearby_time = 0;
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}
MM_EXPORT_DLL void mmFrameTask_Destroy(struct mmFrameTask* p)
{
    mmFrameTimer_Destroy(&p->frame_timer);
    mmClock_Destroy(&p->clock);
    mmTimeval_Destroy(&p->ntime);
    mmTimespec_Destroy(&p->otime);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->locker);
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->frame_cost_usec = 0.0;
    p->sleep_usec = 0;
    p->smooth_mode = 0;
    p->nearby_time = 0;
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

MM_EXPORT_DLL void mmFrameTask_Lock(struct mmFrameTask* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmFrameTask_Unlock(struct mmFrameTask* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_DLL void mmFrameTask_SetContext(struct mmFrameTask* p, void* u)
{
    p->u = u;
}

MM_EXPORT_DLL void mmFrameTask_SetName(struct mmFrameTask* p, const char* name)
{
    mmFrameTimer_SetName(&p->frame_timer, name);
}
MM_EXPORT_DLL void mmFrameTask_SetFrequencyFactor(struct mmFrameTask* p, double frequency_factor)
{
    mmFrameTimer_SetFrequencyFactor(&p->frame_timer, frequency_factor);
}
MM_EXPORT_DLL void mmFrameTask_SetFrequency(struct mmFrameTask* p, double frequency)
{
    mmFrameTimer_SetFrequency(&p->frame_timer, frequency);
}
MM_EXPORT_DLL void mmFrameTask_SetBackground(struct mmFrameTask* p, mmUInt8_t background)
{
    mmFrameTimer_SetBackground(&p->frame_timer, background);
}
MM_EXPORT_DLL void mmFrameTask_SetActive(struct mmFrameTask* p, mmUInt8_t active)
{
    mmFrameTimer_SetActive(&p->frame_timer, active);
}
MM_EXPORT_DLL void mmFrameTask_SetSmoothMode(struct mmFrameTask* p, int mode)
{
    p->smooth_mode = mode;
}
MM_EXPORT_DLL void mmFrameTask_SetCallback(struct mmFrameTask* p, const struct mmFrameTimerCallback* callback)
{
    mmFrameTimer_SetCallback(&p->frame_timer, callback);
}

MM_EXPORT_DLL void mmFrameTask_Update(struct mmFrameTask* p)
{
    // reset cache.
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->sleep_usec = 0;

    // update the motion status.
    mmFrameTimer_UpdateMotionStatus(&p->frame_timer, p->motion_status, &p->update_cost_time);

    // clock reset for time wait.
    mmClock_Reset(&p->clock);
}
MM_EXPORT_DLL void mmFrameTask_UpdateImmediately(struct mmFrameTask* p)
{
    // update frame immediately.
    mmFrameTimer_UpdateImmediately(&p->frame_timer);
}
MM_EXPORT_DLL void mmFrameTask_Timewait(struct mmFrameTask* p)
{
    double wait_usec_1 = 0.0;

    // cache the wait usec.
    p->frame_cost_usec = (double)(mmClock_Microseconds(&p->clock));

    // time wait fixed.
    wait_usec_1 = p->frame_timer.wait_usec - p->frame_cost_usec;

    // calculation the max idle usec.
    p->max_idle_usec = (p->max_idle_usec > wait_usec_1) ? wait_usec_1 : p->max_idle_usec;

    // smooth mode.
    p->max_idle_usec = (!p->smooth_mode) ? p->max_idle_usec : 0.0;

    // calculation sleep usec.
    p->sleep_usec = (int)(p->max_idle_usec);
    p->sleep_usec = (0 > p->sleep_usec) ? 0 : p->sleep_usec;
    // [0, sleep_usec - 1] for little quick timewake.
    p->sleep_usec = (0 < p->sleep_usec) ? p->sleep_usec - 1 : p->sleep_usec;

    // calculation nearby time.
    p->nearby_time = p->sleep_usec;
    // timewait nearby.
    mmTimewait_USecNearby(&p->signal_cond, &p->signal_mutex, &p->ntime, &p->otime, p->nearby_time);
}
MM_EXPORT_DLL void mmFrameTask_Timewake(struct mmFrameTask* p)
{
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}

MM_EXPORT_DLL void mmFrameTask_EnterBackground(struct mmFrameTask* p)
{
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_BACKGROUND;
}
MM_EXPORT_DLL void mmFrameTask_EnterForeground(struct mmFrameTask* p)
{
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
}

MM_EXPORT_DLL void mmFrameTask_Start(struct mmFrameTask* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
}
MM_EXPORT_DLL void mmFrameTask_Interrupt(struct mmFrameTask* p)
{
    p->state = MM_TS_CLOSED;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmFrameTask_Shutdown(struct mmFrameTask* p)
{
    p->state = MM_TS_FINISH;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmFrameTask_Join(struct mmFrameTask* p)
{
    // do nothing here, mmFrameTask not thread context, just api full here.
}

MM_EXPORT_DLL void mmFrameScheduler_Init(struct mmFrameScheduler* p)
{
    mmFrameTimer_Init(&p->frame_timer);
    mmTimerHeap_Init(&p->timer_heap);
    mmClock_Init(&p->clock);
    mmTimeval_Init(&p->next_delay);
    mmTimeval_Init(&p->ntime);
    mmTimespec_Init(&p->otime);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->frame_cost_usec = 0.0;
    p->sleep_usec = 0;
    p->smooth_mode = 0;
    p->nearby_time = 0;
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}
MM_EXPORT_DLL void mmFrameScheduler_Destroy(struct mmFrameScheduler* p)
{
    mmFrameTimer_Destroy(&p->frame_timer);
    mmTimerHeap_Destroy(&p->timer_heap);
    mmClock_Destroy(&p->clock);
    mmTimeval_Destroy(&p->next_delay);
    mmTimeval_Destroy(&p->ntime);
    mmTimespec_Destroy(&p->otime);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->locker);
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->frame_cost_usec = 0.0;
    p->sleep_usec = 0;
    p->smooth_mode = 0;
    p->nearby_time = 0;
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

MM_EXPORT_DLL void mmFrameScheduler_Lock(struct mmFrameScheduler* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmFrameScheduler_unlock(struct mmFrameScheduler* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_DLL void mmFrameScheduler_SetContext(struct mmFrameScheduler* p, void* u)
{
    p->u = u;
}

MM_EXPORT_DLL void mmFrameScheduler_SetName(struct mmFrameScheduler* p, const char* name)
{
    mmFrameTimer_SetName(&p->frame_timer, name);
}
MM_EXPORT_DLL void mmFrameScheduler_SetFrequencyFactor(struct mmFrameScheduler* p, double frequency_factor)
{
    mmFrameTimer_SetFrequencyFactor(&p->frame_timer, frequency_factor);
}
MM_EXPORT_DLL void mmFrameScheduler_SetFrequency(struct mmFrameScheduler* p, double frequency)
{
    mmFrameTimer_SetFrequency(&p->frame_timer, frequency);
}
MM_EXPORT_DLL void mmFrameScheduler_SetBackground(struct mmFrameScheduler* p, mmUInt8_t background)
{
    mmFrameTimer_SetBackground(&p->frame_timer, background);
}
MM_EXPORT_DLL void mmFrameScheduler_SetActive(struct mmFrameScheduler* p, mmUInt8_t active)
{
    mmFrameTimer_SetActive(&p->frame_timer, active);
}
MM_EXPORT_DLL void mmFrameScheduler_SetSmoothMode(struct mmFrameScheduler* p, int mode)
{
    p->smooth_mode = mode;
}
MM_EXPORT_DLL void mmFrameScheduler_SetCallback(struct mmFrameScheduler* p, const struct mmFrameTimerCallback* callback)
{
    mmFrameTimer_SetCallback(&p->frame_timer, callback);
}

MM_EXPORT_DLL struct mmTimerEntry*
mmFrameScheduler_Schedule(
    struct mmFrameScheduler* p,
    mmMSec_t delay,
    mmMSec_t period,
    mmTimerEntryHandleFunc handle,
    void* u)
{
    return mmTimerHeap_Schedule(&p->timer_heap, delay, period, handle, u);
}

MM_EXPORT_DLL void mmFrameScheduler_Cancel(struct mmFrameScheduler* p, struct mmTimerEntry* entry)
{
    mmTimerHeap_Cancel(&p->timer_heap, entry);
}

MM_EXPORT_DLL void mmFrameScheduler_Update(struct mmFrameScheduler* p)
{
    p->update_cost_time = 0;
    p->max_idle_usec = MM_FRAME_TIMER_MAX_IDLE_DEFAULT;
    p->sleep_usec = 0;

    // update the motion status.
    mmFrameTimer_UpdateMotionStatus(&p->frame_timer, p->motion_status, &p->update_cost_time);

    // update the time heap status.
    mmTimerHeap_Poll(&p->timer_heap, &p->next_delay);

    // clock reset for time wait.
    mmClock_Reset(&p->clock);
}
MM_EXPORT_DLL void mmFrameScheduler_UpdateImmediately(struct mmFrameScheduler* p)
{
    // update frame immediately.
    mmFrameTimer_UpdateImmediately(&p->frame_timer);

    // update the time heap status.
    mmTimerHeap_Poll(&p->timer_heap, &p->next_delay);
}
MM_EXPORT_DLL void mmFrameScheduler_Timewait(struct mmFrameScheduler* p)
{
    double wait_usec_1 = 0.0;
    double wait_usec_2 = 0.0;

    // cache the wait usec.
    p->frame_cost_usec = (double)(mmClock_Microseconds(&p->clock));

    // time wait fixed.
    wait_usec_1 = p->frame_timer.wait_usec - p->timer_heap.frame_cost_usec - p->frame_cost_usec;
    wait_usec_2 = p->timer_heap.wait_usec - p->frame_cost_usec;

    // calculation the max idle usec.
    p->max_idle_usec = (p->max_idle_usec > wait_usec_1) ? wait_usec_1 : p->max_idle_usec;
    p->max_idle_usec = (p->max_idle_usec > wait_usec_2) ? wait_usec_2 : p->max_idle_usec;
    // smooth mode.
    p->max_idle_usec = (!p->smooth_mode) ? p->max_idle_usec : 0.0;

    // calculation sleep usec.
    p->sleep_usec = (int)(p->max_idle_usec);
    p->sleep_usec = (0 > p->sleep_usec) ? 0 : p->sleep_usec;
    // [0, sleep_usec - 1] for little quick timewake.
    p->sleep_usec = (0 < p->sleep_usec) ? p->sleep_usec - 1 : p->sleep_usec;

    // calculation nearby time.
    p->nearby_time = p->sleep_usec;
    // timewait nearby.
    mmTimewait_USecNearby(&p->signal_cond, &p->signal_mutex, &p->ntime, &p->otime, p->nearby_time);
}
MM_EXPORT_DLL void mmFrameScheduler_Timewake(struct mmFrameScheduler* p)
{
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}

MM_EXPORT_DLL void mmFrameScheduler_EnterBackground(struct mmFrameScheduler* p)
{
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_BACKGROUND;
}
MM_EXPORT_DLL void mmFrameScheduler_EnterForeground(struct mmFrameScheduler* p)
{
    p->motion_status = MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND;
}

MM_EXPORT_DLL void mmFrameScheduler_Start(struct mmFrameScheduler* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
}
MM_EXPORT_DLL void mmFrameScheduler_Interrupt(struct mmFrameScheduler* p)
{
    p->state = MM_TS_CLOSED;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmFrameScheduler_Shutdown(struct mmFrameScheduler* p)
{
    p->state = MM_TS_FINISH;
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_DLL void mmFrameScheduler_Join(struct mmFrameScheduler* p)
{
    // do nothing here, mmFrameScheduler not thread context, just api full here.
}

