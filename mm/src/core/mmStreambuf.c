/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmStreambuf.h"
#include "core/mmAlloc.h"

// realloc size (nsize + 1024 * 1024 * n, n < 32) will case a malloc and memcpy.
MM_EXPORT_DLL void mmStreambuf_Init(struct mmStreambuf* p)
{
    // setp
    p->pptr = 0;
    // setg
    p->gptr = 0;
    //
    p->size = 0;
    p->buff = NULL;
}
MM_EXPORT_DLL void mmStreambuf_Destroy(struct mmStreambuf* p)
{
    // setp
    p->pptr = 0;
    // setg
    p->gptr = 0;
    //
    mmFree(p->buff);
    //
    p->size = 0;
    p->buff = NULL;
}
MM_EXPORT_DLL void mmStreambuf_CopyFrom(struct mmStreambuf* p, const struct mmStreambuf* q)
{
    mmUInt8_t* buff = (mmUInt8_t*)mmMalloc(q->size);
    mmMemcpy(buff, q->buff, q->size);
    mmFree(p->buff);
    p->buff = buff;
    p->size = q->size;
    // setp
    p->pptr = q->pptr;
    // setg
    p->gptr = q->gptr;
}
MM_EXPORT_DLL void mmStreambuf_AddSize(struct mmStreambuf* p, size_t size)
{
    size_t cursz = p->pptr - p->gptr;
    mmUInt8_t* buff = (mmUInt8_t*)mmMalloc(p->size + size);
    mmMemcpy(buff + p->gptr, p->buff + p->gptr, cursz);
    mmFree(p->buff);
    p->buff = buff;
    p->size += size;
}
MM_EXPORT_DLL void mmStreambuf_RmvSize(struct mmStreambuf* p, size_t size)
{
    size_t cursz = p->pptr - p->gptr;
    mmUInt8_t* buff = (mmUInt8_t*)mmMalloc(p->size - size);
    mmMemcpy(buff + p->gptr, p->buff + p->gptr, cursz);
    mmFree(p->buff);
    p->buff = buff;
    p->size -= size;
}
MM_EXPORT_DLL void mmStreambuf_Removeget(struct mmStreambuf* p)
{
    size_t rsize = p->gptr;
    if (0 < rsize)
    {
        size_t wr = p->pptr;
        mmMemmove(p->buff, p->buff + rsize, p->pptr - p->gptr);
        wr -= rsize;
        // setp
        p->pptr = wr;
        // setg
        p->gptr = 0;
    }
}
MM_EXPORT_DLL void mmStreambuf_RemovegetSize(struct mmStreambuf* p, size_t rsize)
{
    size_t rdmax = 0;
    mmStreambuf_Clearget(p);
    rdmax = p->pptr - p->gptr;
    if (rdmax < rsize)
    {
        rsize = rdmax;
    }
    if (0 < rsize)
    {
        size_t wr = p->pptr;
        mmMemmove(p->buff, p->buff + rsize, p->pptr - p->gptr);
        wr -= rsize;
        // setp
        p->pptr = wr;
        // setg
        p->gptr = 0;
    }
}
MM_EXPORT_DLL void mmStreambuf_Clearget(struct mmStreambuf* p)
{
    // setg
    p->gptr = 0;
}

MM_EXPORT_DLL void mmStreambuf_Removeput(struct mmStreambuf* p)
{
    size_t wsize = p->pptr;
    if (0 < wsize)
    {
        size_t rd = p->gptr;
        mmMemmove(p->buff, p->buff + wsize, p->pptr - p->gptr);
        rd -= wsize;
        // setp
        p->pptr = 0;
        // setg
        p->gptr = rd;
    }
}
MM_EXPORT_DLL void mmStreambuf_RemoveputSize(struct mmStreambuf* p, size_t wsize)
{
    size_t wrmax = 0;
    mmStreambuf_Clearput(p);
    wrmax = p->pptr - p->gptr;
    if (wrmax < wsize)
    {
        wsize = wrmax;
    }
    if (0 < wsize)
    {
        size_t rd = p->gptr;
        mmMemmove(p->buff, p->buff + wsize, p->pptr - p->gptr);
        rd -= wsize;
        // setp
        p->pptr = 0;
        // setg
        p->gptr = rd;
    }
}
MM_EXPORT_DLL void mmStreambuf_Clearput(struct mmStreambuf* p)
{
    // setp
    p->pptr = 0;
}

MM_EXPORT_DLL size_t mmStreambuf_Getsize(const struct mmStreambuf* p)
{
    return p->gptr;
}
MM_EXPORT_DLL size_t mmStreambuf_Putsize(const struct mmStreambuf* p)
{
    return p->pptr;
}
MM_EXPORT_DLL size_t mmStreambuf_Size(const struct mmStreambuf* p)
{
    return p->pptr - p->gptr;
}

// set get pointer to new pointer.
MM_EXPORT_DLL void mmStreambuf_SetGPtr(struct mmStreambuf* p, size_t new_ptr)
{
    // setg
    p->gptr = new_ptr;
}
// set put pointer to new pointer.
MM_EXPORT_DLL void mmStreambuf_SetPPtr(struct mmStreambuf* p, size_t new_ptr)
{
    // setp
    p->pptr = new_ptr;
}
MM_EXPORT_DLL void mmStreambuf_Reset(struct mmStreambuf* p)
{
    // setp
    p->pptr = 0;
    // setg
    p->gptr = 0;
}
// get get pointer(offset).
MM_EXPORT_DLL size_t mmStreambuf_GPtr(const struct mmStreambuf* p)
{
    return p->gptr;
}
// get put pointer(offset).
MM_EXPORT_DLL size_t mmStreambuf_PPtr(const struct mmStreambuf* p)
{
    return p->pptr;
}

MM_EXPORT_DLL size_t mmStreambuf_Sgetn(struct mmStreambuf* p, mmUInt8_t* s, size_t o, size_t n)
{
    while (p->gptr + n > p->size)
    {
        mmStreambuf_OverflowSgetn(p);
    }
    mmMemcpy(s + o, p->buff + p->gptr, n);
    p->gptr += n;
    return n;
}

MM_EXPORT_DLL size_t mmStreambuf_Sputn(struct mmStreambuf* p, mmUInt8_t* s, size_t o, size_t n)
{
    while (p->pptr + n > p->size)
    {
        mmStreambuf_OverflowSputn(p);
    }
    mmMemcpy(p->buff + p->pptr, s + o, n);
    p->pptr += n;
    return n;
}

MM_EXPORT_DLL void mmStreambuf_Gbump(struct mmStreambuf* p, size_t n)
{
    p->gptr += n;
}
MM_EXPORT_DLL void mmStreambuf_Pbump(struct mmStreambuf* p, size_t n)
{
    p->pptr += n;
}

MM_EXPORT_DLL void mmStreambuf_OverflowSgetn(struct mmStreambuf* p)
{
    if (0 < p->gptr)
    {
        // if gptr have g data.we try remove for free space.
        mmStreambuf_Removeget(p);
    }
    else
    {
        // if gptr no data we just add page size.
        mmStreambuf_AddSize(p, MM_STREAMBUF_PAGE_SIZE);
    }
}
MM_EXPORT_DLL void mmStreambuf_OverflowSputn(struct mmStreambuf* p)
{
    mmStreambuf_AddSize(p, MM_STREAMBUF_PAGE_SIZE);
}
// try decrease.
// cursz = p->pptr - p->gptr;
// fresz = p->size - cursz;
// fresz > n && (p->size)(1/4) > page && (p->size)(3/4) < empty_size
MM_EXPORT_DLL void mmStreambuf_TryDecrease(struct mmStreambuf* p, size_t n)
{
    size_t cursz = p->pptr - p->gptr;
    size_t fresz = p->size - cursz;
    // the surplus size for decrease.
    size_t ssize = fresz - n;
    if (fresz > n && MM_STREAMBUF_PAGE_SIZE < ssize)
    {
        size_t sz_1_4 = p->size / 4;
        size_t sz_3_4 = sz_1_4 * 3;
        size_t sz_sub = sz_1_4 / MM_STREAMBUF_PAGE_SIZE;
        if (sz_sub > 0 && sz_3_4 < ssize)
        {
            size_t sz_rel = sz_sub * MM_STREAMBUF_PAGE_SIZE;
            // here first removeget.
            mmStreambuf_Removeget(p);
            // rmv surplus size.
            mmStreambuf_RmvSize(p, sz_rel);
        }
        else
        {
            // only removeget.
            mmStreambuf_Removeget(p);
        }
    }
    else
    {
        // only removeget.
        mmStreambuf_Removeget(p);
    }
}
// try increase.
MM_EXPORT_DLL void mmStreambuf_TryIncrease(struct mmStreambuf* p, size_t n)
{
    if (p->pptr + n > p->size)
    {
        size_t dsz = p->pptr + n - p->size;
        size_t asz = dsz / MM_STREAMBUF_PAGE_SIZE;
        size_t bsz = dsz % MM_STREAMBUF_PAGE_SIZE;
        size_t nsz = (asz + (0 == bsz ? 0 : 1));
        mmStreambuf_AddSize(p, nsz * MM_STREAMBUF_PAGE_SIZE);
    }
}
// aligned streambuf memory if need sputn n size buffer before.
MM_EXPORT_DLL void mmStreambuf_AlignedMemory(struct mmStreambuf* p, size_t n)
{
    // if current pptr + n not enough for target n.we make a aligned memory.
    if (p->pptr + n > p->size)
    {
        size_t cursz = p->pptr - p->gptr;
        // the free size is enough and memmove size is <  MM_STREAMBUF_PAGE_SIZE.
        if (p->size - cursz >= n && MM_STREAMBUF_PAGE_SIZE > cursz)
        {
            mmStreambuf_TryDecrease(p, n);
        }
        else
        {
            mmStreambuf_TryIncrease(p, n);
        }
    }
}
