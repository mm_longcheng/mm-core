/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBinarySerach_h__
#define __mmBinarySerach_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// (arr[idx] <= v && v < arr[idx + 1])
MM_EXPORT_DLL 
size_t 
mmBinarySerachArrayStepIndexFloat(
    const float*                                   arr, 
    size_t                                         o, 
    size_t                                         l, 
    float                                          v);

// (arr[idx] <= v && v < arr[idx + 1])
MM_EXPORT_DLL 
size_t 
mmBinarySerachArrayStepIndexInt(
    const int*                                     arr, 
    size_t                                         o, 
    size_t                                         l, 
    int                                            v);

// base[idx] == key
MM_EXPORT_DLL
size_t
mmBinarySerachArrayFindEqual(
    const void*                                    key,
    const void*                                    base,
    size_t                                         size,
    size_t                                         nitems,
    int(*compare)(const void* item, const void* key));

// base[idx] <= key
MM_EXPORT_DLL
size_t
mmBinarySerachArrayFindFirstEqualLarger(
    const void*                                    key,
    const void*                                    base,
    size_t                                         size,
    size_t                                         nitems,
    int(*compare)(const void* item, const void* key));

#include "core/mmSuffix.h"

#endif//__mmBinarySerach_h__
