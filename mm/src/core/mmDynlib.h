/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDynlib_h__
#define __mmDynlib_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#if MM_PLATFORM == MM_PLATFORM_WIN32
#   if defined(_MSC_VER)
#       pragma warning(disable : 4552)  // warning: operator has no effect; expected operator with side-effect
#   endif
#   ifndef WIN32_LEAN_AND_MEAN
#   define WIN32_LEAN_AND_MEAN
#   endif//WIN32_LEAN_AND_MEAN
#   include <windows.h>
#    define MM_DYNLIB_HANDLE hInstance
#    define MM_DYNLIB_LOAD( a ) LoadLibraryExA( a, NULL, 0 ) // we can not use LOAD_WITH_ALTERED_SEARCH_PATH with relative paths
#    define MM_DYNLIB_GETSYM( a, b ) GetProcAddress( a, b )
#    define MM_DYNLIB_UNLOAD( a ) !FreeLibrary( a )

struct HINSTANCE__;
typedef struct HINSTANCE__* hInstance;

static mmInline void mmDynlib_Error(struct mmString* error_desc)
{
    LPSTR lpMsgBuf;
    FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPSTR)&lpMsgBuf,
        0,
        NULL
    );

    mmString_Assigns(error_desc, lpMsgBuf);
    // Free the buffer.
    LocalFree(lpMsgBuf);
}

#elif MM_PLATFORM == MM_PLATFORM_WINRT
#  undef min
#  undef max
#  if defined( __MINGW32__ )
#    include <unistd.h>
#  endif
#    define MM_DYNLIB_HANDLE hInstance
#    define MM_DYNLIB_LOAD( a ) LoadPackagedLibrary( UTFString(a).asWStr_c_str(), 0 )
#    define MM_DYNLIB_GETSYM( a, b ) GetProcAddress( a, b )
#    define MM_DYNLIB_UNLOAD( a ) !FreeLibrary( a )

struct HINSTANCE__;
typedef struct HINSTANCE__* hInstance;

static mmInline void mmDynlib_Error(struct mmString* error_desc)
{
    WCHAR wideMsgBuf[1024];
    if (0 == FormatMessageW(
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        wideMsgBuf,
        sizeof(wideMsgBuf) / sizeof(wideMsgBuf[0]),
        NULL
    ))
    {
        wideMsgBuf[0] = 0;
    }

    char narrowMsgBuf[2048] = "";
    if (0 == WideCharToMultiByte(
        CP_ACP, 0,
        wideMsgBuf, -1,
        narrowMsgBuf, sizeof(narrowMsgBuf) / sizeof(narrowMsgBuf[0]),
        NULL, NULL))
    {
        narrowMsgBuf[0] = 0;
    }
    mmString_Assigns(error_desc, narrowMsgBuf);
}

#elif MM_PLATFORM == MM_PLATFORM_LINUX || \
      MM_PLATFORM == MM_PLATFORM_ANDROID || \
      MM_PLATFORM == MM_PLATFORM_NACL || \
      MM_PLATFORM == MM_PLATFORM_FLASHCC

#include "core/mmPrefix.h"
#   include <unistd.h>
#   include <dlfcn.h>
#include "core/mmSuffix.h"

#    define MM_DYNLIB_HANDLE void*
#    define MM_DYNLIB_LOAD( a ) dlopen( a, RTLD_LAZY | RTLD_GLOBAL)
#    define MM_DYNLIB_GETSYM( a, b ) dlsym( a, b )
#    define MM_DYNLIB_UNLOAD( a ) dlclose( a )

static mmInline void mmDynlib_Error(struct mmString* error_desc)
{
    const char* e = (const char*)dlerror();
    mmString_Assigns(error_desc, (NULL == e) ? "" : e);
}

#elif MM_PLATFORM == MM_PLATFORM_APPLE || MM_PLATFORM == MM_PLATFORM_APPLE_IOS

#include "core/mmPrefix.h"
#   include <unistd.h>
#   include <sys/param.h>
#   include <CoreFoundation/CoreFoundation.h>
#   include <dlfcn.h>
#include "core/mmSuffix.h"

#    define MM_DYNLIB_HANDLE void*
#    define MM_DYNLIB_LOAD( a ) dlopen( a, RTLD_LAZY | RTLD_GLOBAL)
#    define MM_FRAMEWORK_LOAD( a ) mac_loadFramework( a )
#    define MM_DYNLIB_GETSYM( a, b ) dlsym( a, b )
#    define MM_DYNLIB_UNLOAD( a ) dlclose( a )

static mmInline void mmDynlib_Error(struct mmString* error_desc)
{
    const char* e = (const char*)dlerror();
    mmString_Assigns(error_desc, (NULL == e) ? "" : e);
}

#endif

#endif//__mmDynlib_h__
