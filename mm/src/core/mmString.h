/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmString_h__
#define __mmString_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// clang generally mode simplify string.
// https://github.com/llvm-mirror/libcxx/blob/master/include/string
// https://joellaity.com/2020/01/31/string.html

typedef char mmUtf8_t;

MM_EXPORT_DLL size_t mmUtf8Strlen(const mmUtf8_t* s);

struct mmStringLong
{
    // capacity
    size_t capacity;
    // size
    size_t size;
    // data pointer
    char*  data;
};

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// bigger endian
#define mmStringTinyMaskMacro 0x80
#define mmStringLongMaskMacro ~((size_t)(~((size_t)(0))) >> 1)
#else
// little endian
#define mmStringTinyMaskMacro 0x01
#define mmStringLongMaskMacro 0x1ul
#endif

MM_EXPORT_DLL extern const size_t mmStringTinyMask;
MM_EXPORT_DLL extern const size_t mmStringLongMask;
MM_EXPORT_DLL extern const size_t mmStringNpos;

enum
{
    mmStringLongCapacity = (sizeof(struct mmStringLong) - 1) / sizeof(char),
    mmStringTinyCapacity = mmStringLongCapacity > 2 ? mmStringLongCapacity : 2,
};

struct mmStringTiny
{
    union
    {
        // size
        unsigned char size;
        // lx
        char lx;
    };
    // data buffer
    char data[mmStringTinyCapacity];
};

union mmStringUlxx { struct mmStringLong __lxx; struct mmStringTiny __txx; };

enum { mmStringNWords = sizeof(union mmStringUlxx) / sizeof(size_t) };

struct mmStringRaws
{
    // raw words buffer
    size_t words[mmStringNWords];
};

struct mmStringRep
{
    union
    {
        struct mmStringLong l;
        struct mmStringTiny s;
        struct mmStringRaws r;
    };
};

struct mmString
{
    union
    {
        // long
        struct mmStringLong l;
        // tiny
        struct mmStringTiny s;
        // raws
        struct mmStringRaws r;
    };
};

MM_EXPORT_DLL void mmString_Init(struct mmString* p);
MM_EXPORT_DLL void mmString_Destroy(struct mmString* p);

MM_EXPORT_DLL int mmString_Assign(struct mmString* p, const struct mmString* __str);
MM_EXPORT_DLL int mmString_Assigns(struct mmString* p, const char* __s);
MM_EXPORT_DLL int mmString_Assignsn(struct mmString* p, const char* __s, size_t __n);
MM_EXPORT_DLL int mmString_Assignnc(struct mmString* p, size_t __n, char __c);

MM_EXPORT_DLL int mmString_Insert(struct mmString* p, size_t __pos, const struct mmString* __str);
MM_EXPORT_DLL int mmString_Inserts(struct mmString* p, size_t __pos, const char* __s);
MM_EXPORT_DLL int mmString_Insertsn(struct mmString* p, size_t __pos, const char* __s, size_t __n);
MM_EXPORT_DLL int mmString_Insertnc(struct mmString* p, size_t __pos, size_t __n, char __c);

MM_EXPORT_DLL int mmString_Erase(struct mmString* p, size_t __pos, size_t __n);

MM_EXPORT_DLL size_t mmString_Size(const struct mmString* p);
MM_EXPORT_DLL int mmString_Resize(struct mmString* p, size_t __n);
MM_EXPORT_DLL int mmString_ResizeChar(struct mmString* p, size_t __n, char __c);
MM_EXPORT_DLL size_t mmString_Capacity(const struct mmString* p);
MM_EXPORT_DLL int mmString_Reserve(struct mmString* p, size_t __res_arg);
MM_EXPORT_DLL void mmString_Clear(struct mmString* p);
MM_EXPORT_DLL mmBool_t mmString_Empty(const struct mmString* p);
MM_EXPORT_DLL int mmString_ShrinkToFit(struct mmString* p);

MM_EXPORT_DLL char mmString_At(const struct mmString* p, size_t __n);

MM_EXPORT_DLL int mmString_Append(struct mmString* p, const struct mmString* __str);
MM_EXPORT_DLL int mmString_Appends(struct mmString* p, const char* __s);
MM_EXPORT_DLL int mmString_Appendsn(struct mmString* p, const char* __s, size_t __n);
MM_EXPORT_DLL int mmString_Appendnc(struct mmString* p, size_t __n, char __c);
MM_EXPORT_DLL int mmString_Appendc(struct mmString* p, char __c);
MM_EXPORT_DLL int mmString_PushBack(struct mmString* p, char __c);
MM_EXPORT_DLL void mmString_PopBack(struct mmString* p);

MM_EXPORT_DLL int mmString_Replace(struct mmString* p, size_t __pos, size_t __n1, const struct mmString* __str);
MM_EXPORT_DLL int mmString_Replaces(struct mmString* p, size_t __pos, size_t __n1, const char* __s);
MM_EXPORT_DLL int mmString_Replacesn(struct mmString* p, size_t __pos, size_t __n1, const char* __s, size_t __n2);
MM_EXPORT_DLL int mmString_Replacenc(struct mmString* p, size_t __pos, size_t __n1, size_t __n2, char __c);
// s ==> t
MM_EXPORT_DLL size_t mmString_ReplaceChar(struct mmString* p, char s, char t);

MM_EXPORT_DLL const char* mmString_CStr(const struct mmString* p);
MM_EXPORT_DLL const char* mmString_Data(const struct mmString* p);

MM_EXPORT_DLL size_t mmString_FindLastOf(const struct mmString* p, char c);
MM_EXPORT_DLL size_t mmString_FindFirstOf(const struct mmString* p, char c);
MM_EXPORT_DLL size_t mmString_FindLastNotOf(const struct mmString* p, char c);
MM_EXPORT_DLL size_t mmString_FindFirstNotOf(const struct mmString* p, char c);

MM_EXPORT_DLL void mmString_Substr(const struct mmString* p, struct mmString* s, size_t o, size_t l);

// return 0 is equal.
MM_EXPORT_DLL int mmString_Compare(const struct mmString* p, const struct mmString* __str);
// return 0 is equal.
MM_EXPORT_DLL int mmString_CompareCStr(const struct mmString* p, const char* __s);

/* Expansion module */
// const char* empty string
MM_EXPORT_DLL extern const char* mmStringEmpty;
// make a weak string.
MM_EXPORT_DLL void mmString_MakeWeak(struct mmString* p, const struct mmString* __str);
MM_EXPORT_DLL void mmString_MakeWeaks(struct mmString* p, const char* __s);
MM_EXPORT_DLL void mmString_MakeWeaksn(struct mmString* p, const char* __s, size_t __n);
// set size value.
MM_EXPORT_DLL void mmString_SetSizeValue(struct mmString* p, size_t size);
// reset weak or strong string. Mix reference must reset before reassign.
MM_EXPORT_DLL void mmString_Reset(struct mmString* p);

// string Null.
#define mmString_Null      { { { 0, 0, NULL, }, }, }
// make a const weak string.
#define mmString_Make(str) { { { mmStringLongMaskMacro, (sizeof(str) - 1), (char*)str, }, }, }

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ > 4)
#   define MM_STRING_ATTR_PRINTF(fmt, arg) __attribute__((__format__ (__printf__, fmt, arg)))
#else
#   define MM_STRING_ATTR_PRINTF(fmt, arg)
#endif

#define mmStrlen strlen
#define mmStrcpy strcpy
#define mmPrintf printf
#define mmSscanf sscanf

#if (MM_WIN32)
#   define mmVsnprintf vsnprintf

//_CRT_SECURE_NO_WARNINGS
#if _MSC_VER >= 1800
#   define mmSnprintf snprintf
#else
#   define mmSnprintf _snprintf
#endif
#else
#   define mmVsnprintf vsnprintf
#   define mmSnprintf snprintf
#endif

#define mmVsprintf vsprintf
#define mmSprintf sprintf

#define mmIsspace isspace
#define mmIsgraph isgraph

#define mmTolower tolower
#define mmToupper toupper

#define mmStrcmp strcmp
#define mmStrncmp strncmp
#define mmStrdup strdup
#define mmStrrchr strrchr

MM_EXPORT_DLL int mmString_Vsprintf(struct mmString* p, const char* fmt, va_list ap)
MM_STRING_ATTR_PRINTF(2, 0);

MM_EXPORT_DLL int mmString_Sprintf(struct mmString* p, const char* fmt, ...)
MM_STRING_ATTR_PRINTF(2, 3);

#include "core/mmSuffix.h"

#endif//__mmString_h__
