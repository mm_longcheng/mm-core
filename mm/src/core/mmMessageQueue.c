/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMessageQueue.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "container/mmListVpt.h"

static void __static_mmMessageQueueCallback_HandleDefault(void* obj, void* u, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{

}
MM_EXPORT_DLL void mmMessageQueueCallback_Init(struct mmMessageQueueCallback* p)
{
    p->Handle = &__static_mmMessageQueueCallback_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmMessageQueueCallback_Destroy(struct mmMessageQueueCallback* p)
{
    p->Handle = &__static_mmMessageQueueCallback_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmMessageQueue_Init(struct mmMessageQueue* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmPoolMessage_Init(&p->pool_message);
    mmTwoLockQuque_Init(&p->lock_queue);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmMessageQueueCallback_Init(&p->callback);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->pool_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->max_pop = MM_MESSAGE_QUEUE_MAX_POPER_NUMBER;
    p->u = NULL;

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_DLL void mmMessageQueue_Destroy(struct mmMessageQueue* p)
{
    mmMessageQueue_ClearHandleRbtree(p);
    mmMessageQueue_Dispose(p);
    //
    mmPoolMessage_Destroy(&p->pool_message);
    mmTwoLockQuque_Destroy(&p->lock_queue);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmMessageQueueCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->pool_locker);
    mmSpinlock_Destroy(&p->locker);
    p->max_pop = MM_MESSAGE_QUEUE_MAX_POPER_NUMBER;
    p->u = NULL;
}

MM_EXPORT_DLL void mmMessageQueue_Lock(struct mmMessageQueue* p)
{
    mmPoolMessage_Lock(&p->pool_message);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_DLL void mmMessageQueue_Unlock(struct mmMessageQueue* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmPoolMessage_Unlock(&p->pool_message);
}

MM_EXPORT_DLL void mmMessageQueue_SetHandle(struct mmMessageQueue* p, mmUInt32_t id, mmMessageQueueHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_DLL void mmMessageQueue_SetQueueCallback(struct mmMessageQueue* p, const struct mmMessageQueueCallback* queue_callback)
{
    assert(NULL != queue_callback && "you can not assign null queue_callback.");
    p->callback = *queue_callback;
}
MM_EXPORT_DLL void mmMessageQueue_SetMaxPoperNumber(struct mmMessageQueue* p, size_t max_pop)
{
    p->max_pop = max_pop;
}
// assign context handle.
MM_EXPORT_DLL void mmMessageQueue_SetContext(struct mmMessageQueue* p, void* u)
{
    p->u = u;
}

MM_EXPORT_DLL void mmMessageQueue_ClearHandleRbtree(struct mmMessageQueue* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

MM_EXPORT_DLL int mmMessageQueue_IsEmpty(struct mmMessageQueue* p)
{
    return mmTwoLockQuque_IsEmpty(&p->lock_queue);
}

MM_EXPORT_DLL void mmMessageQueue_ThreadHandle(struct mmMessageQueue* p)
{
    mmMessageQueue_PopEntire(p);
}
MM_EXPORT_DLL void mmMessageQueue_Dispose(struct mmMessageQueue* p)
{
    struct mmPoolMessageElem* data = NULL;
    uintptr_t v = 0;

    while (MM_TRUE == mmTwoLockQuque_Dequeue(&p->lock_queue, &v))
    {
        data = (struct mmPoolMessageElem*)v;

        mmSpinlock_Lock(&p->pool_locker);
        mmPoolMessage_Recycle(&p->pool_message, data);
        mmSpinlock_Unlock(&p->pool_locker);
    }
}
MM_EXPORT_DLL void mmMessageQueue_Push(struct mmMessageQueue* p, void* obj, mmUInt32_t mid, struct mmByteBuffer* byte_buffer)
{
    struct mmPoolMessageElem* data = NULL;

    mmSpinlock_Lock(&p->pool_locker);
    data = mmPoolMessage_Produce(&p->pool_message, byte_buffer->length);
    mmSpinlock_Unlock(&p->pool_locker);

    mmPoolMessageElem_SetMid(data, mid);
    mmPoolMessageElem_SetObject(data, obj);
    mmPoolMessageElem_SetBuffer(data, byte_buffer);
    // push to message queue.
    mmTwoLockQuque_Enqueue(&p->lock_queue, (uintptr_t)data);
}
MM_EXPORT_DLL void mmMessageQueue_PopNumber(struct mmMessageQueue* p, size_t number)
{
    struct mmPoolMessageElem* data = NULL;
    uintptr_t v = 0;
    size_t n = 0;

    mmMessageQueueHandleFunc handle = NULL;

    while (n < number && MM_TRUE == mmTwoLockQuque_Dequeue(&p->lock_queue, &v))
    {
        data = (struct mmPoolMessageElem*)v;

        mmSpinlock_Lock(&p->rbtree_locker);
        handle = (mmMessageQueueHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree, data->mid);
        mmSpinlock_Unlock(&p->rbtree_locker);
        if (NULL != handle)
        {
            (*(handle))(data->obj, p->u, data->mid, &data->byte_buffer);
        }
        else
        {
            // if not define the handler,fire the default function.
            assert(NULL != p->callback.Handle && "p->callback.Handle is a null.");
            (*(p->callback.Handle))(data->obj, p->u, data->mid, &data->byte_buffer);
        }
        mmSpinlock_Lock(&p->pool_locker);
        mmPoolMessage_Recycle(&p->pool_message, data);
        mmSpinlock_Unlock(&p->pool_locker);

        data = NULL;

        n++;
    }
}
MM_EXPORT_DLL void mmMessageQueue_PopEntire(struct mmMessageQueue* p)
{
    mmMessageQueue_PopNumber(p, p->max_pop);
}
MM_EXPORT_DLL void mmMessageQueue_PopSingle(struct mmMessageQueue* p)
{
    mmMessageQueue_PopNumber(p, 1);
}

MM_EXPORT_DLL struct mmPoolMessageElem* mmMessageQueue_Overdraft(struct mmMessageQueue* p, size_t length)
{
    struct mmPoolMessageElem* message_elem = NULL;

    mmSpinlock_Lock(&p->pool_locker);
    message_elem = mmPoolMessage_Produce(&p->pool_message, length);
    mmSpinlock_Unlock(&p->pool_locker);

    return message_elem;
}
MM_EXPORT_DLL void mmMessageQueue_Repayment(struct mmMessageQueue* p, struct mmPoolMessageElem* message_elem)
{
    // push to message queue.
    mmTwoLockQuque_Enqueue(&p->lock_queue, (uintptr_t)message_elem);
}
