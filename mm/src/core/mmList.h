/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmList_h__
#define __mmList_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmListHead
{
    struct mmListHead *next, *prev;
};

struct mmHListNode
{
    struct mmHListNode *next, **pprev;
};

struct mmHListHead
{
    struct mmHListNode *first;
};

/*
 * These are non-NULL pointers that will result in page faults
 * under normal circumstances, used to verify that nobody uses
 * non-initialized list entries.
 */
#define MM_LIST_POISON1  ((void *) (0x00100100))
#define MM_LIST_POISON2  ((void *) (0x00200200))

/*
 * Simple doubly linked list implementation.
 *
 * Some of the internal functions ("__xxx") are useful when
 * manipulating whole lists rather than single entries, as
 * sometimes we already know the next/prev entries and we can
 * generate better code by using them directly rather than
 * using the generic single-entry routines.
 */

#define MM_LIST_HEAD_INIT(name) { &(name), &(name) }

#define MM_LIST_HEAD(name) \
  struct mmListHead name = MM_LIST_HEAD_INIT(name)

static mmInline void MM_LIST_INIT_HEAD(struct mmListHead *list)
{
    list->next = list;
    list->prev = list;
}

/*
 * Insert a new entry between two known consecutive entries.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static mmInline void __mmList_Add(struct mmListHead *newe,
    struct mmListHead *prev,
    struct mmListHead *next)
{
    next->prev = newe;
    newe->next = next;
    newe->prev = prev;
    prev->next = newe;
}

/**
 * mmList_Add - add a new entry
 * @newe: new entry to be added
 * @head: list head to add it after
 *
 * Insert a new entry after the specified head.
 * This is good for implementing stacks.
 */
static mmInline void mmList_Add(struct mmListHead *newe, struct mmListHead *head)
{
    __mmList_Add(newe, head, head->next);
}


/**
 * mmList_AddTail - add a new entry
 * @newe: new entry to be added
 * @head: list head to add it before
 *
 * Insert a new entry before the specified head.
 * This is useful for implementing queues.
 */
static mmInline void mmList_AddTail(struct mmListHead *newe, struct mmListHead *head)
{
    __mmList_Add(newe, head->prev, head);
}

/*
 * Delete a list entry by making the prev/next entries
 * point to each other.
 *
 * This is only for internal list manipulation where we know
 * the prev/next entries already!
 */
static mmInline void __mmList_Del(struct mmListHead * prev, struct mmListHead * next)
{
    next->prev = prev;
    prev->next = next;
}

/**
 * mmList_Del - deletes entry from list.
 * @entry: the element to delete from the list.
 * Note: mmList_Empty() on entry does not return true after this, the entry is
 * in an undefined state.
 */
static mmInline void __mmList_DelEntry(struct mmListHead *entry)
{
    __mmList_Del(entry->prev, entry->next);
}

static mmInline void mmList_Del(struct mmListHead *entry)
{
    __mmList_Del(entry->prev, entry->next);
    entry->next = (struct mmListHead *)MM_LIST_POISON1;
    entry->prev = (struct mmListHead *)MM_LIST_POISON2;
}

/**
 * mmList_Replace - replace old entry by new one
 * @olde : the element to be replaced
 * @newe : the new element to insert
 *
 * If @old was empty, it will be overwritten.
 */
static mmInline void mmList_Replace(struct mmListHead *olde,
    struct mmListHead *newe)
{
    newe->next = olde->next;
    newe->next->prev = newe;
    newe->prev = olde->prev;
    newe->prev->next = newe;
}

static mmInline void mmList_ReplaceInit(struct mmListHead *olde,
    struct mmListHead *newe)
{
    mmList_Replace(olde, newe);
    MM_LIST_INIT_HEAD(olde);
}

/**
 * mmList_DelInit - deletes entry from list and reinitialize it.
 * @entry: the element to delete from the list.
 */
static mmInline void mmList_DelInit(struct mmListHead *entry)
{
    __mmList_DelEntry(entry);
    MM_LIST_INIT_HEAD(entry);
}

/**
 * mmList_Move - delete from one list and add as another's head
 * @list: the entry to move
 * @head: the head that will precede our entry
 */
static mmInline void mmList_Move(struct mmListHead *list, struct mmListHead *head)
{
    __mmList_DelEntry(list);
    mmList_Add(list, head);
}

/**
 * mmList_MoveTail - delete from one list and add as another's tail
 * @list: the entry to move
 * @head: the head that will follow our entry
 */
static mmInline void mmList_MoveTail(struct mmListHead *list,
    struct mmListHead *head)
{
    __mmList_DelEntry(list);
    mmList_AddTail(list, head);
}

/**
 * mmList_IsLast - tests whether @list is the last entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
static mmInline int mmList_IsLast(const struct mmListHead *list,
    const struct mmListHead *head)
{
    return list->next == head;
}

/**
 * mmList_Empty - tests whether a list is empty
 * @head: the list to test.
 */
static mmInline int mmList_Empty(const struct mmListHead *head)
{
    return head->next == head;
}

/**
 * mmList_EmptyCareful - tests whether a list is empty and not being modified
 * @head: the list to test
 *
 * Description:
 * tests whether a list is empty _and_ checks that no other CPU might be
 * in the process of modifying either member (next or prev)
 *
 * NOTE: using mmList_EmptyCareful() without synchronization
 * can only be safe if the only activity that can happen
 * to the list entry is mmList_DelInit(). Eg. it cannot be used
 * if another CPU could re-list_add() it.
 */
static mmInline int mmList_EmptyCareful(const struct mmListHead *head)
{
    struct mmListHead *next = head->next;
    return (next == head) && (next == head->prev);
}

/**
 * mmList_RotateLeft - rotate the list to the left
 * @head: the head of the list
 */
static mmInline void mmList_RotateLeft(struct mmListHead *head)
{
    struct mmListHead *first;

    if (!mmList_Empty(head)) 
    {
        first = head->next;
        mmList_MoveTail(first, head);
    }
}

/**
 * mmList_IsSingular - tests whether a list has just one entry.
 * @head: the list to test.
 */
static mmInline int mmList_IsSingular(const struct mmListHead *head)
{
    return !mmList_Empty(head) && (head->next == head->prev);
}

static mmInline void __mmList_CutPosition(struct mmListHead *list,
    struct mmListHead *head, struct mmListHead *entry)
{
    struct mmListHead *new_first = entry->next;
    list->next = head->next;
    list->next->prev = list;
    list->prev = entry;
    entry->next = list;
    head->next = new_first;
    new_first->prev = head;
}

/**
 * mmList_CutPosition - cut a list into two
 * @list: a new list to add all removed entries
 * @head: a list with entries
 * @entry: an entry within head, could be the head itself
 *  and if so we won't cut the list
 *
 * This helper moves the initial part of @head, up to and
 * including @entry, from @head to @list. You should
 * pass on @entry an element you know is on @head. @list
 * should be an empty list or a list you do not care about
 * losing its data.
 *
 */
static mmInline void mmList_CutPosition(struct mmListHead *list,
    struct mmListHead *head, struct mmListHead *entry)
{
    if (mmList_Empty(head))
        return;
    if (mmList_IsSingular(head) &&
        (head->next != entry && head != entry))
        return;
    if (entry == head)
        MM_LIST_INIT_HEAD(list);
    else
        __mmList_CutPosition(list, head, entry);
}

static mmInline void __mmList_Splice(const struct mmListHead *list,
    struct mmListHead *prev,
    struct mmListHead *next)
{
    struct mmListHead *first = list->next;
    struct mmListHead *last = list->prev;

    first->prev = prev;
    prev->next = first;

    last->next = next;
    next->prev = last;
}

/**
 * mmList_Splice - join two lists, this is designed for stacks
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 */
static mmInline void mmList_Splice(const struct mmListHead *list,
    struct mmListHead *head)
{
    if (!mmList_Empty(list))
        __mmList_Splice(list, head, head->next);
}

/**
 * mmList_SpliceTail - join two lists, each list being a queue
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 */
static mmInline void mmList_SpliceTail(struct mmListHead *list,
    struct mmListHead *head)
{
    if (!mmList_Empty(list))
        __mmList_Splice(list, head->prev, head);
}

/**
 * mmList_Splice_init - join two lists and reinitialise the emptied list.
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 *
 * The list at @list is reinitialised
 */
static mmInline void mmList_SpliceInit(struct mmListHead *list,
    struct mmListHead *head)
{
    if (!mmList_Empty(list)) {
        __mmList_Splice(list, head, head->next);
        MM_LIST_INIT_HEAD(list);
    }
}

/**
 * mmList_SpliceTailInit - join two lists and reinitialise the emptied list
 * @list: the new list to add.
 * @head: the place to add it in the first list.
 *
 * Each of the lists is a queue.
 * The list at @list is reinitialised
 */
static mmInline void mmList_SpliceTailInit(struct mmListHead *list,
    struct mmListHead *head)
{
    if (!mmList_Empty(list)) 
    {
        __mmList_Splice(list, head->prev, head);
        MM_LIST_INIT_HEAD(list);
    }
}

/**
 * mmList_Entry - get the struct for this entry
 * @ptr:  the &struct mmListHead pointer.
 * @type: the type of the struct this is embedded in.
 * @member: the name of the list_head within the struct.
 */
#define mmList_Entry(ptr, type, member) \
    mmContainerOf(ptr, type, member)

/**
 * mmList_FirstEntry - get the first element from a list
 * @ptr: the list head to take the element from.
 * @type:  the type of the struct this is embedded in.
 * @member:  the name of the list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define mmList_FirstEntry(ptr, type, member) \
    mmList_Entry((ptr)->next, type, member)

/**
 * mmList_LastEntry - get the last element from a list
 * @ptr:  the list head to take the element from.
 * @type: the type of the struct this is embedded in.
 * @member: the name of the list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define mmList_LastEntry(ptr, type, member) \
    mmList_Entry((ptr)->prev, type, member)

/**
 * mmList_FirstEntryOrNull - get the first element from a list
 * @ptr: the list head to take the element from.
 * @type:  the type of the struct this is embedded in.
 * @member:  the name of the list_head within the struct.
 *
 * Note that if the list is empty, it returns NULL.
 */
#define mmList_FirstEntryOrNull(ptr, type, member) \
    (!mmList_Empty(ptr) ? mmList_FirstEntry(ptr, type, member) : NULL)

/**
 * mmList_NextEntry - get the next element in list
 * @pos:  the type * to cursor
 * @member: the name of the list_head within the struct.
 */
#define mmList_NextEntry(pos, member) \
    mmList_Entry((pos)->member.next, typeof(*(pos)), member)

/**
 * mmList_PrevEntry - get the prev element in list
 * @pos: the type * to cursor
 * @member:  the name of the list_head within the struct.
 */
#define mmList_PrevEntry(pos, member) \
    mmList_Entry((pos)->member.prev, typeof(*(pos)), member)

/**
 * mmList_ForEach - iterate over a list
 * @pos:  the &struct mmListHead to use as a loop cursor.
 * @head: the head for your list.
 */
#define mmList_ForEach(pos, head) \
    for (pos = (head)->next; pos != (head); pos = pos->next)

/**
 * mmList_ForEachPrev  - iterate over a list backwards
 * @pos: the &struct mmListHead to use as a loop cursor.
 * @head:  the head for your list.
 */
#define mmList_ForEachPrev(pos, head) \
    for (pos = (head)->prev; pos != (head); pos = pos->prev)

/**
 * mmList_ForEachSafe - iterate over a list safe against removal of list entry
 * @pos:  the &struct mmListHead to use as a loop cursor.
 * @n:    another &struct mmListHead to use as temporary storage
 * @head: the head for your list.
 */
#define mmList_ForEachSafe(pos, n, head) \
    for (pos = (head)->next, n = pos->next; pos != (head); \
         pos = n, n = pos->next)

/**
 * mmList_ForEachPrevSafe - iterate over a list backwards safe against removal of list entry
 * @pos: the &struct mmListHead to use as a loop cursor.
 * @n:   another &struct mmListHead to use as temporary storage
 * @head:  the head for your list.
 */
#define mmList_ForEachPrevSafe(pos, n, head) \
    for (pos = (head)->prev, n = pos->prev; \
         pos != (head); \
         pos = n, n = pos->prev)

/**
 * mmList_ForEachEntry  - iterate over list of given type
 * @pos:  the type * to use as a loop cursor.
 * @head: the head for your list.
 * @member: the name of the list_head within the struct.
 */
#define mmList_ForEachEntry(pos, head, member)        \
    for (pos = mmList_FirstEntry(head, typeof(*pos), member); \
         &pos->member != (head);          \
         pos = mmList_NextEntry(pos, member))

/**
 * mmList_ForEachEntryReverse - iterate backwards over list of given type.
 * @pos: the type * to use as a loop cursor.
 * @head:  the head for your list.
 * @member:  the name of the list_head within the struct.
 */
#define mmList_ForEachEntryReverse(pos, head, member)     \
    for (pos = mmList_LastEntry(head, typeof(*pos), member);    \
         &pos->member != (head);          \
         pos = mmList_PrevEntry(pos, member))

/**
 * mmList_PrepareEntry - prepare a pos entry for use in mmList_ForEachEntryContinue()
 * @pos:  the type * to use as a start point
 * @head: the head of the list
 * @member: the name of the list_head within the struct.
 *
 * Prepares a pos entry for use as a start point in mmList_ForEachEntryContinue().
 */
#define mmList_PrepareEntry(pos, head, member) \
    ((pos) ? : mmList_Entry(head, typeof(*pos), member))

/**
 * mmList_ForEachEntryContinue - continue iteration over list of given type
 * @pos: the type * to use as a loop cursor.
 * @head:  the head for your list.
 * @member:  the name of the list_head within the struct.
 *
 * Continue to iterate over list of given type, continuing after
 * the current position.
 */
#define mmList_ForEachEntryContinue(pos, head, member)    \
    for (pos = mmList_NextEntry(pos, member);     \
         &pos->member != (head);          \
         pos = mmList_NextEntry(pos, member))

/**
 * mmList_ForEachEntryContinueReverse - iterate backwards from the given point
 * @pos:  the type * to use as a loop cursor.
 * @head: the head for your list.
 * @member: the name of the list_head within the struct.
 *
 * Start to iterate over list of given type backwards, continuing after
 * the current position.
 */
#define mmList_ForEachEntryContinueReverse(pos, head, member)   \
    for (pos = mmList_PrevEntry(pos, member);     \
         &pos->member != (head);          \
         pos = mmList_PrevEntry(pos, member))

/**
 * mmList_ForEachEntryFrom - iterate over list of given type from the current point
 * @pos: the type * to use as a loop cursor.
 * @head:  the head for your list.
 * @member:  the name of the list_head within the struct.
 *
 * Iterate over list of given type, continuing from current position.
 */
#define mmList_ForEachEntryFrom(pos, head, member)      \
    for (; &pos->member != (head);          \
         pos = mmList_NextEntry(pos, member))

/**
 * mmList_ForEachEntrySafe - iterate over list of given type safe against removal of list entry
 * @pos:  the type * to use as a loop cursor.
 * @n:    another type * to use as temporary storage
 * @head: the head for your list.
 * @member: the name of the list_head within the struct.
 */
#define mmList_ForEachEntrySafe(pos, n, head, member)     \
    for (pos = mmList_FirstEntry(head, typeof(*pos), member), \
         n = mmList_NextEntry(pos, member);     \
         &pos->member != (head);          \
         pos = n, n = mmList_NextEntry(n, member))

/**
 * mmList_ForEachEntrySafeContinue - continue list iteration safe against removal
 * @pos: the type * to use as a loop cursor.
 * @n:   another type * to use as temporary storage
 * @head:  the head for your list.
 * @member:  the name of the list_head within the struct.
 *
 * Iterate over list of given type, continuing after current point,
 * safe against removal of list entry.
 */
#define mmList_ForEachEntrySafeContinue(pos, n, head, member)     \
    for (pos = mmList_NextEntry(pos, member),         \
         n = mmList_NextEntry(pos, member);       \
         &pos->member != (head);            \
         pos = n, n = mmList_NextEntry(n, member))

/**
 * mmList_ForEachEntrySafeFrom - iterate over list from current point safe against removal
 * @pos:  the type * to use as a loop cursor.
 * @n:    another type * to use as temporary storage
 * @head: the head for your list.
 * @member: the name of the list_head within the struct.
 *
 * Iterate over list of given type from current point, safe against
 * removal of list entry.
 */
#define mmList_ForEachEntrySafeFrom(pos, n, head, member)       \
    for (n = mmList_NextEntry(pos, member);         \
         &pos->member != (head);            \
         pos = n, n = mmList_NextEntry(n, member))

/**
 * mmList_ForEachEntrySafeReverse - iterate backwards over list safe against removal
 * @pos: the type * to use as a loop cursor.
 * @n:   another type * to use as temporary storage
 * @head:  the head for your list.
 * @member:  the name of the list_head within the struct.
 *
 * Iterate backwards over list of given type, safe against removal
 * of list entry.
 */
#define mmList_ForEachEntrySafeReverse(pos, n, head, member)    \
    for (pos = mmList_LastEntry(head, typeof(*pos), member),    \
         n = mmList_PrevEntry(pos, member);     \
         &pos->member != (head);          \
         pos = n, n = mmList_PrevEntry(n, member))

/**
 * mmList_SafeResetNext - reset a stale mmList_ForEachEntrySafe loop
 * @pos:  the loop cursor used in the mmList_ForEachEntrySafe loop
 * @n:    temporary storage used in mmList_ForEachEntrySafe
 * @member: the name of the list_head within the struct.
 *
 * mmList_SafeResetNext is not safe to use in general if the list may be
 * modified concurrently (eg. the lock is dropped in the loop body). An
 * exception to this is if the cursor element (pos) is pinned in the list,
 * and mmList_SafeResetNext is called after re-taking the lock and before
 * completing the current iteration of the loop body.
 */
#define mmList_SafeResetNext(pos, n, member)        \
    n = mmList_NextEntry(pos, member)

/*
 * Double linked lists with a single pointer list head.
 * Mostly useful for hash tables where the two pointer list head is
 * too wasteful.
 * You lose the ability to access the tail in O(1).
 */

#define MM_HLIST_HEAD_INIT { .first = NULL }
#define MM_HLIST_HEAD(name) struct mmHListHead name = {  .first = NULL }
#define MM_HLIST_INIT_HEAD(ptr) ((ptr)->first = NULL)
static mmInline void MM_HLIST_INIT_NODE(struct mmHListNode *h)
{
    h->next = NULL;
    h->pprev = NULL;
}

static mmInline int mmHList_Unhashed(const struct mmHListNode *h)
{
    return !h->pprev;
}

static mmInline int mmHList_Empty(const struct mmHListHead *h)
{
    return !h->first;
}

static mmInline void __mmHList_Del(struct mmHListNode *n)
{
    struct mmHListNode *next = n->next;
    struct mmHListNode **pprev = n->pprev;
    *pprev = next;
    if (next)
        next->pprev = pprev;
}

static mmInline void mmHList_Del(struct mmHListNode *n)
{
    __mmHList_Del(n);
    n->next = (struct mmHListNode *)MM_LIST_POISON1;
    n->pprev = (struct mmHListNode **)MM_LIST_POISON2;
}

static mmInline void mmHList_DelInit(struct mmHListNode *n)
{
    if (!mmHList_Unhashed(n)) 
    {
        __mmHList_Del(n);
        MM_HLIST_INIT_NODE(n);
    }
}

static mmInline void mmHList_AddHead(struct mmHListNode *n, struct mmHListHead *h)
{
    struct mmHListNode *first = h->first;
    n->next = first;
    if (first)
        first->pprev = &n->next;
    h->first = n;
    n->pprev = &h->first;
}

/* next must be != NULL */
static mmInline void mmHList_AddBefore(struct mmHListNode *n,
    struct mmHListNode *next)
{
    n->pprev = next->pprev;
    n->next = next;
    next->pprev = &n->next;
    *(n->pprev) = n;
}

static mmInline void mmHList_AddBehind(struct mmHListNode *n,
    struct mmHListNode *prev)
{
    n->next = prev->next;
    prev->next = n;
    n->pprev = &prev->next;

    if (n->next)
        n->next->pprev = &n->next;
}

/* after that we'll appear to be on some hlist and mmHList_Del will work */
static mmInline void mmHList_AddFake(struct mmHListNode *n)
{
    n->pprev = &n->next;
}

/*
 * Move a list from one list head to another. Fixup the pprev
 * reference of the first entry if it exists.
 */
static mmInline void mmHList_MoveList(struct mmHListHead *olde,
    struct mmHListHead *newe)
{
    newe->first = olde->first;
    if (newe->first)
        newe->first->pprev = &newe->first;
    olde->first = NULL;
}

#define mmHList_Entry(ptr, type, member) mmContainerOf(ptr,type,member)

#define mmHList_ForEach(pos, head) \
    for (pos = (head)->first; pos ; pos = pos->next)

#define mmHList_ForEachSafe(pos, n, head) \
    for (pos = (head)->first; pos && ({ n = pos->next; 1; }); \
       pos = n)

#define mmHList_EntrySafe(ptr, type, member) \
    ({ typeof(ptr) ____ptr = (ptr); \
        ____ptr ? mmHList_Entry(____ptr, type, member) : NULL; \
    })

/**
 * mmHList_ForEachEntry - iterate over list of given type
 * @pos:  the type * to use as a loop cursor.
 * @head: the head for your list.
 * @member: the name of the struct mmHListNode within the struct.
 */
#define mmHList_ForEachEntry(pos, head, member)       \
    for (pos = mmHList_EntrySafe((head)->first, typeof(*(pos)), member);\
         pos;             \
         pos = mmHList_EntrySafe((pos)->member.next, typeof(*(pos)), member))

/**
 * mmHList_ForEachEntryContinue - iterate over a hlist continuing after current point
 * @pos: the type * to use as a loop cursor.
 * @member:  the name of the struct mmHListNode within the struct.
 */
#define mmHList_ForEachEntryContinue(pos, member)     \
    for (pos = mmHList_EntrySafe((pos)->member.next, typeof(*(pos)), member);\
         pos;             \
         pos = mmHList_EntrySafe((pos)->member.next, typeof(*(pos)), member))

/**
 * mmHList_ForEachEntryFrom - iterate over a hlist continuing from current point
 * @pos:  the type * to use as a loop cursor.
 * @member: the name of the struct mmHListNode within the struct.
 */
#define mmHList_ForEachEntryFrom(pos, member)       \
    for (; pos;             \
         pos = mmHList_EntrySafe((pos)->member.next, typeof(*(pos)), member))

/**
 * mmHList_ForEachEntrySafe - iterate over list of given type safe against removal of list entry
 * @pos: the type * to use as a loop cursor.
 * @n:   another &struct mmHListNode to use as temporary storage
 * @head:  the head for your list.
 * @member:  the name of the struct mmHListNode within the struct.
 */
#define mmHList_ForEachEntrySafe(pos, n, head, member)    \
    for (pos = mmHList_EntrySafe((head)->first, typeof(*pos), member);\
         pos && ({ n = pos->member.next; 1; });     \
         pos = mmHList_EntrySafe(n, typeof(*pos), member))

#include "core/mmSuffix.h"

#endif//__mmList_h__
