/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFilePath_h__
#define __mmFilePath_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmByte.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL
const char*
mmPathFileExtension(
    const char*                                    filename);

// a/b/c.txt => a/b/ c.txt
MM_EXPORT_DLL 
void 
mmPathSplitFileName(
    struct mmString*                               qualified_name,
    struct mmString*                               basename, 
    struct mmString*                               pathname);

// a.txt => a .txt
MM_EXPORT_DLL 
void 
mmPathSplitSuffixName(
    struct mmString*                               qualified_name, 
    struct mmString*                               basename, 
    struct mmString*                               suffixname);

// clean path.
//   "./xxx" => xxx
//   "xxx/." => xxx
//   "/.." "/./"
// ././a/b/../c.txt => ./a/c.txt
MM_EXPORT_DLL 
void 
mmCleanPath(
    struct mmString*                               clean_path, 
    const char*                                    complex_path);

// a/b  => a/b/
MM_EXPORT_DLL 
void 
mmDirectoryHaveSuffix(
    struct mmString*                               directory_path, 
    const char*                                    complex_path);

// a/b/ => a/b
MM_EXPORT_DLL 
void 
mmDirectoryNoneSuffix(
    struct mmString*                               directory_path, 
    const char*                                    complex_path);

// a/b/c.txt a  => b/c.txt
MM_EXPORT_DLL 
void 
mmDirectoryRemovePrefixPath(
    struct mmString*                               directory_path, 
    const char*                                    pathname, 
    const char*                                    prefix_path);

// a/b/c.txt => a/ b
// a/b/      => a/ b
MM_EXPORT_DLL 
void 
mmDirectoryParentPath(
    struct mmString*                               qualified_name, 
    struct mmString*                               basename, 
    struct mmString*                               pathname);

// a b.txt   => a/b.txt
MM_EXPORT_DLL 
void 
mmConcatenatePath(
    struct mmString*                               path, 
    const char*                                    base, 
    const char*                                    name);

// a/b/c.txt .png => a/b/c.png
// a/b/c     .png => a/b/c.png
MM_EXPORT_DLL
void
mmPathReplaceSuffix(
    const char*                                    path,
    const char*                                    suffix,
    struct mmString*                               result);

// acquire file buffer.
MM_EXPORT_DLL 
void 
mmAbsolutePathAcquireFileByteBuffer(
    const char*                                    file_name, 
    struct mmByteBuffer*                           byte_buffer);

// release file buffer.
MM_EXPORT_DLL 
void 
mmAbsolutePathReleaseFileByteBuffer(
    struct mmByteBuffer*                           byte_buffer);

// mkdir if not exist.
MM_EXPORT_DLL 
int 
mmMkdirIfNotExist(
    const char*                                    directory);

#include "core/mmSuffix.h"

#endif//__mmFilePath_h__
