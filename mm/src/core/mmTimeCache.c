/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimeCache.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmTimeUtils.h"

MM_EXPORT_DLL void mmTimeInfo_Init(struct mmTimeInfo* p)
{
    p->sec = 0;
    p->msec = 0;
    p->gmtoff = 0;
}
MM_EXPORT_DLL void mmTimeInfo_Destroy(struct mmTimeInfo* p)
{
    p->sec = 0;
    p->msec = 0;
    p->gmtoff = 0;
}

static void* __static_time_cache_thread(void* arg);

static struct mmTimeCache gTimeCache;
MM_EXPORT_DLL struct mmTimeCache* mmTimeCache_Instance(void)
{
    return &gTimeCache;
}
MM_EXPORT_DLL void mmTimeCache_Init(struct mmTimeCache* p)
{
    mmMemset(p->cached_time, 0, sizeof(struct mmTimeInfo) * MM_TIME_SLOTS);
    mmMemset(p->cached_timestamp, 0, sizeof(mmChar_t) * MM_TIME_SLOTS * MM_TIME_STRING_LENGTH);
    mmTimeInfo_Init(&p->start);
    mmSpinlock_Init(&p->locker, NULL);
    p->msec_current = 0;
    p->msec_sleep = MM_TIME_UPDATE;
    p->slot = 0;
    p->state = MM_TS_CLOSED;
    p->cache = &p->cached_time[0];
    p->string_log_time = &(p->cached_timestamp[p->slot][0]);
    //
    mmTimeCache_Update(p);
    //
    p->start = *p->cache;
}
MM_EXPORT_DLL void mmTimeCache_Destroy(struct mmTimeCache* p)
{
    mmMemset(p->cached_time, 0, sizeof(struct mmTimeInfo) * MM_TIME_SLOTS);
    mmMemset(p->cached_timestamp, 0, sizeof(mmChar_t) * MM_TIME_SLOTS * MM_TIME_STRING_LENGTH);
    mmTimeInfo_Destroy(&p->start);
    mmSpinlock_Destroy(&p->locker);
    p->msec_current = 0;
    p->msec_sleep = MM_TIME_UPDATE;
    p->slot = 0;
    p->state = MM_TS_CLOSED;
    p->cache = &p->cached_time[0];
    p->string_log_time = &(p->cached_timestamp[p->slot][0]);
}
//
MM_EXPORT_DLL void mmTimeCache_Update(struct mmTimeCache* p)
{
    char* p0 = NULL;
    time_t sec = 0;
    mmUInt_t msec = 0;
    struct mmTimeInfo* tp = NULL;
    struct timeval tv;

    mmSpinlock_Lock(&p->locker);

    mmGettimeofday(&tv, NULL);

    sec = tv.tv_sec;
    msec = tv.tv_usec / 1000;

    p->msec_current = (mmUInt64_t)sec * 1000 + msec;

    p->slot++;
    p->slot = MM_TIME_SLOTS == p->slot ? 0 : p->slot;

    tp = &p->cached_time[p->slot];

    tp->sec = sec;
    tp->msec = msec;

    p0 = &(p->cached_timestamp[p->slot][0]);

    mmTime_String(tp, p0);

    mmMemoryBarrier();

    p->string_log_time = p0;
    p->cache = tp;

    mmSpinlock_Unlock(&p->locker);
}
//
MM_EXPORT_DLL void mmTimeCache_Start(struct mmTimeCache* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->time_thread, NULL, &__static_time_cache_thread, p);
}
MM_EXPORT_DLL void mmTimeCache_Interrupt(struct mmTimeCache* p)
{
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DLL void mmTimeCache_Shutdown(struct mmTimeCache* p)
{
    p->state = MM_TS_FINISH;
}
MM_EXPORT_DLL void mmTimeCache_Join(struct mmTimeCache* p)
{
    pthread_join(p->time_thread, NULL);
}
static void* __static_time_cache_thread(void* arg)
{
    struct mmTimeCache* p = (struct mmTimeCache*)arg;
    while (MM_TS_MOTION == p->state)
    {
        mmTimeCache_Update(p);
        mmMSleep(p->msec_sleep);
    }
    return NULL;
}

MM_EXPORT_DLL void mmTime_Tm(struct mmTimeInfo* ti, struct tm *tp)
{
#if (MM_HAVE_GETTIMEZONE)

    ti->gmtoff = mmGettimezone();
    mmGMTime(ti->sec + ti->gmtoff * 60, tp);

#elif (MM_HAVE_GMTOFF)

    mmLocaltimeR(&ti->sec, tp);
    ti->gmtoff = (mmInt_t)(tp->mmTm_gmtoff / 60);

#else

    mmLocaltime(&ti->sec, tp);
    ti->gmtoff = mmTimezone(tp->mmTm_isdst);

#endif
}
MM_EXPORT_DLL void mmTime_TmConst(const struct mmTimeInfo* ti, struct tm *tp)
{
#if (MM_HAVE_GETTIMEZONE)

    mmInt_t gmtoff = mmGettimezone();
    mmGMTime(ti->sec + gmtoff * 60, tp);

#elif (MM_HAVE_GMTOFF)

    mmLocaltimeR(&ti->sec, tp);
    // ti->gmtoff = (mmInt_t) (tp->mmTm_gmtoff / 60);

#else

    mmLocaltime(&ti->sec, tp);
    // ti->gmtoff = mmTimezone(tp->mmTm_isdst);

#endif
}
MM_EXPORT_DLL void mmTime_String(struct mmTimeInfo* tp, char ts[MM_TIME_STRING_LENGTH])
{
    struct tm tm;
    mmTime_Tm(tp, &tm);
    mmSprintf(ts, "%4d/%02d/%02d %02d:%02d:%02d-%03d",
        tm.mmTm_year, tm.mmTm_mon,
        tm.mmTm_mday, tm.mmTm_hour,
        tm.mmTm_min, tm.mmTm_sec, tp->msec);
}
MM_EXPORT_DLL void mmTime_StringConst(const struct mmTimeInfo* tp, char ts[MM_TIME_STRING_LENGTH])
{
    struct tm tm;
    mmTime_TmConst(tp, &tm);
    mmSprintf(ts, "%4d/%02d/%02d %02d:%02d:%02d-%03d",
        tm.mmTm_year, tm.mmTm_mon,
        tm.mmTm_mday, tm.mmTm_hour,
        tm.mmTm_min, tm.mmTm_sec, tp->msec);
}
