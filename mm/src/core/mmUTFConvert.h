/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
 * Copyright 2001-2004 Unicode, Inc.
 *
 * Disclaimer
 *
 * This source code is provided as is by Unicode, Inc. No claims are
 * made as to fitness for any particular purpose. No warranties of any
 * kind are expressed or implied. The recipient agrees to determine
 * applicability of information provided. If this file has been
 * purchased on magnetic or optical media from Unicode, Inc., the
 * sole remedy for any claim will be exchange of defective media
 * within 90 days of receipt.
 *
 * Limitations on Rights to Redistribute This Code
 *
 * Unicode, Inc. hereby grants the right to freely use the information
 * supplied in this file in the creation of products supporting the
 * Unicode Standard, and to make copies of this file in any form
 * for internal or external distribution as long as this notice
 * remains attached.
 */

/* ---------------------------------------------------------------------

    Conversions between UTF32, UTF-16, and UTF-8. Source code file.
    Author: Mark E. Davis, 1994.
    Rev History: Rick McGowan, fixes & updates May 2001.
    Sept 2001: fixed const & error conditions per
        mods suggested by S. Parent & A. Lillich.
    June 2002: Tim Dodd added detection and handling of incomplete
        source sequences, enhanced error detection, added casts
        to eliminate compiler warnings.
    July 2003: slight mods to back out aggressive FFFE detection.
    Jan 2004: updated switches in from-UTF8 conversions.
    Oct 2004: updated to use UNI_MAX_LEGAL_UTF32 in UTF-32 conversions.

    See the header file "ConvertUTF.h" for complete documentation.

------------------------------------------------------------------------ */

#ifndef __mmUTFConvert_h__
#define __mmUTFConvert_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmUtf16String.h"
#include "core/mmUtf32String.h"

#include "core/mmPrefix.h"

typedef unsigned int    mmUTF32;      /* at least 32 bits */
typedef unsigned short  mmUTF16;      /* at least 16 bits */
typedef unsigned char   mmUTF8;       /* typically 8 bits */
typedef unsigned char   mmUTFBoolean; /* 0 or 1 */

/* Some fundamental constants */
#define MM_UTF_UNI_REPLACEMENT_CHAR (mmUTF32)0x0000FFFD
#define MM_UTF_UNI_MAX_BMP (mmUTF32)0x0000FFFF
#define MM_UTF_UNI_MAX_UTF16 (mmUTF32)0x0010FFFF
#define MM_UTF_UNI_MAX_UTF32 (mmUTF32)0x7FFFFFFF
#define MM_UTF_UNI_MAX_LEGAL_UTF32 (mmUTF32)0x0010FFFF

#define MM_UTF_UNI_MAX_UTF8_BYTES_PER_CODE_POINT 4

#define MM_UTF_UNI_UTF16_BYTE_ORDER_MARK_NATIVE  0xFEFF
#define MM_UTF_UNI_UTF16_BYTE_ORDER_MARK_SWAPPED 0xFFFE

enum mmUTFConversionResult
{
    MM_UTF_CONVERSION_OK    = 0,        /* conversion successful */
    MM_UTF_SOURCE_EXHAUSTED = 1,        /* partial character in source, but hit end */
    MM_UTF_TARGET_EXHAUSTED = 2,        /* insuff. room in target for conversion */
    MM_UTF_SOURCE_ILLEGAL   = 3,        /* source sequence is illegal/malformed */
};

enum mmUTFConversionFlags
{
    MM_UTF_STRICT_CONVERSION  = 0,      /* strict */
    MM_UTF_LENIENT_CONVERSION = 1,      /* lenient */
};

/*
 * li    sourceStart
 * ri    sourceEnd
 * lo    targetStart
 * ro    targetEnd
 * flags mmUTFConversionFlags
 */


/* utf8 to utf16*/
MM_EXPORT_DLL
int 
mmUTF_ConvertUTF8toUTF16(
    const mmUTF8**                                 li,
    const mmUTF8*                                  ri, 
    mmUTF16**                                      lo, 
    mmUTF16*                                       ro, 
    int                                            flags);

/* utf8 to utf32*/
MM_EXPORT_DLL
int 
mmUTF_ConvertUTF8toUTF32(
    const mmUTF8**                                 li, 
    const mmUTF8*                                  ri, 
    mmUTF32**                                      lo, 
    mmUTF32*                                       ro, 
    int                                            flags);

/* utf16 to utf8*/
MM_EXPORT_DLL
int 
mmUTF_ConvertUTF16toUTF8(
    const mmUTF16**                                li, 
    const mmUTF16*                                 ri, 
    mmUTF8**                                       lo, 
    mmUTF8*                                        ro, 
    int                                            flags);

/* utf32 to utf8*/
MM_EXPORT_DLL
int 
mmUTF_ConvertUTF32toUTF8(
    const mmUTF32**                                li, 
    const mmUTF32*                                 ri, 
    mmUTF8**                                       lo, 
    mmUTF8*                                        ro, 
    int                                            flags);

/* utf16 to utf32*/
MM_EXPORT_DLL
int 
mmUTF_ConvertUTF16toUTF32(
    const mmUTF16**                                li, 
    const mmUTF16*                                 ri, 
    mmUTF32**                                      lo, 
    mmUTF32*                                       ro, 
    int                                            flags);

/* utf32 to utf16*/
MM_EXPORT_DLL
int 
mmUTF_ConvertUTF32toUTF16(
    const mmUTF32**                                li, 
    const mmUTF32*                                 ri, 
    mmUTF16**                                      lo, 
    mmUTF16*                                       ro, 
    int                                            flags);

MM_EXPORT_DLL
mmUTFBoolean 
mmUTF_IsLegalUTF8Sequence(
    const mmUTF8*                                  source, 
    const mmUTF8*                                  sourceEnd);

MM_EXPORT_DLL
mmUTFBoolean 
mmUTF_IsLegalUTF8String(
    const mmUTF8**                                 source, 
    const mmUTF8*                                  sourceEnd);

MM_EXPORT_DLL
unsigned 
mmUTF_GetNumBytesForUTF8(
    mmUTF8                                         firstByte);

MM_EXPORT_DLL
int 
mmUTF_GetUTF8StringLength(
    const mmUTF8*                                  utf8);

/*
 * UTF string type convert function.
 */

MM_EXPORT_DLL
void
mmUTF_8to32(
    const struct mmString*                         i,
    struct mmUtf32String*                          o);

MM_EXPORT_DLL
void
mmUTF_32to8(
    const struct mmUtf32String*                    i,
    struct mmString*                               o);

MM_EXPORT_DLL
void
mmUTF_8to16(
    const struct mmString*                         i,
    struct mmUtf16String*                          o);

MM_EXPORT_DLL
void
mmUTF_16to8(
    const struct mmUtf16String*                    i,
    struct mmString*                               o);

MM_EXPORT_DLL
void
mmUTF_16to32(
    const struct mmUtf16String*                    i,
    struct mmUtf32String*                          o);

MM_EXPORT_DLL
void
mmUTF_32to16(
    const struct mmUtf32String*                    i,
    struct mmUtf16String*                          o);

#include "core/mmSuffix.h"

#endif//__mmUTFConvert_h__
