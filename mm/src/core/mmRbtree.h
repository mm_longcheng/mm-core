/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
  Red Black Trees
  (C) 1999  Andrea Arcangeli <andrea@suse.de>
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  linux/include/linux/rbtree.h

  To use rbtrees you'll have to implement your own insert and search cores.
  This will avoid us to use callbacks and to drop drammatically performances.
  I know it's not the cleaner way,  but in C (not in C++) to get
  performances and genericity...

  See Documentation/rbtree.txt for documentation and samples.
*/

#ifndef __mmRbtree_h__
#define __mmRbtree_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct MM_ALIGNED(MM_SIZEOF_UINTPTR_T) mmRbNode
{
    //unsigned long  __rb_parent_color;
    // the original linux version rbtree __rb_parent_color is unsigned long.
    //   gcc clang x86 sizeof(unsigned long) = 4 sizeof(uintptr_t) = 4
    //             x64 sizeof(unsigned long) = 8 sizeof(uintptr_t) = 8
    //   msvc      x86 sizeof(unsigned long) = 4 sizeof(uintptr_t) = 4
    //             x64 sizeof(unsigned long) = 4 sizeof(uintptr_t) = 8
    // we use the uintptr_t for __rb_parent_color cross-platform.
    uintptr_t __rb_parent_color;
    struct mmRbNode *rb_right;
    struct mmRbNode *rb_left;
};
/* The alignment might seem pointless, but allegedly CRIS needs it */

struct mmRbRoot
{
    struct mmRbNode *rb_node;
};

// uintptr_t is type for unsigned long, it is safe.
#define mmRb_Parent(r)   ((struct mmRbNode *)((uintptr_t)((r)->__rb_parent_color & ~3)))

#define MM_RB_ROOT  (struct mmRbRoot) { NULL, }
#define mmRb_Entry(ptr, type, member) mmContainerOf(ptr, type, member)

#define MM_RB_EMPTY_ROOT(root)  ((root)->rb_node == NULL)

/* 'empty' nodes are nodes that are known not to be inserted in an rbree */
#define MM_RB_EMPTY_NODE(node)  \
  ((node)->__rb_parent_color == (uintptr_t)(node))
#define MM_RB_CLEAR_NODE(node)  \
  ((node)->__rb_parent_color = (uintptr_t)(node))


MM_EXPORT_DLL extern void mmRb_InsertColor(struct mmRbNode *, struct mmRbRoot *);
MM_EXPORT_DLL extern void mmRb_Erase(struct mmRbNode *, struct mmRbRoot *);


/* Find logical next and previous nodes in a tree */
MM_EXPORT_DLL extern struct mmRbNode *mmRb_Next(const struct mmRbNode *);
MM_EXPORT_DLL extern struct mmRbNode *mmRb_Prev(const struct mmRbNode *);
MM_EXPORT_DLL extern struct mmRbNode *mmRb_First(const struct mmRbRoot *);
MM_EXPORT_DLL extern struct mmRbNode *mmRb_Last(const struct mmRbRoot *);

/* Postorder iteration - always visit the parent after its children */
MM_EXPORT_DLL extern struct mmRbNode *mmRb_FirstPostorder(const struct mmRbRoot *);
MM_EXPORT_DLL extern struct mmRbNode *mmRb_NextPostorder(const struct mmRbNode *);

/* Fast replacement of a single node without remove/rebalance/add/rebalance */
MM_EXPORT_DLL extern void mmRb_ReplaceNode(struct mmRbNode *victim, struct mmRbNode *newn,
    struct mmRbRoot *root);

static mmInline void mmRb_LinkNode(struct mmRbNode * node, struct mmRbNode * parent,
    struct mmRbNode ** rb_link)
{
    node->__rb_parent_color = (uintptr_t)parent;
    node->rb_left = node->rb_right = NULL;

    *rb_link = node;
}

#define mmRb_EntrySafe(ptr, type, member) \
  ({ typeof(ptr) ____ptr = (ptr); \
     ____ptr ? mmRb_Entry(____ptr, type, member) : NULL; \
  })

/**
 * mmRbtree_PostorderForEachEntrySafe - iterate over rb_root in post order of
 * given type safe against removal of rb_node entry
 *
 * @pos:  the 'type *' to use as a loop cursor.
 * @n:    another 'type *' to use as temporary storage
 * @root: 'mmRbRoot *' of the rbtree.
 * @field:  the name of the mmRbNode field within 'type'.
 */
#define mmRbtree_PostorderForEachEntrySafe(pos, n, root, field) \
  for (pos = mmRb_EntrySafe(mmRb_FirstPostorder(root), typeof(*pos), field); \
       pos && ({ n = mmRb_EntrySafe(mmRb_NextPostorder(&pos->field), \
      typeof(*pos), field); 1; }); \
       pos = n)

#include "core/mmSuffix.h"

#endif//__mmRbtree_h__
