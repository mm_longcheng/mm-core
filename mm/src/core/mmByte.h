/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmByte_h__
#define __mmByte_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// length is size_t.
struct mmByteBuffer
{
    // buffer length.
    size_t     length;
    // buffer offset.
    size_t     offset;
    // byte buffer.
    mmUInt8_t* buffer;
};

MM_EXPORT_DLL void mmByteBuffer_Init(struct mmByteBuffer* p);
MM_EXPORT_DLL void mmByteBuffer_Destroy(struct mmByteBuffer* p);

MM_EXPORT_DLL void mmByteBuffer_Reset(struct mmByteBuffer* p);

MM_EXPORT_DLL void mmByteBuffer_Malloc(struct mmByteBuffer* p, size_t length);
MM_EXPORT_DLL void mmByteBuffer_Free(struct mmByteBuffer* p);

// |----l---|----r---|
// |-uint32-|-uint32-|
// use little byte order.
// assemble
MM_EXPORT_DLL mmUInt64_t mmByte_UInt64A(mmUInt32_t l, mmUInt32_t r);
MM_EXPORT_DLL mmUInt32_t mmByte_UInt64L(mmUInt64_t a);
MM_EXPORT_DLL mmUInt32_t mmByte_UInt64R(mmUInt64_t a);
// |----l---|----r---|
// |-uint16-|-uint16-|
// use little byte order.
// assemble
MM_EXPORT_DLL mmUInt32_t mmByte_UInt32A(mmUInt16_t l, mmUInt16_t r);
MM_EXPORT_DLL mmUInt16_t mmByte_UInt32L(mmUInt32_t v);
MM_EXPORT_DLL mmUInt16_t mmByte_UInt32R(mmUInt32_t v);

#include "core/mmSuffix.h"

#endif//__mmByte_h__
