/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEventSet.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
void
mmEventArgs_Reset(
    struct mmEventArgs*                            p)
{
    p->handled = 0;
}

MM_EXPORT_DLL
void
mmEventSharedPtr_Init(
    struct mmEventSharedPtr*                       p)
{
    p->ref = NULL;
    p->ptr = NULL;
}

MM_EXPORT_DLL
void
mmEventSharedPtr_Destroy(
    struct mmEventSharedPtr*                       p)
{
    mmEventSharedPtr_SubRefCount(p);
    p->ref = NULL;
    p->ptr = NULL;
}

MM_EXPORT_DLL
struct mmEventHandler*
mmEventSharedPtr_MakeReference(
    struct mmEventSharedPtr*                       p)
{
    struct mmEventHandler* u = (struct mmEventHandler*)mmMalloc(sizeof(struct mmEventHandler));
    mmEventHandler_Init(u);
    p->ptr = &u->n;
    p->ref = (long*)mmMalloc(sizeof(long));
    (*p->ref) = 1;
    return u;
}

MM_EXPORT_DLL
void
mmEventSharedPtr_Assign(
    struct mmEventSharedPtr*                       p,
    const struct mmEventSharedPtr*                 q)
{
    if (p->ptr != q->ptr)
    {
        mmEventSharedPtr_SubRefCount(p);
        p->ptr = q->ptr;
        p->ref = q->ref;
        mmEventSharedPtr_AddRefCount(p);
    }
}

MM_EXPORT_DLL
void
mmEventSharedPtr_Reset(
    struct mmEventSharedPtr*                       p)
{
    mmEventSharedPtr_SubRefCount(p);
    p->ptr = NULL;
    p->ref = NULL;
}

MM_EXPORT_DLL
void
mmEventSharedPtr_AddRefCount(
    struct mmEventSharedPtr*                       p)
{
    if (NULL != p->ref)
    {
        ++(*p->ref);
    }
}

MM_EXPORT_DLL
void
mmEventSharedPtr_SubRefCount(
    struct mmEventSharedPtr*                       p)
{
    if (NULL != p->ref && --(*p->ref) == 0)
    {
        struct mmEventHandler* u = mmContainerOf(p->ptr, struct mmEventHandler, n);
        mmEventHandler_Destroy(u);
        mmFree(p->ref);
        mmFree(u);
    }
}

MM_EXPORT_DLL
void
mmEventListHead_Init(
    struct mmEventListHead*                        p)
{
    mmEventSharedPtr_Init(&p->next);
    mmEventSharedPtr_Init(&p->prev);
}

MM_EXPORT_DLL
void
mmEventListHead_Destroy(
    struct mmEventListHead*                        p)
{
    mmEventSharedPtr_Destroy(&p->prev);
    mmEventSharedPtr_Destroy(&p->next);
}

MM_EXPORT_DLL
void
mmEventListHead_Reset(
    struct mmEventListHead*                        p)
{
    mmEventSharedPtr_Reset(&p->next);
    mmEventSharedPtr_Reset(&p->prev);
}

MM_EXPORT_DLL
void
mmEventHandler_Init(
    struct mmEventHandler*                         p)
{
    p->func = NULL;
    p->obj = NULL;
    p->status = 0;
    mmEventListHead_Init(&p->n);
}

MM_EXPORT_DLL
void
mmEventHandler_Destroy(
    struct mmEventHandler*                         p)
{
    mmEventListHead_Destroy(&p->n);
    p->status = 0;
    p->obj = NULL;
    p->func = NULL;
}

MM_EXPORT_DLL
void
mmEventHandler_FireEvent(
    struct mmEventHandler*                         p,
    void*                                          args)
{
    typedef void(*FuncType)(void* obj, void* args);
    (*((FuncType)(p->func)))(p->obj, args);
}

MM_EXPORT_DLL
void
mmEvent_Init(
    struct mmEvent*                                p)
{
    mmString_Init(&p->name);
    mmEventListHead_Init(&p->l);
    p->size = 0;
}

MM_EXPORT_DLL
void
mmEvent_Destroy(
    struct mmEvent*                                p)
{
    mmEvent_Clear(p);
    p->size = 0;
    mmEventListHead_Destroy(&p->l);
    mmString_Destroy(&p->name);
}

MM_EXPORT_DLL
const char*
mmEvent_GetName(
    const struct mmEvent*                          p)
{
    return mmString_CStr(&p->name);
}

MM_EXPORT_DLL
void
mmEvent_SetName(
    struct mmEvent*                                p,
    const char*                                    name)
{
    mmString_Assigns(&p->name, name);
}

MM_EXPORT_DLL
void
mmEvent_FireEvent(
    struct mmEvent*                                p,
    void*                                          args)
{
    struct mmEventSharedPtr next;
    struct mmEventSharedPtr curr;
    struct mmEventHandler* it = NULL;
    mmEventSharedPtr_Init(&next);
    mmEventSharedPtr_Init(&curr);
    mmEventSharedPtr_Assign(&next, &p->l.next);
    while (NULL != next.ptr)
    {
        mmEventSharedPtr_Assign(&curr, &next);
        mmEventSharedPtr_Assign(&next, &next.ptr->next);
        it = mmContainerOf(curr.ptr, struct mmEventHandler, n);
        if (0 != it->status)
        {
            mmEventHandler_FireEvent(it, args);
        }
    }
    mmEventSharedPtr_Destroy(&curr);
    mmEventSharedPtr_Destroy(&next);
}

MM_EXPORT_DLL
struct mmEventHandler*
mmEvent_Subscribe(
    struct mmEvent*                                p,
    void*                                          func,
    void*                                          obj)
{
    struct mmEventHandler* it = NULL;
    struct mmEventSharedPtr node;
    mmEventSharedPtr_Init(&node);
    it = mmEventSharedPtr_MakeReference(&node);
    it->status = 1;
    it->func = func;
    it->obj = obj;
    if(p->l.next.ptr)
    {
        // node->prev = tail;
        // tail->next = node;
        // tail = node;
        mmEventSharedPtr_Assign(&node.ptr->prev, &p->l.prev);
        mmEventSharedPtr_Assign(&p->l.prev.ptr->next, &node);
        mmEventSharedPtr_Assign(&p->l.prev, &node);
    }
    else
    {
        // head = node;
        // tail = node;
        mmEventSharedPtr_Assign(&p->l.prev, &node);
        mmEventSharedPtr_Assign(&p->l.next, &node);
    }
    mmEventSharedPtr_Destroy(&node);
    p->size++;
    return it;
}

MM_EXPORT_DLL
void
mmEvent_UnSubscribeNode(
    struct mmEvent*                                p,
    struct mmEventSharedPtr*                       node)
{
    struct mmEventHandler* it = NULL;
    it = mmContainerOf(node->ptr, struct mmEventHandler, n);
    
    if(node->ptr->next.ptr)
    {
        // node->next->prev = node->prev;
        mmEventSharedPtr_Assign(&node->ptr->next.ptr->prev, &node->ptr->prev);
    }
    if(node->ptr->prev.ptr)
    {
        // node->prev->next = node->next;
        mmEventSharedPtr_Assign(&node->ptr->prev.ptr->next, &node->ptr->next);
    }

    // Mark it as deleted, this must be before the assignment of head and tail below,
    // because node can be a reference to head or tail, and after the assignment, node
    // can be null pointer.
    it->status = 0;

    if(p->l.next.ptr == node->ptr)
    {
        // head = node->next;
        mmEventSharedPtr_Assign(&p->l.next, &node->ptr->next);
    }
    if(p->l.prev.ptr == node->ptr)
    {
        // tail = node->prev;
        mmEventSharedPtr_Assign(&p->l.prev, &node->ptr->prev);
    }

    // don't modify node->previous or node->next
    // because node may be still used in a loop.
    
    p->size--;
}

MM_EXPORT_DLL
void
mmEvent_UnSubscribe(
    struct mmEvent*                                p,
    void*                                          func,
    void*                                          obj)
{
    struct mmEventSharedPtr next;
    struct mmEventSharedPtr curr;
    struct mmEventHandler* it = NULL;
    mmEventSharedPtr_Init(&next);
    mmEventSharedPtr_Init(&curr);
    mmEventSharedPtr_Assign(&next, &p->l.next);
    while (NULL != next.ptr)
    {
        mmEventSharedPtr_Assign(&curr, &next);
        mmEventSharedPtr_Assign(&next, &next.ptr->next);
        it = mmContainerOf(curr.ptr, struct mmEventHandler, n);
        if (it->func == func && it->obj == obj)
        {
            mmEvent_UnSubscribeNode(p, &curr);
        }
    }
    mmEventSharedPtr_Destroy(&curr);
    mmEventSharedPtr_Destroy(&next);
}

MM_EXPORT_DLL
void
mmEvent_Clear(
    struct mmEvent*                                p)
{
    struct mmEventSharedPtr node;
    struct mmEventSharedPtr next;
    mmEventSharedPtr_Init(&node);
    mmEventSharedPtr_Init(&next);
    mmEventSharedPtr_Assign(&node, &p->l.next);
    mmEventSharedPtr_Reset(&p->l.next);
    while (node.ptr)
    {
        mmEventSharedPtr_Assign(&next, &node.ptr->next);
        mmEventSharedPtr_Reset(&node.ptr->prev);
        mmEventSharedPtr_Reset(&node.ptr->next);
        mmEventSharedPtr_Assign(&node, &next);
        p->size--;
    }
    mmEventSharedPtr_Reset(&p->l.prev);
    mmEventSharedPtr_Reset(&node);
    mmEventSharedPtr_Destroy(&next);
    mmEventSharedPtr_Destroy(&node);
}

MM_EXPORT_DLL
void
mmEventSet_Init(
    struct mmEventSet*                             p)
{
    mmRbtreeStringVpt_Init(&p->rbtree);
    p->muted = MM_FALSE;
}

MM_EXPORT_DLL
void
mmEventSet_Destroy(
    struct mmEventSet*                             p)
{
    mmEventSet_ClearEvent(p);
    p->muted = MM_FALSE;
    mmRbtreeStringVpt_Destroy(&p->rbtree);
}

MM_EXPORT_DLL
void
mmEventSet_SetIsMuted(
    struct mmEventSet*                             p,
    int                                            muted)
{
    p->muted = muted;
}

MM_EXPORT_DLL
int
mmEventSet_GetIsMuted(
    const struct mmEventSet*                       p)
{
    return p->muted;
}

MM_EXPORT_DLL
struct mmEvent*
mmEventSet_AddEvent(
    struct mmEventSet*                             p,
    const char*                                    name)
{
    struct mmEvent* e = mmEventSet_GetEvent(p, name);
    if (NULL == e)
    {
        struct mmString hWeakKey;
        mmString_MakeWeaks(&hWeakKey, name);
        e = (struct mmEvent*)mmMalloc(sizeof(struct mmEvent));
        mmEvent_Init(e);
        mmEvent_SetName(e, name);
        mmRbtreeStringVpt_Set(&p->rbtree, &hWeakKey, e);
    }
    return e;
}

MM_EXPORT_DLL
struct mmEvent*
mmEventSet_GetEvent(
    const struct mmEventSet*                       p,
    const char*                                    name)
{
    struct mmString hWeakKey;
    mmString_MakeWeaks(&hWeakKey, name);
    return (struct mmEvent*)mmRbtreeStringVpt_Get(&p->rbtree, &hWeakKey);
}

MM_EXPORT_DLL
struct mmEvent*
mmEventSet_GetEventInstance(
    struct mmEventSet*                             p,
    const char*                                    name)
{
    struct mmEvent* e = mmEventSet_GetEvent(p, name);
    if (NULL == e)
    {
        e = mmEventSet_AddEvent(p, name);
    }
    return e;
}

MM_EXPORT_DLL
void
mmEventSet_RmvEvent(
    struct mmEventSet*                             p,
    const char*                                    name)
{
    struct mmString hWeakKey;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmEvent* e = NULL;
    mmString_MakeWeaks(&hWeakKey, name);
    it = mmRbtreeStringVpt_GetIterator(&p->rbtree, &hWeakKey);
    if (NULL != it)
    {
        e = (struct mmEvent*)(it->v);
        mmRbtreeStringVpt_Erase(&p->rbtree, it);
        mmEvent_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DLL
void
mmEventSet_ClearEvent(
    struct mmEventSet*                             p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmEvent* e = NULL;

    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        e = (struct mmEvent*)(it->v);
        n = mmRb_Next(n);
        mmRbtreeStringVpt_Erase(&p->rbtree, it);
        mmEvent_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DLL
struct mmEventHandler*
mmEventSet_SubscribeEvent(
    struct mmEventSet*                             p,
    const char*                                    name,
    void*                                          func,
    void*                                          obj)
{
    struct mmEvent* e = mmEventSet_AddEvent(p, name);
    if (NULL != e)
    {
        return mmEvent_Subscribe(e, func, obj);
    }
    else
    {
        return NULL;
    }
}

MM_EXPORT_DLL
void
mmEventSet_UnSubscribeEvent(
    struct mmEventSet*                             p,
    const char*                                    name,
    void*                                          func,
    void*                                          obj)
{
    struct mmEvent* e = mmEventSet_GetEvent(p, name);
    if (NULL != e)
    {
        mmEvent_UnSubscribe(e, func, obj);
    }
}

MM_EXPORT_DLL
void
mmEventSet_FireEvent(
    struct mmEventSet*                             p,
    const char*                                    name,
    void*                                          args)
{
    struct mmEvent* e = mmEventSet_GetEvent(p, name);
    if ((NULL != e) && (MM_FALSE == p->muted))
    {
        mmEvent_FireEvent(e, args);
    }
}
