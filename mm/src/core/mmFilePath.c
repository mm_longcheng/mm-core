/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFilePath.h"
#include "core/mmAlloc.h"
#include "core/mmFileSystem.h"
#include "core/mmByte.h"

MM_EXPORT_DLL
const char*
mmPathFileExtension(
    const char*                                    filename)
{
    const char* extension;
    // get the proper extension if we received a filename
    char* place = strrchr((char *)filename, '.');
    extension = (place != NULL) ? ++place : filename;
    return extension;
}

MM_EXPORT_DLL 
void 
mmPathSplitFileName(
    struct mmString*                               qualified_name,
    struct mmString*                               basename, 
    struct mmString*                               pathname)
{
    struct mmString path;
    size_t i = -1;
    mmString_Init(&path);
    mmString_Assign(&path, qualified_name);
    // Replace \ with / first
    mmString_ReplaceChar(&path, '\\', '/');
    // split based on final /
    i = mmString_FindLastOf(&path, '/');
    if (-1 == i)
    {
        mmString_Assign(basename, qualified_name);
        mmString_Clear(pathname);
    }
    else
    {
        char* d = (char*)mmString_Data(&path);
        size_t l = mmString_Size(&path);
        mmString_Assignsn(basename, &(d[i + 1]), l - i - 1);
        mmString_Assignsn(pathname, &(d[0]), i + 1);
    }
    //
    mmString_Destroy(&path);
}

// a.txt => a .txt
MM_EXPORT_DLL 
void 
mmPathSplitSuffixName(
    struct mmString*                               qualified_name,
    struct mmString*                               basename, 
    struct mmString*                               suffixname)
{
    struct mmString path;
    size_t i = (size_t)mmStringNpos;
    char c = 0;
    mmString_Init(&path);
    mmString_Assign(&path, qualified_name);
    // split based on final '.' but can not cross '\\' '/'.
    if (0 < mmString_Size(&path))
    {
        for (i = mmString_Size(&path) - 1; (size_t)mmStringNpos != i; --i)
        {
            c = mmString_At(&path, i);
            if ('/' == c || '\\' == c || '.' == c)
            {
                break;
            }
        }
    }

    if ((size_t)mmStringNpos != i)
    {
        i = ('.' == c) ? i : (size_t)mmStringNpos;
    }

    if ((size_t)mmStringNpos == i)
    {
        mmString_Assign(basename, qualified_name);
        mmString_Clear(suffixname);
    }
    else
    {
        char* d = (char*)mmString_Data(&path);
        size_t l = mmString_Size(&path);
        mmString_Assignsn(suffixname, &(d[i + 1]), l - i - 1);
        mmString_Assignsn(basename, &(d[0]), i);
    }
    //
    mmString_Destroy(&path);
}

MM_EXPORT_DLL 
void 
mmCleanPath(
    struct mmString*                               clean_path,
    const char*                                    complex_path)
{
    static const char* exclude_path_0 = "/..";
    static const char* exclude_path_1 = "/./";
    static const size_t exclude_size = 3;
    size_t i = 0;
    size_t last_index = 0;
    size_t substr_o = 0;
    size_t substr_l = 0;
    size_t vl = 0;
    const char* vs = NULL;
    struct mmString v;
    struct mmString l;
    struct mmString r;
    mmString_Init(&v);
    mmString_Init(&l);
    mmString_Init(&r);
    mmString_Assigns(&v, complex_path);
    // "./xxx" => xxx
    vl = mmString_Size(&v);
    vs = mmString_Data(&v);
    if (2 < vl && '.' == vs[0] && '/' == vs[1])
    {
        mmString_Substr(&v, &v, 2, vl - 2);
    }
    // "xxx/." => xxx
    vl = mmString_Size(&v);
    vs = mmString_Data(&v);
    if (2 < vl && '/' == vs[vl - 2] && '.' == vs[vl - 1])
    {
        mmString_Substr(&v, &v, 0, vl - 2);
    }
    // "/./"
    vl = mmString_Size(&v);
    if (exclude_size < vl)
    {
        mmString_Assign(&l, &v);
        do
        {
            mmString_Assign(&v, &l);
            vl = mmString_Size(&v);
            vs = mmString_Data(&v);
            for (i = 0; i <= vl - exclude_size; ++i)
            {
                if (0 == mmMemcmp(&vs[i], exclude_path_1, exclude_size) && ((0 != i) || (1 == i && '.' != vs[i - 1])))
                {
                    substr_o = i + 2;
                    substr_l = vl - substr_o;
                    mmString_Substr(&v, &l, 0, i);
                    mmString_Substr(&v, &r, substr_o, substr_l);
                    mmString_Append(&l, &r);
                    break;
                }
            }
        } while (i <= mmString_Size(&v) - exclude_size);
        mmString_Assign(&v, &l);
    }
    // "../.." "./.."
    if (exclude_size < mmString_Size(&v))
    {
        mmString_Assign(&l, &v);
        do
        {
            mmString_Assign(&v, &l);
            vl = mmString_Size(&v);
            vs = mmString_Data(&v);
            for (i = 0; i <= vl - exclude_size; ++i)
            {
                if (0 == mmMemcmp(&vs[i], exclude_path_0, exclude_size) && ((0 != i && '.' != vs[i - 1])))
                {
                    substr_o = i + exclude_size;
                    substr_l = vl - substr_o;
                    mmString_Substr(&v, &l, 0, last_index);
                    mmString_Substr(&v, &r, substr_o, substr_l);
                    mmString_Append(&l, &r);
                    break;
                }
                if ('/' == mmString_At(&v, i))
                {
                    last_index = i;
                }
            }
            last_index = 0;
        } while (i <= mmString_Size(&v) - exclude_size);
        mmString_Assign(&v, &l);
    }
    // v directory none suffix.
    mmDirectoryNoneSuffix(&v, mmString_CStr(&v));
    // copy v to clean_path.
    mmString_Assign(clean_path, &v);
    mmString_Destroy(&v);
    mmString_Destroy(&l);
    mmString_Destroy(&r);
}

MM_EXPORT_DLL 
void 
mmDirectoryHaveSuffix(
    struct mmString*                               directory_path,
    const char*                                    complex_path)
{
    mmString_Assigns(directory_path, complex_path);
    if (!mmString_Empty(directory_path))
    {
        size_t vl = mmString_Size(directory_path);
        char* vs = (char*)mmString_Data(directory_path);
        char c = vs[vl - 1];
        if ('/' == c || '\\' == c)
        {
            vs[vl - 1] = '/';
        }
        else
        {
            mmString_Appends(directory_path, "/");
        }
    }
}

MM_EXPORT_DLL 
void 
mmDirectoryNoneSuffix(
    struct mmString*                               directory_path,
    const char*                                    complex_path)
{
    mmString_Assigns(directory_path, complex_path);
    if (!mmString_Empty(directory_path))
    {
        size_t vl = mmString_Size(directory_path);
        char* vs = (char*)mmString_Data(directory_path);
        char c = vs[vl - 1];
        if ('/' == c || '\\' == c)
        {
            vs[vl - 1] = '\0';
            mmString_SetSizeValue(directory_path, vl - 1);
        }
    }
}

MM_EXPORT_DLL 
void 
mmDirectoryRemovePrefixPath(
    struct mmString*                               directory_path,
    const char*                                    pathname, 
    const char*                                    prefix_path)
{
    // prefix_path = "." need remove none.
    size_t substr_o = 0;
    size_t substr_l = 0;
    struct mmString prefix;
    struct mmString path;
    int mv = 0;
    mmString_Init(&prefix);
    mmString_Init(&path);
    mmString_Assigns(&path, pathname);
    mmString_Assigns(&prefix, prefix_path);
    mmString_Appends(&prefix, 0 == mmStrlen(prefix_path) ? "" : "/");
    mv = mmMemcmp(mmString_CStr(&prefix), mmString_CStr(&path), mmString_Size(&prefix));
    substr_o = ((0 == mv) ? mmString_Size(&prefix) : 0);
    substr_l = mmString_Size(&path) - substr_o;
    mmString_Substr(&path, &path, substr_o, substr_l);
    mmCleanPath(&path, mmString_CStr(&path));
    mmString_Assign(directory_path, &path);
    mmString_Destroy(&prefix);
    mmString_Destroy(&path);
}

MM_EXPORT_DLL 
void 
mmDirectoryParentPath(
    struct mmString*                               qualified_name,
    struct mmString*                               basename, 
    struct mmString*                               pathname)
{
    mmPathSplitFileName(qualified_name, basename, pathname);
    mmDirectoryNoneSuffix(pathname, mmString_CStr(pathname));
}

MM_EXPORT_DLL 
void 
mmConcatenatePath(
    struct mmString*                               path,
    const char*                                    base, 
    const char*                                    name)
{
    if (0 == mmStrlen(base) || 0 != mmIsAbsolutePath(name))
    {
        mmString_Assigns(path, name);
    }
    else
    {
        mmString_Assigns(path, base);
        mmDirectoryHaveSuffix(path, mmString_CStr(path));
        mmString_Appends(path, name);
    }
}

// a/b/c.txt .png => a/b/c.png
// a/b/c     .png => a/b/c.png
MM_EXPORT_DLL
void
mmPathReplaceSuffix(
    const char*                                    path,
    const char*                                    suffix,
    struct mmString*                               result)
{
    char* place = strrchr((char *)path, '.');
    size_t l = (place != NULL) ? ((size_t)(place - path)) : ((size_t)strlen(path));
    mmString_Assigns(result, path);
    mmString_Substr(result, result, 0, l);
    mmString_Appends(result, suffix);
}

// acquire file buffer.
MM_EXPORT_DLL 
void 
mmAbsolutePathAcquireFileByteBuffer(
    const char*                                    file_name,
    struct mmByteBuffer*                           byte_buffer)
{
    do
    {
        FILE* fp = NULL;
        long dsize = 0;

        mmByteBuffer_Reset(byte_buffer);

        if (NULL == file_name)
        {
            // file name is invalid.
            break;
        }

        fp = fopen(file_name, "rb");
        if (NULL == fp)
        {
            // fopen failure.
            break;
        }

        fseek(fp, 0, SEEK_END);
        dsize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        mmByteBuffer_Malloc(byte_buffer, dsize + 1);
        byte_buffer->length = (size_t)dsize;
        byte_buffer->buffer[byte_buffer->length] = 0;
        byte_buffer->length = (size_t)fread(byte_buffer->buffer, sizeof(char), dsize, fp);
        fclose(fp);
    } while (0);
}

// release file buffer.
MM_EXPORT_DLL 
void 
mmAbsolutePathReleaseFileByteBuffer(
    struct mmByteBuffer*                           byte_buffer)
{
    mmByteBuffer_Free(byte_buffer);
}

// mkdir if not exist.
MM_EXPORT_DLL 
int 
mmMkdirIfNotExist(
    const char*                                    directory)
{
    return (0 != mmAccess(directory, MM_ACCESS_X_OK)) ? mmMkdir(directory, 0777) : 0;
}
