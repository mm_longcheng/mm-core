/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTimeCache_h__
#define __mmTimeCache_h__
//
#include "core/mmCore.h"

#include "core/mmTime.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"
#include "core/mmThread.h"

#include <pthread.h>

#include "core/mmPrefix.h"

#define MM_TIME_SLOTS   64
// time cache update default is 100ms
#define MM_TIME_UPDATE 100

struct mmTimeInfo
{
    time_t     sec;
    mmUInt_t  msec;
    mmInt_t   gmtoff;
};
MM_EXPORT_DLL void mmTimeInfo_Init(struct mmTimeInfo* p);
MM_EXPORT_DLL void mmTimeInfo_Destroy(struct mmTimeInfo* p);

// time string template "1992/01/26 05:20:14-001"
#define MM_TIME_STRING_LENGTH 32

struct mmTimeCache
{
    struct mmTimeInfo cached_time[MM_TIME_SLOTS];
    char cached_timestamp[MM_TIME_SLOTS][MM_TIME_STRING_LENGTH];
    // the struct start init time info.
    struct mmTimeInfo start;
    mmAtomic_t locker;
    pthread_t time_thread;
    // current milliseconds.
    mmUInt64_t msec_current;
    // sleep milliseconds. default is MM_TIME_UPDATE. when interrupt will sleep until awake.
    mmMSec_t msec_sleep;
    mmUInt_t slot;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // weak ref,now cache time.
    struct mmTimeInfo* cache;
    // weak ref, log time string cache. log_time_string_template
    const char* string_log_time;
};

MM_EXPORT_DLL extern struct mmTimeCache* mmTimeCache_Instance(void);

MM_EXPORT_DLL void mmTimeCache_Init(struct mmTimeCache* p);
MM_EXPORT_DLL void mmTimeCache_Destroy(struct mmTimeCache* p);
//
MM_EXPORT_DLL void mmTimeCache_Update(struct mmTimeCache* p);
//
MM_EXPORT_DLL void mmTimeCache_Start(struct mmTimeCache* p);
MM_EXPORT_DLL void mmTimeCache_Interrupt(struct mmTimeCache* p);
MM_EXPORT_DLL void mmTimeCache_Shutdown(struct mmTimeCache* p);
MM_EXPORT_DLL void mmTimeCache_Join(struct mmTimeCache* p);

MM_EXPORT_DLL void mmTime_Tm(struct mmTimeInfo* ti, struct tm *tp);
MM_EXPORT_DLL void mmTime_TmConst(const struct mmTimeInfo* ti, struct tm *tp);
MM_EXPORT_DLL void mmTime_String(struct mmTimeInfo* tp, char ts[MM_TIME_STRING_LENGTH]);
MM_EXPORT_DLL void mmTime_StringConst(const struct mmTimeInfo* tp, char ts[MM_TIME_STRING_LENGTH]);

#include "core/mmSuffix.h"

#endif//__mmTimeCache_h__
