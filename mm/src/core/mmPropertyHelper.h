/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPropertyHelper_h__
#define __mmPropertyHelper_h__

#include "core/mmCore.h"

#include "core/mmPropertySet.h"

#include "core/mmPrefix.h"

// Property function signature type.
//
// Through function pointer conversion, Type generic use void* through the function call stack.
// 
// typedef void(*SetterFuncType)(void* obj, const void* value);
// typedef void(*GetterFuncType)(const void* obj, void* value);

/* base type value helper */

MM_EXPORT_DLL
void
mmPropertyHelper_SetValue(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v,
    void*                                          func);

MM_EXPORT_DLL
void
mmPropertyHelper_GetValue(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v,
    void*                                          func);

/* pointer */

MM_EXPORT_DLL
void
mmPropertyHelper_SetPointer(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetPointer(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

/* base type */

MM_EXPORT_DLL
void
mmPropertyHelper_SetUChar(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUChar(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSChar(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSChar(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUShort(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUShort(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSShort(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSShort(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetULong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetULong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSLong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSLong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetULLong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetULLong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSLLong(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSLLong(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt8(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt8(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt8(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt8(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt16(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt16(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt16(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt16(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt32(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt32(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt32(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt32(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUInt64(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUInt64(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSInt64(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSInt64(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSize(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSize(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetSIntptr(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetSIntptr(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUIntptr(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUIntptr(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetFloat(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetFloat(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetDouble(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetDouble(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

/* string */

MM_EXPORT_DLL
void
mmPropertyHelper_SetString(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetString(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetUtf16String(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetUtf16String(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

/* math */

MM_EXPORT_DLL
void
mmPropertyHelper_SetVec2(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetVec2(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetVec3(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetVec3(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetVec4(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetVec4(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

MM_EXPORT_DLL
void
mmPropertyHelper_SetQuat(
    const struct mmProperty*                       p,
    void*                                          obj,
    const struct mmString*                         v);

MM_EXPORT_DLL
void
mmPropertyHelper_GetQuat(
    const struct mmProperty*                       p,
    const void*                                    obj,
    struct mmString*                               v);

#include "core/mmSuffix.h"

#endif//__mmPropertyHelper_h__
