/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSignalTask.h"
#include "core/mmTimewait.h"

static void* __static_mmSignalTask_PollWaitThread(void* _arg);

static int __static_mmSignalTask_FactorDefault(void* obj)
{
    return -1;
}
static int __static_mmSignalTask_HandleDefault(void* obj)
{
    return 0;
}
MM_EXPORT_DLL void mmSignalTaskCallback_Init(struct mmSignalTaskCallback* p)
{
    p->Factor = &__static_mmSignalTask_FactorDefault;
    p->Handle = &__static_mmSignalTask_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmSignalTaskCallback_Destroy(struct mmSignalTaskCallback* p)
{
    p->Factor = &__static_mmSignalTask_FactorDefault;
    p->Handle = &__static_mmSignalTask_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmSignalTask_Init(struct mmSignalTask* p)
{
    mmSignalTaskCallback_Init(&p->callback);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
    p->msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
    p->msec_delay = 0;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DLL void mmSignalTask_Destroy(struct mmSignalTask* p)
{
    mmSignalTaskCallback_Init(&p->callback);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->locker);
    p->msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
    p->msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
    p->msec_delay = 0;
    p->state = MM_TS_CLOSED;
}
// lock to make sure the knot is thread safe.
MM_EXPORT_DLL void mmSignalTask_Lock(struct mmSignalTask* p)
{
    mmSpinlock_Lock(&p->locker);
}
// unlock knot.
MM_EXPORT_DLL void mmSignalTask_Unlock(struct mmSignalTask* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_DLL void mmSignalTask_SetCallback(struct mmSignalTask* p, const struct mmSignalTaskCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = (*callback);
}
MM_EXPORT_DLL void mmSignalTask_SetSuccessNearby(struct mmSignalTask* p, mmMSec_t nearby)
{
    p->msec_success_nearby = nearby;
}
MM_EXPORT_DLL void mmSignalTask_SetFailureNearby(struct mmSignalTask* p, mmMSec_t nearby)
{
    p->msec_failure_nearby = nearby;
}

// wait.
MM_EXPORT_DLL void mmSignalTask_SignalWait(struct mmSignalTask* p)
{
    int code = -1;
    struct timeval  ntime;
    struct timespec otime;
    mmMSec_t _nearby_time = 0;
    assert(NULL != p->callback.Factor && "p->callback.Factor is a null.");
    assert(NULL != p->callback.Handle && "p->callback.Handle is a null.");
    if (0 < p->msec_delay)
    {
        mmMSleep(p->msec_delay);
    }
    while (MM_TS_MOTION == p->state)
    {
        // factor
        code = (*(p->callback.Factor))(p);
        if (0 == code)
        {
            pthread_mutex_lock(&p->signal_mutex);
            pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
            pthread_mutex_unlock(&p->signal_mutex);
            // the condition is timeout invalid.we enter a new process.
            // we must make sure the code == 0.
            continue;
        }
        // fire event.
        code = (*(p->callback.Handle))(p);
        _nearby_time = 0 == code ? p->msec_success_nearby : p->msec_failure_nearby;
        // timewait nearby.
        mmTimewait_MSecNearby(&p->signal_cond, &p->signal_mutex, &ntime, &otime, _nearby_time);
    }
}
// signal.
MM_EXPORT_DLL void mmSignalTask_Signal(struct mmSignalTask* p)
{
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}

// start wait thread.
MM_EXPORT_DLL void mmSignalTask_Start(struct mmSignalTask* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->signal_thread, NULL, &__static_mmSignalTask_PollWaitThread, p);
}
// interrupt wait thread.
MM_EXPORT_DLL void mmSignalTask_Interrupt(struct mmSignalTask* p)
{
    p->state = MM_TS_CLOSED;
    mmSignalTask_Signal(p);
}
// shutdown wait thread.
MM_EXPORT_DLL void mmSignalTask_Shutdown(struct mmSignalTask* p)
{
    p->state = MM_TS_FINISH;
    mmSignalTask_Signal(p);
}
// join wait thread.
MM_EXPORT_DLL void mmSignalTask_Join(struct mmSignalTask* p)
{
    pthread_join(p->signal_thread, NULL);
}

static void* __static_mmSignalTask_PollWaitThread(void* _arg)
{
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(_arg);
    mmSignalTask_SignalWait(signal_task);
    return NULL;
}
