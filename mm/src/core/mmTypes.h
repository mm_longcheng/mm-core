/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTypes_h__
#define __mmTypes_h__

#include "core/mmConfigPlatform.h"

#include "core/mmPrefix.h"

// byte     char            short           int             long            long long       uintptr_t
// 16       8byte|1char     16byte|2char    16byte|2char    32byte|4char    64byte|8char    16byte|2char
// 32       8byte|1char     16byte|2char    32byte|4char    32byte|4char    64byte|8char    32byte|4char
// 64       8byte|1char     16byte|2char    32byte|4char    64byte|8char    64byte|8char    64byte|8char

typedef int                 mmBool_t;
typedef unsigned char       mmByte_t;
typedef unsigned short      mmWord_t;

//
typedef double              mmScalar_t;
typedef double              mmReal_t;
//
typedef float               mmFloat32_t;
typedef double              mmFloat64_t;

typedef signed char         mmSChar_t;
typedef unsigned char       mmUChar_t;

typedef signed short        mmSShort_t;
typedef unsigned short      mmUShort_t;

typedef signed int          mmSInt_t;
typedef unsigned int        mmUInt_t;

typedef signed long         mmSLong_t;
typedef unsigned long       mmULong_t;

typedef signed long long    mmSLLong_t;
typedef unsigned long long  mmULLong_t;
// 
typedef int8_t              mmSInt8_t;
typedef uint8_t             mmUInt8_t;

typedef int16_t             mmSInt16_t;
typedef uint16_t            mmUInt16_t;

typedef int32_t             mmSInt32_t;
typedef uint32_t            mmUInt32_t;

typedef int64_t             mmSInt64_t;
typedef uint64_t            mmUInt64_t;
//
typedef char                mmChar_t;
typedef short               mmShort_t;
typedef int                 mmInt_t;
typedef long                mmLong_t;
typedef long long           mmLLong_t;
typedef float               mmFloat_t;
typedef double              mmDouble_t;
//
typedef int                 mmStatus_t;

typedef size_t              mmSize_t;

typedef intptr_t            mmSIntptr_t;
typedef uintptr_t           mmUIntptr_t;

#include "core/mmSuffix.h"
//
#endif//__mmTypes_h__
