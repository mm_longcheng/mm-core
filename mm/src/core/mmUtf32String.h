/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUtf32String_h__
#define __mmUtf32String_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// clang generally mode simplify string.
// https://github.com/llvm-mirror/libcxx/blob/master/include/string
// https://joellaity.com/2020/01/31/string.html

typedef mmUInt32_t mmUtf32_t;

MM_EXPORT_DLL size_t mmUtf32Strlen(const mmUInt32_t* s);

struct mmUtf32StringLong
{
    // capacity
    size_t capacity;
    // size
    size_t size;
    // data pointer
    mmUtf32_t* data;
};

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// bigger endian
#define mmUtf32StringTinyMaskMacro 0x80
#define mmUtf32StringLongMaskMacro ~((size_t)(~((size_t)(0))) >> 1)
#else
// little endian
#define mmUtf32StringTinyMaskMacro 0x01
#define mmUtf32StringLongMaskMacro 0x1ul
#endif

MM_EXPORT_DLL extern const size_t mmUtf32StringTinyMask;
MM_EXPORT_DLL extern const size_t mmUtf32StringLongMask;
MM_EXPORT_DLL extern const size_t mmUtf32StringNpos;

enum
{
    mmUtf32StringLongCapacity = (sizeof(struct mmUtf32StringLong) - 1) / sizeof(mmUtf32_t),
    mmUtf32StringTinyCapacity = mmUtf32StringLongCapacity > 2 ? mmUtf32StringLongCapacity : 2,
};

struct mmUtf32StringTiny
{
    union
    {
        // size
        unsigned char size;
        // lx
        mmUtf32_t lx;
    };
    // data buffer
    mmUtf32_t data[mmUtf32StringTinyCapacity];
};

union mmUtf32StringUlxx { struct mmUtf32StringLong __lxx; struct mmUtf32StringTiny __txx; };

enum { mmUtf32StringNWords = sizeof(union mmUtf32StringUlxx) / sizeof(size_t) };

struct mmUtf32StringRaws
{
    // raw words buffer
    size_t words[mmUtf32StringNWords];
};

struct mmUtf32StringRep
{
    union
    {
        struct mmUtf32StringLong l;
        struct mmUtf32StringTiny s;
        struct mmUtf32StringRaws r;
    };
};

struct mmUtf32String
{
    union
    {
        // long
        struct mmUtf32StringLong l;
        // tiny
        struct mmUtf32StringTiny s;
        // raws
        struct mmUtf32StringRaws r;
    };
};

MM_EXPORT_DLL void mmUtf32String_Init(struct mmUtf32String* p);
MM_EXPORT_DLL void mmUtf32String_Destroy(struct mmUtf32String* p);

MM_EXPORT_DLL int mmUtf32String_Assign(struct mmUtf32String* p, const struct mmUtf32String* __str);
MM_EXPORT_DLL int mmUtf32String_Assigns(struct mmUtf32String* p, const mmUtf32_t* __s);
MM_EXPORT_DLL int mmUtf32String_Assignsn(struct mmUtf32String* p, const mmUtf32_t* __s, size_t __n);
MM_EXPORT_DLL int mmUtf32String_Assignnc(struct mmUtf32String* p, size_t __n, char __c);

MM_EXPORT_DLL int mmUtf32String_Insert(struct mmUtf32String* p, size_t __pos, const struct mmUtf32String* __str);
MM_EXPORT_DLL int mmUtf32String_Inserts(struct mmUtf32String* p, size_t __pos, const mmUtf32_t* __s);
MM_EXPORT_DLL int mmUtf32String_Insertsn(struct mmUtf32String* p, size_t __pos, const mmUtf32_t* __s, size_t __n);
MM_EXPORT_DLL int mmUtf32String_Insertnc(struct mmUtf32String* p, size_t __pos, size_t __n, mmUtf32_t __c);

MM_EXPORT_DLL int mmUtf32String_Erase(struct mmUtf32String* p, size_t __pos, size_t __n);

MM_EXPORT_DLL size_t mmUtf32String_Size(const struct mmUtf32String* p);
MM_EXPORT_DLL int mmUtf32String_Resize(struct mmUtf32String* p, size_t __n);
MM_EXPORT_DLL int mmUtf32String_ResizeChar(struct mmUtf32String* p, size_t __n, mmUtf32_t __c);
MM_EXPORT_DLL size_t mmUtf32String_Capacity(const struct mmUtf32String* p);
MM_EXPORT_DLL int mmUtf32String_Reserve(struct mmUtf32String* p, size_t __res_arg);
MM_EXPORT_DLL void mmUtf32String_Clear(struct mmUtf32String* p);
MM_EXPORT_DLL mmBool_t mmUtf32String_Empty(const struct mmUtf32String* p);
MM_EXPORT_DLL int mmUtf32String_ShrinkToFit(struct mmUtf32String* p);

MM_EXPORT_DLL mmUtf32_t mmUtf32String_At(const struct mmUtf32String* p, size_t __n);

MM_EXPORT_DLL int mmUtf32String_Append(struct mmUtf32String* p, const struct mmUtf32String* __str);
MM_EXPORT_DLL int mmUtf32String_Appends(struct mmUtf32String* p, const mmUtf32_t* __s);
MM_EXPORT_DLL int mmUtf32String_Appendsn(struct mmUtf32String* p, const mmUtf32_t* __s, size_t __n);
MM_EXPORT_DLL int mmUtf32String_Appendnc(struct mmUtf32String* p, size_t __n, mmUtf32_t __c);
MM_EXPORT_DLL int mmUtf32String_Appendc(struct mmUtf32String* p, mmUtf32_t __c);
MM_EXPORT_DLL int mmUtf32String_PushBack(struct mmUtf32String* p, mmUtf32_t __c);
MM_EXPORT_DLL void mmUtf32String_PopBack(struct mmUtf32String* p);

MM_EXPORT_DLL int mmUtf32String_Replace(struct mmUtf32String* p, size_t __pos, size_t __n1, const struct mmUtf32String* __str);
MM_EXPORT_DLL int mmUtf32String_Replaces(struct mmUtf32String* p, size_t __pos, size_t __n1, const mmUtf32_t* __s);
MM_EXPORT_DLL int mmUtf32String_Replacesn(struct mmUtf32String* p, size_t __pos, size_t __n1, const mmUtf32_t* __s, size_t __n2);
MM_EXPORT_DLL int mmUtf32String_Replacenc(struct mmUtf32String* p, size_t __pos, size_t __n1, size_t __n2, mmUtf32_t __c);
// s ==> t
MM_EXPORT_DLL size_t mmUtf32String_ReplaceChar(struct mmUtf32String* p, mmUtf32_t s, mmUtf32_t t);

MM_EXPORT_DLL const mmUtf32_t* mmUtf32String_CStr(const struct mmUtf32String* p);
MM_EXPORT_DLL const mmUtf32_t* mmUtf32String_Data(const struct mmUtf32String* p);

MM_EXPORT_DLL size_t mmUtf32String_FindLastOf(const struct mmUtf32String* p, mmUtf32_t c);
MM_EXPORT_DLL size_t mmUtf32String_FindFirstOf(const struct mmUtf32String* p, mmUtf32_t c);
MM_EXPORT_DLL size_t mmUtf32String_FindLastNotOf(const struct mmUtf32String* p, mmUtf32_t c);
MM_EXPORT_DLL size_t mmUtf32String_FindFirstNotOf(const struct mmUtf32String* p, mmUtf32_t c);

MM_EXPORT_DLL void mmUtf32String_Substr(const struct mmUtf32String* p, struct mmUtf32String* s, size_t o, size_t l);

// return 0 is equal.
MM_EXPORT_DLL int mmUtf32String_Compare(const struct mmUtf32String* p, const struct mmUtf32String* __str);
// return 0 is equal.
MM_EXPORT_DLL int mmUtf32String_CompareCStr(const struct mmUtf32String* p, const mmUtf32_t* __s);

/* Expansion module */
// const char* empty string
MM_EXPORT_DLL extern const mmUtf32_t* mmUtf32StringEmpty;
// make a weak string.
MM_EXPORT_DLL void mmUtf32String_MakeWeak(struct mmUtf32String* p, const struct mmUtf32String* __str);
MM_EXPORT_DLL void mmUtf32String_MakeWeaks(struct mmUtf32String* p, const mmUtf32_t* __s);
MM_EXPORT_DLL void mmUtf32String_MakeWeaksn(struct mmUtf32String* p, const mmUtf32_t* __s, size_t __n);
// set size value.
MM_EXPORT_DLL void mmUtf32String_SetSizeValue(struct mmUtf32String* p, size_t size);
// reset weak or strong string. Mix reference must reset before reassign.
MM_EXPORT_DLL void mmUtf32String_Reset(struct mmUtf32String* p);

// string Null.
#define mmUtf32String_Null      { { { 0, 0, NULL, }, }, }
// make a const weak string.
#define mmUtf32String_Make(str) { { { mmUtf32StringLongMaskMacro, (sizeof(str) / sizeof(mmUtf32_t) - 1), (mmUtf32_t*)str, }, }, }

#include "core/mmSuffix.h"

#endif//__mmUtf32String_h__
