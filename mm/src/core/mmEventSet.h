/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEventSet_h__
#define __mmEventSet_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmRbtreeString.h"

#include "core/mmPrefix.h"

// Simplified EventSet for c.
//
// 1. Only support the one static function signature.
//    void(*func)(void* obj, void* args) recommend void(*func)(void*, struct mmEventArgs*).
//
// 2. Supports nested event. During the process of handling an event, a listener
//    can safely dispatch event and Subscribe/UnSubscribe other listeners.
//
// 3. Subscribe always insert tail.
//
// 4. Not thread safety.
//
// 5. Only support string for event name.
//
// 6. Not support delete the running EventSet at event callback.

// Recommend super class.
struct mmEventArgs
{
    // handle event number.
    long handled;
};

MM_EXPORT_DLL 
void 
mmEventArgs_Reset(
    struct mmEventArgs*                            p);

struct mmEventListHead;

struct mmEventSharedPtr
{
    // reference count.
    long* ref;

    // pointer.
    struct mmEventListHead* ptr;
};

MM_EXPORT_DLL 
void 
mmEventSharedPtr_Init(
    struct mmEventSharedPtr*                       p);

MM_EXPORT_DLL 
void 
mmEventSharedPtr_Destroy(
    struct mmEventSharedPtr*                       p);

MM_EXPORT_DLL 
struct mmEventHandler* 
mmEventSharedPtr_MakeReference(
    struct mmEventSharedPtr*                       p);

MM_EXPORT_DLL 
void 
mmEventSharedPtr_Assign(
    struct mmEventSharedPtr*                       p, 
    const struct mmEventSharedPtr*                 q);

MM_EXPORT_DLL 
void 
mmEventSharedPtr_Reset(
    struct mmEventSharedPtr*                       p);

MM_EXPORT_DLL 
void 
mmEventSharedPtr_AddRefCount(
    struct mmEventSharedPtr*                       p);

MM_EXPORT_DLL 
void 
mmEventSharedPtr_SubRefCount(
    struct mmEventSharedPtr*                       p);

struct mmEventListHead
{
    struct mmEventSharedPtr next;
    struct mmEventSharedPtr prev;
};

MM_EXPORT_DLL 
void 
mmEventListHead_Init(
    struct mmEventListHead*                        p);

MM_EXPORT_DLL 
void 
mmEventListHead_Destroy(
    struct mmEventListHead*                        p);

MM_EXPORT_DLL 
void 
mmEventListHead_Reset(
    struct mmEventListHead*                        p);

struct mmEventHandler
{
    // function signature 
    // generics  void(*func)(void* obj, void* args)
    // recommend void(*func)(void*, struct mmEventArgs*)
    void* func;

    // object
    void* obj;

    // status for UnSubscribe.
    // 1 is Subscribe.
    // 0 is UnSubscribe.
    int status;

    // list head n.
    struct mmEventListHead n;
};

MM_EXPORT_DLL 
void 
mmEventHandler_Init(
    struct mmEventHandler*                         p);

MM_EXPORT_DLL 
void 
mmEventHandler_Destroy(
    struct mmEventHandler*                         p);

MM_EXPORT_DLL 
void
mmEventHandler_FireEvent(
    struct mmEventHandler*                         p, 
    void*                                          args);

struct mmEvent
{
    // event name.
    struct mmString name;

    // head -> l.next
    // tail -> l.prev
    struct mmEventListHead l;

    // listener size.
    size_t size;
};

MM_EXPORT_DLL 
void 
mmEvent_Init(
    struct mmEvent*                                p);

MM_EXPORT_DLL 
void 
mmEvent_Destroy(
    struct mmEvent*                                p);

MM_EXPORT_DLL 
const char* 
mmEvent_GetName(
    const struct mmEvent*                          p);

MM_EXPORT_DLL 
void 
mmEvent_SetName(
    struct mmEvent*                                p, 
    const char*                                    name);

MM_EXPORT_DLL 
void 
mmEvent_FireEvent(
    struct mmEvent*                                p, 
    void*                                          args);

MM_EXPORT_DLL 
struct mmEventHandler* 
mmEvent_Subscribe(
    struct mmEvent*                                p, 
    void*                                          func, 
    void*                                          obj);

MM_EXPORT_DLL 
void 
mmEvent_UnSubscribeNode(
    struct mmEvent*                                p, 
    struct mmEventSharedPtr*                       node);

MM_EXPORT_DLL 
void 
mmEvent_UnSubscribe(
    struct mmEvent*                                p, 
    void*                                          func,
    void*                                          obj);

MM_EXPORT_DLL 
void 
mmEvent_Clear(
    struct mmEvent*                                p);

struct mmEventSet
{
    // event tree.
    struct mmRbtreeStringVpt rbtree;

    // muted status.
    int muted;
};

MM_EXPORT_DLL 
void 
mmEventSet_Init(
    struct mmEventSet*                             p);

MM_EXPORT_DLL 
void 
mmEventSet_Destroy(
    struct mmEventSet*                             p);

MM_EXPORT_DLL 
void 
mmEventSet_SetIsMuted(
    struct mmEventSet*                             p, 
    int                                            muted);

MM_EXPORT_DLL 
int 
mmEventSet_GetIsMuted(
    const struct mmEventSet*                       p);

MM_EXPORT_DLL 
struct mmEvent* 
mmEventSet_AddEvent(
    struct mmEventSet*                             p, 
    const char*                                    name);

MM_EXPORT_DLL 
struct mmEvent* 
mmEventSet_GetEvent(
    const struct mmEventSet*                       p, 
    const char*                                    name);

MM_EXPORT_DLL 
struct mmEvent* 
mmEventSet_GetEventInstance(
    struct mmEventSet*                             p, 
    const char*                                    name);

MM_EXPORT_DLL 
void 
mmEventSet_RmvEvent(
    struct mmEventSet*                             p, 
    const char*                                    name);

MM_EXPORT_DLL 
void 
mmEventSet_ClearEvent(
    struct mmEventSet*                             p);

MM_EXPORT_DLL 
struct mmEventHandler* 
mmEventSet_SubscribeEvent(
    struct mmEventSet*                             p, 
    const char*                                    name, 
    void*                                          func,
    void*                                          obj);

MM_EXPORT_DLL 
void 
mmEventSet_UnSubscribeEvent(
    struct mmEventSet*                             p, 
    const char*                                    name, 
    void*                                          func,
    void*                                          obj);

MM_EXPORT_DLL
void
mmEventSet_FireEvent(
    struct mmEventSet*                             p,
    const char*                                    name,
    void*                                          args);

#include "core/mmSuffix.h"

#endif//__mmEventSet_h__
