/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmValueTransform.h"
#include "core/mmErrno.h"

#include <stdlib.h>

/*
 * http://en.verysource.com/code/7944441_1/ecvt.c.html
 *
 * ecvt converts to decimal
 * the number of digits is specified by ndigit
 * decpt is set to the position of the decimal point
 * sign is set to 0 for positive, 1 for negative
 */
#define NDIG 80

static char* cvt(double arg, int ndigits, int* decpt, int* sign, int eflag, char buf[NDIG])
{
    register int r2;
    register char *p, *p1;
    double fi, fj;
    int f = 0;

    if (ndigits < 0)
    {
        ndigits = 0;
    }

    if (ndigits >= NDIG - 1)
    {
        ndigits = NDIG - 2;
    }

    r2 = 0;
    *sign = 0;
    p = &buf[0];

    if (arg < 0)
    {
        *sign = 1;
        arg = -arg;
    }

    arg = modf(arg, &fi);
    p1 = &buf[NDIG];

    /*
     * Do integer part
     */
    if (fi != 0)
    {
        p1 = &buf[NDIG];
        while (fi != 0)
        {
            fj = modf(fi / 10, &fi);
            if ((0 != f) || (0 != fj))
            {
                *--p1 = (int)((fj + .03) * 10) + '0';
                f = 1;
            }
            r2++;
        }
        while (p1 < &buf[NDIG])
        {
            *p++ = *p1++;
        }
    }
    else if (arg > 0)
    {
        while ((fj = arg * 10) < 1)
        {
            arg = fj;
            r2--;
        }
    }

    p1 = &buf[ndigits];

    if (eflag == 0)
    {
        p1 += r2;
    }

    *decpt = r2;

    if (p1 < &buf[0])
    {
        buf[0] = 0;
        return (buf);
    }

    while (p <= p1 && p < &buf[NDIG])
    {
        arg *= 10;
        arg = modf(arg, &fj);
        *p++ = (int)fj + '0';
    }

    if (p1 >= &buf[NDIG])
    {
        buf[NDIG - 1] = 0;
        return (buf);
    }

    p = p1;
    *p1 += 5;

    while (*p1 > '9')
    {
        *p1 = '0';
        if (p1 > buf)
        {
            ++*--p1;
        }
        else
        {
            *p1 = '1';
            (*decpt)++;
            if (eflag == 0)
            {
                if (p > buf)
                {
                    *p = '0';
                }
                p++;
            }
        }
    }

    *p = 0;
    return (buf);
}

MM_EXPORT_DLL char* mmEcvt(double arg, int ndigits, int* decpt, int* sign)
{
    // not thread safe.
    static char buffer[NDIG];
    return cvt(arg, ndigits, decpt, sign, 1, buffer);
}

MM_EXPORT_DLL char* mmFcvt(double arg, int ndigits, int* decpt, int* sign)
{
    // not thread safe.
    static char buffer[NDIG];
    return cvt(arg, ndigits, decpt, sign, 0, buffer);
}

/*
 * http://en.verysource.com/code/33526404_1/gcvt.c.html
 *
 * gcvt  - Floating output conversion to
 * minimal length string
 * thread safe.
 */
MM_EXPORT_DLL char* mmGcvt(double number, int ndigit, char* buf)
{
    char buffer[NDIG];
    int sign, decpt;
    register char *p1, *p2;
    register int i;

    /* 
     * p1 = mmEcvt(number, ndigit, &decpt, &sign);
     * we use thread safe cvt api.
     */
    p1 = cvt(number, ndigit, &decpt, &sign, 1, buffer);
    p2 = buf;

    if (sign)
    {
        *p2++ = '-';
    }

    for (i = ndigit - 1; i > 0 && p1[i] == '0'; i--)
    {
        ndigit--;
    }

    /* 
     * ndigit = 6     
     *
     *  123456 ->       123456 
     * 1234567 -> 1.23457e+006 
     */
    if ((decpt >= 0 && decpt - ndigit > 0) || (decpt < 0 && decpt < -3))
    {
        /* use E-style */
        decpt--;
        *p2++ = *p1++;
        *p2++ = '.';
        for (i = 1; i < ndigit; i++)
        {
            *p2++ = *p1++;
        }

        *p2++ = 'e';

        if (decpt < 0)
        {
            decpt = -decpt;
            *p2++ = '-';
        }
        else
        {
            *p2++ = '+';
        }

        /* format e+123 */
        *p2++ = '0' + (decpt / 100);
        *p2++ = '0' + (decpt / 10 ) % 10;
        *p2++ = '0' + (decpt % 10 );
    }
    else
    {
        if (decpt <= 0)
        {
            if (*p1 != '0')
            {
                /* format 0.123456 */
                *p2++ = '0';
                *p2++ = '.';
            }

            while (decpt < 0)
            {
                decpt++;
                *p2++ = '0';
            }
        }

        for (i = 1; i <= ndigit; i++)
        {
            *p2++ = *p1++;
            if (i == decpt)
            {
                *p2++ = '.';
            }
        }

        if (ndigit < decpt)
        {
            while (ndigit++ < decpt)
            {
                *p2++ = '0';
            }
            *p2++ = '.';
        }
    }

    if (p2[-1] == '.')
    {
        p2--;
    }

    *p2 = 0;
    return (buf);
}

MM_EXPORT_DLL int mmUI32ToDecA(uint32_t value, char* string)
{
    int len = 0;
    char tmp[64 + 1];
    register char* tp = tmp;
    register int i;
    register unsigned int v = value;
    register unsigned int x;
    char* sp;
    assert(NULL != string && "string is a null.");
    while (v || tp == tmp)
    {
        x = v / 10;
        i = v - x * 10;
        v = x;
        *tp++ = (char)(i + '0');
    }
    sp = string;
    len = (int)(tp - tmp);
    while (tp > tmp)
    {
        *sp++ = *--tp;
    }
    *sp = 0;
    return len;
}

MM_EXPORT_DLL int mmUI64ToDecA(uint64_t value, char* string)
{
    int len = 0;
    char tmp[64 + 1];
    register char* tp = tmp;
    register int64_t i;
    register uint64_t v = value;
    register uint64_t x;
    char* sp;
    assert(NULL != string && "string is a null.");
    while (v || tp == tmp)
    {
        x = v / 10;
        i = v - x * 10;
        v = x;
        *tp++ = (char)(i + '0');
    }
    sp = string;
    len = (int)(tp - tmp);
    while (tp > tmp)
    {
        *sp++ = *--tp;
    }
    *sp = 0;
    return len;
}

#define IEEE754NDIG 64

static void IEEE754CvtDigits(int ndigits, int* decpt, int eflag, int len, char buf[IEEE754NDIG])
{
    register char *p0, *p1;

    p1 = &buf[ndigits];
    p0 = &buf[len];

    if (eflag == 0)
    {
        p1 += len;
    }

    while (p0 <= p1 && p0 < &buf[IEEE754NDIG])
    {
        *p0++ = '0';
    }

    p0 = p1;
    *p1 += 5;

    while (*p1 > '9')
    {
        *p1 = '0';
        if (p1 > buf)
        {
            ++*--p1;
        }
        else
        {
            *p1 = '1';
            (*decpt)++;
            if (eflag == 0)
            {
                if (p0 > buf)
                {
                    *p0 = '0';
                }
                p0++;
            }
        }
    }

    *p0 = 0;
}

static void IEEE754CvtFormat(int ndigit, int sign, int decpt, char* buf, char buffer[IEEE754NDIG])
{
    register char *p1, *p2;
    register int i;

    p1 = buffer;
    p2 = buf;

    if (sign)
    {
        *p2++ = '-';
    }

    for (i = ndigit - 1; i > 0 && p1[i] == '0'; i--)
    {
        ndigit--;
    }

    /*
     * ndigit = 6
     *
     *  123456 ->       123456
     * 1234567 -> 1.23457e+006
     */
    if ((decpt >= 0 && decpt - ndigit > 0) || (decpt < 0 && decpt < -3))
    {
        /* use E-style */
        decpt--;
        *p2++ = *p1++;
        *p2++ = '.';
        for (i = 1; i < ndigit; i++)
        {
            *p2++ = *p1++;
        }

        *p2++ = 'e';

        if (decpt < 0)
        {
            decpt = -decpt;
            *p2++ = '-';
        }
        else
        {
            *p2++ = '+';
        }

        /* format e+123 */
        *p2++ = '0' + (decpt / 100);
        *p2++ = '0' + (decpt / 10) % 10;
        *p2++ = '0' + (decpt % 10);
    }
    else
    {
        if (decpt <= 0)
        {
            if (*p1 != '0')
            {
                /* format 0.123456 */
                *p2++ = '0';
                *p2++ = '.';
            }

            while (decpt < 0)
            {
                decpt++;
                *p2++ = '0';
            }
        }

        for (i = 1; i <= ndigit; i++)
        {
            *p2++ = *p1++;
            if (i == decpt)
            {
                *p2++ = '.';
            }
        }

        if (ndigit < decpt)
        {
            while (ndigit++ < decpt)
            {
                *p2++ = '0';
            }
            *p2++ = '.';
        }
    }

    if (p2[-1] == '.')
    {
        p2--;
    }

    *p2 = 0;
}

static void IEEE754CvtF(float arg, int ndigits, int* decpt, int* sign, int eflag, char buf[IEEE754NDIG])
{
    // IEEE754 float
    //
    // sign      1 
    // exponent  8 
    // mantissa  24
    // bias      (2 ^ 7 / 2) - 1 = 127
    uint32_t mifvi;
    uint32_t mifvs, mifve, mifvm;
    register uint32_t v;
    register int bit, dec;
    int len, exp;

    memcpy(&mifvi, &arg, sizeof(float));
    mifvs = 0x00000001 & (mifvi >> 31);
    mifve = 0x000000FF & (mifvi >> 23);
    mifvm = 0x007FFFFF & (mifvi      );

    mifve -= 127;
    mifvm |= 0x00800000;

    *sign = (int)mifvs;

    bit = (int)(23 - mifve);
    dec = 0;
    v = mifvm;

    if (0 > bit)
    {
        while (bit)
        {
            // v = v * 2 / 10;
            if (0 == (v & 0xF0000000))
            {
                v <<= 1;
                bit++;
            }
            else
            {
                v /= 10;
                dec++;
            }
        }
        len = mmUI32ToDecA(v, buf);
        exp = len + dec;
    }
    else
    {
        while (bit)
        {
            // v = v * 10 / 2;
            if (0 == (v & 0xF0000000))
            {
                v *= 10;
                dec++;
            }
            else
            {
                v >>= 1;
                bit--;
            }
        }
        len = mmUI32ToDecA(v, buf);
        exp = len - dec;
    }

    *decpt = exp;

    IEEE754CvtDigits(ndigits, decpt, eflag, len, buf);
}

static void IEEE754CvtD(double arg, int ndigits, int* decpt, int* sign, int eflag, char buf[IEEE754NDIG])
{
    // IEEE754 double
    //
    // sign      1 
    // exponent  11 
    // mantissa  52
    // bias      (2 ^ 11 / 2) - 1 = 1023
    uint64_t mifvi;
    uint64_t mifvs, mifve, mifvm;
    register uint64_t v;
    register int bit, dec;
    int len, exp;

    memcpy(&mifvi, &arg, sizeof(double));
    mifvs = 0x0000000000000001 & (mifvi >> 63);
    mifve = 0x00000000000007FF & (mifvi >> 52);
    mifvm = 0x000FFFFFFFFFFFFF & (mifvi      );

    mifve -= 1023;
    mifvm |= 0x0010000000000000;

    *sign = (int)mifvs;

    bit = (int)(52 - mifve);
    dec = 0;
    v = mifvm;

    if (0 > bit)
    {
        while (bit)
        {
            // v = v * 2 / 10;
            if (0 == (v & 0xF000000000000000))
            {
                v <<= 1;
                bit++;
            }
            else
            {
                v /= 10;
                dec++;
            }
        }
        len = mmUI64ToDecA(v, buf);
        exp = len + dec;
    }
    else
    {
        while (bit)
        {
            // v = v * 10 / 2;
            if (0 == (v & 0xF000000000000000))
            {
                v *= 10;
                dec++;
            }
            else
            {
                v >>= 1;
                bit--;
            }
        }
        len = mmUI64ToDecA(v, buf);
        exp = len - dec;
    }

    *decpt = exp;

    IEEE754CvtDigits(ndigits, decpt, eflag, len, buf);
}

MM_EXPORT_DLL char* mmIEEE754GcvtF(float number, int ndigit, char* buf)
{
    char buffer[IEEE754NDIG];
    int sign, decpt;
    IEEE754CvtF(number, ndigit, &decpt, &sign, 1, buffer);
    IEEE754CvtFormat(ndigit, sign, decpt, buf, buffer);
    return buf;
}

MM_EXPORT_DLL char* mmIEEE754GcvtD(double number, int ndigit, char* buf)
{
    char buffer[IEEE754NDIG];
    int sign, decpt;
    IEEE754CvtD(number, ndigit, &decpt, &sign, 1, buffer);
    IEEE754CvtFormat(ndigit, sign, decpt, buf, buffer);
    return buf;
}

MM_EXPORT_DLL char* mmIToA(int value, char* string, int radix)
{
    char tmp[32 + 1];
    char* tp = tmp;
    int i;
    unsigned v;
    unsigned x;
    int sign;
    char* sp;

    if (radix > 36 || radix <= 1)
    {
        assert(0 && "radix is invalid.");
        mmSetErrno(EDOM);
        return NULL;
    }

    sign = (radix == 10 && value < 0);
    if (sign)
        v = -value;
    else
        v = (unsigned)value;

    while (v || tp == tmp)
    {
        x = v / radix;
        i = v - x * radix;
        v = x;
        if (i < 10)
            *tp++ = (char)(i + '0');
        else
            *tp++ = (char)(i + 'a' - 10);
    }

    if (string == 0)
        string = (char*)malloc((tp - tmp) + sign + 1);
    sp = string;

    if (sign)
        *sp++ = '-';
    while (tp > tmp)
        *sp++ = *--tp;
    *sp = 0;
    return string;
}

MM_EXPORT_DLL char* mmUIToA(unsigned int value, char* string, int radix)
{
    char tmp[32 + 1];
    char* tp = tmp;
    int i;
    unsigned int v = value;
    unsigned int x;
    char* sp;

    if (radix > 36 || radix <= 1)
    {
        assert(0 && "radix is invalid.");
        mmSetErrno(EDOM);
        return 0;
    }

    while (v || tp == tmp)
    {
        x = v / radix;
        i = v - x * radix;
        v = x;
        if (i < 10)
            *tp++ = (char)(i + '0');
        else
            *tp++ = (char)(i + 'a' - 10);
    }

    if (string == 0)
        string = (char*)malloc((tp - tmp) + 1);
    sp = string;

    while (tp > tmp)
        *sp++ = *--tp;
    *sp = 0;
    return string;
}

MM_EXPORT_DLL char* mmLToA(long value, char* string, int radix)
{
    char tmp[MM_WORDSIZE + 1];
    char* tp = tmp;
    long i;
    unsigned long v;
    unsigned long x;
    int sign;
    char* sp;

    if (radix > 36 || radix <= 1)
    {
        assert(0 && "radix is invalid.");
        mmSetErrno(EDOM);
        return NULL;
    }

    sign = (radix == 10 && value < 0);
    if (sign)
        v = -value;
    else
        v = (unsigned long)value;

    while (v || tp == tmp)
    {
        x = v / radix;
        i = v - x * radix;
        v = x;
        if (i < 10)
            *tp++ = (char)(i + '0');
        else
            *tp++ = (char)(i + 'a' - 10);
    }

    if (string == 0)
        string = (char *)malloc((tp - tmp) + sign + 1);
    sp = string;

    if (sign)
        *sp++ = '-';
    while (tp > tmp)
        *sp++ = *--tp;
    *sp = 0;
    return string;
}

MM_EXPORT_DLL char* mmULToA(unsigned long value, char* string, int radix)
{
    char tmp[MM_WORDSIZE + 1];
    char* tp = tmp;
    long i;
    unsigned long v = value;
    unsigned long x;
    char* sp;

    if (radix > 36 || radix <= 1)
    {
        assert(0 && "radix is invalid.");
        mmSetErrno(EDOM);
        return 0;
    }

    while (v || tp == tmp)
    {
        x = v / radix;
        i = v - x * radix;
        v = x;
        if (i < 10)
            *tp++ = (char)(i + '0');
        else
            *tp++ = (char)(i + 'a' - 10);
    }

    if (string == 0)
        string = (char *)malloc((tp - tmp) + 1);
    sp = string;

    while (tp > tmp)
        *sp++ = *--tp;
    *sp = 0;
    return string;
}

MM_EXPORT_DLL char* mmLLToA(long long value, char* string, int radix)
{
    char tmp[64 + 1];
    char* tp = tmp;
    long long i;
    unsigned long long v;
    unsigned long long x;
    int sign;
    char* sp;

    if (radix > 36 || radix <= 1)
    {
        assert(0 && "radix is invalid.");
        mmSetErrno(EDOM);
        return NULL;
    }

    sign = (radix == 10 && value < 0);
    if (sign)
        v = -value;
    else
        v = (unsigned long long)value;

    while (v || tp == tmp)
    {
        x = v / radix;
        i = v - x * radix;
        v = x;
        if (i < 10)
            *tp++ = (char)(i + '0');
        else
            *tp++ = (char)(i + 'a' - 10);
    }

    if (string == 0)
        string = (char*)malloc((tp - tmp) + sign + 1);
    sp = string;

    if (sign)
        *sp++ = '-';
    while (tp > tmp)
        *sp++ = *--tp;
    *sp = 0;
    return string;
}

MM_EXPORT_DLL char* mmULLToA(unsigned long long value, char* string, int radix)
{
    char tmp[64 + 1];
    char* tp = tmp;
    long long i;
    unsigned long long v = value;
    unsigned long long x;
    char* sp;

    if (radix > 36 || radix <= 1)
    {
        assert(0 && "radix is invalid.");
        mmSetErrno(EDOM);
        return 0;
    }

    while (v || tp == tmp)
    {
        x = v / radix;
        i = v - x * radix;
        v = x;
        if (i < 10)
            *tp++ = (char)(i + '0');
        else
            *tp++ = (char)(i + 'a' - 10);
    }
    if (string == 0)
        string = (char*)malloc((tp - tmp) + 1);
    sp = string;

    while (tp > tmp)
        *sp++ = *--tp;
    *sp = 0;
    return string;
}

MM_EXPORT_DLL int mmAToI(const char* string)
{
    return (int)atoi(string);
}
MM_EXPORT_DLL unsigned int mmAToUI(const char* string)
{
    return (unsigned int)strtoul(string, NULL, 10);
}
MM_EXPORT_DLL long mmAToL(const char* string)
{
    return (long)atol(string);
}
MM_EXPORT_DLL unsigned long mmAToUL(const char* string)
{
    return (unsigned long)strtoul(string, NULL, 10);
}
MM_EXPORT_DLL long long mmAToLL(const char* string)
{
    return (long long)atoll(string);
}
MM_EXPORT_DLL unsigned long long mmAToULL(const char* string)
{
    return (unsigned long long)strtoull(string, NULL, 10);
}

MM_EXPORT_DLL const char* mmHexDigit = "0123456789ABCDEF";

MM_EXPORT_DLL int mmHexCStrToDigit(unsigned int* n, const char* s)
{
    if (!s || !n || strlen(s) == 0)
    {
        return -1;
    }
    else
    {
        // *n = 0;
        *n ^= *n;

        while (s && *s != '\0')
        {
            if (!((*s >= '0') && (*s <= '9')) &&
                !((*s >= 'a') && (*s <= 'f')) &&
                !((*s >= 'A') && (*s <= 'F')))
            {
                return -2;
            }
            else
            {
                (*n) = (((*n) << 4) | (((*s) <= '9') ? ((*s) - '0') : (((*s) & 7) + 9)));
                s++;
            }
        }

        return 0;
    }
}

MM_EXPORT_DLL int mmHexBuffToDigit(unsigned int* n, const char* s, int l)
{
    if (!s || !n || strlen(s) == 0)
    {
        return -1;
    }
    else
    {
        const char* o = s;

        // *n = 0;
        *n ^= *n;

        while (s && *s != '\0' && (s - o) < l)
        {
            if (!((*s >= '0') && (*s <= '9')) &&
                !((*s >= 'a') && (*s <= 'f')) &&
                !((*s >= 'A') && (*s <= 'F')))
            {
                return -2;
            }
            else
            {
                (*n) = (((*n) << 4) | (((*s) <= '9') ? ((*s) - '0') : (((*s) & 7) + 9)));
                s++;
            }
        }

        return 0;
    }
}

MM_EXPORT_DLL void mmValue_SCharToA(const mmSChar_t* v, char s[8])
{
    mmIToA((int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UCharToA(const mmUChar_t* v, char s[8])
{
    mmUIToA((unsigned int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SShortToA(const mmSShort_t* v, char s[8])
{
    mmIToA((int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UShortToA(const mmUShort_t* v, char s[8])
{
    mmUIToA((unsigned int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SIntToA(const mmSInt_t* v, char s[16])
{
    mmIToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UIntToA(const mmUInt_t* v, char s[16])
{
    mmUIToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SLongToA(const mmSLong_t* v, char s[32])
{
    mmLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_ULongToA(const mmULong_t* v, char s[32])
{
    mmULToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SLLongToA(const mmSLLong_t* v, char s[32])
{
    mmLLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_ULLongToA(const mmULLong_t* v, char s[32])
{
    mmULLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SInt8ToA(const mmSInt8_t* v, char s[8])
{
    mmIToA((int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UInt8ToA(const mmUInt8_t* v, char s[8])
{
    mmUIToA((unsigned int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SInt16ToA(const mmSInt16_t* v, char s[8])
{
    mmIToA((int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UInt16ToA(const mmUInt16_t* v, char s[8])
{
    mmUIToA((unsigned int)(*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SInt32ToA(const mmSInt32_t* v, char s[16])
{
    mmIToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UInt32ToA(const mmUInt32_t* v, char s[16])
{
    mmUIToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SInt64ToA(const mmSInt64_t* v, char s[32])
{
    mmLLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UInt64ToA(const mmUInt64_t* v, char s[32])
{
    mmULLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SizeToA(const mmSize_t* v, char s[32])
{
    mmULLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_SIntptrToA(const mmSIntptr_t* v, char s[32])
{
    mmLLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_UIntptrToA(const mmUIntptr_t* v, char s[32])
{
    mmULLToA((*v), s, 10);
}
MM_EXPORT_DLL void mmValue_FloatToA(const float* v, char s[32])
{
    // FLT_DIG = 6
    // = 7 + 6 = 13 -1.23456e+001
    mmIEEE754GcvtF((*v), FLT_DIG, s);
}
MM_EXPORT_DLL void mmValue_DoubleToA(const double* v, char s[64])
{
    // DBL_DIG = 15
    // = 7 + 15 = 22 -1.23456789012345e+001
    mmIEEE754GcvtD((*v), DBL_DIG, s);
}
MM_EXPORT_DLL void mmValue_BoolToA(const int* v, char s[8])
{
    strcpy(s, (*v) ? "true" : "false");
}

MM_EXPORT_DLL void mmValue_AToSChar(const char* s, mmSChar_t* v)
{
    (*v) = (mmSChar_t)atoi(s);
}
MM_EXPORT_DLL void mmValue_AToUChar(const char* s, mmUChar_t* v)
{
    (*v) = (mmUChar_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSShort(const char* s, mmSShort_t* v)
{
    (*v) = (mmSShort_t)atoi(s);
}
MM_EXPORT_DLL void mmValue_AToUShort(const char* s, mmUShort_t* v)
{
    (*v) = (mmUShort_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSInt(const char* s, mmSInt_t* v)
{
    (*v) = (mmSInt_t)atoi(s);
}
MM_EXPORT_DLL void mmValue_AToUInt(const char* s, mmUInt_t* v)
{
    (*v) = (mmUInt_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSLong(const char* s, mmSLong_t* v)
{
    (*v) = (mmSLong_t)atol(s);
}
MM_EXPORT_DLL void mmValue_AToULong(const char* s, mmULong_t* v)
{
    (*v) = (mmULong_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSLLong(const char* s, mmSLLong_t* v)
{
    (*v) = (mmSLLong_t)atoll(s);
}
MM_EXPORT_DLL void mmValue_AToULLong(const char* s, mmULLong_t* v)
{
    (*v) = (mmULLong_t)strtoull(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSInt8(const char* s, mmSInt8_t* v)
{
    (*v) = (mmSInt8_t)atoi(s);
}
MM_EXPORT_DLL void mmValue_AToUInt8(const char* s, mmUInt8_t* v)
{
    (*v) = (mmUInt8_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSInt16(const char* s, mmSInt16_t* v)
{
    (*v) = (mmSInt16_t)atoi(s);
}
MM_EXPORT_DLL void mmValue_AToUInt16(const char* s, mmUInt16_t* v)
{
    (*v) = (mmUInt16_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSInt32(const char* s, mmSInt32_t* v)
{
    (*v) = (mmSInt32_t)atoi(s);
}
MM_EXPORT_DLL void mmValue_AToUInt32(const char* s, mmUInt32_t* v)
{
    (*v) = (mmUInt32_t)strtoul(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSInt64(const char* s, mmSInt64_t* v)
{
    (*v) = (mmSInt64_t)atoll(s);
}
MM_EXPORT_DLL void mmValue_AToUInt64(const char* s, mmUInt64_t* v)
{
    (*v) = (mmUInt64_t)strtoull(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSize(const char* s, mmSize_t* v)
{
    (*v) = (mmSize_t)strtoull(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToSIntptr(const char* s, mmSIntptr_t* v)
{
    (*v) = (mmSIntptr_t)atoll(s);
}
MM_EXPORT_DLL void mmValue_AToUIntptr(const char* s, mmUIntptr_t* v)
{
    (*v) = (mmUIntptr_t)strtoull(s, NULL, 10);
}
MM_EXPORT_DLL void mmValue_AToFloat(const char* s, float* v)
{
    (*v) = (float)atof(s);
}
MM_EXPORT_DLL void mmValue_AToDouble(const char* s, double* v)
{
    (*v) = (double)atof(s);
}
MM_EXPORT_DLL void mmValue_AToBool(const char* s, mmBool_t* v)
{
    (*v) = (0 == strcmp(s, "true")) ? MM_TRUE : MM_FALSE;
}

MM_EXPORT_DLL void mmValue_StringToSChar(const char* s, mmSChar_t* v)
{
    sscanf(s, "%" SCNd8, v);
}
MM_EXPORT_DLL void mmValue_StringToUChar(const char* s, mmUChar_t* v)
{
    sscanf(s, "%" SCNu8, v);
}
MM_EXPORT_DLL void mmValue_StringToSShort(const char* s, mmSShort_t* v)
{
    sscanf(s, "%" SCNd16, v);
}
MM_EXPORT_DLL void mmValue_StringToUShort(const char* s, mmUShort_t* v)
{
    sscanf(s, "%" SCNu16, v);
}
MM_EXPORT_DLL void mmValue_StringToSInt(const char* s, mmSInt_t* v)
{
    sscanf(s, "%" SCNd32, v);
}
MM_EXPORT_DLL void mmValue_StringToUInt(const char* s, mmUInt_t* v)
{
    sscanf(s, "%" SCNu32, v);
}
MM_EXPORT_DLL void mmValue_StringToSLong(const char* s, mmSLong_t* v)
{
    sscanf(s, "%ld", v);
}
MM_EXPORT_DLL void mmValue_StringToULong(const char* s, mmULong_t* v)
{
    sscanf(s, "%lu", v);
}
MM_EXPORT_DLL void mmValue_StringToSLLong(const char* s, mmSLLong_t* v)
{
    sscanf(s, "%lld", v);
}
MM_EXPORT_DLL void mmValue_StringToULLong(const char* s, mmULLong_t* v)
{
    sscanf(s, "%llu", v);
}
MM_EXPORT_DLL void mmValue_StringToSInt8(const char* s, mmSInt8_t* v)
{
    sscanf(s, "%" SCNd8, v);
}
MM_EXPORT_DLL void mmValue_StringToUInt8(const char* s, mmUInt8_t* v)
{
    sscanf(s, "%" SCNu8, v);
}
MM_EXPORT_DLL void mmValue_StringToSInt16(const char* s, mmSInt16_t* v)
{
    sscanf(s, "%" SCNd16, v);
}
MM_EXPORT_DLL void mmValue_StringToUInt16(const char* s, mmUInt16_t* v)
{
    sscanf(s, "%" SCNu16, v);
}
MM_EXPORT_DLL void mmValue_StringToSInt32(const char* s, mmSInt32_t* v)
{
    sscanf(s, "%" SCNd32, v);
}
MM_EXPORT_DLL void mmValue_StringToUInt32(const char* s, mmUInt32_t* v)
{
    sscanf(s, "%" SCNu32, v);
}
MM_EXPORT_DLL void mmValue_StringToSInt64(const char* s, mmSInt64_t* v)
{
    sscanf(s, "%" SCNd64, v);
}
MM_EXPORT_DLL void mmValue_StringToUInt64(const char* s, mmUInt64_t* v)
{
    sscanf(s, "%" SCNu64, v);
}
MM_EXPORT_DLL void mmValue_StringToSize(const char* s, mmSize_t* v)
{
    sscanf(s, "%" SCNuPTR, v);
}
MM_EXPORT_DLL void mmValue_StringToSIntptr(const char* s, mmSIntptr_t* v)
{
    sscanf(s, "%" SCNdPTR, v);
}
MM_EXPORT_DLL void mmValue_StringToUIntptr(const char* s, mmUIntptr_t* v)
{
    sscanf(s, "%" SCNuPTR, v);
}
MM_EXPORT_DLL void mmValue_StringToFloat(const char* s, float* v)
{
    sscanf(s, "%f", v);
}
MM_EXPORT_DLL void mmValue_StringToDouble(const char* s, double* v)
{
    sscanf(s, "%lf", v);
}
MM_EXPORT_DLL void mmValue_StringToBool(const char* s, int* v)
{
    (*v) = (0 == strcmp(s, "true")) ? MM_TRUE : MM_FALSE;
}

MM_EXPORT_DLL void mmValue_SCharToString(const mmSChar_t* v, char s[8])
{
    sprintf(s, "%" PRId8, (*v));
}
MM_EXPORT_DLL void mmValue_UCharToString(const mmUChar_t* v, char s[8])
{
    sprintf(s, "%" PRIu8, (*v));
}
MM_EXPORT_DLL void mmValue_SShortToString(const mmSShort_t* v, char s[8])
{
    sprintf(s, "%" PRId16, (*v));
}
MM_EXPORT_DLL void mmValue_UShortToString(const mmUShort_t* v, char s[8])
{
    sprintf(s, "%" PRIu16, (*v));
}
MM_EXPORT_DLL void mmValue_SIntToString(const mmSInt_t* v, char s[16])
{
    sprintf(s, "%" PRId32, (*v));
}
MM_EXPORT_DLL void mmValue_UIntToString(const mmUInt_t* v, char s[16])
{
    sprintf(s, "%" PRIu32, (*v));
}
MM_EXPORT_DLL void mmValue_SLongToString(const mmSLong_t* v, char s[32])
{
    sprintf(s, "%ld", (*v));
}
MM_EXPORT_DLL void mmValue_ULongToString(const mmULong_t* v, char s[32])
{
    sprintf(s, "%lu", (*v));
}
MM_EXPORT_DLL void mmValue_SLLongToString(const mmSLLong_t* v, char s[32])
{
    sprintf(s, "%lld", (*v));
}
MM_EXPORT_DLL void mmValue_ULLongToString(const mmULLong_t* v, char s[32])
{
    sprintf(s, "%llu", (*v));
}
MM_EXPORT_DLL void mmValue_SInt8ToString(const mmSInt8_t* v, char s[8])
{
    sprintf(s, "%" PRId8, (*v));
}
MM_EXPORT_DLL void mmValue_UInt8ToString(const mmUInt8_t* v, char s[8])
{
    sprintf(s, "%" PRIu8, (*v));
}
MM_EXPORT_DLL void mmValue_SInt16ToString(const mmSInt16_t* v, char s[8])
{
    sprintf(s, "%" PRId16, (*v));
}
MM_EXPORT_DLL void mmValue_UInt16ToString(const mmUInt16_t* v, char s[8])
{
    sprintf(s, "%" PRIu16, (*v));
}
MM_EXPORT_DLL void mmValue_SInt32ToString(const mmSInt32_t* v, char s[16])
{
    sprintf(s, "%" PRId32, (*v));
}
MM_EXPORT_DLL void mmValue_UInt32ToString(const mmUInt32_t* v, char s[16])
{
    sprintf(s, "%" PRIu32, (*v));
}
MM_EXPORT_DLL void mmValue_SInt64ToString(const mmSInt64_t* v, char s[32])
{
    sprintf(s, "%" PRId64, (*v));
}
MM_EXPORT_DLL void mmValue_UInt64ToString(const mmUInt64_t* v, char s[32])
{
    sprintf(s, "%" PRIu64, (*v));
}
MM_EXPORT_DLL void mmValue_SizeToString(const mmSize_t* v, char s[32])
{
    sprintf(s, "%" PRIuPTR, (*v));
}
MM_EXPORT_DLL void mmValue_SIntptrToString(const mmSIntptr_t* v, char s[32])
{
    sprintf(s, "%" PRIdPTR, (*v));
}
MM_EXPORT_DLL void mmValue_UIntptrToString(const mmUIntptr_t* v, char s[32])
{
    sprintf(s, "%" PRIuPTR, (*v));
}
MM_EXPORT_DLL void mmValue_FloatToString(const float* v, char s[32])
{
    sprintf(s, "%f", (*v));
}
MM_EXPORT_DLL void mmValue_DoubleToString(const double* v, char s[64])
{
    sprintf(s, "%lf", (*v));
}
MM_EXPORT_DLL void mmValue_BoolToString(const int* v, char s[8])
{
    strcpy(s, (*v) ? "true" : "false");
}
