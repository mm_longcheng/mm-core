/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmStreambuf_h__
#define __mmStreambuf_h__

#include "core/mmCore.h"
#include "core/mmPlatform.h"
#include "core/mmTypes.h"
#include <stdio.h>

#include "core/mmPrefix.h"

// streambuf page size. default is 1024.
#define MM_STREAMBUF_PAGE_SIZE 1024

/*
 *  output sequence (put)
 *     +++++-------
 *     |    |      |
 *     0  pptr   size
 *
 *  input  sequence (get)
 *     +++++-------
 *     |    |      |
 *     0  gptr   size
 */
struct mmStreambuf
{
    // output sequence (put)
    size_t pptr;

    // input  sequence (get)
    size_t gptr;

    // max size for this streambuf. default is MM_STREAMBUF_PAGE_SIZE = 1024
    size_t size;

    // buffer for real data.
    mmUInt8_t* buff;
};

MM_EXPORT_DLL void mmStreambuf_Init(struct mmStreambuf* p);
MM_EXPORT_DLL void mmStreambuf_Destroy(struct mmStreambuf* p);

// copy q to p.
MM_EXPORT_DLL void mmStreambuf_CopyFrom(struct mmStreambuf* p, const struct mmStreambuf* q);
// add max streambuf size. do not call this function if you known what it do.
MM_EXPORT_DLL void mmStreambuf_AddSize(struct mmStreambuf* p, size_t size);
// rmv max streambuf size. do not call this function if you known what it do.
MM_EXPORT_DLL void mmStreambuf_RmvSize(struct mmStreambuf* p, size_t size);

MM_EXPORT_DLL void mmStreambuf_Removeget(struct mmStreambuf* p);
MM_EXPORT_DLL void mmStreambuf_RemovegetSize(struct mmStreambuf* p, size_t rsize);
MM_EXPORT_DLL void mmStreambuf_Clearget(struct mmStreambuf* p);

MM_EXPORT_DLL void mmStreambuf_Removeput(struct mmStreambuf* p);
MM_EXPORT_DLL void mmStreambuf_RemoveputSize(struct mmStreambuf* p, size_t wsize);
MM_EXPORT_DLL void mmStreambuf_Clearput(struct mmStreambuf* p);

MM_EXPORT_DLL size_t mmStreambuf_Getsize(const struct mmStreambuf* p);
MM_EXPORT_DLL size_t mmStreambuf_Putsize(const struct mmStreambuf* p);

// size for input and output interval.
MM_EXPORT_DLL size_t mmStreambuf_Size(const struct mmStreambuf* p);

// set get pointer to new pointer.
MM_EXPORT_DLL void mmStreambuf_SetGPtr(struct mmStreambuf* p, size_t new_ptr);
// set put pointer to new pointer.
MM_EXPORT_DLL void mmStreambuf_SetPPtr(struct mmStreambuf* p, size_t new_ptr);
// set get and put pointer to zero.
MM_EXPORT_DLL void mmStreambuf_Reset(struct mmStreambuf* p);

// get get pointer(offset).
MM_EXPORT_DLL size_t mmStreambuf_GPtr(const struct mmStreambuf* p);
// get put pointer(offset).
MM_EXPORT_DLL size_t mmStreambuf_PPtr(const struct mmStreambuf* p);

// get data s + offset length is n.and gbump n.
MM_EXPORT_DLL size_t mmStreambuf_Sgetn(struct mmStreambuf* p, mmUInt8_t* s, size_t o, size_t n);
// put data s + offset length is n.and pbump n.
MM_EXPORT_DLL size_t mmStreambuf_Sputn(struct mmStreambuf* p, mmUInt8_t* s, size_t o, size_t n);
// gptr += n
MM_EXPORT_DLL void mmStreambuf_Gbump(struct mmStreambuf* p, size_t n);
// pptr += n
MM_EXPORT_DLL void mmStreambuf_Pbump(struct mmStreambuf* p, size_t n);

MM_EXPORT_DLL void mmStreambuf_OverflowSgetn(struct mmStreambuf* p);
MM_EXPORT_DLL void mmStreambuf_OverflowSputn(struct mmStreambuf* p);

// try decrease.
// cursz = p->pptr - p->gptr;
// fresz = p->size - cursz;
// fresz > n && (p->size)(1/4) > page && (p->size)(3/4) < empty_size
MM_EXPORT_DLL void mmStreambuf_TryDecrease(struct mmStreambuf* p, size_t n);
// try increase.
MM_EXPORT_DLL void mmStreambuf_TryIncrease(struct mmStreambuf* p, size_t n);
// aligned streambuf memory if need sputn n size buffer before.
MM_EXPORT_DLL void mmStreambuf_AlignedMemory(struct mmStreambuf* p, size_t n);

#include "core/mmSuffix.h"

#endif//__mmStreambuf_h__
