/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmStringUtils.h"
#include "core/mmString.h"
#include "core/mmValueTransform.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL
size_t
mmString_FindRSidePrint(
    const struct mmString*                         p)
{
    size_t index = mmStringNpos;
    size_t pl = mmString_Size(p);
    if (0 < pl)
    {
        const char* ps = mmString_Data(p);
        for (index = pl - 1; mmStringNpos != index; --index)
        {
            if (0 != isprint(ps[index]) && ' ' != ps[index])
            {
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmString_FindLSidePrint(
    const struct mmString*                         p)
{
    size_t index = mmStringNpos;
    size_t pl = mmString_Size(p);
    if (0 < pl)
    {
        const char* ps = mmString_Data(p);
        for (index = 0; index < pl; ++index)
        {
            if (0 != isprint(ps[index]) && ' ' != ps[index])
            {
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
void
mmString_RemoveBothSidesSpace(
    struct mmString*                               s)
{
    size_t sz = 0;
    size_t l = mmString_Size(s);
    size_t p0 = mmString_FindLSidePrint(s);
    size_t p1 = mmString_FindRSidePrint(s);
    p0 = (mmStringNpos == p0) ? 0 : p0;
    p1 = (mmStringNpos == p1) ? l : p1;
    sz = p1 - p0 + 1;
    mmString_Substr(s, s, p0, sz);
}

MM_EXPORT_DLL
size_t
mmString_ReplaceCStr(
    struct mmString*                               p,
    const char*                                    s,
    const char*                                    t)
{
    size_t n = 0;
    size_t psz = mmString_Size(p);
    size_t ssz = strlen(s);
    size_t tsz = strlen(t);
    if (0 < psz)
    {
        size_t index = 0;
        const char* ptr = mmString_Data(p);
        while (index < psz)
        {
            if (0 == mmMemcmp(&ptr[index], s, ssz))
            {
                mmString_Replacesn(p, index, ssz, t, tsz);
                n++;
                index += tsz;
                ptr = mmString_Data(p);
                psz = mmString_Size(p);
            }
            else
            {
                index++;
            }
        }
    }
    return n;
}

// ("abc", 5) ==> "abc  "
MM_EXPORT_DLL
void
mmString_LAlignAppends(
    struct mmString*                               p,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length)
{
    size_t usz = ssz > length ? ssz : length;
    size_t rsz = usz - ssz;
    mmString_Appends(p, s);
    mmString_Appendnc(p, rsz, ' ');
}

// ("abc", 5) ==> " abc "
// ("abc", 6) ==> " abc  "
MM_EXPORT_DLL
void
mmString_MAlignAppends(
    struct mmString*                               p,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length)
{
    size_t usz = ssz > length ? ssz : length;
    size_t zsz = usz - ssz;
    size_t lsz = zsz / 2;
    size_t rsz = zsz - lsz;
    mmString_Appendnc(p, lsz, ' ');
    mmString_Appends(p, s);
    mmString_Appendnc(p, rsz, ' ');
}

// ("abc", 5) ==> "  abc"
MM_EXPORT_DLL
void
mmString_RAlignAppends(
    struct mmString*                               p,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length)
{
    size_t usz = ssz > length ? ssz : length;
    size_t lsz = usz - ssz;
    mmString_Appendnc(p, lsz, ' ');
    mmString_Appends(p, s);
}

/*
 * This function transform string to type[].
 *
 * @param v "(1,2,3,4)".
 * @param a type a[4].
 * @param z sizeof(type).
 * @param s "(,,,)".
 * @param d 4.
 * @param f void(*mmAToTypeFunc)(const char*, type*).
 */
MM_EXPORT_DLL
void
mmValueStringToTypeArray(
    const struct mmString*                         v,
    void*                                          a,
    size_t                                         z,
    const char*                                    s,
    int                                            d,
    void*                                          f)
{
    const char* w;
    size_t l;
    size_t n;
    int i;
    
    w = (char*)mmString_CStr(v);
    l = mmString_Size(v);

    for (i = 0;i < d;++i)
    {
        n = mmCStringFindNextOf((const char*)w, s[i], 0, l);
        if (mmStringNpos == n)
        {
            break;
        }
        else
        {
            w = (const char*)(w + n + 1);
            (*((mmAToTypeFunc)f))(w, (mmUInt8_t*)a + z * i);
            l -= (n + 1);
        }
    }
}

/*
 * This function transform type[] to string.
 *
 * @param a type a[4].
 * @param z sizeof(type).
 * @param v "(1,2,3,4)".
 * @param s "(,,,)".
 * @param d 4.
 * @param f void(*mmTypeToAFunc)(const void*, char*).
 */
MM_EXPORT_DLL
void
mmValueTypeArrayToString(
    const void*                                    a,
    size_t                                         z,
    struct mmString*                               v,
    const char*                                    s,
    int                                            d,
    void*                                          f)
{
    char w[64];
    int i;
    int m = (int)strlen(s);
    
    assert(0 < d && "Dimension must > 0");
    assert(0 < m && "m is invalid.");
    
    mmString_Clear(v);
    mmString_Reserve(v, m + MM_ARRAY_SIZE(w) * d);
    
    mmString_Appendc(v, s[0]);
    
    for (i = 0;i < d;++i)
    {
        mmMemset(w, 0, 64);
        (*((mmTypeToAFunc)f))((mmUInt8_t*)a + z * i, w);
        mmString_Appends(v, w);
        mmString_Appendc(v, s[i + 1]);
    }
    
    mmString_ShrinkToFit(v);
}

/*
 * This function transform string format to type[].
 *
 * @param v "((1,2),(3,4))".
 * @param a type a[4].
 * @param z sizeof(type).
 * @param s "((,),(,))".
 * @param x int x[] = { 1, 2, 4, 5 }.
 * @param d 4.
 * @param f void(*mmAToTypeFunc)(const char*, type*).
 */
MM_EXPORT_DLL
void
mmValueStringFormatToTypeArray(
    const struct mmString*                         v,
    void*                                          a,
    size_t                                         z,
    const char*                                    s,
    const int*                                     x,
    int                                            d,
    void*                                          f)
{
    const char* y = NULL;
    const char* w = NULL;
    size_t l = 0;
    size_t n = mmStringNpos;
    size_t o = 0;
    int i;
    int k = 0;
    int m = (int)strlen(s);
    size_t* u = NULL;
    
    w = (char*)mmString_CStr(v);
    l = mmString_Size(v);
    y = w;
    
    assert(0 < m && "m is invalid.");
    
    u = (size_t*)malloc(m * sizeof(size_t));
    mmMemset(u, 0, m * sizeof(size_t));
    
    for (i = 0;i < m;++i)
    {
        n = mmCStringFindNextOf((const char*)w, s[i], 0, l);
        if (mmStringNpos == n)
        {
            break;
        }
        else
        {
            u[i] = o + n + 1;
            o = u[i];
            w = (const char*)(w + n + 1);
            l -= (n + 1);
        }
    }
    
    // mmStringNpos != n is safety.
    // Parse as much data as possible.
    for (i = 0;i < d;++i)
    {
        k = x[i];
        w = (const char*)(y + u[k]);
        (*((mmAToTypeFunc)f))(w, (mmUInt8_t*)a + z * i);
    }
    
    mmFree(u);
}

/*
 * This function transform type[] to string format.
 *
 * @param a type a[4].
 * @param z sizeof(type).
 * @param v "((1,2),(3,4))".
 * @param s "((,),(,))".
 * @param x int x[] = { 1, 2, 4, 5 }.
 * @param d 4.
 * @param f void(*mmTypeToAFunc)(const void*, char*).
 */
MM_EXPORT_DLL
void
mmValueTypeArrayToStringFormat(
    const void*                                    a,
    size_t                                         z,
    struct mmString*                               v,
    const char*                                    s,
    const int*                                     x,
    int                                            d,
    void*                                          f)
{
    char w[64];
    int i;
    int k = 0;
    int m = (int)strlen(s);
    
    assert(0 < d && "Dimension must > 0");
    assert(0 < m && "m is invalid.");
    
    mmString_Clear(v);
    mmString_Reserve(v, m + MM_ARRAY_SIZE(w) * d);
    
    mmString_Appendsn(v, &s[0], x[0]);
    
    for (i = 0;i < d;++i)
    {
        mmString_Appendsn(v, &s[k + 1], x[i] - k);
        k = x[i];
        mmMemset(w, 0, 64);
        (*((mmTypeToAFunc)f))((mmUInt8_t*)a + z * i, w);
        mmString_Appends(v, w);
    }
    
    mmString_Appendsn(v, &s[k + 1], m - k);
    
    mmString_ShrinkToFit(v);
}

/*
 * This function transform string to type[].
 *
 * @param v " 1 2 3 4 ".
 * @param a type a[4].
 * @param z sizeof(type).
 * @param s ' '.
 * @param d 4.
 * @param f void(*mmAToTypeFunc)(const char*, type*).
 */
MM_EXPORT_DLL
void
mmValueSplitStringToTypeArray(
    const struct mmString*                         v,
    void*                                          a,
    size_t                                         z,
    char                                           s,
    int                                            d,
    void*                                          f)
{
    const char* w;
    size_t l;
    size_t n;
    int i;

    w = (char*)mmString_CStr(v);
    l = mmString_Size(v);

    i = 0;
    n = mmCStringFindNextNotOf((const char*)w, s, 0, l);
    if (mmStringNpos == n)
    {
        return;
    }
    else
    {
        w = (const char*)(w + n);
        (*((mmAToTypeFunc)f))(w, (mmUInt8_t*)a + z * i);
        l -= (n);
    }

    for (i = 1; i < d; ++i)
    {
        n = mmCStringFindNextOf((const char*)w, s, 0, l);
        if (mmStringNpos == n)
        {
            break;
        }
        else
        {
            w = (const char*)(w + n + 1);
            (*((mmAToTypeFunc)f))(w, (mmUInt8_t*)a + z * i);
            l -= (n + 1);
        }
    }
}

/*
 * This function transform type[] to string.
 *
 * @param a type a[4].
 * @param z sizeof(type).
 * @param v " 1 2 3 4 ".
 * @param s ' '.
 * @param d 4.
 * @param f void(*mmTypeToAFunc)(const void*, char*).
 */
MM_EXPORT_DLL
void
mmValueTypeArrayToSplitString(
    const void*                                    a,
    size_t                                         z,
    struct mmString*                               v,
    char                                           s,
    int                                            d,
    void*                                          f)
{
    char w[64];
    int i;
    int m = d - 1;

    assert(0 < d && "Dimension must > 0");

    mmString_Clear(v);
    mmString_Reserve(v, m + MM_ARRAY_SIZE(w) * d);

    for (i = 0; i < d; ++i)
    {
        mmMemset(w, 0, 64);
        (*((mmTypeToAFunc)f))((mmUInt8_t*)a + z * i, w);
        mmString_Appends(v, w);
        mmString_Appendc(v, s);
    }
    mmString_PopBack(v);
    mmString_ShrinkToFit(v);
}

MM_EXPORT_DLL
void
mmFloatSizeToString(
    float                                          hSize[2],
    char                                           hString[128])
{
    mmSprintf(
        hString,
        "(%.02f, %.02f)",
        hSize[0], hSize[1]);
}

MM_EXPORT_DLL
void
mmFloatRectToString(
    float                                          hRect[4],
    char                                           hString[128])
{
    mmSprintf(
        hString,
        "(%.02f, %.02f, %.02f, %.02f)",
        hRect[0], hRect[1], hRect[2], hRect[3]);
}

MM_EXPORT_DLL
void
mmDoubleSizeToString(double hSize[2], char hString[128])
{
    mmSprintf(
        hString, 
        "(%.02lf, %.02lf)", 
        hSize[0], hSize[1]);
}

MM_EXPORT_DLL
void
mmDoubleRectToString(double hRect[4], char hString[128])
{
    mmSprintf(
        hString, 
        "(%.02lf, %.02lf, %.02lf, %.02lf)", 
        hRect[0], hRect[1], hRect[2], hRect[3]);
}

MM_EXPORT_DLL
size_t
mmCStringFindNextOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max)
{
    size_t index = mmStringNpos;
    size_t sz = i + max;
    if (0 < sz)
    {
        const char* ps = s + i;
        size_t idx = 0;
        for (idx = 0; idx < sz; ++idx)
        {
            if (ps[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringFindPrevOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max)
{
    size_t index = mmStringNpos;
    size_t sz = i + max;
    if (0 < sz)
    {
        const char* ps = s + i;
        size_t idx = 0;
        for (idx = sz - 1; mmStringNpos != idx; --idx)
        {
            if (ps[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringFindNextNotOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max)
{
    size_t index = mmStringNpos;
    size_t sz = i + max;
    if (0 < sz)
    {
        const char* ps = s + i;
        size_t idx = 0;
        for (idx = 0; idx < sz; ++idx)
        {
            if (ps[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringFindPrevNotOf(
    const char*                                    s,
    char                                           c,
    size_t                                         i,
    size_t                                         max)
{
    size_t index = mmStringNpos;
    size_t sz = i + max;
    if (0 < sz)
    {
        const char* ps = s + i;
        size_t idx = 0;
        for (idx = sz - 1; mmStringNpos != idx; --idx)
        {
            if (ps[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringFindNextStringOf(
    const char*                                    s,
    const char*                                    c,
    size_t                                         i,
    size_t                                         max)
{
    size_t index = mmStringNpos;
    size_t clen = mmStrlen(c);
    size_t sz = i + max;
    if (clen < sz)
    {
        const char* ps = s + i;
        size_t idx = 0;
        size_t ridx = sz - clen;
        for (idx = 0; idx < ridx; ++idx)
        {
            if (0 == strncmp(&ps[idx], c, clen))
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringFindPrevStringOf(
    const char*                                    s,
    const char*                                    c,
    size_t                                         i,
    size_t                                         max)
{
    size_t index = mmStringNpos;
    size_t clen = mmStrlen(c);
    size_t sz = i + max;
    if (clen < sz)
    {
        const char* ps = s + i;
        size_t idx = 0;
        size_t ridx = sz - clen;
        for (idx = ridx - 1; mmStringNpos != idx; --idx)
        {
            if (0 == strncmp(&ps[idx], c, clen))
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringTolower(
    const char*                                    s,
    size_t                                         z,
    char*                                          t,
    size_t                                         m)
{
    size_t l;
    size_t index = mmStringNpos;
    z = (mmStringNpos == z) ? mmStrlen(s) : z;
    l = z > m ? m : z;
    if (0 < l)
    {
        for (index = 0; index < l; ++index)
        {
            t[index] = mmTolower(s[index]);
        }
    }
    return index;
}

MM_EXPORT_DLL
size_t
mmCStringToupper(
    const char*                                    s,
    size_t                                         z,
    char*                                          t,
    size_t                                         m)
{
    size_t l;
    size_t index = mmStringNpos;
    z = (mmStringNpos == z) ? mmStrlen(s) : z;
    l = z > m ? m : z;
    if (0 < l)
    {
        for (index = 0; index < l; ++index)
        {
            t[index] = mmTolower(s[index]);
        }
    }
    return index;
}

// ("abc", 5) ==> "abc  "
MM_EXPORT_DLL
void
mmCStrLAlignAppends(
    char*                                          str,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length)
{
    size_t usz = ssz > length ? ssz : length;
    size_t rsz = usz - ssz;
    char* ss = str;
    mmMemcpy(ss, s, ssz);
    ss += ssz;
    mmMemset(ss, ' ', rsz);
    ss += rsz;
    (*ss) = 0;
}

// ("abc", 5) ==> " abc "
// ("abc", 6) ==> " abc  "
MM_EXPORT_DLL
void
mmCStrMAlignAppends(
    char*                                          str,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length)
{
    size_t usz = ssz > length ? ssz : length;
    size_t zsz = usz - ssz;
    size_t lsz = zsz / 2;
    size_t rsz = zsz - lsz;
    char* ss = str;
    mmMemset(ss, ' ', lsz);
    ss += lsz;
    mmMemcpy(ss, s, ssz);
    ss += ssz;
    mmMemset(ss, ' ', rsz);
    ss += rsz;
    (*ss) = 0;
}

// ("abc", 5) ==> "  abc"
MM_EXPORT_DLL
void
mmCStrRAlignAppends(
    char*                                          str,
    const char*                                    s,
    size_t                                         ssz,
    size_t                                         length)
{
    size_t usz = ssz > length ? ssz : length;
    size_t lsz = usz - ssz;
    char* ss = str;
    mmMemset(ss, ' ', lsz);
    ss += lsz;
    mmMemcpy(ss, s, ssz);
    ss += ssz;
    (*ss) = 0;
}

// " abc " ==> 1
MM_EXPORT_DLL
size_t
mmCStringFindRSidePrint(
    const char*                                    s,
    size_t                                         l)
{
    size_t index = mmStringNpos;
    if (0 < l)
    {
        for (index = l - 1; mmStringNpos != index; --index)
        {
            if (0 != isprint(s[index]) && ' ' != s[index])
            {
                break;
            }
        }
    }
    return index;
}

// " abc " ==> 1
MM_EXPORT_DLL
size_t
mmCStringFindLSidePrint(
    const char*                                    s,
    size_t                                         l)
{
    size_t index = mmStringNpos;
    if (0 < l)
    {
        for (index = 0; index < l; ++index)
        {
            if (0 != isprint(s[index]) && ' ' != s[index])
            {
                break;
            }
        }
    }
    return index;
}

// " abc " ==> "abc"
MM_EXPORT_DLL
void
mmCStringRemoveBothSidesSpace(
    const char*                                    s,
    size_t                                         l,
    size_t*                                        idx,
    size_t*                                        size)
{
    size_t sz = 0;
    size_t p0 = mmCStringFindLSidePrint(s, l);
    size_t p1 = mmCStringFindRSidePrint(s, l);
    p0 = (mmStringNpos == p0) ? 0 : p0;
    p1 = (mmStringNpos == p1) ? l : p1;
    sz = p1 - p0 + 1;
    (*idx) = p0;
    (*size) = sz;
}

MM_EXPORT_DLL
char
mmCStringLSidePrint(
    const char*                                    s,
    size_t                                         l)
{
    size_t idx = mmCStringFindLSidePrint(s, l);
    idx = (mmStringNpos == idx) ? 0 : idx;
    return s[idx];
}

MM_EXPORT_DLL
char
mmCStringRSidePrint(
    const char*                                    s,
    size_t                                         l)
{
    size_t idx = mmCStringFindRSidePrint(s, l);
    idx = (mmStringNpos == idx) ? 0 : idx;
    return s[idx];
}
