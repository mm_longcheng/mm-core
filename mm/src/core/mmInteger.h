/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmInteger_h__
#define __mmInteger_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

MM_EXPORT_DLL void mmInteger_UInt08Swap(mmUInt8_t* u, mmUInt8_t* v);
MM_EXPORT_DLL void mmInteger_UInt16Swap(mmUInt16_t* u, mmUInt16_t* v);
MM_EXPORT_DLL void mmInteger_UInt32Swap(mmUInt32_t* u, mmUInt32_t* v);
MM_EXPORT_DLL void mmInteger_UInt64Swap(mmUInt64_t* u, mmUInt64_t* v);
MM_EXPORT_DLL void mmInteger_UIntptrSwap(uintptr_t* u, uintptr_t* v);

MM_EXPORT_DLL void mmInteger_IntSwap(int* u, int* v);
MM_EXPORT_DLL void mmInteger_ShortSwap(short* u, short* v);
MM_EXPORT_DLL void mmInteger_LongSwap(long* u, long* v);
MM_EXPORT_DLL void mmInteger_LongLongSwap(long long* u, long long* v);
MM_EXPORT_DLL void mmInteger_SizeSwap(size_t* u, size_t* v);

#define mmIntegerSwap(u, v) { u = u ^ v; v = u ^ v; u = u ^ v; }

MM_EXPORT_DLL mmSInt64_t mmAbsSInt64(mmSInt64_t x);
MM_EXPORT_DLL mmSInt32_t mmAbsSInt32(mmSInt32_t x);

#include "core/mmSuffix.h"

#endif//__mmInteger_h__
