/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmByteCoder_h__
#define __mmByteCoder_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmByteBuffer;

// Note: we use mmByteBuffer.offset for bytes coder offset.
MM_EXPORT_DLL void mmByteCoderBiggerEncodeBin(struct mmByteBuffer* b, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteCoderBiggerEncodeU08(struct mmByteBuffer* b, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteCoderBiggerEncodeU16(struct mmByteBuffer* b, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteCoderBiggerEncodeU32(struct mmByteBuffer* b, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteCoderBiggerEncodeU64(struct mmByteBuffer* b, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteCoderBiggerDecodeBin(struct mmByteBuffer* b, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteCoderBiggerDecodeU08(struct mmByteBuffer* b, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteCoderBiggerDecodeU16(struct mmByteBuffer* b, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteCoderBiggerDecodeU32(struct mmByteBuffer* b, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteCoderBiggerDecodeU64(struct mmByteBuffer* b, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteCoderLittleEncodeBin(struct mmByteBuffer* b, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteCoderLittleEncodeU08(struct mmByteBuffer* b, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteCoderLittleEncodeU16(struct mmByteBuffer* b, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteCoderLittleEncodeU32(struct mmByteBuffer* b, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteCoderLittleEncodeU64(struct mmByteBuffer* b, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteCoderLittleDecodeBin(struct mmByteBuffer* b, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteCoderLittleDecodeU08(struct mmByteBuffer* b, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteCoderLittleDecodeU16(struct mmByteBuffer* b, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteCoderLittleDecodeU32(struct mmByteBuffer* b, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteCoderLittleDecodeU64(struct mmByteBuffer* b, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteCoderHostEncodeBin(struct mmByteBuffer* b, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteCoderHostEncodeU08(struct mmByteBuffer* b, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteCoderHostEncodeU16(struct mmByteBuffer* b, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteCoderHostEncodeU32(struct mmByteBuffer* b, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteCoderHostEncodeU64(struct mmByteBuffer* b, mmUInt64_t* v);

MM_EXPORT_DLL void mmByteCoderHostDecodeBin(struct mmByteBuffer* b, mmUInt8_t* v, size_t l);
MM_EXPORT_DLL void mmByteCoderHostDecodeU08(struct mmByteBuffer* b, mmUInt8_t*  v);
MM_EXPORT_DLL void mmByteCoderHostDecodeU16(struct mmByteBuffer* b, mmUInt16_t* v);
MM_EXPORT_DLL void mmByteCoderHostDecodeU32(struct mmByteBuffer* b, mmUInt32_t* v);
MM_EXPORT_DLL void mmByteCoderHostDecodeU64(struct mmByteBuffer* b, mmUInt64_t* v);

#include "core/mmSuffix.h"

#endif//__mmByteCoder_h__
