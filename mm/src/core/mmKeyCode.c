/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmKeyCode.h"

MM_EXPORT_DLL int mmKeyCodeToModifierKey(int key)
{
    switch (key)
    {
    case MM_KC_RSHIFT:
    case MM_KC_LSHIFT:
        return MM_MODIFIER_SHIFT;

    case MM_KC_LCONTROL:
    case MM_KC_RCONTROL:
        return MM_MODIFIER_CONTROL;

    case MM_KC_LMENU:
    case MM_KC_RMENU:
        return MM_MODIFIER_OPTION;
            
    case MM_KC_LWIN:
    case MM_KC_RWIN:
        return MM_MODIFIER_COMMAND;
            
    case MM_KC_CAPITAL:
        return MM_MODIFIER_CAPITAL;

    case MM_KC_NUMLOCK:
        return MM_MODIFIER_NUMLOCK;
    default:
        return 0;
    }
}

MM_EXPORT_DLL int mmModifierKeyToKeyCode(int key)
{
    switch (key)
    {
    case MM_MODIFIER_SHIFT:
        return MM_KC_LSHIFT;

    case MM_MODIFIER_CONTROL:
        return MM_KC_LCONTROL;

    case MM_MODIFIER_OPTION:
        return MM_KC_LMENU;

    case MM_MODIFIER_COMMAND:
        return MM_KC_LWIN;
        
    case MM_MODIFIER_CAPITAL:
        return MM_KC_CAPITAL;
        
    case MM_MODIFIER_NUMLOCK:
        return MM_KC_NUMLOCK;
            
    default:
        return MM_KC_UNKNOWN;
    }
}
