/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
  Red Black Trees
  (C) 1999  Andrea Arcangeli <andrea@suse.de>
  (C) 2002  David Woodhouse <dwmw2@infradead.org>
  (C) 2012  Michel Lespinasse <walken@google.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  linux/lib/rbtree.c
*/

// #include <linux/rbtree_augmented.h>
// #include <linux/export.h>
#include "core/mmRbtreeAugmented.h"
//#include <stdbool.h>
// #define EXPORT_SYMBOL(x)
/*
 * red-black trees properties:  http://en.wikipedia.org/wiki/Rbtree
 *
 *  1) A node is either red or black
 *  2) The root is black
 *  3) All leaves (NULL) are black
 *  4) Both children of every red node are black
 *  5) Every simple path from root to leaves contains the same number
 *     of black nodes.
 *
 *  4 and 5 give the O(log n) guarantee, since 4 implies you cannot have two
 *  consecutive red nodes in a path and every red node is therefore followed by
 *  a black. So if B is the number of black nodes on every simple path (as per
 *  5), then the longest possible path due to 4 is 2B.
 *
 *  We shall indicate color with case, where black nodes are uppercase and red
 *  nodes will be lowercase. Unknown color nodes shall be drawn as red within
 *  parentheses and have some accompanying text comment.
 */

static mmInline void mmRb_SetBlack(struct mmRbNode *rb)
{
    rb->__rb_parent_color |= MM_RB_BLACK;
}

static mmInline struct mmRbNode *mmRb_RedParent(struct mmRbNode *red)
{
    // uintptr_t is type for unsigned long, it is safe.
    return (struct mmRbNode *)((uintptr_t)(red->__rb_parent_color));
}

/*
 * Helper function for rotations:
 * - old's parent and color get assigned to new
 * - old gets assigned new as a parent and 'color' as a color.
 */
static mmInline void __mmRb_RotateSetParents(struct mmRbNode *old_node, struct mmRbNode *new_node,
    struct mmRbRoot *root, int color)
{
    struct mmRbNode *parent = mmRb_Parent(old_node);
    new_node->__rb_parent_color = old_node->__rb_parent_color;
    mmRb_SetParentColor(old_node, new_node, color);
    __mmRb_ChangeChild(old_node, new_node, parent, root);
}

static MM_FORCEINLINE void __mmRb_Insert(struct mmRbNode *node, struct mmRbRoot *root,
    void(*augment_rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node))
{
    struct mmRbNode *parent = mmRb_RedParent(node), *gparent, *tmp;

    while (1) 
    {
        /*
         * Loop invariant: node is red
         *
         * If there is a black parent, we are done.
         * Otherwise, take some corrective action as we don't
         * want a red root or two consecutive red nodes.
         */
        if (!parent) 
        {
            mmRb_SetParentColor(node, NULL, MM_RB_BLACK);
            break;
        }
        else if (mmRb_IsBlack(parent))
            break;

        gparent = mmRb_RedParent(parent);

        tmp = gparent->rb_right;
        if (parent != tmp) 
        {   
            /* parent == gparent->rb_left */
            if (tmp && mmRb_IsRed(tmp)) 
            {
                /*
                 * Case 1 - color flips
                 *
                 *       G            g
                 *      / \          / \
                 *     p   u  -->   P   U
                 *    /            /
                 *   n            n
                 *
                 * However, since g's parent might be red, and
                 * 4) does not allow this, we need to recurse
                 * at g.
                 */
                mmRb_SetParentColor(tmp, gparent, MM_RB_BLACK);
                mmRb_SetParentColor(parent, gparent, MM_RB_BLACK);
                node = gparent;
                parent = mmRb_Parent(node);
                mmRb_SetParentColor(node, parent, MM_RB_RED);
                continue;
            }

            tmp = parent->rb_right;
            if (node == tmp) 
            {
                /*
                 * Case 2 - left rotate at parent
                 *
                 *      G             G
                 *     / \           / \
                 *    p   U  -->    n   U
                 *     \           /
                 *      n         p
                 *
                 * This still leaves us in violation of 4), the
                 * continuation into Case 3 will fix that.
                 */
                parent->rb_right = tmp = node->rb_left;
                node->rb_left = parent;
                if (tmp)
                    mmRb_SetParentColor(tmp, parent,
                        MM_RB_BLACK);
                mmRb_SetParentColor(parent, node, MM_RB_RED);
                augment_rotate(parent, node);
                parent = node;
                tmp = node->rb_right;
            }

            /*
             * Case 3 - right rotate at gparent
             *
             *        G           P
             *       / \         / \
             *      p   U  -->  n   g
             *     /                 \
             *    n                   U
             */
            gparent->rb_left = tmp;  /* == parent->rb_right */
            parent->rb_right = gparent;
            if (tmp)
                mmRb_SetParentColor(tmp, gparent, MM_RB_BLACK);
            __mmRb_RotateSetParents(gparent, parent, root, MM_RB_RED);
            augment_rotate(gparent, parent);
            break;
        }
        else 
        {
            tmp = gparent->rb_left;
            if (tmp && mmRb_IsRed(tmp)) 
            {
                /* Case 1 - color flips */
                mmRb_SetParentColor(tmp, gparent, MM_RB_BLACK);
                mmRb_SetParentColor(parent, gparent, MM_RB_BLACK);
                node = gparent;
                parent = mmRb_Parent(node);
                mmRb_SetParentColor(node, parent, MM_RB_RED);
                continue;
            }

            tmp = parent->rb_left;
            if (node == tmp) 
            {
                /* Case 2 - right rotate at parent */
                parent->rb_left = tmp = node->rb_right;
                node->rb_right = parent;
                if (tmp)
                    mmRb_SetParentColor(tmp, parent,
                        MM_RB_BLACK);
                mmRb_SetParentColor(parent, node, MM_RB_RED);
                augment_rotate(parent, node);
                parent = node;
                tmp = node->rb_left;
            }

            /* Case 3 - left rotate at gparent */
            gparent->rb_right = tmp;  /* == parent->rb_left */
            parent->rb_left = gparent;
            if (tmp)
                mmRb_SetParentColor(tmp, gparent, MM_RB_BLACK);
            __mmRb_RotateSetParents(gparent, parent, root, MM_RB_RED);
            augment_rotate(gparent, parent);
            break;
        }
    }
}

/*
 * Inline version for rb_erase() use - we want to be able to inline
 * and eliminate the dummy_rotate callback there
 */
static MM_FORCEINLINE void
____mmRb_EraseColor(struct mmRbNode *parent, struct mmRbRoot *root,
    void(*augment_rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node))
{
    struct mmRbNode *node = NULL, *sibling, *tmp1, *tmp2;

    while (1) 
    {
        /*
         * Loop invariants:
         * - node is black (or NULL on first iteration)
         * - node is not the root (parent is not NULL)
         * - All leaf paths going through parent and node have a
         *   black node count that is 1 lower than other leaf paths.
         */
        sibling = parent->rb_right;
        if (node != sibling) 
        {   
            /* node == parent->rb_left */
            if (mmRb_IsRed(sibling)) 
            {
                /*
                 * Case 1 - left rotate at parent
                 *
                 *     P               S
                 *    / \             / \
                 *   N   s    -->    p   Sr
                 *      / \         / \
                 *     Sl  Sr      N   Sl
                 */
                parent->rb_right = tmp1 = sibling->rb_left;
                sibling->rb_left = parent;
                mmRb_SetParentColor(tmp1, parent, MM_RB_BLACK);
                __mmRb_RotateSetParents(parent, sibling, root,
                    MM_RB_RED);
                augment_rotate(parent, sibling);
                sibling = tmp1;
            }
            tmp1 = sibling->rb_right;
            if (!tmp1 || mmRb_IsBlack(tmp1)) 
            {
                tmp2 = sibling->rb_left;
                if (!tmp2 || mmRb_IsBlack(tmp2)) 
                {
                    /*
                     * Case 2 - sibling color flip
                     * (p could be either color here)
                     *
                     *    (p)           (p)
                     *    / \           / \
                     *   N   S    -->  N   s
                     *      / \           / \
                     *     Sl  Sr        Sl  Sr
                     *
                     * This leaves us violating 5) which
                     * can be fixed by flipping p to black
                     * if it was red, or by recursing at p.
                     * p is red when coming from Case 1.
                     */
                    mmRb_SetParentColor(sibling, parent,
                        MM_RB_RED);
                    if (mmRb_IsRed(parent))
                        mmRb_SetBlack(parent);
                    else 
                    {
                        node = parent;
                        parent = mmRb_Parent(node);
                        if (parent)
                            continue;
                    }
                    break;
                }
                /*
                 * Case 3 - right rotate at sibling
                 * (p could be either color here)
                 *
                 *   (p)           (p)
                 *   / \           / \
                 *  N   S    -->  N   Sl
                 *     / \             \
                 *    sl  Sr            s
                 *                       \
                 *                        Sr
                 */
                sibling->rb_left = tmp1 = tmp2->rb_right;
                tmp2->rb_right = sibling;
                parent->rb_right = tmp2;
                if (tmp1)
                    mmRb_SetParentColor(tmp1, sibling,
                        MM_RB_BLACK);
                augment_rotate(sibling, tmp2);
                tmp1 = sibling;
                sibling = tmp2;
            }
            /*
             * Case 4 - left rotate at parent + color flips
             * (p and sl could be either color here.
             *  After rotation, p becomes black, s acquires
             *  p's color, and sl keeps its color)
             *
             *      (p)             (s)
             *      / \             / \
             *     N   S     -->   P   Sr
             *        / \         / \
             *      (sl) sr      N  (sl)
             */
            parent->rb_right = tmp2 = sibling->rb_left;
            sibling->rb_left = parent;
            mmRb_SetParentColor(tmp1, sibling, MM_RB_BLACK);
            if (tmp2)
                mmRb_SetParent(tmp2, parent);
            __mmRb_RotateSetParents(parent, sibling, root,
                MM_RB_BLACK);
            augment_rotate(parent, sibling);
            break;
        }
        else 
        {
            sibling = parent->rb_left;
            if (mmRb_IsRed(sibling)) 
            {
                /* Case 1 - right rotate at parent */
                parent->rb_left = tmp1 = sibling->rb_right;
                sibling->rb_right = parent;
                mmRb_SetParentColor(tmp1, parent, MM_RB_BLACK);
                __mmRb_RotateSetParents(parent, sibling, root,
                    MM_RB_RED);
                augment_rotate(parent, sibling);
                sibling = tmp1;
            }
            tmp1 = sibling->rb_left;
            if (!tmp1 || mmRb_IsBlack(tmp1)) 
            {
                tmp2 = sibling->rb_right;
                if (!tmp2 || mmRb_IsBlack(tmp2)) 
                {
                    /* Case 2 - sibling color flip */
                    mmRb_SetParentColor(sibling, parent,
                        MM_RB_RED);
                    if (mmRb_IsRed(parent))
                        mmRb_SetBlack(parent);
                    else 
                    {
                        node = parent;
                        parent = mmRb_Parent(node);
                        if (parent)
                            continue;
                    }
                    break;
                }
                /* Case 3 - right rotate at sibling */
                sibling->rb_right = tmp1 = tmp2->rb_left;
                tmp2->rb_left = sibling;
                parent->rb_left = tmp2;
                if (tmp1)
                    mmRb_SetParentColor(tmp1, sibling,
                        MM_RB_BLACK);
                augment_rotate(sibling, tmp2);
                tmp1 = sibling;
                sibling = tmp2;
            }
            /* Case 4 - left rotate at parent + color flips */
            parent->rb_left = tmp2 = sibling->rb_right;
            sibling->rb_right = parent;
            mmRb_SetParentColor(tmp1, sibling, MM_RB_BLACK);
            if (tmp2)
                mmRb_SetParent(tmp2, parent);
            __mmRb_RotateSetParents(parent, sibling, root,
                MM_RB_BLACK);
            augment_rotate(parent, sibling);
            break;
        }
    }
}

/* Non-inline version for mmRb_EraseAugmented() use */
MM_EXPORT_DLL void __mmRb_EraseColor(struct mmRbNode *parent, struct mmRbRoot *root,
    void(*augment_rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node))
{
    ____mmRb_EraseColor(parent, root, augment_rotate);
}
// EXPORT_SYMBOL(__mmRb_EraseColor);

/*
 * Non-augmented rbtree manipulation functions.
 *
 * We use dummy augmented callbacks here, and have the compiler optimize them
 * out of the rb_insert_color() and rb_erase() function definitions.
 */

static mmInline void mmDummyPropagate(struct mmRbNode *node, struct mmRbNode *stop) {}
static mmInline void mmDummyCopy(struct mmRbNode *old_node, struct mmRbNode *new_node) {}
static mmInline void mmDummyRotate(struct mmRbNode *old_node, struct mmRbNode *new_node) {}

static const struct mmRbAugmentCallbacks mmDummyCallbacks = 
{
    mmDummyPropagate, mmDummyCopy, mmDummyRotate
};

MM_EXPORT_DLL void mmRb_InsertColor(struct mmRbNode *node, struct mmRbRoot *root)
{
    __mmRb_Insert(node, root, mmDummyRotate);
}
// EXPORT_SYMBOL(mmRb_InsertColor);

MM_EXPORT_DLL void mmRb_Erase(struct mmRbNode *node, struct mmRbRoot *root)
{
    struct mmRbNode *rebalance;
    rebalance = __mmRb_EraseAugmented(node, root, &mmDummyCallbacks);
    if (rebalance)
        ____mmRb_EraseColor(rebalance, root, mmDummyRotate);
}
// EXPORT_SYMBOL(mmRb_Erase);

/*
 * Augmented mm_rbtree manipulation functions.
 *
 * This instantiates the same MM_FORCEINLINE functions as in the non-augmented
 * case, but this time with user-defined callbacks.
 */

MM_EXPORT_DLL void __mmRb_InsertAugmented(struct mmRbNode *node, struct mmRbRoot *root,
    void(*augment_rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node))
{
    __mmRb_Insert(node, root, augment_rotate);
}
// EXPORT_SYMBOL(__mmRb_InsertAugmented);

/*
 * This function returns the first node (in sort order) of the tree.
 */
MM_EXPORT_DLL struct mmRbNode *mmRb_First(const struct mmRbRoot *root)
{
    struct mmRbNode *n;

    n = root->rb_node;
    if (!n)
        return NULL;
    while (n->rb_left)
        n = n->rb_left;
    return n;
}
// EXPORT_SYMBOL(mmRb_First);

MM_EXPORT_DLL struct mmRbNode *mmRb_Last(const struct mmRbRoot *root)
{
    struct mmRbNode *n;

    n = root->rb_node;
    if (!n)
        return NULL;
    while (n->rb_right)
        n = n->rb_right;
    return n;
}
// EXPORT_SYMBOL(mmRb_Last);

MM_EXPORT_DLL struct mmRbNode *mmRb_Next(const struct mmRbNode *node)
{
    struct mmRbNode *parent;

    if (MM_RB_EMPTY_NODE(node))
        return NULL;

    /*
     * If we have a right-hand child, go down and then left as far
     * as we can.
     */
    if (node->rb_right) 
    {
        node = node->rb_right;
        while (node->rb_left)
            node = node->rb_left;
        return (struct mmRbNode *)node;
    }

    /*
     * No right-hand children. Everything down and left is smaller than us,
     * so any 'next' node must be in the general direction of our parent.
     * Go up the tree; any time the ancestor is a right-hand child of its
     * parent, keep going up. First time it's a left-hand child of its
     * parent, said parent is our 'next' node.
     */
    while ((parent = mmRb_Parent(node)) && node == parent->rb_right)
        node = parent;

    return parent;
}
// EXPORT_SYMBOL(mmRb_Next);

MM_EXPORT_DLL struct mmRbNode *mmRb_Prev(const struct mmRbNode *node)
{
    struct mmRbNode *parent;

    if (MM_RB_EMPTY_NODE(node))
        return NULL;

    /*
     * If we have a left-hand child, go down and then right as far
     * as we can.
     */
    if (node->rb_left) 
    {
        node = node->rb_left;
        while (node->rb_right)
            node = node->rb_right;
        return (struct mmRbNode *)node;
    }

    /*
     * No left-hand children. Go up till we find an ancestor which
     * is a right-hand child of its parent.
     */
    while ((parent = mmRb_Parent(node)) && node == parent->rb_left)
        node = parent;

    return parent;
}
// EXPORT_SYMBOL(mmRb_Prev);

MM_EXPORT_DLL void mmRb_ReplaceNode(struct mmRbNode *victim, struct mmRbNode *newn,
    struct mmRbRoot *root)
{
    struct mmRbNode *parent = mmRb_Parent(victim);

    /* Set the surrounding nodes to point to the replacement */
    __mmRb_ChangeChild(victim, newn, parent, root);
    if (victim->rb_left)
        mmRb_SetParent(victim->rb_left, newn);
    if (victim->rb_right)
        mmRb_SetParent(victim->rb_right, newn);

    /* Copy the pointers/colour from the victim to the replacement */
    *newn = *victim;
}
// EXPORT_SYMBOL(mmRb_ReplaceNode);

static struct mmRbNode *mmRb_LeftDeepestNode(const struct mmRbNode *node)
{
    for (;;) 
    {
        if (node->rb_left)
            node = node->rb_left;
        else if (node->rb_right)
            node = node->rb_right;
        else
            return (struct mmRbNode *)node;
    }
}

MM_EXPORT_DLL struct mmRbNode *mmRb_NextPostorder(const struct mmRbNode *node)
{
    const struct mmRbNode *parent;
    if (!node)
        return NULL;
    parent = mmRb_Parent(node);

    /* If we're sitting on node, we've already seen our children */
    if (parent && node == parent->rb_left && parent->rb_right) 
    {
        /* If we are the parent's left node, go to the parent's right
         * node then all the way down to the left */
        return mmRb_LeftDeepestNode(parent->rb_right);
    }
    else
        /* Otherwise we are the parent's right node, and the parent
         * should be next */
        return (struct mmRbNode *)parent;
}
// EXPORT_SYMBOL(mmRb_NextPostorder);

MM_EXPORT_DLL struct mmRbNode *mmRb_FirstPostorder(const struct mmRbRoot *root)
{
    if (!root->rb_node)
        return NULL;

    return mmRb_LeftDeepestNode(root->rb_node);
}
// EXPORT_SYMBOL(mmRb_FirstPostorder);
