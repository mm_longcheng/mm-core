/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMetadata_h__
#define __mmMetadata_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*
 * example
 *
 * struct mmMetadataType
 * {
 *     const struct mmMetaAllocator* Allocator;
 *     const struct mmIHandle*       Handle;
 * };
 */

struct mmMetaAllocator
{
    // type name
    const char* TypeName;

    // type size
    size_t TypeSize;

    // typedef void(*Produce)(void* p);
    void* Produce;

    // typedef void(*Recycle)(void* p);
    void* Recycle;
};

/*
 * @brief Use for matedata function call.
 *
 * @param p          Type pointer
 * @param m          matedata offset for Type.
 * @param h          handler offset for matedata.
 * @param f          function offset for ptr.
 */
MM_EXPORT_DLL
void
mmMetadataHandleArgs0(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         f);

/*
 * @brief Use for matedata function call.
 *
 * @param p          Type pointer
 * @param m          matedata offset for Type.
 * @param h          handler offset for matedata.
 * @param f          function offset for ptr.
 */
MM_EXPORT_DLL
void
mmMetadataHandleArgs1(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         f,
    void*                                          a);

/*
 * @brief Use for matedata function call.
 *
 * @param p          Type pointer
 * @param m          matedata offset for Type.
 * @param h          handler offset for matedata.
 * @param g          group offset for handler.
 * @param f          function offset for ptr.
 */
MM_EXPORT_DLL
void
mmMetadataHandleGroupArgs0(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         g,
    size_t                                         f);

/*
 * @brief Use for matedata function call.
 *
 * @param p          Type pointer
 * @param m          matedata offset for Type.
 * @param h          handler offset for matedata.
 * @param g          group offset for handler.
 * @param f          function offset for ptr.
 * @param a          function argument.
 */
MM_EXPORT_DLL
void
mmMetadataHandleGroupArgs1(
    void*                                          p,
    size_t                                         m,
    size_t                                         h,
    size_t                                         g,
    size_t                                         f,
    void*                                          a);

/*
 * @brief Use for matedata function empty call.
 *
 * @param p          Type pointer
 */
MM_EXPORT_DLL
void
mmMetadataDefaultArgs0(
    void*                                          p);

/*
 * @brief Use for matedata function empty call.
 *
 * @param p          Type pointer
 * @param a          function argument.
 */
MM_EXPORT_DLL
void
mmMetadataDefaultArgs1(
    void*                                          p,
    void*                                          a);

/*
 * @brief Use for matedata function empty call.
 *
 * @param p          Type pointer
 * @param a          function argument.
 */
MM_EXPORT_DLL
void
mmMetadataDefaultArgs1Int(
    void*                                          p,
    int                                            a);

#include "core/mmSuffix.h"

#endif//__mmMetadata_h__
