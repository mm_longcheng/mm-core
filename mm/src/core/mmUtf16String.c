/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUtf16String.h"
#include "core/mmAlloc.h"

static mmInline mmUInt16_t* mmUtf16_Allocate(size_t n)
{
    return (mmUInt16_t*)mmMalloc(n * sizeof(mmUInt16_t));
}
static mmInline void mmUtf16_Deallocate(mmUInt16_t* d, size_t n)
{
    mmFree(d);
}
static mmInline void mmUtf16_Copy(mmUInt16_t* t, const mmUInt16_t* s, size_t n)
{
    mmMemcpy(t, s, n * sizeof(mmUInt16_t));
}
static mmInline void mmUtf16_Move(mmUInt16_t* t, const mmUInt16_t* s, size_t n)
{
    mmMemmove(t, s, n * sizeof(mmUInt16_t));
}
static mmInline int mmUtf16_compare(const mmUInt16_t* l, const mmUInt16_t* r, size_t n)
{
    return mmMemcmp(l, r, n * sizeof(mmUInt16_t));
}
static mmInline void mmUtf16_Assignn(mmUInt16_t* d, size_t n, mmUInt16_t c)
{
    mmUInt16_t* r = d;
    for (; n; --n, ++r)
    {
        *r = c;
    }
}
static mmInline void mmUtf16_Assignc(mmUInt16_t* d, mmUInt16_t c)
{
    *d = c;
}

MM_EXPORT_DLL size_t mmUtf16Strlen(const mmUInt16_t* s)
{
    const mmUInt16_t* t = s;
    while (*t != 0)
    {
        ++t;
    }
    return (size_t)(t - s);
}

MM_EXPORT_DLL const size_t mmUtf16StringTinyMask = mmUtf16StringTinyMaskMacro;
MM_EXPORT_DLL const size_t mmUtf16StringLongMask = mmUtf16StringLongMaskMacro;
MM_EXPORT_DLL const size_t mmUtf16StringNpos = -1;

enum { mmUtf16StringAlignment = 16 };

static mmInline size_t mmUtf16String_MaxSize(void)
{
    static const size_t __m = SIZE_MAX;
#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
    return (__m <= ~mmUtf16StringLongMask ? __m : __m / 2) - mmUtf16StringAlignment;
#else
    return __m - mmUtf16StringAlignment;
#endif
}

static mmInline size_t mmUtf16String_AlignIt(size_t __s)
{
    // template <size_type __a>
    // return (__s + (__a-1)) & ~(__a-1);
    return (__s + (mmUtf16StringAlignment - 1)) & ~(mmUtf16StringAlignment - 1);
}

static mmInline size_t mmUtf16String_Recommend(size_t __s)
{
    if (__s < mmUtf16StringTinyCapacity) return mmUtf16StringTinyCapacity - 1;
    // <sizeof(value_type) < mmUtf16StringAlignment ? mmUtf16StringAlignment / sizeof(value_type) : 1>
    size_t __guess = mmUtf16String_AlignIt(__s + 1) - 1;
    return (__guess == mmUtf16StringTinyCapacity) ? ++__guess : __guess;
}

static mmInline mmBool_t mmUtf16String_IsLong(const struct mmUtf16String* p)
{
    return p->s.size & mmUtf16StringTinyMask;
}

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// bigger endian
static mmInline void mmUtf16String_SetTinySize(struct mmUtf16String* p, size_t size)
{
    p->s_size = (unsigned char)(size);
}
static mmInline size_t mmUtf16String_GetTinySize(const struct mmUtf16String* p)
{
    return p->s_size;
}

#else
// little endian
static mmInline void mmUtf16String_SetTinySize(struct mmUtf16String* p, size_t size)
{
    p->s.size = (unsigned char)(size << 1);
}

static mmInline size_t mmUtf16String_GetTinySize(const struct mmUtf16String* p)
{
    return p->s.size >> 1;
}

#endif

static mmInline void mmUtf16String_SetLongSize(struct mmUtf16String* p, size_t size)
{
    p->l.size = size;
}
static mmInline size_t mmUtf16String_GetLongSize(const struct mmUtf16String* p)
{
    return p->l.size;
}
static mmInline void mmUtf16String_SetSize(struct mmUtf16String* p, size_t size)
{
    if (mmUtf16String_IsLong(p))
    {
        mmUtf16String_SetLongSize(p, size);
    }
    else
    {
        mmUtf16String_SetTinySize(p, size);
    }
}

static mmInline void mmUtf16String_SetLongCapacity(struct mmUtf16String* p, size_t capacity)
{
    p->l.capacity = mmUtf16StringLongMask | capacity;
}
static mmInline size_t mmUtf16String_GetLongCapacity(const struct mmUtf16String* p)
{
    return p->l.capacity & (size_t)(~mmUtf16StringLongMask);
}


static mmInline void mmUtf16String_SetLongPointer(struct mmUtf16String* p, mmUtf16_t* data)
{
    p->l.data = data;
}
static mmInline mmUtf16_t* mmUtf16String_GetLongPointer(const struct mmUtf16String* p)
{
    return p->l.data;
}
static mmInline mmUtf16_t* mmUtf16String_GetTinyPointer(const struct mmUtf16String* p)
{
    return (mmUtf16_t*)&p->s.data[0];
}
static mmInline mmUtf16_t* mmUtf16String_GetPointer(const struct mmUtf16String* p)
{
    return mmUtf16String_IsLong(p) ? mmUtf16String_GetLongPointer(p) : mmUtf16String_GetTinyPointer(p);
}

static void mmUtf16String_EraseToEnd(struct mmUtf16String* p, size_t __pos)
{
    if (mmUtf16String_IsLong(p))
    {
        *(mmUtf16String_GetLongPointer(p) + __pos) = 0;
        mmUtf16String_SetLongSize(p, __pos);
    }
    else
    {
        *(mmUtf16String_GetTinyPointer(p) + __pos) = 0;
        mmUtf16String_SetTinySize(p, __pos);
    }
    // __invalidate_iterators_past(__pos);
}

static int mmUtf16String_GrowBy(struct mmUtf16String* p, 
    size_t __old_cap, size_t __delta_cap, size_t __old_sz,
    size_t __n_copy, size_t __n_del, size_t __n_add)
{
    mmUtf16_t* __old_p; mmUtf16_t* __p;
    size_t __n1, __n2, __nm, __cap, __sec_cp_sz;

    size_t __ms = mmUtf16String_MaxSize();
    if (__delta_cap > __ms - __old_cap)
    {
        assert(0 && "mmUtf16String_GrowBy max size length error.");
        return EOF;
    }
    __old_p = mmUtf16String_GetPointer(p);
    __n1 = __old_cap + __delta_cap;
    __n2 = 2 * __old_cap;
    __nm = __n1 > __n2 ? __n1 : __n2;
    __cap = __old_cap < __ms / 2 - mmUtf16StringAlignment ? mmUtf16String_Recommend(__nm) : __ms - 1;
    __p = (mmUtf16_t*)mmUtf16_Allocate(__cap + 1);
    // __invalidate_all_iterators();
    if (__n_copy != 0)
    {
        mmUtf16_Move(__p, __old_p, __n_copy);
    }

    __sec_cp_sz = __old_sz - __n_del - __n_copy;
    if (__sec_cp_sz != 0)
    {
        mmUtf16_Move(__p + __n_copy + __n_add, __old_p + __n_copy + __n_del, __sec_cp_sz);
    }
    if (__old_cap + 1 != mmUtf16StringTinyCapacity)
    {
        // __alloc_traits::deallocate(__alloc(), __old_p, __old_cap + 1);
        mmUtf16_Deallocate(__old_p, __old_cap + 1);
    }
    mmUtf16String_SetLongPointer(p, __p);
    mmUtf16String_SetLongCapacity(p, __cap + 1);
    return 0;
}

static int mmUtf16String_GrowByAndReplace(struct mmUtf16String* p,
    size_t __old_cap, size_t __delta_cap, size_t __old_sz,
    size_t __n_copy, size_t __n_del, size_t __n_add, const mmUtf16_t* __p_new_stuff)
{
    mmUtf16_t* __old_p; mmUtf16_t* __p;
    size_t __b1, __b2, __bn, __cap, __sec_cp_sz;

    size_t __ms = mmUtf16String_MaxSize();
    if (__delta_cap > __ms - __old_cap - 1)
    {
        assert(0 && "mmUtf16String_GrowByAndReplace max size length error.");
        return EOF;
    }
    __old_p = mmUtf16String_GetPointer(p);
    __b1 = __old_cap + __delta_cap;
    __b2 = 2 * __old_cap;
    __bn = __b1 > __b2 ? __b1 : __b2;
    __cap = __old_cap < __ms / 2 - mmUtf16StringAlignment ? mmUtf16String_Recommend(__bn) : __ms - 1;
    __p = (mmUtf16_t*)mmUtf16_Allocate(__cap + 1);
    // __invalidate_all_iterators();
    if (__n_copy != 0)
    {
        mmUtf16_Copy(__p, __old_p, __n_copy);
    }
    if (__n_add != 0)
    {
        mmUtf16_Copy(__p + __n_copy, __p_new_stuff, __n_add);
    }
    __sec_cp_sz = __old_sz - __n_del - __n_copy;
    if (__sec_cp_sz != 0)
    {
        mmUtf16_Copy(__p + __n_copy + __n_add, __old_p + __n_copy + __n_del, __sec_cp_sz);
    }
    if (__old_cap + 1 != mmUtf16StringTinyCapacity)
    {
        // __alloc_traits::deallocate(__alloc(), __old_p, __old_cap + 1);
        mmUtf16_Deallocate(__old_p, __old_cap + 1);
    }
    mmUtf16String_SetLongPointer(p, __p);
    mmUtf16String_SetLongCapacity(p, __cap + 1);
    __old_sz = __n_copy + __n_add + __sec_cp_sz;
    mmUtf16String_SetLongSize(p, __old_sz);
    // traits_type::assign(__p[__old_sz], value_type());
    // __p[__old_sz] = 0;
    mmUtf16_Assignc(&__p[__old_sz], 0);
    return 0;
}

static int mmUtf16String_AppendChar(struct mmUtf16String* p, size_t __n, mmUtf16_t __c)
{
    int r = 0;
    if (__n)
    {
        mmUtf16_t* __p;
        size_t __cap = mmUtf16String_Capacity(p);
        size_t __sz = mmUtf16String_Size(p);
        if (__cap - __sz < __n)
        {
            r = mmUtf16String_GrowBy(p, __cap, __sz + __n - __cap, __sz, __sz, 0, 0);
        }
        __p = mmUtf16String_GetPointer(p);
        mmUtf16_Assignn(__p + __sz, __n, __c);
        __sz += __n;
        mmUtf16String_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf16_Assignc(&__p[__sz], 0);
    }
    return r;
}

static void mmUtf16String_Deallocate(struct mmUtf16String* p)
{
    size_t __cap = mmUtf16String_GetLongCapacity(p);
    if (mmUtf16String_IsLong(p) && 0 != __cap)
    {
        // long mode and long capacity equal 0 is weak or const reference.
        mmUtf16_t* __p = mmUtf16String_GetLongPointer(p);
        // __alloc_traits::deallocate(__alloc(), __p, __cap);
        mmUtf16_Deallocate(__p, __cap);
    }
}

MM_EXPORT_DLL void mmUtf16String_Init(struct mmUtf16String* p)
{
    mmMemset(p, 0, sizeof(struct mmUtf16String));
}
MM_EXPORT_DLL void mmUtf16String_Destroy(struct mmUtf16String* p)
{
    mmUtf16String_Deallocate(p);
    mmMemset(p, 0, sizeof(struct mmUtf16String));
}

MM_EXPORT_DLL int mmUtf16String_Assign(struct mmUtf16String* p, const struct mmUtf16String* __str)
{
    const mmUtf16_t* __p = mmUtf16String_GetPointer(__str);
    size_t __sz = mmUtf16String_Size(__str);
    return mmUtf16String_Assignsn(p, __p, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Assigns(struct mmUtf16String* p, const mmUtf16_t* __s)
{
    size_t __sz = mmUtf16Strlen(__s);
    return mmUtf16String_Assignsn(p, __s, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Assignsn(struct mmUtf16String* p, const mmUtf16_t* __s, size_t __n)
{
    size_t __cap = 0;
    assert((__n == 0 || __s != NULL) && "mmUtf16String_Assignsn received nullptr");
    __cap = mmUtf16String_Capacity(p);
    if (__cap >= __n)
    {
        mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        mmUtf16_Move(__p, __s, __n);
        // traits_type::assign(__p[__n], value_type());
        // __p[__n] = 0;
        mmUtf16_Assignc(&__p[__n], 0);
        mmUtf16String_SetSize(p, __n);
        // __invalidate_iterators_past(__n);
        return 0;
    }
    else
    {
        size_t __sz = mmUtf16String_Size(p);
        return mmUtf16String_GrowByAndReplace(p, __cap, __n - __cap, __sz, 0, __sz, __n, __s);
    }
}
MM_EXPORT_DLL int mmUtf16String_Assignnc(struct mmUtf16String* p, size_t __n, char __c)
{
    int r = 0;
    mmUtf16_t* __p = NULL;
    size_t __cap = mmUtf16String_Capacity(p);
    if (__cap < __n)
    {
        size_t __sz = mmUtf16String_Size(p);
        r = mmUtf16String_GrowBy(p, __cap, __n - __cap, __sz, 0, __sz, 0);
    }
    else
    {
        // __invalidate_iterators_past(__n);
    }
    __p = mmUtf16String_GetPointer(p);
    // traits_type::assign(__p, __n, __c);
    mmUtf16_Assignn(__p, __n, __c);
    // traits_type::assign(__p[__n], value_type());
    // __p[__n] = 0;
    mmUtf16_Assignc(&__p[__n], 0);
    mmUtf16String_SetSize(p, __n);
    return r;
}
MM_EXPORT_DLL int mmUtf16String_Insert(struct mmUtf16String* p, size_t __pos, const struct mmUtf16String* __str)
{
    const mmUtf16_t* __p = mmUtf16String_GetPointer(__str);
    size_t __sz = mmUtf16String_Size(__str);
    return mmUtf16String_Insertsn(p, __pos, __p, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Inserts(struct mmUtf16String* p, size_t __pos, const mmUtf16_t* __s)
{
    size_t __sz = mmUtf16Strlen(__s);
    return mmUtf16String_Insertsn(p, __pos, __s, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Insertsn(struct mmUtf16String* p, size_t __pos, const mmUtf16_t* __s, size_t __n)
{
    int r = 0;
    size_t __sz, __cap;
    assert((__n == 0 || __s != NULL) && "mmUtf16String_Insertsn received nullptr");
    __sz = mmUtf16String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf16String_Insertsn out of range error.");
        return EOF;
    }
    __cap = mmUtf16String_Capacity(p);
    if (__cap - __sz >= __n)
    {
        if (__n)
        {
            mmUtf16_t* __p = mmUtf16String_GetPointer(p);
            size_t __n_move = __sz - __pos;
            if (__n_move != 0)
            {
                if (__p + __pos <= __s && __s < __p + __sz)
                {
                    __s += __n;
                }
                mmUtf16_Move(__p + __pos + __n, __p + __pos, __n_move);
            }
            mmUtf16_Move(__p + __pos, __s, __n);
            __sz += __n;
            mmUtf16String_SetSize(p, __sz);
            // traits_type::assign(__p[__sz], value_type());
            // __p[__sz] = 0;
            mmUtf16_Assignc(&__p[__sz], 0);
        }
    }
    else
    {
        r = mmUtf16String_GrowByAndReplace(p, __cap, __sz + __n - __cap, __sz, __pos, 0, __n, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf16String_Insertnc(struct mmUtf16String* p, size_t __pos, size_t __n, mmUtf16_t __c)
{
    int r = 0;
    size_t __sz = mmUtf16String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf16String_Insertnc out of range error.");
        return EOF;
    }
    if (__n)
    {
        size_t __cap = mmUtf16String_Capacity(p);
        mmUtf16_t* __p;
        if (__cap - __sz >= __n)
        {
            size_t __n_move = __sz - __pos;
            __p = mmUtf16String_GetPointer(p);
            if (__n_move != 0)
            {
                mmUtf16_Move(__p + __pos + __n, __p + __pos, __n_move);
            }
        }
        else
        {
            r = mmUtf16String_GrowBy(p, __cap, __sz + __n - __cap, __sz, __pos, 0, __n);
            __p = mmUtf16String_GetLongPointer(p);
        }
        // traits_type::assign(__p + __pos, __n, __c);
        mmUtf16_Assignn(__p + __pos, __n, __c);
        __sz += __n;
        mmUtf16String_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf16_Assignc(&__p[__sz], 0);
    }
    return r;
}

MM_EXPORT_DLL int mmUtf16String_Erase(struct mmUtf16String* p, size_t __pos, size_t __n)
{
    size_t __sz = mmUtf16String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf16String_Erase out of range error.");
        return EOF;
    }
    if (__n)
    {
        mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        size_t __b1 = __n;
        size_t __b2 = __sz - __pos;
        size_t __bn = __b1 < __b2 ? __b1 : __b2;
        size_t __n_move;
        __n = __bn;
        __n_move = __sz - __pos - __n;
        if (__n_move != 0)
        {
            mmUtf16_Move(__p + __pos, __p + __pos + __n, __n_move);
        }
        __sz -= __n;
        mmUtf16String_SetSize(p, __sz);
        // __invalidate_iterators_past(__sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf16_Assignc(&__p[__sz], 0);
    }
    return 0;
}

MM_EXPORT_DLL size_t mmUtf16String_Size(const struct mmUtf16String* p)
{
    return mmUtf16String_IsLong(p) ? mmUtf16String_GetLongSize(p) : mmUtf16String_GetTinySize(p);
}
MM_EXPORT_DLL int mmUtf16String_Resize(struct mmUtf16String* p, size_t __n)
{
    return mmUtf16String_ResizeChar(p, __n, 0);
}
MM_EXPORT_DLL int mmUtf16String_ResizeChar(struct mmUtf16String* p, size_t __n, mmUtf16_t __c)
{
    int r = 0;
    size_t __sz = mmUtf16String_Size(p);
    if (__n > __sz)
    {
        r = mmUtf16String_AppendChar(p, __n - __sz, __c);
    }
    else
    {
        mmUtf16String_EraseToEnd(p, __n);
    }
    return r;
}
MM_EXPORT_DLL size_t mmUtf16String_Capacity(const struct mmUtf16String* p)
{
    return (mmUtf16String_IsLong(p) ? mmUtf16String_GetLongCapacity(p) : (size_t)(mmUtf16StringTinyCapacity)) - 1;
}
MM_EXPORT_DLL int mmUtf16String_Reserve(struct mmUtf16String* p, size_t __res_arg)
{
    size_t __cap, __sz;
    if (__res_arg > mmUtf16String_MaxSize())
    {
        assert(0 && "mmUtf16String_Reserve max size length error.");
        return EOF;
    }
    __cap = mmUtf16String_Capacity(p);
    __sz = mmUtf16String_Size(p);
    // max
    __res_arg = __res_arg > __sz ? __res_arg : __sz;
    __res_arg = mmUtf16String_Recommend(__res_arg);
    if (__res_arg != __cap)
    {
        mmUtf16_t* __new_data; mmUtf16_t* __p;
        mmBool_t __was_long, __now_long;
        if (__res_arg == mmUtf16StringTinyCapacity - 1)
        {
            __was_long = MM_TRUE;
            __now_long = MM_FALSE;
            __new_data = mmUtf16String_GetTinyPointer(p);
            __p = mmUtf16String_GetLongPointer(p);
        }
        else
        {
            __new_data = (mmUtf16_t*)mmUtf16_Allocate(__res_arg + 1);
            if (__new_data == NULL)
            {
                return EOF;
            }
            __now_long = MM_TRUE;
            __was_long = mmUtf16String_IsLong(p);
            __p = mmUtf16String_GetPointer(p);
        }
        mmUtf16_Copy(__new_data, __p, mmUtf16String_Size(p) + 1);
        if (__was_long)
        {
            // __alloc_traits::deallocate(__alloc(), __p, __cap + 1);
            mmUtf16_Deallocate(__p, __cap + 1);
        }
        if (__now_long)
        {
            mmUtf16String_SetLongCapacity(p, __res_arg + 1);
            mmUtf16String_SetLongSize(p, __sz);
            mmUtf16String_SetLongPointer(p, __new_data);
        }
        else
        {
            mmUtf16String_SetTinySize(p, __sz);
        }
        // __invalidate_all_iterators();
    }
    return 0;
}
MM_EXPORT_DLL void mmUtf16String_Clear(struct mmUtf16String* p)
{
    // __invalidate_all_iterators();
    if (mmUtf16String_IsLong(p))
    {
        *mmUtf16String_GetLongPointer(p) = 0;
        mmUtf16String_SetLongSize(p, 0);
    }
    else
    {
        *mmUtf16String_GetTinyPointer(p) = 0;
        mmUtf16String_SetTinySize(p, 0);
    }
}
MM_EXPORT_DLL mmBool_t mmUtf16String_Empty(const struct mmUtf16String* p)
{
    return (0 == mmUtf16String_Size(p));
}
MM_EXPORT_DLL int mmUtf16String_ShrinkToFit(struct mmUtf16String* p)
{
    return mmUtf16String_Reserve(p, 0);
}

MM_EXPORT_DLL mmUtf16_t mmUtf16String_At(const struct mmUtf16String* p, size_t __n)
{
    if (__n >= mmUtf16String_Size(p))
    {
        assert(0 && "mmUtf16String_At out of range error.");
        return 0;
    }
    else
    {
        return *(mmUtf16String_GetPointer(p) + __n);
    }
}

MM_EXPORT_DLL int mmUtf16String_Append(struct mmUtf16String* p, const struct mmUtf16String* __str)
{
    const mmUtf16_t* __p = mmUtf16String_GetPointer(__str);
    size_t __sz = mmUtf16String_Size(__str);
    return mmUtf16String_Appendsn(p, __p, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Appends(struct mmUtf16String* p, const mmUtf16_t* __s)
{
    size_t __sz = mmUtf16Strlen(__s);
    return mmUtf16String_Appendsn(p, __s, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Appendsn(struct mmUtf16String* p, const mmUtf16_t* __s, size_t __n)
{
    int r = 0;
    size_t __cap, __sz;
    assert((__n == 0 || __s != NULL) && "mmUtf16String_Appendsn received nullptr");
    __cap = mmUtf16String_Capacity(p);
    __sz = mmUtf16String_Size(p);
    if (__cap - __sz >= __n)
    {
        if (__n)
        {
            mmUtf16_t* __p = mmUtf16String_GetPointer(p);
            mmUtf16_Copy(__p + __sz, __s, __n);
            __sz += __n;
            mmUtf16String_SetSize(p, __sz);
            // traits_type::assign(__p[__sz], value_type());
            // __p[__sz] = 0;
            mmUtf16_Assignc(&__p[__sz], 0);
        }
    }
    else
    {
        r = mmUtf16String_GrowByAndReplace(p, __cap, __sz + __n - __cap, __sz, __sz, 0, __n, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf16String_Appendnc(struct mmUtf16String* p, size_t __n, mmUtf16_t __c)
{
    int r = 0;
    if (__n)
    {
        mmUtf16_t* __p = NULL;
        size_t __cap = mmUtf16String_Capacity(p);
        size_t __sz = mmUtf16String_Size(p);
        if (__cap - __sz < __n)
        {
            r = mmUtf16String_GrowBy(p, __cap, __sz + __n - __cap, __sz, __sz, 0, 0);
        }
        __p = mmUtf16String_GetPointer(p);
        // traits_type::assign(_VSTD::__to_raw_pointer(__p) + __sz, __n, __c);
        mmUtf16_Assignn(__p + __sz, __n, __c);
        __sz += __n;
        mmUtf16String_SetSize(p, __sz);
        // traits_type::assign(__p[__sz], value_type());
        mmUtf16_Assignc(__p + __sz, 0);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf16String_Appendc(struct mmUtf16String* p, mmUtf16_t __c)
{
    return mmUtf16String_PushBack(p, __c);
}
MM_EXPORT_DLL int mmUtf16String_PushBack(struct mmUtf16String* p, mmUtf16_t __c)
{
    int r = 0;
    mmBool_t __is_short = !mmUtf16String_IsLong(p);
    size_t __cap;
    size_t __sz;
    mmUtf16_t* __p;
    if (__is_short)
    {
        __cap = mmUtf16StringTinyCapacity - 1;
        __sz = mmUtf16String_GetTinySize(p);
    }
    else
    {
        __cap = mmUtf16String_GetLongCapacity(p) - 1;
        __sz = mmUtf16String_GetLongSize(p);
    }
    if (__sz == __cap)
    {
        r = mmUtf16String_GrowBy(p, __cap, 1, __sz, __sz, 0, 0);
        __is_short = !mmUtf16String_IsLong(p);
    }

    if (__is_short)
    {
        __p = mmUtf16String_GetTinyPointer(p) + __sz;
        mmUtf16String_SetTinySize(p, __sz + 1);
    }
    else
    {
        __p = mmUtf16String_GetLongPointer(p) + __sz;
        mmUtf16String_SetLongSize(p, __sz + 1);
    }
    (*__p) = __c;
    (*++__p) = 0;
    return r;
}
MM_EXPORT_DLL void mmUtf16String_PopBack(struct mmUtf16String* p)
{
    size_t __sz;
    assert(!mmUtf16String_Empty(p) && "mmUtf16String_PopBack string is already empty");
    if (mmUtf16String_IsLong(p))
    {
        __sz = mmUtf16String_GetLongSize(p) - 1;
        mmUtf16String_SetLongSize(p, __sz);
        *(mmUtf16String_GetLongPointer(p) + __sz) = 0;
    }
    else
    {
        __sz = mmUtf16String_GetTinySize(p) - 1;
        mmUtf16String_SetTinySize(p, __sz);
        *(mmUtf16String_GetTinyPointer(p) + __sz) = 0;
    }
    // __invalidate_iterators_past(__sz);
}

MM_EXPORT_DLL int mmUtf16String_Replace(struct mmUtf16String* p, size_t __pos, size_t __n1, const struct mmUtf16String* __str)
{
    const mmUtf16_t* __p = mmUtf16String_GetPointer(__str);
    size_t __sz = mmUtf16String_Size(__str);
    return mmUtf16String_Replacesn(p, __pos, __n1, __p, __sz);
}
MM_EXPORT_DLL int mmUtf16String_Replaces(struct mmUtf16String* p, size_t __pos, size_t __n1, const mmUtf16_t* __s)
{
    size_t __sz = mmUtf16Strlen(__s);
    return mmUtf16String_Replacesn(p, __pos, __n1, __s, __sz);
}

MM_EXPORT_DLL int mmUtf16String_Replacesn(struct mmUtf16String* p, size_t __pos, size_t __n1, const mmUtf16_t* __s, size_t __n2)
{
    int r = 0;
    size_t __sz, __b1, __b2, __bn, __cap;
    assert((__n2 == 0 || __s != NULL) && "mmUtf16String_Replace received nullptr");
    __sz = mmUtf16String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmUtf16String_Replacesn out of range error.");
        return EOF;
    }
    __b1 = __n1;
    __b2 = __sz - __pos;
    __bn = __b1 < __b2 ? __b1 : __b2;
    __n1 = __bn;
    __cap = mmUtf16String_Capacity(p);
    if (__cap - __sz + __n1 >= __n2)
    {
        mmUtf16_t* __p = mmUtf16String_GetPointer(p);

        do
        {
            if (__n1 != __n2)
            {
                size_t __n_move = __sz - __pos - __n1;
                if (__n_move != 0)
                {
                    if (__n1 > __n2)
                    {
                        mmUtf16_Move(__p + __pos, __s, __n2);
                        mmUtf16_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
                        break;
                    }
                    if (__p + __pos < __s && __s < __p + __sz)
                    {
                        if (__p + __pos + __n1 <= __s)
                        {
                            __s += __n2 - __n1;
                        }
                        else // __p + __pos < __s < __p + __pos + __n1
                        {
                            mmUtf16_Move(__p + __pos, __s, __n1);
                            __pos += __n1;
                            __s += __n2;
                            __n2 -= __n1;
                            __n1 = 0;
                        }
                    }
                    mmUtf16_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
                }
            }
            mmUtf16_Move(__p + __pos, __s, __n2);
        } while (0);

        // __sz += __n2 - __n1; in this and the below function below can cause unsigned integer overflow,
        // but this is a safe operation, so we disable the check.
        __sz += __n2 - __n1;
        mmUtf16String_SetSize(p, __sz);
        // __invalidate_iterators_past(__sz);
        // traits_type::assign(__p[__sz], value_type());
        // __p[__sz] = 0;
        mmUtf16_Assignc(&__p[__sz], 0);
    }
    else
    {
        r = mmUtf16String_GrowByAndReplace(p, __cap, __sz - __n1 + __n2 - __cap, __sz, __pos, __n1, __n2, __s);
    }
    return r;
}
MM_EXPORT_DLL int mmUtf16String_Replacenc(struct mmUtf16String* p, size_t __pos, size_t __n1, size_t __n2, mmUtf16_t __c)
{
    int r = 0;
    size_t __sz, __b1, __b2, __bn, __cap;
    mmUtf16_t* __p;
    __sz = mmUtf16String_Size(p);
    if (__pos > __sz)
    {
        assert(0 && "mmString_Replacenc out of range error.");
        return EOF;
    }
    __b1 = __n1;
    __b2 = __sz - __pos;
    __bn = __b1 < __b2 ? __b1 : __b2;
    __n1 = __bn;
    __cap = mmUtf16String_Capacity(p);
    if (__cap - __sz + __n1 >= __n2)
    {
        __p = mmUtf16String_GetPointer(p);
        if (__n1 != __n2)
        {
            size_t __n_move = __sz - __pos - __n1;
            if (__n_move != 0)
            {
                mmUtf16_Move(__p + __pos + __n2, __p + __pos + __n1, __n_move);
            }
        }
    }
    else
    {
        r = mmUtf16String_GrowBy(p, __cap, __sz - __n1 + __n2 - __cap, __sz, __pos, __n1, __n2);
        __p = mmUtf16String_GetLongPointer(p);
    }
    // traits_type::assign(__p + __pos, __n2, __c);
    mmUtf16_Assignn(__p + __pos, __n2, __c);
    __sz += __n2 - __n1;
    mmUtf16String_SetSize(p, __sz);
    // __invalidate_iterators_past(__sz);
    // traits_type::assign(__p[__sz], value_type());
    // __p[__sz] = 0;
    mmUtf16_Assignc(&__p[__sz], 0);
    return r;
}

MM_EXPORT_DLL size_t mmUtf16String_ReplaceChar(struct mmUtf16String* p, mmUtf16_t s, mmUtf16_t t)
{
    size_t n = 0;
    size_t __sz = mmUtf16String_Size(p);
    if (0 < __sz)
    {
        size_t index = 0;
        mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        for (index = 0; index < __sz; ++index)
        {
            if (__p[index] == s)
            {
                __p[index] = t;
                n++;
            }
        }
    }
    return n;
}

MM_EXPORT_DLL const mmUtf16_t* mmUtf16String_CStr(const struct mmUtf16String* p)
{
    return mmUtf16String_GetPointer(p);
}
MM_EXPORT_DLL const mmUtf16_t* mmUtf16String_Data(const struct mmUtf16String* p)
{
    return mmUtf16String_GetPointer(p);
}

MM_EXPORT_DLL size_t mmUtf16String_FindLastOf(const struct mmUtf16String* p, mmUtf16_t c)
{
    size_t index = mmUtf16StringNpos;
    size_t __sz = mmUtf16String_Size(p);
    if (0 < __sz)
    {
        const mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        size_t idx = 0;
        for (idx = __sz - 1; mmUtf16StringNpos != idx; --idx)
        {
            if (__p[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmUtf16String_FindFirstOf(const struct mmUtf16String* p, mmUtf16_t c)
{
    size_t index = mmUtf16StringNpos;
    size_t __sz = mmUtf16String_Size(p);
    if (0 < __sz)
    {
        const mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        size_t idx = 0;
        for (idx = 0; idx < __sz; ++idx)
        {
            if (__p[idx] == c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmUtf16String_FindLastNotOf(const struct mmUtf16String* p, mmUtf16_t c)
{
    size_t index = mmUtf16StringNpos;
    size_t __sz = mmUtf16String_Size(p);
    if (0 < __sz)
    {
        const mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        size_t idx = 0;
        for (idx = __sz - 1; mmUtf16StringNpos != idx; --idx)
        {
            if (__p[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}
MM_EXPORT_DLL size_t mmUtf16String_FindFirstNotOf(const struct mmUtf16String* p, mmUtf16_t c)
{
    size_t index = mmUtf16StringNpos;
    size_t __sz = mmUtf16String_Size(p);
    if (0 < __sz)
    {
        const mmUtf16_t* __p = mmUtf16String_GetPointer(p);
        size_t idx = 0;
        for (idx = 0; idx < __sz; ++idx)
        {
            if (__p[idx] != c)
            {
                index = idx;
                break;
            }
        }
    }
    return index;
}

MM_EXPORT_DLL void mmUtf16String_Substr(const struct mmUtf16String* p, struct mmUtf16String* s, size_t o, size_t l)
{
    size_t __sz = mmUtf16String_Size(p);
    const mmUtf16_t* __p = mmUtf16String_GetPointer(p);
    size_t h = __sz - o;
    size_t r = (l > h) ? h : l;
    mmUtf16String_Assignsn(s, &(__p[o]), r);
}

// return 0 is equal.
MM_EXPORT_DLL int mmUtf16String_Compare(const struct mmUtf16String* p, const struct mmUtf16String* __str)
{
    size_t __lhs_sz = mmUtf16String_Size(p);
    const mmUtf16_t* __lhs_pt = mmUtf16String_GetPointer(p);
    size_t __rhs_sz = mmUtf16String_Size(__str);
    const mmUtf16_t* __rhs_pt = mmUtf16String_GetPointer(__str);
    size_t __nm = __lhs_sz < __rhs_sz ? __lhs_sz : __rhs_sz;
    int __result = mmUtf16_compare(__lhs_pt, __rhs_pt, __nm);
    if (__result != 0) { return __result; }
    if (__lhs_sz < __rhs_sz) { return -1; }
    if (__lhs_sz > __rhs_sz) { return 1; }
    return 0;
}
// return 0 is equal.
MM_EXPORT_DLL int mmUtf16String_CompareCStr(const struct mmUtf16String* p, const mmUtf16_t* __s)
{
    struct mmUtf16String __str;
    mmUtf16String_MakeWeaks(&__str, __s);
    return mmUtf16String_Compare(p, &__str);
}

// empty string.
static mmUtf16_t gUtf16StringEmpty[1] = { 0 };
MM_EXPORT_DLL const mmUtf16_t* mmUtf16StringEmpty = gUtf16StringEmpty;

MM_EXPORT_DLL void mmUtf16String_MakeWeak(struct mmUtf16String* p, const struct mmUtf16String* __str)
{
    const mmUtf16_t* __s = mmUtf16String_CStr(__str);
    size_t __n = mmUtf16String_Size(__str);
    mmUtf16String_MakeWeaksn(p, __s, __n);
}

MM_EXPORT_DLL void mmUtf16String_MakeWeaks(struct mmUtf16String* p, const mmUtf16_t* __s)
{
    size_t __n = mmUtf16Strlen(__s);
    mmUtf16String_MakeWeaksn(p, __s, __n);
}

MM_EXPORT_DLL void mmUtf16String_MakeWeaksn(struct mmUtf16String* p, const mmUtf16_t* __s, size_t __n)
{
    // const reference long capacity is 0.
    p->l.capacity = mmUtf16StringLongMask;
    p->l.size = __n;
    p->l.data = (mmUtf16_t*)__s;
}

MM_EXPORT_DLL void mmUtf16String_SetSizeValue(struct mmUtf16String* p, size_t size)
{
    mmUtf16String_SetSize(p, size);
}

MM_EXPORT_DLL void mmUtf16String_Reset(struct mmUtf16String* p)
{
    mmUtf16String_Deallocate(p);
    mmMemset(p, 0, sizeof(struct mmUtf16String));
}
