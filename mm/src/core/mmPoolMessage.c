/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoolMessage.h"
#include "core/mmAlloc.h"

static const size_t MM_POOL_MESSAGE_MAX_TRIM_LENGTH = (4 * MM_POOL_MESSAGE_TRIM_PAGE - 4 + 3) * MM_STREAMBUF_PAGE_SIZE / 4;

// overdraft byte_buffer for encode.
// will pbump streambuf.
MM_EXPORT_DLL mmUInt32_t mmPoolMessage_StreambufByteBufferOverdraft(struct mmStreambuf* p, struct mmByteBuffer* byte_buffer)
{
    mmUInt32_t msg_size = (mmUInt32_t)byte_buffer->length;

    // if the idle put size is lack, we try remove the get buffer.
    mmStreambuf_AlignedMemory(p, msg_size);

    byte_buffer->buffer = p->buff;
    byte_buffer->offset = p->pptr;

    mmStreambuf_Pbump(p, msg_size);
    return (mmUInt32_t)msg_size;
}

// repayment byte_buffer for encode.
// will gbump streambuf.
MM_EXPORT_DLL mmUInt32_t mmPoolMessage_StreambufByteBufferRepayment(struct mmStreambuf* p, struct mmByteBuffer* byte_buffer)
{
    mmUInt32_t msg_size = (mmUInt32_t)(byte_buffer->length);

    mmStreambuf_Gbump(p, msg_size);
    return (mmUInt32_t)msg_size;
}

MM_EXPORT_DLL void mmPoolMessage_ChunkProduce(struct mmStreambuf* p, struct mmPoolMessageElem* v, size_t length)
{
    mmPoolMessageElem_Init(v);
    v->streambuf = p;
    //
    v->byte_buffer.buffer = NULL;
    v->byte_buffer.offset = 0;
    v->byte_buffer.length = length;
    //
    mmPoolMessage_StreambufByteBufferOverdraft(p, &v->byte_buffer);
}
MM_EXPORT_DLL void mmPoolMessage_ChunkRecycle(struct mmStreambuf* p, struct mmPoolMessageElem* v)
{
    mmPoolMessage_StreambufByteBufferRepayment(p, &v->byte_buffer);
    //
    v->streambuf = NULL;
    mmPoolMessageElem_Destroy(v);
}

MM_EXPORT_DLL int mmPoolMessage_ChunkProduceComplete(struct mmStreambuf* p)
{
    return MM_POOL_MESSAGE_MAX_TRIM_LENGTH < p->pptr;
}
MM_EXPORT_DLL int mmPoolMessage_ChunkRecycleComplete(struct mmStreambuf* p)
{
    return MM_POOL_MESSAGE_MAX_TRIM_LENGTH < p->gptr;
}

MM_EXPORT_DLL void mmPoolMessageElem_Init(struct mmPoolMessageElem* p)
{
    p->mid = 0;
    mmByteBuffer_Init(&p->byte_buffer);
    p->streambuf = NULL;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmPoolMessageElem_Destroy(struct mmPoolMessageElem* p)
{
    p->mid = 0;
    mmByteBuffer_Destroy(&p->byte_buffer);
    p->streambuf = NULL;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmPoolMessageElem_Reset(struct mmPoolMessageElem* p)
{
    p->mid = 0;
    mmByteBuffer_Reset(&p->byte_buffer);
    p->streambuf = NULL;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmPoolMessageElem_SetMid(struct mmPoolMessageElem* p, mmUInt32_t mid)
{
    p->mid = mid;
}
MM_EXPORT_DLL void mmPoolMessageElem_SetObject(struct mmPoolMessageElem* p, void* obj)
{
    p->obj = obj;
}
MM_EXPORT_DLL void mmPoolMessageElem_SetBuffer(struct mmPoolMessageElem* p, struct mmByteBuffer* byte_buffer)
{
    mmMemcpy(p->byte_buffer.buffer + p->byte_buffer.offset, byte_buffer->buffer + byte_buffer->offset, byte_buffer->length);
}

MM_EXPORT_DLL void mmPoolMessage_Init(struct mmPoolMessage* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmPoolElement_Init(&p->pool_element);
    mmRbtsetVpt_Init(&p->pool);
    mmRbtsetVpt_Init(&p->used);
    mmRbtsetVpt_Init(&p->idle);
    p->current = NULL;
    p->rate_trim_border = MM_POOL_MESSAGE_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_POOL_MESSAGE_RATE_TRIM_NUMBER_DEFAULT;

    hAllocator.Produce = &mmPoolMessageElem_Init;
    hAllocator.Recycle = &mmPoolMessageElem_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmPoolMessageElem));
    mmPoolElement_SetChunkSize(&p->pool_element, MM_POOL_MESSAGE_CHUNK_SIZE_DEFAULT);
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_DLL void mmPoolMessage_Destroy(struct mmPoolMessage* p)
{
    mmPoolMessage_ClearChunk(p);
    //
    mmPoolElement_Destroy(&p->pool_element);
    mmRbtsetVpt_Destroy(&p->pool);
    mmRbtsetVpt_Destroy(&p->used);
    mmRbtsetVpt_Destroy(&p->idle);
    p->current = NULL;
    p->rate_trim_border = MM_POOL_MESSAGE_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_POOL_MESSAGE_RATE_TRIM_NUMBER_DEFAULT;
}
MM_EXPORT_DLL void mmPoolMessage_Lock(struct mmPoolMessage* p)
{
    mmPoolElement_Lock(&p->pool_element);
}
MM_EXPORT_DLL void mmPoolMessage_Unlock(struct mmPoolMessage* p)
{
    mmPoolElement_Unlock(&p->pool_element);
}
// can not change at pool using.
MM_EXPORT_DLL void mmPoolMessage_SetChunkSize(struct mmPoolMessage* p, size_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool_element, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
// can not change at pool using.
MM_EXPORT_DLL void mmPoolMessage_SetRateTrimBorder(struct mmPoolMessage* p, float rate_trim_border)
{
    p->rate_trim_border = rate_trim_border;
}
// can not change at pool using.
MM_EXPORT_DLL void mmPoolMessage_SetRateTrimNumber(struct mmPoolMessage* p, float rate_trim_number)
{
    p->rate_trim_number = rate_trim_number;
}
MM_EXPORT_DLL struct mmStreambuf* mmPoolMessage_AddChunk(struct mmPoolMessage* p)
{
    struct mmStreambuf* e = (struct mmStreambuf*)mmMalloc(sizeof(struct mmStreambuf));
    mmStreambuf_Init(e);
    // aligned memory.
    mmStreambuf_AlignedMemory(e, MM_POOL_MESSAGE_MAX_TRIM_LENGTH);
    // add chunk to pool set.
    mmRbtsetVpt_Add(&p->pool, e);
    return e;
}
MM_EXPORT_DLL void mmPoolMessage_RmvChunk(struct mmPoolMessage* p, struct mmStreambuf* e)
{
    // rmv chunk to pool set.
    mmRbtsetVpt_Rmv(&p->pool, e);
    mmStreambuf_Destroy(e);
    mmFree(e);
}
MM_EXPORT_DLL void mmPoolMessage_ClearChunk(struct mmPoolMessage* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    struct mmStreambuf* e = NULL;
    //
    n = mmRb_First(&p->pool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        e = (struct mmStreambuf*)(it->k);
        n = mmRb_Next(n);
        mmRbtsetVpt_Erase(&p->pool, it);
        mmStreambuf_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DLL struct mmPoolMessageElem* mmPoolMessage_Produce(struct mmPoolMessage* p, size_t length)
{
    struct mmPoolMessageElem* v = NULL;
    // pool_element produce.
    mmPoolElement_Lock(&p->pool_element);
    v = (struct mmPoolMessageElem*)mmPoolElement_Produce(&p->pool_element);
    mmPoolElement_Unlock(&p->pool_element);
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmPoolMessageElem_Reset(v);
    //
    struct mmStreambuf* e = p->current;
    if (NULL == e)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // min.
        n = mmRb_First(&p->idle.rbt);
        if (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            e = (struct mmStreambuf*)it->k;
            // rmv chunk to idle set.
            mmRbtsetVpt_Rmv(&p->idle, e);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        if (NULL == e)
        {
            // add new chunk.
            e = mmPoolMessage_AddChunk(p);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        // cache the current chunk.
        p->current = e;
    }
    //
    mmPoolMessage_ChunkProduce(e, v, length);
    if (mmPoolMessage_ChunkProduceComplete(e))
    {
        p->current = NULL;
    }
    return v;
}
MM_EXPORT_DLL void mmPoolMessage_Recycle(struct mmPoolMessage* p, struct mmPoolMessageElem* v)
{
    struct mmStreambuf* e = v->streambuf;
    assert(NULL != e && "NULL != e is a invalid.");
    mmPoolMessage_ChunkRecycle(e, v);
    if (mmPoolMessage_ChunkRecycleComplete(e))
    {
        float idle_sz = 0;
        float memb_sz = 0;
        int trim_number = 0;

        // reset the index for reused.
        mmStreambuf_Reset(e);
        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, e);
        // add chunk to idle set.
        mmRbtsetVpt_Add(&p->idle, e);
        // 
        idle_sz = (float)p->idle.size;
        memb_sz = (float)p->pool.size;
        trim_number = (int)(memb_sz * p->rate_trim_number);

        if (idle_sz / memb_sz > p->rate_trim_border)
        {
            int i = 0;
            // 
            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            // max
            n = mmRb_Last(&p->idle.rbt);
            while (NULL != n && i < trim_number)
            {
                it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                e = (struct mmStreambuf*)it->k;
                n = mmRb_Prev(n);
                mmRbtsetVpt_Erase(&p->idle, it);
                mmPoolMessage_RmvChunk(p, e);
                i++;
            }
        }
    }
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmPoolMessageElem_Reset(v);
    // pool_element recycle.
    mmPoolElement_Lock(&p->pool_element);
    mmPoolElement_Recycle(&p->pool_element, v);
    mmPoolElement_Unlock(&p->pool_element);
}

