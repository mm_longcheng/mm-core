/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmParameters_h__
#define __mmParameters_h__

#include "core/mmCore.h"
#include "core/mmArgument.h"

#include "container/mmRbtreeString.h"

#include "core/mmPrefix.h"

#define MM_PARAMETERS_NODE_NAME_LENGTH 64

// --name -n = data
struct mmParametersOption
{
    // --name
    struct mmString name;
    // -n
    struct mmString snap;
    // data
    struct mmString data;
};
MM_EXPORT_DLL void mmParametersOption_Init(struct mmParametersOption* p);
MM_EXPORT_DLL void mmParametersOption_Destroy(struct mmParametersOption* p);
MM_EXPORT_DLL void* mmParametersOption_RbtreeStringVptStrongAlloc(struct mmRbtreeStringVpt* p, const struct mmString* k);
MM_EXPORT_DLL void* mmParametersOption_RbtreeStringVptStrongRelax(struct mmRbtreeStringVpt* p, const struct mmString* k, void* v);

struct mmParameters
{
    // strong ref.
    struct mmRbtreeStringVpt name_options;
    // weak ref.
    struct mmRbtreeStringVpt snap_options;
};
MM_EXPORT_DLL void mmParameters_Init(struct mmParameters* p);
MM_EXPORT_DLL void mmParameters_Destroy(struct mmParameters* p);

MM_EXPORT_DLL void mmParameters_SetNameSnap(struct mmParameters* p, const char* name, const char* snap);

// --name=mm -n=mm
MM_EXPORT_DLL void mmParameters_SetOption(struct mmParameters* p, const char* option);
MM_EXPORT_DLL void mmParameters_SetArgument(struct mmParameters* p, const struct mmArgument* argument);

MM_EXPORT_DLL struct mmParametersOption* mmParameters_GetInstanceOptionByName(struct mmParameters* p, const char* name);
MM_EXPORT_DLL struct mmParametersOption* mmParameters_GetInstanceOptionBySnap(struct mmParameters* p, const char* snap);

// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
MM_EXPORT_DLL void
mmParameters_Server(
    const char* parameters,
    char node[MM_PARAMETERS_NODE_NAME_LENGTH],
    mmUShort_t* port,
    mmUInt32_t* length);

// 192.168.111.123-65535
MM_EXPORT_DLL void
mmParameters_Client(
    const char* parameters,
    char node[MM_PARAMETERS_NODE_NAME_LENGTH],
    mmUShort_t* port);

#include "core/mmSuffix.h"

#endif//__mmParameters_h__
