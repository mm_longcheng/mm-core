/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFrameStats_h__
#define __mmFrameStats_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmClock.h"

#include "core/mmPrefix.h"

enum
{
    // [0x00, 0xFF] 0x40 = 64 . 
    // Very close to the commonly used 60 frame rate.
    mmFrameStatsCacheSize = 0x40,
    mmFrameStatsCacheMask = mmFrameStatsCacheSize - 1,
    mmFrameStatsCacheTime = mmFrameStatsCacheSize * MM_USEC_PER_SEC,
};

#define MM_FRAME_STATS_BASE_FPS 50.0

// Frame rate statistics based on circular queues
struct mmFrameStats
{
    // time clock.
    struct mmClock clock;

    // frame number, reset api can assign 0.
    mmUInt64_t number;

    // average fps.
    double average;

    // current frame interval.
    double interval;

    // frame interval total.
    double total;

    // frame interval cache.
    double cache[mmFrameStatsCacheSize];

    // cache index.
    mmUInt8_t index;
};

MM_EXPORT_DLL void mmFrameStats_Init(struct mmFrameStats* p);
MM_EXPORT_DLL void mmFrameStats_Destroy(struct mmFrameStats* p);

// reset for reuse.
MM_EXPORT_DLL void mmFrameStats_Reset(struct mmFrameStats* p);

// most of time we need update fps_avge when the frequency is update. 
MM_EXPORT_DLL void mmFrameStats_SetAverage(struct mmFrameStats* p, double average);

// update frame interval cache use current average interval.
MM_EXPORT_DLL void mmFrameStats_UpdateCache(struct mmFrameStats* p);

// update use internal clock.
MM_EXPORT_DLL void mmFrameStats_Update(struct mmFrameStats* p);

// update use external clock. interval is microseconds.
MM_EXPORT_DLL void mmFrameStats_UpdateInterval(struct mmFrameStats* p, double interval);

// for debug logger.
MM_EXPORT_DLL void mmFrameStats_LoggerAverage(struct mmFrameStats* p, const char* tag);

#include "core/mmSuffix.h"

#endif//__mmFrameStats_h__
