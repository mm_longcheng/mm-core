/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFrameTimer_h__
#define __mmFrameTimer_h__

#include "core/mmCore.h"
#include "core/mmThread.h"
#include "core/mmString.h"
#include "core/mmTime.h"
#include "core/mmTimer.h"
#include "core/mmTimeval.h"
#include "core/mmTimeCache.h"
#include "core/mmFrameStats.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// frequency default.
#define MM_FRAME_TIMER_FREQUENCY_DEFAULT 30.0f
// frequency factor default.
#define MM_FRAME_TIMER_FREQUENCY_FACTOR_DEFAULT 0.3

// max idle 1 * 1000000 / 5 = 200000
#define MM_FRAME_TIMER_MAX_IDLE_DEFAULT 200000

enum mmFrameTimerBackground_t
{
    MM_FRAME_TIMER_BACKGROUND_PAUSED = 0,
    MM_FRAME_TIMER_BACKGROUND_RUNING = 1,
};

enum mmFrameTimerMotionStatus_t
{
    MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND = 0,
    MM_FRAME_TIMER_MOTION_STATUS_BACKGROUND = 1,
};

// obj      is mmFrameTimer.
// interval is msec interval.
typedef void(*mmFrameTimerUpdateFunc)(void* obj, double interval);

struct mmFrameTimerCallback
{
    mmFrameTimerUpdateFunc Update;
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_DLL void mmFrameTimerCallback_Init(struct mmFrameTimerCallback* p);
MM_EXPORT_DLL void mmFrameTimerCallback_Destroy(struct mmFrameTimerCallback* p);
MM_EXPORT_DLL void mmFrameTimerCallback_Reset(struct mmFrameTimerCallback* p);
MM_EXPORT_DLL void mmFrameTimerCallback_UpdateDefault(void* obj, double interval);

// we adjustment the update frame interval.
//   1. dynamic frequency modulation.
//   2. complement frame.
struct mmFrameTimer
{
    struct mmString name;
    struct mmClock clock;
    struct mmFrameStats status;
    struct mmFrameTimerCallback callback;
    //
    mmAtomic_t locker;

    // flag for mmFrameTimerBackground_t. default is MM_FRAME_TIMER_BACKGROUND_RUNING.
    mmUInt8_t background;
    // flag for active, 1 is active, 0 is deactive, default is 1.
    mmUInt8_t active;
    //
    double frequency_factor;
    //
    double frequency;
    double frequency_dynamic;
    double frequency_l;
    double frequency_r;
    double frame_interval_usec;
    //
    double wait_usec;
    double frame_cost_usec;
};

MM_EXPORT_DLL void mmFrameTimer_Init(struct mmFrameTimer* p);
MM_EXPORT_DLL void mmFrameTimer_Destroy(struct mmFrameTimer* p);

MM_EXPORT_DLL void mmFrameTimer_Reset(struct mmFrameTimer* p);

/* locker order is
*     locker
*/
MM_EXPORT_DLL void mmFrameTimer_Lock(struct mmFrameTimer* p);
MM_EXPORT_DLL void mmFrameTimer_Unlock(struct mmFrameTimer* p);

MM_EXPORT_DLL void mmFrameTimer_SetName(struct mmFrameTimer* p, const char* name);
MM_EXPORT_DLL void mmFrameTimer_SetFrequencyFactor(struct mmFrameTimer* p, double frequency_factor);
MM_EXPORT_DLL void mmFrameTimer_SetFrequency(struct mmFrameTimer* p, double frequency);
MM_EXPORT_DLL void mmFrameTimer_SetBackground(struct mmFrameTimer* p, mmUInt8_t background);
MM_EXPORT_DLL void mmFrameTimer_SetActive(struct mmFrameTimer* p, mmUInt8_t active);
MM_EXPORT_DLL void mmFrameTimer_SetCallback(struct mmFrameTimer* p, const struct mmFrameTimerCallback* callback);
MM_EXPORT_DLL void mmFrameTimer_Update(struct mmFrameTimer* p, double* update_cost_time);
MM_EXPORT_DLL void mmFrameTimer_UpdateImmediately(struct mmFrameTimer* p);
MM_EXPORT_DLL void mmFrameTimer_UpdateMotionStatus(struct mmFrameTimer* p, mmSInt8_t motion_status, double* update_cost_time);

// Single thread frame timer task.
struct mmFrameTask
{
    struct mmFrameTimer frame_timer;
    struct mmClock clock;

    struct timeval ntime;
    struct timespec otime;

    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;

    mmAtomic_t locker;

    double update_cost_time;
    // default is MM_FRAME_TIMER_MAX_IDLE_DEFAULT
    double max_idle_usec;

    double frame_cost_usec;

    int sleep_usec;

    // smooth if we not need timewait.
    int smooth_mode;

    mmULong_t nearby_time;

    // mmFrameTimerMotionStatus_t, default is MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND(0)
    mmSInt8_t motion_status;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;

    // user data.
    void* u;
};
MM_EXPORT_DLL void mmFrameTask_Init(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Destroy(struct mmFrameTask* p);

/* locker order is
 *     pool_element
 *     locker
 */
MM_EXPORT_DLL void mmFrameTask_Lock(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Unlock(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_SetContext(struct mmFrameTask* p, void* u);

MM_EXPORT_DLL void mmFrameTask_SetName(struct mmFrameTask* p, const char* name);
MM_EXPORT_DLL void mmFrameTask_SetFrequencyFactor(struct mmFrameTask* p, double frequency_factor);
MM_EXPORT_DLL void mmFrameTask_SetFrequency(struct mmFrameTask* p, double frequency);
MM_EXPORT_DLL void mmFrameTask_SetBackground(struct mmFrameTask* p, mmUInt8_t background);
MM_EXPORT_DLL void mmFrameTask_SetActive(struct mmFrameTask* p, mmUInt8_t active);
MM_EXPORT_DLL void mmFrameTask_SetSmoothMode(struct mmFrameTask* p, int mode);
MM_EXPORT_DLL void mmFrameTask_SetCallback(struct mmFrameTask* p, const struct mmFrameTimerCallback* callback);

MM_EXPORT_DLL void mmFrameTask_Update(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_UpdateImmediately(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Timewait(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Timewake(struct mmFrameTask* p);

MM_EXPORT_DLL void mmFrameTask_EnterBackground(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_EnterForeground(struct mmFrameTask* p);

MM_EXPORT_DLL void mmFrameTask_Start(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Interrupt(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Shutdown(struct mmFrameTask* p);
MM_EXPORT_DLL void mmFrameTask_Join(struct mmFrameTask* p);

// Single thread frame timer scheduler.
struct mmFrameScheduler
{
    struct mmFrameTimer frame_timer;
    struct mmTimerHeap timer_heap;
    struct mmClock clock;

    struct timeval next_delay;

    struct timeval ntime;
    struct timespec otime;

    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;

    mmAtomic_t locker;

    double update_cost_time;
    // default is MM_FRAME_TIMER_MAX_IDLE_DEFAULT
    double max_idle_usec;

    double frame_cost_usec;

    int sleep_usec;
    
    // smooth if we not need timewait.
    int smooth_mode;

    mmULong_t nearby_time;

    // mmFrameTimerMotionStatus_t, default is MM_FRAME_TIMER_MOTION_STATUS_FOREGROUND(0)
    mmSInt8_t motion_status;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;

    // user data.
    void* u;
};
MM_EXPORT_DLL void mmFrameScheduler_Init(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_Destroy(struct mmFrameScheduler* p);

/* locker order is
*     pool_element
*     locker
*/
MM_EXPORT_DLL void mmFrameScheduler_Lock(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_unlock(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_SetContext(struct mmFrameScheduler* p, void* u);

MM_EXPORT_DLL void mmFrameScheduler_SetName(struct mmFrameScheduler* p, const char* name);
MM_EXPORT_DLL void mmFrameScheduler_SetFrequencyFactor(struct mmFrameScheduler* p, double frequency_factor);
MM_EXPORT_DLL void mmFrameScheduler_SetFrequency(struct mmFrameScheduler* p, double frequency);
MM_EXPORT_DLL void mmFrameScheduler_SetBackground(struct mmFrameScheduler* p, mmUInt8_t background);
MM_EXPORT_DLL void mmFrameScheduler_SetActive(struct mmFrameScheduler* p, mmUInt8_t active);
MM_EXPORT_DLL void mmFrameScheduler_SetSmoothMode(struct mmFrameScheduler* p, int mode);
MM_EXPORT_DLL void mmFrameScheduler_SetCallback(struct mmFrameScheduler* p, const struct mmFrameTimerCallback* callback);

// schedule a timer entry for delay and period.if 0 == period just call once.
MM_EXPORT_DLL struct mmTimerEntry*
mmFrameScheduler_Schedule(
    struct mmFrameScheduler* p,
    mmMSec_t delay,
    mmMSec_t period,
    mmTimerEntryHandleFunc handle,
    void* u);

MM_EXPORT_DLL void mmFrameScheduler_Cancel(struct mmFrameScheduler* p, struct mmTimerEntry* entry);

MM_EXPORT_DLL void mmFrameScheduler_Update(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_UpdateImmediately(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_Timewait(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_Timewake(struct mmFrameScheduler* p);

MM_EXPORT_DLL void mmFrameScheduler_EnterBackground(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_EnterForeground(struct mmFrameScheduler* p);

MM_EXPORT_DLL void mmFrameScheduler_Start(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_Interrupt(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_Shutdown(struct mmFrameScheduler* p);
MM_EXPORT_DLL void mmFrameScheduler_Join(struct mmFrameScheduler* p);

#include "core/mmSuffix.h"

#endif//__mmFrameTimer_h__
