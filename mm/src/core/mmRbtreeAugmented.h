/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
  Red Black Trees
  (C) 1999  Andrea Arcangeli <andrea@suse.de>
  (C) 2002  David Woodhouse <dwmw2@infradead.org>
  (C) 2012  Michel Lespinasse <walken@google.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  linux/include/linux/rbtree_augmented.h
*/

#ifndef __mmRbtreeAugmented_h__
#define __mmRbtreeAugmented_h__

// #include <linux/compiler.h>
// #include <linux/rbtree.h>

#include "core/mmCore.h"

#include "core/mmRbtree.h"

#include "core/mmPrefix.h"

/*
 * Please note - only struct mmRbAugmentCallbacks and the prototypes for
 * mmRb_InsertAugmented() and mmRb_EraseAugmented() are intended to be public.
 * The rest are implementation details you are not expected to depend on.
 *
 * See Documentation/rbtree.txt for documentation and samples.
 */

struct mmRbAugmentCallbacks
{
    void(*propagate)(struct mmRbNode *node, struct mmRbNode *stop);
    void(*copy)(struct mmRbNode *old_node, struct mmRbNode *new_node);
    void(*rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node);
};

MM_EXPORT_DLL extern void __mmRb_InsertAugmented(struct mmRbNode *node, struct mmRbRoot *root,
    void(*augment_rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node));
/*
 * Fixup the mm_rbtree and update the augmented information when rebalancing.
 *
 * On insertion, the user must update the augmented information on the path
 * leading to the inserted node, then call mmRb_LinkNode() as usual and
 * mm_rb_augment_inserted() instead of the usual mmRb_InsertColor() call.
 * If mm_rb_augment_inserted() rebalances the rbtree, it will callback into
 * a user provided function to update the augmented information on the
 * affected subtrees.
 */
static mmInline void
mmRb_InsertAugmented(struct mmRbNode *node, struct mmRbRoot *root,
    const struct mmRbAugmentCallbacks *augment)
{
    __mmRb_InsertAugmented(node, root, augment->rotate);
}

#define MM_RB_DECLARE_CALLBACKS(rbstatic, rbname, rbstruct, rbfield,    \
        rbtype, rbaugmented, rbcompute)                                 \
static mmInline void                                                    \
rbname ## _propagate(struct mmRbNode *rb, struct mmRbNode *stop)        \
{                                                                       \
    while (rb != stop)                                                  \
    {                                                                   \
        rbstruct *node = mmRb_Entry(rb, rbstruct, rbfield);             \
        rbtype augmented = rbcompute(node);                             \
        if (node->rbaugmented == augmented)                             \
            break;                                                      \
        node->rbaugmented = augmented;                                  \
        rb = mmRb_Parent(&node->rbfield);                               \
    }                                                                   \
}                                                                       \
static mmInline void                                                    \
rbname ## _copy(struct mmRbNode *rb_old, struct mmRbNode *rb_new)       \
{                                                                       \
    rbstruct *old_e = mmRb_Entry(rb_old, rbstruct, rbfield);            \
    rbstruct *new_e = mmRb_Entry(rb_new, rbstruct, rbfield);            \
    new_e->rbaugmented = old_e->rbaugmented;                            \
}                                                                       \
static void                                                             \
rbname ## _rotate(struct mmRbNode *rb_old, struct mmRbNode *rb_new)     \
{                                                                       \
    rbstruct *old_e = mmRb_Entry(rb_old, rbstruct, rbfield);            \
    rbstruct *new_e = mmRb_Entry(rb_new, rbstruct, rbfield);            \
    new_e->rbaugmented = old_e->rbaugmented;                            \
    old_e->rbaugmented = rbcompute(old_e);                              \
}                                                                       \
rbstatic const struct mmRbAugmentCallbacks rbname =                     \
{                                                                       \
    rbname ## _propagate, rbname ## _copy, rbname ## _rotate            \
};


#define MM_RB_RED   0
#define MM_RB_BLACK 1

// uintptr_t is type for unsigned long, it is safe.
#define __mmRb_Parent(pc)    ((struct mmRbNode *)((uintptr_t)(pc & ~3)))

#define __mmRb_Color(pc)     ((pc) & 1)
#define __mmRb_IsBlack(pc)  __mmRb_Color(pc)
#define __mmRb_IsRed(pc)    (!__mmRb_Color(pc))
#define mmRb_Color(rb)       __mmRb_Color((rb)->__rb_parent_color)
#define mmRb_IsRed(rb)      __mmRb_IsRed((rb)->__rb_parent_color)
#define mmRb_IsBlack(rb)    __mmRb_IsBlack((rb)->__rb_parent_color)

static mmInline void mmRb_SetParent(struct mmRbNode *rb, struct mmRbNode *p)
{
    rb->__rb_parent_color = mmRb_Color(rb) | (uintptr_t)p;
}

static mmInline void mmRb_SetParentColor(struct mmRbNode *rb,
    struct mmRbNode *p, int color)
{
    rb->__rb_parent_color = (uintptr_t)p | color;
}

static mmInline void
__mmRb_ChangeChild(struct mmRbNode *old_node, struct mmRbNode *new_node,
    struct mmRbNode *parent, struct mmRbRoot *root)
{
    if (parent)
    {
        if (parent->rb_left == old_node)
            parent->rb_left = new_node;
        else
            parent->rb_right = new_node;
    }
    else
        root->rb_node = new_node;
}

MM_EXPORT_DLL extern void __mmRb_EraseColor(struct mmRbNode *parent, struct mmRbRoot *root,
    void(*augment_rotate)(struct mmRbNode *old_node, struct mmRbNode *new_node));

static MM_FORCEINLINE struct mmRbNode *
__mmRb_EraseAugmented(struct mmRbNode *node, struct mmRbRoot *root,
    const struct mmRbAugmentCallbacks *augment)
{
    struct mmRbNode *child = node->rb_right, *tmp = node->rb_left;
    struct mmRbNode *parent, *rebalance;
    uintptr_t pc;

    if (!tmp)
    {
        /*
         * Case 1: node to erase has no more than 1 child (easy!)
         *
         * Note that if there is one child it must be red due to 5)
         * and node must be black due to 4). We adjust colors locally
         * so as to bypass __mmRb_EraseColor() later on.
         */
        pc = node->__rb_parent_color;
        parent = __mmRb_Parent(pc);
        __mmRb_ChangeChild(node, child, parent, root);
        if (child)
        {
            child->__rb_parent_color = pc;
            rebalance = NULL;
        }
        else
            rebalance = __mmRb_IsBlack(pc) ? parent : NULL;
        tmp = parent;
    }
    else if (!child)
    {
        /* Still case 1, but this time the child is node->rb_left */
        tmp->__rb_parent_color = pc = node->__rb_parent_color;
        parent = __mmRb_Parent(pc);
        __mmRb_ChangeChild(node, tmp, parent, root);
        rebalance = NULL;
        tmp = parent;
    }
    else
    {
        struct mmRbNode *successor = child, *child2;
        tmp = child->rb_left;
        if (!tmp)
        {
            /*
             * Case 2: node's successor is its right child
             *
             *    (n)          (s)
             *    / \          / \
             *  (x) (s)  ->  (x) (c)
             *        \
             *        (c)
             */
            parent = successor;
            child2 = successor->rb_right;
            augment->copy(node, successor);
        }
        else
        {
            /*
             * Case 3: node's successor is leftmost under
             * node's right child subtree
             *
             *    (n)          (s)
             *    / \          / \
             *  (x) (y)  ->  (x) (y)
             *      /            /
             *    (p)          (p)
             *    /            /
             *  (s)          (c)
             *    \
             *    (c)
             */
            do
            {
                parent = successor;
                successor = tmp;
                tmp = tmp->rb_left;
            } while (tmp);
            parent->rb_left = child2 = successor->rb_right;
            successor->rb_right = child;
            mmRb_SetParent(child, successor);
            augment->copy(node, successor);
            augment->propagate(parent, successor);
        }

        successor->rb_left = tmp = node->rb_left;
        mmRb_SetParent(tmp, successor);

        pc = node->__rb_parent_color;
        tmp = __mmRb_Parent(pc);
        __mmRb_ChangeChild(node, successor, tmp, root);
        if (child2)
        {
            successor->__rb_parent_color = pc;
            mmRb_SetParentColor(child2, parent, MM_RB_BLACK);
            rebalance = NULL;
        }
        else
        {
            uintptr_t pc2 = successor->__rb_parent_color;
            successor->__rb_parent_color = pc;
            rebalance = __mmRb_IsBlack(pc2) ? parent : NULL;
        }
        tmp = successor;
    }

    augment->propagate(tmp, NULL);
    return rebalance;
}

static MM_FORCEINLINE void
mmRb_EraseAugmented(struct mmRbNode *node, struct mmRbRoot *root,
    const struct mmRbAugmentCallbacks *augment)
{
    struct mmRbNode *rebalance = __mmRb_EraseAugmented(node, root, augment);
    if (rebalance)
        __mmRb_EraseColor(rebalance, root, augment->rotate);
}

#include "core/mmSuffix.h"

#endif//__mmRbtreeAugmented_h__
