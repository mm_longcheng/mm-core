/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCPUInfo_h__
#define __mmCPUInfo_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

/* Initial CPU stuff to set.
*/
#define MM_CPU_UNKNOWN    0
#define MM_CPU_X86        1
#define MM_CPU_PPC        2
#define MM_CPU_ARM        3
#define MM_CPU_MIPS       4

/* Find CPU type
*/
#if (defined(_MSC_VER) && (defined(_M_IX86) || defined(_M_X64))) || \
    (defined(__GNUC__) && (defined(__i386__) || defined(__x86_64__)))
#   define MM_CPU MM_CPU_X86

#elif MM_PLATFORM == MM_PLATFORM_APPLE && defined(__BIG_ENDIAN__)
#   define MM_CPU MM_CPU_PPC
#elif MM_PLATFORM == MM_PLATFORM_APPLE
#   define MM_CPU MM_CPU_X86
#elif MM_PLATFORM == MM_PLATFORM_APPLE_IOS && (defined(__i386__) || defined(__x86_64__))
#   define MM_CPU MM_CPU_X86
#elif defined(__arm__) || defined(_M_ARM) || defined(__arm64__) || defined(__aarch64__)
#   define MM_CPU MM_CPU_ARM
#elif defined(__mips64) || defined(__mips64_)
#   define MM_CPU MM_CPU_MIPS
#else
#   define MM_CPU MM_CPU_UNKNOWN
#endif

/* Find how to declare aligned variable.
*/
#if MM_COMPILER == MM_COMPILER_MSVC
#   define MM_ALIGNED_DECL(type, var, alignment)  __declspec(align(alignment)) type var

#elif (MM_COMPILER == MM_COMPILER_GNUC) || (MM_COMPILER == MM_COMPILER_CLANG)
#   define MM_ALIGNED_DECL(type, var, alignment)  type var __attribute__((__aligned__(alignment)))

#else
#   define MM_ALIGNED_DECL(type, var, alignment)  type var
#endif

/** Find perfect alignment (should supports SIMD alignment if SIMD available)
*/
#if MM_CPU == MM_CPU_X86
#   define MM_SIMD_ALIGNMENT  16

#else
#   define MM_SIMD_ALIGNMENT  16
#endif

/* Declare variable aligned to SIMD alignment.
*/
#define MM_SIMD_ALIGNED_DECL(type, var)   MM_ALIGNED_DECL(type, var, MM_SIMD_ALIGNMENT)

/* Define whether or not MM compiled with SSE supports.
*/
#if   MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_X86 && MM_COMPILER == MM_COMPILER_MSVC && \
    MM_PLATFORM != MM_PLATFORM_NACL
#   define __MM_HAVE_SSE  1
#elif MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_X86 && \
     (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && \
      MM_PLATFORM != MM_PLATFORM_APPLE_IOS && MM_PLATFORM != MM_PLATFORM_NACL
#   define __MM_HAVE_SSE  1
#endif

/* Define whether or not MM compiled with VFP support.
 */
#if MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_ARM && \
   (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && defined(__ARM_ARCH_6K__) && defined(__VFP_FP__)
#   define __MM_HAVE_VFP  1
#endif

 /* Define whether or not MM compiled with NEON support.
  */
#if MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_ARM && \
   (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && defined(__ARM_ARCH_7A__) && defined(__ARM_NEON__)
#   define __MM_HAVE_NEON  1
#endif

  /* Define whether or not MM compiled with MSA support.
   */
#if MM_DOUBLE_PRECISION == 0 && MM_CPU == MM_CPU_MIPS && \
   (MM_COMPILER == MM_COMPILER_GNUC || MM_COMPILER == MM_COMPILER_CLANG) && defined(__mips_msa)
#   define __MM_HAVE_MSA  1
#endif

#ifndef __MM_HAVE_SSE
#   define __MM_HAVE_SSE  0
#endif

#ifndef __MM_HAVE_VFP
#   define __MM_HAVE_VFP  0
#endif

#ifndef __MM_HAVE_NEON
#   define __MM_HAVE_NEON 0
#endif

#ifndef __MM_HAVE_MSA
#   define __MM_HAVE_MSA  0
#endif

// Enum describing the different CPU features we want to check for, platform-dependent
enum mmCpuFeatures
{
#if MM_CPU == MM_CPU_X86
    MM_CPU_FEATURE_SSE           = 1 << 0,
    MM_CPU_FEATURE_SSE2          = 1 << 1,
    MM_CPU_FEATURE_SSE3          = 1 << 2,
    MM_CPU_FEATURE_SSE41         = 1 << 3,
    MM_CPU_FEATURE_SSE42         = 1 << 4,
    MM_CPU_FEATURE_MMX           = 1 << 5,
    MM_CPU_FEATURE_MMXEXT        = 1 << 6,
    MM_CPU_FEATURE_3DNOW         = 1 << 7,
    MM_CPU_FEATURE_3DNOWEXT      = 1 << 8,
    MM_CPU_FEATURE_CMOV          = 1 << 9,
    MM_CPU_FEATURE_TSC           = 1 << 10,
    MM_CPU_FEATURE_INVARIANT_TSC = 1 << 11,
    MM_CPU_FEATURE_FPU           = 1 << 12,
    MM_CPU_FEATURE_PRO           = 1 << 13,
    MM_CPU_FEATURE_HTT           = 1 << 14,
#elif MM_CPU == MM_CPU_ARM          
    MM_CPU_FEATURE_VFP           = 1 << 15,
    MM_CPU_FEATURE_NEON          = 1 << 16,
#elif MM_CPU == MM_CPU_MIPS         
    MM_CPU_FEATURE_MSA           = 1 << 17,
#endif

    MM_CPU_FEATURE_NONE          = 0
};

struct mmLogger;

//---------------------------------------------------------------------
// Detect whether CPU supports CPUID instruction, returns non-zero if supported.
MM_EXPORT_DLL mmBool_t mmCPU_SupportCPUId(void);
//---------------------------------------------------------------------
// Struct for store CPUID instruction result, compiler-independent
//---------------------------------------------------------------------
struct mmCPUIdInfo
{
    // Note: DO NOT CHANGE THE ORDER, some code based on that.
    mmUInt_t _eax;
    mmUInt_t _ebx;
    mmUInt_t _edx;
    mmUInt_t _ecx;
};
// Performs CPUID instruction with 'query', fill the results, and return value of eax.
MM_EXPORT_DLL mmUInt_t mmPerformCPUId(struct mmCPUIdInfo* p, mmUInt_t query);

//---------------------------------------------------------------------
/** Gets a string of the CPU identifier.
@note
    Actual detecting are performs in the first time call to this function,
    and then all future calls with return internal cached value.
*/
MM_EXPORT_DLL const char* mmCPU_Identifier(void);

/** Gets a or-masked of enum CpuFeatures that are supported by the CPU.
@note
    Actual detecting are performs in the first time call to this function,
    and then all future calls with return internal cached value.
*/
MM_EXPORT_DLL mmUInt_t mmCPU_Features(void);

/** Gets whether a specific feature is supported by the CPU.
@note
    Actual detecting are performs in the first time call to this function,
    and then all future calls with return internal cached value.
*/
MM_EXPORT_DLL mmBool_t mmCPU_HasFeature(mmUInt_t feature);

/** Write the CPU information to the passed in Log */
MM_EXPORT_DLL void mmCPU_LoggerInformation(struct mmLogger* logger);
//---------------------------------------------------------------------
struct mmCPUInfo
{
    struct mmString vendor;
    struct mmString brand;
    mmUInt64_t level;
    mmUInt64_t flags;
};
MM_EXPORT_DLL void mmCPUInfo_Init(struct mmCPUInfo* p);
MM_EXPORT_DLL void mmCPUInfo_Destroy(struct mmCPUInfo* p);
//
MM_EXPORT_DLL void mmCPUInfo_Perform(struct mmCPUInfo* p);
//
#include "core/mmSuffix.h"

#endif//__mmCPUInfo_h__
