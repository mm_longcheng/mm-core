/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimeUtils.h"

#include "core/mmTime.h"

MM_EXPORT_DLL void mmGMTime(time_t t, struct tm *tp)
{
    mmInt_t   yday;
    mmUInt_t  n, sec, min, hour, mday, mon, year, wday, days, leap;

    /* the calculation is valid for positive time_t only */

    n = (mmUInt_t)t;

    days = n / 86400;

    /* January 1, 1970 was Thursday */

    wday = (4 + days) % 7;

    n %= 86400;
    hour = n / 3600;
    n %= 3600;
    min = n / 60;
    sec = n % 60;

    /*
     * the algorithm based on Gauss' formula,
     * see src/http/mm_http_parse_time.c
     */

     /* days since March 1, 1 BC */
    days = days - (31 + 28) + 719527;

    /*
     * The "days" should be adjusted to 1 only, however, some March 1st's go
     * to previous year, so we adjust them to 2.  This causes also shift of the
     * last February days to next year, but we catch the case when "yday"
     * becomes negative.
     */

    year = (days + 2) * 400 / (365 * 400 + 100 - 4 + 1);

    yday = days - (365 * year + year / 4 - year / 100 + year / 400);

    if (yday < 0)
    {
        leap = (year % 4 == 0) && (year % 100 || (year % 400 == 0));
        yday = 365 + leap + yday;
        year--;
    }

    /*
     * The empirical formula that maps "yday" to month.
     * There are at least 10 variants, some of them are:
     *     mon = (yday + 31) * 15 / 459
     *     mon = (yday + 31) * 17 / 520
     *     mon = (yday + 31) * 20 / 612
     */

    mon = (yday + 31) * 10 / 306;

    /* the Gauss' formula that evaluates days before the month */

    mday = yday - (367 * mon / 12 - 30) + 1;

    if (yday >= 306)
    {

        year++;
        mon -= 10;

        /*
         * there is no "yday" in Win32 SYSTEMTIME
         *
         * yday -= 306;
         */

    }
    else
    {

        mon += 2;

        /*
         * there is no "yday" in Win32 SYSTEMTIME
         *
         * yday += 31 + 28 + leap;
         */
    }

    tp->mmTm_sec = (mmTm_sec_t)sec;
    tp->mmTm_min = (mmTm_min_t)min;
    tp->mmTm_hour = (mmTm_hour_t)hour;
    tp->mmTm_mday = (mmTm_mday_t)mday;
    tp->mmTm_mon = (mmTm_mon_t)mon;
    tp->mmTm_year = (mmTm_year_t)year;
    tp->mmTm_wday = (mmTm_wday_t)wday;
}

MM_EXPORT_DLL time_t mmGetDurationFromDawnByOffset(time_t _timecode, time_t _offset)
{
    time_t _fx = _timecode + MM_SECONDS_HOUR * 8;
    time_t _dt = _fx - _offset;
    time_t _day = _dt / MM_SECONDS_DAY;
    time_t _cur = _day * MM_SECONDS_DAY;
    time_t _rt = _dt - _cur;
    return _rt;
}
MM_EXPORT_DLL time_t mmGetDurationFromWeek(time_t _timecode)
{
    struct tm _tw;
    time_t _wd;

    mmLibcLocaltime(&_timecode, &_tw);

    _tw.tm_hour = 0;
    _tw.tm_min = 0;
    _tw.tm_sec = 0;
    _tw.tm_mday = _tw.tm_mday - _tw.tm_wday;

    _wd = mktime(&_tw);
    return _timecode - _wd;
}
MM_EXPORT_DLL int mmGetDayFlip(time_t _timecode, time_t _offset)
{
    time_t _fx = _timecode + MM_SECONDS_HOUR * 8;
    time_t _dt = _fx - _offset;// second
    time_t _day = _dt / MM_SECONDS_DAY;
    return (int)_day;
}

// Normalize time year month.
MM_EXPORT_DLL void mmTimeYMNormalize(int y, int m, int* ny, int* nm)
{
    int m0 = (m - 1);
    int y0 = m0 / 12;
    int y1 = m0 - y0 * 12;
    int y2 = (0 == y1 || 1 <= m) ? 0 : 1;
    int y3 = y0 - y2;
    int y4 = y + y3;
    int m1 = (y1 + 12) % 12 + 1;
    (*ny) = y4;
    (*nm) = m1;
}
