/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmByte.h"
#include "core/mmAlloc.h"
#include "core/mmOSSocket.h"
#include "core/mmByteswap.h"

MM_EXPORT_DLL void mmByteBuffer_Init(struct mmByteBuffer* p)
{
    p->length = 0;
    p->offset = 0;
    p->buffer = NULL;
}
MM_EXPORT_DLL void mmByteBuffer_Destroy(struct mmByteBuffer* p)
{
    p->length = 0;
    p->offset = 0;
    p->buffer = NULL;
}
MM_EXPORT_DLL void mmByteBuffer_Reset(struct mmByteBuffer* p)
{
    p->length = 0;
    p->offset = 0;
    p->buffer = NULL;
}
MM_EXPORT_DLL void mmByteBuffer_Malloc(struct mmByteBuffer* p, size_t length)
{
    p->length = length;
    p->offset = 0;
    p->buffer = (mmUInt8_t*)mmMalloc(p->length);
}
MM_EXPORT_DLL void mmByteBuffer_Free(struct mmByteBuffer* p)
{
    mmFree(p->buffer);
    p->length = 0;
    p->offset = 0;
    p->buffer = NULL;
}

// |----l---|----r---|
// |-uint32-|-uint32-|
// use little byte order.
// assemble
MM_EXPORT_DLL mmUInt64_t mmByte_UInt64A(mmUInt32_t l, mmUInt32_t r)
{
    // reuse value.
    mmUInt64_t a = 0;
    l = mmHtoL32((mmUInt32_t)l);
    r = mmHtoL32((mmUInt32_t)r);
    mmMemcpy((mmUInt8_t*)(&a), (mmUInt8_t*)(&l), sizeof(mmUInt32_t));
    mmMemcpy((mmUInt8_t*)(&a) + sizeof(mmUInt32_t), (mmUInt8_t*)(&r), sizeof(mmUInt32_t));
    return a;
}
MM_EXPORT_DLL mmUInt32_t mmByte_UInt64L(mmUInt64_t a)
{
    mmUInt32_t l = 0;
    mmMemcpy((mmUInt8_t*)(&l), (mmUInt8_t*)(&a), sizeof(mmUInt32_t));
    return mmLtoH32((mmUInt32_t)l);
}
MM_EXPORT_DLL mmUInt32_t mmByte_UInt64R(mmUInt64_t a)
{
    mmUInt32_t r = 0;
    mmMemcpy((mmUInt8_t*)(&r), (mmUInt8_t*)(&a) + sizeof(mmUInt32_t), sizeof(mmUInt32_t));
    return mmLtoH32((mmUInt32_t)r);
}
// |----l---|----r---|
// |-uint16-|-uint16-|
// use little byte order.
// assemble
MM_EXPORT_DLL mmUInt32_t mmByte_UInt32A(mmUInt16_t l, mmUInt16_t r)
{
    // reuse value.
    mmUInt32_t a = 0;
    l = mmHtoL16((mmUInt16_t)l);
    r = mmHtoL16((mmUInt16_t)r);
    mmMemcpy((mmUInt8_t*)(&a), (mmUInt8_t*)(&l), sizeof(mmUInt16_t));
    mmMemcpy((mmUInt8_t*)(&a) + sizeof(mmUInt16_t), (mmUInt8_t*)(&r), sizeof(mmUInt16_t));
    return a;
}
MM_EXPORT_DLL mmUInt16_t mmByte_UInt32L(mmUInt32_t a)
{
    mmUInt16_t l = 0;
    mmMemcpy((mmUInt8_t*)(&l), (mmUInt8_t*)(&a), sizeof(mmUInt16_t));
    return mmLtoH16((mmUInt16_t)l);
}
MM_EXPORT_DLL mmUInt16_t mmByte_UInt32R(mmUInt32_t a)
{
    mmUInt16_t r = 0;
    mmMemcpy((mmUInt8_t*)(&r), (mmUInt8_t*)(&a) + sizeof(mmUInt16_t), sizeof(mmUInt16_t));
    return mmLtoH16((mmUInt16_t)r);
}

