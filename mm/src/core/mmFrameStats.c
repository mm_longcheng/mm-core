/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFrameStats.h"
#include "core/mmLogger.h"
#include "core/mmAlloc.h"

MM_EXPORT_DLL void mmFrameStats_Init(struct mmFrameStats* p)
{
    mmClock_Init(&p->clock);
    p->number = 0;
    p->average = 0.0;
    p->interval = 0.0;
    p->total = 0.0;
    mmMemset(p->cache, 0, sizeof(double) * mmFrameStatsCacheSize);
    p->index = 0;

    // first reset the clock.
    mmClock_Reset(&p->clock);

    // first assign the base fps.
    mmFrameStats_SetAverage(p, MM_FRAME_STATS_BASE_FPS);
}
MM_EXPORT_DLL void mmFrameStats_Destroy(struct mmFrameStats* p)
{
    p->index = 0;
    mmMemset(p->cache, 0, sizeof(double) * mmFrameStatsCacheSize);
    p->total = 0.0;
    p->interval = 0.0;
    p->average = 0.0;
    p->number = 0;
    mmClock_Destroy(&p->clock);
}
MM_EXPORT_DLL void mmFrameStats_Reset(struct mmFrameStats* p)
{
    mmClock_Reset(&p->clock);
    p->number = 0;
    p->average = 0.0;
    p->interval = 0.0;
    p->total = 0.0;
    mmMemset(p->cache, 0, sizeof(double) * mmFrameStatsCacheSize);
    p->index = 0;
}

MM_EXPORT_DLL void mmFrameStats_SetAverage(struct mmFrameStats* p, double average)
{
    p->average = average;
    p->interval = (MM_USEC_PER_SEC * 1.0) / p->average;

    mmFrameStats_UpdateCache(p);
}
MM_EXPORT_DLL void mmFrameStats_UpdateCache(struct mmFrameStats* p)
{
    size_t idx = 0;

    // assign interval cache for avge interval.
    // we preset the buffer to a full queue average.
    for (idx = 0; idx < mmFrameStatsCacheSize; ++idx)
    {
        p->cache[idx] = p->interval;
    }

    // calculate total time.
    p->total = p->interval * mmFrameStatsCacheSize;
}
MM_EXPORT_DLL void mmFrameStats_Update(struct mmFrameStats* p)
{
    // calculate current frame interval.
    double interval = (double)(mmClock_Microseconds(&p->clock));

    // update interval.
    mmFrameStats_UpdateInterval(p, interval);

    // reset clock.
    mmClock_Reset(&p->clock);
}
MM_EXPORT_DLL void mmFrameStats_UpdateInterval(struct mmFrameStats* p, double interval)
{
    // cache the last frame interval.
    p->interval = interval;

    // accumulate the frame number.
    p->number++;

    // sub the index cache interval.
    p->total -= p->cache[p->index];
    // insert to back current interval.
    p->cache[p->index] = interval;
    // add the current interval.
    p->total += interval;

    // index++
    p->index++;
    // index %= CacheSize
    p->index &= mmFrameStatsCacheMask;

    // fps value update.
    // if total == 0.0 the fps_avge will be +inf, but it's correct.
    p->average = ((double)mmFrameStatsCacheTime) / p->total;
}
MM_EXPORT_DLL void mmFrameStats_LoggerAverage(struct mmFrameStats* p, const char* tag)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmLogger_LogI(gLogger, "FrameStats: average %lf %s", p->average, tag);
}
