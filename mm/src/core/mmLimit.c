/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLimit.h"

//
MM_EXPORT_DLL const mmUChar_t      MM_CHAR_BIT     = CHAR_BIT;
//
MM_EXPORT_DLL const mmChar_t       MM_CHAR_MAX     = CHAR_MAX;
MM_EXPORT_DLL const mmChar_t       MM_CHAR_MIN     = CHAR_MIN;

MM_EXPORT_DLL const mmSChar_t      MM_SCHAR_MAX    = SCHAR_MAX;
MM_EXPORT_DLL const mmSChar_t      MM_SCHAR_MIN    = SCHAR_MIN;

MM_EXPORT_DLL const mmUChar_t      MM_UCHAR_MAX    = UCHAR_MAX;
MM_EXPORT_DLL const mmUChar_t      MM_UCHAR_MIN    = 0;
//
MM_EXPORT_DLL const mmSShort_t     MM_SSHRT_MAX    = SHRT_MAX;
MM_EXPORT_DLL const mmSShort_t     MM_SSHRT_MIN    = SHRT_MIN;

MM_EXPORT_DLL const mmUShort_t     MM_USHRT_MAX    = USHRT_MAX;
MM_EXPORT_DLL const mmUShort_t     MM_USHRT_MIN    = 0;
//
MM_EXPORT_DLL const mmSInt_t       MM_SINT_MAX     = INT_MAX;
MM_EXPORT_DLL const mmSInt_t       MM_SINT_MIN     = INT_MIN;

MM_EXPORT_DLL const mmUInt_t       MM_UINT_MAX     = UINT_MAX;
MM_EXPORT_DLL const mmUInt_t       MM_UINT_MIN     = 0;
//
MM_EXPORT_DLL const mmSLong_t      MM_SLONG_MAX    = LONG_MAX;
MM_EXPORT_DLL const mmSLong_t      MM_SLONG_MIN    = LONG_MIN;

MM_EXPORT_DLL const mmULong_t      MM_ULONG_MAX    = ULONG_MAX;
MM_EXPORT_DLL const mmULong_t      MM_ULONG_MIN    = 0;
//
MM_EXPORT_DLL const mmSLLong_t     MM_SLLONG_MAX   = LLONG_MAX;
MM_EXPORT_DLL const mmSLLong_t     MM_SLLONG_MIN   = LLONG_MIN;

MM_EXPORT_DLL const mmULLong_t     MM_ULLONG_MAX   = ULLONG_MAX;
MM_EXPORT_DLL const mmULLong_t     MM_ULLONG_MIN   = 0;

MM_EXPORT_DLL const mmSInt8_t      MM_SINT8_MAX    = SCHAR_MAX;
MM_EXPORT_DLL const mmSInt8_t      MM_SINT8_MIN    = SCHAR_MIN;

MM_EXPORT_DLL const mmUInt8_t      MM_UINT8_MAX    = UCHAR_MAX;
MM_EXPORT_DLL const mmUInt8_t      MM_UINT8_MIN    = 0;
//
MM_EXPORT_DLL const mmSInt16_t     MM_SINT16_MAX   = SHRT_MAX;
MM_EXPORT_DLL const mmSInt16_t     MM_SINT16_MIN   = SHRT_MIN;

MM_EXPORT_DLL const mmUInt16_t     MM_UINT16_MAX   = USHRT_MAX;
MM_EXPORT_DLL const mmUInt16_t     MM_UINT16_MIN   = 0;
//
MM_EXPORT_DLL const mmSInt32_t     MM_SINT32_MAX   = INT_MAX;
MM_EXPORT_DLL const mmSInt32_t     MM_SINT32_MIN   = INT_MIN;

MM_EXPORT_DLL const mmUInt32_t     MM_UINT32_MAX   = UINT_MAX;
MM_EXPORT_DLL const mmUInt32_t     MM_UINT32_MIN   = 0;
//
MM_EXPORT_DLL const mmSInt64_t     MM_SINT64_MAX   = LLONG_MAX;
MM_EXPORT_DLL const mmSInt64_t     MM_SINT64_MIN   = LLONG_MIN;

MM_EXPORT_DLL const mmUInt64_t     MM_UINT64_MAX   = ULLONG_MAX;
MM_EXPORT_DLL const mmUInt64_t     MM_UINT64_MIN   = 0;

MM_EXPORT_DLL const mmFloat_t      MM_FLOAT_MAX    = FLT_MAX;
MM_EXPORT_DLL const mmFloat_t      MM_FLOAT_MIN    = FLT_MIN;

MM_EXPORT_DLL const mmDouble_t     MM_DOUBLE_MAX   = DBL_MAX;
MM_EXPORT_DLL const mmDouble_t     MM_DOUBLE_MIN   = DBL_MIN;

MM_EXPORT_DLL const mmSChar_t      MM_SCHAR_ZERO   = 0;
MM_EXPORT_DLL const mmUChar_t      MM_UCHAR_ZERO   = 0;
MM_EXPORT_DLL const mmSShort_t     MM_SSHORT_ZERO  = 0;
MM_EXPORT_DLL const mmUShort_t     MM_USHORT_ZERO  = 0;
MM_EXPORT_DLL const mmSInt_t       MM_SINT_ZERO    = 0;
MM_EXPORT_DLL const mmUInt_t       MM_UINT_ZERO    = 0;
MM_EXPORT_DLL const mmSLong_t      MM_SLONG_ZERO   = 0;
MM_EXPORT_DLL const mmULong_t      MM_ULONG_ZERO   = 0;
MM_EXPORT_DLL const mmSLLong_t     MM_SLLONG_ZERO  = 0;
MM_EXPORT_DLL const mmULLong_t     MM_ULLONG_ZERO  = 0;
MM_EXPORT_DLL const mmSInt32_t     MM_SINT32_ZERO  = 0;
MM_EXPORT_DLL const mmUInt32_t     MM_UINT32_ZERO  = 0;
MM_EXPORT_DLL const mmSInt64_t     MM_SINT64_ZERO  = 0;
MM_EXPORT_DLL const mmUInt64_t     MM_UINT64_ZERO  = 0;
MM_EXPORT_DLL const mmSize_t       MM_SIZE_ZERO    = 0;
MM_EXPORT_DLL const mmSIntptr_t    MM_SINTPTR_ZERO = 0;
MM_EXPORT_DLL const mmUIntptr_t    MM_UINTPTR_ZERO = 0;
MM_EXPORT_DLL const mmFloat_t      MM_FLOAT_ZERO   = 0;
MM_EXPORT_DLL const mmDouble_t     MM_DOUBLE_ZERO  = 0;
MM_EXPORT_DLL const mmInt_t        MM_INT_ZERO     = 0;
