/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCore_h__
#define __mmCore_h__

#include "core/mmConfig.h"

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <limits.h>
#include <setjmp.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>

#include "core/mmPrefix.h"

// warning: Offset of on non-standard-layout type 'Type'.
#define mmOffsetof(st, m) ((size_t)&(((st *)0)->m))

#define mmMemberSizeof(st, m) ((size_t)sizeof(((st *)0)->m))

#define mmContainerOf(ptr, type, member) (type *)((char*)(ptr) - offsetof(type, member))

#define MM_STACK_MAGIC  0xdeadbeef

#define MM_REPEAT_BYTE(x)   ((~0ul / 0xff) * (x))

#define MM_ARRAY_SIZE( a )  ( sizeof( (a) ) / sizeof( (a[0]) ) )

#include "core/mmSuffix.h"
//
#endif//__mmCore_h__
