/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmKeyUsage_h__
#define __mmKeyUsage_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*
 * https://www.usb.org/sites/default/files/hut1_22.pdf
 *
 * HID Usage Tables FOR Universal Serial Bus (USB)
 *
 * Version 1.22
 */
 

/*
 * 10 Keyboard/Keypad Page (0x07)
 *
 * Selectors (Sel)
 * Dynamic Flags (DV)
 */

enum mmKeyUsage
{
    mmKU_None                       = 0x00, /* Usage ID Usage Name                       Usage Type  */
    mmKU_00_00                      = 0x00, /* 00-00    Reserved                                     */
    mmKU_ErrorRollOver              = 0x01, /* 01       Keyboard ErrorRollOver           Sel         */
    mmKU_POSTFail                   = 0x02, /* 02       Keyboard POSTFail                Sel         */
    mmKU_Undefined                  = 0x03, /* 03       Keyboard ErrorUndefined          Sel         */
    mmKU_Keyboard_A                 = 0x04, /* 04       Keyboard a and A                 Sel         */
    mmKU_Keyboard_B                 = 0x05, /* 05       Keyboard b and B                 Sel         */
    mmKU_Keyboard_C                 = 0x06, /* 06       Keyboard c and C                 Sel         */
    mmKU_Keyboard_D                 = 0x07, /* 07       Keyboard d and D                 Sel         */
    mmKU_Keyboard_E                 = 0x08, /* 08       Keyboard e and E                 Sel         */
    mmKU_Keyboard_F                 = 0x09, /* 09       Keyboard f and F                 Sel         */
    mmKU_Keyboard_G                 = 0x0A, /* 0A       Keyboard g and G                 Sel         */
    mmKU_Keyboard_H                 = 0x0B, /* 0B       Keyboard h and H                 Sel         */
    mmKU_Keyboard_I                 = 0x0C, /* 0C       Keyboard i and I                 Sel         */
    mmKU_Keyboard_J                 = 0x0D, /* 0D       Keyboard j and J                 Sel         */
    mmKU_Keyboard_K                 = 0x0E, /* 0E       Keyboard k and K                 Sel         */
    mmKU_Keyboard_L                 = 0x0F, /* 0F       Keyboard l and L                 Sel         */
    mmKU_Keyboard_M                 = 0x10, /* 10       Keyboard m and M                 Sel         */
    mmKU_Keyboard_N                 = 0x11, /* 11       Keyboard n and N                 Sel         */
    mmKU_Keyboard_O                 = 0x12, /* 12       Keyboard o and O                 Sel         */
    mmKU_Keyboard_P                 = 0x13, /* 13       Keyboard p and P                 Sel         */
    mmKU_Keyboard_Q                 = 0x14, /* 14       Keyboard q and Q                 Sel         */
    mmKU_Keyboard_R                 = 0x15, /* 15       Keyboard r and R                 Sel         */
    mmKU_Keyboard_S                 = 0x16, /* 16       Keyboard s and S                 Sel         */
    mmKU_Keyboard_T                 = 0x17, /* 17       Keyboard t and T                 Sel         */
    mmKU_Keyboard_U                 = 0x18, /* 18       Keyboard u and U                 Sel         */
    mmKU_Keyboard_V                 = 0x19, /* 19       Keyboard v and V                 Sel         */
    mmKU_Keyboard_W                 = 0x1A, /* 1A       Keyboard w and W                 Sel         */
    mmKU_Keyboard_X                 = 0x1B, /* 1B       Keyboard x and X                 Sel         */
    mmKU_Keyboard_Y                 = 0x1C, /* 1C       Keyboard y and Y                 Sel         */
    mmKU_Keyboard_Z                 = 0x1D, /* 1D       Keyboard z and Z                 Sel         */
    mmKU_Keyboard_1                 = 0x1E, /* 1E       Keyboard 1 and !                 Sel         */
    mmKU_Keyboard_2                 = 0x1F, /* 1F       Keyboard 2 and @                 Sel         */
    mmKU_Keyboard_3                 = 0x20, /* 20       Keyboard 3 and #                 Sel         */
    mmKU_Keyboard_4                 = 0x21, /* 21       Keyboard 4 and $                 Sel         */
    mmKU_Keyboard_5                 = 0x22, /* 22       Keyboard 5 and %                 Sel         */
    mmKU_Keyboard_6                 = 0x23, /* 23       Keyboard 6 and ∧                 Sel         */
    mmKU_Keyboard_7                 = 0x24, /* 24       Keyboard 7 and &                 Sel         */
    mmKU_Keyboard_8                 = 0x25, /* 25       Keyboard 8 and *                 Sel         */
    mmKU_Keyboard_9                 = 0x26, /* 26       Keyboard 9 and (                 Sel         */
    mmKU_Keyboard_0                 = 0x27, /* 27       Keyboard 0 and )                 Sel         */
    mmKU_Keyboard_ENTER             = 0x28, /* 28       Keyboard Return (ENTER)          Sel         */
    mmKU_Keyboard_ESCAPE            = 0x29, /* 29       Keyboard ESCAPE                  Sel         */
    mmKU_Keyboard_Backspace         = 0x2A, /* 2A       Keyboard DELETE (Backspace)      Sel         */
    mmKU_Keyboard_Tab               = 0x2B, /* 2B       Keyboard Tab                     Sel         */
    mmKU_Keyboard_Spacebar          = 0x2C, /* 2C       Keyboard Spacebar                Sel         */
    mmKU_Keyboard_Underscore        = 0x2D, /* 2D       Keyboard - and (underscore)      Sel         */
    mmKU_Keyboard_Equal             = 0x2E, /* 2E       Keyboard = and +                 Sel         */
    mmKU_Keyboard_LeftBracket       = 0x2F, /* 2F       Keyboard [ and {                 Sel         */
    mmKU_Keyboard_RightBracket      = 0x30, /* 30       Keyboard ] and }                 Sel         */
    mmKU_Keyboard_Backslash         = 0x31, /* 31       Keyboard \and |                  Sel         */
    mmKU_Keyboard_Pound             = 0x32, /* 32       Keyboard Non-US # and ~          Sel         */
    mmKU_Keyboard_Semicolon         = 0x33, /* 33       Keyboard ; and :                 Sel         */
    mmKU_Keyboard_Quote             = 0x34, /* 34       Keyboard ‘ and “                 Sel         */
    mmKU_Keyboard_Grave             = 0x35, /* 35       Keyboard Grave Accent and Tilde  Sel         */
    mmKU_Keyboard_Comma             = 0x36, /* 36       Keyboard , and <                 Sel         */
    mmKU_Keyboard_Period            = 0x37, /* 37       Keyboard . and >                 Sel         */
    mmKU_Keyboard_Slash             = 0x38, /* 38       Keyboard / and ?                 Sel         */
    mmKU_Keyboard_CapsLock          = 0x39, /* 39       Keyboard Caps Lock               Sel         */
    mmKU_Keyboard_F1                = 0x3A, /* 3A       Keyboard F1                      Sel         */
    mmKU_Keyboard_F2                = 0x3B, /* 3B       Keyboard F2                      Sel         */
    mmKU_Keyboard_F3                = 0x3C, /* 3C       Keyboard F3                      Sel         */
    mmKU_Keyboard_F4                = 0x3D, /* 3D       Keyboard F4                      Sel         */
    mmKU_Keyboard_F5                = 0x3E, /* 3E       Keyboard F5                      Sel         */
    mmKU_Keyboard_F6                = 0x3F, /* 3F       Keyboard F6                      Sel         */
    mmKU_Keyboard_F7                = 0x40, /* 40       Keyboard F7                      Sel         */
    mmKU_Keyboard_F8                = 0x41, /* 41       Keyboard F8                      Sel         */
    mmKU_Keyboard_F9                = 0x42, /* 42       Keyboard F9                      Sel         */
    mmKU_Keyboard_F10               = 0x43, /* 43       Keyboard F10                     Sel         */
    mmKU_Keyboard_F11               = 0x44, /* 44       Keyboard F11                     Sel         */
    mmKU_Keyboard_F12               = 0x45, /* 45       Keyboard F12                     Sel         */
    mmKU_Keyboard_PrintScreen       = 0x46, /* 46       Keyboard PrintScreen             Sel         */
    mmKU_Keyboard_ScrollLock        = 0x47, /* 47       Keyboard Scroll Lock             Sel         */
    mmKU_Keyboard_Pause             = 0x48, /* 48       Keyboard Pause                   Sel         */
    mmKU_Keyboard_Insert            = 0x49, /* 49       Keyboard Insert                  Sel         */
    mmKU_Keyboard_Home              = 0x4A, /* 4A       Keyboard Home                    Sel         */
    mmKU_Keyboard_PageUp            = 0x4B, /* 4B       Keyboard PageUp                  Sel         */
    mmKU_Keyboard_DeleteForward     = 0x4C, /* 4C       Keyboard Delete Forward          Sel         */
    mmKU_Keyboard_End               = 0x4D, /* 4D       Keyboard End                     Sel         */
    mmKU_Keyboard_PageDown          = 0x4E, /* 4E       Keyboard PageDown                Sel         */
    mmKU_Keyboard_RightArrow        = 0x4F, /* 4F       Keyboard RightArrow              Sel         */
    mmKU_Keyboard_LeftArrow         = 0x50, /* 50       Keyboard LeftArrow               Sel         */
    mmKU_Keyboard_DownArrow         = 0x51, /* 51       Keyboard DownArrow               Sel         */
    mmKU_Keyboard_UpArrow           = 0x52, /* 52       Keyboard UpArrow                 Sel         */
    mmKU_Keypad_NumLock             = 0x53, /* 53       Keypad Num Lock and Clear        Sel         */
    mmKU_Keypad_Divide              = 0x54, /* 54       Keypad /                         Sel         */
    mmKU_Keypad_Multiply            = 0x55, /* 55       Keypad *                         Sel         */
    mmKU_Keypad_Minus               = 0x56, /* 56       Keypad -                         Sel         */
    mmKU_Keypad_Plus                = 0x57, /* 57       Keypad +                         Sel         */
    mmKU_Keypad_ENTER               = 0x58, /* 58       Keypad ENTER                     Sel         */
    mmKU_Keypad_1                   = 0x59, /* 59       Keypad 1 and End                 Sel         */
    mmKU_Keypad_2                   = 0x5A, /* 5A       Keypad 2 and Down Arrow          Sel         */
    mmKU_Keypad_3                   = 0x5B, /* 5B       Keypad 3 and PageDn              Sel         */
    mmKU_Keypad_4                   = 0x5C, /* 5C       Keypad 4 and Left Arrow          Sel         */
    mmKU_Keypad_5                   = 0x5D, /* 5D       Keypad 5                         Sel         */
    mmKU_Keypad_6                   = 0x5E, /* 5E       Keypad 6 and Right Arrow         Sel         */
    mmKU_Keypad_7                   = 0x5F, /* 5F       Keypad 7 and Home                Sel         */
    mmKU_Keypad_8                   = 0x60, /* 60       Keypad 8 and Up Arrow            Sel         */
    mmKU_Keypad_9                   = 0x61, /* 61       Keypad 9 and PageUp              Sel         */
    mmKU_Keypad_0                   = 0x62, /* 62       Keypad 0 and Insert              Sel         */
    mmKU_Keypad_DecimalPoint        = 0x63, /* 63       Keypad . and Delete              Sel         */
    mmKU_Keypad_Backslash           = 0x64, /* 64       Keyboard Non-US \and |           Sel         */
    mmKU_Keyboard_Application       = 0x65, /* 65       Keyboard Application             Sel         */
    mmKU_Keyboard_Power             = 0x66, /* 66       Keyboard Power                   Sel         */
    mmKU_Keypad_Evaluate            = 0x67, /* 67       Keypad =                         Sel         */
    mmKU_Keyboard_F13               = 0x68, /* 68       Keyboard F13                     Sel         */
    mmKU_Keyboard_F14               = 0x69, /* 69       Keyboard F14                     Sel         */
    mmKU_Keyboard_F15               = 0x6A, /* 6A       Keyboard F15                     Sel         */
    mmKU_Keyboard_F16               = 0x6B, /* 6B       Keyboard F16                     Sel         */
    mmKU_Keyboard_F17               = 0x6C, /* 6C       Keyboard F17                     Sel         */
    mmKU_Keyboard_F18               = 0x6D, /* 6D       Keyboard F18                     Sel         */
    mmKU_Keyboard_F19               = 0x6E, /* 6E       Keyboard F19                     Sel         */
    mmKU_Keyboard_F20               = 0x6F, /* 6F       Keyboard F20                     Sel         */
    mmKU_Keyboard_F21               = 0x70, /* 70       Keyboard F21                     Sel         */
    mmKU_Keyboard_F22               = 0x71, /* 71       Keyboard F22                     Sel         */
    mmKU_Keyboard_F23               = 0x72, /* 72       Keyboard F23                     Sel         */
    mmKU_Keyboard_F24               = 0x73, /* 73       Keyboard F24                     Sel         */
    mmKU_Keyboard_Execute           = 0x74, /* 74       Keyboard Execute                 Sel         */
    mmKU_Keyboard_Help              = 0x75, /* 75       Keyboard Help                    Sel         */
    mmKU_Keyboard_Menu              = 0x76, /* 76       Keyboard Menu                    Sel         */
    mmKU_Keyboard_Select            = 0x77, /* 77       Keyboard Select                  Sel         */
    mmKU_Keyboard_Stop              = 0x78, /* 78       Keyboard Stop                    Sel         */
    mmKU_Keyboard_Again             = 0x79, /* 79       Keyboard Again                   Sel         */
    mmKU_Keyboard_Undo              = 0x7A, /* 7A       Keyboard Undo                    Sel         */
    mmKU_Keyboard_Cut               = 0x7B, /* 7B       Keyboard Cut                     Sel         */
    mmKU_Keyboard_Copy              = 0x7C, /* 7C       Keyboard Copy                    Sel         */
    mmKU_Keyboard_Paste             = 0x7D, /* 7D       Keyboard Paste                   Sel         */
    mmKU_Keyboard_Find              = 0x7E, /* 7E       Keyboard Find                    Sel         */
    mmKU_Keyboard_Mute              = 0x7F, /* 7F       Keyboard Mute                    Sel         */
    mmKU_Keyboard_VolumeUp          = 0x80, /* 80       Keyboard Volume Up               Sel         */
    mmKU_Keyboard_VolumeDown        = 0x81, /* 81       Keyboard Volume Down             Sel         */
    mmKU_Keyboard_LockingCapsLock   = 0x82, /* 82       Keyboard Locking Caps Lock       Sel         */
    mmKU_Keyboard_LockingNumLock    = 0x83, /* 83       Keyboard Locking Num Lock        Sel         */
    mmKU_Keyboard_LockingScrollLock = 0x84, /* 84       Keyboard Locking Scroll Lock     Sel         */
    mmKU_Keypad_Comma               = 0x85, /* 85       Keypad Comma                     Sel         */
    mmKU_Keypad_EqualSign           = 0x86, /* 86       Keypad Equal Sign                Sel         */
    mmKU_Keyboard_International1    = 0x87, /* 87       Keyboard International1          Sel         */
    mmKU_Keyboard_International2    = 0x88, /* 88       Keyboard International2          Sel         */
    mmKU_Keyboard_International3    = 0x89, /* 89       Keyboard International3          Sel         */
    mmKU_Keyboard_International4    = 0x8A, /* 8A       Keyboard International4          Sel         */
    mmKU_Keyboard_International5    = 0x8B, /* 8B       Keyboard International5          Sel         */
    mmKU_Keyboard_International6    = 0x8C, /* 8C       Keyboard International6          Sel         */
    mmKU_Keyboard_International7    = 0x8D, /* 8D       Keyboard International7          Sel         */
    mmKU_Keyboard_International8    = 0x8E, /* 8E       Keyboard International8          Sel         */
    mmKU_Keyboard_International9    = 0x8F, /* 8F       Keyboard International9          Sel         */
    mmKU_Keyboard_LANG1             = 0x90, /* 90       Keyboard LANG1                   Sel         */
    mmKU_Keyboard_LANG2             = 0x91, /* 91       Keyboard LANG2                   Sel         */
    mmKU_Keyboard_LANG3             = 0x92, /* 92       Keyboard LANG3                   Sel         */
    mmKU_Keyboard_LANG4             = 0x93, /* 93       Keyboard LANG4                   Sel         */
    mmKU_Keyboard_LANG5             = 0x94, /* 94       Keyboard LANG5                   Sel         */
    mmKU_Keyboard_LANG6             = 0x95, /* 95       Keyboard LANG6                   Sel         */
    mmKU_Keyboard_LANG7             = 0x96, /* 96       Keyboard LANG7                   Sel         */
    mmKU_Keyboard_LANG8             = 0x97, /* 97       Keyboard LANG8                   Sel         */
    mmKU_Keyboard_LANG9             = 0x98, /* 98       Keyboard LANG9                   Sel         */
    mmKU_Keyboard_AlternateErase    = 0x99, /* 99       Keyboard Alternate Erase         Sel         */
    mmKU_Keyboard_SysReq            = 0x9A, /* 9A       Keyboard SysReq/Attention        Sel         */
    mmKU_Keyboard_Cancel            = 0x9B, /* 9B       Keyboard Cancel                  Sel         */
    mmKU_Keyboard_Clear             = 0x9C, /* 9C       Keyboard Clear                   Sel         */
    mmKU_Keyboard_Prior             = 0x9D, /* 9D       Keyboard Prior                   Sel         */
    mmKU_Keyboard_Return            = 0x9E, /* 9E       Keyboard Return                  Sel         */
    mmKU_Keyboard_Separator         = 0x9F, /* 9F       Keyboard Separator               Sel         */
    mmKU_Keyboard_Out               = 0xA0, /* A0       Keyboard Out                     Sel         */
    mmKU_Keyboard_Oper              = 0xA1, /* A1       Keyboard Oper                    Sel         */
    mmKU_Keyboard_ClearAgain        = 0xA2, /* A2       Keyboard Clear/Again             Sel         */
    mmKU_Keyboard_CrSelProps        = 0xA3, /* A3       Keyboard CrSel/Props             Sel         */
    mmKU_Keyboard_ExSel             = 0xA4, /* A4       Keyboard ExSel                   Sel         */
    mmKU_A5_AF                      = 0x00, /* A5-AF    Reserved                                     */
    mmKU_Keypad_00                  = 0xB0, /* B0       Keypad 00                        Sel         */
    mmKU_Keypad_000                 = 0xB1, /* B1       Keypad 000                       Sel         */
    mmKU_Thousands_Separator        = 0xB2, /* B2       Thousands Separator              Sel         */
    mmKU_Decimal_Separator          = 0xB3, /* B3       Decimal Separator                Sel         */
    mmKU_CurrencyUnit               = 0xB4, /* B4       Currency Unit                    Sel         */
    mmKU_CurrencySubUnit            = 0xB5, /* B5       Currency Sub-unit                Sel         */
    mmKU_Keypad_LeftBracket         = 0xB6, /* B6       Keypad (                         Sel         */
    mmKU_Keypad_RightBracket        = 0xB7, /* B7       Keypad )                         Sel         */
    mmKU_Keypad_LeftBrace           = 0xB8, /* B8       Keypad {                         Sel         */
    mmKU_Keypad_RightBrace          = 0xB9, /* B9       Keypad }                         Sel         */
    mmKU_Keypad_Tab                 = 0xBA, /* BA       Keypad Tab                       Sel         */
    mmKU_Keypad_Backspace           = 0xBB, /* BB       Keypad Backspace                 Sel         */
    mmKU_Keypad_A                   = 0xBC, /* BC       Keypad A                         Sel         */
    mmKU_Keypad_B                   = 0xBD, /* BD       Keypad B                         Sel         */
    mmKU_Keypad_C                   = 0xBE, /* BE       Keypad C                         Sel         */
    mmKU_Keypad_D                   = 0xBF, /* BF       Keypad D                         Sel         */
    mmKU_Keypad_E                   = 0xC0, /* C0       Keypad E                         Sel         */
    mmKU_Keypad_F                   = 0xC1, /* C1       Keypad F                         Sel         */
    mmKU_Keypad_XOR                 = 0xC2, /* C2       Keypad XOR                       Sel         */
    mmKU_Keypad_Pow                 = 0xC3, /* C3       Keypad ∧                         Sel         */
    mmKU_Keypad_Percent             = 0xC4, /* C4       Keypad %                         Sel         */
    mmKU_Keypad_Less                = 0xC5, /* C5       Keypad <                         Sel         */
    mmKU_Keypad_Greater             = 0xC6, /* C6       Keypad >                         Sel         */
    mmKU_Keypad_BitAnd              = 0xC7, /* C7       Keypad &                         Sel         */
    mmKU_Keypad_And                 = 0xC8, /* C8       Keypad &&                        Sel         */
    mmKU_Keypad_BitOr               = 0xC9, /* C9       Keypad |                         Sel         */
    mmKU_Keypad_Or                  = 0xCA, /* CA       Keypad ||                        Sel         */
    mmKU_Keypad_Colon               = 0xCB, /* CB       Keypad :                         Sel         */
    mmKU_Keypad_Hashtag             = 0xCC, /* CC       Keypad #                         Sel         */
    mmKU_Keypad_KeypadSpace         = 0xCD, /* CD       Keypad Space                     Sel         */
    mmKU_Keypad_At                  = 0xCE, /* CE       Keypad @                         Sel         */
    mmKU_Keypad_Exclamation         = 0xCF, /* CF       Keypad !                         Sel         */
    mmKU_Keypad_MemoryStore         = 0xD0, /* D0       Keypad Memory Store              Sel         */
    mmKU_Keypad_MemoryRecall        = 0xD1, /* D1       Keypad Memory Recall             Sel         */
    mmKU_Keypad_MemoryClear         = 0xD2, /* D2       Keypad Memory Clear              Sel         */
    mmKU_Keypad_MemoryAdd           = 0xD3, /* D3       Keypad Memory Add                Sel         */
    mmKU_Keypad_MemorySubtract      = 0xD4, /* D4       Keypad Memory Subtract           Sel         */
    mmKU_Keypad_MemoryMultiply      = 0xD5, /* D5       Keypad Memory Multiply           Sel         */
    mmKU_Keypad_MemoryDivide        = 0xD6, /* D6       Keypad Memory Divide             Sel         */
    mmKU_Keypad_Sign                = 0xD7, /* D7       Keypad +/-                       Sel         */
    mmKU_Keypad_Clear               = 0xD8, /* D8       Keypad Clear                     Sel         */
    mmKU_Keypad_ClearEntry          = 0xD9, /* D9       Keypad Clear Entry               Sel         */
    mmKU_Keypad_Binary              = 0xDA, /* DA       Keypad Binary                    Sel         */
    mmKU_Keypad_Octal               = 0xDB, /* DB       Keypad Octal                     Sel         */
    mmKU_Keypad_Decimal             = 0xDC, /* DC       Keypad Decimal                   Sel         */
    mmKU_Keypad_Hexadecimal         = 0xDD, /* DD       Keypad Hexadecimal               Sel         */
    mmKU_DE_DF                      = 0x00, /* DE-DF    Reserved                                     */
    mmKU_Keyboard_LeftControl       = 0xE0, /* E0       Keyboard LeftControl             DV          */
    mmKU_Keyboard_LeftShift         = 0xE1, /* E1       Keyboard LeftShift               DV          */
    mmKU_Keyboard_LeftAlt           = 0xE2, /* E2       Keyboard LeftAlt                 DV          */
    mmKU_Keyboard_LeftGUI           = 0xE3, /* E3       Keyboard Left GUI                DV          */
    mmKU_Keyboard_RightControl      = 0xE4, /* E4       Keyboard RightControl            DV          */
    mmKU_Keyboard_RightShift        = 0xE5, /* E5       Keyboard RightShift              DV          */
    mmKU_Keyboard_RightAlt          = 0xE6, /* E6       Keyboard RightAlt                DV          */
    mmKU_Keyboard_RightGUI          = 0xE7, /* E7       Keyboard Right GUI               DV          */
    mmKU_E8_FFFF                    = 0x00, /* E8-FFFF  Reserved                                     */
};

#include "core/mmSuffix.h"

#endif// __mmKeyUsage_h__
