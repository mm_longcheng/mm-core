/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */
#include "mmSpinlock.h"
#include "core/mmOSContext.h"
#include "core/mmProcess.h"

MM_EXPORT_DLL void mmSpinlock_LockImpl(mmAtomic_t *lock, mmAtomicInt_t value, mmUInt_t spin)
{

#if (MM_HAVE_ATOMIC_OPS)

    mmUInt_t  i, n;

    mmUInt32_t cores = mmOSContextCPUCoresNumber();

    for (;;)
    {
        if (*lock == 0 && mmAtomicCmpSet(lock, 0, value))
        {
            return;
        }
        if (cores > 1)
        {
            for (n = 1; n < spin; n <<= 1)
            {
                for (i = 0; i < n; i++)
                {
                    mmCpuPause();
                }
                if (*lock == 0 && mmAtomicCmpSet(lock, 0, value))
                {
                    return;
                }
            }
        }

        mmSchedYield();
    }

#else

#if (MM_THREADS)

#error mmSpinlock_LockImpl() or mmAtomicCmpSet() are not defined !

#endif

#endif

}
