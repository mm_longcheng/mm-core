/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmClock.h"
#include "core/mmString.h"
#include "core/mmBit.h"
#include "core/mmOSContext.h"
#include "core/mmCPUInfo.h"
#include "core/mmTimeval.h"

// ogre is default use AffinityMask, but it is low efficiency.
// #define MM_ENABLE_AFFINITYMASK

MM_EXPORT_DLL void mmClock_Init(struct mmClock* p)
{
    p->zero_clock = 0;
    p->start_tick = 0;
    p->last_time = 0;
    p->start_time.QuadPart = 0;
    p->frequency.QuadPart = 0;
    p->timer_mask = 0;
    //
    mmClock_Reset(p);
}
MM_EXPORT_DLL void mmClock_Destroy(struct mmClock* p)
{
    p->zero_clock = 0;
    p->start_tick = 0;
    p->last_time = 0;
    p->start_time.QuadPart = 0;
    p->frequency.QuadPart = 0;
    p->timer_mask = 0;
}
//
MM_EXPORT_DLL mmBool_t mmClock_SetOption(struct mmClock* p, const char* strKey, const void* pValue)
{
#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    if (0 == strcmp(strKey, "QueryAffinityMask"))
    {
        // Telling timer what core to use for a timer read
        DWORD newTimerMask = *(const DWORD*)(pValue);

        // Get the current process core mask
        DWORD_PTR procMask;
        DWORD_PTR sysMask;
        GetProcessAffinityMask(GetCurrentProcess(), &procMask, &sysMask);

        // If new mask is 0, then set to default behavior, otherwise check
        // to make sure new timer core mask overlaps with process core mask
        // and that new timer core mask is a power of 2 (i.e. a single core)
        if ((newTimerMask == 0) ||
            (((newTimerMask & procMask) != 0) && mmIsPowerOf2(newTimerMask)))
        {
            p->timer_mask = newTimerMask;
            return MM_TRUE;
        }
    }
#endif
    return MM_FALSE;
}
//
MM_EXPORT_DLL void mmClock_Reset(struct mmClock* p)
{
#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    // Get the current process core mask
    DWORD_PTR procMask;
    DWORD_PTR sysMask;
    GetProcessAffinityMask(GetCurrentProcess(), &procMask, &sysMask);

    // If procMask is 0, consider there is only one core available
    // (using 0 as procMask will cause an infinite loop below)
    if (procMask == 0)
        procMask = 1;

    // Find the lowest core that this process uses
    if (p->timer_mask == 0)
    {
        p->timer_mask = 1;
        while ((p->timer_mask & procMask) == 0)
        {
            p->timer_mask <<= 1;
        }
    }

    HANDLE thread = GetCurrentThread();

    // Set affinity to the first core
    DWORD_PTR oldMask = SetThreadAffinityMask(thread, p->timer_mask);
#endif

    // Get the constant frequency
    QueryPerformanceFrequency(&p->frequency);

    // Query the timer
    QueryPerformanceCounter(&p->start_time);
    p->start_tick = GetTickCount();

#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    // Reset affinity
    SetThreadAffinityMask(thread, oldMask);
#endif

    p->last_time = 0;
    p->zero_clock = clock();
}
MM_EXPORT_DLL mmUInt64_t mmClock_Milliseconds(struct mmClock* p)
{
    LARGE_INTEGER curTime;

#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    HANDLE thread = GetCurrentThread();

    // Set affinity to the first core
    DWORD_PTR oldMask = SetThreadAffinityMask(thread, p->timer_mask);
#endif

    // Query the timer
    QueryPerformanceCounter(&curTime);

#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    // Reset affinity
    SetThreadAffinityMask(thread, oldMask);
#endif

    LONGLONG newTime = curTime.QuadPart - p->start_time.QuadPart;

    // scale by 1000 for milliseconds
    unsigned long newTicks = (unsigned long)(1000 * newTime / p->frequency.QuadPart);

    // detect and compensate for performance counter leaps
    // (surprisingly common, see Microsoft KB: Q274323)
    unsigned long check = GetTickCount() - p->start_tick;
    signed long msecOff = (signed long)(newTicks - check);
    if (msecOff < -100 || msecOff > 100)
    {
        // We must keep the timer running forward :)
        LONGLONG l = msecOff * p->frequency.QuadPart / 1000;
        LONGLONG r = newTime - p->last_time;
        LONGLONG adjust = l < r ? l : r;
        p->start_time.QuadPart += adjust;
        newTime -= adjust;

        // Re-calculate milliseconds
        newTicks = (unsigned long)(1000 * newTime / p->frequency.QuadPart);
    }

    // Record last time for adjust
    p->last_time = newTime;

    return newTicks;
}
MM_EXPORT_DLL mmUInt64_t mmClock_Microseconds(struct mmClock* p)
{
    LARGE_INTEGER curTime;

#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    HANDLE thread = GetCurrentThread();

    // Set affinity to the first core
    DWORD_PTR oldMask = SetThreadAffinityMask(thread, p->timer_mask);
#endif

    // Query the timer
    QueryPerformanceCounter(&curTime);

#if MM_PLATFORM == MM_PLATFORM_WIN32 && MM_ENABLE_AFFINITYMASK
    // Reset affinity
    SetThreadAffinityMask(thread, oldMask);
#endif

    LONGLONG newTime = curTime.QuadPart - p->start_time.QuadPart;

    // get milliseconds to check against GetTickCount
    unsigned long newTicks = (unsigned long)(1000 * newTime / p->frequency.QuadPart);

    // detect and compensate for performance counter leaps
    // (surprisingly common, see Microsoft KB: Q274323)
    unsigned long check = GetTickCount() - p->start_tick;
    signed long msecOff = (signed long)(newTicks - check);
    if (msecOff < -100 || msecOff > 100)
    {
        // We must keep the timer running forward :)
        LONGLONG l = msecOff * p->frequency.QuadPart / 1000;
        LONGLONG r = newTime - p->last_time;
        LONGLONG adjust = l < r ? l : r;
        p->start_time.QuadPart += adjust;
        newTime -= adjust;
    }

    // Record last time for adjust
    p->last_time = newTime;

    // scale by 1000000 for microseconds
    mmUInt64_t newMicro = (mmUInt64_t)(1000000 * newTime / p->frequency.QuadPart);

    return newMicro;
}
MM_EXPORT_DLL mmUInt64_t mmClock_MillisecondsCpu(struct mmClock* p)
{
    clock_t newClock = clock();
    return (mmUInt64_t)((float)(newClock - p->zero_clock) / ((float)CLOCKS_PER_SEC / 1000.0));
}
MM_EXPORT_DLL mmUInt64_t mmClock_MicrosecondsCpu(struct mmClock* p)
{
    clock_t newClock = clock();
    return (mmUInt64_t)((float)(newClock - p->zero_clock) / ((float)CLOCKS_PER_SEC / 1000000.0));
}
