/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMmap_h__
#define __mmMmap_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "core/mmPrefix.h"

#define MM_MMAP_MAP_FAILED NULL

#define MM_MMAP_RDONLY 0x00000000
#define MM_MMAP_WRONLY 0x00000001
#define MM_MMAP_RDWR   0x00000002

#define MM_MMAP_CREAT  0x00000100
#define MM_MMAP_TRUNC  0x00001000

struct mmMmap
{
    struct mmString path;
    void* hfd;
    void* hmmap;
    mmUInt8_t* buffer;
    off_t offset;
    mmUInt64_t length;
    int flag;
};
MM_EXPORT_DLL void mmMmap_Init(struct mmMmap* p);
MM_EXPORT_DLL void mmMmap_Destroy(struct mmMmap* p);
MM_EXPORT_DLL void mmMmap_SetPath(struct mmMmap* p, const char* path);
MM_EXPORT_DLL void mmMmap_SetOffset(struct mmMmap* p, off_t offset);
MM_EXPORT_DLL void mmMmap_SetLength(struct mmMmap* p, mmUInt64_t length);
MM_EXPORT_DLL void mmMmap_SetFlag(struct mmMmap* p, int flag);
MM_EXPORT_DLL mmUInt32_t mmMmap_Fopen(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_Fclose(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_Fseek(struct mmMmap* p, mmUInt64_t length);
MM_EXPORT_DLL mmUInt32_t mmMmap_ObtainFilesize(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_MappingFile(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_UnmappingFile(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_MappingView(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_UnmappingView(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_Mapping(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_Unmapping(struct mmMmap* p);
MM_EXPORT_DLL mmUInt32_t mmMmap_Flush(struct mmMmap* p, void* ptr, size_t size);
MM_EXPORT_DLL void mmMmap_Memcpy(struct mmMmap* p, struct mmMmap* t, size_t offset, size_t length);

#include "core/mmSuffix.h"

#endif//__mmMmap_h__