/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMmap.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmErrno.h"
#include "core/mmFileSystem.h"

MM_EXPORT_DLL void mmMmap_Init(struct mmMmap* p)
{
    mmString_Init(&p->path);
    p->hfd = MM_INVALID_HANDLE_VALUE;
    p->hmmap = MM_MMAP_MAP_FAILED;
    p->buffer = NULL;
    p->offset = 0;
    p->length = 0;
    p->flag = MM_MMAP_RDONLY;
}
MM_EXPORT_DLL void mmMmap_Destroy(struct mmMmap* p)
{
    mmMmap_UnmappingView(p);
    mmMmap_UnmappingFile(p);
    mmMmap_Fclose(p);
    //
    mmString_Destroy(&p->path);
    p->hfd = MM_INVALID_HANDLE_VALUE;
    p->hmmap = MM_MMAP_MAP_FAILED;
    p->buffer = NULL;
    p->offset = 0;
    p->length = 0;
    p->flag = MM_MMAP_RDONLY;
}
MM_EXPORT_DLL void mmMmap_SetPath(struct mmMmap* p, const char* path)
{
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_DLL void mmMmap_SetOffset(struct mmMmap* p, off_t offset)
{
    p->offset = offset;
}
MM_EXPORT_DLL void mmMmap_SetLength(struct mmMmap* p, mmUInt64_t length)
{
    p->length = length;
}
MM_EXPORT_DLL void mmMmap_SetFlag(struct mmMmap* p, int flag)
{
    p->flag = flag;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Fopen(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        DWORD dwCreationDisposition = OPEN_EXISTING;
        DWORD dwDesiredAccess = GENERIC_READ;
        mmUInt32_t flag_access = p->flag & 0x00000003;

        if ((MM_MMAP_CREAT & p->flag)) { dwCreationDisposition = OPEN_ALWAYS; }
        if ((MM_MMAP_TRUNC & p->flag)) { dwCreationDisposition = TRUNCATE_EXISTING; }

        if (MM_MMAP_RDONLY == flag_access) { dwDesiredAccess |= GENERIC_READ; }
        if (MM_MMAP_WRONLY == flag_access) { dwDesiredAccess |= GENERIC_WRITE; }
        if (MM_MMAP_RDWR == flag_access) { dwDesiredAccess |= (GENERIC_READ | GENERIC_WRITE); }

        assert(MM_INVALID_HANDLE_VALUE == p->hfd && "unmapping before mapping.");

        if (mmString_Empty(&p->path))
        {
            // file path is empty.
            code = MM_FAILURE;
            break;
        }

        p->hfd = CreateFile(
            mmString_CStr(&p->path),
            dwDesiredAccess,
            FILE_SHARE_READ | FILE_SHARE_WRITE,
            NULL,
            dwCreationDisposition,
            FILE_ATTRIBUTE_NORMAL,
            NULL);

        if (MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // CreateFile by file path is failure.
            code = MM_FAILURE;
            break;
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Fclose(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    if (MM_INVALID_HANDLE_VALUE != p->hfd)
    {
        BOOL bt = FALSE;
        bt = CloseHandle(p->hfd);
        p->hfd = MM_INVALID_HANDLE_VALUE;
        code = bt ? MM_SUCCESS : MM_FAILURE;
    }
    else
    {
        code = MM_SUCCESS;
    }

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Fseek(struct mmMmap* p, mmUInt64_t length)
{
    mmUInt32_t code = MM_UNKNOWN;

    BOOL bt = FALSE;
    LARGE_INTEGER sz;
    sz.QuadPart = (LONGLONG)length;
    bt = SetFilePointerEx(p->hfd, sz, &sz, FILE_BEGIN);
    code = bt ? MM_SUCCESS : MM_FAILURE;

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_ObtainFilesize(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        LARGE_INTEGER sz;

        if (MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // file is not open.
            code = MM_FAILURE;
            break;
        }

        if (!GetFileSizeEx(p->hfd, &sz))
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // the file descriptor is can not get size.
            code = MM_FAILURE;
            break;
        }
        p->length = (mmUInt64_t)sz.QuadPart;

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_MappingFile(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        DWORD dwFileOffsetH = 0;
        DWORD dwFileOffsetL = 0;
        DWORD flProtect = PAGE_NOACCESS;
        mmUInt32_t flag_access = p->flag & 0x00000003;

        if (MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // file is not open.
            code = MM_FAILURE;
            break;
        }
        assert(MM_MMAP_MAP_FAILED == p->hmmap && "unmapping before mapping.");

        dwFileOffsetH = (DWORD)((mmUInt64_t)p->length >> 32);
        dwFileOffsetL = (DWORD)(p->length & 0xffffffff);

        if (MM_MMAP_RDONLY == flag_access) { flProtect = PAGE_READONLY; }
        if (MM_MMAP_WRONLY == flag_access) { flProtect = PAGE_READWRITE; }
        if (MM_MMAP_RDWR == flag_access) { flProtect = PAGE_READWRITE; }

        p->hmmap = CreateFileMapping(
            p->hfd,
            NULL,
            flProtect,
            (DWORD)dwFileOffsetH,
            (DWORD)dwFileOffsetL,
            NULL);

        if (MM_MMAP_MAP_FAILED == p->hmmap)
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // the file descriptor mapping failure.
            code = MM_FAILURE;
            break;
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_UnmappingFile(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    if (MM_MMAP_MAP_FAILED != p->hmmap)
    {
        BOOL bt = FALSE;
        bt = CloseHandle(p->hmmap);
        p->hmmap = MM_MMAP_MAP_FAILED;
        code = bt ? MM_SUCCESS : MM_FAILURE;
    }
    else
    {
        code = MM_SUCCESS;
    }

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_MappingView(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        DWORD dwFileOffsetH = 0;
        DWORD dwFileOffsetL = 0;
        DWORD dwDesiredAccess = FILE_MAP_READ;
        mmUInt32_t flag_access = p->flag & 0x00000003;

        if (MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // file is not open.
            code = MM_FAILURE;
            break;
        }
        if (MM_MMAP_MAP_FAILED == p->hmmap)
        {
            // file is not mapping.
            code = MM_FAILURE;
            break;
        }

        dwFileOffsetH = (DWORD)((mmUInt64_t)p->offset >> 32);
        dwFileOffsetL = (DWORD)(p->offset & 0xffffffff);

        if (MM_MMAP_RDONLY == flag_access) { dwDesiredAccess = FILE_MAP_READ; }
        if (MM_MMAP_WRONLY == flag_access) { dwDesiredAccess = FILE_MAP_WRITE; }
        if (MM_MMAP_RDWR == flag_access) { dwDesiredAccess = (FILE_MAP_READ | FILE_MAP_WRITE); }

        p->buffer = (mmUInt8_t*)MapViewOfFile(p->hmmap, dwDesiredAccess, dwFileOffsetH, dwFileOffsetL, (size_t)p->length);

        if (NULL == p->buffer)
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // the file descriptor mapping failure.
            code = MM_FAILURE;
            break;
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_UnmappingView(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    if (NULL != p->buffer)
    {
        BOOL bt = FALSE;
        bt = UnmapViewOfFile(p->buffer);
        p->buffer = NULL;
        code = bt ? MM_SUCCESS : MM_FAILURE;
    }
    else
    {
        code = MM_SUCCESS;
    }

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Mapping(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        code = mmMmap_Fopen(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_ObtainFilesize(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_MappingFile(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_MappingView(p);
        if (MM_SUCCESS != code) { break; }
        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Unmapping(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        code = mmMmap_UnmappingView(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_UnmappingFile(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_Fclose(p);
        if (MM_SUCCESS != code) { break; }
        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Flush(struct mmMmap* p, void* ptr, size_t size)
{
    mmUInt32_t code = MM_UNKNOWN;

    BOOL bt = FALSE;
    bt = FlushViewOfFile(ptr, size);
    code = bt ? MM_SUCCESS : MM_FAILURE;

    return code;
}
MM_EXPORT_DLL void mmMmap_Memcpy(struct mmMmap* p, struct mmMmap* t, size_t offset, size_t length)
{
    mmMemcpy(t->buffer + offset, p->buffer + offset, length);
}