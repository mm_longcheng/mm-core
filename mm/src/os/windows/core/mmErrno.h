/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmErrno_h__
#define __mmErrno_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

typedef DWORD                      mmErr_t;

#define mmErrno                  GetLastError()
#define mmSetErrno(err)         SetLastError(err)
#define mmSocketErrno           WSAGetLastError()
#define mmSetSocketErrno(err)  WSASetLastError(err)

#define MM_EPERM                  ERROR_ACCESS_DENIED
#define MM_ENOENT                 ERROR_FILE_NOT_FOUND
#define MM_ENOPATH                ERROR_PATH_NOT_FOUND
#define MM_ENOMEM                 ERROR_NOT_ENOUGH_MEMORY
#define MM_EACCES                 ERROR_ACCESS_DENIED
/* it's seems that ERROR_FILE_EXISTS is not appropriate error code */
#define MM_EEXIST                 ERROR_ALREADY_EXISTS
/*
 * could not found cross volume directory move error code,
 * so use ERROR_WRONG_DISK as stub one
 */
#define MM_EXDEV                  ERROR_WRONG_DISK
#define MM_ENOTDIR                ERROR_PATH_NOT_FOUND
#define MM_EISDIR                 ERROR_CANNOT_MAKE
#define MM_ENOSPC                 ERROR_DISK_FULL
#define MM_EPIPE                  EPIPE
#define MM_EINTR                  WSAEINTR
#define MM_EAGAIN                 WSAEWOULDBLOCK
#define MM_EINPROGRESS            WSAEINPROGRESS
#define MM_ENOPROTOOPT            WSAENOPROTOOPT
#define MM_EOPNOTSUPP             WSAEOPNOTSUPP
#define MM_EADDRINUSE             WSAEADDRINUSE
#define MM_ECONNABORTED           WSAECONNABORTED
#define MM_ECONNRESET             WSAECONNRESET
#define MM_ENOTCONN               WSAENOTCONN
#define MM_ENOTSOCK               WSAENOTSOCK
#define MM_ENOBUFS                WSAENOBUFS
#define MM_ETIMEDOUT              WSAETIMEDOUT
#define MM_ECONNREFUSED           WSAECONNREFUSED
#define MM_ENAMETOOLONG           ERROR_BAD_PATHNAME
#define MM_ENETDOWN               WSAENETDOWN
#define MM_ENETUNREACH            WSAENETUNREACH
#define MM_EHOSTDOWN              WSAEHOSTDOWN
#define MM_EHOSTUNREACH           WSAEHOSTUNREACH
#define MM_ENOMOREFILES           ERROR_NO_MORE_FILES
#define MM_EILSEQ                 ERROR_NO_UNICODE_TRANSLATION
#define MM_ELOOP                  0
#define MM_EBADF                  WSAEBADF

#define MM_EALREADY               WSAEALREADY
#define MM_EINVAL                 WSAEINVAL
#define MM_EMFILE                 WSAEMFILE
#define MM_ENFILE                 WSAEMFILE


MM_EXPORT_DLL mmUChar_t *mmStrError(mmErr_t err, mmUChar_t *errstr, size_t size);
MM_EXPORT_DLL mmInt_t mmStrError_Init(void);
MM_EXPORT_DLL mmInt_t mmStrError_Destroy(void);

#include "core/mmSuffix.h"

#endif//__mmErrno_h__
