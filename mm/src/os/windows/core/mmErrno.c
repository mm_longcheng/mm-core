/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmErrno.h"
#include "core/mmString.h"
#include "core/mmAlloc.h"
#include "container/mmRbtreeU32.h"

static struct mmRbtreeU32Vpt gSysErrList;

static void __static_SysErrListClear(struct mmRbtreeU32Vpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmString* e = NULL;
    n = mmRb_First(&gSysErrList.rbt);
    while (n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmString*)(it->v);
        mmString_Destroy(e);
        mmFree(e);
    }
}

static mmUChar_t* __static_StrError(mmErr_t err, mmUChar_t* errstr, size_t size)
{
    mmUInt_t          len = 0;
    static mmULong_t  lang = MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US);

    if (size == 0)
    {
        return errstr;
    }

    len = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, lang, (char *)errstr, (mmLong_t)size, NULL);

    if (len == 0 && lang && GetLastError() == ERROR_RESOURCE_LANG_NOT_FOUND)
    {

        /*
         * Try to use English messages first and fallback to a language,
         * based on locale: non-English Windows have no English messages
         * at all.  This way allows to use English messages at least on
         * Windows with MUI.
         */

        lang = 0;

        len = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, lang, (char *)errstr, (mmLong_t)size, NULL);
    }

    if (len == 0)
    {
        mmSnprintf((char*)errstr, size - 1, "FormatMessage() error:(%d)", GetLastError());
        return errstr;
    }

    /* remove ".\r\n\0" */
    while (errstr[len] == '\0' || errstr[len] == MM_CR || errstr[len] == MM_LF || errstr[len] == '.')
    {
        errstr[len] = '\0';
        --len;
    }

    return &errstr[++len];
}

MM_EXPORT_DLL mmUChar_t* mmStrError(mmErr_t err, mmUChar_t* errstr, size_t size)
{
    struct mmString* err_string = (struct mmString*)mmRbtreeU32Vpt_Get(&gSysErrList, err);
    if (NULL == err_string)
    {
        err_string = (struct mmString*)mmMalloc(sizeof(struct mmString));
        mmString_Init(err_string);
        __static_StrError(err, errstr, size);
        mmString_Assigns(err_string, (char*)errstr);
        mmRbtreeU32Vpt_Set(&gSysErrList, err, err_string);
    }
    else
    {
        mmMemcpy(errstr, mmString_Data(err_string), mmString_Size(err_string));
    }
    return errstr;
}


MM_EXPORT_DLL mmInt_t mmStrError_Init(void)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmRbtreeU32Vpt_Init(&gSysErrList);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&gSysErrList, &_U32VptAllocator);

    return MM_OK;
}
MM_EXPORT_DLL mmInt_t mmStrError_Destroy(void)
{
    // clear first.
    __static_SysErrListClear(&gSysErrList);
    //
    mmRbtreeU32Vpt_Destroy(&gSysErrList);
    return MM_OK;
}
