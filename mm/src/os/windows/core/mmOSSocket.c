/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOSSocket.h"
#include "core/mmLogger.h"
#include <Mstcpip.h>

MM_EXPORT_DLL int mmNonblocking(mmSocket_t s)
{
    unsigned long  nb = 1;

    return ioctlsocket(s, FIONBIO, &nb);
}


MM_EXPORT_DLL int mmBlocking(mmSocket_t s)
{
    unsigned long  nb = 0;

    return ioctlsocket(s, FIONBIO, &nb);
}


MM_EXPORT_DLL int mmTcpPush(mmSocket_t s)
{
    return 0;
}

MM_EXPORT_DLL int mmSocketKeepalive(mmSocket_t s, int flag)
{
    // at windows BOOL int 0 FALSE 1 TRUE same for our define.
    if (SOCKET_ERROR == setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, (char*)&flag, sizeof(flag)))
    {
        mmErr_t errcode = mmErrno;
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        mmLogger_LogE(gLogger, "%s %d can not keepalive fd:%d", __FUNCTION__, __LINE__, s);
        return MM_ERROR;
    }
    return MM_OK;
}

MM_EXPORT_DLL int mmSocketKeepaliveCtl(mmSocket_t s, int tcp_keepidle, int tcp_keepintvl, int tcp_keepcnt)
{
    // set KeepAlive parameter  
    struct tcp_keepalive alive_in;
    struct tcp_keepalive alive_out;
    unsigned long _return = 0;
    alive_in.keepalivetime = tcp_keepidle;
    alive_in.keepaliveinterval = tcp_keepintvl;
    alive_in.onoff = TRUE;

    if (SOCKET_ERROR == WSAIoctl(s, SIO_KEEPALIVE_VALS, &alive_in, sizeof(alive_in),
        &alive_out, sizeof(alive_out), &_return, NULL, NULL))
    {
        mmErr_t errcode = mmErrno;
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        mmLogger_LogE(gLogger, "%s %d can not keepalive ctl fd:%d", __FUNCTION__, __LINE__, s);
        return MM_ERROR;
    }
    return MM_OK;
}