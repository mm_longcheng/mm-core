/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmClock_h__
#define __mmClock_h__

#include "core/mmConfig.h"
#include "core/mmTime.h"

#include "core/mmPrefix.h"

// come from Ogre clock.

struct mmClock
{
    clock_t zero_clock;

    DWORD start_tick;
    LONGLONG last_time;
    LARGE_INTEGER start_time;
    LARGE_INTEGER frequency;

    DWORD_PTR timer_mask;
};
//
MM_EXPORT_DLL void mmClock_Init(struct mmClock* p);
MM_EXPORT_DLL void mmClock_Destroy(struct mmClock* p);

/** Method for setting a specific option of the Timer. These options are usually
    specific for a certain implementation of the Timer class, and may (and probably
    will) not exist across different implementations.  reset() must be called after
    all setOption() calls.
    @par
    Current options supported are:
    <ul><li>"QueryAffinityMask" (DWORD): Set the thread affinity mask to be used
    to check the timer. If 'reset' has been called already this mask should
    overlap with the process mask that was in force at that point, and should
    be a power of two (a single core).</li></ul>
    @param
        strKey The name of the option to set
    @param
        pValue A pointer to the value - the size should be calculated by the timer
        based on the key
    @return
        On success, true is returned.
    @par
        On failure, false is returned.
*/
MM_EXPORT_DLL mmBool_t mmClock_SetOption(struct mmClock* p, const char* strKey, const void* pValue);

/** Resets timer */
MM_EXPORT_DLL void mmClock_Reset(struct mmClock* p);
/** Returns milliseconds since initialisation or last reset */
MM_EXPORT_DLL mmUInt64_t mmClock_Milliseconds(struct mmClock* p);
/** Returns microseconds since initialisation or last reset */
MM_EXPORT_DLL mmUInt64_t mmClock_Microseconds(struct mmClock* p);
/** Returns milliseconds since initialisation or last reset, only CPU time measured */
MM_EXPORT_DLL mmUInt64_t mmClock_MillisecondsCpu(struct mmClock* p);
/** Returns microseconds since initialisation or last reset, only CPU time measured */
MM_EXPORT_DLL mmUInt64_t mmClock_MicrosecondsCpu(struct mmClock* p);
//
#include "core/mmSuffix.h"

#endif//__mmClock_h__
