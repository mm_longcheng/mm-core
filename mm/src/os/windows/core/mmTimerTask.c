/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimerTask.h"
#include "core/mmAlloc.h"
#include "core/mmErrno.h"
#include "core/mmLogger.h"
#include "core/mmOSSocket.h"

// timer task impl select version.
struct mmTimerTask_select
{
    mmSocket_t fd;// timer select fd.
};
MM_EXPORT_DLL void mmTimerTask_select_init(struct mmTimerTask_select* p)
{
    p->fd = -1;
}
MM_EXPORT_DLL void mmTimerTask_select_destroy(struct mmTimerTask_select* p)
{
    if (-1 != p->fd)
    {
        mmShutdownSocket(p->fd, MM_BOTH_SHUTDOWN);
        mmCloseSocket(p->fd);
    }
    p->fd = -1;
}

static void* __static_mmTimerTask_HandleThread(void* _arg);

static void __static_mmTimerTask_Handle(struct mmTimerTask* p)
{

}
MM_EXPORT_DLL void mmTimerTaskCallback_Init(struct mmTimerTaskCallback* p)
{
    p->handle = &__static_mmTimerTask_Handle;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmTimerTaskCallback_Destroy(struct mmTimerTaskCallback* p)
{
    p->handle = &__static_mmTimerTask_Handle;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmTimerTask_Init(struct mmTimerTask* p)
{
    p->impl = mmMalloc(sizeof(struct mmTimerTask_select));
    mmTimerTask_select_init((struct mmTimerTask_select*)p->impl);
    mmTimerTaskCallback_Init(&p->callback);
    p->nearby_time = MM_TIME_TASK_NEARBY_MSEC;
    p->msec_delay = 0;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DLL void mmTimerTask_Destroy(struct mmTimerTask* p)
{
    mmTimerTask_select_destroy((struct mmTimerTask_select*)p->impl);
    mmFree((struct mmTimerTask_select*)p->impl);
    p->impl = NULL;
    mmTimerTaskCallback_Destroy(&p->callback);
    p->nearby_time = 0;
    p->msec_delay = 0;
    p->state = MM_TS_CLOSED;
}

// do not assign when holder have some elem.
MM_EXPORT_DLL void mmTimerTask_SetCallback(struct mmTimerTask* p, struct mmTimerTaskCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = *callback;
}
MM_EXPORT_DLL void mmTimerTask_SetNearbyTime(struct mmTimerTask* p, mmMSec_t nearby_time)
{
    p->nearby_time = nearby_time;
}

// sync.
MM_EXPORT_DLL void mmTimerTask_Handle(struct mmTimerTask* p)
{
    struct mmTimerTask_select* _impl = (struct mmTimerTask_select*)p->impl;
    
    struct timeval otime;
    int rt = 0;
    int _a, _b;
    fd_set impl_r_fd_set;
    fd_set impl_w_fd_set;
    fd_set impl_e_fd_set;
    assert(p->callback.handle&&"p->callback.handle is a null.");
    FD_ZERO(&impl_r_fd_set);
    FD_ZERO(&impl_w_fd_set);
    FD_ZERO(&impl_e_fd_set);
    FD_SET(_impl->fd, &impl_r_fd_set);
    //FD_SET(p->fd, &impl_w_fd_set);
    FD_SET(_impl->fd, &impl_e_fd_set);
    if (0 < p->msec_delay)
    {
        mmMSleep(p->msec_delay);
    }
    while (MM_TS_MOTION == p->state)
    {
        if (0 < p->nearby_time)
        {
            // FD_SET is slow,we just copy it.
            fd_set work_r_fd_set = impl_r_fd_set;
            fd_set work_w_fd_set = impl_w_fd_set;
            fd_set work_e_fd_set = impl_e_fd_set;

            _a = p->nearby_time / 1000;
            _b = p->nearby_time % 1000;

            otime.tv_sec = (long)(_a);
            otime.tv_usec = (long)(_b * 1000);

            rt = select((int)(_impl->fd + 1), &work_r_fd_set, &work_w_fd_set, &work_e_fd_set, &otime);
        }
        if (-1 == rt)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        }
        if (0 == p->nearby_time || 0 <= rt)
        {
            (*(p->callback.handle))(p);
        }
    }
}

// start thread.
MM_EXPORT_DLL void mmTimerTask_Start(struct mmTimerTask* p)
{
    struct mmTimerTask_select* _impl = (struct mmTimerTask_select*)p->impl;
    
    _impl->fd = mmFopenSocket(AF_INET, SOCK_DGRAM, 0);
    // no blocking the control fd.
    mmNonblocking(_impl->fd);
    
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->task_thread, NULL, &__static_mmTimerTask_HandleThread, p);
}
// interrupt thread.
MM_EXPORT_DLL void mmTimerTask_Interrupt(struct mmTimerTask* p)
{
    struct mmTimerTask_select* _impl = (struct mmTimerTask_select*)p->impl;
    if (NULL != _impl)
    {
        p->state = MM_TS_CLOSED;
        mmShutdownSocket(_impl->fd, MM_BOTH_SHUTDOWN);
        mmCloseSocket(_impl->fd);
    }
}
// shutdown thread.
MM_EXPORT_DLL void mmTimerTask_Shutdown(struct mmTimerTask* p)
{
    struct mmTimerTask_select* _impl = (struct mmTimerTask_select*)p->impl;
    if (NULL != _impl)
    {
        p->state = MM_TS_FINISH;
        mmShutdownSocket(_impl->fd, MM_BOTH_SHUTDOWN);
        mmCloseSocket(_impl->fd);
    }
}
// join thread.
MM_EXPORT_DLL void mmTimerTask_Join(struct mmTimerTask* p)
{
    pthread_join(p->task_thread, NULL);
}
MM_EXPORT_DLL const char* mmTimerTask_Api(struct mmTimerTask* p)
{
    return "mmTimerTask_select";
}

static void* __static_mmTimerTask_HandleThread(void* _arg)
{
    struct mmTimerTask* p = (struct mmTimerTask*)(_arg);
    mmTimerTask_Handle(p);
    return NULL;
}

