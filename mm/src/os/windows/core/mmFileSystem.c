/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFileSystem.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"

#include <shlwapi.h>
#include <direct.h>
#include <io.h>

#pragma comment(lib,"shlwapi.lib")

MM_EXPORT_DLL intptr_t mmFindFirst(const char* pattern, struct _finddata_t* data)
{
    return (intptr_t)_findfirst(pattern, data);
}
MM_EXPORT_DLL int mmFindNext(intptr_t id, struct _finddata_t* data)
{
    return (int)_findnext(id, data);
}
MM_EXPORT_DLL int mmFindClose(intptr_t id)
{
    return (int)_findclose(id);
}
MM_EXPORT_DLL int mmPathMatch(const char* path, const char* pattern)
{
    return TRUE == PathMatchSpec(path, pattern);
}

MM_EXPORT_DLL int mmIsReservedDirectory(const char *path)
{
    return (path[0] == '.' && (path[1] == 0 || (path[1] == '.' && path[2] == 0)));
}
MM_EXPORT_DLL int mmIsAbsolutePath(const char* path)
{
    return 2 < mmStrlen(path) && (isalpha((mmUChar_t)path[0])) && ':' == path[1];
}
MM_EXPORT_DLL int mmAbsolutePathIsDataExists(const char* file_name)
{
    return -1 != GetFileAttributes(file_name) ? 1 : 0;
}
MM_EXPORT_DLL int mmAccess(const char* file_name, int mode)
{
    return _access(file_name, mode);
}

MM_EXPORT_DLL mmUInt32_t mmStat_Mode(mmStat_t* s)
{
    mmUInt32_t v = 0;
    unsigned short st_mode = s->st_mode;

    if (S_IFDIR == (st_mode & S_IFMT)) { v |= MM_S_IFDIR; }
    if (S_IFCHR == (st_mode & S_IFMT)) { v |= MM_S_IFCHR; }
    if (S_IFREG == (st_mode & S_IFMT)) { v |= MM_S_IFREG; }

    if (st_mode & S_IREAD ) { v |= MM_S_IRUSR; }
    if (st_mode & S_IWRITE) { v |= MM_S_IWUSR; }
    if (st_mode & S_IEXEC ) { v |= MM_S_IXUSR; }

    return v;
}

MM_EXPORT_DLL int mmMkdir(const char* pathname, int mode)
{
    return _mkdir(pathname);
}
MM_EXPORT_DLL int mmRmdir(const char* pathname)
{
    return _rmdir(pathname);
}
MM_EXPORT_DLL int mmUnlink(const char* pathname)
{
    return _unlink(pathname);
}
MM_EXPORT_DLL int mmLink(const char* oldname, const char* newname)
{
    int code = -1;
    mmStat_t s;
    mmUInt32_t mode;
    BOOLEAN r = 0;
    DWORD  dwFlags = 0x0;

    do
    {
        if (0 != mmAccess(oldname, MM_ACCESS_F_OK))
        {
            // the source item is not exist.
            code = -1;
            break;
        }

        if (0 != mmStat(oldname, &s))
        {
            // the source item can not stat.
            code = -1;
            break;
        }

        mode = mmStat_Mode(&s);

        // https://docs.microsoft.com/zh-cn/windows/win32/api/winbase/nf-winbase-createsymboliclinka
        //
        // If the function succeeds, the return value is nonzero.

        dwFlags = MM_S_ISDIR(mode) ? SYMBOLIC_LINK_FLAG_DIRECTORY : 0x0;
        r = CreateSymbolicLink(oldname, newname, 0x0);
        code = (0 != r) ? 0 : -1;

    } while (0);

    return code;
}

// api for file context.
MM_EXPORT_DLL const char* mmFileSystemApi(void)
{
    return "mmFileSystemWindows";
}

MM_EXPORT_DLL int mmCopyFile(const char* path_f, const char* path_t)
{
    //Here we use kernel-space copying for performance reasons
    return CopyFile(path_f, path_t, FALSE);
}
MM_EXPORT_DLL int mmRename(const char* path_f, const char* path_t)
{
    return rename(path_f, path_t);
}
MM_EXPORT_DLL int mmRemove(const char* path)
{
    return remove(path);
}
MM_EXPORT_DLL int mmRecursiveCopy(const char* f, const char* t)
{
    mmStat_t s;
    mmUInt32_t mode;
    int code = -1;

    do
    {
        if (0 != mmAccess(f, MM_ACCESS_F_OK))
        {
            // the source item is not exist.
            code = -1;
            break;
        }

        if (0 != mmStat(f, &s))
        {
            // the source item can not stat.
            code = -1;
            break;
        }

        mode = mmStat_Mode(&s);

        if (MM_S_ISREG(mode))
        {
            mmCopyFile(f, t);
        }
        else
        {
            if (0 != mmMkdir(t, s.st_mode))
            {
                // mkdir failure.
                code = -1;
                break;
            }
            else
            {
                intptr_t data_handle, res;
                struct _finddata_t find_data;

                struct mmString full_pattern;
                struct mmString f_filename;
                struct mmString t_filename;

                mmString_Init(&full_pattern);
                mmString_Init(&f_filename);
                mmString_Init(&t_filename);

                mmString_Assigns(&full_pattern, f);
                mmString_Appends(&full_pattern, "/");
                mmString_Appends(&full_pattern, "*");

                data_handle = mmFindFirst(mmString_CStr(&full_pattern), &find_data);
                res = 0;
                while (data_handle != -1 && res != -1)
                {
                    if (0 == mmIsReservedDirectory(find_data.name))
                    {
                        mmString_Assigns(&f_filename, f);
                        mmString_Appends(&f_filename, "/");
                        mmString_Appends(&f_filename, find_data.name);

                        mmString_Assigns(&t_filename, t);
                        mmString_Appends(&t_filename, "/");
                        mmString_Appends(&t_filename, find_data.name);

                        mmRecursiveCopy(mmString_CStr(&f_filename), mmString_CStr(&t_filename));
                    }
                    res = mmFindNext(data_handle, &find_data);
                }
                // Close if we found any files
                if (data_handle != -1)
                {
                    mmFindClose(data_handle);
                }

                mmString_Destroy(&full_pattern);
                mmString_Destroy(&f_filename);
                mmString_Destroy(&t_filename);
            }
        }

        code = 0;
    } while (0);
    return code;
}
MM_EXPORT_DLL int mmRecursiveRemove(const char* path)
{
    mmStat_t s;
    mmUInt32_t mode;
    int code = -1;

    do
    {
        if (0 != mmAccess(path, MM_ACCESS_F_OK))
        {
            // the source item is not exist.
            code = -1;
            break;
        }

        if (0 != mmStat(path, &s))
        {
            // the source item can not stat.
            code = -1;
            break;
        }

        mode = mmStat_Mode(&s);

        if (MM_S_ISREG(mode))
        {
            mmRemove(path);
        }
        else
        {
            intptr_t data_handle, res;
            struct _finddata_t find_data;

            struct mmString full_pattern;
            struct mmString f_filename;

            mmString_Init(&full_pattern);
            mmString_Init(&f_filename);

            mmString_Assigns(&full_pattern, path);
            mmString_Appends(&full_pattern, "/");
            mmString_Appends(&full_pattern, "*");

            data_handle = mmFindFirst(mmString_CStr(&full_pattern), &find_data);
            res = 0;
            while (data_handle != -1 && res != -1)
            {
                if (0 == mmIsReservedDirectory(find_data.name))
                {
                    mmString_Assigns(&f_filename, path);
                    mmString_Appends(&f_filename, "/");
                    mmString_Appends(&f_filename, find_data.name);

                    mmRecursiveRemove(mmString_CStr(&f_filename));
                }
                res = mmFindNext(data_handle, &find_data);
            }
            // Close if we found any files
            if (data_handle != -1)
            {
                mmFindClose(data_handle);
            }

            mmRmdir(path);

            mmString_Destroy(&full_pattern);
            mmString_Destroy(&f_filename);
        }

        code = 0;
    } while (0);
    return code;
}

MM_EXPORT_DLL int mmFileTruncate(FILE* f, mmUInt64_t length)
{
    int fd = _fileno(f);
    HANDLE h = (HANDLE) _get_osfhandle(fd);

    int64_t nEndPos = (int64_t)length;
    LARGE_INTEGER nFileSize;    
    nFileSize.u.LowPart  = nEndPos & 0xFFFFFFFF;
    nFileSize.u.HighPart = nEndPos >> 32;
    SetFilePointerEx(h, nFileSize, 0, FILE_BEGIN);

    return SetEndOfFile(h) ? 0 : -1;
}

MM_EXPORT_DLL int fseeko(FILE* stream, off_t offset, int whence)
{
    return _fseeki64(stream, (int64_t)offset, whence);
}

MM_EXPORT_DLL off_t ftello(FILE* stream)
{
    return (off_t)_ftelli64(stream);
}

MM_EXPORT_DLL int fseeko64(FILE* stream, int64_t offset, int whence)
{
    return _fseeki64(stream, offset, whence);
}

MM_EXPORT_DLL int64_t ftello64(FILE* stream)
{
    return _ftelli64(stream);
}