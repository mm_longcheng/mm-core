/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileSystem_h__
#define __mmFileSystem_h__

#include "core/mmCore.h"


#include <io.h>

#include <sys/stat.h>


#include "core/mmPrefix.h"

#define MM_S_IFMT  00170000
#define MM_S_IFSOCK 0140000
#define MM_S_IFLNK  0120000
#define MM_S_IFREG  0100000
#define MM_S_IFBLK  0060000
#define MM_S_IFDIR  0040000
#define MM_S_IFCHR  0020000
#define MM_S_IFIFO  0010000
#define MM_S_ISUID  0004000
#define MM_S_ISGID  0002000
#define MM_S_ISVTX  0001000

#define MM_S_ISLNK(m)      (((m) & MM_S_IFMT) == MM_S_IFLNK)
#define MM_S_ISREG(m)      (((m) & MM_S_IFMT) == MM_S_IFREG)
#define MM_S_ISDIR(m)      (((m) & MM_S_IFMT) == MM_S_IFDIR)
#define MM_S_ISCHR(m)      (((m) & MM_S_IFMT) == MM_S_IFCHR)
#define MM_S_ISBLK(m)      (((m) & MM_S_IFMT) == MM_S_IFBLK)
#define MM_S_ISFIFO(m)     (((m) & MM_S_IFMT) == MM_S_IFIFO)
#define MM_S_ISSOCK(m)     (((m) & MM_S_IFMT) == MM_S_IFSOCK)

#define MM_S_IRWXU 00700
#define MM_S_IRUSR 00400
#define MM_S_IWUSR 00200
#define MM_S_IXUSR 00100

#define MM_S_IRWXG 00070
#define MM_S_IRGRP 00040
#define MM_S_IWGRP 00020
#define MM_S_IXGRP 00010

#define MM_S_IRWXO 00007
#define MM_S_IROTH 00004
#define MM_S_IWOTH 00002
#define MM_S_IXOTH 00001

#define MM_INVALID_HANDLE_VALUE INVALID_HANDLE_VALUE

#define mm_fopen fopen
#define mmFclose fclose

MM_EXPORT_DLL intptr_t mmFindFirst(const char* pattern, struct _finddata_t* data);
MM_EXPORT_DLL int mmFindNext(intptr_t id, struct _finddata_t* data);
MM_EXPORT_DLL int mmFindClose(intptr_t id);

MM_EXPORT_DLL int mmPathMatch(const char* path, const char* pattern);

MM_EXPORT_DLL int mmIsReservedDirectory(const char* path);
MM_EXPORT_DLL int mmIsAbsolutePath(const char* path);
MM_EXPORT_DLL int mmAbsolutePathIsDataExists(const char* file_name);

// windows https://msdn.microsoft.com/en-us/library/1w06ktdy(v=vs.140).aspx
// unix    http://man7.org/linux/man-pages/man2/access.2.html
// 00 Existence only
// 02 Write-only
// 04 Read-only
// 06 Read and write
#define MM_ACCESS_F_OK 00
#define MM_ACCESS_W_OK 02
#define MM_ACCESS_R_OK 04
#define MM_ACCESS_X_OK 06
MM_EXPORT_DLL int mmAccess(const char* file_name, int mode);

typedef struct stat mmStat_t;
#define mmStat stat
#define mmFstat fstat
// struct stat st_mode for cross-platform value.
MM_EXPORT_DLL mmUInt32_t mmStat_Mode(mmStat_t* s);

MM_EXPORT_DLL int mmMkdir(const char* athname, int mode);
MM_EXPORT_DLL int mmRmdir(const char* pathname);
MM_EXPORT_DLL int mmUnlink(const char* pathname);
MM_EXPORT_DLL int mmLink(const char* oldname, const char* newname);

// api for file system.
MM_EXPORT_DLL const char* mmFileSystemApi(void);

MM_EXPORT_DLL int mmCopyFile(const char* path_f, const char* path_t);
MM_EXPORT_DLL int mmRename(const char* path_f, const char* path_t);
MM_EXPORT_DLL int mmRemove(const char* path);
// recursive copy api.
MM_EXPORT_DLL int mmRecursiveCopy(const char* f, const char* t);
// recursive remove api.
MM_EXPORT_DLL int mmRecursiveRemove(const char* path);

#define mmFileNo _fileno
MM_EXPORT_DLL int mmFileTruncate(FILE* f, mmUInt64_t length);

// Port API.
MM_EXPORT_DLL int fseeko(FILE* stream, off_t offset, int whence);
MM_EXPORT_DLL off_t ftello(FILE* stream);
MM_EXPORT_DLL int fseeko64(FILE* stream, int64_t offset, int whence);
MM_EXPORT_DLL int64_t ftello64(FILE* stream);

#define mmFSeeko   fseeko
#define mmFTello   ftello
#define mmFSeeko64 _fseeki64
#define mmFTello64 _ftelli64

#include "core/mmSuffix.h"

#endif//__mmFileSystem_h__