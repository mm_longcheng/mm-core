/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTime.h"

#include <timeapi.h>
#pragma comment(lib, "winmm.lib")

MM_EXPORT_DLL int mmTimeBeginPeriod(mmUInt_t uPeriod)
{
    return timeBeginPeriod(uPeriod);
}
MM_EXPORT_DLL int mmTimeEndPeriod(mmUInt_t uPeriod)
{
    return timeEndPeriod(uPeriod);
}

// MM_EXPORT_DLL int mmGettimeofday(struct timeval *tp, struct timezone *tz)
// {
//     // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
//     static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

//     SYSTEMTIME  system_time;
//     FILETIME    file_time;
//     uint64_t    time;

//     GetSystemTime( &system_time );
//     SystemTimeToFileTime( &system_time, &file_time );
//     time =  ((uint64_t)file_time.dwLowDateTime )      ;
//     time += ((uint64_t)file_time.dwHighDateTime) << 32;

//     tp->tv_sec  = (long) ((time - EPOCH) / 10000000L);
//     tp->tv_usec = (long) (system_time.wMilliseconds * 1000);
//     return 0;
// }

// MM_EXPORT_DLL int mmGettimeofday(struct timeval *tp, struct timezone *tz)
// {
//     time_t clock;
//     struct tm tm;
//     SYSTEMTIME wtm;
//     GetLocalTime(&wtm);
//     tm.tm_year      = wtm.wYear - 1900;
//     tm.tm_mon       = wtm.wMonth - 1;
//     tm.tm_mday      = wtm.wDay;
//     tm.tm_hour      = wtm.wHour;
//     tm.tm_min       = wtm.wMinute;
//     tm.tm_sec       = wtm.wSecond;
//     tm. tm_isdst    = -1;
//     clock = mktime(&tm);
//     tp->tv_sec = (long)clock;
//     tp->tv_usec = wtm.wMilliseconds * 1000;
//     return 0;
// }

// MM_EXPORT_DLL int mmGettimeofday(struct timeval *tp, struct timezone *tz)
// {
//     uint64_t  intervals;
//     FILETIME  ft;
// 
//     GetSystemTimeAsFileTime(&ft);
// 
//     /*
//     * A file time is a 64-bit value that represents the number
//     * of 100-nanosecond intervals that have elapsed since
//     * January 1, 1601 12:00 A.M. UTC.
//     *
//     * Between January 1, 1970 (Epoch) and January 1, 1601 there were
//     * 134744 days,
//     * 11644473600 seconds or
//     * 11644473600,000,000,0 100-nanosecond intervals.
//     *
//     * See also MSKB Q167296.
//     */
// 
//     intervals = ((uint64_t) ft.dwHighDateTime << 32) | ft.dwLowDateTime;
//     intervals -= 116444736000000000;
// 
//     tp->tv_sec = (long) (intervals / 10000000);
//     tp->tv_usec = (long) ((intervals % 10000000) / 10);
//     return 0;
// }

// https://gist.github.com/ugovaretto/5875385
MM_EXPORT_DLL int mmGettimeofday(struct timeval *tp, struct timezone *tz)
{
    FILETIME ft;
    unsigned __int64 tmpres = 0;
    static int tzflag = 0;

    if (NULL != tp)
    {
        GetSystemTimeAsFileTime(&ft);

        tmpres |= ft.dwHighDateTime;
        tmpres <<= 32;
        tmpres |= ft.dwLowDateTime;

        tmpres /= 10;  /*convert into microseconds*/
        /*converting file time to unix epoch*/
        tmpres -= DELTA_EPOCH_IN_MICROSECS;
        tp->tv_sec = (long)(tmpres / 1000000UL);
        tp->tv_usec = (long)(tmpres % 1000000UL);
    }

    if (NULL != tz)
    {
        if (!tzflag)
        {
            _tzset();
            tzflag++;
        }
        {
            long _timezone_val = 0;
            int _daylight_val = 0;

            _get_timezone(&_timezone_val);
            _get_daylight(&_daylight_val);

            tz->tz_minuteswest = _timezone_val / 60;
            tz->tz_dsttime = _daylight_val;
        }
    }

    return 0;
}


MM_EXPORT_DLL void mmLocaltimeR(const time_t* s, struct tm *tm)
{
    localtime_s(tm, s);

    tm->mmTm_mon++;
    tm->mmTm_year += 1900;
}

MM_EXPORT_DLL void mmLibcLocaltime(const time_t* s, struct tm *tm)
{
    localtime_s(tm, s);
}

MM_EXPORT_DLL void mmLibcGmtime(const time_t* s, struct tm *tm)
{
    gmtime_s(tm, s);
}

MM_EXPORT_DLL void mmLibcCtime(char* s, size_t sz, const time_t* t)
{
    ctime_s(s, sz, t);
}

MM_EXPORT_DLL mmLong_t mmGettimezone(void)
{
    u_long                 n;
    TIME_ZONE_INFORMATION  tz;

    n = GetTimeZoneInformation(&tz);

    switch (n) {

    case TIME_ZONE_ID_UNKNOWN:
        return -tz.Bias;

    case TIME_ZONE_ID_STANDARD:
        return -(tz.Bias + tz.StandardBias);

    case TIME_ZONE_ID_DAYLIGHT:
        return -(tz.Bias + tz.DaylightBias);

    default: /* TIME_ZONE_ID_INVALID */
        return 0;
    }
}

MM_EXPORT_DLL void mmSSleep(mmULong_t seconds)
{
    SleepEx(seconds * MM_MSEC_PER_SEC, FALSE);
}
MM_EXPORT_DLL void mmMSleep(mmULong_t milliseconds)
{
    SleepEx(milliseconds, FALSE);
}
MM_EXPORT_DLL void mmUSleep(mmULong_t microseconds)
{
    // https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-select
    // 
    // Microsoft MSDN:
    // 
    // Any two of the parameters, readfds, writefds, or exceptfds, can be given as null. 
    // At least one must be non-null, and any non-null descriptor set must contain at least one handle to a socket.

    // At windows, we can not use the select for usleep.
    //
    // https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-sleepex
    // 

    // postgresql
    //
    // https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/port/pgsleep.c;h=84e1c3049ac4fbf94999bcd53715fde1ff1962c3;hb=HEAD

    SleepEx((microseconds < 500 ? 1 : (microseconds + 500) / 1000), FALSE);
}

/* Return micro-seconds since the Unix epoch (jan. 1, 1970) UTC */
MM_EXPORT_DLL mmUInt64_t mmTime_CurrentUSec(void)
{
    struct timeval tv;
    mmGettimeofday(&tv, NULL);
    // note: 
    //   we must cast tv_sec tv_usec to mmUInt64_t, make sure not overflow.
    return (mmUInt64_t)tv.tv_sec * MM_USEC_PER_SEC + (mmUInt64_t)tv.tv_usec;
}

MM_EXPORT_DLL mmUInt64_t mmTime_GetSystemRunningUSec(void)
{
    return GetTickCount64() * MM_MSEC_PER_SEC;
}