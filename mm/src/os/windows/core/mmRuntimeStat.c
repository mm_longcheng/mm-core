/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRuntimeStat.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "Psapi.h"

MM_EXPORT_DLL void mmRuntimeLoadavg_Init(struct mmRuntimeLoadavg* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeLoadavg));
}
MM_EXPORT_DLL void mmRuntimeLoadavg_Destroy(struct mmRuntimeLoadavg* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeLoadavg));
}
MM_EXPORT_DLL void mmRuntimeLoadavg_Perform(struct mmRuntimeLoadavg* p)
{

}

MM_EXPORT_DLL void mmRuntimeMachine_Init(struct mmRuntimeMachine* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeMachine));
}
MM_EXPORT_DLL void mmRuntimeMachine_Destroy(struct mmRuntimeMachine* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeMachine));
}
MM_EXPORT_DLL void mmRuntimeMachine_Perform(struct mmRuntimeMachine* p)
{
    mmRuntimeMachine_PerformCpu(p);
    mmRuntimeMachine_PerformMem(p);
}
MM_EXPORT_DLL void mmRuntimeMachine_PerformCpu(struct mmRuntimeMachine* p)
{
    FILETIME idleTime, kernelTime, userTime;
    LARGE_INTEGER lg_idleTime, lg_kernelTime, lg_userTime;

    SYSTEM_INFO si;

    GetSystemTimes(&idleTime, &kernelTime, &userTime);
    lg_idleTime.HighPart = idleTime.dwHighDateTime;
    lg_idleTime.LowPart = idleTime.dwLowDateTime;

    lg_kernelTime.HighPart = kernelTime.dwHighDateTime;
    lg_kernelTime.LowPart = kernelTime.dwLowDateTime;

    lg_userTime.HighPart = userTime.dwHighDateTime;
    lg_userTime.LowPart = userTime.dwLowDateTime;

    p->cpu_idle = lg_idleTime.QuadPart;
    p->cpu_system = lg_kernelTime.QuadPart;
    p->cpu_user = lg_userTime.QuadPart;

    // get cpu cores.
    GetSystemInfo(&si);
    p->cores = si.dwNumberOfProcessors;
}
MM_EXPORT_DLL void mmRuntimeMachine_PerformMem(struct mmRuntimeMachine* p)
{
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    p->mem_total = statex.ullTotalPhys;
    p->mem_free = statex.ullAvailPhys;
    p->mem_buff = p->mem_total - p->mem_free;
    p->mem_cache = 0;
}

MM_EXPORT_DLL void mmRuntimeProcess_Init(struct mmRuntimeProcess* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeProcess));
}
MM_EXPORT_DLL void mmRuntimeProcess_Destroy(struct mmRuntimeProcess* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeProcess));
}
MM_EXPORT_DLL void mmRuntimeProcess_Perform(struct mmRuntimeProcess* p)
{
    mmRuntimeProcess_PerformPid(p);
    mmRuntimeProcess_PerformCpu(p);
    mmRuntimeProcess_PerformMem(p);
    mmRuntimeProcess_PerformIO(p);
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformPid(struct mmRuntimeProcess* p)
{
    p->pid = GetCurrentProcessId();
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformCpu(struct mmRuntimeProcess* p)
{
    FILETIME CreateTime, ExitTime, KernelTime, UserTime;
    LARGE_INTEGER lgKernelTime, lgUserTime;

    HANDLE hd = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, (DWORD)p->pid);
    GetProcessTimes(hd, &CreateTime, &ExitTime, &KernelTime, &UserTime);

    lgKernelTime.HighPart = KernelTime.dwHighDateTime;
    lgKernelTime.LowPart = KernelTime.dwLowDateTime;

    lgUserTime.HighPart = UserTime.dwHighDateTime;
    lgUserTime.LowPart = UserTime.dwLowDateTime;

    p->cpu_utime = lgUserTime.QuadPart;
    p->cpu_stime = lgKernelTime.QuadPart;
    p->cpu_cutime = 0;
    p->cpu_cstime = 0;
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformMem(struct mmRuntimeProcess* p)
{
    HANDLE hd = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, (DWORD)p->pid);
    PROCESS_MEMORY_COUNTERS pmc;
    GetProcessMemoryInfo(hd, &pmc, sizeof(PROCESS_MEMORY_COUNTERS));
    p->vm_rss = pmc.WorkingSetSize;
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformIO(struct mmRuntimeProcess* p)
{
    IO_COUNTERS info;
    HANDLE hd = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, (DWORD)p->pid);
    GetProcessIoCounters(hd, &info);
    p->rchar = info.ReadTransferCount;
    p->wchar = info.WriteTransferCount;
}

MM_EXPORT_DLL void mmRuntimeStat_Init(struct mmRuntimeStat* p)
{
    p->pid = 0;
    p->cores = 0;
    p->vm_rss = 0;
    p->rchar = 0;
    p->wchar = 0;
    p->cpu_sys = 0;
    p->cpu_pro = 0;
    p->mem_pro = 0;
    p->timecode_o = 0;
    p->timecode_n = 0;
    p->sampling = 0;
    mmMemset(&p->tv_o, 0, sizeof(struct timeval));
    mmMemset(&p->tv_n, 0, sizeof(struct timeval));
    mmRuntimeLoadavg_Init(&p->loadavg_o);
    mmRuntimeLoadavg_Init(&p->loadavg_n);
    mmRuntimeMachine_Init(&p->machine_o);
    mmRuntimeMachine_Init(&p->machine_n);
    mmRuntimeProcess_Init(&p->process_o);
    mmRuntimeProcess_Init(&p->process_n);
}

MM_EXPORT_DLL void mmRuntimeStat_Destroy(struct mmRuntimeStat* p)
{
    p->pid = 0;
    p->cores = 0;
    p->vm_rss = 0;
    p->rchar = 0;
    p->wchar = 0;
    p->cpu_sys = 0;
    p->cpu_pro = 0;
    p->mem_pro = 0;
    p->timecode_o = 0;
    p->timecode_n = 0;
    p->sampling = 0;
    mmMemset(&p->tv_o, 0, sizeof(struct timeval));
    mmMemset(&p->tv_n, 0, sizeof(struct timeval));
    mmRuntimeLoadavg_Destroy(&p->loadavg_o);
    mmRuntimeLoadavg_Destroy(&p->loadavg_n);
    mmRuntimeMachine_Destroy(&p->machine_o);
    mmRuntimeMachine_Destroy(&p->machine_n);
    mmRuntimeProcess_Destroy(&p->process_o);
    mmRuntimeProcess_Destroy(&p->process_n);
}

MM_EXPORT_DLL void mmRuntimeStat_Update(struct mmRuntimeStat* p)
{
    mmUInt64_t b_utime, b_stime, b_cutime, b_cstime;
    mmUInt64_t b_user, b_nice, b_system, b_idle, a, b, c;
    
    mmGettimeofday(&p->tv_n, NULL);
    p->timecode_n = p->tv_n.tv_sec * 1000 + (p->tv_n.tv_usec / 1000);
    p->sampling = p->timecode_n - p->timecode_o;
    //
    mmRuntimeLoadavg_Perform(&p->loadavg_n);
    mmRuntimeMachine_Perform(&p->machine_n);
    mmRuntimeProcess_Perform(&p->process_n);
    
    p->pid = p->process_n.pid;
    p->vm_rss = p->process_n.vm_rss;
    p->rchar = p->process_n.rchar;
    p->wchar = p->process_n.wchar;
    p->cores = p->machine_n.cores;
    //
    b_utime = p->process_n.cpu_utime - p->process_o.cpu_utime;
    b_stime = p->process_n.cpu_stime - p->process_o.cpu_stime;
    b_cutime = p->process_n.cpu_cutime - p->process_o.cpu_cutime;
    b_cstime = p->process_n.cpu_cstime - p->process_o.cpu_cstime;
    a = b_utime + b_stime + b_cutime + b_cstime;

    b_user = p->machine_n.cpu_user - p->machine_o.cpu_user;
    b_nice = p->machine_n.cpu_nice - p->machine_o.cpu_nice;
    b_system = p->machine_n.cpu_system - p->machine_o.cpu_system;
    b_idle = p->machine_n.cpu_idle - p->machine_o.cpu_idle;
    b = b_user + b_nice + b_system + b_idle;
    c = b_user + b_nice + b_system;

    p->cpu_sys = (0 == c && 0 == b_idle) ? 0 : c * 100.0f / (c + b_idle);
    p->cpu_pro = (0 == b) ? 0 : a * 100.0f / b;
    p->mem_pro = p->vm_rss * 100.0f / p->machine_o.mem_total;
    
    mmMemcpy(&p->loadavg_o, &p->loadavg_n, sizeof(struct mmRuntimeLoadavg));
    mmMemcpy(&p->machine_o, &p->machine_n, sizeof(struct mmRuntimeMachine));
    mmMemcpy(&p->process_o, &p->process_n, sizeof(struct mmRuntimeProcess));
    p->timecode_o = p->timecode_n;
}
