/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAlloc_h__
#define __mmAlloc_h__

#include "core/mmCore.h"

#include <stdlib.h>
#include <string.h>

#include "core/mmPrefix.h"

MM_EXPORT_DLL const mmChar_t* mmAllocApi(void);

#define mmMalloc        malloc
#define mmRealloc       realloc
#define mmFree          free

#define mmCalloc        calloc

#define mmMemzero(buf, n)       (void) memset(buf, 0, n)
#define mmMemset(buf, c, n)     (void) memset(buf, c, n)
#define mmMemcpy(dst, src, n)   (void) memcpy(dst, src, n)
#define mmCpymem(dst, src, n)   (((mmUChar_t *) memcpy(dst, src, n)) + (n))
#define mmMemmove(dst, src, n)   (void) memmove(dst, src, n)
#define mmMovemem(dst, src, n)   (((mmUChar_t *) memmove(dst, src, n)) + (n))
#define mmMemcmp(s1, s2, n)  memcmp((const char *) s1, (const char *) s2, n)

#define mmAlignedMalloc _aligned_malloc
#define mmAlignedFree _aligned_free

#include "core/mmSuffix.h"

#endif//__mmAlloc_h__
