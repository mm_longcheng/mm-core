/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConfigPlatform_h__
#define __mmConfigPlatform_h__

#include "core/mmPlatform.h"

#include "core/mmPrefix.h"
// sign current platform.
#define MM_WIN32 1

#include "core/mmAutoHeaders.h"

#undef  WIN32
#define WIN32         0x0400

#ifndef STRICT
#define STRICT
#endif//STRICT

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif//WIN32_LEAN_AND_MEAN

/* enable getenv() and gmtime() in msvc8 */
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif//_CRT_SECURE_NO_WARNINGS
#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#endif//_CRT_SECURE_NO_DEPRECATE
/*
 * we need to include <windows.h> explicitly before <winsock2.h> because
 * the warning 4201 is enabled in <windows.h>
 */
#include <windows.h>

#ifdef _MSC_VER
#pragma warning(disable:4201)
#endif

#include <winsock2.h>
#include <ws2tcpip.h>  /* ipv6 */
#include <mswsock.h>
#include <shellapi.h>
#include <stddef.h>    /* offsetof() */

#ifndef _WIN32_WINNT
#define _WIN32_WINNT  0x0501
#endif//_WIN32_WINNT

#ifdef __MINGW64_VERSION_MAJOR

/* GCC MinGW-w64 supports _FILE_OFFSET_BITS */
#define _FILE_OFFSET_BITS 64

#elif defined __GNUC__

/* GCC MinGW's stdio.h includes sys/types.h */
#define _OFF_T_

#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#ifdef __MINGW64_VERSION_MAJOR
#include <stdint.h>
#endif
#include <ctype.h>
#include <locale.h>

#ifdef __WATCOMC__
#define _TIME_T_DEFINED
typedef long  time_t;
/* OpenWatcom defines time_t as "unsigned long" */
#endif

#include <time.h>      /* localtime(), strftime() */


#ifdef _MSC_VER

#if _MSC_VER < 1900
/* the end of the precompiled headers */
#pragma hdrstop
#endif// _MSC_VER < 1900

#pragma warning(default:4201)

/* disable some "-W4" level warnings */

/* 'type cast': from function pointer to data pointer */
#pragma warning(disable:4054)

/* 'type cast': from data pointer to function pointer */
#pragma warning(disable:4055)

/* unreferenced formal parameter */
#pragma warning(disable:4100)

/* FD_SET() and FD_CLR(): conditional expression is constant */
#pragma warning(disable:4127)

/* array is too small to include a terminating null character */
#pragma warning(disable:4295)

#endif


#ifdef __WATCOMC__

/* symbol 'mm_rbtree_min' has been defined, but not referenced */
#pragma disable_message(202)

#endif


#ifdef __BORLANDC__

/* the end of the precompiled headers */
#pragma hdrstop

/* functions containing (for|while|some if) are not expanded inline */
#pragma warn -8027

/* unreferenced formal parameter */
#pragma warn -8057

#endif


#include <core/mmAutoConfig.h>


#define mmInline          __inline
#define mmCDECL           __cdecl


#ifdef _MSC_VER
typedef unsigned __int32    uint32_t;
typedef __int32             int32_t;
typedef unsigned __int16    uint16_t;
#define mmLibcCDECL      __cdecl

#elif defined __BORLANDC__
typedef unsigned __int32    uint32_t;
typedef __int32             int32_t;
typedef unsigned __int16    uint16_t;
#define mmLibcCDECL      __cdecl

#else /* __WATCOMC__ */
typedef unsigned int        uint32_t;
typedef int                 int32_t;
typedef unsigned short int  uint16_t;
#define mmLibcCDECL

#endif

typedef __int64             int64_t;
typedef unsigned __int64    uint64_t;

#if _MSC_VER < 1900
#if !defined(_INTPTR_T_DEFINED) 
#if!defined(__WATCOMC__) && !defined(__MINGW64_VERSION_MAJOR)
typedef int                 intptr_t;
typedef u_int               uintptr_t;
#endif
#endif
#endif// _MSC_VER < 1900

#ifndef __MINGW64_VERSION_MAJOR
// version vs2012
#include <sys/types.h>
#ifndef _OFF_T_DEFINED
/* Windows defines off_t as long, which is 32-bit */
typedef __int64             off_t;
typedef __int64             _off_t;
#define _OFF_T_DEFINED
#endif

#endif


#ifdef __WATCOMC__

/* off_t is redefined by sys/types.h used by zlib.h */
#define __TYPES_H_INCLUDED
typedef int                 dev_t;
typedef unsigned int        ino_t;

#elif __BORLANDC__

/* off_t is redefined by sys/types.h used by zlib.h */
#define __TYPES_H

typedef int                 dev_t;
typedef unsigned int        ino_t;

#endif


#ifndef __MINGW64_VERSION_MAJOR

#include <BaseTsd.h>
typedef SSIZE_T ssize_t;

#endif

#ifndef __MINGW64_VERSION_MAJOR
typedef int                 pid_t;
#endif

typedef uint32_t            in_addr_t;
typedef u_short             in_port_t;
typedef int                 sig_atomic_t;


#ifdef _WIN64

#define MM_PTR_SIZE            8
#define MM_SIZE_T_LEN          (sizeof("-9223372036854775808") - 1)
#define MM_MAX_SIZE_T_VALUE    9223372036854775807
#define MM_TIME_T_LEN          (sizeof("-9223372036854775808") - 1)
#define MM_TIME_T_SIZE         8
#define MM_MAX_TIME_T_VALUE    9223372036854775807

#else

#define MM_PTR_SIZE            4
#define MM_SIZE_T_LEN          (sizeof("-2147483648") - 1)
#define MM_MAX_SIZE_T_VALUE    2147483647
#define MM_TIME_T_LEN          (sizeof("-2147483648") - 1)
#define MM_TIME_T_SIZE         4
#define MM_MAX_TIME_T_VALUE    2147483647

#endif


#define MM_OFF_T_LEN           (sizeof("-9223372036854775807") - 1)
#define MM_MAX_OFF_T_VALUE     9223372036854775807
#define MM_SIG_ATOMIC_T_SIZE   4


#define MM_HAVE_LITTLE_ENDIAN  1
#define MM_HAVE_NONALIGNED     1


#define MM_WIN_NT        200000


#define MM_LISTEN_BACKLOG           511


#ifndef MM_HAVE_INHERITED_NONBLOCK
#define MM_HAVE_INHERITED_NONBLOCK  1
#endif

#ifndef MM_HAVE_CASELESS_FILESYSTEM
#define MM_HAVE_CASELESS_FILESYSTEM  1
#endif

#ifndef MM_HAVE_WIN32_TRANSMITPACKETS
#define MM_HAVE_WIN32_TRANSMITPACKETS  1
#define MM_HAVE_WIN32_TRANSMITFILE     0
#endif

#ifndef MM_HAVE_WIN32_TRANSMITFILE
#define MM_HAVE_WIN32_TRANSMITFILE  1
#endif

#if (MM_HAVE_WIN32_TRANSMITPACKETS) || (MM_HAVE_WIN32_TRANSMITFILE)
#define MM_HAVE_SENDFILE  1
#endif

#ifndef MM_HAVE_SO_SNDLOWAT
/* setsockopt(SO_SNDLOWAT) returns error WSAENOPROTOOPT */
#define MM_HAVE_SO_SNDLOWAT         0
#endif

#define MM_HAVE_GETADDRINFO         1

#define MM_random               rand
#define MM_debug_init()

// undef this stupid macro.
#ifdef max
#undef max
#endif//max
#ifdef min
#undef min
#endif//min

#ifndef PATH_MAX
#define PATH_MAX _MAX_PATH
#endif// PATH_MAX

#if _MSC_VER >= 1600
#include <stdint.h>
#else
#include "msvc/stdint.h"
#endif// _MSC_VER >= 1600
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif//__STDC_FORMAT_MACROS

#if _MSC_VER >= 1800
#include <inttypes.h>
#else
#include "msvc/inttypes.h"
#endif// _MSC_VER >= 1800

// word size. windows _M_IX86 _M_X64
#ifdef _M_X64
#define MM_WORDSIZE 64
#define MM_SIZEOF_UINTPTR_T 8
#else
#define MM_WORDSIZE 32
#define MM_SIZEOF_UINTPTR_T 4
#endif

#include <string.h>
// strcasecmp
#ifndef strcasecmp
#define strcasecmp _stricmp
#endif
// strncasecmp
#ifndef strncasecmp
#define strncasecmp _strnicmp
#endif

#include "core/mmSuffix.h"

#endif//__mmConfigPlatform_h__