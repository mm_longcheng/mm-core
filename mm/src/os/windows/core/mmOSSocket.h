/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOSSocket_h__
#define __mmOSSocket_h__

#include "core/mmConfig.h"

#include "core/mmPrefix.h"

#define MM_READ_SHUTDOWN SD_RECEIVE
#define MM_SEND_SHUTDOWN SD_SEND
#define MM_BOTH_SHUTDOWN SD_BOTH

typedef SOCKET  mmSocket_t;
typedef int     socklen_t;

typedef mmUInt8_t mmSASize_t;
typedef ADDRESS_FAMILY mmSAFamily_t;
typedef USHORT mmSAPort_t;

// error C4996 _WINSOCK_DEPRECATED_NO_WARNINGS
#if _MSC_VER >= 1800
#define mmFopenSocket(af, type, proto) WSASocketW(af, type, proto, NULL, 0, WSA_FLAG_OVERLAPPED)
#else
#define mmFopenSocket(af, type, proto) WSASocket(af, type, proto, NULL, 0, WSA_FLAG_OVERLAPPED)
#endif

#define mmSocketN        "WSASocket()"

MM_EXPORT_DLL int mmNonblocking(mmSocket_t s);
MM_EXPORT_DLL int mmBlocking(mmSocket_t s);

#define mmNonblockingN   "ioctlsocket(FIONBIO)"
#define mmBlockingN      "ioctlsocket(!FIONBIO)"

#define mmShutdownSocket    shutdown
#define mmShutdownSocketN  "shutdown()"

#define mmCloseSocket    closesocket
#define mmCloseSocketN  "closesocket()"


#ifndef WSAID_ACCEPTEX

typedef BOOL(PASCAL FAR * LPFN_ACCEPTEX)(
    IN SOCKET sListenSocket,
    IN SOCKET sAcceptSocket,
    IN PVOID lpOutputBuffer,
    IN DWORD dwReceiveDataLength,
    IN DWORD dwLocalAddressLength,
    IN DWORD dwRemoteAddressLength,
    OUT LPDWORD lpdwBytesReceived,
    IN LPOVERLAPPED lpOverlapped
    );

#define WSAID_ACCEPTEX                                                       \
    {0xb5367df1,0xcbac,0x11cf,{0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92}}

#endif


#ifndef WSAID_GETACCEPTEXSOCKADDRS

typedef VOID(PASCAL FAR * LPFN_GETACCEPTEXSOCKADDRS)(
    IN PVOID lpOutputBuffer,
    IN DWORD dwReceiveDataLength,
    IN DWORD dwLocalAddressLength,
    IN DWORD dwRemoteAddressLength,
    OUT struct sockaddr **LocalSockaddr,
    OUT LPINT LocalSockaddrLength,
    OUT struct sockaddr **RemoteSockaddr,
    OUT LPINT RemoteSockaddrLength
    );

#define WSAID_GETACCEPTEXSOCKADDRS                                           \
        {0xb5367df2,0xcbac,0x11cf,{0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92}}

#endif


#ifndef WSAID_TRANSMITFILE

#ifndef TF_DISCONNECT

#define TF_DISCONNECT           1
#define TF_REUSE_SOCKET         2
#define TF_WRITE_BEHIND         4
#define TF_USE_DEFAULT_WORKER   0
#define TF_USE_SYSTEM_THREAD    16
#define TF_USE_KERNEL_APC       32

typedef struct _TRANSMIT_FILE_BUFFERS {
    LPVOID Head;
    DWORD HeadLength;
    LPVOID Tail;
    DWORD TailLength;
} TRANSMIT_FILE_BUFFERS, *PTRANSMIT_FILE_BUFFERS, FAR *LPTRANSMIT_FILE_BUFFERS;

#endif

typedef BOOL(PASCAL FAR * LPFN_TRANSMITFILE)(
    IN SOCKET hSocket,
    IN HANDLE hFile,
    IN DWORD nNumberOfBytesToWrite,
    IN DWORD nNumberOfBytesPerSend,
    IN LPOVERLAPPED lpOverlapped,
    IN LPTRANSMIT_FILE_BUFFERS lpTransmitBuffers,
    IN DWORD dwReserved
    );

#define WSAID_TRANSMITFILE                                                   \
    {0xb5367df0,0xcbac,0x11cf,{0x95,0xca,0x00,0x80,0x5f,0x48,0xa1,0x92}}

#endif


#ifndef WSAID_TRANSMITPACKETS

/* OpenWatcom has a swapped TP_ELEMENT_FILE and TP_ELEMENT_MEMORY definition */

#ifndef TP_ELEMENT_FILE

#ifdef _MSC_VER
#pragma warning(disable:4201) /* Nonstandard extension, nameless struct/union */
#endif

typedef struct _TRANSMIT_PACKETS_ELEMENT {
    ULONG dwElFlags;
#define TP_ELEMENT_MEMORY   1
#define TP_ELEMENT_FILE     2
#define TP_ELEMENT_EOP      4
    ULONG cLength;
    union {
        struct {
            LARGE_INTEGER nFileOffset;
            HANDLE        hFile;
        };
        PVOID             pBuffer;
    };
} TRANSMIT_PACKETS_ELEMENT, *PTRANSMIT_PACKETS_ELEMENT,
FAR *LPTRANSMIT_PACKETS_ELEMENT;

#ifdef _MSC_VER
#pragma warning(default:4201)
#endif

#endif

typedef BOOL(PASCAL FAR * LPFN_TRANSMITPACKETS) (
    SOCKET hSocket,
    TRANSMIT_PACKETS_ELEMENT *lpPacketArray,
    DWORD nElementCount,
    DWORD nSendSize,
    LPOVERLAPPED lpOverlapped,
    DWORD dwFlags
    );

#define WSAID_TRANSMITPACKETS                                                \
    {0xd9689da0,0x1f90,0x11d3,{0x99,0x71,0x00,0xc0,0x4f,0x68,0xc8,0x76}}

#endif


#ifndef WSAID_CONNECTEX

typedef BOOL(PASCAL FAR * LPFN_CONNECTEX) (
    IN SOCKET s,
    IN const struct sockaddr FAR *name,
    IN int namelen,
    IN PVOID lpSendBuffer OPTIONAL,
    IN DWORD dwSendDataLength,
    OUT LPDWORD lpdwBytesSent,
    IN LPOVERLAPPED lpOverlapped
    );

#define WSAID_CONNECTEX \
    {0x25a207b9,0xddf3,0x4660,{0x8e,0xe9,0x76,0xe5,0x8c,0x74,0x06,0x3e}}

#endif


#ifndef WSAID_DISCONNECTEX

typedef BOOL(PASCAL FAR * LPFN_DISCONNECTEX) (
    IN SOCKET s,
    IN LPOVERLAPPED lpOverlapped,
    IN DWORD  dwFlags,
    IN DWORD  dwReserved
    );

#define WSAID_DISCONNECTEX                                                   \
    {0x7fda2e11,0x8630,0x436f,{0xa0,0x31,0xf5,0x36,0xa6,0xee,0xc1,0x57}}

#endif


extern LPFN_ACCEPTEX              mm_acceptex;
extern LPFN_GETACCEPTEXSOCKADDRS  mm_getacceptexsockaddrs;
extern LPFN_TRANSMITFILE          mm_transmitfile;
extern LPFN_TRANSMITPACKETS       mm_transmitpackets;
extern LPFN_CONNECTEX             mm_connectex;
extern LPFN_DISCONNECTEX          mm_disconnectex;


MM_EXPORT_DLL int mmTcpPush(mmSocket_t s);
#define mmTcpPushN            "tcp_push()"

// only open the flag.
// 1 true 0 false.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mmSocketKeepalive(mmSocket_t s, int flag);

// tcp_keepidle  firt keepalive trigger    ms.
// tcp_keepintvl keepalive packet interval ms.
// tcp_keepcnt   try times                 n.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mmSocketKeepaliveCtl(mmSocket_t s, int tcp_keepidle, int tcp_keepintvl, int tcp_keepcnt);

#define MM_INVALID_SOCKET INVALID_SOCKET

#define MM_NONBLOCK 0
#define MM_BLOCKING 1

#define MM_AF_INET4 AF_INET
#define MM_AF_INET6 AF_INET6
#define MM_AF_UNSPE AF_UNSPEC

#define MM_IPPROTO_IPV4 IPPROTO_IP
#define MM_IPPROTO_IPV6 IPPROTO_IPV6

// not keepalive.
#define MM_KEEPALIVE_INACTIVE 0
// mod keepalive.
#define MM_KEEPALIVE_ACTIVATE 1

// udp group
#define MM_IPV4_ENTER_GROUP IP_ADD_MEMBERSHIP
#define MM_IPV4_LEAVE_GROUP IP_DROP_MEMBERSHIP

#define MM_IPV6_ENTER_GROUP IPV6_ADD_MEMBERSHIP
#define MM_IPV6_LEAVE_GROUP IPV6_DROP_MEMBERSHIP

#define mmHtons htons
#define mmNtohs ntohs
#define mmHtonl htonl
#define mmNtohl ntohl
#define mmHtonll htonll
#define mmNtohll ntohll

#define mmHton16 mmHtons
#define mmNtoh16 mmNtohs
#define mmHton32 mmHtonl
#define mmNtoh32 mmNtohl
#define mmHton64 mmHtonll
#define mmNtoh64 mmNtohll

#include "core/mmSuffix.h"

#endif//__mmOSSocket_h__
