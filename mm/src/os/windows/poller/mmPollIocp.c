/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoll.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmErrno.h"
#include "core/mmLogger.h"
#include "container/mmRbtreeU64.h"

#define MM_POLL_IOCP_HANDLE_THREAD_NUMBER 1

struct mmPollIocpOverlapped
{
    OVERLAPPED overlapped;
    // mask for MM_PE_NONEABLE | MM_PE_READABLE | MM_PE_WRITABLE
    int mask;
    mmSocket_t fd;
    void* ud;
};

static void mmPollIocpOverlapped_Init(struct mmPollIocpOverlapped* p)
{
    mmMemset(&p->overlapped, 0, sizeof(OVERLAPPED));
    p->mask = MM_PE_NONEABLE;
    p->fd = MM_INVALID_SOCKET;
    p->ud = NULL;
}

static void mmPollIocpOverlapped_Destroy(struct mmPollIocpOverlapped* p)
{
    mmMemset(&p->overlapped, 0, sizeof(OVERLAPPED));
    p->mask = MM_PE_NONEABLE;
    p->fd = MM_INVALID_SOCKET;
    p->ud = NULL;
}

static void* mmPollIocpOverlapped_RbtreeProduce(struct mmRbtreeU64Vpt* p, mmUInt64_t k)
{
    struct mmPollIocpOverlapped* e = (struct mmPollIocpOverlapped*)mmMalloc(sizeof(struct mmPollIocpOverlapped));
    mmPollIocpOverlapped_Init(e);
    return e;
}

static void* mmPollIocpOverlapped_RbtreeRecycle(struct mmRbtreeU64Vpt* p, mmUInt64_t k, void* v)
{
    struct mmPollIocpOverlapped* e = (struct mmPollIocpOverlapped*)(v);
    mmPollIocpOverlapped_Destroy(e);
    mmFree(e);
    return v;
}

struct mmPollIocp
{
    struct mmRbtreeU64Vpt rbtree;
    HANDLE pfd;
};

static void mmPollIocp_Init(struct mmPollIocp* p)
{
    struct mmRbtreeU64VptAllocator hU64VptAllocator;

    mmRbtreeU64Vpt_Init(&p->rbtree);

    hU64VptAllocator.Produce = &mmPollIocpOverlapped_RbtreeProduce;
    hU64VptAllocator.Recycle = &mmPollIocpOverlapped_RbtreeRecycle;
    mmRbtreeU64Vpt_SetAllocator(&p->rbtree, &hU64VptAllocator);

    //HANDLE WINAPI CreateIoCompletionPort(
    //  _In_     HANDLE    FileHandle,
    //  _In_opt_ HANDLE    ExistingCompletionPort,
    //  _In_     ULONG_PTR CompletionKey,
    //  _In_     DWORD     NumberOfConcurrentThreads
    //  );
    //
    // here we only port the windows iocp api like epoll,
    // so the thread is MM_POLL_IOCP_HANDLE_THREAD_NUMBER.
    p->pfd = CreateIoCompletionPort(
        INVALID_HANDLE_VALUE, 
        NULL, 
        0, 
        MM_POLL_IOCP_HANDLE_THREAD_NUMBER);
}

static void mmPollIocp_Destroy(struct mmPollIocp* p)
{
    mmRbtreeU64Vpt_Destroy(&p->rbtree);
    CloseHandle(p->pfd);
    p->pfd = NULL;
}

static void mmPollIocp_PostBreak(struct mmPollIocp* p, mmUInt32_t length)
{
    mmUInt32_t i = 0;
    for (i = 0; i < length; ++i)
    {
        // PostQueuedCompletionStatus is manual trigger GetQueuedCompletionStatus event.
        //BOOL WINAPI PostQueuedCompletionStatus(
        //  _In_     HANDLE       CompletionPort,
        //  _In_     DWORD        dwNumberOfBytesTransferred,
        //  _In_     ULONG_PTR    dwCompletionKey,
        //  _In_opt_ LPOVERLAPPED lpOverlapped
        //  );
        unsigned long dwNumberOfBytesTransferred = 0;
        ULONG_PTR dwCompletionKey = 0;
        LPOVERLAPPED lpOverlapped = NULL;

        PostQueuedCompletionStatus(
            p->pfd, 
            dwNumberOfBytesTransferred, 
            dwCompletionKey, 
            lpOverlapped);
    }
}

static mmInt_t mmPollIocp_PostMask(struct mmPollIocp* p, struct mmPollIocpOverlapped* overlapped)
{
    mmInt_t code = 0;
    WSABUF databuff;
    DWORD hRecvBytes = 0;
    DWORD flags = 0;
    int hRecvCode = 0;
    mmErr_t errcode = 0;

    databuff.len = 0;
    databuff.buf = NULL;

    //HANDLE WINAPI CreateIoCompletionPort(
    //  _In_     HANDLE    FileHandle,
    //  _In_opt_ HANDLE    ExistingCompletionPort,
    //  _In_     ULONG_PTR CompletionKey,
    //  _In_     DWORD     NumberOfConcurrentThreads
    //  );
    CreateIoCompletionPort(
        (HANDLE)(overlapped->fd), 
        p->pfd, 
        (ULONG_PTR)(overlapped->ud), 
        0);

    if (overlapped->mask & MM_PE_READABLE)
    {
        //int WSARecv(
        //  _In_    SOCKET                             s,
        //  _Inout_ LPWSABUF                           lpBuffers,
        //  _In_    DWORD                              dwBufferCount,
        //  _Out_   LPDWORD                            lpNumberOfBytesRecvd,
        //  _Inout_ LPDWORD                            lpFlags,
        //  _In_    LPWSAOVERLAPPED                    lpOverlapped,
        //  _In_    LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
        //  );
        hRecvCode = WSARecv(
            overlapped->fd, 
            &databuff, 
            1, 
            &hRecvBytes,
            &flags, 
            (OVERLAPPED*)overlapped, 
            NULL);

        if (0 != hRecvCode)
        {
            errcode = mmErrno;
            // not need logger here, logger outside.
            code = (ERROR_IO_PENDING != errcode) ? -1 : 0;
        }
        else
        {
            code = 0;
        }
    }
    if (overlapped->mask & MM_PE_WRITABLE)
    {
        //int WSASend(
        //  _In_  SOCKET                             s,
        //  _In_  LPWSABUF                           lpBuffers,
        //  _In_  DWORD                              dwBufferCount,
        //  _Out_ LPDWORD                            lpNumberOfBytesSent,
        //  _In_  DWORD                              dwFlags,
        //  _In_  LPWSAOVERLAPPED                    lpOverlapped,
        //  _In_  LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
        //  );
        hRecvCode = WSASend(
            overlapped->fd, 
            &databuff, 
            1, 
            &hRecvBytes,
            flags, 
            (OVERLAPPED*)overlapped, 
            NULL);

        if (0 != hRecvCode)
        {
            errcode = mmErrno;
            // not need logger here, logger outside.
            code = (ERROR_IO_PENDING != errcode) ? -1 : 0;
        }
        else
        {
            code = 0;
        }
    }
    return code;
}

MM_EXPORT_NET void mmPoll_Init(struct mmPoll* p)
{
    struct mmPollIocp* _impl = NULL;
    p->length = MM_POLL_EVENT_DEFAULT_NUMBER;
    p->arrays = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * p->length);
    _impl = (struct mmPollIocp*)mmMalloc(sizeof(struct mmPollIocp));
    mmPollIocp_Init(_impl);
    p->impl = _impl;
}

MM_EXPORT_NET void mmPoll_Destroy(struct mmPoll* p)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);
    mmPollIocp_Destroy(_impl);
    mmFree(p->arrays);
    mmFree(_impl);

    p->length = 0;
    p->arrays = NULL;
    p->impl = NULL;
}

// poll ev length default is 256.set it before mmPoll_wait,or mmPoll_wait after.
MM_EXPORT_NET void mmPoll_SetLength(struct mmPoll* p, mmUInt32_t length)
{
    if (length < p->length)
    {
        mmUInt32_t i = 0;
        struct mmPollEvent* e = NULL;
        struct mmPollEvent* v = NULL;

        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);

        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent*) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        p->length = length;
    }
    else if (length > p->length)
    {
        mmUInt32_t i = 0;
        struct mmPollEvent* e = NULL;
        struct mmPollEvent* v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        p->length = length;

        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
    }
}

MM_EXPORT_NET mmUInt32_t mmPoll_GetLength(const struct mmPoll* p)
{
    return p->length;
}

MM_EXPORT_NET mmInt_t mmPoll_AddEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);

    mmInt_t code = 0;
    struct mmPollIocpOverlapped* overlapped;
    struct mmRbtreeU64VptIterator* it;
    it = mmRbtreeU64Vpt_GetIterator(&_impl->rbtree, (mmUInt64_t)fd);
    if (NULL != it)
    {
        overlapped = (struct mmPollIocpOverlapped*)(it->k);
        overlapped->mask = mask;
        overlapped->fd = fd;
        overlapped->ud = ud;
    }
    else
    {
        it = mmRbtreeU64Vpt_Add(&_impl->rbtree, (mmUInt64_t)fd);
        overlapped = (struct mmPollIocpOverlapped*)(it->v);
        overlapped->mask = mask;
        overlapped->fd = fd;
        overlapped->ud = ud;
    }
    code = mmPollIocp_PostMask(_impl, overlapped);
    return code;
}

MM_EXPORT_NET mmInt_t mmPoll_ModEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);

    mmInt_t code = 0;
    struct mmPollIocpOverlapped* overlapped;
    struct mmRbtreeU64VptIterator* it;
    it = mmRbtreeU64Vpt_GetIterator(&_impl->rbtree, (mmUInt64_t)fd);
    if (NULL != it)
    {
        overlapped = (struct mmPollIocpOverlapped*)(it->k);
        if (mask & MM_PE_READABLE) overlapped->mask |= MM_PE_READABLE;
        if (mask & MM_PE_WRITABLE) overlapped->mask |= MM_PE_WRITABLE;
    }
    else
    {
        code = -1;
    }
    return code;
}

MM_EXPORT_NET mmInt_t mmPoll_DelEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);

    mmInt_t code = 0;
    struct mmPollIocpOverlapped* overlapped;
    struct mmRbtreeU64VptIterator* it;
    it = mmRbtreeU64Vpt_GetIterator(&_impl->rbtree, (mmUInt64_t)fd);
    if (NULL != it)
    {
        overlapped = (struct mmPollIocpOverlapped*)(it->k);
        mask = (MM_PE_READABLE | MM_PE_WRITABLE) & (~mask);
        if (mask & MM_PE_READABLE) overlapped->mask |= MM_PE_READABLE;
        if (mask & MM_PE_WRITABLE) overlapped->mask |= MM_PE_WRITABLE;
    }
    else
    {
        code = -1;
    }
    return code;
}

MM_EXPORT_NET mmInt_t mmPoll_WaitEvent(struct mmPoll* p, mmMSec_t milliseconds)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);
    int n = 0;
    int i = 0;
    unsigned long lpNumberOfBytes = 0;
    ULONG_PTR lpCompletionKey = 0;
    LPOVERLAPPED lpOverlapped = NULL;
    BOOL rt = FALSE;
    mmInt_t code = 0;
    struct mmPollIocpOverlapped* overlapped;

    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);

    //BOOL WINAPI GetQueuedCompletionStatus(
    //  _In_  HANDLE       CompletionPort,
    //  _Out_ LPDWORD      lpNumberOfBytes,
    //  _Out_ PULONG_PTR   lpCompletionKey,
    //  _Out_ LPOVERLAPPED *lpOverlapped,
    //  _In_  DWORD        dwMilliseconds
    //  );
    rt = GetQueuedCompletionStatus(
        _impl->pfd, 
        &lpNumberOfBytes, 
        &lpCompletionKey, 
        &lpOverlapped, 
        (unsigned long)milliseconds);

    if (FALSE == rt)
    {
        mmErr_t errcode = mmErrno;

        switch (errcode)
        {
        case WAIT_TIMEOUT:
        {
            // WAIT_TIMEOUT
            // not error here.
            // not message.
            // not need post mask.
            n = 0;
            break;
        }
        case ERROR_MORE_DATA:
        {
            // ERROR_MORE_DATA the lpNumberOfBytes we use for 0, so have this error. we recv at after.
            // not error here.
            // have message.
            // need post mask.
            overlapped = (struct mmPollIocpOverlapped*)lpOverlapped;
            p->arrays[i].s = (void*)lpCompletionKey;
            p->arrays[i].mask = overlapped->mask;
            code = mmPollIocp_PostMask(_impl, overlapped);
            n = (0 == code) ? 1 : 0;
            break;
        }
        case ERROR_NETNAME_DELETED:
        {
            // ERROR_NETNAME_DELETED            64L
            // The specified network name is no longer available.
            //
            // at windows, this meaning the fd is can recv if fd is socket.
            // not error here.
            // have message.
            // not need post mask.
            overlapped = (struct mmPollIocpOverlapped*)lpOverlapped;
            p->arrays[i].s = (void*)lpCompletionKey;
            p->arrays[i].mask = overlapped->mask;
            n = 1;
            break;
        }
        default:
        {
            // error here.
            // not message.
            // not need post mask.
            //
            // not need logger here, logger outside.
            n = -1;
            break;
        }
        }
    }
    else
    {
        // if PostQueuedCompletionStatus was fire.the GetQueuedCompletionStatus return TRUE;
        // I/O logic.
        if (0 != lpCompletionKey)
        {
            // not error here.
            // have message.
            // need post mask.
            overlapped = (struct mmPollIocpOverlapped*)lpOverlapped;
            p->arrays[i].s = (void*)lpCompletionKey;
            p->arrays[i].mask = overlapped->mask;
            code = mmPollIocp_PostMask(_impl, overlapped);
            n = (0 == code) ? 1 : 0;
        }
        else
        {
            // not error here.
            // not have message.
            // not need post mask.
            n = 0;
        }
    }
    return n;
}

MM_EXPORT_NET void mmPoll_Start(struct mmPoll* p)
{

}

MM_EXPORT_NET void mmPoll_Interrupt(struct mmPoll* p)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);
    mmPollIocp_PostBreak(_impl, p->length);
}

MM_EXPORT_NET void mmPoll_Shutdown(struct mmPoll* p)
{
    struct mmPollIocp* _impl = (struct mmPollIocp*)(p->impl);
    mmPollIocp_PostBreak(_impl, p->length);
}

MM_EXPORT_NET void mmPoll_Join(struct mmPoll* p)
{

}

MM_EXPORT_NET const mmChar_t* mmPoll_Api(void)
{
    return "mmPollIocp";
}
