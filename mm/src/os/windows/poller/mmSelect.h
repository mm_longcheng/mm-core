/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSelect_h__
#define __mmSelect_h__

#include "core/mmCore.h"

#include "core/mmOSSocket.h"
#include "core/mmTime.h"

#include "net/mmNetExport.h"

#include "poller/mmPoll.h"

#include "core/mmPrefix.h"

struct mmSelect
{
    struct mmPollEvent poll_event;
    // event mask
    int mask;
    // read
    fd_set sr;
    // write
    fd_set sw;
    // fd.
    mmSocket_t fd;
};

MM_EXPORT_NET void mmSelect_Init(struct mmSelect* p);
MM_EXPORT_NET void mmSelect_Destroy(struct mmSelect* p);

MM_EXPORT_NET void mmSelect_SetFd(struct mmSelect* p, mmSocket_t fd);

MM_EXPORT_NET mmInt_t mmSelect_AddEvent(struct mmSelect* p, int mask, void* ud);
MM_EXPORT_NET mmInt_t mmSelect_ModEvent(struct mmSelect* p, int mask, void* ud);
MM_EXPORT_NET mmInt_t mmSelect_DelEvent(struct mmSelect* p, int mask, void* ud);
MM_EXPORT_NET mmInt_t mmSelect_WaitEvent(struct mmSelect* p, mmMSec_t milliseconds);

#include "core/mmSuffix.h"


#endif//__mmSelect_h__