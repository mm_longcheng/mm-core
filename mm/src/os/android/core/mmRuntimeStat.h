/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRuntimeStat_h__
#define __mmRuntimeStat_h__

#include "core/mmCore.h"
#include "core/mmTime.h"

#include "core/mmPrefix.h"

#define MM_PROC_MAX_LENGTH 1024

enum mmProcessState
{
    MM_PS_RU = 'R',// R:runnign, 
    MM_PS_SS = 'S',// S:sleeping (TASK_INTERRUPTIBLE), 
    MM_PS_DD = 'D',// D:disk sleep (TASK_UNINTERRUPTIBLE), 
    MM_PS_TS = 'T',// T:stopped, 
    MM_PS_TT = 't',// t:tracing stop,
    MM_PS_ZZ = 'Z',// Z:zombie, 
    MM_PS_XD = 'X',// X:dead
};
struct mmRuntimeLoadavg
{
    mmFloat32_t lavg_01;
    mmFloat32_t lavg_05;
    mmFloat32_t lavg_15;
    mmUInt32_t nr_running;
    mmUInt32_t nr_threads;
    mmUInt64_t last_pid;
};
MM_EXPORT_DLL void mmRuntimeLoadavg_Init(struct mmRuntimeLoadavg* p);
MM_EXPORT_DLL void mmRuntimeLoadavg_Destroy(struct mmRuntimeLoadavg* p);
MM_EXPORT_DLL void mmRuntimeLoadavg_Perform(struct mmRuntimeLoadavg* p);
// global.
struct mmRuntimeMachine
{
    // cpu
    mmUInt64_t cpu_user;
    mmUInt64_t cpu_nice;
    mmUInt64_t cpu_system;
    mmUInt64_t cpu_idle;
    // mem
    mmUInt64_t mem_total;// kb
    mmUInt64_t mem_free; // kb
    mmUInt64_t mem_buff; // kb
    mmUInt64_t mem_cache;// kb
    //
    mmUInt32_t cores;
};
MM_EXPORT_DLL void mmRuntimeMachine_Init(struct mmRuntimeMachine* p);
MM_EXPORT_DLL void mmRuntimeMachine_Destroy(struct mmRuntimeMachine* p);
MM_EXPORT_DLL void mmRuntimeMachine_Perform(struct mmRuntimeMachine* p);
MM_EXPORT_DLL void mmRuntimeMachine_PerformCpu(struct mmRuntimeMachine* p);
MM_EXPORT_DLL void mmRuntimeMachine_PerformMem(struct mmRuntimeMachine* p);

// process
struct mmRuntimeProcess
{
    // pid
    mmUInt64_t pid;
    // state mmProcessState
    mmUInt8_t state;
    // cpu
    mmUInt64_t cpu_utime;
    mmUInt64_t cpu_stime;
    mmUInt64_t cpu_cutime;
    mmUInt64_t cpu_cstime;
    // thread
    mmUInt32_t threads;
    // mem
    mmUInt64_t vm_size;
    mmUInt64_t vm_rss;
    mmUInt64_t vm_data;
    mmUInt64_t vm_stk;
    // io
    mmUInt64_t rchar;
    mmUInt64_t wchar;
};
MM_EXPORT_DLL void mmRuntimeProcess_Init(struct mmRuntimeProcess* p);
MM_EXPORT_DLL void mmRuntimeProcess_Destroy(struct mmRuntimeProcess* p);
MM_EXPORT_DLL void mmRuntimeProcess_Perform(struct mmRuntimeProcess* p);
MM_EXPORT_DLL void mmRuntimeProcess_PerformPid(struct mmRuntimeProcess* p);
MM_EXPORT_DLL void mmRuntimeProcess_PerformCpu(struct mmRuntimeProcess* p);
MM_EXPORT_DLL void mmRuntimeProcess_PerformMem(struct mmRuntimeProcess* p);
MM_EXPORT_DLL void mmRuntimeProcess_PerformIO(struct mmRuntimeProcess* p);

struct mmRuntimeStat
{
    mmUInt64_t pid;
    mmUInt32_t cores;
    mmUInt64_t vm_rss;
    mmUInt64_t rchar;
    mmUInt64_t wchar;
    //
    mmFloat32_t cpu_sys;
    mmFloat32_t cpu_pro;
    mmFloat32_t mem_pro;
    //
    mmUInt64_t timecode_o;
    mmUInt64_t timecode_n;
    // sampling time ms
    mmUInt64_t sampling;
    struct timeval tv_o;
    struct timeval tv_n;
    struct mmRuntimeLoadavg loadavg_o;
    struct mmRuntimeLoadavg loadavg_n;
    struct mmRuntimeMachine machine_o;
    struct mmRuntimeMachine machine_n;
    struct mmRuntimeProcess process_o;
    struct mmRuntimeProcess process_n;
};
MM_EXPORT_DLL void mmRuntimeStat_Init(struct mmRuntimeStat* p);
MM_EXPORT_DLL void mmRuntimeStat_Destroy(struct mmRuntimeStat* p);
MM_EXPORT_DLL void mmRuntimeStat_Update(struct mmRuntimeStat* p);

#include "core/mmSuffix.h"

#endif//__mmRuntimeStat_h__