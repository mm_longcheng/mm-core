/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOSContext_h__
#define __mmOSContext_h__

#include "core/mmCore.h"

#include "core/mmLogger.h"
#include "core/mmCPUInfo.h"

#include "core/mmPrefix.h"

// os context cores number. thread safe.
MM_EXPORT_DLL 
mmUInt32_t 
mmOSContextCPUCoresNumber(void);

MM_EXPORT_DLL
mmUInt32_t
mmOSContextCPUPageSize(void);

MM_EXPORT_DLL
int
mmOSContextStackIsUnbounded(void);

MM_EXPORT_DLL
size_t
mmOSContextStackMinimumSize(void);

MM_EXPORT_DLL
size_t
mmOSContextStackMaximumSize(void);

struct mmOSContext
{
    struct mmString name;
    struct mmCPUInfo cpuinfo;
    mmUInt32_t cacheline_size;
    mmUInt32_t cores;
    mmUInt32_t pagesize;
    mmUInt32_t pagesize_shift;
};

MM_EXPORT_DLL 
extern 
struct mmOSContext* 
mmOSContext_Instance(void);

MM_EXPORT_DLL
void
mmOSContext_Init(
    struct mmOSContext* p);

MM_EXPORT_DLL
void
mmOSContext_Destroy(
    struct mmOSContext* p);

MM_EXPORT_DLL
void
mmOSContext_Perform(
    struct mmOSContext* p);

MM_EXPORT_DLL
void
mmOSConsoleLogger(
    const char* section,
    const char* timestamp,
    int lvl,
    const char* message);

MM_EXPORT_DLL
int
mmOSConsolePrintf(
    int lvl,
    const char* tag,
    const char* fmt,
    ...)
    MM_STRING_ATTR_PRINTF(3, 4);

#include "core/mmSuffix.h"

#endif//__mmOSContext_h__