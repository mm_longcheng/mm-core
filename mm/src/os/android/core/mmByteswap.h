/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmByteswap_h__
#define __mmByteswap_h__

#include "core/mmCore.h"

#include <byteswap.h>

#include "core/mmPrefix.h"

#define mmByteswap16 bswap_16
#define mmByteswap32 bswap_32
#define mmByteswap64 bswap_64

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
// host <=> bigger
static mmInline mmUInt16_t mmHtoB16(mmUInt16_t v) { return (v); }
static mmInline mmUInt16_t mmBtoH16(mmUInt16_t v) { return (v); }
static mmInline mmUInt32_t mmHtoB32(mmUInt32_t v) { return (v); }
static mmInline mmUInt32_t mmBtoH32(mmUInt32_t v) { return (v); }
static mmInline mmUInt64_t mmHtoB64(mmUInt64_t v) { return (v); }
static mmInline mmUInt64_t mmBtoH64(mmUInt64_t v) { return (v); }
// host <=> little
static mmInline mmUInt16_t mmHtoL16(mmUInt16_t v) { return mmByteswap16(v); }
static mmInline mmUInt16_t mmLtoH16(mmUInt16_t v) { return mmByteswap16(v); }
static mmInline mmUInt32_t mmHtoL32(mmUInt32_t v) { return mmByteswap32(v); }
static mmInline mmUInt32_t mmLtoH32(mmUInt32_t v) { return mmByteswap32(v); }
static mmInline mmUInt64_t mmHtoL64(mmUInt64_t v) { return mmByteswap64(v); }
static mmInline mmUInt64_t mmLtoH64(mmUInt64_t v) { return mmByteswap64(v); }
#else
// host <=> bigger
static mmInline mmUInt16_t mmHtoB16(mmUInt16_t v) { return mmByteswap16(v); }
static mmInline mmUInt16_t mmBtoH16(mmUInt16_t v) { return mmByteswap16(v); }
static mmInline mmUInt32_t mmHtoB32(mmUInt32_t v) { return mmByteswap32(v); }
static mmInline mmUInt32_t mmBtoH32(mmUInt32_t v) { return mmByteswap32(v); }
static mmInline mmUInt64_t mmHtoB64(mmUInt64_t v) { return mmByteswap64(v); }
static mmInline mmUInt64_t mmBtoH64(mmUInt64_t v) { return mmByteswap64(v); }
// host <=> little
static mmInline mmUInt16_t mmHtoL16(mmUInt16_t v) { return (v); }
static mmInline mmUInt16_t mmLtoH16(mmUInt16_t v) { return (v); }
static mmInline mmUInt32_t mmHtoL32(mmUInt32_t v) { return (v); }
static mmInline mmUInt32_t mmLtoH32(mmUInt32_t v) { return (v); }
static mmInline mmUInt64_t mmHtoL64(mmUInt64_t v) { return (v); }
static mmInline mmUInt64_t mmLtoH64(mmUInt64_t v) { return (v); }
#endif

#define mmHtoB08(v) (v)
#define mmBtoH08(v) (v)
#define mmHtoL08(v) (v)
#define mmLtoH08(v) (v)

#include "core/mmSuffix.h"

#endif//__mmByteswap_h__
