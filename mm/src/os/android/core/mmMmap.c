/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

// include the sysroot first, avoid warning.
#include <sys/mman.h>

#include "mmMmap.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmErrno.h"
#include "core/mmFileSystem.h"

MM_EXPORT_DLL void mmMmap_Init(struct mmMmap* p)
{
    mmString_Init(&p->path);
    p->hfd = (void*)MM_INVALID_HANDLE_VALUE;
    p->hmmap = MM_MMAP_MAP_FAILED;
    p->buffer = NULL;
    p->offset = 0;
    p->length = 0;
    p->flag = MM_MMAP_RDONLY;
}
MM_EXPORT_DLL void mmMmap_Destroy(struct mmMmap* p)
{
    mmMmap_UnmappingView(p);
    mmMmap_UnmappingFile(p);
    mmMmap_Fclose(p);
    //
    mmString_Destroy(&p->path);
    p->hfd = (void*)MM_INVALID_HANDLE_VALUE;
    p->hmmap = MM_MMAP_MAP_FAILED;
    p->buffer = NULL;
    p->offset = 0;
    p->length = 0;
    p->flag = MM_MMAP_RDONLY;
}
MM_EXPORT_DLL void mmMmap_SetPath(struct mmMmap* p, const char* path)
{
    mmString_Assigns(&p->path, path);
}
MM_EXPORT_DLL void mmMmap_SetOffset(struct mmMmap* p, off_t offset)
{
    p->offset = offset;
}
MM_EXPORT_DLL void mmMmap_SetLength(struct mmMmap* p, mmUInt64_t length)
{
    p->length = length;
}
MM_EXPORT_DLL void mmMmap_SetFlag(struct mmMmap* p, int flag)
{
    p->flag = flag;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Fopen(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        assert((void*)MM_INVALID_HANDLE_VALUE == p->hfd && "unmapping before mapping.");

        if (mmString_Empty(&p->path))
        {
            // file path is empty.
            code = MM_FAILURE;
            break;
        }

        p->hfd = (void*)((uintptr_t)open(mmString_CStr(&p->path), p->flag, 0x0774));

        if ((void*)MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // CreateFile by file path is failure.
            code = MM_FAILURE;
            break;
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Fclose(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    if ((void*)MM_INVALID_HANDLE_VALUE != p->hfd)
    {
        int bt = -1;
        bt = close((int)((uintptr_t)p->hfd));
        p->hfd = (void*)MM_INVALID_HANDLE_VALUE;
        code = (0 == bt) ? MM_SUCCESS : MM_FAILURE;
    }
    else
    {
        code = MM_SUCCESS;
    }

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Fseek(struct mmMmap* p, mmUInt64_t length)
{
    mmUInt32_t code = MM_UNKNOWN;

    mmUInt64_t bt = -1;

    bt = lseek((int)((uintptr_t)p->hfd), length, SEEK_SET);
    code = (-1 == bt) ? MM_FAILURE : MM_SUCCESS;

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_ObtainFilesize(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        struct stat statbuf;

        if ((void*)MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // file is not open.
            code = MM_FAILURE;
            break;
        }

        if (fstat((int)((uintptr_t)p->hfd), &statbuf) < 0)
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // the file descriptor is can not get size.
            code = MM_FAILURE;
            break;
        }

        p->length = (mmUInt64_t)statbuf.st_size;

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_MappingFile(struct mmMmap* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        int prot = PROT_NONE;
        mmUInt32_t flag_access = p->flag & 0x00000003;

        if ((void*)MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // file is not open.
            code = MM_FAILURE;
            break;
        }
        assert(MM_MMAP_MAP_FAILED == p->hmmap && "unmapping before mapping.");

        if (MM_MMAP_RDONLY == flag_access) { prot = PROT_READ; }
        if (MM_MMAP_WRONLY == flag_access) { prot = PROT_WRITE; }
        if (MM_MMAP_RDWR == flag_access) { prot = (PROT_READ | PROT_WRITE); }

        p->hmmap = mmap(
            NULL,
            (size_t)p->length,
            prot,
            MAP_SHARED,
            (int)((uintptr_t)p->hfd),
            0);

        if (MM_MMAP_MAP_FAILED == p->hmmap)
        {
            // error.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            // the file descriptor mapping failure.
            code = MM_FAILURE;
            break;
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_UnmappingFile(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    if (MM_MMAP_MAP_FAILED != p->hmmap)
    {
        int bt = -1;
        bt = munmap(p->hmmap, (size_t)p->length);
        p->hmmap = MM_MMAP_MAP_FAILED;
        code = (0 == bt) ? MM_SUCCESS : MM_FAILURE;
    }
    else
    {
        code = MM_SUCCESS;
    }

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_MappingView(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        if ((void*)MM_INVALID_HANDLE_VALUE == p->hfd)
        {
            // file is not open.
            code = MM_FAILURE;
            break;
        }
        if (MM_MMAP_MAP_FAILED == p->hmmap)
        {
            // file is not mapping.
            code = MM_FAILURE;
            break;
        }

        p->buffer = (mmUInt8_t*)p->hmmap;

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_UnmappingView(struct mmMmap* p)
{
    p->buffer = NULL;
    return MM_SUCCESS;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Mapping(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        code = mmMmap_Fopen(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_ObtainFilesize(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_MappingFile(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_MappingView(p);
        if (MM_SUCCESS != code) { break; }
        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Unmapping(struct mmMmap* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        code = mmMmap_UnmappingView(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_UnmappingFile(p);
        if (MM_SUCCESS != code) { break; }
        code = mmMmap_Fclose(p);
        if (MM_SUCCESS != code) { break; }
        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_DLL mmUInt32_t mmMmap_Flush(struct mmMmap* p, void* ptr, size_t size)
{
    mmUInt32_t code = MM_UNKNOWN;

    int bt = -1;
    bt = msync(ptr, size, MS_ASYNC);
    code = (0 == bt) ? MM_SUCCESS : MM_FAILURE;

    return code;
}
MM_EXPORT_DLL void mmMmap_Memcpy(struct mmMmap* p, struct mmMmap* t, size_t offset, size_t length)
{
    mmMemcpy(t->buffer + offset, p->buffer + offset, length);
}