/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "core/mmPrefix.h"

#ifndef MM_HAVE_UNISTD_H
#define MM_HAVE_UNISTD_H  1
#endif


#ifndef MM_HAVE_INTTYPES_H
#define MM_HAVE_INTTYPES_H  1
#endif


#ifndef MM_HAVE_LIMITS_H
#define MM_HAVE_LIMITS_H  1
#endif


#ifndef MM_HAVE_SYS_PARAM_H
#define MM_HAVE_SYS_PARAM_H  1
#endif


#ifndef MM_HAVE_SYS_MOUNT_H
#define MM_HAVE_SYS_MOUNT_H  1
#endif


#ifndef MM_HAVE_SYS_STATVFS_H
#define MM_HAVE_SYS_STATVFS_H  1
#endif


#ifndef MM_HAVE_CRYPT_H
#define MM_HAVE_CRYPT_H  1
#endif


#ifndef MM_LINUX
#define MM_LINUX  1
#endif


#ifndef MM_HAVE_SYS_PRCTL_H
#define MM_HAVE_SYS_PRCTL_H  1
#endif


#ifndef MM_HAVE_SYS_VFS_H
#define MM_HAVE_SYS_VFS_H  1
#endif

#include "core/mmSuffix.h"