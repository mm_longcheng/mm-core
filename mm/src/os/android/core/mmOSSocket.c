/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOSSocket.h"
#include "core/mmLogger.h"

/*
 * ioctl(FIONBIO) sets a non-blocking mode with the single syscall
 * while fcntl(F_SETFL, O_NONBLOCK) needs to learn the current state
 * using fcntl(F_GETFL).
 *
 * ioctl() and fcntl() are syscalls at least in FreeBSD 2.x, Linux 2.2
 * and Solaris 7.
 *
 * ioctl() in Linux 2.4 and 2.6 uses BKL, however, fcntl(F_SETFL) uses it too.
 */


#if (MM_HAVE_FIONBIO)

MM_EXPORT_DLL int mmNonblocking(mmSocket_t s)
{
    int  nb;

    nb = 1;

    return ioctl(s, FIONBIO, &nb);
}


MM_EXPORT_DLL int mmBlocking(mmSocket_t s)
{
    int  nb;

    nb = 0;

    return ioctl(s, FIONBIO, &nb);
}

#endif


#if (MM_FREEBSD)

MM_EXPORT_DLL int mmTcpNopush(mmSocket_t s)
{
    int  tcp_nopush;

    tcp_nopush = 1;

    return setsockopt(s, IPPROTO_TCP, TCP_NOPUSH,
        (const void *)&tcp_nopush, sizeof(int));
}


MM_EXPORT_DLL int mmTcpPush(mmSocket_t s)
{
    int  tcp_nopush;

    tcp_nopush = 0;

    return setsockopt(s, IPPROTO_TCP, TCP_NOPUSH,
        (const void *)&tcp_nopush, sizeof(int));
}

#elif (MM_LINUX)


MM_EXPORT_DLL int mmTcpNopush(mmSocket_t s)
{
    int  cork;

    cork = 1;

    return setsockopt(s, IPPROTO_TCP, TCP_CORK,
        (const void *)&cork, sizeof(int));
}


MM_EXPORT_DLL int mmTcpPush(mmSocket_t s)
{
    int  cork;

    cork = 0;

    return setsockopt(s, IPPROTO_TCP, TCP_CORK,
        (const void *)&cork, sizeof(int));
}

#else

MM_EXPORT_DLL int mmTcpNopush(mmSocket_t s)
{
    return 0;
}


MM_EXPORT_DLL int mmTcpPush(mmSocket_t s)
{
    return 0;
}

#endif

MM_EXPORT_DLL int mmSocketKeepalive(mmSocket_t s, int flag)
{
    if (0 != setsockopt(s, SOL_SOCKET, SO_KEEPALIVE, (char*)&flag, sizeof(flag)))
    {
        mmErr_t errcode = mmErrno;
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        mmLogger_LogE(gLogger, "%s %d can not keepalive fd:%d", __FUNCTION__, __LINE__, s);
        return MM_ERROR;
    }
    return MM_OK;
}

MM_EXPORT_DLL int mmSocketKeepaliveCtl(mmSocket_t s, int tcp_keepidle, int tcp_keepintvl, int tcp_keepcnt)
{
    if (0 != setsockopt(s, SOL_TCP, TCP_KEEPIDLE, (void *)&tcp_keepidle, sizeof(tcp_keepidle)))
    {
        mmErr_t errcode = mmErrno;
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        mmLogger_LogE(gLogger, "%s %d can not keepalive ctl idle fd:%d", __FUNCTION__, __LINE__, s);
        return MM_ERROR;
    }
    if (0 != setsockopt(s, SOL_TCP, TCP_KEEPINTVL, (void *)&tcp_keepintvl, sizeof(tcp_keepintvl)))
    {
        mmErr_t errcode = mmErrno;
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        mmLogger_LogE(gLogger, "%s %d can not keepalive ctl intvl fd:%d", __FUNCTION__, __LINE__, s);
        return MM_ERROR;
    }
    if (0 != setsockopt(s, SOL_TCP, TCP_KEEPCNT, (void *)&tcp_keepcnt, sizeof(tcp_keepcnt)))
    {
        mmErr_t errcode = mmErrno;
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        mmLogger_LogE(gLogger, "%s %d can not keepalive ctl cnt fd:%d", __FUNCTION__, __LINE__, s);
        return MM_ERROR;
    }
    return MM_OK;
}