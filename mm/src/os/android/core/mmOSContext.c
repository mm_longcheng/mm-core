/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOSContext.h"
#include "core/mmAlloc.h"
#include "core/mmOSSocket.h"
#include "core/mmLogger.h"
#include "core/mmErrno.h"
#include "core/mmTime.h"
#include "core/mmCPUInfo.h"

// os context cores number. thread safe.
MM_EXPORT_DLL 
mmUInt32_t 
mmOSContextCPUCoresNumber(void)
{
    static mmBool_t gUpdate = MM_FALSE;
    static mmUInt32_t gCores = -1;
    if (MM_FALSE == gUpdate)
    {
        gUpdate = MM_TRUE;
        // twice is fine.
        gCores = (mmUInt32_t)sysconf(_SC_NPROCESSORS_ONLN);
    }
    return gCores;
}

#if !defined (MINSIGSTKSZ)
# define MINSIGSTKSZ (131072) // 128kb recommended stack size
# define UDEF_MINSIGSTKSZ
#endif

static rlim_t mmOSContextStacklimitSize(void)
{
    static mmBool_t gUpdate = MM_FALSE;
    static rlim_t gLimit = MINSIGSTKSZ;
    if (MM_FALSE == gUpdate)
    {
        struct rlimit limit;
        // conforming to POSIX.1-2001
        gUpdate = MM_TRUE;
        getrlimit(RLIMIT_STACK, &limit);
        gLimit = limit.rlim_max;
    }
    return gLimit;
}

MM_EXPORT_DLL
mmUInt32_t
mmOSContextCPUPageSize(void)
{
    static mmBool_t gUpdate = MM_FALSE;
    static mmUInt32_t gPageSize = -1;
    if (MM_FALSE == gUpdate)
    {
        gUpdate = MM_TRUE;
        gPageSize = (mmUInt32_t)sysconf(_SC_PAGESIZE);
    }
    return gPageSize;
}

MM_EXPORT_DLL
int
mmOSContextStackIsUnbounded(void)
{
    return (RLIM_INFINITY == mmOSContextStacklimitSize());
}

MM_EXPORT_DLL
size_t
mmOSContextStackMinimumSize(void)
{
    return MINSIGSTKSZ;
}

MM_EXPORT_DLL
size_t
mmOSContextStackMaximumSize(void)
{
    return (size_t)mmOSContextStacklimitSize();
}

#ifdef UDEF_MINSIGSTKSZ
# undef MINSIGSTKSZ
#endif

#ifndef LOGGER_ALL_MSG
#define LOGGER_ALL_MSG 1
#endif//LOGGER_ALL_MSG
//
#include <android/log.h>

#if defined( NDEBUG ) || ( LOGGER_ALL_MSG == 1 )
#  define LOGSV(_section,...)  ((void)__android_log_print(ANDROID_LOG_VERBOSE, (_section), __VA_ARGS__))
#else
#  define LOGSV(_section,...)  ((void)0)
#endif

#define LOGSD(_section,...) ((void)__android_log_print(ANDROID_LOG_DEBUG  , (_section), __VA_ARGS__))
#define LOGSI(_section,...) ((void)__android_log_print(ANDROID_LOG_INFO   , (_section), __VA_ARGS__))
#define LOGSW(_section,...) ((void)__android_log_print(ANDROID_LOG_WARN   , (_section), __VA_ARGS__))
#define LOGSE(_section,...) ((void)__android_log_print(ANDROID_LOG_ERROR  , (_section), __VA_ARGS__))
#define LOGSA(_section,...) ((void)__android_log_print(ANDROID_LOG_ASSERT , (_section), __VA_ARGS__))

static const int gAndroidLogPrioritys[] =
{
    ANDROID_LOG_UNKNOWN, //MM_LOG_UNKNOWN =  0,
    ANDROID_LOG_FATAL  , //MM_LOG_FATAL   =  1,
    ANDROID_LOG_FATAL  , //MM_LOG_CRIT    =  2,
    ANDROID_LOG_ERROR  , //MM_LOG_ERROR   =  3,
    ANDROID_LOG_ERROR  , //MM_LOG_ALERT   =  4,
    ANDROID_LOG_WARN   , //MM_LOG_WARNING =  5,
    ANDROID_LOG_WARN   , //MM_LOG_NOTICE  =  6,
    ANDROID_LOG_INFO   , //MM_LOG_INFO    =  7,
    ANDROID_LOG_INFO   , //MM_LOG_TRACE   =  8,
    ANDROID_LOG_DEBUG  , //MM_LOG_DEBUG   =  9,
    ANDROID_LOG_VERBOSE, //MM_LOG_VERBOSE = 10,
};

static int GetAndroidLogPriority(int lvl)
{
    if (MM_LOG_UNKNOWN <= lvl && lvl <= MM_LOG_VERBOSE)
    {
        return gAndroidLogPrioritys[lvl];
    }
    else
    {
        return ANDROID_LOG_VERBOSE;
    }
}

static void mmOSContext_PerformCachelineSize(struct mmOSContext* p)
{
    const char* vendor = mmString_CStr(&p->cpuinfo.vendor);
    if (mmStrcmp(vendor, "GenuineIntel") == 0)
    {
        uint32_t cpu_eax = p->cpuinfo.level >> 32;
        uint32_t model;
        switch ((cpu_eax & 0xf00) >> 8)
        {
            /* Pentium */
        case 5:
            p->cacheline_size = 32;
            break;

            /* Pentium Pro, II, III */
        case 6:
            p->cacheline_size = 32;

            model = ((cpu_eax & 0xf0000) >> 8) | (cpu_eax & 0xf0);

            if (model >= 0xd0)
            {
                /* Intel Core, Core 2, Atom */
                p->cacheline_size = 64;
            }
            break;

            /*
             * Pentium 4, although its cache line size is 64 bytes,
             * it prefetches up to two cache lines during memory read
             */
        case 15:
            p->cacheline_size = 128;
            break;
        }

    }
    else if (mmStrcmp(vendor, "AuthenticAMD") == 0)
    {
        p->cacheline_size = 64;
    }
}

static struct mmOSContext gOSContext =
{
    mmString_Null,
    {
        mmString_Null,
        mmString_Null,
        0, 0,
    },
    0, 0, 0, 0,
};

MM_EXPORT_DLL 
extern 
struct mmOSContext* 
mmOSContext_Instance(void)
{
    return &gOSContext;
}

MM_EXPORT_DLL
void
mmOSContext_Init(
    struct mmOSContext* p)
{
    mmString_Init(&p->name);
    mmCPUInfo_Init(&p->cpuinfo);
    p->cacheline_size = 64;
    p->cores = 1;
    p->pagesize = 4096;
    p->pagesize_shift = 12;
    //
    mmStrError_Init();
    mmString_Assigns(&p->name, "android");
}

MM_EXPORT_DLL
void
mmOSContext_Destroy(
    struct mmOSContext* p)
{
    mmString_Destroy(&p->name);
    mmCPUInfo_Destroy(&p->cpuinfo);
}

MM_EXPORT_DLL
void
mmOSContext_Perform(
    struct mmOSContext* p)
{
    mmUInt_t n;
    struct mmLogger* gLogger = mmLogger_Instance();
    p->pagesize = (mmUInt32_t)sysconf(_SC_PAGESIZE);
    p->cores = (mmUInt32_t)sysconf(_SC_NPROCESSORS_ONLN);
    p->cacheline_size = MM_CPU_CACHE_LINE;
    mmCPUInfo_Perform(&p->cpuinfo);
    for (n = p->pagesize; n >>= 1; p->pagesize_shift++) { /* void */ }
    mmOSContext_PerformCachelineSize(p);
    //
    mmLogger_LogI(gLogger, "name           :%s", mmString_CStr(&p->name));
    mmLogger_LogI(gLogger, "cpuinfo.vendor :%s", mmString_CStr(&p->cpuinfo.vendor));
    mmLogger_LogI(gLogger, "cpuinfo.brand  :%s", mmString_CStr(&p->cpuinfo.brand));
    mmLogger_LogI(gLogger, "cpuinfo.level  :%" PRIu64, p->cpuinfo.level);
    mmLogger_LogI(gLogger, "cpuinfo.flags  :%" PRIu64, p->cpuinfo.flags);
    mmLogger_LogI(gLogger, "cacheline_size :%u", p->cacheline_size);
    mmLogger_LogI(gLogger, "cores          :%u", p->cores);
    mmLogger_LogI(gLogger, "pagesize       :%u", p->pagesize);
    mmLogger_LogI(gLogger, "pagesize_shift :%u", p->pagesize_shift);
}

MM_EXPORT_DLL
void
mmOSConsoleLogger(
    const char* section,
    const char* timestamp,
    int lvl,
    const char* message)
{
    // [1992/01/26 09:13:14-520 8 V ]
    const struct mmLevelMark* mark = mmLoggerLevelMark(lvl);
    const char* m = mmString_CStr(&mark->m);
    int prio = GetAndroidLogPriority(lvl);
    // android logger timestamp self.
    __android_log_print(prio, section, "%d %s %s", lvl, m, message);
}

MM_EXPORT_DLL
int
mmOSConsolePrintf(
    int lvl,
    const char* tag,
    const char* fmt,
    ...)
{
    va_list ap;
    int l;
    int prio = GetAndroidLogPriority(lvl);
    va_start(ap, fmt);
    l = __android_log_vprint(prio, tag, fmt, ap);
    va_end(ap);
    return l;
}