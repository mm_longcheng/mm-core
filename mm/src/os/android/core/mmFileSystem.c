/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

// include the sysroot first, avoid warning.
#include <sys/sendfile.h>

#include "mmFileSystem.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"

struct _find_search_t
{
    char *pattern;
    char *curfn;
    char *directory;
    int dirlen;
    DIR *dirfd;
};
MM_EXPORT_DLL intptr_t mmFindFirst(const char* pattern, struct _finddata_t* data)
{
    const char *mask = NULL;
    struct _find_search_t *fs = (struct _find_search_t*)mmMalloc(sizeof(struct _find_search_t));
    fs->curfn = NULL;
    fs->pattern = NULL;

    // Separate the mask from directory name
    mask = mmStrrchr(pattern, '/');
    if (mask)
    {
        fs->dirlen = (int)(mask - pattern);
        mask++;
        fs->directory = (char *)mmMalloc(fs->dirlen + 1);
        mmMemcpy(fs->directory, pattern, fs->dirlen);
        fs->directory[fs->dirlen] = 0;
    }
    else
    {
        mask = pattern;
        fs->directory = mmStrdup(".");
        fs->dirlen = 1;
    }

    fs->dirfd = opendir(fs->directory);
    if (!fs->dirfd)
    {
        mmFindClose((intptr_t)fs);
        return -1;
    }

    /* Hack for "*.*" -> "*' from DOS/Windows */
    if (mmStrcmp(mask, "*.*") == 0)
        mask += 2;
    fs->pattern = mmStrdup(mask);

    /* Get the first entry */
    if (mmFindNext((intptr_t)fs, data) < 0)
    {
        mmFindClose((intptr_t)fs);
        return -1;
    }

    return (intptr_t)fs;
}
MM_EXPORT_DLL int mmFindNext(intptr_t id, struct _finddata_t* data)
{
    struct _find_search_t *fs = (struct _find_search_t*)(id);
    size_t namelen = 0;
    size_t xfn_sz = 0;
    char* xfn = NULL;
    struct stat stat_buf;
    /* Loop until we run out of entries or find the next one */
    struct dirent *entry;
    for (;;)
    {
        if (!(entry = readdir(fs->dirfd)))
            return -1;

        /* See if the filename matches our pattern */
        if (fnmatch(fs->pattern, entry->d_name, 0) == 0)
            break;
    }

    if (fs->curfn)
        mmFree(fs->curfn);
    data->name = fs->curfn = mmStrdup(entry->d_name);

    namelen = mmStrlen(entry->d_name);
    xfn_sz = fs->dirlen + 1 + namelen + 1;
    xfn = (char*)mmMalloc(xfn_sz);
    mmMemset(xfn, 0, xfn_sz);
    mmSprintf(xfn, "%s/%s", fs->directory, entry->d_name);

    /* stat the file to get if it's a subdir and to find its length */
    if (stat(xfn, &stat_buf))
    {
        // Hmm strange, imitate a zero-length file then
        data->attrib = _A_NORMAL;
        data->size = 0;
    }
    else
    {
        if (S_ISDIR(stat_buf.st_mode))
            data->attrib = _A_SUBDIR;
        else
            /* Default type to a normal file */
            data->attrib = _A_NORMAL;

        data->size = (unsigned long)stat_buf.st_size;
    }

    mmFree(xfn);

    /* Files starting with a dot are hidden files in Unix */
    if (data->name[0] == '.')
        data->attrib |= _A_HIDDEN;

    return 0;
}
MM_EXPORT_DLL int mmFindClose(intptr_t id)
{
    int ret;
    struct _find_search_t *fs = (struct _find_search_t*)(id);

    ret = fs->dirfd ? closedir(fs->dirfd) : 0;
    mmFree(fs->pattern);
    mmFree(fs->directory);
    if (fs->curfn)
        mmFree(fs->curfn);
    mmFree(fs);

    return ret;
}
MM_EXPORT_DLL int mmPathMatch(const char* path, const char* pattern)
{
    return 0 == fnmatch(pattern, path, 0);
}

MM_EXPORT_DLL int mmIsReservedDirectory(const char *path)
{
    return (path[0] == '.' && (path[1] == 0 || (path[1] == '.' && path[2] == 0)));
}
MM_EXPORT_DLL int mmIsAbsolutePath(const char* path)
{
    return NULL != path && '/' == path[0];
}
MM_EXPORT_DLL int mmAbsolutePathIsDataExists(const char* file_name)
{
    return 0 == access(file_name, F_OK) ? 1 : 0;
}
MM_EXPORT_DLL int mmAccess(const char* file_name, int mode)
{
    return access(file_name, mode);
}

MM_EXPORT_DLL mmUInt32_t mmStat_Mode(mmStat_t* s)
{
    return s->st_mode;
}

MM_EXPORT_DLL int mmMkdir(const char *pathname, int mode)
{
    return mkdir(pathname, mode);
}
MM_EXPORT_DLL int mmRmdir(const char* pathname)
{
    return rmdir(pathname);
}
MM_EXPORT_DLL int mmUnlink(const char* pathname)
{
    return unlink(pathname);
}
MM_EXPORT_DLL int mmLink(const char* oldname, const char* newname)
{
    return link(oldname, newname);
}

// api for file system.
MM_EXPORT_DLL const char* mmFileSystemApi(void)
{
    return "mmFileSystemAndroid";
}

MM_EXPORT_DLL int mmCopyFile(const char* path_f, const char* path_t)
{
    int code = -1;
    int i, o;

    off_t bytes_copied = 0;
    mmStat_t s;

    do
    {
        if ((i = open(path_f, O_RDONLY)) == -1)
        {
            code = -1;
            break;
        }

        if (0 != mmFstat(i, &s))
        {
            close(i);
            code = -1;
            break;
        }

        if ((o = open(path_t, O_RDWR | O_CREAT, s.st_mode)) == -1)
        {
            close(i);
            code = -1;
            break;
        }

        //Here we use kernel-space copying for performance reasons
        //sendfile will work with non-socket output (i.e. regular file) on Linux 2.6.33+
        code = sendfile(o, i, &bytes_copied, s.st_size);

        close(i);
        close(o);
    } while (0);

    return code;
}
MM_EXPORT_DLL int mmRename(const char* path_f, const char* path_t)
{
    return rename(path_f, path_t);
}
MM_EXPORT_DLL int mmRemove(const char* path)
{
    return remove(path);
}
MM_EXPORT_DLL int mmRecursiveCopy(const char* f, const char* t)
{
    mmStat_t s;
    mmUInt32_t mode;
    int code = -1;

    do
    {
        if (0 != mmAccess(f, MM_ACCESS_F_OK))
        {
            // the source item is not exist.
            code = -1;
            break;
        }

        if (0 != mmStat(f, &s))
        {
            // the source item can not stat.
            code = -1;
            break;
        }

        mode = mmStat_Mode(&s);

        if (MM_S_ISREG(mode))
        {
            mmCopyFile(f, t);
        }
        else
        {
            if (0 != mmMkdir(t, s.st_mode))
            {
                // mkdir failure.
                code = -1;
                break;
            }
            else
            {
                intptr_t data_handle, res;
                struct _finddata_t find_data;

                struct mmString full_pattern;
                struct mmString f_filename;
                struct mmString t_filename;

                mmString_Init(&full_pattern);
                mmString_Init(&f_filename);
                mmString_Init(&t_filename);

                mmString_Assigns(&full_pattern, f);
                mmString_Appends(&full_pattern, "/");
                mmString_Appends(&full_pattern, "*");

                data_handle = mmFindFirst(mmString_CStr(&full_pattern), &find_data);
                res = 0;
                while (data_handle != -1 && res != -1)
                {
                    if (0 == mmIsReservedDirectory(find_data.name))
                    {
                        mmString_Assigns(&f_filename, f);
                        mmString_Appends(&f_filename, "/");
                        mmString_Appends(&f_filename, find_data.name);

                        mmString_Assigns(&t_filename, t);
                        mmString_Appends(&t_filename, "/");
                        mmString_Appends(&t_filename, find_data.name);

                        mmRecursiveCopy(mmString_CStr(&f_filename), mmString_CStr(&t_filename));
                    }
                    res = mmFindNext(data_handle, &find_data);
                }
                // Close if we found any files
                if (data_handle != -1)
                {
                    mmFindClose(data_handle);
                }

                mmString_Destroy(&full_pattern);
                mmString_Destroy(&f_filename);
                mmString_Destroy(&t_filename);
            }
        }

        code = 0;
    } while (0);
    return code;
}
MM_EXPORT_DLL int mmRecursiveRemove(const char* path)
{
    mmStat_t s;
    mmUInt32_t mode;
    int code = -1;

    do
    {
        if (0 != mmAccess(path, MM_ACCESS_F_OK))
        {
            // the source item is not exist.
            code = -1;
            break;
        }

        if (0 != mmStat(path, &s))
        {
            // the source item can not stat.
            code = -1;
            break;
        }

        mode = mmStat_Mode(&s);

        if (MM_S_ISREG(mode))
        {
            mmRemove(path);
        }
        else
        {
            intptr_t data_handle, res;
            struct _finddata_t find_data;

            struct mmString full_pattern;
            struct mmString f_filename;

            mmString_Init(&full_pattern);
            mmString_Init(&f_filename);

            mmString_Assigns(&full_pattern, path);
            mmString_Appends(&full_pattern, "/");
            mmString_Appends(&full_pattern, "*");

            data_handle = mmFindFirst(mmString_CStr(&full_pattern), &find_data);
            res = 0;
            while (data_handle != -1 && res != -1)
            {
                if (0 == mmIsReservedDirectory(find_data.name))
                {
                    mmString_Assigns(&f_filename, path);
                    mmString_Appends(&f_filename, "/");
                    mmString_Appends(&f_filename, find_data.name);

                    mmRecursiveRemove(mmString_CStr(&f_filename));
                }
                res = mmFindNext(data_handle, &find_data);
            }
            // Close if we found any files
            if (data_handle != -1)
            {
                mmFindClose(data_handle);
            }

            mmRmdir(path);

            mmString_Destroy(&full_pattern);
            mmString_Destroy(&f_filename);
        }

        code = 0;
    } while (0);
    return code;
}

MM_EXPORT_DLL int mmFileTruncate(FILE* f, mmUInt64_t length)
{
    int fd = fileno(f);
    return ftruncate(fd, length);
}

MM_EXPORT_DLL int fseeko(FILE* stream, off_t offset, int whence)
{
    return fseek(stream, (long)offset, whence);
}

MM_EXPORT_DLL off_t ftello(FILE* stream)
{
    return (off_t)ftell(stream);
}

MM_EXPORT_DLL int fseeko64(FILE* stream, int64_t offset, int whence)
{
    return fseek(stream, (long)offset, whence);
}

MM_EXPORT_DLL int64_t ftello64(FILE* stream)
{
    return (int64_t)ftell(stream);
}
