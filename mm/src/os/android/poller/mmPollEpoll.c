/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoll.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"

struct mmPollEpoll
{
    mmSocket_t pfd;
    struct epoll_event* ev;
};

MM_EXPORT_NET const mmChar_t* mmPoll_Api(void)
{
    return "mmPollEpoll";
}

MM_EXPORT_NET void mmPoll_Init(struct mmPoll* p)
{
    struct mmPollEpoll* _impl = NULL;
    p->length = MM_POLL_EVENT_DEFAULT_NUMBER;
    p->arrays = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * p->length);
    _impl = (struct mmPollEpoll*)mmMalloc(sizeof(struct mmPollEpoll));
    _impl->pfd = epoll_create(1024);
    _impl->ev = (struct epoll_event*)mmMalloc(sizeof(struct epoll_event) * p->length);
    p->impl = _impl;
    //
    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
    mmMemset(_impl->ev, 0, sizeof(struct epoll_event) * p->length);
}
MM_EXPORT_NET void mmPoll_Destroy(struct mmPoll* p)
{
    struct mmPollEpoll* _impl = (struct mmPollEpoll*)(p->impl);
    mmCloseSocket(_impl->pfd);
    mmFree(_impl->ev);
    mmFree(p->arrays);
    mmFree(_impl);
    //
    p->length = 0;
    p->arrays = NULL;
    p->impl = NULL;
}
MM_EXPORT_NET void mmPoll_SetLength(struct mmPoll* p, mmUInt32_t length)
{
    struct mmPollEpoll* _impl = (struct mmPollEpoll*)(p->impl);
    if (length < p->length)
    {
        struct mmPollEvent* v = NULL;
        struct epoll_event* ep_v = NULL;
        //
        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
        mmMemset(_impl->ev, 0, sizeof(struct epoll_event) * p->length);
        //
        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * length);
        mmFree(p->arrays);
        p->arrays = v;
        //
        ep_v = (struct epoll_event*)mmMalloc(sizeof(struct epoll_event) * length);
        mmMemcpy(ep_v, _impl->ev, sizeof(struct epoll_event) * length);
        mmFree(_impl->ev);
        _impl->ev = ep_v;
        //
        p->length = length;
    }
    else if (length > p->length)
    {
        struct mmPollEvent* v = NULL;
        struct epoll_event* ev = NULL;
        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        //
        ev = (struct epoll_event*)mmMalloc(sizeof(struct epoll_event) * length);
        mmMemcpy(ev, _impl->ev, sizeof(struct epoll_event) * p->length);
        mmFree(_impl->ev);
        _impl->ev = ev;
        //
        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * length);
        mmMemset(_impl->ev, 0, sizeof(struct epoll_event) * length);
        //
        p->length = length;
    }
}
MM_EXPORT_NET mmUInt32_t mmPoll_GetLength(const struct mmPoll* p)
{
    return p->length;
}
MM_EXPORT_NET mmInt_t mmPoll_AddEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollEpoll* _impl = (struct mmPollEpoll*)(p->impl);
    struct epoll_event ev = { 0, {0} };
    if (mask & MM_PE_READABLE) ev.events |= EPOLLIN;
    if (mask & MM_PE_WRITABLE) ev.events |= EPOLLOUT;
    ev.events |= EPOLLET;
    ev.data.ptr = ud;
    if (epoll_ctl(_impl->pfd, EPOLL_CTL_ADD, fd, &ev) == -1) return -1;
    return 0;
}
MM_EXPORT_NET mmInt_t mmPoll_ModEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollEpoll* _impl = (struct mmPollEpoll*)(p->impl);
    struct epoll_event ev = { 0, {0} };
    if (mask & MM_PE_READABLE) ev.events |= EPOLLIN;
    if (mask & MM_PE_WRITABLE) ev.events |= EPOLLOUT;
    ev.events |= EPOLLET;
    ev.data.ptr = ud;
    if (epoll_ctl(_impl->pfd, EPOLL_CTL_MOD, fd, &ev) == -1) return -1;
    return 0;
}
MM_EXPORT_NET mmInt_t mmPoll_DelEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollEpoll* _impl = (struct mmPollEpoll*)(p->impl);
    struct epoll_event ev = { 0, {0} };
    mask = (MM_PE_READABLE | MM_PE_WRITABLE) & (~mask);
    ev.events = 0;
    if (mask & MM_PE_READABLE) ev.events |= EPOLLIN;
    if (mask & MM_PE_WRITABLE) ev.events |= EPOLLOUT;
    ev.events |= EPOLLET;
    ev.data.ptr = ud;
    if (mask != MM_PE_NONEABLE)
    {
        if (epoll_ctl(_impl->pfd, EPOLL_CTL_MOD, fd, &ev) == -1) return -1;
    }
    else
    {
        /* Note, Kernel < 2.6.9 requires a non null event pointer even for
         * EPOLL_CTL_DEL. */
        if (epoll_ctl(_impl->pfd, EPOLL_CTL_DEL, fd, &ev) == -1) return -1;
    }
    return 0;
}
MM_EXPORT_NET mmInt_t mmPoll_WaitEvent(struct mmPoll* p, mmMSec_t milliseconds)
{
    // struct epoll_event ev[len];
    int n = 0;
    int i;
    //
    struct mmPollEpoll* _impl = (struct mmPollEpoll*)(p->impl);
    //
    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
    n = epoll_wait(_impl->pfd, _impl->ev, p->length, milliseconds);

    for (i = 0; i < n; i++)
    {
        int mask = 0;
        struct epoll_event* ee = &(_impl->ev[i]);

        if (ee->events & EPOLLIN)  mask |= MM_PE_READABLE;
        if (ee->events & EPOLLOUT) mask |= MM_PE_WRITABLE;
        if (ee->events & EPOLLERR) mask |= MM_PE_WRITABLE;
        if (ee->events & EPOLLHUP) mask |= MM_PE_WRITABLE;

        p->arrays[i].s = ee->data.ptr;
        p->arrays[i].mask = mask;
    }
    return n;
}
MM_EXPORT_NET void mmPoll_Start(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Interrupt(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Shutdown(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Join(struct mmPoll* p)
{

}