/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPoll_h__
#define __mmPoll_h__

#include "core/mmCore.h"

#include "core/mmOSSocket.h"
#include "core/mmTime.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

#define MM_POLL_EVENT_DEFAULT_NUMBER 256

#define MM_PE_NONEABLE 0
#define MM_PE_READABLE 1
#define MM_PE_WRITABLE 2

struct mmPollEvent
{
    void* s;
    // mask for MM_PE_NONEABLE | MM_PE_READABLE | MM_PE_WRITABLE
    int mask;
};

struct mmPoll
{
    // default MM_POLL_EVENT_DEFAULT_NUMBER.
    mmUInt32_t length;
    struct mmPollEvent* arrays;
    void* impl;
};
MM_EXPORT_NET void mmPoll_Init(struct mmPoll* p);
MM_EXPORT_NET void mmPoll_Destroy(struct mmPoll* p);
// poll ev length default is 256.set it before mmPoll_wait,or mmPoll_wait after.
MM_EXPORT_NET void mmPoll_SetLength(struct mmPoll* p, mmUInt32_t length);
MM_EXPORT_NET mmUInt32_t mmPoll_GetLength(const struct mmPoll* p);
MM_EXPORT_NET mmInt_t mmPoll_AddEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud);
MM_EXPORT_NET mmInt_t mmPoll_ModEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud);
MM_EXPORT_NET mmInt_t mmPoll_DelEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud);
MM_EXPORT_NET mmInt_t mmPoll_WaitEvent(struct mmPoll* p, mmMSec_t milliseconds);

MM_EXPORT_NET void mmPoll_Start(struct mmPoll* p);
MM_EXPORT_NET void mmPoll_Interrupt(struct mmPoll* p);
MM_EXPORT_NET void mmPoll_Shutdown(struct mmPoll* p);
MM_EXPORT_NET void mmPoll_Join(struct mmPoll* p);

MM_EXPORT_NET const mmChar_t* mmPoll_Api(void);

#include "core/mmSuffix.h"


#endif//__mmPoll_h__