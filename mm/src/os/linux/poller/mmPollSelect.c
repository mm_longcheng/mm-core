/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoll.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmErrno.h"
#include "core/mmLogger.h"

#include "container/mmRbtreeU32.h"

static void* __static_mmPoll_PollEventProduce(struct mmRbtreeU32Vpt* p, mmUInt32_t k)
{
    struct mmPollEvent* e = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent));
    return e;
}
static void* __static_mmPoll_PollEventRecycle(struct mmRbtreeU32Vpt* p, mmUInt32_t k, void* v)
{
    struct mmPollEvent* e = (struct mmPollEvent*)(v);
    mmFree(e);
    return v;
}

struct mmPollSelect
{
    struct mmRbtreeU32Vpt rbtree;
    // read
    fd_set sr;
    // write
    fd_set sw;
    mmSocket_t max_fd;
};
static void mmPollSelect_Init(struct mmPollSelect* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmRbtreeU32Vpt_Init(&p->rbtree);
    FD_ZERO(&p->sr);
    FD_ZERO(&p->sw);
    p->max_fd = 0;

    _U32VptAllocator.Produce = &__static_mmPoll_PollEventProduce;
    _U32VptAllocator.Recycle = &__static_mmPoll_PollEventRecycle;
    _U32VptAllocator.obj = p;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
static void mmPollSelect_Destroy(struct mmPollSelect* p)
{
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    FD_ZERO(&p->sr);
    FD_ZERO(&p->sw);
    p->max_fd = 0;
}
static void mmPollSelect_CheckUpdateMaxfd(struct mmPollSelect* p, mmSocket_t fd)
{
    if (fd == p->max_fd)
    {
        p->max_fd = 0;
        if (0 != p->rbtree.size)
        {
            struct mmRbNode* n = NULL;
            struct mmRbtreeU32VptIterator* it = NULL;
            n = mmRb_First(&p->rbtree.rbt);
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            p->max_fd = it->k;
        }
    }
}

MM_EXPORT_NET const mmChar_t* mmPoll_Api(void)
{
    return "mmPollSelect";
}

MM_EXPORT_NET void mmPoll_Init(struct mmPoll* p)
{
    struct mmPollSelect* _impl = NULL;
    p->length = MM_POLL_EVENT_DEFAULT_NUMBER;
    p->arrays = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * p->length);
    _impl = (struct mmPollSelect*)mmMalloc(sizeof(struct mmPollSelect));
    mmPollSelect_Init(_impl);
    p->impl = _impl;
    //
    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
}
MM_EXPORT_NET void mmPoll_Destroy(struct mmPoll* p)
{
    struct mmPollSelect* _impl = (struct mmPollSelect*)(p->impl);
    mmPollSelect_Destroy(_impl);
    mmFree(p->arrays);
    mmFree(_impl);
    //
    p->length = 0;
    p->arrays = NULL;
    p->impl = NULL;
}
MM_EXPORT_NET void mmPoll_SetLength(struct mmPoll* p, mmUInt32_t length)
{
    if (length < p->length)
    {
        struct mmPollEvent* v = NULL;
        //
        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
        //
        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * length);
        mmFree(p->arrays);
        p->arrays = v;
        //
        p->length = length;
    }
    else if (length > p->length)
    {
        struct mmPollEvent* v = NULL;
        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        //
        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * length);
        //
        p->length = length;
    }
}
MM_EXPORT_NET mmUInt32_t mmPoll_GetLength(const struct mmPoll* p)
{
    return p->length;
}
// int select(int maxfdp,fd_set *readfds,fd_set *writefds,fd_set *errorfds,struct timeval *timeout);
//
// Anfd_set is a fixed size buffer. Executing FD_CLR() or FD_SET() with a value of fd that is 
// negative or is equal to or larger than FD_SETSIZE will result in undefined behavior. Moreover, 
// POSIX requires fd to be a valid file descriptor.
//
// if fd is larger than FD_SETSIZE it will rollback and cover old value.
MM_EXPORT_NET mmInt_t mmPoll_AddEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollEvent* e = NULL;
    struct mmPollSelect* _impl = (struct mmPollSelect*)(p->impl);
    struct mmRbtreeU32VptIterator* it = mmRbtreeU32Vpt_GetIterator(&_impl->rbtree, (mmUInt32_t)fd);
    assert(_impl->rbtree.size + 1 < FD_SETSIZE && " fd is larger than FD_SETSIZE it will rollback and cover old value.");
    if (NULL == it)
    {
        // if it is NULL the e must NULL.
        e = (struct mmPollEvent*)mmRbtreeU32Vpt_Add(&_impl->rbtree, (mmUInt32_t)fd);
    }
    else
    {
        // if it is not NULL the e must not NULL.
        e = (struct mmPollEvent*)(it->v);
    }
    e->s = ud;
    e->mask = 0;
    if (mask & MM_PE_READABLE)
    {
        e->mask |= MM_PE_READABLE;
        FD_SET(fd, &_impl->sr);
    }
    if (mask & MM_PE_WRITABLE)
    {
        e->mask |= MM_PE_WRITABLE;
        FD_SET(fd, &_impl->sw);
    }
    _impl->max_fd = _impl->max_fd > fd ? _impl->max_fd : fd;
    return 0;
}
MM_EXPORT_NET mmInt_t mmPoll_ModEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollEvent* e = NULL;
    struct mmPollSelect* _impl = (struct mmPollSelect*)(p->impl);
    struct mmRbtreeU32VptIterator* it = mmRbtreeU32Vpt_GetIterator(&_impl->rbtree, (mmUInt32_t)fd);
    if (NULL != it)
    {
        // if it is not NULL the e must not NULL.
        e = (struct mmPollEvent*)(it->v);
        e->s = ud;
        if (mask & MM_PE_READABLE)
        {
            if (!(e->mask & MM_PE_READABLE)) FD_SET(fd, &_impl->sr);
        }
        else
        {
            if (e->mask & MM_PE_READABLE) FD_CLR(fd, &_impl->sr);
        }
        if (mask & MM_PE_WRITABLE)
        {
            if (!(e->mask & MM_PE_WRITABLE)) FD_SET(fd, &_impl->sw);
        }
        else
        {
            if (e->mask & MM_PE_WRITABLE) FD_CLR(fd, &_impl->sw);
        }
        e->mask = mask;
        return 0;
    }
    return -1;
}
MM_EXPORT_NET mmInt_t mmPoll_DelEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollEvent* e = NULL;
    struct mmPollSelect* _impl = (struct mmPollSelect*)(p->impl);
    struct mmRbtreeU32VptIterator* it = mmRbtreeU32Vpt_GetIterator(&_impl->rbtree, (mmUInt32_t)fd);
    if (NULL != it)
    {
        // if it is not NULL the e must not NULL.
        e = (struct mmPollEvent*)(it->v);
        if ((MM_PE_READABLE | MM_PE_WRITABLE) == mask)
        {
            FD_CLR(fd, &_impl->sw);
            FD_CLR(fd, &_impl->sr);
            mmRbtreeU32Vpt_Erase(&_impl->rbtree, it);
            mmPollSelect_CheckUpdateMaxfd(_impl, fd);
            return 0;
        }
        else
        {
            if (mask & MM_PE_READABLE)
            {
                e->mask &= ~(1 << MM_PE_READABLE);
                FD_CLR(fd, &_impl->sr);
            }
            if (mask & MM_PE_WRITABLE)
            {
                e->mask &= ~(1 << MM_PE_WRITABLE);
                FD_CLR(fd, &_impl->sw);
            }
            if (!(MM_PE_READABLE & e->mask || MM_PE_WRITABLE & e->mask))
            {
                mmRbtreeU32Vpt_Erase(&_impl->rbtree, it);
                mmPollSelect_CheckUpdateMaxfd(_impl, fd);
            }
            return 0;
        }
    }
    return -1;
}
MM_EXPORT_NET mmInt_t mmPoll_WaitEvent(struct mmPoll* p, mmMSec_t milliseconds)
{
    fd_set work_r_fd_set;
    fd_set work_w_fd_set;
    //fd_set work_e_fd_set;

    struct timeval  tv;
    struct timeval* tp = NULL;

    int ready = 0;
    mmUInt32_t find_number = 0;
    //
    struct mmRbNode* n = NULL;
    struct mmPollEvent pe;
    struct mmPollEvent* ue = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    //
    struct mmPollSelect* _impl = (struct mmPollSelect*)(p->impl);
    // quick checking.
    if (0 == _impl->rbtree.size)
    {
        // if nothing to, time wait milliseconds.
        mmMSleep(milliseconds);
        return 0;
    }

    tp = &tv;

    if (MM_TIMER_INFINITE == milliseconds)
    {
        // when select fd_set add or remove we have no way to re select,
        // so if timeout is forever.we use do while for simulate.
        // here we just 200ms for timeout.
        tv.tv_sec = (mmTimevalSSec_t)0;// (200 / 1000);
        tv.tv_usec = (mmTimevalUSec_t)200000;// ((200 % 1000) * 1000);
        do
        {
            work_r_fd_set = _impl->sr;
            work_w_fd_set = _impl->sw;
            //work_e_fd_set = _fd_read->m_fd_set;

            ready = select((mmUInt32_t)(_impl->max_fd + 1), &work_r_fd_set, &work_w_fd_set, NULL, tp);
        } while (0 == ready);
    }
    else
    {
        tv.tv_sec = (mmTimevalSSec_t)(milliseconds / 1000);
        tv.tv_usec = (mmTimevalUSec_t)((milliseconds % 1000) * 1000);

        work_r_fd_set = _impl->sr;
        work_w_fd_set = _impl->sw;
        //work_e_fd_set = _fd_read->m_fd_set;

        // int select(int maxfdp,fd_set *readfds,fd_set *writefds,fd_set *errorfds,struct timeval *timeout);
        //ready = select(_fd_read->m_max_fd + 1, &work_r_fd_set, &work_w_fd_set, &work_e_fd_set, tp);
        ready = select((mmUInt32_t)(_impl->max_fd + 1), &work_r_fd_set, &work_w_fd_set, NULL, tp);
    }

    if (ready == 0)
    {
        if (milliseconds != MM_TIMER_INFINITE)
        {
            return MM_OK;
        }
        return MM_ERROR;
    }
    else if (-1 == ready)
    {
        // not socket .
        mmErr_t _errcode = mmErrno;
        if (_errcode == MM_ECONNABORTED || _errcode == MM_EAGAIN)
        {
            // An operation on a socket could not be performed because the system 
            // lacked sufficient buffer space or because a queue was full.
            return 0;
        }
        else
        {
            // error handler.
            return -1;
        }
        return 0;
    }
    //
    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
    for (n = mmRb_First(&_impl->rbtree.rbt); n; n = mmRb_Next(n))
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        if (0 != it->k)
        {
            ue = (struct mmPollEvent*)(it->v);
            pe.s = ue->s;
            pe.mask = 0;
            if (FD_ISSET(it->k, &work_r_fd_set)) pe.mask |= MM_PE_READABLE;
            if (FD_ISSET(it->k, &work_w_fd_set)) pe.mask |= MM_PE_WRITABLE;
            if (MM_PE_NONEABLE != pe.mask)
            {
                p->arrays[find_number] = pe;
                find_number++;
            }
            if (find_number >= p->length)
            {
                break;
            }
        }
    }
    return find_number;
}
MM_EXPORT_NET void mmPoll_Start(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Interrupt(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Shutdown(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Join(struct mmPoll* p)
{

}