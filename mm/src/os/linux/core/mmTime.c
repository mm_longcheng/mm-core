/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTime.h"


/*
 * FreeBSD does not test /etc/localtime change, however, we can workaround it
 * by calling tzset() with TZ and then without TZ to update timezone.
 * The trick should work since FreeBSD 2.1.0.
 *
 * Linux does not test /etc/localtime change in localtime(),
 * but may stat("/etc/localtime") several times in every strftime(),
 * therefore we use it to update timezone.
 *
 * Solaris does not test /etc/TIMEZONE change too and no workaround available.
 */

MM_EXPORT_DLL void mmTimezone_Update(void)
{
#if (MM_FREEBSD)

    if (getenv("TZ")) {
        return;
    }

    putenv("TZ=UTC");

    tzset();

    unsetenv("TZ");

    tzset();

#elif (MM_LINUX)
    time_t      s;
    struct tm  *t;
    char        buf[4];

    s = time(0);

    t = localtime(&s);

    strftime(buf, 4, "%H", t);

#endif
}


MM_EXPORT_DLL void mmLocaltimeR(const time_t* s, struct tm *tm)
{
#if (MM_HAVE_LOCALTIME_R)
    (void)localtime_r(s, tm);

#else
    struct tm  *t;

    t = localtime(s);
    *tm = *t;

#endif

    tm->mmTm_mon++;
    tm->mmTm_year += 1900;
}


MM_EXPORT_DLL void mmLibcLocaltime(const time_t* s, struct tm *tm)
{
#if (MM_HAVE_LOCALTIME_R)
    (void)localtime_r(s, tm);

#else
    struct tm  *t;

    t = localtime(s);
    *tm = *t;

#endif
}


MM_EXPORT_DLL void mmLibcGmtime(const time_t* s, struct tm *tm)
{
#if (MM_HAVE_LOCALTIME_R)
    (void)gmtime_r(s, tm);

#else
    struct tm  *t;

    t = gmtime(s);
    *tm = *t;

#endif
}

MM_EXPORT_DLL void mmLibcCtime(char* s, size_t sz, const time_t* t)
{
    ctime_r(t, s);
}

MM_EXPORT_DLL void mmSSleep(mmULong_t seconds)
{
    struct timespec request;
    struct timespec remaining;

    request.tv_sec = seconds;
    request.tv_nsec = 0;

    while (nanosleep(&request, &remaining) == -1 && errno == EINTR)
    {
        // copy the remaining time to request, go to the next wait.
        request = remaining;
    }
}
MM_EXPORT_DLL void mmMSleep(mmULong_t milliseconds)
{
    struct timespec request;
    struct timespec remaining;

    request.tv_sec = milliseconds / MM_MSEC_PER_SEC;
    request.tv_nsec = 1000000 * (milliseconds % MM_MSEC_PER_SEC);

    while (nanosleep(&request, &remaining) == -1 && errno == EINTR)
    {
        // copy the remaining time to request, go to the next wait.
        request = remaining;
    }
}
MM_EXPORT_DLL void mmUSleep(mmULong_t microseconds)
{
    // At unix system.
    //
    // select low precision.
    //
    // http://man7.org/linux/man-pages/man3/usleep.3.html
    // usleep is not recommended.
    //
    // http://man7.org/linux/man-pages/man2/nanosleep.2.html
    // nanosleep

    struct timespec request;
    struct timespec remaining;

    request.tv_sec = microseconds / MM_USEC_PER_SEC;
    request.tv_nsec = 1000 * (microseconds % MM_USEC_PER_SEC);

    while (nanosleep(&request, &remaining) == -1 && errno == EINTR)
    {
        // copy the remaining time to request, go to the next wait.
        request = remaining;
    }
}

/* NB NB NB NB This returns GMT!!!!!!!!!! */
MM_EXPORT_DLL mmUInt64_t mmTime_CurrentUSec(void)
{
    struct timeval tv;
    mmGettimeofday(&tv, NULL);
    // note: 
    //   we must cast tv_sec tv_usec to mmUInt64_t, make sure not overflow.
    return (mmUInt64_t)tv.tv_sec * MM_USEC_PER_SEC + (mmUInt64_t)tv.tv_usec;
}

MM_EXPORT_DLL mmUInt64_t mmTime_GetSystemRunningUSec(void)
{
    /* get monotonic clock time */
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (mmUInt64_t)ts.tv_sec * MM_USEC_PER_SEC + (mmUInt64_t)ts.tv_nsec / MM_MSEC_PER_SEC;
}