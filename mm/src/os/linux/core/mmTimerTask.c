/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTimerTask.h"
#include "core/mmAlloc.h"
#include "core/mmErrno.h"
#include "core/mmLogger.h"
#include "core/mmTimewait.h"
#include "core/mmOSSocket.h"

// timer task impl pthread version.
struct mmTimerTaskPthread
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};
MM_EXPORT_DLL void mmTimerTaskPthread_Init(struct mmTimerTaskPthread* p)
{
    pthread_mutex_init(&p->mutex, NULL);
    pthread_cond_init(&p->cond, NULL);
}
MM_EXPORT_DLL void mmTimerTaskPthread_Destroy(struct mmTimerTaskPthread* p)
{
    pthread_mutex_destroy(&p->mutex);
    pthread_cond_destroy(&p->cond);
}

static void* __static_mmTimerTask_HandleThread(void* _arg);

static void __static_mmTimerTask_Handle(struct mmTimerTask* p)
{

}
MM_EXPORT_DLL void mmTimerTaskCallback_Init(struct mmTimerTaskCallback* p)
{
    p->handle = &__static_mmTimerTask_Handle;
    p->obj = NULL;
}
MM_EXPORT_DLL void mmTimerTaskCallback_Destroy(struct mmTimerTaskCallback* p)
{
    p->handle = &__static_mmTimerTask_Handle;
    p->obj = NULL;
}

MM_EXPORT_DLL void mmTimerTask_Init(struct mmTimerTask* p)
{
    p->impl = mmMalloc(sizeof(struct mmTimerTaskPthread));
    mmTimerTaskPthread_Init((struct mmTimerTaskPthread*)p->impl);
    mmTimerTaskCallback_Init(&p->callback);
    p->nearby_time = MM_TIME_TASK_NEARBY_MSEC;
    p->msec_delay = 0;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DLL void mmTimerTask_Destroy(struct mmTimerTask* p)
{
    mmTimerTaskPthread_Destroy((struct mmTimerTaskPthread*)p->impl);
    mmFree((struct mmTimerTask_select*)p->impl);
    p->impl = NULL;
    mmTimerTaskCallback_Destroy(&p->callback);
    p->nearby_time = 0;
    p->msec_delay = 0;
    p->state = MM_TS_CLOSED;
}

// do not assign when holder have some elem.
MM_EXPORT_DLL void mmTimerTask_SetCallback(struct mmTimerTask* p, struct mmTimerTaskCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = *callback;
}
MM_EXPORT_DLL void mmTimerTask_SetNearbyTime(struct mmTimerTask* p, mmMSec_t nearby_time)
{
    p->nearby_time = nearby_time;
}

// sync.
MM_EXPORT_DLL void mmTimerTask_Handle(struct mmTimerTask* p)
{
    struct mmTimerTaskPthread* _impl = (struct mmTimerTaskPthread*)p->impl;
    
    struct timeval  ntime;
    struct timespec otime;
    int rt = 0;
    assert(p->callback.handle&&"p->callback.handle is a null.");
    if (0 < p->msec_delay)
    {
        mmMSleep(p->msec_delay);
    }
    while (MM_TS_MOTION == p->state)
    {
        // timewait nearby.
        rt = mmTimewait_MSecNearby(&_impl->cond, &_impl->mutex, &ntime, &otime, p->nearby_time);
        // check the timewait for callback.
        if (0 == p->nearby_time || 0 == rt || ETIMEDOUT == rt)
        {
            (*(p->callback.handle))(p);
        }
    }
}

// start thread.
MM_EXPORT_DLL void mmTimerTask_Start(struct mmTimerTask* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->task_thread, NULL, &__static_mmTimerTask_HandleThread, p);
}
// interrupt thread.
MM_EXPORT_DLL void mmTimerTask_Interrupt(struct mmTimerTask* p)
{
    struct mmTimerTaskPthread* _impl = (struct mmTimerTaskPthread*)p->impl;
    if (NULL != _impl)
    {
        p->state = MM_TS_CLOSED;
        pthread_mutex_lock(&_impl->mutex);
        pthread_cond_signal(&_impl->cond);
        pthread_mutex_unlock(&_impl->mutex);
    }
}
// shutdown thread.
MM_EXPORT_DLL void mmTimerTask_Shutdown(struct mmTimerTask* p)
{
    struct mmTimerTaskPthread* _impl = (struct mmTimerTaskPthread*)p->impl;
    if (NULL != _impl)
    {
        p->state = MM_TS_FINISH;
        pthread_mutex_lock(&_impl->mutex);
        pthread_cond_signal(&_impl->cond);
        pthread_mutex_unlock(&_impl->mutex);
    }
}
// join thread.
MM_EXPORT_DLL void mmTimerTask_Join(struct mmTimerTask* p)
{
    pthread_join(p->task_thread, NULL);
}
MM_EXPORT_DLL const char* mmTimerTask_Api(struct mmTimerTask* p)
{
    return "mmTimerTaskPthread";
}
//////////////////////////////////////////////////////////////////////////
static void* __static_mmTimerTask_HandleThread(void* _arg)
{
    struct mmTimerTask* p = (struct mmTimerTask*)(_arg);
    mmTimerTask_Handle(p);
    return NULL;
}