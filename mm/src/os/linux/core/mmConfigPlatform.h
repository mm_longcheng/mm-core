/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConfigPlatform_h__
#define __mmConfigPlatform_h__

#include "core/mmPlatform.h"

#include "core/mmPrefix.h"
// sign current platform.
#define MM_LINUX 1

#include "core/mmAutoHeaders.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE             /* pread(), pwrite(), gethostname() */
#endif

#define _FILE_OFFSET_BITS  64

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdarg.h>
#include <stddef.h>             /* offsetof() */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <glob.h>
#include <sys/vfs.h>            /* statfs() */

#include <sys/uio.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>        /* TCP_NODELAY, TCP_CORK */
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/un.h>

#include <time.h>               /* tzset() */
#include <malloc.h>             /* memalign() */
#include <limits.h>             /* IOV_MAX */
#include <sys/ioctl.h>
#include <crypt.h>
#include <sys/utsname.h>        /* uname() */


#include <core/mmAutoConfig.h>


#if (MM_HAVE_POSIX_SEM)
#include <semaphore.h>
#endif


#if (MM_HAVE_SYS_PRCTL_H)
#include <sys/prctl.h>
#endif


#if (MM_HAVE_SENDFILE64)
#include <sys/sendfile.h>
#else
// http://man7.org/linux/man-pages/man2/sendfile.2.html
extern ssize_t sendfile(int out_fd, int in_fd, off_t *offset, size_t count);
#define MM_SENDFILE_LIMIT  0x80000000
#endif


#if (MM_HAVE_POLL)
#include <poll.h>
#endif


#if (MM_HAVE_EPOLL)
#include <sys/epoll.h>
#endif


#if (MM_HAVE_SYS_EVENTFD_H)
#include <sys/eventfd.h>
#endif
#include <sys/syscall.h>
#if (MM_HAVE_FILE_AIO)
#include <linux/aio_abi.h>
typedef struct iocb  mm_aiocb_t;
#endif


#define MM_LISTEN_BACKLOG        511


#ifndef MM_HAVE_SO_SNDLOWAT
/* setsockopt(SO_SNDLOWAT) returns ENOPROTOOPT */
#define MM_HAVE_SO_SNDLOWAT         0
#endif


#ifndef MM_HAVE_INHERITED_NONBLOCK
#define MM_HAVE_INHERITED_NONBLOCK  0
#endif


#define MM_HAVE_OS_SPECIFIC_INIT    1
#define mm_debug_init()


extern char **environ;
//////////////////////////////////////////////////////////////////////////
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
//////////////////////////////////////////////////////////////////////////
// word size.
// word size. __GNUC__ __i386__ __x86_64__
#ifdef __x86_64__
#define MM_WORDSIZE 64
#define MM_SIZEOF_UINTPTR_T 8
#else
#define MM_WORDSIZE 32
#define MM_SIZEOF_UINTPTR_T 4
#endif
//////////////////////////////////////////////////////////////////////////

#include <strings.h>
// strcasecmp
// strncasecmp

#include "core/mmSuffix.h"

#endif//__mmConfigPlatform_h__