/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRuntimeStat.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

const char* __seek_items(const char* buffer, int ie)
{
    int i;
    int count = 0;
    assert(buffer);
    const char* p = buffer;
    int len = strlen(buffer);
    if (ie <= 0)
    {
        return p;
    }
    for (i = 0; i < len; i++)
    {
        if (' ' == *p)
        {
            count++;
            if (count == ie)
            {
                p++;
                break;
            }
        }
        p++;
    }
    return p;
}

MM_EXPORT_DLL void mmRuntimeLoadavg_Init(struct mmRuntimeLoadavg* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeLoadavg));
}
MM_EXPORT_DLL void mmRuntimeLoadavg_Destroy(struct mmRuntimeLoadavg* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeLoadavg));
}
MM_EXPORT_DLL void mmRuntimeLoadavg_Perform(struct mmRuntimeLoadavg* p)
{
    FILE* f = fopen("/proc/loadavg", "rb");
    if (f)
    {
        char line[MM_PROC_MAX_LENGTH] = { 0 };
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%f %f %f %d/%d %" PRIu64
                , &p->lavg_01
                , &p->lavg_05
                , &p->lavg_15
                , &p->nr_running
                , &p->nr_threads
                , &p->last_pid);
        }
        fclose(f);
    }
}

MM_EXPORT_DLL void mmRuntimeMachine_Init(struct mmRuntimeMachine* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeMachine));
}
MM_EXPORT_DLL void mmRuntimeMachine_Destroy(struct mmRuntimeMachine* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeMachine));
}
MM_EXPORT_DLL void mmRuntimeMachine_Perform(struct mmRuntimeMachine* p)
{
    mmRuntimeMachine_PerformCpu(p);
    mmRuntimeMachine_PerformMem(p);
}
MM_EXPORT_DLL void mmRuntimeMachine_PerformCpu(struct mmRuntimeMachine* p)
{
    FILE* f = fopen("/proc/stat", "rb");
    if (f)
    {
        char line[MM_PROC_MAX_LENGTH] = { 0 };
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64 " %" PRIu64 " %" PRIu64 " %" PRIu64
                , &p->cpu_user
                , &p->cpu_nice
                , &p->cpu_system
                , &p->cpu_idle);
        }
        fclose(f);
    }
    p->cores = (mmUInt32_t)sysconf(_SC_NPROCESSORS_ONLN);
}
MM_EXPORT_DLL void mmRuntimeMachine_PerformMem(struct mmRuntimeMachine* p)
{
    FILE* f = fopen("/proc/meminfo", "rb");
    if (f)
    {
        char line[MM_PROC_MAX_LENGTH] = { 0 };
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->mem_total);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->mem_free);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->mem_buff);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->mem_cache);
        }
        fclose(f);
    }
}

MM_EXPORT_DLL void mmRuntimeProcess_Init(struct mmRuntimeProcess* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeProcess));
}
MM_EXPORT_DLL void mmRuntimeProcess_Destroy(struct mmRuntimeProcess* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeProcess));
}
MM_EXPORT_DLL void mmRuntimeProcess_Perform(struct mmRuntimeProcess* p)
{
    mmRuntimeProcess_PerformPid(p);
    mmRuntimeProcess_PerformCpu(p);
    mmRuntimeProcess_PerformMem(p);
    mmRuntimeProcess_PerformIO(p);
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformPid(struct mmRuntimeProcess* p)
{
    p->pid = getpid();
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformCpu(struct mmRuntimeProcess* p)
{
    char cmd[128];
    mmSprintf(cmd, "/proc/%llu/stat", (mmULLong_t)p->pid);
    FILE* f = fopen(cmd, "rb");
    if (f)
    {
        char line[MM_PROC_MAX_LENGTH] = { 0 };
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            const char* b = __seek_items(line, 1);
            sscanf(b, "%*s %c", &p->state);
            //get data in line 14 15 16 17
            const char* q = __seek_items(b, 12);
            sscanf(q, "%" PRIu64 " %" PRIu64 " %" PRIu64 " %" PRIu64 ""
                , &p->cpu_utime
                , &p->cpu_stime
                , &p->cpu_cutime
                , &p->cpu_cstime);
            //get data in line 20
            const char* t = __seek_items(q, 6);
            sscanf(t, "%d", &p->threads);
        }
        fclose(f);
    }
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformMem(struct mmRuntimeProcess* p)
{
    char cmd[128];
    mmSprintf(cmd, "/proc/%" PRIu64 "/status", p->pid);
    FILE* f = fopen(cmd, "rb");
    if (f)
    {
        int i = 0;
        char line[MM_PROC_MAX_LENGTH] = { 0 };
        //read before line 12
        for (i = 0; i < 12; i++)
        {
            fgets(line, MM_PROC_MAX_LENGTH, f);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->vm_size);
        }
        //read before line 4
        for (i = 0; i < 2; i++)
        {
            fgets(line, MM_PROC_MAX_LENGTH, f);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->vm_rss);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->vm_data);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->vm_stk);
        }
        fclose(f);
    }
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformIO(struct mmRuntimeProcess* p)
{
    char cmd[128];
    mmSprintf(cmd, "/proc/%" PRIu64 "/io", p->pid);
    FILE* f = fopen(cmd, "rb");
    if (f)
    {
        char line[MM_PROC_MAX_LENGTH] = { 0 };
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->rchar);
        }
        if (NULL != fgets(line, MM_PROC_MAX_LENGTH, f))
        {
            sscanf(line, "%*s %" PRIu64, &p->wchar);
        }
        fclose(f);
    }
}

MM_EXPORT_DLL void mmRuntimeStat_Init(struct mmRuntimeStat* p)
{
    p->pid = 0;
    p->cores = 0;
    p->vm_rss = 0;
    p->rchar = 0;
    p->wchar = 0;
    p->cpu_sys = 0;
    p->cpu_pro = 0;
    p->mem_pro = 0;
    p->timecode_o = 0;
    p->timecode_n = 0;
    p->sampling = 0;
    mmMemset(&p->tv_o, 0, sizeof(struct timeval));
    mmMemset(&p->tv_n, 0, sizeof(struct timeval));
    mmRuntimeLoadavg_Init(&p->loadavg_o);
    mmRuntimeLoadavg_Init(&p->loadavg_n);
    mmRuntimeMachine_Init(&p->machine_o);
    mmRuntimeMachine_Init(&p->machine_n);
    mmRuntimeProcess_Init(&p->process_o);
    mmRuntimeProcess_Init(&p->process_n);
}

MM_EXPORT_DLL void mmRuntimeStat_Destroy(struct mmRuntimeStat* p)
{
    p->pid = 0;
    p->cores = 0;
    p->vm_rss = 0;
    p->rchar = 0;
    p->wchar = 0;
    p->cpu_sys = 0;
    p->cpu_pro = 0;
    p->mem_pro = 0;
    p->timecode_o = 0;
    p->timecode_n = 0;
    p->sampling = 0;
    mmMemset(&p->tv_o, 0, sizeof(struct timeval));
    mmMemset(&p->tv_n, 0, sizeof(struct timeval));
    mmRuntimeLoadavg_Destroy(&p->loadavg_o);
    mmRuntimeLoadavg_Destroy(&p->loadavg_n);
    mmRuntimeMachine_Destroy(&p->machine_o);
    mmRuntimeMachine_Destroy(&p->machine_n);
    mmRuntimeProcess_Destroy(&p->process_o);
    mmRuntimeProcess_Destroy(&p->process_n);
}

MM_EXPORT_DLL void mmRuntimeStat_Update(struct mmRuntimeStat* p)
{
    mmUInt64_t b_utime, b_stime, b_cutime, b_cstime;
    mmUInt64_t b_user, b_nice, b_system, b_idle, a, b, c;
    
    mmGettimeofday(&p->tv_n, NULL);
    p->timecode_n = p->tv_n.tv_sec * 1000 + (p->tv_n.tv_usec / 1000);
    p->sampling = p->timecode_n - p->timecode_o;
    //
    mmRuntimeLoadavg_Perform(&p->loadavg_n);
    mmRuntimeMachine_Perform(&p->machine_n);
    mmRuntimeProcess_Perform(&p->process_n);
    
    p->pid = p->process_n.pid;
    p->vm_rss = p->process_n.vm_rss;
    p->rchar = p->process_n.rchar;
    p->wchar = p->process_n.wchar;
    p->cores = p->machine_n.cores;
    //
    b_utime = p->process_n.cpu_utime - p->process_o.cpu_utime;
    b_stime = p->process_n.cpu_stime - p->process_o.cpu_stime;
    b_cutime = p->process_n.cpu_cutime - p->process_o.cpu_cutime;
    b_cstime = p->process_n.cpu_cstime - p->process_o.cpu_cstime;
    a = b_utime + b_stime + b_cutime + b_cstime;

    b_user = p->machine_n.cpu_user - p->machine_o.cpu_user;
    b_nice = p->machine_n.cpu_nice - p->machine_o.cpu_nice;
    b_system = p->machine_n.cpu_system - p->machine_o.cpu_system;
    b_idle = p->machine_n.cpu_idle - p->machine_o.cpu_idle;
    b = b_user + b_nice + b_system + b_idle;
    c = b_user + b_nice + b_system;

    p->cpu_sys = (0 == c && 0 == b_idle) ? 0 : c * 100.0f / (c + b_idle);
    p->cpu_pro = (0 == b) ? 0 : a * 100.0f / b;
    p->mem_pro = p->vm_rss * 100.0f / p->machine_o.mem_total;
    
    mmMemcpy(&p->loadavg_o, &p->loadavg_n, sizeof(struct mmRuntimeLoadavg));
    mmMemcpy(&p->machine_o, &p->machine_n, sizeof(struct mmRuntimeMachine));
    mmMemcpy(&p->process_o, &p->process_n, sizeof(struct mmRuntimeProcess));
    p->timecode_o = p->timecode_n;
}
