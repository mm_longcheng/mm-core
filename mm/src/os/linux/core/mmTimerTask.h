/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTimerTask_h__
#define __mmTimerTask_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmThread.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// timer task default nearby time milliseconds.
#define MM_TIME_TASK_NEARBY_MSEC 200

// windows pthread pthread_cond_timedwait working terrible.
// linux   select  can not wake up for shutdown.
struct mmTimerTask;

struct mmTimerTaskCallback
{
    void(*handle)(struct mmTimerTask* p);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DLL void mmTimerTaskCallback_Init(struct mmTimerTaskCallback* p);
MM_EXPORT_DLL void mmTimerTaskCallback_Destroy(struct mmTimerTaskCallback* p);

struct mmTimerTask
{
    // impl for time wait.strong ref.
    void* impl;
    // check thread.
    pthread_t task_thread;
    struct mmTimerTaskCallback callback;
    // nearby_time milliseconds for each time wait.default is MM_TIME_TASK_NEARBY_MSEC ms.
    mmMSec_t nearby_time;
    // delay milliseconds before first callback.default is 0 ms.
    mmMSec_t msec_delay;
    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_DLL void mmTimerTask_Init(struct mmTimerTask* p);
MM_EXPORT_DLL void mmTimerTask_Destroy(struct mmTimerTask* p);

// do not assign when holder have some elem.
MM_EXPORT_DLL void mmTimerTask_SetCallback(struct mmTimerTask* p, struct mmTimerTaskCallback* callback);
MM_EXPORT_DLL void mmTimerTask_SetNearbyTime(struct mmTimerTask* p, mmMSec_t nearby_time);

// sync.
MM_EXPORT_DLL void mmTimerTask_Handle(struct mmTimerTask* p);

// start thread.
MM_EXPORT_DLL void mmTimerTask_Start(struct mmTimerTask* p);
// interrupt thread.
MM_EXPORT_DLL void mmTimerTask_Interrupt(struct mmTimerTask* p);
// shutdown thread.
MM_EXPORT_DLL void mmTimerTask_Shutdown(struct mmTimerTask* p);
// join thread.
MM_EXPORT_DLL void mmTimerTask_Join(struct mmTimerTask* p);

MM_EXPORT_DLL const char* mmTimerTask_Api(struct mmTimerTask* p);

#include "core/mmSuffix.h"

#endif//__mmTimerTask_h__