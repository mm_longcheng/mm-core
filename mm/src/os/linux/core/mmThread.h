/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmThread_h__
#define __mmThread_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// thread have some interface.
//  init         state = MM_TS_CLOSED
//  start        state = MM_TS_FINISH == state ? MM_TS_CLOSED : MM_TS_MOTION;
//  interrupt    state = MM_TS_CLOSED
//  shutdown     state = MM_TS_FINISH
//  join         state = state
//  destroy      state = MM_TS_CLOSED
enum mmThreadState_t
{
    MM_TS_CLOSED = 0,// thread not start or be closed or be interrupt.
    MM_TS_MOTION = 1,// thread is running.
    MM_TS_FINISH = 2,// application is termination and can not restart.
};
//
MM_EXPORT_DLL unsigned long int mmCurrentThreadId(void);
//
#include "core/mmSuffix.h"

#endif//__mmThread_h__