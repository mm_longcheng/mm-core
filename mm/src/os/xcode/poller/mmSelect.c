/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSelect.h"
#include "core/mmErrno.h"

MM_EXPORT_NET void mmSelect_Init(struct mmSelect* p)
{
    p->poll_event.s = NULL;
    p->poll_event.mask = MM_PE_NONEABLE;
    p->mask = 0;
    FD_ZERO(&p->sr);
    FD_ZERO(&p->sw);
    p->fd = MM_INVALID_SOCKET;
}
MM_EXPORT_NET void mmSelect_Destroy(struct mmSelect* p)
{
    p->poll_event.s = NULL;
    p->poll_event.mask = MM_PE_NONEABLE;
    p->mask = 0;
    FD_ZERO(&p->sr);
    FD_ZERO(&p->sw);
    p->fd = MM_INVALID_SOCKET;
}

MM_EXPORT_NET void mmSelect_SetFd(struct mmSelect* p, mmSocket_t fd)
{
    p->fd = fd;
}

MM_EXPORT_NET mmInt_t mmSelect_AddEvent(struct mmSelect* p, int mask, void* ud)
{
    if (MM_INVALID_SOCKET != p->fd)
    {
        p->poll_event.s = ud;
        p->mask = 0;
        if (mask & MM_PE_READABLE)
        {
            p->mask |= MM_PE_READABLE;
            FD_SET(p->fd, &p->sr);
        }
        if (mask & MM_PE_WRITABLE)
        {
            p->mask |= MM_PE_WRITABLE;
            FD_SET(p->fd, &p->sw);
        }
    }
    return 0;
}
MM_EXPORT_NET mmInt_t mmSelect_ModEvent(struct mmSelect* p, int mask, void* ud)
{
    if (MM_INVALID_SOCKET != p->fd)
    {
        p->poll_event.s = ud;
        if (mask & MM_PE_READABLE)
        {
            if (!(p->mask & MM_PE_READABLE)) FD_SET(p->fd, &p->sr);
        }
        else
        {
            if (p->mask & MM_PE_READABLE) FD_CLR(p->fd, &p->sr);
        }
        if (mask & MM_PE_WRITABLE)
        {
            if (!(p->mask & MM_PE_WRITABLE)) FD_SET(p->fd, &p->sw);
        }
        else
        {
            if (p->mask & MM_PE_WRITABLE) FD_CLR(p->fd, &p->sw);
        }
        p->mask = mask;
    }
    return 0;
}
MM_EXPORT_NET mmInt_t mmSelect_DelEvent(struct mmSelect* p, int mask, void* ud)
{
    if (MM_INVALID_SOCKET != p->fd)
    {
        if ((MM_PE_READABLE | MM_PE_WRITABLE) == mask)
        {
            FD_CLR(p->fd, &p->sw);
            FD_CLR(p->fd, &p->sr);
        }
        else
        {
            if (mask & MM_PE_READABLE)
            {
                p->mask &= ~(1 << MM_PE_READABLE);
                FD_CLR(p->fd, &p->sr);
            }
            if (mask & MM_PE_WRITABLE)
            {
                p->mask &= ~(1 << MM_PE_WRITABLE);
                FD_CLR(p->fd, &p->sw);
            }
        }
    }
    return 0;
}
MM_EXPORT_NET mmInt_t mmSelect_WaitEvent(struct mmSelect* p, mmMSec_t milliseconds)
{
    fd_set wsr;
    fd_set wsw;

    struct timeval  tv;
    struct timeval* tp = NULL;

    int ready = 0;
    //
    // quick checking.
    if (MM_INVALID_SOCKET == p->fd)
    {
        // if nothing to, time wait milliseconds.
        mmMSleep(milliseconds);
        return 0;
    }

    tp = &tv;

    if (MM_TIMER_INFINITE == milliseconds)
    {
        // when select fd_set add or remove we have no way to re select,
        // so if timeout is forever.we use do while for simulate.
        // here we just 200ms for timeout.
        tv.tv_sec = (mmTimevalSSec_t)0;// (200 / 1000);
        tv.tv_usec = (mmTimevalUSec_t)200000;// ((200 % 1000) * 1000);
        do
        {
            wsr = p->sr;
            wsw = p->sw;

            ready = select((mmUInt32_t)(p->fd + 1), &wsr, &wsw, NULL, tp);
        } while (0 == ready);
    }
    else
    {
        tv.tv_sec = (mmTimevalSSec_t)(milliseconds / 1000);
        tv.tv_usec = (mmTimevalUSec_t)((milliseconds % 1000) * 1000);

        wsr = p->sr;
        wsw = p->sw;

        // int select(int maxfdp,fd_set *readfds,fd_set *writefds,fd_set *errorfds,struct timeval *timeout);
        ready = select((mmUInt32_t)(p->fd + 1), &wsr, &wsw, NULL, tp);
    }

    if (ready == 0)
    {
        if (milliseconds != MM_TIMER_INFINITE)
        {
            return MM_OK;
        }
        else
        {
            return MM_ERROR;
        }
    }
    else if (-1 == ready)
    {
        // not socket .
        mmErr_t _errcode = mmErrno;
        if (_errcode == MM_ECONNABORTED || _errcode == MM_EAGAIN)
        {
            // An operation on a socket could not be performed because the system 
            // lacked sufficient buffer space or because a queue was full.
            return 0;
        }
        else
        {
            // error handler.
            return -1;
        }
    }

    // statistical reality event mask.
    if (FD_ISSET(p->fd, &wsr)) p->poll_event.mask |= MM_PE_READABLE;
    if (FD_ISSET(p->fd, &wsw)) p->poll_event.mask |= MM_PE_WRITABLE;

    return ready;
}
