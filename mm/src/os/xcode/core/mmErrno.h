/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmErrno_h__
#define __mmErrno_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"


typedef int               mmErr_t;

#define MM_EPERM         EPERM
#define MM_ENOENT        ENOENT
#define MM_ENOPATH       ENOENT
#define MM_ESRCH         ESRCH
#define MM_EINTR         EINTR
#define MM_ECHILD        ECHILD
#define MM_ENOMEM        ENOMEM
#define MM_EACCES        EACCES
#define MM_EBUSY         EBUSY
#define MM_EEXIST        EEXIST
#define MM_EXDEV         EXDEV
#define MM_ENOTDIR       ENOTDIR
#define MM_EISDIR        EISDIR
#define MM_EINVAL        EINVAL
#define MM_ENFILE        ENFILE
#define MM_EMFILE        EMFILE
#define MM_ENOSPC        ENOSPC
#define MM_EPIPE         EPIPE
#define MM_EINPROGRESS   EINPROGRESS
#define MM_ENOPROTOOPT   ENOPROTOOPT
#define MM_EOPNOTSUPP    EOPNOTSUPP
#define MM_EADDRINUSE    EADDRINUSE
#define MM_ECONNABORTED  ECONNABORTED
#define MM_ECONNRESET    ECONNRESET
#define MM_ENOTCONN      ENOTCONN
#define MM_ENOTSOCK      ENOTSOCK
#define MM_ENOBUFS       ENOBUFS
#define MM_ETIMEDOUT     ETIMEDOUT
#define MM_ECONNREFUSED  ECONNREFUSED
#define MM_ENAMETOOLONG  ENAMETOOLONG
#define MM_ENETDOWN      ENETDOWN
#define MM_ENETUNREACH   ENETUNREACH
#define MM_EHOSTDOWN     EHOSTDOWN
#define MM_EHOSTUNREACH  EHOSTUNREACH
#define MM_ENOSYS        ENOSYS
#define MM_ECANCELED     ECANCELED
#define MM_EILSEQ        EILSEQ
#define MM_ENOMOREFILES  0
#define MM_ELOOP         ELOOP
#define MM_EBADF         EBADF

#if (MM_HAVE_OPENAT)
#define MM_EMLINK        EMLINK
#endif

#if (__hpux__)
#define MM_EAGAIN        EWOULDBLOCK
#else
#define MM_EAGAIN        EAGAIN
#endif


#define mmErrno                 errno
#define mmSocketErrno           errno
#define mmSetErrno(err)         errno = err
#define mmSetSocketErrno(err)   errno = err


MM_EXPORT_DLL mmUChar_t* mmStrError(mmErr_t err, mmUChar_t* errstr, size_t size);
MM_EXPORT_DLL mmInt_t mmStrError_Init(void);
MM_EXPORT_DLL mmInt_t mmStrError_Destroy(void);

#include "core/mmSuffix.h"

#endif//__mmErrno_h__
