/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOSSocket_h__
#define __mmOSSocket_h__

#include "core/mmConfig.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "core/mmPrefix.h"

#define MM_READ_SHUTDOWN SHUT_RD
#define MM_SEND_SHUTDOWN SHUT_WR
#define MM_BOTH_SHUTDOWN SHUT_RDWR

typedef int  mmSocket_t;

typedef mmUInt8_t mmSASize_t;
typedef sa_family_t mmSAFamily_t;
typedef in_port_t mmSAPort_t;

#define mmFopenSocket     socket
#define mmSocketN        "socket()"


#if (MM_HAVE_FIONBIO)

MM_EXPORT_DLL int mmNonblocking(mmSocket_t s);
MM_EXPORT_DLL int mmBlocking(mmSocket_t s);

#define mmNonblockingN   "ioctl(FIONBIO)"
#define mmBlockingN      "ioctl(!FIONBIO)"

#else

#define mmNonblocking(s)  fcntl(s, F_SETFL, fcntl(s, F_GETFL) | O_NONBLOCK)
#define mmNonblockingN   "fcntl(O_NONBLOCK)"

#define mmBlocking(s)     fcntl(s, F_SETFL, fcntl(s, F_GETFL) & ~O_NONBLOCK)
#define mmBlockingN      "fcntl(!O_NONBLOCK)"

#endif

MM_EXPORT_DLL int mmTcpNopush(mmSocket_t s);
MM_EXPORT_DLL int mmTcpPush(mmSocket_t s);

#if (MM_LINUX)

#define mmTcpNopushN   "setsockopt(TCP_CORK)"
#define mmTcpPushN     "setsockopt(!TCP_CORK)"

#else

#define mmTcpNopushN   "setsockopt(TCP_NOPUSH)"
#define mmTcpPushN     "setsockopt(!TCP_NOPUSH)"

#endif


#define mmShutdownSocket    shutdown
#define mmShutdownSocketN  "shutdown()"

#define mmCloseSocket    close
#define mmCloseSocketN  "close() socket"

// only open the flag.
// 1 true 0 false.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mmSocketKeepalive(mmSocket_t s, int flag);

// tcp_keepidle  firt keepalive trigger    ms.
// tcp_keepintvl keepalive packet interval ms.
// tcp_keepcnt   try times                 n.
// 0(MM_OK) success -1(MM_ERROR) failure.
MM_EXPORT_DLL int mmSocketKeepaliveCtl(mmSocket_t s, int tcp_keepidle, int tcp_keepintvl, int tcp_keepcnt);

#define MM_INVALID_SOCKET (-1)

#define MM_NONBLOCK 0
#define MM_BLOCKING 1

#define MM_AF_INET4 AF_INET
#define MM_AF_INET6 AF_INET6
#define MM_AF_UNSPE AF_UNSPEC

#define MM_IPPROTO_IPV4 IPPROTO_IP
#define MM_IPPROTO_IPV6 IPPROTO_IPV6

// not keepalive.
#define MM_KEEPALIVE_INACTIVE 0
// mod keepalive.
#define MM_KEEPALIVE_ACTIVATE 1

// udp group
#define MM_IPV4_ENTER_GROUP IP_ADD_MEMBERSHIP
#define MM_IPV4_LEAVE_GROUP IP_DROP_MEMBERSHIP

#define MM_IPV6_ENTER_GROUP IPV6_JOIN_GROUP
#define MM_IPV6_LEAVE_GROUP IPV6_LEAVE_GROUP

#define mmHtons htons
#define mmNtohs ntohs
#define mmHtonl htonl
#define mmNtohl ntohl
#define mmHtonll htobe64
#define mmNtohll be64toh

#define mmHton16 mmHtons
#define mmNtoh16 mmNtohs
#define mmHton32 mmHtonl
#define mmNtoh32 mmNtohl
#define mmHton64 mmHtonll
#define mmNtoh64 mmNtohll

#include "core/mmSuffix.h"

#endif//__mmOSSocket_h__
