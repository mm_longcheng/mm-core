/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTime_h__
#define __mmTime_h__

#include "core/mmCore.h"

#include <unistd.h>
#include <sys/time.h>

#include "core/mmPrefix.h"

typedef mmUInt_t        mmMSec_t;
typedef mmInt_t         mmMSecInt_t;

typedef mmSInt64_t      mmTime_t;

/** intervals for I/O timeouts, in microseconds */
typedef mmSInt64_t      mmIntervalTime_t;
/** short interval for I/O timeouts, in microseconds */
typedef mmSInt32_t      mmShortIntervalTime_t;

#define mmTm_sec            tm_sec
#define mmTm_min            tm_min
#define mmTm_hour           tm_hour
#define mmTm_mday           tm_mday
#define mmTm_mon            tm_mon
#define mmTm_year           tm_year
#define mmTm_wday           tm_wday
#define mmTm_isdst          tm_isdst

#define mmTm_sec_t          int
#define mmTm_min_t          int
#define mmTm_hour_t         int
#define mmTm_mday_t         int
#define mmTm_mon_t          int
#define mmTm_year_t         int
#define mmTm_wday_t         int
// struct timeval
#define mmTimevalSSec_t    time_t
#define mmTimevalUSec_t    suseconds_t

#if (MM_HAVE_GMTOFF)
#define mmTm_gmtoff         tm_gmtoff
#define mmTm_zone           tm_zone
#endif


#if (MM_SOLARIS)

#define mmTimezone(isdst) (- (isdst ? altzone : timezone) / 60)

#else

#define mmTimezone(isdst) (- (isdst ? timezone + 3600 : timezone) / 60)

#endif

#define mmLocaltime localtime
MM_EXPORT_DLL void mmTimezone_Update(void);
MM_EXPORT_DLL void mmLocaltimeR(const time_t* s, struct tm *tm);
MM_EXPORT_DLL void mmLibcLocaltime(const time_t* s, struct tm *tm);
MM_EXPORT_DLL void mmLibcGmtime(const time_t* s, struct tm *tm);
MM_EXPORT_DLL void mmLibcCtime(char* s, size_t sz, const time_t* t);

#define mmGettimeofday(tp,tz)  gettimeofday(tp,tz)
MM_EXPORT_DLL void mmSSleep(mmULong_t seconds);
MM_EXPORT_DLL void mmMSleep(mmULong_t milliseconds);
MM_EXPORT_DLL void mmUSleep(mmULong_t microseconds);
MM_EXPORT_DLL mmUInt64_t mmTime_CurrentUSec(void);

// Get the system running Microsecond.
MM_EXPORT_DLL mmUInt64_t mmTime_GetSystemRunningUSec(void);

#define MM_TIMER_INFINITE  (mmMSec_t) -1
#define MM_TIMER_LAZY_DELAY  300

#define MM_MSEC_PER_SEC 1000
#define MM_USEC_PER_SEC 1000000
#define MM_NSEC_PER_SEC 1000000000

/** @return mmTime_t as a second */
#define mmTime_Sec(time) ((time) / MM_USEC_PER_SEC)

/** @return mmTime_t as a usec */
#define mmTime_USec(time) ((time) % MM_USEC_PER_SEC)

/** @return mmTime_t as a msec */
#define mmTime_MSec(time) (((time) / 1000) % 1000)

/** @return mmTime_t as a msec */
#define mmTime_AsMSec(time) ((time) / 1000)

/** @return milliseconds as an mmTime_t */
#define mmTime_FromMSec(msec) ((mmTime_t)(msec) * 1000)

/** @return seconds as an mmTime_t */
#define mmTime_FromSec(sec) ((mmTime_t)(sec) * MM_USEC_PER_SEC)

/** @return a second and usec combination as an mmTime_t */
#define mmTime_Make(sec, usec) ((mmTime_t)(sec) * MM_USEC_PER_SEC + (mmTime_t)(usec))

static const mmUInt32_t     MM_SECONDS_MIN   = 60;      // 60
static const mmUInt32_t     MM_SECONDS_HOUR  = 3600;    // 60 * 60
static const mmUInt32_t     MM_SECONDS_DAY   = 86400;   // 24 * 60 * 60;
static const mmUInt32_t     MM_SECONDS_WEEK  = 604800;  //  7 * 24 * 60 * 60;
static const mmUInt32_t     MM_SECONDS_MONTH = 2592000; // 30 * 24 * 60 * 60;

#include "core/mmSuffix.h"

#endif//__mmTime_h__
