/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRuntimeStat.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

MM_EXPORT_DLL void mmRuntimeLoadavg_Init(struct mmRuntimeLoadavg* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeLoadavg));
}
MM_EXPORT_DLL void mmRuntimeLoadavg_Destroy(struct mmRuntimeLoadavg* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeLoadavg));
}
MM_EXPORT_DLL void mmRuntimeLoadavg_Perform(struct mmRuntimeLoadavg* p)
{
    // struct loadavg {
    //         fixpt_t ldavg[3];
    //         long    fscale;
    // };
    struct loadavg _loadavg;
    size_t size = sizeof(struct loadavg);
    static const int arg_n = 2;
    int mib[arg_n];
    mib[0] = CTL_VM;
    mib[1] = VM_LOADAVG;
    sysctl(mib, arg_n, &_loadavg, &size, NULL, 0);
    p->lavg_01 = _loadavg.ldavg[0] * 1.0f / _loadavg.fscale;
    p->lavg_05 = _loadavg.ldavg[1] * 1.0f / _loadavg.fscale;
    p->lavg_15 = _loadavg.ldavg[2] * 1.0f / _loadavg.fscale;
}

MM_EXPORT_DLL void mmRuntimeMachine_Init(struct mmRuntimeMachine* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeMachine));
}
MM_EXPORT_DLL void mmRuntimeMachine_Destroy(struct mmRuntimeMachine* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeMachine));
}
MM_EXPORT_DLL void mmRuntimeMachine_Perform(struct mmRuntimeMachine* p)
{
    mmRuntimeMachine_PerformCpu(p);
    mmRuntimeMachine_PerformMem(p);
}
MM_EXPORT_DLL void mmRuntimeMachine_PerformCpu(struct mmRuntimeMachine* p)
{
    p->cores = (mmUInt32_t)sysconf(_SC_NPROCESSORS_ONLN);
}
MM_EXPORT_DLL void mmRuntimeMachine_PerformMem(struct mmRuntimeMachine* p)
{
    {
        uint64_t hw_memsize = 0;
        size_t size = sizeof(uint64_t);
        static const int arg_n = 2;
        int mib[arg_n];
        mib[0] = CTL_HW;
        mib[1] = HW_MEMSIZE;
        sysctl(mib, arg_n, &hw_memsize, &size, NULL, 0);
        p->mem_total = hw_memsize;
    }
    // {
    //  uint64_t vm_free_target = 0;
    //  size_t size = sizeof(uint64_t);
    //  static const int arg_n = 2;
    //  int mib[arg_n];
    //  mib[0] = CTL_VM;
    //  mib[1] = VM_V_FREE_TARGET;
    //  sysctl(mib,arg_n, &vm_free_target, &size, NULL, 0);
    //  p->mem_free = vm_free_target;
    // }
}

MM_EXPORT_DLL void mmRuntimeProcess_Init(struct mmRuntimeProcess* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeProcess));
}
MM_EXPORT_DLL void mmRuntimeProcess_Destroy(struct mmRuntimeProcess* p)
{
    mmMemset(p, 0, sizeof(struct mmRuntimeProcess));
}
MM_EXPORT_DLL void mmRuntimeProcess_Perform(struct mmRuntimeProcess* p)
{
    mmRuntimeProcess_PerformPid(p);
    mmRuntimeProcess_PerformCpu(p);
    mmRuntimeProcess_PerformMem(p);
    mmRuntimeProcess_PerformIO(p);
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformPid(struct mmRuntimeProcess* p)
{
    p->pid = getpid();
}
MM_EXPORT_DLL void mmRuntimeProcess_PerformCpu(struct mmRuntimeProcess* p)
{
    struct kinfo_proc proc;
    size_t size = sizeof(struct kinfo_proc);
    static const int arg_n = 4;
    int mib[arg_n];
    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = (int)p->pid;
    sysctl(mib, arg_n, &proc, &size, NULL, 0);

    // u_quad_t p_uticks;       /* Statclock hits in user mode. */
    // u_quad_t p_sticks;       /* Statclock hits in system mode. */
    // u_quad_t p_iticks;       /* Statclock hits processing intr. */

    p->cpu_utime = proc.kp_proc.p_uticks;
    p->cpu_stime = proc.kp_proc.p_sticks;
    p->cpu_cutime = 0;
    p->cpu_cstime = 0;

}
MM_EXPORT_DLL void mmRuntimeProcess_PerformMem(struct mmRuntimeProcess* p)
{

}
MM_EXPORT_DLL void mmRuntimeProcess_PerformIO(struct mmRuntimeProcess* p)
{

}

MM_EXPORT_DLL void mmRuntimeStat_Init(struct mmRuntimeStat* p)
{
    p->pid = 0;
    p->cores = 0;
    p->vm_rss = 0;
    p->rchar = 0;
    p->wchar = 0;
    p->cpu_sys = 0;
    p->cpu_pro = 0;
    p->mem_pro = 0;
    p->timecode_o = 0;
    p->timecode_n = 0;
    p->sampling = 0;
    mmMemset(&p->tv_o, 0, sizeof(struct timeval));
    mmMemset(&p->tv_n, 0, sizeof(struct timeval));
    mmRuntimeLoadavg_Init(&p->loadavg_o);
    mmRuntimeLoadavg_Init(&p->loadavg_n);
    mmRuntimeMachine_Init(&p->machine_o);
    mmRuntimeMachine_Init(&p->machine_n);
    mmRuntimeProcess_Init(&p->process_o);
    mmRuntimeProcess_Init(&p->process_n);
}

MM_EXPORT_DLL void mmRuntimeStat_Destroy(struct mmRuntimeStat* p)
{
    p->pid = 0;
    p->cores = 0;
    p->vm_rss = 0;
    p->rchar = 0;
    p->wchar = 0;
    p->cpu_sys = 0;
    p->cpu_pro = 0;
    p->mem_pro = 0;
    p->timecode_o = 0;
    p->timecode_n = 0;
    p->sampling = 0;
    mmMemset(&p->tv_o, 0, sizeof(struct timeval));
    mmMemset(&p->tv_n, 0, sizeof(struct timeval));
    mmRuntimeLoadavg_Destroy(&p->loadavg_o);
    mmRuntimeLoadavg_Destroy(&p->loadavg_n);
    mmRuntimeMachine_Destroy(&p->machine_o);
    mmRuntimeMachine_Destroy(&p->machine_n);
    mmRuntimeProcess_Destroy(&p->process_o);
    mmRuntimeProcess_Destroy(&p->process_n);
}

MM_EXPORT_DLL void mmRuntimeStat_Update(struct mmRuntimeStat* p)
{
    mmUInt64_t b_utime, b_stime, b_cutime, b_cstime;
    mmUInt64_t b_user, b_nice, b_system, b_idle, a, b, c;
    //
    mmGettimeofday(&p->tv_n, NULL);
    p->timecode_n = p->tv_n.tv_sec * 1000 + (p->tv_n.tv_usec / 1000);
    p->sampling = p->timecode_n - p->timecode_o;
    //
    mmRuntimeLoadavg_Perform(&p->loadavg_n);
    mmRuntimeMachine_Perform(&p->machine_n);
    mmRuntimeProcess_Perform(&p->process_n);
    //
    p->pid = p->process_n.pid;
    p->vm_rss = p->process_n.vm_rss;
    p->rchar = p->process_n.rchar;
    p->wchar = p->process_n.wchar;
    p->cores = p->machine_n.cores;
    //
    b_utime = p->process_n.cpu_utime - p->process_o.cpu_utime;
    b_stime = p->process_n.cpu_stime - p->process_o.cpu_stime;
    b_cutime = p->process_n.cpu_cutime - p->process_o.cpu_cutime;
    b_cstime = p->process_n.cpu_cstime - p->process_o.cpu_cstime;
    a = b_utime + b_stime + b_cutime + b_cstime;

    b_user = p->machine_n.cpu_user - p->machine_o.cpu_user;
    b_nice = p->machine_n.cpu_nice - p->machine_o.cpu_nice;
    b_system = p->machine_n.cpu_system - p->machine_o.cpu_system;
    b_idle = p->machine_n.cpu_idle - p->machine_o.cpu_idle;
    b = b_user + b_nice + b_system + b_idle;
    c = b_user + b_nice + b_system;

    p->cpu_sys = (0 == c && 0 == b_idle) ? 0 : c * 100.0f / (c + b_idle);
    p->cpu_pro = (0 == b) ? 0 : a * 100.0f / b;
    p->mem_pro = p->vm_rss * 100.0f / p->machine_o.mem_total;
    //
    mmMemcpy(&p->loadavg_o, &p->loadavg_n, sizeof(struct mmRuntimeLoadavg));
    mmMemcpy(&p->machine_o, &p->machine_n, sizeof(struct mmRuntimeMachine));
    mmMemcpy(&p->process_o, &p->process_n, sizeof(struct mmRuntimeProcess));
    p->timecode_o = p->timecode_n;
}

