/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoll.h"
#include "core/mmAlloc.h"
#include "core/mmTime.h"
#include "core/mmString.h"

struct mmPollKqueue
{
    mmSocket_t pfd;
    struct kevent* ev;
};

MM_EXPORT_NET const mmChar_t* mmPoll_Api(void)
{
    return "mmPollKqueue";
}

MM_EXPORT_NET void mmPoll_Init(struct mmPoll* p)
{
    struct mmPollKqueue* _impl = NULL;
    p->length = MM_POLL_EVENT_DEFAULT_NUMBER;
    p->arrays = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * p->length);
    _impl = (struct mmPollKqueue*)mmMalloc(sizeof(struct mmPollKqueue));
    _impl->pfd = kqueue();
    _impl->ev = (struct kevent*)mmMalloc(sizeof(struct kevent) * p->length);
    p->impl = _impl;
    //
    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
    mmMemset(_impl->ev, 0, sizeof(struct kevent) * p->length);
}
MM_EXPORT_NET void mmPoll_Destroy(struct mmPoll* p)
{
    struct mmPollKqueue* _impl = (struct mmPollKqueue*)(p->impl);
    mmCloseSocket(_impl->pfd);
    mmFree(_impl->ev);
    mmFree(p->arrays);
    mmFree(_impl);
    //
    p->length = 0;
    p->arrays = NULL;
    p->impl = NULL;
}
MM_EXPORT_NET void mmPoll_SetLength(struct mmPoll* p, mmUInt32_t length)
{
    struct mmPollKqueue* _impl = (struct mmPollKqueue*)(p->impl);
    if (length < p->length)
    {
        struct mmPollEvent* v = NULL;
        struct kevent* ep_v = NULL;
        //
        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
        mmMemset(_impl->ev, 0, sizeof(struct kevent) * p->length);
        //
        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * length);
        mmFree(p->arrays);
        p->arrays = v;
        //
        ep_v = (struct kevent*)mmMalloc(sizeof(struct kevent) * length);
        mmMemcpy(ep_v, _impl->ev, sizeof(struct kevent) * length);
        mmFree(_impl->ev);
        _impl->ev = ep_v;
        //
        p->length = length;
    }
    else if (length > p->length)
    {
        struct mmPollEvent* v = NULL;
        struct kevent* ev = NULL;
        v = (struct mmPollEvent*)mmMalloc(sizeof(struct mmPollEvent) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmPollEvent) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        //
        ev = (struct kevent*)mmMalloc(sizeof(struct kevent) * length);
        mmMemcpy(ev, _impl->ev, sizeof(struct kevent) * p->length);
        mmFree(_impl->ev);
        _impl->ev = ev;
        //
        mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * length);
        mmMemset(_impl->ev, 0, sizeof(struct kevent) * length);
        //
        p->length = length;
    }
}
MM_EXPORT_NET mmUInt32_t mmPoll_GetLength(const struct mmPoll* p)
{
    return p->length;
}
MM_EXPORT_NET mmInt_t mmPoll_AddEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollKqueue* _impl = (struct mmPollKqueue*)(p->impl);
    struct kevent ke;
    if (mask & MM_PE_READABLE)
    {
        EV_SET(&ke, fd, EVFILT_READ, EV_ADD | EV_ENABLE, 0, 0, ud);
        if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
    }
    if (mask & MM_PE_WRITABLE)
    {
        EV_SET(&ke, fd, EVFILT_WRITE, EV_ADD | EV_ENABLE, 0, 0, ud);
        if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
    }
    return 0;
}
MM_EXPORT_NET mmInt_t mmPoll_ModEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollKqueue* _impl = (struct mmPollKqueue*)(p->impl);
    struct kevent ke;
    int ffr = mask & MM_PE_READABLE ? EV_ENABLE : EV_DISABLE;
    int ffw = mask & MM_PE_WRITABLE ? EV_ENABLE : EV_DISABLE;
    //
    EV_SET(&ke, fd, EVFILT_READ, ffr, 0, 0, ud);
    if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
    //
    EV_SET(&ke, fd, EVFILT_WRITE, ffw, 0, 0, ud);
    if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
    return 0;
}
MM_EXPORT_NET mmInt_t mmPoll_DelEvent(struct mmPoll* p, mmSocket_t fd, int mask, void* ud)
{
    struct mmPollKqueue* _impl = (struct mmPollKqueue*)(p->impl);
    struct kevent ke;
    if (mask & MM_PE_READABLE)
    {
        EV_SET(&ke, fd, EVFILT_READ, EV_DELETE, 0, 0, ud);
        if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
    }
    if (mask & MM_PE_WRITABLE)
    {
        EV_SET(&ke, fd, EVFILT_WRITE, EV_DELETE, 0, 0, ud);
        if (kevent(_impl->pfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
    }
    return 0;
}

MM_EXPORT_NET mmInt_t mmPoll_WaitEvent(struct mmPoll* p, mmMSec_t milliseconds)
{
    //
    struct timespec  tv;
    struct timespec* tp;
    //
    int n = 0;
    int i;
    //
    struct mmPollKqueue* _impl = (struct mmPollKqueue*)(p->impl);
    //
    if (milliseconds == MM_TIMER_INFINITE)
    {
        tp = NULL;
    }
    else
    {
        tv.tv_sec = (mmTimevalSSec_t)(milliseconds / 1000);
        tv.tv_nsec = (mmTimevalUSec_t)((milliseconds % 1000) * 1000000);
        tp = &tv;
    }
    mmMemset(p->arrays, 0, sizeof(struct mmPollEvent) * p->length);
    n = kevent(_impl->pfd, NULL, 0, _impl->ev, p->length, tp);

    for (i = 0; i < n; i++)
    {
        int mask = 0;
        struct kevent* ee = &(_impl->ev[i]);

        if (ee->filter & EVFILT_READ)  mask |= MM_PE_READABLE;
        if (ee->filter & EVFILT_WRITE) mask |= MM_PE_WRITABLE;

        p->arrays[i].s = ee->udata;
        p->arrays[i].mask = mask;
    }
    return n;
}
MM_EXPORT_NET void mmPoll_Start(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Interrupt(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Shutdown(struct mmPoll* p)
{

}
MM_EXPORT_NET void mmPoll_Join(struct mmPoll* p)
{

}