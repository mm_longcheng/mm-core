/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmErrno.h"
#include "core/mmString.h"
#include "core/mmLogger.h"
#include "core/mmAlloc.h"


/*
 * The strerror() messages are copied because:
 *
 * 1) strerror() and strerror_r() functions are not Async-Signal-Safe,
 *    therefore, they cannot be used in signal handlers;
 *
 * 2) a direct sys_errlist[] array may be used instead of these functions,
 *    but Linux linker warns about its usage:
 *
 * warning: `sys_errlist' is deprecated; use `strerror' or `strerror_r' instead
 * warning: `sys_nerr' is deprecated; use `strerror' or `strerror_r' instead
 *
 *    causing false bug reports.
 */


static struct mmString*  mm_sys_errlist;
static struct mmString   mm_unknown_error = mmString_Make("Unknown error");


MM_EXPORT_DLL mmUChar_t* mmStrError(mmErr_t err, mmUChar_t *errstr, size_t size)
{
    struct mmString* msg;

    msg = ((mmUInt_t)err < MM_SYS_NERR) ? &mm_sys_errlist[err] : &mm_unknown_error;
    size = size < mmString_Size(msg) ? size : mmString_Size(msg);

    return mmCpymem(errstr, mmString_CStr(msg), size);
}

MM_EXPORT_DLL mmInt_t mmStrError_Init(void)
{
    char* msg;
    size_t len;
    mmErr_t err;
    struct mmString* e = NULL;

    /*
     * mmStrError() is not ready to work at this stage, therefore,
     * malloc() is used and possible errors are logged using strerror().
     */

    len = MM_SYS_NERR * sizeof(struct mmString);

    mm_sys_errlist = (struct mmString*)mmMalloc(len);
    if (NULL != mm_sys_errlist)
    {
        for (err = 0; err < MM_SYS_NERR; err++)
        {
            msg = strerror(err);
            len = mmStrlen(msg);
            e = &mm_sys_errlist[err];
            mmString_Init(e);
            mmString_Assignsn(e, msg, len);
        }
        return MM_OK;
    }
    else
    {
        err = errno;
        mmLog_E("malloc(%u) failed (%d: %s)", (unsigned int)len, err, strerror(err));
    }
    return MM_ERROR;
}
MM_EXPORT_DLL mmInt_t mmStrError_Destroy(void)
{
    if (mm_sys_errlist)
    {
        mmErr_t err;
        struct mmString* e = NULL;
        for (err = 0; err < MM_SYS_NERR; err++)
        {
            e = &mm_sys_errlist[err];
            mmString_Destroy(e);
        }
    }
    return MM_OK;
}
