/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmClock.h"
#include "core/mmTimeval.h"

MM_EXPORT_DLL void mmClock_Init(struct mmClock* p)
{
    mmTimeval_Init(&p->start);
    p->zero_clock = 0;
    //
    mmClock_Reset(p);
}
MM_EXPORT_DLL void mmClock_Destroy(struct mmClock* p)
{
    mmTimeval_Init(&p->start);
    p->zero_clock = 0;
}
//
MM_EXPORT_DLL mmBool_t mmClock_SetOption(struct mmClock* p, const char* strKey, const void* pValue)
{
    return MM_FALSE;
}
//
MM_EXPORT_DLL void mmClock_Reset(struct mmClock* p)
{
    p->zero_clock = clock();
    mmGettimeofday(&p->start, NULL);
}
MM_EXPORT_DLL mmUInt64_t mmClock_Milliseconds(struct mmClock* p)
{
    struct timeval now;
    mmGettimeofday(&now, NULL);
    return (now.tv_sec - p->start.tv_sec) * 1000 + (now.tv_usec - p->start.tv_usec) / 1000;
}
MM_EXPORT_DLL mmUInt64_t mmClock_Microseconds(struct mmClock* p)
{
    struct timeval now;
    mmGettimeofday(&now, NULL);
    return (now.tv_sec - p->start.tv_sec) * 1000000 + (now.tv_usec - p->start.tv_usec);
}
MM_EXPORT_DLL mmUInt64_t mmClock_MillisecondsCpu(struct mmClock* p)
{
    clock_t new_clock = clock();
    return (mmUInt64_t)((float)(new_clock - p->zero_clock) / ((float)CLOCKS_PER_SEC / 1000.0));
}
MM_EXPORT_DLL mmUInt64_t mmClock_MicrosecondsCpu(struct mmClock* p)
{
    clock_t new_clock = clock();
    return (mmUInt64_t)((float)(new_clock - p->zero_clock) / ((float)CLOCKS_PER_SEC / 1000000.0));
}