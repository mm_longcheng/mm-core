/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileSystem_h__
#define __mmFileSystem_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

#include <dirent.h>
#include <unistd.h>
#include <fnmatch.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define MM_S_IFMT   S_IFMT
#define MM_S_IFSOCK S_IFSOCK
#define MM_S_IFLNK  S_IFLNK
#define MM_S_IFREG  S_IFREG
#define MM_S_IFBLK  S_IFBLK
#define MM_S_IFDIR  S_IFDIR
#define MM_S_IFCHR  S_IFCHR
#define MM_S_IFIFO  S_IFIFO
#define MM_S_ISUID  S_ISUID
#define MM_S_ISGID  S_ISGID
#define MM_S_ISVTX  S_ISVTX

#define MM_S_ISLNK(m)      S_ISLNK(m)
#define MM_S_ISREG(m)      S_ISREG(m)
#define MM_S_ISDIR(m)      S_ISDIR(m)
#define MM_S_ISCHR(m)      S_ISCHR(m)
#define MM_S_ISBLK(m)      S_ISBLK(m)
#define MM_S_ISFIFO(m)     S_ISFIFO(m)
#define MM_S_ISSOCK(m)     S_ISSOCK(m)

#define MM_S_IRWXU S_IRWXU
#define MM_S_IRUSR S_IRUSR
#define MM_S_IWUSR S_IWUSR
#define MM_S_IXUSR S_IXUSR

#define MM_S_IRWXG S_IRWXG
#define MM_S_IRGRP S_IRGRP
#define MM_S_IWGRP S_IWGRP
#define MM_S_IXGRP S_IXGRP

#define MM_S_IRWXO S_IRWXO
#define MM_S_IROTH S_IROTH
#define MM_S_IWOTH S_IWOTH
#define MM_S_IXOTH S_IXOTH

#define MM_INVALID_HANDLE_VALUE -1

/* Our simplified data entry structure */
struct _finddata_t
{
    char *name;
    int attrib;
    unsigned long size;
};

#define _A_NORMAL 0x00  /* Normalfile-Noread/writerestrictions */
#define _A_RDONLY 0x01  /* Read only file */
#define _A_HIDDEN 0x02  /* Hidden file */
#define _A_SYSTEM 0x04  /* System file */
#define _A_SUBDIR 0x10  /* Subdirectory */
#define _A_ARCH   0x20  /* Archive file */

#define mm_fopen fopen
#define mmFclose fclose

MM_EXPORT_DLL intptr_t mmFindFirst(const char* pattern, struct _finddata_t* data);
MM_EXPORT_DLL int mmFindNext(intptr_t id, struct _finddata_t* data);
MM_EXPORT_DLL int mmFindClose(intptr_t id);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mmPathMatch(const char* path, const char* pattern);
//////////////////////////////////////////////////////////////////////////
MM_EXPORT_DLL int mmIsReservedDirectory(const char* path);
MM_EXPORT_DLL int mmIsAbsolutePath(const char* path);
MM_EXPORT_DLL int mmAbsolutePathIsDataExists(const char* file_name);

// windows https://msdn.microsoft.com/en-us/library/1w06ktdy(v=vs.140).aspx
// unix    http://man7.org/linux/man-pages/man2/access.2.html
// 00 Existence only
// 02 Write-only
// 04 Read-only
// 06 Read and write
#define MM_ACCESS_F_OK F_OK
#define MM_ACCESS_W_OK W_OK
#define MM_ACCESS_R_OK R_OK
#define MM_ACCESS_X_OK X_OK
MM_EXPORT_DLL int mmAccess(const char* file_name, int mode);

typedef struct stat mmStat_t;
#define mmStat stat
#define mmFstat fstat
// struct stat st_mode for cross-platform value.
MM_EXPORT_DLL mmUInt32_t mmStat_Mode(mmStat_t* s);

MM_EXPORT_DLL int mmMkdir(const char *pathname, int mode);
MM_EXPORT_DLL int mmRmdir(const char* pathname);
MM_EXPORT_DLL int mmUnlink(const char* pathname);
MM_EXPORT_DLL int mmLink(const char* oldname, const char* newname);

// api for file system.
MM_EXPORT_DLL const char* mmFileSystemApi(void);

MM_EXPORT_DLL int mmCopyFile(const char* path_f, const char* path_t);
MM_EXPORT_DLL int mmRename(const char* path_f, const char* path_t);
MM_EXPORT_DLL int mmRemove(const char* path);
// recursive copy api.
MM_EXPORT_DLL int mmRecursiveCopy(const char* f, const char* t);
// recursive remove api.
MM_EXPORT_DLL int mmRecursiveRemove(const char* path);

#define mmFileNo fileno
MM_EXPORT_DLL int mmFileTruncate(FILE* f, mmUInt64_t length);

// Port API
MM_EXPORT_DLL int fseeko64(FILE* stream, int64_t offset, int whence);
MM_EXPORT_DLL int64_t ftello64(FILE* stream);

#define mmFSeeko   fseeko
#define mmFTello   ftello
#define mmFSeeko64 fseeko64
#define mmFTello64 ftello64

#include "core/mmSuffix.h"

#endif//__mmFileSystem_h__