/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConfigPlatform_h__
#define __mmConfigPlatform_h__

#include "core/mmPlatform.h"

#include "core/mmPrefix.h"
// sign current platform.
#define MM_DARWIN 1

#include "core/mmAutoHeaders.h"

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stddef.h>             /* offsetof() */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <glob.h>
#include <sys/mount.h>          /* statfs() */

#include <sys/filio.h>          /* FIONBIO */
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/wait.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sched.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>        /* TCP_NODELAY */
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/un.h>

#include <sys/sysctl.h>
#include <xlocale.h>


#ifndef IOV_MAX
#define IOV_MAX   64
#endif


#include <core/mmAutoConfig.h>


#if (MM_HAVE_POSIX_SEM)
#include <semaphore.h>
#endif


#if (MM_HAVE_POLL)
#include <poll.h>
#endif


#if (MM_HAVE_KQUEUE)
#include <sys/event.h>
#endif


#define MM_LISTEN_BACKLOG  -1


#ifndef MM_HAVE_INHERITED_NONBLOCK
#define MM_HAVE_INHERITED_NONBLOCK  1
#endif


#ifndef MM_HAVE_CASELESS_FILESYSTEM
#define MM_HAVE_CASELESS_FILESYSTEM  1
#endif


#define MM_HAVE_OS_SPECIFIC_INIT    1
#define MM_HAVE_DEBUG_MALLOC        1


extern char **environ;

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

// word size.
// word size. __GNUC__ __i386__ __x86_64__
#ifdef __x86_64__
#define MM_WORDSIZE 64
#define MM_SIZEOF_UINTPTR_T 8
#else
#define MM_WORDSIZE 32
#define MM_SIZEOF_UINTPTR_T 4
#endif

#include <strings.h>
// strcasecmp
// strncasecmp

#include "core/mmSuffix.h"

#endif//__mmConfigPlatform_h__