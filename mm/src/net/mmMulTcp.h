/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMulTcp_h__
#define __mmMulTcp_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"
#include "core/mmList.h"
#include "core/mmTime.h"
#include "core/mmString.h"

#include "net/mmNetExport.h"
#include "net/mmNetTcp.h"
#include "net/mmPacket.h"

#include "container/mmRbtreeU32.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// cache timeout default.
#define MM_MULTCP_CACHE_TIMEOUT_DEFAULT 5000

struct mmMulTcpCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack);
    void(*Broken)(void* obj);
    void(*Nready)(void* obj);
    void(*Finish)(void* obj);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmMulTcpCallback_Init(struct mmMulTcpCallback* p);
MM_EXPORT_NET void mmMulTcpCallback_Destroy(struct mmMulTcpCallback* p);

struct mmMulTcp
{
    // native end point.
    struct mmSockaddr ss_native;
    // remote end point.
    struct mmSockaddr ss_remote;
    // silk strong ref list.
    struct mmListHead list;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    // tcp callback handle implement.
    struct mmTcpCallback tcp_callback;
    // value ref. tcp callback.
    struct mmMulTcpCallback callback;
    // crypto callback.
    struct mmCryptoCallback crypto_callback;
    // cache ss_native node use for quick compare.
    struct mmString cache_native_node;
    // cache ss_remote node use for quick compare.
    struct mmString cache_remote_node;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t rbtree_locker;
    mmAtomic_t list_locker;
    mmAtomic_t locker;
    // thread key.
    pthread_key_t thread_key;
    // cache ss_remote port use for quick compare.
    mmUShort_t cache_remote_port;

    // tcp nonblock timeout.default is MM_NETTCP_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;

    // cache timecode, milliseconds.
    mmUInt64_t cache_timecode;
    // cache timeout, milliseconds. default MM_MULTCP_CACHE_TIMEOUT_DEFAULT.
    mmUInt32_t cache_timeout;

    // try bind and listen times.default is -1.
    mmUInt32_t trytimes;

    // unique_id.
    mmUInt64_t unique_id;
    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmMulTcp_Init(struct mmMulTcp* p);
MM_EXPORT_NET void mmMulTcp_Destroy(struct mmMulTcp* p);

// assign native address but not connect.
MM_EXPORT_NET void mmMulTcp_SetNative(struct mmMulTcp* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmMulTcp_SetRemote(struct mmMulTcp* p, const char* node, mmUShort_t port);

MM_EXPORT_NET void mmMulTcp_SetHandle(struct mmMulTcp* p, mmUInt32_t id, mmNetTcpHandleFunc callback);
MM_EXPORT_NET void mmMulTcp_SetDefaultCallback(struct mmMulTcp* p, struct mmMulTcpCallback* multcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMulTcp_SetTcpCallback(struct mmMulTcp* p, struct mmTcpCallback* tcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMulTcp_SetCryptoCallback(struct mmMulTcp* p, struct mmCryptoCallback* crypto_callback);
// assign context handle.
MM_EXPORT_NET void mmMulTcp_SetContext(struct mmMulTcp* p, void* u);
// cache timecode
MM_EXPORT_NET void mmMulTcp_SetCacheTimecode(struct mmMulTcp* p, mmUInt64_t cache_timecode);
// cache timeout
MM_EXPORT_NET void mmMulTcp_SetCacheTimeout(struct mmMulTcp* p, mmUInt32_t cache_timeout);

MM_EXPORT_NET void mmMulTcp_ClearHandleRbtree(struct mmMulTcp* p);

/* locker order is
*     locker
*/
MM_EXPORT_NET void mmMulTcp_Lock(struct mmMulTcp* p);
MM_EXPORT_NET void mmMulTcp_Unlock(struct mmMulTcp* p);

// fopen socket.
MM_EXPORT_NET void mmMulTcp_FopenSocket(struct mmMulTcp* p);
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_NET void mmMulTcp_Connect(struct mmMulTcp* p);
// close socket.
MM_EXPORT_NET void mmMulTcp_CloseSocket(struct mmMulTcp* p);

// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_NET void mmMulTcp_Check(struct mmMulTcp* p);

// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmMulTcp_AttachSocket(struct mmMulTcp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmMulTcp_DetachSocket(struct mmMulTcp* p);

// get current thread net tcp.
// note 
MM_EXPORT_NET struct mmNetTcp* mmMulTcp_ThreadInstance(struct mmMulTcp* p);
// clear all net tcp.
MM_EXPORT_NET void mmMulTcp_Clear(struct mmMulTcp* p);

MM_EXPORT_NET void mmMulTcp_Start(struct mmMulTcp* p);
MM_EXPORT_NET void mmMulTcp_Interrupt(struct mmMulTcp* p);
MM_EXPORT_NET void mmMulTcp_Shutdown(struct mmMulTcp* p);
MM_EXPORT_NET void mmMulTcp_Join(struct mmMulTcp* p);

#include "core/mmSuffix.h"

#endif//__mmMulTcp_h__
