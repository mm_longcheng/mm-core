/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmClientTcp_h__
#define __mmClientTcp_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmThread.h"
#include "core/mmSignalTask.h"

#include "net/mmNetTcp.h"
#include "net/mmPacketQueue.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

// client tcp connect handle check milliseconds.default is 5 second(5000 ms).
#define MM_CLIENT_TCP_HANDLE_MSEC_CHECK 5000
// client tcp connect broken check milliseconds.default is 1 second(1000 ms).
#define MM_CLIENT_TCP_BROKEN_MSEC_CHECK 1000

// send thread msleep interval handle time.
#define MM_CLIENT_TCP_SEND_INTERVAL_HANDLE_TIME 0
// send thread msleep interval broken time.
#define MM_CLIENT_TCP_SEND_INTERVAL_BROKEN_TIME 1000

enum mmClientTcpCheckFlag_t
{
    MM_CLIENT_TCP_CHECK_INACTIVE = 0,// do not check the connect state.
    MM_CLIENT_TCP_CHECK_ACTIVATE = 1,// check the connect state.
};
enum mmClientTcpQueueFlag_t
{
    MM_CLIENT_TCP_QUEUE_INACTIVE = 0,// not push to queue.
    MM_CLIENT_TCP_QUEUE_ACTIVATE = 1,// push to queue.
};

typedef void(*mmClientTcpHandleFunc)(void* obj, void* u, struct mmPacket* pack);
struct mmClientTcpCallback
{
    mmClientTcpHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmClientTcpCallback_Init(struct mmClientTcpCallback* p);
MM_EXPORT_NET void mmClientTcpCallback_Destroy(struct mmClientTcpCallback* p);

struct mmClientTcp
{
    // real net tcp.
    struct mmNetTcp net_tcp;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_n;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_q;
    // thread handle message queue.
    struct mmPacketQueue queue_recv;
    // thread handle message queue.
    struct mmPacketQueue queue_send;
    // value ref. tcp callback.
    struct mmClientTcpCallback callback_n;
    // value ref. tcp callback.
    struct mmClientTcpCallback callback_q;
    // flush task for flush send message buffer.
    struct mmSignalTask flush_task;
    // state task for check state.
    struct mmSignalTask state_task;
    mmAtomic_t locker_rbtree_n;
    mmAtomic_t locker_rbtree_q;
    mmAtomic_t locker;
    mmUInt8_t ss_native_update;
    mmUInt8_t ss_remote_update;
    // default is MM_CLIENT_TCP_CHECK_ACTIVATE.
    mmUInt8_t state_check_flag;
    // default is MM_CLIENT_TCP_QUEUE_ACTIVATE.
    mmUInt8_t state_queue_flag;
    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmClientTcp_Init(struct mmClientTcp* p);
MM_EXPORT_NET void mmClientTcp_Destroy(struct mmClientTcp* p);

/* locker order is
*     net_tcp
*     queue_recv
*     queue_send
*     flush_task
*     state_task
*     locker
*/
MM_EXPORT_NET void mmClientTcp_Lock(struct mmClientTcp* p);
MM_EXPORT_NET void mmClientTcp_Unlock(struct mmClientTcp* p);
// i locker
MM_EXPORT_NET void mmClientTcp_ILock(struct mmClientTcp* p);
MM_EXPORT_NET void mmClientTcp_IUnlock(struct mmClientTcp* p);
// o locker
MM_EXPORT_NET void mmClientTcp_OLock(struct mmClientTcp* p);
MM_EXPORT_NET void mmClientTcp_OUnlock(struct mmClientTcp* p);

// assign native address but not connect.
MM_EXPORT_NET void mmClientTcp_SetNative(struct mmClientTcp* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmClientTcp_SetRemote(struct mmClientTcp* p, const char* node, mmUShort_t port);

MM_EXPORT_NET void mmClientTcp_SetQDefaultCallback(struct mmClientTcp* p, struct mmClientTcpCallback* client_tcp_q_callback);
MM_EXPORT_NET void mmClientTcp_SetNDefaultCallback(struct mmClientTcp* p, struct mmClientTcpCallback* client_tcp_n_callback);
// assign queue callback.at main thread.
MM_EXPORT_NET void mmClientTcp_SetQHandle(struct mmClientTcp* p, mmUInt32_t id, mmClientTcpHandleFunc callback);
// assign net callback.not at main thread.
MM_EXPORT_NET void mmClientTcp_SetNHandle(struct mmClientTcp* p, mmUInt32_t id, mmClientTcpHandleFunc callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientTcp_SetTcpCallback(struct mmClientTcp* p, struct mmTcpCallback* tcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientTcp_SetEventTcpAllocator(struct mmClientTcp* p, struct mmNetTcpEventTcpAllocator* event_tcp_allocator);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientTcp_SetCryptoCallback(struct mmClientTcp* p, struct mmCryptoCallback* crypto_callback);
// assign context handle.
MM_EXPORT_NET void mmClientTcp_SetContext(struct mmClientTcp* p, void* u);

// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmClientTcp_SetAddrContext(struct mmClientTcp* p, void* u);
MM_EXPORT_NET void* mmClientTcp_GetAddrContext(struct mmClientTcp* p);

MM_EXPORT_NET void mmClientTcp_ClearHandleRbtree(struct mmClientTcp* p);

// client_tcp crypto encrypt buffer by tcp.
MM_EXPORT_NET void mmClientTcp_CryptoEncrypt(struct mmClientTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length);
// client_tcp crypto decrypt buffer by tcp.
MM_EXPORT_NET void mmClientTcp_CryptoDecrypt(struct mmClientTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length);

MM_EXPORT_NET void mmClientTcp_SetFlushHandleNearby(struct mmClientTcp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientTcp_SetFlushBrokenNearby(struct mmClientTcp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientTcp_SetStateHandleNearby(struct mmClientTcp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientTcp_SetStateBrokenNearby(struct mmClientTcp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientTcp_SetStateCheckFlag(struct mmClientTcp* p, mmUInt8_t flag);
MM_EXPORT_NET void mmClientTcp_SetStateQueueFlag(struct mmClientTcp* p, mmUInt8_t flag);

MM_EXPORT_NET void mmClientTcp_SetMaxPoperNumber(struct mmClientTcp* p, size_t max_pop);

// fopen socket.
MM_EXPORT_NET void mmClientTcp_FopenSocket(struct mmClientTcp* p);
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_NET void mmClientTcp_Connect(struct mmClientTcp* p);
// close socket.
MM_EXPORT_NET void mmClientTcp_CloseSocket(struct mmClientTcp* p);
// shutdown socket MM_BOTH_SHUTDOWN.
MM_EXPORT_NET void mmClientTcp_ShutdownSocket(struct mmClientTcp* p);

// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_NET void mmClientTcp_Check(struct mmClientTcp* p);
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmClientTcp_FinallyState(struct mmClientTcp* p);

// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmClientTcp_AttachSocket(struct mmClientTcp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmClientTcp_DetachSocket(struct mmClientTcp* p);
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_NET void mmClientTcp_ThreadHandleRecv(struct mmClientTcp* p);
// net thread trigger.
MM_EXPORT_NET void mmClientTcp_ThreadHandleSend(struct mmClientTcp* p);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_NET void mmClientTcp_PushRecv(struct mmClientTcp* p, void* obj, struct mmPacket* pack);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_NET void mmClientTcp_PushSend(struct mmClientTcp* p, void* obj, struct mmPacket* pack);

// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientTcp_BufferSend(struct mmClientTcp* p, mmUInt8_t* buffer, size_t offset, size_t length);
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientTcp_FlushSend(struct mmClientTcp* p);
// send buffer, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_NET void mmClientTcp_SendBuffer(struct mmClientTcp* p, struct mmPacketHead* packet_head, size_t hlength, struct mmByteBuffer* buffer);

// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_NET void mmClientTcp_FlushSignal(struct mmClientTcp* p);
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_NET void mmClientTcp_StateSignal(struct mmClientTcp* p);

// start wait thread.
MM_EXPORT_NET void mmClientTcp_Start(struct mmClientTcp* p);
// interrupt wait thread.
MM_EXPORT_NET void mmClientTcp_Interrupt(struct mmClientTcp* p);
// shutdown wait thread.
MM_EXPORT_NET void mmClientTcp_Shutdown(struct mmClientTcp* p);
// join wait thread.
MM_EXPORT_NET void mmClientTcp_Join(struct mmClientTcp* p);

MM_EXPORT_NET void mmClientTcp_TcpHandlePacketCallback(void* obj, struct mmPacket* pack);
MM_EXPORT_NET int mmClientTcp_TcpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length);
MM_EXPORT_NET int mmClientTcp_TcpBrokenCallback(void* obj);

#include "core/mmSuffix.h"

#endif//__mmClientTcp_h__
