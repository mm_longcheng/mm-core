/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmClientTcp.h"
#include "net/mmStreambufPacket.h"
#include "net/mmMessageCoder.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmClientTcp_NetTcpHandle(void* obj, void* u, struct mmPacket* pack);
static void __static_mmClientTcp_NetTcpBroken(void* obj);
static void __static_mmClientTcp_NetTcpNready(void* obj);
static void __static_mmClientTcp_NetTcpFinish(void* obj);

static int __static_mmClientTcp_FlushSignalTaskFactor(void* obj);
static int __static_mmClientTcp_FlushSignalTaskHandle(void* obj);
static int __static_mmClientTcp_StateSignalTaskFactor(void* obj);
static int __static_mmClientTcp_StateSignalTaskHandle(void* obj);

static void __static_mmClientTcp_PacketQueueSendHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmClientTcp_PacketQueueRecvHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmClientTcp_EmptyHandleForCommonEvent(struct mmClientTcp* p);

static void __static_mmClientTcp_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{

}
MM_EXPORT_NET void mmClientTcpCallback_Init(struct mmClientTcpCallback* p)
{
    p->Handle = &__static_mmClientTcp_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmClientTcpCallback_Destroy(struct mmClientTcpCallback* p)
{
    p->Handle = &__static_mmClientTcp_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmClientTcp_Init(struct mmClientTcp* p)
{
    struct mmTcpCallback tcp_callback;
    struct mmNetTcpCallback net_tcp_callback;
    struct mmSignalTaskCallback flush_task_callback;
    struct mmSignalTaskCallback state_task_callback;
    struct mmPacketQueueCallback queue_send_tcp_callback;
    struct mmPacketQueueCallback queue_recv_tcp_callback;

    mmNetTcp_Init(&p->net_tcp);
    mmRbtreeU32Vpt_Init(&p->rbtree_n);
    mmRbtreeU32Vpt_Init(&p->rbtree_q);
    mmPacketQueue_Init(&p->queue_recv);
    mmPacketQueue_Init(&p->queue_send);
    mmClientTcpCallback_Init(&p->callback_n);
    mmClientTcpCallback_Init(&p->callback_q);
    mmSignalTask_Init(&p->flush_task);
    mmSignalTask_Init(&p->state_task);
    mmSpinlock_Init(&p->locker_rbtree_n, NULL);
    mmSpinlock_Init(&p->locker_rbtree_q, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->ss_native_update = 0;
    p->ss_remote_update = 0;
    p->state_check_flag = MM_CLIENT_TCP_CHECK_INACTIVE;
    p->state_queue_flag = MM_CLIENT_TCP_QUEUE_ACTIVATE;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    tcp_callback.Handle = &mmClientTcp_TcpHandleCallback;
    tcp_callback.Broken = &mmClientTcp_TcpBrokenCallback;
    tcp_callback.obj = &p->net_tcp;
    mmNetTcp_SetTcpCallback(&p->net_tcp, &tcp_callback);

    net_tcp_callback.Handle = &__static_mmClientTcp_NetTcpHandle;
    net_tcp_callback.Broken = &__static_mmClientTcp_NetTcpBroken;
    net_tcp_callback.Nready = &__static_mmClientTcp_NetTcpNready;
    net_tcp_callback.Finish = &__static_mmClientTcp_NetTcpFinish;
    net_tcp_callback.obj = p;
    mmNetTcp_SetNetTcpCallback(&p->net_tcp, &net_tcp_callback);

    flush_task_callback.Factor = &__static_mmClientTcp_FlushSignalTaskFactor;
    flush_task_callback.Handle = &__static_mmClientTcp_FlushSignalTaskHandle;
    flush_task_callback.obj = p;
    mmSignalTask_SetCallback(&p->flush_task, &flush_task_callback);

    state_task_callback.Factor = &__static_mmClientTcp_StateSignalTaskFactor;
    state_task_callback.Handle = &__static_mmClientTcp_StateSignalTaskHandle;
    state_task_callback.obj = p;
    mmSignalTask_SetCallback(&p->state_task, &state_task_callback);

    queue_send_tcp_callback.Handle = &__static_mmClientTcp_PacketQueueSendHandle;
    queue_send_tcp_callback.obj = p;
    mmPacketQueue_SetQueueCallback(&p->queue_send, &queue_send_tcp_callback);

    queue_recv_tcp_callback.Handle = &__static_mmClientTcp_PacketQueueRecvHandle;
    queue_recv_tcp_callback.obj = p;
    mmPacketQueue_SetQueueCallback(&p->queue_recv, &queue_recv_tcp_callback);

    mmSignalTask_SetSuccessNearby(&p->flush_task, MM_CLIENT_TCP_SEND_INTERVAL_HANDLE_TIME);
    mmSignalTask_SetFailureNearby(&p->flush_task, MM_CLIENT_TCP_SEND_INTERVAL_BROKEN_TIME);
    mmSignalTask_SetSuccessNearby(&p->state_task, MM_CLIENT_TCP_HANDLE_MSEC_CHECK);
    mmSignalTask_SetFailureNearby(&p->state_task, MM_CLIENT_TCP_BROKEN_MSEC_CHECK);
    // empty handle for common event.
    __static_mmClientTcp_EmptyHandleForCommonEvent(p);
}
MM_EXPORT_NET void mmClientTcp_Destroy(struct mmClientTcp* p)
{
    mmClientTcp_ClearHandleRbtree(p);
    // empty handle for common event.
    __static_mmClientTcp_EmptyHandleForCommonEvent(p);
    //
    mmNetTcp_Destroy(&p->net_tcp);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_n);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_q);
    mmPacketQueue_Destroy(&p->queue_recv);
    mmPacketQueue_Destroy(&p->queue_send);
    mmClientTcpCallback_Destroy(&p->callback_n);
    mmClientTcpCallback_Destroy(&p->callback_q);
    mmSignalTask_Destroy(&p->flush_task);
    mmSignalTask_Destroy(&p->state_task);
    mmSpinlock_Destroy(&p->locker_rbtree_n);
    mmSpinlock_Destroy(&p->locker_rbtree_q);
    mmSpinlock_Destroy(&p->locker);
    p->ss_native_update = 0;
    p->ss_remote_update = 0;
    p->state_check_flag = MM_CLIENT_TCP_CHECK_ACTIVATE;
    p->state_queue_flag = MM_CLIENT_TCP_QUEUE_ACTIVATE;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

MM_EXPORT_NET void mmClientTcp_Lock(struct mmClientTcp* p)
{
    mmNetTcp_Lock(&p->net_tcp);
    mmPacketQueue_Lock(&p->queue_recv);
    mmPacketQueue_Lock(&p->queue_send);
    mmSignalTask_Lock(&p->flush_task);
    mmSignalTask_Lock(&p->state_task);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmClientTcp_Unlock(struct mmClientTcp* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmSignalTask_Unlock(&p->state_task);
    mmSignalTask_Unlock(&p->flush_task);
    mmPacketQueue_Unlock(&p->queue_send);
    mmPacketQueue_Unlock(&p->queue_recv);
    mmNetTcp_Unlock(&p->net_tcp);
}
// i locker
MM_EXPORT_NET void mmClientTcp_ILock(struct mmClientTcp* p)
{
    mmNetTcp_ILock(&p->net_tcp);
}
MM_EXPORT_NET void mmClientTcp_IUnlock(struct mmClientTcp* p)
{
    mmNetTcp_IUnlock(&p->net_tcp);
}
// o locker
MM_EXPORT_NET void mmClientTcp_OLock(struct mmClientTcp* p)
{
    mmNetTcp_OLock(&p->net_tcp);
}
MM_EXPORT_NET void mmClientTcp_OUnlock(struct mmClientTcp* p)
{
    mmNetTcp_OUnlock(&p->net_tcp);
}

// assign native address but not connect.
MM_EXPORT_NET void mmClientTcp_SetNative(struct mmClientTcp* p, const char* node, mmUShort_t port)
{
    mmNetTcp_SetNative(&p->net_tcp, node, port);
    p->ss_native_update = 1;
}
// assign remote address but not connect.
MM_EXPORT_NET void mmClientTcp_SetRemote(struct mmClientTcp* p, const char* node, mmUShort_t port)
{
    mmNetTcp_SetRemote(&p->net_tcp, node, port);
    p->ss_remote_update = 1;
}

MM_EXPORT_NET void mmClientTcp_SetQDefaultCallback(struct mmClientTcp* p, struct mmClientTcpCallback* client_tcp_q_callback)
{
    assert(NULL != client_tcp_q_callback && "you can not assign null client_tcp_q_callback.");
    p->callback_q = *client_tcp_q_callback;
}
MM_EXPORT_NET void mmClientTcp_SetNDefaultCallback(struct mmClientTcp* p, struct mmClientTcpCallback* client_tcp_n_callback)
{
    assert(NULL != client_tcp_n_callback && "you can not assign null client_tcp_n_callback.");
    p->callback_n = *client_tcp_n_callback;
}
// assign queue callback.at main thread.
MM_EXPORT_NET void mmClientTcp_SetQHandle(struct mmClientTcp* p, mmUInt32_t id, mmClientTcpHandleFunc callback)
{
    mmSpinlock_Lock(&p->locker_rbtree_q);
    mmRbtreeU32Vpt_Set(&p->rbtree_q, id, (void*)callback);
    mmSpinlock_Unlock(&p->locker_rbtree_q);
}
// assign net callback.not at main thread.
MM_EXPORT_NET void mmClientTcp_SetNHandle(struct mmClientTcp* p, mmUInt32_t id, mmClientTcpHandleFunc callback)
{
    mmSpinlock_Lock(&p->locker_rbtree_n);
    mmRbtreeU32Vpt_Set(&p->rbtree_n, id, (void*)callback);
    mmSpinlock_Unlock(&p->locker_rbtree_n);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientTcp_SetTcpCallback(struct mmClientTcp* p, struct mmTcpCallback* tcp_callback)
{
    mmNetTcp_SetTcpCallback(&p->net_tcp, tcp_callback);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientTcp_SetEventTcpAllocator(struct mmClientTcp* p, struct mmNetTcpEventTcpAllocator* event_tcp_allocator)
{
    mmNetTcp_SetEventTcpAllocator(&p->net_tcp, event_tcp_allocator);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientTcp_SetCryptoCallback(struct mmClientTcp* p, struct mmCryptoCallback* crypto_callback)
{
    mmNetTcp_SetCryptoCallback(&p->net_tcp, crypto_callback);
}
// assign context handle.
MM_EXPORT_NET void mmClientTcp_SetContext(struct mmClientTcp* p, void* u)
{
    p->u = u;
    mmPacketQueue_SetContext(&p->queue_recv, p->u);
    mmPacketQueue_SetContext(&p->queue_send, p->u);
    mmNetTcp_SetContext(&p->net_tcp, p->u);
}

// context for tcp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmClientTcp_SetAddrContext(struct mmClientTcp* p, void* u)
{
    mmNetTcp_SetAddrContext(&p->net_tcp, u);
}
MM_EXPORT_NET void* mmClientTcp_GetAddrContext(struct mmClientTcp* p)
{
    return mmNetTcp_GetAddrContext(&p->net_tcp);
}

MM_EXPORT_NET void mmClientTcp_ClearHandleRbtree(struct mmClientTcp* p)
{
    mmSpinlock_Lock(&p->locker_rbtree_n);
    mmRbtreeU32Vpt_Clear(&p->rbtree_n);
    mmSpinlock_Unlock(&p->locker_rbtree_n);

    mmSpinlock_Lock(&p->locker_rbtree_q);
    mmRbtreeU32Vpt_Clear(&p->rbtree_q);
    mmSpinlock_Unlock(&p->locker_rbtree_q);

    mmPacketQueue_ClearHandleRbtree(&p->queue_recv);
    mmPacketQueue_ClearHandleRbtree(&p->queue_send);
    mmNetTcp_ClearHandleRbtree(&p->net_tcp);
}

// client_tcp crypto encrypt buffer by tcp.
MM_EXPORT_NET void mmClientTcp_CryptoEncrypt(struct mmClientTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmNetTcp_CryptoEncrypt(&p->net_tcp, tcp, buffer, offset, length);
}
// client_tcp crypto decrypt buffer by tcp.
MM_EXPORT_NET void mmClientTcp_CryptoDecrypt(struct mmClientTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmNetTcp_CryptoDecrypt(&p->net_tcp, tcp, buffer, offset, length);
}

MM_EXPORT_NET void mmClientTcp_SetFlushHandleNearby(struct mmClientTcp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetSuccessNearby(&p->flush_task, milliseconds);
}
MM_EXPORT_NET void mmClientTcp_SetFlushBrokenNearby(struct mmClientTcp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetFailureNearby(&p->flush_task, milliseconds);
}
MM_EXPORT_NET void mmClientTcp_SetStateHandleNearby(struct mmClientTcp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetSuccessNearby(&p->state_task, milliseconds);
}
MM_EXPORT_NET void mmClientTcp_SetStateBrokenNearby(struct mmClientTcp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetFailureNearby(&p->state_task, milliseconds);
}
MM_EXPORT_NET void mmClientTcp_SetStateCheckFlag(struct mmClientTcp* p, mmUInt8_t flag)
{
    p->state_check_flag = flag;
}
MM_EXPORT_NET void mmClientTcp_SetStateQueueFlag(struct mmClientTcp* p, mmUInt8_t flag)
{
    p->state_queue_flag = flag;
}

MM_EXPORT_NET void mmClientTcp_SetMaxPoperNumber(struct mmClientTcp* p, size_t max_pop)
{
    mmPacketQueue_SetMaxPoperNumber(&p->queue_recv, max_pop);
    mmPacketQueue_SetMaxPoperNumber(&p->queue_send, max_pop);
}

// fopen socket.
MM_EXPORT_NET void mmClientTcp_FopenSocket(struct mmClientTcp* p)
{
    mmNetTcp_FopenSocket(&p->net_tcp);
}
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_NET void mmClientTcp_Connect(struct mmClientTcp* p)
{
    mmNetTcp_Connect(&p->net_tcp);
}
// close socket.
MM_EXPORT_NET void mmClientTcp_CloseSocket(struct mmClientTcp* p)
{
    mmNetTcp_CloseSocket(&p->net_tcp);
}
// shutdown socket MM_BOTH_SHUTDOWN.
MM_EXPORT_NET void mmClientTcp_ShutdownSocket(struct mmClientTcp* p)
{
    mmNetTcp_ShutdownSocket(&p->net_tcp, MM_BOTH_SHUTDOWN);
}

MM_EXPORT_NET void mmClientTcp_Check(struct mmClientTcp* p)
{
    mmNetTcp_Check(&p->net_tcp);
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmClientTcp_FinallyState(struct mmClientTcp* p)
{
    return mmNetTcp_FinallyState(&p->net_tcp);
}

// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmClientTcp_AttachSocket(struct mmClientTcp* p)
{
    mmNetTcp_AttachSocket(&p->net_tcp);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmClientTcp_DetachSocket(struct mmClientTcp* p)
{
    mmNetTcp_DetachSocket(&p->net_tcp);
}
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_NET void mmClientTcp_ThreadHandleRecv(struct mmClientTcp* p)
{
    mmPacketQueue_ThreadHandle(&p->queue_recv);
}
// net thread trigger.
MM_EXPORT_NET void mmClientTcp_ThreadHandleSend(struct mmClientTcp* p)
{
    mmPacketQueue_ThreadHandle(&p->queue_send);
}
MM_EXPORT_NET void mmClientTcp_PushRecv(struct mmClientTcp* p, void* obj, struct mmPacket* pack)
{
    mmPacketQueue_Push(&p->queue_recv, obj, pack, &p->net_tcp.tcp.socket.ss_remote);
}
MM_EXPORT_NET void mmClientTcp_PushSend(struct mmClientTcp* p, void* obj, struct mmPacket* pack)
{
    mmPacketQueue_Push(&p->queue_send, obj, pack, &p->net_tcp.tcp.socket.ss_remote);
}

// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientTcp_BufferSend(struct mmClientTcp* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    return mmNetTcp_BufferSend(&p->net_tcp, buffer, offset, length);
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientTcp_FlushSend(struct mmClientTcp* p)
{
    return mmNetTcp_FlushSend(&p->net_tcp);
}
MM_EXPORT_NET void mmClientTcp_SendBuffer(struct mmClientTcp* p, struct mmPacketHead* packet_head, size_t hlength, struct mmByteBuffer* buffer)
{
    struct mmTcp* tcp = &p->net_tcp.tcp;
    struct mmPoolPacketElem* pool_packet_elem = mmPacketQueue_Overdraft(&p->queue_send, hlength, buffer->length);

    mmPoolPacketElem_SetRemote(pool_packet_elem, &tcp->socket.ss_remote);
    mmPoolPacketElem_SetObject(pool_packet_elem, tcp);
    mmPoolPacketElem_SetBuffer(pool_packet_elem, packet_head, buffer);

    mmPacketQueue_Repayment(&p->queue_send, pool_packet_elem);
}

// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_NET void mmClientTcp_FlushSignal(struct mmClientTcp* p)
{
    mmSignalTask_Signal(&p->flush_task);
}
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_NET void mmClientTcp_StateSignal(struct mmClientTcp* p)
{
    mmSignalTask_Signal(&p->state_task);
}

// start wait thread.
MM_EXPORT_NET void mmClientTcp_Start(struct mmClientTcp* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmNetTcp_Start(&p->net_tcp);
    mmSignalTask_Start(&p->flush_task);
    mmSignalTask_Start(&p->state_task);
}
// interrupt wait thread.
MM_EXPORT_NET void mmClientTcp_Interrupt(struct mmClientTcp* p)
{
    p->state = MM_TS_CLOSED;

    p->state_check_flag = MM_CLIENT_TCP_CHECK_INACTIVE;
    p->state_queue_flag = MM_CLIENT_TCP_QUEUE_INACTIVE;

    mmNetTcp_Interrupt(&p->net_tcp);
    // signal the thread.
    mmSignalTask_Interrupt(&p->flush_task);
    mmSignalTask_Interrupt(&p->state_task);
}
// shutdown wait thread.
MM_EXPORT_NET void mmClientTcp_Shutdown(struct mmClientTcp* p)
{
    p->state = MM_TS_FINISH;

    p->state_check_flag = MM_CLIENT_TCP_CHECK_INACTIVE;
    p->state_queue_flag = MM_CLIENT_TCP_QUEUE_INACTIVE;

    mmNetTcp_Shutdown(&p->net_tcp);
    // signal the thread.
    mmSignalTask_Shutdown(&p->flush_task);
    mmSignalTask_Shutdown(&p->state_task);
}
// join wait thread.
MM_EXPORT_NET void mmClientTcp_Join(struct mmClientTcp* p)
{
    mmNetTcp_Join(&p->net_tcp);
    // signal the thread.
    mmSignalTask_Join(&p->flush_task);
    mmSignalTask_Join(&p->state_task);
}

MM_EXPORT_NET int mmClientTcp_TcpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    mmNetTcp_CryptoDecrypt(net_tcp, tcp, buffer, offset, length);
    return mmStreambufPacket_HandleTcp(&tcp->buff_recv, &mmClientTcp_TcpHandlePacketCallback, tcp);
}
MM_EXPORT_NET int mmClientTcp_TcpBrokenCallback(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    assert(NULL != net_tcp->callback.Broken && "net_tcp->callback.Broken is a null.");
    mmNetTcp_ApplyBroken(net_tcp);
    return 0;
}
MM_EXPORT_NET void mmClientTcp_TcpHandlePacketCallback(void* obj, struct mmPacket* pack)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    assert(NULL != net_tcp->callback.Handle && "net_tcp->callback.Handle is a null.");
    (*(net_tcp->callback.Handle))(tcp, net_tcp->u, pack);
}

static void __static_mmClientTcp_NetTcpHandle(void* obj, void* u, struct mmPacket* pack)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(net_tcp->callback.obj);
    mmClientTcpHandleFunc handle = NULL;
    mmSpinlock_Lock(&client_tcp->locker_rbtree_n);
    handle = (mmClientTcpHandleFunc)mmRbtreeU32Vpt_Get(&client_tcp->rbtree_n, pack->phead.mid);
    mmSpinlock_Unlock(&client_tcp->locker_rbtree_n);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(tcp, u, pack);
    }
    else
    {
        assert(NULL != client_tcp->callback_n.Handle && "client_tcp->callback_n.Handle is a null.");
        (*(client_tcp->callback_n.Handle))(tcp, u, pack);
    }
    // push to queue thread.
    if (MM_CLIENT_TCP_QUEUE_ACTIVATE == client_tcp->state_queue_flag)
    {
        // push recv api is lock free.
        mmClientTcp_PushRecv(client_tcp, obj, pack);
    }
}
static void __static_mmClientTcp_NetTcpBroken(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(net_tcp->callback.obj);

    mmNetTcp_MidEventHandle(obj, MM_TCP_MID_BROKEN, &__static_mmClientTcp_NetTcpHandle);
    // net state signal.
    mmClientTcp_StateSignal(client_tcp);
}
static void __static_mmClientTcp_NetTcpNready(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(net_tcp->callback.obj);

    mmNetTcp_MidEventHandle(obj, MM_TCP_MID_NREADY, &__static_mmClientTcp_NetTcpHandle);
    // net state signal.
    mmClientTcp_StateSignal(client_tcp);
}
static void __static_mmClientTcp_NetTcpFinish(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(net_tcp->callback.obj);

    mmNetTcp_MidEventHandle(obj, MM_TCP_MID_FINISH, &__static_mmClientTcp_NetTcpHandle);
    // net state signal.
    mmClientTcp_StateSignal(client_tcp);
}
static int __static_mmClientTcp_FlushSignalTaskFactor(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(signal_task->callback.obj);
    struct mmTcp* tcp = &client_tcp->net_tcp.tcp;
    size_t buff_len = 0;
    int queue_empty = 0;
    mmTcp_OLock(tcp);
    buff_len = mmStreambuf_Size(&tcp->buff_send);
    mmTcp_OUnlock(tcp);
    queue_empty = mmPacketQueue_IsEmpty(&client_tcp->queue_send);
    code = (0 == buff_len && queue_empty ? 0 : -1);
    return code;
}
static int __static_mmClientTcp_FlushSignalTaskHandle(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(signal_task->callback.obj);
    int real_len = 0;
    // check the state.
    if (0 == mmClientTcp_FinallyState(client_tcp))
    {
        do
        {
            {
                // handle send data by flush send buffer.
                // 0 <  rt,means rt buffer is send,we must gbump rt size.
                // 0 == rt,means the send buffer can be full.
                // 0 >  rt,means the send process can be failure.
                mmClientTcp_OLock(client_tcp);
                real_len = mmClientTcp_FlushSend(client_tcp);
                mmClientTcp_OUnlock(client_tcp);
                if (0 > real_len)
                {
                    // the socket is broken.
                    // but the send thread can not broken.
                    // it is wait the reconnect and send.
                    code = -1;
                    break;
                }
                else
                {
                    // the finally state is regular.
                    // if 0 == real_len.the state is broken or the socket send buffer is full.
                    // just think it is regular.
                    code = 0;
                }
            }
            {
                // here we only pop single message for handle.
                mmPacketQueue_PopSingle(&client_tcp->queue_send);

                // handle send data by.
                // use the net state for time task.
                if (0 == mmClientTcp_FinallyState(client_tcp))
                {
                    // the finally state is regular.
                    // if 0 == real_len.the state is broken or the socket send buffer is full.
                    // just think it is regular.
                    code = 0;
                }
                else
                {
                    // the socket is broken.
                    // but the send thread can not broken.
                    // it is wait the reconnect and send.
                    code = -1;
                    break;
                }
            }
        } while (0);
    }
    else
    {
        // the finally state is broken.
        // wake the state check process.
        mmClientTcp_StateSignal(client_tcp);
        code = -1;
    }
    return code;
}
static int __static_mmClientTcp_StateSignalTaskFactor(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(signal_task->callback.obj);
    code = (
        0 == client_tcp->ss_native_update &&
        0 == client_tcp->ss_remote_update &&
        0 == mmClientTcp_FinallyState(client_tcp)) ? 0 : -1;
    return code;
}
static int __static_mmClientTcp_StateSignalTaskHandle(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(signal_task->callback.obj);
    if (MM_CLIENT_TCP_CHECK_ACTIVATE == client_tcp->state_check_flag)
    {
        struct mmNetTcp* net_tcp = &client_tcp->net_tcp;
        struct mmTcp* tcp = &client_tcp->net_tcp.tcp;
        struct mmSocket* socket = &tcp->socket;
        if (
            0 != client_tcp->ss_native_update ||
            0 != client_tcp->ss_remote_update)
        {
            // if target address is not the real address,we disconnect current socket.
            mmNetTcp_SLock(net_tcp);
            // first disconnect current socket.
            mmNetTcp_DetachSocket(net_tcp);
            mmNetTcp_SUnlock(net_tcp);

            client_tcp->ss_native_update = 0;
            client_tcp->ss_remote_update = 0;
        }
        // most of time default ip port is valid ss_native address, not neeed check.
        if (0 == mmSockaddr_CompareAddress(&socket->ss_remote, MM_ADDR_DEFAULT_NODE, MM_ADDR_DEFAULT_PORT))
        {
            // if the native and remote is empty.fire the broken event immediately.
            mmNetTcp_SLock(net_tcp);
            mmNetTcp_ApplyBroken(net_tcp);
            mmNetTcp_SUnlock(net_tcp);
        }
        else
        {
            mmNetTcp_SLock(net_tcp);
            mmNetTcp_Check(net_tcp);
            mmNetTcp_SUnlock(net_tcp);
        }
    }
    code = (0 == mmClientTcp_FinallyState(client_tcp)) ? 0 : -1;
    return code;
}
static void __static_mmClientTcp_PacketQueueSendHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);

    // if connect state is good.
    if (0 == mmNetTcp_FinallyState(net_tcp))
    {
        struct mmPacket packet;

        struct mmStreambuf* streambuf = &tcp->buff_send;
        // handle send data by flush send buffer.
        // 0 <  rt,means rt buffer is send,we must gbump rt size.
        // 0 == rt,means the send buffer can be full.
        // 0 >  rt,means the send process can be failure.
        mmNetTcp_OLock(net_tcp);
        mmMessageCoder_EncodeMessageByteBuffer(&tcp->buff_send, &pack->bbuff, &pack->phead, pack->hbuff.length, &packet);
        mmNetTcp_CryptoEncrypt(net_tcp, tcp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
        mmNetTcp_FlushSend(net_tcp);
        mmNetTcp_OUnlock(net_tcp);
    }
}
static void __static_mmClientTcp_PacketQueueRecvHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmClientTcp* client_tcp = (struct mmClientTcp*)(net_tcp->callback.obj);

    mmClientTcpHandleFunc handle = NULL;
    mmSpinlock_Lock(&client_tcp->locker_rbtree_q);
    handle = (mmClientTcpHandleFunc)mmRbtreeU32Vpt_Get(&client_tcp->rbtree_q, pack->phead.mid);
    mmSpinlock_Unlock(&client_tcp->locker_rbtree_q);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(tcp, u, pack);
    }
    else
    {
        assert(NULL != client_tcp->callback_q.Handle && "client_tcp->callback_q.Handle is a null.");
        (*(client_tcp->callback_q.Handle))(tcp, u, pack);
    }
}
static void __static_mmClientTcp_EmptyHandleForCommonEvent(struct mmClientTcp* p)
{
    mmClientTcp_SetQHandle(p, MM_TCP_MID_FINISH, &__static_mmClientTcp_HandleDefault);
    mmClientTcp_SetQHandle(p, MM_TCP_MID_NREADY, &__static_mmClientTcp_HandleDefault);
    mmClientTcp_SetQHandle(p, MM_TCP_MID_BROKEN, &__static_mmClientTcp_HandleDefault);

    mmClientTcp_SetNHandle(p, MM_TCP_MID_FINISH, &__static_mmClientTcp_HandleDefault);
    mmClientTcp_SetNHandle(p, MM_TCP_MID_NREADY, &__static_mmClientTcp_HandleDefault);
    mmClientTcp_SetNHandle(p, MM_TCP_MID_BROKEN, &__static_mmClientTcp_HandleDefault);
}
