/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBufferPacket.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmByteswap.h"

// packet handle tcp buffer and length for string handle only one packet.
MM_EXPORT_NET int
mmBufferPacket_HandleTcp(
    mmUInt8_t* buffer,
    size_t offset,
    size_t length,
    mmPacketHandleTcpFunc handle,
    void* obj)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    int code = 0;
    struct mmPacket pack;
    struct mmPacketHead phead;
    mmUInt16_t msg_size = 0;
    size_t buff_size = 0;
    // use net byte order.
    mmUInt16_t message_size_ne = 0;
    mmUInt16_t hbuff_length_ne = 0;

    mmUInt8_t* buff = (mmUInt8_t*)(buffer + offset);

    buff_size = length;
    mmMemcpy(&message_size_ne, (void*)(buffer + offset), sizeof(mmUInt16_t));
    msg_size = mmLtoH16((mmUInt16_t)(message_size_ne));
    if (buff_size == msg_size)
    {
        mmMemcpy(&hbuff_length_ne, (void*)(buffer + offset + sizeof(mmUInt16_t)), sizeof(mmUInt16_t));
        // here we just use the weak ref from streambuf.for data recv callback.
        pack.hbuff.length = mmLtoH16((mmUInt16_t)(hbuff_length_ne));
        pack.hbuff.buffer = buff;
        pack.hbuff.offset = (mmUInt32_t)(sizeof(mmUInt32_t));
        //
        pack.bbuff.length = msg_size - sizeof(mmUInt32_t) - pack.hbuff.length;
        pack.bbuff.buffer = buff;
        pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
        // check buffer.
        if (msg_size < pack.hbuff.length + sizeof(mmUInt32_t) || MM_MSG_COMM_HEAD_SIZE < pack.hbuff.length)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid tcp message buffer.", __FUNCTION__, __LINE__);
            code = -1;
        }
        else
        {
            mmPacketHead_Reset(&phead);
            mmPacket_HeadDecode(&pack, &pack.hbuff, &phead);
            //
            // fire the field buffer recv event.
            (*(handle))(obj, &pack);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d data size is invalid. buff_size: %" PRIuPTR " msg_size: %u",
                      __FUNCTION__, __LINE__, buff_size, msg_size);
    }
    return code;
}

MM_EXPORT_NET int
mmBufferPacket_HandleUdp(
    mmUInt8_t* buffer,
    size_t offset,
    size_t length,
    mmPacketHandleUdpFunc handle,
    void* obj,
    struct mmSockaddr* remote)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    int code = 0;
    struct mmPacket pack;
    struct mmPacketHead phead;
    mmUInt16_t msg_size = 0;
    size_t buff_size = 0;
    // use net byte order.
    mmUInt16_t message_size_ne = 0;
    mmUInt16_t hbuff_length_ne = 0;

    mmUInt8_t* buff = (mmUInt8_t*)(buffer + offset);

    buff_size = length;
    mmMemcpy(&message_size_ne, (void*)(buffer + offset), sizeof(mmUInt16_t));
    msg_size = mmLtoH16((mmUInt16_t)(message_size_ne));
    if (buff_size == msg_size)
    {
        mmMemcpy(&hbuff_length_ne, (void*)(buffer + offset + sizeof(mmUInt16_t)), sizeof(mmUInt16_t));
        // here we just use the weak ref from streambuf.for data recv callback.
        pack.hbuff.length = mmLtoH16((mmUInt16_t)(hbuff_length_ne));
        pack.hbuff.buffer = buff;
        pack.hbuff.offset = (mmUInt32_t)(sizeof(mmUInt32_t));
        //
        pack.bbuff.length = msg_size - sizeof(mmUInt32_t) - pack.hbuff.length;
        pack.bbuff.buffer = buff;
        pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
        // check buffer.
        if (msg_size < pack.hbuff.length + sizeof(mmUInt32_t) || MM_MSG_COMM_HEAD_SIZE < pack.hbuff.length)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogE(gLogger, "%s %d invalid udp message buffer.", __FUNCTION__, __LINE__);
            code = -1;
        }
        else
        {
            mmPacketHead_Reset(&phead);
            mmPacket_HeadDecode(&pack, &pack.hbuff, &phead);
            //
            // fire the field buffer recv event.
            (*(handle))(obj, &pack, remote);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d data size is invalid. buff_size: %" PRIuPTR " msg_size: %u",
                      __FUNCTION__, __LINE__, buff_size, msg_size);
    }
    return code;
}
