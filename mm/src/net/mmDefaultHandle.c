/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDefaultHandle.h"
#include "net/mmSockaddr.h"
#include "net/mmPacket.h"
#include "net/mmUdp.h"
#include "net/mmTcp.h"
#include "core/mmLogger.h"

MM_EXPORT_NET void mmNetUdp_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSockaddr_NativeRemoteString(remote, &udp->socket.ss_native, udp->socket.socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X udp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmNetUdp_BrokenDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d udp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmNetUdp_NreadyDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d udp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmNetUdp_FinishDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d udp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmNetTcp_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X tcp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmNetTcp_BrokenDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmNetTcp_NreadyDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d tcp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmNetTcp_FinishDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}

MM_EXPORT_NET void mmMailbox_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X tcp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmMailbox_BrokenDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmMailbox_NreadyDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d tcp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmMailbox_FinishDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmHeadset_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSockaddr_NativeRemoteString(remote, &udp->socket.ss_native, udp->socket.socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X udp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmHeadset_BrokenDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d udp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmHeadset_NreadyDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d udp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmHeadset_FinishDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d udp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmContact_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X tcp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmContact_BrokenDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmContact_NreadyDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d tcp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmContact_FinishDefault(void* obj)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}

MM_EXPORT_NET void mmPacketQueue_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmSockaddr ss_native;
    mmSockaddr_Init(&ss_native);
    mmSockaddr_NativeRemoteString(remote, &ss_native, -1, link_name);
    mmSockaddr_Destroy(&ss_native);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X net:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmPacketQueue_BrokenDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmSockaddr ss_native;
    mmSockaddr_Init(&ss_native);
    mmSockaddr_NativeRemoteString(remote, &ss_native, -1, link_name);
    mmSockaddr_Destroy(&ss_native);
    mmLogger_LogI(gLogger, "%s %d net:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmPacketQueue_NreadyDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmSockaddr ss_native;
    mmSockaddr_Init(&ss_native);
    mmSockaddr_NativeRemoteString(remote, &ss_native, -1, link_name);
    mmSockaddr_Destroy(&ss_native);
    mmLogger_LogW(gLogger, "%s %d net:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmPacketQueue_FinishDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmSockaddr ss_native;
    mmSockaddr_Init(&ss_native);
    mmSockaddr_NativeRemoteString(remote, &ss_native, -1, link_name);
    mmSockaddr_Destroy(&ss_native);
    mmLogger_LogI(gLogger, "%s %d net:%s is finish.", __FUNCTION__, __LINE__, link_name);
}

MM_EXPORT_NET void mmClientUdp_NHandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSockaddr_NativeRemoteString(remote, &udp->socket.ss_native, udp->socket.socket, link_name);
    mmLogger_LogV(gLogger, "%s %d mid:0x%08X udp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmClientUdp_QHandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSockaddr_NativeRemoteString(remote, &udp->socket.ss_native, udp->socket.socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X udp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmClientUdp_BrokenDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d udp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmClientUdp_NreadyDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d udp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmClientUdp_FinishDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&udp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d udp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}

MM_EXPORT_NET void mmClientTcp_NHandleDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogV(gLogger, "%s %d mid:0x%08X tcp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmClientTcp_QHandleDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X tcp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}
MM_EXPORT_NET void mmClientTcp_BrokenDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is broken.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmClientTcp_NreadyDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogW(gLogger, "%s %d tcp:%s is nready.", __FUNCTION__, __LINE__, link_name);
}
MM_EXPORT_NET void mmClientTcp_FinishDefault(void* obj, void* u, struct mmPacket* pack)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSocket_ToString(&tcp->socket, link_name);
    mmLogger_LogI(gLogger, "%s %d tcp:%s is finish.", __FUNCTION__, __LINE__, link_name);
}
