/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSocket_h__
#define __mmSocket_h__

#include "core/mmCore.h"
#include "core/mmOSSocket.h"
#include "core/mmAtomic.h"

#include "net/mmNetExport.h"
#include "net/mmSockaddr.h"

#include "core/mmPrefix.h"

//   Network             MTU (bytes)
//   -------------------------------
//   16 Mbps Token Ring        17914
//    4 Mbps Token Ring         4464
//   FDDI                       4352
//   Ethernet                   1500
//   IEEE 802.3/802.2           1492
//   PPPoE (WAN Miniport)       1480
//   X.25                        576
//
// socket default page size.
#define MM_SOCKET_PAGE_SIZE 1024

struct mmSocket
{
    // native end point.
    // most of time, the data is lock free.
    struct mmSockaddr ss_native;

    // remote end point.
    // most of time, the data is lock free.
    struct mmSockaddr ss_remote;

    // strong ref.
    // most of time, the data is lock free.
    mmSocket_t socket;

    // external locker.
    mmAtomic_t locker;
};

MM_EXPORT_NET void mmSocket_Init(struct mmSocket* p);
MM_EXPORT_NET void mmSocket_Destroy(struct mmSocket* p);

/* The external lock interface */
MM_EXPORT_NET void mmSocket_Lock(struct mmSocket* p);
MM_EXPORT_NET void mmSocket_Unlock(struct mmSocket* p);

MM_EXPORT_NET void mmSocket_Reset(struct mmSocket* p);

// alloc socket fd.
// tcp PF_INET, SOCK_STREAM, 0
// udp PF_INET, SOCK_DGRAM , 0
MM_EXPORT_NET void mmSocket_FopenSocket(struct mmSocket* p, int domain, int type, int protocol);
// close socket fd if fd is valid. fd == -1 is call safe.
MM_EXPORT_NET void mmSocket_CloseSocket(struct mmSocket* p);
// close socket fd. not set socket_t to invalid -1.
// use to fire socket fd close event.
MM_EXPORT_NET void mmSocket_CloseSocketEvent(struct mmSocket* p);
// shutdown socket.
MM_EXPORT_NET void mmSocket_ShutdownSocket(struct mmSocket* p, int opcode);
// assign addr native by ip port. AF_INET; after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmSocket_SetNative(struct mmSocket* p, const char* node, mmUShort_t port);
// assign addr remote by ip port. AF_INET; after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmSocket_SetRemote(struct mmSocket* p, const char* node, mmUShort_t port);
// assign addr native by sockaddr_storage. AF_INET; after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmSocket_SetNativeStorage(struct mmSocket* p, struct mmSockaddr* ss_native);
// assign addr remote by sockaddr_storage. AF_INET; after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmSocket_SetRemoteStorage(struct mmSocket* p, struct mmSockaddr* ss_remote);
// default is blocking. MM_BLOCKING means block MM_NONBLOCK no block.
MM_EXPORT_NET int mmSocket_SetBlock(struct mmSocket* p, int block);

// connect remote address.
MM_EXPORT_NET int mmSocket_Connect(struct mmSocket* p);
// bind native address.
MM_EXPORT_NET int mmSocket_Bind(struct mmSocket* p);
// listen native address.
MM_EXPORT_NET int mmSocket_Listen(struct mmSocket* p, int backlog);
// obtain bind socket real sockaddr. such as bind for port 0. need use this obtain real port.
MM_EXPORT_NET int mmSocket_CurrentBindSockaddr(struct mmSocket* p, struct mmSockaddr* ss_native);
// SO_REUSEADDR
MM_EXPORT_NET void mmSocket_ReUseAddr(struct mmSocket* p);

// we not use c style char* but use buffer and offset target the real buffer, because other language might not have pointer.

// @param b buffer
// @param o offset
// @param l lenght
// @param f flags
MM_EXPORT_NET int mmSocket_Recv(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f);

// @param b buffer
// @param o offset
// @param l lenght
// @param f flags
MM_EXPORT_NET int mmSocket_Send(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f);

// @param b buffer
// @param o offset
// @param l lenght
// @param f flags
// @param a remote sockaddr pointer
// @param s remote sockaddr size
MM_EXPORT_NET int mmSocket_SendDgram(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f, struct sockaddr* a, socklen_t s);

// @param b buffer
// @param o offset
// @param l lenght
// @param f flags
// @param a remote sockaddr pointer
// @param s remote sockaddr size pointer
MM_EXPORT_NET int mmSocket_RecvDgram(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f, struct sockaddr* a, socklen_t* s);

// (fd)|ip-port --> ip-port
// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
MM_EXPORT_NET void mmSocket_ToString(struct mmSocket* p, char link_name[MM_LINK_NAME_LENGTH]);

#include "core/mmSuffix.h"

#endif//__mmSocket_h__
