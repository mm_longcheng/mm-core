/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDefaultHandle_h__
#define __mmDefaultHandle_h__

#include "core/mmCore.h"

#include "net/mmPacket.h"
#include "net/mmSockaddr.h"

#include "core/mmPrefix.h"

MM_EXPORT_NET void mmNetUdp_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmNetUdp_BrokenDefault(void* obj);
MM_EXPORT_NET void mmNetUdp_NreadyDefault(void* obj);
MM_EXPORT_NET void mmNetUdp_FinishDefault(void* obj);

MM_EXPORT_NET void mmNetTcp_HandleDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmNetTcp_BrokenDefault(void* obj);
MM_EXPORT_NET void mmNetTcp_NreadyDefault(void* obj);
MM_EXPORT_NET void mmNetTcp_FinishDefault(void* obj);

MM_EXPORT_NET void mmMailbox_HandleDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmMailbox_BrokenDefault(void* obj);
MM_EXPORT_NET void mmMailbox_NreadyDefault(void* obj);
MM_EXPORT_NET void mmMailbox_FinishDefault(void* obj);

MM_EXPORT_NET void mmHeadset_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmHeadset_BrokenDefault(void* obj);
MM_EXPORT_NET void mmHeadset_NreadyDefault(void* obj);
MM_EXPORT_NET void mmHeadset_FinishDefault(void* obj);

MM_EXPORT_NET void mmContact_HandleDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmContact_BrokenDefault(void* obj);
MM_EXPORT_NET void mmContact_NreadyDefault(void* obj);
MM_EXPORT_NET void mmContact_FinishDefault(void* obj);

MM_EXPORT_NET void mmPacketQueue_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmPacketQueue_BrokenDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmPacketQueue_NreadyDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmPacketQueue_FinishDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

MM_EXPORT_NET void mmClientUdp_NHandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmClientUdp_QHandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmClientUdp_BrokenDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmClientUdp_NreadyDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET void mmClientUdp_FinishDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

MM_EXPORT_NET void mmClientTcp_NHandleDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmClientTcp_QHandleDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmClientTcp_BrokenDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmClientTcp_NreadyDefault(void* obj, void* u, struct mmPacket* pack);
MM_EXPORT_NET void mmClientTcp_FinishDefault(void* obj, void* u, struct mmPacket* pack);

#include "core/mmSuffix.h"

#endif//__mmDefaultHandle_h__
