/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNetUdp.h"
#include "net/mmStreambufPacket.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmNetUdp_SelectorHandleRecv(void* obj, void* u);
static void __static_mmNetUdp_SelectorHandleSend(void* obj, void* u);

static void __static_mmNetUdp_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{

}
static void __static_mmNetUdp_BrokenDefault(void* obj)
{

}
static void __static_mmNetUdp_NreadyDefault(void* obj)
{

}
static void __static_mmNetUdp_FinishDefault(void* obj)
{

}
MM_EXPORT_NET void mmNetUdpCallback_Init(struct mmNetUdpCallback* p)
{
    p->Handle = &__static_mmNetUdp_HandleDefault;
    p->Broken = &__static_mmNetUdp_BrokenDefault;
    p->Nready = &__static_mmNetUdp_NreadyDefault;
    p->Finish = &__static_mmNetUdp_FinishDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmNetUdpCallback_Destroy(struct mmNetUdpCallback* p)
{
    p->Handle = &__static_mmNetUdp_HandleDefault;
    p->Broken = &__static_mmNetUdp_BrokenDefault;
    p->Nready = &__static_mmNetUdp_NreadyDefault;
    p->Finish = &__static_mmNetUdp_FinishDefault;
    p->obj = NULL;
}

static void __static_mmNetUdp_EventUdpProduce(void* p, struct mmUdp* udp)
{

}
static void __static_mmNetUdp_EventUdpRecycle(void* p, struct mmUdp* udp)
{

}
MM_EXPORT_NET void mmNetUdpEventUdpAllocator_Init(struct mmNetUdpEventUdpAllocator* p)
{
    p->EventUdpProduce = &__static_mmNetUdp_EventUdpProduce;
    p->EventUdpRecycle = &__static_mmNetUdp_EventUdpRecycle;
    p->obj = NULL;
}
MM_EXPORT_NET void mmNetUdpEventUdpAllocator_Destroy(struct mmNetUdpEventUdpAllocator* p)
{
    p->EventUdpProduce = &__static_mmNetUdp_EventUdpProduce;
    p->EventUdpRecycle = &__static_mmNetUdp_EventUdpRecycle;
    p->obj = NULL;
}

MM_EXPORT_NET void mmNetUdp_Init(struct mmNetUdp* p)
{
    struct mmUdpCallback udp_callback;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    struct mmSelectorCallback selector_callback;

    mmUdp_Init(&p->udp);
    mmSelector_Init(&p->selector);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmUdpCallback_Init(&p->udp_callback);
    mmNetUdpCallback_Init(&p->callback);
    mmNetUdpEventUdpAllocator_Init(&p->event_udp_allocator);
    mmCryptoCallback_Init(&p->crypto_callback);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->nonblock_timeout = MM_NETUDP_NONBLOCK_TIMEOUT;
    p->udp_state = MM_NETUDP_STATE_CLOSED;
    p->trytimes = MM_NETUDP_TRYTIME;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    p->udp_callback.Handle = &mmNetUdp_UdpHandleCallback;
    p->udp_callback.Broken = &mmNetUdp_UdpBrokenCallback;
    p->udp_callback.obj = p;

    udp_callback.Handle = p->udp_callback.Handle;
    udp_callback.Broken = p->udp_callback.Broken;
    udp_callback.obj = p;
    mmUdp_SetCallback(&p->udp, &udp_callback);

    selector_callback.HandleRecv = &__static_mmNetUdp_SelectorHandleRecv;
    selector_callback.HandleSend = &__static_mmNetUdp_SelectorHandleSend;
    selector_callback.obj = p;
    mmSelector_SetCallback(&p->selector, &selector_callback);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_NET void mmNetUdp_Destroy(struct mmNetUdp* p)
{
    mmNetUdp_DetachSocket(p);
    mmNetUdp_ClearHandleRbtree(p);
    //
    mmUdp_Destroy(&p->udp);
    mmSelector_Destroy(&p->selector);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmUdpCallback_Destroy(&p->udp_callback);
    mmNetUdpCallback_Destroy(&p->callback);
    mmNetUdpEventUdpAllocator_Destroy(&p->event_udp_allocator);
    mmCryptoCallback_Destroy(&p->crypto_callback);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->nonblock_timeout = 0;
    p->udp_state = MM_NETUDP_STATE_CLOSED;
    p->trytimes = 0;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

// streambuf reset.
MM_EXPORT_NET void mmNetUdp_ResetStreambuf(struct mmNetUdp* p)
{
    mmUdp_ResetStreambuf(&p->udp);
}

MM_EXPORT_NET void mmNetUdp_Lock(struct mmNetUdp* p)
{
    mmUdp_Lock(&p->udp);
    mmSelector_Lock(&p->selector);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmNetUdp_Unlock(struct mmNetUdp* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmSelector_Unlock(&p->selector);
    mmUdp_Unlock(&p->udp);
}
// s locker. 
MM_EXPORT_NET void mmNetUdp_SLock(struct mmNetUdp* p)
{
    mmUdp_SLock(&p->udp);
}
MM_EXPORT_NET void mmNetUdp_SUnlock(struct mmNetUdp* p)
{
    mmUdp_SUnlock(&p->udp);
}
// i locker. 
MM_EXPORT_NET void mmNetUdp_ILock(struct mmNetUdp* p)
{
    mmUdp_ILock(&p->udp);
}
MM_EXPORT_NET void mmNetUdp_IUnlock(struct mmNetUdp* p)
{
    mmUdp_IUnlock(&p->udp);
}
// o locker. 
MM_EXPORT_NET void mmNetUdp_OLock(struct mmNetUdp* p)
{
    mmUdp_OLock(&p->udp);
}
MM_EXPORT_NET void mmNetUdp_OUnlock(struct mmNetUdp* p)
{
    mmUdp_OUnlock(&p->udp);
}

// assign native address but not connect.
MM_EXPORT_NET void mmNetUdp_SetNative(struct mmNetUdp* p, const char* node, mmUShort_t port)
{
    mmUdp_SetNative(&p->udp, node, port);
}
// assign remote address but not connect.
MM_EXPORT_NET void mmNetUdp_SetRemote(struct mmNetUdp* p, const char* node, mmUShort_t port)
{
    mmUdp_SetRemote(&p->udp, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmNetUdp_SetNativeStorage(struct mmNetUdp* p, struct mmSockaddr* ss_native)
{
    mmUdp_SetNativeStorage(&p->udp, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmNetUdp_SetRemoteStorage(struct mmNetUdp* p, struct mmSockaddr* ss_remote)
{
    mmUdp_SetRemoteStorage(&p->udp, ss_remote);
}

MM_EXPORT_NET void mmNetUdp_SetHandle(struct mmNetUdp* p, mmUInt32_t id, mmNetUdpHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmNetUdp_SetNetUdpCallback(struct mmNetUdp* p, struct mmNetUdpCallback* net_udp_callback)
{
    assert(NULL != net_udp_callback && "you can not assign null net_udp_callback.");
    p->callback = *net_udp_callback;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetUdp_SetUdpCallback(struct mmNetUdp* p, struct mmUdpCallback* udp_callback)
{
    struct mmUdpCallback _udp_callback;
    assert(NULL != udp_callback && "you can not assign null udp_callback.");
    p->udp_callback = *udp_callback;

    _udp_callback.Handle = p->udp_callback.Handle;
    _udp_callback.Broken = p->udp_callback.Broken;
    _udp_callback.obj = p;
    mmUdp_SetCallback(&p->udp, &_udp_callback);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetUdp_SetEventUdpAllocator(struct mmNetUdp* p, struct mmNetUdpEventUdpAllocator* event_udp_allocator)
{
    assert(NULL != event_udp_allocator && "you can not assign null event_udp_alloc.");
    p->event_udp_allocator = *event_udp_allocator;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetUdp_SetCryptoCallback(struct mmNetUdp* p, struct mmCryptoCallback* crypto_callback)
{
    assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
    p->crypto_callback = *crypto_callback;
}
// assign context handle.
MM_EXPORT_NET void mmNetUdp_SetContext(struct mmNetUdp* p, void* u)
{
    p->u = u;
}
// assign nonblock_timeout.
MM_EXPORT_NET void mmNetUdp_SetNonblockTimeout(struct mmNetUdp* p, mmMSec_t nonblock_timeout)
{
    p->nonblock_timeout = nonblock_timeout;
}
// assign trytimes.
MM_EXPORT_NET void mmNetUdp_SetTrytimes(struct mmNetUdp* p, mmUInt32_t trytimes)
{
    p->trytimes = trytimes;
}

// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmNetUdp_SetAddrContext(struct mmNetUdp* p, void* u)
{
    mmUdp_SetContext(&p->udp, u);
}
MM_EXPORT_NET void* mmNetUdp_GetAddrContext(struct mmNetUdp* p)
{
    return mmUdp_GetContext(&p->udp);
}

MM_EXPORT_NET void mmNetUdp_ClearHandleRbtree(struct mmNetUdp* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

// net_udp crypto encrypt buffer by udp.
MM_EXPORT_NET void mmNetUdp_CryptoEncrypt(struct mmNetUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Encrypt))(p, udp, buffer, offset, buffer, offset, length);
}
// net_udp crypto decrypt buffer by udp.
MM_EXPORT_NET void mmNetUdp_CryptoDecrypt(struct mmNetUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Decrypt))(p, udp, buffer, offset, buffer, offset, length);
}

// fopen socket.
MM_EXPORT_NET void mmNetUdp_FopenSocket(struct mmNetUdp* p)
{
    struct mmSocket* socket = &p->udp.socket;

    mmUdp_FopenSocket(&p->udp);
    //
    // SO_REUSEADDR
    mmSocket_ReUseAddr(socket);
}
// bind.
MM_EXPORT_NET void mmNetUdp_Bind(struct mmNetUdp* p)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmSocket* socket = &p->udp.socket;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmSocket_ToString(socket, link_name);
    //
    if (MM_INVALID_SOCKET != socket->socket)
    {
        mmUInt32_t _try_times = 0;
        int rt = -1;
        p->udp_state = MM_NETUDP_STATE_MOTION;
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        do
        {
            _try_times++;
            rt = mmSocket_Bind(socket);
            if (0 == rt)
            {
                // bind immediately.
                break;
            }
            if (MM_TS_FINISH == p->state)
            {
                break;
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d bind %s failure. try times: %d", __FUNCTION__, __LINE__, link_name, _try_times);
                mmMSleep(MM_NETUDP_TRY_MSLEEP_TIME);
            }
        } while (0 != rt && _try_times < p->trytimes);
        // set socket to not block.
        mmSocket_SetBlock(socket, MM_NONBLOCK);
        if (0 != rt)
        {
            mmErr_t errcode = mmErrno;
            mmLogger_LogE(gLogger, "%s %d bind %s failure. try times: %d", __FUNCTION__, __LINE__, link_name, _try_times);
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            mmNetUdp_ApplyBroken(p);
            mmSocket_CloseSocket(socket);
        }
        else
        {
            // we check the port.
            if (0 == mmSockaddr_Port(&socket->ss_native))
            {
                // if the port for ss_native is zero,we need obtain it manual.
                // synchronize the real bind socket sockaddr.
                mmSocket_CurrentBindSockaddr(socket, &socket->ss_native);
            }
            // apply finish.
            mmNetUdp_ApplyFinish(p);
            mmLogger_LogI(gLogger, "%s %d %s success. try times: %d", __FUNCTION__, __LINE__, link_name, _try_times);
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d bind target fd is invalid.", __FUNCTION__, __LINE__);
    }
}

// close socket.
MM_EXPORT_NET void mmNetUdp_CloseSocket(struct mmNetUdp* p)
{
    mmUdp_CloseSocket(&p->udp);
}
// shutdown socket.
MM_EXPORT_NET void mmNetUdp_ShutdownSocket(struct mmNetUdp* p, int opcode)
{
    mmSocket_ShutdownSocket(&p->udp.socket, opcode);
}
// set socket block.
MM_EXPORT_NET void mmNetUdp_SetBlock(struct mmNetUdp* p, int block)
{
    mmSocket_SetBlock(&p->udp.socket, block);
}

MM_EXPORT_NET int mmNetUdp_MulcastJoin(struct mmNetUdp* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmUdp_MulcastJoin(&p->udp, mr_multiaddr, mr_interface);
}
MM_EXPORT_NET int mmNetUdp_MulcastDrop(struct mmNetUdp* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmUdp_MulcastDrop(&p->udp, mr_multiaddr, mr_interface);
}

// check state.not thread safe.
MM_EXPORT_NET void mmNetUdp_Check(struct mmNetUdp* p)
{
    if (0 != mmNetUdp_FinallyState(p))
    {
        // we first disconnect this old connect.
        mmNetUdp_DetachSocket(p);
        // fopen a socket.
        mmNetUdp_FopenSocket(p);
        // net tcp connect.
        mmNetUdp_Bind(p);
    }
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmNetUdp_FinallyState(struct mmNetUdp* p)
{
    return (MM_INVALID_SOCKET != p->udp.socket.socket && MM_NETUDP_STATE_FINISH == p->udp_state) ? 0 : -1;
}

MM_EXPORT_NET void mmNetUdp_ApplyBroken(struct mmNetUdp* p)
{
    p->udp_state = MM_NETUDP_STATE_CLOSED;
    mmUdp_ResetStreambuf(&p->udp);
    (*p->callback.Broken)(&p->udp);
    mmSelector_FdRmv(&p->selector, p->udp.socket.socket, &p->udp);
}
MM_EXPORT_NET void mmNetUdp_ApplyFinish(struct mmNetUdp* p)
{
    p->udp_state = MM_NETUDP_STATE_FINISH;
    mmUdp_ResetStreambuf(&p->udp);
    mmSelector_FdAdd(&p->selector, p->udp.socket.socket, &p->udp);
    (*p->callback.Finish)(&p->udp);
}
// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmNetUdp_AttachSocket(struct mmNetUdp* p)
{
    mmNetUdp_Bind(p);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmNetUdp_DetachSocket(struct mmNetUdp* p)
{
    struct mmUdp* udp = &p->udp;
    if (MM_INVALID_SOCKET != udp->socket.socket)
    {
        mmNetUdp_ApplyBroken(p);
        // set block state.
        mmNetUdp_SetBlock(p, MM_BLOCKING);
        // shutdown socket first.
        mmNetUdp_ShutdownSocket(p, MM_BOTH_SHUTDOWN);
        // close socket.
        mmNetUdp_CloseSocket(p);
    }
}

// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmNetUdp_BufferSend(struct mmNetUdp* p, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote)
{
    int rt = -1;
    if (MM_NETUDP_STATE_FINISH == p->udp_state && MM_INVALID_SOCKET != p->udp.socket.socket)
    {
        rt = mmUdp_BufferSend(&p->udp, buffer, offset, length, remote);
        // update the udp state,because the mmUdp_BufferSend can not fire broken event immediately.
        p->udp_state = -1 == rt ? MM_NETUDP_STATE_BROKEN : p->udp_state;
    }
    else
    {
        // net udp nready.fire event.
        (*p->callback.Nready)(&p->udp);
    }
    return rt;
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmNetUdp_FlushSend(struct mmNetUdp* p, struct mmSockaddr* remote)
{
    int rt = -1;
    if (MM_NETUDP_STATE_FINISH == p->udp_state && MM_INVALID_SOCKET != p->udp.socket.socket)
    {
        size_t sz = 0;
        do
        {
            if (MM_TS_FINISH == p->state ||
                MM_NETUDP_STATE_FINISH != p->udp_state ||
                MM_INVALID_SOCKET == p->udp.socket.socket)
            {
                // the net tcp state can not send any thing.
                break;
            }
            rt = mmUdp_FlushSend(&p->udp, remote);
            // update the udp state,because the mmUdp_BufferSend can not fire broken event immediately.
            p->udp_state = -1 == rt ? MM_NETUDP_STATE_BROKEN : p->udp_state;
            if (0 != rt)
            {
                // -1 == rt socket send failure.
                //  0 == rt socket send socket is MM_EAGAIN or buffer is full.
                //  0  < rt socket send success.
                break;
            }
            sz = mmStreambuf_Size(&p->udp.buff_send);
            if (0 == sz)
            {
                // nothing buffer need send.
                break;
            }
            // we need cycle send.after wait for a while.
            mmMSleep(MM_NETUDP_BLOCK_TIME);
        } while (1);
    }
    else
    {
        // net tcp nready.fire event.
        (*p->callback.Nready)(&p->udp);
    }
    return rt;
}

MM_EXPORT_NET void mmNetUdp_Start(struct mmNetUdp* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmSelector_Start(&p->selector);
    (*(p->event_udp_allocator.EventUdpProduce))(p, &p->udp);
}
MM_EXPORT_NET void mmNetUdp_Interrupt(struct mmNetUdp* p)
{
    p->state = MM_TS_CLOSED;
    
    mmNetUdp_ShutdownSocket(p, MM_BOTH_SHUTDOWN);
    mmNetUdp_CloseSocket(p);
    
    mmSelector_Interrupt(&p->selector);
}
MM_EXPORT_NET void mmNetUdp_Shutdown(struct mmNetUdp* p)
{
    p->state = MM_TS_FINISH;
    
    mmNetUdp_ShutdownSocket(p, MM_BOTH_SHUTDOWN);
    mmNetUdp_CloseSocket(p);
    
    mmSelector_Shutdown(&p->selector);
}
MM_EXPORT_NET void mmNetUdp_Join(struct mmNetUdp* p)
{
    mmSelector_Join(&p->selector);
    (*(p->event_udp_allocator.EventUdpRecycle))(p, &p->udp);
}

MM_EXPORT_NET void mmNetUdp_UdpHandlePacketCallback(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    mmNetUdpHandleFunc handle = NULL;
    mmSpinlock_Lock(&net_udp->rbtree_locker);
    handle = (mmNetUdpHandleFunc)mmRbtreeU32Vpt_Get(&net_udp->rbtree, pack->phead.mid);
    mmSpinlock_Unlock(&net_udp->rbtree_locker);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, net_udp->u, pack, remote);
    }
    else
    {
        assert(NULL != net_udp->callback.Handle && "net_udp->Handle is a null.");
        (*(net_udp->callback.Handle))(udp, net_udp->u, pack, remote);
    }
}
MM_EXPORT_NET int mmNetUdp_UdpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    mmNetUdp_CryptoDecrypt(net_udp, udp, buffer, offset, length);
    return mmStreambufPacket_HandleUdp(&udp->buff_recv, &mmNetUdp_UdpHandlePacketCallback, udp, remote);
}
MM_EXPORT_NET int mmNetUdp_UdpBrokenCallback(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    assert(NULL != net_udp->callback.Broken && "net_udp->Broken is a null.");
    mmNetUdp_ApplyBroken(net_udp);
    return 0;
}
MM_EXPORT_NET void mmNetUdp_MidEventHandle(void* obj, mmUInt32_t mid, mmNetUdpHandleFunc callback)
{
    struct mmSockaddr remote;
    struct mmPacket pack;
    struct mmPacketHead phead;
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);

    mmSockaddr_Init(&remote);
    mmPacket_Init(&pack);
    mmPacket_HeadBaseZero(&pack);
    pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    pack.bbuff.length = 0;
    mmPacket_ShadowAlloc(&pack);
    pack.phead.mid = mid;
    mmPacket_HeadEncode(&pack, &pack.hbuff, &phead);
    (*(callback))(obj, net_udp->u, &pack, &remote);
    mmPacket_ShadowFree(&pack);
    mmPacket_Destroy(&pack);
    mmSockaddr_Destroy(&remote);
}

static void __static_mmNetUdp_SelectorHandleRecv(void* obj, void* u)
{
    struct mmUdp* udp = (struct mmUdp*)u;
    mmUdp_HandleRecv(udp);
}
static void __static_mmNetUdp_SelectorHandleSend(void* obj, void* u)
{
    struct mmUdp* udp = (struct mmUdp*)u;
    mmUdp_HandleSend(udp);
}

