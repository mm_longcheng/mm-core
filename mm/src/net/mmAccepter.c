/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAccepter.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmTime.h"

static void __static_mmAccepter_AcceptDefault(void* obj, mmSocket_t fd, struct mmSockaddr* remote)
{

}

MM_EXPORT_NET void mmAccepterCallback_Init(struct mmAccepterCallback* p)
{
    p->Accept = &__static_mmAccepter_AcceptDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmAccepterCallback_Destroy(struct mmAccepterCallback* p)
{
    p->Accept = &__static_mmAccepter_AcceptDefault;
    p->obj = NULL;
}

// accept task.
static void* __static_mmAccepter_AcceptDefaultThread(void* _arg);
//
MM_EXPORT_NET void mmAccepter_Init(struct mmAccepter* p)
{
    mmSocket_Init(&p->socket);
    mmAccepterCallback_Init(&p->callback);
    p->backlog = MM_ACCEPTER_BACKLOG;
    p->keepalive = MM_KEEPALIVE_ACTIVATE;
    p->accepter_state = MM_ACCEPTER_STATE_CLOSED;
    p->nonblock_timeout = MM_ACCEPTER_NONBLOCK_TIMEOUT;
    p->trytimes = MM_NET_ACCEPTER_TRYTIME;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmAccepter_Destroy(struct mmAccepter* p)
{
    mmSocket_Destroy(&p->socket);
    mmAccepterCallback_Destroy(&p->callback);
    p->backlog = 0;
    p->keepalive = MM_KEEPALIVE_ACTIVATE;
    p->accepter_state = MM_ACCEPTER_STATE_CLOSED;
    p->nonblock_timeout = 0;
    p->trytimes = 0;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmAccepter_Lock(struct mmAccepter* p)
{
    mmSocket_Lock(&p->socket);
}
MM_EXPORT_NET void mmAccepter_Unlock(struct mmAccepter* p)
{
    mmSocket_Unlock(&p->socket);
}
// assign native address but not connect.
MM_EXPORT_NET void mmAccepter_SetNative(struct mmAccepter* p, const char* node, mmUShort_t port)
{
    mmSocket_SetNative(&p->socket, node, port);
}
// assign remote address but not connect.
MM_EXPORT_NET void mmAccepter_SetRemote(struct mmAccepter* p, const char* node, mmUShort_t port)
{
    mmSocket_SetRemote(&p->socket, node, port);
}

MM_EXPORT_NET void mmAccepter_SetCallback(struct mmAccepter* p, struct mmAccepterCallback* cb)
{
    assert(NULL != cb && "cb is a null.");
    p->callback = (*cb);
}

// reset streambuf and fire event broken.
MM_EXPORT_NET void mmAccepter_ApplyBroken(struct mmAccepter* p)
{
    p->accepter_state = MM_ACCEPTER_STATE_CLOSED;
}
// reset streambuf and fire event broken.
MM_EXPORT_NET void mmAccepter_ApplyFinish(struct mmAccepter* p)
{
    p->accepter_state = MM_ACCEPTER_STATE_FINISH;
}
// fopen.
MM_EXPORT_NET void mmAccepter_FopenSocket(struct mmAccepter* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmSocket* socket = &p->socket;
    //
    mmSocket_FopenSocket(&p->socket, p->socket.ss_native.addr.sa_family, SOCK_STREAM, 0);
    //
    mmSocket_ToString(&p->socket, link_name);
    //
    if (MM_INVALID_SOCKET != p->socket.socket)
    {
        mmLogger_LogI(gLogger, "%s %d fopen_socket %s success.", __FUNCTION__, __LINE__, link_name);
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d fopen_socket %s failure.", __FUNCTION__, __LINE__, link_name);
    }
    
    // SO_REUSEADDR
    mmSocket_ReUseAddr(socket);
}

// bind address.
MM_EXPORT_NET void mmAccepter_Bind(struct mmAccepter* p)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmSocket* socket = &p->socket;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmSocket_ToString(socket, link_name);
    //
    if (MM_INVALID_SOCKET != socket->socket)
    {
        mmUInt32_t _try_times = 0;
        int rt = -1;
        p->accepter_state = MM_ACCEPTER_STATE_MOTION;
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        do
        {
            _try_times++;
            rt = mmSocket_Bind(socket);
            if (0 == rt)
            {
                // bind immediately.
                break;
            }
            if (MM_TS_FINISH == p->state)
            {
                break;
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d bind %s failure. try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
                mmMSleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
            }
        } while (0 != rt && _try_times < p->trytimes);
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        if (0 != rt)
        {
            mmErr_t errcode = mmErrno;
            mmLogger_LogE(gLogger, "%s %d bind %s failure. try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            mmAccepter_ApplyBroken(p);
            mmSocket_CloseSocket(socket);
        }
        else
        {
            // cache current bind socketaddr into ss_native.
            mmAccepter_CurrentBindSockaddr(p);
            // apply finish.
            mmAccepter_ApplyFinish(p);
            mmLogger_LogI(gLogger, "%s %d %s success. try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d bind target fd is invalid.", __FUNCTION__, __LINE__);
    }
}
// listen address.
MM_EXPORT_NET void mmAccepter_Listen(struct mmAccepter* p)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmSocket* socket = &p->socket;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmSocket_ToString(socket, link_name);
    //
    if (MM_INVALID_SOCKET != socket->socket)
    {
        mmUInt32_t _try_times = 0;
        int rt = -1;
        p->accepter_state = MM_ACCEPTER_STATE_MOTION;
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        do
        {
            _try_times++;
            rt = mmSocket_Listen(socket, p->backlog);
            if (0 == rt)
            {
                // listen immediately.
                break;
            }
            if (MM_TS_FINISH == p->state)
            {
                break;
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d listen %s failure. try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
                mmMSleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
            }
        } while (0 != rt && _try_times < p->trytimes);
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        if (0 != rt)
        {
            mmErr_t errcode = mmErrno;
            mmLogger_LogE(gLogger, "%s %d listen %s failure. try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            mmAccepter_ApplyBroken(p);
            mmSocket_CloseSocket(socket);
        }
        else
        {
            mmAccepter_ApplyFinish(p);
            mmLogger_LogI(gLogger, "%s %d listen %s success. try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d listen target fd is invalid.", __FUNCTION__, __LINE__);
    }
}
// close socket. mmAccepter_Join before call this.
MM_EXPORT_NET void mmAccepter_CloseSocket(struct mmAccepter* p)
{
    mmSocket_CloseSocket(&p->socket);
}
// accept sync.
MM_EXPORT_NET void mmAccepter_Accept(struct mmAccepter* p)
{
    socklen_t remote_size;
    struct mmSockaddr remote;
    mmSocket_t afd;
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    // we not need assert the fd is must valid.
    // assert( MM_INVALID_SOCKET != p->socket.fd&&"you must alloc socket fd first.");
    p->state = (MM_INVALID_SOCKET == p->socket.socket || MM_TS_FINISH == p->state) ? MM_TS_CLOSED : MM_TS_MOTION;
    //
    remote_size = sizeof(struct mmSockaddr);
    while (MM_TS_MOTION == p->state)
    {
        mmMemset(&remote, 0, sizeof(struct mmSockaddr));
        afd = accept(p->socket.socket, (struct sockaddr*)(&remote), &remote_size);
        if (MM_INVALID_SOCKET == afd)
        {
            mmErr_t errcode = mmErrno;
            if (MM_EINTR == errcode)
            {
                // MM_EINTR errno:(10004) A blocking operation was interrupted.
                // this error is not serious.
                continue;
            }
            else
            {
                mmSocket_ToString(&p->socket, link_name);
                // at here when we want interrupt accept, will case a socket errno.
                // [errno:(53) Software caused connection abort] it is expected result.
                mmLogger_LogI(gLogger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                p->state = MM_TS_CLOSED;
                p->accepter_state = MM_ACCEPTER_STATE_BROKEN;
            }
        }
        else
        {
            if (MM_KEEPALIVE_ACTIVATE == p->keepalive)
            {
                mmSocketKeepalive(afd, p->keepalive);
            }
            //
            (*p->callback.Accept)(p, afd, &remote);
            mmSockaddr_NativeRemoteString(&p->socket.ss_native, &remote, afd, link_name);
            mmLogger_LogI(gLogger, "%s %d %s connect.", __FUNCTION__, __LINE__, link_name);
        }
    }
}
MM_EXPORT_NET void mmAccepter_CurrentBindSockaddr(struct mmAccepter* p)
{
    struct mmSocket* socket = &p->socket;
    // we check the port.
    if (0 == mmSockaddr_Port(&socket->ss_native))
    {
        // if the port for ss_native is zero,we need obtain it manual.
        // synchronize the real bind socket sockaddr.
        mmSocket_CurrentBindSockaddr(&p->socket, &p->socket.ss_native);
    }
}
// start accept thread.
MM_EXPORT_NET void mmAccepter_Start(struct mmAccepter* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->accept_thread, NULL, &__static_mmAccepter_AcceptDefaultThread, p);
}
// interrupt accept thread.
MM_EXPORT_NET void mmAccepter_Interrupt(struct mmAccepter* p)
{
    p->state = MM_TS_CLOSED;
    mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    mmSocket_CloseSocket(&p->socket);
}
// shutdown accept thread.
MM_EXPORT_NET void mmAccepter_Shutdown(struct mmAccepter* p)
{
    p->state = MM_TS_FINISH;
    mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    mmSocket_CloseSocket(&p->socket);
}
// join accept thread.
MM_EXPORT_NET void mmAccepter_Join(struct mmAccepter* p)
{
    pthread_join(p->accept_thread, NULL);
}

static void* __static_mmAccepter_AcceptDefaultThread(void* arg)
{
    struct mmAccepter* p = (struct mmAccepter*)(arg);
    mmAccepter_Accept(p);
    return NULL;
}
