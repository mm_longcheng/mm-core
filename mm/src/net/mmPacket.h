/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPacket_h__
#define __mmPacket_h__

#include "core/mmCore.h"
#include "core/mmByte.h"

#include "net/mmNetExport.h"
#include "net/mmSockaddr.h"

#include "core/mmPrefix.h"

// Ethernet len[46~1500] MTU 20 
// udp head  8 1500 - 20 -  8 = 1472
// tcp head 20 1500 - 20 - 20 = 1460
// we define packet page size to 1024.
#define MM_PACKET_PAGE_SIZE 1024

// we not use typedef a new name for this type.
// because uint16 uint32 is common type when cross language.
//
// msg_size    type is mmUInt16_t
// msg_id      type is mmUInt32_t
// proxy_id    type is mmUInt32_t
// session_id  type is mmUInt64_t
// unique_id   type is mmUInt64_t
// msg_head    type is mmUInt32_t = 2 * msg_size
//
// msg pack struct.
// |-mmUInt16_t--|-mmUInt16_t--|---char*---|---char*---|
//
// use this quick decode encode.
// |--------mmUInt32_t---------|---char*---|---char*---|
// |-------------|-------------------------|-----------|
// |--msg_size---|--head_size--|-head_data-|-body_data-|
// |-------head_size_data------|-head_data-|-body_data-|
//
// package_size = 2 * sizeof(mm_msg_size_t) + head_size + body_size
//
// package buffer is little endian.
// https://en.wikipedia.org/wiki/Comparison_of_instruction_set_architectures
// system devices endian.
//     ARM android devices is little endian.
//     ARM iOS     devices is little endian.
//     x86 desktop devices is little endian.
// cpu    devices endian.
//     x86 little.
//     ARM little/bigger.

#define MM_MSG_SIZE_BIT_NUM 16
#define MM_MSG_HEAD_H_MASK 0xFFFF0000 // hight mask
#define MM_MSG_HEAD_L_MASK 0x0000FFFF // low   mask

// the hsize must use sizeof(T),make sure the package byte aligned.
//
// msg base head size.
// mmUInt32_t   mid;// msg     id.
// byte aligned this struct sizeof is 4.
#define MM_MSG_BASE_HEAD_SIZE 4
// mmUInt32_t   mid;// msg     id.
// mmUInt32_t   pid;// proxy   id.
// mmUInt64_t   sid;// section id.
// mmUInt64_t   uid;// unique  id.
// byte aligned this struct sizeof is 24.
#define MM_MSG_COMM_HEAD_SIZE 24

struct mmPacketHead
{
    // msg base head. include into head data.

    // msg     id.
    mmUInt32_t   mid;

    // proxy   id.
    mmUInt32_t   pid;

    // section id.
    mmUInt64_t   sid;

    // unique  id.
    mmUInt64_t   uid;
};
MM_EXPORT_NET void mmPacketHead_Init(struct mmPacketHead* p);
MM_EXPORT_NET void mmPacketHead_Destroy(struct mmPacketHead* p);
MM_EXPORT_NET void mmPacketHead_Reset(struct mmPacketHead* p);

// packet struct.
// use net byte order.
struct mmPacket
{
    // packet head.
    struct mmPacketHead phead;
    // buffer for head.
    struct mmByteBuffer hbuff;
    // buffer for body.
    struct mmByteBuffer bbuff;
};

MM_EXPORT_NET void mmPacket_Init(struct mmPacket* p);
MM_EXPORT_NET void mmPacket_Destroy(struct mmPacket* p);

// reset all to zero.
MM_EXPORT_NET void mmPacket_Reset(struct mmPacket* p);
// use for quick zero head base.(mid,uid,sid,pid)
MM_EXPORT_NET void mmPacket_HeadBaseZero(struct mmPacket* p);
// use for quick copy head base r-->p.(mid,uid,sid,pid)
MM_EXPORT_NET void mmPacket_HeadBaseCopy(struct mmPacket* p, struct mmPacket* r);
// decode p phead to hbuff.phead is cache.
MM_EXPORT_NET void mmPacket_HeadDecode(struct mmPacket* p, struct mmByteBuffer* hbuff, struct mmPacketHead* phead);
// decode hbuff phead to p.phead is cache.
MM_EXPORT_NET void mmPacket_HeadEncode(struct mmPacket* p, struct mmByteBuffer* hbuff, struct mmPacketHead* phead);

// copy data from p to t.will realloc the hbuff and bbuff.
MM_EXPORT_NET void mmPacket_CopyRealloc(struct mmPacket* p, struct mmPacket* t);
// copy data from p to t.will alloc the hbuff and bbuff.
MM_EXPORT_NET void mmPacket_CopyAlloc(struct mmPacket* p, struct mmPacket* t);
// copy data from p to t.not realloc the hbuff and bbuff.
MM_EXPORT_NET void mmPacket_CopyWeak(struct mmPacket* p, struct mmPacket* t);
// free copy realloc data from.
MM_EXPORT_NET void mmPacket_FreeCopyRealloc(struct mmPacket* p);
// free copy alloc data from.
MM_EXPORT_NET void mmPacket_FreeCopyAlloc(struct mmPacket* p);

MM_EXPORT_NET void mmPacket_ShadowAlloc(struct mmPacket* p);
MM_EXPORT_NET void mmPacket_ShadowFree(struct mmPacket* p);

// packet handle function type.
typedef void(*mmPacketHandleTcpFunc)(void* obj, struct mmPacket* pack);
typedef void(*mmPacketSignalTcpFunc)(void* obj);
// packet handle function type.
typedef void(*mmPacketHandleUdpFunc)(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
typedef void(*mmPacketSignalUdpFunc)(void* obj);

#include "core/mmSuffix.h"

#endif//__mmPacket_h__
