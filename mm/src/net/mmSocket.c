/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSocket.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"
#include "core/mmLogger.h"

MM_EXPORT_NET void mmSocket_Init(struct mmSocket* p)
{
    mmSockaddr_Init(&p->ss_native);
    mmSockaddr_Init(&p->ss_remote);
    p->socket = MM_INVALID_SOCKET;
    mmSpinlock_Init(&p->locker, NULL);
}

MM_EXPORT_NET void mmSocket_Destroy(struct mmSocket* p)
{
    mmSocket_CloseSocket(p);
    //
    mmSockaddr_Destroy(&p->ss_native);
    mmSockaddr_Destroy(&p->ss_remote);
    p->socket = MM_INVALID_SOCKET;
    mmSpinlock_Destroy(&p->locker);
}
MM_EXPORT_NET void mmSocket_Lock(struct mmSocket* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmSocket_Unlock(struct mmSocket* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_NET void mmSocket_Reset(struct mmSocket* p)
{
    mmSocket_CloseSocket(p);
    //
    mmSockaddr_Reset(&p->ss_native);
    mmSockaddr_Reset(&p->ss_remote);
    p->socket = MM_INVALID_SOCKET;
    // spinlock not need reset.
}
// fopen socket fd.
MM_EXPORT_NET void mmSocket_FopenSocket(struct mmSocket* p, int domain, int type, int protocol)
{
    mmSocket_CloseSocket(p);
    p->socket = mmFopenSocket(domain, type, protocol);
}
// close socket fd.
MM_EXPORT_NET void mmSocket_CloseSocket(struct mmSocket* p)
{
    if (MM_INVALID_SOCKET != p->socket)
    {
        mmCloseSocket(p->socket);
        p->socket = MM_INVALID_SOCKET;
    }
}
// close socket fd.not set socket_t to invalid -1.
// use to fire socket fd close event.
MM_EXPORT_NET void mmSocket_CloseSocketEvent(struct mmSocket* p)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    mmCloseSocket(p->socket);
}
// shutdown socket.
MM_EXPORT_NET void mmSocket_ShutdownSocket(struct mmSocket* p, int opcode)
{
    if (MM_INVALID_SOCKET != p->socket)
    {
        mmShutdownSocket(p->socket, opcode);
    }
}
// assign addr native by ip port.
MM_EXPORT_NET void mmSocket_SetNative(struct mmSocket* p, const char* node, mmUShort_t port)
{
    mmSockaddr_Assign(&p->ss_native, node, port);
    p->ss_remote.addr.sa_family = p->ss_native.addr.sa_family;
}
// assign addr remote by ip port.
MM_EXPORT_NET void mmSocket_SetRemote(struct mmSocket* p, const char* node, mmUShort_t port)
{
    mmSockaddr_Assign(&p->ss_remote, node, port);
    p->ss_native.addr.sa_family = p->ss_remote.addr.sa_family;
}
// assign addr native by sockaddr_storage. AF_INET; after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmSocket_SetNativeStorage(struct mmSocket* p, struct mmSockaddr* ss_native)
{
    mmSockaddr_CopyFrom(&p->ss_native, ss_native);
    p->ss_remote.addr.sa_family = p->ss_native.addr.sa_family;
}
// assign addr remote by sockaddr_storage. AF_INET; after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmSocket_SetRemoteStorage(struct mmSocket* p, struct mmSockaddr* ss_remote)
{
    mmSockaddr_CopyFrom(&p->ss_remote, ss_remote);
    p->ss_native.addr.sa_family = p->ss_remote.addr.sa_family;
}
// default is blocking. 1 means block 0 no block.
MM_EXPORT_NET int mmSocket_SetBlock(struct mmSocket* p, int block)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    return MM_NONBLOCK == block ? mmNonblocking(p->socket) : mmBlocking(p->socket);
}

MM_EXPORT_NET int mmSocket_Connect(struct mmSocket* p)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    return (int)connect(p->socket, (struct sockaddr*)&p->ss_remote, mmSockaddr_Length(&p->ss_remote));
}
// bind address.
MM_EXPORT_NET int mmSocket_Bind(struct mmSocket* p)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    return (int)bind(p->socket, (struct sockaddr*)&p->ss_native, mmSockaddr_Length(&p->ss_native));
}
// listen address.
MM_EXPORT_NET int mmSocket_Listen(struct mmSocket* p, int backlog)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    return (int)listen(p->socket, backlog);
}
MM_EXPORT_NET int mmSocket_CurrentBindSockaddr(struct mmSocket* p, struct mmSockaddr* ss_native)
{
    socklen_t ss_native_length = mmSockaddr_Length(ss_native);
    return (int)getsockname(p->socket, (struct sockaddr *)ss_native, &ss_native_length);
}

MM_EXPORT_NET void mmSocket_ReUseAddr(struct mmSocket* p)
{
    if (0 != mmSockaddr_Port(&p->ss_native))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        int hReuseaddrFlag = 1;
        char hLinkName[MM_LINK_NAME_LENGTH] = { 0 };
        
        mmSocket_ToString(p, hLinkName);
        
        //
        //SO_REUSEADDR
        //  Indicates that the rules used in validating addresses supplied
        //  in a bind(2) call should allow reuse of local addresses.  For
        //  AF_INET sockets this means that a socket may bind, except when
        //  there is an active listening socket bound to the address.
        //  When the listening socket is bound to INADDR_ANY with a
        //  specific port then it is not possible to bind to this port for
        //  any local address.  Argument is an integer boolean flag.
        if (-1 == setsockopt(p->socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&hReuseaddrFlag, sizeof(hReuseaddrFlag)))
        {
            // the option for SO_REUSEADDR socket flag is optional.
            // this error is not serious.
            mmErr_t errcode = mmErrno;
            mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
            mmLogger_LogI(gLogger, "%s %d reuseaddr %s failure", __FUNCTION__, __LINE__, hLinkName);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d reuseaddr %s success", __FUNCTION__, __LINE__, hLinkName);
        }
    }
}

MM_EXPORT_NET int mmSocket_Recv(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    return (int)recv(p->socket, (char*)(b + o), (int)l, f);
}
MM_EXPORT_NET int mmSocket_Send(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    return (int)send(p->socket, (char*)(b + o), (int)l, f);
}

MM_EXPORT_NET int mmSocket_SendDgram(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f, struct sockaddr* a, socklen_t s)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    // int sendto(int s, void * msg, int len, unsigned int flags, const struct sockaddr * to, socklen_t tolen);
    return (int)sendto(p->socket, (char*)(b + o), (int)l, f, a, s);
}

MM_EXPORT_NET int mmSocket_RecvDgram(struct mmSocket* p, mmUInt8_t* b, size_t o, size_t l, int f, struct sockaddr* a, socklen_t* s)
{
    // we not need assert the fd is must valid.
    // assert( -1 != p->fd && "you must allocator socket fd first." );
    // int recvfrom(int s, void *buf, int len, unsigned int flags, struct sockaddr *from,socklen_t *fromlen);
    return (int)recvfrom(p->socket, (char*)(b + o), (int)l, f, a, s);
}

MM_EXPORT_NET void mmSocket_ToString(struct mmSocket* p, char link_name[MM_LINK_NAME_LENGTH])
{
    mmSockaddr_NativeRemoteString(&p->ss_native, &p->ss_remote, p->socket, link_name);
}
