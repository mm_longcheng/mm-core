/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmClientUdp.h"
#include "net/mmStreambufPacket.h"
#include "net/mmMessageCoder.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmClientUdp_NetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmClientUdp_NetUdpBroken(void* obj);
static void __static_mmClientUdp_NetUdpNready(void* obj);
static void __static_mmClientUdp_NetUdpFinish(void* obj);

static int __static_mmClientUdp_FlushSignalTaskFactor(void* obj);
static int __static_mmClientUdp_FlushSignalTaskHandle(void* obj);
static int __static_mmClientUdp_StateSignalTaskFactor(void* obj);
static int __static_mmClientUdp_StateSignalTaskHandle(void* obj);

static void __static_mmClientUdp_PacketQueueSendHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmClientUdp_PacketQueueRecvHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmClientUdp_EmptyHandleForCommonEvent(struct mmClientUdp* p);

static void __static_mmClientUdp_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{

}
MM_EXPORT_NET void mmClientUdpCallback_Init(struct mmClientUdpCallback* p)
{
    p->Handle = &__static_mmClientUdp_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmClientUdpCallback_Destroy(struct mmClientUdpCallback* p)
{
    p->Handle = &__static_mmClientUdp_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmClientUdp_Init(struct mmClientUdp* p)
{
    struct mmUdpCallback udp_callback;
    struct mmNetUdpCallback net_udp_callback;
    struct mmSignalTaskCallback flush_task_callback;
    struct mmSignalTaskCallback state_task_callback;
    struct mmPacketQueueCallback queue_send_udp_callback;
    struct mmPacketQueueCallback queue_recv_udp_callback;

    mmNetUdp_Init(&p->net_udp);
    mmRbtreeU32Vpt_Init(&p->rbtree_n);
    mmRbtreeU32Vpt_Init(&p->rbtree_q);
    mmPacketQueue_Init(&p->queue_recv);
    mmPacketQueue_Init(&p->queue_send);
    mmClientUdpCallback_Init(&p->callback_n);
    mmClientUdpCallback_Init(&p->callback_q);
    mmSignalTask_Init(&p->flush_task);
    mmSignalTask_Init(&p->state_task);
    mmSpinlock_Init(&p->locker_rbtree_n, NULL);
    mmSpinlock_Init(&p->locker_rbtree_q, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->ss_native_update = 0;
    p->ss_remote_update = 0;
    p->state_check_flag = MM_CLIENT_UDP_CHECK_INACTIVE;
    p->state_queue_flag = MM_CLIENT_UDP_QUEUE_ACTIVATE;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    udp_callback.Handle = &mmClientUdp_UdpHandleCallback;
    udp_callback.Broken = &mmClientUdp_UdpBrokenCallback;
    udp_callback.obj = &p->net_udp;
    mmNetUdp_SetUdpCallback(&p->net_udp, &udp_callback);

    net_udp_callback.Handle = &__static_mmClientUdp_NetUdpHandle;
    net_udp_callback.Broken = &__static_mmClientUdp_NetUdpBroken;
    net_udp_callback.Nready = &__static_mmClientUdp_NetUdpNready;
    net_udp_callback.Finish = &__static_mmClientUdp_NetUdpFinish;
    net_udp_callback.obj = p;
    mmNetUdp_SetNetUdpCallback(&p->net_udp, &net_udp_callback);

    flush_task_callback.Factor = &__static_mmClientUdp_FlushSignalTaskFactor;
    flush_task_callback.Handle = &__static_mmClientUdp_FlushSignalTaskHandle;
    flush_task_callback.obj = p;
    mmSignalTask_SetCallback(&p->flush_task, &flush_task_callback);

    state_task_callback.Factor = &__static_mmClientUdp_StateSignalTaskFactor;
    state_task_callback.Handle = &__static_mmClientUdp_StateSignalTaskHandle;
    state_task_callback.obj = p;
    mmSignalTask_SetCallback(&p->state_task, &state_task_callback);

    queue_send_udp_callback.Handle = &__static_mmClientUdp_PacketQueueSendHandle;
    queue_send_udp_callback.obj = p;
    mmPacketQueue_SetQueueCallback(&p->queue_send, &queue_send_udp_callback);

    queue_recv_udp_callback.Handle = &__static_mmClientUdp_PacketQueueRecvHandle;
    queue_recv_udp_callback.obj = p;
    mmPacketQueue_SetQueueCallback(&p->queue_recv, &queue_recv_udp_callback);

    mmSignalTask_SetSuccessNearby(&p->flush_task, MM_CLIENT_UDP_SEND_INTERVAL_HANDLE_TIME);
    mmSignalTask_SetFailureNearby(&p->flush_task, MM_CLIENT_UDP_SEND_INTERVAL_BROKEN_TIME);
    mmSignalTask_SetSuccessNearby(&p->state_task, MM_CLIENT_UDP_HANDLE_MSEC_CHECK);
    mmSignalTask_SetFailureNearby(&p->state_task, MM_CLIENT_UDP_BROKEN_MSEC_CHECK);
    // empty handle for common event.
    __static_mmClientUdp_EmptyHandleForCommonEvent(p);
}
MM_EXPORT_NET void mmClientUdp_Destroy(struct mmClientUdp* p)
{
    mmClientUdp_ClearHandleRbtree(p);
    // empty handle for common event.
    __static_mmClientUdp_EmptyHandleForCommonEvent(p);
    //
    mmNetUdp_Destroy(&p->net_udp);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_n);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_q);
    mmPacketQueue_Destroy(&p->queue_recv);
    mmPacketQueue_Destroy(&p->queue_send);
    mmClientUdpCallback_Destroy(&p->callback_n);
    mmClientUdpCallback_Destroy(&p->callback_q);
    mmSignalTask_Destroy(&p->flush_task);
    mmSignalTask_Destroy(&p->state_task);
    mmSpinlock_Destroy(&p->locker_rbtree_n);
    mmSpinlock_Destroy(&p->locker_rbtree_q);
    mmSpinlock_Destroy(&p->locker);
    p->ss_native_update = 0;
    p->ss_remote_update = 0;
    p->state_check_flag = MM_CLIENT_UDP_CHECK_ACTIVATE;
    p->state_queue_flag = MM_CLIENT_UDP_QUEUE_ACTIVATE;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

MM_EXPORT_NET void mmClientUdp_Lock(struct mmClientUdp* p)
{
    mmNetUdp_Lock(&p->net_udp);
    mmPacketQueue_Lock(&p->queue_recv);
    mmPacketQueue_Lock(&p->queue_send);
    mmSignalTask_Lock(&p->flush_task);
    mmSignalTask_Lock(&p->state_task);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmClientUdp_Unlock(struct mmClientUdp* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmSignalTask_Unlock(&p->state_task);
    mmSignalTask_Unlock(&p->flush_task);
    mmPacketQueue_Unlock(&p->queue_send);
    mmPacketQueue_Unlock(&p->queue_recv);
    mmNetUdp_Unlock(&p->net_udp);
}
// i locker
MM_EXPORT_NET void mmClientUdp_ILock(struct mmClientUdp* p)
{
    mmNetUdp_ILock(&p->net_udp);
}
MM_EXPORT_NET void mmClientUdp_IUnlock(struct mmClientUdp* p)
{
    mmNetUdp_IUnlock(&p->net_udp);
}
// o locker
MM_EXPORT_NET void mmClientUdp_OLock(struct mmClientUdp* p)
{
    mmNetUdp_OLock(&p->net_udp);
}
MM_EXPORT_NET void mmClientUdp_OUnlock(struct mmClientUdp* p)
{
    mmNetUdp_OUnlock(&p->net_udp);
}

// assign native address but not connect.
MM_EXPORT_NET void mmClientUdp_SetNative(struct mmClientUdp* p, const char* node, mmUShort_t port)
{
    mmNetUdp_SetNative(&p->net_udp, node, port);
    p->ss_native_update = 1;
}
// assign remote address but not connect.
MM_EXPORT_NET void mmClientUdp_SetRemote(struct mmClientUdp* p, const char* node, mmUShort_t port)
{
    mmNetUdp_SetRemote(&p->net_udp, node, port);
    p->ss_remote_update = 1;
}

MM_EXPORT_NET void mmClientUdp_SetQDefaultCallback(struct mmClientUdp* p, struct mmClientUdpCallback* client_udp_q_callback)
{
    assert(NULL != client_udp_q_callback && "you can not assign null client_udp_q_callback.");
    p->callback_q = *client_udp_q_callback;
}
MM_EXPORT_NET void mmClientUdp_SetNDefaultCallback(struct mmClientUdp* p, struct mmClientUdpCallback* client_udp_n_callback)
{
    assert(NULL != client_udp_n_callback && "you can not assign null client_udp_n_callback.");
    p->callback_n = *client_udp_n_callback;
}
// assign queue callback.at main thread.
MM_EXPORT_NET void mmClientUdp_SetQHandle(struct mmClientUdp* p, mmUInt32_t id, mmClientUdpHandleFunc callback)
{
    mmSpinlock_Lock(&p->locker_rbtree_q);
    mmRbtreeU32Vpt_Set(&p->rbtree_q, id, (void*)callback);
    mmSpinlock_Unlock(&p->locker_rbtree_q);
}
// assign net callback.not at main thread.
MM_EXPORT_NET void mmClientUdp_SetNHandle(struct mmClientUdp* p, mmUInt32_t id, mmClientUdpHandleFunc callback)
{
    mmSpinlock_Lock(&p->locker_rbtree_n);
    mmRbtreeU32Vpt_Set(&p->rbtree_n, id, (void*)callback);
    mmSpinlock_Unlock(&p->locker_rbtree_n);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientUdp_SetUdpCallback(struct mmClientUdp* p, struct mmUdpCallback* udp_callback)
{
    mmNetUdp_SetUdpCallback(&p->net_udp, udp_callback);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientUdp_SetEventUdpAllocator(struct mmClientUdp* p, struct mmNetUdpEventUdpAllocator* event_udp_allocator)
{
    mmNetUdp_SetEventUdpAllocator(&p->net_udp, event_udp_allocator);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientUdp_SetCryptoCallback(struct mmClientUdp* p, struct mmCryptoCallback* crypto_callback)
{
    mmNetUdp_SetCryptoCallback(&p->net_udp, crypto_callback);
}
// assign context handle.
MM_EXPORT_NET void mmClientUdp_SetContext(struct mmClientUdp* p, void* u)
{
    p->u = u;
    mmPacketQueue_SetContext(&p->queue_recv, p->u);
    mmPacketQueue_SetContext(&p->queue_send, p->u);
    mmNetUdp_SetContext(&p->net_udp, p->u);
}

// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmClientUdp_SetAddrContext(struct mmClientUdp* p, void* u)
{
    mmNetUdp_SetAddrContext(&p->net_udp, u);
}
MM_EXPORT_NET void* mmClientUdp_GetAddrContext(struct mmClientUdp* p)
{
    return mmNetUdp_GetAddrContext(&p->net_udp);
}

MM_EXPORT_NET void mmClientUdp_ClearHandleRbtree(struct mmClientUdp* p)
{
    mmSpinlock_Lock(&p->locker_rbtree_n);
    mmRbtreeU32Vpt_Clear(&p->rbtree_n);
    mmSpinlock_Unlock(&p->locker_rbtree_n);

    mmSpinlock_Lock(&p->locker_rbtree_q);
    mmRbtreeU32Vpt_Clear(&p->rbtree_q);
    mmSpinlock_Unlock(&p->locker_rbtree_q);

    mmPacketQueue_ClearHandleRbtree(&p->queue_recv);
    mmPacketQueue_ClearHandleRbtree(&p->queue_send);
    mmNetUdp_ClearHandleRbtree(&p->net_udp);
}

// client_udp crypto encrypt buffer by udp.
MM_EXPORT_NET void mmClientUdp_CryptoEncrypt(struct mmClientUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmNetUdp_CryptoEncrypt(&p->net_udp, udp, buffer, offset, length);
}
// client_udp crypto decrypt buffer by udp.
MM_EXPORT_NET void mmClientUdp_CryptoDecrypt(struct mmClientUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    mmNetUdp_CryptoDecrypt(&p->net_udp, udp, buffer, offset, length);
}

MM_EXPORT_NET void mmClientUdp_SetFlushHandleNearby(struct mmClientUdp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetSuccessNearby(&p->flush_task, milliseconds);
}
MM_EXPORT_NET void mmClientUdp_SetFlushBrokenNearby(struct mmClientUdp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetFailureNearby(&p->flush_task, milliseconds);
}
MM_EXPORT_NET void mmClientUdp_SetStateHandleNearby(struct mmClientUdp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetSuccessNearby(&p->state_task, milliseconds);
}
MM_EXPORT_NET void mmClientUdp_SetStateBrokenNearby(struct mmClientUdp* p, mmMSec_t milliseconds)
{
    mmSignalTask_SetFailureNearby(&p->state_task, milliseconds);
}
MM_EXPORT_NET void mmClientUdp_SetStateCheckFlag(struct mmClientUdp* p, mmUInt8_t flag)
{
    p->state_check_flag = flag;
}
MM_EXPORT_NET void mmClientUdp_SetStateQueueFlag(struct mmClientUdp* p, mmUInt8_t flag)
{
    p->state_queue_flag = flag;
}

MM_EXPORT_NET void mmClientUdp_SetMaxPoperNumber(struct mmClientUdp* p, size_t max_pop)
{
    mmPacketQueue_SetMaxPoperNumber(&p->queue_recv, max_pop);
    mmPacketQueue_SetMaxPoperNumber(&p->queue_send, max_pop);
}

// fopen socket.
MM_EXPORT_NET void mmClientUdp_FopenSocket(struct mmClientUdp* p)
{
    mmNetUdp_FopenSocket(&p->net_udp);
}
// bind.
MM_EXPORT_NET void mmClientUdp_Bind(struct mmClientUdp* p)
{
    mmNetUdp_Bind(&p->net_udp);
}
// close socket.
MM_EXPORT_NET void mmClientUdp_CloseSocket(struct mmClientUdp* p)
{
    mmNetUdp_CloseSocket(&p->net_udp);
}
// shutdown socket MM_BOTH_SHUTDOWN.
MM_EXPORT_NET void mmClientUdp_ShutdownSocket(struct mmClientUdp* p)
{
    mmNetUdp_ShutdownSocket(&p->net_udp, MM_BOTH_SHUTDOWN);
}

MM_EXPORT_NET int mmClientUdp_MulcastJoin(struct mmClientUdp* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmNetUdp_MulcastJoin(&p->net_udp, mr_multiaddr, mr_interface);
}
MM_EXPORT_NET int mmClientUdp_MulcastDrop(struct mmClientUdp* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmNetUdp_MulcastDrop(&p->net_udp, mr_multiaddr, mr_interface);
}

MM_EXPORT_NET void mmClientUdp_Check(struct mmClientUdp* p)
{
    mmNetUdp_Check(&p->net_udp);
}
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmClientUdp_FinallyState(struct mmClientUdp* p)
{
    return mmNetUdp_FinallyState(&p->net_udp);
}

// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmClientUdp_AttachSocket(struct mmClientUdp* p)
{
    mmNetUdp_AttachSocket(&p->net_udp);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmClientUdp_DetachSocket(struct mmClientUdp* p)
{
    mmNetUdp_DetachSocket(&p->net_udp);
}
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_NET void mmClientUdp_ThreadHandleRecv(struct mmClientUdp* p)
{
    mmPacketQueue_ThreadHandle(&p->queue_recv);
}
// net thread trigger.
MM_EXPORT_NET void mmClientUdp_ThreadHandleSend(struct mmClientUdp* p)
{
    mmPacketQueue_ThreadHandle(&p->queue_send);
}
MM_EXPORT_NET void mmClientUdp_PushRecv(struct mmClientUdp* p, void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    mmPacketQueue_Push(&p->queue_recv, obj, pack, remote);
}
MM_EXPORT_NET void mmClientUdp_PushSend(struct mmClientUdp* p, void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    mmPacketQueue_Push(&p->queue_send, obj, pack, remote);
}

// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientUdp_BufferSend(struct mmClientUdp* p, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote)
{
    return mmNetUdp_BufferSend(&p->net_udp, buffer, offset, length, remote);
}
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientUdp_FlushSend(struct mmClientUdp* p, struct mmSockaddr* remote)
{
    return mmNetUdp_FlushSend(&p->net_udp, remote);
}
MM_EXPORT_NET void mmClientUdp_SendBuffer(struct mmClientUdp* p, struct mmPacketHead* packet_head, size_t hlength, struct mmByteBuffer* buffer, struct mmSockaddr* remote)
{
    struct mmUdp* udp = &p->net_udp.udp;
    struct mmPoolPacketElem* pool_packet_elem = mmPacketQueue_Overdraft(&p->queue_send, hlength, buffer->length);

    mmPoolPacketElem_SetRemote(pool_packet_elem, remote);
    mmPoolPacketElem_SetObject(pool_packet_elem, udp);
    mmPoolPacketElem_SetBuffer(pool_packet_elem, packet_head, buffer);

    mmPacketQueue_Repayment(&p->queue_send, pool_packet_elem);
}

// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_NET void mmClientUdp_FlushSignal(struct mmClientUdp* p)
{
    mmSignalTask_Signal(&p->flush_task);
}
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_NET void mmClientUdp_StateSignal(struct mmClientUdp* p)
{
    mmSignalTask_Signal(&p->state_task);
}

// start wait thread.
MM_EXPORT_NET void mmClientUdp_Start(struct mmClientUdp* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmNetUdp_Start(&p->net_udp);
    mmSignalTask_Start(&p->flush_task);
    mmSignalTask_Start(&p->state_task);
}
// interrupt wait thread.
MM_EXPORT_NET void mmClientUdp_Interrupt(struct mmClientUdp* p)
{
    p->state = MM_TS_CLOSED;

    p->state_check_flag = MM_CLIENT_UDP_CHECK_INACTIVE;
    p->state_queue_flag = MM_CLIENT_UDP_QUEUE_INACTIVE;

    mmNetUdp_Interrupt(&p->net_udp);
    // signal the thread.
    mmSignalTask_Interrupt(&p->flush_task);
    mmSignalTask_Interrupt(&p->state_task);
}
// shutdown wait thread.
MM_EXPORT_NET void mmClientUdp_Shutdown(struct mmClientUdp* p)
{
    p->state = MM_TS_FINISH;

    p->state_check_flag = MM_CLIENT_UDP_CHECK_INACTIVE;
    p->state_queue_flag = MM_CLIENT_UDP_QUEUE_INACTIVE;

    mmNetUdp_Shutdown(&p->net_udp);
    // signal the thread.
    mmSignalTask_Shutdown(&p->flush_task);
    mmSignalTask_Shutdown(&p->state_task);
}
// join wait thread.
MM_EXPORT_NET void mmClientUdp_Join(struct mmClientUdp* p)
{
    mmNetUdp_Join(&p->net_udp);
    // signal the thread.
    mmSignalTask_Join(&p->flush_task);
    mmSignalTask_Join(&p->state_task);
}
MM_EXPORT_NET int mmClientUdp_UdpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    mmNetUdp_CryptoDecrypt(net_udp, udp, buffer, offset, length);
    return mmStreambufPacket_HandleUdp(&udp->buff_recv, &mmClientUdp_UdpHandlePacketCallback, udp, remote);
}
MM_EXPORT_NET int mmClientUdp_UdpBrokenCallback(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    assert(NULL != net_udp->callback.Broken && "net_udp->callback.Broken is a null.");
    mmNetUdp_ApplyBroken(net_udp);
    return 0;
}
MM_EXPORT_NET void mmClientUdp_UdpHandlePacketCallback(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    assert(NULL != net_udp->callback.Handle && "net_udp->callback.Handle is a null.");
    (*(net_udp->callback.Handle))(udp, net_udp->u, pack, remote);
}

static void __static_mmClientUdp_NetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);

    mmClientUdpHandleFunc handle = NULL;
    mmSpinlock_Lock(&client_udp->locker_rbtree_n);
    handle = (mmClientUdpHandleFunc)mmRbtreeU32Vpt_Get(&client_udp->rbtree_n, pack->phead.mid);
    mmSpinlock_Unlock(&client_udp->locker_rbtree_n);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, u, pack, remote);
    }
    else
    {
        assert(NULL != client_udp->callback_n.Handle && "client_udp->callback_n.Handle is a null.");
        (*(client_udp->callback_n.Handle))(udp, u, pack, remote);
    }
    // push to queue thread.
    if (MM_CLIENT_UDP_QUEUE_ACTIVATE == client_udp->state_queue_flag)
    {
        // push recv api is lock free.
        mmClientUdp_PushRecv(client_udp, obj, pack, remote);
    }
}
static void __static_mmClientUdp_NetUdpBroken(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);

    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_BROKEN, &__static_mmClientUdp_NetUdpHandle);
    // net state signal.
    mmClientUdp_StateSignal(client_udp);
}
static void __static_mmClientUdp_NetUdpNready(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);

    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_NREADY, &__static_mmClientUdp_NetUdpHandle);
    // net state signal.
    mmClientUdp_StateSignal(client_udp);
}
static void __static_mmClientUdp_NetUdpFinish(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);

    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_FINISH, &__static_mmClientUdp_NetUdpHandle);
    // net state signal.
    mmClientUdp_StateSignal(client_udp);
}
static int __static_mmClientUdp_FlushSignalTaskFactor(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(signal_task->callback.obj);
    struct mmUdp* udp = &client_udp->net_udp.udp;
    size_t buff_len = 0;
    int queue_empty = 0;
    mmUdp_OLock(udp);
    buff_len = mmStreambuf_Size(&udp->buff_send);
    mmUdp_OUnlock(udp);
    queue_empty = mmPacketQueue_IsEmpty(&client_udp->queue_send);
    code = (0 == buff_len && queue_empty ? 0 : -1);
    return code;
}
static int __static_mmClientUdp_FlushSignalTaskHandle(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(signal_task->callback.obj);
    struct mmUdp* udp = &client_udp->net_udp.udp;
    int real_len = 0;
    // check the state.
    if (0 == mmClientUdp_FinallyState(client_udp))
    {
        do
        {
            {
                // handle send data by flush send buffer.
                // 0 <  rt,means rt buffer is send,we must gbump rt size.
                // 0 == rt,means the send buffer can be full.
                // 0 >  rt,means the send process can be failure.
                mmClientUdp_OLock(client_udp);
                real_len = mmClientUdp_FlushSend(client_udp, &udp->socket.ss_remote);
                mmClientUdp_OUnlock(client_udp);
                if (0 > real_len)
                {
                    // the socket is broken.
                    // but the send thread can not broken.
                    // it is wait the reconnect and send.
                    code = -1;
                    break;
                }
                else
                {
                    // the finally state is regular.
                    // if 0 == real_len.the state is broken or the socket send buffer is full.
                    // just think it is regular.
                    code = 0;
                }
            }
            {
                // here we only pop single message for handle.
                mmPacketQueue_PopSingle(&client_udp->queue_send);

                // handle send data by.
                // use the net state for time task.
                if (0 == mmClientUdp_FinallyState(client_udp))
                {
                    // the finally state is regular.
                    // if 0 == real_len.the state is broken or the socket send buffer is full.
                    // just think it is regular.
                    code = 0;
                }
                else
                {
                    // the socket is broken.
                    // but the send thread can not broken.
                    // it is wait the reconnect and send.
                    code = -1;
                    break;
                }
            }
        } while (0);
    }
    else
    {
        // the finally state is broken.
        // wake the state check process.
        mmClientUdp_StateSignal(client_udp);
        code = -1;
    }
    return code;
}
static int __static_mmClientUdp_StateSignalTaskFactor(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(signal_task->callback.obj);
    code = (
        0 == client_udp->ss_native_update &&
        0 == client_udp->ss_remote_update &&
        0 == mmClientUdp_FinallyState(client_udp)) ? 0 : -1;
    return code;
}
static int __static_mmClientUdp_StateSignalTaskHandle(void* obj)
{
    int code = -1;
    struct mmSignalTask* signal_task = (struct mmSignalTask*)(obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(signal_task->callback.obj);
    if (MM_CLIENT_UDP_CHECK_ACTIVATE == client_udp->state_check_flag)
    {
        struct mmNetUdp* net_udp = &client_udp->net_udp;
        struct mmUdp* udp = &client_udp->net_udp.udp;
        struct mmSocket* socket = &udp->socket;
        if (
            0 != client_udp->ss_native_update ||
            0 != client_udp->ss_remote_update)
        {
            // if target address is not the real address,we disconnect current socket.
            mmNetUdp_SLock(net_udp);
            // first disconnect current socket.
            mmNetUdp_DetachSocket(net_udp);
            mmNetUdp_SUnlock(net_udp);

            client_udp->ss_native_update = 0;
            client_udp->ss_remote_update = 0;
        }
        // most of time default ip port is valid ss_native address, not neeed check.
        if (0 == mmSockaddr_CompareAddress(&socket->ss_remote, MM_ADDR_DEFAULT_NODE, MM_ADDR_DEFAULT_PORT))
        {
            // if the native and remote is empty.fire the broken event immediately.
            mmNetUdp_SLock(net_udp);
            mmNetUdp_ApplyBroken(net_udp);
            mmNetUdp_SUnlock(net_udp);
        }
        else
        {
            mmNetUdp_SLock(net_udp);
            mmNetUdp_Check(net_udp);
            mmNetUdp_SUnlock(net_udp);
        }
    }
    code = (0 == mmClientUdp_FinallyState(client_udp)) ? 0 : -1;
    return code;
}
static void __static_mmClientUdp_PacketQueueSendHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);

    // if connect state is good.
    if (0 == mmNetUdp_FinallyState(net_udp))
    {
        struct mmPacket packet;

        struct mmStreambuf* streambuf = &udp->buff_send;
        // handle send data by flush send buffer.
        // 0 <  rt,means rt buffer is send,we must gbump rt size.
        // 0 == rt,means the send buffer can be full.
        // 0 >  rt,means the send process can be failure.
        mmNetUdp_OLock(net_udp);
        mmMessageCoder_EncodeMessageByteBuffer(&udp->buff_send, &pack->bbuff, &pack->phead, pack->hbuff.length, &packet);
        mmNetUdp_CryptoEncrypt(net_udp, udp, streambuf->buff, streambuf->gptr, mmStreambuf_Size(streambuf));
        mmNetUdp_FlushSend(net_udp, remote);
        mmNetUdp_OUnlock(net_udp);
    }
}
static void __static_mmClientUdp_PacketQueueRecvHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);

    mmClientUdpHandleFunc handle = NULL;
    mmSpinlock_Lock(&client_udp->locker_rbtree_q);
    handle = (mmClientUdpHandleFunc)mmRbtreeU32Vpt_Get(&client_udp->rbtree_q, pack->phead.mid);
    mmSpinlock_Unlock(&client_udp->locker_rbtree_q);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, u, pack, remote);
    }
    else
    {
        assert(NULL != client_udp->callback_q.Handle && "client_udp->callback_q.Handle is a null.");
        (*(client_udp->callback_q.Handle))(udp, u, pack, remote);
    }
}
static void __static_mmClientUdp_EmptyHandleForCommonEvent(struct mmClientUdp* p)
{
    mmClientUdp_SetQHandle(p, MM_UDP_MID_FINISH, &__static_mmClientUdp_HandleDefault);
    mmClientUdp_SetQHandle(p, MM_UDP_MID_NREADY, &__static_mmClientUdp_HandleDefault);
    mmClientUdp_SetQHandle(p, MM_UDP_MID_BROKEN, &__static_mmClientUdp_HandleDefault);

    mmClientUdp_SetNHandle(p, MM_UDP_MID_FINISH, &__static_mmClientUdp_HandleDefault);
    mmClientUdp_SetNHandle(p, MM_UDP_MID_NREADY, &__static_mmClientUdp_HandleDefault);
    mmClientUdp_SetNHandle(p, MM_UDP_MID_BROKEN, &__static_mmClientUdp_HandleDefault);
}
