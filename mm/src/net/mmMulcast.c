/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMulcast.h"
#include "net/mmSockaddr.h"

#include "core/mmLogger.h"

#if MM_PLATFORM_WIN32 == MM_PLATFORM
// windows
#include <Iphlpapi.h>
// Iphlpapi.lib
#else
// unix
#include <net/if.h>
#endif//MM_PLATFORM

static int __static_mmMulcast_JoinInetv4(mmSocket_t fd, const char* mr_multiaddr, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct ip_mreq mreq_v4;
    const char* rl_interface = NULL == mr_interface ? "0.0.0.0" : mr_interface;
    inet_pton(MM_AF_INET4, rl_interface, &(mreq_v4.imr_interface));
    inet_pton(MM_AF_INET4, mr_multiaddr, &(mreq_v4.imr_multiaddr));
    if (0 != setsockopt(fd, MM_IPPROTO_IPV4, MM_IPV4_ENTER_GROUP, (const char *)&mreq_v4, sizeof(struct ip_mreq)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast join socket:(%d) mr_multiaddr: %s rl_interface: %s success.",
                      __FUNCTION__, __LINE__, fd, mr_multiaddr, rl_interface);
        return 0;
    }
}
static int __static_mmMulcast_JoinInetv6(mmSocket_t fd, const char* mr_multiaddr, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct ipv6_mreq mreq_v6;
    const char* rl_interface = NULL == mr_interface ? "" : mr_interface;
    mreq_v6.ipv6mr_interface = if_nametoindex(rl_interface);
    inet_pton(MM_AF_INET6, mr_multiaddr, &(mreq_v6.ipv6mr_multiaddr));
    if (0 != setsockopt(fd, MM_IPPROTO_IPV6, MM_IPV6_ENTER_GROUP, (const char *)&mreq_v6, sizeof(struct ipv6_mreq)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast join socket:(%d) mr_multiaddr: %s rl_interface: %s success.",
                      __FUNCTION__, __LINE__, fd, mr_multiaddr, rl_interface);
        return 0;
    }
}
static int __static_mmMulcast_DropInetv4(mmSocket_t fd, const char* mr_multiaddr, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct ip_mreq mreq_v4;
    const char* rl_interface = NULL == mr_interface ? "0.0.0.0" : mr_interface;
    inet_pton(MM_AF_INET4, rl_interface, &(mreq_v4.imr_interface));
    inet_pton(MM_AF_INET4, mr_multiaddr, &(mreq_v4.imr_multiaddr));
    if (0 != setsockopt(fd, MM_IPPROTO_IPV4, MM_IPV4_LEAVE_GROUP, (const char *)&mreq_v4, sizeof(struct ip_mreq)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast drop socket:(%d) mr_multiaddr: %s rl_interface: %s success.",
                      __FUNCTION__, __LINE__, fd, mr_multiaddr, rl_interface);
        return 0;
    }
}
static int __static_mmMulcast_DropInetv6(mmSocket_t fd, const char* mr_multiaddr, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct ipv6_mreq mreq_v6;
    const char* rl_interface = NULL == mr_interface ? "" : mr_interface;
    mreq_v6.ipv6mr_interface = if_nametoindex(rl_interface);
    inet_pton(MM_AF_INET6, mr_multiaddr, &(mreq_v6.ipv6mr_multiaddr));
    if (0 != setsockopt(fd, MM_IPPROTO_IPV6, MM_IPV6_LEAVE_GROUP, (const char *)&mreq_v6, sizeof(struct ipv6_mreq)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast drop socket:(%d) mr_multiaddr: %s rl_interface: %s success.",
                      __FUNCTION__, __LINE__, fd, mr_multiaddr, rl_interface);
        return 0;
    }
}
static int __static_mmMulcast_SetInterfaceInetv4(mmSocket_t fd, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct in_addr in_v4;
    const char* rl_interface = NULL == mr_interface ? "0.0.0.0" : mr_interface;
    inet_pton(MM_AF_INET4, rl_interface, &in_v4);
    if (0 != setsockopt(fd, IPPROTO_IP, IPV6_MULTICAST_IF, (const char *)&in_v4, sizeof(struct in_addr)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast set_interface socket:(%d) rl_interface: %s success.",
                      __FUNCTION__, __LINE__, fd, rl_interface);
        return 0;
    }
}
static int __static_mmMulcast_SetInterfaceInetv6(mmSocket_t fd, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmUInt32_t in_v6;
    const char* rl_interface = NULL == mr_interface ? "" : mr_interface;
    in_v6 = if_nametoindex(rl_interface);
    if (0 != setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_IF, (const char *)&in_v6, sizeof(mmUInt32_t)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast set_interface socket:(%d) rl_interface: %s success.",
                      __FUNCTION__, __LINE__, fd, rl_interface);
        return 0;
    }
}
static int __static_mmMulcast_GetInterfaceInetv4(mmSocket_t fd, struct mmString* mr_interface)
{
    struct in_addr in_v4;
    socklen_t optlen = sizeof(struct in_addr);
    if (0 != getsockopt(fd, IPPROTO_IP, IPV6_MULTICAST_IF, (char *)&in_v4, &optlen))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        char node[MM_NODE_NAME_LENGTH] = { 0 };
        inet_ntop(MM_AF_INET4, &in_v4, node, MM_NODE_NAME_LENGTH);
        mmString_Assigns(mr_interface, node);
        return 0;
    }
}
static int __static_mmMulcast_GetInterfaceInetv6(mmSocket_t fd, struct mmString* mr_interface)
{
    mmUInt32_t in_v6;
    socklen_t optlen = sizeof(mmUInt32_t);
    if (0 != getsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_IF, (char *)&in_v6, &optlen))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        char node[MM_NODE_NAME_LENGTH] = { 0 };
        if_indextoname(in_v6, node);
        mmString_Assigns(mr_interface, node);
        return 0;
    }
}
static int __static_mmMulcast_SetLoopInetv4(mmSocket_t fd, unsigned int loop)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmUInt8_t in_v4 = (mmUInt8_t)loop;
    if (0 != setsockopt(fd, IPPROTO_IP, IP_MULTICAST_LOOP, (const char*)&in_v4, sizeof(mmUInt8_t)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast set_loop socket:(%d) loop: %u success.",
                      __FUNCTION__, __LINE__, fd, loop);
        return 0;
    }
}
static int __static_mmMulcast_SetLoopInetv6(mmSocket_t fd, unsigned int loop)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmUInt32_t in_v6 = (mmUInt8_t)loop;
    if (0 != setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, (const char*)&in_v6, sizeof(mmUInt32_t)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast set_loop socket:(%d) loop: %u success.",
                      __FUNCTION__, __LINE__, fd, loop);
        return 0;
    }
}
static int __static_mmMulcast_GetLoopInetv4(mmSocket_t fd, unsigned int* loop)
{
    mmUInt8_t in_v4 = 0;
    socklen_t optlen = sizeof(mmUInt8_t);
    if (0 != getsockopt(fd, IPPROTO_IP, IP_MULTICAST_LOOP, (char*)&in_v4, &optlen))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        *loop = (unsigned int)in_v4;
        return 0;
    }
}
static int __static_mmMulcast_GetLoopInetv6(mmSocket_t fd, unsigned int* loop)
{
    mmUInt32_t in_v6 = 0;
    socklen_t optlen = sizeof(mmUInt32_t);
    if (0 != getsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, (char*)&in_v6, &optlen))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        *loop = (unsigned int)in_v6;
        return 0;
    }
}
static int __static_mmMulcast_SetTtlInetv4(mmSocket_t fd, int ttl)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmUInt8_t in_v4 = (mmUInt8_t)ttl;
    if (0 != setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, (const char*)&in_v4, sizeof(mmUInt8_t)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast set_ttl socket:(%d) ttl: %d success.",
                      __FUNCTION__, __LINE__, fd, ttl);
        return 0;
    }
}
static int __static_mmMulcast_SetTtlInetv6(mmSocket_t fd, int ttl)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    mmUInt32_t in_v6 = (mmUInt8_t)ttl;
    if (0 != setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, (const char*)&in_v6, sizeof(mmUInt32_t)))
    {
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        mmLogger_LogI(gLogger, "%s %d mulcast set_ttl socket:(%d) ttl: %d success.",
                      __FUNCTION__, __LINE__, fd, ttl);
        return 0;
    }
}
static int __static_mmMulcast_GetTtlInetv4(mmSocket_t fd, int* ttl)
{
    mmUInt8_t in_v4 = 0;
    socklen_t optlen = sizeof(mmUInt8_t);
    if (0 != getsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, (char*)&in_v4, &optlen))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        *ttl = (int)in_v4;
        return 0;
    }
}
static int __static_mmMulcast_GetTtlInetv6(mmSocket_t fd, int* ttl)
{
    mmUInt32_t in_v6 = 0;
    socklen_t optlen = sizeof(mmUInt32_t);
    if (0 != getsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, (char*)&in_v6, &optlen))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
        return -1;
    }
    else
    {
        *ttl = (int)in_v6;
        return 0;
    }
}

MM_EXPORT_NET int mmMulcast_Join(mmSocket_t fd, mmSAFamily_t ss_family, const char* mr_multiaddr, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_JoinInetv4(fd, mr_multiaddr, mr_interface);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_JoinInetv6(fd, mr_multiaddr, mr_interface);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support join mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_Drop(mmSocket_t fd, mmSAFamily_t ss_family, const char* mr_multiaddr, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_DropInetv4(fd, mr_multiaddr, mr_interface);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_DropInetv6(fd, mr_multiaddr, mr_interface);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support drop mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_SetInterface(mmSocket_t fd, mmSAFamily_t ss_family, const char* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_SetInterfaceInetv4(fd, mr_interface);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_SetInterfaceInetv6(fd, mr_interface);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support set_interface mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_GetInterface(mmSocket_t fd, mmSAFamily_t ss_family, struct mmString* mr_interface)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_GetInterfaceInetv4(fd, mr_interface);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_GetInterfaceInetv6(fd, mr_interface);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support get_interface mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_SetLoop(mmSocket_t fd, mmSAFamily_t ss_family, unsigned int loop)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_SetLoopInetv4(fd, loop);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_SetLoopInetv6(fd, loop);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support set_loop mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_GetLoop(mmSocket_t fd, mmSAFamily_t ss_family, unsigned int* loop)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_GetLoopInetv4(fd, loop);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_GetLoopInetv6(fd, loop);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support get_loop mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_SetTtl(mmSocket_t fd, mmSAFamily_t ss_family, int ttl)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_SetTtlInetv4(fd, ttl);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_SetTtlInetv6(fd, ttl);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support set_ttl mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
MM_EXPORT_NET int mmMulcast_GetTtl(mmSocket_t fd, mmSAFamily_t ss_family, int* ttl)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    int code = -1;
    switch (ss_family)
    {
    case MM_AF_INET4:
        code = __static_mmMulcast_GetTtlInetv4(fd, ttl);
        break;
    case MM_AF_INET6:
        code = __static_mmMulcast_GetTtlInetv6(fd, ttl);
        break;
    default:
        mmLogger_LogI(gLogger, "%s %d ss_family:%d is not support get_ttl mulcast.",
                      __FUNCTION__, __LINE__, (int)ss_family);
        break;
    }
    return code;
}
