/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMailbox.h"
#include "net/mmStreambufPacket.h"

#include "core/mmParameters.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
//
static void __static_mmMailbox_NuclearHandleRecv(void* obj, void* u);
static void __static_mmMailbox_NuclearHandleSend(void* obj, void* u);

static void __static_mmMailbox_AccepterAccept(void* obj, mmSocket_t fd, struct mmSockaddr* remote);

static void __static_mmMailbox_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{

}
static void __static_mmMailbox_BrokenDefault(void* obj)
{

}
static void __static_mmMailbox_NreadyDefault(void* obj)
{

}
static void __static_mmMailbox_FinishDefault(void* obj)
{

}
MM_EXPORT_NET void mmMailboxCallback_init(struct mmMailboxCallback* p)
{
    p->Handle = &__static_mmMailbox_HandleDefault;
    p->Broken = &__static_mmMailbox_BrokenDefault;
    p->Nready = &__static_mmMailbox_NreadyDefault;
    p->Finish = &__static_mmMailbox_FinishDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmMailboxCallback_destroy(struct mmMailboxCallback* p)
{
    p->Handle = &__static_mmMailbox_HandleDefault;
    p->Broken = &__static_mmMailbox_BrokenDefault;
    p->Nready = &__static_mmMailbox_NreadyDefault;
    p->Finish = &__static_mmMailbox_FinishDefault;
    p->obj = NULL;
}

static void __static_mmMailbox_EventTcpProduce(void* p, struct mmTcp* tcp)
{

}
static void __static_mmMailbox_EventTcpRecycle(void* p, struct mmTcp* tcp)
{

}

MM_EXPORT_NET void mmMailboxEventTcpAllocator_Init(struct mmMailboxEventTcpAllocator* p)
{
    p->EventTcpProduce = &__static_mmMailbox_EventTcpProduce;
    p->EventTcpRecycle = &__static_mmMailbox_EventTcpRecycle;
    p->obj = NULL;
}
MM_EXPORT_NET void mmMailboxEventTcpAllocator_Destroy(struct mmMailboxEventTcpAllocator* p)
{
    p->EventTcpProduce = &__static_mmMailbox_EventTcpProduce;
    p->EventTcpRecycle = &__static_mmMailbox_EventTcpRecycle;
    p->obj = NULL;
}

MM_EXPORT_NET void mmMailbox_Init(struct mmMailbox* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    struct mmAccepterCallback accepter_callback;
    struct mmNuclearCallback nuclear_callback;
    struct mmPoolElementAllocator hAllocator;

    mmPoolElement_Init(&p->pool_element);
    mmAccepter_Init(&p->accepter);
    mmNucleus_Init(&p->nucleus);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmTcpCallback_Init(&p->tcp_callback);
    mmMailboxCallback_init(&p->callback);
    mmMailboxEventTcpAllocator_Init(&p->event_tcp_allocator);
    mmCryptoCallback_Init(&p->crypto_callback);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->u = NULL;

    p->tcp_callback.Handle = &mmMailbox_TcpHandleCallback;
    p->tcp_callback.Broken = &mmMailbox_TcpBrokenCallback;
    p->tcp_callback.obj = p;

    accepter_callback.Accept = &__static_mmMailbox_AccepterAccept;
    accepter_callback.obj = p;
    mmAccepter_SetCallback(&p->accepter, &accepter_callback);

    nuclear_callback.HandleRecv = &__static_mmMailbox_NuclearHandleRecv;
    nuclear_callback.HandleSend = &__static_mmMailbox_NuclearHandleSend;
    nuclear_callback.obj = p;
    mmNucleus_SetCallback(&p->nucleus, &nuclear_callback);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);

    hAllocator.Produce = &mmTcp_Init;
    hAllocator.Recycle = &mmTcp_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmTcp));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_NET void mmMailbox_Destroy(struct mmMailbox* p)
{
    mmMailbox_ClearHandleRbtree(p);
    mmMailbox_Clear(p);

    mmPoolElement_Destroy(&p->pool_element);
    mmAccepter_Destroy(&p->accepter);
    mmNucleus_Destroy(&p->nucleus);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmTcpCallback_Destroy(&p->tcp_callback);
    mmMailboxCallback_destroy(&p->callback);
    mmMailboxEventTcpAllocator_Destroy(&p->event_tcp_allocator);
    mmCryptoCallback_Destroy(&p->crypto_callback);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->u = NULL;
}
MM_EXPORT_NET void mmMailbox_SetNative(struct mmMailbox* p, const char* node, mmUShort_t port)
{
    mmAccepter_SetNative(&p->accepter, node, port);
}
MM_EXPORT_NET void mmMailbox_SetRemote(struct mmMailbox* p, const char* node, mmUShort_t port)
{
    mmAccepter_SetRemote(&p->accepter, node, port);
}
// poll thread number.
MM_EXPORT_NET void mmMailbox_SetThreadNumber(struct mmMailbox* p, mmUInt32_t thread_number)
{
    mmMailbox_SetLength(p, thread_number);
}
// assign protocol_size.
MM_EXPORT_NET void mmMailbox_SetLength(struct mmMailbox* p, mmUInt32_t length)
{
    mmNucleus_SetLength(&p->nucleus, length);
}
MM_EXPORT_NET mmUInt32_t mmMailbox_GetLength(struct mmMailbox* p)
{
    return mmNucleus_GetLength(&p->nucleus);
}
// 192.168.111.123-65535(2)
MM_EXPORT_NET void mmMailbox_SetParameters(struct mmMailbox* p, const char* parameters)
{
    char node[MM_NODE_NAME_LENGTH] = { 0 };
    mmUInt32_t length = 0;
    mmUShort_t port = 0;
    mmParameters_Server(parameters, node, &port, &length);
    // 
    mmMailbox_SetNative(p, node, port);
    mmMailbox_SetLength(p, length);
}
// assign context handle.
MM_EXPORT_NET void mmMailbox_SetContext(struct mmMailbox* p, void* u)
{
    p->u = u;
}
// fopen.
MM_EXPORT_NET void mmMailbox_FopenSocket(struct mmMailbox* p)
{
    mmAccepter_FopenSocket(&p->accepter);
}
// bind.
MM_EXPORT_NET void mmMailbox_Bind(struct mmMailbox* p)
{
    mmAccepter_Bind(&p->accepter);
}
// listen.
MM_EXPORT_NET void mmMailbox_Listen(struct mmMailbox* p)
{
    mmAccepter_Listen(&p->accepter);
}
// close socket. mmMailbox_Join before call this.
MM_EXPORT_NET void mmMailbox_CloseSocket(struct mmMailbox* p)
{
    mmAccepter_CloseSocket(&p->accepter);
}

// lock to make sure the mailbox is thread safe.
MM_EXPORT_NET void mmMailbox_Lock(struct mmMailbox* p)
{
    mmPoolElement_Lock(&p->pool_element);
    mmAccepter_Lock(&p->accepter);
    mmSpinlock_Lock(&p->locker);
}
// unlock mailbox.
MM_EXPORT_NET void mmMailbox_Unlock(struct mmMailbox* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmAccepter_Unlock(&p->accepter);
    mmPoolElement_Unlock(&p->pool_element);
}

MM_EXPORT_NET void mmMailbox_SetHandle(struct mmMailbox* p, mmUInt32_t id, mmMailboxHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmMailbox_SetTcpCallback(struct mmMailbox* p, struct mmTcpCallback* tcp_callback)
{
    assert(NULL != tcp_callback && "you can not assign null tcp_callback.");
    p->tcp_callback = *tcp_callback;
}
MM_EXPORT_NET void mmMailbox_SetEventTcpAllocator(struct mmMailbox* p, struct mmMailboxEventTcpAllocator* event_tcp_allocator)
{
    assert(NULL != event_tcp_allocator && "you can not assign null event_tcp_allocator.");
    p->event_tcp_allocator = *event_tcp_allocator;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetCryptoCallback(struct mmMailbox* p, struct mmCryptoCallback* crypto_callback)
{
    assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
    p->crypto_callback = *crypto_callback;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetMailboxCallback(struct mmMailbox* p, struct mmMailboxCallback* mailbox_callback)
{
    assert(NULL != mailbox_callback && "you can not assign null mailbox_callback.");
    p->callback = *mailbox_callback;
}
MM_EXPORT_NET void mmMailbox_SetTcpAllocator(struct mmMailbox* p, struct mmRbtreeU32VptAllocator* allocator)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, allocator);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmMailbox_ClearHandleRbtree(struct mmMailbox* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
// mailbox crypto encrypt buffer by tcp.
MM_EXPORT_NET void mmMailbox_CryptoEncrypt(struct mmMailbox* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Encrypt))(p, tcp, buffer, offset, buffer, offset, length);
}
// mailbox crypto decrypt buffer by tcp.
MM_EXPORT_NET void mmMailbox_CryptoDecrypt(struct mmMailbox* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Decrypt))(p, tcp, buffer, offset, buffer, offset, length);
}

// not lock inside, lock tcp manual.
MM_EXPORT_NET void mmMailbox_Traver(struct mmMailbox* p, mmRbtreeVisibleU32HandleFunc handle, void* u)
{
    mmNucleus_Traver(&p->nucleus, handle, u);
}
// poll size.
MM_EXPORT_NET size_t mmMailbox_PollSize(struct mmMailbox* p)
{
    return mmNucleus_PollSize(&p->nucleus);
}

MM_EXPORT_NET void mmMailbox_Start(struct mmMailbox* p)
{
    // we must explicit assign thread number manual.
    assert(0 != p->nucleus.length && "0 != p->nucleus.length is invalid, not poll thread will do nothing.");
    mmAccepter_Start(&p->accepter);
    mmNucleus_Start(&p->nucleus);
}
MM_EXPORT_NET void mmMailbox_Interrupt(struct mmMailbox* p)
{
    mmAccepter_Interrupt(&p->accepter);
    mmNucleus_Interrupt(&p->nucleus);
}
MM_EXPORT_NET void mmMailbox_Shutdown(struct mmMailbox* p)
{
    mmAccepter_Shutdown(&p->accepter);
    mmNucleus_Shutdown(&p->nucleus);
}
MM_EXPORT_NET void mmMailbox_Join(struct mmMailbox* p)
{
    mmAccepter_Join(&p->accepter);
    mmNucleus_Join(&p->nucleus);
}

MM_EXPORT_NET struct mmTcp* mmMailbox_Add(struct mmMailbox* p, mmSocket_t fd, struct mmSockaddr* remote)
{
    struct mmTcp* tcp = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    tcp = mmMailbox_Get(p, fd);
    if (NULL == tcp)
    {
        struct mmTcpCallback tcp_callback;

        // pool_element produce.
        mmPoolElement_Lock(&p->pool_element);
        tcp = (struct mmTcp*)mmPoolElement_Produce(&p->pool_element);
        mmPoolElement_Unlock(&p->pool_element);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmTcp_Reset(tcp);
        //
        tcp_callback.Handle = p->tcp_callback.Handle;
        tcp_callback.Broken = p->tcp_callback.Broken;
        tcp_callback.obj = p;
        mmTcp_SetCallback(tcp, &tcp_callback);
        //
        mmTcp_SetRemoteStorage(tcp, remote);
        mmTcp_SetNativeStorage(tcp, &p->accepter.socket.ss_native);
        tcp->socket.socket = fd;
        mmSocket_SetBlock(&tcp->socket, MM_NONBLOCK);
        //
        mmNucleus_FdAdd(&p->nucleus, fd, tcp);
        //
        (*(p->event_tcp_allocator.EventTcpProduce))(p, tcp);
        assert(p->callback.Finish && "p->Finish is a null.");
        (*(p->callback.Finish))(tcp);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return tcp;
}
MM_EXPORT_NET struct mmTcp* mmMailbox_Get(struct mmMailbox* p, mmSocket_t fd)
{
    return (struct mmTcp*)mmNucleus_FdGet(&p->nucleus, fd);
}
MM_EXPORT_NET struct mmTcp* mmMailbox_GetInstance(struct mmMailbox* p, mmSocket_t fd, struct mmSockaddr* remote)
{
    struct mmTcp* tcp = mmMailbox_Get(p, fd);
    if (NULL == tcp)
    {
        tcp = mmMailbox_Add(p, fd, remote);
    }
    return tcp;
}
MM_EXPORT_NET void mmMailbox_Rmv(struct mmMailbox* p, mmSocket_t fd)
{
    struct mmTcp* tcp = mmMailbox_Get(p, fd);
    if (NULL != tcp)
    {
        // mmMailbox_RmvTcp(p, tcp);
        // here we can lock the tcp, becaue all `callback.broken` only recv process trigger.
        mmTcp_Lock(tcp);
        mmNucleus_FdRmv(&p->nucleus, fd, tcp);
        mmTcp_Unlock(tcp);
        (*(p->event_tcp_allocator.EventTcpRecycle))(p, tcp);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmTcp_Reset(tcp);
        // pool_element recycle.
        mmPoolElement_Lock(&p->pool_element);
        mmPoolElement_Recycle(&p->pool_element, tcp);
        mmPoolElement_Unlock(&p->pool_element);
    }
}
MM_EXPORT_NET void mmMailbox_RmvTcp(struct mmMailbox* p, struct mmTcp* tcp)
{
    mmNucleus_FdRmv(&p->nucleus, tcp->socket.socket, tcp);
}
MM_EXPORT_NET void mmMailbox_Clear(struct mmMailbox* p)
{
    mmUInt32_t i = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmTcp* tcp = NULL;
    struct mmNuclear* nuclear = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->nucleus.arrays_locker);
    for (i = 0; i < p->nucleus.length; ++i)
    {
        nuclear = p->nucleus.arrays[i];
        pthread_mutex_lock(&nuclear->cond_locker);
        // rbtree_m locker is lock inside at mmNuclear_FdRmvPoll.
        // here we only need lock the nuclear->cond_locker.
        // mmSpinlock_Lock(&nuclear->rbtree_visible.locker_m);
        n = mmRb_First(&nuclear->rbtree_visible.rbtree_m.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
            n = mmRb_Next(n);
            tcp = (struct mmTcp*)(it->v);
            mmTcp_Lock(tcp);
            mmNuclear_FdRmvPoll(nuclear, tcp->socket.socket, tcp);
            mmTcp_Unlock(tcp);
            (*(p->event_tcp_allocator.EventTcpRecycle))(p, tcp);
            // note: 
            // pool_element manager init and destroy.
            // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
            mmTcp_Reset(tcp);
            // pool_element recycle.
            mmPoolElement_Lock(&p->pool_element);
            mmPoolElement_Recycle(&p->pool_element, tcp);
            mmPoolElement_Unlock(&p->pool_element);
        }
        // mmSpinlock_Unlock(&nuclear->rbtree_visible.locker_m);
        pthread_mutex_unlock(&nuclear->cond_locker);
        // trigger poll wake signal.
        mmNuclear_FdPollSignal(nuclear);
    }
    mmSpinlock_Unlock(&p->nucleus.arrays_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}

// use this function for manual shutdown tcp.
// note: you can not use rmv or rmv_tcp for shutdown a activation tcp.
MM_EXPORT_NET void mmMailbox_ShutdownTcp(struct mmMailbox* p, struct mmTcp* tcp)
{
    mmTcp_ShutdownSocket(tcp, MM_BOTH_SHUTDOWN);
}

MM_EXPORT_NET void mmMailbox_TcpHandlePacketCallback(void* obj, struct mmPacket* pack)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmMailbox* mailbox = (struct mmMailbox*)(tcp->callback.obj);
    mmMailboxHandleFunc handle = NULL;
    mmSpinlock_Lock(&mailbox->rbtree_locker);
    handle = (mmMailboxHandleFunc)mmRbtreeU32Vpt_Get(&mailbox->rbtree, pack->phead.mid);
    mmSpinlock_Unlock(&mailbox->rbtree_locker);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(tcp, mailbox->u, pack);
    }
    else
    {
        assert(mailbox->callback.Handle && "mailbox->Handle is a null.");
        (*(mailbox->callback.Handle))(tcp, mailbox->u, pack);
    }
}
MM_EXPORT_NET int mmMailbox_TcpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmMailbox* mailbox = (struct mmMailbox*)(tcp->callback.obj);
    mmMailbox_CryptoDecrypt(mailbox, tcp, buffer, offset, length);
    return mmStreambufPacket_HandleTcp(&tcp->buff_recv, &mmMailbox_TcpHandlePacketCallback, tcp);
}
MM_EXPORT_NET int mmMailbox_TcpBrokenCallback(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmMailbox* mailbox = (struct mmMailbox*)(tcp->callback.obj);
    assert(mailbox->callback.Broken && "mailbox->Broken is a null.");
    (*(mailbox->callback.Broken))(tcp);
    mmMailbox_Rmv(mailbox, tcp->socket.socket);
    return 0;
}
MM_EXPORT_NET void mmMailbox_MidEventHandle(void* obj, mmUInt32_t mid, mmMailboxHandleFunc callback)
{
    struct mmPacket pack;
    struct mmPacketHead phead;
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmMailbox* mailbox = (struct mmMailbox*)(tcp->callback.obj);

    mmPacket_Init(&pack);
    mmPacket_HeadBaseZero(&pack);
    pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    pack.bbuff.length = 0;
    mmPacket_ShadowAlloc(&pack);
    pack.phead.mid = mid;
    mmPacket_HeadEncode(&pack, &pack.hbuff, &phead);
    (*(callback))(obj, mailbox->u, &pack);
    mmPacket_ShadowFree(&pack);
    mmPacket_Destroy(&pack);
}

static void __static_mmMailbox_NuclearHandleRecv(void* obj, void* u)
{
    struct mmTcp* tcp = (struct mmTcp*)u;
    mmTcp_HandleRecv(tcp);
}
static void __static_mmMailbox_NuclearHandleSend(void* obj, void* u)
{
    struct mmTcp* tcp = (struct mmTcp*)u;
    mmTcp_HandleSend(tcp);
}
static void __static_mmMailbox_AccepterAccept(void* obj, mmSocket_t fd, struct mmSockaddr* remote)
{
    struct mmAccepter* accepter = (struct mmAccepter*)obj;
    struct mmMailbox* mailbox = (struct mmMailbox*)(accepter->callback.obj);
    mmMailbox_GetInstance(mailbox, fd, remote);
}
