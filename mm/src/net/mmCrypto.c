/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmCrypto.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmCrypto_EncryptDefault(
    void* p, void* e,
    mmUInt8_t* buffer_i, size_t offset_i,
    mmUInt8_t* buffer_o, size_t offset_o,
    size_t length)
{

}

static void __static_mmCrypto_DecryptDefault(
    void* p, void* e,
    mmUInt8_t* buffer_i, size_t offset_i,
    mmUInt8_t* buffer_o, size_t offset_o,
    size_t length)
{

}

MM_EXPORT_NET void mmCryptoCallback_Init(struct mmCryptoCallback* p)
{
    p->Encrypt = &__static_mmCrypto_EncryptDefault;
    p->Decrypt = &__static_mmCrypto_DecryptDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmCryptoCallback_Destroy(struct mmCryptoCallback* p)
{
    p->Encrypt = &__static_mmCrypto_EncryptDefault;
    p->Decrypt = &__static_mmCrypto_DecryptDefault;
    p->obj = NULL;
}
