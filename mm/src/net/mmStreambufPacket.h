/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmStreambufPacket_h__
#define __mmStreambufPacket_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmByte.h"
#include "core/mmString.h"

#include "net/mmNetExport.h"
#include "net/mmPacket.h"

#include "core/mmPrefix.h"

/*
* util about struct mmStreambuf and struct mmPacket.
*/

// overdraft pack.hbuff pack.bbuff for encode.
// will pbump streambuf.
MM_EXPORT_NET mmUInt32_t mmStreambufPacket_Overdraft(struct mmStreambuf* p, struct mmPacket* pack);

// repayment pack.hbuff pack.bbuff for encode.
// will gbump streambuf.
MM_EXPORT_NET mmUInt32_t mmStreambufPacket_Repayment(struct mmStreambuf* p, struct mmPacket* pack);

/*
* MM_MSG_COMM_HEAD_SIZE valid.
*/

// packet handle tcp buffer and length for streambuf handle separate packet.
MM_EXPORT_NET int mmStreambufPacket_HandleTcp(struct mmStreambuf* p, mmPacketHandleTcpFunc handle, void* obj);
// packet handle udp buffer and length for streambuf handle separate packet.
MM_EXPORT_NET int mmStreambufPacket_HandleUdp(struct mmStreambuf* p, mmPacketHandleUdpFunc handle, void* obj, struct mmSockaddr* remote);

#include "core/mmSuffix.h"

#endif//__mmStreambufPacket_h__
