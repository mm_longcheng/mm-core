/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSelector_h__
#define __mmSelector_h__

#include "core/mmCore.h"
#include "core/mmThread.h"
#include "core/mmSpinlock.h"

#include "poller/mmSelect.h"

#include <pthread.h>

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

// selector default poll idle sleep milliseconds.
#define MM_SELECTOR_IDLE_SLEEP_MSEC 200

struct mmSelectorCallback
{
    void(*HandleRecv)(void* obj, void* u);
    void(*HandleSend)(void* obj, void* u);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmSelectorCallback_Init(struct mmSelectorCallback* p);
MM_EXPORT_NET void mmSelectorCallback_Destroy(struct mmSelectorCallback* p);

// selector is suitable for use select api for single file descriptor I/O event.
struct mmSelector
{
    // callback.
    struct mmSelectorCallback callback;
    // poller.
    struct mmSelect poll;
    // poll thread.
    pthread_t poll_thread;
    // external lock.
    mmAtomic_t locker;
    // poll timeout.
    mmMSec_t poll_timeout;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_NET void mmSelector_Init(struct mmSelector* p);
MM_EXPORT_NET void mmSelector_Destroy(struct mmSelector* p);

/* locker order is
*     locker
*/
MM_EXPORT_NET void mmSelector_Lock(struct mmSelector* p);
MM_EXPORT_NET void mmSelector_Unlock(struct mmSelector* p);

MM_EXPORT_NET void mmSelector_SetCallback(struct mmSelector* p, struct mmSelectorCallback* callback);

MM_EXPORT_NET void mmSelector_FdAdd(struct mmSelector* p, mmSocket_t fd, void* u);
MM_EXPORT_NET void mmSelector_FdRmv(struct mmSelector* p, mmSocket_t fd, void* u);

// wait for activation fd. 0 == milliseconds is noblock.
MM_EXPORT_NET void mmSelector_PollWait(struct mmSelector* p, mmMSec_t milliseconds);
// Poll loop.
MM_EXPORT_NET void mmSelector_PollLoop(struct mmSelector* p);

// start wait thread.
MM_EXPORT_NET void mmSelector_Start(struct mmSelector* p);
// interrupt wait thread.
MM_EXPORT_NET void mmSelector_Interrupt(struct mmSelector* p);
// shutdown wait thread.
MM_EXPORT_NET void mmSelector_Shutdown(struct mmSelector* p);
// join wait thread.
MM_EXPORT_NET void mmSelector_Join(struct mmSelector* p);

#include "core/mmSuffix.h"

#endif//__mmSelector_h__
