/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTcp_h__
#define __mmTcp_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmOSSocket.h"

#include "net/mmNetExport.h"
#include "net/mmSocket.h"

#include "core/mmPrefix.h"

enum mmTcpMid_t
{
    MM_TCP_MID_BROKEN = 0x0F000000,
    MM_TCP_MID_NREADY = 0x0F000001,
    MM_TCP_MID_FINISH = 0x0F000002,
};

struct mmTcpCallback
{
    // @param obj struct mmTcp*
    // @param b buffer
    // @param o offset
    // @param l lenght
    int(*Handle)(void* obj, mmUInt8_t* b, size_t o, size_t l);
    
    // @param obj struct mmTcp
    int(*Broken)(void* obj);
    
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmTcpCallback_Init(struct mmTcpCallback* p);
MM_EXPORT_NET void mmTcpCallback_Destroy(struct mmTcpCallback* p);
MM_EXPORT_NET void mmTcpCallback_Reset(struct mmTcpCallback* p);

struct mmTcp
{
    // strong ref. tcp socket. 
    // s_locker will lock this data.
    struct mmSocket socket;

    // strong ref.
    // i_locker will lock this data.
    struct mmStreambuf buff_recv;

    // strong ref.
    // o_locker will lock this data.
    struct mmStreambuf buff_send;

    // value ref. transport callback.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    struct mmTcpCallback callback;

    // external locker, locker_i.
    mmAtomic_t locker_i;

    // external locker, locker_o.
    mmAtomic_t locker_o;

    // feedback unique_id.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    mmUInt64_t unique_id;

    // user data.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    void* u;
};

MM_EXPORT_NET void mmTcp_Init(struct mmTcp* p);
MM_EXPORT_NET void mmTcp_Destroy(struct mmTcp* p);

/* locker order is
 *     socket
 *     i_locker
 *     o_locker
 */
MM_EXPORT_NET void mmTcp_Lock(struct mmTcp* p);
MM_EXPORT_NET void mmTcp_Unlock(struct mmTcp* p);
// s locker. 
MM_EXPORT_NET void mmTcp_SLock(struct mmTcp* p);
MM_EXPORT_NET void mmTcp_SUnlock(struct mmTcp* p);
// i locker. 
MM_EXPORT_NET void mmTcp_ILock(struct mmTcp* p);
MM_EXPORT_NET void mmTcp_IUnlock(struct mmTcp* p);
// o locker. 
MM_EXPORT_NET void mmTcp_OLock(struct mmTcp* p);
MM_EXPORT_NET void mmTcp_OUnlock(struct mmTcp* p);

/* reset api no locker. */
MM_EXPORT_NET void mmTcp_Reset(struct mmTcp* p);
// tcp streambuf recv send reset.
MM_EXPORT_NET void mmTcp_ResetStreambuf(struct mmTcp* p);

/* assign api no locker. */
// assign addr native by ip port.
MM_EXPORT_NET void mmTcp_SetNative(struct mmTcp* p, const char* node, mmUShort_t port);
// assign addr remote by ip port.
MM_EXPORT_NET void mmTcp_SetRemote(struct mmTcp* p, const char* node, mmUShort_t port);
// assign addr native by sockaddr_storage.AF_INET;
// after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmTcp_SetNativeStorage(struct mmTcp* p, struct mmSockaddr* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;
// after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmTcp_SetRemoteStorage(struct mmTcp* p, struct mmSockaddr* ss_remote);

MM_EXPORT_NET void mmTcp_SetCallback(struct mmTcp* p, struct mmTcpCallback* callback);

MM_EXPORT_NET void mmTcp_SetUniqueId(struct mmTcp* p, mmUInt64_t unique_id);
MM_EXPORT_NET mmUInt64_t mmTcp_GetUniqueId(struct mmTcp* p);

// context for tcp will use the mm_addr. u attribute. such as crypto context.
MM_EXPORT_NET void mmTcp_SetContext(struct mmTcp* p, void* u);
MM_EXPORT_NET void* mmTcp_GetContext(struct mmTcp* p);

// fopen socket.ss_remote.ss_family, SOCK_STREAM, 0
MM_EXPORT_NET void mmTcp_FopenSocket(struct mmTcp* p);
// close socket.
MM_EXPORT_NET void mmTcp_CloseSocket(struct mmTcp* p);
// shutdown socket.
MM_EXPORT_NET void mmTcp_ShutdownSocket(struct mmTcp* p, int opcode);

// handle recv for buffer pool and pool max size.
MM_EXPORT_NET void mmTcp_HandleRecv(struct mmTcp* p);
// handle send for buffer pool and pool max size.
MM_EXPORT_NET void mmTcp_HandleSend(struct mmTcp* p);

// handle recv data from buffer and buffer length.0 success -1 failure.
// @param b buffer
// @param o offset
// @param l lenght
MM_EXPORT_NET int mmTcp_BufferRecv(struct mmTcp* p, mmUInt8_t* b, size_t o, size_t l);

// handle send data from buffer and buffer length. > 0 is send size success -1 failure.
// note this function do not trigger the `callback.broken` it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger `callback.broken`, make sure not dead lock and invalid free pinter appear.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
//
// @param b buffer
// @param o offset
// @param l lenght
MM_EXPORT_NET int mmTcp_BufferSend(struct mmTcp* p, mmUInt8_t* b, size_t o, size_t l);

// handle send data by flush send buffer.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
MM_EXPORT_NET int mmTcp_FlushSend(struct mmTcp* p);

#include "core/mmSuffix.h"

#endif//__mmTcp_h__
