/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMessageCoder_h__
#define __mmMessageCoder_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmByte.h"
#include "core/mmString.h"

#include "net/mmNetExport.h"
#include "net/mmPacket.h"

#include "core/mmPrefix.h"

struct mmMessageCoder
{
    size_t(*Length)(void* obj);
    int(*Decode)(void* obj, struct mmByteBuffer* byte_buffer);
    int(*Encode)(void* obj, struct mmByteBuffer* byte_buffer);
};

MM_EXPORT_NET void mmMessageCoder_Init(struct mmMessageCoder* p);
MM_EXPORT_NET void mmMessageCoder_Destroy(struct mmMessageCoder* p);

MM_EXPORT_NET struct mmMessageCoder* mmMessageCoder_ByteBuffer(void);
MM_EXPORT_NET struct mmMessageCoder* mmMessageCoder_Streambuf(void);
MM_EXPORT_NET struct mmMessageCoder* mmMessageCoder_String(void);

MM_EXPORT_NET void
mmMessageCoder_StreambufOverdraft(
    struct mmStreambuf* streambuf,
    size_t hlength,
    size_t blength,
    struct mmPacket* pack);

// not call mmMessageCoder_StreambufOverdraft at inside.
MM_EXPORT_NET int
mmMessageCoder_AppendImplPacketHead(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadZero(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt64_t uid,
    mmUInt32_t mid,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadCopy(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt32_t mid,
    void* rs_msg,
    struct mmPacket* rq_pack,
    struct mmPacket* rs_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadCopyProxyRq(
    struct mmStreambuf* streambuf,
    mmUInt32_t pid,
    mmUInt64_t sid,
    struct mmPacket* rq_pack,
    struct mmPacket* pr_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadCopyProxyRs(
    struct mmStreambuf* streambuf,
    struct mmPacket* rs_pack,
    struct mmPacket* pr_pack);

// call mmMessageCoder_StreambufOverdraft at inside.
MM_EXPORT_NET int
mmMessageCoder_AppendPacketHead(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendHeadZero(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt64_t uid,
    mmUInt32_t mid,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendHeadCopy(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt32_t mid,
    void* rs_msg,
    struct mmPacket* rq_pack,
    struct mmPacket* rs_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendHeadCopyProxyRq(
    struct mmStreambuf* streambuf,
    mmUInt32_t pid,
    mmUInt64_t sid,
    struct mmPacket* rq_pack,
    struct mmPacket* pr_pack);

MM_EXPORT_NET int
mmMessageCoder_AppendHeadCopyProxyRs(
    struct mmStreambuf* streambuf,
    struct mmPacket* rs_pack,
    struct mmPacket* pr_pack);

// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_NET int
mmMessageCoder_EncodeMessageByteBuffer(
    struct mmStreambuf* streambuf,
    struct mmByteBuffer* message,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmPacket* pack);

// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_NET int
mmMessageCoder_EncodeMessageStreambuf(
    struct mmStreambuf* streambuf,
    struct mmStreambuf* message,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmPacket* pack);

// encode message message --> mmPacket.
// return code.
//   -1 is failure.
//    0 is success.
MM_EXPORT_NET int
mmMessageCoder_EncodeMessageString(
    struct mmStreambuf* streambuf,
    struct mmString* message,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmPacket* pack);

#include "core/mmSuffix.h"

#endif//__mmMessageCoder_h__
