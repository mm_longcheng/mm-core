/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmHeadset_h__
#define __mmHeadset_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmSpinlock.h"
#include "core/mmTime.h"

#include "net/mmNetExport.h"
#include "net/mmNetUdp.h"

#include "container/mmRbtreeU32.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// net udp nonblock timeout..
#define MM_HEADSET_NONBLOCK_TIMEOUT 20
// if bind listen failure will msleep this time.
#define MM_HEADSET_TRY_MSLEEP_TIME 1000
// net udp one time try time.
#define MM_HEADSET_TRYTIME -1

enum mmHeadsetState_t
{
    MM_HEADSET_STATE_CLOSED = 0,// fd is closed,connect closed.invalid at all.
    MM_HEADSET_STATE_MOTION = 1,// fd is opened,connect closed.at connecting.
    MM_HEADSET_STATE_FINISH = 2,// fd is opened,connect opened.at connected.
    MM_HEADSET_STATE_BROKEN = 3,// fd is opened,connect broken.connect is broken fd not closed.
};

// microphone. special for udp server.
struct mmMicUdp
{
    // weak ref.
    struct mmUdp udp;
    // thread.
    pthread_t poll_thread;
    // locker.
    mmAtomic_t locker;
    // mmNetUdpState_t, default is MM_HEADSET_STATE_CLOSED(0)
    int udp_state;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_NET void mmMicUdp_Init(struct mmMicUdp* p);
MM_EXPORT_NET void mmMicUdp_Destroy(struct mmMicUdp* p);

/* locker order is
*     udp
*     locker
*/
MM_EXPORT_NET void mmMicUdp_Lock(struct mmMicUdp* p);
MM_EXPORT_NET void mmMicUdp_Unlock(struct mmMicUdp* p);

MM_EXPORT_NET void mmMicUdp_SetUdpShared(struct mmMicUdp* p, struct mmUdp* udp);
MM_EXPORT_NET void mmMicUdp_UpdateUdpState(struct mmMicUdp* p, int state);
MM_EXPORT_NET void mmMicUdp_Clear(struct mmMicUdp* p);

MM_EXPORT_NET void mmMicUdp_Start(struct mmMicUdp* p);
MM_EXPORT_NET void mmMicUdp_Interrupt(struct mmMicUdp* p);
MM_EXPORT_NET void mmMicUdp_Shutdown(struct mmMicUdp* p);
MM_EXPORT_NET void mmMicUdp_Join(struct mmMicUdp* p);

MM_EXPORT_NET void mmMicUdp_PollWait(struct mmMicUdp* p);

// mic streambuf recv send reset.
MM_EXPORT_NET void mmMicUdp_ResetStreambuf(struct mmMicUdp* p);

typedef void(*mmHeadsetHandleFunc)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

struct mmHeadsetCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
    void(*Broken)(void* obj);
    void(*Nready)(void* obj);
    void(*Finish)(void* obj);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmHeadsetCallback_Init(struct mmHeadsetCallback* p);
MM_EXPORT_NET void mmHeadsetCallback_Destroy(struct mmHeadsetCallback* p);

// mmHeadset is a multithreading udp event accept.
struct mmHeadset
{
    // strong ref.
    struct mmUdp udp;
    // udp callback handle implement.
    struct mmUdpCallback udp_callback;
    struct mmHeadsetCallback callback;
    // crypto callback.
    struct mmCryptoCallback crypto_callback;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t arrays_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    // length. default is 0.
    mmUInt32_t length;
    // array pointer.
    struct mmMicUdp** arrays;
    // udp nonblock timeout.default is MM_HEADSET_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;
    // try bind and listen times.default is MM_HEADSET_TRYTIME.
    mmUInt32_t trytimes;

    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // mmNetUdpState_t,default is udp_state_closed(0)
    int udp_state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmHeadset_Init(struct mmHeadset* p);
MM_EXPORT_NET void mmHeadset_Destroy(struct mmHeadset* p);

/* locker order is
*     udp
*     locker
*/
MM_EXPORT_NET void mmHeadset_Lock(struct mmHeadset* p);
MM_EXPORT_NET void mmHeadset_Unlock(struct mmHeadset* p);

// assign native address but not connect.
MM_EXPORT_NET void mmHeadset_SetNative(struct mmHeadset* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmHeadset_SetRemote(struct mmHeadset* p, const char* node, mmUShort_t port);
// poll thread number.
MM_EXPORT_NET void mmHeadset_SetThreadNumber(struct mmHeadset* p, mmUInt32_t thread_number);
// assign protocol_size.
MM_EXPORT_NET void mmHeadset_SetLength(struct mmHeadset* p, mmUInt32_t length);
MM_EXPORT_NET mmUInt32_t mmHeadset_GetLength(struct mmHeadset* p);
// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
MM_EXPORT_NET void mmHeadset_SetParameters(struct mmHeadset* p, const char* parameters);
// assign context handle.
MM_EXPORT_NET void mmHeadset_SetContext(struct mmHeadset* p, void* u);
// assign nonblock_timeout.
MM_EXPORT_NET void mmHeadset_SetNonblockTimeout(struct mmHeadset* p, mmMSec_t nonblock_timeout);
// assign trytimes.
MM_EXPORT_NET void mmHeadset_SetTrytimes(struct mmHeadset* p, mmUInt32_t trytimes);

MM_EXPORT_NET void mmHeadset_SetHandle(struct mmHeadset* p, mmUInt32_t id, mmHeadsetHandleFunc callback);

MM_EXPORT_NET void mmHeadset_ClearHandleRbtree(struct mmHeadset* p);

// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmHeadset_SetUdpCallback(struct mmHeadset* p, struct mmUdpCallback* udp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmHeadset_SetCryptoCallback(struct mmHeadset* p, struct mmCryptoCallback* crypto_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmHeadset_SetHeadsetCallback(struct mmHeadset* p, struct mmHeadsetCallback* headset_callback);
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmHeadset_SetAddrContext(struct mmHeadset* p, void* u);
MM_EXPORT_NET void* mmHeadset_GetAddrContext(struct mmHeadset* p);

// headset crypto encrypt buffer by udp.
MM_EXPORT_NET void mmHeadset_CryptoEncrypt(struct mmHeadset* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length);
// headset crypto decrypt buffer by udp.
MM_EXPORT_NET void mmHeadset_CryptoDecrypt(struct mmHeadset* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length);

// fopen.
MM_EXPORT_NET void mmHeadset_FopenSocket(struct mmHeadset* p);
// bind.
MM_EXPORT_NET void mmHeadset_Bind(struct mmHeadset* p);
// close socket.mmHeadset_Join before call this.
MM_EXPORT_NET void mmHeadset_CloseSocket(struct mmHeadset* p);
// shutdown socket.
MM_EXPORT_NET void mmHeadset_ShutdownSocket(struct mmHeadset* p);

// get finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmHeadset_FinallyState(struct mmHeadset* p);

MM_EXPORT_NET int mmHeadset_MulcastJoin(struct mmHeadset* p, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_NET int mmHeadset_MulcastDrop(struct mmHeadset* p, const char* mr_multiaddr, const char* mr_interface);

MM_EXPORT_NET void mmHeadset_Clear(struct mmHeadset* p);

MM_EXPORT_NET void mmHeadset_ApplyBroken(struct mmHeadset* p);
MM_EXPORT_NET void mmHeadset_ApplyFinish(struct mmHeadset* p);

// this api will lock nothing.
MM_EXPORT_NET void mmHeadset_SendBuffer(
    struct mmHeadset* p,
    struct mmUdp* udp,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmByteBuffer* buffer,
    struct mmSockaddr* remote);

MM_EXPORT_NET void mmHeadset_Start(struct mmHeadset* p);
MM_EXPORT_NET void mmHeadset_Interrupt(struct mmHeadset* p);
MM_EXPORT_NET void mmHeadset_Shutdown(struct mmHeadset* p);
MM_EXPORT_NET void mmHeadset_Join(struct mmHeadset* p);

MM_EXPORT_NET void mmHeadset_UdpHandlePacketCallback(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET int mmHeadset_UdpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote);
MM_EXPORT_NET int mmHeadset_UdpBrokenCallback(void* obj);

MM_EXPORT_NET void mmHeadset_MidEventHandle(void* obj, mmUInt32_t mid, mmHeadsetHandleFunc callback);

#include "core/mmSuffix.h"

#endif//__mmHeadset_h__
