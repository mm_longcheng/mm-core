/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRbtreeSockaddr.h"
#include "core/mmAlloc.h"

MM_EXPORT_NET void mmRbtreeSockaddrU32Iterator_Init(struct mmRbtreeSockaddrU32Iterator* p)
{
    mmSockaddr_Init(&p->k);
    p->v = 0;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrU32Iterator_Destroy(struct mmRbtreeSockaddrU32Iterator* p)
{
    mmSockaddr_Destroy(&p->k);
    p->v = 0;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_NET void mmRbtreeSockaddrU32_Init(struct mmRbtreeSockaddrU32* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}
MM_EXPORT_NET void mmRbtreeSockaddrU32_Destroy(struct mmRbtreeSockaddrU32* p)
{
    mmRbtreeSockaddrU32_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_NET mmUInt32_t* mmRbtreeSockaddrU32_Add(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrU32Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeSockaddrU32Iterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrU32Iterator));
    mmRbtreeSockaddrU32Iterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return &it->v;
}
MM_EXPORT_NET void mmRbtreeSockaddrU32_Rmv(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrU32Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeSockaddrU32_Erase(p, it);
            // break.
            return;
        }
    }
}
MM_EXPORT_NET void mmRbtreeSockaddrU32_Erase(struct mmRbtreeSockaddrU32* p, struct mmRbtreeSockaddrU32Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeSockaddrU32Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}
MM_EXPORT_NET mmUInt32_t* mmRbtreeSockaddrU32_GetInstance(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k)
{
    mmUInt32_t* v = (mmUInt32_t*)mmRbtreeSockaddrU32_Get(p, k);
    if (NULL == v)
    {
        v = mmRbtreeSockaddrU32_Add(p, k);
    }
    return v;
}
MM_EXPORT_NET mmUInt32_t* mmRbtreeSockaddrU32_Get(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrU32Iterator* it = mmRbtreeSockaddrU32_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}
MM_EXPORT_NET struct mmRbtreeSockaddrU32Iterator* mmRbtreeSockaddrU32_GetIterator(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrU32Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrU32_Clear(struct mmRbtreeSockaddrU32* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeSockaddrU32Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeSockaddrU32Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeSockaddrU32_Erase(p, it);
    }
}
MM_EXPORT_NET size_t mmRbtreeSockaddrU32_Size(struct mmRbtreeSockaddrU32* p)
{
    return p->size;
}
MM_EXPORT_NET void mmRbtreeSockaddrU32_Set(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k, mmUInt32_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrU32Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrU32Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return;
        }
    }
    it = (struct mmRbtreeSockaddrU32Iterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrU32Iterator));
    mmRbtreeSockaddrU32Iterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
}

MM_EXPORT_NET void mmRbtreeSockaddrU64Iterator_Init(struct mmRbtreeSockaddrU64Iterator* p)
{
    mmSockaddr_Init(&p->k);
    p->v = 0;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrU64Iterator_Destroy(struct mmRbtreeSockaddrU64Iterator* p)
{
    mmSockaddr_Destroy(&p->k);
    p->v = 0;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_NET void mmRbtreeSockaddrU64_Init(struct mmRbtreeSockaddrU64* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}
MM_EXPORT_NET void mmRbtreeSockaddrU64_Destroy(struct mmRbtreeSockaddrU64* p)
{
    mmRbtreeSockaddrU64_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_NET mmUInt64_t* mmRbtreeSockaddrU64_Add(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrU64Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeSockaddrU64Iterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrU64Iterator));
    mmRbtreeSockaddrU64Iterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    it->v = 0;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return &it->v;
}
MM_EXPORT_NET void mmRbtreeSockaddrU64_Rmv(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrU64Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeSockaddrU64_Erase(p, it);
            // break.
            return;
        }
    }
}
MM_EXPORT_NET void mmRbtreeSockaddrU64_Erase(struct mmRbtreeSockaddrU64* p, struct mmRbtreeSockaddrU64Iterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeSockaddrU64Iterator_Destroy(it);
    mmFree(it);
    p->size--;
}
MM_EXPORT_NET mmUInt64_t* mmRbtreeSockaddrU64_GetInstance(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k)
{
    mmUInt64_t* v = (mmUInt64_t*)mmRbtreeSockaddrU64_Get(p, k);
    if (NULL == v)
    {
        v = mmRbtreeSockaddrU64_Add(p, k);
    }
    return v;
}
MM_EXPORT_NET mmUInt64_t* mmRbtreeSockaddrU64_Get(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrU64Iterator* it = mmRbtreeSockaddrU64_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}
MM_EXPORT_NET struct mmRbtreeSockaddrU64Iterator* mmRbtreeSockaddrU64_GetIterator(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrU64Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrU64_Clear(struct mmRbtreeSockaddrU64* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeSockaddrU64Iterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeSockaddrU64Iterator, n);
        n = mmRb_Next(n);
        mmRbtreeSockaddrU64_Erase(p, it);
    }
}
MM_EXPORT_NET size_t mmRbtreeSockaddrU64_Size(struct mmRbtreeSockaddrU64* p)
{
    return p->size;
}
MM_EXPORT_NET void mmRbtreeSockaddrU64_Set(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k, mmUInt64_t v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrU64Iterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrU64Iterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return;
        }
    }
    it = (struct mmRbtreeSockaddrU64Iterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrU64Iterator));
    mmRbtreeSockaddrU64Iterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
}

MM_EXPORT_NET void mmRbtreeSockaddrStringIterator_Init(struct mmRbtreeSockaddrStringIterator* p)
{
    mmSockaddr_Init(&p->k);
    mmString_Init(&p->v);
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrStringIterator_Destroy(struct mmRbtreeSockaddrStringIterator* p)
{
    mmSockaddr_Destroy(&p->k);
    mmString_Destroy(&p->v);
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_NET void mmRbtreeSockaddrString_Init(struct mmRbtreeSockaddrString* p)
{
    p->rbt.rb_node = NULL;
    p->size = 0;
}
MM_EXPORT_NET void mmRbtreeSockaddrString_Destroy(struct mmRbtreeSockaddrString* p)
{
    mmRbtreeSockaddrString_Clear(p);
    p->rbt.rb_node = NULL;
    p->size = 0;
}

MM_EXPORT_NET struct mmString* mmRbtreeSockaddrString_Add(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrStringIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeSockaddrStringIterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrStringIterator));
    mmRbtreeSockaddrStringIterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    mmString_Assigns(&it->v, "");
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return &it->v;
}
MM_EXPORT_NET void mmRbtreeSockaddrString_Rmv(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrStringIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeSockaddrString_Erase(p, it);
            // break.
            return;
        }
    }
}
MM_EXPORT_NET void mmRbtreeSockaddrString_Erase(struct mmRbtreeSockaddrString* p, struct mmRbtreeSockaddrStringIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    mmRbtreeSockaddrStringIterator_Destroy(it);
    mmFree(it);
    p->size--;
}
MM_EXPORT_NET struct mmString* mmRbtreeSockaddrString_GetInstance(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k)
{
    struct mmString* v = (struct mmString*)mmRbtreeSockaddrString_Get(p, k);
    if (NULL == v)
    {
        v = mmRbtreeSockaddrString_Add(p, k);
    }
    return v;
}
MM_EXPORT_NET struct mmString* mmRbtreeSockaddrString_Get(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrStringIterator* it = mmRbtreeSockaddrString_GetIterator(p, k);
    if (NULL != it) { return &it->v; }
    return NULL;
}
MM_EXPORT_NET struct mmRbtreeSockaddrStringIterator* mmRbtreeSockaddrString_GetIterator(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrStringIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrStringIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrString_Clear(struct mmRbtreeSockaddrString* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeSockaddrStringIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeSockaddrStringIterator, n);
        n = mmRb_Next(n);
        mmRbtreeSockaddrString_Erase(p, it);
    }
}
MM_EXPORT_NET size_t mmRbtreeSockaddrString_Size(struct mmRbtreeSockaddrString* p)
{
    return p->size;
}

// weak ref can use this.
MM_EXPORT_NET void mmRbtreeSockaddrString_Set(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k, struct mmString* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrStringIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrStringIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            mmString_Assign(&it->v, v);
            return;
        }
    }
    it = (struct mmRbtreeSockaddrStringIterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrStringIterator));
    mmRbtreeSockaddrStringIterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    mmString_Assign(&it->v, v);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
}

MM_EXPORT_NET void mmRbtreeSockaddrVptIterator_Init(struct mmRbtreeSockaddrVptIterator* p)
{
    mmSockaddr_Init(&p->k);
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrVptIterator_Destroy(struct mmRbtreeSockaddrVptIterator* p)
{
    mmSockaddr_Destroy(&p->k);
    p->v = NULL;
    p->n.rb_right = NULL;
    p->n.rb_left = NULL;
}

MM_EXPORT_NET void mmRbtreeSockaddrVptAllocator_Init(struct mmRbtreeSockaddrVptAllocator* p)
{
    p->Produce = &mmRbtreeSockaddrVpt_WeakProduce;
    p->Recycle = &mmRbtreeSockaddrVpt_WeakRecycle;
    p->obj = NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrVptAllocator_Destroy(struct mmRbtreeSockaddrVptAllocator* p)
{
    p->Produce = &mmRbtreeSockaddrVpt_WeakProduce;
    p->Recycle = &mmRbtreeSockaddrVpt_WeakRecycle;
    p->obj = NULL;
}

MM_EXPORT_NET void mmRbtreeSockaddrVpt_Init(struct mmRbtreeSockaddrVpt* p)
{
    p->rbt.rb_node = NULL;
    mmRbtreeSockaddrVptAllocator_Init(&p->allocator);
    p->size = 0;
}
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Destroy(struct mmRbtreeSockaddrVpt* p)
{
    mmRbtreeSockaddrVpt_Clear(p);
    p->rbt.rb_node = NULL;
    mmRbtreeSockaddrVptAllocator_Destroy(&p->allocator);
    p->size = 0;
}

MM_EXPORT_NET void mmRbtreeSockaddrVpt_SetAllocator(struct mmRbtreeSockaddrVpt* p, struct mmRbtreeSockaddrVptAllocator* allocator)
{
    assert(allocator && "you can not assign null allocator.");
    p->allocator = *allocator;
}

MM_EXPORT_NET void* mmRbtreeSockaddrVpt_Add(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrVptIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
            return NULL;
    }
    it = (struct mmRbtreeSockaddrVptIterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrVptIterator));
    mmRbtreeSockaddrVptIterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    it->v = (*(p->allocator.Produce))(p, k);
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
    return it->v;
}
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Rmv(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrVptIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
        {
            mmRbtreeSockaddrVpt_Erase(p, it);
            // break.
            return;
        }
    }
}
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Erase(struct mmRbtreeSockaddrVpt* p, struct mmRbtreeSockaddrVptIterator* it)
{
    mmRb_Erase(&it->n, &p->rbt);
    (*(p->allocator.Recycle))(p, &it->k, it->v);
    mmRbtreeSockaddrVptIterator_Destroy(it);
    mmFree(it);
    p->size--;
}
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_GetInstance(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k)
{
    void* v = mmRbtreeSockaddrVpt_Get(p, k);
    if (NULL == v)
    {
        v = mmRbtreeSockaddrVpt_Add(p, k);
    }
    return v;
}
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_Get(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrVptIterator* it = mmRbtreeSockaddrVpt_GetIterator(p, k);
    if (NULL != it) { return it->v; }
    return NULL;
}
MM_EXPORT_NET struct mmRbtreeSockaddrVptIterator* mmRbtreeSockaddrVpt_GetIterator(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k)
{
    struct mmRbtreeSockaddrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    struct mmRbNode* node = p->rbt.rb_node;
    while (NULL != node)
    {
        it = mmContainerOf(node, struct mmRbtreeSockaddrVptIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        if (result < 0)
            node = node->rb_left;
        else if (result > 0)
            node = node->rb_right;
        else
            return it;
    }
    return NULL;
}
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Clear(struct mmRbtreeSockaddrVpt* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeSockaddrVptIterator* it = NULL;
    //
    n = mmRb_First(&p->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeSockaddrVptIterator, n);
        n = mmRb_Next(n);
        mmRbtreeSockaddrVpt_Erase(p, it);
    }
}
MM_EXPORT_NET size_t mmRbtreeSockaddrVpt_Size(struct mmRbtreeSockaddrVpt* p)
{
    return p->size;
}
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Set(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k, void* v)
{
    struct mmRbNode **newe = NULL;
    struct mmRbNode *parent = NULL;
    struct mmRbtreeSockaddrVptIterator* it = NULL;
    mmSInt32_t result = 0;
    newe = &(p->rbt.rb_node);
    /* Figure out where to put new node */
    while (NULL != *newe)
    {
        it = mmContainerOf(*newe, struct mmRbtreeSockaddrVptIterator, n);
        result = mmRbtreeSockaddrCompareFunc(&it->k, k);
        parent = *newe;
        if (result < 0)
            newe = &((*newe)->rb_left);
        else if (result > 0)
            newe = &((*newe)->rb_right);
        else
        {
            it->v = v;
            return;
        }
    }
    it = (struct mmRbtreeSockaddrVptIterator*)mmMalloc(sizeof(struct mmRbtreeSockaddrVptIterator));
    mmRbtreeSockaddrVptIterator_Init(it);
    mmSockaddr_CopyFrom(&it->k, k);
    it->v = v;
    /* Add new node and rebalance tree. */
    mmRb_LinkNode(&it->n, parent, newe);
    mmRb_InsertColor(&it->n, &p->rbt);
    p->size++;
}
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_WeakProduce(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k)
{
    return NULL;
}
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_WeakRecycle(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k, void* v)
{
    return v;
}

