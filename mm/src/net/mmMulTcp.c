/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMulTcp.h"
#include "core/mmAlloc.h"
#include "container/mmListVpt.h"

static void __static_mmMulTcp_NetTcpHandle(void* obj, void* u, struct mmPacket* pack);
static void __static_mmMulTcp_NetTcpBroken(void* obj);
static void __static_mmMulTcp_NetTcpNready(void* obj);
static void __static_mmMulTcp_NetTcpFinish(void* obj);

static void __static_mmMulTcp_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{

}
static void __static_mmMulTcp_BrokenDefault(void* obj)
{

}
static void __static_mmMulTcp_NreadyDefault(void* obj)
{

}
static void __static_mmMulTcp_FinishDefault(void* obj)
{

}
MM_EXPORT_NET void mmMulTcpCallback_Init(struct mmMulTcpCallback* p)
{
    p->Handle = &__static_mmMulTcp_HandleDefault;
    p->Broken = &__static_mmMulTcp_BrokenDefault;
    p->Nready = &__static_mmMulTcp_NreadyDefault;
    p->Finish = &__static_mmMulTcp_FinishDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmMulTcpCallback_Destroy(struct mmMulTcpCallback* p)
{
    p->Handle = &__static_mmMulTcp_HandleDefault;
    p->Broken = &__static_mmMulTcp_BrokenDefault;
    p->Nready = &__static_mmMulTcp_NreadyDefault;
    p->Finish = &__static_mmMulTcp_FinishDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmMulTcp_Init(struct mmMulTcp* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmSockaddr_Init(&p->ss_native);
    mmSockaddr_Init(&p->ss_remote);
    MM_LIST_INIT_HEAD(&p->list);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmTcpCallback_Init(&p->tcp_callback);
    mmMulTcpCallback_Init(&p->callback);
    mmCryptoCallback_Init(&p->crypto_callback);
    mmString_Init(&p->cache_native_node);
    mmString_Init(&p->cache_remote_node);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->list_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    pthread_key_create(&p->thread_key, NULL);
    p->cache_remote_port = 0;

    p->nonblock_timeout = MM_NETTCP_NONBLOCK_TIMEOUT;

    p->cache_timecode = 0;
    p->cache_timeout = MM_MULTCP_CACHE_TIMEOUT_DEFAULT;

    p->trytimes = -1;
    p->unique_id = 0;
    p->state = MM_TS_CLOSED;

    p->tcp_callback.Handle = &mmNetTcp_TcpHandleCallback;
    p->tcp_callback.Broken = &mmNetTcp_TcpBrokenCallback;
    p->tcp_callback.obj = p;

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_NET void mmMulTcp_Destroy(struct mmMulTcp* p)
{
    mmMulTcp_ClearHandleRbtree(p);
    mmMulTcp_Clear(p);
    //
    mmSockaddr_Destroy(&p->ss_native);
    mmSockaddr_Destroy(&p->ss_remote);
    MM_LIST_INIT_HEAD(&p->list);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmTcpCallback_Destroy(&p->tcp_callback);
    mmMulTcpCallback_Destroy(&p->callback);
    mmCryptoCallback_Destroy(&p->crypto_callback);
    mmString_Destroy(&p->cache_native_node);
    mmString_Destroy(&p->cache_remote_node);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->list_locker);
    mmSpinlock_Destroy(&p->locker);
    pthread_key_delete(p->thread_key);
    p->cache_remote_port = 0;

    p->nonblock_timeout = 0;

    p->cache_timecode = 0;
    p->cache_timeout = MM_MULTCP_CACHE_TIMEOUT_DEFAULT;

    p->trytimes = 0;
    p->unique_id = 0;
    p->state = MM_TS_CLOSED;
}

// assign native address but not connect.
MM_EXPORT_NET void mmMulTcp_SetNative(struct mmMulTcp* p, const char* node, mmUShort_t port)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmSockaddr_Assign(&p->ss_native, node, port);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_SetNative(net_tcp, node, port);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// assign remote address but not connect.
MM_EXPORT_NET void mmMulTcp_SetRemote(struct mmMulTcp* p, const char* node, mmUShort_t port)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmSockaddr_Assign(&p->ss_remote, node, port);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_SetRemote(net_tcp, node, port);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_NET void mmMulTcp_SetHandle(struct mmMulTcp* p, mmUInt32_t id, mmNetTcpHandleFunc callback)
{
    // we not need assign all net tcp instance,but use default and hander by p->rbtree.
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmMulTcp_SetDefaultCallback(struct mmMulTcp* p, struct mmMulTcpCallback* multcp_callback)
{
    assert(NULL != multcp_callback && "you can not assign null multcp_callback.");
    p->callback = *multcp_callback;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMulTcp_SetTcpCallback(struct mmMulTcp* p, struct mmTcpCallback* tcp_callback)
{
    struct mmTcpCallback _tcp_callback;
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    assert(NULL != tcp_callback && "you can not assign null tcp_callback.");
    p->tcp_callback = *tcp_callback;
    _tcp_callback.Handle = p->tcp_callback.Handle;
    _tcp_callback.Broken = p->tcp_callback.Broken;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        _tcp_callback.obj = net_tcp;
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_SetTcpCallback(net_tcp, &_tcp_callback);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMulTcp_SetCryptoCallback(struct mmMulTcp* p, struct mmCryptoCallback* crypto_callback)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
    p->crypto_callback = *crypto_callback;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_SetCryptoCallback(net_tcp, &p->crypto_callback);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// assign context handle.
MM_EXPORT_NET void mmMulTcp_SetContext(struct mmMulTcp* p, void* u)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->u = u;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_SetContext(net_tcp, p->u);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// cache timecode
MM_EXPORT_NET void mmMulTcp_SetCacheTimecode(struct mmMulTcp* p, mmUInt64_t cache_timecode)
{
    p->cache_timecode = cache_timecode;
}
// cache timeout
MM_EXPORT_NET void mmMulTcp_SetCacheTimeout(struct mmMulTcp* p, mmUInt32_t cache_timeout)
{
    p->cache_timeout = cache_timeout;
}

MM_EXPORT_NET void mmMulTcp_ClearHandleRbtree(struct mmMulTcp* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

// lock to make sure the multcp is thread safe.
MM_EXPORT_NET void mmMulTcp_Lock(struct mmMulTcp* p)
{
    mmSpinlock_Lock(&p->locker);
}
// unlock multcp.
MM_EXPORT_NET void mmMulTcp_Unlock(struct mmMulTcp* p)
{
    mmSpinlock_Unlock(&p->locker);
}

// fopen socket.
MM_EXPORT_NET void mmMulTcp_FopenSocket(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_FopenSocket(net_tcp);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// synchronize connect.can call this as we already open connect check thread.
MM_EXPORT_NET void mmMulTcp_Connect(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_Connect(net_tcp);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// close socket.
MM_EXPORT_NET void mmMulTcp_CloseSocket(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_CloseSocket(net_tcp);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_NET void mmMulTcp_Check(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_Check(net_tcp);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmMulTcp_AttachSocket(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_AttachSocket(net_tcp);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmMulTcp_DetachSocket(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_SLock(net_tcp);
        mmNetTcp_DetachSocket(net_tcp);
        mmNetTcp_SUnlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

// get current thread net tcp.
MM_EXPORT_NET struct mmNetTcp* mmMulTcp_ThreadInstance(struct mmMulTcp* p)
{
    struct mmNetTcp* net_tcp = NULL;
    struct mmListVptIterator* lvp = NULL;
    lvp = (struct mmListVptIterator*)pthread_getspecific(p->thread_key);
    if (NULL == lvp)
    {
        struct mmNetTcpCallback net_tcp_callback;
        struct mmTcpCallback _tcp_callback;
        lvp = (struct mmListVptIterator*)mmMalloc(sizeof(struct mmListVptIterator));
        mmListVptIterator_Init(lvp);
        pthread_setspecific(p->thread_key, lvp);
        net_tcp = (struct mmNetTcp*)mmMalloc(sizeof(struct mmNetTcp));
        mmNetTcp_Init(net_tcp);
        mmNetTcp_SetNativeStorage(net_tcp, &p->ss_native);
        mmNetTcp_SetRemoteStorage(net_tcp, &p->ss_remote);
        mmNetTcp_SetContext(net_tcp, p->u);

        net_tcp_callback.Handle = &__static_mmMulTcp_NetTcpHandle;
        net_tcp_callback.Broken = &__static_mmMulTcp_NetTcpBroken;
        net_tcp_callback.Nready = &__static_mmMulTcp_NetTcpNready;
        net_tcp_callback.Finish = &__static_mmMulTcp_NetTcpFinish;
        net_tcp_callback.obj = p;
        mmNetTcp_SetNetTcpCallback(net_tcp, &net_tcp_callback);

        mmNetTcp_SetCryptoCallback(net_tcp, &p->crypto_callback);

        _tcp_callback.Handle = p->tcp_callback.Handle;
        _tcp_callback.Broken = p->tcp_callback.Broken;
        _tcp_callback.obj = net_tcp;
        mmNetTcp_SetTcpCallback(net_tcp, &_tcp_callback);
        //
        if (MM_TS_MOTION == p->state)
        {
            // if current route is in motion we start the new net tcp .
            mmNetTcp_Start(net_tcp);
        }
        else
        {
            // if current route is not motion,we assign the same state.
            net_tcp->state = p->state;
        }
        //
        lvp->v = net_tcp;
        mmSpinlock_Lock(&p->list_locker);
        mmList_Add(&lvp->n, &p->list);
        mmSpinlock_Unlock(&p->list_locker);
    }
    else
    {
        net_tcp = (struct mmNetTcp*)(lvp->v);
    }
    return net_tcp;
}
// clear all net tcp.
MM_EXPORT_NET void mmMulTcp_Clear(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    struct mmListVptIterator* lvp = NULL;
    struct mmNetTcp* net_tcp = NULL;
    mmSpinlock_Lock(&p->list_locker);
    pos = p->list.next;
    while (pos != &p->list)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        lvp = mmList_Entry(curr, struct mmListVptIterator, n);
        net_tcp = (struct mmNetTcp*)(lvp->v);
        mmNetTcp_Lock(net_tcp);
        mmList_Del(curr);
        mmNetTcp_DetachSocket(net_tcp);
        mmNetTcp_Unlock(net_tcp);
        mmNetTcp_Destroy(net_tcp);
        mmFree(net_tcp);
        mmListVptIterator_Destroy(lvp);
        mmFree(lvp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

MM_EXPORT_NET void mmMulTcp_Start(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_Lock(net_tcp);
        mmNetTcp_Start(net_tcp);
        mmNetTcp_Unlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}
MM_EXPORT_NET void mmMulTcp_Interrupt(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_CLOSED;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_Lock(net_tcp);
        mmNetTcp_Interrupt(net_tcp);
        mmNetTcp_Unlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_NET void mmMulTcp_Shutdown(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    mmSpinlock_Lock(&p->list_locker);
    p->state = MM_TS_FINISH;
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_Lock(net_tcp);
        mmNetTcp_Shutdown(net_tcp);
        mmNetTcp_Unlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_NET void mmMulTcp_Join(struct mmMulTcp* p)
{
    struct mmListHead* pos = NULL;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->list_locker);
    mmList_ForEach(pos, &p->list)
    {
        struct mmListVptIterator* e = mmList_Entry(pos, struct mmListVptIterator, n);
        struct mmNetTcp* net_tcp = (struct mmNetTcp*)(e->v);
        mmNetTcp_Lock(net_tcp);
        mmNetTcp_Join(net_tcp);
        mmNetTcp_Unlock(net_tcp);
    }
    mmSpinlock_Unlock(&p->list_locker);
}

static void __static_mmMulTcp_NetTcpHandle(void* obj, void* u, struct mmPacket* pack)
{
    struct mmTcp* tcp = (struct mmTcp*)obj;
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmMulTcp* multcp = (struct mmMulTcp*)(net_tcp->callback.obj);
    mmNetTcpHandleFunc handle = NULL;
    mmSpinlock_Lock(&multcp->rbtree_locker);
    handle = (mmNetTcpHandleFunc)mmRbtreeU32Vpt_Get(&multcp->rbtree, pack->phead.mid);
    mmSpinlock_Unlock(&multcp->rbtree_locker);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(tcp, u, pack);
    }
    else
    {
        assert(NULL != multcp->callback.Handle && "multcp->callback.Handle is a null.");
        (*(multcp->callback.Handle))(tcp, u, pack);
    }
}
static void __static_mmMulTcp_NetTcpBroken(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)obj;
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmMulTcp* multcp = (struct mmMulTcp*)(net_tcp->callback.obj);
    assert(NULL != multcp->callback.Broken && "multcp->callback.Broken is a null.");
    (*(multcp->callback.Broken))(tcp);
}
static void __static_mmMulTcp_NetTcpNready(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)obj;
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmMulTcp* multcp = (struct mmMulTcp*)(net_tcp->callback.obj);
    assert(NULL != multcp->callback.Nready && "multcp->callback.Nready is a null.");
    (*(multcp->callback.Nready))(tcp);
}
static void __static_mmMulTcp_NetTcpFinish(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)obj;
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    struct mmMulTcp* multcp = (struct mmMulTcp*)(net_tcp->callback.obj);
    assert(NULL != multcp->callback.Finish && "multcp->callback.Finish is a null.");
    (*(multcp->callback.Finish))(tcp);
}

