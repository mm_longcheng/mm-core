/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCrypto_h__
#define __mmCrypto_h__

#include "core/mmCore.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

struct mmCryptoCallback
{
    // event encrypt.
    void(*Encrypt)(void* p, void* e,
                   mmUInt8_t* buffer_i, size_t offset_i,
                   mmUInt8_t* buffer_o, size_t offset_o,
                   size_t length);
    
    // event decrypt.
    void(*Decrypt)(void* p, void* e,
                   mmUInt8_t* buffer_i, size_t offset_i,
                   mmUInt8_t* buffer_o, size_t offset_o,
                   size_t length);

    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_NET void mmCryptoCallback_Init(struct mmCryptoCallback* p);
MM_EXPORT_NET void mmCryptoCallback_Destroy(struct mmCryptoCallback* p);

#include "core/mmSuffix.h"

#endif//__mmCrypto_h__
