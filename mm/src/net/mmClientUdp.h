/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmClientUdp_h__
#define __mmClientUdp_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmThread.h"
#include "core/mmSignalTask.h"

#include "net/mmNetUdp.h"
#include "net/mmPacketQueue.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

// client udp connect handle check milliseconds.default is 5 second(5000 ms).
#define MM_CLIENT_UDP_HANDLE_MSEC_CHECK 5000
// client udp connect broken check milliseconds.default is 1 second(1000 ms).
#define MM_CLIENT_UDP_BROKEN_MSEC_CHECK 1000

// send thread msleep interval handle time.
#define MM_CLIENT_UDP_SEND_INTERVAL_HANDLE_TIME 0
// send thread msleep interval broken time.
#define MM_CLIENT_UDP_SEND_INTERVAL_BROKEN_TIME 1000

enum mmClientUdpCheckFlag_t
{
    MM_CLIENT_UDP_CHECK_INACTIVE = 0,// do not check the connect state.
    MM_CLIENT_UDP_CHECK_ACTIVATE = 1,// check the connect state.
};
enum mmClientUdpQueueFlag_t
{
    MM_CLIENT_UDP_QUEUE_INACTIVE = 0,// not push to queue.
    MM_CLIENT_UDP_QUEUE_ACTIVATE = 1,// push to queue.
};

typedef void(*mmClientUdpHandleFunc)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
struct mmClientUdpCallback
{
    mmClientUdpHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmClientUdpCallback_Init(struct mmClientUdpCallback* p);
MM_EXPORT_NET void mmClientUdpCallback_Destroy(struct mmClientUdpCallback* p);

struct mmClientUdp
{
    // real net udp.
    struct mmNetUdp net_udp;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_n;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_q;
    // thread handle message queue.
    struct mmPacketQueue queue_recv;
    // thread handle message queue.
    struct mmPacketQueue queue_send;
    // value ref. udp callback.
    struct mmClientUdpCallback callback_n;
    // value ref. udp callback.
    struct mmClientUdpCallback callback_q;
    // flush task for flush send message buffer.
    struct mmSignalTask flush_task;
    // state task for check state.
    struct mmSignalTask state_task;
    mmAtomic_t locker_rbtree_n;
    mmAtomic_t locker_rbtree_q;
    mmAtomic_t locker;
    mmUInt8_t ss_native_update;
    mmUInt8_t ss_remote_update;
    // default is MM_CLIENT_UDP_CHECK_INACTIVE.
    mmUInt8_t state_check_flag;
    // default is MM_CLIENT_UDP_QUEUE_ACTIVATE.
    mmUInt8_t state_queue_flag;
    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmClientUdp_Init(struct mmClientUdp* p);
MM_EXPORT_NET void mmClientUdp_Destroy(struct mmClientUdp* p);

/* locker order is
*     net_udp
*     queue_recv
*     queue_send
*     flush_task
*     state_task
*     locker
*/
MM_EXPORT_NET void mmClientUdp_Lock(struct mmClientUdp* p);
MM_EXPORT_NET void mmClientUdp_Unlock(struct mmClientUdp* p);
// i locker
MM_EXPORT_NET void mmClientUdp_ILock(struct mmClientUdp* p);
MM_EXPORT_NET void mmClientUdp_IUnlock(struct mmClientUdp* p);
// o locker
MM_EXPORT_NET void mmClientUdp_OLock(struct mmClientUdp* p);
MM_EXPORT_NET void mmClientUdp_OUnlock(struct mmClientUdp* p);

// assign native address but not connect.
MM_EXPORT_NET void mmClientUdp_SetNative(struct mmClientUdp* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmClientUdp_SetRemote(struct mmClientUdp* p, const char* node, mmUShort_t port);

MM_EXPORT_NET void mmClientUdp_SetQDefaultCallback(struct mmClientUdp* p, struct mmClientUdpCallback* client_udp_q_callback);
MM_EXPORT_NET void mmClientUdp_SetNDefaultCallback(struct mmClientUdp* p, struct mmClientUdpCallback* client_udp_n_callback);
// assign queue callback.at main thread.
MM_EXPORT_NET void mmClientUdp_SetQHandle(struct mmClientUdp* p, mmUInt32_t id, mmClientUdpHandleFunc callback);
// assign net callback.not at main thread.
MM_EXPORT_NET void mmClientUdp_SetNHandle(struct mmClientUdp* p, mmUInt32_t id, mmClientUdpHandleFunc callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientUdp_SetUdpCallback(struct mmClientUdp* p, struct mmUdpCallback* udp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientUdp_SetEventUdpAllocator(struct mmClientUdp* p, struct mmNetUdpEventUdpAllocator* event_udp_allocator);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmClientUdp_SetCryptoCallback(struct mmClientUdp* p, struct mmCryptoCallback* crypto_callback);
// assign context handle.
MM_EXPORT_NET void mmClientUdp_SetContext(struct mmClientUdp* p, void* u);

// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmClientUdp_SetAddrContext(struct mmClientUdp* p, void* u);
MM_EXPORT_NET void* mmClientUdp_GetAddrContext(struct mmClientUdp* p);

MM_EXPORT_NET void mmClientUdp_ClearHandleRbtree(struct mmClientUdp* p);

// client_udp crypto encrypt buffer by udp.
MM_EXPORT_NET void mmClientUdp_CryptoEncrypt(struct mmClientUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length);
// client_udp crypto decrypt buffer by udp.
MM_EXPORT_NET void mmClientUdp_CryptoDecrypt(struct mmClientUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length);

MM_EXPORT_NET void mmClientUdp_SetFlushHandleNearby(struct mmClientUdp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientUdp_SetFlushBrokenNearby(struct mmClientUdp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientUdp_SetStateHandleNearby(struct mmClientUdp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientUdp_SetStateBrokenNearby(struct mmClientUdp* p, mmMSec_t milliseconds);
MM_EXPORT_NET void mmClientUdp_SetStateCheckFlag(struct mmClientUdp* p, mmUInt8_t flag);
MM_EXPORT_NET void mmClientUdp_SetStateQueueFlag(struct mmClientUdp* p, mmUInt8_t flag);

MM_EXPORT_NET void mmClientUdp_SetMaxPoperNumber(struct mmClientUdp* p, size_t max_pop);

// fopen socket.
MM_EXPORT_NET void mmClientUdp_FopenSocket(struct mmClientUdp* p);
// bind.
MM_EXPORT_NET void mmClientUdp_Bind(struct mmClientUdp* p);
// close socket.
MM_EXPORT_NET void mmClientUdp_CloseSocket(struct mmClientUdp* p);
// shutdown socket MM_BOTH_SHUTDOWN.
MM_EXPORT_NET void mmClientUdp_ShutdownSocket(struct mmClientUdp* p);

MM_EXPORT_NET int mmClientUdp_MulcastJoin(struct mmClientUdp* p, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_NET int mmClientUdp_MulcastDrop(struct mmClientUdp* p, const char* mr_multiaddr, const char* mr_interface);

// check connect and reconnect if disconnect.not thread safe.
MM_EXPORT_NET void mmClientUdp_Check(struct mmClientUdp* p);
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmClientUdp_FinallyState(struct mmClientUdp* p);

// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmClientUdp_AttachSocket(struct mmClientUdp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmClientUdp_DetachSocket(struct mmClientUdp* p);
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_NET void mmClientUdp_ThreadHandleRecv(struct mmClientUdp* p);
// net thread trigger.
MM_EXPORT_NET void mmClientUdp_ThreadHandleSend(struct mmClientUdp* p);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_NET void mmClientUdp_PushRecv(struct mmClientUdp* p, void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_NET void mmClientUdp_PushSend(struct mmClientUdp* p, void* obj, struct mmPacket* pack, struct mmSockaddr* remote);

// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientUdp_BufferSend(struct mmClientUdp* p, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote);
// handle send data by flush send buffer.
// 0 <  rt,means rt buffer is send,we must gbump rt size.
// 0 == rt,means the send buffer can be full.
// 0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmClientUdp_FlushSend(struct mmClientUdp* p, struct mmSockaddr* remote);
// send buffer, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_NET void mmClientUdp_SendBuffer(struct mmClientUdp* p, struct mmPacketHead* packet_head, size_t hlength, struct mmByteBuffer* buffer, struct mmSockaddr* remote);

// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_NET void mmClientUdp_FlushSignal(struct mmClientUdp* p);
// use for trigger the state check thread signal.
// is useful for main thread asynchronous state check.
MM_EXPORT_NET void mmClientUdp_StateSignal(struct mmClientUdp* p);

// start wait thread.
MM_EXPORT_NET void mmClientUdp_Start(struct mmClientUdp* p);
// interrupt wait thread.
MM_EXPORT_NET void mmClientUdp_Interrupt(struct mmClientUdp* p);
// shutdown wait thread.
MM_EXPORT_NET void mmClientUdp_Shutdown(struct mmClientUdp* p);
// join wait thread.
MM_EXPORT_NET void mmClientUdp_Join(struct mmClientUdp* p);

MM_EXPORT_NET void mmClientUdp_UdpHandlePacketCallback(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET int mmClientUdp_UdpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote);
MM_EXPORT_NET int mmClientUdp_UdpBrokenCallback(void* obj);

#include "core/mmSuffix.h"

#endif//__mmClientUdp_h__
