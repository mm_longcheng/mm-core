/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTcp.h"
#include "core/mmLogger.h"
#include "core/mmErrno.h"
#include "core/mmAlloc.h"
#include "core/mmSpinlock.h"

static int __static_mmTcp_HandleDefault(void* obj, mmUInt8_t* b, size_t o, size_t l)
{
    return 0;
}
static int __static_mmTcp_BrokenDefault(void* obj)
{
    return 0;
}

MM_EXPORT_NET void mmTcpCallback_Init(struct mmTcpCallback* p)
{
    p->Handle = &__static_mmTcp_HandleDefault;
    p->Broken = &__static_mmTcp_BrokenDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmTcpCallback_Destroy(struct mmTcpCallback* p)
{
    p->Handle = &__static_mmTcp_HandleDefault;
    p->Broken = &__static_mmTcp_BrokenDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmTcpCallback_Reset(struct mmTcpCallback* p)
{
    p->Handle = &__static_mmTcp_HandleDefault;
    p->Broken = &__static_mmTcp_BrokenDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmTcp_Init(struct mmTcp* p)
{
    mmSocket_Init(&p->socket);
    mmStreambuf_Init(&p->buff_recv);
    mmStreambuf_Init(&p->buff_send);
    mmTcpCallback_Init(&p->callback);
    mmSpinlock_Init(&p->locker_i, NULL);
    mmSpinlock_Init(&p->locker_o, NULL);
    p->unique_id = 0;
    p->u = NULL;
}
MM_EXPORT_NET void mmTcp_Destroy(struct mmTcp* p)
{
    mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    mmSocket_Destroy(&p->socket);
    mmStreambuf_Destroy(&p->buff_recv);
    mmStreambuf_Destroy(&p->buff_send);
    mmTcpCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->locker_i);
    mmSpinlock_Destroy(&p->locker_o);
    p->unique_id = 0;
    p->u = NULL;
}

MM_EXPORT_NET void mmTcp_Lock(struct mmTcp* p)
{
    mmSocket_Lock(&p->socket);
    mmSpinlock_Lock(&p->locker_i);
    mmSpinlock_Lock(&p->locker_o);
}
MM_EXPORT_NET void mmTcp_Unlock(struct mmTcp* p)
{
    mmSpinlock_Unlock(&p->locker_o);
    mmSpinlock_Unlock(&p->locker_i);
    mmSocket_Unlock(&p->socket);
}
// s locker. 
MM_EXPORT_NET void mmTcp_SLock(struct mmTcp* p)
{
    mmSocket_Lock(&p->socket);
}
MM_EXPORT_NET void mmTcp_SUnlock(struct mmTcp* p)
{
    mmSocket_Unlock(&p->socket);
}
// i locker. 
MM_EXPORT_NET void mmTcp_ILock(struct mmTcp* p)
{
    mmSpinlock_Lock(&p->locker_i);
}
MM_EXPORT_NET void mmTcp_IUnlock(struct mmTcp* p)
{
    mmSpinlock_Unlock(&p->locker_i);
}
// o locker. 
MM_EXPORT_NET void mmTcp_OLock(struct mmTcp* p)
{
    mmSpinlock_Lock(&p->locker_o);
}
MM_EXPORT_NET void mmTcp_OUnlock(struct mmTcp* p)
{
    mmSpinlock_Unlock(&p->locker_o);
}

MM_EXPORT_NET void mmTcp_Reset(struct mmTcp* p)
{
    mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    mmSocket_Reset(&p->socket);
    mmStreambuf_Reset(&p->buff_recv);
    mmStreambuf_Reset(&p->buff_send);
    mmTcpCallback_Reset(&p->callback);
    // spinlock not reset.
    p->unique_id = 0;
    p->u = NULL;
}
// tcp streambuf recv send reset.
MM_EXPORT_NET void mmTcp_ResetStreambuf(struct mmTcp* p)
{
    mmStreambuf_Reset(&p->buff_recv);
    mmStreambuf_Reset(&p->buff_send);
}

// assign addr native by ip port.
MM_EXPORT_NET void mmTcp_SetNative(struct mmTcp* p, const char* node, mmUShort_t port)
{
    mmSocket_SetNative(&p->socket, node, port);
}
// assign addr remote by ip port.
MM_EXPORT_NET void mmTcp_SetRemote(struct mmTcp* p, const char* node, mmUShort_t port)
{
    mmSocket_SetRemote(&p->socket, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;
// after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmTcp_SetNativeStorage(struct mmTcp* p, struct mmSockaddr* ss_native)
{
    mmSocket_SetNativeStorage(&p->socket, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;
// after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmTcp_SetRemoteStorage(struct mmTcp* p, struct mmSockaddr* ss_remote)
{
    mmSocket_SetRemoteStorage(&p->socket, ss_remote);
}

MM_EXPORT_NET void mmTcp_SetCallback(struct mmTcp* p, struct mmTcpCallback* callback)
{
    assert(NULL != callback && "callback is a null.");
    p->callback = (*callback);
}

MM_EXPORT_NET void mmTcp_SetUniqueId(struct mmTcp* p, mmUInt64_t unique_id)
{
    p->unique_id = unique_id;
}
MM_EXPORT_NET mmUInt64_t mmTcp_GetUniqueId(struct mmTcp* p)
{
    return p->unique_id;
}
// context for tcp will use the mmSocket. u user data.
MM_EXPORT_NET void mmTcp_SetContext(struct mmTcp* p, void* u)
{
    p->u = u;
}
MM_EXPORT_NET void* mmTcp_GetContext(struct mmTcp* p)
{
    return p->u;
}

// fopen socket.
MM_EXPORT_NET void mmTcp_FopenSocket(struct mmTcp* p)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmSocket_FopenSocket(&p->socket, p->socket.ss_remote.addr.sa_family, SOCK_STREAM, 0);
    //
    mmSocket_ToString(&p->socket, link_name);
    //
    if (MM_INVALID_SOCKET != p->socket.socket)
    {
        mmLogger_LogI(gLogger, "%s %d %s success.", __FUNCTION__, __LINE__, link_name);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d %s failure.", __FUNCTION__, __LINE__, link_name);
    }
}
// close socket.
MM_EXPORT_NET void mmTcp_CloseSocket(struct mmTcp* p)
{
    mmSocket_CloseSocket(&p->socket);
}
// shutdown socket.
MM_EXPORT_NET void mmTcp_ShutdownSocket(struct mmTcp* p, int opcode)
{
    mmSocket_ShutdownSocket(&p->socket, opcode);
}

// handle recv for buffer pool and pool max size.
MM_EXPORT_NET void mmTcp_HandleRecv(struct mmTcp* p)
{
    // mmStreambuf page size MM_STREAMBUF_PAGE_SIZE is equal MM_SOCKET_PAGE_SIZE.
    // reduce the mmStreambuf frequently change max_size, here we use 3 * MM_SOCKET_PAGE_SIZE / 4 for max_length for recv.
    size_t max_length = 3 * MM_SOCKET_PAGE_SIZE / 4;
    int real_len = 0;
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    assert(p->callback.Handle && p->callback.Broken && "callback is invalid.");
    //
    do
    {
        // if the idle put size is lack, we try remove the get buffer.
        mmStreambuf_AlignedMemory(&p->buff_recv, max_length);
        // recv.
        real_len = mmSocket_Recv(&p->socket, p->buff_recv.buff, p->buff_recv.pptr, max_length, 0);
        if (-1 == real_len)
        {
            mmErr_t errcode = mmErrno;

            switch (errcode)
            {
            case MM_EAGAIN:
            {
                // MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
                // this error is not serious.
                break;
            }
            case MM_ECONNRESET:
            case MM_ECONNABORTED:
            case MM_ENOTSOCK:
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmSocket_ToString(&p->socket, link_name);
                // An existing connection was forcibly closed by the remote host.
                mmLogger_LogI(gLogger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                (*(p->callback.Broken))(p);
                break;
            }
            case MM_EBADF:
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                // blocking operation was interrupted.
                // this error is not serious.
                mmLogger_LogI(gLogger, "%s %d %p error occur.", __FUNCTION__, __LINE__, p);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                break;
            }
            default:
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmSocket_ToString(&p->socket, link_name);
                // error occur.
                mmLogger_LogI(gLogger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                (*(p->callback.Broken))(p);
                break;
            }
            }

            // No matter what the result, we break this while loop.
            break;
        }
        else if (0 == real_len)
        {
            // if this recv operate 0 == real_len, means the recv buffer is full or target socket is closed.
            // so we must use poll for checking the io event make sure read event is fire.
            // at here send length is 0 will fire the broken event.
            mmErr_t errcode = mmErrno;
            struct mmLogger* gLogger = mmLogger_Instance();
            mmSocket_ToString(&p->socket, link_name);
            // the other side close the socket.
            mmLogger_LogI(gLogger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
            mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
            (*(p->callback.Broken))(p);
            break;
        }
        // pbump the position.
        // note: the real buffer is 
        //       buffer = p->buff_recv.buff
        //       offset = (mmUInt32_t)p->buff_recv.pptr - real_len
        //       length = real_len
        mmStreambuf_Pbump(&p->buff_recv, real_len);
        //
        if (real_len != max_length)
        {
            mmTcp_BufferRecv(p, p->buff_recv.buff, (size_t)p->buff_recv.pptr - real_len, real_len);
            break;
        }
        else
        {
            mmTcp_BufferRecv(p, p->buff_recv.buff, (size_t)p->buff_recv.pptr - real_len, real_len);
        }
    } while (1);
}
// handle send for buffer pool and pool max size.
MM_EXPORT_NET void mmTcp_HandleSend(struct mmTcp* p)
{
    // do nothing.
}

// handle recv for buffer pool and pool max size.
MM_EXPORT_NET int mmTcp_BufferRecv(struct mmTcp* p, mmUInt8_t* b, size_t o, size_t l)
{
    int code = 0;
    assert(p->callback.Handle && "callback.Handle is invalid.");
    code = (*(p->callback.Handle))(p, b, o, l);
    if (0 != code)
    {
        // invalid tcp message buffer decode.
        // shutdown socket immediately.
        mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    }
    return code;
}
// handle send for buffer pool and pool max size.
MM_EXPORT_NET int mmTcp_BufferSend(struct mmTcp* p, mmUInt8_t* b, size_t o, size_t l)
{
    int real_len = 0;
    int send_len = 0;
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    // assert(p->callback.handle&&p->callback.broken && "callback.handle or callback.broken is invalid.");
    do
    {
        if (0 == l)
        {
            // nothing for send break immediately.
            break;
        }
        real_len = mmSocket_Send(&p->socket, b, o, l, 0);
        if (-1 == real_len)
        {
            mmErr_t errcode = mmErrno;
            if (MM_EAGAIN == errcode ||
                MM_ENOBUFS == errcode)
            {
                // MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
                // this error is not serious.
                // MM_ENOBUFS if send buffer is full. break.
                break;
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                //
                mmSocket_ToString(&p->socket, link_name);
                mmLogger_LogI(gLogger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                // (*(p->callback.broken))(p);
                // at send process, we can not trigger the broken call back.
                mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
                // error is occur, we assign the return code to -1 and break it immediately.
                send_len = -1;
                break;
            }
        }
        else if (real_len == l)
        {
            // complete
            send_len += real_len;
            break;
        }
        else if (0 == real_len)
        {
            // if this send operate 0 == real_len, means the send buffer is full or target socket is closed.
            // so we must use poll for checking the io event make sure read event is fire.
            // at here send length is 0 will shutdown this socket.
            mmErr_t errcode = mmErrno;
            struct mmLogger* gLogger = mmLogger_Instance();
            mmSocket_ToString(&p->socket, link_name);
            // conn disconnect
            mmLogger_LogI(gLogger, "%s %d %s broken.", __FUNCTION__, __LINE__, link_name);
            mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
            // (*(p->callback.broken))(p);
            // at send process, we can not trigger the broken call back.
            mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
            // error is occur, we assign the return code to -1 and break it immediately.
            send_len = -1;
            break;
        }
        else
        {
            // need cycle send.
            o += real_len;
            l -= real_len;
            send_len += real_len;
        }
    } while (1);
    return send_len;
}
// handle send data by flush send buffer.
MM_EXPORT_NET int mmTcp_FlushSend(struct mmTcp* p)
{
    int send_sz = 0;
    size_t sz = 0;
    int rt = 0;
    do
    {
        sz = mmStreambuf_Size(&p->buff_send);
        if (0 == sz)
        {
            // streambuf size is empty, break this flush send.
            break;
        }
        // sometimes the streambuf size is so large, we need send multiple times.
        sz = sz < MM_SOCKET_PAGE_SIZE ? sz : MM_SOCKET_PAGE_SIZE;
        rt = mmTcp_BufferSend(p, p->buff_send.buff, p->buff_send.gptr, (mmUInt32_t)sz);
        if (0 < rt)
        {
            // 0 <  rt, means rt buffer is send, we must gbump rt size.
            // 0 == rt, means the send buffer can be full.
            // 0 >  rt, means the send process can be failure.
            mmStreambuf_Gbump(&p->buff_send, rt);
            send_sz += rt;
        }
        else
        {
            // an error occurred during the sending process.
            break;
        }
    } while (1);
    return 0 < rt ? send_sz : rt;
}
