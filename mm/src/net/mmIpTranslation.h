/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIpTranslation_h__
#define __mmIpTranslation_h__

#include "core/mmTypes.h"

#include "net/mmSockaddr.h"

#include "core/mmPrefix.h"

/* @brief ipv4 to int address translation
*    @param node      string ipv4.
*    @param ipv4_addr uint32 address
*    @return 0 success other failure
*/
MM_EXPORT_NET int mmIpv4ToI(const char* node, mmUInt32_t* ipv4_addr);

/* @brief ipv6 to int address translation
*    @param node      string ipv6.
*    @param ipv6_addr uint32[4] address
*    @param length    ipv6 prefix length:
*      - 64 for EUI-64 addresses
*      -128 for non-EUI-64 addresses
*    @return 0 success other failure
*/
MM_EXPORT_NET int mmIpv6ToI(const char* node, mmUInt32_t ipv6_addr[4], int length);

/* @brief int to ipv4 address translation
*    @param ipv4_addr uint32 address
*    @param node      string ipv4.
*    @return 0 success other failure
*  @remarks
*    Ipv4 address 32 bits, array ip dimension defaults to uint32
*    The output format is: A.B.C.D.
*/
MM_EXPORT_NET int mmIToIpv4(mmUInt32_t ipv4_addr, char node[MM_ADDR_NAME_LENGTH]);

/* @brief int to ipv6 address translation
*    @param ipv6_addr uint32[4] address
*    @param node      string ipv6.
*    @return 0 success other failure
*  @remarks
*    Ipv6 address 128 bits, array ip dimension defaults to 4
*    The output format is: A:B:C:D:E:F:G:H.
*/
MM_EXPORT_NET int mmIToIpv6(mmUInt32_t ipv6_addr[4], char node[MM_ADDR_NAME_LENGTH]);

/* @brief ipv4 add
*    @param ipv4_addr uint32 address
*    @param d         uint32 add value.
*/
MM_EXPORT_NET void mmIIpv4Add(mmUInt32_t* ipv4_addr, mmUInt32_t d);

/* @brief ipv4 sub
*    @param ipv4_addr uint32 address
*    @param d         uint32 sub value.
*/
MM_EXPORT_NET void mmIIpv4Sub(mmUInt32_t* ipv4_addr, mmUInt32_t d);

/* @brief ipv6 add
*    @param ipv6_addr uint32[4] address
*    @param d         uint32 add value.
*/
MM_EXPORT_NET void mmIIpv6Add(mmUInt32_t ipv6_addr[4], mmUInt32_t d);

/* @brief ipv6 sub
*    @param ipv6_addr uint32[4] address
*    @param d         uint32 sub value.
*/
MM_EXPORT_NET void mmIIpv6Sub(mmUInt32_t ipv6_addr[4], mmUInt32_t d);

/* @brief string ip add
*    @param node_i string address
*    @param d      uint32 add value.
*    @param node_o string address
*/
MM_EXPORT_NET void mmIpAdd(const char* node_i, mmUInt32_t d, char node_o[MM_ADDR_NAME_LENGTH]);

/* @brief string ip sub
*    @param node_i string address
*    @param d      uint32 sub value.
*    @param node_o string address
*/
MM_EXPORT_NET void mmIpSub(const char* node_i, mmUInt32_t d, char node_o[MM_ADDR_NAME_LENGTH]);

#include "core/mmSuffix.h"

#endif//__mmIpTranslation_h__

