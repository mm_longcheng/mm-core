/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNetTcp.h"
#include "net/mmStreambufPacket.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmNetTcp_SelectorHandleRecv(void* obj, void* u);
static void __static_mmNetTcp_SelectorHandleSend(void* obj, void* u);

static void __static_mmNetTcp_HandleDefault(void* obj, void* u, struct mmPacket* pack)
{

}
static void __static_mmNetTcp_BrokenDefault(void* obj)
{

}
static void __static_mmNetTcp_NreadyDefault(void* obj)
{

}
static void __static_mmNetTcp_FinishDefault(void* obj)
{

}
MM_EXPORT_NET void mmNetTcpCallback_Init(struct mmNetTcpCallback* p)
{
    p->Handle = &__static_mmNetTcp_HandleDefault;
    p->Broken = &__static_mmNetTcp_BrokenDefault;
    p->Nready = &__static_mmNetTcp_NreadyDefault;
    p->Finish = &__static_mmNetTcp_FinishDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmNetTcpCallback_Destroy(struct mmNetTcpCallback* p)
{
    p->Handle = &__static_mmNetTcp_HandleDefault;
    p->Broken = &__static_mmNetTcp_BrokenDefault;
    p->Nready = &__static_mmNetTcp_NreadyDefault;
    p->Finish = &__static_mmNetTcp_FinishDefault;
    p->obj = NULL;
}

static void __static_mmNetTcp_EventTcpProduce(void* p, struct mmTcp* tcp)
{

}
static void __static_mmNetTcp_EventTcpRecycle(void* p, struct mmTcp* tcp)
{

}
MM_EXPORT_NET void mmNetTcpEventTcpAllocator_Init(struct mmNetTcpEventTcpAllocator* p)
{
    p->EventTcpProduce = &__static_mmNetTcp_EventTcpProduce;
    p->EventTcpRecycle = &__static_mmNetTcp_EventTcpRecycle;
    p->obj = NULL;
}
MM_EXPORT_NET void mmNetTcpEventTcpAllocator_Destroy(struct mmNetTcpEventTcpAllocator* p)
{
    p->EventTcpProduce = &__static_mmNetTcp_EventTcpProduce;
    p->EventTcpRecycle = &__static_mmNetTcp_EventTcpRecycle;
    p->obj = NULL;
}

MM_EXPORT_NET void mmNetTcp_Init(struct mmNetTcp* p)
{
    struct mmTcpCallback tcp_callback;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    struct mmSelectorCallback selector_callback;

    mmTcp_Init(&p->tcp);
    mmSelector_Init(&p->selector);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmTcpCallback_Init(&p->tcp_callback);
    mmNetTcpCallback_Init(&p->callback);
    mmNetTcpEventTcpAllocator_Init(&p->event_tcp_allocator);
    mmCryptoCallback_Init(&p->crypto_callback);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->nonblock_timeout = MM_NETTCP_NONBLOCK_TIMEOUT;
    p->tcp_state = MM_NETTCP_STATE_CLOSED;
    p->trytimes = MM_NETTCP_TRYTIME;
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    p->tcp_callback.Handle = &mmNetTcp_TcpHandleCallback;
    p->tcp_callback.Broken = &mmNetTcp_TcpBrokenCallback;
    p->tcp_callback.obj = p;

    tcp_callback.Handle = p->tcp_callback.Handle;
    tcp_callback.Broken = p->tcp_callback.Broken;
    tcp_callback.obj = p;
    mmTcp_SetCallback(&p->tcp, &tcp_callback);

    selector_callback.HandleRecv = &__static_mmNetTcp_SelectorHandleRecv;
    selector_callback.HandleSend = &__static_mmNetTcp_SelectorHandleSend;
    selector_callback.obj = p;
    mmSelector_SetCallback(&p->selector, &selector_callback);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_NET void mmNetTcp_Destroy(struct mmNetTcp* p)
{
    mmNetTcp_DetachSocket(p);
    mmNetTcp_ClearHandleRbtree(p);
    //
    mmTcp_Destroy(&p->tcp);
    mmSelector_Destroy(&p->selector);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmTcpCallback_Destroy(&p->tcp_callback);
    mmNetTcpCallback_Destroy(&p->callback);
    mmNetTcpEventTcpAllocator_Destroy(&p->event_tcp_allocator);
    mmCryptoCallback_Destroy(&p->crypto_callback);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->nonblock_timeout = 0;
    p->tcp_state = MM_NETTCP_STATE_CLOSED;
    p->trytimes = 0;
    p->state = MM_TS_CLOSED;
    p->u = NULL;
}

// streambuf reset.
MM_EXPORT_NET void mmNetTcp_ResetStreambuf(struct mmNetTcp* p)
{
    mmTcp_ResetStreambuf(&p->tcp);
}

MM_EXPORT_NET void mmNetTcp_Lock(struct mmNetTcp* p)
{
    mmTcp_Lock(&p->tcp);
    mmSelector_Lock(&p->selector);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmNetTcp_Unlock(struct mmNetTcp* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmSelector_Unlock(&p->selector);
    mmTcp_Unlock(&p->tcp);
}
// s locker. 
MM_EXPORT_NET void mmNetTcp_SLock(struct mmNetTcp* p)
{
    mmTcp_SLock(&p->tcp);
}
MM_EXPORT_NET void mmNetTcp_SUnlock(struct mmNetTcp* p)
{
    mmTcp_SUnlock(&p->tcp);
}
// i locker. 
MM_EXPORT_NET void mmNetTcp_ILock(struct mmNetTcp* p)
{
    mmTcp_ILock(&p->tcp);
}
MM_EXPORT_NET void mmNetTcp_IUnlock(struct mmNetTcp* p)
{
    mmTcp_IUnlock(&p->tcp);
}
// o locker. 
MM_EXPORT_NET void mmNetTcp_OLock(struct mmNetTcp* p)
{
    mmTcp_OLock(&p->tcp);
}
MM_EXPORT_NET void mmNetTcp_OUnlock(struct mmNetTcp* p)
{
    mmTcp_OUnlock(&p->tcp);
}

// assign native address but not connect.
MM_EXPORT_NET void mmNetTcp_SetNative(struct mmNetTcp* p, const char* node, mmUShort_t port)
{
    mmTcp_SetNative(&p->tcp, node, port);
}
// assign remote address but not connect.
MM_EXPORT_NET void mmNetTcp_SetRemote(struct mmNetTcp* p, const char* node, mmUShort_t port)
{
    mmTcp_SetRemote(&p->tcp, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;
// after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmNetTcp_SetNativeStorage(struct mmNetTcp* p, struct mmSockaddr* ss_native)
{
    mmTcp_SetNativeStorage(&p->tcp, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;
// after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmNetTcp_SetRemoteStorage(struct mmNetTcp* p, struct mmSockaddr* ss_remote)
{
    mmTcp_SetRemoteStorage(&p->tcp, ss_remote);
}

MM_EXPORT_NET void mmNetTcp_SetHandle(struct mmNetTcp* p, mmUInt32_t id, mmNetTcpHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmNetTcp_SetNetTcpCallback(struct mmNetTcp* p, struct mmNetTcpCallback* net_tcp_callback)
{
    struct mmTcpCallback _tcp_callback;
    assert(NULL != net_tcp_callback && "you can not assign null net_tcp_callback.");
    p->callback = *net_tcp_callback;

    _tcp_callback.Handle = p->tcp_callback.Handle;
    _tcp_callback.Broken = p->tcp_callback.Broken;
    _tcp_callback.obj = p;
    mmTcp_SetCallback(&p->tcp, &_tcp_callback);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetTcp_SetTcpCallback(struct mmNetTcp* p, struct mmTcpCallback* tcp_callback)
{
    assert(NULL != tcp_callback && "you can not assign null tcp_callback.");
    p->tcp_callback = *tcp_callback;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetTcp_SetEventTcpAllocator(struct mmNetTcp* p, struct mmNetTcpEventTcpAllocator* event_tcp_allocator)
{
    assert(NULL != event_tcp_allocator && "you can not assign null event_tcp_allocator.");
    p->event_tcp_allocator = *event_tcp_allocator;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetTcp_SetCryptoCallback(struct mmNetTcp* p, struct mmCryptoCallback* crypto_callback)
{
    assert(NULL != crypto_callback && "you can not assign null crypto_callback.");
    p->crypto_callback = *crypto_callback;
}
// assign context handle.
MM_EXPORT_NET void mmNetTcp_SetContext(struct mmNetTcp* p, void* u)
{
    p->u = u;
}
// assign nonblock_timeout.
MM_EXPORT_NET void mmNetTcp_SetNonblockTimeout(struct mmNetTcp* p, mmMSec_t nonblock_timeout)
{
    p->nonblock_timeout = nonblock_timeout;
}
// assign trytimes.
MM_EXPORT_NET void mmNetTcp_SetTrytimes(struct mmNetTcp* p, mmUInt32_t trytimes)
{
    p->trytimes = trytimes;
}

// context for tcp will use the mm_addr. u attribute. such as crypto context.
MM_EXPORT_NET void mmNetTcp_SetAddrContext(struct mmNetTcp* p, void* u)
{
    mmTcp_SetContext(&p->tcp, u);
}
MM_EXPORT_NET void* mmNetTcp_GetAddrContext(struct mmNetTcp* p)
{
    return mmTcp_GetContext(&p->tcp);
}

MM_EXPORT_NET void mmNetTcp_ClearHandleRbtree(struct mmNetTcp* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

// net_tcp crypto encrypt buffer by tcp.
MM_EXPORT_NET void mmNetTcp_CryptoEncrypt(struct mmNetTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Encrypt))(p, tcp, buffer, offset, buffer, offset, length);
}
// net_tcp crypto decrypt buffer by tcp.
MM_EXPORT_NET void mmNetTcp_CryptoDecrypt(struct mmNetTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Decrypt))(p, tcp, buffer, offset, buffer, offset, length);
}

// fopen socket.
MM_EXPORT_NET void mmNetTcp_FopenSocket(struct mmNetTcp* p)
{
    mmTcp_FopenSocket(&p->tcp);
}
// synchronize connect. do not call this as we already open connect check thread.
MM_EXPORT_NET void mmNetTcp_Connect(struct mmNetTcp* p)
{
    char link_name[MM_LINK_NAME_LENGTH];
    struct mmSocket* socket = &p->tcp.socket;
    struct mmLogger* gLogger = mmLogger_Instance();
    struct timeval tv;
    fd_set rset;
    fd_set wset;
    //
    mmSocket_ToString(socket, link_name);
    //
    tv.tv_sec = (mmTimevalSSec_t)(p->nonblock_timeout / 1000);
    tv.tv_usec = (mmTimevalUSec_t)((p->nonblock_timeout % 1000) * 1000);
    //
    if (MM_INVALID_SOCKET != socket->socket)
    {
        mmUInt32_t _try_times = 0;
        int rt = -1;
        p->tcp_state = MM_NETTCP_STATE_MOTION;
        // set socket to not block.
        mmSocket_SetBlock(socket, MM_NONBLOCK);
        do
        {
            _try_times++;
            FD_ZERO(&rset);
            FD_ZERO(&wset);
            FD_SET(socket->socket, &rset);
            FD_SET(socket->socket, &wset);
            rt = mmSocket_Connect(socket);
            if (0 == rt)
            {
                // connect immediately.
                break;
            }
            rt = select((int)(socket->socket + 1), &rset, &wset, NULL, &tv);
            if (0 >= rt)
            {
                mmLogger_LogE(gLogger, "%s %d connect %s failure. try times: %d", __FUNCTION__, __LINE__, link_name, _try_times);
                mmMSleep(MM_NETTCP_TRY_MSLEEP_TIME);
                rt = -1;
            }
            else
            {
                // select return > 0 means success.
                rt = 0;
            }
            if (MM_TS_FINISH == p->state)
            {
                break;
            }
        } while (0 != rt && _try_times < p->trytimes);
        if (0 != rt)
        {
            mmErr_t errcode = mmErrno;
            mmLogger_LogE(gLogger, "%s %d connect %s failure. try times: %d", __FUNCTION__, __LINE__, link_name, _try_times);
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            mmNetTcp_ApplyBroken(p);
            mmSocket_CloseSocket(socket);
        }
        else
        {
            mmNetTcp_ApplyFinish(p);
            mmLogger_LogI(gLogger, "%s %d %s success. try times: %d", __FUNCTION__, __LINE__, link_name, _try_times);
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d connect target fd is invalid.", __FUNCTION__, __LINE__);
    }
}
// close socket.
MM_EXPORT_NET void mmNetTcp_CloseSocket(struct mmNetTcp* p)
{
    mmSocket_CloseSocket(&p->tcp.socket);
}
// shutdown socket.
MM_EXPORT_NET void mmNetTcp_ShutdownSocket(struct mmNetTcp* p, int opcode)
{
    mmSocket_ShutdownSocket(&p->tcp.socket, opcode);
}
// set socket block.
MM_EXPORT_NET void mmNetTcp_SetBlock(struct mmNetTcp* p, int block)
{
    mmSocket_SetBlock(&p->tcp.socket, block);
}
// check connect and reconnect if disconnect.
MM_EXPORT_NET void mmNetTcp_Check(struct mmNetTcp* p)
{
    if (0 != mmNetTcp_FinallyState(p))
    {
        // we first disconnect this old connect.
        mmNetTcp_DetachSocket(p);
        // fopen a socket.
        mmNetTcp_FopenSocket(p);
        // net tcp connect.
        mmNetTcp_Connect(p);
    }
}
// get state finally. return 0 for all regular. not thread safe.
MM_EXPORT_NET int mmNetTcp_FinallyState(struct mmNetTcp* p)
{
    return (MM_INVALID_SOCKET != p->tcp.socket.socket && MM_NETTCP_STATE_FINISH == p->tcp_state) ? 0 : -1;
}
// synchronize attach to socket. not thread safe.
MM_EXPORT_NET void mmNetTcp_AttachSocket(struct mmNetTcp* p)
{
    mmNetTcp_Connect(p);
}
// synchronize detach to socket. not thread safe.
MM_EXPORT_NET void mmNetTcp_DetachSocket(struct mmNetTcp* p)
{
    if (MM_INVALID_SOCKET != p->tcp.socket.socket)
    {
        mmNetTcp_ApplyBroken(p);
        // set block state.
        mmNetTcp_SetBlock(p, MM_BLOCKING);
        // shutdown socket first.
        mmNetTcp_ShutdownSocket(p, MM_BOTH_SHUTDOWN);
        // close socket.
        mmNetTcp_CloseSocket(p);
    }
}

MM_EXPORT_NET void mmNetTcp_ApplyBroken(struct mmNetTcp* p)
{
    p->tcp_state = MM_NETTCP_STATE_CLOSED;
    mmTcp_ResetStreambuf(&p->tcp);
    (*p->callback.Broken)(&p->tcp);
    mmSelector_FdRmv(&p->selector, p->tcp.socket.socket, &p->tcp);
}
MM_EXPORT_NET void mmNetTcp_ApplyFinish(struct mmNetTcp* p)
{
    p->tcp_state = MM_NETTCP_STATE_FINISH;
    mmTcp_ResetStreambuf(&p->tcp);
    mmSelector_FdAdd(&p->selector, p->tcp.socket.socket, &p->tcp);
    (*p->callback.Finish)(&p->tcp);
}

// handle send data from buffer and buffer length. > 0 is send size success -1 failure.
// note this function do not trigger the `callback.broken` it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger `callback.broken`, make sure not dead lock and invalid free pinter appear.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
MM_EXPORT_NET int mmNetTcp_BufferSend(struct mmNetTcp* p, mmUInt8_t* buffer, size_t offset, size_t length)
{
    int rt = -1;
    if (MM_NETTCP_STATE_FINISH == p->tcp_state && MM_INVALID_SOCKET != p->tcp.socket.socket)
    {
        rt = mmTcp_BufferSend(&p->tcp, buffer, offset, length);
        // update the tcp state,because the mmTcp_BufferSend can not fire broken event immediately.
        p->tcp_state = -1 == rt ? MM_NETTCP_STATE_BROKEN : p->tcp_state;
    }
    else
    {
        // net tcp nready.fire event.
        (*p->callback.Nready)(&p->tcp);
    }
    return rt;
}
// handle send data by flush send buffer.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
MM_EXPORT_NET int mmNetTcp_FlushSend(struct mmNetTcp* p)
{
    int rt = -1;
    if (MM_NETTCP_STATE_FINISH == p->tcp_state && MM_INVALID_SOCKET != p->tcp.socket.socket)
    {
        size_t sz = 0;
        do
        {
            if (MM_TS_FINISH == p->state ||
                MM_NETTCP_STATE_FINISH != p->tcp_state ||
                MM_INVALID_SOCKET == p->tcp.socket.socket)
            {
                // the net tcp state can not send any thing.
                break;
            }
            rt = mmTcp_FlushSend(&p->tcp);
            // update the tcp state, because the mmTcp_BufferSend can not fire broken event immediately.
            p->tcp_state = -1 == rt ? MM_NETTCP_STATE_BROKEN : p->tcp_state;
            if (0 != rt)
            {
                // -1 == rt socket send failure.
                //  0 == rt socket send socket is MM_EAGAIN or buffer is full.
                //  0  < rt socket send success.
                break;
            }
            sz = mmStreambuf_Size(&p->tcp.buff_send);
            if (0 == sz)
            {
                // nothing buffer need send.
                break;
            }
            // we need cycle send. after wait for a while.
            mmMSleep(MM_NETTCP_BLOCK_TIME);
        } while (1);
    }
    else
    {
        // net tcp nready. fire event.
        (*p->callback.Nready)(&p->tcp);
    }
    return rt;
}

MM_EXPORT_NET void mmNetTcp_Start(struct mmNetTcp* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmSelector_Start(&p->selector);
    (*(p->event_tcp_allocator.EventTcpProduce))(p, &p->tcp);
}
MM_EXPORT_NET void mmNetTcp_Interrupt(struct mmNetTcp* p)
{
    p->state = MM_TS_CLOSED;
    
    mmNetTcp_ShutdownSocket(p, MM_BOTH_SHUTDOWN);
    mmNetTcp_CloseSocket(p);
    
    mmSelector_Interrupt(&p->selector);
}
MM_EXPORT_NET void mmNetTcp_Shutdown(struct mmNetTcp* p)
{
    p->state = MM_TS_FINISH;
    
    mmNetTcp_ShutdownSocket(p, MM_BOTH_SHUTDOWN);
    mmNetTcp_CloseSocket(p);
    
    mmSelector_Shutdown(&p->selector);
}
MM_EXPORT_NET void mmNetTcp_Join(struct mmNetTcp* p)
{
    mmSelector_Join(&p->selector);
    (*(p->event_tcp_allocator.EventTcpRecycle))(p, &p->tcp);
}

MM_EXPORT_NET void mmNetTcp_TcpHandlePacketCallback(void* obj, struct mmPacket* pack)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    mmNetTcpHandleFunc handle = NULL;
    mmSpinlock_Lock(&net_tcp->rbtree_locker);
    handle = (mmNetTcpHandleFunc)mmRbtreeU32Vpt_Get(&net_tcp->rbtree, pack->phead.mid);
    mmSpinlock_Unlock(&net_tcp->rbtree_locker);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(tcp, net_tcp->u, pack);
    }
    else
    {
        assert(NULL != net_tcp->callback.Handle && "net_tcp->callback.Handle is a null.");
        (*(net_tcp->callback.Handle))(tcp, net_tcp->u, pack);
    }
}
MM_EXPORT_NET int mmNetTcp_TcpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    mmNetTcp_CryptoDecrypt(net_tcp, tcp, buffer, offset, length);
    return mmStreambufPacket_HandleTcp(&tcp->buff_recv, &mmNetTcp_TcpHandlePacketCallback, tcp);
}
MM_EXPORT_NET int mmNetTcp_TcpBrokenCallback(void* obj)
{
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);
    assert(NULL != net_tcp->callback.Broken && "net_tcp->callback.Broken is a null.");
    mmNetTcp_ApplyBroken(net_tcp);
    return 0;
}
MM_EXPORT_NET void mmNetTcp_MidEventHandle(void* obj, mmUInt32_t mid, mmNetTcpHandleFunc callback)
{
    struct mmPacket pack;
    struct mmPacketHead phead;
    struct mmTcp* tcp = (struct mmTcp*)(obj);
    struct mmNetTcp* net_tcp = (struct mmNetTcp*)(tcp->callback.obj);

    mmPacket_Init(&pack);
    mmPacket_HeadBaseZero(&pack);
    pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    pack.bbuff.length = 0;
    mmPacket_ShadowAlloc(&pack);
    pack.phead.mid = mid;
    mmPacket_HeadEncode(&pack, &pack.hbuff, &phead);
    (*(callback))(obj, net_tcp->u, &pack);
    mmPacket_ShadowFree(&pack);
    mmPacket_Destroy(&pack);
}

static void __static_mmNetTcp_SelectorHandleRecv(void* obj, void* u)
{
    struct mmTcp* tcp = (struct mmTcp*)u;
    mmTcp_HandleRecv(tcp);
}
static void __static_mmNetTcp_SelectorHandleSend(void* obj, void* u)
{
    struct mmTcp* tcp = (struct mmTcp*)u;
    mmTcp_HandleSend(tcp);
}

