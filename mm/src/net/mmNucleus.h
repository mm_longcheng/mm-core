/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNucleus_h__
#define __mmNucleus_h__

#include "core/mmCore.h"
#include "core/mmThread.h"

#include "net/mmNetExport.h"
#include "net/mmNuclear.h"

#include "core/mmPrefix.h"

struct mmNucleus
{
    struct mmNuclearCallback callback;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    mmAtomic_t arrays_locker;
    mmAtomic_t locker;
    // length. default is 0.
    mmUInt32_t length;
    // array pointer.
    struct mmNuclear** arrays;
    // set poll len for mmPoll_wait.
    // default is MM_NUCLEAR_POLL_LEN. set it before wait, or join_wait after.
    // poll length.
    mmUInt32_t poll_length;
    // set poll timeout interval.-1 is forever. default is MM_NUCLEAR_IDLE_SLEEP_MSEC.
    // if forever, you must resolve break block state while poll wait, use pipeline, or other way.
    // poll timeout.
    mmMSec_t poll_timeout;
    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_NET void mmNucleus_Init(struct mmNucleus* p);
MM_EXPORT_NET void mmNucleus_Destroy(struct mmNucleus* p);

/* locker order is
*     locker
*/
MM_EXPORT_NET void mmNucleus_Lock(struct mmNucleus* p);
MM_EXPORT_NET void mmNucleus_Unlock(struct mmNucleus* p);

// must join before set length.
MM_EXPORT_NET void mmNucleus_SetLength(struct mmNucleus* p, mmUInt32_t length);
MM_EXPORT_NET mmUInt32_t mmNucleus_GetLength(struct mmNucleus* p);

MM_EXPORT_NET void mmNucleus_SetCallback(struct mmNucleus* p, struct mmNuclearCallback* callback);

// poll size.
MM_EXPORT_NET size_t mmNucleus_PollSize(struct mmNucleus* p);

// wait for activation fd.
MM_EXPORT_NET void mmNucleus_PollWait(struct mmNucleus* p, mmMSec_t milliseconds);
// Poll loop.
MM_EXPORT_NET void mmNucleus_PollLoop(struct mmNucleus* p);

// start wait thread.
MM_EXPORT_NET void mmNucleus_Start(struct mmNucleus* p);
// interrupt wait thread.
MM_EXPORT_NET void mmNucleus_Interrupt(struct mmNucleus* p);
// shutdown wait thread.
MM_EXPORT_NET void mmNucleus_Shutdown(struct mmNucleus* p);
// join wait thread.
MM_EXPORT_NET void mmNucleus_Join(struct mmNucleus* p);

// traver member for nucleus.
MM_EXPORT_NET void mmNucleus_Traver(struct mmNucleus* p, mmRbtreeVisibleU32HandleFunc handle, void* u);

MM_EXPORT_NET void mmNucleus_Clear(struct mmNucleus* p);

MM_EXPORT_NET mmUInt32_t mmNucleus_HashIndex(struct mmNucleus* p, mmSocket_t fd);

// add u into poll_fd.
MM_EXPORT_NET void mmNucleus_FdAdd(struct mmNucleus* p, mmSocket_t fd, void* u);
// rmv u from poll_fd.
MM_EXPORT_NET void mmNucleus_FdRmv(struct mmNucleus* p, mmSocket_t fd, void* u);
// mod u event from poll wait.
MM_EXPORT_NET void mmNucleus_FdMod(struct mmNucleus* p, mmSocket_t fd, void* u, int flag);
// get u from poll.
MM_EXPORT_NET void* mmNucleus_FdGet(struct mmNucleus* p, mmSocket_t fd);

#include "core/mmSuffix.h"

#endif//__mmNucleus_h__
