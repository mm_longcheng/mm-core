/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmHeadset.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmParameters.h"
#include "net/mmBufferPacket.h"
#include "net/mmMessageCoder.h"

static void* __static_mmMicUdp_UdpPollThread(void* arg);

MM_EXPORT_NET void mmMicUdp_Init(struct mmMicUdp* p)
{
    mmUdp_Init(&p->udp);
    mmSpinlock_Init(&p->locker, NULL);
    p->udp_state = MM_HEADSET_STATE_CLOSED;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmMicUdp_Destroy(struct mmMicUdp* p)
{
    mmMicUdp_Clear(p);
    //
    mmUdp_Destroy(&p->udp);
    mmSpinlock_Destroy(&p->locker);
    p->udp_state = MM_HEADSET_STATE_CLOSED;
    p->state = MM_TS_CLOSED;
}

MM_EXPORT_NET void mmMicUdp_Lock(struct mmMicUdp* p)
{
    mmUdp_Lock(&p->udp);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmMicUdp_Unlock(struct mmMicUdp* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmUdp_Unlock(&p->udp);
}

MM_EXPORT_NET void mmMicUdp_SetUdpShared(struct mmMicUdp* p, struct mmUdp* udp)
{
    mmSockaddr_CopyFrom(&p->udp.socket.ss_native, &udp->socket.ss_native);
    mmSockaddr_CopyFrom(&p->udp.socket.ss_remote, &udp->socket.ss_remote);
    p->udp.socket.socket = udp->socket.socket;
    p->udp.socket = udp->socket;
    p->udp.callback = udp->callback;
    p->udp.unique_id = udp->unique_id;
    p->udp.u = udp->u;
}
MM_EXPORT_NET void mmMicUdp_UpdateUdpState(struct mmMicUdp* p, int state)
{
    p->udp_state = state;
}
MM_EXPORT_NET void mmMicUdp_Clear(struct mmMicUdp* p)
{
    p->udp.socket.socket = MM_INVALID_SOCKET;
}

MM_EXPORT_NET void mmMicUdp_Start(struct mmMicUdp* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->poll_thread, NULL, &__static_mmMicUdp_UdpPollThread, p);
}
MM_EXPORT_NET void mmMicUdp_Interrupt(struct mmMicUdp* p)
{
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmMicUdp_Shutdown(struct mmMicUdp* p)
{
    p->state = MM_TS_FINISH;
}
MM_EXPORT_NET void mmMicUdp_Join(struct mmMicUdp* p)
{
    pthread_join(p->poll_thread, NULL);
}

MM_EXPORT_NET void mmMicUdp_PollWait(struct mmMicUdp* p)
{
    while (MM_TS_MOTION == p->state && MM_HEADSET_STATE_BROKEN != p->udp_state)
    {
        mmUdp_HandleRecv(&p->udp);
        mmUdp_HandleSend(&p->udp);
    }
}

// mic streambuf recv send reset.
MM_EXPORT_NET void mmMicUdp_ResetStreambuf(struct mmMicUdp* p)
{
    mmUdp_ResetStreambuf(&p->udp);
}

static void* __static_mmMicUdp_UdpPollThread(void* arg)
{
    struct mmMicUdp* p = (struct mmMicUdp*)(arg);
    mmMicUdp_PollWait(p);
    return NULL;
}

static void __static_mmHeadset_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{

}
static void __static_mmHeadset_BrokenDefault(void* obj)
{

}
static void __static_mmHeadset_NreadyDefault(void* obj)
{

}
static void __static_mmHeadset_FinishDefault(void* obj)
{

}
MM_EXPORT_NET void mmHeadsetCallback_Init(struct mmHeadsetCallback* p)
{
    p->Handle = &__static_mmHeadset_HandleDefault;
    p->Broken = &__static_mmHeadset_BrokenDefault;
    p->Nready = &__static_mmHeadset_NreadyDefault;
    p->Finish = &__static_mmHeadset_FinishDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmHeadsetCallback_Destroy(struct mmHeadsetCallback* p)
{
    p->Handle = &__static_mmHeadset_HandleDefault;
    p->Broken = &__static_mmHeadset_BrokenDefault;
    p->Nready = &__static_mmHeadset_NreadyDefault;
    p->Finish = &__static_mmHeadset_FinishDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmHeadset_Init(struct mmHeadset* p)
{
    struct mmUdpCallback udp_callback;
    struct mmRbtreeU32VptAllocator _U32VptAllocator;
    //
    mmUdp_Init(&p->udp);
    mmUdpCallback_Init(&p->udp_callback);
    mmHeadsetCallback_Init(&p->callback);
    mmCryptoCallback_Init(&p->crypto_callback);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->arrays_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->length = 0;
    p->arrays = NULL;
    p->nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;
    p->trytimes = MM_HEADSET_TRYTIME;
    p->state = MM_TS_CLOSED;
    p->udp_state = MM_HEADSET_STATE_CLOSED;
    p->u = NULL;
    //
    p->udp_callback.Handle = &mmHeadset_UdpHandleCallback;
    p->udp_callback.Broken = &mmHeadset_UdpBrokenCallback;
    p->udp_callback.obj = p;

    udp_callback.Handle = p->udp_callback.Handle;
    udp_callback.Broken = p->udp_callback.Broken;
    udp_callback.obj = p;
    mmUdp_SetCallback(&p->udp, &udp_callback);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_NET void mmHeadset_Destroy(struct mmHeadset* p)
{
    mmHeadset_ClearHandleRbtree(p);
    mmHeadset_Clear(p);
    //
    mmUdp_Destroy(&p->udp);
    mmUdpCallback_Destroy(&p->udp_callback);
    mmHeadsetCallback_Destroy(&p->callback);
    mmCryptoCallback_Destroy(&p->crypto_callback);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->arrays_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->length = 0;
    p->arrays = NULL;
    p->nonblock_timeout = MM_HEADSET_NONBLOCK_TIMEOUT;
    p->trytimes = MM_HEADSET_TRYTIME;
    p->state = MM_TS_CLOSED;
    p->udp_state = MM_HEADSET_STATE_CLOSED;
    p->u = NULL;
}

// lock.
MM_EXPORT_NET void mmHeadset_Lock(struct mmHeadset* p)
{
    mmUdp_Lock(&p->udp);
    mmSpinlock_Lock(&p->locker);
}
// unlock.
MM_EXPORT_NET void mmHeadset_Unlock(struct mmHeadset* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmUdp_Unlock(&p->udp);
}

// assign native address but not connect.
MM_EXPORT_NET void mmHeadset_SetNative(struct mmHeadset* p, const char* node, mmUShort_t port)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    mmUdp_SetNative(&p->udp, node, port);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmUdp_SetNativeStorage(&e->udp, &p->udp.socket.ss_native);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// assign remote address but not connect.
MM_EXPORT_NET void mmHeadset_SetRemote(struct mmHeadset* p, const char* node, mmUShort_t port)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    mmUdp_SetRemote(&p->udp, node, port);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmUdp_SetRemoteStorage(&e->udp, &p->udp.socket.ss_remote);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// poll thread number.
MM_EXPORT_NET void mmHeadset_SetThreadNumber(struct mmHeadset* p, mmUInt32_t thread_number)
{
    mmHeadset_SetLength(p, thread_number);
}
// assign protocol_size.
MM_EXPORT_NET void mmHeadset_SetLength(struct mmHeadset* p, mmUInt32_t length)
{
    mmSpinlock_Lock(&p->arrays_locker);
    if (length < p->length)
    {
        mmUInt32_t i = 0;
        struct mmMicUdp* e = NULL;
        struct mmMicUdp** v = NULL;
        for (i = length; i < p->length; ++i)
        {
            e = p->arrays[i];
            mmMicUdp_Lock(e);
            p->arrays[i] = NULL;
            mmMicUdp_Unlock(e);
            mmMicUdp_Destroy(e);
            mmFree(e);
        }
        v = (struct mmMicUdp**)mmMalloc(sizeof(struct mmMicUdp*) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmMicUdp*) * length);
        mmFree(p->arrays);
        p->arrays = v;
        p->length = length;
    }
    else if (length > p->length)
    {
        mmUInt32_t i = 0;
        struct mmMicUdp* e = NULL;
        struct mmMicUdp** v = NULL;
        v = (struct mmMicUdp**)mmMalloc(sizeof(struct mmMicUdp*) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmMicUdp*) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        for (i = p->length; i < length; ++i)
        {
            e = (struct mmMicUdp*)mmMalloc(sizeof(struct mmMicUdp));
            mmMicUdp_Init(e);
            mmMicUdp_SetUdpShared(e, &p->udp);
            e->state = p->state;
            p->arrays[i] = e;
        }
        p->length = length;
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_NET mmUInt32_t mmHeadset_GetLength(struct mmHeadset* p)
{
    return p->length;
}
// 192.168.111.123-65535(2)
MM_EXPORT_NET void mmHeadset_SetParameters(struct mmHeadset* p, const char* parameters)
{
    char node[MM_NODE_NAME_LENGTH] = { 0 };
    mmUInt32_t length = 0;
    mmUShort_t port = 0;
    mmParameters_Server(parameters, node, &port, &length);
    // 
    mmHeadset_SetNative(p, node, port);
    mmHeadset_SetLength(p, length);
}
// assign context handle.
MM_EXPORT_NET void mmHeadset_SetContext(struct mmHeadset* p, void* u)
{
    p->u = u;
}
// assign nonblock_timeout.
MM_EXPORT_NET void mmHeadset_SetNonblockTimeout(struct mmHeadset* p, mmMSec_t nonblock_timeout)
{
    p->nonblock_timeout = nonblock_timeout;
}
// assign trytimes.
MM_EXPORT_NET void mmHeadset_SetTrytimes(struct mmHeadset* p, mmUInt32_t trytimes)
{
    p->trytimes = trytimes;
}

MM_EXPORT_NET void mmHeadset_SetHandle(struct mmHeadset* p, mmUInt32_t id, mmHeadsetHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmHeadset_ClearHandleRbtree(struct mmHeadset* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmHeadset_SetUdpCallback(struct mmHeadset* p, struct mmUdpCallback* udp_callback)
{
    struct mmUdpCallback _udp_callback;
    p->udp_callback = *udp_callback;

    _udp_callback.Handle = p->udp_callback.Handle;
    _udp_callback.Broken = p->udp_callback.Broken;
    _udp_callback.obj = p;
    mmUdp_SetCallback(&p->udp, &_udp_callback);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmHeadset_SetCryptoCallback(struct mmHeadset* p, struct mmCryptoCallback* crypto_callback)
{
    p->crypto_callback = *crypto_callback;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmHeadset_SetHeadsetCallback(struct mmHeadset* p, struct mmHeadsetCallback* headset_callback)
{
    assert(NULL != headset_callback && "you can not assign null headset_callback.");
    p->callback = *headset_callback;
}
// context for udp will use the mm_addr.u attribute.such as crypto context.
MM_EXPORT_NET void mmHeadset_SetAddrContext(struct mmHeadset* p, void* u)
{
    mmUdp_SetContext(&p->udp, u);
}
MM_EXPORT_NET void* mmHeadset_GetAddrContext(struct mmHeadset* p)
{
    return mmUdp_GetContext(&p->udp);
}

// headset crypto encrypt buffer by udp.
MM_EXPORT_NET void mmHeadset_CryptoEncrypt(struct mmHeadset* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Encrypt))(p, udp, buffer, offset, buffer, offset, length);
}
// headset crypto decrypt buffer by udp.
MM_EXPORT_NET void mmHeadset_CryptoDecrypt(struct mmHeadset* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length)
{
    (*(p->crypto_callback.Decrypt))(p, udp, buffer, offset, buffer, offset, length);
}

// fopen.
MM_EXPORT_NET void mmHeadset_FopenSocket(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;

    struct mmSocket* socket = &p->udp.socket;

    mmUdp_FopenSocket(&p->udp);
    //
    // SO_REUSEADDR
    mmSocket_ReUseAddr(socket);

    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        e->udp.socket.socket = p->udp.socket.socket;
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// bind.
MM_EXPORT_NET void mmHeadset_Bind(struct mmHeadset* p)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmSocket* socket = &p->udp.socket;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmSocket_ToString(socket, link_name);
    //
    if (MM_INVALID_SOCKET != socket->socket)
    {
        mmUInt32_t _try_times = 0;
        int rt = -1;
        p->udp_state = MM_HEADSET_STATE_MOTION;
        {
            mmUInt32_t i = 0;
            struct mmMicUdp* e = NULL;
            mmSpinlock_Lock(&p->arrays_locker);
            for (i = 0; i < p->length; ++i)
            {
                e = p->arrays[i];
                mmMicUdp_Lock(e);
                mmMicUdp_UpdateUdpState(e, p->udp_state);
                mmMicUdp_Unlock(e);
            }
            mmSpinlock_Unlock(&p->arrays_locker);
        }
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        do
        {
            _try_times++;
            rt = mmSocket_Bind(socket);
            if (0 == rt)
            {
                // bind immediately.
                break;
            }
            if (MM_TS_FINISH == p->state)
            {
                break;
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d bind %s failure.try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
                mmMSleep(MM_HEADSET_TRY_MSLEEP_TIME);
            }
        } while (0 != rt && _try_times < p->trytimes);
        // set socket to blocking.
        mmSocket_SetBlock(socket, MM_BLOCKING);
        if (0 != rt)
        {
            mmErr_t errcode = mmErrno;
            mmLogger_LogE(gLogger, "%s %d bind %s failure.try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
            mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
            mmHeadset_ApplyBroken(p);
            mmSocket_CloseSocket(socket);
        }
        else
        {
            // we check the port.
            if (0 == mmSockaddr_Port(&socket->ss_native))
            {
                mmUInt32_t i = 0;
                struct mmMicUdp* e = NULL;
                // if the port for ss_native is zero,we need obtain it manual.
                // synchronize the real bind socket sockaddr.
                mmSocket_CurrentBindSockaddr(&p->udp.socket, &p->udp.socket.ss_native);
                for (i = 0; i < p->length; ++i)
                {
                    e = p->arrays[i];
                    mmMicUdp_Lock(e);
                    mmSockaddr_CopyFrom(&e->udp.socket.ss_native, &p->udp.socket.ss_native);
                    mmMicUdp_Unlock(e);
                }
            }
            // apply finish.
            mmHeadset_ApplyFinish(p);
            mmLogger_LogI(gLogger, "%s %d %s success.try times:%d", __FUNCTION__, __LINE__, link_name, _try_times);
        }
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d bind target fd is invalid.", __FUNCTION__, __LINE__);
    }
}
// close socket.mmHeadset_Join before call this.
MM_EXPORT_NET void mmHeadset_CloseSocket(struct mmHeadset* p)
{
    mmUdp_CloseSocket(&p->udp);
}
// shutdown socket.
MM_EXPORT_NET void mmHeadset_ShutdownSocket(struct mmHeadset* p)
{
    mmUdp_ShutdownSocket(&p->udp, MM_BOTH_SHUTDOWN);
}

// get finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmHeadset_FinallyState(struct mmHeadset* p)
{
    return (MM_INVALID_SOCKET != p->udp.socket.socket && MM_HEADSET_STATE_FINISH == p->udp_state) ? 0 : -1;
}

MM_EXPORT_NET int mmHeadset_MulcastJoin(struct mmHeadset* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmUdp_MulcastJoin(&p->udp, mr_multiaddr, mr_interface);
}
MM_EXPORT_NET int mmHeadset_MulcastDrop(struct mmHeadset* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmUdp_MulcastDrop(&p->udp, mr_multiaddr, mr_interface);
}

MM_EXPORT_NET void mmHeadset_Clear(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        p->arrays[i] = NULL;
        mmMicUdp_Unlock(e);
        mmMicUdp_Destroy(e);
        mmFree(e);
    }
    mmFree(p->arrays);
    p->arrays = NULL;
    p->length = 0;
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_NET void mmHeadset_ApplyBroken(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->udp_state = MM_HEADSET_STATE_CLOSED;
    mmUdp_ResetStreambuf(&p->udp);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmMicUdp_ResetStreambuf(e);
        mmMicUdp_UpdateUdpState(e, p->udp_state);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    (*p->callback.Broken)(&p->udp);
}
MM_EXPORT_NET void mmHeadset_ApplyFinish(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->udp_state = MM_HEADSET_STATE_FINISH;
    mmUdp_ResetStreambuf(&p->udp);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmMicUdp_ResetStreambuf(e);
        mmMicUdp_UpdateUdpState(e, p->udp_state);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    (*p->callback.Finish)(&p->udp);
}

MM_EXPORT_NET void mmHeadset_SendBuffer(
    struct mmHeadset* p,
    struct mmUdp* udp,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmByteBuffer* buffer,
    struct mmSockaddr* remote)
{
    struct mmPacket packet;

    mmPacket_Init(&packet);

    mmMessageCoder_EncodeMessageByteBuffer(&udp->buff_send, buffer, packet_head, hlength, &packet);
    mmHeadset_CryptoEncrypt(p, udp, packet.bbuff.buffer, packet.bbuff.offset, packet.bbuff.length);
    mmUdp_FlushSend(udp, remote);

    mmPacket_Destroy(&packet);
}

MM_EXPORT_NET void mmHeadset_Start(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    // we must explicit assign thread number manual.
    assert(0 != p->length && "0 != p->length is invalid, not poll thread will do nothing.");
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmMicUdp_Start(e);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_NET void mmHeadset_Interrupt(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    
    mmUdp_ShutdownSocket(&p->udp, MM_BOTH_SHUTDOWN);
    mmUdp_CloseSocket(&p->udp);
    
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_CLOSED;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmMicUdp_Interrupt(e);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_NET void mmHeadset_Shutdown(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    
    mmUdp_ShutdownSocket(&p->udp, MM_BOTH_SHUTDOWN);
    mmUdp_CloseSocket(&p->udp);
    
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmMicUdp_Shutdown(e);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_NET void mmHeadset_Join(struct mmHeadset* p)
{
    mmUInt32_t i = 0;
    struct mmMicUdp* e = NULL;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmMicUdp_Lock(e);
        mmMicUdp_Join(e);
        mmMicUdp_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_NET void mmHeadset_UdpHandlePacketCallback(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    mmNetUdpHandleFunc handle = NULL;
    mmSpinlock_Lock(&headset->rbtree_locker);
    handle = (mmNetUdpHandleFunc)mmRbtreeU32Vpt_Get(&headset->rbtree, pack->phead.mid);
    mmSpinlock_Unlock(&headset->rbtree_locker);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, headset->u, pack, remote);
    }
    else
    {
        assert(NULL != headset->callback.Handle && "headset->Handle is a null.");
        (*(headset->callback.Handle))(udp, headset->u, pack, remote);
    }
}
MM_EXPORT_NET int mmHeadset_UdpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    mmHeadset_CryptoDecrypt(headset, udp, buffer, offset, length);
    return mmBufferPacket_HandleUdp(buffer, offset, length, &mmHeadset_UdpHandlePacketCallback, udp, remote);
}
MM_EXPORT_NET int mmHeadset_UdpBrokenCallback(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    assert(NULL != headset->callback.Broken && "headset->Broken is a null.");
    mmHeadset_ApplyBroken(headset);
    return 0;
}
MM_EXPORT_NET void mmHeadset_MidEventHandle(void* obj, mmUInt32_t mid, mmHeadsetHandleFunc callback)
{
    struct mmSockaddr remote;
    struct mmPacket pack;
    struct mmPacketHead phead;
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);

    mmSockaddr_Init(&remote);
    mmPacket_Init(&pack);
    mmPacket_HeadBaseZero(&pack);
    pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    pack.bbuff.length = 0;
    mmPacket_ShadowAlloc(&pack);
    pack.phead.mid = mid;
    mmPacket_HeadEncode(&pack, &pack.hbuff, &phead);
    (*(callback))(obj, headset->u, &pack, &remote);
    mmPacket_ShadowFree(&pack);
    mmPacket_Destroy(&pack);
    mmSockaddr_Destroy(&remote);
}

