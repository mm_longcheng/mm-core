/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMailbox_h__
#define __mmMailbox_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"
#include "core/mmThread.h"
#include "core/mmPoolElement.h"

#include "net/mmNetExport.h"
#include "net/mmAccepter.h"
#include "net/mmNucleus.h"
#include "net/mmTcp.h"
#include "net/mmCrypto.h"
#include "net/mmPacket.h"

#include "container/mmRbtreeU32.h"

#include "core/mmPrefix.h"

typedef void(*mmMailboxHandleFunc)(void* obj, void* u, struct mmPacket* pack);

struct mmMailboxCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack);
    void(*Broken)(void* obj);
    void(*Nready)(void* obj);
    void(*Finish)(void* obj);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmMailboxCallback_init(struct mmMailboxCallback* p);
MM_EXPORT_NET void mmMailboxCallback_destroy(struct mmMailboxCallback* p);

struct mmMailboxEventTcpAllocator
{
    // event tcp produce and add rbtree.
    void(*EventTcpProduce)(void* p, struct mmTcp* tcp);
    // event tcp recycle and rmv rbtree.
    void(*EventTcpRecycle)(void* p, struct mmTcp* tcp);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmMailboxEventTcpAllocator_Init(struct mmMailboxEventTcpAllocator* p);
MM_EXPORT_NET void mmMailboxEventTcpAllocator_Destroy(struct mmMailboxEventTcpAllocator* p);

struct mmMailbox
{
    struct mmPoolElement pool_element;
    struct mmAccepter accepter;
    // strong ref.
    struct mmNucleus nucleus;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    // mailbox tcp callback handle implement.
    struct mmTcpCallback tcp_callback;
    // value ref. tcp callback.
    struct mmMailboxCallback callback;
    // value ref.event tcp alloc.
    struct mmMailboxEventTcpAllocator event_tcp_allocator;
    // crypto callback.
    struct mmCryptoCallback crypto_callback;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmMailbox_Init(struct mmMailbox* p);
MM_EXPORT_NET void mmMailbox_Destroy(struct mmMailbox* p);

// assign native address but not connect.
MM_EXPORT_NET void mmMailbox_SetNative(struct mmMailbox* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmMailbox_SetRemote(struct mmMailbox* p, const char* node, mmUShort_t port);
// poll thread number.
MM_EXPORT_NET void mmMailbox_SetThreadNumber(struct mmMailbox* p, mmUInt32_t thread_number);
// assign protocol_size.
MM_EXPORT_NET void mmMailbox_SetLength(struct mmMailbox* p, mmUInt32_t length);
MM_EXPORT_NET mmUInt32_t mmMailbox_GetLength(struct mmMailbox* p);
// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
MM_EXPORT_NET void mmMailbox_SetParameters(struct mmMailbox* p, const char* parameters);
// assign context handle.
MM_EXPORT_NET void mmMailbox_SetContext(struct mmMailbox* p, void* u);
// fopen.
MM_EXPORT_NET void mmMailbox_FopenSocket(struct mmMailbox* p);
// bind.
MM_EXPORT_NET void mmMailbox_Bind(struct mmMailbox* p);
// listen.
MM_EXPORT_NET void mmMailbox_Listen(struct mmMailbox* p);
// close socket. mmMailbox_Join before call this.
MM_EXPORT_NET void mmMailbox_CloseSocket(struct mmMailbox* p);

/* locker order is
*     pool_element
*     accepter
*     locker
*/
MM_EXPORT_NET void mmMailbox_Lock(struct mmMailbox* p);
MM_EXPORT_NET void mmMailbox_Unlock(struct mmMailbox* p);

MM_EXPORT_NET void mmMailbox_SetHandle(struct mmMailbox* p, mmUInt32_t id, mmMailboxHandleFunc callback);

// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetTcpCallback(struct mmMailbox* p, struct mmTcpCallback* tcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetEventTcpAllocator(struct mmMailbox* p, struct mmMailboxEventTcpAllocator* event_tcp_allocator);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetCryptoCallback(struct mmMailbox* p, struct mmCryptoCallback* crypto_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetMailboxCallback(struct mmMailbox* p, struct mmMailboxCallback* mailbox_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmMailbox_SetTcpAllocator(struct mmMailbox* p, struct mmRbtreeU32VptAllocator* allocator);

MM_EXPORT_NET void mmMailbox_ClearHandleRbtree(struct mmMailbox* p);

// mailbox crypto encrypt buffer by tcp.
MM_EXPORT_NET void mmMailbox_CryptoEncrypt(struct mmMailbox* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length);
// mailbox crypto decrypt buffer by tcp.
MM_EXPORT_NET void mmMailbox_CryptoDecrypt(struct mmMailbox* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length);

// not lock inside,lock tcp manual.
// obj is mm_rbtree_visible. u is traver user data.k is fd.v is bind for fd.
// note:
// rbtree_visible traver callback we lock instance_locker outside, so the v is valid.
// if at traver callback handle trigger other callback, do not lock the v.
// if lock v before trigger other callback, some time will case dead lock at other callback inside.
// although the v is valid but we can lock v at this function, but careful for it.
MM_EXPORT_NET void mmMailbox_Traver(struct mmMailbox* p, mmRbtreeVisibleU32HandleFunc handle, void* u);
// poll size.
MM_EXPORT_NET size_t mmMailbox_PollSize(struct mmMailbox* p);

MM_EXPORT_NET void mmMailbox_Start(struct mmMailbox* p);
MM_EXPORT_NET void mmMailbox_Interrupt(struct mmMailbox* p);
MM_EXPORT_NET void mmMailbox_Shutdown(struct mmMailbox* p);
MM_EXPORT_NET void mmMailbox_Join(struct mmMailbox* p);

MM_EXPORT_NET struct mmTcp* mmMailbox_Add(struct mmMailbox* p, mmSocket_t fd, struct mmSockaddr* remote);
MM_EXPORT_NET struct mmTcp* mmMailbox_Get(struct mmMailbox* p, mmSocket_t fd);
MM_EXPORT_NET struct mmTcp* mmMailbox_GetInstance(struct mmMailbox* p, mmSocket_t fd, struct mmSockaddr* remote);
MM_EXPORT_NET void mmMailbox_Rmv(struct mmMailbox* p, mmSocket_t fd);
MM_EXPORT_NET void mmMailbox_RmvTcp(struct mmMailbox* p, struct mmTcp* tcp);
MM_EXPORT_NET void mmMailbox_Clear(struct mmMailbox* p);

// use this function for manual shutdown tcp.
// note: you can not use rmv or rmv_tcp for shutdown a activation tcp.
MM_EXPORT_NET void mmMailbox_ShutdownTcp(struct mmMailbox* p, struct mmTcp* tcp);

MM_EXPORT_NET void mmMailbox_TcpHandlePacketCallback(void* obj, struct mmPacket* pack);
MM_EXPORT_NET int mmMailbox_TcpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length);
MM_EXPORT_NET int mmMailbox_TcpBrokenCallback(void* obj);

MM_EXPORT_NET void mmMailbox_MidEventHandle(void* obj, mmUInt32_t mid, mmMailboxHandleFunc callback);

#include "core/mmSuffix.h"

#endif//__mmMailbox_h__
