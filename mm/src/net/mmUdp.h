/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUdp_h__
#define __mmUdp_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmOSSocket.h"

#include "net/mmNetExport.h"
#include "net/mmSocket.h"

#include "core/mmPrefix.h"

enum mmUdpMid_t
{
    MM_UDP_MID_BROKEN = 0x0F000010,
    MM_UDP_MID_NREADY = 0x0F000011,
    MM_UDP_MID_FINISH = 0x0F000012,
};

struct mmUdpCallback
{
    // @param obj struct mmUdp*
    // @param b buffer
    // @param o offset
    // @param l lenght
    // @param r remote struct mmSockaddr
    int(*Handle)(void* obj, mmUInt8_t* b, size_t o, size_t l, struct mmSockaddr* r);
    
    // @param obj struct mmUdp*
    int(*Broken)(void* obj);
    
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmUdpCallback_Init(struct mmUdpCallback* p);
MM_EXPORT_NET void mmUdpCallback_Destroy(struct mmUdpCallback* p);
MM_EXPORT_NET void mmUdpCallback_Reset(struct mmUdpCallback* p);

struct mmUdp
{
    // strong ref. udp socket. 
    // s_locker will lock this data.
    struct mmSocket socket;

    // strong ref.
    // i_locker will lock this data.
    struct mmStreambuf buff_recv;

    // strong ref.
    // o_locker will lock this data.
    struct mmStreambuf buff_send;

    // value ref. transport callback.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    struct mmUdpCallback callback;

    // external locker, locker_i.
    mmAtomic_t locker_i;

    // external locker, locker_o.
    mmAtomic_t locker_o;

    // feedback unique_id.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    mmUInt64_t unique_id;

    // user data.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    void* u;
};

MM_EXPORT_NET void mmUdp_Init(struct mmUdp* p);
MM_EXPORT_NET void mmUdp_Destroy(struct mmUdp* p);

/* locker order is
*     socket
*     i_locker
*     o_locker
*/
MM_EXPORT_NET void mmUdp_Lock(struct mmUdp* p);
MM_EXPORT_NET void mmUdp_Unlock(struct mmUdp* p);
// s locker. 
MM_EXPORT_NET void mmUdp_SLock(struct mmUdp* p);
MM_EXPORT_NET void mmUdp_SUnlock(struct mmUdp* p);
// i locker. 
MM_EXPORT_NET void mmUdp_ILock(struct mmUdp* p);
MM_EXPORT_NET void mmUdp_IUnlock(struct mmUdp* p);
// o locker. 
MM_EXPORT_NET void mmUdp_OLock(struct mmUdp* p);
MM_EXPORT_NET void mmUdp_OUnlock(struct mmUdp* p);

/* reset api no locker. */
MM_EXPORT_NET void mmUdp_Reset(struct mmUdp* p);

// udp streambuf recv send reset.
MM_EXPORT_NET void mmUdp_ResetStreambuf(struct mmUdp* p);

/* assign api no locker. */
// assign addr native by ip port.
MM_EXPORT_NET void mmUdp_SetNative(struct mmUdp* p, const char* node, mmUShort_t port);
// assign addr remote by ip port.
MM_EXPORT_NET void mmUdp_SetRemote(struct mmUdp* p, const char* node, mmUShort_t port);
// assign addr native by sockaddr_storage.AF_INET;
// after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmUdp_SetNativeStorage(struct mmUdp* p, struct mmSockaddr* ss_native);
// assign addr remote by sockaddr_storage.AF_INET;
// after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmUdp_SetRemoteStorage(struct mmUdp* p, struct mmSockaddr* ss_remote);

MM_EXPORT_NET void mmUdp_SetCallback(struct mmUdp* p, struct mmUdpCallback* cb);

MM_EXPORT_NET void mmUdp_SetUniqueId(struct mmUdp* p, mmUInt64_t unique_id);
MM_EXPORT_NET mmUInt64_t mmUdp_GetUniqueId(struct mmUdp* p);

// context for udp will use the mmSocket. u attribute. such as crypto context.
MM_EXPORT_NET void mmUdp_SetContext(struct mmUdp* p, void* u);
MM_EXPORT_NET void* mmUdp_GetContext(struct mmUdp* p);

// fopen socket.ss_native.ss_family, SOCK_DGRAM, 0
MM_EXPORT_NET void mmUdp_FopenSocket(struct mmUdp* p);
// close socket.
MM_EXPORT_NET void mmUdp_CloseSocket(struct mmUdp* p);
// shutdown socket.
MM_EXPORT_NET void mmUdp_ShutdownSocket(struct mmUdp* p, int opcode);

MM_EXPORT_NET int mmUdp_MulcastJoin(struct mmUdp* p, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_NET int mmUdp_MulcastDrop(struct mmUdp* p, const char* mr_multiaddr, const char* mr_interface);

// handle recv for buffer pool and pool max size.
MM_EXPORT_NET void mmUdp_HandleRecv(struct mmUdp* p);
// handle send for buffer pool and pool max size.
MM_EXPORT_NET void mmUdp_HandleSend(struct mmUdp* p);

// handle recv data from buffer and buffer length. 0 success -1 failure.
// @param b buffer
// @param o offset
// @param l lenght
// @param r remote struct mmSockaddr
MM_EXPORT_NET int mmUdp_BufferRecv(struct mmUdp* p, mmUInt8_t* b, size_t o, size_t l, struct mmSockaddr* r);

// handle send data from buffer and buffer length. > 0 is send size success -1 failure.
// note this function do not trigger the `callback.broken` it is only mmSocket_ShutdownSocket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger `callback.broken`, make sure not dead lock and invalid free pinter appear.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
//
// @param b buffer
// @param o offset
// @param l lenght
// @param r remote struct mmSockaddr
MM_EXPORT_NET int mmUdp_BufferSend(struct mmUdp* p, mmUInt8_t* b, size_t o, size_t l, struct mmSockaddr* r);

// handle send data by flush send buffer.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
MM_EXPORT_NET int mmUdp_FlushSend(struct mmUdp* p, struct mmSockaddr* remote);

#include "core/mmSuffix.h"

#endif//__mmUdp_h__
