/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPoolPacket_h__
#define __mmPoolPacket_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmPoolElement.h"

#include "container/mmRbtsetVpt.h"

#include "net/mmPacket.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

#define MM_POOL_PACKET_RATE_TRIM_BORDER_DEFAULT 0.75
#define MM_POOL_PACKET_RATE_TRIM_NUMBER_DEFAULT 0.25

#define MM_POOL_PACKET_TRIM_PAGE 4

struct mmPoolPacketElem;

MM_EXPORT_NET void mmPoolPacket_ChunkProduce(struct mmStreambuf* p, struct mmPoolPacketElem* v, size_t hlength, size_t blength);
MM_EXPORT_NET void mmPoolPacket_ChunkRecycle(struct mmStreambuf* p, struct mmPoolPacketElem* v);
MM_EXPORT_NET int mmPoolPacket_ChunkProduceComplete(struct mmStreambuf* p);
MM_EXPORT_NET int mmPoolPacket_ChunkRecycleComplete(struct mmStreambuf* p);

struct mmPoolPacketElem
{
    // packet ref to streambuf.
    struct mmPacket packet;
    // remote end point address.
    struct mmSockaddr ss_remote;
    // weak ref for streambuf.
    struct mmStreambuf* streambuf;
    // weak ref for handle callback.
    void* obj;
};
MM_EXPORT_NET void mmPoolPacketElem_Init(struct mmPoolPacketElem* p);
MM_EXPORT_NET void mmPoolPacketElem_Destroy(struct mmPoolPacketElem* p);
MM_EXPORT_NET void mmPoolPacketElem_Reset(struct mmPoolPacketElem* p);
MM_EXPORT_NET void mmPoolPacketElem_SetRemote(struct mmPoolPacketElem* p, struct mmSockaddr* remote);
MM_EXPORT_NET void mmPoolPacketElem_SetObject(struct mmPoolPacketElem* p, void* obj);
MM_EXPORT_NET void mmPoolPacketElem_SetPacket(struct mmPoolPacketElem* p, struct mmPacket* packet);
MM_EXPORT_NET void mmPoolPacketElem_SetBuffer(struct mmPoolPacketElem* p, struct mmPacketHead* packet_head, struct mmByteBuffer* bbuff);

struct mmPoolPacket
{
    struct mmPoolElement pool_element;
    // strong ref.
    struct mmRbtsetVpt pool;
    // weak ref.
    struct mmRbtsetVpt used;
    // weak ref.
    struct mmRbtsetVpt idle;
    // weak ref for current streambuf.
    struct mmStreambuf* current;
    float rate_trim_border;
    float rate_trim_number;
};

MM_EXPORT_NET void mmPoolPacket_Init(struct mmPoolPacket* p);
MM_EXPORT_NET void mmPoolPacket_Destroy(struct mmPoolPacket* p);

/* locker order is
*     pool_element
*/
MM_EXPORT_NET void mmPoolPacket_Lock(struct mmPoolPacket* p);
MM_EXPORT_NET void mmPoolPacket_Unlock(struct mmPoolPacket* p);

// can not change at pool using.
MM_EXPORT_NET void mmPoolPacket_SetChunkSize(struct mmPoolPacket* p, size_t chunk_size);
// can not change at pool using.
MM_EXPORT_NET void mmPoolPacket_SetRateTrimBorder(struct mmPoolPacket* p, float rate_trim_border);
// can not change at pool using.
MM_EXPORT_NET void mmPoolPacket_SetRateTrimNumber(struct mmPoolPacket* p, float rate_trim_number);

MM_EXPORT_NET struct mmStreambuf* mmPoolPacket_AddChunk(struct mmPoolPacket* p);
MM_EXPORT_NET void mmPoolPacket_RmvChunk(struct mmPoolPacket* p, struct mmStreambuf* e);
MM_EXPORT_NET void mmPoolPacket_ClearChunk(struct mmPoolPacket* p);

MM_EXPORT_NET struct mmPoolPacketElem* mmPoolPacket_Produce(struct mmPoolPacket* p, size_t hlength, size_t blength);
MM_EXPORT_NET void mmPoolPacket_Recycle(struct mmPoolPacket* p, struct mmPoolPacketElem* v);

#include "core/mmSuffix.h"

#endif//__mmPoolPacket_h__
