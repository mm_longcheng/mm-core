/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPacketQueue_h__
#define __mmPacketQueue_h__

#include "core/mmCore.h"
#include "core/mmTime.h"
#include "core/mmThread.h"

#include "net/mmNetExport.h"
#include "net/mmPacket.h"
#include "net/mmPoolPacket.h"

#include "container/mmConcurrentQueue.h"
#include "container/mmRbtreeU32.h"

#include "core/mmPrefix.h"

// max poper number at one loop.the default main thread timer frequency is 1/20 = 50 ms.
// 2ms one message handle is suitable 50/2 = 25.
//
// draw frequency is 60, the net sampling rate some time is 60.
// we must pop all message at this case, so we need twice rate 120.
// when net need repaird packet the sampling rate some time is 120, twice rate 240.
//
// value -1, pop message until size is 0.
// 
// we use 240 default. 50 / 240 = 0.20833 ms = 208.33 us.
#define MM_PACKET_QUEUE_MAX_POPER_NUMBER 240

typedef void(*mmPacketQueueHandleFunc)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

struct mmPacketQueueCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmPacketQueueCallback_Init(struct mmPacketQueueCallback* p);
MM_EXPORT_NET void mmPacketQueueCallback_Destroy(struct mmPacketQueueCallback* p);

// message queue.
struct mmPacketQueue
{
    struct mmPoolPacket pool_packet;
    struct mmTwoLockQuque lock_queue;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    // value ref. queue callback.
    struct mmPacketQueueCallback callback;
    mmAtomic_t rbtree_locker;
    mmAtomic_t pool_locker;
    mmAtomic_t locker;
    // max pop to make sure current not wait long times.
    // default is MM_PACKET_QUEUE_MAX_POPER_NUMBER.
    size_t max_pop;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmPacketQueue_Init(struct mmPacketQueue* p);
MM_EXPORT_NET void mmPacketQueue_Destroy(struct mmPacketQueue* p);

/* locker order is
*     pool_packet
*     locker
*/
MM_EXPORT_NET void mmPacketQueue_Lock(struct mmPacketQueue* p);
MM_EXPORT_NET void mmPacketQueue_Unlock(struct mmPacketQueue* p);

MM_EXPORT_NET void mmPacketQueue_SetHandle(struct mmPacketQueue* p, mmUInt32_t id, mmPacketQueueHandleFunc callback);
MM_EXPORT_NET void mmPacketQueue_SetQueueCallback(struct mmPacketQueue* p, struct mmPacketQueueCallback* queue_callback);
MM_EXPORT_NET void mmPacketQueue_SetMaxPoperNumber(struct mmPacketQueue* p, size_t max_pop);
// assign context handle.
MM_EXPORT_NET void mmPacketQueue_SetContext(struct mmPacketQueue* p, void* u);

MM_EXPORT_NET void mmPacketQueue_ClearHandleRbtree(struct mmPacketQueue* p);

MM_EXPORT_NET int mmPacketQueue_IsEmpty(struct mmPacketQueue* p);

// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_NET void mmPacketQueue_ThreadHandle(struct mmPacketQueue* p);
// delete all not handle pack at queue.
MM_EXPORT_NET void mmPacketQueue_Dispose(struct mmPacketQueue* p);
// push a pack and context tp queue.
MM_EXPORT_NET void mmPacketQueue_Push(struct mmPacketQueue* p, void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
// pop and trigger handle number.
MM_EXPORT_NET void mmPacketQueue_PopNumber(struct mmPacketQueue* p, size_t number);
// pop and trigger handle entire.
MM_EXPORT_NET void mmPacketQueue_PopEntire(struct mmPacketQueue* p);
// pop and trigger handle single.
MM_EXPORT_NET void mmPacketQueue_PopSingle(struct mmPacketQueue* p);

MM_EXPORT_NET struct mmPoolPacketElem* mmPacketQueue_Overdraft(struct mmPacketQueue* p, size_t hlength, size_t blength);
MM_EXPORT_NET void mmPacketQueue_Repayment(struct mmPacketQueue* p, struct mmPoolPacketElem* packet_elem);

#include "core/mmSuffix.h"

#endif//__mmPacketQueue_h__
