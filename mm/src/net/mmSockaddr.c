/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSockaddr.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmAtoi.h"
#include "core/mmLogger.h"

static void __static_mmSockaddr_UnspecAssign(struct mmSockaddr* p, const char* node, mmUShort_t port);
static void __static_mmSockaddr_Inetv4Assign(struct mmSockaddr* p, const char* node, mmUShort_t port);
static void __static_mmSockaddr_Inetv6Assign(struct mmSockaddr* p, const char* node, mmUShort_t port);

static void __static_mmSockaddr_UnspecString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH]);
static void __static_mmSockaddr_Inetv4String(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH]);
static void __static_mmSockaddr_Inetv6String(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH]);

static void __static_mmSockaddr_UnspecPort(struct mmSockaddr* p, mmUShort_t* port);
static void __static_mmSockaddr_Inetv4Port(struct mmSockaddr* p, mmUShort_t* port);
static void __static_mmSockaddr_Inetv6Port(struct mmSockaddr* p, mmUShort_t* port);

static void __static_mmSockaddr_UnspecNodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port);
static void __static_mmSockaddr_Inetv4NodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port);
static void __static_mmSockaddr_Inetv6NodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port);

MM_EXPORT_NET void mmSockaddr_Init(struct mmSockaddr* p)
{
    mmMemset(p, 0, sizeof(struct mmSockaddr));
    p->addr.sa_family = MM_AF_INET6;
}
MM_EXPORT_NET void mmSockaddr_Destroy(struct mmSockaddr* p)
{
    mmMemset(p, 0, sizeof(struct mmSockaddr));
    p->addr.sa_family = MM_AF_INET6;
}

// copy t to p.
MM_EXPORT_NET void mmSockaddr_CopyFrom(struct mmSockaddr* p, struct mmSockaddr* t)
{
    mmMemcpy(p, t, sizeof(struct mmSockaddr));
}
// reset.
MM_EXPORT_NET void mmSockaddr_Reset(struct mmSockaddr* p)
{
    mmMemset(p, 0, sizeof(struct mmSockaddr));
    p->addr.sa_family = MM_AF_INET6;
}
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_Assign(struct mmSockaddr* p, const char* node, mmUShort_t port)
{
    char addr_name[MM_ADDR_NAME_LENGTH] = { 0 };
    mmStrcpy(addr_name, node);
    mmMemset(p, 0, sizeof(struct mmSockaddr));
    p->addr.sa_family = mmSockaddr_NodeFamily(node);
    assert(AF_UNSPEC <= p->addr.sa_family && p->addr.sa_family <= MM_AF_INET6 && "p->addr.sa_family is invalid.");
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        __static_mmSockaddr_Inetv4Assign(p, node, port);
        break;
    case MM_AF_INET6:
        __static_mmSockaddr_Inetv6Assign(p, node, port);
        break;
    default:
        __static_mmSockaddr_UnspecAssign(p, node, port);
        break;
    }
}
// 2001:0DB8:0000:0023:0008:0800:200C:417A 64 38
// 65535                                   8  5
// addr_name:
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_ToString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH])
{
    assert(AF_UNSPEC <= p->addr.sa_family && p->addr.sa_family <= MM_AF_INET6 && "p->addr.sa_family is invalid.");
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        __static_mmSockaddr_Inetv4String(p, addr_name);
        break;
    case MM_AF_INET6:
        __static_mmSockaddr_Inetv6String(p, addr_name);
        break;
    default:
        __static_mmSockaddr_UnspecString(p, addr_name);
        break;
    }
}

// (fd)|ip-port --> ip-port
// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
MM_EXPORT_NET void
mmSockaddr_NativeRemoteString(
    struct mmSockaddr* native_addr,
    struct mmSockaddr* remote_addr,
    mmSocket_t fd,
    char link_name[MM_LINK_NAME_LENGTH])
{
    char addr_n_name[MM_ADDR_NAME_LENGTH] = { 0 };
    char addr_r_name[MM_ADDR_NAME_LENGTH] = { 0 };
    mmSockaddr_ToString(native_addr, addr_n_name);
    mmSockaddr_ToString(remote_addr, addr_r_name);
    mmSprintf(link_name, "(%d)|%s-->%s", (int)fd, addr_n_name, addr_r_name);
}

// 0 is same.
MM_EXPORT_NET int mmSockaddr_Compare(struct mmSockaddr* p, struct mmSockaddr* r)
{
    return mmMemcmp(p, r, sizeof(struct mmSockaddr));
}
// 0 is same.
MM_EXPORT_NET int mmSockaddr_CompareAddress(struct mmSockaddr* p, const char* node, mmUShort_t port)
{
    struct mmSockaddr r;
    mmSockaddr_Assign(&r, node, port);
    return mmSockaddr_Compare(p, &r);
}
// decode by string.
MM_EXPORT_NET void mmSockaddr_DecodeString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH])
{
    char node_name[MM_NODE_NAME_LENGTH] = { 0 };
    mmUShort_t port = 0;
    mmSockaddr_FormatDecodeString(addr_name, node_name, &port);
    mmSockaddr_Assign(p, node_name, port);
}
// encode to string.
MM_EXPORT_NET void mmSockaddr_EncodeString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH])
{
    mmSockaddr_ToString(p, addr_name);
}

// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_NET void mmSockaddr_DecodeZeroString(struct mmSockaddr* p, const char* node)
{
    p->addr.sa_family = mmSockaddr_NodeFamily(node);
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        __static_mmSockaddr_Inetv4Assign(p, "::", 0);
        break;
    case MM_AF_INET6:
        __static_mmSockaddr_Inetv6Assign(p, "0.0.0.0", 0);
        break;
    default:
        __static_mmSockaddr_UnspecAssign(p, "0.0.0.0", 0);
        break;
    }
}
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_NET void mmSockaddr_DecodeZero(struct mmSockaddr* p, const char* node, mmUShort_t port)
{
    char addr_name[MM_ADDR_NAME_LENGTH] = { 0 };
    mmStrcpy(addr_name, node);
    p->addr.sa_family = mmSockaddr_NodeFamily(node);
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        __static_mmSockaddr_Inetv4Assign(p, "::", 0);
        break;
    case MM_AF_INET6:
        __static_mmSockaddr_Inetv6Assign(p, "0.0.0.0", 0);
        break;
    default:
        __static_mmSockaddr_UnspecAssign(p, "0.0.0.0", 0);
        break;
    }
}

// calculate the socket real length for this function.
// connect bind sendto
// for linux windows use the sizeof(struct sockaddr_storage) os not checking and anything correct.
// for freebsd must use the real sockaddr length.if not well case errno(EAFNOSUPPORT) Address family not supported by protocol family.
MM_EXPORT_NET socklen_t mmSockaddr_Length(struct mmSockaddr* p)
{
    socklen_t length = 0;
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        length = sizeof(struct sockaddr_in);
        break;
    case MM_AF_INET6:
        length = sizeof(struct sockaddr_in6);
        break;
    default:
        length = sizeof(struct sockaddr_storage);
        break;
    }
    return length;
}
MM_EXPORT_NET mmUShort_t mmSockaddr_Port(struct mmSockaddr* p)
{
    mmUShort_t port = 0;
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        __static_mmSockaddr_Inetv4Port(p, &port);
        break;
    case MM_AF_INET6:
        __static_mmSockaddr_Inetv6Port(p, &port);
        break;
    default:
        __static_mmSockaddr_UnspecPort(p, &port);
        break;
    }
    return port;
}
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_NodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port)
{
    switch (p->addr.sa_family)
    {
    case MM_AF_INET4:
        __static_mmSockaddr_Inetv4NodePort(p, node, port);
        break;
    case MM_AF_INET6:
        __static_mmSockaddr_Inetv6NodePort(p, node, port);
        break;
    default:
        __static_mmSockaddr_UnspecNodePort(p, node, port);
        break;
    }
}

MM_EXPORT_NET void mmSockaddrStorage_Init(struct sockaddr_storage* p)
{
    mmMemset(p, 0, sizeof(struct sockaddr_storage));
    p->ss_family = MM_AF_INET6;
}
MM_EXPORT_NET void mmSockaddrStorage_Destroy(struct sockaddr_storage* p)
{
    mmMemset(p, 0, sizeof(struct sockaddr_storage));
    p->ss_family = MM_AF_INET6;
}

// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void
mmSockaddr_FormatDecodeString(
    char addr_name[MM_ADDR_NAME_LENGTH],
    char node[MM_NODE_NAME_LENGTH],
    mmUShort_t* port)
{
    // MM_NODE_NAME_LENGTH + 1 + MM_INT32_LEN = 76;
    // 128 buffer for memory alignment.
    static const size_t acl = 128;
    char buffer[128] = { 0 };
    size_t len = mmStrlen(addr_name);
    size_t min_len = len < acl ? len : acl;
    size_t i = 0;
    mmMemcpy(buffer, addr_name, min_len);
    if (0 < min_len)
    {
        for (i = min_len - 1; 0 < i; --i)
        {
            if ('-' == buffer[i])
            {
                buffer[i] = ' ';
                break;
            }
        }
    }
    mmSscanf(buffer, "%s %" SCNu16 "", node, port);
}

// encode to string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void
mmSockaddr_FormatEncodeString(
    char addr_name[MM_ADDR_NAME_LENGTH],
    char node[MM_NODE_NAME_LENGTH],
    mmUShort_t* port)
{
    mmSprintf(addr_name, "%s-%u", node, *port);
}

// family ipv4 ipv6.
MM_EXPORT_NET int mmSockaddr_NodeFamily(const char* node)
{
    int ss_family = MM_AF_UNSPE;
    size_t l = mmStrlen(node);
    size_t i = 0;
    size_t a = 0;// a number
    size_t d = 0;// . number
    size_t m = 0;// : number
    size_t t = MM_ADDR_NAME_LENGTH < l ? MM_ADDR_NAME_LENGTH : l;
    //  l  a  d  m
    // 16 13  3  0      www.google.co.uk localhost
    // 15  0  3  0      192.168.111.123
    // 39  4  0  7      2001:0DB8:0000:0023:0008:0800:200C:417A
    do
    {
        if (MM_ADDR_NAME_LENGTH < l)
        {
            ss_family = MM_AF_UNSPE;
            break;
        }
        for (i = 0; i < t; ++i)
        {
            if (0 != isalpha(node[i]))
            {
                a++;
            }
            if ('.' == node[i])
            {
                d++;
            }
            if (':' == node[i])
            {
                m++;
            }
        }
        if (0 < a && 0 == m)
        {
            ss_family = MM_AF_UNSPE;
            break;
        }
        if (0 == a && 0 < d && 0 == m)
        {
            ss_family = MM_AF_INET4;
            break;
        }
        if (0 == d && 0 < m)
        {
            ss_family = MM_AF_INET6;
            break;
        }
    } while (0);
    return ss_family;
}

static void __static_mmSockaddr_UnspecAssign(struct mmSockaddr* p, const char* node, mmUShort_t port)
{
    struct addrinfo* addr_info = NULL;
    char port_string[MM_PORT_NAME_LENGTH] = { 0 };
    mmMemset(p, 0, sizeof(struct mmSockaddr));
    mmSprintf(port_string, "%u", port);
    if (0 == getaddrinfo(node, port_string, NULL, &addr_info) && NULL != addr_info)
    {
        mmMemcpy(p, addr_info->ai_addr, addr_info->ai_addrlen);
        freeaddrinfo(addr_info);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmErr_t errcode = mmErrno;
        mmLogger_LogI(gLogger, "%s %d getaddrinfo failure.", __FUNCTION__, __LINE__);
        mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
    }
}
static void __static_mmSockaddr_Inetv4Assign(struct mmSockaddr* p, const char* node, mmUShort_t port)
{
    struct sockaddr_in* addr_inetv4 = (struct sockaddr_in*)(p);
    assert(p->addr.sa_family == MM_AF_INET4 && "addr->addr.sa_family == MM_AF_INET4 is invalid.");
    addr_inetv4->sin_family = p->addr.sa_family;
    addr_inetv4->sin_port = mmHtons(port);
    inet_pton(MM_AF_INET4, node, &addr_inetv4->sin_addr);
}
static void __static_mmSockaddr_Inetv6Assign(struct mmSockaddr* p, const char* node, mmUShort_t port)
{
    struct sockaddr_in6* addr_inetv6 = (struct sockaddr_in6*)(p);
    assert(p->addr.sa_family == MM_AF_INET6 && "addr->addr.sa_family == MM_AF_INET6 is invalid.");
    addr_inetv6->sin6_family = p->addr.sa_family;
    addr_inetv6->sin6_port = mmHtons(port);
    inet_pton(MM_AF_INET6, node, &addr_inetv6->sin6_addr);
}

static void __static_mmSockaddr_UnspecString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH])
{
    char node_string[MM_NODE_NAME_LENGTH] = { 0 };
    char port_string[MM_PORT_NAME_LENGTH] = { 0 };
    struct addrinfo addr_info;
    mmMemset(&addr_info, 0, sizeof(struct addrinfo));
    addr_info.ai_addrlen = sizeof(struct mmSockaddr);
    addr_info.ai_addr = (struct sockaddr*)p;
    getnameinfo(addr_info.ai_addr, (socklen_t)addr_info.ai_addrlen, node_string, MM_NODE_NAME_LENGTH, port_string, MM_PORT_NAME_LENGTH, addr_info.ai_flags);
    mmSprintf(addr_name, "%s-%s", node_string, port_string);
}
static void __static_mmSockaddr_Inetv4String(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH])
{
    struct sockaddr_in* addr_inetv4 = (struct sockaddr_in*)(p);
    char node_string[MM_NODE_NAME_LENGTH] = { 0 };
    assert(p->addr.sa_family == MM_AF_INET4 && "addr->addr.sa_family == MM_AF_INET4 is invalid.");
    inet_ntop(MM_AF_INET4, &addr_inetv4->sin_addr, node_string, MM_NODE_NAME_LENGTH);
    mmSprintf(addr_name, "%s-%u", node_string, mmNtohs(addr_inetv4->sin_port));
}
static void __static_mmSockaddr_Inetv6String(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH])
{
    struct sockaddr_in6* addr_inetv6 = (struct sockaddr_in6*)(p);
    char node_string[MM_NODE_NAME_LENGTH] = { 0 };
    assert(p->addr.sa_family == MM_AF_INET6 && "addr->addr.sa_family == MM_AF_INET6 is invalid.");
    inet_ntop(MM_AF_INET6, &addr_inetv6->sin6_addr, node_string, MM_NODE_NAME_LENGTH);
    mmSprintf(addr_name, "%s-%u", node_string, mmNtohs(addr_inetv6->sin6_port));
}

static void __static_mmSockaddr_UnspecPort(struct mmSockaddr* p, mmUShort_t* port)
{
    char node_string[MM_NODE_NAME_LENGTH] = { 0 };
    char port_string[MM_PORT_NAME_LENGTH] = { 0 };
    struct addrinfo addr_info;
    assert(NULL != port && "NULL != port is invalid.");
    mmMemset(&addr_info, 0, sizeof(struct addrinfo));
    addr_info.ai_addrlen = sizeof(struct mmSockaddr);
    addr_info.ai_addr = (struct sockaddr*)p;
    getnameinfo(addr_info.ai_addr, (socklen_t)addr_info.ai_addrlen, node_string, MM_NODE_NAME_LENGTH, port_string, MM_PORT_NAME_LENGTH, addr_info.ai_flags);
    *port = mmAtoi32(port_string);
}
static void __static_mmSockaddr_Inetv4Port(struct mmSockaddr* p, mmUShort_t* port)
{
    struct sockaddr_in* addr_inetv4 = (struct sockaddr_in*)(p);
    assert(NULL != port && "NULL != port is invalid.");
    assert(p->addr.sa_family == MM_AF_INET4 && "addr->addr.sa_family == MM_AF_INET4 is invalid.");
    *port = mmNtohs(addr_inetv4->sin_port);
}
static void __static_mmSockaddr_Inetv6Port(struct mmSockaddr* p, mmUShort_t* port)
{
    struct sockaddr_in6* addr_inetv6 = (struct sockaddr_in6*)(p);
    assert(NULL != port && "NULL != port is invalid.");
    assert(p->addr.sa_family == MM_AF_INET6 && "addr->addr.sa_family == MM_AF_INET6 is invalid.");
    *port = mmNtohs(addr_inetv6->sin6_port);
}
static void __static_mmSockaddr_UnspecNodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port)
{
    char port_string[MM_PORT_NAME_LENGTH] = { 0 };
    struct addrinfo addr_info;
    mmMemset(&addr_info, 0, sizeof(struct addrinfo));
    addr_info.ai_addrlen = sizeof(struct mmSockaddr);
    addr_info.ai_addr = (struct sockaddr*)p;
    getnameinfo(addr_info.ai_addr, (socklen_t)addr_info.ai_addrlen, node, MM_NODE_NAME_LENGTH, port_string, MM_PORT_NAME_LENGTH, addr_info.ai_flags);
    *port = mmAtoi32(port_string);
}
static void __static_mmSockaddr_Inetv4NodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port)
{
    struct sockaddr_in* addr_inetv4 = (struct sockaddr_in*)(p);
    assert(p->addr.sa_family == MM_AF_INET4 && "addr->addr.sa_family == MM_AF_INET4 is invalid.");
    inet_ntop(MM_AF_INET4, &addr_inetv4->sin_addr, node, MM_NODE_NAME_LENGTH);
    *port = mmNtohs(addr_inetv4->sin_port);
}
static void __static_mmSockaddr_Inetv6NodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port)
{
    struct sockaddr_in6* addr_inetv6 = (struct sockaddr_in6*)(p);
    assert(p->addr.sa_family == MM_AF_INET6 && "addr->addr.sa_family == MM_AF_INET6 is invalid.");
    inet_ntop(MM_AF_INET6, &addr_inetv6->sin6_addr, node, MM_NODE_NAME_LENGTH);
    *port = mmNtohs(addr_inetv6->sin6_port);
}
