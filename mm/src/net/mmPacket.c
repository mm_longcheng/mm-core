/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPacket.h"
#include "core/mmAlloc.h"
#include "core/mmByteswap.h"

MM_EXPORT_NET void mmPacketHead_Init(struct mmPacketHead* p)
{
    p->mid = 0;// msg     id.
    p->pid = 0;// proxy   id.
    p->sid = 0;// section id.
    p->uid = 0;// unique  id.
}
MM_EXPORT_NET void mmPacketHead_Destroy(struct mmPacketHead* p)
{
    p->mid = 0;// msg     id.
    p->pid = 0;// proxy   id.
    p->sid = 0;// section id.
    p->uid = 0;// unique  id.
}
MM_EXPORT_NET void mmPacketHead_Reset(struct mmPacketHead* p)
{
    p->mid = 0;// msg     id.
    p->pid = 0;// proxy   id.
    p->sid = 0;// section id.
    p->uid = 0;// unique  id.
}

MM_EXPORT_NET void mmPacket_Init(struct mmPacket* p)
{
    mmPacketHead_Init(&p->phead);
    mmByteBuffer_Init(&p->hbuff);
    mmByteBuffer_Init(&p->bbuff);
}
MM_EXPORT_NET void mmPacket_Destroy(struct mmPacket* p)
{
    mmPacketHead_Destroy(&p->phead);
    mmByteBuffer_Destroy(&p->hbuff);
    mmByteBuffer_Destroy(&p->bbuff);
}

// reset all to zero.
MM_EXPORT_NET void mmPacket_Reset(struct mmPacket* p)
{
    mmPacketHead_Reset(&p->phead);
    mmByteBuffer_Reset(&p->hbuff);
    mmByteBuffer_Reset(&p->bbuff);
}
// use for quick zero head base.(mid,uid,sid,pid)
MM_EXPORT_NET void mmPacket_HeadBaseZero(struct mmPacket* p)
{
    mmPacketHead_Reset(&p->phead);
}
// use for quick copy head base r-->p.(mid,uid,sid,pid)
MM_EXPORT_NET void mmPacket_HeadBaseCopy(struct mmPacket* p, struct mmPacket* r)
{
    mmMemcpy(&p->phead, &r->phead, sizeof(struct mmPacketHead));
}
// decode p phead to hbuff;
MM_EXPORT_NET void mmPacket_HeadDecode(struct mmPacket* p, struct mmByteBuffer* hbuff, struct mmPacketHead* phead)
{
    mmMemcpy(phead, (void*)(hbuff->buffer + hbuff->offset), hbuff->length);
    p->phead.mid = mmLtoH32(phead->mid);
    p->phead.pid = mmLtoH32(phead->pid);
    p->phead.sid = mmLtoH64(phead->sid);
    p->phead.uid = mmLtoH64(phead->uid);
}
// decode hbuff phead to p;
MM_EXPORT_NET void mmPacket_HeadEncode(struct mmPacket* p, struct mmByteBuffer* hbuff, struct mmPacketHead* phead)
{
    phead->mid = mmHtoL32(p->phead.mid);
    phead->pid = mmHtoL32(p->phead.pid);
    phead->sid = mmHtoL64(p->phead.sid);
    phead->uid = mmHtoL64(p->phead.uid);
    mmMemcpy((void*)(hbuff->buffer + hbuff->offset), phead, hbuff->length);
}

MM_EXPORT_NET void mmPacket_CopyRealloc(struct mmPacket* p, struct mmPacket* t)
{
    assert(NULL != t->hbuff.buffer && "t->hbuff.buffer is null.");
    assert(NULL != t->bbuff.buffer && "t->bbuff.buffer is null.");
    t->hbuff.buffer = (mmUInt8_t*)mmRealloc(t->hbuff.buffer, p->hbuff.length);
    t->bbuff.buffer = (mmUInt8_t*)mmRealloc(t->bbuff.buffer, p->bbuff.length);
    mmPacket_CopyWeak(p, t);
}
// copy data from p to t.will alloc the hbuff and bbuff.
MM_EXPORT_NET void mmPacket_CopyAlloc(struct mmPacket* p, struct mmPacket* t)
{
    assert(NULL == t->hbuff.buffer && "t->hbuff.buffer is not null.");
    assert(NULL == t->bbuff.buffer && "t->bbuff.buffer is not null.");
    t->hbuff.buffer = (mmUInt8_t*)mmMalloc(p->hbuff.length);
    t->bbuff.buffer = (mmUInt8_t*)mmMalloc(p->bbuff.length);
    mmPacket_CopyWeak(p, t);
}
MM_EXPORT_NET void mmPacket_CopyWeak(struct mmPacket* p, struct mmPacket* t)
{
    struct mmPacketHead phead;
    t->hbuff.length = p->hbuff.length;
    t->bbuff.length = p->bbuff.length;
    mmMemcpy(t->hbuff.buffer + t->hbuff.offset, p->hbuff.buffer + p->hbuff.offset, p->hbuff.length);
    mmMemcpy(t->bbuff.buffer + t->bbuff.offset, p->bbuff.buffer + p->bbuff.offset, p->bbuff.length);
    mmPacket_HeadDecode(t, &t->hbuff, &phead);
}
// free copy realloc data from.
MM_EXPORT_NET void mmPacket_FreeCopyRealloc(struct mmPacket* p)
{
    mmFree(p->hbuff.buffer);
    mmFree(p->bbuff.buffer);
    mmPacket_Reset(p);
}
// free copy alloc data from.
MM_EXPORT_NET void mmPacket_FreeCopyAlloc(struct mmPacket* p)
{
    mmFree(p->hbuff.buffer);
    mmFree(p->bbuff.buffer);
    mmPacket_Reset(p);
}
MM_EXPORT_NET void mmPacket_ShadowAlloc(struct mmPacket* p)
{
    assert(NULL == p->hbuff.buffer && "p->hbuff.buffer is not null.");
    assert(NULL == p->bbuff.buffer && "p->bbuff.buffer is not null.");
    p->hbuff.buffer = (mmUInt8_t*)mmMalloc(p->hbuff.length);
    p->bbuff.buffer = (mmUInt8_t*)mmMalloc(p->bbuff.length);
}
MM_EXPORT_NET void mmPacket_ShadowFree(struct mmPacket* p)
{
    mmFree(p->hbuff.buffer);
    mmFree(p->bbuff.buffer);
}
