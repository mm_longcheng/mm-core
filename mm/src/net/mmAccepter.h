/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAccepter_h__
#define __mmAccepter_h__

#include "core/mmCore.h"
#include "core/mmThread.h"
#include "core/mmTime.h"

#include "net/mmNetExport.h"
#include "net/mmSocket.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// listen backlog default 5;
#define MM_ACCEPTER_BACKLOG 5
// if bind listen failure will msleep this time.
#define MM_ACCEPTER_TRY_MSLEEP_TIME 1000
// net accept nonblock timeout..
#define MM_ACCEPTER_NONBLOCK_TIMEOUT 1000
// net accept one time try time.
#define MM_NET_ACCEPTER_TRYTIME -1

struct mmAccepterCallback
{
    void(*Accept)(void* obj, mmSocket_t fd, struct mmSockaddr* remote);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmAccepterCallback_Init(struct mmAccepterCallback* p);
MM_EXPORT_NET void mmAccepterCallback_Destroy(struct mmAccepterCallback* p);

enum mmAccepterState_t
{
    MM_ACCEPTER_STATE_CLOSED = 0,// fd is closed,connect closed.invalid at all.
    MM_ACCEPTER_STATE_MOTION = 1,// fd is opened,connect closed.at connecting.
    MM_ACCEPTER_STATE_FINISH = 2,// fd is opened,connect opened.at connected.
    MM_ACCEPTER_STATE_BROKEN = 3,// fd is opened,connect broken.connect is broken fd not closed.
};
// we not use select timeout for accept interrupt.
// we just use close listen socket fd and trigger a socket errno 
// [errno:(53) Software caused connection abort] to interrupt accept.
// this mode will make this class simpleness and negative effects receivability.
struct mmAccepter
{
    // strong ref. accepter socket.
    struct mmSocket socket;
    // value ref. accepter callback.
    struct mmAccepterCallback callback;
    // listen backlog,default MM_ACCEPTER_BACKLOG.
    mmInt_t backlog;
    // thread.
    pthread_t accept_thread;
    // accepter_keepalive_activate true accepter_keepalive_inactive false keepalive the accept fd.
    // default is accepter_keepalive_activate.
    int keepalive;
    // tcp nonblock timeout.
    // default is MM_ACCEPTER_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;
    // mmAccepterState_t, default is MM_ACCEPTER_STATE_CLOSED(0)
    int accepter_state;
    // try bind and listen times.default is -1.
    mmUInt32_t trytimes;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_NET void mmAccepter_Init(struct mmAccepter* p);
MM_EXPORT_NET void mmAccepter_Destroy(struct mmAccepter* p);

/* locker order is
*     socket
*/
MM_EXPORT_NET void mmAccepter_Lock(struct mmAccepter* p);
MM_EXPORT_NET void mmAccepter_Unlock(struct mmAccepter* p);

// assign native address but not connect.
MM_EXPORT_NET void mmAccepter_SetNative(struct mmAccepter* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmAccepter_SetRemote(struct mmAccepter* p, const char* node, mmUShort_t port);

MM_EXPORT_NET void mmAccepter_SetCallback(struct mmAccepter* p, struct mmAccepterCallback* cb);

// reset streambuf and fire event broken.
MM_EXPORT_NET void mmAccepter_ApplyBroken(struct mmAccepter* p);
// reset streambuf and fire event broken.
MM_EXPORT_NET void mmAccepter_ApplyFinish(struct mmAccepter* p);

// fopen socket.ss_native.ss_family,SOCK_STREAM,0
MM_EXPORT_NET void mmAccepter_FopenSocket(struct mmAccepter* p);
// bind address.
MM_EXPORT_NET void mmAccepter_Bind(struct mmAccepter* p);
// listen address.
MM_EXPORT_NET void mmAccepter_Listen(struct mmAccepter* p);
// accept sync.
MM_EXPORT_NET void mmAccepter_Accept(struct mmAccepter* p);
// close socket. mmAccepter_Join before call this.
MM_EXPORT_NET void mmAccepter_CloseSocket(struct mmAccepter* p);

// get current bind sockaddr, and cache into ss_native.
MM_EXPORT_NET void mmAccepter_CurrentBindSockaddr(struct mmAccepter* p);

// start accept thread.
MM_EXPORT_NET void mmAccepter_Start(struct mmAccepter* p);
// interrupt accept thread.
MM_EXPORT_NET void mmAccepter_Interrupt(struct mmAccepter* p);
// shutdown accept thread.
MM_EXPORT_NET void mmAccepter_Shutdown(struct mmAccepter* p);
// join accept thread.
MM_EXPORT_NET void mmAccepter_Join(struct mmAccepter* p);

#include "core/mmSuffix.h"

#endif//__mmAccepter_h__
