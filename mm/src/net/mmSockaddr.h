/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSockaddr_h__
#define __mmSockaddr_h__

#include "core/mmCore.h"
#include "core/mmOSSocket.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

#define MM_ADDR_DEFAULT_NODE "::"
#define MM_ADDR_DEFAULT_PORT 0

#define MM_NODE_NAME_LENGTH 64
#define MM_PORT_NAME_LENGTH 8
#define MM_SOCK_NAME_LENGTH 16
#define MM_ADDR_NAME_LENGTH 64
#define MM_LINK_NAME_LENGTH 128

// A data struct for only ipv4 and ipv6.
// We not use the struct sockaddr_storage.It's so big.
struct mmSockaddr
{
    union
    {
        // impl addr
        struct sockaddr     addr;
        // impl ipv4
        struct sockaddr_in  ipv4;
        // impl ipv6
        struct sockaddr_in6 ipv6;
    };
};
MM_EXPORT_NET void mmSockaddr_Init(struct mmSockaddr* p);
MM_EXPORT_NET void mmSockaddr_Destroy(struct mmSockaddr* p);

// copy t to p.
MM_EXPORT_NET void mmSockaddr_CopyFrom(struct mmSockaddr* p, struct mmSockaddr* t);
// reset.
MM_EXPORT_NET void mmSockaddr_Reset(struct mmSockaddr* p);
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_Assign(struct mmSockaddr* p, const char* node, mmUShort_t port);
// 2001:0DB8:0000:0023:0008:0800:200C:417A 64 38
// 65535                                   8  5
// addr_name:
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_ToString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH]);

// (fd)|ip-port --> ip-port
// ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
MM_EXPORT_NET void
mmSockaddr_NativeRemoteString(
    struct mmSockaddr* native_addr,
    struct mmSockaddr* remote_addr,
    mmSocket_t fd,
    char link_name[MM_LINK_NAME_LENGTH]);

// 0 is same.
MM_EXPORT_NET int mmSockaddr_Compare(struct mmSockaddr* p, struct mmSockaddr* r);
// 0 is same.
MM_EXPORT_NET int mmSockaddr_CompareAddress(struct mmSockaddr* p, const char* node, mmUShort_t port);

// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_DecodeString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH]);
// encode to string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_EncodeString(struct mmSockaddr* p, char addr_name[MM_ADDR_NAME_LENGTH]);

// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_NET void mmSockaddr_DecodeZeroString(struct mmSockaddr* p, const char* node);
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535 =>      ::-0
// ipv4                         192.168.111.123-65535 => 0.0.0.0-0
MM_EXPORT_NET void mmSockaddr_DecodeZero(struct mmSockaddr* p, const char* node, mmUShort_t port);

// calculate the socket real length for this function.
// connect bind sendto
// for linux windows use the sizeof(struct sockaddr_storage) os not checking and anything correct.
// for freebsd must use the real sockaddr length.
// if not, will case errno(EAFNOSUPPORT) Address family not supported by protocol family.
MM_EXPORT_NET socklen_t mmSockaddr_Length(struct mmSockaddr* p);
// port for mmSockaddr.note the type must the ipv4 or ipv6.
MM_EXPORT_NET mmUShort_t mmSockaddr_Port(struct mmSockaddr* p);

// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void mmSockaddr_NodePort(struct mmSockaddr* p, char node[MM_NODE_NAME_LENGTH], mmUShort_t* port);

MM_EXPORT_NET void mmSockaddrStorage_Init(struct sockaddr_storage* p);
MM_EXPORT_NET void mmSockaddrStorage_Destroy(struct sockaddr_storage* p);

// decode by string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void
mmSockaddr_FormatDecodeString(
    char addr_name[MM_ADDR_NAME_LENGTH],
    char node[MM_NODE_NAME_LENGTH],
    mmUShort_t* port);

// encode to string.
// ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
// ipv4                         192.168.111.123-65535
MM_EXPORT_NET void
mmSockaddr_FormatEncodeString(
    char addr_name[MM_ADDR_NAME_LENGTH],
    char node[MM_NODE_NAME_LENGTH],
    mmUShort_t* port);

// family only support ipv4 ipv6.
MM_EXPORT_NET int mmSockaddr_NodeFamily(const char* node);

#include "core/mmSuffix.h"

#endif//__mmSockaddr_h__
