/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMessageCoder.h"
#include "net/mmStreambufPacket.h"

#include "core/mmAlloc.h"

static size_t __static_mmMessageCoder_LengthDefault(void* message)
{
    return 0;
}
static int __static_mmMessageCoder_DecodeDefault(void* message, struct mmByteBuffer* byte_buffer)
{
    return 0;
}
static int __static_mmMessageCoder_EncodeDefault(void* message, struct mmByteBuffer* byte_buffer)
{
    return 0;
}

MM_EXPORT_NET void mmMessageCoder_Init(struct mmMessageCoder* p)
{
    p->Length = &__static_mmMessageCoder_LengthDefault;
    p->Decode = &__static_mmMessageCoder_DecodeDefault;
    p->Encode = &__static_mmMessageCoder_EncodeDefault;
}
MM_EXPORT_NET void mmMessageCoder_Destroy(struct mmMessageCoder* p)
{
    p->Length = &__static_mmMessageCoder_LengthDefault;
    p->Decode = &__static_mmMessageCoder_DecodeDefault;
    p->Encode = &__static_mmMessageCoder_EncodeDefault;
}

// struct mmByteBuffer
static size_t __static_mmMessageCoder_ByteBufferLength(void* message)
{
    struct mmByteBuffer* message_byte_buffer = (struct mmByteBuffer*)message;
    return message_byte_buffer->length;
}
static int __static_mmMessageCoder_ByteBufferDecode(void* message, struct mmByteBuffer* byte_buffer)
{
    struct mmByteBuffer* message_byte_buffer = (struct mmByteBuffer*)message;
    void* src = (void*)(byte_buffer->buffer + byte_buffer->offset);
    void* dst = (void*)(message_byte_buffer->buffer + message_byte_buffer->offset);
    mmMemcpy(dst, src, byte_buffer->length);
    return 0;
}
static int __static_mmMessageCoder_ByteBufferEncode(void* message, struct mmByteBuffer* byte_buffer)
{
    struct mmByteBuffer* message_byte_buffer = (struct mmByteBuffer*)message;
    void* src = (void*)(message_byte_buffer->buffer + message_byte_buffer->offset);
    void* dst = (void*)(byte_buffer->buffer + byte_buffer->offset);
    mmMemcpy(dst, src, byte_buffer->length);
    return 0;
}
static struct mmMessageCoder g_mmMessageCoder_ByteBuffer =
{
    &__static_mmMessageCoder_ByteBufferLength,
    &__static_mmMessageCoder_ByteBufferDecode,
    &__static_mmMessageCoder_ByteBufferEncode,
};

// struct mmStreambuf
static size_t __static_mmMessageCoder_StreambufLength(void* message)
{
    struct mmStreambuf* message_streambuf = (struct mmStreambuf*)message;
    return (size_t)mmStreambuf_Size(message_streambuf);
}
static int __static_mmMessageCoder_StreambufDecode(void* message, struct mmByteBuffer* byte_buffer)
{
    struct mmStreambuf* message_streambuf = (struct mmStreambuf*)message;
    void* src = (void*)(byte_buffer->buffer + byte_buffer->offset);
    void* dst = (void*)(message_streambuf->buff + message_streambuf->gptr);
    mmMemcpy(dst, src, byte_buffer->length);
    return 0;
}
static int __static_mmMessageCoder_StreambufEncode(void* message, struct mmByteBuffer* byte_buffer)
{
    struct mmStreambuf* message_streambuf = (struct mmStreambuf*)message;
    void* src = (void*)(message_streambuf->buff + message_streambuf->gptr);
    void* dst = (void*)(byte_buffer->buffer + byte_buffer->offset);
    mmMemcpy(dst, src, byte_buffer->length);
    return 0;
}
static struct mmMessageCoder g_mmMessageCoder_Streambuf =
{
    &__static_mmMessageCoder_StreambufLength,
    &__static_mmMessageCoder_StreambufDecode,
    &__static_mmMessageCoder_StreambufEncode,
};

// struct mmString
static size_t __static_mmMessageCoder_StringLength(void* message)
{
    struct mmString* message_string = (struct mmString*)message;
    return (size_t)mmString_Size(message_string);
}
static int __static_mmMessageCoder_StringDecode(void* message, struct mmByteBuffer* byte_buffer)
{
    struct mmString* message_string = (struct mmString*)message;
    void* src = (void*)(byte_buffer->buffer + byte_buffer->offset);
    void* dst = (void*)(mmString_Data(message_string));
    mmMemcpy(dst, src, byte_buffer->length);
    return 0;
}
static int __static_mmMessageCoder_StringEncode(void* message, struct mmByteBuffer* byte_buffer)
{
    struct mmString* message_string = (struct mmString*)message;
    void* src = (void*)(mmString_Data(message_string));
    void* dst = (void*)(byte_buffer->buffer + byte_buffer->offset);
    mmMemcpy(dst, src, byte_buffer->length);
    return 0;
}
static struct mmMessageCoder g_mmMessageCoder_String =
{
    &__static_mmMessageCoder_StringLength,
    &__static_mmMessageCoder_StringDecode,
    &__static_mmMessageCoder_StringEncode,
};

MM_EXPORT_NET struct mmMessageCoder* mmMessageCoder_ByteBuffer(void)
{
    return &g_mmMessageCoder_ByteBuffer;
}
MM_EXPORT_NET struct mmMessageCoder* mmMessageCoder_Streambuf(void)
{
    return &g_mmMessageCoder_Streambuf;
}
MM_EXPORT_NET struct mmMessageCoder* mmMessageCoder_String(void)
{
    return &g_mmMessageCoder_String;
}

MM_EXPORT_NET void
mmMessageCoder_StreambufOverdraft(
    struct mmStreambuf* streambuf,
    size_t hlength,
    size_t blength,
    struct mmPacket* pack)
{
    // message encode length
    pack->hbuff.length = hlength;
    pack->bbuff.length = blength;
    // alloc message encode buffer
    mmStreambufPacket_Overdraft(streambuf, pack);
}

MM_EXPORT_NET int
mmMessageCoder_AppendImplPacketHead(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int rt = -1;
    struct mmPacketHead phead;
    // message packet head init.
    mmPacket_HeadBaseZero(rq_pack);
    // message packet head assignment
    rq_pack->phead = *packet_head;
    // message packet head encode
    mmPacket_HeadEncode(rq_pack, &rq_pack->hbuff, &phead);
    // encode message
    rt = (*(coder->Encode))(rq_msg, &rq_pack->bbuff);
    return rt;
}

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadZero(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt64_t uid,
    mmUInt32_t mid,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int rt = -1;
    struct mmPacketHead phead;
    // message packet head init.
    mmPacket_HeadBaseZero(rq_pack);
    // message packet head assignment
    rq_pack->phead.mid = mid;
    rq_pack->phead.pid = 0;
    rq_pack->phead.sid = 0;
    rq_pack->phead.uid = uid;
    // message packet head encode
    mmPacket_HeadEncode(rq_pack, &rq_pack->hbuff, &phead);
    // encode message
    rt = (*(coder->Encode))(rq_msg, &rq_pack->bbuff);
    return rt;
}

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadCopy(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt32_t mid,
    void* rs_msg,
    struct mmPacket* rq_pack,
    struct mmPacket* rs_pack)
{
    int rt = -1;
    struct mmPacketHead phead;
    // message packet head init.
    mmPacket_HeadBaseCopy(rs_pack, rq_pack);
    // message packet head assignment
    rs_pack->phead.mid = mid;
    // message packet head encode
    mmPacket_HeadEncode(rs_pack, &rs_pack->hbuff, &phead);
    // encode message
    rt = (*(coder->Encode))(rs_msg, &rs_pack->bbuff);
    return rt;
}

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadCopyProxyRq(
    struct mmStreambuf* streambuf,
    mmUInt32_t pid,
    mmUInt64_t sid,
    struct mmPacket* rq_pack,
    struct mmPacket* pr_pack)
{
    struct mmPacketHead phead;
    // message packet head init.
    mmPacket_HeadBaseCopy(pr_pack, rq_pack);
    // message packet head assignment
    pr_pack->phead.pid = pid;
    pr_pack->phead.sid = sid;
    // message packet head encode
    mmPacket_HeadEncode(pr_pack, &pr_pack->hbuff, &phead);
    // encode message
    mmMemcpy(
             (void*)(pr_pack->bbuff.buffer + pr_pack->bbuff.offset),
             (void*)(rq_pack->bbuff.buffer + rq_pack->bbuff.offset),
             rq_pack->bbuff.length);
    return 0;
}

MM_EXPORT_NET int
mmMessageCoder_AppendImplHeadCopyProxyRs(
    struct mmStreambuf* streambuf,
    struct mmPacket* rs_pack,
    struct mmPacket* pr_pack)
{
    struct mmPacketHead phead;
    // message packet head init.
    mmPacket_HeadBaseCopy(pr_pack, rs_pack);
    // message packet head assignment
    // message packet head encode
    mmPacket_HeadEncode(pr_pack, &pr_pack->hbuff, &phead);
    // encode message
    mmMemcpy(
             (void*)(pr_pack->bbuff.buffer + pr_pack->bbuff.offset),
             (void*)(rs_pack->bbuff.buffer + rs_pack->bbuff.offset),
             rs_pack->bbuff.length);
    return 0;
}

MM_EXPORT_NET int
mmMessageCoder_AppendPacketHead(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    size_t blength = (*(coder->Length))(rq_msg);
    mmMessageCoder_StreambufOverdraft(streambuf, hlength, blength, rq_pack);
    return mmMessageCoder_AppendImplPacketHead(streambuf, coder, packet_head, rq_msg, hlength, rq_pack);
}

MM_EXPORT_NET int
mmMessageCoder_AppendHeadZero(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt64_t uid,
    mmUInt32_t mid,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    size_t blength = (*(coder->Length))(rq_msg);
    mmMessageCoder_StreambufOverdraft(streambuf, hlength, blength, rq_pack);
    return mmMessageCoder_AppendImplHeadZero(streambuf, coder, uid, mid, rq_msg, hlength, rq_pack);
}

MM_EXPORT_NET int
mmMessageCoder_AppendHeadCopy(
    struct mmStreambuf* streambuf,
    struct mmMessageCoder* coder,
    mmUInt32_t mid,
    void* rs_msg,
    struct mmPacket* rq_pack,
    struct mmPacket* rs_pack)
{
    size_t hlength = rq_pack->hbuff.length;
    size_t blength = (*(coder->Length))(rs_msg);
    mmMessageCoder_StreambufOverdraft(streambuf, hlength, blength, rs_pack);
    return mmMessageCoder_AppendImplHeadCopy(streambuf, coder, mid, rs_msg, rq_pack, rs_pack);
}

MM_EXPORT_NET int
mmMessageCoder_AppendHeadCopyProxyRq(
    struct mmStreambuf* streambuf,
    mmUInt32_t pid,
    mmUInt64_t sid,
    struct mmPacket* rq_pack,
    struct mmPacket* pr_pack)
{
    size_t hlength = rq_pack->hbuff.length;
    size_t blength = rq_pack->bbuff.length;
    mmMessageCoder_StreambufOverdraft(streambuf, hlength, blength, pr_pack);
    return mmMessageCoder_AppendImplHeadCopyProxyRq(streambuf, pid, sid, rq_pack, pr_pack);
}

MM_EXPORT_NET int
mmMessageCoder_AppendHeadCopyProxyRs(
    struct mmStreambuf* streambuf,
    struct mmPacket* rs_pack,
    struct mmPacket* pr_pack)
{
    size_t hlength = rs_pack->hbuff.length;
    size_t blength = rs_pack->bbuff.length;
    mmMessageCoder_StreambufOverdraft(streambuf, hlength, blength, pr_pack);
    return mmMessageCoder_AppendImplHeadCopyProxyRs(streambuf, rs_pack, pr_pack);
}

MM_EXPORT_NET int
mmMessageCoder_EncodeMessageByteBuffer(
    struct mmStreambuf* streambuf,
    struct mmByteBuffer* message,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmPacket* pack)
{
    return mmMessageCoder_AppendPacketHead(streambuf, &g_mmMessageCoder_ByteBuffer, packet_head, message, hlength, pack);
}

MM_EXPORT_NET int
mmMessageCoder_EncodeMessageStreambuf(
    struct mmStreambuf* streambuf,
    struct mmStreambuf* message,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmPacket* pack)
{
    return mmMessageCoder_AppendPacketHead(streambuf, &g_mmMessageCoder_Streambuf, packet_head, message, hlength, pack);
}

MM_EXPORT_NET int
mmMessageCoder_EncodeMessageString(
    struct mmStreambuf* streambuf,
    struct mmString* message,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmPacket* pack)
{
    return mmMessageCoder_AppendPacketHead(streambuf, &g_mmMessageCoder_String, packet_head, message, hlength, pack);
}
