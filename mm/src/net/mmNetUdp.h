/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNetUdp_h__
#define __mmNetUdp_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"
#include "core/mmTimerTask.h"

#include "net/mmNetExport.h"
#include "net/mmUdp.h"
#include "net/mmSelector.h"
#include "net/mmCrypto.h"
#include "net/mmPacket.h"

#include "container/mmRbtreeU32.h"

#include "core/mmPrefix.h"

// net udp nonblock timeout..
#define MM_NETUDP_NONBLOCK_TIMEOUT 20
// if bind listen failure will msleep this time.
#define MM_NETUDP_TRY_MSLEEP_TIME 1000
// net udp one time try time.
#define MM_NETUDP_TRYTIME 3
// net udp block time, default is 10 ms.
#define MM_NETUDP_BLOCK_TIME 10

typedef void(*mmNetUdpHandleFunc)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

struct mmNetUdpCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
    void(*Broken)(void* obj);
    void(*Nready)(void* obj);
    void(*Finish)(void* obj);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmNetUdpCallback_Init(struct mmNetUdpCallback* p);
MM_EXPORT_NET void mmNetUdpCallback_Destroy(struct mmNetUdpCallback* p);

struct mmNetUdpEventUdpAllocator
{
    // event udp produce and add rbtree.
    void(*EventUdpProduce)(void* p, struct mmUdp* udp);
    // event udp recycle and rmv rbtree.
    void(*EventUdpRecycle)(void* p, struct mmUdp* udp);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmNetUdpEventUdpAllocator_Init(struct mmNetUdpEventUdpAllocator* p);
MM_EXPORT_NET void mmNetUdpEventUdpAllocator_Destroy(struct mmNetUdpEventUdpAllocator* p);

enum mmNetUdpState_t
{
    MM_NETUDP_STATE_CLOSED = 0,// fd is closed, connect closed. invalid at all.
    MM_NETUDP_STATE_MOTION = 1,// fd is opened, connect closed. at connecting.
    MM_NETUDP_STATE_FINISH = 2,// fd is opened, connect opened. at connected.
    MM_NETUDP_STATE_BROKEN = 3,// fd is opened, connect broken. connect is broken fd not closed.
};

struct mmNetUdp
{
    // strong ref.
    struct mmUdp udp;
    // strong ref.
    struct mmSelector selector;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    // udp callback handle implement.
    struct mmUdpCallback udp_callback;
    // value ref. udp callback.
    struct mmNetUdpCallback callback;
    struct mmNetUdpEventUdpAllocator event_udp_allocator;
    // crypto callback.
    struct mmCryptoCallback crypto_callback;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;

    // udp nonblock timeout. default is MM_NETUDP_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;

    // mmNetUdpState_t, default is udp_state_closed(0)
    int udp_state;

    // try bind and listen times. default is MM_NETUDP_TRYTIME.
    mmUInt32_t trytimes;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmNetUdp_Init(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_Destroy(struct mmNetUdp* p);

// streambuf reset.
MM_EXPORT_NET void mmNetUdp_ResetStreambuf(struct mmNetUdp* p);

/* locker order is
*     udp
*     selector
*     locker
*/
MM_EXPORT_NET void mmNetUdp_Lock(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_Unlock(struct mmNetUdp* p);
// s locker. 
MM_EXPORT_NET void mmNetUdp_SLock(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_SUnlock(struct mmNetUdp* p);
// i locker
MM_EXPORT_NET void mmNetUdp_ILock(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_IUnlock(struct mmNetUdp* p);
// o locker
MM_EXPORT_NET void mmNetUdp_OLock(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_OUnlock(struct mmNetUdp* p);

// assign native address but not connect.
MM_EXPORT_NET void mmNetUdp_SetNative(struct mmNetUdp* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmNetUdp_SetRemote(struct mmNetUdp* p, const char* node, mmUShort_t port);
// assign addr native by sockaddr_storage. AF_INET; after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmNetUdp_SetNativeStorage(struct mmNetUdp* p, struct mmSockaddr* ss_native);
// assign addr remote by sockaddr_storage. AF_INET; after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmNetUdp_SetRemoteStorage(struct mmNetUdp* p, struct mmSockaddr* ss_remote);

MM_EXPORT_NET void mmNetUdp_SetHandle(struct mmNetUdp* p, mmUInt32_t id, mmNetUdpHandleFunc callback);
MM_EXPORT_NET void mmNetUdp_SetNetUdpCallback(struct mmNetUdp* p, struct mmNetUdpCallback* net_udp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetUdp_SetUdpCallback(struct mmNetUdp* p, struct mmUdpCallback* udp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetUdp_SetEventUdpAllocator(struct mmNetUdp* p, struct mmNetUdpEventUdpAllocator* event_udp_allocator);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetUdp_SetCryptoCallback(struct mmNetUdp* p, struct mmCryptoCallback* crypto_callback);
// assign context handle.
MM_EXPORT_NET void mmNetUdp_SetContext(struct mmNetUdp* p, void* u);
// assign nonblock_timeout.
MM_EXPORT_NET void mmNetUdp_SetNonblockTimeout(struct mmNetUdp* p, mmMSec_t nonblock_timeout);
// assign trytimes.
MM_EXPORT_NET void mmNetUdp_SetTrytimes(struct mmNetUdp* p, mmUInt32_t trytimes);

// context for udp will use the mm_addr.u attribute. such as crypto context.
MM_EXPORT_NET void mmNetUdp_SetAddrContext(struct mmNetUdp* p, void* u);
MM_EXPORT_NET void* mmNetUdp_GetAddrContext(struct mmNetUdp* p);

MM_EXPORT_NET void mmNetUdp_ClearHandleRbtree(struct mmNetUdp* p);

// net_udp crypto encrypt buffer by udp.
MM_EXPORT_NET void mmNetUdp_CryptoEncrypt(struct mmNetUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length);
// net_udp crypto decrypt buffer by udp.
MM_EXPORT_NET void mmNetUdp_CryptoDecrypt(struct mmNetUdp* p, struct mmUdp* udp, mmUInt8_t* buffer, size_t offset, size_t length);

// fopen socket.
MM_EXPORT_NET void mmNetUdp_FopenSocket(struct mmNetUdp* p);
// bind.
MM_EXPORT_NET void mmNetUdp_Bind(struct mmNetUdp* p);
// close socket.
MM_EXPORT_NET void mmNetUdp_CloseSocket(struct mmNetUdp* p);
// shutdown socket.
MM_EXPORT_NET void mmNetUdp_ShutdownSocket(struct mmNetUdp* p, int opcode);
// set socket block.
MM_EXPORT_NET void mmNetUdp_SetBlock(struct mmNetUdp* p, int block);

MM_EXPORT_NET int mmNetUdp_MulcastJoin(struct mmNetUdp* p, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_NET int mmNetUdp_MulcastDrop(struct mmNetUdp* p, const char* mr_multiaddr, const char* mr_interface);

// check state.not thread safe.
MM_EXPORT_NET void mmNetUdp_Check(struct mmNetUdp* p);
// get state finally.return 0 for all regular.not thread safe.
MM_EXPORT_NET int mmNetUdp_FinallyState(struct mmNetUdp* p);

// reset streambuf and fire event broken.
MM_EXPORT_NET void mmNetUdp_ApplyBroken(struct mmNetUdp* p);
// reset streambuf and fire event finish.
MM_EXPORT_NET void mmNetUdp_ApplyFinish(struct mmNetUdp* p);
// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmNetUdp_AttachSocket(struct mmNetUdp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmNetUdp_DetachSocket(struct mmNetUdp* p);

// handle send data from buffer and buffer length.>0 is send size success -1 failure.
// note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
//     0 <  rt,means rt buffer is send,we must gbump rt size.
//     0 == rt,means the send buffer can be full.
//     0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmNetUdp_BufferSend(struct mmNetUdp* p, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote);
// handle send data by flush send buffer.
//     0 <  rt,means rt buffer is send,we must gbump rt size.
//     0 == rt,means the send buffer can be full.
//     0 >  rt,means the send process can be failure.
MM_EXPORT_NET int mmNetUdp_FlushSend(struct mmNetUdp* p, struct mmSockaddr* remote);

MM_EXPORT_NET void mmNetUdp_Start(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_Interrupt(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_Shutdown(struct mmNetUdp* p);
MM_EXPORT_NET void mmNetUdp_Join(struct mmNetUdp* p);

MM_EXPORT_NET void mmNetUdp_UdpHandlePacketCallback(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
MM_EXPORT_NET int mmNetUdp_UdpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length, struct mmSockaddr* remote);
MM_EXPORT_NET int mmNetUdp_UdpBrokenCallback(void* obj);

MM_EXPORT_NET void mmNetUdp_MidEventHandle(void* obj, mmUInt32_t mid, mmNetUdpHandleFunc callback);

#include "core/mmSuffix.h"

#endif//__mmNetUdp_h__
