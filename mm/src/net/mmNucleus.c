/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNucleus.h"
#include "core/mmLogger.h"
#include "core/mmAlloc.h"
#include "algorithm/mmWangHash.h"

MM_EXPORT_NET void mmNucleus_Init(struct mmNucleus* p)
{
    mmNuclearCallback_Init(&p->callback);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->arrays_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->length = 0;
    p->arrays = NULL;
    p->poll_length = MM_NUCLEAR_POLL_LENGTH;
    p->poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmNucleus_Destroy(struct mmNucleus* p)
{
    mmNucleus_Clear(p);
    //
    mmNuclearCallback_Destroy(&p->callback);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->arrays_locker);
    mmSpinlock_Destroy(&p->locker);
    p->poll_length = MM_NUCLEAR_POLL_LENGTH;
    p->poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
    p->state = MM_TS_CLOSED;
}

MM_EXPORT_NET void mmNucleus_Lock(struct mmNucleus* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmNucleus_Unlock(struct mmNucleus* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_NET void mmNucleus_SetLength(struct mmNucleus* p, mmUInt32_t length)
{
    mmSpinlock_Lock(&p->arrays_locker);
    if (length < p->length)
    {
        mmUInt32_t i = 0;
        struct mmNuclear* e = NULL;
        struct mmNuclear** v = NULL;
        for (i = length; i < p->length; ++i)
        {
            e = p->arrays[i];
            mmNuclear_Lock(e);
            p->arrays[i] = NULL;
            mmNuclear_Unlock(e);
            mmNuclear_Destroy(e);
            mmFree(e);
        }
        v = (struct mmNuclear**)mmMalloc(sizeof(struct mmNuclear*) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmNuclear*) * length);
        mmFree(p->arrays);
        p->arrays = v;
        p->length = length;
    }
    else if (length > p->length)
    {
        mmUInt32_t i = 0;
        struct mmNuclear* e = NULL;
        struct mmNuclear** v = NULL;
        v = (struct mmNuclear**)mmMalloc(sizeof(struct mmNuclear*) * length);
        mmMemcpy(v, p->arrays, sizeof(struct mmNuclear*) * p->length);
        mmFree(p->arrays);
        p->arrays = v;
        for (i = p->length; i < length; ++i)
        {
            e = (struct mmNuclear*)mmMalloc(sizeof(struct mmNuclear));
            mmNuclear_Init(e);
            e->poll_length = p->poll_length;
            e->poll_timeout = p->poll_timeout;
            e->state = p->state;
            mmNuclear_SetCallback(e, &p->callback);
            p->arrays[i] = e;
        }
        p->length = length;
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_NET mmUInt32_t mmNucleus_GetLength(struct mmNucleus* p)
{
    return p->length;
}
MM_EXPORT_NET void mmNucleus_SetCallback(struct mmNucleus* p, struct mmNuclearCallback* callback)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    assert(NULL != callback && "you can not assign null callback.");
    mmSpinlock_Lock(&p->arrays_locker);
    p->callback = (*callback);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_SetCallback(e, callback);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

// poll size.
MM_EXPORT_NET size_t mmNucleus_PollSize(struct mmNucleus* p)
{
    size_t sz = 0;
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        sz += mmNuclear_PollSize(e);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    return sz;
}

// wait for activation fd.
MM_EXPORT_NET void mmNucleus_PollWait(struct mmNucleus* p, mmMSec_t milliseconds)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_PollWait(e, milliseconds);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

// Poll loop.
MM_EXPORT_NET void mmNucleus_PollLoop(struct mmNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_PollLoop(e);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}

// start wait thread.
MM_EXPORT_NET void mmNucleus_Start(struct mmNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_Start(e);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// interrupt wait thread.
MM_EXPORT_NET void mmNucleus_Interrupt(struct mmNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_CLOSED;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_Interrupt(e);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// shutdown wait thread.
MM_EXPORT_NET void mmNucleus_Shutdown(struct mmNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    p->state = MM_TS_FINISH;
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_Shutdown(e);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
// join wait thread.
MM_EXPORT_NET void mmNucleus_Join(struct mmNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_Join(e);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
// traver member for nucleus.
MM_EXPORT_NET void mmNucleus_Traver(struct mmNucleus* p, mmRbtreeVisibleU32HandleFunc handle, void* u)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        mmNuclear_Traver(e, handle, u);
        mmNuclear_Unlock(e);
    }
    mmSpinlock_Unlock(&p->arrays_locker);
}
MM_EXPORT_NET void mmNucleus_Clear(struct mmNucleus* p)
{
    mmUInt32_t i = 0;
    struct mmNuclear* e = NULL;
    mmSpinlock_Lock(&p->arrays_locker);
    for (i = 0; i < p->length; ++i)
    {
        e = p->arrays[i];
        mmNuclear_Lock(e);
        p->arrays[i] = NULL;
        mmNuclear_Unlock(e);
        mmNuclear_Destroy(e);
        mmFree(e);
    }
    mmFree(p->arrays);
    p->arrays = NULL;
    p->length = 0;
    mmSpinlock_Unlock(&p->arrays_locker);
}

MM_EXPORT_NET mmUInt32_t mmNucleus_HashIndex(struct mmNucleus* p, mmSocket_t fd)
{
    // windows mmSocket_t is uint64_t.
    // linux   mmSocket_t is int.
    return (mmUInt32_t)(mmWangHash64(fd) % p->length);
}

// add u into poll_fd.
MM_EXPORT_NET void mmNucleus_FdAdd(struct mmNucleus* p, mmSocket_t fd, void* u)
{
    mmUInt32_t idx = mmNucleus_HashIndex(p, fd);
    assert(idx >= 0 && idx < p->length && "idx is out range.");
    mmNuclear_FdAdd(p->arrays[idx], fd, u);
}
// rmv u from poll_fd.
MM_EXPORT_NET void mmNucleus_FdRmv(struct mmNucleus* p, mmSocket_t fd, void* u)
{
    mmUInt32_t idx = mmNucleus_HashIndex(p, fd);
    assert(idx >= 0 && idx < p->length && "idx is out range.");
    mmNuclear_FdRmv(p->arrays[idx], fd, u);
}
// mod u event from poll wait.
MM_EXPORT_NET void mmNucleus_FdMod(struct mmNucleus* p, mmSocket_t fd, void* u, int flag)
{
    mmUInt32_t idx = mmNucleus_HashIndex(p, fd);
    assert(idx >= 0 && idx < p->length && "idx is out range.");
    mmNuclear_FdMod(p->arrays[idx], fd, u, flag);
}
// get u from poll.
MM_EXPORT_NET void* mmNucleus_FdGet(struct mmNucleus* p, mmSocket_t fd)
{
    mmUInt32_t idx = mmNucleus_HashIndex(p, fd);
    assert(idx >= 0 && idx < p->length && "idx is out range.");
    return mmNuclear_FdGet(p->arrays[idx], fd);
}

