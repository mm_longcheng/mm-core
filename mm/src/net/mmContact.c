/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmContact.h"
#include "core/mmAlloc.h"

static void __static_mmContact_EventMulTcpProduce(void* p, struct mmMulTcp* multcp)
{

}
static void __static_mmContact_EventMulTcpRecycle(void* p, struct mmMulTcp* multcp)
{

}
MM_EXPORT_NET void mmContactEventMulTcpAllocator_Init(struct mmContactEventMulTcpAllocator* p)
{
    p->EventMultcpProduce = &__static_mmContact_EventMulTcpProduce;
    p->EventMultcpRecycle = &__static_mmContact_EventMulTcpRecycle;
    p->obj = NULL;
}
MM_EXPORT_NET void mmContactEventMulTcpAllocator_Destroy(struct mmContactEventMulTcpAllocator* p)
{
    p->EventMultcpProduce = &__static_mmContact_EventMulTcpProduce;
    p->EventMultcpRecycle = &__static_mmContact_EventMulTcpRecycle;
    p->obj = NULL;
}

MM_EXPORT_NET void mmContact_Init(struct mmContact* p)
{
    struct mmRbtreeU64VptAllocator _U64VptAllocator;

    mmTcpCallback_Init(&p->tcp_callback);
    mmContactEventMulTcpAllocator_Init(&p->event_multcp_allocator);
    mmCryptoCallback_Init(&p->crypto_callback);
    mmRbtreeU64Vpt_Init(&p->rbtree);
    pthread_mutex_init(&p->signal_mutex, NULL);
    pthread_cond_init(&p->signal_cond, NULL);
    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->state = MM_TS_CLOSED;
    p->u = NULL;

    p->tcp_callback.Handle = &mmNetTcp_TcpHandleCallback;
    p->tcp_callback.Broken = &mmNetTcp_TcpBrokenCallback;
    p->tcp_callback.obj = p;

    _U64VptAllocator.Produce = &mmRbtreeU64Vpt_WeakProduce;
    _U64VptAllocator.Recycle = &mmRbtreeU64Vpt_WeakRecycle;
    mmRbtreeU64Vpt_SetAllocator(&p->rbtree, &_U64VptAllocator);
}
MM_EXPORT_NET void mmContact_Destroy(struct mmContact* p)
{
    mmContact_Clear(p);
    //
    mmTcpCallback_Destroy(&p->tcp_callback);
    mmContactEventMulTcpAllocator_Destroy(&p->event_multcp_allocator);
    mmCryptoCallback_Destroy(&p->crypto_callback);
    mmRbtreeU64Vpt_Destroy(&p->rbtree);
    pthread_mutex_destroy(&p->signal_mutex);
    pthread_cond_destroy(&p->signal_cond);
    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->locker);
    p->state = 0;
    p->u = NULL;
}

// lock to make sure the mt_contact is thread safe.
MM_EXPORT_NET void mmContact_Lock(struct mmContact* p)
{
    mmSpinlock_Lock(&p->locker);
}
// unlock mt_contact.
MM_EXPORT_NET void mmContact_Unlock(struct mmContact* p)
{
    mmSpinlock_Unlock(&p->locker);
}

// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmContact_SetEventMulTcpAllocator(struct mmContact* p, struct mmContactEventMulTcpAllocator* event_multcp_allocator)
{
    assert(NULL != event_multcp_allocator && "you can not assign null event_multcp_allocator.");
    p->event_multcp_allocator = *event_multcp_allocator;
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmContact_SetTcpCallback(struct mmContact* p, struct mmTcpCallback* tcp_callback)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    assert(NULL != tcp_callback && "tcp_callback is a null.");
    p->tcp_callback = *tcp_callback;
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_SetTcpCallback(multcp, &p->tcp_callback);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmContact_SetCryptoCallback(struct mmContact* p, struct mmCryptoCallback* crypto_callback)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    assert(NULL != crypto_callback && "crypto_callback is a null.");
    p->crypto_callback = *crypto_callback;
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_SetCryptoCallback(multcp, &p->crypto_callback);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
// assign context handle.
MM_EXPORT_NET void mmContact_SetContext(struct mmContact* p, void* u)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    p->u = u;
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_SetContext(multcp, p->u);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}

MM_EXPORT_NET void mmContact_Check(struct mmContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_Check(multcp);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}

MM_EXPORT_NET void mmContact_Start(struct mmContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_Start(multcp);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmContact_Interrupt(struct mmContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    p->state = MM_TS_CLOSED;
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_Interrupt(multcp);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_NET void mmContact_Shutdown(struct mmContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    p->state = MM_TS_FINISH;
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_Shutdown(multcp);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    //
    pthread_mutex_lock(&p->signal_mutex);
    pthread_cond_signal(&p->signal_cond);
    pthread_mutex_unlock(&p->signal_mutex);
}
MM_EXPORT_NET void mmContact_Join(struct mmContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    if (MM_TS_MOTION == p->state)
    {
        // we can not lock or join until cond wait all thread is shutdown.
        pthread_mutex_lock(&p->signal_mutex);
        pthread_cond_wait(&p->signal_cond, &p->signal_mutex);
        pthread_mutex_unlock(&p->signal_mutex);
    }
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmMulTcp_Join(multcp);
        mmMulTcp_Unlock(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}

MM_EXPORT_NET struct mmMulTcp* mmContact_Add(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port)
{
    struct mmMulTcp* multcp = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    multcp = mmContact_Get(p, unique_id);
    if (NULL == multcp)
    {
        multcp = (struct mmMulTcp*)mmMalloc(sizeof(struct mmMulTcp));
        mmMulTcp_Init(multcp);

        mmMulTcp_SetNative(multcp, node_n, port);
        mmMulTcp_SetRemote(multcp, node_r, port);
        // assign sid index.
        multcp->unique_id = unique_id;
        mmMulTcp_SetContext(multcp, p->u);
        //
        mmMulTcp_SetCryptoCallback(multcp, &p->crypto_callback);
        //
        mmMulTcp_SetTcpCallback(multcp, &p->tcp_callback);
        //
        (*(p->event_multcp_allocator.EventMultcpProduce))(p, multcp);
        //
        if (MM_TS_MOTION == p->state)
        {
            // if current mt_contact is in motion we start the new route .
            mmMulTcp_Start(multcp);
        }
        else
        {
            // if current mt_contact is not motion,we assign the same state.
            multcp->state = p->state;
        }
        //
        mmSpinlock_Lock(&p->rbtree_locker);
        mmRbtreeU64Vpt_Set(&p->rbtree, unique_id, multcp);
        mmSpinlock_Unlock(&p->rbtree_locker);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return multcp;
}
MM_EXPORT_NET void mmContact_Rmv(struct mmContact* p, mmUInt64_t unique_id)
{
    struct mmRbtreeU64VptIterator* it = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    it = mmRbtreeU64Vpt_GetIterator(&p->rbtree, unique_id);
    if (NULL != it)
    {
        struct mmMulTcp* multcp = (struct mmMulTcp*)(it->v);
        mmMulTcp_Lock(multcp);
        mmRbtreeU64Vpt_Erase(&p->rbtree, it);
        mmMulTcp_Unlock(multcp);
        (*(p->event_multcp_allocator.EventMultcpRecycle))(p, multcp);
        mmMulTcp_Destroy(multcp);
        mmFree(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET struct mmMulTcp* mmContact_Get(struct mmContact* p, mmUInt64_t unique_id)
{
    struct mmMulTcp* e = NULL;
    //
    mmSpinlock_Lock(&p->rbtree_locker);
    e = (struct mmMulTcp*)mmRbtreeU64Vpt_Get(&p->rbtree, unique_id);
    mmSpinlock_Unlock(&p->rbtree_locker);
    return e;
}
MM_EXPORT_NET struct mmMulTcp* mmContact_GetInstance(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port)
{
    struct mmMulTcp* multcp = mmContact_Get(p, unique_id);
    if (NULL == multcp)
    {
        multcp = mmContact_Add(p, unique_id, node_n, node_r, port);
        //cache.
        mmMulTcp_Lock(multcp);
        mmString_Assigns(&multcp->cache_native_node, node_n);
        mmString_Assigns(&multcp->cache_remote_node, node_r);
        multcp->cache_remote_port = port;
        mmMulTcp_Unlock(multcp);
    }
    else
    {
        mmMulTcp_Lock(multcp);
        if (
            0 != mmString_CompareCStr(&multcp->cache_native_node, node_n) ||
            0 != mmString_CompareCStr(&multcp->cache_remote_node, node_r) ||
            multcp->cache_remote_port != port)
        {
            // if ip and port not equal , we reset it.
            // reset multcp address.
            mmMulTcp_SetNative(multcp, node_n, port);
            mmMulTcp_SetRemote(multcp, node_r, port);
            //cache.
            mmString_Assigns(&multcp->cache_native_node, node_n);
            mmString_Assigns(&multcp->cache_remote_node, node_r);
            multcp->cache_remote_port = port;
            // the multcp is invalid.
            mmMulTcp_DetachSocket(multcp);
        }
        mmMulTcp_Unlock(multcp);
    }
    return multcp;
}
// get instance quick.if get instance by unique_id,not compare whether the address is equal.
MM_EXPORT_NET struct mmMulTcp* mmContact_GetInstanceQuick(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port)
{
    struct mmMulTcp* multcp = mmContact_Get(p, unique_id);
    if (NULL == multcp)
    {
        multcp = mmContact_Add(p, unique_id, node_n, node_r, port);
        //cache.
        mmMulTcp_Lock(multcp);
        mmString_Assigns(&multcp->cache_native_node, node_n);
        mmString_Assigns(&multcp->cache_remote_node, node_r);
        multcp->cache_remote_port = port;
        mmMulTcp_Unlock(multcp);
    }
    return multcp;
}
// get instance timeout. timecode for address timeout calculate, if timeout, compare whether the address is equal.
// timecode milliseconds.
MM_EXPORT_NET struct mmMulTcp* mmContact_GetInstanceTimeout(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port, mmUInt64_t timecode)
{
    struct mmMulTcp* multcp = mmContact_Get(p, unique_id);
    if (NULL == multcp)
    {
        multcp = mmContact_Add(p, unique_id, node_n, node_r, port);
        //cache.
        mmMulTcp_Lock(multcp);
        mmString_Assigns(&multcp->cache_native_node, node_n);
        mmString_Assigns(&multcp->cache_remote_node, node_r);
        multcp->cache_remote_port = port;
        mmMulTcp_SetCacheTimecode(multcp, timecode);
        mmMulTcp_Unlock(multcp);
    }
    else
    {
        mmSInt64_t time_interval = 0;
        mmSInt64_t timeout = 0;

        mmMulTcp_Lock(multcp);
        time_interval = (mmSInt32_t)(timecode - multcp->cache_timecode);
        timeout = multcp->cache_timeout;
        mmMulTcp_Unlock(multcp);

        if (time_interval > timeout)
        {
            mmMulTcp_Lock(multcp);

            mmMulTcp_SetCacheTimecode(multcp, timecode);

            if (
                0 != mmString_CompareCStr(&multcp->cache_native_node, node_n) ||
                0 != mmString_CompareCStr(&multcp->cache_remote_node, node_r) ||
                multcp->cache_remote_port != port)
            {
                // if ip and port not equal , we reset it.
                // reset multcp address.
                mmMulTcp_SetNative(multcp, node_n, port);
                mmMulTcp_SetRemote(multcp, node_r, port);
                //cache.
                mmString_Assigns(&multcp->cache_native_node, node_n);
                mmString_Assigns(&multcp->cache_remote_node, node_r);
                multcp->cache_remote_port = port;
                // the multcp is invalid.
                mmMulTcp_DetachSocket(multcp);
            }
            mmMulTcp_Unlock(multcp);
        }
    }
    return multcp;
}
MM_EXPORT_NET void mmContact_Clear(struct mmContact* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU64VptIterator* it = NULL;
    struct mmMulTcp* multcp = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);
    mmSpinlock_Lock(&p->rbtree_locker);
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU64VptIterator, n);
        multcp = (struct mmMulTcp*)(it->v);
        n = mmRb_Next(n);
        mmMulTcp_Lock(multcp);
        mmRbtreeU64Vpt_Erase(&p->rbtree, it);
        mmMulTcp_Unlock(multcp);
        (*(p->event_multcp_allocator.EventMultcpRecycle))(p, multcp);
        mmMulTcp_Destroy(multcp);
        mmFree(multcp);
    }
    mmSpinlock_Unlock(&p->rbtree_locker);
    mmSpinlock_Unlock(&p->instance_locker);
}
