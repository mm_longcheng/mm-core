/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNuclear_h__
#define __mmNuclear_h__

#include "core/mmCore.h"
#include "core/mmThread.h"
#include "core/mmSpinlock.h"

#include "net/mmNetExport.h"

#include "poller/mmPoll.h"

#include "container/mmRbtreeVisibleU32.h"

#include <pthread.h>

#include "core/mmPrefix.h"

// nuclear default poll length.
#define MM_NUCLEAR_POLL_LENGTH 256
// nuclear default poll idle sleep milliseconds.
#define MM_NUCLEAR_IDLE_SLEEP_MSEC 200

struct mmNuclearCallback
{
    void(*HandleRecv)(void* obj, void* u);
    void(*HandleSend)(void* obj, void* u);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmNuclearCallback_Init(struct mmNuclearCallback* p);
MM_EXPORT_NET void mmNuclearCallback_Destroy(struct mmNuclearCallback* p);

struct mmNuclear
{
    // rb tree for fd <--> addr.
    struct mmRbtreeVisibleU32 rbtree_visible;
    //
    struct mmNuclearCallback callback;
    // poller.
    struct mmPoll poll;
    //
    pthread_t poll_thread;
    pthread_mutex_t cond_locker;
    pthread_cond_t cond_not_null;
    pthread_cond_t cond_not_full;
    mmAtomic_t locker;
    // set poll len for mmPoll_wait. default is MM_NUCLEAR_POLL_LEN.
    // set it before wait, or join_wait after.
    // poll length.
    mmUInt32_t poll_length;
    // set poll timeout interval. -1 is forever. default is MM_NUCLEAR_IDLE_SLEEP_MSEC.
    // if forever, you must resolve break block state while poll wait, use pipeline, or other way.
    // poll timeout.
    mmMSec_t poll_timeout;
    // addr size. we not use the rbtree_visible size.
    // we here need lock free and quick data struct.
    size_t size;
    // max addr size. default is -1;
    size_t max_size;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_NET void mmNuclear_Init(struct mmNuclear* p);
MM_EXPORT_NET void mmNuclear_Destroy(struct mmNuclear* p);

/* locker order is
*     locker
*/
MM_EXPORT_NET void mmNuclear_Lock(struct mmNuclear* p);
MM_EXPORT_NET void mmNuclear_Unlock(struct mmNuclear* p);

MM_EXPORT_NET void mmNuclear_SetCallback(struct mmNuclear* p, struct mmNuclearCallback* callback);

// poll size.
MM_EXPORT_NET size_t mmNuclear_PollSize(struct mmNuclear* p);

// wait for activation fd. 0 == milliseconds is noblock.
MM_EXPORT_NET void mmNuclear_PollWait(struct mmNuclear* p, mmMSec_t milliseconds);
// Poll loop.
MM_EXPORT_NET void mmNuclear_PollLoop(struct mmNuclear* p);

// start wait thread.
MM_EXPORT_NET void mmNuclear_Start(struct mmNuclear* p);
// interrupt wait thread.
MM_EXPORT_NET void mmNuclear_Interrupt(struct mmNuclear* p);
// shutdown wait thread.
MM_EXPORT_NET void mmNuclear_Shutdown(struct mmNuclear* p);
// join wait thread.
MM_EXPORT_NET void mmNuclear_Join(struct mmNuclear* p);

// traver member for nuclear.
MM_EXPORT_NET void mmNuclear_Traver(struct mmNuclear* p, mmRbtreeVisibleU32HandleFunc handle, void* u);

// add u into poll_fd. you must add rmv in pairs.
MM_EXPORT_NET void mmNuclear_FdAdd(struct mmNuclear* p, mmSocket_t fd, void* u);
// rmv u from poll_fd. you must add rmv in pairs.
MM_EXPORT_NET void mmNuclear_FdRmv(struct mmNuclear* p, mmSocket_t fd, void* u);
// mod u event from poll wait.
MM_EXPORT_NET void mmNuclear_FdMod(struct mmNuclear* p, mmSocket_t fd, void* u, int flag);
// get u event from poll wait.
MM_EXPORT_NET void* mmNuclear_FdGet(struct mmNuclear* p, mmSocket_t fd);

// add u into poll_fd. not cond and locker. you must lock it outside manual.
MM_EXPORT_NET void mmNuclear_FdAddPoll(struct mmNuclear* p, mmSocket_t fd, void* u);
// rmv u from poll_fd. not cond and locker. you must lock it outside manual.
MM_EXPORT_NET void mmNuclear_FdRmvPoll(struct mmNuclear* p, mmSocket_t fd, void* u);
// signal.
MM_EXPORT_NET void mmNuclear_FdPollSignal(struct mmNuclear* p);

#include "core/mmSuffix.h"

#endif//__mmNuclear_h__
