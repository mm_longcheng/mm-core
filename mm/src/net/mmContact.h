/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmContact_h__
#define __mmContact_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"

#include "net/mmNetExport.h"
#include "net/mmMulTcp.h"

#include "container/mmRbtreeU64.h"

#include "core/mmPrefix.h"

struct mmContactEventMulTcpAllocator
{
    // event produce and add rbtree.
    void(*EventMultcpProduce)(void* p, struct mmMulTcp* multcp);
    // event recycle and rmv rbtree.
    void(*EventMultcpRecycle)(void* p, struct mmMulTcp* multcp);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmContactEventMulTcpAllocator_Init(struct mmContactEventMulTcpAllocator* p);
MM_EXPORT_NET void mmContactEventMulTcpAllocator_Destroy(struct mmContactEventMulTcpAllocator* p);

struct mmContact
{
    // tcp callback handle implement.
    struct mmTcpCallback tcp_callback;
    struct mmContactEventMulTcpAllocator event_multcp_allocator;
    // crypto callback.
    struct mmCryptoCallback crypto_callback;
    struct mmRbtreeU64Vpt rbtree;
    pthread_mutex_t signal_mutex;
    pthread_cond_t signal_cond;
    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;
    // mmThreadState_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmContact_Init(struct mmContact* p);
MM_EXPORT_NET void mmContact_Destroy(struct mmContact* p);

/* locker order is
*     locker
*/
MM_EXPORT_NET void mmContact_Lock(struct mmContact* p);
MM_EXPORT_NET void mmContact_Unlock(struct mmContact* p);

// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmContact_SetTcpCallback(struct mmContact* p, struct mmTcpCallback* tcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmContact_SetEventMulTcpAllocator(struct mmContact* p, struct mmContactEventMulTcpAllocator* event_multcp_allocator);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmContact_SetCryptoCallback(struct mmContact* p, struct mmCryptoCallback* crypto_callback);
// assign context handle.
MM_EXPORT_NET void mmContact_SetContext(struct mmContact* p, void* u);

MM_EXPORT_NET void mmContact_Check(struct mmContact* p);

MM_EXPORT_NET void mmContact_Start(struct mmContact* p);
MM_EXPORT_NET void mmContact_Interrupt(struct mmContact* p);
MM_EXPORT_NET void mmContact_Shutdown(struct mmContact* p);
MM_EXPORT_NET void mmContact_Join(struct mmContact* p);

// this func is all thread safe,not need lock.
MM_EXPORT_NET struct mmMulTcp* mmContact_Add(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port);
MM_EXPORT_NET void mmContact_Rmv(struct mmContact* p, mmUInt64_t unique_id);
MM_EXPORT_NET struct mmMulTcp* mmContact_Get(struct mmContact* p, mmUInt64_t unique_id);
MM_EXPORT_NET struct mmMulTcp* mmContact_GetInstance(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port);
// get instance quick.if get instance by unique_id,not compare whether the address is equal.
MM_EXPORT_NET struct mmMulTcp* mmContact_GetInstanceQuick(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port);
// get instance timeout. timecode for address timeout calculate, if timeout, compare whether the address is equal.
// timecode milliseconds.
MM_EXPORT_NET struct mmMulTcp* mmContact_GetInstanceTimeout(struct mmContact* p, mmUInt64_t unique_id, const char* node_n, const char* node_r, mmUShort_t port, mmUInt64_t timecode);
MM_EXPORT_NET void mmContact_Clear(struct mmContact* p);

#include "core/mmSuffix.h"

#endif//__mmContact_h__
