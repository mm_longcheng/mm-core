/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmStreambufPacket.h"

#include "core/mmLogger.h"
#include "core/mmErrno.h"
#include "core/mmAlloc.h"
#include "core/mmByteswap.h"

// overdraft pack.hbuff pack.bbuff for encode.
// will pbump streambuf.
MM_EXPORT_NET mmUInt32_t mmStreambufPacket_Overdraft(struct mmStreambuf* p, struct mmPacket* pack)
{
    mmUInt16_t msg_size = (mmUInt16_t)(sizeof(mmUInt32_t) + pack->hbuff.length + pack->bbuff.length);
    // use net byte order.
    mmUInt16_t message_size_ne = mmHtoL16((mmUInt16_t)(msg_size));
    mmUInt16_t hbuff_length_ne = mmHtoL16((mmUInt16_t)(pack->hbuff.length));
    // if the idle put size is lack, we try remove the get buffer.
    mmStreambuf_AlignedMemory(p, msg_size);
    //
    pack->hbuff.buffer = p->buff;
    pack->hbuff.offset = (size_t)(p->pptr + sizeof(mmUInt32_t));
    pack->bbuff.buffer = p->buff;
    pack->bbuff.offset = pack->hbuff.offset + pack->hbuff.length;
    mmMemcpy(p->buff + p->pptr, (void*)&message_size_ne, sizeof(mmUInt16_t));
    mmMemcpy(p->buff + p->pptr + sizeof(mmUInt16_t), (void*)&hbuff_length_ne, sizeof(mmUInt16_t));
    mmStreambuf_Pbump(p, msg_size);
    return (mmUInt32_t)msg_size;
}

// repayment pack.hbuff pack.bbuff for encode.
// will gbump streambuf.
MM_EXPORT_NET mmUInt32_t mmStreambufPacket_Repayment(struct mmStreambuf* p, struct mmPacket* pack)
{
    mmUInt16_t msg_size = (mmUInt16_t)(sizeof(mmUInt32_t) + pack->hbuff.length + pack->bbuff.length);
    mmStreambuf_Gbump(p, (size_t)msg_size);
    return (mmUInt32_t)msg_size;
}

// packet handle tcp buffer and length for streambuf handle separate packet.
MM_EXPORT_NET int mmStreambufPacket_HandleTcp(struct mmStreambuf* p, mmPacketHandleTcpFunc handle, void* obj)
{
    int code = 0;
    struct mmPacket pack;
    struct mmPacketHead phead;
    mmUInt16_t msg_size = 0;
    size_t buff_size = 0;
    // use net byte order.
    mmUInt16_t message_size_ne = 0;
    mmUInt16_t hbuff_length_ne = 0;
    assert(handle && "handle callback is invalid.");
    do
    {
        buff_size = mmStreambuf_Size(p);
        mmMemcpy(&message_size_ne, (void*)(p->buff + p->gptr), sizeof(mmUInt16_t));
        msg_size = mmLtoH16((mmUInt16_t)(message_size_ne));
        if (buff_size >= msg_size)
        {
            mmMemcpy(&hbuff_length_ne, (void*)(p->buff + p->gptr + sizeof(mmUInt16_t)), sizeof(mmUInt16_t));
            // here we just use the weak ref from streambuf.for data recv callback.
            pack.hbuff.length = mmLtoH16((mmUInt16_t)(hbuff_length_ne));
            pack.hbuff.buffer = p->buff;
            pack.hbuff.offset = (mmUInt32_t)(p->gptr + sizeof(mmUInt32_t));
            //
            pack.bbuff.length = msg_size - sizeof(mmUInt32_t) - pack.hbuff.length;
            pack.bbuff.buffer = p->buff;
            pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
            // check buffer.
            if (msg_size < pack.hbuff.length + sizeof(mmUInt32_t) || MM_MSG_COMM_HEAD_SIZE < pack.hbuff.length)
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "%s %d invalid tcp message buffer.", __FUNCTION__, __LINE__);
                code = -1;
                break;
            }
            else
            {
                mmPacketHead_Reset(&phead);
                mmPacket_HeadDecode(&pack, &pack.hbuff, &phead);
                //
                // fire the field buffer recv event.
                (*(handle))(obj, &pack);
                // no matter session_recv how much buffer reading,
                // we removeread fixed size to make sure the socket buffer sequence is correct.
                mmStreambuf_Gbump(p, msg_size);
            }
        }
    } while (buff_size > msg_size && msg_size >= MM_MSG_BASE_HEAD_SIZE);
    return code;
}
// packet handle udp buffer and length for streambuf handle separate packet.
MM_EXPORT_NET int mmStreambufPacket_HandleUdp(struct mmStreambuf* p, mmPacketHandleUdpFunc handle, void* obj, struct mmSockaddr* remote)
{
    int code = 0;
    struct mmPacket pack;
    struct mmPacketHead phead;
    mmUInt16_t msg_size = 0;
    size_t buff_size = 0;
    // use net byte order.
    mmUInt16_t message_size_ne = 0;
    mmUInt16_t hbuff_length_ne = 0;
    assert(handle && "handle callback is invalid.");
    do
    {
        buff_size = mmStreambuf_Size(p);
        mmMemcpy(&message_size_ne, (void*)(p->buff + p->gptr), sizeof(mmUInt16_t));
        msg_size = mmLtoH16((mmUInt16_t)(message_size_ne));
        if (buff_size >= msg_size)
        {
            mmMemcpy(&hbuff_length_ne, (void*)(p->buff + p->gptr + sizeof(mmUInt16_t)), sizeof(mmUInt16_t));
            // here we just use the weak ref from streambuf.for data recv callback.
            pack.hbuff.length = mmLtoH16((mmUInt16_t)(hbuff_length_ne));
            pack.hbuff.buffer = p->buff;
            pack.hbuff.offset = (mmUInt32_t)(p->gptr + sizeof(mmUInt32_t));
            //
            pack.bbuff.length = msg_size - sizeof(mmUInt32_t) - pack.hbuff.length;
            pack.bbuff.buffer = p->buff;
            pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
            // check buffer.
            if (msg_size < pack.hbuff.length + sizeof(mmUInt32_t) || MM_MSG_COMM_HEAD_SIZE < pack.hbuff.length)
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmLogger_LogE(gLogger, "%s %d invalid udp message buffer.", __FUNCTION__, __LINE__);
                code = -1;
                break;
            }
            else
            {
                mmPacketHead_Reset(&phead);
                mmPacket_HeadDecode(&pack, &pack.hbuff, &phead);
                //
                // fire the field buffer recv event.
                (*(handle))(obj, &pack, remote);
                // no matter session_recv how much buffer reading,
                // we removeread fixed size to make sure the socket buffer sequence is correct.
                mmStreambuf_Gbump(p, msg_size);
            }
        }
    } while (buff_size > msg_size && msg_size >= MM_MSG_BASE_HEAD_SIZE);
    return code;
}
