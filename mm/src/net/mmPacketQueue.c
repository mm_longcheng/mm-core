/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPacketQueue.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "container/mmListVpt.h"

static void __static_mmPacketQueue_HandleDefault(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{

}
MM_EXPORT_NET void mmPacketQueueCallback_Init(struct mmPacketQueueCallback* p)
{
    p->Handle = &__static_mmPacketQueue_HandleDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmPacketQueueCallback_Destroy(struct mmPacketQueueCallback* p)
{
    p->Handle = &__static_mmPacketQueue_HandleDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmPacketQueue_Init(struct mmPacketQueue* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmPoolPacket_Init(&p->pool_packet);
    mmTwoLockQuque_Init(&p->lock_queue);
    mmRbtreeU32Vpt_Init(&p->rbtree);
    mmPacketQueueCallback_Init(&p->callback);
    mmSpinlock_Init(&p->rbtree_locker, NULL);
    mmSpinlock_Init(&p->pool_locker, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->max_pop = MM_PACKET_QUEUE_MAX_POPER_NUMBER;
    p->u = NULL;

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}
MM_EXPORT_NET void mmPacketQueue_Destroy(struct mmPacketQueue* p)
{
    mmPacketQueue_ClearHandleRbtree(p);
    mmPacketQueue_Dispose(p);
    //
    mmPoolPacket_Destroy(&p->pool_packet);
    mmTwoLockQuque_Destroy(&p->lock_queue);
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
    mmPacketQueueCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->rbtree_locker);
    mmSpinlock_Destroy(&p->pool_locker);
    mmSpinlock_Destroy(&p->locker);
    p->max_pop = -1;
    p->u = NULL;
}

MM_EXPORT_NET void mmPacketQueue_Lock(struct mmPacketQueue* p)
{
    mmPoolPacket_Lock(&p->pool_packet);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_NET void mmPacketQueue_Unlock(struct mmPacketQueue* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmPoolPacket_Unlock(&p->pool_packet);
}

MM_EXPORT_NET void mmPacketQueue_SetHandle(struct mmPacketQueue* p, mmUInt32_t id, mmPacketQueueHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Set(&p->rbtree, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker);
}
MM_EXPORT_NET void mmPacketQueue_SetQueueCallback(struct mmPacketQueue* p, struct mmPacketQueueCallback* queue_callback)
{
    assert(NULL != queue_callback && "you can not assign null queue_callback.");
    p->callback = *queue_callback;
}
MM_EXPORT_NET void mmPacketQueue_SetMaxPoperNumber(struct mmPacketQueue* p, size_t max_pop)
{
    p->max_pop = max_pop;
}
// assign context handle.
MM_EXPORT_NET void mmPacketQueue_SetContext(struct mmPacketQueue* p, void* u)
{
    p->u = u;
}

MM_EXPORT_NET void mmPacketQueue_ClearHandleRbtree(struct mmPacketQueue* p)
{
    mmSpinlock_Lock(&p->rbtree_locker);
    mmRbtreeU32Vpt_Clear(&p->rbtree);
    mmSpinlock_Unlock(&p->rbtree_locker);
}

MM_EXPORT_NET int mmPacketQueue_IsEmpty(struct mmPacketQueue* p)
{
    return mmTwoLockQuque_IsEmpty(&p->lock_queue);
}

MM_EXPORT_NET void mmPacketQueue_ThreadHandle(struct mmPacketQueue* p)
{
    mmPacketQueue_PopEntire(p);
}
MM_EXPORT_NET void mmPacketQueue_Dispose(struct mmPacketQueue* p)
{
    struct mmPoolPacketElem* data = NULL;
    uintptr_t v = 0;

    while (MM_TRUE == mmTwoLockQuque_Dequeue(&p->lock_queue, &v))
    {
        data = (struct mmPoolPacketElem*)v;

        mmSpinlock_Lock(&p->pool_locker);
        mmPoolPacket_Recycle(&p->pool_packet, data);
        mmSpinlock_Unlock(&p->pool_locker);
    }
}
MM_EXPORT_NET void mmPacketQueue_Push(struct mmPacketQueue* p, void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmPoolPacketElem* data = NULL;

    mmSpinlock_Lock(&p->pool_locker);
    data = mmPoolPacket_Produce(&p->pool_packet, pack->hbuff.length, pack->bbuff.length);
    mmSpinlock_Unlock(&p->pool_locker);

    data->obj = obj;
    mmPacket_CopyWeak(pack, &data->packet);
    mmMemcpy(&data->ss_remote, remote, sizeof(struct mmSockaddr));
    // push to message queue.
    mmTwoLockQuque_Enqueue(&p->lock_queue, (uintptr_t)data);
}
MM_EXPORT_NET void mmPacketQueue_PopNumber(struct mmPacketQueue* p, size_t number)
{
    struct mmPoolPacketElem* data = NULL;
    uintptr_t v = 0;
    size_t n = 0;

    struct mmPacket* pack = NULL;
    void* obj = NULL;
    struct mmSockaddr* remote = NULL;
    mmPacketQueueHandleFunc handle = NULL;

    while (n < number && MM_TRUE == mmTwoLockQuque_Dequeue(&p->lock_queue, &v))
    {
        data = (struct mmPoolPacketElem*)v;

        pack = &data->packet;
        obj = data->obj;
        remote = &data->ss_remote;
        handle = NULL;

        mmSpinlock_Lock(&p->rbtree_locker);
        handle = (mmPacketQueueHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree, pack->phead.mid);
        mmSpinlock_Unlock(&p->rbtree_locker);
        if (NULL != handle)
        {
            (*(handle))(obj, p->u, pack, remote);
        }
        else
        {
            // if not define the handler,fire the default function.
            assert(NULL != p->callback.Handle && "p->callback.Handle is a null.");
            (*(p->callback.Handle))(obj, p->u, pack, remote);
        }
        mmSpinlock_Lock(&p->pool_locker);
        mmPoolPacket_Recycle(&p->pool_packet, data);
        mmSpinlock_Unlock(&p->pool_locker);

        data = NULL;

        n++;
    }
}
MM_EXPORT_NET void mmPacketQueue_PopEntire(struct mmPacketQueue* p)
{
    mmPacketQueue_PopNumber(p, p->max_pop);
}
MM_EXPORT_NET void mmPacketQueue_PopSingle(struct mmPacketQueue* p)
{
    mmPacketQueue_PopNumber(p, 1);
}

MM_EXPORT_NET struct mmPoolPacketElem* mmPacketQueue_Overdraft(struct mmPacketQueue* p, size_t hlength, size_t blength)
{
    struct mmPoolPacketElem* packet_elem = NULL;

    mmSpinlock_Lock(&p->pool_locker);
    packet_elem = mmPoolPacket_Produce(&p->pool_packet, hlength, blength);
    mmSpinlock_Unlock(&p->pool_locker);

    return packet_elem;
}
MM_EXPORT_NET void mmPacketQueue_Repayment(struct mmPacketQueue* p, struct mmPoolPacketElem* packet_elem)
{
    // push to message queue.
    mmTwoLockQuque_Enqueue(&p->lock_queue, (uintptr_t)packet_elem);
}
