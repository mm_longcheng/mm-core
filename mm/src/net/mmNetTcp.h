/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNetTcp_h__
#define __mmNetTcp_h__

#include "core/mmCore.h"
#include "core/mmSpinlock.h"
#include "core/mmTimerTask.h"

#include "net/mmNetExport.h"
#include "net/mmTcp.h"
#include "net/mmSelector.h"
#include "net/mmCrypto.h"
#include "net/mmPacket.h"

#include "container/mmRbtreeU32.h"

#include "core/mmPrefix.h"

// if bind listen failure will msleep this time.
#define MM_NETTCP_TRY_MSLEEP_TIME 20
// net tcp nonblock timeout..
#define MM_NETTCP_NONBLOCK_TIMEOUT 1000
// net tcp one time try time.
#define MM_NETTCP_TRYTIME 3
// net tcp block time, default is 10 ms.
#define MM_NETTCP_BLOCK_TIME 10

typedef void(*mmNetTcpHandleFunc)(void* obj, void* u, struct mmPacket* pack);

struct mmNetTcpCallback
{
    void(*Handle)(void* obj, void* u, struct mmPacket* pack);
    void(*Broken)(void* obj);
    void(*Nready)(void* obj);
    void(*Finish)(void* obj);
    void* obj;// weak ref. user data for callback.
};
MM_EXPORT_NET void mmNetTcpCallback_Init(struct mmNetTcpCallback* p);
MM_EXPORT_NET void mmNetTcpCallback_Destroy(struct mmNetTcpCallback* p);

struct mmNetTcpEventTcpAllocator
{
    // event tcp produce and add rbtree.
    void(*EventTcpProduce)(void* p, struct mmTcp* tcp);
    // event tcp recycle and rmv rbtree.
    void(*EventTcpRecycle)(void* p, struct mmTcp* tcp);
    
    void* obj;// weak ref. user data for callback.
};
MM_EXPORT_NET void mmNetTcpEventTcpAllocator_Init(struct mmNetTcpEventTcpAllocator* p);
MM_EXPORT_NET void mmNetTcpEventTcpAllocator_Destroy(struct mmNetTcpEventTcpAllocator* p);

enum mmNetTcpState_t
{
    MM_NETTCP_STATE_CLOSED = 0,// fd is closed, connect closed. invalid at all.
    MM_NETTCP_STATE_MOTION = 1,// fd is opened, connect closed. at connecting.
    MM_NETTCP_STATE_FINISH = 2,// fd is opened, connect opened. at connected.
    MM_NETTCP_STATE_BROKEN = 3,// fd is opened, connect broken. connect is broken fd not closed.
};

struct mmNetTcp
{
    // strong ref.
    struct mmTcp tcp;
    // strong ref.
    struct mmSelector selector;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree;
    // tcp callback handle implement.
    struct mmTcpCallback tcp_callback;
    // value ref. tcp callback.
    struct mmNetTcpCallback callback;
    struct mmNetTcpEventTcpAllocator event_tcp_allocator;
    // crypto callback.
    struct mmCryptoCallback crypto_callback;
    mmAtomic_t rbtree_locker;
    mmAtomic_t locker;

    // tcp nonblock timeout. default is MM_NETTCP_NONBLOCK_TIMEOUT milliseconds.
    mmMSec_t nonblock_timeout;

    // mmNetTcpState_t, default is tcp_state_closed(0)
    int tcp_state;

    // try bind and listen times. default is MM_NETTCP_TRYTIME.
    mmUInt32_t trytimes;
    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
    // user data.
    void* u;
};

MM_EXPORT_NET void mmNetTcp_Init(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_Destroy(struct mmNetTcp* p);

// streambuf reset.
MM_EXPORT_NET void mmNetTcp_ResetStreambuf(struct mmNetTcp* p);

/* locker order is
*     tcp
*     selector
*     locker
*/
MM_EXPORT_NET void mmNetTcp_Lock(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_Unlock(struct mmNetTcp* p);
// s locker. 
MM_EXPORT_NET void mmNetTcp_SLock(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_SUnlock(struct mmNetTcp* p);
// i locker
MM_EXPORT_NET void mmNetTcp_ILock(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_IUnlock(struct mmNetTcp* p);
// o locker
MM_EXPORT_NET void mmNetTcp_OLock(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_OUnlock(struct mmNetTcp* p);

// assign native address but not connect.
MM_EXPORT_NET void mmNetTcp_SetNative(struct mmNetTcp* p, const char* node, mmUShort_t port);
// assign remote address but not connect.
MM_EXPORT_NET void mmNetTcp_SetRemote(struct mmNetTcp* p, const char* node, mmUShort_t port);
// assign addr native by sockaddr_storage. AF_INET;
// after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmNetTcp_SetNativeStorage(struct mmNetTcp* p, struct mmSockaddr* ss_native);
// assign addr remote by sockaddr_storage. AF_INET;
// after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmNetTcp_SetRemoteStorage(struct mmNetTcp* p, struct mmSockaddr* ss_remote);

MM_EXPORT_NET void mmNetTcp_SetHandle(struct mmNetTcp* p, mmUInt32_t id, mmNetTcpHandleFunc callback);
MM_EXPORT_NET void mmNetTcp_SetNetTcpCallback(struct mmNetTcp* p, struct mmNetTcpCallback* net_tcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetTcp_SetTcpCallback(struct mmNetTcp* p, struct mmTcpCallback* tcp_callback);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetTcp_SetEventTcpAllocator(struct mmNetTcp* p, struct mmNetTcpEventTcpAllocator* event_tcp_allocator);
// do not assign when rbtree have some elem.
MM_EXPORT_NET void mmNetTcp_SetCryptoCallback(struct mmNetTcp* p, struct mmCryptoCallback* crypto_callback);
// assign context handle.
MM_EXPORT_NET void mmNetTcp_SetContext(struct mmNetTcp* p, void* u);
// assign nonblock_timeout.
MM_EXPORT_NET void mmNetTcp_SetNonblockTimeout(struct mmNetTcp* p, mmMSec_t nonblock_timeout);
// assign trytimes.
MM_EXPORT_NET void mmNetTcp_SetTrytimes(struct mmNetTcp* p, mmUInt32_t trytimes);

// context for tcp will use the mm_addr. u attribute. such as crypto context.
MM_EXPORT_NET void mmNetTcp_SetAddrContext(struct mmNetTcp* p, void* u);
MM_EXPORT_NET void* mmNetTcp_GetAddrContext(struct mmNetTcp* p);

MM_EXPORT_NET void mmNetTcp_ClearHandleRbtree(struct mmNetTcp* p);

// net_tcp crypto encrypt buffer by tcp.
MM_EXPORT_NET void mmNetTcp_CryptoEncrypt(struct mmNetTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length);
// net_tcp crypto decrypt buffer by tcp.
MM_EXPORT_NET void mmNetTcp_CryptoDecrypt(struct mmNetTcp* p, struct mmTcp* tcp, mmUInt8_t* buffer, size_t offset, size_t length);

// fopen socket.
MM_EXPORT_NET void mmNetTcp_FopenSocket(struct mmNetTcp* p);
// synchronize connect. can call this as we already open connect check thread.
MM_EXPORT_NET void mmNetTcp_Connect(struct mmNetTcp* p);
// close socket.
MM_EXPORT_NET void mmNetTcp_CloseSocket(struct mmNetTcp* p);
// shutdown socket.
MM_EXPORT_NET void mmNetTcp_ShutdownSocket(struct mmNetTcp* p, int opcode);
// set socket block.
MM_EXPORT_NET void mmNetTcp_SetBlock(struct mmNetTcp* p, int block);

// check connect and reconnect if disconnect. not thread safe.
MM_EXPORT_NET void mmNetTcp_Check(struct mmNetTcp* p);
// get finally. return 0 for all regular. not thread safe.
MM_EXPORT_NET int mmNetTcp_FinallyState(struct mmNetTcp* p);

// reset streambuf and fire event broken.
MM_EXPORT_NET void mmNetTcp_ApplyBroken(struct mmNetTcp* p);
// reset streambuf and fire event finish.
MM_EXPORT_NET void mmNetTcp_ApplyFinish(struct mmNetTcp* p);
// synchronize attach to socket.not thread safe.
MM_EXPORT_NET void mmNetTcp_AttachSocket(struct mmNetTcp* p);
// synchronize detach to socket.not thread safe.
MM_EXPORT_NET void mmNetTcp_DetachSocket(struct mmNetTcp* p);

// handle send data from buffer and buffer length. > 0 is send size success -1 failure.
// note this function do not trigger the `callback.broken` it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
// only recv can trigger `callback.broken`, make sure not dead lock and invalid free pinter appear.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
MM_EXPORT_NET int mmNetTcp_BufferSend(struct mmNetTcp* p, mmUInt8_t* buffer, size_t offset, size_t length);
//     handle send data by flush send buffer.
//     0 <  rt, means rt buffer is send, we must gbump rt size.
//     0 == rt, means the send buffer can be full.
//     0 >  rt, means the send process can be failure.
MM_EXPORT_NET int mmNetTcp_FlushSend(struct mmNetTcp* p);

MM_EXPORT_NET void mmNetTcp_Start(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_Interrupt(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_Shutdown(struct mmNetTcp* p);
MM_EXPORT_NET void mmNetTcp_Join(struct mmNetTcp* p);

MM_EXPORT_NET void mmNetTcp_TcpHandlePacketCallback(void* obj, struct mmPacket* pack);
MM_EXPORT_NET int mmNetTcp_TcpHandleCallback(void* obj, mmUInt8_t* buffer, size_t offset, size_t length);
MM_EXPORT_NET int mmNetTcp_TcpBrokenCallback(void* obj);

MM_EXPORT_NET void mmNetTcp_MidEventHandle(void* obj, mmUInt32_t mid, mmNetTcpHandleFunc callback);

#include "core/mmSuffix.h"

#endif//__mmNetTcp_h__
