/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMulcast_h__
#define __mmMulcast_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmOSSocket.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NET int mmMulcast_Join(mmSocket_t fd, mmSAFamily_t ss_family, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_NET int mmMulcast_Drop(mmSocket_t fd, mmSAFamily_t ss_family, const char* mr_multiaddr, const char* mr_interface);

MM_EXPORT_NET int mmMulcast_SetInterface(mmSocket_t fd, mmSAFamily_t ss_family, const char* mr_interface);
MM_EXPORT_NET int mmMulcast_GetInterface(mmSocket_t fd, mmSAFamily_t ss_family, struct mmString* mr_interface);

MM_EXPORT_NET int mmMulcast_SetLoop(mmSocket_t fd, mmSAFamily_t ss_family, unsigned int loop);
MM_EXPORT_NET int mmMulcast_GetLoop(mmSocket_t fd, mmSAFamily_t ss_family, unsigned int* loop);

MM_EXPORT_NET int mmMulcast_SetTtl(mmSocket_t fd, mmSAFamily_t ss_family, int ttl);
MM_EXPORT_NET int mmMulcast_GetTtl(mmSocket_t fd, mmSAFamily_t ss_family, int* ttl);

#include "core/mmSuffix.h"

#endif//__mmMulcast_h__
