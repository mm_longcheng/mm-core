/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmIpTranslation.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

// come from https://www.cnblogs.com/wangjq19920210/p/10395458.html

static char* __static_mmIp_StringWhiteSpaceTrim(char* str);
static int __static_mmIp_StringCharCount(const char* str, char character);
static int __static_mmIp_Ipv6AddressFieldTypeGet(const char* field_str);
static int __static_mmIp_IToIpv6Hex(mmUInt32_t ipv4_addr, char hex[16]);

// Ipv4 address translation
MM_EXPORT_NET int mmIpv4ToI(const char* node, mmUInt32_t* ipv4_addr)
{
    char str_ip_index[4] = { '\0' };
    mmUInt32_t ip_int, ip_add = 0;
    mmUInt32_t j = 0, a = 3, i = 0;

    for (i = 0; i <= strlen(node); i++)
    {
        if (node[i] == '\0' || node[i] == '.')
        {
            ip_int = atoi(str_ip_index);
            if (ip_int > 255)
            {
                return 1;
            }

            ip_add += (ip_int * (mmUInt32_t)pow(256, a));
            a--;
            memset(str_ip_index, 0, sizeof(str_ip_index));

            j = 0;
            continue;
        }

        str_ip_index[j] = node[i];
        j++;
    }

    *ipv4_addr = ip_add;

    return 0;
}

MM_EXPORT_NET int mmIpv6ToI(const char* node, mmUInt32_t ipv6_addr[4], int length)
{
    /***************************************************************************/
    /* Features��Parse the ipv6 address string, convert it to unsigned integer,                                 */
    /*     and store a one-dimensional array of four unsigned integers.                                         */
    /* Ipv6 address 128 bits,prefix length:                                                                     */
    /*                         - 64 for EUI-64 addresses                                                        */
    /*                         - 128 for non-EUI-64 addresses                                                   */
    /* Input : ipv6 address string, address digits, default 128 bits                                            */
    /* Output: return parsing success or failure; pointer to a one-dimensional array of 4 unsigned integers     */
    /****************************************************************************/

    char addr_str_copy[256];
    int i, num_fields;
    // mmUInt32_t* ret_addr_ptr;
    unsigned short int addr_field_arr[8];
    int addr_index;
    char* ith_field; // Point to the current domain of the address
    int  ith_field_type; // Address field type
    char *next_field;
    int  double_colon_field_index = -1; // The position of the "::" in the string address
    mmUInt32_t ipv4_address; // Ipv4 part of ipv6 address
    mmUInt32_t msw, lsw;
    int error = 0;

    // Make a copy to operate
    strcpy(addr_str_copy, node);

    // Remove space characters from strings
    __static_mmIp_StringWhiteSpaceTrim(addr_str_copy);

    /* IPv6 addresses may be in several formats:                                                        */
    /* 1) 2006:DB8:2A0:2F3B:34:E35:45:1      Represent the value of each field in hexadecimal (16 bits) */
    /* 2) 2006:DB8::E34:1                   "::" Represents 0 and can only appear once                  */
    /* 3) 2002:9D36:1:2:0:5EFE:192.168.12.9 With ipv4 address                                           */

    // Calculate the colon in the string, the number of address fields in the string is one more than the colon
    num_fields = __static_mmIp_StringCharCount(addr_str_copy, ':') + 1;

    // The maximum number of domains is length/16 + 2
    // Such as "::0:0:0:0:0:0:0:0".
    if (num_fields > ((length >> 4) + 2))
    {
        memset(ipv6_addr, 0, sizeof(mmUInt32_t) * 4);
        return 1;
    }

    // initialization
    ith_field = addr_str_copy;

    for (i = 0, addr_index = 0; i < num_fields; i++)
    {
        // Get pointer to the next field
        next_field = strchr(ith_field, ':');

        /* If it is currently the last domain, next_field is NULL                                            */
        /* Otherwise, replacing ':' with '\0', the string can end, so ith_field points to the current domain */
        /* Next_field points to the next domain header                                                       */
        if (NULL != next_field)
        {
            *next_field = '\0';
            ++next_field;
        }

        // Found the type of this domain
        ith_field_type = __static_mmIp_Ipv6AddressFieldTypeGet(ith_field);

        switch (ith_field_type)
        {
        case 0:
            // The field type is hexadecimal

            if (addr_index >= (length >> 4))
            {
                error = 1;
                break;
            }
            // Convert string to hexadecimal
            addr_field_arr[addr_index] = (unsigned short)strtoul(ith_field, NULL, 16);
            ++addr_index;
            break;

        case 1:
            // The domain type is "::"

            // Ignore if it appears at the beginning or end of the string
            if ((0 == i) || (i == num_fields - 1))
            {
                break;
            }

            // If it appears more than once, is error
            if (double_colon_field_index != -1)
            {
                error = 1;
                break;
            }

            // Write down the location
            double_colon_field_index = addr_index;

            break;

        case 2:
            // The domain type is ipv4 address

            // Make sure there are two unset fields in the address
            if (addr_index >= 7)
            {
                error = 1;
                break;
            }

            // Ipv4 address resolution
            mmIpv4ToI(ith_field, &ipv4_address);

            // Store high 16 bits
            addr_field_arr[addr_index] = (unsigned short)(ipv4_address >> 16);

            // Store low 16 bits
            addr_field_arr[addr_index + 1] = (unsigned short)(ipv4_address & 0x0000ffff);

            addr_index += 2;

            break;
        default:
            error = 1;
            break;
        }

        if (error)
        {
            memset(ipv6_addr, 0, sizeof(mmUInt32_t) * 4);
            return 1;
        }

        ith_field = next_field;
    }

    // The calculated field is not 8, and there is no "::", error
    if ((addr_index != (length >> 4)) && (-1 == double_colon_field_index))
    {
        memset(ipv6_addr, 0, sizeof(mmUInt32_t) * 4);
        return 1;
    }

    if ((addr_index != (length >> 4)) && (-1 != double_colon_field_index))
    {
        // Set the corresponding "::" corresponding to the position in addr_field_arr to 0.
        memmove(
            addr_field_arr + (double_colon_field_index + (length >> 4) - addr_index),
            addr_field_arr + double_colon_field_index, (addr_index - double_colon_field_index) * 2);

        memset(addr_field_arr + double_colon_field_index, 0, ((length >> 4) - addr_index) * 2);
    }

    for (i = 0; i < 4; i++)
    {
        msw = addr_field_arr[2 * i];
        lsw = addr_field_arr[2 * i + 1];

        (ipv6_addr)[i] = (msw << 16 | lsw);
    }

    return 0;
}

MM_EXPORT_NET int mmIToIpv4(mmUInt32_t ipv4_addr, char node[MM_ADDR_NAME_LENGTH])
{
    mmUInt32_t addr_i[4] = { 0 };
    addr_i[0] = (ipv4_addr >> 24) & 0xff;
    addr_i[1] = (ipv4_addr >> 16) & 0xff;
    addr_i[2] = (ipv4_addr >>  8) & 0xff;
    addr_i[3] = (ipv4_addr >>  0) & 0xff;
    sprintf(node, "%u.%u.%u.%u", addr_i[0], addr_i[1], addr_i[2], addr_i[3]);
    return 0;
}

MM_EXPORT_NET int mmIToIpv6(mmUInt32_t ipv6_addr[4], char node[MM_ADDR_NAME_LENGTH])
{
    // note the warning.
    // missing braces around initializer [-Wmissing-braces]
    char hex[4][16];
    memset(hex, 0, sizeof(char) * 4 * 16);
    __static_mmIp_IToIpv6Hex(ipv6_addr[0], hex[0]);
    __static_mmIp_IToIpv6Hex(ipv6_addr[1], hex[1]);
    __static_mmIp_IToIpv6Hex(ipv6_addr[2], hex[2]);
    __static_mmIp_IToIpv6Hex(ipv6_addr[3], hex[3]);
    sprintf(node, "%s:%s:%s:%s", hex[0], hex[1], hex[2], hex[3]);
    return 0;
}

MM_EXPORT_NET void mmIIpv4Add(mmUInt32_t* ipv4_addr, mmUInt32_t d)
{
    *ipv4_addr += d;
}

MM_EXPORT_NET void mmIIpv4Sub(mmUInt32_t* ipv4_addr, mmUInt32_t d)
{
    *ipv4_addr -= d;
}

MM_EXPORT_NET void mmIIpv6Add(mmUInt32_t ipv6_addr[4], mmUInt32_t d)
{
    int i = 0;
    mmUInt64_t fat_value[4] = { 0 };
    mmUInt64_t a = 0;
    mmUInt64_t b = 0;

    fat_value[0] = ipv6_addr[0];
    fat_value[1] = ipv6_addr[1];
    fat_value[2] = ipv6_addr[2];
    fat_value[3] = ipv6_addr[3];

    fat_value[3] += d;

    for (i = 3; i >= 0; i--)
    {
        a = fat_value[i] / 0xffffffff;
        b = fat_value[i] % 0xffffffff;

        if (0 != a)
        {
            fat_value[i - 1] += a;
            fat_value[i] = b;
        }
        else
        {
            break;
        }
    }

    ipv6_addr[0] = (mmUInt32_t)fat_value[0];
    ipv6_addr[1] = (mmUInt32_t)fat_value[1];
    ipv6_addr[2] = (mmUInt32_t)fat_value[2];
    ipv6_addr[3] = (mmUInt32_t)fat_value[3];
}
MM_EXPORT_NET void mmIIpv6Sub(mmUInt32_t ipv6_addr[4], mmUInt32_t d)
{
    int i = 0;
    mmUInt64_t fat_value[4] = { 0 };
    mmUInt64_t a = 0;
    mmUInt64_t b = 0;

    fat_value[0] = 0xffffffff00000000 | ((mmUInt64_t)ipv6_addr[0]);
    fat_value[1] = 0xffffffff00000000 | ((mmUInt64_t)ipv6_addr[1]);
    fat_value[2] = 0xffffffff00000000 | ((mmUInt64_t)ipv6_addr[2]);
    fat_value[3] = 0xffffffff00000000 | ((mmUInt64_t)ipv6_addr[3]);

    fat_value[3] -= d;

    for (i = 3; i >= 0; i--)
    {
        a = 0xffffffff - (fat_value[i] >> 32);
        b = fat_value[i] % 0xffffffff;

        if (0 != a)
        {
            fat_value[i - 1] -= a;
            fat_value[i] = b;
        }
        else
        {
            break;
        }
    }

    ipv6_addr[0] = (mmUInt32_t)(fat_value[0] & 0xffffffff);
    ipv6_addr[1] = (mmUInt32_t)(fat_value[1] & 0xffffffff);
    ipv6_addr[2] = (mmUInt32_t)(fat_value[2] & 0xffffffff);
    ipv6_addr[3] = (mmUInt32_t)(fat_value[3] & 0xffffffff);
}

MM_EXPORT_NET void mmIpAdd(const char* node_i, mmUInt32_t d, char node_o[MM_ADDR_NAME_LENGTH])
{
    mmUInt32_t ipv4_addr = 0;
    mmUInt32_t ipv6_addr[4] = { 0 };
    int ss_family = mmSockaddr_NodeFamily(node_i);
    switch (ss_family)
    {
    case MM_AF_INET4:
        mmIpv4ToI(node_i, &ipv4_addr);
        mmIIpv4Add(&ipv4_addr, d);
        mmIToIpv4(ipv4_addr, node_o);
        break;
    case MM_AF_INET6:
        mmIpv6ToI(node_i, ipv6_addr, 128);
        mmIIpv6Add(ipv6_addr, d);
        mmIToIpv6(ipv6_addr, node_o);
        break;
    default:
        break;
    }
}

MM_EXPORT_NET void mmIpSub(const char* node_i, mmUInt32_t d, char node_o[MM_ADDR_NAME_LENGTH])
{
    mmUInt32_t ipv4_addr = 0;
    mmUInt32_t ipv6_addr[4] = { 0 };
    int ss_family = mmSockaddr_NodeFamily(node_i);
    switch (ss_family)
    {
    case MM_AF_INET4:
        mmIpv4ToI(node_i, &ipv4_addr);
        mmIIpv4Sub(&ipv4_addr, d);
        mmIToIpv4(ipv4_addr, node_o);
        break;
    case MM_AF_INET6:
        mmIpv6ToI(node_i, ipv6_addr, 128);
        mmIIpv6Sub(ipv6_addr, d);
        mmIToIpv6(ipv6_addr, node_o);
        break;
    default:
        break;
    }
}

static char* __static_mmIp_StringWhiteSpaceTrim(char* str)
{
    /* Remove spaces in strings */
    int index;
    int new_index;
    int str_length;

    str_length = (int)strlen(str);

    for (index = 0, new_index = 0; index < str_length; index++)
    {
        if (!isspace((unsigned char)str[index]))
        {
            str[new_index] = str[index];
            new_index++;
        }
    }

    str[new_index] = '\0';

    return str;
}

static int __static_mmIp_StringCharCount(const char* str, char character)
{
    /* Calculate the number of given characters in a string */
    int i;
    int str_length;
    int count = 0;

    str_length = (int)strlen(str);
    for (i = 0; i < str_length; i++)
    {
        if (str[i] == character)
        {
            count++;
        }
    }

    return count;
}

static int __static_mmIp_Ipv6AddressFieldTypeGet(const char* field_str)
{
    /* Determine the ipv6 address field type */
    int i = 0;
    int length;
    int type;
    mmUInt32_t ipv4_addr;

    /* Judging by length                 */
    /* Hexadecimal number field�� 1-4    */
    /* "::"field��0                      */
    /* Ipv4 address field�� 7-15         */

    length = (int)strlen(field_str);

    if (0 == length)
    {
        type = 1;
    }
    else if (length <= 4)
    {
        // Make sure each number is hexadecimal
        for (i = 0; i < length; i++)
        {
            if (!isxdigit((unsigned char)field_str[i]))
            {
                return -1;
            }
        }
        type = 0;
    }
    else if ((length >= 7) && (length <= 15))
    {
        // Make sure it is a valid ipv4 address
        if (mmIpv4ToI(field_str, &ipv4_addr))
        {
            type = 2;
        }
        else
        {
            type = -1;
        }
    }
    else
    {
        type = -1;
    }

    return type;
}

// FFFF:FFFF
static int __static_mmIp_IToIpv6Hex(mmUInt32_t ipv4_addr, char hex[16])
{
    mmUInt32_t addr_i[2] = { 0 };
    addr_i[0] = (ipv4_addr >> 16) & 0xffff;
    addr_i[1] = (ipv4_addr >>  0) & 0xffff;
    sprintf(hex, "%X:%X", addr_i[0], addr_i[1]);
    return 0;
}
