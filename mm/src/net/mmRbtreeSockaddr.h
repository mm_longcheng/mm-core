/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mm_rbtree_sockaddr_h__
#define __mm_rbtree_sockaddr_h__

#include "core/mmCore.h"
#include "core/mmRbtree.h"
#include "core/mmString.h"

#include "net/mmSockaddr.h"

#include "net/mmNetExport.h"

#include "core/mmPrefix.h"

static mmInline mmSInt32_t mmRbtreeSockaddrCompareFunc(struct mmSockaddr* lhs, struct mmSockaddr* rhs)
{
    return mmSockaddr_Compare(lhs, rhs);
}

struct mmRbtreeSockaddrU32Iterator
{
    struct mmSockaddr k;
    mmUInt32_t v;
    struct mmRbNode n;
};
MM_EXPORT_NET void mmRbtreeSockaddrU32Iterator_Init(struct mmRbtreeSockaddrU32Iterator* p);
MM_EXPORT_NET void mmRbtreeSockaddrU32Iterator_Destroy(struct mmRbtreeSockaddrU32Iterator* p);

struct mmRbtreeSockaddrU32
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_NET void mmRbtreeSockaddrU32_Init(struct mmRbtreeSockaddrU32* p);
MM_EXPORT_NET void mmRbtreeSockaddrU32_Destroy(struct mmRbtreeSockaddrU32* p);

MM_EXPORT_NET mmUInt32_t* mmRbtreeSockaddrU32_Add(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrU32_Rmv(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrU32_Erase(struct mmRbtreeSockaddrU32* p, struct mmRbtreeSockaddrU32Iterator* it);
MM_EXPORT_NET mmUInt32_t* mmRbtreeSockaddrU32_GetInstance(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k);
MM_EXPORT_NET mmUInt32_t* mmRbtreeSockaddrU32_Get(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k);
MM_EXPORT_NET struct mmRbtreeSockaddrU32Iterator* mmRbtreeSockaddrU32_GetIterator(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrU32_Clear(struct mmRbtreeSockaddrU32* p);
MM_EXPORT_NET size_t mmRbtreeSockaddrU32_Size(struct mmRbtreeSockaddrU32* p);

// weak ref can use this.
MM_EXPORT_NET void mmRbtreeSockaddrU32_Set(struct mmRbtreeSockaddrU32* p, struct mmSockaddr* k, mmUInt32_t v);

// mmRbtreeSockaddrU64
struct mmRbtreeSockaddrU64Iterator
{
    struct mmSockaddr k;
    mmUInt64_t v;
    struct mmRbNode n;
};
MM_EXPORT_NET void mmRbtreeSockaddrU64Iterator_Init(struct mmRbtreeSockaddrU64Iterator* p);
MM_EXPORT_NET void mmRbtreeSockaddrU64Iterator_Destroy(struct mmRbtreeSockaddrU64Iterator* p);

struct mmRbtreeSockaddrU64
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_NET void mmRbtreeSockaddrU64_Init(struct mmRbtreeSockaddrU64* p);
MM_EXPORT_NET void mmRbtreeSockaddrU64_Destroy(struct mmRbtreeSockaddrU64* p);

MM_EXPORT_NET mmUInt64_t* mmRbtreeSockaddrU64_Add(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrU64_Rmv(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrU64_Erase(struct mmRbtreeSockaddrU64* p, struct mmRbtreeSockaddrU64Iterator* it);
MM_EXPORT_NET mmUInt64_t* mmRbtreeSockaddrU64_GetInstance(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k);
MM_EXPORT_NET mmUInt64_t* mmRbtreeSockaddrU64_Get(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k);
MM_EXPORT_NET struct mmRbtreeSockaddrU64Iterator* mmRbtreeSockaddrU64_GetIterator(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrU64_Clear(struct mmRbtreeSockaddrU64* p);
MM_EXPORT_NET size_t mmRbtreeSockaddrU64_Size(struct mmRbtreeSockaddrU64* p);

// weak ref can use this.
MM_EXPORT_NET void mmRbtreeSockaddrU64_Set(struct mmRbtreeSockaddrU64* p, struct mmSockaddr* k, mmUInt64_t v);

// mmRbtreeSockaddrString
struct mmRbtreeSockaddrStringIterator
{
    struct mmSockaddr k;
    struct mmString v;
    struct mmRbNode n;
};
MM_EXPORT_NET void mmRbtreeSockaddrStringIterator_Init(struct mmRbtreeSockaddrStringIterator* p);
MM_EXPORT_NET void mmRbtreeSockaddrStringIterator_Destroy(struct mmRbtreeSockaddrStringIterator* p);

struct mmRbtreeSockaddrString
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    size_t size;
};

MM_EXPORT_NET void mmRbtreeSockaddrString_Init(struct mmRbtreeSockaddrString* p);
MM_EXPORT_NET void mmRbtreeSockaddrString_Destroy(struct mmRbtreeSockaddrString* p);

MM_EXPORT_NET struct mmString* mmRbtreeSockaddrString_Add(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrString_Rmv(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrString_Erase(struct mmRbtreeSockaddrString* p, struct mmRbtreeSockaddrStringIterator* it);
MM_EXPORT_NET struct mmString* mmRbtreeSockaddrString_GetInstance(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k);
MM_EXPORT_NET struct mmString* mmRbtreeSockaddrString_Get(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k);
MM_EXPORT_NET struct mmRbtreeSockaddrStringIterator* mmRbtreeSockaddrString_GetIterator(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrString_Clear(struct mmRbtreeSockaddrString* p);
MM_EXPORT_NET size_t mmRbtreeSockaddrString_Size(struct mmRbtreeSockaddrString* p);

// weak ref can use this.
MM_EXPORT_NET void mmRbtreeSockaddrString_Set(struct mmRbtreeSockaddrString* p, struct mmSockaddr* k, struct mmString* v);

// mmRbtreeSockaddrVpt
struct mmRbtreeSockaddrVptIterator
{
    struct mmSockaddr k;
    void* v;
    struct mmRbNode n;
};
MM_EXPORT_NET void mmRbtreeSockaddrVptIterator_Init(struct mmRbtreeSockaddrVptIterator* p);
MM_EXPORT_NET void mmRbtreeSockaddrVptIterator_Destroy(struct mmRbtreeSockaddrVptIterator* p);

struct mmRbtreeSockaddrVpt;

struct mmRbtreeSockaddrVptAllocator
{
    void* (*Produce)(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
    void* (*Recycle)(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k, void* v);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_NET void mmRbtreeSockaddrVptAllocator_Init(struct mmRbtreeSockaddrVptAllocator* p);
MM_EXPORT_NET void mmRbtreeSockaddrVptAllocator_Destroy(struct mmRbtreeSockaddrVptAllocator* p);

struct mmRbtreeSockaddrVpt
{
    // rb tree for k <--> v.
    struct mmRbRoot rbt;
    struct mmRbtreeSockaddrVptAllocator allocator;
    size_t size;
};

MM_EXPORT_NET void mmRbtreeSockaddrVpt_Init(struct mmRbtreeSockaddrVpt* p);
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Destroy(struct mmRbtreeSockaddrVpt* p);

MM_EXPORT_NET void mmRbtreeSockaddrVpt_SetAllocator(struct mmRbtreeSockaddrVpt* p, struct mmRbtreeSockaddrVptAllocator* allocator);

MM_EXPORT_NET void* mmRbtreeSockaddrVpt_Add(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Rmv(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Erase(struct mmRbtreeSockaddrVpt* p, struct mmRbtreeSockaddrVptIterator* it);
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_GetInstance(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_Get(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
MM_EXPORT_NET struct mmRbtreeSockaddrVptIterator* mmRbtreeSockaddrVpt_GetIterator(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Clear(struct mmRbtreeSockaddrVpt* p);
MM_EXPORT_NET size_t mmRbtreeSockaddrVpt_Size(struct mmRbtreeSockaddrVpt* p);

// weak ref can use this.
MM_EXPORT_NET void mmRbtreeSockaddrVpt_Set(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k, void* v);

MM_EXPORT_NET void* mmRbtreeSockaddrVpt_WeakProduce(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k);
MM_EXPORT_NET void* mmRbtreeSockaddrVpt_WeakRecycle(struct mmRbtreeSockaddrVpt* p, struct mmSockaddr* k, void* v);

#include "core/mmSuffix.h"

#endif//__mm_rbtree_sockaddr_h__
