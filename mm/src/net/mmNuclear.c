/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNuclear.h"
#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmErrno.h"

static void* __static_mmNuclear_PollWaitThread(void* _arg);

static void __static_mmNuclear_HandleRecvDefault(void* obj, void* u)
{

}
static void __static_mmNuclear_HandleSendDefault(void* obj, void* u)
{

}
MM_EXPORT_NET void mmNuclearCallback_Init(struct mmNuclearCallback* p)
{
    p->HandleRecv = &__static_mmNuclear_HandleRecvDefault;
    p->HandleSend = &__static_mmNuclear_HandleSendDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmNuclearCallback_Destroy(struct mmNuclearCallback* p)
{
    p->HandleRecv = &__static_mmNuclear_HandleRecvDefault;
    p->HandleSend = &__static_mmNuclear_HandleSendDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmNuclear_Init(struct mmNuclear* p)
{
    mmRbtreeVisibleU32_Init(&p->rbtree_visible);
    mmNuclearCallback_Init(&p->callback);
    mmPoll_Init(&p->poll);
    pthread_mutex_init(&p->cond_locker, NULL);
    pthread_cond_init(&p->cond_not_null, NULL);
    pthread_cond_init(&p->cond_not_full, NULL);
    mmSpinlock_Init(&p->locker, NULL);
    p->poll_length = MM_NUCLEAR_POLL_LENGTH;
    p->poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
    p->size = 0;
    p->max_size = -1;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmNuclear_Destroy(struct mmNuclear* p)
{
    mmRbtreeVisibleU32_Destroy(&p->rbtree_visible);
    mmNuclearCallback_Destroy(&p->callback);
    mmPoll_Destroy(&p->poll);
    pthread_mutex_destroy(&p->cond_locker);
    pthread_cond_destroy(&p->cond_not_null);
    pthread_cond_destroy(&p->cond_not_full);
    mmSpinlock_Destroy(&p->locker);

    p->poll_length = MM_NUCLEAR_POLL_LENGTH;
    p->poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
    p->size = 0;
    p->max_size = -1;
    p->state = MM_TS_CLOSED;
}
// lock to make sure the knot is thread safe.
MM_EXPORT_NET void mmNuclear_Lock(struct mmNuclear* p)
{
    mmSpinlock_Lock(&p->locker);
}
// unlock knot.
MM_EXPORT_NET void mmNuclear_Unlock(struct mmNuclear* p)
{
    mmSpinlock_Unlock(&p->locker);
}
MM_EXPORT_NET void mmNuclear_SetCallback(struct mmNuclear* p, struct mmNuclearCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = (*callback);
}

// poll size.
MM_EXPORT_NET size_t mmNuclear_PollSize(struct mmNuclear* p)
{
    return p->size;
}

// wait for activation fd.
MM_EXPORT_NET void mmNuclear_PollWait(struct mmNuclear* p, mmMSec_t milliseconds)
{
    struct mmPollEvent* pe = NULL;
    mmInt_t real_len = 0;
    int i = 0;
    struct mmLogger* gLogger = mmLogger_Instance();
    mmPoll_SetLength(&p->poll, p->poll_length);
    //
    do
    {
        // the first quick size checking.
        if (0 == p->size)
        {
            pthread_mutex_lock(&p->cond_locker);
            // double lock checking.
            // because the first quick size checking is not thread safe.
            // here we lock and check the size once again.
            if (0 == p->size)
            {
                mmLogger_LogI(gLogger, "%s %d %p wait.", __FUNCTION__, __LINE__, p);
                pthread_cond_wait(&p->cond_not_null, &p->cond_locker);
                mmLogger_LogI(gLogger, "%s %d %p wake.", __FUNCTION__, __LINE__, p);
            }
            pthread_mutex_unlock(&p->cond_locker);
            continue;
        }
        real_len = mmPoll_WaitEvent(&p->poll, milliseconds);
        if (0 == real_len)
        {
            continue;
        }
        else if (-1 == real_len)
        {
            mmErr_t errcode = mmErrno;
            if (MM_ENOTSOCK == errcode)
            {
                mmLogger_LogE(gLogger, "%s %d %p error occur.", __FUNCTION__, __LINE__, p);
                mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
                break;
            }
            else
            {
                mmLogger_LogE(gLogger, "%s %d %p error occur.", __FUNCTION__, __LINE__, p);
                mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
                continue;
            }
        }
        for (i = 0; i < real_len; ++i)
        {
            pe = &p->poll.arrays[i];
            assert(NULL != pe->s && "pe->s is a null.");
            //
            if (pe->mask & MM_PE_READABLE)
            {
                (*p->callback.HandleRecv)(p, pe->s);
            }
            if (pe->mask & MM_PE_WRITABLE)
            {
                (*p->callback.HandleSend)(p, pe->s);
            }
        }
    } while(0);
}

// Poll loop.
MM_EXPORT_NET void mmNuclear_PollLoop(struct mmNuclear* p)
{
    while (MM_TS_MOTION == p->state)
    {
        mmNuclear_PollWait(p, p->poll_timeout);
    }
}

// start wait thread.
MM_EXPORT_NET void mmNuclear_Start(struct mmNuclear* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    mmPoll_Start(&p->poll);
    pthread_create(&p->poll_thread, NULL, &__static_mmNuclear_PollWaitThread, p);
}
// interrupt wait thread.
MM_EXPORT_NET void mmNuclear_Interrupt(struct mmNuclear* p)
{
    p->state = MM_TS_CLOSED;
    mmPoll_Interrupt(&p->poll);
    pthread_mutex_lock(&p->cond_locker);
    pthread_cond_signal(&p->cond_not_null);
    pthread_cond_signal(&p->cond_not_full);
    pthread_mutex_unlock(&p->cond_locker);
}
// shutdown wait thread.
MM_EXPORT_NET void mmNuclear_Shutdown(struct mmNuclear* p)
{
    p->state = MM_TS_FINISH;
    mmPoll_Shutdown(&p->poll);
    pthread_mutex_lock(&p->cond_locker);
    pthread_cond_signal(&p->cond_not_null);
    pthread_cond_signal(&p->cond_not_full);
    pthread_mutex_unlock(&p->cond_locker);
}
// join wait thread.
MM_EXPORT_NET void mmNuclear_Join(struct mmNuclear* p)
{
    mmPoll_Join(&p->poll);
    pthread_join(p->poll_thread, NULL);
}

// traver member for nuclear.
MM_EXPORT_NET void mmNuclear_Traver(struct mmNuclear* p, mmRbtreeVisibleU32HandleFunc handle, void* u)
{
    // note:
    // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
    mmRbtreeVisibleU32_Traver(&p->rbtree_visible, handle, u);
}

// add u into poll_fd.you must add rmv in pairs.
MM_EXPORT_NET void mmNuclear_FdAdd(struct mmNuclear* p, mmSocket_t fd, void* u)
{
    do
    {
        if (MM_INVALID_SOCKET == fd)
        {
            // the socket is invalid.do nothing.
            break;
        }
        //
        if (p->max_size <= p->size)
        {
            // pthread_cond_wait(&p->cond_not_null, &p->locker);
            // nothing need remove and not need cond wait at all.
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogT(gLogger, "%s %d we can not fd add into full poll.", __FUNCTION__, __LINE__);
            break;
        }
        mmPoll_AddEvent(&p->poll, fd, MM_PE_READABLE, u);
        // add addr to rbt.
        pthread_mutex_lock(&p->cond_locker);
        // add fd u.
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleU32_Add(&p->rbtree_visible, (int)fd, u);
        p->size++;
        //
        pthread_cond_signal(&p->cond_not_null);
        pthread_mutex_unlock(&p->cond_locker);
    } while (0);
}
// rmv u from poll_fd. you must add rmv in pairs.
MM_EXPORT_NET void mmNuclear_FdRmv(struct mmNuclear* p, mmSocket_t fd, void* u)
{
    do
    {
        if (MM_INVALID_SOCKET == fd)
        {
            // the socket is invalid.do nothing.
            break;
        }
        //
        if (0 >= p->size)
        {
            // pthread_cond_wait(&p->cond_not_full, &p->locker);
            // nothing need remove and not need cond wait at all.
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogT(gLogger, "%s %d we can not fd rmv from null poll.", __FUNCTION__, __LINE__);
            break;
        }
        mmPoll_DelEvent(&p->poll, fd, MM_PE_READABLE | MM_PE_WRITABLE, u);
        // rmv addr from rbt.
        pthread_mutex_lock(&p->cond_locker);
        // rmv fd u.
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleU32_Rmv(&p->rbtree_visible, (int)fd);
        p->size--;
        //
        pthread_cond_signal(&p->cond_not_full);
        pthread_mutex_unlock(&p->cond_locker);
    } while (0);
}
// mod u event from poll wait.
MM_EXPORT_NET void mmNuclear_FdMod(struct mmNuclear* p, mmSocket_t fd, void* u, int flag)
{
    mmPoll_ModEvent(&p->poll, fd, flag, u);
}
// get u event from poll wait.
MM_EXPORT_NET void* mmNuclear_FdGet(struct mmNuclear* p, mmSocket_t fd)
{
    void* e = NULL;
    pthread_mutex_lock(&p->cond_locker);
    // note:
    // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
    e = mmRbtreeVisibleU32_Get(&p->rbtree_visible, (int)fd);
    pthread_mutex_unlock(&p->cond_locker);
    return e;
}

// add u into poll_fd. not cond and locker.you must lock it outside manual.
MM_EXPORT_NET void mmNuclear_FdAddPoll(struct mmNuclear* p, mmSocket_t fd, void* u)
{
    mmPoll_AddEvent(&p->poll, fd, MM_PE_READABLE, u);
    // add fd u.
    // note:
    // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
    mmRbtreeVisibleU32_Add(&p->rbtree_visible, (int)fd, u);
    p->size++;
}
// rmv u from poll_fd. not cond and locker. you must lock it outside manual.
MM_EXPORT_NET void mmNuclear_FdRmvPoll(struct mmNuclear* p, mmSocket_t fd, void* u)
{
    mmPoll_DelEvent(&p->poll, fd, MM_PE_READABLE | MM_PE_WRITABLE, u);
    // rmv fd u.
    // note:
    // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
    mmRbtreeVisibleU32_Rmv(&p->rbtree_visible, (int)fd);
    p->size--;
}
// signal.
MM_EXPORT_NET void mmNuclear_FdPollSignal(struct mmNuclear* p)
{
    pthread_mutex_lock(&p->cond_locker);
    pthread_cond_signal(&p->cond_not_null);
    pthread_cond_signal(&p->cond_not_full);
    pthread_mutex_unlock(&p->cond_locker);
}

static void* __static_mmNuclear_PollWaitThread(void* arg)
{
    struct mmNuclear* p = (struct mmNuclear*)(arg);
    mmNuclear_PollLoop(p);
    return NULL;
}

