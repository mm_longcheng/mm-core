/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSelector.h"
#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmErrno.h"

static void* __static_mmSelector_PollWaitThread(void* args);

static void __static_mmSelector_HandleRecv(void* obj, void* u)
{

}
static void __static_mmSelector_HandleSend(void* obj, void* u)
{

}

MM_EXPORT_NET void mmSelectorCallback_Init(struct mmSelectorCallback* p)
{
    p->HandleRecv = &__static_mmSelector_HandleRecv;
    p->HandleSend = &__static_mmSelector_HandleSend;
    p->obj = NULL;
}
MM_EXPORT_NET void mmSelectorCallback_Destroy(struct mmSelectorCallback* p)
{
    p->HandleRecv = &__static_mmSelector_HandleRecv;
    p->HandleSend = &__static_mmSelector_HandleSend;
    p->obj = NULL;
}

MM_EXPORT_NET void mmSelector_Init(struct mmSelector* p)
{
    mmSelectorCallback_Init(&p->callback);
    mmSelect_Init(&p->poll);
    mmSpinlock_Init(&p->locker, NULL);
    p->poll_timeout = MM_SELECTOR_IDLE_SLEEP_MSEC;
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_NET void mmSelector_Destroy(struct mmSelector* p)
{
    mmSelectorCallback_Destroy(&p->callback);
    mmSelect_Destroy(&p->poll);
    mmSpinlock_Destroy(&p->locker);
    p->poll_timeout = MM_SELECTOR_IDLE_SLEEP_MSEC;
    p->state = MM_TS_CLOSED;
}

// lock.
MM_EXPORT_NET void mmSelector_Lock(struct mmSelector* p)
{
    mmSpinlock_Lock(&p->locker);
}
// unlock.
MM_EXPORT_NET void mmSelector_Unlock(struct mmSelector* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_NET void mmSelector_SetCallback(struct mmSelector* p, struct mmSelectorCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = (*callback);
}

MM_EXPORT_NET void mmSelector_FdAdd(struct mmSelector* p, mmSocket_t fd, void* u)
{
    mmSelect_SetFd(&p->poll, fd);
    mmSelect_AddEvent(&p->poll, MM_PE_READABLE, u);
}
MM_EXPORT_NET void mmSelector_FdRmv(struct mmSelector* p, mmSocket_t fd, void* u)
{
    mmSelect_DelEvent(&p->poll, MM_PE_READABLE, u);
    mmSelect_SetFd(&p->poll, MM_INVALID_SOCKET);
}

MM_EXPORT_NET void mmSelector_PollWait(struct mmSelector* p, mmMSec_t milliseconds)
{
    mmInt_t real_len = 0;
    struct mmPollEvent* pe = NULL;
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    pe = &p->poll.poll_event;
    //
    do
    {
        // the first quick size checking.
        real_len = mmSelect_WaitEvent(&p->poll, milliseconds);
        if (0 == real_len)
        {
            continue;
        }
        else if (-1 == real_len)
        {
            mmErr_t errcode = mmErrno;
            switch (errcode)
            {
            case MM_ENOTSOCK:
            case MM_EBADF:
            {
                // blocking operation was interrupted.
                // this error is not serious.
                mmLogger_LogI(gLogger, "%s %d %p error occur.", __FUNCTION__, __LINE__, p);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                break;
            }
            default:
            {
                mmLogger_LogE(gLogger, "%s %d %p error occur.", __FUNCTION__, __LINE__, p);
                mmLogger_SocketError(gLogger, MM_LOG_ERROR, errcode);
                continue;
            }
            }
        }
        //
        if (pe->mask & MM_PE_READABLE)
        {
            (*p->callback.HandleRecv)(p, pe->s);
        }
        if (pe->mask & MM_PE_WRITABLE)
        {
            (*p->callback.HandleSend)(p, pe->s);
        }
    } while(0);
}

// Poll loop.
MM_EXPORT_NET void mmSelector_PollLoop(struct mmSelector* p)
{
    while (MM_TS_MOTION == p->state)
    {
        mmSelector_PollWait(p, p->poll_timeout);
    }
}

// start wait thread.
MM_EXPORT_NET void mmSelector_Start(struct mmSelector* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->poll_thread, NULL, &__static_mmSelector_PollWaitThread, p);
}
// interrupt wait thread.
MM_EXPORT_NET void mmSelector_Interrupt(struct mmSelector* p)
{
    p->state = MM_TS_CLOSED;
}
// shutdown wait thread.
MM_EXPORT_NET void mmSelector_Shutdown(struct mmSelector* p)
{
    p->state = MM_TS_FINISH;
}
// join wait thread.
MM_EXPORT_NET void mmSelector_Join(struct mmSelector* p)
{
    pthread_join(p->poll_thread, NULL);
}

static void* __static_mmSelector_PollWaitThread(void* args)
{
    struct mmSelector* p = (struct mmSelector*)(args);
    mmSelector_PollLoop(p);
    return NULL;
}
