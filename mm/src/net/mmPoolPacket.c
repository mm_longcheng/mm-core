/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPoolPacket.h"
#include "net/mmStreambufPacket.h"

#include "core/mmAlloc.h"

static const size_t MM_POOL_PACKET_MAX_TRIM_LENGTH = (4 * MM_POOL_PACKET_TRIM_PAGE - 4 + 3) * MM_STREAMBUF_PAGE_SIZE / 4;

MM_EXPORT_NET void mmPoolPacket_ChunkProduce(struct mmStreambuf* p, struct mmPoolPacketElem* v, size_t hlength, size_t blength)
{
    mmPoolPacketElem_Init(v);
    v->streambuf = p;
    //
    v->packet.hbuff.length = hlength;
    v->packet.bbuff.length = blength;
    //
    mmStreambufPacket_Overdraft(p, &v->packet);
}
MM_EXPORT_NET void mmPoolPacket_ChunkRecycle(struct mmStreambuf* p, struct mmPoolPacketElem* v)
{
    mmStreambufPacket_Repayment(p, &v->packet);
    //
    v->streambuf = NULL;
    mmPoolPacketElem_Destroy(v);
}

MM_EXPORT_NET int mmPoolPacket_ChunkProduceComplete(struct mmStreambuf* p)
{
    return MM_POOL_PACKET_MAX_TRIM_LENGTH < p->pptr;
}
MM_EXPORT_NET int mmPoolPacket_ChunkRecycleComplete(struct mmStreambuf* p)
{
    return MM_POOL_PACKET_MAX_TRIM_LENGTH < p->gptr;
}

MM_EXPORT_NET void mmPoolPacketElem_Init(struct mmPoolPacketElem* p)
{
    mmPacket_Init(&p->packet);
    mmSockaddr_Init(&p->ss_remote);
    p->streambuf = NULL;
    p->obj = NULL;
}
MM_EXPORT_NET void mmPoolPacketElem_Destroy(struct mmPoolPacketElem* p)
{
    mmPacket_Destroy(&p->packet);
    mmSockaddr_Destroy(&p->ss_remote);
    p->streambuf = NULL;
    p->obj = NULL;
}
MM_EXPORT_NET void mmPoolPacketElem_Reset(struct mmPoolPacketElem* p)
{
    mmPacket_Reset(&p->packet);
    mmSockaddr_Reset(&p->ss_remote);
    p->streambuf = NULL;
    p->obj = NULL;
}
MM_EXPORT_NET void mmPoolPacketElem_SetRemote(struct mmPoolPacketElem* p, struct mmSockaddr* remote)
{
    mmMemcpy(&p->ss_remote, remote, sizeof(struct mmSockaddr));
}
MM_EXPORT_NET void mmPoolPacketElem_SetObject(struct mmPoolPacketElem* p, void* obj)
{
    p->obj = obj;
}
MM_EXPORT_NET void mmPoolPacketElem_SetPacket(struct mmPoolPacketElem* p, struct mmPacket* packet)
{
    mmPacket_CopyWeak(packet, &p->packet);
}
MM_EXPORT_NET void mmPoolPacketElem_SetBuffer(struct mmPoolPacketElem* p, struct mmPacketHead* packet_head, struct mmByteBuffer* bbuff)
{
    struct mmPacketHead phead;
    p->packet.phead = *packet_head;
    mmMemcpy(p->packet.bbuff.buffer + p->packet.bbuff.offset, bbuff->buffer + bbuff->offset, bbuff->length);
    mmPacket_HeadEncode(&p->packet, &p->packet.hbuff, &phead);
}

MM_EXPORT_NET void mmPoolPacket_Init(struct mmPoolPacket* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmPoolElement_Init(&p->pool_element);
    mmRbtsetVpt_Init(&p->pool);
    mmRbtsetVpt_Init(&p->used);
    mmRbtsetVpt_Init(&p->idle);
    p->current = NULL;
    p->rate_trim_border = MM_POOL_PACKET_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_POOL_PACKET_RATE_TRIM_NUMBER_DEFAULT;

    hAllocator.Produce = &mmPoolPacketElem_Init;
    hAllocator.Recycle = &mmPoolPacketElem_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmPoolPacketElem));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_NET void mmPoolPacket_Destroy(struct mmPoolPacket* p)
{
    mmPoolPacket_ClearChunk(p);
    //
    mmPoolElement_Destroy(&p->pool_element);
    mmRbtsetVpt_Destroy(&p->pool);
    mmRbtsetVpt_Destroy(&p->used);
    mmRbtsetVpt_Destroy(&p->idle);
    p->current = NULL;
    p->rate_trim_border = MM_POOL_PACKET_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_POOL_PACKET_RATE_TRIM_NUMBER_DEFAULT;
}
MM_EXPORT_NET void mmPoolPacket_Lock(struct mmPoolPacket* p)
{
    mmPoolElement_Lock(&p->pool_element);
}
MM_EXPORT_NET void mmPoolPacket_Unlock(struct mmPoolPacket* p)
{
    mmPoolElement_Unlock(&p->pool_element);
}
// can not change at pool using.
MM_EXPORT_NET void mmPoolPacket_SetChunkSize(struct mmPoolPacket* p, size_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool_element, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
// can not change at pool using.
MM_EXPORT_NET void mmPoolPacket_SetRateTrimBorder(struct mmPoolPacket* p, float rate_trim_border)
{
    p->rate_trim_border = rate_trim_border;
}
// can not change at pool using.
MM_EXPORT_NET void mmPoolPacket_SetRateTrimNumber(struct mmPoolPacket* p, float rate_trim_number)
{
    p->rate_trim_number = rate_trim_number;
}

MM_EXPORT_NET struct mmStreambuf* mmPoolPacket_AddChunk(struct mmPoolPacket* p)
{
    struct mmStreambuf* e = (struct mmStreambuf*)mmMalloc(sizeof(struct mmStreambuf));
    mmStreambuf_Init(e);
    // aligned memory.
    mmStreambuf_AlignedMemory(e, MM_POOL_PACKET_MAX_TRIM_LENGTH);
    // add chunk to pool set.
    mmRbtsetVpt_Add(&p->pool, e);
    return e;
}
MM_EXPORT_NET void mmPoolPacket_RmvChunk(struct mmPoolPacket* p, struct mmStreambuf* e)
{
    // rmv chunk to pool set.
    mmRbtsetVpt_Rmv(&p->pool, e);
    mmStreambuf_Destroy(e);
    mmFree(e);
}
MM_EXPORT_NET void mmPoolPacket_ClearChunk(struct mmPoolPacket* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtsetVptIterator* it = NULL;
    struct mmStreambuf* e = NULL;
    //
    n = mmRb_First(&p->pool.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
        e = (struct mmStreambuf*)(it->k);
        n = mmRb_Next(n);
        mmRbtsetVpt_Erase(&p->pool, it);
        mmStreambuf_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_NET struct mmPoolPacketElem* mmPoolPacket_Produce(struct mmPoolPacket* p, size_t hlength, size_t blength)
{
    struct mmPoolPacketElem* v = NULL;
    // pool_element produce.
    mmPoolElement_Lock(&p->pool_element);
    v = (struct mmPoolPacketElem*)mmPoolElement_Produce(&p->pool_element);
    mmPoolElement_Unlock(&p->pool_element);
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmPoolPacketElem_Reset(v);
    //
    struct mmStreambuf* e = p->current;
    if (NULL == e)
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetVptIterator* it = NULL;
        // min.
        n = mmRb_First(&p->idle.rbt);
        if (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
            e = (struct mmStreambuf*)it->k;
            // rmv chunk to idle set.
            mmRbtsetVpt_Rmv(&p->idle, e);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        if (NULL == e)
        {
            // add new chunk.
            e = mmPoolPacket_AddChunk(p);
            // add chunk to used set.
            mmRbtsetVpt_Add(&p->used, e);
        }
        // cache the current chunk.
        p->current = e;
    }
    //
    mmPoolPacket_ChunkProduce(e, v, hlength, blength);
    if (mmPoolPacket_ChunkProduceComplete(e))
    {
        p->current = NULL;
    }
    return v;
}
MM_EXPORT_NET void mmPoolPacket_Recycle(struct mmPoolPacket* p, struct mmPoolPacketElem* v)
{
    struct mmStreambuf* e = v->streambuf;
    assert(NULL != e && "NULL != e is a invalid.");
    mmPoolPacket_ChunkRecycle(e, v);
    if (mmPoolPacket_ChunkRecycleComplete(e))
    {
        float idle_sz = 0;
        float memb_sz = 0;
        int trim_number = 0;

        // reset the index for reused.
        mmStreambuf_Reset(e);
        // rmv chunk to used set.
        mmRbtsetVpt_Rmv(&p->used, e);
        // add chunk to idle set.
        mmRbtsetVpt_Add(&p->idle, e);
        // 
        idle_sz = (float)p->idle.size;
        memb_sz = (float)p->pool.size;
        trim_number = (int)(memb_sz * p->rate_trim_number);

        if (idle_sz / memb_sz > p->rate_trim_border)
        {
            int i = 0;
            // 
            struct mmRbNode* n = NULL;
            struct mmRbtsetVptIterator* it = NULL;
            // max
            n = mmRb_Last(&p->idle.rbt);
            while (NULL != n && i < trim_number)
            {
                it = mmRb_Entry(n, struct mmRbtsetVptIterator, n);
                e = (struct mmStreambuf*)it->k;
                n = mmRb_Prev(n);
                mmRbtsetVpt_Erase(&p->idle, it);
                mmPoolPacket_RmvChunk(p, e);
                i++;
            }
        }
    }
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmPoolPacketElem_Reset(v);
    // pool_element recycle.
    mmPoolElement_Lock(&p->pool_element);
    mmPoolElement_Recycle(&p->pool_element, v);
    mmPoolElement_Unlock(&p->pool_element);
}

