/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUdp.h"
#include "net/mmMulcast.h"
#include "core/mmLogger.h"
#include "core/mmErrno.h"
#include "core/mmAlloc.h"
#include "core/mmSpinlock.h"

static int __static_mmUdp_HandleDefault(void* obj, mmUInt8_t* b, size_t o, size_t l, struct mmSockaddr* r)
{
    return 0;
}
static int __static_mmUdp_BrokenDefault(void* obj)
{
    return 0;
}

MM_EXPORT_NET void mmUdpCallback_Init(struct mmUdpCallback* p)
{
    p->Handle = &__static_mmUdp_HandleDefault;
    p->Broken = &__static_mmUdp_BrokenDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmUdpCallback_Destroy(struct mmUdpCallback* p)
{
    p->Handle = &__static_mmUdp_HandleDefault;
    p->Broken = &__static_mmUdp_BrokenDefault;
    p->obj = NULL;
}
MM_EXPORT_NET void mmUdpCallback_Reset(struct mmUdpCallback* p)
{
    p->Handle = &__static_mmUdp_HandleDefault;
    p->Broken = &__static_mmUdp_BrokenDefault;
    p->obj = NULL;
}

MM_EXPORT_NET void mmUdp_Init(struct mmUdp* p)
{
    mmSocket_Init(&p->socket);
    mmStreambuf_Init(&p->buff_recv);
    mmStreambuf_Init(&p->buff_send);
    mmUdpCallback_Init(&p->callback);
    mmSpinlock_Init(&p->locker_i, NULL);
    mmSpinlock_Init(&p->locker_o, NULL);
    p->unique_id = 0;
    p->u = NULL;
}
MM_EXPORT_NET void mmUdp_Destroy(struct mmUdp* p)
{
    mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    mmSocket_Destroy(&p->socket);
    mmStreambuf_Destroy(&p->buff_recv);
    mmStreambuf_Destroy(&p->buff_send);
    mmUdpCallback_Destroy(&p->callback);
    mmSpinlock_Destroy(&p->locker_i);
    mmSpinlock_Destroy(&p->locker_o);
    p->unique_id = 0;
    p->u = NULL;
}

MM_EXPORT_NET void mmUdp_Lock(struct mmUdp* p)
{
    mmSocket_Lock(&p->socket);
    mmSpinlock_Lock(&p->locker_i);
    mmSpinlock_Lock(&p->locker_o);
}
MM_EXPORT_NET void mmUdp_Unlock(struct mmUdp* p)
{
    mmSpinlock_Unlock(&p->locker_o);
    mmSpinlock_Unlock(&p->locker_i);
    mmSocket_Unlock(&p->socket);
}
// s locker. 
MM_EXPORT_NET void mmUdp_SLock(struct mmUdp* p)
{
    mmSocket_Lock(&p->socket);
}
MM_EXPORT_NET void mmUdp_SUnlock(struct mmUdp* p)
{
    mmSocket_Unlock(&p->socket);
}
// i locker. 
MM_EXPORT_NET void mmUdp_ILock(struct mmUdp* p)
{
    mmSpinlock_Lock(&p->locker_i);
}
MM_EXPORT_NET void mmUdp_IUnlock(struct mmUdp* p)
{
    mmSpinlock_Unlock(&p->locker_i);
}
// o locker. 
MM_EXPORT_NET void mmUdp_OLock(struct mmUdp* p)
{
    mmSpinlock_Lock(&p->locker_o);
}
MM_EXPORT_NET void mmUdp_OUnlock(struct mmUdp* p)
{
    mmSpinlock_Unlock(&p->locker_o);
}

MM_EXPORT_NET void mmUdp_Reset(struct mmUdp* p)
{
    mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    mmSocket_Reset(&p->socket);
    mmStreambuf_Reset(&p->buff_recv);
    mmStreambuf_Reset(&p->buff_send);
    mmUdpCallback_Reset(&p->callback);
    // spinlock not reset.
    p->unique_id = 0;
    p->u = NULL;
}
// udp streambuf recv send reset.
MM_EXPORT_NET void mmUdp_ResetStreambuf(struct mmUdp* p)
{
    mmStreambuf_Reset(&p->buff_recv);
    mmStreambuf_Reset(&p->buff_send);
}

// assign addr native by ip port.
MM_EXPORT_NET void mmUdp_SetNative(struct mmUdp* p, const char* node, mmUShort_t port)
{
    mmSocket_SetNative(&p->socket, node, port);
}
// assign addr remote by ip port.
MM_EXPORT_NET void mmUdp_SetRemote(struct mmUdp* p, const char* node, mmUShort_t port)
{
    mmSocket_SetRemote(&p->socket, node, port);
}
// assign addr native by sockaddr_storage.AF_INET;
// after assign native will auto assign the remote ss_family.
MM_EXPORT_NET void mmUdp_SetNativeStorage(struct mmUdp* p, struct mmSockaddr* ss_native)
{
    mmSocket_SetNativeStorage(&p->socket, ss_native);
}
// assign addr remote by sockaddr_storage.AF_INET;
// after assign remote will auto assign the native ss_family.
MM_EXPORT_NET void mmUdp_SetRemoteStorage(struct mmUdp* p, struct mmSockaddr* ss_remote)
{
    mmSocket_SetRemoteStorage(&p->socket, ss_remote);
}

MM_EXPORT_NET void mmUdp_SetCallback(struct mmUdp* p, struct mmUdpCallback* cb)
{
    assert(NULL != cb && "cb is a null.");
    p->callback = (*cb);
}

MM_EXPORT_NET void mmUdp_SetUniqueId(struct mmUdp* p, mmUInt64_t unique_id)
{
    p->unique_id = unique_id;
}
MM_EXPORT_NET mmUInt64_t mmUdp_GetUniqueId(struct mmUdp* p)
{
    return p->unique_id;
}
// context for udp will use the mmSocket. u attribute. such as crypto context.
MM_EXPORT_NET void mmUdp_SetContext(struct mmUdp* p, void* u)
{
    p->u = u;
}
MM_EXPORT_NET void* mmUdp_GetContext(struct mmUdp* p)
{
    return p->u;
}

// fopen socket.
MM_EXPORT_NET void mmUdp_FopenSocket(struct mmUdp* p)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmSocket_FopenSocket(&p->socket, p->socket.ss_native.addr.sa_family, SOCK_DGRAM, 0);
    //
    mmSocket_ToString(&p->socket, link_name);
    //
    if (MM_INVALID_SOCKET != p->socket.socket)
    {
        mmLogger_LogI(gLogger, "%s %d %s success.", __FUNCTION__, __LINE__, link_name);
    }
    else
    {
        mmLogger_LogE(gLogger, "%s %d %s failure.", __FUNCTION__, __LINE__, link_name);
    }
}
// close socket.
MM_EXPORT_NET void mmUdp_CloseSocket(struct mmUdp* p)
{
    mmSocket_CloseSocket(&p->socket);
}
// shutdown socket.
MM_EXPORT_NET void mmUdp_ShutdownSocket(struct mmUdp* p, int opcode)
{
    mmSocket_ShutdownSocket(&p->socket, opcode);
}

MM_EXPORT_NET int mmUdp_MulcastJoin(struct mmUdp* p, const char* mr_multiaddr, const char* mr_interface)
{
    // ss_native ss_remote is the same ss_family.
    return mmMulcast_Join(p->socket.socket, p->socket.ss_native.addr.sa_family, mr_multiaddr, mr_interface);
}
MM_EXPORT_NET int mmUdp_MulcastDrop(struct mmUdp* p, const char* mr_multiaddr, const char* mr_interface)
{
    // ss_native ss_remote is the same ss_family.
    return mmMulcast_Drop(p->socket.socket, p->socket.ss_native.addr.sa_family, mr_multiaddr, mr_interface);
}

// handle recv for buffer pool and pool max size.
MM_EXPORT_NET void mmUdp_HandleRecv(struct mmUdp* p)
{
    // mmStreambuf page size MM_STREAMBUF_PAGE_SIZE is equal MM_SOCKET_PAGE_SIZE.
    // reduce the mmStreambuf frequently change max_size, here we use MM_SOCKET_PAGE_SIZE for max_length for recv dgram.
    size_t max_length = MM_SOCKET_PAGE_SIZE;
    socklen_t remote_size;
    struct mmSockaddr remote;
    int real_len = 0;
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    //
    remote_size = sizeof(struct mmSockaddr);
    // we not need assert the fd is must valid.
    assert(p->callback.Handle && p->callback.Broken && "callback is invalid.");
    //
    do
    {
        // if the idle put size is lack, we try remove the get buffer.
        mmStreambuf_AlignedMemory(&p->buff_recv, max_length);
        // recv dgram.
        real_len = mmSocket_RecvDgram(&p->socket, p->buff_recv.buff, p->buff_recv.pptr, max_length, 0, (struct sockaddr*)(&remote), &remote_size);
        if (-1 == real_len)
        {
            mmErr_t errcode = mmErrno;
            //-MM_EBADF
            //-MM_ENOTSOCK
            //-MM_EAGAIN
            //-MM_ENOBUFS
            //-MM_ENOMEM
            //-MM_EINVAL

            switch (errcode)
            {
            case MM_EINVAL:
            {
                // errno:(10022) An invalid argument was supplied.
                // when we not sendto but recvfrom will case this error.
                // this error is not serious.
                //
                // sleep_msec for waiting for something sendto remote.
                break;
            }
            case MM_ECONNRESET:
            {
                // An existing connection was forcibly closed by the remote host.
                // when the remote connection not open will case this error.
                break;
            }
            case MM_EAGAIN:
            {
                // MM_EAGAIN EWOULDBLOCK
                // is timeout if setsocketopt SO_RCVTIMEO.
                break;
            }
            case MM_EBADF:
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                // blocking operation was interrupted.
                // this error is not serious.
                mmLogger_LogI(gLogger, "%s %d %p error occur.", __FUNCTION__, __LINE__, p);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                break;
            }
            default:
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                mmSocket_ToString(&p->socket, link_name);
                // at here when we want interrupt accept, will case a socket errno.
                // [errno:(53) Software caused connection abort] it is expected result.
                mmLogger_LogI(gLogger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                // (*(p->callback.broken))(p);
                break;
            }
            }

            // No matter what the result, we break this while loop.
            break;
        }
        else if (0 == real_len)
        {
            mmErr_t errcode = mmErrno;
            // if this recv operate 0 == real_len, means the recv buffer is full or target socket is closed.
            // so we must use poll for checking the io event make sure read event is fire.
            // at here send length is 0 will fire the broken event.
            struct mmLogger* gLogger = mmLogger_Instance();
            mmSocket_ToString(&p->socket, link_name);
            // the other side close the socket.
            mmLogger_LogI(gLogger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
            mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
            // (*(p->callback.broken))(p);
            break;
        }
        // pbump the position.
        // note: the real buffer is 
        //       buffer = p->buff_recv.buff
        //       offset = (mmUInt32_t)p->buff_recv.pptr - real_len
        //       length = real_len
        mmStreambuf_Pbump(&p->buff_recv, real_len);
        //
        // udp not like tcp socket just recv until the udp socket is empty.
        mmUdp_BufferRecv(p, p->buff_recv.buff, (size_t)p->buff_recv.pptr - real_len, real_len, &remote);
    } while (1);
}
// handle send for buffer pool and pool max size.
MM_EXPORT_NET void mmUdp_HandleSend(struct mmUdp* p)
{
    // do nothing.
}

// handle recv for buffer pool and pool max size.
MM_EXPORT_NET int mmUdp_BufferRecv(struct mmUdp* p, mmUInt8_t* b, size_t o, size_t l, struct mmSockaddr* r)
{
    int code = 0;
    assert(p->callback.Handle && "callback.Handle is invalid.");
    code = (*(p->callback.Handle))(p, b, o, l, r);
    if (0 != code)
    {
        // invalid tcp message buffer decode.
        // shutdown socket immediately.
        mmSocket_ShutdownSocket(&p->socket, MM_BOTH_SHUTDOWN);
    }
    return code;
}
// handle send for buffer pool and pool max size.
MM_EXPORT_NET int mmUdp_BufferSend(struct mmUdp* p, mmUInt8_t* b, size_t o, size_t l, struct mmSockaddr* r)
{
    int real_len = 0;
    int send_len = 0;
    do
    {
        if (0 == l)
        {
            // nothing for send break immediately.
            break;
        }
        real_len = mmSocket_SendDgram(&p->socket, b, o, l, 0, (struct sockaddr*)r, mmSockaddr_Length(r));
        if (-1 == real_len)
        {
            mmErr_t errcode = mmErrno;
            if (MM_EAGAIN == errcode)
            {
                // MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
                // MM_EAGAIN is well.
                break;
            }
            else
            {
                char link_name[MM_LINK_NAME_LENGTH];
                // if udp send error we logger it out.
                struct mmLogger* gLogger = mmLogger_Instance();
                mmSocket_ToString(&p->socket, link_name);
                // error occur.
                mmLogger_LogI(gLogger, "%s %d %s error occur.", __FUNCTION__, __LINE__, link_name);
                mmLogger_SocketError(gLogger, MM_LOG_INFO, errcode);
                send_len = -1;
                break;
            }
        }
        else
        {
            send_len = real_len;
            break;
        }
    } while (1);
    return send_len;
}

// handle send data by flush send buffer.
MM_EXPORT_NET int mmUdp_FlushSend(struct mmUdp* p, struct mmSockaddr* remote)
{
    size_t sz = mmStreambuf_Size(&p->buff_send);
    // not like tcp, we just send udp buffer all in one.
    // use self streambuf.
    mmUdp_BufferSend(p, p->buff_send.buff, (mmUInt32_t)p->buff_send.gptr, (mmUInt32_t)sz, remote);
    // we not need check why udp send error. just gbump the buffer.
    mmStreambuf_Gbump(&p->buff_send, sz);
    return (int)sz;
}
