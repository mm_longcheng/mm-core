/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmColor_h__
#define __mmColor_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmColor
{
    float r;
    float g;
    float b;
    float a;
};

MM_EXPORT_DLL void mmColor_Init(struct mmColor* p);
MM_EXPORT_DLL void mmColor_Destroy(struct mmColor* p);

MM_EXPORT_DLL void mmColor_Reset(struct mmColor* p);
MM_EXPORT_DLL void mmColor_Zero(struct mmColor* p);
MM_EXPORT_DLL void mmColor_Make(struct mmColor* p, float r, float g, float b, float a);

MM_EXPORT_DLL int mmColor_Equals(struct mmColor* p, struct mmColor* q);
MM_EXPORT_DLL void mmColor_Assign(struct mmColor* p, struct mmColor* q);

MM_EXPORT_DLL int mmColor_Compare(struct mmColor* p, struct mmColor* q);

MM_EXPORT_DLL extern const struct mmColor mmColorBlackTransparent;

MM_EXPORT_DLL void mmColorAssign(float l[4], const float r[4]);
MM_EXPORT_DLL int mmColorCompare(const float l[4], const float r[4]);
MM_EXPORT_DLL void mmColorMake(float c[4], float r, float g, float b, float a);

// 0xAARRGGBB
MM_EXPORT_DLL void mmColorFromARGB(float v[4], mmUInt32_t c);
// 0xAARRGGBB
MM_EXPORT_DLL void mmColorToARGB(float v[4], mmUInt32_t* c);

MM_EXPORT_DLL int mmColorIsTransparent(float v[4]);

// "#RRGGBBAA" or "#RRGGBB"(AA = FF) ==> RRGGBBAA(unsigned int[4])
MM_EXPORT_DLL void mmValue_AToUIntARGB(const char* s, unsigned int v[4]);
MM_EXPORT_DLL void mmValue_UIntARGBToA(const unsigned int v[4], char s[16]);
// "#RRGGBBAA" or "#RRGGBB"(AA = FF) ==> AARRGGBB(mmUInt32_t)
MM_EXPORT_DLL void mmValue_AToARGB(const char* s, mmUInt32_t* v);
MM_EXPORT_DLL void mmValue_ARGBToA(const mmUInt32_t* v, char s[16]);
// "#RRGGBBAA" or "#RRGGBB"(AA = FF) ==> RRGGBBAA(float[4])
MM_EXPORT_DLL void mmValue_AToFloatRGBA(const char* s, float v[4]);
MM_EXPORT_DLL void mmValue_FloatRGBAToA(const float v[4], char s[16]);

/*!
 * @brief averages the color channels into one value
 *
 * @param[in]  c RGB color
 */
mmInline
float
mmColorLuminance(float c[3])
{
    float l[3] = { 0.212671f, 0.715160f, 0.072169f };
    return c[0] * l[0] + c[1] * l[1] + c[2] * l[2];
}

#include "core/mmSuffix.h"

#endif//__mmRange_h__
