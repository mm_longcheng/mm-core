/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmColor.h"

#include "core/mmAlloc.h"
#include "core/mmStringUtils.h"
#include "core/mmValueTransform.h"

MM_EXPORT_DLL void mmColor_Init(struct mmColor* p)
{
    p->r = 0.0f;
    p->g = 0.0f;
    p->b = 0.0f;
    p->a = 0.0f;
}
MM_EXPORT_DLL void mmColor_Destroy(struct mmColor* p)
{
    p->r = 0.0f;
    p->g = 0.0f;
    p->b = 0.0f;
    p->a = 0.0f;
}

MM_EXPORT_DLL void mmColor_Reset(struct mmColor* p)
{
    p->r = 0.0f;
    p->g = 0.0f;
    p->b = 0.0f;
    p->a = 0.0f;
}
MM_EXPORT_DLL void mmColor_Zero(struct mmColor* p)
{
    p->r = 0.0f;
    p->g = 0.0f;
    p->b = 0.0f;
    p->a = 0.0f;
}
MM_EXPORT_DLL void mmColor_Make(struct mmColor* p, float r, float g, float b, float a)
{
    p->r = r;
    p->g = g;
    p->b = b;
    p->a = a;
}

MM_EXPORT_DLL int mmColor_Equals(struct mmColor* p, struct mmColor* q)
{
    return p->r == q->r && p->g == q->g && p->b == q->b && p->a == q->a;
}
MM_EXPORT_DLL void mmColor_Assign(struct mmColor* p, struct mmColor* q)
{
    p->r = q->r;
    p->g = q->g;
    p->b = q->b;
    p->a = q->a;
}

MM_EXPORT_DLL int mmColor_Compare(struct mmColor* p, struct mmColor* q)
{
    return mmMemcmp(p, q, sizeof(struct mmColor));
}

MM_EXPORT_DLL const struct mmColor mmColorBlackTransparent = { 0.0f, 0.0f, 0.0f, 0.0f, };

MM_EXPORT_DLL void mmColorAssign(float l[4], const float r[4])
{
    mmMemcpy(l, r, sizeof(float) * 4);
}
MM_EXPORT_DLL int mmColorCompare(const float l[4], const float r[4])
{
    return mmMemcmp(l, r, sizeof(float) * 4);
}
MM_EXPORT_DLL void mmColorMake(float c[4], float r, float g, float b, float a)
{
    c[0] = r;
    c[1] = g;
    c[2] = b;
    c[3] = a;
}
MM_EXPORT_DLL void mmColorFromARGB(float v[4], mmUInt32_t c)
{
    // 0xAARRGGBB
    v[2] = (c & 0x000000FF) / 255.0f;
    c >>= 8;
    v[1] = (c & 0x000000FF) / 255.0f;
    c >>= 8;
    v[0] = (c & 0x000000FF) / 255.0f;
    c >>= 8;
    v[3] = (c & 0x000000FF) / 255.0f;
}
MM_EXPORT_DLL void mmColorToARGB(float v[4], mmUInt32_t* c) 
{
    // 0xAARRGGBB
    mmUInt32_t r = (mmUInt32_t)(v[0] * 255.0f);
    mmUInt32_t g = (mmUInt32_t)(v[1] * 255.0f);
    mmUInt32_t b = (mmUInt32_t)(v[2] * 255.0f);
    mmUInt32_t a = (mmUInt32_t)(v[3] * 255.0f);
    (*c) = (a << 24) | (r << 16) | (g << 8) | (b);
}

MM_EXPORT_DLL int mmColorIsTransparent(float v[4])
{
    return (0 == v[3]) || (0 == v[0] && 0 == v[1] && 0 == v[2]);
}

MM_EXPORT_DLL void mmValue_AToUIntARGB(const char* s, unsigned int v[4])
{
    size_t idx;
    size_t size;
    size_t l = strlen(s);
    mmCStringRemoveBothSidesSpace(s, l, &idx, &size);
    mmHexBuffToDigit(&v[0], &s[idx + 1], 2);
    mmHexBuffToDigit(&v[1], &s[idx + 3], 2);
    mmHexBuffToDigit(&v[2], &s[idx + 5], 2);
    if (size > 7)
    {
        // "#RRGGBBAA"
        mmHexBuffToDigit(&v[3], &s[idx + 7], 2);
    }
    else
    {
        // "#RRGGBB"
        v[3] = 0xFF;
    }
}

MM_EXPORT_DLL void mmValue_UIntARGBToA(const unsigned int v[4], char s[16])
{
    s[0] = '#';
    s[1] = mmHexDigit[(v[0] >> 4) & 0x0F];
    s[2] = mmHexDigit[(v[0]     ) & 0x0F];
    s[3] = mmHexDigit[(v[1] >> 4) & 0x0F];
    s[4] = mmHexDigit[(v[1]     ) & 0x0F];
    s[5] = mmHexDigit[(v[2] >> 4) & 0x0F];
    s[6] = mmHexDigit[(v[2]     ) & 0x0F];
    if (0xFF == v[3])
    {
        // "#RRGGBB"
        s[7] = 0;
    }
    else
    {
        // "#RRGGBBAA"
        s[7] = mmHexDigit[(v[3] >> 4) & 0x0F];
        s[8] = mmHexDigit[(v[3]     ) & 0x0F];
        s[9] = 0;
    }
}
MM_EXPORT_DLL void mmValue_AToARGB(const char* s, mmUInt32_t* v)
{
    unsigned int u[4];
    mmUInt32_t c = 0;
    mmValue_AToUIntARGB(s, u);
    // AARRGGBB(mmUInt32_t)
    c |= (u[3] << 24);
    c |= (u[0] << 16);
    c |= (u[1] <<  8);
    c |= (u[2]      );
    (*v) = c;
}

MM_EXPORT_DLL void mmValue_ARGBToA(const mmUInt32_t* v, char s[16])
{
    unsigned int u[4];
    mmUInt32_t c = (*v);
    // AARRGGBB(mmUInt32_t)
    u[3] = (mmUInt8_t)((c >> 24) & 0x000000FF);
    u[0] = (mmUInt8_t)((c >> 16) & 0x000000FF);
    u[1] = (mmUInt8_t)((c >>  8) & 0x000000FF);
    u[2] = (mmUInt8_t)((c      ) & 0x000000FF);
    mmValue_UIntARGBToA(u, s);
}

MM_EXPORT_DLL void mmValue_AToFloatRGBA(const char* s, float v[4])
{
    unsigned int u[4];
    mmValue_AToUIntARGB(s, u);
    // RRGGBBAA(float[4])
    v[0] = u[0] / 255.0f;
    v[1] = u[1] / 255.0f;
    v[2] = u[2] / 255.0f;
    v[3] = u[3] / 255.0f;
}

MM_EXPORT_DLL void mmValue_FloatRGBAToA(const float v[4], char s[16])
{
    unsigned int u[4];
    // RRGGBBAA
    u[0] = (unsigned int)(v[0] * 255.0f);
    u[1] = (unsigned int)(v[1] * 255.0f);
    u[2] = (unsigned int)(v[2] * 255.0f);
    u[3] = (unsigned int)(v[3] * 255.0f);
    mmValue_UIntARGBToA(u, s);
}
