/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmParseTSV_h__
#define __mmParseTSV_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmStreambuf.h"

#include "container/mmRbtreeString.h"

#include "core/mmPrefix.h"

// https://www.rfc-editor.org/rfc/rfc4180.txt
//
// TSV not CSV, more simple.
//     (0, 0) is row number.
//     '\t'   separate word
//     '\r\n' separate line
//     column is same.
//     word can not have '\t' '\r' '\n'
struct mmParseTSV
{
    struct mmStreambuf hStreambuf;
    struct mmRbtreeStrVpt hPrimaryKeyTable;
    char** pTable;
    size_t hTableSize;
    size_t hMaxRowNumber;
    size_t hMaxColumnNumber;
    size_t hRowNumber;
    size_t hColumnNumber;
};
MM_EXPORT_DLL void mmParseTSV_Init(struct mmParseTSV* p);
MM_EXPORT_DLL void mmParseTSV_Destroy(struct mmParseTSV* p);

MM_EXPORT_DLL void mmParseTSV_Reset(struct mmParseTSV* p);

// Pre-allocated.
MM_EXPORT_DLL void mmParseTSV_SetMaxNumber(struct mmParseTSV* p, size_t hMaxRowNumber, size_t hMaxColumnNumber);

// Analysis file.
MM_EXPORT_DLL void mmParseTSV_AnalysisFile(struct mmParseTSV* p, const char* pFileName);

// Analysis buffer.
MM_EXPORT_DLL void mmParseTSV_AnalysisBuffer(struct mmParseTSV* p, mmUInt8_t* buffer, size_t length);

// Analysis current buffer.
MM_EXPORT_DLL void mmParseTSV_AnalysisCurrentBuffer(struct mmParseTSV* p);

// Make primary key table.
// @param o the serach of the starting point
// @param c primary key of the column
MM_EXPORT_DLL void mmParseTSV_MakePrimaryKeyTable(struct mmParseTSV* p, size_t o, size_t c);

// Get word by x and y.
// @param c physical column
// @param r physical row
MM_EXPORT_DLL const char* mmParseTSV_GetWord(const struct mmParseTSV* p, size_t r, size_t c);

// Get physical line by y. Usually used to hold the meter of the table.
// @param r physical row
MM_EXPORT_DLL const char** mmParseTSV_GetPhysicalRow(const struct mmParseTSV* p, size_t r);

// Get line.The primary key column 'must' already sorted.
// @param o the serach of the starting point
// @param c primary key of the column
// @param k row key for serach
MM_EXPORT_DLL const char** mmParseTSV_GetRowByPrimaryKey(const struct mmParseTSV* p, const char* k);

// Clear cache.
MM_EXPORT_DLL void mmParseTSV_Clear(struct mmParseTSV* p);

#include "core/mmSuffix.h"

#endif//__mmParseTSV_h__
