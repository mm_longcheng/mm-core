/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmParseINI_h__
#define __mmParseINI_h__

#include "core/mmString.h"
#include "core/mmStreambuf.h"

#include "container/mmRbtreeString.h"

#include "core/mmPrefix.h"

// Short and simple micro configuration file parse.
struct mmParseINI
{
    struct mmString hFileName;
    struct mmStreambuf hStreambuf;
    struct mmRbtreeStringString hRbTree;
};

MM_EXPORT_DLL void mmParseINI_Init(struct mmParseINI* p);
MM_EXPORT_DLL void mmParseINI_Destroy(struct mmParseINI* p);

MM_EXPORT_DLL void mmParseINI_Reset(struct mmParseINI* p);

// Set INI filename.
MM_EXPORT_DLL void mmParseINI_SetFileName(struct mmParseINI* p, const char* pFileName);

// Analysis file.
MM_EXPORT_DLL void mmParseINI_Analysis(struct mmParseINI* p);

// Analysis buffer.
MM_EXPORT_DLL void mmParseINI_AnalysisBuffer(struct mmParseINI* p, mmUInt8_t* buffer, size_t length);

// Analysis current buffer.
MM_EXPORT_DLL void mmParseINI_AnalysisCurrentBuffer(struct mmParseINI* p);

// Get word by key.
// @param k key
MM_EXPORT_DLL const char* mmParseINI_GetValue(const struct mmParseINI* p, const char* k);

#include "core/mmSuffix.h"

#endif//__mmParseINI_h__
