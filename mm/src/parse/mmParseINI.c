/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmParseINI.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmFileSystem.h"
#include "core/mmStringUtils.h"

static void __static_mmParseINI_AnalysisCurrentWord(struct mmParseINI* p, struct mmString* k, struct mmString* v, size_t b, size_t a, size_t e);

MM_EXPORT_DLL void mmParseINI_Init(struct mmParseINI* p)
{
    mmString_Init(&p->hFileName);
    mmStreambuf_Init(&p->hStreambuf);
    mmRbtreeStringString_Init(&p->hRbTree);
}

MM_EXPORT_DLL void mmParseINI_Destroy(struct mmParseINI* p)
{
    mmRbtreeStringString_Destroy(&p->hRbTree);
    mmStreambuf_Destroy(&p->hStreambuf);
    mmString_Destroy(&p->hFileName);
}

MM_EXPORT_DLL void mmParseINI_Reset(struct mmParseINI* p)
{
    mmString_Reset(&p->hFileName);
    mmStreambuf_Reset(&p->hStreambuf);
    mmRbtreeStringString_Clear(&p->hRbTree);
}

MM_EXPORT_DLL void mmParseINI_SetFileName(struct mmParseINI* p, const char* pFileName)
{
    mmString_Assigns(&p->hFileName, pFileName);
}

MM_EXPORT_DLL void mmParseINI_Analysis(struct mmParseINI* p)
{
    int code = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmStat_t s;
        FILE* f = NULL;
        size_t n = 0;
        mmUInt8_t* buffer = NULL;
        const char* pFileName = mmString_CStr(&p->hFileName);
        
        if (0 != mmAccess(pFileName, MM_ACCESS_F_OK))
        {
            // the file is not exist.
            mmLogger_LogE(gLogger, "%s %d The file: %s is not exist.", __FUNCTION__, __LINE__, pFileName);
            code = 1;
            break;
        }
        if (0 != mmStat(pFileName, &s))
        {
            // the file can not stat.
            mmLogger_LogE(gLogger, "%s %d The file: %s stat failure.", __FUNCTION__, __LINE__, pFileName);
            code = 2;
            break;
        }
        if (0 == s.st_size)
        {
            // the file size is 0.
            mmLogger_LogE(gLogger, "%s %d The file: %s size is 0.", __FUNCTION__, __LINE__, pFileName);
            code = 3;
            break;
        }
        f = fopen(pFileName, "rb");
        if (NULL == f)
        {
            // the file can not fopen.
            mmLogger_LogE(gLogger, "%s %d The file: %s fopen failure.", __FUNCTION__, __LINE__, pFileName);
            code = 4;
            break;
        }
        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, (size_t)s.st_size);
        buffer = (p->hStreambuf.buff + p->hStreambuf.pptr);
        n = fread(buffer, 1, (size_t)s.st_size, f);
        if (n != s.st_size)
        {
            // the file can fread .
            mmLogger_LogE(gLogger, "%s %d The file: %s fread failure.", __FUNCTION__, __LINE__, pFileName);
            code = 5;
            break;
        }
        mmStreambuf_Pbump(&p->hStreambuf, n);
        mmParseINI_AnalysisCurrentBuffer(p);
        mmStreambuf_Gbump(&p->hStreambuf, n);
        fclose(f);
        f = NULL;
    }while(0);
}
MM_EXPORT_DLL void mmParseINI_AnalysisBuffer(struct mmParseINI* p, mmUInt8_t* buffer, size_t length)
{
    mmUInt8_t* pBuffer = NULL;

    mmStreambuf_Reset(&p->hStreambuf);
    mmStreambuf_AlignedMemory(&p->hStreambuf, length);
    pBuffer = (p->hStreambuf.buff + p->hStreambuf.pptr);

    mmMemcpy(pBuffer, buffer, length);

    mmStreambuf_Pbump(&p->hStreambuf, length);
    mmParseINI_AnalysisCurrentBuffer(p);
    mmStreambuf_Gbump(&p->hStreambuf, length);
}
MM_EXPORT_DLL void mmParseINI_AnalysisCurrentBuffer(struct mmParseINI* p)
{
    size_t i = 0;
    size_t b = 0;
    size_t e = 0;
    size_t a = 0;

    mmUInt8_t c = 0;

    struct mmString k;
    struct mmString v;

    mmUInt8_t* buffer = (p->hStreambuf.buff + p->hStreambuf.gptr);

    mmString_Init(&k);
    mmString_Init(&v);

    mmRbtreeStringString_Clear(&p->hRbTree);

    while (i < p->hStreambuf.pptr)
    {
        c = buffer[i];
        switch (c)
        {
        case '=':
        {
            a = i;
            i++;
        }
        break;

        case '\n':
        {
            __static_mmParseINI_AnalysisCurrentWord(p, &k, &v, b, a, e);

            i++;
            b = i;
            e = i;
            a = i;
        }
        break;

        default:
        {
            i++;
            e++;
        }
        break;
        }
    }

    // the last line can be not '\n'.
    __static_mmParseINI_AnalysisCurrentWord(p, &k, &v, b, a, e);

    mmString_Destroy(&k);
    mmString_Destroy(&v);
}

MM_EXPORT_DLL const char* mmParseINI_GetValue(const struct mmParseINI* p, const char* k)
{
    struct mmString hWeakKey;
    struct mmRbtreeStringStringIterator* it = NULL;
    mmString_MakeWeaks(&hWeakKey, k);
    it = mmRbtreeStringString_GetIterator(&p->hRbTree, &hWeakKey);
    return (NULL == it) ? "" : mmString_CStr(&it->v);
}

static void __static_mmParseINI_AnalysisCurrentWord(struct mmParseINI* p, struct mmString* k, struct mmString* v, size_t b, size_t a, size_t e)
{
    mmUInt8_t* buffer = (p->hStreambuf.buff + p->hStreambuf.gptr);
    mmUInt8_t s0 = buffer[b + 0];
    mmUInt8_t s1 = buffer[b + 1];
    
    mmString_Assignsn(k, (const char*)&buffer[b + 0], a - b);
    mmString_Assignsn(v, (const char*)&buffer[a + 1], e - a);

    // 1. a == b   no '='
    // 2. "//"     comment
    if((a != b) && ('/' != s0 || '/' != s1))
    {
        mmString_RemoveBothSidesSpace(k);
        mmString_RemoveBothSidesSpace(v);
        
        // mmPrintf("[%s] = [%s] \n", k->s, v->s);
        
        mmRbtreeStringString_Set(&p->hRbTree, k, v);
    }
}
