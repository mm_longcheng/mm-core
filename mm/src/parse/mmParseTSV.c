/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmParseTSV.h"
#include "core/mmFileSystem.h"
#include "core/mmLogger.h"
#include "core/mmAlloc.h"
#include "core/mmAtoi.h"

static void __static_ParseTSV_AnalysisTableNumber(struct mmParseTSV* p);
static void __static_ParseTSV_AnalysisTableBuffer(struct mmParseTSV* p);

static MM_INLINE void __static_ParseTSV_AnalysisCurrentWord(struct mmParseTSV* p, mmUInt8_t* w, size_t z, size_t x, size_t y)
{
    // we only recording valid range data.
    if (x < p->hMaxColumnNumber && y < p->hMaxRowNumber)
    {
        // simple replacement '\t' '\r' ==> '\0'
        w[z] = '\0';
        p->pTable[y * p->hColumnNumber + x] = (char*)w;
        // mmPrintf("(%u, %u)%s\t", (uint32_t)x, (uint32_t)y, w);
    }
}

MM_EXPORT_DLL void mmParseTSV_Init(struct mmParseTSV* p)
{
    mmStreambuf_Init(&p->hStreambuf);
    mmRbtreeStrVpt_Init(&p->hPrimaryKeyTable);
    p->pTable = NULL;
    p->hTableSize = 0;
    p->hMaxRowNumber = 0;
    p->hMaxColumnNumber = 0;
    p->hRowNumber = 0;
    p->hColumnNumber = 0;
}
MM_EXPORT_DLL void mmParseTSV_Destroy(struct mmParseTSV* p)
{
    mmParseTSV_Clear(p);
    
    p->hColumnNumber = 0;
    p->hRowNumber = 0;
    p->hMaxColumnNumber = 0;
    p->hMaxRowNumber = 0;
    p->hTableSize = 0;
    p->pTable = NULL;
    mmRbtreeStrVpt_Destroy(&p->hPrimaryKeyTable);
    mmStreambuf_Destroy(&p->hStreambuf);
}

MM_EXPORT_DLL void mmParseTSV_Reset(struct mmParseTSV* p)
{
    mmParseTSV_Clear(p);

    mmStreambuf_Reset(&p->hStreambuf);
    mmRbtreeStrVpt_Clear(&p->hPrimaryKeyTable);
    p->pTable = NULL;
    p->hTableSize = 0;
    p->hMaxRowNumber = 0;
    p->hMaxColumnNumber = 0;
    p->hRowNumber = 0;
    p->hColumnNumber = 0;
}

MM_EXPORT_DLL void mmParseTSV_SetMaxNumber(struct mmParseTSV* p, size_t hMaxRowNumber, size_t hMaxColumnNumber)
{
    p->hMaxRowNumber = hMaxRowNumber;
    p->hMaxColumnNumber = hMaxColumnNumber;
}

MM_EXPORT_DLL void mmParseTSV_AnalysisFile(struct mmParseTSV* p, const char* pFileName)
{
    int code = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        mmStat_t s;
        FILE* f = NULL;
        size_t n = 0;
        mmUInt8_t* buff = NULL;

        if (0 != mmAccess(pFileName, MM_ACCESS_F_OK))
        {
            // the file is not exist.
            mmLogger_LogE(gLogger, "%s %d The file: %s is not exist.", __FUNCTION__, __LINE__, pFileName);
            code = 1;
            break;
        }
        if (0 != mmStat(pFileName, &s))
        {
            // the file can not stat.
            mmLogger_LogE(gLogger, "%s %d The file: %s stat failure.", __FUNCTION__, __LINE__, pFileName);
            code = 2;
            break;
        }
        if (0 == s.st_size)
        {
            // the file size is 0.
            mmLogger_LogE(gLogger, "%s %d The file: %s size is 0.", __FUNCTION__, __LINE__, pFileName);
            code = 3;
            break;
        }
        f = fopen(pFileName, "rb");
        if (NULL == f)
        {
            // the file can not fopen.
            mmLogger_LogE(gLogger, "%s %d The file: %s fopen failure.", __FUNCTION__, __LINE__, pFileName);
            code = 4;
            break;
        }
        // termination is need by last line of the end. append 1 byte for '\0'.
        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, (size_t)(s.st_size + 1));
        buff = (p->hStreambuf.buff + p->hStreambuf.pptr);
        buff[s.st_size] = '\0';
        n = fread(buff, 1, (size_t)s.st_size, f);
        if (n != s.st_size)
        {
            // the file can not fread.
            mmLogger_LogE(gLogger, "%s %d The file: %s fread failure.", __FUNCTION__, __LINE__, pFileName);
            code = 5;
            break;
        }
        mmStreambuf_Pbump(&p->hStreambuf, n);
        mmParseTSV_AnalysisCurrentBuffer(p);
        mmStreambuf_Gbump(&p->hStreambuf, n);
        fclose(f);
        f = NULL;
    }while(0);
}
MM_EXPORT_DLL void mmParseTSV_AnalysisBuffer(struct mmParseTSV* p, mmUInt8_t* buffer, size_t length)
{
    mmUInt8_t* buff = NULL;
    mmStreambuf_Reset(&p->hStreambuf);
    mmStreambuf_AlignedMemory(&p->hStreambuf, length + 1);
    buff = (p->hStreambuf.buff + p->hStreambuf.pptr);
    mmMemcpy(buff, buffer, length);
    buff[length] = '\0';
    mmStreambuf_Pbump(&p->hStreambuf, length);
    mmParseTSV_AnalysisCurrentBuffer(p);
    mmStreambuf_Gbump(&p->hStreambuf, length);
}
MM_EXPORT_DLL void mmParseTSV_AnalysisCurrentBuffer(struct mmParseTSV* p)
{
    if (0 == p->hMaxRowNumber || 0 == p->hMaxColumnNumber)
    {
        __static_ParseTSV_AnalysisTableNumber(p);
    }
    
    if (0 == p->hMaxRowNumber || 0 == p->hMaxColumnNumber)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d The max number is 0.", __FUNCTION__, __LINE__);
    }
    else
    {
        __static_ParseTSV_AnalysisTableBuffer(p);
    }
}
MM_EXPORT_DLL void mmParseTSV_MakePrimaryKeyTable(struct mmParseTSV* p, size_t o, size_t c)
{
    size_t index = 0;
    const char** u = NULL;
    const char* w = NULL;
    char** pTable = p->pTable;
    
    // clear first.
    mmRbtreeStrVpt_Clear(&p->hPrimaryKeyTable);
    
    for(index = o;index < p->hRowNumber;++index)
    {
        u = (const char**)(&pTable[index * p->hColumnNumber]);
        w = u[c];
        mmRbtreeStrVpt_Set(&p->hPrimaryKeyTable, w, (void*)u);
    }
}
MM_EXPORT_DLL const char* mmParseTSV_GetWord(const struct mmParseTSV* p, size_t r, size_t c)
{
    if(r >= p->hRowNumber || c >= p->hColumnNumber)
    {
        return NULL;
    }
    else
    {
        return p->pTable[r * p->hColumnNumber + c];
    }
}
MM_EXPORT_DLL const char** mmParseTSV_GetPhysicalRow(const struct mmParseTSV* p, size_t r)
{
    if(r >= p->hRowNumber)
    {
        return NULL;
    }
    else
    {
        return (const char**)(&p->pTable[r * p->hColumnNumber]);
    }
}
MM_EXPORT_DLL const char** mmParseTSV_GetRowByPrimaryKey(const struct mmParseTSV* p, const char* k)
{
    const char** u = NULL;
    u = (const char**)mmRbtreeStrVpt_Get(&p->hPrimaryKeyTable, k);
    return (NULL == u) ? (const char**)NULL : u;
}
MM_EXPORT_DLL void mmParseTSV_Clear(struct mmParseTSV* p)
{
    mmFree(p->pTable);
    p->pTable = NULL;
    mmStreambuf_Reset(&p->hStreambuf);
    mmRbtreeStrVpt_Clear(&p->hPrimaryKeyTable);
}

static void __static_ParseTSV_AnalysisTableNumber(struct mmParseTSV* p)
{
    size_t i = 0;
    size_t b = 0;
    size_t e = 0;
    size_t x = 0;
    size_t y = 0;
    size_t r = 0;
    size_t rn = 0;
    mmUInt8_t u = 0;
    mmUInt8_t* buff = p->hStreambuf.buff;
    size_t pptr = p->hStreambuf.pptr;
    size_t gptr = p->hStreambuf.gptr;

    i = gptr;

    while (i < pptr)
    {
        switch (buff[i])
        {
        case '\t':
        case '\r':
        {
            r = (0 == x) ? e : r;
            x++;
            i++;
            b = i;
            e = i;
        }
        break;
                
        case '\n':
        {
            p->hMaxColumnNumber = (p->hMaxColumnNumber > x) ? p->hMaxColumnNumber : x;

            x = 0;
            y++;
            i++;
            b = i;
            e = i;

            u = buff[r];
            buff[r] = 0;
            rn = (size_t)mmAtoi64((const char*)buff);
            buff[r] = u;
            p->hMaxRowNumber = (p->hMaxRowNumber > rn) ? p->hMaxRowNumber : rn;
            return;
        }
        break;
                
        default:
        {
            i++;
            e++;
        }
        break;
        }
    }
}
static void __static_ParseTSV_AnalysisTableBuffer(struct mmParseTSV* p)
{
    size_t i = 0;
    size_t b = 0;
    size_t e = 0;
    size_t x = 0;
    size_t y = 0;
    size_t sz = 0;
    char** pTable = NULL;
    mmUInt8_t* w = NULL;
    mmUInt8_t* buff = p->hStreambuf.buff;
    size_t pptr = p->hStreambuf.pptr;
    size_t gptr = p->hStreambuf.gptr;
    
    sz = sizeof(const char*) * p->hMaxRowNumber * p->hMaxColumnNumber;
    if (p->hTableSize < sz)
    {
        mmFree(p->pTable);
        p->pTable = NULL;
        
        pTable = (char**)mmMalloc(sz);
        p->pTable = pTable;
        p->hTableSize = sz;
    }
    else
    {
        pTable = p->pTable;
    }
    
    mmMemset(pTable, 0, sz);
    
    p->hColumnNumber = p->hColumnNumber > p->hMaxColumnNumber ? p->hColumnNumber : p->hMaxColumnNumber;

    i = gptr;

    while (i < pptr)
    {
        switch (buff[i])
        {
        case '\t':
        case '\r':
        {
            w = (buff + gptr + b);
            __static_ParseTSV_AnalysisCurrentWord(p, w, e - b, x, y);
            x++;
            i++;
            b = i;
            e = i;
        }
        break;
                
        case '\n':
        {
            // mmPrintf("\n");
            w = (buff + gptr + b);
            __static_ParseTSV_AnalysisCurrentWord(p, w, e - b, x, y);
            x = 0;
            y++;
            i++;
            b = i;
            e = i;
        }
        break;
                
        default:
        {
            i++;
            e++;
        }
        break;
        }
    }
    
    if (b != e)
    {
        w = (buff + gptr + b);
        __static_ParseTSV_AnalysisCurrentWord(p, w, e - b, x, y);
    }
    
    p->hRowNumber = p->hMaxRowNumber > y ? y : p->hMaxRowNumber;
}
