/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMath_h__
#define __mmMath_h__

#include "core/mmCore.h"

#include "math/mmMathConst.h"

#include "core/mmPrefix.h"

/**
 * @brief use for type conversion. we usually use float[n] float[m][n] for parameter.
 */
typedef float mmVec2[2];
typedef float mmVec3[3];
typedef float mmVec4[4];
typedef float mmQuat[4];
typedef float mmMat4x4[4][4];
typedef float mmMat3x3[3][3];

/**
 * @brief use for value reference.
 *    
 * float m[4][4];
 * mmMat4x4Ref r = m;
 *
 */
typedef float* mmVec2Ref;
typedef float* mmVec3Ref;
typedef float* mmVec4Ref;
typedef float* mmQuatRef;
typedef float(*mmMat4x4Ref)[4];
typedef float(*mmMat3x3Ref)[3];

/**
 * @brief use for const value reference.
 *    
 * const float m[4][4];
 * mmMat4x4ConstRef r = m;
 *
 */
typedef const float* mmVec2ConstRef;
typedef const float* mmVec3ConstRef;
typedef const float* mmVec4ConstRef;
typedef const float* mmQuatConstRef;
typedef const float(*mmMat4x4ConstRef)[4];
typedef const float(*mmMat3x3ConstRef)[3];

#define mmMax(val1, val2)  (((val1) < (val2)) ? (val2) : (val1))
#define mmMin(val1, val2)  (((val1) > (val2)) ? (val2) : (val1))
#define mmClamp(val, minVal, maxVal) mmMin(mmMax(val, minVal), maxVal)

/**
 * @brief get sign of 32 bit float as +1, -1, 0
 *
 * Important: It returns 0 for zero/NaN input
 *
 * @param val float value
 */
static 
mmInline
float
mmMathSign(
    float val) 
{
    return (float)((val > 0.0f) - (val < 0.0f));
}

/*!
 * @brief convert degree to radian
 *
 * @param[in] degree angle in degrees
 */
static
mmInline
float
mmMathRadian(
    float degree)
{
    return degree * (float)MM_PI / 180.0f;
}

/*!
 * @brief convert radian to degree
 *
 * @param[in] radian angle in radians
 */
static
mmInline
float
mmMathDegree(
    float radian)
{
    return radian * 180.0f / (float)MM_PI;
}

/*!
 * @brief multiplies given parameter with itself = x * x or powf(x, 2)
 *
 * @param[in] x x
 */
static
mmInline
float
mmMathPow2(
    float x) 
{
    return x * x;
}

/*!
 * @brief find minimum of given two values
 *
 * @param[in] a number 1
 * @param[in] b number 2
 */
static
mmInline
float
mmMathMin(
    float a, 
    float b)
{
    return (a < b) ? a : b;
}

/*!
 * @brief find maximum of given two values
 *
 * @param[in] a number 1
 * @param[in] b number 2
 */
static
mmInline
float
mmMathMax(
    float a, 
    float b)
{
    return (a > b) ? a : b;
}

/*!
 * @brief clamp a number between min and max
 *
 * @param[in] val    value to clamp
 * @param[in] minVal minimum value
 * @param[in] maxVal maximum value
 */
static
mmInline
float
mmMathClamp(
    float val, 
    float minVal, 
    float maxVal)
{
    return mmMathMin(mmMathMax(val, minVal), maxVal);
}

/*!
 * @brief linear interpolation between two numbers
 *
 * formula:  from + t * (to - from)
 *
 * @param[in]   from from value
 * @param[in]   to   to value
 * @param[in]   t    interpolant (amount)
 */
static
mmInline
float
mmMathLerp(
    float from, 
    float to, 
    float t)
{
    return from + t * (to - from);
}

/*!
 * @brief mix between two numbers
 *
 * formula:  x*(1-a) +y*a
 *
 * @param[in]   x  x value
 * @param[in]   y  y value
 * @param[in]   a  mix value
 */
static
mmInline
float
mmMathMix(
    float x,
    float y,
    float a)
{
    return x * (1.0f - a) + y * a;
}

/*!
 * @brief threshold function
 *
 * @param[in]   edge    threshold
 * @param[in]   x       value to test against threshold
 * @return      returns 0.0 if x < edge, else 1.0
 */
static
mmInline
float
mmMathStep(
    float edge, 
    float x)
{
    return (x < edge) ? 0.0f : 1.0f;
}

/*!
 * @brief smooth Hermite interpolation
 *
 * formula:  t^2 * (3-2t)
 *
 * @param[in]   t    interpolant (amount)
 */
static
mmInline
float
mmMathSmooth(
    float t)
{
    return t * t * (3.0f - 2.0f * t);
}

/*!
 * @brief threshold function with a smooth transition (according to OpenCL specs)
 *
 * formula:  t^2 * (3-2t)
 *
 * @param[in]   edge0 low threshold
 * @param[in]   edge1 high threshold
 * @param[in]   x     interpolant (amount)
 */
static
mmInline
float
mmMathSmoothstep(
    float edge0, 
    float edge1, 
    float x)
{
    float t;
    t = mmMathClamp((x - edge0) / (edge1 - edge0), 0.0f, 1.0f);
    return mmMathSmooth(t);
}

/*!
 * @brief check if two float equal with using EPSILON
 *
 * @param[in]   a   a
 * @param[in]   b   b
 */
static
mmInline
int
mmMathEquals(
    float a, 
    float b)
{
    return fabsf(a - b) <= FLT_EPSILON;
}

/*!
 * @brief percentage of current value between start and end value
 *
 * maybe fraction could be alternative name.
 *
 * @param[in]   from    from value
 * @param[in]   to      to value
 * @param[in]   current current value
 */
static
mmInline
float
mmMathPercent(
    float from, 
    float to, 
    float current)
{
    float t;
    if ((t = to - from) == 0.0f)
    {
        return 1.0f;
    }
    else
    {
        return (current - from) / t;
    }
}

/*!
 * @brief inverse sqrt
 *
 * formula:  1.0f / sqrtf(a)
 *
 * @param[in]   a    value
 */
static
mmInline
float
mmMathInvSqrt(
    float a)
{
    return 1.0f / sqrtf(a);
}

/*!
 * @brief mod between two numbers
 *
 * formula:  r = mod(a, n)
 *
 * @param[in]   a  x value
 * @param[in]   n  y value
 */
static
mmInline
float
mmMathMod(
    float a,
    float n)
{
    // fmodf(-1.12125642E-8, 1.0f) some standard library are negative
    // -1.12125642E-8 + 1.0f = 1.0f not less than 1.0f.
    return fmodf(a - n * floorf(a / n), n);
}

/*!
 * @brief cubic bezier interpolation
 *
 * Formula:
 *  B(s) = P0*(1-s)^3 + 3*C0*s*(1-s)^2 + 3*C1*s^2*(1-s) + P1*s^3
 *
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  c0   control point 1
 * @param[in]  c1   control point 2
 * @param[in]  p1   end point
 *
 * @return B(s)
 */
MM_EXPORT_DLL
float
mmMathBezier(
    float s, 
    float p0, 
    float c0, 
    float c1, 
    float p1);

/*!
 * @brief cubic hermite interpolation
 *
 * Formula:
 *  H(s) = P0*(2*s^3 - 3*s^2 + 1) + T0*(s^3 - 2*s^2 + s)
 *            + P1*(-2*s^3 + 3*s^2) + T1*(s^3 - s^2)
 *
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  t0   tangent 1
 * @param[in]  t1   tangent 2
 * @param[in]  p1   end point
 *
 * @return H(s)
 */
MM_EXPORT_DLL
float
mmMathHermite(
    float s, 
    float p0, 
    float t0, 
    float t1, 
    float p1);

#include "core/mmSuffix.h"

#endif//__mmMath_h__
