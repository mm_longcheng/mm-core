/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRange.h"

/*!
 * @brief Init a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL
void
mmRange_Init(
    struct mmRange* p)
{
    p->o = 0;
    p->l = 0;
}

/*!
 * @brief Destroy a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL
void
mmRange_Destroy(
    struct mmRange* p)
{
    p->o = 0;
    p->l = 0;
}

/*!
 * @brief Reset a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL
void
mmRange_Reset(
    struct mmRange* p)
{
    p->o = 0;
    p->l = 0;
}

/*!
 * @brief Zero a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL
void
mmRange_Zero(
    struct mmRange* p)
{
    p->o = 0;
    p->l = 0;
}

/*!
 * @brief Make a Range.
 *
 * @param[out] p   Range
 * @param[in]  o   offset
 * @param[in]  l   length
 */
MM_EXPORT_DLL
void
mmRange_Make(
    struct mmRange* p,
    size_t o, size_t l)
{
    p->o = o;
    p->l = l;
}

/*!
 * @brief Check if two range are equal.
 *
 * formula:  return p == q
 *
 * @param[in]  p   range p
 * @param[in]  q   range q
 * @return         p == b
 */
MM_EXPORT_DLL
int
mmRange_Equals(
    const struct mmRange* p,
    const struct mmRange* q)
{
    return p->o == q->o && p->l == q->l;
}

/*!
 * @brief Assign a Range.
 *
 * @param[out] p   Range
 * @param[in]  q   Range
 */
MM_EXPORT_DLL
void
mmRange_Assign(
    struct mmRange* p,
    const struct mmRange* q)
{
    p->o = q->o;
    p->l = q->l;
}

/*!
 * @brief Check if two range memery are equal.
 *
 * formula:  return memcmp(p, q)
 *
 * @param[in]  p   range p
 * @param[in]  q   range q
 * @return         memcmp(p, q)
 */
MM_EXPORT_DLL
intptr_t
mmRange_Compare(
    const struct mmRange* p,
    const struct mmRange* q)
{
    // [l, r)
    size_t lo = p->o;
    size_t ro = q->o;
    size_t lr = p->o + p->l;
    size_t rr = q->o + q->l;
    if (lo <= ro && rr <= lr)
    {
        return 0;
    }
    else if (lo >= rr)
    {
        return (lo == rr) ? +1 : (lo - rr);
    }
    else if (ro >= lr)
    {
        return (lr == ro) ? -1 : (lr - ro);
    }
    else
    {
        return lo - ro;
    }
}

MM_EXPORT_DLL const struct mmRange mmRangeZero = { 0 , 0, };
