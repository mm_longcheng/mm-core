/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRectangle.h"

/*!
 * @brief Init a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL
void
mmRectF_Init(
    struct mmRectF* p)
{
    mmVec2Make(p->min, 0, 0);
    mmVec2Make(p->max, 0, 0);
}

/*!
 * @brief Destroy a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL
void
mmRectF_Destroy(
    struct mmRectF* p)
{
    mmVec2Make(p->max, 0, 0);
    mmVec2Make(p->min, 0, 0);
}

/*!
 * @brief Reset a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL
void
mmRectF_Reset(
    struct mmRectF* p)
{
    mmVec2Make(p->min, 0, 0);
    mmVec2Make(p->max, 0, 0);
}

/*!
 * @brief Zero a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL
void
mmRectF_Zero(
    struct mmRectF* p)
{
    mmVec2Make(p->min, 0, 0);
    mmVec2Make(p->max, 0, 0);
}

/*!
 * @brief Make a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL
void
mmRectF_Make(
    struct mmRectF* p,
    float x1, float y1,
    float x2, float y2)
{
    mmVec2Make(p->min, x1, y1);
    mmVec2Make(p->max, x2, y2);
}

/*!
 * @brief Check if two Rect are equal.
 *
 * formula:  return p == q
 *
 * @param[in]  p   rect p
 * @param[in]  q   rect q
 * @return         p == q
 */
MM_EXPORT_DLL
int
mmRectF_Equals(
    const struct mmRectF* p,
    const struct mmRectF* q)
{
    return mmVec2Equals(p->min, q->min) && mmVec2Equals(p->max, q->max);
}

/*!
 * @brief Assign a Rect.
 *
 * @param[out] p   rect
 * @param[in]  q   rect
 */
MM_EXPORT_DLL
void
mmRectF_Assign(
    struct mmRectF* p,
    const struct mmRectF* q)
{
    mmVec2Assign(p->min, q->min);
    mmVec2Assign(p->max, q->max);
}

/*!
 * @brief Union tow Rect.
 *
 * @param[out] r   rect
 * @param[in]  p   rect
 * @param[in]  q   rect
 */
MM_EXPORT_DLL
void
mmRectF_Union(
    struct mmRectF* r,
    const struct mmRectF* p,
    const struct mmRectF* q)
{
    mmVec2Min(r->min, p->min, q->min);
    mmVec2Max(r->max, p->max, q->max);
}

/*!
 * @brief Intersection tow Rect.
 *
 * @param[out] r   rect
 * @param[in]  p   rect
 * @param[in]  q   rect
 */
MM_EXPORT_DLL
void
mmRectF_Intersection(
    struct mmRectF* r,
    const struct mmRectF* p,
    const struct mmRectF* q)
{
    mmVec2Max(r->min, p->min, q->min);
    mmVec2Min(r->max, p->max, q->max);
}

/*!
 * @brief Check if contains point.
 *
 * formula:  return p == q
 *
 * @param[in]  p     rect p
 * @param[in]  q     vec2 q
 * @return     value bool
 */
MM_EXPORT_DLL
int
mmRectF_ContainsPoint(
    struct mmRectF* p,
    const float q[2])
{
    return
        q[0] >= p->min[0] &&
        q[0] <= p->max[0] &&
        q[1] >= p->min[1] &&
        q[1] <= p->max[1];
}

/*!
 * @brief Check if two rect overlap.
 *
 * @param[in]  p     rect p
 * @param[in]  q     rect q
 * @return     value bool
 */
MM_EXPORT_DLL
int
mmRectF_Overlap(
    const struct mmRectF* p,
    const struct mmRectF* q)
{
    // The rectangles don't overlap if
    // one rectangle's minimum in some dimension
    // is greater than the other's maximum in
    // that dimension.
    //
    // not
    //    p->min.x > q->max.x ||
    //    q->min.x > p->max.x ||
    //    p->min.y > q->max.y ||
    //    q->min.y > p->max.y;
        
    return
        p->min[0] <= q->max[0] &&
        q->min[0] <= p->max[0] &&
        p->min[1] <= q->max[1] &&
        q->min[1] <= p->max[1];
}

