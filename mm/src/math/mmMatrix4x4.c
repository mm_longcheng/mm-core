/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMatrix4x4.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmQuaternion.h"

#include "core/mmAlloc.h"

/*!
 * @brief Init a Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4Init(
    float m[4][4])
{
    mmMemset(m, 0, sizeof(mmMat4x4Identity));
}

/*!
 * @brief Destroy a Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4Destroy(
    float m[4][4])
{
    mmMemset(m, 0, sizeof(mmMat4x4Identity));
}

/*!
 * @brief Make a Matrix4x4 from value.
 *
 * @param[out] m           mat4x4 result
 */
MM_EXPORT_DLL
void
mmMat4x4Make(
    float m[4][4],
    float m00, float m01, float m02, float m03,
    float m10, float m11, float m12, float m13,
    float m20, float m21, float m22, float m23,
    float m30, float m31, float m32, float m33)
{
    m[0][0] = m00; m[1][0] = m01; m[2][0] = m02; m[3][0] = m03;
    m[0][1] = m10; m[1][1] = m11; m[2][1] = m12; m[3][1] = m13;
    m[0][2] = m20; m[1][2] = m21; m[2][2] = m22; m[3][2] = m23;
    m[0][3] = m30; m[1][3] = m31; m[2][3] = m32; m[3][3] = m33;
}

/*!
 * @brief Make a identity Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4MakeIdentity(
    float m[4][4])
{
    m[0][0] = 1; m[1][0] = 0; m[2][0] = 0; m[3][0] = 0;
    m[0][1] = 0; m[1][1] = 1; m[2][1] = 0; m[3][1] = 0;
    m[0][2] = 0; m[1][2] = 0; m[2][2] = 1; m[3][2] = 0;
    m[0][3] = 0; m[1][3] = 0; m[2][3] = 0; m[3][3] = 1;
}

/*!
 * @brief Make a zero Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4MakeZero(
    float m[4][4])
{
    mmMemset(m, 0, sizeof(mmMat4x4Identity));
}

/*!
 * @brief Check if two matrices memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 * @return                 memcmp(a, b)
 */
MM_EXPORT_DLL
int
mmMat4x4Compare(
    const float a[4][4],
    const float b[4][4])
{
    return mmMemcmp(a, b, sizeof(mmMat4x4Identity));
}

/*!
 * @brief Check if two matrices are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 * @return                 a == b
 */
MM_EXPORT_DLL
int
mmMat4x4Equals(
    const float a[4][4],
    const float b[4][4])
{
    return (mmVec4Equals(a[0], b[0]) &&
            mmVec4Equals(a[1], b[1]) &&
            mmVec4Equals(a[2], b[2]) &&
            mmVec4Equals(a[3], b[3]));
}

/*!
 * @brief Assign a Matrix4x4.
 *
 * formula:  m = a
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4Assign(
    float m[4][4],
    const float a[4][4])
{
    mmMemcpy(m, a, sizeof(mmMat4x4Identity));
}

/*!
 * @brief Make a duplicate from a Matrix4x4.
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4Duplicate(
    float r[4][4],
    const float m[4][4])
{
    mmMemcpy(r, m, sizeof(mmMat4x4Identity));
}

/*!
 * @brief Get row i from a Matrix4x4.
 *
 * formula:  r = row(m, i)
 *
 * @param[out] r           vec4 result
 * @param[in]  m           mat4x4
 * @param[in]  i           row index
 */
MM_EXPORT_DLL
void
mmMat4x4Row(
    float r[4],
    const float m[4][4],
    int i)
{
    r[0] = m[0][i];
    r[1] = m[1][i];
    r[2] = m[2][i];
    r[3] = m[3][i];
}

/*!
 * @brief Get column i from a Matrix4x4.
 *
 * formula:  r = column(m, i)
 *
 * @param[out] r           vec4 result
 * @param[in]  m           mat4x4
 * @param[in]  i           column index
 */
MM_EXPORT_DLL
void
mmMat4x4Col(
    float r[4],
    const float m[4][4],
    int i)
{
    r[0] = m[i][0];
    r[1] = m[i][1];
    r[2] = m[i][2];
    r[3] = m[i][3];
}

/*!
 * @brief Transpose a Matrix4x4.
 *
 * formula:  r = Transpose(m)
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4Transpose(
    float r[4][4],
    const float m[4][4])
{
    // r can be m.
    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
    float m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

    r[0][0] = m00; r[1][0] = m01; r[2][0] = m02; r[3][0] = m03;
    r[0][1] = m10; r[1][1] = m11; r[2][1] = m12; r[3][1] = m13;
    r[0][2] = m20; r[1][2] = m21; r[2][2] = m22; r[3][2] = m23;
    r[0][3] = m30; r[1][3] = m31; r[2][3] = m32; r[3][3] = m33;
}

/*!
 * @brief Make a mat4x4 by matrix addition matrix.
 *
 * formula:  m = a + b
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 */
MM_EXPORT_DLL
void
mmMat4x4Add(
    float m[4][4],
    const float a[4][4],
    const float b[4][4])
{
    mmVec4Add(m[0], a[0], b[0]);
    mmVec4Add(m[1], a[1], b[1]);
    mmVec4Add(m[2], a[2], b[2]);
    mmVec4Add(m[3], a[3], b[3]);
}

/*!
 * @brief Make a mat4x4 by matrix subtraction matrix.
 *
 * formula:  m = a - b
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 */
MM_EXPORT_DLL
void
mmMat4x4Sub(
    float m[4][4],
    const float a[4][4],
    const float b[4][4])
{
    mmVec4Sub(m[0], a[0], b[0]);
    mmVec4Sub(m[1], a[1], b[1]);
    mmVec4Sub(m[2], a[2], b[2]);
    mmVec4Sub(m[3], a[3], b[3]);
}

/*!
 * @brief Scale each element of the matrix.
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  k           k
 */
MM_EXPORT_DLL
void
mmMat4x4Scale(
    float m[4][4],
    const float a[4][4],
    float k)
{
    mmVec4Scale(m[0], a[0], k);
    mmVec4Scale(m[1], a[1], k);
    mmVec4Scale(m[2], a[2], k);
    mmVec4Scale(m[3], a[3], k);
}

/*!
 * @brief Make a mat4x4 by scale (x, y, z).
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  x           x
 * @param[in]  y           y
 * @param[in]  z           z
 */
MM_EXPORT_DLL
void
mmMat4x4ScaleXYZ(
    float m[4][4],
    const float a[4][4],
    float x, float y, float z)
{
    mmVec4Scale(m[0], a[0], x);
    mmVec4Scale(m[1], a[1], y);
    mmVec4Scale(m[2], a[2], z);

    m[3][0] = a[3][0];
    m[3][1] = a[3][1];
    m[3][2] = a[3][2];
    m[3][3] = a[3][3];
}

/*!
 * @brief Make a mat4x4 by matrix multiply matrix.
 *
 * formula:  m = a * b
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 */
MM_EXPORT_DLL
void
mmMat4x4Mul(
    float m[4][4],
    const float a[4][4],
    const float b[4][4])
{
    float a00 = a[0][0], a01 = a[0][1], a02 = a[0][2], a03 = a[0][3];
    float a10 = a[1][0], a11 = a[1][1], a12 = a[1][2], a13 = a[1][3];
    float a20 = a[2][0], a21 = a[2][1], a22 = a[2][2], a23 = a[2][3];
    float a30 = a[3][0], a31 = a[3][1], a32 = a[3][2], a33 = a[3][3];

    float b00 = b[0][0], b01 = b[0][1], b02 = b[0][2], b03 = b[0][3];
    float b10 = b[1][0], b11 = b[1][1], b12 = b[1][2], b13 = b[1][3];
    float b20 = b[2][0], b21 = b[2][1], b22 = b[2][2], b23 = b[2][3];
    float b30 = b[3][0], b31 = b[3][1], b32 = b[3][2], b33 = b[3][3];

    m[0][0] = a00 * b00 + a10 * b01 + a20 * b02 + a30 * b03;
    m[0][1] = a01 * b00 + a11 * b01 + a21 * b02 + a31 * b03;
    m[0][2] = a02 * b00 + a12 * b01 + a22 * b02 + a32 * b03;
    m[0][3] = a03 * b00 + a13 * b01 + a23 * b02 + a33 * b03;
    m[1][0] = a00 * b10 + a10 * b11 + a20 * b12 + a30 * b13;
    m[1][1] = a01 * b10 + a11 * b11 + a21 * b12 + a31 * b13;
    m[1][2] = a02 * b10 + a12 * b11 + a22 * b12 + a32 * b13;
    m[1][3] = a03 * b10 + a13 * b11 + a23 * b12 + a33 * b13;
    m[2][0] = a00 * b20 + a10 * b21 + a20 * b22 + a30 * b23;
    m[2][1] = a01 * b20 + a11 * b21 + a21 * b22 + a31 * b23;
    m[2][2] = a02 * b20 + a12 * b21 + a22 * b22 + a32 * b23;
    m[2][3] = a03 * b20 + a13 * b21 + a23 * b22 + a33 * b23;
    m[3][0] = a00 * b30 + a10 * b31 + a20 * b32 + a30 * b33;
    m[3][1] = a01 * b30 + a11 * b31 + a21 * b32 + a31 * b33;
    m[3][2] = a02 * b30 + a12 * b31 + a22 * b32 + a32 * b33;
    m[3][3] = a03 * b30 + a13 * b31 + a23 * b32 + a33 * b33;
}

/*!
 * @brief Make a vec4 by vec4 right mul mat4x4.
 *
 * formula:  r = m * v
 *
 * @param[out] r           vec4 result
 * @param[in]  m           mat4x4
 * @param[in]  v           vec4 value.
 */
MM_EXPORT_DLL
void
mmMat4x4MulVec4(
    float r[4],
    const float m[4][4],
    const float v[4])
{
    r[0] = m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2] + m[3][0] * v[3];
    r[1] = m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2] + m[3][1] * v[3];
    r[2] = m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2] + m[3][2] * v[3];
    r[3] = m[0][3] * v[0] + m[1][3] * v[1] + m[2][3] * v[2] + m[3][3] * v[3];
}

/*!
 * @brief Make a vec3 by vec3 right mul mat4x4.
 *
 * formula:  r = m * v
 *
 * @param[out] r           vec3 result
 * @param[in]  m           mat4x4
 * @param[in]  v           vec3 value.
 */
MM_EXPORT_DLL
void
mmMat4x4MulVec3(
    float r[3],
    const float m[4][4],
    const float v[3])
{
    // r = (m * vec4(v, 1)).xyz
    r[0] = m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2] + m[3][0];
    r[1] = m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2] + m[3][1];
    r[2] = m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2] + m[3][2];
}

/*!
 * @brief Translate a Matrix4x4.
 *
 * @param[out] m           mat4x4 result
 * @param[in]  x           x
 * @param[in]  y           y
 * @param[in]  z           x
 */
MM_EXPORT_DLL
void
mmMat4x4Translate(
    float m[4][4],
    float x, float y, float z)
{
    mmMat4x4MakeIdentity(m);
    m[3][0] = x;
    m[3][1] = y;
    m[3][2] = z;
}

/*!
 * @brief Rotate a Matrix4x4 by v(x, y, z) axis.
 *
 * formula:  r = Rotate(m, angle, v)
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  v           vec3 v
 * @param[in]  angle       angle by (x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4Rotate(
    float r[4][4],
    const float m[4][4],
    float angle,
    const float v[3])
{
    // https://github.com/g-truc/glm
    // glm::rotate api

    const float a = angle;
    const float c = cosf(a);
    const float s = sinf(a);
    float axis[3];
    float t[3];
    float rotate[3][3];
    float v0[4], v1[4], v2[4];

    mmVec3Normalize(axis, v);
    mmVec3MulK(t, axis, 1.0f - c);

    rotate[0][0] = t[0] * axis[0] + c;
    rotate[0][1] = t[0] * axis[1] + s * axis[2];
    rotate[0][2] = t[0] * axis[2] - s * axis[1];

    rotate[1][0] = t[1] * axis[0] - s * axis[2];
    rotate[1][1] = t[1] * axis[1] + c;
    rotate[1][2] = t[1] * axis[2] + s * axis[0];

    rotate[2][0] = t[2] * axis[0] + s * axis[1];
    rotate[2][1] = t[2] * axis[1] - s * axis[0];
    rotate[2][2] = t[2] * axis[2] + c;

    mmVec4MulK(v0, m[0], rotate[0][0]);
    mmVec4MulK(v1, m[1], rotate[0][1]);
    mmVec4MulK(v2, m[2], rotate[0][2]);
    mmVec4Add(r[0], v0, v1); mmVec4Add(r[0], r[0], v2);

    mmVec4MulK(v0, m[0], rotate[1][0]);
    mmVec4MulK(v1, m[1], rotate[1][1]);
    mmVec4MulK(v2, m[2], rotate[1][2]);
    mmVec4Add(r[1], v0, v1); mmVec4Add(r[1], r[1], v2);

    mmVec4MulK(v0, m[0], rotate[2][0]);
    mmVec4MulK(v1, m[1], rotate[2][1]);
    mmVec4MulK(v2, m[2], rotate[2][2]);
    mmVec4Add(r[2], v0, v1); mmVec4Add(r[2], r[2], v2);

    mmVec4Assign(r[3], m[3]);
}

/*!
 * @brief Rotate a Matrix4x4 by x-axis.
 *
 * formula:  q = RotateX(m, angle)
 *
 * @param[out] q           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  angle       angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4RotateX(
    float q[4][4],
    const float m[4][4],
    float angle)
{
    float R[4][4];
    mmMat4x4FromEulerAngleX(R, angle);
    mmMat4x4Mul(q, m, R);
}

/*!
 * @brief Rotate a Matrix4x4 by y-axis.
 *
 * formula:  q = RotateY(m, angle)
 *
 * @param[out] q           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  angle       angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4RotateY(
    float q[4][4],
    const float m[4][4],
    float angle)
{
    float R[4][4];
    mmMat4x4FromEulerAngleY(R, angle);
    mmMat4x4Mul(q, m, R);
}

/*!
 * @brief Rotate a Matrix4x4 by z-axis.
 *
 * formula:  q = RotateZ(m, angle)
 *
 * @param[out] q           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  angle       angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4RotateZ(
    float q[4][4],
    const float m[4][4],
    float angle)
{
    float R[4][4];
    mmMat4x4FromEulerAngleZ(R, angle);
    mmMat4x4Mul(q, m, R);
}

/*!
 * @brief Inverse a Matrix4x4.
 *
 * formula:  r = Inverse(m)
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL
void
mmMat4x4Inverse(
    float r[4][4],
    const float m[4][4])
{
    // linmath.h mat4x4_invert
    // r can be m.
    float idet;
    float s[6];
    float c[6];

    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
    float m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

    s[0] = m00 * m11 - m10 * m01;
    s[1] = m00 * m12 - m10 * m02;
    s[2] = m00 * m13 - m10 * m03;
    s[3] = m01 * m12 - m11 * m02;
    s[4] = m01 * m13 - m11 * m03;
    s[5] = m02 * m13 - m12 * m03;

    c[0] = m20 * m31 - m30 * m21;
    c[1] = m20 * m32 - m30 * m22;
    c[2] = m20 * m33 - m30 * m23;
    c[3] = m21 * m32 - m31 * m22;
    c[4] = m21 * m33 - m31 * m23;
    c[5] = m22 * m33 - m32 * m23;

    /* Assumes it is invertible */
    idet = 1.0f / (s[0] * c[5] - s[1] * c[4] + s[2] * c[3] + s[3] * c[2] - s[4] * c[1] + s[5] * c[0]);

    r[0][0] = (+m11 * c[5] - m12 * c[4] + m13 * c[3]) * idet;
    r[0][1] = (-m01 * c[5] + m02 * c[4] - m03 * c[3]) * idet;
    r[0][2] = (+m31 * s[5] - m32 * s[4] + m33 * s[3]) * idet;
    r[0][3] = (-m21 * s[5] + m22 * s[4] - m23 * s[3]) * idet;

    r[1][0] = (-m10 * c[5] + m12 * c[2] - m13 * c[1]) * idet;
    r[1][1] = (+m00 * c[5] - m02 * c[2] + m03 * c[1]) * idet;
    r[1][2] = (-m30 * s[5] + m32 * s[2] - m33 * s[1]) * idet;
    r[1][3] = (+m20 * s[5] - m22 * s[2] + m23 * s[1]) * idet;

    r[2][0] = (+m10 * c[4] - m11 * c[2] + m13 * c[0]) * idet;
    r[2][1] = (-m00 * c[4] + m01 * c[2] - m03 * c[0]) * idet;
    r[2][2] = (+m30 * s[4] - m31 * s[2] + m33 * s[0]) * idet;
    r[2][3] = (-m20 * s[4] + m21 * s[2] - m23 * s[0]) * idet;

    r[3][0] = (-m10 * c[3] + m11 * c[1] - m12 * c[0]) * idet;
    r[3][1] = (+m00 * c[3] - m01 * c[1] + m02 * c[0]) * idet;
    r[3][2] = (-m30 * s[3] + m31 * s[1] - m32 * s[0]) * idet;
    r[3][3] = (+m20 * s[3] - m21 * s[1] + m22 * s[0]) * idet;
}

/*!
 * @brief Make Matrix4x4 form translate.
 *
 * formula:  r = FromTranslate(t)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t           translate(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4FromTranslate(
    float m[4][4],
    const float t[3])
{
    m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
    m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
    m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
    m[3][0] = t[0]; m[3][1] = t[1]; m[3][2] = t[2]; m[3][3] = 1.0f;
}

/*!
 * @brief Make Matrix4x4 form quaternion.
 *
 * formula:  m = FromQuat(q)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL
void
mmMat4x4FromQuat(
    float m[4][4],
    const float q[4])
{
    // https://github.com/g-truc/glm
    // quaternion::mat4_cast api
    float x = q[0];
    float y = q[1];
    float z = q[2];
    float w = q[3];

    float qxx = x * x;
    float qyy = y * y;
    float qzz = z * z;
    float qxz = x * z;
    float qxy = x * y;
    float qyz = y * z;
    float qwx = w * x;
    float qwy = w * y;
    float qwz = w * z;

    m[0][0] = 1.0f - 2.0f * (qyy + qzz);
    m[0][1] =        2.0f * (qxy + qwz);
    m[0][2] =        2.0f * (qxz - qwy);
    m[0][3] = 0.0f;

    m[1][0] =        2.0f * (qxy - qwz);
    m[1][1] = 1.0f - 2.0f * (qxx + qzz);
    m[1][2] =        2.0f * (qyz + qwx);
    m[1][3] = 0.0f;

    m[2][0] =        2.0f * (qxz + qwy);
    m[2][1] =        2.0f * (qyz - qwx);
    m[2][2] = 1.0f - 2.0f * (qxx + qyy);
    m[2][3] = 0.0f;

    m[3][0] = m[3][1] = m[3][2] = 0.0f;
    m[3][3] = 1.0f;
}

/*!
 * @brief Make Matrix4x4 form scale.
 *
 * formula:  m = FromScale(s)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4FromScale(
    float m[4][4],
    const float s[3])
{
    m[0][0] = s[0]; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
    m[1][0] = 0.0f; m[1][1] = s[1]; m[1][2] = 0.0f; m[1][3] = 0.0f;
    m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = s[2]; m[2][3] = 0.0f;
    m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
}

/*!
 * @brief Make Matrix4x4 multiply form translate.
 *
 * formula:  r = m * FromTranslate(t)
 *
 * @param[out] r           result
 * @param[in]  m           mat4x4
 * @param[in]  t           translate(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4MulTranslate(
    float r[4][4],
    const float m[4][4],
    const float t[3])
{
    // r = m * FromTranslate(t)
    mmVec4Assign(r[0], m[0]);
    mmVec4Assign(r[1], m[1]);
    mmVec4Assign(r[2], m[2]);

    r[3][0] += m[0][0] * t[0] + m[1][0] * t[1] + m[2][0] * t[2];
    r[3][1] += m[0][1] * t[0] + m[1][1] * t[1] + m[2][1] * t[2];
    r[3][2] += m[0][2] * t[0] + m[1][2] * t[1] + m[2][2] * t[2];
    r[3][3] += m[0][3] * t[0] + m[1][3] * t[1] + m[2][3] * t[2];
}

/*!
 * @brief Make Matrix4x4 multiply form quaternion.
 *
 * formula:  r = m * FromQuat(q)
 *
 * @param[out] r           result
 * @param[in]  m           mat4x4
 * @param[in]  q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL
void
mmMat4x4MulQuat(
    float r[4][4],
    const float m[4][4],
    const float q[4])
{
    // r = m * FromQuat(q)
    float a00 = m[0][0], a01 = m[0][1], a02 = m[0][2], a03 = m[0][3];
    float a10 = m[1][0], a11 = m[1][1], a12 = m[1][2], a13 = m[1][3];
    float a20 = m[2][0], a21 = m[2][1], a22 = m[2][2], a23 = m[2][3];
    float a30 = m[3][0], a31 = m[3][1], a32 = m[3][2], a33 = m[3][3];

    float x = q[0];
    float y = q[1];
    float z = q[2];
    float w = q[3];

    float qxx = x * x;
    float qyy = y * y;
    float qzz = z * z;
    float qxz = x * z;
    float qxy = x * y;
    float qyz = y * z;
    float qwx = w * x;
    float qwy = w * y;
    float qwz = w * z;

    float b00 = 1.0f - 2.0f * (qyy + qzz);
    float b01 = 2.0f        * (qxy + qwz);
    float b02 = 2.0f        * (qxz - qwy);

    float b10 = 2.0f        * (qxy - qwz);
    float b11 = 1.0f - 2.0f * (qxx + qzz);
    float b12 = 2.0f        * (qyz + qwx);

    float b20 = 2.0f        * (qxz + qwy);
    float b21 = 2.0f        * (qyz - qwx);
    float b22 = 1.0f - 2.0f * (qxx + qyy);

    r[0][0] = a00 * b00 + a10 * b01 + a20 * b02;
    r[0][1] = a01 * b00 + a11 * b01 + a21 * b02;
    r[0][2] = a02 * b00 + a12 * b01 + a22 * b02;
    r[0][3] = a03 * b00 + a13 * b01 + a23 * b02;
    r[1][0] = a00 * b10 + a10 * b11 + a20 * b12;
    r[1][1] = a01 * b10 + a11 * b11 + a21 * b12;
    r[1][2] = a02 * b10 + a12 * b11 + a22 * b12;
    r[1][3] = a03 * b10 + a13 * b11 + a23 * b12;
    r[2][0] = a00 * b20 + a10 * b21 + a20 * b22;
    r[2][1] = a01 * b20 + a11 * b21 + a21 * b22;
    r[2][2] = a02 * b20 + a12 * b21 + a22 * b22;
    r[2][3] = a03 * b20 + a13 * b21 + a23 * b22;
    r[3][0] = a30;
    r[3][1] = a31;
    r[3][2] = a32;
    r[3][3] = a33;
}

/*!
 * @brief Make Matrix4x4 multiply form scale.
 *
 * formula:  r = m * FromScale(s)
 *
 * @param[out] r           result
 * @param[in]  m           mat4x4
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4MulScale(
    float r[4][4],
    const float m[4][4],
    const float s[3])
{
    // r = m * FromScale(s)
    mmVec4Scale(r[0], m[0], s[0]);
    mmVec4Scale(r[1], m[1], s[1]);
    mmVec4Scale(r[2], m[2], s[2]);

    r[3][0] = m[3][0];
    r[3][1] = m[3][1];
    r[3][2] = m[3][2];
    r[3][3] = m[3][3];
}

/*!
 * @brief Make Matrix4x4 form translate quaternion scale.
 *
 * formula:  m = m = FromTranslate(t) * FromQuat(q) * FromScale(s)
 *
 * @param[out] m           mat4x4
 * @param[in]  t           translate(x, y, z)
 * @param[in]  q           quaternion(x, y, z, w)
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4MakeTransform(
    float m[4][4],
    const float t[3],
    const float q[4],
    const float s[3])
{
    // m = FromTranslate(t) * FromQuat(q) * FromScale(s)

    // Ordering:
    //    1. scale      s
    //    2. rotate     q
    //    3. translate  t

    float tx = t[0];
    float ty = t[1];
    float tz = t[2];

    float qx = q[0];
    float qy = q[1];
    float qz = q[2];
    float qw = q[3];

    float sx = s[0];
    float sy = s[1];
    float sz = s[2];

    m[0][0] = (1.0f - 2.0f * (qy * qy + qz * qz)) * sx;
    m[0][1] = (       2.0f * (qx * qy + qz * qw)) * sx;
    m[0][2] = (       2.0f * (qx * qz - qy * qw)) * sx;
    m[0][3] = (0.0f);

    m[1][0] = (       2.0f * (qx * qy - qz * qw)) * sy;
    m[1][1] = (1.0f - 2.0f * (qx * qx + qz * qz)) * sy;
    m[1][2] = (       2.0f * (qy * qz + qx * qw)) * sy;
    m[1][3] = (0.0f);

    m[2][0] = (       2.0f * (qx * qz + qy * qw)) * sz;
    m[2][1] = (       2.0f * (qy * qz - qx * qw)) * sz;
    m[2][2] = (1.0f - 2.0f * (qx * qx + qy * qy)) * sz;
    m[2][3] = (0.0f);

    m[3][0] = tx;
    m[3][1] = ty;
    m[3][2] = tz;
    m[3][3] = 1.0f;
}

/*!
 * @brief determinant value for a Matrix4x4.
 *
 * @param[in]  m           mat4x4
 * @return                 determinant value
 */
MM_EXPORT_DLL
float
mmMat4x4Determinant(
    const float m[4][4])
{
    // https://github.com/recp/cglm
    /* [square] det(A) = det(At) */
    float t[6];
    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
    float m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

    t[0] = m22 * m33 - m32 * m23;
    t[1] = m21 * m33 - m31 * m23;
    t[2] = m21 * m32 - m31 * m22;
    t[3] = m20 * m33 - m30 * m23;
    t[4] = m20 * m32 - m30 * m22;
    t[5] = m20 * m31 - m30 * m21;

    return
        + m00 * (m11 * t[0] - m12 * t[1] + m13 * t[2])
        - m01 * (m10 * t[0] - m12 * t[3] + m13 * t[4])
        + m02 * (m10 * t[1] - m11 * t[3] + m13 * t[5])
        - m03 * (m10 * t[2] - m11 * t[4] + m12 * t[5]);
}

/*!
 * @brief Check if the matrix is affine.
 *
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL
int
mmMat4x4IsAffine(
    const float m[4][4])
{
    return m[0][3] == 0 && m[1][3] == 0 && m[2][3] == 0 && m[3][3] == 1;
}

/*!
 * @brief Matrix4x4 QDU decomposition.
 *
 * @param[in]  m           mat4x4
 * @param[out] kQ          Q
 * @param[out] kD          D
 * @param[out] kU          U
 */
MM_EXPORT_DLL
void
mmMat4x4QDUDecomposition(
    const float m[4][4],
    float kQ[4][4],
    float kD[3],
    float kU[3])
{
    // https://github.com/OGRECave/ogre-next Matrix3 QDUDecomposition
    // 
    // Factor M = QR = QDU where Q is orthogonal, D is diagonal,
    // and U is upper triangular with ones on its diagonal.  Algorithm uses
    // Gram-Schmidt orthogonalization (the QR algorithm).
    //
    // If M = [ m0 | m1 | m2 ] and Q = [ q0 | q1 | q2 ], then
    //
    //   q0 = m0/|m0|
    //   q1 = (m1-(q0*m1)q0)/|m1-(q0*m1)q0|
    //   q2 = (m2-(q0*m2)q0-(q1*m2)q1)/|m2-(q0*m2)q0-(q1*m2)q1|
    //
    // where |V| indicates length of vector V and A*B indicates dot
    // product of vectors A and B.  The matrix R has entries
    //
    //   r00 = q0*m0  r01 = q0*m1  r02 = q0*m2
    //   r10 = 0      r11 = q1*m1  r12 = q1*m2
    //   r20 = 0      r21 = 0      r22 = q2*m2
    //
    // so D = diag(r00,r11,r22) and U has entries u01 = r01/r00,
    // u02 = r02/r00, and u12 = r12/r11.

    // Q = rotation
    // D = scaling
    // U = shear

    // D stores the three diagonal entries r00, r11, r22
    // U stores the entries U[0] = u01, U[1] = u02, U[2] = u12

    // build orthogonal matrix Q
    float fInvLength;
    float fDot;
    float fDet;
    float fInvD0;
    float kR[3][3];

    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

    fInvLength = mmMathInvSqrt(m00 * m00 + m01 * m01 + m02 * m02);

    kQ[0][0] = m00 * fInvLength;
    kQ[0][1] = m01 * fInvLength;
    kQ[0][2] = m02 * fInvLength;

    fDot = kQ[0][0] * m10 + kQ[0][1] * m11 + kQ[0][2] * m12;
    kQ[1][0] = m10 - fDot * kQ[0][0];
    kQ[1][1] = m11 - fDot * kQ[0][1];
    kQ[1][2] = m12 - fDot * kQ[0][2];
    fInvLength = mmMathInvSqrt(kQ[1][0] * kQ[1][0] + kQ[1][1] * kQ[1][1] + kQ[1][2] * kQ[1][2]);

    kQ[1][0] *= fInvLength;
    kQ[1][1] *= fInvLength;
    kQ[1][2] *= fInvLength;

    fDot = kQ[0][0] * m20 + kQ[0][1] * m21 + kQ[0][2] * m22;
    kQ[2][0] = m20 - fDot * kQ[0][0];
    kQ[2][1] = m21 - fDot * kQ[0][1];
    kQ[2][2] = m22 - fDot * kQ[0][2];
    fDot = kQ[1][0] * m20 + kQ[1][1] * m21 + kQ[1][2] * m22;
    kQ[2][0] -= fDot * kQ[1][0];
    kQ[2][1] -= fDot * kQ[1][1];
    kQ[2][2] -= fDot * kQ[1][2];
    fInvLength = mmMathInvSqrt(kQ[2][0] * kQ[2][0] + kQ[2][1] * kQ[2][1] + kQ[2][2] * kQ[2][2]);

    kQ[2][0] *= fInvLength;
    kQ[2][1] *= fInvLength;
    kQ[2][2] *= fInvLength;

    // guarantee that orthogonal matrix has determinant 1 (no reflections)
    fDet = 0.0f;
    fDet += kQ[0][0] * kQ[1][1] * kQ[2][2] + kQ[1][0] * kQ[2][1] * kQ[0][2];
    fDet += kQ[2][0] * kQ[0][1] * kQ[1][2] - kQ[2][0] * kQ[1][1] * kQ[0][2];
    fDet -= kQ[1][0] * kQ[0][1] * kQ[2][2] - kQ[0][0] * kQ[2][1] * kQ[1][2];

    if (fDet < 0.0f)
    {
        mmVec3Scale(kQ[0], kQ[0], -1);
        mmVec3Scale(kQ[1], kQ[1], -1);
        mmVec3Scale(kQ[2], kQ[2], -1);
    }

    // build "right" matrix R
    kR[0][0] = kQ[0][0] * m00 + kQ[0][1] * m01 + kQ[0][2] * m02;
    kR[1][0] = kQ[0][0] * m10 + kQ[0][1] * m11 + kQ[0][2] * m12;
    kR[1][1] = kQ[1][0] * m10 + kQ[1][1] * m11 + kQ[1][2] * m12;
    kR[2][0] = kQ[0][0] * m20 + kQ[0][1] * m21 + kQ[0][2] * m22;
    kR[2][1] = kQ[1][0] * m20 + kQ[1][1] * m21 + kQ[1][2] * m22;
    kR[2][2] = kQ[2][0] * m20 + kQ[2][1] * m21 + kQ[2][2] * m22;

    // the scaling component
    kD[0] = kR[0][0];
    kD[1] = kR[1][1];
    kD[2] = kR[2][2];

    // the shear component
    fInvD0 = 1.0f / kD[0];
    kU[0] = kR[1][0] * fInvD0;
    kU[1] = kR[2][0] * fInvD0;
    kU[2] = kR[2][1] / kD[1];
}

/*!
 * @brief Matrix4x4 decomposition. is simplify decompose.
 *
 * @param[in]  m           mat4x4
 * @param[out] s           scale(x, y, z)
 * @param[out] q           quaternion(x, y, z, w)
 * @param[out] t           translate(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4Decomposition(
    const float m[4][4],
    float s[3],
    float q[4],
    float t[3])
{
    float skew[3];
    float perspective[4];
    mmMat4x4Decompose(m, s, q, t, skew, perspective);
}

/*!
 * @brief Matrix4x4 decompose.
 *
 * @param[in]  mat         mat4x4
 * @param[out] scale       scale(x, y, z)
 * @param[out] quaternion  quaternion(x, y, z, w)
 * @param[out] translate   translate(x, y, z)
 * @param[out] skew        skew(skewXY, skewXZ, skewYZ)
 * @param[out] perspective perspective(x, y, z, w)
 */
MM_EXPORT_DLL 
int 
mmMat4x4Decompose(
    const float mat[4][4], 
    float scale[3], 
    float quaternion[4], 
    float translate[3], 
    float skew[3], 
    float perspective[4])
{
    // http://www.opensource.apple.com/source/WebCore/WebCore-514/platform/graphics/transforms/TransformationMatrix.cpp
    // https://github.com/g-truc/glm/blob/master/glm/gtx/matrix_decompose.inl

    int i, j, k;
    float root, trace;
    float localMatrix[4][4];
    float perspectiveMatrix[4][4];

    // Vector4 type and functions need to be added to the common set.
    float row[3][3];
    float pdum3[3];

    // Normalize the matrix.
    if (mat[3][3] == 0)
    {
        return MM_FALSE;
    }

    mmMat4x4Assign(localMatrix, mat);

    if (1 != localMatrix[3][3])
    {
        float m33 = localMatrix[3][3];
        mmVec4DivK(localMatrix[0], localMatrix[0], m33);
        mmVec4DivK(localMatrix[1], localMatrix[1], m33);
        mmVec4DivK(localMatrix[2], localMatrix[2], m33);
        mmVec4DivK(localMatrix[3], localMatrix[3], m33);
    }

    // perspectiveMatrix is used to solve for perspective, but it also provides
    // an easy way to test for singularity of the upper 3x3 component.
    mmMat4x4Assign(perspectiveMatrix, localMatrix);
    perspectiveMatrix[0][3] = 0;
    perspectiveMatrix[1][3] = 0;
    perspectiveMatrix[2][3] = 0;
    perspectiveMatrix[3][3] = 1;

    if (mmMat4x4Determinant(perspectiveMatrix) == 0)
    {
        return MM_FALSE;
    }

    // First, isolate perspective.  This is the messiest.
    if (localMatrix[0][3] != 0 || localMatrix[1][3] != 0 || localMatrix[2][3] != 0)
    {
        float rightHandSide[4];
        float inversePerspectiveMatrix[4][4];
        float transposedInversePerspectiveMatrix[4][4];

        // rightHandSide is the right hand side of the equation.
        rightHandSide[0] = localMatrix[0][3];
        rightHandSide[1] = localMatrix[1][3];
        rightHandSide[2] = localMatrix[2][3];
        rightHandSide[3] = localMatrix[3][3];

        // Solve the equation by inverting perspectiveMatrix and multiplying
        // rightHandSide by the inverse.  (This is the easiest way, not
        // necessarily the best.)
        mmMat4x4Inverse(inversePerspectiveMatrix, perspectiveMatrix);
        mmMat4x4Transpose(transposedInversePerspectiveMatrix, inversePerspectiveMatrix);

        // perspective point.
        mmMat4x4MulVec4(perspective, transposedInversePerspectiveMatrix, rightHandSide);

        // Clear the perspective partition
        localMatrix[0][3] = localMatrix[1][3] = localMatrix[2][3] = 0;
        localMatrix[3][3] = 1;
    }
    else
    {
        // No perspective.
        perspective[0] = 0;
        perspective[1] = 0;
        perspective[2] = 0;
        perspective[3] = 1;
    }

    // Next take care of translation (easy).
    translate[0] = localMatrix[3][0];
    localMatrix[3][0] = 0;
    translate[1] = localMatrix[3][1];
    localMatrix[3][1] = 0;
    translate[2] = localMatrix[3][2];
    localMatrix[3][2] = 0;

    // Now get scale and shear.
    mmVec3Assign(row[0], localMatrix[0]);
    mmVec3Assign(row[1], localMatrix[1]);
    mmVec3Assign(row[2], localMatrix[2]);

    // Compute X scale factor and normalize first row.
    scale[0] = mmVec3Length(row[0]);
    mmVec3ScaleToLength(row[0], 1.0);

    // Compute XY shear factor and make 2nd row orthogonal to 1st.
    skew[0] = mmVec3DotProduct(row[0], row[1]);
    mmVec3LinearCombine(row[1], row[1], row[0], 1.0, -skew[0]);

    // Now, compute Y scale and normalize 2nd row.
    scale[1] = mmVec3Length(row[1]);
    mmVec3ScaleToLength(row[1], 1.0);
    skew[0] /= scale[1];

    // Compute XZ and YZ shears, orthogonalize 3rd row.
    skew[1] = mmVec3DotProduct(row[0], row[2]);
    mmVec3LinearCombine(row[2], row[2], row[0], 1.0, -skew[1]);
    skew[2] = mmVec3DotProduct(row[1], row[2]);
    mmVec3LinearCombine(row[2], row[2], row[1], 1.0, -skew[2]);

    // Next, get Z scale and normalize 3rd row.
    scale[2] = mmVec3Length(row[2]);
    mmVec3ScaleToLength(row[2], 1.0);
    skew[1] /= scale[2];
    skew[2] /= scale[2];

    // At this point, the matrix (in rows[]) is orthonormal.
    // Check for a coordinate system flip.  If the determinant
    // is -1, then negate the matrix and the scaling factors.
    // v3Cross(row[1], row[2], pdum3);
    mmVec3CrossProduct(pdum3, row[1], row[2]);
    if (mmVec3DotProduct(row[0], pdum3) < 0)
    {
        for (i = 0; i < 3; i++)
        {
            scale[0] *= -1;
            row[i][0] *= -1;
            row[i][1] *= -1;
            row[i][2] *= -1;
        }
    }

    // Now, get the rotations out, as described in the gem.

    // FIXME - Add the ability to return either quaternions (which are
    // easier to recompose with) or Euler angles (rx, ry, rz), which
    // are easier for authors to deal with. The latter will only be useful
    // when we fix https://bugs.webkit.org/show_bug.cgi?id=23799, so I
    // will leave the Euler angle code here for now.

    // ret.rotateY = asin(-row[0][2]);
    // if (cos(ret.rotateY) != 0)
    // {
    //     ret.rotateX = atan2(row[1][2], row[2][2]);
    //     ret.rotateZ = atan2(row[0][1], row[0][0]);
    // }
    // else
    // {
    //     ret.rotateX = atan2(-row[2][0], row[1][1]);
    //     ret.rotateZ = 0;
    // }

    k = 0;
    trace = row[0][0] + row[1][1] + row[2][2];
    if (trace > 0)
    {
        root = sqrtf(trace + 1.0f);
        // (x, y, z, w)
        quaternion[3] = 0.5f * root;
        root = 0.5f / root;
        quaternion[0] = root * (row[1][2] - row[2][1]);
        quaternion[1] = root * (row[2][0] - row[0][2]);
        quaternion[2] = root * (row[0][1] - row[1][0]);
    }
    else
    {
        static const int Next[3] = { 1, 2, 0 };
        i = 0;
        if (row[1][1] > row[0][0]) i = 1;
        if (row[2][2] > row[i][i]) i = 2;
        j = Next[i];
        k = Next[j];

        root = sqrtf(row[i][i] - row[j][j] - row[k][k] + 1.0f);
        // (x, y, z, w)
        quaternion[i] = 0.5f * root;
        root = 0.5f / root;
        quaternion[j] = root * (row[i][j] + row[j][i]);
        quaternion[k] = root * (row[i][k] + row[k][i]);
        quaternion[3] = root * (row[j][k] - row[k][j]);
    }

    return MM_TRUE;
}

/*!
 * @brief Make a Matrix4x4 by x-axis.
 *
 * formula:  m = FromEulerAngleX(angle)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  angle       angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleX(
    float m[4][4],
    float angle)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleX api
    float s = sinf(angle);
    float c = cosf(angle);

    m[0][0] = 1.f; m[0][1] = 0.f; m[0][2] = 0.f; m[0][3] = 0.f;
    m[1][0] = 0.f; m[1][1] =   c; m[1][2] =   s; m[1][3] = 0.f;
    m[2][0] = 0.f; m[2][1] =  -s; m[2][2] =   c; m[2][3] = 0.f;
    m[3][0] = 0.f; m[3][1] = 0.f; m[3][2] = 0.f; m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by y-axis.
 *
 * formula:  m = FromEulerAngleY(angle)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  angle       angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleY(
    float m[4][4],
    float angle)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleY api
    float s = sinf(angle);
    float c = cosf(angle);

    m[0][0] =   c; m[0][1] = 0.f; m[0][2] =  -s; m[0][3] = 0.f;
    m[1][0] = 0.f; m[1][1] = 1.f; m[1][2] = 0.f; m[1][3] = 0.f;
    m[2][0] =   s; m[2][1] = 0.f; m[2][2] =   c; m[2][3] = 0.f;
    m[3][0] = 0.f; m[3][1] = 0.f; m[3][2] = 0.f; m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by z-axis.
 *
 * formula:  m = FromEulerAngleZ(angle)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  angle       angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleZ(
    float m[4][4],
    float angle)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleZ api
    float s = sinf(angle);
    float c = cosf(angle);

    m[0][0] =   c; m[0][1] =   s; m[0][2] = 0.f; m[0][3] = 0.f;
    m[1][0] =  -s; m[1][1] =   c; m[1][2] = 0.f; m[1][3] = 0.f;
    m[2][0] = 0.f; m[2][1] = 0.f; m[2][2] = 1.f; m[2][3] = 0.f;
    m[3][0] = 0.f; m[3][1] = 0.f; m[3][2] = 0.f; m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by xyz-axis.
 *
 * formula:  m = FromEulerAngleXYZ(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by x-axis
 * @param[in]  t2          angle by y-axis
 * @param[in]  t3          angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleXYZ(
    float m[4][4],
    float t1,
    float t2,
    float t3)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleXYZ api
    float c1 = cosf(t1);
    float s1 = sinf(t1);
    float c2 = cosf(t2);
    float s2 = sinf(t2);
    float c3 = cosf(t3);
    float s3 = sinf(t3);

    m[0][0] = c2 * c3;
    m[0][1] = c1 * s3 + s1 * s2 * c3;
    m[0][2] = s1 * s3 - c1 * s2 * c3;
    m[0][3] = 0.f;
    m[1][0] = -c2 * s3;
    m[1][1] = c1 * c3 - s1 * s2 * s3;
    m[1][2] = s1 * c3 + c1 * s2 * s3;
    m[1][3] = 0.f;
    m[2][0] = s2;
    m[2][1] = -s1 * c2;
    m[2][2] = c1 * c2;
    m[2][3] = 0.f;
    m[3][0] = 0.f;
    m[3][1] = 0.f;
    m[3][2] = 0.f;
    m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by xzy-axis.
 *
 * formula:  m = FromEulerAngleXZY(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by x-axis
 * @param[in]  t2          angle by z-axis
 * @param[in]  t3          angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleXZY(
    float m[4][4],
    float t1,
    float t2,
    float t3)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleXZY api
    float c1 = cosf(t1);
    float s1 = sinf(t1);
    float c2 = cosf(t2);
    float s2 = sinf(t2);
    float c3 = cosf(t3);
    float s3 = sinf(t3);

    m[0][0] = c2 * c3;
    m[0][1] = s1 * s3 + c1 * c3 * s2;
    m[0][2] = c3 * s1 * s2 - c1 * s3;
    m[0][3] = 0.f;
    m[1][0] = -s2;
    m[1][1] = c1 * c2;
    m[1][2] = c2 * s1;
    m[1][3] = 0.f;
    m[2][0] = c2 * s3;
    m[2][1] = c1 * s2 * s3 - c3 * s1;
    m[2][2] = c1 * c3 + s1 * s2 * s3;
    m[2][3] = 0.f;
    m[3][0] = 0.f;
    m[3][1] = 0.f;
    m[3][2] = 0.f;
    m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by yxz-axis(yaw, pitch, roll).
 *
 * formula:  m = FromEulerAngleYXZ(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by y-axis
 * @param[in]  t2          angle by x-axis
 * @param[in]  t3          angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleYXZ(
    float m[4][4],
    float t1,
    float t2,
    float t3)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleYXZ api
    float c1 = cosf(t1);
    float s1 = sinf(t1);
    float c2 = cosf(t2);
    float s2 = sinf(t2);
    float c3 = cosf(t3);
    float s3 = sinf(t3);

    m[0][0] = c1 * c3 + s1 * s2 * s3;
    m[0][1] = s3 * c2;
    m[0][2] = c1 * s2 * s3 - s1 * c3;
    m[0][3] = 0.f;
    m[1][0] = s1 * s2 * c3 - c1 * s3;
    m[1][1] = c3 * c2;
    m[1][2] = s3 * s1 + c1 * s2 * c3;
    m[1][3] = 0.f;
    m[2][0] = s1 * c2;
    m[2][1] = -s2;
    m[2][2] = c1 * c2;
    m[2][3] = 0.f;
    m[3][0] = 0.f;
    m[3][1] = 0.f;
    m[3][2] = 0.f;
    m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by yzx-axis.
 *
 * formula:  m = FromEulerAngleYZX(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by y-axis
 * @param[in]  t2          angle by z-axis
 * @param[in]  t3          angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleYZX(
    float m[4][4],
    float t1,
    float t2,
    float t3)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleYZX api
    float c1 = cosf(t1);
    float s1 = sinf(t1);
    float c2 = cosf(t2);
    float s2 = sinf(t2);
    float c3 = cosf(t3);
    float s3 = sinf(t3);

    m[0][0] = c1 * c2;
    m[0][1] = s2;
    m[0][2] = -c2 * s1;
    m[0][3] = 0.f;
    m[1][0] = s1 * s3 - c1 * c3 * s2;
    m[1][1] = c2 * c3;
    m[1][2] = c1 * s3 + c3 * s1 * s2;
    m[1][3] = 0.f;
    m[2][0] = c3 * s1 + c1 * s2 * s3;
    m[2][1] = -c2 * s3;
    m[2][2] = c1 * c3 - s1 * s2 * s3;
    m[2][3] = 0.f;
    m[3][0] = 0.f;
    m[3][1] = 0.f;
    m[3][2] = 0.f;
    m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by zxy-axis.
 *
 * formula:  m = FromEulerAngleZXY(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by z-axis
 * @param[in]  t2          angle by x-axis
 * @param[in]  t3          angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleZXY(
    float m[4][4],
    float t1,
    float t2,
    float t3)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleZXY api
    float c1 = cosf(t1);
    float s1 = sinf(t1);
    float c2 = cosf(t2);
    float s2 = sinf(t2);
    float c3 = cosf(t3);
    float s3 = sinf(t3);

    m[0][0] = c1 * c3 - s1 * s2 * s3;
    m[0][1] = c3 * s1 + c1 * s2 * s3;
    m[0][2] = -c2 * s3;
    m[0][3] = 0.f;
    m[1][0] = -c2 * s1;
    m[1][1] = c1 * c2;
    m[1][2] = s2;
    m[1][3] = 0.f;
    m[2][0] = c1 * s3 + c3 * s1 * s2;
    m[2][1] = s1 * s3 - c1 * c3 * s2;
    m[2][2] = c2 * c3;
    m[2][3] = 0.f;
    m[3][0] = 0.f;
    m[3][1] = 0.f;
    m[3][2] = 0.f;
    m[3][3] = 1.f;
}

/*!
 * @brief Make a Matrix4x4 by zyx-axis.
 *
 * formula:  m = FromEulerAngleZYX(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by z-axis
 * @param[in]  t2          angle by y-axis
 * @param[in]  t3          angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleZYX(
    float m[4][4],
    float t1,
    float t2,
    float t3)
{
    // https://github.com/g-truc/glm
    // glm::eulerAngleZYX api
    float c1 = cosf(t1);
    float s1 = sinf(t1);
    float c2 = cosf(t2);
    float s2 = sinf(t2);
    float c3 = cosf(t3);
    float s3 = sinf(t3);

    m[0][0] = c1 * c2;
    m[0][1] = c2 * s1;
    m[0][2] = -s2;
    m[0][3] = 0.f;
    m[1][0] = c1 * s2 * s3 - c3 * s1;
    m[1][1] = c1 * c3 + s1 * s2 * s3;
    m[1][2] = c2 * s3;
    m[1][3] = 0.f;
    m[2][0] = s1 * s3 + c1 * c3 * s2;
    m[2][1] = c3 * s1 * s2 - c1 * s3;
    m[2][2] = c2 * c3;
    m[2][3] = 0.f;
    m[3][0] = 0.f;
    m[3][1] = 0.f;
    m[3][2] = 0.f;
    m[3][3] = 1.f;
}

/*!
 * @brief Extract a Matrix4x4 by xyz-axis.
 *
 * formula:  ToEulerAngleXYZ(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by xyz-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleXYZ(
    const float m[4][4],
    float a[3])
{
    // https://github.com/g-truc/glm
    // glm::extractEulerAngleXYZ api
    float T1 = atan2f(m[2][1], m[2][2]);
    float C2 = sqrtf(m[0][0] * m[0][0] + m[1][0] * m[1][0]);
    float T2 = atan2f(-m[2][0], C2);
    float S1 = sinf(T1);
    float C1 = cosf(T1);
    float T3 = atan2f(S1 * m[0][2] - C1 * m[0][1], C1 * m[1][1] - S1 * m[1][2]);
    a[0] = -T1;
    a[1] = -T2;
    a[2] = -T3;
}

/*!
 * @brief Extract a Matrix4x4 by xzy-axis.
 *
 * formula:  ToEulerAngleXZY(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by xzy-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleXZY(
    const float m[4][4],
    float a[3])
{
    // https://github.com/g-truc/glm
    // glm::extractEulerAngleXZY api
    float T1 = atan2f(m[1][2], m[1][1]);
    float C2 = sqrtf(m[0][0] * m[0][0] + m[2][0] * m[2][0]);
    float T2 = atan2f(-m[1][0], C2);
    float S1 = sinf(T1);
    float C1 = cosf(T1);
    float T3 = atan2f(S1 * m[0][1] - C1 * m[0][2], C1 * m[2][2] - S1 * m[2][1]);
    a[0] = T1;
    a[1] = T2;
    a[2] = T3;
}

/*!
 * @brief Extract a Matrix4x4 by yxz-axis(yaw, pitch, roll).
 *
 * formula:  ToEulerAngleYXZ(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by yxz-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleYXZ(
    const float m[4][4],
    float a[3])
{
    // https://github.com/g-truc/glm
    // glm::extractEulerAngleYXZ api
    float T1 = atan2f(m[2][0], m[2][2]);
    float C2 = sqrtf(m[0][1] * m[0][1] + m[1][1] * m[1][1]);
    float T2 = atan2f(-m[2][1], C2);
    float S1 = sinf(T1);
    float C1 = cosf(T1);
    float T3 = atan2f(S1 * m[1][2] - C1 * m[1][0], C1 * m[0][0] - S1 * m[0][2]);
    a[0] = T1;
    a[1] = T2;
    a[2] = T3;
}

/*!
 * @brief Extract a Matrix4x4 by yzx-axis.
 *
 * formula:  ToEulerAngleYZX(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by yzx-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleYZX(
    const float m[4][4],
    float a[3])
{
    // https://github.com/g-truc/glm
    // glm::extractEulerAngleYZX api
    float T1 = atan2f(-m[0][2], m[0][0]);
    float C2 = sqrtf(m[1][1] * m[1][1] + m[2][1] * m[2][1]);
    float T2 = atan2f(m[0][1], C2);
    float S1 = sinf(T1);
    float C1 = cosf(T1);
    float T3 = atan2f(S1 * m[1][0] + C1 * m[1][2], S1 * m[2][0] + C1 * m[2][2]);
    a[0] = T1;
    a[1] = T2;
    a[2] = T3;
}

/*!
 * @brief Extract a Matrix4x4 by zxy-axis.
 *
 * formula:  ToEulerAngleZXY(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by zxy-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleZXY(
    const float m[4][4],
    float a[3])
{
    // https://github.com/g-truc/glm
    // glm::extractEulerAngleZXY api
    float T1 = atan2f(-m[1][0], m[1][1]);
    float C2 = sqrtf(m[0][2] * m[0][2] + m[2][2] * m[2][2]);
    float T2 = atan2f(m[1][2], C2);
    float S1 = sinf(T1);
    float C1 = cosf(T1);
    float T3 = atan2f(C1 * m[2][0] + S1 * m[2][1], C1 * m[0][0] + S1 * m[0][1]);
    a[0] = T1;
    a[1] = T2;
    a[2] = T3;
}

/*!
 * @brief Extract a Matrix4x4 by zyx-axis.
 *
 * formula:  ToEulerAngleZYX(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by zyx-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleZYX(
    const float m[4][4],
    float a[3])
{
    // https://github.com/g-truc/glm
    // glm::extractEulerAngleZYX api
    float T1 = atan2f(m[0][1], m[0][0]);
    float C2 = sqrtf(m[1][2] * m[1][2] + m[2][2] * m[2][2]);
    float T2 = atan2f(-m[0][2], C2);
    float S1 = sinf(T1);
    float C1 = cosf(T1);
    float T3 = atan2f(S1 * m[2][0] - C1 * m[2][1], C1 * m[1][1] - S1 * m[1][0]);
    a[0] = T1;
    a[1] = T2;
    a[2] = T3;
}

MM_EXPORT_DLL const float mmMat4x4Identity[4][4] =
{
    { 1, 0, 0, 0, },
    { 0, 1, 0, 0, },
    { 0, 0, 1, 0, },
    { 0, 0, 0, 1, },
};

MM_EXPORT_DLL const float mmMat4x4Zero[4][4] =
{
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
};
