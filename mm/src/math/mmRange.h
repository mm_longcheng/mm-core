/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRange_h__
#define __mmRange_h__

#include "core/mmCore.h"

#include "math/mmVector2.h"

#include "core/mmPrefix.h"

struct mmRange
{
    // offset
    size_t o;
    // length
    size_t l;
};

/*!
 * @brief Init a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL 
void 
mmRange_Init(
    struct mmRange* p);

/*!
 * @brief Destroy a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL 
void 
mmRange_Destroy(
    struct mmRange* p);

/*!
 * @brief Reset a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL 
void 
mmRange_Reset(
    struct mmRange* p);

/*!
 * @brief Zero a Range.
 *
 * @param[in,out] p   Range
 */
MM_EXPORT_DLL 
void 
mmRange_Zero(
    struct mmRange* p);

/*!
 * @brief Make a Range.
 *
 * @param[out] p   Range
 * @param[in]  o   offset
 * @param[in]  l   length
 */
MM_EXPORT_DLL 
void 
mmRange_Make(
    struct mmRange* p, 
    size_t o, size_t l);

/*!
 * @brief Check if two range are equal.
 *
 * formula:  return p == q
 *
 * @param[in]  p   range p
 * @param[in]  q   range q
 * @return         p == b
 */
MM_EXPORT_DLL 
int 
mmRange_Equals(
    const struct mmRange* p, 
    const struct mmRange* q);

/*!
 * @brief Assign a Range.
 *
 * @param[out] p   Range
 * @param[in]  q   Range
 */
MM_EXPORT_DLL 
void 
mmRange_Assign(
    struct mmRange* p, 
    const struct mmRange* q);

/*!
 * @brief Check if two range memery are equal.
 *
 * formula:  return memcmp(p, q)
 *
 * @param[in]  p   range p
 * @param[in]  q   range q
 * @return         memcmp(p, q)
 */
MM_EXPORT_DLL 
intptr_t 
mmRange_Compare(
    const struct mmRange* p, 
    const struct mmRange* q);

MM_EXPORT_DLL extern const struct mmRange mmRangeZero;

#include "core/mmSuffix.h"

#endif//__mmRange_h__
