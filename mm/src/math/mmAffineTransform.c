/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAffineTransform.h"

#include "math/mmVector2.h"
#include "math/mmMath.h"

/*!
 * @brief Init a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Init(
    struct mmAffineTransform* p)
{
    p->a = 1; p->b = 0;
    p->c = 0; p->d = 1;
    p->x = 0; p->y = 0;
}

/*!
 * @brief Init a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Destroy(
    struct mmAffineTransform* p)
{
    p->a = 1; p->b = 0;
    p->c = 0; p->d = 1;
    p->x = 0; p->y = 0;
}

/*!
 * @brief Reset a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Reset(
    struct mmAffineTransform* p)
{
    p->a = 1; p->b = 0;
    p->c = 0; p->d = 1;
    p->x = 0; p->y = 0;
}

/*!
 * @brief Zero a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Zero(
    struct mmAffineTransform* p)
{
    p->a = 0; p->b = 0;
    p->c = 0; p->d = 0;
    p->x = 0; p->y = 0;
}

/*!
 * @brief Make a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Make(
    struct mmAffineTransform* p,
    float a, float b,
    float c, float d,
    float x, float y)
{
    p->a = a; p->b = b;
    p->c = c; p->d = d;
    p->x = x; p->y = y;
}

/*!
 * @brief Make a Identity Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_MakeIdentity(
    struct mmAffineTransform* p)
{
    p->a = 1; p->b = 0;
    p->c = 0; p->d = 1;
    p->x = 0; p->y = 0;
}

/*!
 * @brief Assign a Transform.
 *
 * @param[out] p   Transform
 * @param[in]  q   Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Assign(
    struct mmAffineTransform* p,
    const struct mmAffineTransform* q)
{
    p->a = q->a;
    p->b = q->b;
    p->c = q->c;
    p->d = q->d;
    p->x = q->x;
    p->y = q->y;
}

/*!
 * @brief Apply vec2.
 *
 * @param[out] r        vec2
 * @param[in]  m        Transform
 * @param[in]  point    vec2
 */
MM_EXPORT_DLL
void
mmAffineTransform_ApplyVec2(
    float r[2],
    const struct mmAffineTransform* m,
    const float point[2])
{
    r[0] = m->a * point[0] + m->c * point[1] + m->x;
    r[1] = m->b * point[0] + m->d * point[1] + m->y;
}

/*!
 * @brief Apply Size.
 *
 * @param[out] r        vec2
 * @param[in]  m        Transform
 * @param[in]  size     vec2
 */
MM_EXPORT_DLL
void
mmAffineTransform_ApplySize(
    float r[2],
    const struct mmAffineTransform* m,
    const float size[2])
{
    r[0] = m->a * size[0] + m->c * size[1];
    r[1] = m->b * size[0] + m->d * size[1];
}

/*!
 * @brief Apply Rect.
 *
 * @param[out] v        rect
 * @param[in]  m        Transform
 * @param[in]  rect     rect
 */
MM_EXPORT_DLL
void
mmAffineTransform_ApplyRect(
    struct mmRectF* v,
    const struct mmAffineTransform* m,
    const struct mmRectF* rect)
{
    float tl[2];
    float tr[2];
    float bl[2];
    float br[2];
    
    float t = rect->min[1];
    float l = rect->min[0];
    float r = rect->max[0];
    float b = rect->max[1];
    
    mmVec2Make(tl, l, t);
    mmVec2Make(tr, r, t);
    mmVec2Make(bl, l, b);
    mmVec2Make(br, r, b);
    
    mmAffineTransform_ApplyVec2(tl, m, tl);
    mmAffineTransform_ApplyVec2(tr, m, tr);
    mmAffineTransform_ApplyVec2(bl, m, bl);
    mmAffineTransform_ApplyVec2(br, m, br);
    
    v->min[0] = mmMathMin(mmMathMin(tl[0], tr[0]), mmMathMin(bl[0], br[0]));
    v->min[1] = mmMathMin(mmMathMin(tl[1], tr[1]), mmMathMin(bl[1], br[1]));
    v->max[0] = mmMathMax(mmMathMax(tl[0], tr[0]), mmMathMax(bl[0], br[0]));
    v->max[1] = mmMathMax(mmMathMax(tl[1], tr[1]), mmMathMax(bl[1], br[1]));
}

/*!
 * @brief Translate Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 * @param[in]  tx       float
 * @param[in]  ty       float
 */
MM_EXPORT_DLL
void
mmAffineTransform_Translate(
    struct mmAffineTransform* r,
    const struct mmAffineTransform* m,
    float tx, float ty)
{
    r->a = m->a;
    r->b = m->b;
    r->c = m->c;
    r->d = m->d;
    r->x = m->x + m->a * tx + m->c * ty;
    r->y = m->y + m->b * tx + m->d * ty;
}

/*!
 * @brief Scale Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 * @param[in]  sx       float
 * @param[in]  sy       float
 */
MM_EXPORT_DLL
void
mmAffineTransform_Scale(
    struct mmAffineTransform* r,
    const struct mmAffineTransform* m,
    float sx, float sy)
{
    r->a = m->a * sx;
    r->b = m->b * sx;
    r->c = m->c * sy;
    r->d = m->d * sy;
    r->x = m->x;
    r->y = m->y;
}

/*!
 * @brief Rotate Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 * @param[in]  anAngle  float
 */
MM_EXPORT_DLL
void
mmAffineTransform_Rotate(
    struct mmAffineTransform* r,
    const struct mmAffineTransform* m,
    float anAngle)
{
    float s = sinf(anAngle);
    float c = cosf(anAngle);

    r->a = m->a * c + m->c * s;
    r->b = m->b * c + m->d * s;
    r->c = m->c * c - m->a * s;
    r->d = m->d * c - m->b * s;
    r->x = m->x;
    r->y = m->y;
}

/*!
 * @brief Concat Transform.
 *
 * @param[out] r        Transform
 * @param[in]  t1       Transform
 * @param[in]  t2       Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Concat(
    struct mmAffineTransform* r,
    const struct mmAffineTransform* t1,
    const struct mmAffineTransform* t2)
{
    r->a = t1->a * t2->a + t1->b * t2->c;
    r->b = t1->a * t2->b + t1->b * t2->d;
    r->c = t1->c * t2->a + t1->d * t2->c;
    r->d = t1->c * t2->b + t1->d * t2->d;
    r->x = t1->x * t2->a + t1->y * t2->c + t2->x;
    r->y = t1->x * t2->b + t1->y * t2->d + t2->y;
}

/*!
 * @brief Check if two Transform are equal.
 *
 * formula:  return t1 == t2
 *
 * @param[in]  t1   range t1
 * @param[in]  t2   range t2
 * @return          t1 == t2
 */
MM_EXPORT_DLL
int
mmAffineTransform_Equals(
    const struct mmAffineTransform* t1,
    const struct mmAffineTransform* t2)
{
    return 
        t1->a == t2->a && t1->b == t2->b && 
        t1->c == t2->c && t1->d == t2->d && 
        t1->x == t2->x && t1->y == t2->y;
}

/*!
 * @brief Invert Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 */
MM_EXPORT_DLL
void
mmAffineTransform_Invert(
    struct mmAffineTransform* r,
    const struct mmAffineTransform* m)
{
    float determinant = 1.0f / (m->a * m->d - m->b * m->c);

    r->a = +determinant * m->d;
    r->b = -determinant * m->b;
    r->c = -determinant * m->c;
    r->d = +determinant * m->a;
    r->x = +determinant * (m->c * m->y - m->d * m->x);
    r->y = +determinant * (m->b * m->x - m->a * m->y);
}

MM_EXPORT_DLL const struct mmAffineTransform mmAffineTransformIdentity =
{
    1, 0,
    0, 1,
    0, 0,
};
