/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMathConst_h__
#define __mmMathConst_h__

// https://github.com/recp/cglm
// modify name 
#define MM_E                  2.71828182845904523536028747135266250   /* e           */
#define MM_LOG2E              1.44269504088896340735992468100189214   /* log2(e)     */
#define MM_LOG10E             0.434294481903251827651128918916605082  /* log10(e)    */
#define MM_LN2                0.693147180559945309417232121458176568  /* loge(2)     */
#define MM_LN10               2.30258509299404568401799145468436421   /* loge(10)    */
#define MM_PI                 3.14159265358979323846264338327950288   /* pi          */
#define MM_PI_DIV_2           1.57079632679489661923132169163975144   /* pi/2        */
#define MM_PI_DIV_4           0.785398163397448309615660845819875721  /* pi/4        */
#define MM_1_DIV_PI           0.318309886183790671537767526745028724  /* 1/pi        */
#define MM_2_DIV_PI           0.636619772367581343075535053490057448  /* 2/pi        */
#define MM_2_DIV_SQRTPI       1.12837916709551257389615890312154517   /* 2/sqrt(pi)  */
#define MM_SQRT2              1.41421356237309504880168872420969808   /* sqrt(2)     */
#define MM_1_DIV_SQRT2        0.707106781186547524400844362104849039  /* 1/sqrt(2)   */

#define MM_EPSILON            1e-6

#define MM_MATH_LOG2E         MM_LOG2E              // log2(e)
#define MM_MATH_LOG10E        MM_LOG10E             // log10(e)
#define MM_MATH_LN2           MM_LN2                // ln(2)
#define MM_MATH_LN10          MM_LN10               // ln(10)

#define MM_MATH_E             MM_E                  // E
#define MM_MATH_SQRTE         1.6487212707001282    // sqrt(E)
#define MM_MATH_1_DIV_E       0.36787944117144233   // 1 / E
#define MM_MATH_1_DIV_SQRTE   0.60653065971263342   // 1 / sqrt(E)
#define MM_MATH_LOG02_E       MM_LOG2E              // ln(E)
#define MM_MATH_LOG10_E       MM_LOG10E             // log(E)
#define MM_MATH_LN2           MM_LN2                // ln(2)
#define MM_MATH_LN10          MM_LN10               // ln(10)
#define MM_MATH_E_POW_4       54.598150033144229    // e ^ 4

#define MM_MATH_PI            MM_PI                 // PI
#define MM_MATH_SQRT2         MM_SQRT2              // sqrt(2)
#define MM_MATH_PI_DIV_2      MM_PI_DIV_2           // PI / 2
#define MM_MATH_PI_DIV_4      MM_PI_DIV_4           // PI / 4
#define MM_MATH_1_DIV_PI      MM_1_DIV_PI           // 1 / PI
#define MM_MATH_2_DIV_PI      MM_2_DIV_PI           // 2 / PI
#define MM_MATH_2_MUL_PI      6.2831853071795862    // 2 * PI
#define MM_MATH_2_DIV_SQRTPI  MM_2_DIV_SQRTPI       // 2 / sqrt(PI)
#define MM_MATH_1_DIV_SQRT2   MM_1_DIV_SQRT2        // 1 / sqrt(2)

#define MM_MATH_PI_DIV_180    0.017453292519943295  // PI / 180
#define MM_MATH_180_DIV_PI    57.295779513082323    // 180 / PI

#endif//__mmMathConst_h__
