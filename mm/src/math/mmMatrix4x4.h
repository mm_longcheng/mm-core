/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMatrix4x4_h__
#define __mmMatrix4x4_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*
 *  Column-Major layout
 *
 *  [0][0] [1][0] [2][0] [3][0]
 *  [0][1] [1][1] [2][1] [3][1]
 *  [0][2] [1][2] [2][2] [3][2]
 *  [0][3] [1][3] [2][3] [3][3]
 */

/*!
 * @brief Init a Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4Init(
    float m[4][4]);

/*!
 * @brief Destroy a Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4Destroy(
    float m[4][4]);

/*!
 * @brief Make a Matrix4x4 from value.
 *
 * @param[out] m           mat4x4 result
 */
MM_EXPORT_DLL 
void 
mmMat4x4Make(
    float m[4][4],
    float m00, float m01, float m02, float m03,
    float m10, float m11, float m12, float m13,
    float m20, float m21, float m22, float m23,
    float m30, float m31, float m32, float m33);

/*!
 * @brief Make a identity Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4MakeIdentity(
    float m[4][4]);

/*!
 * @brief Make a zero Matrix4x4.
 *
 * @param[in,out] m        mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4MakeZero(
    float m[4][4]);

/*!
 * @brief Check if two matrices memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 * @return                 memcmp(a, b)
 */
MM_EXPORT_DLL 
int 
mmMat4x4Compare(
    const float a[4][4], 
    const float b[4][4]);

/*!
 * @brief Check if two matrices are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 * @return                 a == b
 */
MM_EXPORT_DLL 
int 
mmMat4x4Equals(
    const float a[4][4], 
    const float b[4][4]);

/*!
 * @brief Assign a Matrix4x4.
 *
 * formula:  m = a
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4Assign(
    float m[4][4], 
    const float a[4][4]);

/*!
 * @brief Make a duplicate from a Matrix4x4.
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4Duplicate(
    float r[4][4], 
    const float m[4][4]);

/*!
 * @brief Get row i from a Matrix4x4.
 *
 * formula:  r = row(m, i)
 *
 * @param[out] r           vec4 result
 * @param[in]  m           mat4x4
 * @param[in]  i           row index
 */
MM_EXPORT_DLL 
void 
mmMat4x4Row(
    float r[4], 
    const float m[4][4], 
    int i);

/*!
 * @brief Get column i from a Matrix4x4.
 *
 * formula:  r = column(m, i)
 *
 * @param[out] r           vec4 result
 * @param[in]  m           mat4x4
 * @param[in]  i           column index
 */
MM_EXPORT_DLL 
void 
mmMat4x4Col(
    float r[4], 
    const float m[4][4], 
    int i);

/*!
 * @brief Transpose a Matrix4x4.
 *
 * formula:  r = Transpose(m)
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4Transpose(
    float r[4][4], 
    const float m[4][4]);

/*!
 * @brief Make a mat4x4 by matrix addition matrix.
 *
 * formula:  m = a + b
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 */
MM_EXPORT_DLL 
void 
mmMat4x4Add(
    float m[4][4], 
    const float a[4][4], 
    const float b[4][4]);

/*!
 * @brief Make a mat4x4 by matrix subtraction matrix.
 *
 * formula:  m = a - b
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 */
MM_EXPORT_DLL 
void 
mmMat4x4Sub(
    float m[4][4], 
    const float a[4][4], 
    const float b[4][4]);

/*!
 * @brief Scale each element of the matrix.
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  k           k
 */
MM_EXPORT_DLL 
void 
mmMat4x4Scale(
    float m[4][4], 
    const float a[4][4], 
    float k);

/*!
 * @brief Make a mat4x4 by scale (x, y, z).
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  x           x
 * @param[in]  y           y
 * @param[in]  z           z
 */
MM_EXPORT_DLL 
void 
mmMat4x4ScaleXYZ(
    float m[4][4], 
    const float a[4][4], 
    float x, float y, float z);

/*!
 * @brief Make a mat4x4 by matrix multiply matrix.
 *
 * formula:  m = a * b
 *
 * @param[out] m           mat4x4 result
 * @param[in]  a           mat4x4 a
 * @param[in]  b           mat4x4 b
 */
MM_EXPORT_DLL 
void 
mmMat4x4Mul(
    float m[4][4], 
    const float a[4][4], 
    const float b[4][4]);

/*!
 * @brief Make a vec4 by vec4 right mul mat4x4.
 *
 * formula:  r = m * v
 *
 * @param[out] r           vec4 result
 * @param[in]  m           mat4x4
 * @param[in]  v           vec4 value. 
 */
MM_EXPORT_DLL 
void 
mmMat4x4MulVec4(
    float r[4], 
    const float m[4][4], 
    const float v[4]);


/*!
 * @brief Make a vec3 by vec3 right mul mat4x4.
 *
 * formula:  r = m * v
 *
 * @param[out] r           vec3 result
 * @param[in]  m           mat4x4
 * @param[in]  v           vec3 value. 
 */
MM_EXPORT_DLL 
void 
mmMat4x4MulVec3(
    float r[3], 
    const float m[4][4], 
    const float v[3]);

/*!
 * @brief Make a Translate Matrix4x4.
 *
 * @param[out] m           mat4x4 result
 * @param[in]  x           x
 * @param[in]  y           y
 * @param[in]  z           x
 */
MM_EXPORT_DLL 
void 
mmMat4x4Translate(
    float m[4][4], 
    float x, float y, float z);

/*!
 * @brief Rotate a Matrix4x4 by v(x, y, z) axis.
 *
 * formula:  r = Rotate(m, angle, v)
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  v           vec3 v
 * @param[in]  angle       angle by (x, y, z)
 */
MM_EXPORT_DLL
void
mmMat4x4Rotate(
    float r[4][4],
    const float m[4][4],
    float angle,
    const float v[3]);

/*!
 * @brief Rotate a Matrix4x4 by x-axis.
 *
 * formula:  q = RotateX(m, angle)
 *
 * @param[out] q           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  angle       angle by x-axis
 */
MM_EXPORT_DLL 
void 
mmMat4x4RotateX(
    float q[4][4], 
    const float m[4][4], 
    float angle);

/*!
 * @brief Rotate a Matrix4x4 by y-axis.
 *
 * formula:  q = RotateY(m, angle)
 *
 * @param[out] q           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  angle       angle by y-axis
 */
MM_EXPORT_DLL 
void 
mmMat4x4RotateY(
    float q[4][4], 
    const float m[4][4], 
    float angle);

/*!
 * @brief Rotate a Matrix4x4 by z-axis.
 *
 * formula:  q = RotateZ(m, angle)
 *
 * @param[out] q           mat4x4 result
 * @param[in]  m           mat4x4
 * @param[in]  angle       angle by z-axis
 */
MM_EXPORT_DLL 
void 
mmMat4x4RotateZ(
    float q[4][4], 
    const float m[4][4], 
    float angle);

/*!
 * @brief Inverse a Matrix4x4.
 *
 * formula:  r = Inverse(m)
 *
 * @param[out] r           mat4x4 result
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL 
void 
mmMat4x4Inverse(
    float r[4][4], 
    const float m[4][4]);

/*!
 * @brief Make Matrix4x4 form translate.
 *
 * formula:  m = FromTranslate(t)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t           translate(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat4x4FromTranslate(
    float m[4][4], 
    const float t[3]);

/*!
 * @brief Make Matrix4x4 form quaternion.
 *
 * formula:  m = FromQuat(q)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL 
void 
mmMat4x4FromQuat(
    float m[4][4], 
    const float q[4]);

/*!
 * @brief Make Matrix4x4 form scale.
 *
 * formula:  m = FromScale(s)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat4x4FromScale(
    float m[4][4], 
    const float s[3]);

/*!
 * @brief Make Matrix4x4 multiply form translate.
 *
 * formula:  r = m * FromTranslate(t)
 *
 * @param[out] r           result
 * @param[in]  m           mat4x4
 * @param[in]  t           translate(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat4x4MulTranslate(
    float r[4][4], 
    const float m[4][4], 
    const float t[3]);

/*!
 * @brief Make Matrix4x4 multiply form quaternion.
 *
 * formula:  r = m * FromQuat(q)
 *
 * @param[out] r           result
 * @param[in]  m           mat4x4
 * @param[in]  q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL
void 
mmMat4x4MulQuat(
    float r[4][4], 
    const float m[4][4], 
    const float q[4]);

/*!
 * @brief Make Matrix4x4 multiply form scale.
 *
 * formula:  r = m * FromScale(s)
 *
 * @param[out] r           result
 * @param[in]  m           mat4x4
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat4x4MulScale(
    float r[4][4], 
    const float m[4][4], 
    const float s[3]);

/*!
 * @brief Make Matrix4x4 form translate quaternion scale.
 *
 * formula:  m = FromTranslate(t) * FromQuat(q) * FromScale(s)
 *
 * @param[out] m           mat4x4
 * @param[in]  t           translate(x, y, z)
 * @param[in]  q           quaternion(x, y, z, w)
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat4x4MakeTransform(
    float m[4][4], 
    const float t[3], 
    const float q[4], 
    const float s[3]);

/*!
 * @brief determinant value for a Matrix4x4.
 *
 * @param[in]  m           mat4x4
 * @return                 determinant value
 */
MM_EXPORT_DLL 
float 
mmMat4x4Determinant(
    const float m[4][4]);

/*!
 * @brief Check if the matrix is affine.
 *
 * @param[in]  m           mat4x4
 */
MM_EXPORT_DLL 
int 
mmMat4x4IsAffine(
    const float m[4][4]);

/*!
 * @brief Matrix4x4 QDU decomposition.
 *
 * @param[in]  m           mat4x4
 * @param[out] kQ          Q
 * @param[out] kD          D
 * @param[out] kU          U
 */
MM_EXPORT_DLL 
void 
mmMat4x4QDUDecomposition(
    const float m[4][4], 
    float kQ[4][4], 
    float kD[3], 
    float kU[3]);

/*!
 * @brief Matrix4x4 decomposition. is simplify decompose.
 *
 * @param[in]  m           mat4x4
 * @param[out] s           scale(x, y, z)
 * @param[out] q           quaternion(x, y, z, w)
 * @param[out] t           translate(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat4x4Decomposition(
    const float m[4][4], 
    float s[3], 
    float q[4], 
    float t[3]);

/*!
 * @brief Matrix4x4 decompose.
 *
 * @param[in]  mat         mat4x4
 * @param[out] scale       scale(x, y, z)
 * @param[out] quaternion  quaternion(x, y, z, w)
 * @param[out] translate   translate(x, y, z)
 * @param[out] skew        skew(skewXY, skewXZ, skewYZ)
 * @param[out] perspective perspective(x, y, z, w)
 */
MM_EXPORT_DLL
int
mmMat4x4Decompose(
    const float mat[4][4],
    float scale[3],
    float quaternion[4],
    float translate[3],
    float skew[3],
    float perspective[4]);

/*!
 * @brief Make a Matrix4x4 by x-axis.
 *
 * formula:  m = FromEulerAngleX(angle)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  angle       angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleX(
    float m[4][4],
    float angle);

/*!
 * @brief Make a Matrix4x4 by y-axis.
 *
 * formula:  m = FromEulerAngleY(angle)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  angle       angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleY(
    float m[4][4],
    float angle);

/*!
 * @brief Make a Matrix4x4 by z-axis.
 *
 * formula:  m = FromEulerAngleZ(angle)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  angle       angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleZ(
    float m[4][4],
    float angle);

/*!
 * @brief Make a Matrix4x4 by xyz-axis.
 *
 * formula:  m = FromEulerAngleXYZ(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by x-axis
 * @param[in]  t2          angle by y-axis
 * @param[in]  t3          angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleXYZ(
    float m[4][4],
    float t1,
    float t2,
    float t3);

/*!
 * @brief Make a Matrix4x4 by xzy-axis.
 *
 * formula:  m = FromEulerAngleXZY(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by x-axis
 * @param[in]  t2          angle by z-axis
 * @param[in]  t3          angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleXZY(
    float m[4][4],
    float t1,
    float t2,
    float t3);

/*!
 * @brief Make a Matrix4x4 by yxz-axis(yaw, pitch, roll).
 *
 * formula:  m = FromEulerAngleYXZ(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by y-axis
 * @param[in]  t2          angle by x-axis
 * @param[in]  t3          angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleYXZ(
    float m[4][4],
    float t1,
    float t2,
    float t3);

/*!
 * @brief Make a Matrix4x4 by yzx-axis.
 *
 * formula:  m = FromEulerAngleYZX(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by y-axis
 * @param[in]  t2          angle by z-axis
 * @param[in]  t3          angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleYZX(
    float m[4][4],
    float t1,
    float t2,
    float t3);

/*!
 * @brief Make a Matrix4x4 by zxy-axis.
 *
 * formula:  m = FromEulerAngleZXY(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by z-axis
 * @param[in]  t2          angle by x-axis
 * @param[in]  t3          angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleZXY(
    float m[4][4],
    float t1,
    float t2,
    float t3);

/*!
 * @brief Make a Matrix4x4 by zyx-axis.
 *
 * formula:  m = FromEulerAngleZYX(t1, t2, t3)
 *
 * @param[out] m           mat4x4 result
 * @param[in]  t1          angle by z-axis
 * @param[in]  t2          angle by y-axis
 * @param[in]  t3          angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat4x4FromEulerAngleZYX(
    float m[4][4],
    float t1,
    float t2,
    float t3);

/*!
 * @brief Extract a Matrix4x4 by xyz-axis.
 *
 * formula:  ToEulerAngleXYZ(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by xyz-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleXYZ(
    const float m[4][4],
    float a[3]);

/*!
 * @brief Extract a Matrix4x4 by xzy-axis.
 *
 * formula:  ToEulerAngleXZY(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by xzy-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleXZY(
    const float m[4][4],
    float a[3]);

/*!
 * @brief Extract a Matrix4x4 by yxz-axis(yaw, pitch, roll).
 *
 * formula:  ToEulerAngleYXZ(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by yxz-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleYXZ(
    const float m[4][4],
    float a[3]);

/*!
 * @brief Extract a Matrix4x4 by yzx-axis.
 *
 * formula:  ToEulerAngleYZX(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by yzx-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleYZX(
    const float m[4][4],
    float a[3]);

/*!
 * @brief Extract a Matrix4x4 by zxy-axis.
 *
 * formula:  ToEulerAngleZXY(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by zxy-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleZXY(
    const float m[4][4],
    float a[3]);

/*!
 * @brief Extract a Matrix4x4 by zyx-axis.
 *
 * formula:  ToEulerAngleZYX(m, a)
 *
 * @param[in]  m           mat4x4 result
 * @param[out] a           angle by zyx-axis
 */
MM_EXPORT_DLL
void
mmMat4x4ToEulerAngleZYX(
    const float m[4][4],
    float a[3]);

MM_EXPORT_DLL extern const float mmMat4x4Identity[4][4];
MM_EXPORT_DLL extern const float mmMat4x4Zero[4][4];

#include "core/mmSuffix.h"

#endif//__mmMatrix4x4_h__
