/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMatrix4x4Projection.h"

#include "math/mmMatrix4x4.h"
#include "math/mmVector3.h"

MM_EXPORT_DLL
void
mmMat4x4LookAtRH(
    float m[4][4],
    const float eye[3],
    const float center[3],
    const float up[3])
{
    /* view matrix with right handed coordinate system. */
    float f[3];
    float s[3];
    float t[3];

    float m30, m31, m32;

    mmVec3Sub(f, center, eye);
    mmVec3Normalize(f, f);

    mmVec3MulCross(s, f, up);
    mmVec3Normalize(s, s);

    mmVec3MulCross(t, s, f);

    m30 = -mmVec3DotProduct(s, eye);
    m31 = -mmVec3DotProduct(t, eye);
    m32 = +mmVec3DotProduct(f, eye);

    m[0][0] = s[0]; m[0][1] = t[0]; m[0][2] = -f[0]; m[0][3] = 0.f;
    m[1][0] = s[1]; m[1][1] = t[1]; m[1][2] = -f[1]; m[1][3] = 0.f;
    m[2][0] = s[2]; m[2][1] = t[2]; m[2][2] = -f[2]; m[2][3] = 0.f;
    m[3][0] =  m30; m[3][1] =  m31; m[3][2] =   m32; m[3][3] = 1.f;
}

MM_EXPORT_DLL
void
mmMat4x4LookAtLH(
    float m[4][4],
    const float eye[3],
    const float center[3],
    const float up[3])
{
    /* view matrix with lift handed coordinate system. */
    float f[3];
    float s[3];
    float t[3];

    float m30, m31, m32;

    mmVec3Sub(f, center, eye);
    mmVec3Normalize(f, f);

    mmVec3MulCross(s, f, up);
    mmVec3Normalize(s, s);

    mmVec3MulCross(t, s, f);

    m30 = -mmVec3DotProduct(s, eye);
    m31 = -mmVec3DotProduct(t, eye);
    m32 = -mmVec3DotProduct(f, eye);

    m[0][0] = s[0]; m[0][1] = t[0]; m[0][2] = f[0]; m[0][3] = 0.f;
    m[1][0] = s[1]; m[1][1] = t[1]; m[1][2] = f[1]; m[1][3] = 0.f;
    m[2][0] = s[2]; m[2][1] = t[2]; m[2][2] = f[2]; m[2][3] = 0.f;
    m[3][0] =  m30; m[3][1] =  m31; m[3][2] =  m32; m[3][3] = 1.f;
}

// lift-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void 
mmMat4x4OrthogonalLHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f) 
{
    float rl, tb, fn;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);

    m[0][0] =  2.0f * rl;
    m[1][1] =  2.0f * tb;
    m[2][2] = -2.0f * fn;
    m[3][0] = -(r + l) * rl;
    m[3][1] = -(t + b) * tb;
    m[3][2] =  (f + n) * fn;
    m[3][3] = 1.0f;
}

// lift-hand coordinate clip-space of [ 0, 1].
MM_EXPORT_DLL
void 
mmMat4x4OrthogonalLHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f) 
{
    float rl, tb, fn;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);

    m[0][0] = 2.0f * rl;
    m[1][1] = 2.0f * tb;
    m[2][2] = -fn;
    m[3][0] = -(r + l) * rl;
    m[3][1] = -(t + b) * tb;
    m[3][2] =        n * fn;
    m[3][3] = 1.0f;
}

// right-hand coordinate clip-space of [-1, 1].
MM_EXPORT_DLL
void 
mmMat4x4OrthogonalRHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    float rl, tb, fn;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);

    m[0][0] = 2.0f * rl;
    m[1][1] = 2.0f * tb;
    m[2][2] = 2.0f * fn;
    m[3][0] = -(r + l) * rl;
    m[3][1] = -(t + b) * tb;
    m[3][2] =  (f + n) * fn;
    m[3][3] = 1.0f;
}

// right-hand coordinate clip-space of [ 0, 1].
MM_EXPORT_DLL
void 
mmMat4x4OrthogonalRHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    float rl, tb, fn;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);

    m[0][0] = 2.0f * rl;
    m[1][1] = 2.0f * tb;
    m[2][2] = fn;
    m[3][0] = -(r + l) * rl;
    m[3][1] = -(t + b) * tb;
    m[3][2] =        n * fn;
    m[3][3] = 1.0f;
}

// left-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumLHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f) 
{
    float rl, tb, fn, nv;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);
    nv =  2.0f * n;

    m[0][0] = nv * rl;
    m[1][1] = nv * tb;
    m[2][0] =  (r + l) * rl;
    m[2][1] =  (t + b) * tb;
    m[2][2] = -(f + n) * fn;
    m[2][3] = 1.0f;
    m[3][2] = f * nv * fn;
}

// lift-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumLHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    float rl, tb, fn, nv;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);
    nv =  2.0f * n;

    m[0][0] = nv * rl;
    m[1][1] = nv * tb;
    m[2][0] = (r + l) * rl;
    m[2][1] = (t + b) * tb;
    m[2][2] =      -f * fn;
    m[2][3] = 1.0f;
    m[3][2] = f * n * fn;
}

// right-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumRHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    float rl, tb, fn, nv;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);
    nv =  2.0f * n;

    m[0][0] = nv * rl;
    m[1][1] = nv * tb;
    m[2][0] = (r + l) * rl;
    m[2][1] = (t + b) * tb;
    m[2][2] = (f + n) * fn;
    m[2][3] = -1.0f;
    m[3][2] = f * nv * fn;
}

// right-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumRHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    float rl, tb, fn, nv;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);
    nv =  2.0f * n;

    m[0][0] = nv * rl;
    m[1][1] = nv * tb;
    m[2][0] = (r + l) * rl;
    m[2][1] = (t + b) * tb;
    m[2][2] =       f * fn;
    m[2][3] = -1.0f;
    m[3][2] = f * n * fn;
}

// left-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveLHNOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f) 
{
    float ft, fn;

    mmMat4x4MakeZero(m);

    ft = 1.0f / tanf(fovy * 0.5f);
    fn = 1.0f / (n - f);

    m[0][0] = ft / aspect;
    m[1][1] = ft;
    m[2][2] = -(n + f) * fn;
    m[2][3] = 1.0f;
    m[3][2] = 2.0f * n * f * fn;
}

// lift-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveLHZOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f)
{
    float ft, fn;

    mmMat4x4MakeZero(m);

    ft = 1.0f / tanf(fovy * 0.5f);
    fn = 1.0f / (n - f);

    m[0][0] = ft / aspect;
    m[1][1] = ft;
    m[2][2] = -f * fn;
    m[2][3] = 1.0f;
    m[3][2] = n * f * fn;
}

// right-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveRHNOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f)
{
    float ft, fn;

    mmMat4x4MakeZero(m);

    ft = 1.0f / tanf(fovy * 0.5f);
    fn = 1.0f / (n - f);

    m[0][0] = ft / aspect;
    m[1][1] = ft;
    m[2][2] = (n + f) * fn;
    m[2][3] = -1.0f;
    m[3][2] = 2.0f * n * f * fn;
}

// right-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveRHZOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f)
{
    float ft, fn;

    mmMat4x4MakeZero(m);

    ft = 1.0f / tanf(fovy * 0.5f);
    fn = 1.0f / (n - f);

    m[0][0] = ft / aspect;
    m[1][1] = ft;
    m[2][2] = f * fn;
    m[2][3] = -1.0f;
    m[3][2] = n * f * fn;
}

MM_EXPORT_DLL
void
mmMat4x4OrthogonalNormalize(
    float r[4][4], 
    const float m[4][4])
{
    float s = 1.0f;
    float h[3];

    mmMat4x4Duplicate(r, m);

    mmVec3Normalize(r[2], r[2]);

    s = mmVec3MulInner(r[1], r[2]);
    mmVec3Scale(h, r[2], s);
    mmVec3Sub(r[1], r[1], h);
    mmVec3Normalize(r[2], r[2]);

    s = mmVec3MulInner(r[1], r[2]);
    mmVec3Scale(h, r[2], s);
    mmVec3Sub(r[1], r[1], h);
    mmVec3Normalize(r[1], r[1]);

    s = mmVec3MulInner(r[0], r[1]);
    mmVec3Scale(h, r[1], s);
    mmVec3Sub(r[0], r[0], h);
    mmVec3Normalize(r[0], r[0]);
}

// GL
// NDC x:r[l,r]=>[-1,+1],y:u[b,t]=>[-1,+1],z:i[n,f]=>[-1,+1]
// VLC x:r,y:u,z:o
MM_EXPORT_DLL
void
mmMat4x4OrthogonalGL(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    // right-hand coordinate clip-space of [-1, 1] +y up.
    mmMat4x4OrthogonalRHNOYU(m, l, r, b, t, n, f);
}

MM_EXPORT_DLL
void
mmMat4x4FrustumGL(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    // right-hand coordinate clip-space of [-1, 1] +y up.
    mmMat4x4FrustumRHNOYU(m, l, r, b, t, n, f);
}

MM_EXPORT_DLL
void
mmMat4x4PerspectiveGL(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f)
{
    // right-hand coordinate clip-space of [-1, 1] +y up.
    mmMat4x4PerspectiveRHNOYU(m, fovy, aspect, n, f);
}

// VK
// NDC x:r[l,r]=>[-1,+1],y:d[b,t]=>[-1,+1],z:i[n,f]=>[ 0,+1]
// VLC x:r,y:d,z:o
MM_EXPORT_DLL
void
mmMat4x4OrthogonalVK(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    // lift-hand coordinate clip-space of [ 0, 1] +y down.
    float rl, tb, fn;

    mmMat4x4MakeZero(m);

    rl = 1.0f / (r - l);
    tb = 1.0f / (t - b);
    fn = 1.0f / (f - n);

    m[0][0] =  2.0f * rl;
    m[1][1] = -2.0f * tb;
    m[2][2] =         fn;
    m[3][0] = -(r + l) * rl;
    m[3][1] = -(t + b) * tb - 2.0f;
    m[3][2] = -      n * fn;
    m[3][3] = 1.0f;
}

MM_EXPORT_DLL
void
mmMat4x4FrustumVK(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f)
{
    // right-hand coordinate clip-space of [ 0, 1] +y down.
    float rl, tb, fn, nv;

    mmMat4x4MakeZero(m);

    rl =  1.0f / (r - l);
    tb =  1.0f / (t - b);
    fn = -1.0f / (f - n);
    nv =  2.0f * n;

    m[0][0] =  nv * rl;
    m[1][1] = -nv * tb;
    m[2][0] =  (r + l) * rl;
    m[2][1] =  (t + b) * tb;
    m[2][2] =        f * fn;
    m[2][3] = -1.0f;
    m[3][2] =  f * n * fn;
}

MM_EXPORT_DLL
void
mmMat4x4PerspectiveVK(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f)
{
    // right-hand coordinate clip-space of [ 0, 1] +y down.
    float ft, fn;

    mmMat4x4MakeZero(m);

    ft = 1.0f / tanf(fovy * 0.5f);
    fn = 1.0f / (n - f);

    m[0][0] =  ft / aspect;
    m[1][1] = -ft;
    m[2][2] =  f * fn;
    m[2][3] = -1.0f;
    m[3][2] =  n * f * fn;
}
