/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmQuaternion_h__
#define __mmQuaternion_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*!
 * Quat(x, y, z, w)
 */

MM_EXPORT_DLL extern const float mmQuatEpsilon;

MM_EXPORT_DLL extern const float mmQuatIdentity[4];

/*!
 * @brief Assign a Quaternion.
 *
 * formula:  r = a
 *
 * @param[out] r               quat result
 * @param[in]  a               quat
 */
MM_EXPORT_DLL
void 
mmQuatAssign(
    float r[4], 
    const float a[4]);

/*!
 * @brief Check if two Quaternions memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a               quat a
 * @param[in]  b               quat b
 * @return                     memcmp(a, b)
 */
MM_EXPORT_DLL 
int 
mmQuatCompare(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Check if two Quaternions are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a               quat a
 * @param[in]  b               quat b
 * @return                     a == b
 */
MM_EXPORT_DLL 
int 
mmQuatEquals(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Make Quaternion.
 *
 * @param[out] q               quat
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmQuatMake(
    float q[4], 
    float x, 
    float y, 
    float z, 
    float w);

/*!
 * @brief Make identity Quaternion.
 *
 * @param[in,out] q            quat
 */
MM_EXPORT_DLL 
void 
mmQuatMakeIdentity(
    float q[4]);

/*!
 * @brief Addition Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 * @param[in]  b               quat
 */
MM_EXPORT_DLL 
void 
mmQuatAdd(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Subtract Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 * @param[in]  b               quat
 */
MM_EXPORT_DLL 
void 
mmQuatSub(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Multiply Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  p               quat
 * @param[in]  q               quat
 */
MM_EXPORT_DLL 
void 
mmQuatMul(
    float r[4], 
    const float p[4], 
    const float q[4]);

/*!
 * @brief Scale Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  v               quat
 * @param[in]  s               float
 */
MM_EXPORT_DLL 
void 
mmQuatScale(
    float r[4], 
    const float v[4], 
    float s);

/*!
 * @brief Inner product Quaternion.
 *
 * @param[in]  a               quat
 * @param[in]  b               quat
 * @return     InnerProduct(a, b)
 */
MM_EXPORT_DLL 
float 
mmQuatInnerProduct(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Make a Conjugate from a Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  q               quat
 */
MM_EXPORT_DLL 
void 
mmQuatConjugate(
    float r[4], 
    const float q[4]);

/*!
 * @brief right vec3 to Quaternion.
 *
 * formula:  r = q * v
 *
 * @param[out] r               vec3
 * @param[in]  q               quat
 * @param[in]  v               vec3
 */
MM_EXPORT_DLL 
void 
mmQuatMulVec3(
    float r[3], 
    const float q[4], 
    const float v[3]);


/*!
 * @brief Make a Quaternion from mat4x4.
 *
 * @param[out] q               quat
 * @param[in]  m               mat4x4
 */
MM_EXPORT_DLL 
void 
mmQuatFromMat4x4(
    float q[4], 
    const float m[4][4]);

/*!
 * @brief Make a Quaternion from mat3x3.
 *
 * @param[out] q               quat
 * @param[in]  m               mat3x3
 */
MM_EXPORT_DLL 
void 
mmQuatFromMat3x3(
    float q[4], 
    const float m[3][3]);

/*!
 * @brief Quaternion multiply k.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmQuatMulK(
    float r[4], 
    const float a[4], 
    float k);

/*!
 * @brief Negate a quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 */
MM_EXPORT_DLL 
void 
mmQuatNegate(
    float r[4], 
    const float a[4]);

/*!
 * @brief Dot product.
 *
 * @param[in]  a               quat
 * @param[in]  b               quat
 * @return     dot(a, b)
 */
MM_EXPORT_DLL 
float 
mmQuatDotProduct(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Inverse a quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 */
MM_EXPORT_DLL 
void 
mmQuatInverse(
    float r[4], 
    const float a[4]);

/*!
 * @brief From angle axis.
 *
 * @param[out]  q              quat
 * @param[in]   rfAngle        float
 * @param[in]   rkAxis         vec3
 */
MM_EXPORT_DLL 
void 
mmQuatFromAngleAxis(
    float q[4], 
    float rfAngle, 
    const float rkAxis[3]);

/*!
 * @brief To angle axis.
 *
 * @param[in]   q              quat
 * @param[out]  rfAngle        float*
 * @param[out]  rkAxis         vec3
 */
MM_EXPORT_DLL 
void 
mmQuatToAngleAxis(
    const float q[4], 
    float* rfAngle, 
    float rkAxis[3]);

/*!
 * @brief Get Rotate x-axis.
 *
 * @param[in]   q              quat
 * @param[out]  xAxis          vec3
 */
MM_EXPORT_DLL 
void 
mmQuatXAxis(
    const float q[4], 
    float xAxis[3]);

/*!
 * @brief Get Rotate y-axis.
 *
 * @param[in]   q              quat
 * @param[out]  yAxis          vec3
 */
MM_EXPORT_DLL 
void 
mmQuatYAxis(
    const float q[4], 
    float yAxis[3]);

/*!
 * @brief Get Rotate z-axis.
 *
 * @param[in]   q              quat
 * @param[out]  zAxis          vec3
 */
MM_EXPORT_DLL 
void 
mmQuatZAxis(
    const float q[4], 
    float zAxis[3]);

/*!
 * @brief Get Rotate x-axis.
 *
 * @param[in]   q              quat
 * @param[in]   reprojectAxis  int
 * @return      rotate
 */
MM_EXPORT_DLL 
float 
mmQuatGetRotateX(
    const float q[4], 
    int reprojectAxis);

/*!
 * @brief Get Rotate y-axis.
 *
 * @param[in]   q              quat
 * @param[in]   reprojectAxis  int
 * @return      rotate
 */
MM_EXPORT_DLL 
float 
mmQuatGetRotateY(
    const float q[4], 
    int reprojectAxis);

/*!
 * @brief Get Rotate z-axis.
 *
 * @param[in]   q              quat
 * @param[in]   reprojectAxis  int
 * @return      rotate
 */
MM_EXPORT_DLL 
float 
mmQuatGetRotateZ(
    const float q[4], 
    int reprojectAxis);

/*!
 * @brief Get tolerance equals.
 *
 * @param[in]   a         quat
 * @param[in]   b         quat
 * @param[in]   tolerance float
 * @return      equals
 */
MM_EXPORT_DLL 
int 
mmQuatToleranceEquals(
    const float a[4], 
    const float b[4], 
    float tolerance);

/*!
 * @brief Get orientation equals.
 *
 * @param[in]   a         quat
 * @param[in]   b         quat
 * @param[in]   tolerance float
 * @return      equals
 */
MM_EXPORT_DLL 
int 
mmQuatOrientationEquals(
    const float a[4], 
    const float b[4], 
    float tolerance);

/*!
 * @brief Get rotation to.
 *
 * @param[out]  r            quat result
 * @param[in]   rkVector     vec3
 * @param[in]   dest         vec3
 * @param[in]   fallbackAxis vec3
 */
MM_EXPORT_DLL 
void 
mmQuatGetRotationTo(
    float r[4], 
    const float rkVector[3], 
    const float dest[3], 
    const float fallbackAxis[3]);

/*!
 * @brief normalize quaternion and store result in dest
 *
 * @param[out]  r     destination quaternion
 * @param[in]   q     quaternion to normalze
 */
MM_EXPORT_DLL
void
mmQuatNormalize(
    float r[4], 
    const float q[4]);

/*!
 * @brief interpolates between two quaternions
 *        using linear interpolation (LERP)
 *
 * @param[out]  r     result quaternion
 * @param[in]   from  from
 * @param[in]   to    to
 * @param[in]   t     interpolant (amount)
 */
MM_EXPORT_DLL
void
mmQuatLerp(
    float r[4], 
    const float from[4], 
    const float to[4], 
    float t);

/*!
 * @brief interpolates between two quaternions
 *        taking the shortest rotation path using
 *        normalized linear interpolation (NLERP)
 *
 * @param[out]  r     result quaternion
 * @param[in]   from  from
 * @param[in]   to    to
 * @param[in]   t     interpolant (amount)
 */
MM_EXPORT_DLL
void
mmQuatNlerp(
    float r[4], 
    const float from[3], 
    const float to[3], 
    float t);

/*!
 * @brief interpolates between two quaternions
 *        using spherical linear interpolation (SLERP)
 *
 * @param[out]  r     result quaternion
 * @param[in]   from  from
 * @param[in]   to    to
 * @param[in]   t     amout
 */
MM_EXPORT_DLL
void
mmQuatSlerp(
    float r[4], 
    const float from[4], 
    const float to[4], 
    float t);

#include "core/mmSuffix.h"

#endif//__mmQuaternion_h__

