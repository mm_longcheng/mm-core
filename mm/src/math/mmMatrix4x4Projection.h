/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMatrix4x4Projection_h__
#define __mmMatrix4x4Projection_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// right-hand coordinate look at.
MM_EXPORT_DLL
void
mmMat4x4LookAtRH(
    float m[4][4], 
    const float eye[3],
    const float center[3],
    const float up[3]);

// lift-hand coordinate look at.
MM_EXPORT_DLL
void
mmMat4x4LookAtLH(
    float m[4][4], 
    const float eye[3],
    const float center[3],
    const float up[3]);

// lift-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4OrthogonalLHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// lift-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4OrthogonalLHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// right-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4OrthogonalRHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// right-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4OrthogonalRHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// left-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumLHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// lift-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumLHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// right-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumRHNOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// right-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4FrustumRHZOYU(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

// left-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveLHNOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f);

// lift-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveLHZOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f);

// right-hand coordinate clip-space of [-1, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveRHNOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f);

// right-hand coordinate clip-space of [ 0, 1] +y up.
MM_EXPORT_DLL
void
mmMat4x4PerspectiveRHZOYU(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f);

// Orthogonal Normalize.
MM_EXPORT_DLL
void
mmMat4x4OrthogonalNormalize(
    float r[4][4],
    const float m[4][4]);

// GL
// NDC x:r[l,r]=>[-1,+1],y:u[b,t]=>[-1,+1],z:i[n,f]=>[-1,+1]
// VLC x:r,y:u,z:o
MM_EXPORT_DLL
void
mmMat4x4OrthogonalGL(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

MM_EXPORT_DLL
void
mmMat4x4FrustumGL(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

MM_EXPORT_DLL
void
mmMat4x4PerspectiveGL(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f);

// VK
// NDC x:r[l,r]=>[-1,+1],y:d[b,t]=>[-1,+1],z:i[n,f]=>[ 0,+1]
// VLC x:r,y:d,z:o
MM_EXPORT_DLL
void
mmMat4x4OrthogonalVK(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

MM_EXPORT_DLL
void
mmMat4x4FrustumVK(
    float m[4][4],
    float l, float r,
    float b, float t,
    float n, float f);

MM_EXPORT_DLL
void
mmMat4x4PerspectiveVK(
    float m[4][4],
    float fovy,
    float aspect,
    float n,
    float f);

#include "core/mmSuffix.h"

#endif//__mmMatrix4x4Projection_h__
