/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAffineTransform_h__
#define __mmAffineTransform_h__

#include "core/mmCore.h"

#include "math/mmRectangle.h"

#include "core/mmPrefix.h"

/*
 *  Column-Major layout
 *
 *  [0][0] [1][0] [2][0]
 *  [0][1] [1][1] [2][1]
 *  [0][2] [1][2] [2][2]
 *
 *  [ a, b, 0, ]
 *  [ c, d, 0, ]
 *  [ x, y, 1, ]
 */

struct mmAffineTransform
{
    float a, b;
    float c, d;
    float x, y;
};

/*!
 * @brief Init a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Init(
    struct mmAffineTransform* p);

/*!
 * @brief Init a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Destroy(
    struct mmAffineTransform* p);

/*!
 * @brief Reset a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Reset(
    struct mmAffineTransform* p);

/*!
 * @brief Zero a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Zero(
    struct mmAffineTransform* p);

/*!
 * @brief Make a Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Make(
    struct mmAffineTransform* p, 
    float a, float b, 
    float c, float d, 
    float x, float y);

/*!
 * @brief Make a Identity Transform.
 *
 * @param[in,out] p   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_MakeIdentity(
    struct mmAffineTransform* p);

/*!
 * @brief Assign a Transform.
 *
 * @param[out] p   Transform
 * @param[in]  q   Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Assign(
    struct mmAffineTransform* p, 
    const struct mmAffineTransform* q);

/*!
 * @brief Apply vec2.
 *
 * @param[out] r        vec2
 * @param[in]  m        Transform
 * @param[in]  point    vec2
 */
MM_EXPORT_DLL
void
mmAffineTransform_ApplyVec2(
    float r[2],
    const struct mmAffineTransform* m,
    const float point[2]);

/*!
 * @brief Apply Size.
 *
 * @param[out] r        vec2
 * @param[in]  m        Transform
 * @param[in]  size     vec2
 */
MM_EXPORT_DLL
void
mmAffineTransform_ApplySize(
    float r[2],
    const struct mmAffineTransform* m,
    const float size[2]);

/*!
 * @brief Apply Rect.
 *
 * @param[out] v        rect
 * @param[in]  m        Transform
 * @param[in]  rect     rect
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_ApplyRect(
    struct mmRectF* v, 
    const struct mmAffineTransform* m,
    const struct mmRectF* rect);

/*!
 * @brief Translate Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 * @param[in]  tx       float
 * @param[in]  ty       float
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Translate(
    struct mmAffineTransform* r, 
    const struct mmAffineTransform* m, 
    float tx, float ty);

/*!
 * @brief Scale Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 * @param[in]  sx       float
 * @param[in]  sy       float
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Scale(
    struct mmAffineTransform* r, 
    const struct mmAffineTransform* m, 
    float sx, float sy);

/*!
 * @brief Rotate Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 * @param[in]  anAngle  float
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Rotate(
    struct mmAffineTransform* r, 
    const struct mmAffineTransform* m, 
    float anAngle);

/*!
 * @brief Concat Transform.
 *
 * @param[out] r        Transform
 * @param[in]  t1       Transform
 * @param[in]  t2       Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Concat(
    struct mmAffineTransform* r, 
    const struct mmAffineTransform* t1, 
    const struct mmAffineTransform* t2);

/*!
 * @brief Check if two Transform are equal.
 *
 * formula:  return t1 == t2
 *
 * @param[in]  t1   range t1
 * @param[in]  t2   range t2
 * @return          t1 == t2
 */
MM_EXPORT_DLL 
int 
mmAffineTransform_Equals(
    const struct mmAffineTransform* t1, 
    const struct mmAffineTransform* t2);

/*!
 * @brief Invert Transform.
 *
 * @param[out] r        Transform
 * @param[in]  m        Transform
 */
MM_EXPORT_DLL 
void 
mmAffineTransform_Invert(
    struct mmAffineTransform* r, 
    const struct mmAffineTransform* m);

MM_EXPORT_DLL extern const struct mmAffineTransform mmAffineTransformIdentity;

#include "core/mmSuffix.h"

#endif//__mmAffineTransform_h__
