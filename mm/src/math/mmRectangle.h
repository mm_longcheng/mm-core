/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRectangle_h__
#define __mmRectangle_h__

#include "core/mmCore.h"

#include "math/mmVector2.h"

#include "core/mmPrefix.h"

struct mmRectI
{
    int min[2];
    int max[2];
};

struct mmRectF
{
    float min[2];
    float max[2];
};

/*!
 * @brief Init a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Init(
    struct mmRectF* p);

/*!
 * @brief Destroy a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Destroy(
    struct mmRectF* p);

/*!
 * @brief Reset a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Reset(
    struct mmRectF* p);

/*!
 * @brief Zero a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Zero(
    struct mmRectF* p);

/*!
 * @brief Make a Rect.
 *
 * @param[in,out] p   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Make(
    struct mmRectF* p, 
    float x1, float y1, 
    float x2, float y2);

/*!
 * @brief Check if two Rect are equal.
 *
 * formula:  return p == q
 *
 * @param[in]  p   rect p
 * @param[in]  q   rect q
 * @return         p == q
 */
MM_EXPORT_DLL 
int 
mmRectF_Equals(
    const struct mmRectF* p, 
    const struct mmRectF* q);

/*!
 * @brief Assign a Rect.
 *
 * @param[out] p   rect
 * @param[in]  q   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Assign(
    struct mmRectF* p, 
    const struct mmRectF* q);

/*!
 * @brief Union tow Rect.
 *
 * @param[out] r   rect
 * @param[in]  p   rect
 * @param[in]  q   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Union(
    struct mmRectF* r,
    const struct mmRectF* p, 
    const struct mmRectF* q);

/*!
 * @brief Intersection tow Rect.
 *
 * @param[out] r   rect
 * @param[in]  p   rect
 * @param[in]  q   rect
 */
MM_EXPORT_DLL 
void 
mmRectF_Intersection(
    struct mmRectF* r,
    const struct mmRectF* p, 
    const struct mmRectF* q);

/*!
 * @brief Check if contains point.
 *
 * formula:  return p == q
 *
 * @param[in]  p     rect p
 * @param[in]  q     vec2 q
 * @return     value bool
 */
MM_EXPORT_DLL 
int 
mmRectF_ContainsPoint(
    struct mmRectF* p, 
    const float q[2]);

/*!
 * @brief Check if two rect overlap.
 *
 * @param[in]  p     rect p
 * @param[in]  q     rect q
 * @return     value bool
 */
MM_EXPORT_DLL 
int 
mmRectF_Overlap(
    const struct mmRectF* p, 
    const struct mmRectF* q);

#include "core/mmSuffix.h"

#endif//__mmRectangle_h__
