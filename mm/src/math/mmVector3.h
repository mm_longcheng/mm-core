/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVector3_h__
#define __mmVector3_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*!
 * Vec3(x, y, z)
 */

MM_EXPORT_DLL extern const float mmVec3Zero[3];
MM_EXPORT_DLL extern const float mmVec3Unit[3];
MM_EXPORT_DLL extern const float mmVec3PositiveUnitX[3];
MM_EXPORT_DLL extern const float mmVec3PositiveUnitY[3];
MM_EXPORT_DLL extern const float mmVec3PositiveUnitZ[3];
MM_EXPORT_DLL extern const float mmVec3NegativeUnitX[3];
MM_EXPORT_DLL extern const float mmVec3NegativeUnitY[3];
MM_EXPORT_DLL extern const float mmVec3NegativeUnitZ[3];
MM_EXPORT_DLL extern const float mmVec3UnitScale[3];
MM_EXPORT_DLL extern const float mmVec3UnitHalf[3];

/*!
 * @brief Assign a vec3.
 *
 * formula:  r = a
 *
 * @param[out] r             vec3 result
 * @param[in]  v             vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Assign(
    float r[3], 
    const float v[3]);

/*!
 * @brief Assign value to a vec3.
 *
 * formula:  r = vec3(v, v, v)
 *
 * @param[out] r             vec3 result
 * @param[in]  v             float
 */
MM_EXPORT_DLL 
void 
mmVec3AssignValue(
    float r[3], 
    float v);

/*!
 * @brief Check if two vec3 memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a             vec3 a
 * @param[in]  b             vec3 b
 * @return                   memcmp(a, b)
 */
MM_EXPORT_DLL 
int 
mmVec3Compare(
    const float a[3], 
    const float b[3]);

/*!
 * @brief Check if two vec3 are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a             vec3 a
 * @param[in]  b             vec3 b
 * @return                   a == b
 */
MM_EXPORT_DLL 
int 
mmVec3Equals(
    const float a[3], 
    const float b[3]);

/*!
 * @brief Make vec3.
 *
 * @param[out] v               vec3
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 */
MM_EXPORT_DLL 
void 
mmVec3Make(
    float v[3], 
    float x, float y, float z);

/*!
 * @brief Make vec3 from vec2 and z.
 *
 * formula:  v = vec3(a, z)
 *
 * @param[out] v               vec3
 * @param[in]  a               vec2
 * @param[in]  z               float
 */
MM_EXPORT_DLL
void
mmVec3MakeFromVec2(
    float v[3],
    float a[2], float z);

/*!
 * @brief Addition vec3.
 *
 * formula:  r = a + b
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Add(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief Addition vec3.
 *
 * formula:  r = a + vec3(x, y, z)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  x               float
 * @param[in]  y               float
 */
MM_EXPORT_DLL 
void 
mmVec3AddXYZ(
    float r[3], 
    const float a[3], 
    float x, float y, float z);

/*!
 * @brief Addition vec3.
 *
 * formula:  r = a + vec3(k, k, k)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec3AddK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Subtract vec3.
 *
 * formula:  r = a - b
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Sub(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief Subtract vec3.
 *
 * formula:  r = a - vec3(x, y, z)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 */
MM_EXPORT_DLL 
void 
mmVec3SubXYZ(
    float r[3], 
    const float a[3], 
    float x, float y, float z);

/*!
 * @brief Subtract vec3.
 *
 * formula:  r = a - vec3(k, k, k)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec3SubK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Multiply vec3.
 *
 * formula:  r = a * b
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Mul(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief Multiply vec3.
 *
 * formula:  r = a * vec3(x, y, z)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 */
MM_EXPORT_DLL 
void 
mmVec3MulXYZ(
    float r[3], 
    const float a[3], 
    float x, float y, float z);

/*!
 * @brief Multiply vec3.
 *
 * formula:  r = a * vec3(k, k, k)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec3MulK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Division vec3.
 *
 * formula:  r = a / b
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Div(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief Division vec3.
 *
 * formula:  r = a / vec3(x, y, z)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 */
MM_EXPORT_DLL 
void 
mmVec3DivXYZ(
    float r[3], 
    const float a[3], 
    float x, float y, float z);

/*!
 * @brief Division vec3.
 *
 * formula:  r = a / vec3(k, k, k)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec3DivK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Negate vec3.
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Negate(
    float r[3], 
    const float a[3]);

/*!
 * @brief Length vec3.
 *
 * formula:  Length(a)
 *
 * @param[in]  a               vec3
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec3Length(
    const float a[3]);

/*!
 * @brief Squared length vec3.
 *
 * formula:  Length(a)^2
 *
 * @param[in]  a               vec3
 * @return     value           float
 */
MM_EXPORT_DLL 
float
mmVec3SquaredLength(
    const float a[3]);

/*!
 * @brief Distance vec3.
 *
 * formula:  Distance(a, b)
 *
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec3Distance(
    const float a[3], 
    const float b[3]);

/*!
 * @brief Squared distance vec3.
 *
 * formula:  Distance(a, b)^2
 *
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec3SquaredDistance(
    const float a[3], 
    const float b[3]);

/*!
 * @brief Perpendicular vec3.
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Perpendicular(
    float r[3], 
    const float a[3]);

/*!
 * @brief Dot product vec3.
 *
 * formula:  Dot(a, b)
 *
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 * @return     Dot(a, b)       float
 */
MM_EXPORT_DLL 
float 
mmVec3DotProduct(
    const float a[3], 
    const float b[3]);

/*!
 * @brief Cross product vec3.
 *
 * formula:  Cross(a, b)
 *
 * @param[out] r               vec3
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3CrossProduct(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief Scale each element of the vec3.
 *
 * @param[out] r               vec3 result
 * @param[in]  v               vec3
 * @param[in]  s               s
 */
MM_EXPORT_DLL 
void 
mmVec3Scale(
    float r[3], 
    const float v[3], 
    const float s);

/*!
 * @brief Multiply inner vec3.
 *
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 * @return     MulInner(a, b)  float
 */
MM_EXPORT_DLL 
float 
mmVec3MulInner(
    const float a[3], 
    const float b[3]);

/*!
 * @brief Multiply cross vec3.
 *
 * @param[out] r               vec3 result
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3MulCross(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief Normalize vec3.
 *
 * @param[out] r               vec3 result
 * @param[in]  v               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Normalize(
    float r[3], 
    const float v[3]);

/*!
 * @brief Reflect vec3.
 *
 * @param[out] r               vec3 result
 * @param[in]  v               vec3
 * @param[in]  n               vec3
 */
MM_EXPORT_DLL 
void 
mmVec3Reflect(
    float r[3], 
    const float v[3], 
    const float n[3]);

/*!
 * @brief Angle between two vec3.
 *
 * @param[in]  a               vec3
 * @param[in]  b               vec3
 * @return     angle           float
 */
MM_EXPORT_DLL 
float 
mmVec3AngleBetween(
    const float a[3], 
    const float b[3]);

/*!
 * @brief vector scale to length.
 *
 * Formula:
 *  v = desiredLength / length(v)
 *
 * @param[in,out] v                vector
 * @param[in]     desiredLength    desired length
 */
MM_EXPORT_DLL
void
mmVec3ScaleToLength(
    float v[3], 
    float desiredLength);

/*!
 * @brief Make a linear combination of two vectors and return the result.
 *
 * Formula:
 *  r = (a * ascl) + (b * bscl)
 *
 * @param[out] r       result
 * @param[in]  a       vector a
 * @param[in]  b       vector b
 * @param[in]  ascl    scale a
 * @param[in]  bscl    scale b
 */
MM_EXPORT_DLL
void
mmVec3LinearCombine(
    float r[3], 
    const float a[3], 
    const float b[3], 
    float ascl, 
    float bscl);

/*!
 * @brief abs values of vectors
 *
 * Formula:
 *  r =  abs(a)
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 */
MM_EXPORT_DLL
void
mmVec3Abs(
    float r[3],
    const float a[3]);

/*!
 * @brief max values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec3Max(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief min values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec3Min(
    float r[3], 
    const float a[3], 
    const float b[3]);

/*!
 * @brief clamp vector's individual members between min and max values
 *
 * @param[out] r      vector
 * @param[in]  v      vector
 * @param[in]  minVal minimum value
 * @param[in]  maxVal maximum value
 */
MM_EXPORT_DLL
void
mmVec3Clamp(
    float r[3], 
    float v[3], 
    float minVal, 
    float maxVal);

/*!
 * @brief linear interpolation between two vectors
 *
 * formula:  from + s * (to - from)
 *
 * @param[out] r    destination
 * @param[in]  from from value
 * @param[in]  to   to value
 * @param[in]  t    interpolant (amount)
 */
MM_EXPORT_DLL
void
mmVec3Lerp(
    float r[3], 
    const float from[3], 
    const float to[3], 
    float t);

/*!
 * @brief mix between two numbers
 *
 * formula:  r = x*(1-a) +y*a
 *
 * @param[out] r    destination
 * @param[in]  x  x value
 * @param[in]  y  y value
 * @param[in]  a  mix value
 */
MM_EXPORT_DLL
void
mmVec3Mix(
    float r[3],
    const float x[3],
    const float y[3],
    const float a);

/*!
 * @brief cubic bezier interpolation
 *
 * Formula:
 *  B(s) = P0*(1-s)^3 + 3*C0*s*(1-s)^2 + 3*C1*s^2*(1-s) + P1*s^3
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  c0   control point 1
 * @param[in]  c1   control point 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec3Bezier(
    float r[3], 
    float s, 
    const float p0[3], 
    const float c0[3], 
    const float c1[3], 
    const float p1[3]);

/*!
 * @brief cubic hermite interpolation
 *
 * Formula:
 *  H(s) = P0*(2*s^3 - 3*s^2 + 1) + T0*(s^3 - 2*s^2 + s)
 *            + P1*(-2*s^3 + 3*s^2) + T1*(s^3 - s^2)
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  t0   tangent 1
 * @param[in]  t1   tangent 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec3Hermite(
    float r[3], 
    float s, 
    const float p0[3], 
    const float t0[3], 
    const float t1[3], 
    const float p1[3]);

/*!
 * @brief threshold function
 *
 * @param[out] r       destination
 * @param[in]  edge    threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec3Step(
    float r[3], 
    const float edge[3], 
    const float x[3]);

/*!
 * @brief threshold function with a smooth transition
 *
 * @param[out] r       destination
 * @param[in]  edge0   low threshold
 * @param[in]  edge1   high threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec3Smoothstep(
    float r[3], 
    const float edge0[3], 
    const float edge1[3], 
    const float x[3]);

/*!
 * @brief swizzle vector components
 *
 * you can use existin masks e.g. GLM_XXX, GLM_ZYX
 *
 * @param[out] r    destination
 * @param[in]  v    source
 * @param[in]  mask mask
 */
MM_EXPORT_DLL
void
mmVec3Swizzle(
    float r[3], 
    const float v[3], 
    int mask);

#include "core/mmSuffix.h"

#endif//__mmVector3_h__
