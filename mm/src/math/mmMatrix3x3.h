/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMatrix3x3_h__
#define __mmMatrix3x3_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*
 *  Column-Major layout
 *
 *  [0][0] [1][0] [2][0]
 *  [0][1] [1][1] [2][1]
 *  [0][2] [1][2] [2][2]
 */

MM_EXPORT_DLL extern const float mmMat3x3Identity[3][3];
MM_EXPORT_DLL extern const float mmMat3x3Zero[3][3];


/*!
 * @brief Init a Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3Init(
    float m[3][3]);

/*!
 * @brief Destroy a Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3Destroy(
    float m[3][3]);

/*!
 * @brief Assign a Matrix3x3.
 *
 * formula:  m = a
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3Assign(
    float m[3][3], 
    const float a[3][3]);

/*!
 * @brief Check if two matrices memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 * @return                 memcmp(a, b)
 */
MM_EXPORT_DLL 
int 
mmMat3x3Compare(
    const float a[3][3], 
    const float b[3][3]);

/*!
 * @brief Check if two matrices are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 * @return                 a == b
 */
MM_EXPORT_DLL 
int 
mmMat3x3Equals(
    const float a[3][3], 
    const float b[3][3]);

/*!
 * @brief Make a Matrix3x3 from value.
 *
 * @param[out] m           mat3x3 result
 */
MM_EXPORT_DLL 
void 
mmMat3x3Make(
    float m[3][3],
    float m00, float m01, float m02,
    float m10, float m11, float m12,
    float m20, float m21, float m22);

/*!
 * @brief Make a identity Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3MakeIdentity(
    float m[3][3]);

/*!
 * @brief Make a zero Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3MakeZero(
    float m[3][3]);

/*!
 * @brief Make a mat3x3 by matrix addition matrix.
 *
 * formula:  m = a + b
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 */
MM_EXPORT_DLL 
void 
mmMat3x3Add(
    float m[3][3], 
    const float a[3][3], 
    const float b[3][3]);

/*!
 * @brief Make a mat3x3 by matrix subtraction matrix.
 *
 * formula:  m = a - b
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 */
MM_EXPORT_DLL 
void 
mmMat3x3Sub(
    float m[3][3], 
    const float a[3][3], 
    const float b[3][3]);

/*!
 * @brief Scale each element of the matrix.
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  k           k
 */
MM_EXPORT_DLL 
void 
mmMat3x3Scale(
    float m[3][3], 
    const float a[3][3], 
    float k);

/*!
 * @brief Make a mat3x3 by matrix multiply matrix.
 *
 * formula:  m = a * b
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 */
MM_EXPORT_DLL 
void 
mmMat3x3Mul(
    float m[3][3], 
    const float a[3][3], 
    const float b[3][3]);

/*!
 * @brief Make a vec3 by vec3 right mul mat3x3.
 *
 * formula:  r = m * v
 *
 * @param[out] r           vec3 result
 * @param[in]  m           mat3x3
 * @param[in]  v           vec3 value.
 */
MM_EXPORT_DLL 
void 
mmMat3x3MulVec3(
    float r[3], 
    const float m[3][3], 
    const float v[3]);

/*!
 * @brief Make Matrix3x3 form translate.
 *
 * formula:  m = FromTranslate(t)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  t           translate(x, y)
 */
MM_EXPORT_DLL 
void 
mmMat3x3FromTranslate(
    float m[3][3], 
    const float t[2]);

/*!
 * @brief Make Matrix3x3 form quaternion.
 *
 * formula:  m = FromQuat(q)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL 
void 
mmMat3x3FromQuat(
    float m[3][3], 
    const float q[4]);

/*!
 * @brief Make Matrix3x3 form scale.
 *
 * formula:  m = FromScale(s)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL 
void 
mmMat3x3FromScale(
    float m[3][3], 
    const float s[3]);

/*!
 * @brief Transpose a Matrix3x3.
 *
 * formula:  r = Transpose(m)
 *
 * @param[out] r           mat3x3 result
 * @param[in]  m           mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3Transpose(
    float r[3][3], 
    const float m[3][3]);

/*!
 * @brief Inverse Matrix3x3.
 *
 * formula:  r = Inverse(m)
 *
 * @param[out] r           mat3x3 result
 * @param[in]  m           mat3x3
 */
MM_EXPORT_DLL 
void 
mmMat3x3Inverse(
    float r[3][3], 
    const float m[3][3]);

/*!
 * @brief Matrix3x3 QDU decomposition.
 *
 * @param[in]  m           mat3x3
 * @param[out] kQ          Q
 * @param[out] kD          D
 * @param[out] kU          U
 */
MM_EXPORT_DLL 
void 
mmMat3x3QDUDecomposition(
    const float m[3][3], 
    float kQ[3][3], 
    float kD[3], 
    float kU[3]);

/*!
 * @brief Matrix3x3 decomposition.
 *
 * @param[in]  m           mat3x3
 * @param[out] s           scale(x, y, z)
 * @param[out] q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL 
void 
mmMat3x3Decomposition(
    const float m[3][3], 
    float s[3], 
    float q[4]);

/*!
 * @brief Make a Matrix3x3 by x-axis.
 *
 * formula:  m = FromEulerAngleX(angle)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  angle       angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat3x3FromEulerAngleX(
    float m[3][3],
    float angle);

/*!
 * @brief Make a Matrix3x3 by y-axis.
 *
 * formula:  m = FromEulerAngleY(angle)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  angle       angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat3x3FromEulerAngleY(
    float m[3][3],
    float angle);

/*!
 * @brief Make a Matrix3x3 by z-axis.
 *
 * formula:  m = FromEulerAngleZ(angle)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  angle       angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat3x3FromEulerAngleZ(
    float m[3][3],
    float angle);

#include "core/mmSuffix.h"

#endif//__mmMatrix3x3_h__

