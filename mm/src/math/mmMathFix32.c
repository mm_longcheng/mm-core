/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMathFix32.h"

#ifdef __GNUC__

// Count leading zeros, using processor-specific instruction if available.
#define __mmFix32_CountLeadingZeroesU32Inline(x) (__builtin_clzl (x) - (8 * sizeof(long     ) - 32))
#define __mmFix32_CountLeadingZeroesU64Inline(x) (__builtin_clzll(x) - (8 * sizeof(long long) - 64))

#else

static mmInline mmUInt8_t __mmFix32_CountLeadingZeroesU32Inline(mmUInt32_t x)
{
    mmUInt8_t result = 0;
    if (x == 0) return 32;
    while (!(x & 0xF0000000)) { result += 4; x <<= 4; }
    while (!(x & 0x80000000)) { result += 1; x <<= 1; }
    return result;
}
static mmInline mmUInt8_t __mmFix32_CountLeadingZeroesU64Inline(mmUInt64_t x)
{
    mmUInt8_t result = 0;
    if (x == 0) return 64;
    while (!(x & 0xF000000000000000)) { result += 4; x <<= 4; }
    while (!(x & 0x8000000000000000)) { result += 1; x <<= 1; }
    return result;
}

#endif

/*
* operate "+".
*/
static mmInline mmFix32_t __static_mmFix32_Add(mmFix32_t x, mmFix32_t y);

/*
* operate "-".
*/
static mmInline mmFix32_t __static_mmFix32_Sub(mmFix32_t x, mmFix32_t y);

/*
* operate "*".
*/
static mmInline mmFix32_t __static_mmFix32_Mul(mmFix32_t x, mmFix32_t y);

/*
* operate "/".
*/
static mmInline mmFix32_t __static_mmFix32_Div(mmFix32_t x, mmFix32_t y);

/*
* operate "inv".
*/
static mmInline mmFix32_t __static_mmFix32_Inv(mmFix32_t x);

/*
* mmFix32_t right shifter 1.
*/
static mmInline mmFix32_t __static_mmFix32_RightShifter1(mmFix32_t x);

/**
* This assumes that the input value is >= 1.
*
* Note that this is only ever called with inValue >= 1 (because it has a wrapper to check.
* As such, the result is always less than the input.
*/
static mmFix32_t __static_mmFix32_Log2Inner(mmFix32_t x);

/*
* mmFix32_t to string itoa.
*/
static char* __static_mmFix32_ItoaLoop(char *buf, mmUInt64_t scale, mmUInt64_t value, int skip);

/*
* Unpack IEEE 754 floating point double format into fixed point sll format
*
* Description
*
*   IEEE 754 specifies the binary64 type ("double" in C) as having:
*
*   1 bit sign
*   11 bit exponent
*   53 bit significand
*
*   The first bit of the significand is an implied 1 which is not stored.
*   The decimal would be to the right of that implied 1, or to the left of
*   the stored significand.
*
*   The exponent is unsigned, and biased with an offset of 1023.
*
*   The IEEE 754 standard does not specify endianess, but the endian used is
*   traditionally the same endian that the processor uses.
*/

MM_EXPORT_DLL mmFix32_t mmFix32_Dbl2FixSlow(double d)
{
    union
    {
        double d;
        unsigned u[2];
        mmFix32U64_t ull;
        mmFix32S64_t sll;
    } in, retval;
    unsigned exp;

    /* Move into memory as args might be passed in regs */
    in.d = d;

    // Be careful dereferencing type-punned pointer will break strict-aliasing rules.
    if (MM_MATH_FIX32_DOUBLE_POSITIVE_INF == (mmFix32S64_t)in.sll) { return MM_MATH_FIX32_POSITIVE_INF; }
    if (MM_MATH_FIX32_DOUBLE_NEGATIVE_INF == (mmFix32S64_t)in.sll) { return MM_MATH_FIX32_NEGATIVE_INF; }
    if (MM_MATH_FIX32_DOUBLE_POSITIVE_NAN == (mmFix32S64_t)in.sll) { return MM_MATH_FIX32_POSITIVE_NAN; }
    if (MM_MATH_FIX32_DOUBLE_NEGATIVE_NAN == (mmFix32S64_t)in.sll) { return MM_MATH_FIX32_NEGATIVE_NAN; }

#if defined(BROKEN_IEEE754_DOUBLE)

    exp = in.u[0];
    in.u[0] = in.u[1];
    in.u[1] = exp;

#endif /* defined(BROKEN_IEEE754_DOUBLE) */

    /* Leading 1 is assumed by IEEE */
    retval.u[1] = 0x40000000;

    /* Unpack the mantissa into the unsigned long */
    retval.u[1] |= (in.u[1] << 10) & 0x3ffffc00;
    retval.u[1] |= (in.u[0] >> 22) & 0x000003ff;
    retval.u[0] = in.u[0] << 10;

    /* Extract the exponent and align the decimals */
    exp = (in.u[1] >> 20) & 0x7ff;
    if (exp)
        /* IEEE 754 decimal begins at right of bit position 30 */
        retval.ull >>= (1023 + 30) - exp;
    else
        return 0L;

    /* Negate if negative flag set */
    if (in.u[1] & 0x80000000)
        retval.sll = __mmFix32_Neg(retval.sll);

    return retval.sll;
}

/*
* Pack fixed point sll format into IEEE 754 floating point double format
*
* Description
*
*   IEEE 754 specifies the binary64 type ("double" in C) as having:
*
*   1 bit sign
*   11 bit exponent
*   53 bit significand
*
*   The first bit of the significand is an implied 1 which is not stored.
*   The decimal would be to the right of that implied 1, or to the left of
*   the stored significand.
*
*   The exponent is unsigned, and biased with an offset of 1023.
*
*   The IEEE 754 standard does not specify endianess, but the endian used is
*   traditionally the same endian that the processor uses.
*/
MM_EXPORT_DLL double mmFix32_Fix2DblSlow(mmFix32_t f)
{
    union
    {
        double d;
        unsigned u[2];
        mmFix32U64_t ull;
        mmFix32S64_t sll;
    } in, retval;
    unsigned exp;
    unsigned flag;

    // Be careful dereferencing type-punned pointer will break strict-aliasing rules.
    if (MM_MATH_FIX32_POSITIVE_INF == f) { retval.sll = (mmFix32S64_t)MM_MATH_FIX32_DOUBLE_POSITIVE_INF; return retval.d; }
    if (MM_MATH_FIX32_NEGATIVE_INF == f) { retval.sll = (mmFix32S64_t)MM_MATH_FIX32_DOUBLE_NEGATIVE_INF; return retval.d; }
    if (MM_MATH_FIX32_POSITIVE_NAN == f) { retval.sll = (mmFix32S64_t)MM_MATH_FIX32_DOUBLE_POSITIVE_NAN; return retval.d; }
    if (MM_MATH_FIX32_NEGATIVE_NAN == f) { retval.sll = (mmFix32S64_t)MM_MATH_FIX32_DOUBLE_NEGATIVE_NAN; return retval.d; }

    if (f == 0)
        return 0.0;

    /* Move into memory as args might be passed in regs */
    in.sll = f;

    /* Handle the negative flag */
    if (in.sll < 1)
    {
        flag = 0x80000000;
        in.ull = __mmFix32_Neg(in.sll);
    }
    else
        flag = 0x00000000;

    /*
    * Normalize
    *
    * IEEE 754 decimal-point begins at right of bit position 30
    */
    for (exp = (1023 + 30); in.ull && (in.u[1] & 0x80000000) == 0; exp--)
    {
        in.ull <<= 1;
    }
    in.ull <<= 1;
    exp++;
    in.ull >>= 12;
    retval.ull = in.ull;
    retval.u[1] |= flag | (exp << 20);

#if defined(BROKEN_IEEE754_DOUBLE)

    exp = retval.u[0];
    retval.u[0] = retval.u[1];
    retval.u[1] = exp;

#endif /* defined(BROKEN_IEEE754_DOUBLE)  */

    return retval.d;
}

/*
* Convert double to fix fast.
* the math-sll convert is too slow.
* https://github.com/PetteriAimonen/libfixmath
* here we not rounding.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Dbl2FixFast(double d)
{
    // Be careful dereferencing type-punned pointer will break strict-aliasing rules.
    mmFix32_t n = 0;
    memcpy(&n, &d, sizeof(double));
    if (MM_MATH_FIX32_DOUBLE_POSITIVE_INF == n) { return MM_MATH_FIX32_POSITIVE_INF; }
    if (MM_MATH_FIX32_DOUBLE_NEGATIVE_INF == n) { return MM_MATH_FIX32_NEGATIVE_INF; }
    if (MM_MATH_FIX32_DOUBLE_POSITIVE_NAN == n) { return MM_MATH_FIX32_POSITIVE_NAN; }
    if (MM_MATH_FIX32_DOUBLE_NEGATIVE_NAN == n) { return MM_MATH_FIX32_NEGATIVE_NAN; }
    return (mmFix32_t)(d * (double)MM_MATH_FIX32_1);
}

/*
* Convert fix to double fast.
* the math-sll convert is too slow.
* here we not rounding.
* https://github.com/PetteriAimonen/libfixmath
*/
MM_EXPORT_DLL double mmFix32_Fix2DblFast(mmFix32_t f)
{
    // Be careful dereferencing type-punned pointer will break strict-aliasing rules.
    double d = 0.0;
    if (MM_MATH_FIX32_POSITIVE_INF == f) { memcpy(&d, &MM_MATH_FIX32_DOUBLE_POSITIVE_INF, sizeof(double)); return d; }
    if (MM_MATH_FIX32_NEGATIVE_INF == f) { memcpy(&d, &MM_MATH_FIX32_DOUBLE_NEGATIVE_INF, sizeof(double)); return d; }
    if (MM_MATH_FIX32_POSITIVE_NAN == f) { memcpy(&d, &MM_MATH_FIX32_DOUBLE_POSITIVE_NAN, sizeof(double)); return d; }
    if (MM_MATH_FIX32_NEGATIVE_NAN == f) { memcpy(&d, &MM_MATH_FIX32_DOUBLE_NEGATIVE_NAN, sizeof(double)); return d; }
    return (double)((double)f / MM_MATH_FIX32_1);
}

/*
* Addition x + y.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Add(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t v = __mmFix32_Add(x, y);
    // if signs of operands are equal and signs of sum and x are different
    if ((((~(x ^ y) & (x ^ v)) & MM_MATH_FIX32_SIGN_MASK) != 0) && (MM_MATH_FIX32_NEGATIVE_INF < v && v < MM_MATH_FIX32_POSITIVE_INF))
    {
        v = x > 0 ? MM_MATH_FIX32_MAXIMUM : MM_MATH_FIX32_MINIMUM;
    }
    return v;
}

/*
* Subtraction x - y.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Sub(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t v = __mmFix32_Sub(x, y);
    // if signs of operands are different and signs of sum and x are different
    if (((((x ^ y) & (x ^ v)) & MM_MATH_FIX32_SIGN_MASK) != 0) && (MM_MATH_FIX32_NEGATIVE_INF < v && v < MM_MATH_FIX32_POSITIVE_INF))
    {
        v = x < 0 ? MM_MATH_FIX32_MINIMUM : MM_MATH_FIX32_MAXIMUM;
    }
    return v;
}

/*
* Multiply x * y
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Mul(mmFix32_t x, mmFix32_t y)
{
    return __mmFix32_Mul(x, y);
}

/*
* Multiply x / y
*/
/*
* Division
* 32-bit implementation of fix16_div. Fastest version for e.g. ARM Cortex M3.
* Performs 32-bit divisions repeatedly to reduce the remainder. For this to
* be efficient, the processor has to have 32-bit hardware division.
* https://github.com/PetteriAimonen/libfixmath
*/

MM_EXPORT_DLL mmFix32_t mmFix32_Div(mmFix32_t x, mmFix32_t y)
{
    return __mmFix32_Div(x, y);
}

/*
* Calculate cos x for any value of x, by quadrant
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Cos(mmFix32_t x)
{
    int i;
    mmFix32_t retval;

    /* Calculate cos (x - i * pi/2), where -pi/4 <= x - i * pi/2 <= pi/4  */
    i = __mmFix32_Fix2Int(__mmFix32_Add(__mmFix32_Mul(x, MM_MATH_FIX32_2_DIV_PI), MM_MATH_FIX32_1_DIV_002));
    x = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Int2Fix(i), MM_MATH_FIX32_PI_DIV_2));

    /* Locate the quadrant */
    switch (i & 3)
    {
    default:
    case 0:
        retval = __mmFix32_Cos(x);
        break;
    case 1:
        retval = __mmFix32_Neg(__mmFix32_Sin(x));
        break;
    case 2:
        retval = __mmFix32_Neg(__mmFix32_Cos(x));
        break;
    case 3:
        retval = __mmFix32_Sin(x);
        break;
    }

    return retval;
}

/*
* Calculate sin x for any value of x, by quadrant
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Sin(mmFix32_t x)
{
    int i;
    mmFix32_t retval;

    /* Calculate sin (x - n * pi/2), where -pi/4 <= x - i * pi/2 <= pi/4 */
    i = __mmFix32_Fix2Int(__mmFix32_Add(__mmFix32_Mul(x, MM_MATH_FIX32_2_DIV_PI), MM_MATH_FIX32_1_DIV_002));
    x = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Int2Fix(i), MM_MATH_FIX32_PI_DIV_2));

    /* Locate the quadrant */
    switch (i & 3)
    {
    default:
    case 0:
        retval = __mmFix32_Sin(x);
        break;
    case 1:
        retval = __mmFix32_Cos(x);
        break;
    case 2:
        retval = __mmFix32_Neg(__mmFix32_Sin(x));
        break;
    case 3:
        retval = __mmFix32_Neg(__mmFix32_Cos(x));
        break;
    }

    return retval;
}

/*
* Calculate tan x for any value of x, by quadrant
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Tan(mmFix32_t x)
{
    int i;
    mmFix32_t retval;

    i = __mmFix32_Fix2Int(__mmFix32_Add(__mmFix32_Mul(x, MM_MATH_FIX32_2_DIV_PI), MM_MATH_FIX32_1_DIV_002));
    x = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Int2Fix(i), MM_MATH_FIX32_PI_DIV_2));

    /* Locate the quadrant */
    switch (i & 3)
    {
    default:
    case 0:
    case 2:
        retval = __mmFix32_Div(__mmFix32_Sin(x), __mmFix32_Cos(x));
        break;
    case 1:
    case 3:
        retval = __mmFix32_Neg(__mmFix32_Div(__mmFix32_Cos(x), __mmFix32_Sin(x)));
        break;
    }

    return retval;
}

/*
*
* Calculate asin x, where |x| <= 1
*
* Description
*
*   asin x = SUM[n=0,) C(2 * n, n) * x^(2 * n + 1) / (4^n * (2 * n + 1)), |x| <= 1
*
*   where C(n, r) = nCr = n! / (r! * (n - r)!)
*
*   Using a two term approximation:
* [1]   a = x + x^3 / 6
*
*   Results in:
*   asin x = a + D
*   where D is the difference from the exact result
*
*   Letting D = asin d results in:
* [2]   asin x = a + asin d
*
*   Re-arranging:
*   asin x - a = asin d
*
*   Applying sin to both sides:
*   sin (asin x - a) = sin asin d
*   sin (asin x - a) = d
*   d = sin (asin x - a)
*
*   Applying the standard identity:
*   sin (u - v) = sin u * cos v - cos u * sin v
*
*   Results in:
*   d = sin asin x * cos a - cos asin x * sin a
*   d = x * cos a - cos asin x * sin a
*
*   Applying the standard identity:
*   cos asin u = (1 - u^2)^(1 / 2)
*
*   Results in:
* [3]   d = x * cos a - (1 - x^2)^(1 / 2) * sin a
*
*   Putting the pieces together:
* [1]   a = x + x^3 / 6
* [3]   d = x * cos a - (1 - x^2)^(1 / 2) * sin a
* [2]   asin x = a + asin d
*
*   The worst case is x = 1.0 which converges after 2 iterations.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Asin(mmFix32_t x)
{
    int left_side;
    mmFix32_t a;
    mmFix32_t retval;

    /* asin -x = -asin x */
    if ((left_side = x < 0))
        x = __mmFix32_Neg(x);

    /* Out-of-range */
    if (x > MM_MATH_FIX32_1)
        return 0;

    /* Initial approximate */
    a = __mmFix32_Mul(x, __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(x, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_006))));
    retval = a;

    /* First iteration */
    x = __mmFix32_Sub(__mmFix32_Mul(x, mmFix32_Cos(a)), __mmFix32_Mul(mmFix32_Sqrt(__mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(x, x))), mmFix32_Sin(a)));
    a = __mmFix32_Mul(x, __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(x, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_006))));
    retval = __mmFix32_Add(retval, a);

    /* Second iteration */
    x = __mmFix32_Sub(__mmFix32_Mul(x, mmFix32_Cos(a)), __mmFix32_Mul(mmFix32_Sqrt(__mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(x, x))), mmFix32_Sin(a)));
    a = __mmFix32_Mul(x, __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(x, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_006))));
    retval = __mmFix32_Add(retval, a);

    /* Negate result if necessary */
    return (left_side ? __mmFix32_Neg(retval) : retval);
}

/*
* Calculate atan x
*
* Description
*
*   atan x = SUM[n=0,) (-1)^n * x^(2  * n + 1) / (2 * n + 1), |x| <= 1
*
*   Using a two term approximation:
* [1]   a = x - x^3 / 3
*
*   Results in:
*   atan x = a + D
*   where D is the difference from the exact result
*
*   Letting D = atan d results in:
* [2]   atan x = a + atan d
*
*   Re-arranging:
*   atan x - a = atan d
*
*   Applying tan to both sides:
*   tan (atan x - a) = tan atan d
*   tan (atan x - a) = d
*   d = tan (atan x - a)
*
*   Applying the standard identity:
*   tan (u - v) = (tan u - tan v) / (1 + tan u * tan v)
*
*   Results in:
*   d = tan (atan x - a) = (tan atan x - tan a) / (1 + tan atan x * tan a)
*   d = tan (atan x - a) = (x - tan a) / (1 + x * tan a)
*
*   Let:
* [3]   t = tan a
*
*   Results in:
* [4]   d = (x - t) / (1 + x * t)
*
*   So putting the pieces together:
* [1]   a = x - x^3 / 3
* [3]   t = tan a
* [4]   d = (x - t) / (1 + x * t)
* [2]   atan x = a + atan d
*   atan x = a + atan ((x - t) / (1 + x * t))
*
*   The worst case is x = 1.0 which converges after 2 iterations.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_AtanSlow(mmFix32_t x)
{
    int side;
    mmFix32_t a;
    mmFix32_t t;
    mmFix32_t retval;


    if (x < MM_MATH_FIX32_1)
    {
        /* Left:  if (x < -1) then atan x = pi / 2 + atan 1 / x */
        side = -1;
        x = mmFix32_Inv(x);

    }
    else if (x > MM_MATH_FIX32_1)
    {
        /* Right:  if (x > 1) then atan x = pi / 2 - atan 1 / x */
        side = 1;
        x = mmFix32_Inv(x);
    }
    else
    {
        /* Middle:  -1 <= x <= 1 */
        side = 0;
    }

    /* Initial approximate */
    a = __mmFix32_Mul(x, __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(x, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_003))));
    retval = a;

    /* First iteration */
    t = __mmFix32_Div(__mmFix32_Sin(a), __mmFix32_Cos(a));
    x = __mmFix32_Div(__mmFix32_Sub(x, t), __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(x, t)));
    a = __mmFix32_Mul(x, __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(x, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_003))));
    retval = __mmFix32_Add(retval, a);

    /* Second iteration */
    t = __mmFix32_Div(__mmFix32_Sin(a), __mmFix32_Cos(a));
    x = __mmFix32_Div(__mmFix32_Sub(x, t), __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(x, t)));
    a = __mmFix32_Mul(x, __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(x, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_003))));
    retval = __mmFix32_Add(retval, a);

    if (side == -1)
    {
        /* Left:  if (x < -1) then atan x = pi / 2 + atan 1 / x */
        retval = __mmFix32_Add(MM_MATH_FIX32_PI_DIV_2, retval);
    }
    else if (side == 1)
    {
        /* Right:  if (x > 1) then atan x = pi / 2 - atan 1 / x */
        retval = __mmFix32_Sub(MM_MATH_FIX32_PI_DIV_2, retval);
    }

    return retval;
}

/*
 * http://dspguru.com/dsp/tricks/fixed-point-atan2-with-self-normalization/
 * https://github.com/PetteriAimonen/libfixmath
 */
MM_EXPORT_DLL mmFix32_t mmFix32_Atan2Slow(mmFix32_t y, mmFix32_t x)
{
    mmFix32_t angle = 0;
    mmFix32_t r = 0;
    // pi / 4
    mmFix32_t coeff_1 = MM_MATH_FIX32_PI_DIV_4;
    // 3 * pi / 4
    mmFix32_t coeff_2 = mmFix32_Mul(MM_MATH_FIX32_3, coeff_1);
    // kludge to prevent 0/0 condition
    mmFix32_t abs_y = mmFix32_Add(mmFix32_Fabs(y), mmFix32_Dbl2Fix(1e-10));
    if (x >= MM_MATH_FIX32_0)
    {
        r = mmFix32_Div(mmFix32_Sub(x, abs_y), mmFix32_Add(x, abs_y));
        angle = mmFix32_Sub(coeff_1, mmFix32_Mul(coeff_1, r));
    }
    else
    {
        r = mmFix32_Div(mmFix32_Add(x, abs_y), mmFix32_Sub(abs_y, x));
        angle = mmFix32_Sub(coeff_2, mmFix32_Mul(coeff_1, r));
    }
    
    if (y < MM_MATH_FIX32_0)
    {
        // negate if in quad III or IV
        return mmFix32_Neg(angle);
    }
    else
    {
        return angle;
    }
}

/*
* Calculate atan x fast.
*
* Approximates atan(x) normalized to the [-pi/2, pi/2] range
* with a maximum error of 0.1620 degrees.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_AtanFast(mmFix32_t x)
{
    mmFix32_t n = __mmFix32_NormalizedAtan(x);
    return __mmFix32_Mul(MM_MATH_FIX32_PI_DIV_2, n);
}

/*
* Approximates atan2(y, x) normalized to the [-pi, pi) range
* with a maximum error of 0.1620 degrees
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Atan2Fast(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t n = __mmFix32_NormalizedAtan2(__mmFix32_Neg(x), __mmFix32_Neg(y));
    return __mmFix32_Mul(MM_MATH_FIX32_PI_DIV_2, __mmFix32_Sub(n, MM_MATH_FIX32_2));
}

/*
* Calculate e^x for any value of x
*/
MM_EXPORT_DLL mmFix32_t mmFix32_ExpSlow(mmFix32_t x)
{
    int i;
    mmFix32_t e;
    mmFix32_t retval;

    e = MM_MATH_FIX32_E;

    /* -0.5 <= x <= 0.5  */
    i = __mmFix32_Fix2Int(__mmFix32_Add(x, MM_MATH_FIX32_1_DIV_002));
    retval = __mmFix32_Exp(__mmFix32_Sub(x, __mmFix32_Int2Fix(i)));

    /* i >= 0 */
    if (i < 0)
    {
        i = -i;
        e = MM_MATH_FIX32_1_DIV_E;
    }

    /* Scale the result */
    for (; i; i >>= 1)
    {
        if (i & 1)
            retval = __mmFix32_Mul(retval, e);
        e = __mmFix32_Mul(e, e);
    }

    return retval;
}

/*
* exp/pow2 x fast.
* https://github.com/PetteriAimonen/libfixmath
* https://github.com/asik/FixedMath.Net
*/

MM_EXPORT_DLL mmFix32_t mmFix32_ExpFast(mmFix32_t x)
{
    static const mmFix32_t fix_s32_maximum = 0x000000157cd0e702;// ln(MM_MATH_FIX32_MAXIMUM)
    static const mmFix32_t fix_s32_minimum = 0xffffffe9d1bd0106;// ln(MM_MATH_FIX32_MINIFRA)

    mmUInt8_t neg;

    mmFix32_t result = 0;
    mmFix32_t term = 0;

    uint_fast8_t i;

    if (x == MM_MATH_FIX32_0) return MM_MATH_FIX32_1;
    if (x == MM_MATH_FIX32_1) return MM_MATH_FIX32_E;
    if (x >= fix_s32_maximum) return MM_MATH_FIX32_MAXIMUM;
    if (x <= fix_s32_minimum) return MM_MATH_FIX32_0;

    /* The algorithm is based on the power series for exp(x):
    * http://en.wikipedia.org/wiki/Exponential_function#Formal_definition
    *
    * From term n, we get term n+1 by multiplying with x/n.
    * When the sum term drops to zero, we can stop summing.
    */

    // The power-series converges much faster on positive values
    // and exp(-x) = 1/exp(x).
    neg = (x < 0);
    if (neg) x = -x;

    result = x + MM_MATH_FIX32_1;
    term = x;

    for (i = 2; i < 30; i++)
    {
        term = __mmFix32_Mul(term, __mmFix32_Div(x, mmFix32_Int2Fix(i)));
        result += term;

        if ((term < 500) && ((i > 15) || (term < 20)))
            break;
    }

    if (neg) result = __mmFix32_Div(MM_MATH_FIX32_1, result);

    return result;
}

/*
* mmFix32_t right shifter 1.
*/
static mmInline mmFix32_t __static_mmFix32_RightShifter1(mmFix32_t x)
{
    return (x >> 1);
}

/**
* This assumes that the input value is >= 1.
*
* Note that this is only ever called with inValue >= 1 (because it has a wrapper to check.
* As such, the result is always less than the input.
*/
static mmFix32_t __static_mmFix32_Log2Inner(mmFix32_t x)
{
    mmFix32_t result = 0;

    while (x >= MM_MATH_FIX32_2)
    {
        result++;
        x = __static_mmFix32_RightShifter1(x);
    }

    if (x == 0) return (result << 16);

    mmUInt8_t i;
    for (i = 16; i > 0; i--)
    {
        x = __mmFix32_Mul(x, x);
        result <<= 1;
        if (x >= MM_MATH_FIX32_2)
        {
            result |= 1;
            x = __static_mmFix32_RightShifter1(x);
        }
    }

    x = __mmFix32_Mul(x, x);
    if (x >= MM_MATH_FIX32_2) result++;

    return result;
}

/*
* mmFix32_t to string itoa.
*/
static char* __static_mmFix32_ItoaLoop(char *buf, mmUInt64_t scale, mmUInt64_t value, int skip)
{
    while (scale)
    {
        unsigned digit = (unsigned)(value / scale);

        if (!skip || digit || scale == 1)
        {
            skip = 0;
            *buf++ = '0' + digit;
            value %= scale;
        }

        scale /= 10;
    }
    return buf;
}

/**
* calculates the log base 2 of input.
* Note that negative inputs are invalid! (will return fix16_overflow, since there are no exceptions)
*
* i.e. 2 to the power output = input.
* It's equivalent to the log or ln functions, except it uses base 2 instead of base 10 or base e.
* This is useful as binary things like this are easy for binary devices, like modern microprocessros, to calculate.
*
* This can be used as a helper function to calculate powers with non-integer powers and/or bases.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Log2Slow(mmFix32_t x)
{
    // Note that a negative x gives a non-real result.
    // If x == 0, the limit of log2(x)  as x -> 0 = -infinity.
    // log2(-ve) gives a complex result.
    if (x <= 0)
    {
        // non-positive value passed for log2, -nan value.
        return MM_MATH_FIX32_NEGATIVE_NAN;
    }

    // If the input is less than one, the result is -log2(1.0 / in)
    if (x < MM_MATH_FIX32_1)
    {
        // Note that the inverse of this would overflow.
        // This is the exact answer for log2(1.0 / 2 ^ MM_MATH_FIX32_BIT_HALF)
        if (x == 1) return mmFix32_Int2Fix(-MM_MATH_FIX32_BIT_HALF);

        mmFix32_t inverse = __mmFix32_Div(MM_MATH_FIX32_1, x);
        return -__static_mmFix32_Log2Inner(inverse);
    }

    // If input >= 1, just proceed as normal.
    // Note that x == fix16_one is a special case, where the answer is 0.
    return __static_mmFix32_Log2Inner(x);
}

/*
* log2 x fast.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Log2Fast(mmFix32_t x)
{
    mmFix32S64_t b = ((mmFix32S64_t)1) << (MM_MATH_FIX32_BIT_FRACTION - 1);
    mmFix32S64_t y = 0;
    mmFix32_t z = 0;
    int i = 0;

    if (x <= 0)
    {
        // non-positive value passed for log2, -nan value.
        return MM_MATH_FIX32_NEGATIVE_NAN;
    }

    // This implementation is based on Clay. S. Turner's fast binary logarithm
    // algorithm (C. S. Turner,  "A Fast Binary Logarithm Algorithm", IEEE Signal
    //     Processing Mag., pp. 124,140, Sep. 2010.)

    while (x < MM_MATH_FIX32_1)
    {
        x <<= 1;
        y -= MM_MATH_FIX32_1;
    }

    while (x >= (MM_MATH_FIX32_1 << 1))
    {
        x >>= 1;
        y += MM_MATH_FIX32_1;
    }

    z = x;

    for (i = 0; i < MM_MATH_FIX32_BIT_FRACTION; i++)
    {
        z = __mmFix32_Mul(z, z);
        if (z >= (MM_MATH_FIX32_1 << 1))
        {
            z = z >> 1;
            y += b;
        }
        b >>= 1;
    }

    return (mmFix32_t)y;
}

/*
* log x slow.
*
* Calculate natural logarithm using Netwton-Raphson method
*/
MM_EXPORT_DLL mmFix32_t mmFix32_LogSlow(mmFix32_t x)
{
    mmFix32_t x1;
    mmFix32_t ln;

    ln = 0;

    /* Scale: e^(-1/2) <= x <= e^(1/2) */
    while (x < MM_MATH_FIX32_1_DIV_SQRTE)
    {
        ln = __mmFix32_Sub(ln, MM_MATH_FIX32_1);
        x = __mmFix32_Mul(x, MM_MATH_FIX32_E);
    }
    while (x > MM_MATH_FIX32_SQRTE)
    {
        ln = __mmFix32_Add(ln, MM_MATH_FIX32_1);
        x = __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_E);
    }

    /* First iteration */
    x1 = __mmFix32_Mul(__mmFix32_Sub(x, MM_MATH_FIX32_1), __mmFix32_Div2(__mmFix32_Sub(x, MM_MATH_FIX32_3)));
    ln = __mmFix32_Sub(ln, x1);
    x = __mmFix32_Mul(x, __mmFix32_Exp(x1));

    /* Second iteration */
    x1 = __mmFix32_Mul(__mmFix32_Sub(x, MM_MATH_FIX32_1), __mmFix32_Div2(__mmFix32_Sub(x, MM_MATH_FIX32_3)));
    ln = __mmFix32_Sub(ln, x1);
    x = __mmFix32_Mul(x, __mmFix32_Exp(x1));

    /* Third iteration */
    x1 = __mmFix32_Mul(__mmFix32_Sub(x, MM_MATH_FIX32_1), __mmFix32_Div2(__mmFix32_Sub(x, MM_MATH_FIX32_3)));
    ln = __mmFix32_Sub(ln, x1);

    return ln;
}

/*
* log x fast.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_LogFast(mmFix32_t x)
{
    mmFix32_t guess = MM_MATH_FIX32_2;
    mmFix32_t delta;
    mmFix32_t e = 0;
    int scaling = 0;
    int count = 0;

    if (x <= 0)
    {
        // non-positive value passed for log2, -nan value.
        return MM_MATH_FIX32_NEGATIVE_NAN;
    }

    // Bring the value to the most accurate range (1 < x < 100)
    while (x > MM_MATH_FIX32_100)
    {
        x = __mmFix32_Div(x, MM_MATH_FIX32_E_POW_4);
        scaling += 4;
    }

    while (x < MM_MATH_FIX32_1)
    {
        x = __mmFix32_Mul(x, MM_MATH_FIX32_E_POW_4);
        scaling -= 4;
    }

    do
    {
        // Solving e(x) = y using Newton's method
        // f(x) = e(x) - y
        // f'(x) = e(x)
        e = mmFix32_Exp(guess);
        delta = __mmFix32_Div(x - e, e);

        // It's unlikely that logarithm is very large, so avoid overshooting.
        if (delta > MM_MATH_FIX32_3)
            delta = MM_MATH_FIX32_3;

        guess += delta;
    } while ((count++ < 10) && ((delta > 1) || (delta < -1)));

    return guess + mmFix32_Int2Fix(scaling);
}

/*
* Calculate x^y
*
* Description
*
*   The standard identity:
*   ln x^y = y * log x
*
*   Raising e to the power of either sides:
*   e^(ln x^y) = e^(y * log x)
*
*   Which simplifies to:
*   x^y = e^(y * ln x)
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Pow(mmFix32_t x, mmFix32_t y)
{
    if (x == MM_MATH_FIX32_1) return MM_MATH_FIX32_1;
    if (y == MM_MATH_FIX32_0) return MM_MATH_FIX32_1;
    if (x == MM_MATH_FIX32_0)
    {
        if (y < MM_MATH_FIX32_0)
        {
            // divide by zero.
            // non-positive value passed for pow y, +nan value.
            return MM_MATH_FIX32_POSITIVE_INF;
        }
        return MM_MATH_FIX32_0;
    }

    return mmFix32_Exp(__mmFix32_Mul(y, mmFix32_Log(x)));
}

/*
* Calculate the inverse for non-zero values
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Inv(mmFix32_t x)
{
    return __mmFix32_Inv(x);
}

/*
* Calculate the square-root
*
* Description
*
*   Consider a parabola centered on the y-axis:
*   y = a * x^2 + b
*
*   Has zeros (y = 0) located at:
*   a * x^2 + b = 0
*   a * x^2 = -b
*   x^2 = -b / a
*   x = +- (-b / a)^(1 / 2)
*
*   Letting a = 1 and b = -X results in:
*   y = x^2 - X
*   x = +- X^(1 / 2)
*
*   Which is a convenient result, since we want to find the square root of X,
*   and we can * use Newton's Method to find the zeros of any f(x):
*   xn = x - f(x) / f'(x)
*
*   Applying Newton's Method to our parabola:
*   f(x) = x^2 - X
*   xn = x - (x^2 - X) / (2 * x)
*   xn = x - (x - X / x) / 2
*
*   To make this converge quickly, we scale X so that:
*   X = 4^N * z
*
*   Taking the roots of both sides
*   X^(1 / 2) = (4^n * z)^(1 / 2)
*   X^(1 / 2) = 2^n * z^(1 / 2)
*
*   Letting N = 2^n results in:
*   x^(1 / 2) = N * z^(1 / 2)
*
*   We want this to converge to the positive root, so we must start at a point
*   0 < start <= x^(1 / 2)
*   or
*   x^(1/2) <= start <= infinity
*
*   Since:
*   (1/2)^(1/2) = 0.707
*   2^(1/2) = 1.414
*
*   A good choice is 1 which lies in the middle, and takes 4 iterations to
*   converge from either extreme.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_SqrtSlow(mmFix32_t x)
{
    mmFix32_t n;
    mmFix32_t xn;

    /* Quick solutions for the simple cases */
    if (x <= MM_MATH_FIX32_0 || x == MM_MATH_FIX32_1)
        return x;

    /* Start with a scaling factor of 1 */
    n = MM_MATH_FIX32_1;

    /* Scale x so that 0.5 <= x < 2 */
    while (x >= MM_MATH_FIX32_2)
    {
        x = __mmFix32_Div4(x);
        n = __mmFix32_Mul2(n);
    }
    while (x < MM_MATH_FIX32_1_DIV_002)
    {
        x = __mmFix32_Mul4(x);
        n = __mmFix32_Div2(n);
    }

    /* Simple solution if x = 4^n */
    if (x == MM_MATH_FIX32_1)
        return n;

    /* The starting point */
    xn = MM_MATH_FIX32_1;

    /* Four iterations will be enough */
    xn = __mmFix32_Sub(xn, __mmFix32_Div2(__mmFix32_Sub(xn, __mmFix32_Div(x, xn))));
    xn = __mmFix32_Sub(xn, __mmFix32_Div2(__mmFix32_Sub(xn, __mmFix32_Div(x, xn))));
    xn = __mmFix32_Sub(xn, __mmFix32_Div2(__mmFix32_Sub(xn, __mmFix32_Div(x, xn))));
    xn = __mmFix32_Sub(xn, __mmFix32_Div2(__mmFix32_Sub(xn, __mmFix32_Div(x, xn))));

    /* Scale the result */
    return __mmFix32_Mul(n, xn);
}

/*
* sqrt x fast.
* https://github.com/PetteriAimonen/libfixmath
* https://github.com/asik/FixedMath.Net
*/
MM_EXPORT_DLL mmFix32_t mmFix32_SqrtFast(mmFix32_t x)
{
    static const mmUInt8_t bits_number_div_4 = MM_MATH_FIX32_BIT_NUMBER / 2;
    static const mmUInt32_t u32_sign_mask = 0x80000000;

    mmUInt8_t neg = (x < 0);
    mmFix32U64_t num = (neg ? -x : x);
    mmFix32U64_t result = 0;
    mmFix32U64_t bit;
    mmUInt8_t n;

    if (x < 0)
    {
        // We cannot represent infinities like Single and Double, and Sqrt is
        // mathematically undefined for x < 0. So we just throw an exception.
        // non-positive value passed for sqrt, -nan value.
        return MM_MATH_FIX32_NEGATIVE_NAN;
    }

    // Many numbers will be less than 15, so
    // this gives a good balance between time spent
    // in if vs. time spent in the while loop
    // when searching for the starting value.
    //if (num & 0xFFF00000)
    //  bit = (uint32_t)1 << 30;
    //else
    //  bit = (uint32_t)1 << 18;

    // second-to-top bit
    bit = ((mmFix32U64_t)1) << (MM_MATH_FIX32_BIT_NUMBER - 2);

    while (bit > num)
    {
        bit >>= 2;
    }

    // The main part is executed twice, in order to avoid
    // using 128 bit values in computations.
    for (n = 0; n < 2; ++n)
    {
        // First we get the top 48 bits of the answer.
        while (bit)
        {
            if (num >= result + bit)
            {
                num -= result + bit;
                result = (result >> 1) + bit;
            }
            else
            {
                result = result >> 1;
            }
            bit >>= 2;
        }

        if (n == 0)
        {
            // Then process it again to get the lowest bit_number / 4 bits.
            if (num > ((mmFix32U64_t)(1) << (bits_number_div_4)) - 1)
            {
                // The remainder 'num' is too large to be shifted left
                // by 32, so we have to add 1 to result manually and
                // adjust 'num' accordingly.
                // num = a - (result + 0.5)^2
                //       = num + result^2 - (result + 0.5)^2
                //       = num - result - 0.5
                num -= result;
                num = (num << (bits_number_div_4)) - u32_sign_mask;
                result = (result << (bits_number_div_4)) + u32_sign_mask;
            }
            else
            {
                num <<= (bits_number_div_4);
                result <<= (bits_number_div_4);
            }

            bit = ((mmFix32U64_t)1) << (bits_number_div_4 - 2);
        }
    }
    // Finally, if next bit would have been 1, round the result upwards.
    if (num > result)
    {
        ++result;
    }
    return (neg ? -(mmFix32_t)result : (mmFix32_t)result);
}

MM_EXPORT_DLL mmFix32_t mmFix32_Round(mmFix32_t x)
{
    mmFix32_t part_i = __mmFix32_PartInt(x);
    mmFix32_t part_f = __mmFix32_PartFra(x);

    if (part_f < 0x80000000)
    {
        return part_i;
    }
    if (part_f > 0x80000000)
    {
        return __mmFix32_Add(part_i, MM_MATH_FIX32_1);
    }
    // if number is halfway between two values, round to the nearest even number
    // this is the method used by System.Math.Round().
    return (part_i & MM_MATH_FIX32_1) == 0 ? part_i : part_i + MM_MATH_FIX32_1;
}

/*
* fix32 to string.
* buff     1 + 10 + 1 + 15 = 27 suitable is 32.
* decimals [0, 15]
*/
MM_EXPORT_DLL void mmFix32_ToString(mmFix32_t value, char* buf, int decimals)
{
    static const mmUInt64_t __Fix32ToStringScales[16] =
    {
        /* decimals precision */
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000,
        10000000000,
        100000000000,
        1000000000000,
        10000000000000,
        100000000000000,
        1000000000000000,
    };
    mmUInt64_t intpart = 0;
    mmUInt64_t frapart = 0;
    mmUInt64_t scale = 0;

    if (value < 0)
    {
        *buf++ = '-';
    }

    /* Separate the integer and decimal parts of the value */
    intpart = mmFix32_PartInt(value) >> MM_MATH_FIX32_BIT_FRACTION;
    frapart = mmFix32_PartFra(value);
    // [0, 15]  
    scale = __Fix32ToStringScales[decimals & 0xf];
    frapart = mmFix32_Mul(frapart, scale);

    if (frapart >= scale)
    {
        /* Handle carry from decimal part */
        intpart++;
        frapart -= scale;
    }

    /* Format integer part */
    /* [+2147483648.000000, +2147483648.000000] */
    buf = __static_mmFix32_ItoaLoop(buf, 1000000000, intpart, 1);

    /* Format decimal part (if any) */
    if (scale != 1)
    {
        *buf++ = '.';
        buf = __static_mmFix32_ItoaLoop(buf, scale / 10, frapart, 0);
    }

    *buf = '\0';
}

/*
* string to fix32.
* 1234567890.123456789012345678901234567890
*
*        |         hex        |    mmFix32_ToString 16   |          double 16
* double | 0x499602d21f9adc00 | 1234567890.123456716537475 | 1234567890.1234567165374756
* fix32  | 0x499602d21f9add37 | 1234567890.123456788947805 | 1234567890.1234567165374756
*/
MM_EXPORT_DLL mmFix32_t mmFix32_FromString(const char* buf)
{
    int negative = 0;
    mmUInt64_t intpart = 0;
    int count = 0;
    mmFix32_t value = 0;

    while (isspace(*buf))
    {
        buf++;
    }

    /* Decode the sign */
    negative = (*buf == '-');
    if (*buf == '+' || *buf == '-')
    {
        buf++;
    }

    /* Decode the integer part */
    while (isdigit(*buf))
    {
        intpart *= 10;
        intpart += *buf++ - '0';
        count++;
    }

    /* [+2147483648.000000, +2147483648.000000] */
    /* coune = 10 */
    if (count == 0 || count > 10 || intpart > (mmUInt64_t)(0x7FFFFFFF) + 1 || (!negative && intpart > (mmUInt64_t)(0x7FFFFFFF)))
    {
        return negative ? MM_MATH_FIX32_NEGATIVE_INF : MM_MATH_FIX32_POSITIVE_INF;
    }

    value = intpart << MM_MATH_FIX32_BIT_FRACTION;

    /* Decode the decimal part */
    if (*buf == '.' || *buf == ',')
    {
        mmUInt64_t frapart = 0;
        mmUInt64_t scale = 1;

        buf++;

        while (isdigit(*buf) && scale < 1000000000000000)
        {
            scale *= 10;
            frapart *= 10;
            frapart += *buf++ - '0';
        }

        value += __mmFix32_Div(frapart, scale);
    }

    /* Verify that there is no garbage left over */
    while (*buf != '\0')
    {
        if (!isdigit(*buf) && !isspace(*buf))
        {
            return negative ? MM_MATH_FIX32_NEGATIVE_INF : MM_MATH_FIX32_POSITIVE_INF;
        }

        buf++;
    }

    return negative ? -value : value;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
/*
* operate "+".
*/
static mmInline mmFix32_t __static_mmFix32_Add(mmFix32_t x, mmFix32_t y)
{
    return x + y;
}

/*
* operate "-".
*/
static mmInline mmFix32_t __static_mmFix32_Sub(mmFix32_t x, mmFix32_t y)
{
    return x - y;
}

/*
* operate "*".
*/
/*
* Multiply two sll values
*
* Description
*
*   When multiplying two 64 bit sll numbers, the result is 128 bits, but there
*   is only room for a 64 bit result with sll!
*
*   The 128 bit result has 64 bits on either side of the decimal, so 32 bits
*   of overflow to the left of the decimal, and 32 bits of underflow to the
*   right of the decmial.
*
*   32.32 * 32.32 = 64.64 = overflow(32) + 32.32 + underflow(32)
*
*   However, a "long long" multiply has 64 bits of overflow to the left of the
*   decimal, resulting in the entire integer part being lost!
*
*   32.32 * 32.32 = 64.64 = overflow(64) + .64
*
*   Hence a custom multiply routine is required, to preserve the parts
*   of the result that sll needs.
*
*   Consider two sll numbers, x and y:
*
*   Let x = x_hi * 2^0 + x_lo * 2^(-32)
*   Let y = y_hi * 2^0 + y_lo * 2^(-32)
*
*   Where:
*
*   *_hi is the signed 32 bit integer part to the left of the decimal
*   *_lo is the unsigned 32 bit fractional part to the right of the decimal
*
*   x * y = (x_hi * 2^0 + x_lo * 2^(-32))
*         * (y_hi * 2^0 + y_lo * 2^(-32))
*
*   Expanding the terms, we get:
*
*   = x_hi * y_hi * 2^0 + x_hi * y_lo * 2^(-32)
*   + x_lo * y_hi * 2^(-32) + x_lo * y_lo * 2^(-64)
*
*   Grouping by powers of 2, we get:
*
*   (x_hi * y_hi) * 2^0
*   We only need the low 32 bits of this term, as the rest is overflow
*
*   (x_hi * y_lo + x_lo * y_hi) * 2^-32
*   We need all bits of this term
*
*   x_lo * y_lo * 2^-64
*   We only need the high 32 bits of this term, as the rest is underflow
*/
static mmInline mmFix32_t __static_mmFix32_Mul(mmFix32_t x, mmFix32_t y)
{
    // fix point expression real number, operate mul is "*".
    mmFix32S32_t x_hi = (mmFix32S32_t)(x >> MM_MATH_FIX32_BIT_FRACTION);    // A Discard lower bits
    mmFix32U32_t x_lo = (mmFix32U32_t)(x & MM_MATH_FIX32_L_MASK);           // B Discard upper bits

    mmFix32S32_t y_hi = (mmFix32S32_t)(y >> MM_MATH_FIX32_BIT_FRACTION);    // C Discard lower bits
    mmFix32U32_t y_lo = (mmFix32U32_t)(y & MM_MATH_FIX32_L_MASK);           // D Discard upper bits

    mmFix32U64_t ac = (mmFix32U64_t)(x_hi * y_hi) << MM_MATH_FIX32_BIT_FRACTION;// AC
    mmFix32U64_t bd = ((mmFix32U64_t)x_lo * y_lo) >> MM_MATH_FIX32_BIT_FRACTION;// BD

    mmFix32U64_t ad_cb = (mmFix32U64_t)x_hi * y_lo + (mmFix32U64_t)y_hi * x_lo;// AD_CB

    return (mmFix32_t)(ac + ad_cb + bd);
}

/*
* operate "/".
*/
static mmInline mmFix32_t __static_mmFix32_Div(mmFix32_t x, mmFix32_t y)
{
    mmFix32U64_t div = 0;
    mmFix32_t result = 0;

    mmFix32U64_t remainder = (x >= 0) ? x : (-x);
    mmFix32U64_t divider = (y >= 0) ? y : (-y);
    mmFix32U64_t quotient = 0;
    int bit_pos = MM_MATH_FIX32_BIT_POSITION;

    mmFix32U64_t uxy_s = (x ^ y) & MM_MATH_FIX32_SIGN_MASK;

    if (0 == y)
    {
        return 0 == uxy_s ? MM_MATH_FIX32_POSITIVE_INF : MM_MATH_FIX32_NEGATIVE_INF;
    }

    // u64 not have this fast step.
    // Kick-start the division a bit.
    // This improves speed in the worst-case scenarios where N and D are large
    // It gets a lower estimate for the result by N/(D >> 17 + 1).
    //if (divider & 0xFFF00000)
    //{
    //  mmFix32U64_t shifted_div = ((divider >> MM_MATH_FIX32_BIT_POSITION) + 1);
    //  quotient = remainder / shifted_div;
    //  remainder -= ((mmFix32U64_t)quotient * divider) >> MM_MATH_FIX32_BIT_POSITION;
    //}

    // If the divider is divisible by 2^n, take advantage of it.
    while (!(divider & 0xF) && bit_pos >= 4)
    {
        divider >>= 4;
        bit_pos -= 4;
    }

    while (0 != remainder && bit_pos >= 0)
    {
        // Shift remainder as much as we can without overflowing
        int shift = __mmFix32_CountLeadingZeroesU64Inline(remainder);
        if (shift > bit_pos) shift = bit_pos;
        remainder <<= shift;
        bit_pos -= shift;

        div = remainder / divider;
        remainder = remainder % divider;
        quotient += div << bit_pos;

        // detect overflow
        if (div & ~(MM_MATH_FIX32_BITS_MASK >> bit_pos))
        {
            return 0 == uxy_s ? MM_MATH_FIX32_POSITIVE_INF : MM_MATH_FIX32_NEGATIVE_INF;
        }

        remainder <<= 1;
        bit_pos--;
    }

    // Quotient is always positive so rounding is easy
    quotient++;

    result = quotient >> 1;

    // Figure out the sign of the result
    if ((x ^ y) & MM_MATH_FIX32_SIGN_MASK)
    {
        // detect overflow
        if (result == MM_MATH_FIX32_MINIMUM)
        {
            return 0 == uxy_s ? MM_MATH_FIX32_POSITIVE_INF : MM_MATH_FIX32_NEGATIVE_INF;
        }

        result = -result;
    }

    return result;
}

/*
* operate "inv".
*/
static mmInline mmFix32_t __static_mmFix32_Inv(mmFix32_t x)
{
    static const mmFix32_t fix_s32_maximum = 0x0000000000000002;// 1.0 / MM_MATH_FIX32_MAXIMUM
    static const mmFix32_t fix_s32_minifra = 0x0000000000000001;// MM_MATH_FIX32_MINIFRA

    int sgn;
    mmFix32S64_t u;
    mmFix32U64_t s;

    mmFix32_t x_abs = mmFix32_Fabs(x);
    mmFix32U64_t ux_s = x & MM_MATH_FIX32_SIGN_MASK;

    if (x_abs <= fix_s32_minifra) return 0 == (ux_s) ? MM_MATH_FIX32_POSITIVE_INF : MM_MATH_FIX32_NEGATIVE_INF;
    if (x_abs == fix_s32_maximum) return 0 == (ux_s) ? MM_MATH_FIX32_MAXIMUM : MM_MATH_FIX32_MINIMUM;

    /* Use positive numbers, or the approximation won't work */
    if (x < MM_MATH_FIX32_0)
    {
        x = __mmFix32_Neg(x);
        sgn = 1;
    }
    else if (x > MM_MATH_FIX32_0)
    {
        sgn = 0;
    }
    else
    {
        return MM_MATH_FIX32_POSITIVE_INF;
    }

    /* Starting-point (gets shifted right to become positive) */
    s = -1;

    /* An approximation - must be larger than the actual value */
    for (u = x; u; u = ((mmFix32U64_t)u) >> 1)
        s >>= 1;

    /* Newton's Method */
    u = __mmFix32_Mul(s, __mmFix32_Sub(MM_MATH_FIX32_2, __mmFix32_Mul(x, s)));
    u = __mmFix32_Mul(u, __mmFix32_Sub(MM_MATH_FIX32_2, __mmFix32_Mul(x, u)));
    u = __mmFix32_Mul(u, __mmFix32_Sub(MM_MATH_FIX32_2, __mmFix32_Mul(x, u)));
    u = __mmFix32_Mul(u, __mmFix32_Sub(MM_MATH_FIX32_2, __mmFix32_Mul(x, u)));
    u = __mmFix32_Mul(u, __mmFix32_Sub(MM_MATH_FIX32_2, __mmFix32_Mul(x, u)));
    u = __mmFix32_Mul(u, __mmFix32_Sub(MM_MATH_FIX32_2, __mmFix32_Mul(x, u)));

    return ((sgn) ? __mmFix32_Neg(u) : u);
}

MM_EXPORT_DLL mmFix32_t __mmFix32_Add(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t v = 0;
    do
    {
        if (MM_MATH_FIX32_POSITIVE_NAN == x || MM_MATH_FIX32_NEGATIVE_NAN == x)
        {
            // +nan -nan
            v = x;
            break;
        }
        if (MM_MATH_FIX32_POSITIVE_NAN == y || MM_MATH_FIX32_NEGATIVE_NAN == y)
        {
            // +nan -nan
            v = y;
            break;
        }
        if (MM_MATH_FIX32_NEGATIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // -inf + -inf = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // -inf + +inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            {
                // -inf + +  n = -inf
                // -inf + -  n = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
        }
        if (MM_MATH_FIX32_POSITIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // +inf + -inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // +inf + +inf = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            {
                // +inf + +  n = +inf
                // +inf + -  n = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
        }
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // +  n + -inf = -inf
                // -  n + -inf = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // +  n + +inf = +inf
                // -  n + +inf = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
        }
        // fix point expression real number, operate add is "+".
        v = __static_mmFix32_Add(x, y);
    } while (0);
    return v;
}

MM_EXPORT_DLL mmFix32_t __mmFix32_Sub(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t v = 0;
    do
    {
        if (MM_MATH_FIX32_POSITIVE_NAN == x || MM_MATH_FIX32_NEGATIVE_NAN == x)
        {
            // +nan -nan
            v = x;
            break;
        }
        if (MM_MATH_FIX32_POSITIVE_NAN == y || MM_MATH_FIX32_NEGATIVE_NAN == y)
        {
            // +nan -nan
            v = y;
            break;
        }
        if (MM_MATH_FIX32_NEGATIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // -inf - -inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // -inf - +inf = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            {
                // -inf - +  n = -inf
                // -inf - -  n = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
        }
        if (MM_MATH_FIX32_POSITIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // +inf - -inf = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // +inf - +inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            {
                // +inf - +  n = +inf
                // +inf - -  n = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
        }
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // +  n - -inf = +inf
                // -  n - -inf = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // +  n - +inf = -inf
                // -  n - +inf = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
        }
        // fix point expression real number, operate sub is "-".
        v = __static_mmFix32_Sub(x, y);
    } while (0);
    return v;
}

MM_EXPORT_DLL mmFix32_t __mmFix32_Mul(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t v = 0;
    do
    {
        if (MM_MATH_FIX32_POSITIVE_NAN == x || MM_MATH_FIX32_NEGATIVE_NAN == x)
        {
            // +nan -nan
            v = x;
            break;
        }
        if (MM_MATH_FIX32_POSITIVE_NAN == y || MM_MATH_FIX32_NEGATIVE_NAN == y)
        {
            // +nan -nan
            v = y;
            break;
        }
        if (MM_MATH_FIX32_NEGATIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // -inf * -inf = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // -inf * +inf = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (0 == y)
            {
                // -inf * +  0 = -nan
                // -inf * -  0 = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (0 < y)
            {
                // -inf * +  n = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (0 > y)
            {
                // -inf * -  n = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
        }
        if (MM_MATH_FIX32_POSITIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // +inf * -inf = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // +inf * +inf = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (0 == y)
            {
                // +inf * +  0 = -nan
                // +inf * -  0 = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (0 < y)
            {
                // +inf * +  n = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (0 > y)
            {
                // +inf * -  n = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
        }
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                if (0 < x)
                {
                    // +  n * -inf = -inf
                    v = MM_MATH_FIX32_NEGATIVE_INF;
                    break;
                }
                if (0 > x)
                {
                    // -  n * -inf = +inf
                    v = MM_MATH_FIX32_POSITIVE_INF;
                    break;
                }
                if (0 == x)
                {
                    // +  0 * -inf = -nan
                    // -  0 * -inf = -nan
                    v = MM_MATH_FIX32_NEGATIVE_NAN;
                    break;
                }
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                if (0 < x)
                {
                    // +  n * +inf = +inf
                    v = MM_MATH_FIX32_POSITIVE_INF;
                    break;
                }
                if (0 > x)
                {
                    // -  n * +inf = -inf
                    v = MM_MATH_FIX32_NEGATIVE_INF;
                    break;
                }
                if (0 == x)
                {
                    // +  0 * +inf = -nan
                    // -  0 * +inf = -nan
                    v = MM_MATH_FIX32_NEGATIVE_NAN;
                    break;
                }
            }
        }
        // fix point expression real number, operate mul is "*".
        v = __static_mmFix32_Mul(x, y);
    } while (0);
    return v;
}

MM_EXPORT_DLL mmFix32_t __mmFix32_Div(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t v = 0;
    do
    {
        if (MM_MATH_FIX32_POSITIVE_NAN == x || MM_MATH_FIX32_NEGATIVE_NAN == x)
        {
            // +nan -nan
            v = x;
            break;
        }
        if (MM_MATH_FIX32_POSITIVE_NAN == y || MM_MATH_FIX32_NEGATIVE_NAN == y)
        {
            // +nan -nan
            v = y;
            break;
        }
        if (MM_MATH_FIX32_NEGATIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // -inf / -inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // -inf / +inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (0 == y)
            {
                // -inf / +  0 = -inf
                // -inf / -  0 = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (0 < y)
            {
                // -inf / +  n = +inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
            if (0 > y)
            {
                // -inf / -  n = -inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
        }
        if (MM_MATH_FIX32_POSITIVE_INF == x)
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                // +inf / -inf = +nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                // +inf / +inf = -nan
                v = MM_MATH_FIX32_NEGATIVE_NAN;
                break;
            }
            if (0 == y)
            {
                // +inf / +  0 = +inf
                // +inf / -  0 = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (0 < y)
            {
                // +inf / +  n = +inf
                v = MM_MATH_FIX32_POSITIVE_INF;
                break;
            }
            if (0 > y)
            {
                // +inf / -  n = -inf
                v = MM_MATH_FIX32_NEGATIVE_INF;
                break;
            }
        }
        {
            if (MM_MATH_FIX32_NEGATIVE_INF == y)
            {
                if (0 < x)
                {
                    // +  n / -inf = -  0
                    v = 0;
                    break;
                }
                if (0 > x)
                {
                    // -  n / -inf = +  0
                    v = 0;
                    break;
                }
                if (0 == x)
                {
                    // +  0 / -inf = -  0
                    // -  0 / -inf = +  0
                    v = 0;
                    break;
                }
            }
            if (MM_MATH_FIX32_POSITIVE_INF == y)
            {
                if (0 < x)
                {
                    // +  n / +inf = +  0
                    v = 0;
                    break;
                }
                if (0 > x)
                {
                    // -  n / +inf = -  0
                    v = 0;
                    break;
                }
                if (0 == x)
                {
                    // +  0 / +inf = -  0
                    // -  0 / +inf = +  0
                    v = 0;
                    break;
                }
            }
            if (0 == y)
            {
                if (0 < x)
                {
                    // +  n / +  0 = +inf
                    v = MM_MATH_FIX32_POSITIVE_INF;
                    break;
                }
                if (0 > x)
                {
                    // +  n / -  0 = -inf
                    v = MM_MATH_FIX32_NEGATIVE_INF;
                    break;
                }
                if (0 == x)
                {
                    // -  0 / +  0 = +  0
                    // -  0 / -  0 = +  0
                    // +  0 / +  0 = +  0
                    // +  0 / -  0 = +  0
                    v = MM_MATH_FIX32_NEGATIVE_NAN;
                    break;
                }
            }
        }
        // fix point expression real number, operate div is "/".
        v = __static_mmFix32_Div(x, y);
    } while (0);
    return v;
}

MM_EXPORT_DLL mmFix32_t __mmFix32_Inv(mmFix32_t x)
{
    mmFix32_t v = 0;
    do
    {
        if (MM_MATH_FIX32_POSITIVE_NAN == x || MM_MATH_FIX32_NEGATIVE_NAN == x)
        {
            // +nan -nan
            v = x;
            break;
        }
        if (MM_MATH_FIX32_NEGATIVE_INF == x)
        {
            // +  1 / +inf = +  0
            v = 0;
            break;
        }
        if (MM_MATH_FIX32_POSITIVE_INF == x)
        {
            // +  1 / -inf = -  0
            v = 0;
            break;
        }
        if (0 == x)
        {
            // +  1 / +  0 = +inf
            // +  1 / -  0 = -inf
            v = MM_MATH_FIX32_POSITIVE_INF;
            break;
        }
        // fix point expression real number, operate div is "inv".
        v = __static_mmFix32_Inv(x);
    } while (0);
    return v;
}

/*
* Calculate cos x where -pi/4 <= x <= pi/4
*
* Description
*
*   cos x = 1 - x^2 / 2! + x^4 / 4! - ... + x^(2N) / (2N)!
*   Note that (pi/4)^12 / 12! < 2^-32 which is the smallest possible number.
*
*   cos x = t0 + t1 + t2 + t3 + t4 + t5 + t6
*
*   Consider only the factorials:
*   f0 =  0! =  1
*   f1 =  2! =  2 *  1 * f0 =   2 * f0
*   f2 =  4! =  4 *  3 * f1 =  12 * f1
*   f3 =  6! =  6 *  5 * f2 =  30 * f2
*   f4 =  8! =  8 *  7 * f3 =  56 * f3
*   f5 = 10! = 10 *  9 * f4 =  90 * f4
*   f6 = 12! = 12 * 11 * f5 = 132 * f5
*
*   Now consider each term of the series:
*   t0 = 1
*   t1 = -t0 * x^2 / f1 = -t0 * x^2 * CONST_1_2
*   t2 = -t1 * x^2 / f2 = -t1 * x^2 * CONST_1_12
*   t3 = -t2 * x^2 / f3 = -t2 * x^2 * CONST_1_30
*   t4 = -t3 * x^2 / f4 = -t3 * x^2 * CONST_1_56
*   t5 = -t4 * x^2 / f5 = -t4 * x^2 * CONST_1_90
*   t6 = -t5 * x^2 / f6 = -t5 * x^2 * CONST_1_132
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_Cos(mmFix32_t x)
{
    mmFix32_t retval;
    mmFix32_t x2;

    x2 = __mmFix32_Mul(x, x);

    retval = __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(x2, MM_MATH_FIX32_1_DIV_132));
    retval = __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_090));
    retval = __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_056));
    retval = __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_030));
    retval = __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_012));
    retval = __mmFix32_Sub(MM_MATH_FIX32_1, __mmFix32_Div2(__mmFix32_Mul(x2, retval)));

    return retval;
}

/*
* Calculate sin x where -pi/4 <= x <= pi/4
*
* Description
*
*   sin x = x - x^3 / 3! + x^5 / 5! - ... + x^(2N+1) / (2N+1)!
*   Note that (pi/4)^13 / 13! < 2^-32 which is the smallest possible number.
*
*   sin x = t0 + t1 + t2 + t3 + t4 + t5 + t6
*
*   Consider only the factorials:
*   f0 =  0! =  1
*   f1 =  3! =  3 *  2 * f0 =   6 * f0
*   f2 =  5! =  5 *  4 * f1 =  20 * f1
*   f3 =  7! =  7 *  6 * f2 =  42 * f2
*   f4 =  9! =  9 *  8 * f3 =  72 * f3
*   f5 = 11! = 11 * 10 * f4 = 110 * f4
*   f6 = 13! = 13 * 12 * f5 = 156 * f5
*
*   Now consider each term of the series:
*   t0 = 1
*   t1 = -t0 * x^2 /   6 = -t0 * x^2 * CONST_1_6
*   t2 = -t1 * x^2 /  20 = -t1 * x^2 * CONST_1_20
*   t3 = -t2 * x^2 /  42 = -t2 * x^2 * CONST_1_42
*   t4 = -t3 * x^2 /  72 = -t3 * x^2 * CONST_1_72
*   t5 = -t4 * x^2 / 110 = -t4 * x^2 * CONST_1_110
*   t6 = -t5 * x^2 / 156 = -t5 * x^2 * CONST_1_156
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_Sin(mmFix32_t x)
{
    mmFix32_t retval;
    mmFix32_t x2;

    x2 = __mmFix32_Mul(x, x);

    retval = __mmFix32_Sub(x, __mmFix32_Mul(x2, MM_MATH_FIX32_1_DIV_156));
    retval = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_110));
    retval = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_072));
    retval = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_042));
    retval = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_020));
    retval = __mmFix32_Sub(x, __mmFix32_Mul(__mmFix32_Mul(x2, retval), MM_MATH_FIX32_1_DIV_006));

    return retval;
}

/*
* Calculate e^x where -0.5 <= x <= 0.5
*
* Description:
*   e^x = x^0 / 0! + x^1 / 1! + ... + x^N / N!
*   Note that 0.5^11 / 11! < 2^-32 which is the smallest possible number.
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_Exp(mmFix32_t x)
{
    mmFix32_t retval;

    retval = MM_MATH_FIX32_1;

    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_011)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_010)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_009)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Div2n(x, 3)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_007)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_006)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_005)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Div4(x)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Mul(x, MM_MATH_FIX32_1_DIV_003)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, __mmFix32_Div2(x)));
    retval = __mmFix32_Add(MM_MATH_FIX32_1, __mmFix32_Mul(retval, x));

    return retval;
}

MM_EXPORT_DLL mmUInt8_t __mmFix32_CountLeadingZeroesU32(mmUInt32_t x)
{
    return __mmFix32_CountLeadingZeroesU32Inline(x);
}
MM_EXPORT_DLL mmUInt8_t __mmFix32_CountLeadingZeroesU64(mmUInt64_t x)
{
    return __mmFix32_CountLeadingZeroesU64Inline(x);
}

/*
* Approximates atan(x) normalized to the [-1, 1] range
* with a maximum error of 0.1620 degrees.
* normalized_atan(x)~=(b x + x^2) / (1 + 2 b x + x^2)
* where b = 0.596227
*
* In case you need more precision, there is a 3rd order rational function:
* normalized_atan(x) ~ ( c x + x^2 + x^3) / ( 1 + (c + 1) x + (c + 1) x^2 + x^3)
* where c = (1 + sqrt(17)) / 8
* which has a maximum approximation error of 0.00811 degrees
*/

/*
* float normalized_atan(float x)
* {
*   static const uint32_t sign_mask = 0x80000000;
*   static const float b = 0.596227f;
*
*   // Extract the sign bit
*   uint32_t ux_s = sign_mask & (uint32_t &)x;
*
*   // Calculate the arctangent in the first quadrant
*   float bx_a = ::fabs(b * x);
*   float num = bx_a + x * x;
*   float atan_1q = num / (1.f + bx_a + num);
*
*   // Restore the sign bit
*   uint32_t atan_2q = ux_s | (uint32_t &)atan_1q;
*   return (float &)atan_2q;
* }
*
* float normalized_atan2(float y, float x)
* {
*   static const uint32_t sign_mask = 0x80000000;
*   static const float b = 0.596227f;
*
*   // Extract the sign bits
*   uint32_t ux_s = sign_mask & (uint32_t &)x;
*   uint32_t uy_s = sign_mask & (uint32_t &)y;
*
*   // Determine the quadrant offset
*   float q = (float)((~ux_s & uy_s) >> 29 | ux_s >> 30);
*
*   // Calculate the arctangent in the first quadrant
*   float bxy_a = ::fabs(b * x * y);
*   float num = bxy_a + y * y;
*   float atan_1q = num / (x * x + bxy_a + num);
*
*   // Translate it to the proper quadrant
*   uint32_t uatan_2q = (ux_s ^ uy_s) | (uint32_t &)atan_1q;
*   return q + (float &)uatan_2q;
* }
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_NormalizedAtan(mmFix32_t x)
{
    // Extract the sign bit
    mmFix32U64_t ux_s = MM_MATH_FIX32_SIGN_MASK & (*(mmFix32U64_t*)&x);

    // Calculate the arctangent in the first quadrant
    mmFix32_t bx_a = mmFix32_Fabs(__mmFix32_Mul(MM_MATH_FIX32_2RD_B, x));
    mmFix32_t num = __mmFix32_Add(bx_a, __mmFix32_Mul(x, x));
    mmFix32_t atan_1q = __mmFix32_Div(num, (__mmFix32_Add(__mmFix32_Add(MM_MATH_FIX32_1, bx_a), num)));

    // Restore the sign bit
    if (ux_s) { atan_1q = __mmFix32_Neg(atan_1q); }
    return atan_1q;
}

/*
* Approximates atan2(y, x) normalized to the [0, 4) range
* with a maximum error of 0.1620 degrees
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_NormalizedAtan2(mmFix32_t y, mmFix32_t x)
{
    // Extract the sign bits
    mmFix32U64_t ux_s = MM_MATH_FIX32_SIGN_MASK & (*(mmFix32U64_t*)&x);
    mmFix32U64_t uy_s = MM_MATH_FIX32_SIGN_MASK & (*(mmFix32U64_t*)&y);

    // Determine the quadrant offset
    mmFix32_t q = (mmFix32_t)__mmFix32_Int2Fix((~ux_s & uy_s) >> 61 | ux_s >> 62);

    // Calculate the arctangent in the first quadrant
    mmFix32_t bxy_a = mmFix32_Fabs(__mmFix32_Mul(__mmFix32_Mul(MM_MATH_FIX32_2RD_B, x), y));
    mmFix32_t num = __mmFix32_Add(bxy_a, __mmFix32_Mul(y, y));
    mmFix32_t atan_1q = __mmFix32_Div(num, (__mmFix32_Add(__mmFix32_Add(__mmFix32_Mul(x, x), bxy_a), num)));

    // Translate it to the proper quadrant
    if (ux_s ^ uy_s) { atan_1q = __mmFix32_Neg(atan_1q); }
    return __mmFix32_Add(q, atan_1q);
}

