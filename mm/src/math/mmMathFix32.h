/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMathFix32_h__
#define __mmMathFix32_h__

#include "core/mmCore.h"
#include "core/mmTypes.h"

#include "core/mmPrefix.h"

/*
* mmFix32_t is use for quick and medium and low precision 32.32 u64 integer expression real numbers. 
* 
* common is http://github.com/aemileski/math-sll
* we not use the asm code version, we need accuracy but not speed.
* we also remove "register" keyword, it deprecated in C++11.
*
* atan atan2 is Approximates atan series, Full Quadrant Approximations for the Arctangent Function [Tips and Tricks]
* https://ieeexplore.ieee.org/document/6375931/?tp=&arnumber=6375931&url=http:%2F%2Fieeexplore.ieee.org%2Fiel5%2F79%2F6375903%2F06375931.pdf%3Farnumber%3D6375931
* math-sll has no atan2 and the atan implement is hight and slow. 
*/


/*
* Credits
*
*   Maintained, conceived, written, and fiddled with by:
*
*       Andrew E. Mileski <andrewm@isoar.ca>
*
*   Other source code contributors:
*
*       Kevin Rockel
*       Kevin Michael Woley
*       Mark Anthony Lisher
*       Nicolas Pitre
*       Anonymous
*
* License
*
*   Licensed under the terms of the MIT license:
*
* Copyright (c) 2000,2002,2006,2012,2016 Andrew E. Mileski <andrewm@isoar.ca>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to
* deal in the Software without restriction, including without limitation the
* rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
* sell copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The copyright notice, and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
* DEALINGS IN THE SOFTWARE.
*/

#if (MM_ENDIAN == MM_ENDIAN_BIGGER)
#   define BROKEN_IEEE754_DOUBLE
#   warn Assuming big - endian double
#endif

typedef mmSInt64_t mmFix32S64_t;
typedef mmUInt64_t mmFix32U64_t;
typedef mmSInt32_t mmFix32S32_t;
typedef mmUInt32_t mmFix32U32_t;

typedef mmFix32S64_t mmFix32_t;

/*
* unit vector
* 0x0000000000000001
* -nan               -inf               min                   max                +inf               +nan
* 0x8000000000000001 0x8000000000000002 0x8000000000000003 -> 0x7ffffffffffffffd 0x7ffffffffffffffe 0x7fffffffffffffff
*
* note:
*      int    +  0 -  0 is same.
*      double +0.0 -0.0 is different.
*      will lost sign for 0.
*      different result like this:
*              x  | operate |   y  | double | fix32
*           -----------------------------------------
*           -inf  |    /    | -  0 |   +inf |  -inf
*           +inf  |    /    | -  0 |   -inf |  +inf
*           +  0  |    *    | -  0 |   -  0 |  +  0
*           +  0  |    *    | -  1 |   -  0 |  +  0
*           +  0  |    /    | -inf |   -  0 |  +  0
*           +  0  |    /    | -  1 |   -  0 |  +  0
*           -  0  |    +    | -  0 |   -  0 |  +  0
*           -  0  |    -    | +  0 |   -  0 |  +  0
*           -  0  |    *    | +  0 |   -  0 |  +  0
*           -  0  |    *    | -  1 |   -  0 |  +  0
*           -  0  |    /    | +inf |   -  0 |  +  0
*           -  0  |    /    | +  1 |   -  0 |  +  0
*           +  1  |    *    | -  0 |   -  0 |  +  0
*           +  1  |    /    | -inf |   -  0 |  +  0
*           +  1  |    /    | -  0 |   -inf |  +inf
*           -  1  |    *    | +  0 |   -  0 |  +  0
*           -  1  |    /    | +inf |   -  0 |  +  0
*           -  1  |    /    | -  0 |   -inf |  +inf
*/

/*
* Constants (converted from double)
*/

static const mmFix32_t MM_MATH_FIX32_0                   = 0x0000000000000000; // 0.0
static const mmFix32_t MM_MATH_FIX32_1                   = 0x0000000100000000; // 1.0
static const mmFix32_t MM_MATH_FIX32_2                   = 0x0000000200000000; // 2.0
static const mmFix32_t MM_MATH_FIX32_3                   = 0x0000000300000000; // 3.0
static const mmFix32_t MM_MATH_FIX32_4                   = 0x0000000400000000; // 4.0

static const mmFix32_t MM_MATH_FIX32_10                  = 0x0000000a00000000; // 10.0

static const mmFix32_t MM_MATH_FIX32_100                 = 0x0000000a00000000; // 100.0

static const mmFix32_t MM_MATH_FIX32_1_DIV_002           = 0x0000000080000000; // 1.0 / 002.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_003           = 0x0000000055555555; // 1.0 / 003.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_004           = 0x0000000040000000; // 1.0 / 004.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_005           = 0x0000000033333333; // 1.0 / 005.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_006           = 0x000000002aaaaaaa; // 1.0 / 006.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_007           = 0x0000000024924924; // 1.0 / 007.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_008           = 0x0000000020000000; // 1.0 / 008.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_009           = 0x000000001c71c71c; // 1.0 / 009.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_010           = 0x0000000019999999; // 1.0 / 010.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_011           = 0x000000001745d174; // 1.0 / 011.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_012           = 0x0000000015555555; // 1.0 / 012.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_020           = 0x000000000ccccccc; // 1.0 / 020.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_030           = 0x0000000008888888; // 1.0 / 030.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_042           = 0x0000000006186186; // 1.0 / 042.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_056           = 0x0000000004924924; // 1.0 / 056.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_072           = 0x00000000038e38e3; // 1.0 / 072.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_090           = 0x0000000002d82d82; // 1.0 / 090.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_110           = 0x000000000253c825; // 1.0 / 110.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_132           = 0x0000000001f07c1f; // 1.0 / 132.0
static const mmFix32_t MM_MATH_FIX32_1_DIV_156           = 0x0000000001a41a41; // 1.0 / 156.0

static const mmFix32_t MM_MATH_FIX32_E                   = 0x00000002b7e15162; // E
static const mmFix32_t MM_MATH_FIX32_SQRTE               = 0x00000001a61298e1; // sqrt(E)
static const mmFix32_t MM_MATH_FIX32_1_DIV_E             = 0x000000005e2d58d8; // 1 / E
static const mmFix32_t MM_MATH_FIX32_1_DIV_SQRTE         = 0x000000009b4597e3; // 1 / sqrt(E)
static const mmFix32_t MM_MATH_FIX32_LOG02_E             = 0x0000000171547652; // ln(E)
static const mmFix32_t MM_MATH_FIX32_LOG10_E             = 0x000000006f2dec54; // log(E)
static const mmFix32_t MM_MATH_FIX32_LN2                 = 0x00000000b17217f7; // ln(2)
static const mmFix32_t MM_MATH_FIX32_LN10                = 0x000000024d763776; // ln(10)
static const mmFix32_t MM_MATH_FIX32_E_POW_4             = 0x0000003699205c4e; // e ^ 4

static const mmFix32_t MM_MATH_FIX32_PI                  = 0x00000003243f6a88; // PI
static const mmFix32_t MM_MATH_FIX32_SQRT2               = 0x000000016a09e667; // sqrt(2)
static const mmFix32_t MM_MATH_FIX32_PI_DIV_2            = 0x00000001921fb544; // PI / 2
static const mmFix32_t MM_MATH_FIX32_PI_DIV_4            = 0x00000000c90fdaa2; // PI / 4
static const mmFix32_t MM_MATH_FIX32_1_DIV_PI            = 0x00000000517cc1b7; // 1 / PI
static const mmFix32_t MM_MATH_FIX32_2_DIV_PI            = 0x00000000a2f9836e; // 2 / PI
static const mmFix32_t MM_MATH_FIX32_2_MUL_PI            = 0x00000006487ed511; // 2 * PI
static const mmFix32_t MM_MATH_FIX32_2_DIV_SQRTPI        = 0x0000000120dd7504; // 2 / sqrt(PI)
static const mmFix32_t MM_MATH_FIX32_1_DIV_SQRT2         = 0x00000000b504f333; // 1 / sqrt(2)

static const mmFix32_t MM_MATH_FIX32_PI_DIV_180          = 0x000000000477d1a8; // PI / 180
static const mmFix32_t MM_MATH_FIX32_180_DIV_PI          = 0x000000394bb834c7; // 180 / PI

static const mmFix32_t MM_MATH_FIX32_FACT_00             = 0x0000000100000000; // 00!
static const mmFix32_t MM_MATH_FIX32_FACT_01             = 0x0000000100000000; // 01!
static const mmFix32_t MM_MATH_FIX32_FACT_02             = 0x0000000200000000; // 02!
static const mmFix32_t MM_MATH_FIX32_FACT_03             = 0x0000000600000000; // 03!
static const mmFix32_t MM_MATH_FIX32_FACT_04             = 0x0000001800000000; // 04!
static const mmFix32_t MM_MATH_FIX32_FACT_05             = 0x0000007800000000; // 05!
static const mmFix32_t MM_MATH_FIX32_FACT_06             = 0x000002d000000000; // 06!
static const mmFix32_t MM_MATH_FIX32_FACT_07             = 0x000013b000000000; // 07!
static const mmFix32_t MM_MATH_FIX32_FACT_08             = 0x00009d8000000000; // 08!
static const mmFix32_t MM_MATH_FIX32_FACT_09             = 0x0005898000000000; // 09!
static const mmFix32_t MM_MATH_FIX32_FACT_10             = 0x00375f0000000000; // 10!
static const mmFix32_t MM_MATH_FIX32_FACT_11             = 0x0261150000000000; // 11!
static const mmFix32_t MM_MATH_FIX32_FACT_12             = 0x1c8cfc0000000000; // 12!

static const mmFix32_t MM_MATH_FIX32_2RD_B               = 0x0000000098a25529; // approximates 2rd b = 0.596227
static const mmFix32_t MM_MATH_FIX32_3RD_C               = 0x00000000a3f07b35; // approximates 3rd c = (1.0 + sqrt(17.0)) / 8.0

static const mmFix32_t MM_MATH_FIX32_SIGN_MASK           = 0x8000000000000000; // sign  mask
static const mmFix32_t MM_MATH_FIX32_BITS_MASK           = 0xffffffffffffffff; // bits  mask
static const mmFix32_t MM_MATH_FIX32_H_MASK              = 0xffffffff00000000; // hight mask
static const mmFix32_t MM_MATH_FIX32_L_MASK              = 0x00000000ffffffff; // low   mask

static const mmFix32_t MM_MATH_FIX32_MINIFRA             = 0x0000000000000001; // the minifra value of mmFix32_t, the unit vector.

static const mmFix32_t MM_MATH_FIX32_MAXIMUM             = 0x7ffffffffffffffd; // the maximum value of mmFix32_t +2147483648.000000
static const mmFix32_t MM_MATH_FIX32_MINIMUM             = 0x8000000000000003; // the minimum value of mmFix32_t -2147483648.000000

static const mmFix32_t MM_MATH_FIX32_POSITIVE_INF        = 0x7ffffffffffffffe; // +inf value of mmFix32_t
static const mmFix32_t MM_MATH_FIX32_NEGATIVE_INF        = 0x8000000000000002; // -inf value of mmFix32_t
static const mmFix32_t MM_MATH_FIX32_POSITIVE_NAN        = 0x7fffffffffffffff; // +nan value of mmFix32_t
static const mmFix32_t MM_MATH_FIX32_NEGATIVE_NAN        = 0x8000000000000001; // -nan value of mmFix32_t

static const mmFix32_t MM_MATH_FIX32_DOUBLE_POSITIVE_INF = 0x7ff0000000000000; // +inf value of double
static const mmFix32_t MM_MATH_FIX32_DOUBLE_NEGATIVE_INF = 0xfff0000000000000; // -inf value of double
static const mmFix32_t MM_MATH_FIX32_DOUBLE_POSITIVE_NAN = 0x7ff8000000000000; // +nan value of double
static const mmFix32_t MM_MATH_FIX32_DOUBLE_NEGATIVE_NAN = 0xfff8000000000000; // -nan value of double

// Macros const value.
#define MM_MATH_FIX32_BIT_NUMBER                           64                  // the bit number of mmFix32_t
#define MM_MATH_FIX32_BIT_NUMBER_SUB_1                     63                  // the bit_number - 1 of mmFix32_t
#define MM_MATH_FIX32_BIT_HALF                             32                  // the (bit_number / 2) of mmFix32_t
#define MM_MATH_FIX32_BIT_POSITION                         33                  // (bit_number / 2) + 1
#define MM_MATH_FIX32_BIT_FRACTION                         32                  // the bit fraction number of mmFix32_t

/*
* Pre define function.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Asin(mmFix32_t x);

MM_EXPORT_DLL mmFix32_t mmFix32_Inv(mmFix32_t x);

static mmInline mmFix32_t mmFix32_Exp(mmFix32_t x);

/*
* Macros
*
* Note: - Macros don't type-check!
*/

#define __mmFix32_Int2Fix(X)        ((mmFix32_t)(((mmFix32U64_t) (X)) << MM_MATH_FIX32_BIT_FRACTION))
#define __mmFix32_Fix2Int(X)        ((int) ((X) >> MM_MATH_FIX32_BIT_FRACTION))

#define __mmFix32_PartInt(X)        ((X) & MM_MATH_FIX32_H_MASK)
#define __mmFix32_PartFra(X)        ((X) & MM_MATH_FIX32_L_MASK)

#define __mmFix32_Neg(X)            (-(X))

#define __mmFix32_Mul2(X)           ((X) << 1)
#define __mmFix32_Mul4(X)           ((X) << 2)
#define __mmFix32_Mul2n(X,N)        ((X) << (N))

#define __mmFix32_Div2(X)           ((X) >> 1)
#define __mmFix32_Div4(X)           ((X) >> 2)
#define __mmFix32_Div2n(X,N)        ((X) >> (N))

MM_EXPORT_DLL mmFix32_t __mmFix32_Add(mmFix32_t x, mmFix32_t y);
MM_EXPORT_DLL mmFix32_t __mmFix32_Sub(mmFix32_t x, mmFix32_t y);
MM_EXPORT_DLL mmFix32_t __mmFix32_Mul(mmFix32_t x, mmFix32_t y);
MM_EXPORT_DLL mmFix32_t __mmFix32_Div(mmFix32_t x, mmFix32_t y);

MM_EXPORT_DLL mmFix32_t __mmFix32_Inv(mmFix32_t x);

MM_EXPORT_DLL mmFix32_t __mmFix32_Cos(mmFix32_t x);
MM_EXPORT_DLL mmFix32_t __mmFix32_Sin(mmFix32_t x);

MM_EXPORT_DLL mmFix32_t __mmFix32_Exp(mmFix32_t x);

MM_EXPORT_DLL mmUInt8_t __mmFix32_CountLeadingZeroesU32(mmUInt32_t x);
MM_EXPORT_DLL mmUInt8_t __mmFix32_CountLeadingZeroesU64(mmUInt64_t x);

/*
* Approximates atan(x) normalized to the [-1, 1] range
* with a maximum error of 0.1620 degrees.
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_NormalizedAtan(mmFix32_t x);

/*
* Approximates atan2(y, x) normalized to the [0, 4) range
* with a maximum error of 0.1620 degrees
*/
MM_EXPORT_DLL mmFix32_t __mmFix32_NormalizedAtan2(mmFix32_t y, mmFix32_t x);

/*
* Function prototypes
*/


/*
* Convert double to fix slow.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Dbl2FixSlow(double d);

/*
* Convert fix to double slow.
*/
MM_EXPORT_DLL double mmFix32_Fix2DblSlow(mmFix32_t f);

/*
* Convert double to fix fast.
* the math-sll convert is too slow.
* https://github.com/PetteriAimonen/libfixmath
* here we not rounding.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Dbl2FixFast(double d);

/*
* Convert fix to double fast.
* the math-sll convert is too slow.
* here we not rounding.
* https://github.com/PetteriAimonen/libfixmath
*/
MM_EXPORT_DLL double mmFix32_Fix2DblFast(mmFix32_t f);

/*
* Convert double to fix
*/
static mmInline mmFix32_t mmFix32_Dbl2Fix(double d)
{
    return mmFix32_Dbl2FixFast(d);
}

/*
* Convert fix to double
*/
static mmInline double mmFix32_Fix2Dbl(mmFix32_t f)
{
    return mmFix32_Fix2DblFast(f);
}

/*
* Convert integer to sll
*/
static mmInline mmFix32_t mmFix32_Int2Fix(int i)
{
    return __mmFix32_Int2Fix(i);
}

/*
* Convert sll to integer (truncates)
*/
static mmInline int mmFix32_Fix2Int(mmFix32_t s)
{
    return __mmFix32_Fix2Int(s);
}

/*
* Integer-part of fix (fractional-part set to 0)
*/
static mmInline mmFix32_t mmFix32_PartInt(mmFix32_t s)
{
    return __mmFix32_PartInt(s);
}

/*
* Fractional-part of fix (integer-part set to 0)
*/
static mmInline mmFix32_t mmFix32_PartFra(mmFix32_t s)
{
    return __mmFix32_PartFra(s);
}

/*
* Negate x
*/
static mmInline mmFix32_t mmFix32_Neg(mmFix32_t x)
{
    return __mmFix32_Neg(x);
}

/*
* Addition x + y.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Add(mmFix32_t x, mmFix32_t y);

/*
* Subtraction x - y.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Sub(mmFix32_t x, mmFix32_t y);

/*
* Multiply two sll values
*
* Description
*
*   Let a = A * 2^32 + a_h * 2^0 + a_l * 2^(-32)
*   Let b = B * 2^32 + b_h * 2^0 + b_l * 2^(-32)
*
*   Where:
*
*   *_h is the integer part
*   *_l the fractional part
*   A and B are the sign (0 for positive, -1 for negative).
*
*   a * b = (A * 2^32 + a_h * 2^0 + a_l * 2^-32)
*       * (B * 2^32 + b_h * 2^0 + b_l * 2^-32)
*
*   Expanding the terms, we get:
*
*   = A * B * 2^64 + A * b_h * 2^32 + A * b_l * 2^0
*   + a_h * B * 2^32 + a_h * b_h * 2^0 + a_h * b_l * 2^-32
*   + a_l * B * 2^0 + a_l * b_h * 2^-32 + a_l * b_l * 2^-64
*
*   Grouping by powers of 2, we get:
*
*   = A * B * 2^64
*   Meaningless overflow from sign extension - ignore
*
*   + (A * b_h + a_h * B) * 2^32
*   Overflow which we can't handle - ignore
*
*   + (A * b_l + a_h * b_h + a_l * B) * 2^0
*   We only need the low 32 bits of this term, as the rest is overflow
*
*   + (a_h * b_l + a_l * b_h) * 2^-32
*   We need all 64 bits of this term
*
*   +  a_l * b_l * 2^-64
*   We only need the high 32 bits of this term, as the rest is underflow
*
*   Note that:
*   a > 0 && b > 0: A =  0, B =  0 and the third term is a_h * b_h
*   a < 0 && b > 0: A = -1, B =  0 and the third term is a_h * b_h - b_l
*   a > 0 && b < 0: A =  0, B = -1 and the third term is a_h * b_h - a_l
*   a < 0 && b < 0: A = -1, B = -1 and the third term is a_h * b_h - a_l - b_l
*/

/*
* Multiplication x * y
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Mul(mmFix32_t x, mmFix32_t y);

/*
* Multiplication by 2
*/
static mmInline mmFix32_t mmFix32_Mul2(mmFix32_t x)
{
    return __mmFix32_Mul2(x);
}

/*
* Multiplication by 4
*/
static mmInline mmFix32_t mmFix32_Mul4(mmFix32_t x)
{
    return __mmFix32_Mul4(x);
}

/*
* Multiplication by power of 2
*/
static mmInline mmFix32_t mmFix32_Mul2n(mmFix32_t x, int n)
{
    return __mmFix32_Mul2n(x, n);
}

/*
* Division x / y.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Div(mmFix32_t x, mmFix32_t y);

/*
* Division by 2
*/
static mmInline mmFix32_t mmFix32_Div2(mmFix32_t x)
{
    return __mmFix32_Div2(x);
}

/*
* Division by 4
*/
static mmInline mmFix32_t mmFix32_Div4(mmFix32_t x)
{
    return __mmFix32_Div4(x);
}

/*
* Division by power of 2
*/
static mmInline mmFix32_t mmFix32_Div2n(mmFix32_t x, int n)
{
    return __mmFix32_Div2n(x, n);
}

/*
* Calculate cos x for any value of x, by quadrant
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Cos(mmFix32_t x);

/*
* Calculate sin x for any value of x, by quadrant
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Sin(mmFix32_t x);

/*
* Calculate tan x for any value of x, by quadrant
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Tan(mmFix32_t x);


/*
*
* Calculate acos x, where |x| <= 1
*
* Description
*
*   acos x = pi / 2 - asin x
*   acos x = pi / 2 - SUM[n=0,) C(2 * n, n) * x^(2 * n + 1) / (4^n * (2 * n + 1)), |x| <= 1
*
*   where C(n, r) = nCr = n! / (r! * (n - r)!)
*/
static mmInline mmFix32_t mmFix32_Acos(mmFix32_t x)
{
    return __mmFix32_Sub(MM_MATH_FIX32_PI_DIV_2, mmFix32_Asin(x));
}

/*
*
* Calculate asin x, where |x| <= 1
*
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Asin(mmFix32_t x);

/*
* Calculate atan x slow.
*
*/
MM_EXPORT_DLL mmFix32_t mmFix32_AtanSlow(mmFix32_t x);

/*
 * Calculate atan2 y, x slow.
 *
 */
MM_EXPORT_DLL mmFix32_t mmFix32_Atan2Slow(mmFix32_t y, mmFix32_t x);

/*
* Calculate atan x fast.
*
* Approximates atan(x) normalized to the [-pi/2, pi/2] range
* with a maximum error of 0.1620 degrees.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_AtanFast(mmFix32_t x);

/*
* Calculate atan x
*/
static mmInline mmFix32_t mmFix32_Atan(mmFix32_t x)
{
    return mmFix32_AtanFast(x);
}

/*
* Approximates atan2(y, x) normalized to the [-pi, pi) range
* with a maximum error of 0.1620 degrees
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Atan2Fast(mmFix32_t x, mmFix32_t y);

/*
 * Calculate atan2 y, x
 */
static mmInline mmFix32_t mmFix32_Atan2(mmFix32_t y, mmFix32_t x)
{
    return mmFix32_Atan2Fast(y, x);
}

/*
* Trigonometric secant
*
* Description
*
*   sec x = 1 / cos x
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Sec(mmFix32_t x)
{
    return mmFix32_Inv(mmFix32_Cos(x));
}

/*
* Trigonometric cosecant
*
* Description
*
*   csc x = 1 / sin x
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Csc(mmFix32_t x)
{
    return mmFix32_Inv(mmFix32_Sin(x));
}

/*
* Trigonometric cotangent
*
* Description
*
*   cot x = 1 / tan x
*
*   cot x = cos x / sin x
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Cot(mmFix32_t x)
{
    return __mmFix32_Div(mmFix32_Cos(x), mmFix32_Sin(x));
}

/*
* Hyperbolic cosine
*
* Description
*
*   cosh x = (e^x + e^(-x)) / 2
*
*   cosh x = 1 + x^2 / 2! + ... + x^(2 * N) / (2 * N)!
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Cosh(mmFix32_t x)
{
    return __mmFix32_Div2(__mmFix32_Add(mmFix32_Exp(x), mmFix32_Exp(__mmFix32_Neg(x))));
}

/*
* Hyperbolic sine
*
* Description
*
*   sinh x = (e^x - e^(-x)) / 2
*
*   sinh x = 1 + x^3 / 3! + ... + x^(2 * N + 1) / (2 * N + 1)!
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Sinh(mmFix32_t x)
{
    return __mmFix32_Div2(__mmFix32_Sub(mmFix32_Exp(x), mmFix32_Exp(__mmFix32_Neg(x))));
}

/*
* Hyperbolic tangent
*
* Description
*
*   tanh x = sinh x / cosh x
*
*   tanh x = (e^x - e^(-x)) / (e^x + e^(-x))
*
*   tanh x = (e^(2 * x) - 1) / (e^(2 * x) + 1)
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Tanh(mmFix32_t x)
{
    mmFix32_t e2x = mmFix32_Exp(__mmFix32_Mul2(x));
    return __mmFix32_Div(__mmFix32_Sub(e2x, MM_MATH_FIX32_1), __mmFix32_Add(e2x, MM_MATH_FIX32_1));
}

/*
* Hyperbolic secant
*
* Description
*
*   sech x = 1 / cosh x
*
*   sech x = 2 / (e^x + e^(-x))
*
*   sech x = 2 * e^x / (e^(2 * x) + 1)
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Sech(mmFix32_t x)
{
    return __mmFix32_Div(__mmFix32_Mul2(mmFix32_Exp(x)), __mmFix32_Add(mmFix32_Exp(__mmFix32_Mul2(x)), MM_MATH_FIX32_1));
}

/*
* Hyperbolic cosecant
*
* Description
*
*   csch x = = 1 / sinh x
*
*   csch x = 2 / (e^x - e^(-x))
*
*   csch x = 2 * e^x / (e^(2 * x) - 1)
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Csch(mmFix32_t x)
{
    return __mmFix32_Div(__mmFix32_Mul2(mmFix32_Exp(x)), __mmFix32_Sub(mmFix32_Exp(__mmFix32_Mul2(x)), MM_MATH_FIX32_1));
}

/*
* Hyperbolic cotangent
*
* Description
*
*   coth x =  1 / tanh x
*
*   coth x = cosh x / sinh x
*
*   coth x = (e^x + e^(-x)) / (e^x - e^(-x))
*
*   coth x = (e^(2 * x) + 1) / (e^(2 * x) - 1)
*
* An alternate algorithm, like a power series, would be more accurate.
*/
static mmInline mmFix32_t mmFix32_Coth(mmFix32_t x)
{
    mmFix32_t e2x = mmFix32_Exp(mmFix32_Mul2(x));
    return __mmFix32_Div(__mmFix32_Add(e2x, MM_MATH_FIX32_1), __mmFix32_Sub(e2x, MM_MATH_FIX32_1));
}

/*
* Calculate e^x for any value of x slow.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_ExpSlow(mmFix32_t x);

/*
* Calculate e^x for any value of x fast.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_ExpFast(mmFix32_t x);

/*
* Calculate e^x for any value of x
*/
static mmInline mmFix32_t mmFix32_Exp(mmFix32_t x)
{
    return mmFix32_ExpFast(x);
}

/*
* log2 x slow.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Log2Slow(mmFix32_t x);

/*
* log2 x fast.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Log2Fast(mmFix32_t x);

/*
* log2 x
*/
static mmInline mmFix32_t mmFix32_Log2(mmFix32_t x)
{
    return mmFix32_Log2Fast(x);
}


/*
* log x slow.
*
* Calculate natural logarithm using Netwton-Raphson method
*/
MM_EXPORT_DLL mmFix32_t mmFix32_LogSlow(mmFix32_t x);

/*
* log x fast.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_LogFast(mmFix32_t x);

/*
* log x
*/
static mmInline mmFix32_t mmFix32_Log(mmFix32_t x)
{
    return mmFix32_LogFast(x);
}

/*
* Calculate x^y
*
* Description
*
*   The standard identity:
*   ln x^y = y * log x
*
*   Raising e to the power of either sides:
*   e^(ln x^y) = e^(y * log x)
*
*   Which simplifies to:
*   x^y = e^(y * ln x)
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Pow(mmFix32_t x, mmFix32_t y);

/*
* Returns the natural logarithm of a specified number.
* Provides at least 7 decimals of accuracy.
* https://github.com/asik/FixedMath.Net
*/
static mmInline mmFix32_t mmFix32_Ln(mmFix32_t x)
{
    return __mmFix32_Mul(mmFix32_Log2(x), MM_MATH_FIX32_LN2);
}

/*
* Calculate the inverse for non-zero values
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Inv(mmFix32_t x);

/*
* Calculate the square-root
*
* Description
*
*   Consider a parabola centered on the y-axis:
*   y = a * x^2 + b
*
*   Has zeros (y = 0) located at:
*   a * x^2 + b = 0
*   a * x^2 = -b
*   x^2 = -b / a
*   x = +- (-b / a)^(1 / 2)
*
*   Letting a = 1 and b = -X results in:
*   y = x^2 - X
*   x = +- X^(1 / 2)
*
*   Which is a convenient result, since we want to find the square root of X,
*   and we can * use Newton's Method to find the zeros of any f(x):
*   xn = x - f(x) / f'(x)
*
*   Applying Newton's Method to our parabola:
*   f(x) = x^2 - X
*   xn = x - (x^2 - X) / (2 * x)
*   xn = x - (x - X / x) / 2
*
*   To make this converge quickly, we scale X so that:
*   X = 4^N * z
*
*   Taking the roots of both sides
*   X^(1 / 2) = (4^n * z)^(1 / 2)
*   X^(1 / 2) = 2^n * z^(1 / 2)
*
*   Letting N = 2^n results in:
*   x^(1 / 2) = N * z^(1 / 2)
*
*   We want this to converge to the positive root, so we must start at a point
*   0 < start <= x^(1 / 2)
*   or
*   x^(1/2) <= start <= infinity
*
*   Since:
*   (1/2)^(1/2) = 0.707
*   2^(1/2) = 1.414
*
*   A good choice is 1 which lies in the middle, and takes 4 iterations to
*   converge from either extreme.
*/
MM_EXPORT_DLL mmFix32_t mmFix32_SqrtSlow(mmFix32_t x);

/*
* sqrt x fast.
* https://github.com/PetteriAimonen/libfixmath
* https://github.com/asik/FixedMath.Net
*/
MM_EXPORT_DLL mmFix32_t mmFix32_SqrtFast(mmFix32_t x);

/*
* sqrt x.
*/
static mmInline mmFix32_t mmFix32_Sqrt(mmFix32_t x)
{
    return mmFix32_SqrtFast(x);
}

/*
* Floor
*
* Description
*
*   floor x = largest integer not larger than x
*/
static mmInline mmFix32_t mmFix32_Floor(mmFix32_t x)
{
    mmFix32_t retval = __mmFix32_PartInt(x);
    return ((retval > x) ? __mmFix32_Sub(retval, MM_MATH_FIX32_1) : retval);
}

/*
* Ceiling
*
* Description
*
*   ceil x = smallest integer not smaller than x
*/
static mmInline mmFix32_t mmFix32_Ceil(mmFix32_t x)
{
    mmFix32_t retval = __mmFix32_PartInt(x);
    return ((retval < x) ? __mmFix32_Add(retval, MM_MATH_FIX32_1) : retval);
}

/*
* Round
*/
MM_EXPORT_DLL mmFix32_t mmFix32_Round(mmFix32_t x);

/*
* fabs
*/
static mmInline mmFix32_t mmFix32_Fabs(mmFix32_t x)
{
    mmFix32_t mask = x >> MM_MATH_FIX32_BIT_NUMBER_SUB_1;
    return (x + mask) ^ mask;
}

/*
* fmod
*/
static mmInline mmFix32_t mmFix32_Fmod(mmFix32_t x, mmFix32_t y)
{
    /* Note that in C90, the sign of result of the modulo operation is
    * undefined. in C99, it's the same as the dividend (aka numerator).
    */
    return x % y;
}

/*
* min
*/
static mmInline mmFix32_t mmFix32_Min(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t d = y - x;
    // b < x : x + ( d & -1 )
    // b > x : x + ( d &  0 )
    return x + (d & (d >> MM_MATH_FIX32_BIT_NUMBER_SUB_1));
}

/*
* max
*/
static mmInline mmFix32_t mmFix32_Max(mmFix32_t x, mmFix32_t y)
{
    mmFix32_t d = y - x;
    // b < x : y - ( d & -1 )
    // b > x : y - ( d &  0 )
    return y - (d & (d >> MM_MATH_FIX32_BIT_NUMBER_SUB_1));
}

/*
* clamp
*/
static mmInline mmFix32_t mmFix32_Clamp(mmFix32_t x, mmFix32_t min_v, mmFix32_t max_v)
{
    return mmFix32_Min(mmFix32_Max(x, min_v), max_v);
}

/*
* fix32 to string.
* buff     1 + 10 + 1 + 15 = 27 suitable is 32.
* decimals [0, 15].
*/
MM_EXPORT_DLL void mmFix32_ToString(mmFix32_t value, char* buf, int decimals);

/*
* string to fix32.
* 1234567890.123456789012345678901234567890
*
*        |         hex        |     mmFix32_ToString 16    |          double 16
* double | 0x499602d21f9adc00 | 1234567890.123456716537475 | 1234567890.1234567165374756
* fix32  | 0x499602d21f9add37 | 1234567890.123456788947805 | 1234567890.1234567165374756
*/
MM_EXPORT_DLL mmFix32_t mmFix32_FromString(const char* buf);

#include "core/mmSuffix.h"

#endif//__mmMathFix32_h__
