/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVector4_h__
#define __mmVector4_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*!
 * Vec4(x, y, z, w)
 */

MM_EXPORT_DLL extern const float mmVec4Zero[4];
MM_EXPORT_DLL extern const float mmVec4Unit[4];
MM_EXPORT_DLL extern const float mmVec4Half[4];

/*!
 * @brief Assign a vec4.
 *
 * formula:  r = a
 *
 * @param[out] r             vec4 result
 * @param[in]  v             vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Assign(
    float r[4], 
    const float v[4]);

/*!
 * @brief Assign value to a vec4.
 *
 * formula:  r = vec4(v, v, v, v)
 *
 * @param[out] r             vec4 result
 * @param[in]  v             float
 */
MM_EXPORT_DLL 
void 
mmVec4AssignValue(
    float r[4], 
    float v);

/*!
 * @brief Check if two vec4 memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a             vec4 a
 * @param[in]  b             vec4 b
 * @return                   memcmp(a, b)
 */
MM_EXPORT_DLL 
int 
mmVec4Compare(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Check if two vec4 are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a             vec4 a
 * @param[in]  b             vec4 b
 * @return                   a == b
 */
MM_EXPORT_DLL 
int 
mmVec4Equals(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Make vec4.
 *
 * @param[out] v               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmVec4Make(
    float v[4], 
    float x, float y, float z, float w);

/*!
 * @brief Make vec4 from vec3 and w.
 *
 * formula:  v = vec4(a, w)
 *
 * @param[out] v               vec4
 * @param[in]  a               vec3
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmVec4MakeFromVec3(
    float v[4], 
    float a[3], float w);

/*!
 * @brief Addition vec4.
 *
 * formula:  r = a + b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Add(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Addition vec4.
 *
 * formula:  r = a + vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmVec4AddXYZW(
    float r[3], 
    const float a[3], 
    float x, float y, float z, float w);

/*!
 * @brief Addition vec4.
 *
 * formula:  r = a + vec4(k, k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec4AddK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Subtract vec4.
 *
 * formula:  r = a - b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Sub(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Subtract vec4.
 *
 * formula:  r = a - vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmVec4SubXYZW(
    float r[3], 
    const float a[3], 
    float x, float y, float z, float w);

/*!
 * @brief Subtract vec4.
 *
 * formula:  r = a - vec4(k, k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec4SubK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Multiply vec4.
 *
 * formula:  r = a * b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Mul(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Multiply vec4.
 *
 * formula:  r = a * vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmVec4MulXYZW(
    float r[3], 
    const float a[3], 
    float x, float y, float z, float w);

/*!
 * @brief Multiply vec4.
 *
 * formula:  r = a * vec4(k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec4MulK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Division vec4.
 *
 * formula:  r = a / b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Div(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Division vec4.
 *
 * formula:  r = a / vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL 
void 
mmVec4DivXYZW(
    float r[3], 
    const float a[3], 
    float x, float y, float z, float w);

/*!
 * @brief Division vec4.
 *
 * formula:  r = a / vec4(k, k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec4DivK(
    float r[3], 
    const float a[3], 
    float k);

/*!
 * @brief Negate vec4.
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Negate(
    float r[4], 
    const float a[4]);

/*!
 * @brief Length vec4.
 *
 * formula:  Length(a)
 *
 * @param[in]  a               vec4
 * @return     value           float
 */
MM_EXPORT_DLL 
float
mmVec4Length(
    const float a[4]);

/*!
 * @brief Squared length vec4.
 *
 * formula:  Length(a)^2
 *
 * @param[in]  a               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4SquaredLength(
    const float a[4]);

/*!
 * @brief Distance vec4.
 *
 * formula:  Distance(a, b)
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4Distance(
    const float a[4],
    const float b[4]);

/*!
 * @brief Squared distance vec4.
 *
 * formula:  Distance(a, b)^2
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4SquaredDistance(
    const float a[4],
    const float b[4]);

/*!
 * @brief Dot product vec4.
 *
 * formula:  Dot(a, b)
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     Dot(a, b)       float
 */
MM_EXPORT_DLL 
float 
mmVec4DotProduct(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Scale each element of the vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  v               vec4
 * @param[in]  s               s
 */
MM_EXPORT_DLL 
void 
mmVec4Scale(
    float r[4], 
    const float v[4], 
    float s);

/*!
 * @brief Multiply inner vec4.
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     MulInner(a, b)  float
 */
MM_EXPORT_DLL 
float 
mmVec4MulInner(
    const float a[4], 
    const float b[4]);

/*!
 * @brief Multiply cross vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4MulCross(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief Normalize vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  v               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Normalize(
    float r[4], 
    const float v[4]);

/*!
 * @brief Reflect vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  v               vec4
 * @param[in]  n               vec4
 */
MM_EXPORT_DLL 
void 
mmVec4Reflect(
    float r[4], 
    const float v[4], 
    const float n[4]);

/*!
 * @brief Angle between two vec4.
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     angle           float
 */
MM_EXPORT_DLL
float
mmVec4AngleBetween(
    const float a[4],
    const float b[4]);

/*!
 * @brief vector scale to length.
 *
 * Formula:
 *  v = desiredLength / length(v)
 *
 * @param[in,out] v                vector
 * @param[in]     desiredLength    desired length
 */
MM_EXPORT_DLL
void
mmVec4ScaleToLength(
    float v[4],
    float desiredLength);

/*!
 * @brief Make a linear combination of two vectors and return the result.
 *
 * Formula:
 *  r = (a * ascl) + (b * bscl)
 *
 * @param[out] r       result
 * @param[in]  a       vector a
 * @param[in]  b       vector b
 * @param[in]  ascl    scale a
 * @param[in]  bscl    scale b
 */
MM_EXPORT_DLL
void
mmVec4LinearCombine(
    float r[4],
    const float a[4],
    const float b[4],
    float ascl,
    float bscl);

/*!
 * @brief abs values of vectors
 *
 * Formula:
 *  r =  abs(a)
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 */
MM_EXPORT_DLL
void
mmVec4Abs(
    float r[4],
    const float a[4]);

/*!
 * @brief max values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec4Max(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief min values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec4Min(
    float r[4], 
    const float a[4], 
    const float b[4]);

/*!
 * @brief clamp vector's individual members between min and max values
 *
 * @param[out] r     vector
 * @param[in]  v     vector
 * @param[in]  minVal minimum value
 * @param[in]  maxVal maximum value
 */
MM_EXPORT_DLL
void
mmVec4Clamp(
    float r[4], 
    const float v[4], 
    float minVal, 
    float maxVal);

/*!
 * @brief linear interpolation between two vectors
 *
 * formula:  from + t * (to - from)
 *
 * @param[out] r    destination
 * @param[in]  from from value
 * @param[in]  to   to value
 * @param[in]  t    interpolant (amount)
 */
MM_EXPORT_DLL
void
mmVec4Lerp(
    float r[4], 
    const float from[4], 
    const float to[4], 
    float t);

/*!
 * @brief mix between two numbers
 *
 * formula:  r = x*(1-a) +y*a
 *
 * @param[out] r    destination
 * @param[in]  x  x value
 * @param[in]  y  y value
 * @param[in]  a  mix value
 */
MM_EXPORT_DLL
void
mmVec4Mix(
    float r[4],
    const float x[4],
    const float y[4],
    const float a);

/*!
 * @brief cubic bezier interpolation
 *
 * Formula:
 *  B(s) = P0*(1-s)^3 + 3*C0*s*(1-s)^2 + 3*C1*s^2*(1-s) + P1*s^3
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  c0   control point 1
 * @param[in]  c1   control point 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec4Bezier(
    float r[4], 
    float s, 
    const float p0[4], 
    const float c0[4], 
    const float c1[4], 
    const float p1[4]);

/*!
 * @brief cubic hermite interpolation
 *
 * Formula:
 *  H(s) = P0*(2*s^3 - 3*s^2 + 1) + T0*(s^3 - 2*s^2 + s)
 *            + P1*(-2*s^3 + 3*s^2) + T1*(s^3 - s^2)
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  t0   tangent 1
 * @param[in]  t1   tangent 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec4Hermite(
    float r[4], 
    float s, 
    const float p0[4], 
    const float t0[4], 
    const float t1[4], 
    const float p1[4]);

/*!
 * @brief threshold function
 *
 * @param[out] r       destination
 * @param[in]  edge    threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec4Step(
    float r[4], 
    const float edge[4], 
    const float x[4]);

/*!
 * @brief threshold function with a smooth transition
 *
 * @param[out] r       destination
 * @param[in]  edge0   low threshold
 * @param[in]  edge1   high threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec4Smoothstep(
    float r[4], 
    const float edge0[4], 
    const float edge1[4], 
    const float x[4]);

/*!
 * @brief helper to fill vec4 as [S^3, S^2, S, 1]
 *
 * @param[out] r    destination
 * @param[in]  s    parameter
 */
MM_EXPORT_DLL
void
mmVec4Cubic(
    float r[4], 
    float s);

/*!
 * @brief swizzle vector components
 *
 * you can use existin masks e.g. GLM_XXXX, GLM_WZYX
 *
 * @param[out] r    destination
 * @param[in]  v    source
 * @param[in]  mask mask
 */
MM_EXPORT_DLL
void
mmVec4Swizzle(
    float r[4], 
    const float v[4], 
    int mask);

#include "core/mmSuffix.h"

#endif//__mmVector4_h__

