/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmVector4.h"
#include "math/mmMath.h"

#include "core/mmAlloc.h"


MM_EXPORT_DLL const float mmVec4Zero[4] = { 0, 0, 0, 0, };
MM_EXPORT_DLL const float mmVec4Unit[4] = { 1, 1, 1, 1, };
MM_EXPORT_DLL const float mmVec4Half[4] = { 0.5f, 0.5f, 0.5f, 0.5f, };

/*!
 * @brief Assign a vec4.
 *
 * formula:  r = a
 *
 * @param[out] r             vec4 result
 * @param[in]  v             vec4
 */
MM_EXPORT_DLL
void
mmVec4Assign(
    float r[4],
    const float v[4])
{
    mmMemcpy(r, v, sizeof(mmVec4Zero));
}

/*!
 * @brief Assign value to a vec4.
 *
 * formula:  r = vec4(v, v, v, v)
 *
 * @param[out] r             vec4 result
 * @param[in]  v             float
 */
MM_EXPORT_DLL
void
mmVec4AssignValue(
    float r[4],
    float v)
{
    r[0] = v;
    r[1] = v;
    r[2] = v;
    r[3] = v;
}

/*!
 * @brief Check if two vec4 memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a             vec4 a
 * @param[in]  b             vec4 b
 * @return                   memcmp(a, b)
 */
MM_EXPORT_DLL
int
mmVec4Compare(
    const float a[4],
    const float b[4])
{
    return mmMemcmp(a, b, sizeof(mmVec4Zero));
}

/*!
 * @brief Check if two vec4 are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a             vec4 a
 * @param[in]  b             vec4 b
 * @return                   a == b
 */
MM_EXPORT_DLL
int
mmVec4Equals(
    const float a[4],
    const float b[4])
{
    return 
        a[0] == b[0] && 
        a[1] == b[1] && 
        a[2] == b[2] && 
        a[3] == b[3];
}

/*!
 * @brief Make vec4.
 *
 * @param[out] v               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmVec4Make(
    float v[4],
    float x, float y, float z, float w)
{
    v[0] = x;
    v[1] = y;
    v[2] = z;
    v[3] = w;
}

/*!
 * @brief Make vec4 from vec3 and w.
 *
 * formula:  v = vec4(a, w)
 *
 * @param[out] v               vec4
 * @param[in]  a               vec3
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmVec4MakeFromVec3(
    float v[4],
    float a[3], float w)
{
    v[0] = a[0];
    v[1] = a[1];
    v[2] = a[2];
    v[3] = w;
}

/*!
 * @brief Addition vec4.
 *
 * formula:  r = a + b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL
void
mmVec4Add(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[0] + b[0];
    r[1] = a[1] + b[1];
    r[2] = a[2] + b[2];
    r[3] = a[3] + b[3];
}

/*!
 * @brief Addition vec4.
 *
 * formula:  r = a + vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmVec4AddXYZW(
    float r[3],
    const float a[3],
    float x, float y, float z, float w)
{
    r[0] = a[0] + x;
    r[1] = a[1] + y;
    r[2] = a[2] + z;
    r[3] = a[3] + w;
}

/*!
 * @brief Addition vec4.
 *
 * formula:  r = a + vec4(k, k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL
void
mmVec4AddK(
    float r[3],
    const float a[3],
    float k)
{
    r[0] = a[0] + k;
    r[1] = a[1] + k;
    r[2] = a[2] + k;
    r[3] = a[3] + k;
}

/*!
 * @brief Subtract vec4.
 *
 * formula:  r = a - b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL
void
mmVec4Sub(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[0] - b[0];
    r[1] = a[1] - b[1];
    r[2] = a[2] - b[2];
    r[3] = a[3] - b[3];
}

/*!
 * @brief Subtract vec4.
 *
 * formula:  r = a - vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmVec4SubXYZW(
    float r[3],
    const float a[3],
    float x, float y, float z, float w)
{
    r[0] = a[0] - x;
    r[1] = a[1] - y;
    r[2] = a[2] - z;
    r[3] = a[3] - w;
}

/*!
 * @brief Subtract vec4.
 *
 * formula:  r = a - vec4(k, k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL
void
mmVec4SubK(
    float r[3],
    const float a[3],
    float k)
{
    r[0] = a[0] - k;
    r[1] = a[1] - k;
    r[2] = a[2] - k;
    r[3] = a[3] - k;
}

/*!
 * @brief Multiply vec4.
 *
 * formula:  r = a * b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL
void
mmVec4Mul(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[0] * b[0];
    r[1] = a[1] * b[1];
    r[2] = a[2] * b[2];
    r[3] = a[3] * b[3];
}

/*!
 * @brief Multiply vec4.
 *
 * formula:  r = a * vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmVec4MulXYZW(
    float r[3],
    const float a[3],
    float x, float y, float z, float w)
{
    r[0] = a[0] * x;
    r[1] = a[1] * y;
    r[2] = a[2] * z;
    r[3] = a[3] * w;
}

/*!
 * @brief Multiply vec4.
 *
 * formula:  r = a * vec4(k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL
void
mmVec4MulK(
    float r[3],
    const float a[3],
    float k)
{
    r[0] = a[0] * k;
    r[1] = a[1] * k;
    r[2] = a[2] * k;
    r[3] = a[3] * k;
}

/*!
 * @brief Division vec4.
 *
 * formula:  r = a / b
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL
void
mmVec4Div(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[0] / b[0];
    r[1] = a[1] / b[1];
    r[2] = a[2] / b[2];
    r[3] = a[3] / b[3];
}

/*!
 * @brief Division vec4.
 *
 * formula:  r = a / vec4(x, y, z, w)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmVec4DivXYZW(
    float r[3],
    const float a[3],
    float x, float y, float z, float w)
{
    r[0] = a[0] / x;
    r[1] = a[1] / y;
    r[2] = a[2] / z;
    r[3] = a[3] / w;
}

/*!
 * @brief Division vec4.
 *
 * formula:  r = a / vec4(k, k, k, k)
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 * @param[in]  k               float
 */
MM_EXPORT_DLL
void
mmVec4DivK(
    float r[3],
    const float a[3],
    float k)
{
    r[0] = a[0] / k;
    r[1] = a[1] / k;
    r[2] = a[2] / k;
    r[3] = a[3] / k;
}

/*!
 * @brief Negate vec4.
 *
 * @param[out] r               vec4
 * @param[in]  a               vec4
 */
MM_EXPORT_DLL
void
mmVec4Negate(
    float r[4],
    const float a[4])
{
    r[0] = -a[0];
    r[1] = -a[1];
    r[2] = -a[2];
    r[3] = -a[3];
}

/*!
 * @brief Length vec4.
 *
 * formula:  Length(a)
 *
 * @param[in]  a               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4Length(
    const float a[4])
{
    return sqrtf(mmVec4MulInner(a, a));
}

/*!
 * @brief Squared length vec4.
 *
 * formula:  Length(a)^2
 *
 * @param[in]  a               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4SquaredLength(
    const float a[4])
{
    return mmVec4MulInner(a, a);
}

/*!
 * @brief Distance vec4.
 *
 * formula:  Distance(a, b)
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4Distance(
    const float a[4],
    const float b[4])
{
    float r[4];
    mmVec4Sub(r, a, b);
    return mmVec4Length(r);
}

/*!
 * @brief Squared distance vec4.
 *
 * formula:  Distance(a, b)^2
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     value           float
 */
MM_EXPORT_DLL
float
mmVec4SquaredDistance(
    const float a[4],
    const float b[4])
{
    float r[4];
    mmVec4Sub(r, a, b);
    return mmVec4SquaredLength(r);
}

/*!
 * @brief Dot product vec4.
 *
 * formula:  Dot(a, b)
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     Dot(a, b)       float
 */
MM_EXPORT_DLL
float
mmVec4DotProduct(
    const float a[4],
    const float b[4])
{
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
}

/*!
 * @brief Scale each element of the vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  v               vec4
 * @param[in]  s               s
 */
MM_EXPORT_DLL
void
mmVec4Scale(
    float r[4],
    const float v[4],
    float s)
{
    r[0] = v[0] * s;
    r[1] = v[1] * s;
    r[2] = v[2] * s;
    r[3] = v[3] * s;
}

/*!
 * @brief Multiply inner vec4.
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     MulInner(a, b)  float
 */
MM_EXPORT_DLL
float
mmVec4MulInner(
    const float a[4],
    const float b[4])
{
    return b[0] * a[0] + b[1] * a[1] + b[2] * a[2] + b[3] * a[3];
}

/*!
 * @brief Multiply cross vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 */
MM_EXPORT_DLL
void
mmVec4MulCross(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[1] * b[2] - a[2] * b[1];
    r[1] = a[2] * b[0] - a[0] * b[2];
    r[2] = a[0] * b[1] - a[1] * b[0];
    r[3] = 1.0f;
}

/*!
 * @brief Normalize vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  v               vec4
 */
MM_EXPORT_DLL
void
mmVec4Normalize(
    float r[4],
    const float v[4])
{
    // Zero = Normalize(Zero), but for the performance, we return NAN.
    // https://github.com/g-truc/glm is return NAN.
    // https://github.com/recp/cglm  is return Zero.

    float k = 1.f / mmVec4Length(v);
    mmVec4Scale(r, v, k);
}

/*!
 * @brief Reflect vec4.
 *
 * @param[out] r               vec4 result
 * @param[in]  v               vec4
 * @param[in]  n               vec4
 */
MM_EXPORT_DLL
void
mmVec4Reflect(
    float r[4],
    const float v[4],
    const float n[4])
{
    float p = 2.f * mmVec4MulInner(v, n);
    r[0] = v[0] - p * n[0];
    r[1] = v[1] - p * n[1];
    r[2] = v[2] - p * n[2];
    r[3] = v[3] - p * n[3];
}

/*!
 * @brief Angle between two vec4.
 *
 * @param[in]  a               vec4
 * @param[in]  b               vec4
 * @return     angle           float
 */
MM_EXPORT_DLL
float
mmVec4AngleBetween(
    const float a[4],
    const float b[4])
{
    float f = 0.0f;
    float lenProduct = mmVec4Length(a) * mmVec4Length(b);

    // Divide by zero check
    if (lenProduct < 1e-6f)
    {
        lenProduct = 1e-6f;
    }

    f = mmVec4DotProduct(a, b) / lenProduct;

    f = mmMathClamp(f, -1.0f, 1.0f);
    return acosf(f);
}

/*!
 * @brief vector scale to length.
 *
 * Formula:
 *  v = desiredLength / length(v)
 *
 * @param[in,out] v                vector
 * @param[in]     desiredLength    desired length
 */
MM_EXPORT_DLL
void
mmVec4ScaleToLength(
    float v[4],
    float desiredLength)
{
    float len = mmVec4Length(v);
    if (len != 0)
    {
        float l = desiredLength / len;
        v[0] *= l;
        v[1] *= l;
        v[2] *= l;
        v[3] *= l;
    }
}

/*!
 * @brief Make a linear combination of two vectors and return the result.
 *
 * Formula:
 *  r = (a * ascl) + (b * bscl)
 *
 * @param[out] r       result
 * @param[in]  a       vector a
 * @param[in]  b       vector b
 * @param[in]  ascl    scale a
 * @param[in]  bscl    scale b
 */
MM_EXPORT_DLL
void
mmVec4LinearCombine(
    float r[4],
    const float a[4],
    const float b[4],
    float ascl,
    float bscl)
{
    r[0] = (ascl * a[0]) + (bscl * b[0]);
    r[1] = (ascl * a[1]) + (bscl * b[1]);
    r[2] = (ascl * a[2]) + (bscl * b[2]);
    r[3] = (ascl * a[3]) + (bscl * b[3]);
}

/*!
 * @brief abs values of vectors
 *
 * Formula:
 *  r =  abs(a)
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 */
MM_EXPORT_DLL
void
mmVec4Abs(
    float r[4],
    const float a[4])
{
    r[0] = fabsf(a[0]);
    r[1] = fabsf(a[1]);
    r[2] = fabsf(a[2]);
    r[3] = fabsf(a[3]);
}

/*!
 * @brief max values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec4Max(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = mmMathMax(a[0], b[0]);
    r[1] = mmMathMax(a[1], b[1]);
    r[2] = mmMathMax(a[2], b[2]);
    r[3] = mmMathMax(a[3], b[3]);
}

/*!
 * @brief min values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec4Min(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = mmMathMin(a[0], b[0]);
    r[1] = mmMathMin(a[1], b[1]);
    r[2] = mmMathMin(a[2], b[2]);
    r[3] = mmMathMin(a[3], b[3]);
}

/*!
 * @brief clamp vector's individual members between min and max values
 *
 * @param[out] r     vector
 * @param[in]  v     vector
 * @param[in]  minVal minimum value
 * @param[in]  maxVal maximum value
 */
MM_EXPORT_DLL
void
mmVec4Clamp(
    float r[4],
    const float v[4],
    float minVal,
    float maxVal)
{
    r[0] = mmMathClamp(v[0], minVal, maxVal);
    r[1] = mmMathClamp(v[1], minVal, maxVal);
    r[2] = mmMathClamp(v[2], minVal, maxVal);
    r[3] = mmMathClamp(v[3], minVal, maxVal);
}

/*!
 * @brief linear interpolation between two vectors
 *
 * formula:  from + t * (to - from)
 *
 * @param[out] r    destination
 * @param[in]  from from value
 * @param[in]  to   to value
 * @param[in]  t    interpolant (amount)
 */
MM_EXPORT_DLL
void
mmVec4Lerp(
    float r[4],
    const float from[4],
    const float to[4],
    float t)
{
    float s[4];
    float v[4];

    /* from + s * (to - from) */
    mmVec4AssignValue(s, t);
    mmVec4Sub(v, to, from);
    mmVec4Mul(v, s, v);
    mmVec4Add(r, from, v);
}

/*!
 * @brief mix between two numbers
 *
 * formula:  r = x*(1-a) +y*a
 *
 * @param[out] r    destination
 * @param[in]  x  x value
 * @param[in]  y  y value
 * @param[in]  a  mix value
 */
MM_EXPORT_DLL
void
mmVec4Mix(
    float r[4],
    const float x[4],
    const float y[4],
    const float a)
{
    r[0] = mmMathMix(x[0], y[0], a);
    r[1] = mmMathMix(x[1], y[1], a);
    r[2] = mmMathMix(x[2], y[2], a);
    r[3] = mmMathMix(x[3], y[3], a);
}

/*!
 * @brief cubic bezier interpolation
 *
 * Formula:
 *  B(s) = P0*(1-s)^3 + 3*C0*s*(1-s)^2 + 3*C1*s^2*(1-s) + P1*s^3
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  c0   control point 1
 * @param[in]  c1   control point 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec4Bezier(
    float r[4],
    float s,
    const float p0[4],
    const float c0[4],
    const float c1[4],
    const float p1[4])
{
    r[0] = mmMathBezier(s, p0[0], c0[0], c1[0], p1[0]);
    r[1] = mmMathBezier(s, p0[1], c0[1], c1[1], p1[1]);
    r[2] = mmMathBezier(s, p0[2], c0[2], c1[2], p1[2]);
    r[3] = mmMathBezier(s, p0[3], c0[3], c1[3], p1[3]);
}

/*!
 * @brief cubic hermite interpolation
 *
 * Formula:
 *  H(s) = P0*(2*s^3 - 3*s^2 + 1) + T0*(s^3 - 2*s^2 + s)
 *            + P1*(-2*s^3 + 3*s^2) + T1*(s^3 - s^2)
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  t0   tangent 1
 * @param[in]  t1   tangent 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec4Hermite(
    float r[4],
    float s,
    const float p0[4],
    const float t0[4],
    const float t1[4],
    const float p1[4])
{
    r[0] = mmMathHermite(s, p0[0], t0[0], t1[0], p1[0]);
    r[1] = mmMathHermite(s, p0[1], t0[1], t1[1], p1[1]);
    r[2] = mmMathHermite(s, p0[2], t0[2], t1[2], p1[2]);
    r[3] = mmMathHermite(s, p0[3], t0[3], t1[2], p1[3]);
}

/*!
 * @brief threshold function
 *
 * @param[out] r       destination
 * @param[in]  edge    threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec4Step(
    float r[4],
    const float edge[4],
    const float x[4])
{
    r[0] = mmMathStep(edge[0], x[0]);
    r[1] = mmMathStep(edge[1], x[1]);
    r[2] = mmMathStep(edge[2], x[2]);
    r[3] = mmMathStep(edge[3], x[3]);
}

/*!
 * @brief threshold function with a smooth transition
 *
 * @param[out] r       destination
 * @param[in]  edge0   low threshold
 * @param[in]  edge1   high threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec4Smoothstep(
    float r[4],
    const float edge0[4],
    const float edge1[4],
    const float x[4])
{
    r[0] = mmMathSmoothstep(edge0[0], edge1[0], x[0]);
    r[1] = mmMathSmoothstep(edge0[1], edge1[1], x[1]);
    r[2] = mmMathSmoothstep(edge0[2], edge1[2], x[2]);
    r[3] = mmMathSmoothstep(edge0[3], edge1[3], x[3]);
}

/*!
 * @brief helper to fill vec4 as [S^3, S^2, S, 1]
 *
 * @param[out] r    destination
 * @param[in]  s    parameter
 */
MM_EXPORT_DLL
void
mmVec4Cubic(
    float r[4],
    float s)
{
    float ss;

    ss = s * s;

    r[0] = ss * s;
    r[1] = ss;
    r[2] = s;
    r[3] = 1.0f;
}

/*!
 * @brief swizzle vector components
 *
 * you can use existin masks e.g. GLM_XXXX, GLM_WZYX
 *
 * @param[out] r    destination
 * @param[in]  v    source
 * @param[in]  mask mask
 */
MM_EXPORT_DLL
void
mmVec4Swizzle(
    float r[4],
    const float v[4],
    int mask)
{
    r[0] = v[(mask & (3 << 0))];
    r[1] = v[(mask & (3 << 2)) >> 2];
    r[2] = v[(mask & (3 << 4)) >> 4];
    r[3] = v[(mask & (3 << 6)) >> 6];
}
