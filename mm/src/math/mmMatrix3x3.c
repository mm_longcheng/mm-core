/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMatrix3x3.h"

#include "math/mmMath.h"
#include "math/mmVector3.h"
#include "math/mmQuaternion.h"

#include "core/mmAlloc.h"


MM_EXPORT_DLL const float mmMat3x3Identity[3][3] = 
{
    { 1, 0, 0, },
    { 0, 1, 0, },
    { 0, 0, 1, },
};

MM_EXPORT_DLL const float mmMat3x3Zero[3][3] = 
{
    { 0, 0, 0, },
    { 0, 0, 0, },
    { 0, 0, 0, },
};

/*!
 * @brief Init a Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3Init(
    float m[3][3])
{
    mmMemset(m, 0, sizeof(mmMat3x3Identity));
}

/*!
 * @brief Destroy a Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3Destroy(
    float m[3][3])
{
    mmMemset(m, 0, sizeof(mmMat3x3Identity));
}

/*!
 * @brief Assign a Matrix3x3.
 *
 * formula:  m = a
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3Assign(
    float m[3][3],
    const float a[3][3])
{
    mmMemcpy(m, a, sizeof(mmMat3x3Identity));
}

/*!
 * @brief Check if two matrices memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 * @return                 memcmp(a, b)
 */
MM_EXPORT_DLL
int
mmMat3x3Compare(
    const float a[3][3],
    const float b[3][3])
{
    return mmMemcmp(a, b, sizeof(mmMat3x3Identity));
}

/*!
 * @brief Check if two matrices are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 * @return                 a == b
 */
MM_EXPORT_DLL
int
mmMat3x3Equals(
    const float a[3][3],
    const float b[3][3])
{
    return (mmVec3Equals(a[0], b[0]) &&
            mmVec3Equals(a[1], b[1]) &&
            mmVec3Equals(a[2], b[2]));
}

/*!
 * @brief Make a Matrix3x3 from value.
 *
 * @param[out] m           mat3x3 result
 */
MM_EXPORT_DLL
void
mmMat3x3Make(
    float m[3][3],
    float m00, float m01, float m02,
    float m10, float m11, float m12,
    float m20, float m21, float m22)
{
    m[0][0] = m00; m[1][0] = m01; m[2][0] = m02;
    m[0][1] = m10; m[1][1] = m11; m[2][1] = m12;
    m[0][2] = m20; m[1][2] = m21; m[2][2] = m22;
}

/*!
 * @brief Make a identity Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3MakeIdentity(
    float m[3][3])
{
    m[0][0] = 1; m[1][0] = 0; m[2][0] = 0;
    m[0][1] = 0; m[1][1] = 1; m[2][1] = 0;
    m[0][2] = 0; m[1][2] = 0; m[2][2] = 1;
}

/*!
 * @brief Make a zero Matrix3x3.
 *
 * @param[in,out] m        mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3MakeZero(
    float m[3][3])
{
    mmMemset(m, 0, sizeof(mmMat3x3Identity));
}

/*!
 * @brief Make a mat3x3 by matrix addition matrix.
 *
 * formula:  m = a + b
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 */
MM_EXPORT_DLL
void
mmMat3x3Add(
    float m[3][3],
    const float a[3][3],
    const float b[3][3])
{
    mmVec3Add(m[0], a[0], b[0]);
    mmVec3Add(m[1], a[1], b[1]);
    mmVec3Add(m[2], a[2], b[2]);
}

/*!
 * @brief Make a mat3x3 by matrix subtraction matrix.
 *
 * formula:  m = a - b
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 */
MM_EXPORT_DLL
void
mmMat3x3Sub(
    float m[3][3],
    const float a[3][3],
    const float b[3][3])
{
    mmVec3Sub(m[0], a[0], b[0]);
    mmVec3Sub(m[1], a[1], b[1]);
    mmVec3Sub(m[2], a[2], b[2]);
}

/*!
 * @brief Scale each element of the matrix.
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  k           k
 */
MM_EXPORT_DLL
void
mmMat3x3Scale(
    float m[3][3],
    const float a[3][3],
    float k)
{
    mmVec3Scale(m[0], a[0], k);
    mmVec3Scale(m[1], a[1], k);
    mmVec3Scale(m[2], a[2], k);
    mmVec3Scale(m[3], a[3], k);
}

/*!
 * @brief Make a mat3x3 by matrix multiply matrix.
 *
 * formula:  m = a * b
 *
 * @param[out] m           mat3x3 result
 * @param[in]  a           mat3x3 a
 * @param[in]  b           mat3x3 b
 */
MM_EXPORT_DLL
void
mmMat3x3Mul(
    float m[3][3],
    const float a[3][3],
    const float b[3][3])
{
    float a00 = a[0][0], a01 = a[0][1], a02 = a[0][2];
    float a10 = a[1][0], a11 = a[1][1], a12 = a[1][2];
    float a20 = a[2][0], a21 = a[2][1], a22 = a[2][2];

    float b00 = b[0][0], b01 = b[0][1], b02 = b[0][2];
    float b10 = b[1][0], b11 = b[1][1], b12 = b[1][2];
    float b20 = b[2][0], b21 = b[2][1], b22 = b[2][2];

    m[0][0] = a00 * b00 + a10 * b01 + a20 * b02;
    m[0][1] = a01 * b00 + a11 * b01 + a21 * b02;
    m[0][2] = a02 * b00 + a12 * b01 + a22 * b02;
    m[1][0] = a00 * b10 + a10 * b11 + a20 * b12;
    m[1][1] = a01 * b10 + a11 * b11 + a21 * b12;
    m[1][2] = a02 * b10 + a12 * b11 + a22 * b12;
    m[2][0] = a00 * b20 + a10 * b21 + a20 * b22;
    m[2][1] = a01 * b20 + a11 * b21 + a21 * b22;
    m[2][2] = a02 * b20 + a12 * b21 + a22 * b22;
}

/*!
 * @brief Make a vec3 by vec3 right mul mat3x3.
 *
 * formula:  r = m * v
 *
 * @param[out] r           vec3 result
 * @param[in]  m           mat3x3
 * @param[in]  v           vec3 value.
 */
MM_EXPORT_DLL
void
mmMat3x3MulVec3(
    float r[3],
    const float m[3][3],
    const float v[3])
{
    r[0] = m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2];
    r[1] = m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2];
    r[2] = m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2];
}

/*!
 * @brief Make Matrix3x3 form translate.
 *
 * formula:  m = FromTranslate(t)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  t           translate(x, y)
 */
MM_EXPORT_DLL
void
mmMat3x3FromTranslate(
    float m[3][3],
    const float t[2])
{
    m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f;
    m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f;
    m[2][0] = t[0]; m[2][1] = t[1]; m[2][2] = 1.0f;
}

/*!
 * @brief Make Matrix3x3 form quaternion.
 *
 * formula:  m = FromQuat(q)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL
void
mmMat3x3FromQuat(
    float m[3][3],
    const float q[4])
{
    // https://github.com/g-truc/glm
    // quaternion::mat3_cast api
    float x = q[0];
    float y = q[1];
    float z = q[2];
    float w = q[3];

    float qxx = x * x;
    float qyy = y * y;
    float qzz = z * z;
    float qxz = x * z;
    float qxy = x * y;
    float qyz = y * z;
    float qwx = w * x;
    float qwy = w * y;
    float qwz = w * z;

    m[0][0] = 1.0f - 2.0f * (qyy + qzz);
    m[0][1] =        2.0f * (qxy + qwz);
    m[0][2] =        2.0f * (qxz - qwy);

    m[1][0] =        2.0f * (qxy - qwz);
    m[1][1] = 1.0f - 2.0f * (qxx + qzz);
    m[1][2] =        2.0f * (qyz + qwx);

    m[2][0] =        2.0f * (qxz + qwy);
    m[2][1] =        2.0f * (qyz - qwx);
    m[2][2] = 1.0f - 2.0f * (qxx + qyy);
}

/*!
 * @brief Make Matrix3x3 form scale.
 *
 * formula:  m = FromScale(s)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  s           scale(x, y, z)
 */
MM_EXPORT_DLL
void
mmMat3x3FromScale(
    float m[3][3],
    const float s[3])
{
    m[0][0] = s[0]; m[0][1] = 0.0f; m[0][2] = 0.0f;
    m[1][0] = 0.0f; m[1][1] = s[1]; m[1][2] = 0.0f;
    m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = s[2];
}

/*!
 * @brief Transpose a Matrix3x3.
 *
 * formula:  r = Transpose(m)
 *
 * @param[out] r           mat3x3 result
 * @param[in]  m           mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3Transpose(
    float r[3][3],
    const float m[3][3])
{
    // r can be m.
    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

    r[0][0] = m00; r[1][0] = m01; r[2][0] = m02;
    r[0][1] = m10; r[1][1] = m11; r[2][1] = m12;
    r[0][2] = m20; r[1][2] = m21; r[2][2] = m22;
}

/*!
 * @brief Inverse Matrix3x3.
 *
 * formula:  r = Inverse(m)
 *
 * @param[out] r           mat3x3 result
 * @param[in]  m           mat3x3
 */
MM_EXPORT_DLL
void
mmMat3x3Inverse(
    float r[3][3],
    const float m[3][3])
{
    float idet;

    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

    r[0][0] = +(m11 * m22 - m12 * m21);
    r[0][1] = -(m01 * m22 - m21 * m02);
    r[0][2] = +(m01 * m12 - m11 * m02);
    r[1][0] = -(m10 * m22 - m20 * m12);
    r[1][1] = +(m00 * m22 - m02 * m20);
    r[1][2] = -(m00 * m12 - m10 * m02);
    r[2][0] = +(m10 * m21 - m20 * m11);
    r[2][1] = -(m00 * m21 - m20 * m01);
    r[2][2] = +(m00 * m11 - m01 * m10);

    idet = 1.0f / (m00 * r[0][0] + m01 * r[1][0] + m02 * r[2][0]);

    mmMat3x3Scale(r, r, idet);
}

/*!
 * @brief Matrix3x3 QDU decomposition.
 *
 * @param[in]  m           mat3x3
 * @param[out] kQ          Q
 * @param[out] kD          D
 * @param[out] kU          U
 */
MM_EXPORT_DLL
void
mmMat3x3QDUDecomposition(
    const float m[3][3],
    float kQ[3][3],
    float kD[3],
    float kU[3])
{
    // https://github.com/OGRECave/ogre-next Matrix3 QDUDecomposition
    // 
    // Factor M = QR = QDU where Q is orthogonal, D is diagonal,
    // and U is upper triangular with ones on its diagonal.  Algorithm uses
    // Gram-Schmidt orthogonalization (the QR algorithm).
    //
    // If M = [ m0 | m1 | m2 ] and Q = [ q0 | q1 | q2 ], then
    //
    //   q0 = m0/|m0|
    //   q1 = (m1-(q0*m1)q0)/|m1-(q0*m1)q0|
    //   q2 = (m2-(q0*m2)q0-(q1*m2)q1)/|m2-(q0*m2)q0-(q1*m2)q1|
    //
    // where |V| indicates length of vector V and A*B indicates dot
    // product of vectors A and B.  The matrix R has entries
    //
    //   r00 = q0*m0  r01 = q0*m1  r02 = q0*m2
    //   r10 = 0      r11 = q1*m1  r12 = q1*m2
    //   r20 = 0      r21 = 0      r22 = q2*m2
    //
    // so D = diag(r00,r11,r22) and U has entries u01 = r01/r00,
    // u02 = r02/r00, and u12 = r12/r11.

    // Q = rotation
    // D = scaling
    // U = shear

    // D stores the three diagonal entries r00, r11, r22
    // U stores the entries U[0] = u01, U[1] = u02, U[2] = u12

    // build orthogonal matrix Q
    float fInvLength;
    float fDot;
    float fDet;
    float fInvD0;
    float kR[3][3];

    float m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];
    float m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
    float m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

    fInvLength = mmMathInvSqrt(m00 * m00 + m01 * m01 + m02 * m02);

    kQ[0][0] = m00 * fInvLength;
    kQ[0][1] = m01 * fInvLength;
    kQ[0][2] = m02 * fInvLength;

    fDot = kQ[0][0] * m10 + kQ[0][1] * m11 + kQ[0][2] * m12;
    kQ[1][0] = m10 - fDot * kQ[0][0];
    kQ[1][1] = m11 - fDot * kQ[0][1];
    kQ[1][2] = m12 - fDot * kQ[0][2];
    fInvLength = mmMathInvSqrt(kQ[1][0] * kQ[1][0] + kQ[1][1] * kQ[1][1] + kQ[1][2] * kQ[1][2]);

    kQ[1][0] *= fInvLength;
    kQ[1][1] *= fInvLength;
    kQ[1][2] *= fInvLength;

    fDot = kQ[0][0] * m20 + kQ[0][1] * m21 + kQ[0][2] * m22;
    kQ[2][0] = m20 - fDot * kQ[0][0];
    kQ[2][1] = m21 - fDot * kQ[0][1];
    kQ[2][2] = m22 - fDot * kQ[0][2];
    fDot = kQ[1][0] * m20 + kQ[1][1] * m21 + kQ[1][2] * m22;
    kQ[2][0] -= fDot * kQ[1][0];
    kQ[2][1] -= fDot * kQ[1][1];
    kQ[2][2] -= fDot * kQ[1][2];
    fInvLength = mmMathInvSqrt(kQ[2][0] * kQ[2][0] + kQ[2][1] * kQ[2][1] + kQ[2][2] * kQ[2][2]);

    kQ[2][0] *= fInvLength;
    kQ[2][1] *= fInvLength;
    kQ[2][2] *= fInvLength;

    // guarantee that orthogonal matrix has determinant 1 (no reflections)
    fDet = 0.0f;
    fDet += kQ[0][0] * kQ[1][1] * kQ[2][2] + kQ[1][0] * kQ[2][1] * kQ[0][2];
    fDet += kQ[2][0] * kQ[0][1] * kQ[1][2] - kQ[2][0] * kQ[1][1] * kQ[0][2];
    fDet -= kQ[1][0] * kQ[0][1] * kQ[2][2] - kQ[0][0] * kQ[2][1] * kQ[1][2];

    if (fDet < 0.0f)
    {
        mmVec3Scale(kQ[0], kQ[0], -1);
        mmVec3Scale(kQ[1], kQ[1], -1);
        mmVec3Scale(kQ[2], kQ[2], -1);
    }

    // build "right" matrix R
    kR[0][0] = kQ[0][0] * m00 + kQ[0][1] * m01 + kQ[0][2] * m02;
    kR[1][0] = kQ[0][0] * m10 + kQ[0][1] * m11 + kQ[0][2] * m12;
    kR[1][1] = kQ[1][0] * m10 + kQ[1][1] * m11 + kQ[1][2] * m12;
    kR[2][0] = kQ[0][0] * m20 + kQ[0][1] * m21 + kQ[0][2] * m22;
    kR[2][1] = kQ[1][0] * m20 + kQ[1][1] * m21 + kQ[1][2] * m22;
    kR[2][2] = kQ[2][0] * m20 + kQ[2][1] * m21 + kQ[2][2] * m22;

    // the scaling component
    kD[0] = kR[0][0];
    kD[1] = kR[1][1];
    kD[2] = kR[2][2];

    // the shear component
    fInvD0 = 1.0f / kD[0];
    kU[0] = kR[1][0] * fInvD0;
    kU[1] = kR[2][0] * fInvD0;
    kU[2] = kR[2][1] / kD[1];
}

/*!
 * @brief Matrix3x3 decomposition.
 *
 * @param[in]  m           mat3x3
 * @param[out] s           scale(x, y, z)
 * @param[out] q           quaternion(x, y, z, w)
 */
MM_EXPORT_DLL
void
mmMat3x3Decomposition(
    const float m[3][3],
    float s[3],
    float q[4])
{
    float vecU[3];
    float matQ[3][3];

    mmMat3x3QDUDecomposition(m, matQ, s, vecU);
    mmQuatFromMat3x3(q, matQ);
}

/*!
 * @brief Make a Matrix3x3 by x-axis.
 *
 * formula:  m = FromEulerAngleX(angle)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  angle       angle by x-axis
 */
MM_EXPORT_DLL
void
mmMat3x3FromEulerAngleX(
    float m[3][3],
    float angle)
{
    float s = sinf(angle);
    float c = cosf(angle);

    m[0][0] = 1.f; m[0][1] = 0.f; m[0][2] = 0.f;
    m[1][0] = 0.f; m[1][1] =   c; m[1][2] =   s;
    m[2][0] = 0.f; m[2][1] =  -s; m[2][2] =   c;
}

/*!
 * @brief Make a Matrix3x3 by y-axis.
 *
 * formula:  m = FromEulerAngleY(angle)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  angle       angle by y-axis
 */
MM_EXPORT_DLL
void
mmMat3x3FromEulerAngleY(
    float m[3][3],
    float angle)
{
    float s = sinf(angle);
    float c = cosf(angle);

    m[0][0] =   c; m[0][1] = 0.f; m[0][2] =  -s;
    m[1][0] = 0.f; m[1][1] = 1.f; m[1][2] = 0.f;
    m[2][0] =   s; m[2][1] = 0.f; m[2][2] =   c;
}

/*!
 * @brief Make a Matrix3x3 by z-axis.
 *
 * formula:  m = FromEulerAngleZ(angle)
 *
 * @param[out] m           mat3x3 result
 * @param[in]  angle       angle by z-axis
 */
MM_EXPORT_DLL
void
mmMat3x3FromEulerAngleZ(
    float m[3][3],
    float angle)
{
    float s = sinf(angle);
    float c = cosf(angle);

    m[0][0] =   c; m[0][1] =   s; m[0][2] = 0.f;
    m[1][0] =  -s; m[1][1] =   c; m[1][2] = 0.f;
    m[2][0] = 0.f; m[2][1] = 0.f; m[2][2] = 1.f;
}
