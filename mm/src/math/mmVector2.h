/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmVector2_h__
#define __mmVector2_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/*!
 * Vec2(x, y)
 */

MM_EXPORT_DLL extern const float mmVec2Zero[2];
MM_EXPORT_DLL extern const float mmVec2Unit[2];
MM_EXPORT_DLL extern const float mmVec2PositiveUnitX[2];
MM_EXPORT_DLL extern const float mmVec2PositiveUnitY[2];
MM_EXPORT_DLL extern const float mmVec2NegativeUnitX[2];
MM_EXPORT_DLL extern const float mmVec2NegativeUnitY[2];
MM_EXPORT_DLL extern const float mmVec2UnitScale[2];
MM_EXPORT_DLL extern const float mmVec2UnitHalf[2];

/*!
 * @brief Assign a vec2.
 *
 * formula:  r = a
 *
 * @param[out] r             vec2 result
 * @param[in]  v             vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Assign(
    float r[2], 
    const float v[2]);

/*!
 * @brief Assign value to a vec2.
 *
 * formula:  r = vec2(v, v)
 *
 * @param[out] r             vec2 result
 * @param[in]  v             float
 */
MM_EXPORT_DLL 
void 
mmVec2AssignValue(
    float r[2], 
    float v);

/*!
 * @brief Check if two vec2 memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a             vec2 a
 * @param[in]  b             vec2 b
 * @return                   memcmp(a, b)
 */
MM_EXPORT_DLL 
int 
mmVec2Compare(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Check if two vec2 are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a             vec2 a
 * @param[in]  b             vec2 b
 * @return                   a == b
 */
MM_EXPORT_DLL 
int 
mmVec2Equals(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Make vec2.
 *
 * @param[out] v               vec2
 * @param[in]  x               float
 * @param[in]  y               float
 */
MM_EXPORT_DLL 
void 
mmVec2Make(
    float v[2], 
    float x, 
    float y);

/*!
 * @brief Addition vec2.
 *
 * formula:  r = a + b
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Add(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief Addition vec2.
 *
 * formula:  r = a + vec2(x, y)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  x               float
 * @param[in]  y               float
 */
MM_EXPORT_DLL 
void 
mmVec2AddXY(
    float r[2], 
    const float a[2], 
    float x, float y);

/*!
 * @brief Addition vec2.
 *
 * formula:  r = a + vec2(k, k)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec2AddK(
    float r[2], 
    const float a[2], 
    float k);

/*!
 * @brief Subtract vec2.
 *
 * formula:  r = a - b
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Sub(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief Subtract vec2.
 *
 * formula:  r = a - vec2(x, y)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  x               float
 * @param[in]  y               float
 */
MM_EXPORT_DLL 
void 
mmVec2SubXY(
    float r[2], 
    const float a[2],
    float x, float y);

/*!
 * @brief Subtract vec2.
 *
 * formula:  r = a - vec2(k, k)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec2SubK(
    float r[2], 
    const float a[2], 
    float k);

/*!
 * @brief Multiply vec2.
 *
 * formula:  r = a * b
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Mul(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief Multiply vec2.
 *
 * formula:  r = a * vec2(x, y)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  x               float
 * @param[in]  y               float
 */
MM_EXPORT_DLL 
void 
mmVec2MulXY(
    float r[2], 
    const float a[2], 
    float x, float y);

/*!
 * @brief Multiply vec2.
 *
 * formula:  r = a * vec2(k, k)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec2MulK(
    float r[2], 
    const float a[2], 
    float k);

/*!
 * @brief Division vec2.
 *
 * formula:  r = a / b
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Div(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief Division vec2.
 *
 * formula:  r = a / vec2(x, y)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  x               float
 * @param[in]  y               float
 */
MM_EXPORT_DLL 
void 
mmVec2DivXY(
    float r[2], 
    const float a[2], 
    float x, float y);

/*!
 * @brief Division vec2.
 *
 * formula:  r = a / vec2(k, k)
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 * @param[in]  k               float
 */
MM_EXPORT_DLL 
void 
mmVec2DivK(
    float r[2], 
    const float a[2], 
    float k);

/*!
 * @brief Dot product vec2.
 *
 * formula:  Dot(a, b)
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     Dot(a, b)       float
 */
MM_EXPORT_DLL 
float 
mmVec2DotProduct(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Cross product vec2.
 *
 * formula:  Cross(a, b)
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     Cross(a, b)     float
 */
MM_EXPORT_DLL 
float 
mmVec2CrossProduct(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Negate vec2.
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Negate(
    float r[2], 
    const float a[2]);

/*!
 * @brief Length vec2.
 *
 * formula:  Length(a)
 *
 * @param[in]  a               vec2
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec2Length(
    const float a[2]);

/*!
 * @brief Squared length vec2.
 *
 * formula:  Length(a)^2
 *
 * @param[in]  a               vec2
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec2SquaredLength(
    const float a[2]);

/*!
 * @brief Distance vec2.
 *
 * formula:  Distance(a, b)
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec2Distance(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Squared distance vec2.
 *
 * formula:  Distance(a, b)^2
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     value           float
 */
MM_EXPORT_DLL 
float 
mmVec2SquaredDistance(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Perpendicular vec2.
 *
 * @param[out] r               vec2
 * @param[in]  a               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Perpendicular(
    float r[2], 
    const float a[2]);

/*!
 * @brief Scale each element of the vec2.
 *
 * @param[out] r               vec2 result
 * @param[in]  v               vec2
 * @param[in]  s               s
 */
MM_EXPORT_DLL 
void 
mmVec2Scale(
    float r[2], 
    const float v[2], 
    const float s);

/*!
 * @brief Multiply inner vec2.
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     MulInner(a, b)  float
 */
MM_EXPORT_DLL 
float 
mmVec2MulInner(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Multiply cross vec2.
 *
 * @param[out] r               vec2 result
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2MulCross(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief Normalize vec2.
 *
 * @param[out] r               vec2 result
 * @param[in]  v               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Normalize(
    float r[2], 
    const float v[2]);

/*!
 * @brief Reflect vec2.
 *
 * @param[out] r               vec2 result
 * @param[in]  v               vec2
 * @param[in]  n               vec2
 */
MM_EXPORT_DLL 
void 
mmVec2Reflect(
    float r[2], 
    const float v[2], 
    const float n[2]);

/*!
 * @brief Angle between two vec2.
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     angle           float
 */
MM_EXPORT_DLL 
float 
mmVec2AngleBetween(
    const float a[2], 
    const float b[2]);

/*!
 * @brief Angle to vec2.
 *
 * @param[in]  a               vec2
 * @param[in]  b               vec2
 * @return     angle           float
 */
MM_EXPORT_DLL 
float 
mmVec2AngleTo(
    const float a[2], 
    const float b[2]);

/*!
 * @brief vector scale to length.
 *
 * Formula:
 *  v = desiredLength / length(v)
 *
 * @param[in,out] v                vector
 * @param[in]     desiredLength    desired length
 */
MM_EXPORT_DLL
void
mmVec2ScaleToLength(
    float v[2],
    float desiredLength);

/*!
 * @brief Make a linear combination of two vectors and return the result.
 *
 * Formula:
 *  r = (a * ascl) + (b * bscl)
 *
 * @param[out] r       result
 * @param[in]  a       vector a
 * @param[in]  b       vector b
 * @param[in]  ascl    scale a
 * @param[in]  bscl    scale b
 */
MM_EXPORT_DLL
void
mmVec2LinearCombine(
    float r[2],
    const float a[2],
    const float b[2],
    float ascl,
    float bscl);

/*!
 * @brief abs values of vectors
 *
 * Formula:
 *  r =  abs(a)
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 */
MM_EXPORT_DLL
void
mmVec2Abs(
    float r[2],
    const float a[2]);

/*!
 * @brief max values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec2Max(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief min values of vectors
 *
 * @param[out] r    destination
 * @param[in]  a    vector1
 * @param[in]  b    vector2
 */
MM_EXPORT_DLL
void
mmVec2Min(
    float r[2], 
    const float a[2], 
    const float b[2]);

/*!
 * @brief clamp vector's individual members between min and max values
 *
 * @param[out] r      destination
 * @param[in]  v      vector
 * @param[in]  minval minimum value
 * @param[in]  maxval maximum value
 */
MM_EXPORT_DLL
void
mmVec2Clamp(
    float r[2], 
    const float v[2], 
    float minval, 
    float maxval);

/*!
 * @brief linear interpolation between two vector
 *
 * formula:  from + s * (to - from)
 *
 * @param[out] r    destination
 * @param[in]  from from value
 * @param[in]  to   to value
 * @param[in]  t    interpolant (amount) clamped between 0 and 1
 */
MM_EXPORT_DLL
void
mmVec2Lerp(
    float r[2], 
    const float from[2], 
    const float to[2], 
    const float t);

/*!
 * @brief mix between two numbers
 *
 * formula:  r = x*(1-a) +y*a
 *
 * @param[out] r    destination
 * @param[in]  x  x value
 * @param[in]  y  y value
 * @param[in]  a  mix value
 */
MM_EXPORT_DLL
void
mmVec2Mix(
    float r[2],
    const float x[2],
    const float y[2],
    const float a);

/*!
 * @brief cubic bezier interpolation
 *
 * Formula:
 *  B(s) = P0*(1-s)^3 + 3*C0*s*(1-s)^2 + 3*C1*s^2*(1-s) + P1*s^3
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  c0   control point 1
 * @param[in]  c1   control point 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec2Bezier(
    float r[2], 
    float s, 
    const float p0[2], 
    const float c0[2], 
    const float c1[2], 
    const float p1[2]);

/*!
 * @brief cubic hermite interpolation
 *
 * Formula:
 *  H(s) = P0*(2*s^3 - 3*s^2 + 1) + T0*(s^3 - 2*s^2 + s)
 *            + P1*(-2*s^3 + 3*s^2) + T1*(s^3 - s^2)
 *
 * @param[out] r    destination
 * @param[in]  s    parameter between 0 and 1
 * @param[in]  p0   begin point
 * @param[in]  t0   tangent 1
 * @param[in]  t1   tangent 2
 * @param[in]  p1   end point
 */
MM_EXPORT_DLL
void
mmVec2Hermite(
    float r[2], 
    float s, 
    const float p0[2], 
    const float t0[2], 
    const float t1[2], 
    const float p1[2]);

/*!
 * @brief threshold function
 *
 * @param[out] r       destination
 * @param[in]  edge    threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec2Step(
    float r[2], 
    const float edge[2], 
    const float x[2]);

/*!
 * @brief threshold function with a smooth transition
 *
 * @param[out] r       destination
 * @param[in]  edge0   low threshold
 * @param[in]  edge1   high threshold
 * @param[in]  x       value to test against threshold
 */
MM_EXPORT_DLL
void
mmVec2Smoothstep(
    float r[2], 
    const float edge0[2], 
    const float edge1[2], 
    const float x[2]);

#include "core/mmSuffix.h"

#endif//__mmVector2_h__
