/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmQuaternion.h"
#include "math/mmVector3.h"
#include "math/mmVector4.h"
#include "math/mmMathConst.h"
#include "math/mmMath.h"

#include "core/mmAlloc.h"

MM_EXPORT_DLL const float mmQuatEpsilon = 1e-03f;

MM_EXPORT_DLL const float mmQuatIdentity[4] = { 0, 0, 0, 1 };

/*!
 * @brief Assign a Quaternion.
 *
 * formula:  r = a
 *
 * @param[out] r               quat result
 * @param[in]  a               quat
 */
MM_EXPORT_DLL
void
mmQuatAssign(
    float r[4],
    const float a[4])
{
    mmMemcpy(r, a, sizeof(mmQuatIdentity));
}

/*!
 * @brief Check if two Quaternions memery are equal.
 *
 * formula:  return memcmp(a, b)
 *
 * @param[in]  a               quat a
 * @param[in]  b               quat b
 * @return                     memcmp(a, b)
 */
MM_EXPORT_DLL
int
mmQuatCompare(
    const float a[4],
    const float b[4])
{
    return mmMemcmp(a, b, sizeof(mmQuatIdentity));
}

/*!
 * @brief Check if two Quaternions are equal.
 *
 * formula:  return a == b
 *
 * @param[in]  a               quat a
 * @param[in]  b               quat b
 * @return                     a == b
 */
MM_EXPORT_DLL
int
mmQuatEquals(
    const float a[4],
    const float b[4])
{
    return
        a[0] == b[0] && 
        a[1] == b[1] && 
        a[2] == b[2] && 
        a[3] == b[3];
}

/*!
 * @brief Make Quaternion.
 *
 * @param[out] q               quat
 * @param[in]  x               float
 * @param[in]  y               float
 * @param[in]  z               float
 * @param[in]  w               float
 */
MM_EXPORT_DLL
void
mmQuatMake(
    float q[4],
    float x,
    float y,
    float z,
    float w)
{
    q[0] = x;
    q[1] = y;
    q[2] = z;
    q[3] = w;
}

/*!
 * @brief Make identity Quaternion.
 *
 * @param[in,out] q            quat
 */
MM_EXPORT_DLL
void
mmQuatMakeIdentity(
    float q[4])
{
    q[0] = 0.f;
    q[1] = 0.f;
    q[2] = 0.f;
    q[3] = 1.f;
}

/*!
 * @brief Addition Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 * @param[in]  b               quat
 */
MM_EXPORT_DLL
void
mmQuatAdd(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[0] + b[0];
    r[1] = a[1] + b[1];
    r[2] = a[2] + b[2];
    r[3] = a[3] + b[3];
}

/*!
 * @brief Subtract Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 * @param[in]  b               quat
 */
MM_EXPORT_DLL
void
mmQuatSub(
    float r[4],
    const float a[4],
    const float b[4])
{
    r[0] = a[0] - b[0];
    r[1] = a[1] - b[1];
    r[2] = a[2] - b[2];
    r[3] = a[3] - b[3];
}

/*!
 * @brief Multiply Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  p               quat
 * @param[in]  q               quat
 */
MM_EXPORT_DLL
void
mmQuatMul(
    float r[4],
    const float p[4],
    const float q[4])
{
    r[0] = p[3] * q[0] + p[0] * q[3] + p[1] * q[2] - p[2] * q[1];
    r[1] = p[3] * q[1] - p[0] * q[2] + p[1] * q[3] + p[2] * q[0];
    r[2] = p[3] * q[2] + p[0] * q[1] - p[1] * q[0] + p[2] * q[3];
    r[3] = p[3] * q[3] - p[0] * q[0] - p[1] * q[1] - p[2] * q[2];
}

/*!
 * @brief Scale Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  v               quat
 * @param[in]  s               float
 */
MM_EXPORT_DLL
void
mmQuatScale(
    float r[4],
    const float v[4],
    float s)
{
    r[0] = v[0] * s;
    r[1] = v[1] * s;
    r[2] = v[2] * s;
    r[3] = v[3] * s;
}

/*!
 * @brief Inner product Quaternion.
 *
 * @param[in]  a               quat
 * @param[in]  b               quat
 * @return     InnerProduct(a, b)
 */
MM_EXPORT_DLL
float
mmQuatInnerProduct(
    const float a[4],
    const float b[4])

{
    float p = 0.f;
    p += b[0] * a[0];
    p += b[1] * a[1];
    p += b[2] * a[2];
    p += b[3] * a[3];
    return p;
}

/*!
 * @brief Make a Conjugate from a Quaternion.
 *
 * @param[out] r               quat
 * @param[in]  q               quat
 */
MM_EXPORT_DLL
void
mmQuatConjugate(
    float r[4],
    const float q[4])
{
    r[0] = -q[0];
    r[1] = -q[1];
    r[2] = -q[2];
    r[3] = +q[3];
}

/*!
 * @brief right vec3 to Quaternion.
 *
 * formula:  r = q * v
 *
 * @param[out] r               vec3
 * @param[in]  q               quat
 * @param[in]  v               vec3
 */
MM_EXPORT_DLL
void
mmQuatMulVec3(
    float r[3],
    const float q[4],
    const float v[3])
{
    float t[4] = { v[0], v[1], v[2], 0.f };
    mmQuatConjugate(r, q);
    mmQuatNormalize(r, r);
    mmQuatMul(r, t, r);
    mmQuatMul(r, q, r);
}

/*!
 * @brief Make a Quaternion from mat4x4.
 *
 * @param[out] q               quat
 * @param[in]  m               mat4x4
 */
MM_EXPORT_DLL
void
mmQuatFromMat4x4(
    float q[4],
    const float m[4][4])
{
    // https://github.com/recp/cglm
    // quaternion glm_mat4_quat api

    float trace, r, rinv;

    /* it seems using like m12 instead of m[1][2] causes extra instructions */

    trace = m[0][0] + m[1][1] + m[2][2];
    if (trace >= 0.0f) 
    {
        r = sqrtf(1.0f + trace);
        rinv = 0.5f / r;

        q[0] = rinv * (m[1][2] - m[2][1]);
        q[1] = rinv * (m[2][0] - m[0][2]);
        q[2] = rinv * (m[0][1] - m[1][0]);
        q[3] = r * 0.5f;
    }
    else if (m[0][0] >= m[1][1] && m[0][0] >= m[2][2]) 
    {
        r = sqrtf(1.0f - m[1][1] - m[2][2] + m[0][0]);
        rinv = 0.5f / r;

        q[0] = r * 0.5f;
        q[1] = rinv * (m[0][1] + m[1][0]);
        q[2] = rinv * (m[0][2] + m[2][0]);
        q[3] = rinv * (m[1][2] - m[2][1]);
    }
    else if (m[1][1] >= m[2][2]) 
    {
        r = sqrtf(1.0f - m[0][0] - m[2][2] + m[1][1]);
        rinv = 0.5f / r;

        q[0] = rinv * (m[0][1] + m[1][0]);
        q[1] = r * 0.5f;
        q[2] = rinv * (m[1][2] + m[2][1]);
        q[3] = rinv * (m[2][0] - m[0][2]);
    }
    else 
    {
        r = sqrtf(1.0f - m[0][0] - m[1][1] + m[2][2]);
        rinv = 0.5f / r;

        q[0] = rinv * (m[0][2] + m[2][0]);
        q[1] = rinv * (m[1][2] + m[2][1]);
        q[2] = r * 0.5f;
        q[3] = rinv * (m[0][1] - m[1][0]);
    }
}

/*!
 * @brief Make a Quaternion from mat3x3.
 *
 * @param[out] q               quat
 * @param[in]  m               mat3x3
 */
MM_EXPORT_DLL
void
mmQuatFromMat3x3(
    float q[4],
    const float m[3][3])
{
    // https://github.com/recp/cglm
    // quaternion glm_mat3_quat api

    float trace, r, rinv;

    /* it seems using like m12 instead of m[1][2] causes extra instructions */

    trace = m[0][0] + m[1][1] + m[2][2];
    if (trace >= 0.0f)
    {
        r = sqrtf(1.0f + trace);
        rinv = 0.5f / r;

        q[0] = rinv * (m[1][2] - m[2][1]);
        q[1] = rinv * (m[2][0] - m[0][2]);
        q[2] = rinv * (m[0][1] - m[1][0]);
        q[3] = r * 0.5f;
    }
    else if (m[0][0] >= m[1][1] && m[0][0] >= m[2][2])
    {
        r = sqrtf(1.0f - m[1][1] - m[2][2] + m[0][0]);
        rinv = 0.5f / r;

        q[0] = r * 0.5f;
        q[1] = rinv * (m[0][1] + m[1][0]);
        q[2] = rinv * (m[0][2] + m[2][0]);
        q[3] = rinv * (m[1][2] - m[2][1]);
    }
    else if (m[1][1] >= m[2][2])
    {
        r = sqrtf(1.0f - m[0][0] - m[2][2] + m[1][1]);
        rinv = 0.5f / r;

        q[0] = rinv * (m[0][1] + m[1][0]);
        q[1] = r * 0.5f;
        q[2] = rinv * (m[1][2] + m[2][1]);
        q[3] = rinv * (m[2][0] - m[0][2]);
    }
    else
    {
        r = sqrtf(1.0f - m[0][0] - m[1][1] + m[2][2]);
        rinv = 0.5f / r;

        q[0] = rinv * (m[0][2] + m[2][0]);
        q[1] = rinv * (m[1][2] + m[2][1]);
        q[2] = r * 0.5f;
        q[3] = rinv * (m[0][1] - m[1][0]);
    }
}

/*!
 * @brief Quaternion multiply k.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 * @param[in]  k               float
 */
MM_EXPORT_DLL
void
mmQuatMulK(
    float r[4],
    const float a[4],
    float k)
{
    r[0] = k * a[0];
    r[1] = k * a[1];
    r[2] = k * a[2];
    r[3] = k * a[3];
}

/*!
 * @brief Negate a quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 */
MM_EXPORT_DLL
void
mmQuatNegate(
    float r[4],
    const float a[4])
{
    r[0] = -a[0];
    r[1] = -a[1];
    r[2] = -a[2];
    r[3] = -a[3];
}

/*!
 * @brief Dot product.
 *
 * @param[in]  a               quat
 * @param[in]  b               quat
 * @return     dot(a, b)
 */
MM_EXPORT_DLL
float
mmQuatDotProduct(
    const float a[4],
    const float b[4])
{
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
}

/*!
 * @brief Inverse a quaternion.
 *
 * @param[out] r               quat
 * @param[in]  a               quat
 */
MM_EXPORT_DLL
void
mmQuatInverse(
    float r[4],
    const float a[4])
{
    float fNorm = mmQuatDotProduct(a, a);
    if (fNorm > 0.0f)
    {
        float fInvNorm = 1.0f / fNorm;
        
        r[0] = -a[0] * fInvNorm;
        r[1] = -a[1] * fInvNorm;
        r[2] = -a[2] * fInvNorm;
        r[3] = +a[3] * fInvNorm;
    }
    else
    {
        // return an invalid result to flag the error
        r[0] = 0;
        r[1] = 0;
        r[2] = 0;
        r[3] = 0;
    }
}

/*!
 * @brief From angle axis.
 *
 * @param[out]  q              quat
 * @param[in]   rfAngle        float
 * @param[in]   rkAxis         vec3
 */
MM_EXPORT_DLL
void
mmQuatFromAngleAxis(
    float q[4],
    float rfAngle,
    const float rkAxis[3])
{
    // assert:  axis[] is unit length
    //
    // The quaternion representing the rotation is
    //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

    float fHalfAngle = 0.5f * rfAngle;
    float fSin = sinf(fHalfAngle);
    q[3] = cosf(fHalfAngle);
    q[0] = fSin * rkAxis[0];
    q[1] = fSin * rkAxis[1];
    q[2] = fSin * rkAxis[2];
}

/*!
 * @brief To angle axis.
 *
 * @param[in]   q              quat
 * @param[out]  rfAngle        float*
 * @param[out]  rkAxis         vec3
 */
MM_EXPORT_DLL
void
mmQuatToAngleAxis(
    const float q[4],
    float* rfAngle,
    float rkAxis[3])
{
    // The quaternion representing the rotation is
    //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

    float fSqrLength = q[0] * q[0] + q[1] * q[1] + q[2] * q[2];
    if (fSqrLength > 0.0)
    {
        float fInvLength = 0.0f;
        (*rfAngle) = 2.0f * acosf(q[3]);
        fInvLength = mmMathInvSqrt(fSqrLength);
        rkAxis[0] = q[0] * fInvLength;
        rkAxis[1] = q[1] * fInvLength;
        rkAxis[2] = q[2] * fInvLength;
    }
    else
    {
        // angle is 0 (mod 2*pi), so any axis will do
        (*rfAngle) = 0.0f;
        rkAxis[0] = 1.0f;
        rkAxis[1] = 0.0f;
        rkAxis[2] = 0.0f;
    }
}

/*!
 * @brief Get Rotate x-axis.
 *
 * @param[in]   q              quat
 * @param[out]  xAxis          vec3
 */
MM_EXPORT_DLL
void
mmQuatXAxis(
    const float q[4],
    float xAxis[3])
{
    //Real fTx  = 2.0 * q[0];
    float fTy = 2.0f * q[1];
    float fTz = 2.0f * q[2];
    float fTwy = fTy * q[3];
    float fTwz = fTz * q[3];
    float fTxy = fTy * q[0];
    float fTxz = fTz * q[0];
    float fTyy = fTy * q[1];
    float fTzz = fTz * q[2];

    mmVec3Make(xAxis, 1.0f - (fTyy + fTzz), fTxy + fTwz, fTxz - fTwy);
}

/*!
 * @brief Get Rotate y-axis.
 *
 * @param[in]   q              quat
 * @param[out]  yAxis          vec3
 */
MM_EXPORT_DLL
void
mmQuatYAxis(
    const float q[4],
    float yAxis[3])
{
    float fTx = 2.0f * q[0];
    float fTy = 2.0f * q[1];
    float fTz = 2.0f * q[2];
    float fTwx = fTx * q[3];
    float fTwz = fTz * q[3];
    float fTxx = fTx * q[0];
    float fTxy = fTy * q[0];
    float fTyz = fTz * q[1];
    float fTzz = fTz * q[2];

    mmVec3Make(yAxis, fTxy - fTwz, 1.0f - (fTxx + fTzz), fTyz + fTwx);
}

/*!
 * @brief Get Rotate z-axis.
 *
 * @param[in]   q              quat
 * @param[out]  zAxis          vec3
 */
MM_EXPORT_DLL
void
mmQuatZAxis(
    const float q[4],
    float zAxis[3])
{
    float fTx = 2.0f * q[0];
    float fTy = 2.0f * q[1];
    float fTz = 2.0f * q[2];
    float fTwx = fTx * q[3];
    float fTwy = fTy * q[3];
    float fTxx = fTx * q[0];
    float fTxz = fTz * q[0];
    float fTyy = fTy * q[1];
    float fTyz = fTz * q[1];

    mmVec3Make(zAxis, fTxz + fTwy, fTyz - fTwx, 1.0f - (fTxx + fTyy));
}

/*!
 * @brief Get Rotate x-axis.
 *
 * @param[in]   q              quat
 * @param[in]   reprojectAxis  int
 * @return      rotate
 */
MM_EXPORT_DLL
float
mmQuatGetRotateX(
    const float q[4],
    int reprojectAxis)
{
    if (reprojectAxis)
    {
        // roll = atan2(localx.y, localx.x)
        // pick parts of xAxis() implementation that we need
        // Real fTx  = 2.0*x;
        float fTy = 2.0f * q[1];
        float fTz = 2.0f * q[2];
        float fTwz = fTz * q[3];
        float fTxy = fTy * q[0];
        float fTyy = fTy * q[1];
        float fTzz = fTz * q[2];

        // Vector3(1.0-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);
        return atan2f(fTxy + fTwz, 1.0f - (fTyy + fTzz));

    }
    else
    {
        float dy = 2 * (q[0] * q[1] + q[3] * q[2]);
        float dx = q[3] * q[3] + q[0] * q[0] - q[1] * q[1] - q[2] * q[2];
        return atan2f(dy, dx);
    }
}

/*!
 * @brief Get Rotate y-axis.
 *
 * @param[in]   q              quat
 * @param[in]   reprojectAxis  int
 * @return      rotate
 */
MM_EXPORT_DLL
float
mmQuatGetRotateY(
    const float q[4],
    int reprojectAxis)
{
    if (reprojectAxis)
    {
        // pitch = atan2(localy.z, localy.y)
        // pick parts of yAxis() implementation that we need
        float fTx = 2.0f * q[0];
        // float fTy  = 2.0f * q[1];
        float fTz = 2.0f * q[2];
        float fTwx = fTx * q[3];
        float fTxx = fTx * q[0];
        float fTyz = fTz * q[1];
        float fTzz = fTz * q[2];

        // Vector3(fTxy-fTwz, 1.0-(fTxx+fTzz), fTyz+fTwx);
        return atan2f(fTyz + fTwx, 1.0f - (fTxx + fTzz));
    }
    else
    {
        // internal version
        float dy = 2 * (q[1] * q[2] + q[3] * q[0]);
        float dx = q[3] * q[3] - q[0] * q[0] - q[1] * q[1] + q[2] * q[2];
        return atan2f(dy, dx);
    }
}

/*!
 * @brief Get Rotate z-axis.
 *
 * @param[in]   q              quat
 * @param[in]   reprojectAxis  int
 * @return      rotate
 */
MM_EXPORT_DLL
float
mmQuatGetRotateZ(
    const float q[4],
    int reprojectAxis)
{
    if (reprojectAxis)
    {
        // yaw = atan2(localz.x, localz.z)
        // pick parts of zAxis() implementation that we need
        float fTx = 2.0f * q[0];
        float fTy = 2.0f * q[1];
        float fTz = 2.0f * q[2];
        float fTwy = fTy * q[3];
        float fTxx = fTx * q[0];
        float fTxz = fTz * q[0];
        float fTyy = fTy * q[1];

        // Vector3(fTxz+fTwy, fTyz-fTwx, 1.0-(fTxx+fTyy));
        return atan2f(fTxz + fTwy, 1.0f - (fTxx + fTyy));

    }
    else
    {
        // internal version
        return asinf(-2 * (q[0] * q[2] - q[3] * q[1]));
    }
}

/*!
 * @brief Get tolerance equals.
 *
 * @param[in]   a         quat
 * @param[in]   b         quat
 * @param[in]   tolerance float
 * @return      equals
 */
MM_EXPORT_DLL
int
mmQuatToleranceEquals(
    const float a[4],
    const float b[4],
    float tolerance)
{
    float d = mmQuatDotProduct(a, b);
    float angle = acosf(2.0f * d * d - 1.0f);
    return fabsf(angle) <= tolerance;
}

/*!
 * @brief Get orientation equals.
 *
 * @param[in]   a         quat
 * @param[in]   b         quat
 * @param[in]   tolerance float
 * @return      equals
 */
MM_EXPORT_DLL
int
mmQuatOrientationEquals(
    const float a[4],
    const float b[4],
    float tolerance)
{
    float d = mmQuatDotProduct(a, b);
    return 1 - d * d < tolerance;
}

/*!
 * @brief Get rotation to.
 *
 * @param[out]  r            quat result
 * @param[in]   rkVector     vec3
 * @param[in]   dest         vec3
 * @param[in]   fallbackAxis vec3
 */
MM_EXPORT_DLL
void
mmQuatGetRotationTo(
    float r[4],
    const float rkVector[3],
    const float dest[3],
    const float fallbackAxis[3])
{
    // From Sam Hocevar's article "Quaternion from two vectors:
    // the final version"
    float axis[3];
    float a = sqrtf(mmVec3SquaredLength(rkVector) * mmVec3SquaredLength(dest));
    float b = a + mmVec3DotProduct(rkVector, dest);

    if (b < 1e-06f * a)
    {
        b = 0.0f;
        if (!mmVec3Equals(fallbackAxis, mmVec3Zero))
        {
            mmVec3Assign(axis, fallbackAxis);
        }
        else
        {
            if (fabsf(rkVector[0]) > fabsf(rkVector[2]))
            {
                mmVec3Make(axis, -rkVector[1], rkVector[0], 0.0f);
            }
            else
            {
                mmVec3Make(axis, 0.0f, -rkVector[2], rkVector[1]);
            }
        }
    }
    else
    {
        mmVec3CrossProduct(axis, rkVector, dest);
    }

    mmQuatMake(r, b, axis[0], axis[1], axis[2]);
    mmQuatNormalize(r, r);
}

/*!
 * @brief normalize quaternion and store result in dest
 *
 * @param[out]  r     destination quaternion
 * @param[in]   q     quaternion to normalze
 */
MM_EXPORT_DLL
void
mmQuatNormalize(
    float r[4],
    const float q[4])
{
    // Identity = Normalize(Zero), not like vec, we can not return NAN.
    // https://github.com/g-truc/glm is also return Identity.
    // https://github.com/recp/cglm  is also return Identity.

    float dot;

    dot = mmVec4DotProduct(q, q);

    if (dot <= 0.0f)
    {
        mmQuatMakeIdentity(r);
    }
    else
    {
        mmVec4Scale(r, q, 1.0f / sqrtf(dot));
    }
}

/*!
 * @brief interpolates between two quaternions
 *        using linear interpolation (LERP)
 *
 * @param[out]  r     result quaternion
 * @param[in]   from  from
 * @param[in]   to    to
 * @param[in]   t     interpolant (amount)
 */
MM_EXPORT_DLL
void
mmQuatLerp(
    float r[4],
    const float from[4],
    const float to[4],
    float t)
{
    mmVec4Lerp(r, from, to, t);
}

/*!
 * @brief interpolates between two quaternions
 *        taking the shortest rotation path using
 *        normalized linear interpolation (NLERP)
 *
 * @param[out]  r     result quaternion
 * @param[in]   from  from
 * @param[in]   to    to
 * @param[in]   t     interpolant (amount)
 */
MM_EXPORT_DLL
void
mmQuatNlerp(
    float r[4],
    const float from[3],
    const float to[3],
    float t)
{
    float target[3];
    float  dot;

    dot = mmVec4DotProduct(from, to);

    mmVec4Scale(target, to, (dot >= 0) ? 1.0f : -1.0f);
    mmQuatLerp(r, from, target, t);
    mmQuatNormalize(r, r);
}

/*!
 * @brief interpolates between two quaternions
 *        using spherical linear interpolation (SLERP)
 *
 * @param[out]  r     result quaternion
 * @param[in]   from  from
 * @param[in]   to    to
 * @param[in]   t     amout
 */
MM_EXPORT_DLL
void
mmQuatSlerp(
    float r[4],
    const float from[4],
    const float to[4],
    float t)
{
    float q1[4];
    float q2[4];
    float cosTheta, sinTheta, angle;

    cosTheta = mmQuatDotProduct(from, to);
    mmQuatAssign(q1, from);

    if (fabsf(cosTheta) >= 1.0f)
    {
        mmQuatAssign(r, q1);
        return;
    }

    if (cosTheta < 0.0f)
    {
        mmVec4Negate(q1, q1);
        cosTheta = -cosTheta;
    }

    sinTheta = sqrtf(1.0f - cosTheta * cosTheta);

    /* LERP to avoid zero division */
    if (fabsf(sinTheta) < 0.001f)
    {
        mmQuatLerp(r, from, to, t);
        return;
    }

    /* SLERP */
    angle = acosf(cosTheta);
    mmVec4Scale(q1, q1, sinf((1.0f - t) * angle));
    mmVec4Scale(q2, to, sinf(t * angle));

    mmVec4Add(q1, q1, q2);
    mmVec4Scale(r, q1, 1.0f / sinTheta);
}
