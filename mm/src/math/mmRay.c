/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRay.h"
#include "math/mmVector3.h"

MM_EXPORT_DLL
int
mmRayTriangle(
    const float origin[3],
    const float direction[3],
    const float v0[3],
    const float v1[3],
    const float v2[3],
    float       *d)
{
    float edge1[3], edge2[3], p[3], t[3], q[3];
    float det, inv_det, u, v, dist;
    const float epsilon = 0.000001f;

    mmVec3Sub(edge1, v1, v0);
    mmVec3Sub(edge2, v2, v0);
    mmVec3CrossProduct(p, direction, edge2);

    det = mmVec3DotProduct(edge1, p);
    if (det > -epsilon && det < epsilon)
    {
        return MM_FALSE;
    }

    inv_det = 1.0f / det;

    mmVec3Sub(t, origin, v0);

    u = inv_det * mmVec3DotProduct(t, p);
    if (u < 0.0f || u > 1.0f)
    {
        return MM_FALSE;
    }

    mmVec3CrossProduct(q, t, edge1);

    v = inv_det * mmVec3DotProduct(direction, q);
    if (v < 0.0f || u + v > 1.0f)
    {
        return MM_FALSE;
    }

    dist = inv_det * mmVec3DotProduct(edge2, q);

    if (d)
    {
        *d = dist;
    }

    return dist > epsilon;
}
