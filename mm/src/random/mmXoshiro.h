/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmXoshiro_h__
#define __mmXoshiro_h__

#include "core/mmConfig.h"
#include "core/mmTypes.h"

#include "core/mmPrefix.h"

/*
** {==================================================================
** Pseudo-Random Number Generator based on 'xoshiro'.
** http://xoshiro.di.unimi.it/
** See <http://creativecommons.org/publicdomain/zero/1.0/>.
** ===================================================================
*/

static mmInline mmUInt32_t mmXoshiro_UInt32Rotl(const mmUInt32_t x, int k)
{
    return (x << k) | (x >> (32 - k));
}
static mmInline mmUInt64_t mmXoshiro_UInt64Rotl(const mmUInt64_t x, int k)
{
    return (x << k) | (x >> (64 - k));
}

/* This is xoroshiro64* 1.0, our best and fastest 32-bit small-state generator
for 32-bit floating-point numbers. We suggest to use its upper bits for
floating-point generation, as it is slightly faster than
xoroshiro64**. It passes all tests we are aware of except for
linearity tests, as the lowest six bits have low linear complexity, so
if low linear complexity is not considered an issue (as it is usually
the case) it can be used to generate 32-bit outputs, too.

We suggest to use a sign test to extract a random Boolean value, and
right shifts to extract subsets of bits.

The state must be seeded so that it is not everywhere zero. */
struct mmXoroshiro64star
{
    mmUInt32_t s[2];
};
MM_EXPORT_DLL void mmXoroshiro64star_Init(struct mmXoroshiro64star* p);
MM_EXPORT_DLL void mmXoroshiro64star_Destroy(struct mmXoroshiro64star* p);
MM_EXPORT_DLL void mmXoroshiro64star_Srand(struct mmXoroshiro64star* p, mmUInt32_t seed);
MM_EXPORT_DLL mmUInt32_t mmXoroshiro64star_Next(struct mmXoroshiro64star* p);

/* This is xoroshiro64** 1.0, our 32-bit all-purpose, rock-solid, small-state
generator. It is extremely fast and it passes all tests we are aware
of, but its state space is not large enough for any parallel
application.

For generating just single-precision (i.e., 32-bit) floating-point
numbers, xoroshiro64* is even faster.

The state must be seeded so that it is not everywhere zero. */
struct mmXoroshiro64starstar
{
    mmUInt32_t s[2];
};
MM_EXPORT_DLL void mmXoroshiro64starstar_Init(struct mmXoroshiro64starstar* p);
MM_EXPORT_DLL void mmXoroshiro64starstar_Destroy(struct mmXoroshiro64starstar* p);
MM_EXPORT_DLL void mmXoroshiro64starstar_Srand(struct mmXoroshiro64starstar* p, mmUInt32_t seed);
MM_EXPORT_DLL mmUInt32_t mmXoroshiro64starstar_Next(struct mmXoroshiro64starstar* p);

/* This is xoroshiro128+ 1.0, our best and fastest small-state generator
for floating-point numbers. We suggest to use its upper bits for
floating-point generation, as it is slightly faster than
xoroshiro128**. It passes all tests we are aware of except for the four
lower bits, which might fail linearity tests (and just those), so if
low linear complexity is not considered an issue (as it is usually the
case) it can be used to generate 64-bit outputs, too; moreover, this
generator has a very mild Hamming-weight dependency making our test
(http://prng.di.unimi.it/hwd.php) fail after 8 TB of output; we believe
this slight bias cannot affect any application. If you are concerned,
use xoroshiro128** or xoshiro256+.

We suggest to use a sign test to extract a random Boolean value, and
right shifts to extract subsets of bits.

The state must be seeded so that it is not everywhere zero. If you have
a 64-bit seed, we suggest to seed a splitmix64 generator and use its
output to fill s.

NOTE: the parameters (a=24, b=16, b=37) of this version give slightly
better results in our test than the 2016 version (a=55, b=14, c=36).
*/
struct mmXoroshiro128plus
{
    mmUInt64_t s[2];
};
MM_EXPORT_DLL void mmXoroshiro128plus_Init(struct mmXoroshiro128plus* p);
MM_EXPORT_DLL void mmXoroshiro128plus_Destroy(struct mmXoroshiro128plus* p);
MM_EXPORT_DLL void mmXoroshiro128plus_Srand(struct mmXoroshiro128plus* p, mmUInt64_t seed);
MM_EXPORT_DLL mmUInt64_t mmXoroshiro128plus_Next(struct mmXoroshiro128plus* p);
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoroshiro128plus_Jump(struct mmXoroshiro128plus* p);

/* This is xoroshiro128** 1.0, our all-purpose, rock-solid, small-state
generator. It is extremely (sub-ns) fast and it passes all tests we are
aware of, but its state space is large enough only for mild parallelism.

For generating just floating-point numbers, xoroshiro128+ is even
faster (but it has a very mild bias, see notes in the comments).

The state must be seeded so that it is not everywhere zero. If you have
a 64-bit seed, we suggest to seed a splitmix64 generator and use its
output to fill s. */
struct mmXoroshiro128starstar
{
    mmUInt64_t s[2];
};
MM_EXPORT_DLL void mmXoroshiro128starstar_Init(struct mmXoroshiro128starstar* p);
MM_EXPORT_DLL void mmXoroshiro128starstar_Destroy(struct mmXoroshiro128starstar* p);
MM_EXPORT_DLL void mmXoroshiro128starstar_Srand(struct mmXoroshiro128starstar* p, mmUInt64_t seed);
MM_EXPORT_DLL mmUInt64_t mmXoroshiro128starstar_Next(struct mmXoroshiro128starstar* p);
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoroshiro128starstar_Jump(struct mmXoroshiro128starstar* p);

/* This is xoshiro128+ 1.0, our best and fastest 32-bit generator for 32-bit
floating-point numbers. We suggest to use its upper bits for
floating-point generation, as it is slightly faster than xoshiro128**.
It passes all tests we are aware of except for
linearity tests, as the lowest four bits have low linear complexity, so
if low linear complexity is not considered an issue (as it is usually
the case) it can be used to generate 32-bit outputs, too.

We suggest to use a sign test to extract a random Boolean value, and
right shifts to extract subsets of bits.

The state must be seeded so that it is not everywhere zero. */
struct mmXoshiro128plus
{
    mmUInt32_t s[4];
};
MM_EXPORT_DLL void mmXoshiro128plus_Init(struct mmXoshiro128plus* p);
MM_EXPORT_DLL void mmXoshiro128plus_Destroy(struct mmXoshiro128plus* p);
MM_EXPORT_DLL void mmXoshiro128plus_Srand(struct mmXoshiro128plus* p, mmUInt32_t seed);
MM_EXPORT_DLL mmUInt32_t mmXoshiro128plus_Next(struct mmXoshiro128plus* p);
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro128plus_Jump(struct mmXoshiro128plus* p);

/* This is xoshiro128** 1.0, our 32-bit all-purpose, rock-solid generator. It
has excellent (sub-ns) speed, a state size (128 bits) that is large
enough for mild parallelism, and it passes all tests we are aware of.

For generating just single-precision (i.e., 32-bit) floating-point
numbers, xoshiro128+ is even faster.

The state must be seeded so that it is not everywhere zero. */
struct mmXoshiro128starstar
{
    mmUInt32_t s[4];
};
MM_EXPORT_DLL void mmXoshiro128starstar_Init(struct mmXoshiro128starstar* p);
MM_EXPORT_DLL void mmXoshiro128starstar_Destroy(struct mmXoshiro128starstar* p);
MM_EXPORT_DLL void mmXoshiro128starstar_Srand(struct mmXoshiro128starstar* p, mmUInt32_t seed);
MM_EXPORT_DLL mmUInt32_t mmXoshiro128starstar_Next(struct mmXoshiro128starstar* p);
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro128starstar_Jump(struct mmXoshiro128starstar* p);

/* This is xoshiro256+ 1.0, our best and fastest generator for floating-point
numbers. We suggest to use its upper bits for floating-point
generation, as it is slightly faster than xoshiro256**. It passes all
tests we are aware of except for the lowest three bits, which might
fail linearity tests (and just those), so if low linear complexity is
not considered an issue (as it is usually the case) it can be used to
generate 64-bit outputs, too.

We suggest to use a sign test to extract a random Boolean value, and
right shifts to extract subsets of bits.

The state must be seeded so that it is not everywhere zero. If you have
a 64-bit seed, we suggest to seed a splitmix64 generator and use its
output to fill s. */
struct mmXoshiro256plus
{
    mmUInt64_t s[4];
};
MM_EXPORT_DLL void mmXoshiro256plus_Init(struct mmXoshiro256plus* p);
MM_EXPORT_DLL void mmXoshiro256plus_Destroy(struct mmXoshiro256plus* p);
MM_EXPORT_DLL void mmXoshiro256plus_Srand(struct mmXoshiro256plus* p, mmUInt64_t seed);
MM_EXPORT_DLL mmUInt64_t mmXoshiro256plus_Next(struct mmXoshiro256plus* p);
/* This is the jump function for the generator. It is equivalent
to 2^128 calls to next(); it can be used to generate 2^128
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro256plus_Jump(struct mmXoshiro256plus* p);

/* This is xoshiro256** 1.0, our all-purpose, rock-solid generator. It has
excellent (sub-ns) speed, a state (256 bits) that is large enough for
any parallel application, and it passes all tests we are aware of.

For generating just floating-point numbers, xoshiro256+ is even faster.

The state must be seeded so that it is not everywhere zero. If you have
a 64-bit seed, we suggest to seed a splitmix64 generator and use its
output to fill s. */
struct mmXoshiro256starstar
{
    mmUInt64_t s[4];
};
MM_EXPORT_DLL void mmXoshiro256starstar_Init(struct mmXoshiro256starstar* p);
MM_EXPORT_DLL void mmXoshiro256starstar_Destroy(struct mmXoshiro256starstar* p);
MM_EXPORT_DLL void mmXoshiro256starstar_Srand(struct mmXoshiro256starstar* p, mmUInt64_t seed);
MM_EXPORT_DLL mmUInt64_t mmXoshiro256starstar_Next(struct mmXoshiro256starstar* p);
/* This is the jump function for the generator. It is equivalent
to 2^128 calls to next(); it can be used to generate 2^128
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro256starstar_Jump(struct mmXoshiro256starstar* p);

#include "core/mmSuffix.h"

#endif//__mmXoshiro_h__
