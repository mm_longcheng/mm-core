/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmXoshiro.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"

MM_EXPORT_DLL void mmXoroshiro64star_Init(struct mmXoroshiro64star* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro64star_Destroy(struct mmXoroshiro64star* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro64star_Srand(struct mmXoroshiro64star* p, mmUInt32_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoroshiro64star_Next(p);
    }
}
MM_EXPORT_DLL mmUInt32_t mmXoroshiro64star_Next(struct mmXoroshiro64star* p)
{
    const mmUInt32_t s0 = p->s[0];
    mmUInt32_t s1 = p->s[1];
    const mmUInt32_t result_star = s0 * 0x9E3779BB;

    s1 ^= s0;
    p->s[0] = mmXoshiro_UInt32Rotl(s0, 26) ^ s1 ^ (s1 << 9); // a, b
    p->s[1] = mmXoshiro_UInt32Rotl(s1, 13); // c

    return result_star;
}

MM_EXPORT_DLL void mmXoroshiro64starstar_Init(struct mmXoroshiro64starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro64starstar_Destroy(struct mmXoroshiro64starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro64starstar_Srand(struct mmXoroshiro64starstar* p, mmUInt32_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoroshiro64starstar_Next(p);
    }
}
MM_EXPORT_DLL mmUInt32_t mmXoroshiro64starstar_Next(struct mmXoroshiro64starstar* p)
{
    const mmUInt32_t s0 = p->s[0];
    mmUInt32_t s1 = p->s[1];
    const mmUInt32_t result_starstar = mmXoshiro_UInt32Rotl(s0 * 0x9E3779BB, 5) * 5;

    s1 ^= s0;
    p->s[0] = mmXoshiro_UInt32Rotl(s0, 26) ^ s1 ^ (s1 << 9); // a, b
    p->s[1] = mmXoshiro_UInt32Rotl(s1, 13); // c

    return result_starstar;
}

MM_EXPORT_DLL void mmXoroshiro128plus_Init(struct mmXoroshiro128plus* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro128plus_Destroy(struct mmXoroshiro128plus* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro128plus_Srand(struct mmXoroshiro128plus* p, mmUInt64_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoroshiro128plus_Next(p);
    }
}
MM_EXPORT_DLL mmUInt64_t mmXoroshiro128plus_Next(struct mmXoroshiro128plus* p)
{
    const mmUInt64_t s0 = p->s[0];
    mmUInt64_t s1 = p->s[1];
    const mmUInt64_t result = s0 + s1;

    s1 ^= s0;
    p->s[0] = mmXoshiro_UInt64Rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
    p->s[1] = mmXoshiro_UInt64Rotl(s1, 37); // c

    return result;
}
MM_EXPORT_DLL void mmXoroshiro128plus_Jump(struct mmXoroshiro128plus* p)
{
    static const mmUInt64_t JUMP[] = { 0xdf900294d8f554a5, 0x170865df4b3201fc };

    mmUInt64_t s0 = 0;
    mmUInt64_t s1 = 0;

    int i = 0;
    int b = 0;

    for (i = 0; i < sizeof(JUMP) / sizeof(*JUMP); i++)
    {
        for (b = 0; b < 64; b++)
        {
            if (JUMP[i] & UINT64_C(1) << b)
            {
                s0 ^= p->s[0];
                s1 ^= p->s[1];
            }
            mmXoroshiro128plus_Next(p);
        }
    }

    p->s[0] = s0;
    p->s[1] = s1;
}

MM_EXPORT_DLL void mmXoroshiro128starstar_Init(struct mmXoroshiro128starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro128starstar_Destroy(struct mmXoroshiro128starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 2);
}
MM_EXPORT_DLL void mmXoroshiro128starstar_Srand(struct mmXoroshiro128starstar* p, mmUInt64_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoroshiro128starstar_Next(p);
    }
}
MM_EXPORT_DLL mmUInt64_t mmXoroshiro128starstar_Next(struct mmXoroshiro128starstar* p)
{
    const mmUInt64_t s0 = p->s[0];
    mmUInt64_t s1 = p->s[1];
    const mmUInt64_t result = mmXoshiro_UInt64Rotl(s0 * 5, 7) * 9;

    s1 ^= s0;
    p->s[0] = mmXoshiro_UInt64Rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
    p->s[1] = mmXoshiro_UInt64Rotl(s1, 37); // c

    return result;
}
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoroshiro128starstar_Jump(struct mmXoroshiro128starstar* p)
{
    static const mmUInt64_t JUMP[] = { 0xdf900294d8f554a5, 0x170865df4b3201fc };

    mmUInt64_t s0 = 0;
    mmUInt64_t s1 = 0;

    int i = 0;
    int b = 0;

    for (i = 0; i < sizeof(JUMP) / sizeof(*JUMP); i++)
    {
        for (b = 0; b < 64; b++)
        {
            if (JUMP[i] & UINT64_C(1) << b)
            {
                s0 ^= p->s[0];
                s1 ^= p->s[1];
            }
            mmXoroshiro128starstar_Next(p);
        }
    }

    p->s[0] = s0;
    p->s[1] = s1;
}

MM_EXPORT_DLL void mmXoshiro128plus_Init(struct mmXoshiro128plus* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro128plus_Destroy(struct mmXoshiro128plus* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro128plus_Srand(struct mmXoshiro128plus* p, mmUInt32_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;
    p->s[2] = seed;
    p->s[3] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoshiro128plus_Next(p);
    }
}
MM_EXPORT_DLL mmUInt32_t mmXoshiro128plus_Next(struct mmXoshiro128plus* p)
{
    const mmUInt32_t result_plus = p->s[0] + p->s[3];

    const mmUInt32_t t = p->s[1] << 9;

    p->s[2] ^= p->s[0];
    p->s[3] ^= p->s[1];
    p->s[1] ^= p->s[2];
    p->s[0] ^= p->s[3];

    p->s[2] ^= t;

    p->s[3] = mmXoshiro_UInt32Rotl(p->s[3], 11);

    return result_plus;
}
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro128plus_Jump(struct mmXoshiro128plus* p)
{
    static const mmUInt32_t JUMP[] = { 0x8764000b, 0xf542d2d3, 0x6fa035c3, 0x77f2db5b };

    mmUInt32_t s0 = 0;
    mmUInt32_t s1 = 0;
    mmUInt32_t s2 = 0;
    mmUInt32_t s3 = 0;

    int i = 0;
    int b = 0;

    for (i = 0; i < sizeof(JUMP) / sizeof(*JUMP); i++)
    {
        for (b = 0; b < 32; b++)
        {
            if (JUMP[i] & UINT32_C(1) << b)
            {
                s0 ^= p->s[0];
                s1 ^= p->s[1];
                s2 ^= p->s[2];
                s3 ^= p->s[3];
            }
            mmXoshiro128plus_Next(p);
        }
    }

    p->s[0] = s0;
    p->s[1] = s1;
    p->s[2] = s2;
    p->s[3] = s3;
}

MM_EXPORT_DLL void mmXoshiro128starstar_Init(struct mmXoshiro128starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro128starstar_Destroy(struct mmXoshiro128starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt32_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro128starstar_Srand(struct mmXoshiro128starstar* p, mmUInt32_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;
    p->s[2] = seed;
    p->s[3] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoshiro128starstar_Next(p);
    }
}
MM_EXPORT_DLL mmUInt32_t mmXoshiro128starstar_Next(struct mmXoshiro128starstar* p)
{
    const mmUInt32_t result_starstar = mmXoshiro_UInt32Rotl(p->s[0] * 5, 7) * 9;

    const mmUInt32_t t = p->s[1] << 9;

    p->s[2] ^= p->s[0];
    p->s[3] ^= p->s[1];
    p->s[1] ^= p->s[2];
    p->s[0] ^= p->s[3];

    p->s[2] ^= t;

    p->s[3] = mmXoshiro_UInt32Rotl(p->s[3], 11);

    return result_starstar;
}
/* This is the jump function for the generator. It is equivalent
to 2^64 calls to next(); it can be used to generate 2^64
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro128starstar_Jump(struct mmXoshiro128starstar* p)
{
    static const mmUInt32_t JUMP[] = { 0x8764000b, 0xf542d2d3, 0x6fa035c3, 0x77f2db5b };

    mmUInt32_t s0 = 0;
    mmUInt32_t s1 = 0;
    mmUInt32_t s2 = 0;
    mmUInt32_t s3 = 0;

    int i = 0;
    int b = 0;

    for (i = 0; i < sizeof(JUMP) / sizeof(*JUMP); i++)
    {
        for (b = 0; b < 32; b++)
        {
            if (JUMP[i] & UINT32_C(1) << b)
            {
                s0 ^= p->s[0];
                s1 ^= p->s[1];
                s2 ^= p->s[2];
                s3 ^= p->s[3];
            }
            mmXoshiro128starstar_Next(p);
        }
    }

    p->s[0] = s0;
    p->s[1] = s1;
    p->s[2] = s2;
    p->s[3] = s3;
}

MM_EXPORT_DLL void mmXoshiro256plus_Init(struct mmXoshiro256plus* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro256plus_Destroy(struct mmXoshiro256plus* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro256plus_Srand(struct mmXoshiro256plus* p, mmUInt64_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;
    p->s[2] = seed;
    p->s[3] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoshiro256plus_Next(p);
    }
}
MM_EXPORT_DLL mmUInt64_t mmXoshiro256plus_Next(struct mmXoshiro256plus* p)
{
    const mmUInt64_t result_plus = p->s[0] + p->s[3];

    const mmUInt64_t t = p->s[1] << 17;

    p->s[2] ^= p->s[0];
    p->s[3] ^= p->s[1];
    p->s[1] ^= p->s[2];
    p->s[0] ^= p->s[3];

    p->s[2] ^= t;

    p->s[3] = mmXoshiro_UInt64Rotl(p->s[3], 45);

    return result_plus;
}
/* This is the jump function for the generator. It is equivalent
to 2^128 calls to next(); it can be used to generate 2^128
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro256plus_Jump(struct mmXoshiro256plus* p)
{
    static const mmUInt64_t JUMP[] = { 0x180ec6d33cfd0aba, 0xd5a61266f0c9392c, 0xa9582618e03fc9aa, 0x39abdc4529b1661c };

    mmUInt64_t s0 = 0;
    mmUInt64_t s1 = 0;
    mmUInt64_t s2 = 0;
    mmUInt64_t s3 = 0;

    int i = 0;
    int b = 0;

    for (i = 0; i < sizeof(JUMP) / sizeof(*JUMP); i++)
    {
        for (b = 0; b < 64; b++)
        {
            if (JUMP[i] & UINT64_C(1) << b)
            {
                s0 ^= p->s[0];
                s1 ^= p->s[1];
                s2 ^= p->s[2];
                s3 ^= p->s[3];
            }
            mmXoshiro256plus_Next(p);
        }
    }

    p->s[0] = s0;
    p->s[1] = s1;
    p->s[2] = s2;
    p->s[3] = s3;
}

MM_EXPORT_DLL void mmXoshiro256starstar_Init(struct mmXoshiro256starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro256starstar_Destroy(struct mmXoshiro256starstar* p)
{
    mmMemset(p->s, 0, sizeof(mmUInt64_t) * 4);
}
MM_EXPORT_DLL void mmXoshiro256starstar_Srand(struct mmXoshiro256starstar* p, mmUInt64_t seed)
{
    size_t i = 0;

    p->s[0] = seed;
    p->s[1] = ~seed;
    p->s[2] = seed;
    p->s[3] = ~seed;

    for (i = 0; i < 16; i++)
    {
        /* discard initial values */
        mmXoshiro256starstar_Next(p);
    }
}
MM_EXPORT_DLL mmUInt64_t mmXoshiro256starstar_Next(struct mmXoshiro256starstar* p)
{
    const mmUInt64_t result_starstar = mmXoshiro_UInt64Rotl(p->s[1] * 5, 7) * 9;

    const mmUInt64_t t = p->s[1] << 17;

    p->s[2] ^= p->s[0];
    p->s[3] ^= p->s[1];
    p->s[1] ^= p->s[2];
    p->s[0] ^= p->s[3];

    p->s[2] ^= t;

    p->s[3] = mmXoshiro_UInt64Rotl(p->s[3], 45);

    return result_starstar;
}
/* This is the jump function for the generator. It is equivalent
to 2^128 calls to next(); it can be used to generate 2^128
non-overlapping subsequences for parallel computations. */
MM_EXPORT_DLL void mmXoshiro256starstar_Jump(struct mmXoshiro256starstar* p)
{
    static const mmUInt64_t JUMP[] = { 0x180ec6d33cfd0aba, 0xd5a61266f0c9392c, 0xa9582618e03fc9aa, 0x39abdc4529b1661c };

    mmUInt64_t s0 = 0;
    mmUInt64_t s1 = 0;
    mmUInt64_t s2 = 0;
    mmUInt64_t s3 = 0;

    int i = 0;
    int b = 0;

    for (i = 0; i < sizeof(JUMP) / sizeof(*JUMP); i++)
    {
        for (b = 0; b < 64; b++)
        {
            if (JUMP[i] & UINT64_C(1) << b)
            {
                s0 ^= p->s[0];
                s1 ^= p->s[1];
                s2 ^= p->s[2];
                s3 ^= p->s[3];
            }
            mmXoshiro256starstar_Next(p);
        }
    }

    p->s[0] = s0;
    p->s[1] = s1;
    p->s[2] = s2;
    p->s[3] = s3;
}
