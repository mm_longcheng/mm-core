/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRand31_h__
#define __mmRand31_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

#define MM_RAND31_SEED_DEFAULT 1

#define MM_RAND31_MIN_VALUE 1
#define MM_RAND31_MAX_VALUE 0x7FFFFFFE
#define MM_RAND31_LIM_VALUE 0x7FFFFFFF

/*
*
* Initialize the PRNG with a seed between 1 and 0x7FFFFFFE
* (2^^31-2) inclusive.
*
* Derived from rand31pmc, Robin Whittle, Sept 20th 2005.
* http://www.firstpr.com.au/dsp/rand31/
*   16807       multiplier constant (7^^5)
*   0x7FFFFFFF  modulo constant (2^^31-1)
* The inner PRNG produces a value between 1 and 0x7FFFFFFE
* (2^^31-2) inclusive.
* This is the PRNG required by the LDPC-staircase RFC 5170.
*
* v = mmRand31_Next(p);
* [1, maxv] r = ( (mmUInt64_t) ( (double) v * (double) maxv / (double) 0x7FFFFFFF));
*/
struct mmRand31
{
    mmUInt64_t s;
};
MM_EXPORT_DLL void mmRand31_Init(struct mmRand31* p);
MM_EXPORT_DLL void mmRand31_Destroy(struct mmRand31* p);
MM_EXPORT_DLL void mmRand31_Srand(struct mmRand31* p, mmUInt32_t seed);
MM_EXPORT_DLL mmUInt64_t mmRand31_Next(struct mmRand31* p);

#include "core/mmSuffix.h"

#endif//__mmRand31_h__
