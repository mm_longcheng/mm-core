/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDRand48_h__
#define __mmDRand48_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

#define MM_DRAND48_SEED_DEFAULT 1

/*
* x(A) = a * x(A-1) + c (mod m)
* m = 2^48   a = 0x5DEECE66DLL = 273673163155  c = 0xB16 = 138
* [0, 1]
*/
struct mmDRand48
{
    mmUInt64_t s;
};
MM_EXPORT_DLL void mmDRand48_Init(struct mmDRand48* p);
MM_EXPORT_DLL void mmDRand48_Destroy(struct mmDRand48* p);
MM_EXPORT_DLL void mmDRand48_Srand(struct mmDRand48* p, mmUInt32_t seed);
MM_EXPORT_DLL double mmDRand48_Next(struct mmDRand48* p);

#include "core/mmSuffix.h"

#endif//__mmDRand48_h__
