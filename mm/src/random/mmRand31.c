/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRand31.h"
#include <assert.h>

MM_EXPORT_DLL void mmRand31_Init(struct mmRand31* p)
{
    p->s = MM_RAND31_SEED_DEFAULT;
}
MM_EXPORT_DLL void mmRand31_Destroy(struct mmRand31* p)
{
    p->s = MM_RAND31_SEED_DEFAULT;
}
MM_EXPORT_DLL void mmRand31_Srand(struct mmRand31* p, mmUInt32_t seed)
{
    assert((seed >= 1) && (seed <= 0x7FFFFFFE) && "(seed >= 1) && (seed <= 0x7FFFFFFE)");
    p->s = seed;
}
MM_EXPORT_DLL mmUInt64_t mmRand31_Next(struct mmRand31* p)
{
    mmUInt64_t  hi, lo;
    lo = 16807 * (p->s & 0xFFFF);
    hi = 16807 * (p->s >> 16);
    lo += (hi & 0x7FFF) << 16;
    lo += hi >> 15;
    if (lo > 0x7FFFFFFF) lo -= 0x7FFFFFFF;
    p->s = (mmUInt64_t)lo;
    return p->s;
}
