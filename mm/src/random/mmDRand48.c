/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDRand48.h"

MM_EXPORT_DLL void mmDRand48_Init(struct mmDRand48* p)
{
    p->s = MM_DRAND48_SEED_DEFAULT;
}
MM_EXPORT_DLL void mmDRand48_Destroy(struct mmDRand48* p)
{
    p->s = MM_DRAND48_SEED_DEFAULT;
}
MM_EXPORT_DLL void mmDRand48_Srand(struct mmDRand48* p, mmUInt32_t seed)
{
    // 48 bit init only.
    p->s = (((mmUInt64_t)seed) << 16) | (mmUInt64_t)(~seed);
}
MM_EXPORT_DLL double mmDRand48_Next(struct mmDRand48* p)
{
    static const mmUInt64_t m = 0x100000000LL;
    static const mmUInt32_t c = 0xB16;
    static const mmUInt64_t a = 0x5DEECE66DLL;

    p->s = (a * p->s + c) & 0xFFFFFFFFFFFFLL;
    return  ((double)(p->s >> 16) / (double)m);
}