/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


/*
 * The ppc assembler treats ";" as comment, so we have to use "\n".
 * The minus in "bne-" is a hint for the branch prediction unit that
 * this branch is unlikely to be taken.
 * The "1b" means the nearest backward label "1" and the "1f" means
 * the nearest forward label "1".
 *
 * The "b" means that the base registers can be used only, i.e.
 * any register except r0.  The r0 register always has a zero value and
 * could not be used in "addi  r0, r0, 1".
 * The "=&b" means that no input registers can be used.
 *
 * "sync"    read and write barriers
 * "isync"   read barrier, is faster than "sync"
 * "eieio"   write barrier, is faster than "sync"
 * "lwsync"  write barrier, is faster than "eieio" on ppc64
 */

#if (MM_PTR_SIZE == 8)

static mmInline mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set)
{
    mmAtomicUInt_t  res, temp;

    __asm__ volatile (

    "    li      %0, 0       \n" /* preset "0" to "res"                      */
    "    lwsync              \n" /* write barrier                            */
    "1:                      \n"
    "    ldarx   %1, 0, %2   \n" /* load from [lock] into "temp"             */
                                 /*   and store reservation                  */
    "    cmpd    %1, %3      \n" /* compare "temp" and "old"                 */
    "    bne-    2f          \n" /* not equal                                */
    "    stdcx.  %4, 0, %2   \n" /* store "set" into [lock] if reservation   */
                                 /*   is not cleared                         */
    "    bne-    1b          \n" /* the reservation was cleared              */
    "    isync               \n" /* read barrier                             */
    "    li      %0, 1       \n" /* set "1" to "res"                         */
    "2:                      \n"

    : "=&b" (res), "=&b" (temp)
    : "b" (lock), "b" (old), "b" (set)
    : "cc", "memory");

    return res;
}


static mmInline mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add)
{
    mmAtomicUInt_t  res, temp;

    __asm__ volatile (

    "    lwsync              \n" /* write barrier                            */
    "1:  ldarx   %0, 0, %2   \n" /* load from [value] into "res"             */
                                 /*   and store reservation                  */
    "    add     %1, %0, %3  \n" /* "res" + "add" store in "temp"            */
    "    stdcx.  %1, 0, %2   \n" /* store "temp" into [value] if reservation */
                                 /*   is not cleared                         */
    "    bne-    1b          \n" /* try again if reservation was cleared     */
    "    isync               \n" /* read barrier                             */

    : "=&b" (res), "=&b" (temp)
    : "b" (value), "b" (add)
    : "cc", "memory");

    return res;
}


#if (MM_SMP)
#define mmMemoryBarrier()                                                  \
    __asm__ volatile ("isync  \n  lwsync  \n" ::: "memory")
#else
#define mmMemoryBarrier()   __asm__ volatile ("" ::: "memory")
#endif

#else

static mmInline mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set)
{
    mmAtomicUInt_t  res, temp;

    __asm__ volatile (

    "    li      %0, 0       \n" /* preset "0" to "res"                      */
    "    eieio               \n" /* write barrier                            */
    "1:                      \n"
    "    lwarx   %1, 0, %2   \n" /* load from [lock] into "temp"             */
                                 /*   and store reservation                  */
    "    cmpw    %1, %3      \n" /* compare "temp" and "old"                 */
    "    bne-    2f          \n" /* not equal                                */
    "    stwcx.  %4, 0, %2   \n" /* store "set" into [lock] if reservation   */
                                 /*   is not cleared                         */
    "    bne-    1b          \n" /* the reservation was cleared              */
    "    isync               \n" /* read barrier                             */
    "    li      %0, 1       \n" /* set "1" to "res"                         */
    "2:                      \n"

    : "=&b" (res), "=&b" (temp)
    : "b" (lock), "b" (old), "b" (set)
    : "cc", "memory");

    return res;
}


static mmInline mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add)
{
    mmAtomicUInt_t  res, temp;

    __asm__ volatile (

    "    eieio               \n" /* write barrier                            */
    "1:  lwarx   %0, 0, %2   \n" /* load from [value] into "res"             */
                                 /*   and store reservation                  */
    "    add     %1, %0, %3  \n" /* "res" + "add" store in "temp"            */
    "    stwcx.  %1, 0, %2   \n" /* store "temp" into [value] if reservation */
                                 /*   is not cleared                         */
    "    bne-    1b          \n" /* try again if reservation was cleared     */
    "    isync               \n" /* read barrier                             */

    : "=&b" (res), "=&b" (temp)
    : "b" (value), "b" (add)
    : "cc", "memory");

    return res;
}


#if (MM_SMP)
#define mmMemoryBarrier()                                                  \
    __asm__ volatile ("isync  \n  eieio  \n" ::: "memory")
#else
#define mmMemoryBarrier()   __asm__ volatile ("" ::: "memory")
#endif

#endif


#define mmCpuPause()
