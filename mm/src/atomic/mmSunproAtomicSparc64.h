/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


#if (MM_PTR_SIZE == 4)
#define MM_CASA  mmCasa
#else
#define MM_CASA  mmCasxa
#endif


mmAtomicUInt_t
mmCasa(mmAtomicUInt_t set, mmAtomicUInt_t old, mmAtomic_t *lock);

mmAtomicUInt_t
mmCasxa(mmAtomicUInt_t set, mmAtomicUInt_t old, mmAtomic_t *lock);

/* the code in src/os/unix/mmSunproSparc64.il */


static mmInline mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set)
{
    set = MM_CASA(set, old, lock);

    return (set == old);
}


static mmInline mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add)
{
    mmAtomicUInt_t  old, res;

    old = *value;

    for ( ;; )
    {
        res = old + add;
        res = MM_CASA(res, old, value);

        if (res == old)
        {
            return res;
        }

        old = res;
    }
}


#define mmMemoryBarrier()                                                    \
        __asm (".volatile");                                                 \
        __asm ("membar #LoadLoad | #LoadStore | #StoreStore | #StoreLoad");  \
        __asm (".nonvolatile")

#define mmCpuPause()
