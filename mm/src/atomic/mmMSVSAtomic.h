/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include <intrin.h>

#define MM_HAVE_ATOMIC_OPS  1

typedef mmSInt32_t                    mmAtomicInt_t;
typedef mmUInt32_t                    mmAtomicUInt_t;
typedef volatile mmAtomicUInt_t       mmAtomic_t;
#define MM_ATOMIC_T_LEN               (sizeof("-2147483648") - 1)


static mmInline mmAtomicUInt_t
mmAtomicCmpSet(mmAtomic_t *lock, mmAtomicUInt_t old, mmAtomicUInt_t set)
{
#if defined(__ICC)      //x86 version
    return _InterlockedCompareExchange((void*)lock, set, old) == old;
#elif defined(__ECC)    //IA-64 version
    return _InterlockedCompareExchange((void*)lock, set, old) == old;
#elif defined(__ICL) || defined(_MSC_VER)
    return _InterlockedCompareExchange((volatile long*)(lock), set, old) == old;
#else
    return 0;
#endif
}


static mmInline mmAtomicInt_t
mmAtomicFetchAdd(mmAtomic_t *value, mmAtomicInt_t add)
{
#if defined(__ICC)      //x86 version
    return _InterlockedExchangeAdd((void*)value, add);
#elif defined(__ECC)    //IA-64 version
    return _InterlockedExchangeAdd((void*)value, add);
#elif defined(__ICL) || defined(_MSC_VER)
    return _InterlockedExchangeAdd((volatile long*)(value), add);
#else
    return 0;
#endif
}

#define mmMemoryBarrier() MemoryBarrier()
#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define mmCpuPause() __asm__ ("pause")
#else
#define mmCpuPause()
#endif
