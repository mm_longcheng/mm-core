/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

// https://github.com/aappleby
//
// MurmurHash1 was written by Austin Appleby, and is placed in the public
// domain. The author hereby disclaims copyright to this source code.

#ifndef __mmMurmurHash_h__
#define __mmMurmurHash_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

// MurmurHash1
//-----------------------------------------------------------------------------

MM_EXPORT_DLL uint32_t mmMurmurHash1        ( const void * key, int len, uint32_t seed );
MM_EXPORT_DLL uint32_t mmMurmurHash1Aligned ( const void * key, int len, uint32_t seed );

//-----------------------------------------------------------------------------

// MurmurHash2
//-----------------------------------------------------------------------------

MM_EXPORT_DLL uint32_t mmMurmurHash2        ( const void * key, int len, uint32_t seed );
MM_EXPORT_DLL uint64_t mmMurmurHash64A      ( const void * key, int len, uint64_t seed );
MM_EXPORT_DLL uint64_t mmMurmurHash64B      ( const void * key, int len, uint64_t seed );
MM_EXPORT_DLL uint32_t mmMurmurHash2A       ( const void * key, int len, uint32_t seed );
MM_EXPORT_DLL uint32_t mmMurmurHashNeutral2 ( const void * key, int len, uint32_t seed );
MM_EXPORT_DLL uint32_t mmMurmurHashAligned2 ( const void * key, int len, uint32_t seed );

//-----------------------------------------------------------------------------

// MurmurHash3
//-----------------------------------------------------------------------------

MM_EXPORT_DLL void mmMurmurHash3_x86_32  ( const void * key, int len, uint32_t seed, void * out );

MM_EXPORT_DLL void mmMurmurHash3_x86_128 ( const void * key, int len, uint32_t seed, void * out );

MM_EXPORT_DLL void mmMurmurHash3_x64_128 ( const void * key, int len, uint32_t seed, void * out );

//-----------------------------------------------------------------------------

#include "core/mmSuffix.h"

#endif//__mmMurmurHash_h__
