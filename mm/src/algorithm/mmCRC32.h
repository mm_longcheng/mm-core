/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCRC32_h__
#define __mmCRC32_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/* CRC32 implementation.
*
*  0xFFFFFFFF for initialization.
*/

// polynomial 0x04C11DB7 Base version.
MM_EXPORT_DLL uint32_t mmCRC32B(const void* key, int len, uint32_t seed);

// polynomial 0xEDB88320 Castagnoli More commonly used.
MM_EXPORT_DLL uint32_t mmCRC32C(const void* key, int len, uint32_t seed);

#include "core/mmSuffix.h"

#endif//__mmCRC32_h__
