/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMd5_h__
#define __mmMd5_h__

#include "core/mmTypes.h"
#include "core/mmCoreExport.h"

#include "core/mmPrefix.h"

typedef mmUInt8_t  md5_uint8;
typedef mmUInt32_t md5_uint32;

struct mmMd5Context
{
    md5_uint32 total[2];
    md5_uint32 state[4];
    md5_uint8 buffer[64];
};

MM_EXPORT_DLL void mmMd5_Starts(struct mmMd5Context* p);
MM_EXPORT_DLL void mmMd5_Update(struct mmMd5Context* p, md5_uint8* data, md5_uint32 length);
MM_EXPORT_DLL void mmMd5_Finish(struct mmMd5Context* p, md5_uint8 digest[16]);

#include "core/mmSuffix.h"

#endif//__mmMd5_h__
