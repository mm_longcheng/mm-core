/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

// https://github.com/aappleby
//
/* public api for steve reid's public domain SHA-1 implementation */

#ifndef __mmSHA1_h__
#define __mmSHA1_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

struct mmSHA1Context
{
    uint32_t state[5];
    uint32_t count[2];
    uint8_t  buffer[64];
};

MM_EXPORT_DLL void mmSHA1_Init(struct mmSHA1Context* p);
MM_EXPORT_DLL void mmSHA1_Update(struct mmSHA1Context* p, const uint8_t* data, const size_t len);
MM_EXPORT_DLL void mmSHA1_Final(struct mmSHA1Context* p, uint8_t digest[20]);

#include "core/mmSuffix.h"

#endif//__mmSHA1_h__
