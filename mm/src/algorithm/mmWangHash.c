/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmWangHash.h"

MM_EXPORT_DLL uint32_t mmWangHash32(uint32_t key)
{
    key += ~(key << 15);
    key ^=  (key >> 10);
    key +=  (key <<  3);
    key ^=  (key >>  6);
    key += ~(key << 11);
    key ^=  (key >> 16);
    return key;
}

MM_EXPORT_DLL uint64_t mmWangHash64(uint64_t key)
{
    key = (~key) + (key << 21);               // key = (key << 21) - key - 1;
    key =   key  ^ (key >> 24);
    key = ( key  + (key <<  3)) + (key << 8); // key * 265
    key =   key  ^ (key >> 14);
    key = ( key  + (key <<  2)) + (key << 4); // key * 21
    key =   key  ^ (key >> 28);
    key =   key  + (key << 31);
    return key;
}
