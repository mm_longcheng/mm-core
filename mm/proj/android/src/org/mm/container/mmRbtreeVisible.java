/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.container;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

import org.mm.core.mmSpinlock;

//Suitable for:
//1.multi-threading add and rmv.
//2.multi-threading traverse.
//3.multi-threading get.
//4.allow traverse and main rbtree have little missing member(cache in two list).
@SuppressWarnings({"unused", "Convert2Diamond"})
public class mmRbtreeVisible
{
    public interface mmRbtreeVisibleCallback
    {
        // obj is struct mm_rbtree_visible.
        void Handle(Object obj, Object u, int k, Object v);
    }
    
    public TreeMap<Integer,Object> rbtree_m = new TreeMap<Integer,Object>();// main rbtree.
    public TreeSet<Integer> rbtset_s = new TreeSet<Integer>();// use for traverse
    //
    public LinkedList<Integer> l_a = new LinkedList<Integer>();// cache for add when map_s is busy.
    public LinkedList<Integer> l_r = new LinkedList<Integer>();// cache for add when map_s is busy.
    // 
    public mmSpinlock locker_m = new mmSpinlock();
    public mmSpinlock locker_s = new mmSpinlock();
    public mmSpinlock locker_a = new mmSpinlock();
    public mmSpinlock locker_r = new mmSpinlock();
    //////////////////////////////////////////////////////////////////////////
    public void Init()
    {
        this.rbtree_m.clear();
        this.rbtset_s.clear();
        this.l_a.clear();
        this.l_r.clear();
        this.locker_m.Init();
        this.locker_s.Init();
        this.locker_a.Init();
        this.locker_r.Init();
    }
    public void Destroy()
    {
        this.rbtree_m.clear();
        this.rbtset_s.clear();
        this.l_a.clear();
        this.l_r.clear();
        this.locker_m.Destroy();
        this.locker_s.Destroy();
        this.locker_a.Destroy();
        this.locker_r.Destroy();
    }
    //////////////////////////////////////////////////////////////////////////
    public void Add(int k, Object v)
    {
        this.locker_m.Lock();
        this.rbtree_m.put(k,  v);
        this.locker_m.Unlock();
        //
        if(this.locker_s.Trylock())
        {
            this.TryMergeAdd();
            this.rbtset_s.add(k);
            this.locker_s.Unlock();
        }
        else
        {
            this.locker_a.Lock();
            this.l_a.add(k);
            this.locker_a.Unlock();
        }
    }
    public void Rmv(int k)
    {
        this.locker_m.Lock();
        this.rbtree_m.remove(k);
        this.locker_m.Unlock();
        //
        if(this.locker_s.Trylock())
        {
            this.TryMergeRmv();
            this.rbtset_s.add(k);
            this.locker_s.Unlock();
        }
        else
        {
            this.locker_r.Lock();
            this.l_r.add(k);
            this.locker_r.Unlock();
        }
    }
    @SuppressWarnings("UnusedAssignment")
    public Object Get(Integer k)
    {
        Object e = null;
        this.locker_m.Lock();
        e = this.rbtree_m.get(k);
        this.locker_m.Unlock();
        return e;
    }
    public void Clear()
    {
        this.locker_m.Lock();
        this.rbtree_m.clear();
        this.locker_m.Unlock();
        //
        this.locker_s.Lock();
        this.rbtset_s.clear();
        this.locker_s.Unlock();
        //
        this.locker_a.Lock();
        this.l_a.clear();
        this.locker_a.Unlock();
        //
        this.locker_r.Lock();
        this.l_r.clear();
        this.locker_r.Unlock();
    }
    public int Size()
    {
        return this.rbtree_m.size();
    }
    @SuppressWarnings("UnusedAssignment")
    public void Traver(mmRbtreeVisibleCallback handle, Object u )
    {
        Integer k = 0;
        Object v = null;
        //////////////////////////////////////////////////////////////////////////
        this.locker_s.Lock();
        //////////////////////////////////////////////////////////////////////////
        this.TryMergeAdd();
        this.TryMergeRmv();
        //////////////////////////////////////////////////////////////////////////
        if ( 0 != this.rbtset_s.size() )
        {
            Iterator<Integer> it = null;
            //
            it = this.rbtset_s.iterator();
            while (it.hasNext()) 
            {
                k = it.next();
                v = this.Get(k);
                if (null != v)
                {
                    handle.Handle(this, u, k, v);
                }
            }
        }
        //////////////////////////////////////////////////////////////////////////
        this.locker_s.Unlock();
    }
    //
    @SuppressWarnings("UnusedAssignment")
    private void TryMergeAdd()
    {
        if( 0 != this.l_a.size() )
        {
            if(this.locker_a.Trylock())
            {
                Integer u = null;
                Iterator<Integer> it = null;
                it = this.l_a.iterator();
                while(it.hasNext())
                {
                    u = it.next();
                    this.rbtset_s.add(u);
                    it.remove();
                }
                this.locker_a.Unlock();
            }
        }
    }
    @SuppressWarnings("UnusedAssignment")
    private void TryMergeRmv()
    {
        if( 0!=this.l_r.size() )
        {
            if(this.locker_r.Trylock())
            {
                Integer u = null;
                Iterator<Integer> it = null;
                it = this.l_r.iterator();
                while(it.hasNext())
                {
                    u = it.next();
                    this.rbtset_s.remove(u);
                    it.remove();
                }
                this.locker_r.Unlock();
            }
        }
    }
}
