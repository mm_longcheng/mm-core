/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;

import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;
import org.mm.core.mmThread;
import org.mm.core.mmTime;

//we catch all Exception e but print nothing but catch the error code.

//we not use select timeout for accept interrupt.
//we just use close listen socket fd and trigger a socket errno 
//[errno:(53) Software caused connection abort] to interrupt accept.
//this mode will make this class simpleness and negative effects receivability.
public class mmAccepter
{
    public static final String TAG = mmAccepter.class.getSimpleName();
    
    // listen backlog default 5;
    public static final int MM_ACCEPTER_BACKLOG = 5;
    // if bind listen failure will msleep this time.
    public static final int MM_ACCEPTER_TRY_MSLEEP_TIME = 1000;
    // net tcp nonblock timeout..
    public static final int MM_ACCEPTER_NONBLOCK_TIMEOUT = 1000;
    // net accept one time try time.
    public static final int MM_NET_ACCEPTER_TRYTIME = Integer.MAX_VALUE;
    
    // enum mmAccepterState_t
    public static final int MM_ACCEPTER_STATE_CLOSED = 0;// fd is closed,connect closed.invalid at all.
    public static final int MM_ACCEPTER_STATE_MOTION = 1;// fd is opened,connect closed.at connecting.
    public static final int MM_ACCEPTER_STATE_FINISH = 2;// fd is opened,connect opened.at connected.
    public static final int MM_ACCEPTER_STATE_BROKEN = 3;// fd is opened,connect broken.connect is broken fd not closed.
    
    public static class mmAccepterCallback
    {   
        public void Accept(Object obj, AbstractSelectableChannel fd, mmSockaddr remote){}
        
        Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
    }
    
    // strong ref. accepter socket.
    public mmSocket socket = new mmSocket();
    // value ref. accepter callback.
    public mmAccepterCallback callback = new mmAccepterCallback();
    // listen backlog,default MM_ACCEPTER_BACKLOG.
    public int backlog = MM_ACCEPTER_BACKLOG;
    // thread.
    public mmThread accept_thread = new mmThread();
    // accepter_keepalive_activate true accepter_keepalive_inactive false keepalive the accept fd.
    // default is accepter_keepalive_activate.
    public int keepalive = mmOSSocket.MM_KEEPALIVE_ACTIVATE;
    // tcp nonblock timeout.default is MM_ACCEPTER_NONBLOCK_TIMEOUT milliseconds.
    public int nonblock_timeout = MM_ACCEPTER_NONBLOCK_TIMEOUT;
    // mm_accepter_state_t,default is accepter_state_closed(0)
    public int accepter_state = MM_ACCEPTER_STATE_CLOSED;
    // try bind and listen times.default is MM_NET_ACCEPTER_TRYTIME.
    public int trytimes = MM_NET_ACCEPTER_TRYTIME;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    public int state = mmThread.MM_TS_CLOSED;

    public void Init()
    {
        this.socket.Init();
        this.callback.Init();
        this.backlog = MM_ACCEPTER_BACKLOG;
        this.accept_thread.Init();
        this.keepalive = mmOSSocket.MM_KEEPALIVE_ACTIVATE;
        this.accepter_state = MM_ACCEPTER_STATE_CLOSED;
        this.nonblock_timeout = MM_ACCEPTER_NONBLOCK_TIMEOUT;
        this.trytimes = MM_NET_ACCEPTER_TRYTIME;
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Destroy()
    {
        this.socket.Destroy();
        this.callback.Destroy();
        this.backlog = 0;
        this.accept_thread.Destroy();
        this.keepalive = mmOSSocket.MM_KEEPALIVE_ACTIVATE;
        this.accepter_state = MM_ACCEPTER_STATE_CLOSED;
        this.nonblock_timeout = 0;
        this.trytimes = 0;
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Lock()
    {
        this.socket.Lock();
    }
    public void Unlock()
    {
        this.socket.Unlock();
    }
    // assign native address but not connect.
    public void SetNative(String node, int port)
    {
        this.socket.SetNative(node, port);
    }
    // assign remote address but not connect.
    public void SetRemote(String node, int port)
    {
        this.socket.SetRemote(node, port);
    }
    public void SetCallback(mmAccepterCallback cb)
    {
        // android assertions are unreliable.
        // assert null != cb : "you can not assign null callback.";
        this.callback = cb;
    }
    // reset streambuf and fire event broken.
    public void ApplyBroken()
    {
        this.accepter_state = MM_ACCEPTER_STATE_CLOSED;
    }
    // reset streambuf and fire event broken.
    public void ApplyFinish()
    {
        this.accepter_state = MM_ACCEPTER_STATE_FINISH;
    }
    // fopen socket.ss_native.ss_family,SOCK_STREAM,0
    public void FopenSocket()
    {
        // java special.we must assign the addr type for fopen correct socket struct.
        this.socket.SetAddrType(mmSocket.MM_ADDR_SERVER);
        this.socket.FopenSocket(mmOSSocket.MM_AF_INET6, mmOSSocket.SOCK_STREAM, 0);
        if (mmOSSocket.MM_INVALID_SOCKET != this.socket.socket)
        {
            mmLogger.LogI(TAG + " fopen_socket " + this.socket.ToString() + " success.");
        }
        else
        {
            mmLogger.LogE(TAG + " fopen_socket " + this.socket.ToString() + " failure.");
        }
        if (0 != this.socket.ss_native.port)
        {
            //SO_REUSEADDR
            //  Indicates that the rules used in validating addresses supplied
            //  in a bind(2) call should allow reuse of local addresses.  For
            //  AF_INET sockets this means that a socket may bind, except when
            //  there is an active listening socket bound to the address.
            //  When the listening socket is bound to INADDR_ANY with a
            //  specific port then it is not possible to bind to this port for
            //  any local address.  Argument is an integer boolean flag.
            ServerSocketChannel _socket = (ServerSocketChannel) this.socket.socket;
            if(-1 == mmOSSocket.SetReuseAddress(_socket.socket(), true))
            {
                // the option for SO_REUSEADDR socket flag is optional.
                // this error is not serious.
                mmLogger.LogI(TAG + " reuseaddr " + this.socket.ToString() + " success.");
            }
            else
            {
                mmLogger.LogI(TAG + " reuseaddr " + this.socket.ToString() + " failure.");
            }
        }
    }
    // bind address.
    @SuppressWarnings({"UnusedAssignment", "SingleStatementInBlock", "ConstantConditions"})
    public void Bind()
    {
        mmSocket socket = this.socket;
        //
        if (mmOSSocket.MM_INVALID_SOCKET != socket.socket)
        {
            int _try_times = 0;
            int rt = -1;
            this.accepter_state = MM_ACCEPTER_STATE_MOTION;
            // set socket to blocking.
            socket.SetBlock(mmOSSocket.MM_BLOCKING);
            do 
            {
                _try_times ++;
                rt = socket.Bind();
                if (0 == rt)
                {
                    // bind immediately.
                    break;
                }
                if (mmThread.MM_TS_FINISH == this.state)
                {
                    break;
                }
                else
                {
                    mmLogger.LogE(TAG + " " + "bind " + socket.ToString() + " failure.try times:" + _try_times);
                    mmTime.mmMSleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
                }
            } while ( 0 != rt && _try_times < this.trytimes );
            // set socket to blocking.
            socket.SetBlock(mmOSSocket.MM_BLOCKING);
            if ( 0 != rt )
            {
                mmLogger.LogE(TAG + " " + "bind " + socket.ToString() + " failure.try times:" + _try_times);
                this.ApplyBroken();
                socket.CloseSocket();
            }
            else
            {
             // cache current bind socketaddr into ss_native.
                this.CurrentBindSockaddr();
                // apply finish.
                this.ApplyFinish();
                mmLogger.LogI(TAG + " " + "bind " + socket.ToString() + " success.try times:" + _try_times);
            }
        }
        else
        {
            mmLogger.LogE(TAG + " " + "bind target fd is invalid.");
        }
    }
    // listen address.
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public void Listen()
    {
        mmSocket socket = this.socket;
        //
        if (mmOSSocket.MM_INVALID_SOCKET != socket.socket)
        {
            int _try_times = 0;
            int rt = -1;
            this.accepter_state = MM_ACCEPTER_STATE_MOTION;
            // set socket to blocking.
            socket.SetBlock(mmOSSocket.MM_BLOCKING);
            do 
            {
                _try_times ++;
                rt = socket.Listen(this.backlog);
                if (0 == rt)
                {
                    // listen immediately.
                    break;
                }
                if (mmThread.MM_TS_FINISH == this.state)
                {
                    break;
                }
                else
                {
                    mmLogger.LogE(TAG + " " + "listen " + socket.ToString() + " failure.try times:" + _try_times);
                    mmTime.mmMSleep(MM_ACCEPTER_TRY_MSLEEP_TIME);
                }
            } while (0 != rt && _try_times < this.trytimes);
            // set socket to blocking.
            socket.SetBlock(mmOSSocket.MM_BLOCKING);
            if (0 != rt)
            {
                mmLogger.LogE(TAG + " " + "listen " + socket.ToString() + " failure.try times:" + _try_times);
                this.ApplyBroken();
                socket.CloseSocket();
            }
            else
            {
                this.ApplyFinish();
                mmLogger.LogI(TAG + " " + "listen " + socket.ToString() + " success.try times:" + _try_times);
            }
        }
        else
        {
            mmLogger.LogE(TAG + " " + "listen target fd is invalid.");
        }
    }
    // close socket. mm_accepter_join before call this.
    public void CloseSocket()
    {
        this.socket.CloseSocket();
    }
    // get current bind sockaddr, and cache into ss_native.
    public void CurrentBindSockaddr()
    {
        // we check the port.
        if (0 == this.socket.ss_native.port)
        {
            // if the port for ss_native is zero,we need obtain it manual.
            // synchronize the real bind socket sockaddr.
            this.socket.CurrentBindSockaddr(this.socket.ss_native);
        }
    }
    // accept sync.
    @SuppressWarnings({"UnusedAssignment", "UnnecessaryContinue"})
    public void Accept()
    {
        mmSockaddr remote = new mmSockaddr();
        SocketChannel afd = null;
        // we not need assert the fd is must valid.
        // assert( mm_socket.MM_INVALID_SOCKET != p->addr.fd&&"you must alloc socket fd first.");
        this.state = (mmOSSocket.MM_INVALID_SOCKET == this.socket.socket || mmThread.MM_TS_FINISH == this.state) ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        //
        while(mmThread.MM_TS_MOTION == this.state)
        {
            remote.Reset();
            
            afd = this.socket.Accept();
            if(mmOSSocket.MM_INVALID_SOCKET == afd)
            {
                if(mmErrno.MM_EINTR == mmErrno.GetCode())
                {
                    // MM_EINTR errno:(10004) A blocking operation was interrupted.
                    // this error is not serious.
                    continue;
                }
                else
                {
                    mmLogger.LogI(TAG + " " + this.socket.ToString() + " error occur.");
                    this.state = mmThread.MM_TS_CLOSED;
                    this.accepter_state = MM_ACCEPTER_STATE_BROKEN;
                }
            }
            else
            {
                Socket socket = afd.socket();
                if (mmOSSocket.MM_KEEPALIVE_ACTIVATE == this.keepalive)
                {
                    mmOSSocket.Keepalive(socket, this.keepalive);
                }
                remote.node = socket.getInetAddress().getHostAddress();
                remote.port = socket.getPort();
                this.callback.Accept(this, afd, remote);
                
                String link_name = mmSockaddr.SockaddrNativeRemoteString(this.socket.ss_native, remote, afd.hashCode());
                mmLogger.LogI(TAG + " " + link_name + " connect.");
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////
    //start accept thread.
    public void Start()
    {
        this.state = mmThread.MM_TS_FINISH == this.state ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        this.accept_thread.Start(new Thread(new __Accepter_AcceptThreadRunnable(this)));
    }
    //interrupt accept thread.
    public void Interrupt()
    {
        this.state = mmThread.MM_TS_CLOSED;
        this.socket.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
        this.socket.CloseSocket();
    }
    //shutdown accept thread.
    public void Shutdown()
    {
        this.state = mmThread.MM_TS_FINISH;
        this.socket.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
        this.socket.CloseSocket();
    }
    //join accept thread.
    public void Join()
    {
        this.accept_thread.Join();
    }
    //////////////////////////////////////////////////////////////////////////
    static final class __Accepter_AcceptThreadRunnable implements Runnable
    {
        __Accepter_AcceptThreadRunnable(mmAccepter _p)
        {
            this.p = _p;
        }
        @SuppressWarnings("UnusedAssignment")
        public mmAccepter p = null;
        @Override
        public void run() 
        {
            p.Accept();
        }       
    }
}
