/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.nio.ByteOrder;

import org.mm.core.mmByteBuffer;

// use net byte order.
@SuppressWarnings("unused")
public class mmPacket
{
    // Ethernet len[46~1500] MTU 20 
    // udp head  8 1500 - 20 -  8 = 1472
    // tcp head 20 1500 - 20 - 20 = 1460
    // we define packet page size to 1024.
    public static final int MM_PACKET_PAGE_SIZE = 1024;

    // we not use typedef a new name for this type.
    // because uint16 uint32 is common type when cross language.

    // msg_size    type is mm_uint16_t
    // msg_id      type is mm_uint32_t
    // proxy_id    type is mm_uint32_t
    // session_id  type is mm_uint64_t
    // unique_id   type is mm_uint64_t
    // msg_head    type is mm_uint32_t = 2 * msg_size

    // msg pack struct.
    // |-mm_uint16_t-|-mm_uint16_t-|---char*---|---char*---|

    // use this quick decode encode.
    // |--------mm_uint32_t--------|---char*---|---char*---|

    // |-------------|-------------------------|-----------|
    // |--msg_size---|--head_size--|-head_data-|-body_data-|
    // |---------head_data---------|-head_data-|-body_data-|

    // package_size = 2 * sizeof(mm_msg_size_t) + head_size + body_size

    // package buffer is little endian.
    // https://en.wikipedia.org/wiki/Comparison_of_instruction_set_architectures
    // system devices endian.
    //       ARM android devices is little endian.
    //       ARM iOS     devices is little endian.
    //       x86 desktop devices is little endian.
    // cpu    devices endian.
    //       x86 little.
    //       ARM little/bigger.

    public static final int MM_MSG_SIZE_BIT_NUM = 16;
    public static final int MM_MSG_HEAD_H_MASK = 0xFFFF0000;// hight mask
    public static final int MM_MSG_HEAD_L_MASK = 0x0000FFFF;// low   mask

    // the hsize must use sizeof(T),make sure the package byte aligned.

    // msg base head size.
    // mm_uint32_t   mid;// msg     id.
    // byte aligned this struct sizeof is 4.
    public static final int MM_MSG_BASE_HEAD_SIZE = 4;
    // mm_uint32_t   mid;// msg     id.
    // mm_uint32_t   pid;// proxy   id.
    // mm_uint64_t   sid;// section id.
    // mm_uint64_t   uid;// unique  id.
    // byte aligned this struct sizeof is 24.
    public static final int MM_MSG_COMM_HEAD_SIZE = 24;

    public static class mmPacketHead
    {
        // msg base head.include into head data.
        public int    mid = 0;// msg     id.
        public int    pid = 0;// proxy   id.
        public long   sid = 0;// section id.
        public long   uid = 0;// unique  id.
        
        public void Init()
        {
            this.mid = 0;
            this.pid = 0;
            this.sid = 0;
            this.uid = 0;
        }
        public void Destroy()
        {
            this.mid = 0;
            this.pid = 0;
            this.sid = 0;
            this.uid = 0;
        }
        public void Reset()
        {
            this.mid = 0;
            this.pid = 0;
            this.sid = 0;
            this.uid = 0;
        }
        
        public void CopyFrom(mmPacketHead q)
        {
            this.mid = q.mid;
            this.pid = q.pid;
            this.sid = q.sid;
            this.uid = q.uid;
        }
    }
    
    // packet struct.
    public mmPacketHead phead = new mmPacketHead();// packet head.
    public mmByteBuffer hbuff = new mmByteBuffer();// buffer for head.
    public mmByteBuffer bbuff = new mmByteBuffer();// buffer for body.
    //////////////////////////////////////////////////////////////////////////
    public void Init()
    {
        this.phead.Init();
        this.hbuff.Init();
        this.bbuff.Init();
    }
    //////////////////////////////////////////////////////////////////////////
    public void Destroy()
    {
        this.phead.Destroy();
        this.hbuff.Destroy();
        this.bbuff.Destroy();
    }
    // reset all to zero.
    public void Reset()
    {
        this.phead.Reset();
        this.hbuff.Reset();
        this.bbuff.Reset();
    }
    // use for quick zero head base.(mid,uid,sid,pid)
    public void HeadBaseZero()
    {
        this.phead.Reset();
    }
    // use for quick copy head base r-->p.(mid,uid,sid,pid)
    public void HeadBaseCopy(mmPacket r)
    {
        this.phead = r.phead;
    }
    // decode p phead to hbuff.phead is cache.
    public void HeadDecode(mmByteBuffer hbuff, mmPacketHead phead)
    {
        byte[] buffer = new byte[MM_MSG_COMM_HEAD_SIZE];
        System.arraycopy(hbuff.buffer, hbuff.offset, buffer, 0, hbuff.length);
        java.nio.ByteBuffer byte_buffer = java.nio.ByteBuffer.wrap(buffer);
        // use net byte order. little endian.
        byte_buffer.order(ByteOrder.LITTLE_ENDIAN);     
        this.phead.mid = byte_buffer.getInt();
        this.phead.pid = byte_buffer.getInt();
        this.phead.sid = byte_buffer.getLong();
        this.phead.uid = byte_buffer.getLong();
    }
    // decode hbuff phead to p.phead is cache.
    public void HeadEncode(mmByteBuffer hbuff, mmPacketHead phead)
    {
        byte[] buffer = new byte[MM_MSG_COMM_HEAD_SIZE];
        java.nio.ByteBuffer byte_buffer = java.nio.ByteBuffer.wrap(buffer);
        // use net byte order. little endian.
        byte_buffer.order(ByteOrder.LITTLE_ENDIAN); 
        byte_buffer.putInt(this.phead.mid);
        byte_buffer.putInt(this.phead.pid);
        byte_buffer.putLong(this.phead.sid);
        byte_buffer.putLong(this.phead.uid);
        System.arraycopy(buffer, 0, hbuff.buffer, hbuff.offset, hbuff.length);
    }
    //////////////////////////////////////////////////////////////////////////
    // copy data from p to t.will realloc the hbuff and bbuff.
    public void CopyRealloc(mmPacket t)
    {
        // android assertions are unreliable.
        // assert null != t.hbuff.buffer : "t.hbuff.buffer is null.";
        // assert null != t.bbuff.buffer : "t.bbuff.buffer is null.";       
        t.hbuff.buffer = new byte[this.hbuff.length];
        t.bbuff.buffer = new byte[this.bbuff.length];       
        this.CopyWeak(t);
    }
    // copy data from p to t.will alloc the hbuff and bbuff.
    public void CopyAlloc(mmPacket t)
    {
        // android assertions are unreliable.
        // assert null != t.hbuff.buffer : "t.hbuff.buffer is null.";
        // assert null != t.bbuff.buffer : "t.bbuff.buffer is null.";       
        t.hbuff.buffer = new byte[this.hbuff.length];
        t.bbuff.buffer = new byte[this.bbuff.length];       
        this.CopyWeak(t);
    }
    // copy data from p to t.not realloc the hbuff and bbuff.
    public void CopyWeak(mmPacket t)
    {
        mmPacketHead phead = new mmPacketHead();
        t.hbuff.length = this.hbuff.length;
        t.bbuff.length = this.bbuff.length;
        System.arraycopy(this.hbuff.buffer,this.hbuff.offset,t.hbuff.buffer,t.hbuff.offset,this.hbuff.length);
        System.arraycopy(this.bbuff.buffer,this.bbuff.offset,t.bbuff.buffer,t.bbuff.offset,this.bbuff.length);
        t.HeadDecode(t.hbuff, phead);
    }
    // free copy realloc data from.
    public void FreeCopyRealloc()
    {
        this.hbuff.buffer = null;
        this.bbuff.buffer = null;
        this.Reset();
    }
    // free copy alloc data from.
    public void FreeCopyAlloc()
    {
        this.hbuff.buffer = null;
        this.bbuff.buffer = null;
        this.Reset();
    }
    //////////////////////////////////////////////////////////////////////////
    public void ShadowAlloc()
    {
        // android assertions are unreliable.
        // assert null == this.hbuff.buffer : "this.hbuff.buffer is not null.";
        // assert null == this.bbuff.buffer : "this.bbuff.buffer is not null.";
        this.hbuff.buffer = new byte[this.hbuff.length];
        this.bbuff.buffer = new byte[this.bbuff.length];    
    }
    public void ShadowFree()
    {
        this.hbuff.buffer = null;
        this.bbuff.buffer = null;
    }
    //////////////////////////////////////////////////////////////////////////
    // packet handle function type.
    public interface TcpCallBack
    {  
        void HandleTcp(Object obj, mmPacket pack);
    }
    // packet handle function type.
    public interface UdpCallBack
    {  
        void HandleUdp(Object obj, mmPacket pack, mmSockaddr remote);
    } 
    //////////////////////////////////////////////////////////////////////////
}
