/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.nio.ByteOrder;

import org.mm.core.mmLogger;
import org.mm.core.mmTypes;
import org.mm.net.mmPacket.mmPacketHead;

@SuppressWarnings("unused")
public class mmBufferPacket
{
    public static final String TAG = mmBufferPacket.class.getSimpleName();
    
    // packet handle tcp buffer and length for string handle only one packet.
    @SuppressWarnings("UnusedAssignment")
    public static int HandleTcp(byte[] buffer, int offset, int length, mmPacket.TcpCallBack handle, Object obj)
    {
        int code = 0;
        mmPacket pack = new mmPacket();
        mmPacketHead phead = new mmPacketHead();
        // cache s.
        byte[] s = new byte[mmTypes.MM_INT32_SIZE];
        short msg_size = 0;
        int buff_size = 0;
        //
        buff_size = length;
        System.arraycopy(buffer, offset, s, 0, mmTypes.MM_INT32_SIZE);
        java.nio.ByteBuffer byte_buffer = java.nio.ByteBuffer.wrap(s);
        // use net byte order. little endian.
        byte_buffer.order(ByteOrder.LITTLE_ENDIAN); 
        msg_size = byte_buffer.getShort();
        if (buff_size == msg_size)
        {
            // here we just use the weak ref from streambuf.for data recv callback.
            pack.hbuff.length = byte_buffer.getShort();
            pack.hbuff.buffer = buffer;
            pack.hbuff.offset = offset + mmTypes.MM_INT32_SIZE;
            //
            pack.bbuff.length = msg_size - mmTypes.MM_INT32_SIZE - pack.hbuff.length;
            pack.bbuff.buffer = buffer;
            pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
            // check buffer.
            if (msg_size - mmTypes.MM_INT32_SIZE < pack.hbuff.length || mmPacket.MM_MSG_COMM_HEAD_SIZE < pack.hbuff.length)
            {
                mmLogger.LogE(TAG + " invalid tcp message buffer.");
                code = -1;
            }
            else
            {
                pack.HeadDecode(pack.hbuff, phead);
                //
                // fire the field buffer recv event.
                handle.HandleTcp(obj,pack);
            }
        }
        else
        {
            mmLogger.LogW(TAG + " data size is invalid.buff_size:" + buff_size + " msg_size:" + msg_size);
        }
        return code;
    }
    @SuppressWarnings("UnusedAssignment")
    public static int HandleUdp(byte[] buffer, int offset, int length, mmPacket.UdpCallBack handle, Object obj, mmSockaddr remote)
    {
        int code = 0;
        mmPacket pack = new mmPacket();
        mmPacketHead phead = new mmPacketHead();
        byte[] s = new byte[mmTypes.MM_INT32_SIZE];
        short msg_size = 0;
        int buff_size = 0;
        //
        buff_size = length;
        System.arraycopy(buffer, offset, s, 0, mmTypes.MM_INT32_SIZE);
        java.nio.ByteBuffer byte_buffer = java.nio.ByteBuffer.wrap(s);
        // use net byte order. little endian.
        byte_buffer.order(ByteOrder.LITTLE_ENDIAN); 
        msg_size = byte_buffer.getShort();
        if (buff_size == msg_size)
        {
            // here we just use the weak ref from streambuf.for data recv callback.
            pack.hbuff.length = byte_buffer.getShort();
            pack.hbuff.buffer = buffer;
            pack.hbuff.offset = offset + mmTypes.MM_INT32_SIZE;
            //
            pack.bbuff.length = msg_size - mmTypes.MM_INT32_SIZE - pack.hbuff.length;
            pack.bbuff.buffer = buffer;
            pack.bbuff.offset = pack.hbuff.offset + pack.hbuff.length;
            // check buffer.
            if (msg_size - mmTypes.MM_INT32_SIZE < pack.hbuff.length || mmPacket.MM_MSG_COMM_HEAD_SIZE < pack.hbuff.length)
            {
                mmLogger.LogE(TAG + " invalid udp message buffer.");
                code = -1;
            }
            else
            {
                pack.HeadDecode(pack.hbuff, phead);
                //
                // fire the field buffer recv event.
                handle.HandleUdp(obj, pack, remote);
            }
        }
        else
        {
            mmLogger.LogW(TAG + " data size is invalid.buff_size:" + buff_size + " msg_size:" + msg_size);
        }
        return code;
    }
}
