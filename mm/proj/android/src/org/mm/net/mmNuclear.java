/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;

import org.mm.container.mmRbtreeVisible;
import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;
import org.mm.core.mmSpinlock;
import org.mm.core.mmThread;
import org.mm.poller.mmPoll;
import org.mm.poller.mmPollEvent;

@SuppressWarnings("unused")
public class mmNuclear
{
    public static final String TAG = mmNuclear.class.getSimpleName();
    
    // nuclear default poll length.
    public static final int MM_NUCLEAR_POLL_LENGTH = 256;
    // nuclear default poll idle sleep milliseconds.
    public static final int MM_NUCLEAR_IDLE_SLEEP_MSEC = 200;
    
    public static class mmNuclearCallback
    {       
        public void HandleRecv(Object obj, Object u){}
        public void HandleSend(Object obj, Object u){}
        
        public Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
    }

    // rb tree for fd <--> addr.
    public mmRbtreeVisible rbtree_visible = new mmRbtreeVisible();
    
    public mmNuclearCallback callback = new mmNuclearCallback();
    
    public mmPoll poll = new mmPoll();
    
    public mmThread poll_thread = new mmThread();
    public mmThread.Mutex cond_locker = new mmThread.Mutex();
    public mmThread.Cond cond_not_null = new mmThread.Cond(this.cond_locker);
    public mmThread.Cond cond_not_full = new mmThread.Cond(this.cond_locker);
    public mmSpinlock locker = new mmSpinlock();
    
    // set poll len for mm_poll_wait.default is MM_NUCLEAR_POLL_LEN.set it before wait,or join_wait after.
    public int poll_length = MM_NUCLEAR_POLL_LENGTH;// poll length.
    // set poll timeout interval.-1 is forever.default is MM_nuclear_IDLE_SLEEP_MSEC.
    // if forever,you must resolve break block state while poll wait,use pipeline,or other way.
    // poll timeout.
    public int poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
    // addr size.we not use the rbtree_visible size.we here need lock free and quick data struct.
    public int size = 0;
    // max addr size.default is -1;
    public int max_size = Integer.MAX_VALUE;
    
    public int state = mmThread.MM_TS_CLOSED;// mm_thread_state_t,default is MM_TS_CLOSED(0)
    
    public void Init()
    {
        this.rbtree_visible.Init();
        this.callback.Init();
        this.poll.Init();
        this.poll_thread.Init();
        this.cond_locker.Init();
        this.cond_not_null.Init();
        this.cond_not_full.Init();
        this.locker.Init();     
        this.poll_length = MM_NUCLEAR_POLL_LENGTH;
        this.poll_timeout = MM_NUCLEAR_IDLE_SLEEP_MSEC;
        this.size = 0;
        this.max_size = Integer.MAX_VALUE;
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Destroy()
    {
        this.rbtree_visible.Destroy();
        this.callback.Destroy();
        this.poll.Destroy();
        this.poll_thread.Destroy();
        this.cond_locker.Destroy();
        this.cond_not_null.Destroy();
        this.cond_not_full.Destroy();
        this.locker.Destroy();  
        this.poll_length = 0;
        this.poll_timeout = 0;
        this.size = 0;
        this.max_size = 0;      
        this.state = mmThread.MM_TS_CLOSED;
    }
    // lock to make sure the knot is thread safe.
    public void Lock()
    {
        this.locker.Lock();
    }
    // unlock knot.
    public void Unlock()
    {
        this.locker.Unlock();
    }
    public void SetCallback(mmNuclearCallback callback)
    {
        // android assertions are unreliable.
        // assert null != callback : "you can not assign null callback.";
        this.callback = callback;
    }
    // poll size.
    public int PollSize()
    {
        return this.size;
    }
    // wait for activation fd.
    @SuppressWarnings("UnusedAssignment")
    public void PollWait()
    {
        mmPollEvent pe = null;
        int real_len = 0;
        int i = 0;
        while(mmThread.MM_TS_MOTION == this.state)
        {
            // the first quick size checking.
            if (0 == this.size)
            {
                this.cond_locker.Lock();
                // double lock checking.
                // because the first quick size checking is not thread safe.
                // here we lock and check the size once again.
                if (0 == this.size)
                {
                    mmLogger.LogI(TAG + " " + this.hashCode() + " " + "wait.");
                    this.cond_not_null.Await();
                    mmLogger.LogI(TAG + " " + this.hashCode() + " " + "wake.");
                }
                this.cond_locker.Unlock();
                continue;
            }
            real_len = this.poll.WaitEvent(this.poll_timeout);
            if(0 == real_len)
            {
                continue;
            }
            else if(-1 == real_len)
            {
                int errcode = mmErrno.GetCode();
                if (mmErrno.MM_ENOTSOCK == errcode)
                {
                    mmLogger.LogE(TAG + " " + this.hashCode() + " error occur.");
                    break;
                }
                else
                {
                    mmLogger.LogE(TAG + " " + this.hashCode() + " error occur.");
                    continue;
                }
            }
            for (i = 0;i < real_len;++i)
            {
                pe = this.poll.pe.get(i);
                // android assertions are unreliable.
                // assert null != pe.s : "pe.s is a null.";
                //
                if (0 != (pe.mask & mmPollEvent.MM_PE_READABLE))
                {
                    this.callback.HandleRecv(this, pe.s);
                }
                if (0 != (pe.mask & mmPollEvent.MM_PE_WRITABLE))
                {
                    this.callback.HandleSend(this, pe.s);
                }
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////
    //start wait thread.
    public void Start()
    {
        this.state = mmThread.MM_TS_FINISH == this.state ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        this.poll_thread.Start(new __Nuclear_WaitThreadRunnable(this));
    }
    //interrupt wait thread.
    public void Interrupt()
    {
        this.state = mmThread.MM_TS_CLOSED;
        this.cond_locker.Lock();
        this.cond_not_null.Signal();
        this.cond_not_full.Signal();
        this.cond_locker.Unlock();
    }
    //shutdown wait thread.
    public void Shutdown()
    {
        this.state = mmThread.MM_TS_FINISH;
        this.cond_locker.Lock();
        this.cond_not_null.Signal();
        this.cond_not_full.Signal();
        this.cond_locker.Unlock();
    }
    //join wait thread.
    public void Join()
    {
        this.poll_thread.Join();
    }
    //////////////////////////////////////////////////////////////////////////
    //traver member for nuclear.
    public void Traver(mmRbtreeVisible.mmRbtreeVisibleCallback handle, Object u)
    {
        this.rbtree_visible.Traver(handle, u);
    }
    //////////////////////////////////////////////////////////////////////////
    // add u into poll_fd.you must add rmv in pairs.
    @SuppressWarnings("ConstantConditions")
    public void FdAdd(AbstractSelectableChannel fd, Object u)
    {
        do
        {
            if (mmOSSocket.MM_INVALID_SOCKET == fd)
            {
                // the socket is invalid.do nothing.
                break;
            }
            //
            if (this.max_size <= this.size)
            {
                // pthread_cond_wait(&p->cond_not_null,&p->locker);
                // nothing need remove and not need cond wait at all.
                mmLogger.LogT(TAG + " we can not fd add into full poll.");
                break;
            }
            this.poll.AddEvent(fd, mmPollEvent.MM_PE_READABLE, u);
            // add addr to rbt.
            this.cond_locker.Lock();
            // add fd u.
            this.rbtree_visible.Add(fd.hashCode(), u);
            this.size ++;
            this.cond_not_null.Signal();
            this.cond_locker.Unlock();  
        }while(false);  
    }
    // rmv u from poll_fd.you must add rmv in pairs.
    @SuppressWarnings("ConstantConditions")
    public void FdRmv(AbstractSelectableChannel fd, Object u)
    {
        do
        {
            if (mmOSSocket.MM_INVALID_SOCKET == fd)
            {
                // the socket is invalid.do nothing.
                break;
            }
            //
            if (0 >= this.size)
            {
                // pthread_cond_wait(&p->cond_not_full,&p->locker);
                // nothing need remove and not need cond wait at all.
                mmLogger.LogT(TAG + " we can not fd rmv from null poll.");
                break;
            }
            this.poll.DelEvent(fd, mmPollEvent.MM_PE_READABLE | mmPollEvent.MM_PE_WRITABLE, u);
            // rmv addr from rbt.
            this.cond_locker.Lock();
            // rmv fd u.
            this.rbtree_visible.Rmv(fd.hashCode());
            this.size --;
            this.cond_not_full.Signal();
            this.cond_locker.Unlock();  
        }while(false);
    }
    // mod u event from poll wait.
    public void FdMod(AbstractSelectableChannel fd, Object u, int flag)
    {
        this.poll.ModEvent(fd, flag, u);
    }
    // get u event from poll wait.
    @SuppressWarnings("UnusedAssignment")
    public Object FdGet(AbstractSelectableChannel fd)
    {
        Object e = null;
        this.cond_locker.Lock();
        e = this.rbtree_visible.Get(fd.hashCode());
        this.cond_locker.Unlock();
        return e;
    }
    // add u into poll_fd.not cond and locker.you must lock it outside manual.
    public void FdAddPoll(AbstractSelectableChannel fd, Object u)
    {
        this.poll.AddEvent(fd, mmPollEvent.MM_PE_READABLE,u);
        // add fd u.
        this.rbtree_visible.Add(fd.hashCode(), u);
        this.size ++;
    }
    // rmv u from poll_fd..not cond and locker.you must lock it outside manual.
    public void FdRmvPoll(AbstractSelectableChannel fd, Object u)
    {
        this.poll.DelEvent(fd, mmPollEvent.MM_PE_READABLE | mmPollEvent.MM_PE_WRITABLE, u);
        // rmv fd u.
        this.rbtree_visible.Rmv(fd.hashCode());
        this.size --;
    }
    // signal.
    public void FdPollSignal()
    {
        this.cond_locker.Lock();
        this.cond_not_null.Signal();
        this.cond_not_full.Signal();
        this.cond_locker.Unlock();
    }
    //////////////////////////////////////////////////////////////////////////
    static final class __Nuclear_WaitThreadRunnable implements Runnable
    {
        __Nuclear_WaitThreadRunnable(mmNuclear _p)
        {
            this.p = _p;
        }
        @SuppressWarnings("UnusedAssignment")
        public mmNuclear p = null;
        @Override
        public void run()
        {
            this.p.PollWait();
        }
    }
}
