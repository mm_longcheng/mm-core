/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import org.mm.core.mmByteBuffer;
import org.mm.core.mmStreambuf;
import org.mm.net.mmPacket.mmPacketHead;

@SuppressWarnings("unused")
public class mmMessageCoder
{
    public int Length(Object message) { return 0; }
    public int Decode(Object message, mmByteBuffer byte_buffer){ return 0; }
    public int Encode(Object message, mmByteBuffer byte_buffer){ return 0; }
    
    public Object obj = null;// weak ref. user data for callback.
    
    public void Init()
    {
        this.obj = null;
    }
    public void Destroy()
    {
        this.obj = null;
    }
    
    private static class mmMessageCoderByteBuffer extends mmMessageCoder
    {
        public int Length(Object message) 
        {
            mmByteBuffer message_byte_buffer = (mmByteBuffer)message;
            return message_byte_buffer.length;
        }
        public int Decode(Object message, mmByteBuffer byte_buffer)
        {
            mmByteBuffer message_byte_buffer = (mmByteBuffer)message;
            System.arraycopy(
                    byte_buffer.buffer, byte_buffer.offset, 
                    message_byte_buffer.buffer, message_byte_buffer.offset, 
                    byte_buffer.length);
            return 0;
        }
        public int Encode(Object message, mmByteBuffer byte_buffer)
        {
            mmByteBuffer message_byte_buffer = (mmByteBuffer)message;
            System.arraycopy(
                    message_byte_buffer.buffer, message_byte_buffer.offset, 
                    byte_buffer.buffer, byte_buffer.offset,                     
                    byte_buffer.length);
            return 0;
        }
    }
    
    private static mmMessageCoder g_mmMessageCoder_ByteBuffer = new mmMessageCoderByteBuffer();
    
    private static class mmMessageCoderStreambuf extends mmMessageCoder
    {
        public int Length(Object message) 
        {
            mmStreambuf message_streambuf = (mmStreambuf)message;
            return message_streambuf.Size();
        }
        public int Decode(Object message, mmByteBuffer byte_buffer)
        {
            mmStreambuf message_streambuf = (mmStreambuf)message;
            System.arraycopy(
                    byte_buffer.buffer, byte_buffer.offset, 
                    message_streambuf.buff, message_streambuf.gptr, 
                    byte_buffer.length);
            return 0;
        }
        public int Encode(Object message, mmByteBuffer byte_buffer)
        {
            mmStreambuf message_streambuf = (mmStreambuf)message;
            System.arraycopy(
                    message_streambuf.buff, message_streambuf.gptr, 
                    byte_buffer.buffer,byte_buffer.offset,                     
                    byte_buffer.length);
            return 0;
        }
    }
    
    private static mmMessageCoder g_mmMessageCoder_Streambuf = new mmMessageCoderStreambuf();
    
    public static mmMessageCoder ByteBuffer()
    {
        return g_mmMessageCoder_ByteBuffer;
    }    
    public static mmMessageCoder Streambuf()
    {
        return g_mmMessageCoder_Streambuf;
    }
    
    public static void StreambufOverdraft(
        mmStreambuf streambuf,
        int hlength,
        int blength,
        mmPacket pack)
    {
        // message encode length
        pack.hbuff.length = hlength;
        pack.bbuff.length = blength;
        // alloc message encode buffer
        mmStreambufPacket.Overdraft(streambuf, pack);
    }

    @SuppressWarnings("UnusedAssignment")
    public static int AppendImplPacketHead(
        mmStreambuf streambuf,
        mmMessageCoder coder,
        mmPacketHead packet_head,
        Object rq_msg,
        int hlength,
        mmPacket rq_pack)
    {
        int rt = -1;
        mmPacketHead phead = new mmPacketHead();
        // message packet head init.
        rq_pack.HeadBaseZero();
        // message packet head assignment
        rq_pack.phead.CopyFrom(packet_head);
        // message packet head encode
        rq_pack.HeadEncode(rq_pack.hbuff, phead);
        // encode message
        rt = coder.Encode(rq_msg, rq_pack.bbuff);
        return rt;
    }

    @SuppressWarnings("UnusedAssignment")
    public static int AppendImplHeadZero(
        mmStreambuf streambuf,
        mmMessageCoder coder,
        long uid,
        int mid,
        Object rq_msg,
        int hlength,
        mmPacket rq_pack)
    {
        int rt = -1;
        mmPacketHead phead = new mmPacketHead();
        // message packet head init.
        rq_pack.HeadBaseZero();
        // message packet head assignment
        rq_pack.phead.mid = mid;
        rq_pack.phead.pid = 0;
        rq_pack.phead.sid = 0;
        rq_pack.phead.uid = uid;
        // message packet head encode
        rq_pack.HeadEncode(rq_pack.hbuff, phead);
        // encode message
        rt = coder.Encode(rq_msg, rq_pack.bbuff);
        return rt;
    }

    @SuppressWarnings("UnusedAssignment")
    public static int AppendImplHeadCopy(
        mmStreambuf streambuf,
        mmMessageCoder coder,
        int mid,
        Object rs_msg,
        mmPacket rq_pack,
        mmPacket rs_pack)
    {
        int rt = -1;
        mmPacketHead phead = new mmPacketHead();
        // message packet head init.
        rs_pack.HeadBaseCopy(rq_pack);
        // message packet head assignment
        rs_pack.phead.mid = mid;
        // message packet head encode
        rs_pack.HeadEncode(rs_pack.hbuff, phead);
        // encode message
        rt = coder.Encode(rs_msg, rs_pack.bbuff);
        return rt;
    }

    public static int AppendImplHeadCopyProxyRq(
        mmStreambuf streambuf,
        int pid,
        long sid,
        mmPacket rq_pack,
        mmPacket pr_pack)
    {
        mmPacketHead phead = new mmPacketHead();
        // message packet head init.
        pr_pack.HeadBaseCopy(rq_pack);
        // message packet head assignment
        pr_pack.phead.pid = pid;
        pr_pack.phead.sid = sid;
        // message packet head encode
        pr_pack.HeadEncode(pr_pack.hbuff, phead);
        // encode message
        System.arraycopy(
                rq_pack.bbuff.buffer, rq_pack.bbuff.offset, 
                pr_pack.bbuff.buffer, pr_pack.bbuff.offset,                     
                rq_pack.bbuff.length);
        return 0;
    }

    public static int AppendImplHeadCopyProxyRs(
        mmStreambuf streambuf,
        mmPacket rs_pack,
        mmPacket pr_pack)
    {
        mmPacketHead phead = new mmPacketHead();
        // message packet head init.
        pr_pack.HeadBaseCopy(rs_pack);
        // message packet head assignment
        // message packet head encode
        pr_pack.HeadEncode(pr_pack.hbuff, phead);
        // encode message
        System.arraycopy(
                rs_pack.bbuff.buffer, rs_pack.bbuff.offset, 
                pr_pack.bbuff.buffer, pr_pack.bbuff.offset,                     
                rs_pack.bbuff.length);
        return 0;
    }

    public static int AppendPacketHead(
        mmStreambuf streambuf,
        mmMessageCoder coder,
        mmPacketHead packet_head,
        Object rq_msg,
        int hlength,
        mmPacket rq_pack)
    {
        int blength = coder.Length(rq_msg);
        mmMessageCoder.StreambufOverdraft(streambuf, hlength, blength, rq_pack);
        return mmMessageCoder.AppendImplPacketHead(streambuf, coder, packet_head, rq_msg, hlength, rq_pack);
    }

    public static int AppendHeadZero(
        mmStreambuf streambuf,
        mmMessageCoder coder,
        long uid,
        int mid,
        Object rq_msg,
        int hlength,
        mmPacket rq_pack)
    {
        int blength = coder.Length(rq_msg);
        mmMessageCoder.StreambufOverdraft(streambuf, hlength, blength, rq_pack);
        return mmMessageCoder.AppendImplHeadZero(streambuf, coder, uid, mid, rq_msg, hlength, rq_pack);
    }

    public static int AppendHeadCopy(
        mmStreambuf streambuf,
        mmMessageCoder coder,
        int mid,
        Object rs_msg,
        mmPacket rq_pack,
        mmPacket rs_pack)
    {
        int hlength = rq_pack.hbuff.length;
        int blength = coder.Length(rs_msg);
        mmMessageCoder.StreambufOverdraft(streambuf, hlength, blength, rs_pack);
        return mmMessageCoder.AppendImplHeadCopy(streambuf, coder, mid, rs_msg, rq_pack, rs_pack);
    }

    public static int AppendHeadCopyProxyRq(
        mmStreambuf streambuf,
        int pid,
        long sid,
        mmPacket rq_pack,
        mmPacket pr_pack)
    {
        int hlength = rq_pack.hbuff.length;
        int blength = rq_pack.bbuff.length;
        mmMessageCoder.StreambufOverdraft(streambuf, hlength, blength, pr_pack);
        return mmMessageCoder.AppendImplHeadCopyProxyRq(streambuf, pid, sid, rq_pack, pr_pack);
    }

    public static int AppendHeadCopyProxyRs(
        mmStreambuf streambuf,
        mmPacket rs_pack,
        mmPacket pr_pack)
    {
        int hlength = rs_pack.hbuff.length;
        int blength = rs_pack.bbuff.length;
        mmMessageCoder.StreambufOverdraft(streambuf, hlength, blength, pr_pack);
        return mmMessageCoder.AppendImplHeadCopyProxyRs(streambuf, rs_pack, pr_pack);
    }

    public static int EncodeMessageByteBuffer(
        mmStreambuf streambuf,
        mmByteBuffer message,
        mmPacketHead packet_head,
        int hlength,
        mmPacket pack)
    {
        return mmMessageCoder.AppendPacketHead(
                streambuf, 
                g_mmMessageCoder_ByteBuffer, 
                packet_head, 
                message, 
                hlength, 
                pack);
    }

    public static int EncodeMessageStreambuf(
        mmStreambuf streambuf,
        mmStreambuf message,
        mmPacketHead packet_head,
        int hlength,
        mmPacket pack)
    {
        return mmMessageCoder.AppendPacketHead(
                streambuf, 
                g_mmMessageCoder_Streambuf, 
                packet_head, 
                message, 
                hlength, 
                pack);
    }
}
