/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;
import org.mm.core.mmSpinlock;
import org.mm.core.mmStreambuf;

@SuppressWarnings("unused")
public class mmTcp
{
    public static final String TAG = mmTcp.class.getSimpleName();
    
    // enum mmTcpMid_t
    public static final int MM_TCP_MID_BROKEN = 0x0F000000;
    public static final int MM_TCP_MID_NREADY = 0x0F000001;
    public static final int MM_TCP_MID_FINISH = 0x0F000002;
    
    @SuppressWarnings("UnusedReturnValue")
    public static class mmTcpCallback
    {
        public int Handle(Object obj, byte[] buffer, int offset, int length){ return 0; }
        public int Broken(Object obj ){ return 0; }
        public Object obj = null;// weak ref. user data for callback.
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
        public void Reset()
        {
            this.obj = null;
        }
        @Override  
        public Object clone()  
        {  
            mmTcpCallback o = null;
            try 
            {
                o = (mmTcpCallback)super.clone();
            } 
            catch (CloneNotSupportedException e) 
            {
                // print stack trace.
                e.printStackTrace();
            }
            if(null != o)
            {
                o.obj = this.obj;
            }
            return o;
        } 
    }

    // strong ref. tcp socket. 
    public mmSocket socket = new mmSocket();
    // strong ref. locker_i.
    public mmStreambuf buff_recv = new mmStreambuf();
    // strong ref. locker_o.
    public mmStreambuf buff_send = new mmStreambuf();
    // value ref. transport callback.
    public mmTcpCallback callback = new mmTcpCallback();
    // external locker, locker_i.
    public mmSpinlock locker_i = new mmSpinlock();
    // external locker, locker_o.
    public mmSpinlock locker_o = new mmSpinlock();  
    // feedback unique_id.
    public long unique_id = 0;
    // user data.
    // most of time, the data is lock free.
    // s_locker will lock this data.
    public Object u = null;
    
    public void Init()
    {
        this.socket.Init();
        this.buff_recv.Init();
        this.buff_send.Init();
        this.callback.Init();
        this.locker_i.Init();
        this.locker_o.Init();
        this.unique_id = 0;
        this.u = null;
    }
    public void Destroy()
    {
        this.socket.Destroy();
        this.buff_recv.Destroy();
        this.buff_send.Destroy();
        this.callback.Destroy();
        this.locker_i.Destroy();
        this.locker_o.Destroy();
        this.unique_id = 0;
        this.u = null;
    }
    public void Lock()
    {
        this.socket.Lock();
        this.locker_i.Lock();
        this.locker_o.Lock();
    }
    public void Unlock()
    {
        this.locker_o.Unlock();
        this.locker_i.Unlock();
        this.socket.Unlock();
    }
    public void SLock()
    {
        this.socket.Lock();
    }
    public void SUlock()
    {
        this.socket.Unlock();
    }
    public void ILock()
    {
        this.locker_i.Lock();
    }
    public void IUnlock()
    {
        this.locker_i.Unlock();
    }
    public void OLock()
    {
        this.locker_o.Lock();
    }
    public void OUnlock()
    {
        this.locker_o.Unlock();
    }
    public void Reset()
    {
        this.socket.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
        this.socket.Reset();
        this.buff_recv.Reset();
        this.buff_send.Reset();
        this.callback.Reset();
        // spinlock not reset.
        this.unique_id = 0;
        this.u = null;
    }
    // tcp streambuf recv send reset.
    public void ResetStreambuf()
    {
        this.buff_recv.Reset();
        this.buff_send.Reset();
    }
    // assign addr native by ip port.
    public void SetNative(String node, int port)
    {
        this.socket.SetNative(node, port);
    }
    // assign addr remote by ip port.
    public void SetRemote(String node, int port)
    {
        this.socket.SetRemote(node, port);
    }
    // assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
    public void SetNativeStorage(mmSockaddr ss_native)
    {
        this.socket.SetNativeStorage(ss_native);
    }
    // assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
    public void SetRemoteStorage(mmSockaddr ss_remote)
    {
        this.socket.SetRemoteStorage(ss_remote);
    }
    //////////////////////////////////////////////////////////////////////////
    public void SetCallback(mmTcpCallback cb)
    {
        assert null != cb : "you can not assign null callback.";
        this.callback = cb;
    }
    public void SetUniqueId(long unique_id)
    {
        this.unique_id = unique_id;
    }
    public long GetUniqueId()
    {
        return this.unique_id;
    }
    // context for tcp will use the mm_addr.u attribute.such as crypto context.
    public void SetContext(Object u)
    {
        this.u = u;
    }
    public Object GetContext()
    {
        return this.u;
    }
    //////////////////////////////////////////////////////////////////////////
    // fopen socket.ss_remote.ss_family,SOCK_STREAM,0 
    public void FopenSocket()
    {
        this.socket.FopenSocket(mmOSSocket.MM_AF_INET6, mmOSSocket.SOCK_STREAM, 0);
        if (mmOSSocket.MM_INVALID_SOCKET != this.socket.socket)
        {
            mmLogger.LogI(TAG + " fopen_socket " + this.socket.ToString() + " success.");
        }
        else
        {
            mmLogger.LogE(TAG + " fopen_socket " + this.socket.ToString() + " failure.");
        }
    }
    //close socket.
    public void CloseSocket()
    {
        this.socket.CloseSocket();
    }
    // shutdown socket.
    public void ShutdownSocket(int opcode)
    {
        this.socket.ShutdownSocket(opcode);
    }
    //////////////////////////////////////////////////////////////////////////
    //handle recv for buffer pool and pool max size.
    @SuppressWarnings("UnusedAssignment")
    public void HandleRecv()
    {
        // mm_streambuf page size MM_STREAMBUF_PAGE_SIZE is equal MM_SOCKET_PAGE_SIZE.
        // reduce the mm_streambuf frequently change max_size, here we use 3 * MM_SOCKET_PAGE_SIZE / 4 for max_length for recv.
        int max_length = 3 * mmSocket.MM_SOCKET_PAGE_SIZE / 4;
        int real_len = 0;
        // char link_name[MM_LINK_NAME_LENGTH];
        assert null != this.callback : "this.callback is invalid.";
        // assert(p->callback.handle && p->callback.broken && "callback.handle or callback.broken is invalid.");
        //
        do
        {
            // if the idle put size is lack, we try remove the get buffer.
            this.buff_recv.AlignedMemory(max_length);
            // recv.
            real_len = this.socket.Recv(this.buff_recv.buff, this.buff_recv.pptr, max_length, 0);
            // real_len = mm_addr_recv(&p->addr, buffer, offset, (int)max_length, 0);
            if( -1 == real_len )
            {
                int errcode = mmErrno.GetCode();
                if(mmErrno.MM_EAGAIN == errcode)
                {
                    // MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
                    // this error is not serious.
                    break;
                }
                else if(
                        mmErrno.MM_ECONNRESET == errcode ||
                        mmErrno.MM_ECONNABORTED == errcode ||
                        mmErrno.MM_ENOTSOCK == errcode )
                {
                    // An existing connection was forcibly closed by the remote host.
                    mmLogger.LogI(TAG + " " + this.socket.ToString() + " broken.");
                    this.callback.Broken(this); 
                    break;
                }
                else
                {
                    // error occur.
                    mmLogger.LogI(TAG + " " + this.socket.ToString() + " error occur.");
                    this.callback.Broken(this);
                    break;
                }
            }
            else if(0 == real_len)
            {
                // if this recv operate 0 == real_len,means the recv buffer is full or target socket is closed.
                // so we must use poll for checking the io event make sure read event is fire.
                // at here send length is 0 will fire the broken event.
                
                // the other side close the socket.
                
                mmLogger.LogI(TAG + " " + this.socket.ToString() + " broken.");
                this.callback.Broken(this);
                break;
            }
            // pbump the position.
            // note: the real buffer is 
            //       buffer = p->buff_recv.buff
            //       offset = (mm_uint32_t)p->buff_recv.pptr - real_len
            //       length = real_len
            this.buff_recv.Pbump(real_len);
            //
            if(real_len != max_length)
            {
                this.BufferRecv(this.buff_recv.buff, this.buff_recv.pptr - real_len, real_len);
                break;
            }
            else
            {
                this.BufferRecv(this.buff_recv.buff, this.buff_recv.pptr - real_len, real_len);
            }
        } while (true);
    }
    //handle send for buffer pool and pool max size.
    public void HandleSend()
    {
        // do nothing.
    }
    
    //////////////////////////////////////////////////////////////////////////
    //handle recv data from buffer and buffer length.0 success -1 failure.
    @SuppressWarnings({"UnusedAssignment", "UnusedReturnValue"})
    public int BufferRecv(byte[] buffer, int offset, int length)
    {
        int code = 0;
        // assert null != this.callback : "this.callback is invalid.";
        code = this.callback.Handle(this, buffer, offset, length);
        if (0 != code)
        {
            // invalid tcp message buffer decode.
            // shutdown socket immediately.         
            this.socket.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
        }
        return code;
    }
    //handle send data from buffer and buffer length.>0 is send size success -1 failure.
    //note this function do not trigger the callback.broken it is only mm_addr_shutdown_socket the fd MM_BOTH_SHUTDOWN.
    //only recv can trigger callback.broken ,make sure not dead lock and invalid free pinter appear.
    //0 <  rt,means rt buffer is send,we must gbump rt size.
    //0 == rt,means the send buffer can be full.
    //0 >  rt,means the send process can be failure.
    @SuppressWarnings({"UnusedAssignment", "IfStatementWithIdenticalBranches"})
    public int BufferSend(byte[] buffer, int offset, int length)
    {
        int real_len = 0;
        int send_len = 0;
        // char link_name[MM_LINK_NAME_LENGTH];
        assert null != this.callback : "this.callback is invalid.";
        // assert(p->callback.handle&&p->callback.broken&&"callback.handle or callback.broken is invalid.");
        do 
        {
            if (0 == length)
            {
                // nothing for send break immediately.
                break;
            }
            real_len = this.socket.Send(buffer, offset, (int)length, 0 );
            if (-1 == real_len)
            {
                int errcode = mmErrno.GetCode();
                if ( 
                    mmErrno.MM_EAGAIN == errcode ||
                    mmErrno.MM_ENOBUFS == errcode)
                {
                    // MM_EAGAIN errno:(10035) A non-blocking socket operation could not be completed immediately.
                    // this error is not serious.
                    // MM_ENOBUFS if send buffer is full.break.
                    break;
                }
                else
                {
                    // at send process,we can not trigger the broken call back.
                    mmLogger.LogI(TAG + " " + this.socket.ToString() + " broken.");
                    this.socket.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
                    // error is occur,we assign the return code to -1 and break it immediately.
                    send_len = -1;
                    break;
                }
            }
            else if (real_len == length)
            {
                // complete
                send_len += real_len;
                break;
            }
            else if (0 == real_len)
            {
                // if this send operate 0 == real_len,means the send buffer is full or target socket is closed.
                // so we must use poll for checking the io event make sure read event is fire.
                // at here send length is 0 will shutdown this socket.
                // at send process,we can not trigger the broken call back.
                mmLogger.LogI(TAG + " " + this.socket.ToString() + " broken.");
                this.socket.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
                // error is occur,we assign the return code to -1 and break it immediately.
                send_len = -1;
                break;
            }
            else
            {
                // need cycle send.
                offset += real_len;
                length -= real_len;
                send_len += real_len;
            }
        } while (true);
        return send_len;
    }
    //////////////////////////////////////////////////////////////////////////
    //handle send data by flush send buffer.
    //0 <  rt,means rt buffer is send,we must gbump rt size.
    //0 == rt,means the send buffer can be full.
    //0 >  rt,means the send process can be failure.
    public int FlushSend()
    {
        int sz = this.buff_send.Size();
        int rt = this.BufferSend(this.buff_send.buff, this.buff_send.gptr, sz);
        if (0 < rt)
        {
            // 0 <  rt,means rt buffer is send,we must gbump rt size.
            // 0 == rt,means the send buffer can be full.
            // 0 >  rt,means the send process can be failure.
            this.buff_send.Gbump(rt);
        }
        return rt;
    }
    //////////////////////////////////////////////////////////////////////////
}
