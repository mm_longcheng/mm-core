/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.mm.container.mmRbtreeVisible;
import org.mm.core.mmOSSocket;
import org.mm.core.mmParameters;
import org.mm.core.mmSpinlock;
import org.mm.core.mmParameters.mmServerInfo;
import org.mm.core.mmPoolElement;
import org.mm.net.mmTcp.mmTcpCallback;
import org.mm.net.mmCrypto.mmCryptoCallback;

@SuppressWarnings({"unused", "Convert2Diamond"})
public class mmMailbox
{
	public static final String TAG = mmMailbox.class.getSimpleName();
	
	static final mmMailboxTcpHandlePacketCallback __MailboxTcpHandlePacketCallback = new mmMailboxTcpHandlePacketCallback();
	
	// packet handle function type.
	public interface mmMailboxHandleCallback 
	{  
	    void Handle(Object obj, Object u, mmPacket pack);
	}
	
	public static class mmMailboxCallback
	{	
		public void Handle(Object obj, Object u, mmPacket pack){}
		public void Broken(Object obj){}
		public void Nready(Object obj){}
		public void Finish(Object obj){}
		
		public Object obj = null;// weak ref. user data for callback.
		
		public void Init()
		{
			this.obj = null;
		}
		public void Destroy()
		{
			this.obj = null;
		}
	}
	
	public static class mmMailboxEventTcpAllocator
	{	
		public void EventTcpProduce(Object p, mmTcp tcp){}
		public void EventTcpRecycle(Object p, mmTcp tcp){}
		
		Object obj = null;// weak ref. user data for callback.
		
		public void Init()
		{
			this.obj = null;
		}
		public void Destroy()
		{
			this.obj = null;
		}
	}

	public mmPoolElement pool_element = new mmPoolElement();
	public mmAccepter accepter = new mmAccepter();
	// strong ref.
	public mmNucleus nucleus = new mmNucleus();
	// rb tree for msg_id <--> callback.
	public TreeMap<Integer, mmMailboxHandleCallback> rbtree = new TreeMap<Integer, mmMailboxHandleCallback>();
	// tcp callback handle implement.
	public mmTcpCallback tcp_callback = new mmTcpCallback();
	// value ref. tcp callback.
	public mmMailboxCallback callback = new mmMailboxCallback();
	// value ref.event tcp alloc.
	public mmMailboxEventTcpAllocator event_tcp_alloc = new mmMailboxEventTcpAllocator();
	// crypto callback.
	public mmCryptoCallback crypto_callback = new mmCryptoCallback();
	// the locker for only get instance process thread safe.
	public mmSpinlock instance_locker = new mmSpinlock();
	public mmSpinlock rbtree_locker = new mmSpinlock();
	public mmSpinlock pool_locker = new mmSpinlock();
	public mmSpinlock locker = new mmSpinlock();
	// user data.
	public Object u = null;
	
	//////////////////////////////////////////////////////////////////////////	
	public void Init()
	{
		this.accepter.Init();
		this.nucleus.Init();
		this.rbtree.clear();
		this.tcp_callback.Init();
		this.callback.Init();
		this.event_tcp_alloc.Init();
		this.crypto_callback.Init();
		this.instance_locker.Init();
		this.rbtree_locker.Init();
		this.pool_locker.Init();
		this.locker.Init();
		this.u = null;
		//
		this.tcp_callback = new mmMailboxTcpCallback(this);
		//
		this.accepter.SetCallback(new __mmMailbox_AccepterAcceptCallback(this));		
		this.nucleus.SetCallback(new __mmMailbox_NuclearCallback(this));
		
		this.pool_element.SetElementType(mmTcp.class);
		this.pool_element.SetCallback(new __mmMailbox_PoolElementCallback(this));		
	}
	public void Destroy()
	{
		this.ClearHandleRbtree();
		//
		this.Clear();
		//
		this.accepter.Destroy();
		this.nucleus.Destroy();
		this.rbtree.clear();
		this.tcp_callback.Destroy();
		this.callback.Destroy();
		this.event_tcp_alloc.Destroy();
		this.crypto_callback.Destroy();
		this.instance_locker.Destroy();
		this.rbtree_locker.Destroy();
		this.pool_locker.Destroy();
		this.locker.Destroy();
		this.u = null;
	}
	public void SetNative(String node,int port)
	{
		this.accepter.SetNative(node, port);
	}
	public void SetRemote(String node,int port)
	{
		this.accepter.SetRemote(node, port);
	}
	public void SetThreadNumber(int thread_number)
	{
		this.SetLength(thread_number);
	}
	// assign protocol_size.
	public void SetLength(int length)
	{
		this.nucleus.SetLength(length);
	}
	public int GetLength()
	{
		return this.nucleus.GetLength();
	}
	// 192.168.111.123-65535(2) 192.168.111.123-65535[2]
	public void SetParameters(String parameters)
	{
		mmServerInfo server_info = new mmServerInfo();
		mmParameters.Server(parameters, server_info);
		this.SetNative(server_info.node, server_info.port);
		this.SetLength(server_info.length);
	}
	// fopen.
	public void FopenSocket()
	{
		this.accepter.FopenSocket();
	}
	// bind.
	public void Bind()
	{
		this.accepter.Bind();
	}
	// listen.
	public void Listen()
	{
		this.accepter.Listen();
	}
	// close socket. mm_mailbox_join before call this.
	public void CloseSocket()
	{
		this.accepter.CloseSocket();
	}
	//////////////////////////////////////////////////////////////////////////
	// lock to make sure the mailbox is thread safe.
	public void Lock()
	{
	    this.pool_element.Lock();
	    this.accepter.Lock();
	    this.locker.Lock();
	}
	// unlock mailbox.
	public void Unlock()
	{
		this.locker.Unlock();
		this.accepter.Lock();
		this.pool_element.Lock();
	}

	public void SetHandle(int id, mmMailboxHandleCallback callback)
	{
		this.rbtree_locker.Lock();
		this.rbtree.put(id, callback);
		this.rbtree_locker.Unlock();
	}
	// do not assign when rbtree have some elem.
	public void SetTcpCallback(mmTcpCallback tcp_callback)
	{
		// android assertions are unreliable.
		// assert null != tcp_callback : "you can not assign null tcp_callback.";
		this.tcp_callback = tcp_callback;
	}
	public void SetEventTcpAllocator(mmMailboxEventTcpAllocator event_tcp_alloc)
	{
		// android assertions are unreliable.
		// assert null != event_tcp_alloc : "you can not assign null event_tcp_alloc.";
		this.event_tcp_alloc = event_tcp_alloc;
	}
	public void SetMailboxCallback(mmMailboxCallback mailbox_callback)
	{
		// android assertions are unreliable.
		// assert null != mailbox_callback : "you can not assign null mailbox_callback.";
		this.callback = mailbox_callback;	
	}
	// do not assign when rbtree have some elem.
	public void SetCryptoCallback(mmCryptoCallback crypto_callback)
	{
		// android assertions are unreliable.
		// assert null != crypto_callback : "you can not assign null crypto_callback.";
		this.crypto_callback = crypto_callback;
	}
	public void ClearHandleRbtree()
	{
		this.rbtree_locker.Lock();
		this.rbtree.clear();
		this.rbtree_locker.Unlock();
	}
	// mailbox crypto encrypt buffer by tcp.
	public void CryptoEncrypt(mmTcp tcp, byte[] buffer, int offset, int length)
	{
		this.crypto_callback.Encrypt(this, tcp, buffer, offset, buffer, offset, length);
	}
	// mailbox crypto decrypt buffer by tcp.
	public void CryptoDecrypt(mmTcp tcp, byte[] buffer, int offset, int length)
	{
		this.crypto_callback.Decrypt(this, tcp, buffer, offset, buffer, offset, length);
	}
	// not lock inside,lock tcp manual.this func will copy each nuclear poll fd rbtree,
	// and traver all fd(not the tcp weak ref),use fd get the tcp for nuclear and trigger callback.
	public void Traver(mmRbtreeVisible.mmRbtreeVisibleCallback handle, Object u )
	{
		this.nucleus.Traver(handle, u);
	}
	// poll size.
	public int PollSize()
	{
		return this.nucleus.PollSize();
	}
	public void Start()
	{
		// we must explicit assign thread number manual.
		// assert(0 != p->nucleus.length && "0 != p->nucleus.length is invalid, not poll thread will do nothing.");
		this.accepter.Start();
		this.nucleus.Start();
	}
	public void Interrupt()
	{
		this.accepter.Interrupt();
		this.nucleus.Interrupt();
	}
	public void Shutdown()
	{
		this.accepter.Shutdown();
		this.nucleus.Shutdown();
	}
	public void Join()
	{
		this.accepter.Join();
		this.nucleus.Join();
	}
	@SuppressWarnings("UnusedAssignment")
	public mmTcp add(AbstractSelectableChannel fd, mmSockaddr remote)
	{
	    mmTcp tcp = null;
		// only lock for instance create process.
		this.instance_locker.Lock();
		tcp = this.Get(fd);
		if (null == tcp)
		{
			mmTcpCallback tcp_callback = (mmTcpCallback)this.tcp_callback.clone();
			
			// pool_element produce.
			this.pool_locker.Lock();
			tcp = (mmTcp)this.pool_element.Produce();
			this.pool_locker.Unlock();
	        // note: 
	        // pool_element manager init and destroy.
	        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
			tcp.Reset();
			//
			tcp_callback.obj = this;
			tcp.SetCallback(tcp_callback);
			//
			tcp.SetRemoteStorage(remote);
			tcp.SetNativeStorage(this.accepter.socket.ss_native);
			tcp.socket.socket = fd;
			tcp.socket.SetBlock(mmOSSocket.MM_NONBLOCK);
			//
			this.nucleus.FdAdd(fd, tcp);
			//
			this.event_tcp_alloc.EventTcpProduce(this, tcp);
			// assert null != mailbox.callback : "mailbox.callback is a null.";
			this.callback.Finish(tcp);
		}
		this.instance_locker.Unlock();
		return tcp;
	}
	public mmTcp Get(AbstractSelectableChannel fd)
	{
		return (mmTcp)this.nucleus.FdGet(fd);
	}
	@SuppressWarnings("UnusedReturnValue")
	public mmTcp GetInstance(AbstractSelectableChannel fd, mmSockaddr remote)
	{
	    mmTcp tcp = this.Get(fd);
		if (null == tcp)
		{
			tcp = this.add(fd, remote);
		}
		return tcp;
	}
	public void Rmv(AbstractSelectableChannel fd)
	{
	    mmTcp tcp = this.Get(fd);
		if (null != tcp)
		{
			// mm_mailbox_rmv_tcp(p,tcp);
			// here we can lock the tcp,becaue all callback.broken only recv process trigger.
			tcp.Lock();
			this.nucleus.FdRmv(fd, tcp);
			tcp.Unlock();
			this.event_tcp_alloc.EventTcpRecycle(this, tcp);
	        // note: 
	        // pool_element manager init and destroy.
	        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
			tcp.Reset();
			// pool_element recycle.
			this.pool_locker.Lock();
			this.pool_element.Recycle(tcp);
			this.pool_locker.Unlock();
		}
	}
	public void RmvTcp(mmTcp tcp)
	{
		this.nucleus.FdRmv(tcp.socket.socket, tcp);
	}
	@SuppressWarnings("UnusedAssignment")
	public void Clear()
	{
		int i = 0;
		Entry<Integer, Object> n = null;
		Iterator<Entry<Integer, Object>> it = null;
		mmTcp tcp = null;
		mmNuclear nuclear = null;
		//
		this.instance_locker.Lock();
		this.nucleus.arrays_locker.Lock();
		for (i = 0;i < this.nucleus.length;++i)
		{
			nuclear = this.nucleus.arrays[i];
			nuclear.cond_locker.Lock();
			// rbtree_m locker is lock inside at mm_nuclear_fd_rmv_poll.
			// here we only need lock the nuclear->cond_locker.
			// nuclear.rbtree_visible.locker_m.lock();
			it = nuclear.rbtree_visible.rbtree_m.entrySet().iterator();
			while (it.hasNext()) 
			{
				n = it.next();
				tcp = (mmTcp)n.getValue();
				tcp.Lock();
				nuclear.FdRmvPoll(tcp.socket.socket, tcp);
				tcp.Unlock();
				this.event_tcp_alloc.EventTcpRecycle(this, tcp);
	            // note: 
	            // pool_element manager init and destroy.
	            // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
				tcp.Destroy();
				// pool_element recycle.
				this.pool_locker.Lock();
				this.pool_element.Recycle(tcp);
				this.pool_locker.Unlock();
			}
			// nuclear.rbtree_visible.locker_m.unlock();
			nuclear.cond_locker.Unlock();
			// trigger poll wake signal.
			nuclear.FdPollSignal();
		}
		this.nucleus.arrays_locker.Unlock();
		this.instance_locker.Unlock();
	}
	// use this function for manual shutdown tcp.
	// note: you can not use rmv or rmv_tcp for shutdown a activation tcp.
	public void ShutdownTcp(mmTcp tcp)
	{
		tcp.ShutdownSocket(mmOSSocket.MM_BOTH_SHUTDOWN);
	}
	/////////////////////////////////////////////////////////////
	static final class __mmMailbox_NuclearCallback extends mmNuclear.mmNuclearCallback
	{
	    __mmMailbox_NuclearCallback(mmMailbox _p)
		{
			this.obj = _p;
		}
		@Override
		public void HandleRecv(Object obj, Object u)
		{
			mmTcp tcp = (mmTcp)(u);
			tcp.HandleRecv();	
		}
		@Override
		public void HandleSend(Object obj, Object u)
		{
		    mmTcp tcp = (mmTcp)(u);
			tcp.HandleSend();
		}
	}

	static final class __mmMailbox_AccepterAcceptCallback extends mmAccepter.mmAccepterCallback
	{
	    __mmMailbox_AccepterAcceptCallback(mmMailbox _p)
		{
			this.obj = _p;
		}
		@Override
		public void Accept( Object obj, AbstractSelectableChannel fd, mmSockaddr remote)
		{
		    mmAccepter accepter = (mmAccepter)obj;
		    mmMailbox mailbox = (mmMailbox)(accepter.callback.obj);
			mailbox.GetInstance(fd,remote);
		}	
	}
	static final class mmMailboxTcpCallback extends mmTcp.mmTcpCallback
	{
	    mmMailboxTcpCallback(mmMailbox _p)
		{
			this.obj = _p;
		}
		@Override
		public int Handle(Object obj, byte[] buffer, int offset, int length)
		{
		    mmTcp tcp = (mmTcp)(obj);
		    mmMailbox mailbox = (mmMailbox)(tcp.callback.obj);
			mailbox.CryptoDecrypt(tcp,buffer,offset,length);
			return mmStreambufPacket.HandleTcp(tcp.buff_recv, __MailboxTcpHandlePacketCallback, tcp);
		}
		@Override
		public int Broken(Object obj)
		{
		    mmTcp tcp = (mmTcp)(obj);
		    mmMailbox mailbox = (mmMailbox)(tcp.callback.obj);
			// android assertions are unreliable.
			// assert null != mailbox.callback : "mailbox.callback is a null.";
			mailbox.callback.Broken(tcp);
			mailbox.Rmv(tcp.socket.socket);
			return 0;
		}	
	}
	static final class mmMailboxTcpHandlePacketCallback implements mmPacket.TcpCallBack
	{
		@SuppressWarnings("UnusedAssignment")
		@Override
		public void HandleTcp(Object obj, mmPacket pack) 
		{
			mmTcp tcp = (mmTcp)(obj);
			mmMailbox mailbox = (mmMailbox)(tcp.callback.obj);
			mmMailboxHandleCallback handle = null;
			mailbox.rbtree_locker.Lock();
			handle = mailbox.rbtree.get(pack.phead.mid);
			mailbox.rbtree_locker.Unlock();
			if (null != handle)
			{
				// fire the handle event.
				handle.Handle(tcp, mailbox.u, pack);
			}
			else
			{
				// android assertions are unreliable.
				// assert null != mailbox.callback : "mailbox.callback is a null.";
				mailbox.callback.Handle(tcp, mailbox.u, pack);
			}
		}		
	}
	static final class __mmMailbox_PoolElementCallback extends mmPoolElement.mmPoolElementCallback
	{
	    __mmMailbox_PoolElementCallback(mmMailbox _p)
		{
			this.obj = _p;
		}
		public void Produce(Object obj, Object u, Object v)
		{
		    // note: 
		    // pool_element manager init and destroy.
		    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
		    mmTcp e = (mmTcp)(v);
		    e.Init();
		}
		public void Recycle(Object obj, Object u, Object v)
		{
		    // note: 
		    // pool_element manager init and destroy.
		    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
		    mmTcp e = (mmTcp)(v);
		    e.Destroy();
		}
	}
}
