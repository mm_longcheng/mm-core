/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.net.InetAddress;

@SuppressWarnings("unused")
public class mmSockaddr
{
    //////////////////////////////////////////////////////////////////////////
    public static final String MM_ADDR_DEFAULT_NODE = "::";
    public static final int MM_ADDR_DEFAULT_PORT = 0;
    //////////////////////////////////////////////////////////////////////////
    public static final int MM_NODE_NAME_LENGTH = 64;
    public static final int MM_PORT_NAME_LENGTH = 8;
    public static final int MM_SOCK_NAME_LENGTH = 16;
    public static final int MM_ADDR_NAME_LENGTH = 64;
    public static final int MM_LINK_NAME_LENGTH = 128;
    
    //////////////////////////////////////////////////////////////////////////
    public String node = MM_ADDR_DEFAULT_NODE;// socketaddr node.
    public int port = MM_ADDR_DEFAULT_PORT;   // socketaddr port.
    //////////////////////////////////////////////////////////////////////////
    public void Init()
    {
        this.node = MM_ADDR_DEFAULT_NODE;
        this.port = MM_ADDR_DEFAULT_PORT;
    }
    public void Destroy()
    {
        this.node = MM_ADDR_DEFAULT_NODE;
        this.port = MM_ADDR_DEFAULT_PORT;
    }
    public void Reset()
    {
        this.node = MM_ADDR_DEFAULT_NODE;
        this.port = MM_ADDR_DEFAULT_PORT;
    }
    public void CopyForm(mmSockaddr rhs)
    {
        SockaddrAssign(this,rhs.node,rhs.port);
    }
    public void Assign(String node, int port)
    {
        SockaddrAssign(this,node,port);
    }   
    // ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
    // ipv4                         192.168.111.123-65535
    public static String SockaddrString(mmSockaddr addr)
    {
        return addr.node + "-" + addr.port;
    }
    // ipv6 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
    // ipv4                         192.168.111.123-65535
    public static void SockaddrAssign(mmSockaddr addr, String node, int port)
    {
        addr.node = node;
        addr.port = port;
    }
    // (fd)|ip-port --> ip-port
    // ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
    // ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
    public static String SockaddrNativeRemoteString(mmSockaddr native_addr, mmSockaddr remote_addr, int fd)
    {
        return "(" + fd + ")|" + SockaddrString(native_addr) + "-->" + SockaddrString(remote_addr);
    }
    // true if same.
    public static boolean SockaddrCompareAddress(mmSockaddr addr,String node, int port)
    {
        return addr.node.equals(node) && addr.port == port;
    }
    public static boolean SockaddrCompare(mmSockaddr addrl, mmSockaddr addrr )
    {
        return addrl.node.equals(addrr.node) && addrl.port == addrr.port;
    }
    public static InetAddress DefaultInetAddress()
    {
        InetAddress value = null;
        try 
        {
            value = InetAddress.getLocalHost();
        } 
        catch (Exception e) 
        {
            // do nothing here.we only need the finally code.
            // ok here have special real exception.
            e.printStackTrace();
        }
        return value;
    }
}
