/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

public class mmCrypto
{
    @SuppressWarnings("unused")
    public static class mmCryptoCallback
    {   
        public void Encrypt(
                Object p, Object e, 
                byte[] buffer_i, int offset_i, 
                byte[] buffer_o, int offset_o, 
                int length)
        {
            
        }
        
        public void Decrypt(
                Object p, Object e, 
                byte[] buffer_i, int offset_i, 
                byte[] buffer_o, int offset_o, 
                int length)
        {
            
        }
        
        Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
    }
}

