/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;

import org.mm.container.mmRbtreeVisible;
import org.mm.core.mmSpinlock;
import org.mm.core.mmThread;

@SuppressWarnings("unused")
public class mmNucleus
{
    public mmNuclear.mmNuclearCallback callback = new mmNuclear.mmNuclearCallback();
    
    public mmThread.Mutex signal_mutex = new mmThread.Mutex();
    public mmThread.Cond signal_cond = new mmThread.Cond(this.signal_mutex);

    public mmSpinlock arrays_locker = new mmSpinlock();
    public mmSpinlock locker = new mmSpinlock();
    
    // length. default is 0.
    public int length = 0;
    // array pointer.
    public mmNuclear[] arrays = null;
    // set poll len for mm_poll_wait. default is MM_NUCLEAR_POLL_LEN. set it before wait,or join_wait after.
    public int poll_length = mmNuclear.MM_NUCLEAR_POLL_LENGTH;// poll length.
    // set poll timeout interval.-1 is forever. default is MM_NUCLEAR_IDLE_SLEEP_MSEC.
    // if forever, you must resolve break block state while poll wait, use pipeline, or other way.
    // poll timeout.
    public int poll_timeout = mmNuclear.MM_NUCLEAR_IDLE_SLEEP_MSEC;
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    public int state = mmThread.MM_TS_CLOSED;
    
    public void Init()
    {
        this.callback.Init();
        this.signal_mutex.Init();
        this.signal_cond.Init();
        this.arrays_locker.Init();
        this.locker.Init();
        this.length = 0;
        this.arrays = null;
        this.poll_length = mmNuclear.MM_NUCLEAR_POLL_LENGTH;
        this.poll_timeout = mmNuclear.MM_NUCLEAR_IDLE_SLEEP_MSEC;
        
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Destroy()
    {
        this.Clear();
        //
        this.callback.Destroy();
        this.signal_mutex.Destroy();
        this.signal_cond.Destroy();
        this.arrays_locker.Destroy();
        this.locker.Destroy();
        this.poll_length = 0;
        this.poll_timeout = 0;
        
        this.state = mmThread.MM_TS_CLOSED;
    }
    //////////////////////////////////////////////////////////////////////////
    @SuppressWarnings({"UnusedAssignment", "MismatchedReadAndWriteOfArray"})
    public void SetLength(int length)
    {
        this.arrays_locker.Lock();
        if (length < this.length)
        {
            int i = 0;
            mmNuclear e = null;
            for ( i = length; i < this.length; ++i)
            {
                e = this.arrays[i];
                e.Lock();
                this.arrays[i] = null;
                e.Unlock();
                e.Destroy();
                e = null;
            }
            mmNuclear[] v = new mmNuclear[length];
            System.arraycopy(this.arrays, 0, v, 0, length);
            this.arrays = v;
            this.length = length;
        }
        else if (length > this.length)
        {
            int i = 0;
            mmNuclear e = null;
            mmNuclear[] v = new mmNuclear[length];
            if ( 0 < this.length )
            {
                // if arrays length is zero, will case a NullPointerException.
                System.arraycopy(this.arrays, 0, v, 0, this.length);
            }
            for ( i = this.length; i < length; ++i)
            {
                e = new mmNuclear();
                e.Init();
                e.poll_length = this.poll_length;
                e.poll_timeout = this.poll_timeout;
                e.state = this.state;
                e.SetCallback(this.callback);
                this.arrays[i] = e;
            }
            this.length = length;
        }
        this.arrays_locker.Unlock();
    }
    public int GetLength()
    {
        return this.length;
    }
    @SuppressWarnings("UnusedAssignment")
    public void SetCallback(mmNuclear.mmNuclearCallback callback)
    {
        int i = 0;
        mmNuclear e = null;
        // android assertions are unreliable.
        // assert null != cb : "you can not assign null callback.";
        this.arrays_locker.Lock();
        this.callback = callback;
        for ( i = 0; i < this.length; ++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.SetCallback(this.callback);
            e.Unlock();
        }
        this.arrays_locker.Unlock();
    }
    // poll size.
    @SuppressWarnings("UnusedAssignment")
    public int PollSize()
    {
        int sz = 0;
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        for ( i = 0; i < this.length; ++i)
        {
            e = this.arrays[i];
            e.Lock();
            sz += e.PollSize();
            e.Unlock();
        }
        this.arrays_locker.Unlock();
        return sz;
    }
    //////////////////////////////////////////////////////////////////////////
    //wait for activation fd.
    @SuppressWarnings("UnusedAssignment")
    public void PollWait()
    {
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        for (i = 0;i < this.length;++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.PollWait();
            e.Unlock();
        }
        this.arrays_locker.Unlock();
    }
    //////////////////////////////////////////////////////////////////////////
    //start wait thread.
    @SuppressWarnings("UnusedAssignment")
    public void Start()
    {
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        this.state = mmThread.MM_TS_FINISH == this.state ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        for (i = 0;i < this.length;++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.Start();
            e.Unlock();
        }
        this.arrays_locker.Unlock();
    }
    //interrupt wait thread.
    @SuppressWarnings("UnusedAssignment")
    public void Interrupt()
    {
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        this.state = mmThread.MM_TS_CLOSED;
        for (i = 0;i < this.length;++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.Interrupt();
            e.Unlock();
        }
        this.arrays_locker.Unlock();
        //
        this.signal_mutex.Lock();
        this.signal_cond.Signal();
        this.signal_mutex.Unlock();
    }
    //shutdown wait thread.
    @SuppressWarnings("UnusedAssignment")
    public void Shutdown()
    {
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        this.state = mmThread.MM_TS_FINISH;
        for ( i = 0; i < this.length; ++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.Shutdown();
            e.Unlock();
        }
        this.arrays_locker.Unlock();
        //
        this.signal_mutex.Lock();
        this.signal_cond.Signal();
        this.signal_mutex.Unlock();
    }
    //join wait thread.
    @SuppressWarnings("UnusedAssignment")
    public void Join()
    {
        int i = 0;
        mmNuclear e = null;
        if (mmThread.MM_TS_MOTION == this.state)
        {
            // we can not lock or join until cond wait all thread is shutdown.
            this.signal_mutex.Lock();
            this.signal_cond.Await();
            this.signal_mutex.Unlock(); 
        }
        //
        this.arrays_locker.Lock();
        for ( i = 0; i < this.length; ++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.Join();
            e.Unlock();
        }
        this.arrays_locker.Unlock();
    }
    // traver member for nucleus.
    @SuppressWarnings("UnusedAssignment")
    public void Traver(mmRbtreeVisible.mmRbtreeVisibleCallback handle, Object u)
    {
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        for ( i = 0; i < this.length; ++i)
        {
            e = this.arrays[i];
            e.Lock();
            e.Traver(handle, u);
            e.Unlock();
        }
        this.arrays_locker.Unlock();
    }
    @SuppressWarnings("UnusedAssignment")
    public void Clear()
    {
        int i = 0;
        mmNuclear e = null;
        this.arrays_locker.Lock();
        for ( i = 0; i < this.length; ++i)
        {
            e = this.arrays[i];
            e.Lock();
            this.arrays[i] = null;
            e.Unlock();
            e.Destroy();
            e = null;
        }
        this.arrays = null;
        this.length = 0;
        this.arrays_locker.Unlock();
    }
    //////////////////////////////////////////////////////////////////////////
    public int HashIndex(AbstractSelectableChannel fd)
    {
        return fd.hashCode() % this.length;
    }
    //////////////////////////////////////////////////////////////////////////
    // add u into poll_fd.
    public void FdAdd(AbstractSelectableChannel fd, Object u)
    {
        int idx = this.HashIndex(fd);
        // android assertions are unreliable.
        // assert idx >= 0 && idx < this.length : "idx is out range.";
        this.arrays[idx].FdAdd(fd, u);
    }
    // rmv u from poll_fd.
    public void FdRmv(AbstractSelectableChannel fd, Object u)
    {
        int idx = this.HashIndex(fd);
        // android assertions are unreliable.
        // assert idx >= 0 && idx < this.length : "idx is out range.";
        this.arrays[idx].FdRmv(fd, u);
    }
    // mod u event from poll wait.
    public void FdMod(AbstractSelectableChannel fd, Object u, int flag)
    {
        int idx = this.HashIndex(fd);
        // android assertions are unreliable.
        // assert idx >= 0 && idx < this.length : "idx is out range.";
        this.arrays[idx].FdMod(fd, u, flag);
    }
    // get u from poll.
    public Object FdGet(AbstractSelectableChannel fd)
    {
        int idx = this.HashIndex(fd);
        // android assertions are unreliable.
        // assert idx >= 0 && idx < this.length : "idx is out range.";
        return this.arrays[idx].FdGet(fd);
    }
    //////////////////////////////////////////////////////////////////////////
}
