/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.nio.channels.spi.AbstractSelectableChannel;

import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;
import org.mm.core.mmSpinlock;
import org.mm.core.mmThread;
import org.mm.poller.mmPollEvent;
import org.mm.poller.mmSelect;

@SuppressWarnings("unused")
public class mmSelector
{
    public static final String TAG = mmSelector.class.getSimpleName();
    
    // nuclear default poll idle sleep milliseconds.
    public static final int MM_SELECTOR_IDLE_SLEEP_MSEC = 200;
    
    public static class mmSelectorCallback
    {       
        public void HandleRecv(Object obj, Object u){}
        public void HandleSend(Object obj, Object u){}
        
        public Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
    }
    
    public mmSelectorCallback callback = new mmSelectorCallback();

    public mmSelect poll = new mmSelect();// poller.

    public mmThread poll_thread = new mmThread();
    public mmSpinlock locker = new mmSpinlock();

    // set poll timeout interval.-1 is forever.default is MM_SELECTOR_IDLE_SLEEP_MSEC.
    // if forever,you must resolve break block state while poll wait,use pipeline,or other way.
    // poll timeout.
    public int poll_timeout = MM_SELECTOR_IDLE_SLEEP_MSEC;

    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    public int state = mmThread.MM_TS_CLOSED;

    public void Init()
    {
        this.callback.Init();
        this.poll.Init();
        this.poll_thread.Init();
        this.locker.Init();     
        this.poll_timeout = MM_SELECTOR_IDLE_SLEEP_MSEC;
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Destroy()
    {
        this.callback.Destroy();
        this.poll.Destroy();
        this.poll_thread.Destroy();
        this.locker.Destroy();  
        this.poll_timeout = MM_SELECTOR_IDLE_SLEEP_MSEC;
        this.state = mmThread.MM_TS_CLOSED;
    }
    // lock to make sure the knot is thread safe.
    public void Lock()
    {
        this.locker.Lock();
    }
    // unlock knot.
    public void Unlock()
    {
        this.locker.Unlock();
    }
    public void SetCallback(mmSelectorCallback callback)
    {
        // android assertions are unreliable.
        // assert null != callback : "you can not assign null callback.";
        this.callback = callback;
    }
    //
    public void FdAdd(AbstractSelectableChannel fd, Object u)
    {
        this.poll.SetFd(fd);
        this.poll.AddEvent(mmPollEvent.MM_PE_READABLE, u);
    }
    public void FdRmv(AbstractSelectableChannel fd, Object u)
    {
        this.poll.SetFd((AbstractSelectableChannel) mmOSSocket.MM_INVALID_SOCKET);
        this.poll.DelEvent(mmPollEvent.MM_PE_READABLE, u);
    }
    // wait for activation fd.
    @SuppressWarnings("UnusedAssignment")
    public void PollWait()
    {
        mmPollEvent pe = null;
        int real_len = 0;
        //
        pe = this.poll.poll_event;
        //
        while(mmThread.MM_TS_MOTION == this.state)
        {
            // the first quick size checking.
            real_len = this.poll.WaitEvent(this.poll_timeout);
            if( 0 == real_len)
            {
                continue;
            }
            else if(-1 == real_len)
            {
                int errcode = mmErrno.GetCode();
                if (mmErrno.MM_ENOTSOCK == errcode)
                {
                    mmLogger.LogE(TAG + " " + this.hashCode() + " error occur.");
                    break;
                }
                else
                {
                    mmLogger.LogE(TAG + " " + this.hashCode() + " error occur.");
                    continue;
                }
            }
            if (0 != (pe.mask & mmPollEvent.MM_PE_READABLE))
            {
                this.callback.HandleRecv(this, pe.s);
            }
            if (0 != (pe.mask & mmPollEvent.MM_PE_WRITABLE))
            {
                this.callback.HandleSend(this, pe.s);
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////
    //start wait thread.
    public void Start()
    {
        this.state = mmThread.MM_TS_FINISH == this.state ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        this.poll_thread.Start(new __Selector_WaitThreadRunnable(this));
    }
    //interrupt wait thread.
    public void Interrupt()
    {
        this.state = mmThread.MM_TS_CLOSED;
    }
    //shutdown wait thread.
    public void Shutdown()
    {
        this.state = mmThread.MM_TS_FINISH;
    }
    //join wait thread.
    public void Join()
    {
        this.poll_thread.Join();
    }
    //////////////////////////////////////////////////////////////////////////
    static final class __Selector_WaitThreadRunnable implements Runnable
    {
        __Selector_WaitThreadRunnable(mmSelector _p)
        {
            this.p = _p;
        }
        @SuppressWarnings("UnusedAssignment")
        public mmSelector p = null;
        @Override
        public void run()
        {
            this.p.PollWait();
        }
    }
}

