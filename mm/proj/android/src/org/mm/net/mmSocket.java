/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.net;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.NotYetBoundException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;

import java.nio.channels.ServerSocketChannel;

import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;
import org.mm.core.mmSpinlock;

// java socket is very different for c socket.
// we catch all Exception e but print nothing but catch the error code.
@SuppressWarnings("unused")
public class mmSocket
{
    // socket default page size.
    public static final int MM_SOCKET_PAGE_SIZE = 1024;

    // this type is java special.
    public static final int MM_ADDR_CLIENT = 0;
    public static final int MM_ADDR_SERVER = 1;
    
    public mmSockaddr ss_native = new mmSockaddr();
    public mmSockaddr ss_remote = new mmSockaddr(); 
    public AbstractSelectableChannel socket = null;
    public mmSpinlock locker = new mmSpinlock();
    
    private int sock_type = mmOSSocket.SOCK_STREAM;
    private int addr_type = MM_ADDR_CLIENT;// default is client.
    
    public void Init()
    {
        this.ss_native.Init();
        this.ss_remote.Init();
        this.socket = (AbstractSelectableChannel) mmOSSocket.MM_INVALID_SOCKET;
        this.locker.Init();
        this.sock_type = mmOSSocket.SOCK_STREAM;
    }
    public void Destroy()
    {
        this.ss_native.Destroy();
        this.ss_remote.Destroy();
        this.socket = (AbstractSelectableChannel) mmOSSocket.MM_INVALID_SOCKET;
        this.locker.Destroy();
        this.sock_type = mmOSSocket.SOCK_STREAM;
    }
    public void Lock()
    {
        this.locker.Lock();
    }
    public void Unlock()
    {
        this.locker.Unlock();
    }
    public void Reset()
    {
        this.CloseSocket();
        //
        this.ss_native.Reset();
        this.ss_remote.Reset();
        this.socket = (AbstractSelectableChannel) mmOSSocket.MM_INVALID_SOCKET;
        // spinlock not need reset.
    }
    // this api is java special.
    public void SetAddrType(int _type)
    {
        this.addr_type = _type;
    }
    // this api is java special.
    public int GetAddrType()
    {
        return this.addr_type;
    }
    // alloc socket fd.
    // tcp PF_INET, SOCK_STREAM, 0
    // udp PF_INET, SOCK_DGRAM , 0
    public void FopenSocket(int domain, int type, int protocol)
    {
        this.CloseSocket();
        this.sock_type = type;
        if(MM_ADDR_CLIENT == this.addr_type)
        {
            if(mmOSSocket.SOCK_STREAM == this.sock_type)
            {
                try 
                {
                    this.socket = SocketChannel.open();
                } 
                catch (Exception e)
                {
                    // real Exception.
                    mmLogger.LogE("exception: " + e.toString());
                }
            }
            else
            {
                try 
                {
                    this.socket = DatagramChannel.open();
                } 
                catch (Exception e) 
                {
                    // real Exception.
                    mmLogger.LogE("exception: " + e.toString());
                }
            }
        }
        else
        {
            if(mmOSSocket.SOCK_STREAM == this.sock_type)
            {
                try 
                {
                    this.socket = ServerSocketChannel.open();
                } 
                catch (Exception e) 
                {
                    // real Exception.
                    mmLogger.LogE("exception: " + e.toString());
                }
            }
            else
            {
                try 
                {
                    this.socket = DatagramChannel.open();
                } 
                catch (Exception e) 
                {
                    // real Exception.
                    mmLogger.LogE("exception: " + e.toString());
                }
            }
        }
    }
    public void CloseSocket()
    {
        if (mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            try 
            {
                this.socket.close();
            } 
            catch (Exception e) 
            {
                // do nothing here.we only need the finally code.
            }
            this.socket = null;
        }
    }
    // close socket fd.not set socket_t to invalid -1.
    // use to fire socket fd close event.
    public void CloseSocketEvent()
    {
        if (mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            try 
            {
                this.socket.close();
            } 
            catch (Exception e) 
            {
                // do nothing here.we only need the finally code.
            }
        }
    }
    // shutdown socket.
    @SuppressWarnings("StatementWithEmptyBody")
    public void ShutdownSocket(int opcode )
    {
        // at java the socket is shutdown.not api.
        // here we just close it.
        if(MM_ADDR_CLIENT == this.addr_type)
        {
            if (mmOSSocket.MM_INVALID_SOCKET != this.socket)
            {
                if(mmOSSocket.SOCK_STREAM == this.sock_type)
                {
                    SocketChannel _tcp_socket = (SocketChannel) this.socket;
                    Socket _socket = _tcp_socket.socket();
                    try 
                    {
                        if(mmOSSocket.MM_READ_SHUTDOWN == opcode)
                        {
                            _socket.shutdownInput();
                        }
                        else if(mmOSSocket.MM_SEND_SHUTDOWN == opcode)
                        {
                            _socket.shutdownOutput();
                        }
                        else
                        {
                            _socket.shutdownInput();
                            _socket.shutdownOutput();
                        }
                    } 
                    catch (Exception e) 
                    {
                        // do nothing here.we only need the finally code.
                    }
                }
                else
                {
                    // udp not need shutdown_socket.
                }
            }
        }
        else
        {
            // java server socket not need shutdown_socket.
        }
    }
    // assign addr native by ip port.AF_INET;
    public void SetNative(String node, int port)
    {
        this.ss_native.Assign(node, port);
    }
    // assign addr remote by ip port.AF_INET;
    public void SetRemote(String node, int port)
    {
        this.ss_remote.Assign(node, port);
    }
    // assign addr native by sockaddr_storage.AF_INET;after assign native will auto assign the remote ss_family.
    public void SetNativeStorage(mmSockaddr ss_native)
    {
        this.ss_native.CopyForm(ss_native);
    }
    // assign addr remote by sockaddr_storage.AF_INET;after assign remote will auto assign the native ss_family.
    public void SetRemoteStorage(mmSockaddr ss_remote)
    {
        this.ss_remote.CopyForm(ss_remote);
    }
    // default is blocking.MM_BLOCKING means block MM_NONBLOCK no block.
    public void SetBlock( int block )
    {
        if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            if( block == mmOSSocket.MM_BLOCKING )
            {
                // socket blocking.
                try 
                {
                    this.socket.configureBlocking(true);
                } 
                catch (Exception e) 
                {
                    // do nothing here.we only need the finally code.
                }
            }
            else
            {
                // socket nonblock.
                try 
                {
                    this.socket.configureBlocking(false);
                } 
                catch (Exception e) 
                {
                    // do nothing here.we only need the finally code.
                }
            }
        }
    }
    // connect address.
    public int Connect()
    {
        int rt = -1;
        if(MM_ADDR_CLIENT == this.addr_type)
        {
            if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
            {
                if(mmOSSocket.SOCK_STREAM == this.sock_type)
                {
                    SocketChannel _tcp_socket = (SocketChannel) this.socket;
                    // java fopen_socket will try connect here must check state.
                    if(!_tcp_socket.isConnected())
                    {
                        try 
                        {
                            InetSocketAddress isa = new InetSocketAddress(this.ss_remote.node,this.ss_remote.port);
                            rt = _tcp_socket.connect(isa) ? 0 : -1;
                        } 
                        catch (Exception e) 
                        {
                            // do nothing here.we only need the finally code.
                        }
                    }
                    else
                    {
                        // it is already connect.
                        rt = 0;
                    }
                }
                else
                {
                    DatagramChannel _udp_socket = (DatagramChannel) this.socket;
                    // java fopen_socket will try connect here must check state.
                    if(!_udp_socket.isConnected())
                    {
                        try 
                        {
                            InetSocketAddress isa = new InetSocketAddress(this.ss_remote.node,this.ss_remote.port);
                            _udp_socket.connect(isa);
                            rt = 0;
                        } 
                        catch (Exception e) 
                        {
                            // do nothing here.we only need the finally code.
                        }
                    }
                    else
                    {
                        // it is already connect.
                        rt = 0;
                    }
                }   
            }
        }
        else
        {
            // server socket not need connect anything.
            rt = 0;
        }
        return rt;
    }
    // bind address.
    @SuppressWarnings("UnusedAssignment")
    public int Bind()
    {
        int rt = -1;
        if(MM_ADDR_CLIENT == this.addr_type)
        {
            if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
            {
                if(mmOSSocket.SOCK_STREAM == this.sock_type)
                {
                    SocketChannel _tcp_socket = (SocketChannel) this.socket;
                    try 
                    {
                        InetSocketAddress isa = new InetSocketAddress(this.ss_native.node,this.ss_native.port);
                        Socket _socket = _tcp_socket.socket();
                        _socket.bind(isa);
                        rt = 0;
                    } 
                    catch (Exception e) 
                    {
                        // do nothing here.we only need the finally code.
                    }
                }
                else
                {
                    DatagramChannel _udp_socket = (DatagramChannel) this.socket;
                    try 
                    {
                        InetSocketAddress isa = new InetSocketAddress(this.ss_native.node,this.ss_native.port);
                        DatagramSocket _socket = _udp_socket.socket();
                        _socket.bind(isa);
                        rt = 0;
                    } 
                    catch (Exception e) 
                    {
                        // do nothing here.we only need the finally code.
                    }
                }   
            }
        }
        else
        {
            if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
            {
                ServerSocketChannel _tcp_socket = (ServerSocketChannel) this.socket;
                try 
                {
                    InetSocketAddress isa = new InetSocketAddress(this.ss_native.node,this.ss_native.port);
                    ServerSocket _server_socket = _tcp_socket.socket();
                    _server_socket.bind(isa);
                    rt = 0;
                } 
                catch (Exception e) 
                {
                    // do nothing here.we only need the finally code.
                }
            }
            // java server socket bind at listen process.
            rt = 0;
        }
        return rt;
    }
    // listen address.
    public int Listen( int backlog )
    {
        // at java ServerSocket.
        // not have listen function and will listen at bind.
        // so we use the system default backlog at bind.
        return 0;
    }
    // // obtain bind socket real sockaddr.such as bind for port 0.need use this obtain real port.
    @SuppressWarnings({"UnusedAssignment", "UnusedReturnValue", "IfStatementWithIdenticalBranches"})
    public int CurrentBindSockaddr(mmSockaddr ss_native)
    {
        int rt = -1;
        if(MM_ADDR_CLIENT == this.addr_type)
        {
            // java client socket do nothing here.
            rt = 0;
        }
        else
        {
            if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
            {
                ServerSocketChannel _tcp_socket = (ServerSocketChannel) this.socket;
                try 
                {
                    ServerSocket _server_socket = _tcp_socket.socket();
                    ss_native.node = _server_socket.getInetAddress().getHostAddress();
                    ss_native.port = _server_socket.getLocalPort();
                    rt = 0;
                } 
                catch (Exception e) 
                {
                    // do nothing here.we only need the finally code.
                }
            }
            // java server socket bind at listen process.
            rt = 0;
        }
        return rt;
    }
    // accept address.
    public SocketChannel Accept()
    {
        SocketChannel afd = null;
        if(MM_ADDR_CLIENT == this.addr_type)
        {
            // client socket not need accept anything.
            mmErrno.SetCode(mmErrno.MM_EINVAL);
        }
        else
        {
            if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
            {
                ServerSocketChannel _tcp_socket = (ServerSocketChannel) this.socket;                
                try 
                {
                    afd = _tcp_socket.accept();
                    mmErrno.SetCode(0);
                } 
                catch (ClosedByInterruptException e) 
                {
                    // do nothing here.we only need the finally code.
                    mmErrno.SetCode(mmErrno.MM_EINTR);
                }
                catch (AsynchronousCloseException e) 
                {
                    // do nothing here.we only need the finally code.
                    mmErrno.SetCode(mmErrno.MM_ECONNABORTED);
                }
                catch (ClosedChannelException e) 
                {
                    // do nothing here.we only need the finally code.
                    mmErrno.SetCode(mmErrno.MM_ENETRESET);
                }
                catch (NotYetBoundException e) 
                {
                    // do nothing here.we only need the finally code.
                    mmErrno.SetCode(mmErrno.MM_EINVAL);
                }
                catch (SecurityException e) 
                {
                    // do nothing here.we only need the finally code.
                    mmErrno.SetCode(mmErrno.MM_EINVAL);
                }
                catch (IOException e) 
                {
                    // do nothing here.we only need the finally code.
                    mmErrno.SetCode(mmErrno.MM_EIO);
                }
                catch (Exception e) 
                {
                    // real Exception.
                    mmLogger.LogE("exception: " + e.toString());
                    mmErrno.SetCode(mmErrno.MM_EINVAL);
                }
            }
            else
            {
                // the socket kis a invalid.
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
        }
        return afd;
    }
    //we not use c style char* but use buffer and offset target the real buffer,because other language might not have pointer.
    public int Recv(byte[] buffer, int offset, int length, int flags)
    {
        // android assertions are unreliable.
        // assert mm_socket.SOCK_STREAM == this.sock_type : "mm_socket.SOCK_STREAM == this.d_type is not valid.";
        int rt = -1;
        if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            SocketChannel _tcp_socket = (SocketChannel) this.socket;
            try 
            {
                ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
                rt = _tcp_socket.read(byte_buffer);
                mmErrno.SetCode(0);
            } 
            catch (NotYetConnectedException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_ENOTCONN);
            }   
            catch (IOException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EIO);
            }           
            catch (Exception e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
        }
        return rt;
    }
    public int Send(byte[] buffer, int offset, int length, int flags)
    {
        // android assertions are unreliable.
        // assert mm_socket.SOCK_STREAM == this.sock_type : "mm_socket.SOCK_STREAM == this.d_type is not valid.";
        int rt = -1;
        if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            SocketChannel _tcp_socket = (SocketChannel) this.socket;
            try 
            {
                ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
                rt = _tcp_socket.write(byte_buffer);
                mmErrno.SetCode(0);
            } 
            catch (NotYetConnectedException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_ENOTCONN);
            }   
            catch (IOException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EIO);
            }           
            catch (Exception e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
        }
        return rt;
    }
    public int SendDgram(byte[] buffer, int offset, int length, int flags, mmSockaddr remote)
    {
        // android assertions are unreliable.
        // assert mm_socket.SOCK_DGRAM == this.sock_type : "mm_socket.SOCK_DGRAM == this.d_type is not valid.";
        int rt = -1;
        if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            DatagramChannel _udp_socket = (DatagramChannel) this.socket;
            try 
            {
                ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
                InetSocketAddress isa = new InetSocketAddress(remote.node,remote.port);
                rt = _udp_socket.send(byte_buffer, isa);
                mmErrno.SetCode(0);
            } 
            catch (ClosedByInterruptException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EINTR);
            }
            catch (AsynchronousCloseException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_ECONNABORTED);
            }
            catch (ClosedChannelException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_ENETRESET);
            }
            catch (SecurityException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
            catch (IOException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EIO);
            }
            catch (Exception e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
        }
        return rt;  
    }
    public int RecvDgram(byte[] buffer, int offset, int length, int flags, mmSockaddr remote)
    {
        // android assertions are unreliable.
        // assert mm_socket.SOCK_DGRAM == this.sock_type : "mm_socket.SOCK_DGRAM == this.d_type is not valid.";
        int rt = -1;
        if(mmOSSocket.MM_INVALID_SOCKET != this.socket)
        {
            DatagramChannel _udp_socket = (DatagramChannel) this.socket;
            try 
            {
                ByteBuffer byte_buffer = ByteBuffer.wrap(buffer, offset, length);
                InetSocketAddress isa = (InetSocketAddress)_udp_socket.receive(byte_buffer);
                if(null == isa)
                {
                    mmErrno.SetCode(mmErrno.MM_EAGAIN);
                }
                else
                {
                    remote.node = isa.getAddress().getHostAddress();
                    remote.port = isa.getPort();
                    rt = byte_buffer.position();
                    mmErrno.SetCode(0);
                }
            }
            catch (ClosedByInterruptException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EINTR);
            }
            catch (AsynchronousCloseException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_ECONNABORTED);
            }
            catch (ClosedChannelException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_ENETRESET);
            }
            catch (SecurityException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
            catch (IOException e) 
            {
                // do nothing here.we only need the finally code.
                mmErrno.SetCode(mmErrno.MM_EIO);
            }
            catch (Exception e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }   
        }
        return rt;
    }
    // (fd)|ip-port --> ip-port
    // ipv6 (65535)|2001:0DB8:0000:0023:0008:0800:200C:417A-65535 --> 2001:0DB8:0000:0023:0008:0800:200C:417A-65535
    // ipv4                         (65535)|192.168.111.123-65535 --> 192.168.111.123-65535
    public String ToString()
    {
        return mmSockaddr.SockaddrNativeRemoteString(this.ss_native,this.ss_remote,this.hashCode());
    }
}
