package org.mm.graphics;

@SuppressWarnings({"unused"})
public class mmGeometry
{
    // The default is mmLineJoinMiter.
    // enum mmLineJoin_t
    public static final int mmLineJoinMiter      = 0;
    public static final int mmLineJoinRound      = 1;
    public static final int mmLineJoinBevel      = 2;

    // The default is mmLineCapButt.
    // enum mmLineCap_t
    public static final int mmLineCapButt        = 0;
    public static final int mmLineCapRound       = 1;
    public static final int mmLineCapSquare      = 2;

    // default stroke path LineMiterLimit 10.0f
    public static final float mmLineMiterLimitDefault      = 10.0f;
}
