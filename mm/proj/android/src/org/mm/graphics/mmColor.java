package org.mm.graphics;

@SuppressWarnings({"unused"})
public class mmColor
{
    public float r = 0.0f;
    public float g = 0.0f;
    public float b = 0.0f;
    public float a = 0.0f;

    public void Init()
    {
        this.Reset();
    }
    public void Destroy()
    {
        this.Reset();
    }

    public void Reset()
    {
        this.r = 0.0f;
        this.g = 0.0f;
        this.b = 0.0f;
        this.a = 0.0f;
    }

    public void Zero()
    {
        this.Reset();
    }

    public void Make(float r, float g, float b, float a)
    {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public boolean Equals(mmColor q)
    {
        return
            this.r == q.r &&
            this.g == q.g &&
            this.b == q.b &&
            this.a == q.a;
    }

    public void Assign(mmColor q)
    {
        this.r = q.r;
        this.g = q.g;
        this.b = q.b;
        this.a = q.a;
    }

    @SuppressWarnings("UnusedAssignment")
    public int Compare(mmColor q)
    {
        float d = 0.0f;
        if(this.r != q.r)
        {
            d = this.r - q.r;
            return (d > 0.0f) ? (int)Math.ceil(d) : (int)Math.floor(d);
        }
        if(this.g != q.g)
        {
            d = this.g - q.g;
            return (d > 0.0f) ? (int)Math.ceil(d) : (int)Math.floor(d);
        }
        if(this.b != q.b)
        {
            d = this.b - q.b;
            return (d > 0.0f) ? (int)Math.ceil(d) : (int)Math.floor(d);
        }
        if(this.a != q.a)
        {
            d = this.a - q.a;
            return (d > 0.0f) ? (int)Math.ceil(d) : (int)Math.floor(d);
        }
        return 0;
    }

    // l ==> r
    static public void mmColorAssign(final float[] l, final float[] r)
    {
        l[0] = r[0];
        l[1] = r[1];
        l[2] = r[2];
        l[3] = r[3];
    }
    @SuppressWarnings("UnusedAssignment")
    static public int mmColorCompare(final float[] l, final float[] r)
    {
        float d = 0.0f;
        int i = 0;
        for (i = 0;i < 4;++i)
        {
            if(l[i] != r[i])
            {
                d = l[0] - r[0];
                return (d > 0.0f) ? (int)Math.ceil(d) : (int)Math.floor(d);
            }
        }
        return 0;
    }

    static public void mmColorMake(final float[] c, float r, float g, float b, float a)
    {
        c[0] = r;
        c[1] = g;
        c[2] = b;
        c[3] = a;
    }
}
