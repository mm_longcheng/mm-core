package org.mm.math;

@SuppressWarnings({"unused"})
public class mmRange
{
    // offset
    public int o = 0;
    // length
    public int l = 0;

    public void Init()
    {
        this.o = 0;
        this.l = 0;
    }
    public void Destroy()
    {
        this.o = 0;
        this.l = 0;
    }

    public void Reset()
    {
        this.o = 0;
        this.l = 0;
    }

    public void Zero()
    {
        this.o = 0;
        this.l = 0;
    }

    public void Make(int o, int l)
    {
        this.o = o;
        this.l = l;
    }

    public boolean Equals(mmRange q)
    {
        return this.o == q.o && this.l == q.l;
    }

    public void Assign(mmRange q)
    {
        this.o = q.o;
        this.l = q.l;
    }

    public int Compare(mmRange q)
    {
        mmRange p = this;
        // [l, r)
        int lo = p.o;
        int ro = q.o;
        int lr = p.o + p.l;
        int rr = q.o + q.l;
        if (lo <= ro && rr <= lr)
        {
            return 0;
        }
        else if (lo >= rr)
        {
            return (lo == rr) ? +1 : (lo - rr);
        }
        else if (ro >= lr)
        {
            return (lr == ro) ? -1 : (lr - ro);
        }
        else
        {
            return lo - ro;
        }
    }
}
