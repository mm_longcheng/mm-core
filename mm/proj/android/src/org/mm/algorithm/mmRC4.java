/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.algorithm;

@SuppressWarnings("unused")
public class mmRC4
{
    // sbox length.
    public static final int SBOX_LENGTH = 256;
    
    public int x;
    public int y;
    public int[] data = new int[SBOX_LENGTH];

    public void Init()
    {
        this.x = 0;
        this.y = 0;
        java.util.Arrays.fill(this.data, 0, SBOX_LENGTH, (byte) 0);
    }
    
    public void Destroy()
    {
        this.x = 0;
        this.y = 0;
        java.util.Arrays.fill(this.data, 0, SBOX_LENGTH, (byte) 0);
    }
    
    public String Options()
    {
        return ("rc4(int)");
    }
    
    private static class __SkData
    {
        public int tmp = 0;
        public int id1 = 0;
        public int id2 = 0;
        public int[] d = null;
        //
        public byte[] buffer = null;
        public int offset = 0;
        public int length = 0;
    }
    /*-
     * #define SK_LOOP(d,n) { \
     *       tmp=d[(n)]; \
     *       id2 = (data[id1] + tmp + id2) & 0xff; \
     *       if (++id1 == len) id1=0; \
     *       d[(n)]=d[id2]; \
     *       d[id2]=tmp; }
     */
    private static void __SKLoop(__SkData m, int[] d, int n)
    {
        m.tmp=d[n];
        m.id2 = (m.buffer[m.offset+m.id1] + m.tmp + m.id2) & 0xff;
        if (++m.id1 == m.length) m.id1=0;
        d[n]=d[m.id2];
        d[m.id2]=m.tmp;
    }
    /*-
     * RC4 as implemented from a posting from
     * Newsgroups: sci.crypt
     * From: sterndark@netcom.com (David Sterndark)
     * Subject: RC4 Algorithm revealed.
     * Message-ID: <sternCvKL4B.Hyy@netcom.com>
     * Date: Wed, 14 Sep 1994 06:35:31 GMT
     */
    @SuppressWarnings({"UnusedAssignment", "PointlessArithmeticExpression"})
    public void SetKey(byte[] buffer , int offset, int length)
    {
        __SkData m = new __SkData();
        int i = 0;
        //
        m.d = this.data;
        this.x = 0;
        this.y = 0;
        m.id1 = m.id2 = 0;
        //
        m.buffer = buffer;
        m.offset = offset;
        m.length = length;
        //
        for (i = 0; i < SBOX_LENGTH; i++)
        {
            m.d[i] = i;
        }
        for (i = 0; i < SBOX_LENGTH; i += 4) 
        {
            __SKLoop(m, m.d, i + 0);
            __SKLoop(m, m.d, i + 1);
            __SKLoop(m, m.d, i + 2);
            __SKLoop(m, m.d, i + 3);
        }
    }

    private static class __CoderData
    {       
        public int[] d = null;
        public int x = 0;
        public int y = 0;
        public int tx = 0;
        public int ty = 0;
    }
    /*-
     * #define LOOP(in,out) \
     *    x=((x+1)&0xff); \
     *    tx=d[x]; \
     *    y=(tx+y)&0xff; \
     *    d[x]=ty=d[y]; \
     *    d[y]=tx; \
     *    (out) = d[(tx+ty)&0xff]^ (in);
     */
    private static void __CoderLoop(__CoderData m, byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o)
    {
        m.x=((m.x+1)&0xff);
        m.tx=m.d[m.x];
        m.y=(m.tx+m.y)&0xff;
        m.d[m.x]=m.ty=m.d[m.y];
        m.d[m.y]=m.tx;
        buffer_o[offset_o] = (byte) (m.d[(m.tx+m.ty)&0xff] ^ (buffer_i[offset_i]));
    }
    /*-
     * RC4 as implemented from a posting from
     * Newsgroups: sci.crypt
     * From: sterndark@netcom.com (David Sterndark)
     * Subject: RC4 Algorithm revealed.
     * Message-ID: <sternCvKL4B.Hyy@netcom.com>
     * Date: Wed, 14 Sep 1994 06:35:31 GMT
     */
    @SuppressWarnings({"UnusedAssignment", "PointlessArithmeticExpression", "ConditionalBreakInInfiniteLoop"})
    public void RC4(int len, byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o)
    {
        __CoderData m = new __CoderData();
        int i = 0;
        //
        m.x = this.x;
        m.y = this.y;
        m.d = this.data;

        i = len >> 3;
        if ( 0 != i ) 
        {
            for (;;) 
            {
                __CoderLoop(m, buffer_i, offset_i + 0, buffer_o, offset_o + 0);
                __CoderLoop(m, buffer_i, offset_i + 1, buffer_o, offset_o + 1);
                __CoderLoop(m, buffer_i, offset_i + 2, buffer_o, offset_o + 2);
                __CoderLoop(m, buffer_i, offset_i + 3, buffer_o, offset_o + 3);
                __CoderLoop(m, buffer_i, offset_i + 4, buffer_o, offset_o + 4);
                __CoderLoop(m, buffer_i, offset_i + 5, buffer_o, offset_o + 5);
                __CoderLoop(m, buffer_i, offset_i + 6, buffer_o, offset_o + 6);
                __CoderLoop(m, buffer_i, offset_i + 7, buffer_o, offset_o + 7);
                offset_i += 8;
                offset_o += 8;
                if (--i == 0)
                {
                    break;
                }   
            }
        }
        i = len & 0x07;
        if ( 0 != i ) 
        {
            for (;;) 
            {
                __CoderLoop(m, buffer_i, offset_i + 0, buffer_o, offset_o + 0);
                if (--i == 0)
                    break;
                __CoderLoop(m, buffer_i, offset_i + 1, buffer_o, offset_o + 1);
                if (--i == 0)
                    break;
                __CoderLoop(m, buffer_i, offset_i + 2, buffer_o, offset_o + 2);
                if (--i == 0)
                    break;
                __CoderLoop(m, buffer_i, offset_i + 3, buffer_o, offset_o + 3);
                if (--i == 0)
                    break;
                __CoderLoop(m, buffer_i, offset_i + 4, buffer_o, offset_o + 4);
                if (--i == 0)
                    break;
                __CoderLoop(m, buffer_i, offset_i + 5, buffer_o, offset_o + 5);
                if (--i == 0)
                    break;
                __CoderLoop(m, buffer_i, offset_i + 6, buffer_o, offset_o + 6);
                if (--i == 0)
                    break;
            }
        }
        this.x = m.x;
        this.y = m.y;
    }

    public void Encrypt(byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o, int length) 
    {
        this.RC4(length, buffer_i, offset_i, buffer_o, offset_o);
    }
    
    public void Decrypt(byte[] buffer_i, int offset_i, byte[] buffer_o, int offset_o, int length) 
    {
        this.RC4(length, buffer_i, offset_i, buffer_o, offset_o);
    }
}
