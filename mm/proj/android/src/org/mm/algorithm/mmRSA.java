/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.algorithm;

import java.math.BigInteger;
import java.util.Random;

import org.mm.random.mmRandom;

@SuppressWarnings("unused")
public class mmRSA
{
    public BigInteger p = null;
    public BigInteger q = null;
    public BigInteger n = null;
    public BigInteger m = null;
    public BigInteger e = null;
    public BigInteger d = null;
    
    public int bits_size = 1024;
    
    public void Init()
    {
        this.e = BigInteger.valueOf(3);
    }
    public void Destroy()
    {
        this.e = BigInteger.valueOf(3);
    }
    public void SetBitsSize(int bits_size)
    {
        this.bits_size = bits_size;
    }
    public void SetPQ(BigInteger p, BigInteger q)
    {
        this.p = p;
        this.q = q;
    }
    public void SetE(BigInteger e)
    {
        this.e = e;
    }
    
    public void RandomPQ()
    {
        Random random = mmRandom.ThreadInstance();
        
        int bitsp = (this.bits_size + 1) / 2;
        int bitsq = this.bits_size - bitsp;
        // p.
        for (;;)
        {
            this.p = BigInteger.probablePrime(bitsp, random);
            BigInteger r2 = this.p.subtract(BigInteger.ONE);
            BigInteger r1 = this.e.gcd(r2);
            if(r1.equals(BigInteger.ONE))
            { 
                break;
            }
        }
        // q
        for (;;) 
        {
            do 
            {
                this.q = BigInteger.probablePrime(bitsq, random);
            } while (this.q.equals(this.p));
            BigInteger r2 = this.p.subtract(BigInteger.ONE);
            BigInteger r1 = this.e.gcd(r2);
            if(r1.equals(BigInteger.ONE))
            { 
                break;
            }
        }
        if(this.p.compareTo(this.q) < 0)
        {
            BigInteger tmp = this.p;
            this.p = this.q;
            this.q = tmp;
        }
    }
    public void Completion()
    {
        // Compute n = pq (modulus)
        this.n = p.multiply(this.q);
        // Compute f(n) = f(p)f(q) = (p - 1)(q - 1) = n - (p + q -1), where f is Euler's totient function.
        // and choose an integer e such that 1 < e < f(n) and gcd(e, f(n)) = 1; i.e., e and f(n) are coprime.
        this.m = (p.subtract(BigInteger.ONE)).multiply(this.q.subtract(BigInteger.ONE));
        // Determine d as d = e-1 (mod f(n)); i.e., d is the multiplicative inverse of e (modulo f(n)).
        BigInteger[] u = mmRSA.Egcd(this.m, this.e);
        BigInteger y = u[1];
        this.d = y.mod(this.m);
    }
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions", "ManualMinMaxCalculation"})
    public static void BigIntegerBinary(BigInteger bi, byte[] buffer, int offset, int length)
    {
        byte[] ba = bi.toByteArray();
        int off = 0;
        int size = ba.length;
        // remove symbol byte.
        if(0 == ba[0])
        {
            size = ba.length - 1;
            off = 1;
        }
        else
        {
            size = ba.length;
            off = 0;
        }
        if(size <= length)
        {
            System.arraycopy(ba,off,buffer,0,size);
        }
        else
        {
            System.arraycopy(ba,off,buffer,0,length);
        }
    }
    public static BigInteger[] Egcd(BigInteger d1, BigInteger d2) 
    {  
        BigInteger[] ret = new BigInteger[3];
        BigInteger u = BigInteger.valueOf(1), u1 = BigInteger.valueOf(0);
        BigInteger v = BigInteger.valueOf(0), v1 = BigInteger.valueOf(1);
        if (d2.compareTo(d1) > 0)
        {  
            BigInteger tem = d1;
            d1 = d2;
            d2 = tem;
        }  
        while (d2.compareTo(BigInteger.valueOf(0)) != 0) 
        {  
            BigInteger tq = d1.divide(d2);      // tq = d1 / d2  
            BigInteger tu = u;
            u = u1;
            u1 = tu.subtract(tq.multiply(u1));  // u1 =tu - tq * u1  
            BigInteger tv = v;
            v = v1;
            v1 = tv.subtract(tq.multiply(v1));  // v1 = tv - tq * v1  
            BigInteger td1 = d1;
            d1 = d2;
            d2 = td1.subtract(tq.multiply(d2)); // d2 = td1 - tq * d2  
            ret[0] = u;
            ret[1] = v;
            ret[2] = d1;
        }  
        return ret;
    }
}
