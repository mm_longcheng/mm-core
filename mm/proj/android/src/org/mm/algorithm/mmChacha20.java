/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.algorithm;

/*
 * Quick-n-dirty standalone implementation of ChaCha 256-bit
 * <p/>
 * Created by Clarence Ho on 20150729
 * <p/>
 * References:
 * ~ http://cr.yp.to/chacha/chacha-20080128.pdf
 * ~ https://tools.ietf.org/html/draft-irtf-cfrg-chacha20-poly1305-01
 * ~ https://github.com/quartzjer/chacha20
 * ~ https://github.com/jotcmd/chacha20
 */
@SuppressWarnings("unused")
public class mmChacha20
{
    /*
     * matrix size in int
     */
    public static final int MATRIX_SIZE = 16;
    
    /*
     * out block size in int
     */
    public static final int BLOCK_SIZE = 64;
    
    /*
     * transform times in int
     */
    public static final int TRANSFORM_TIMES = 20;
    
    /*
     * key size in int
     */
    public static final int KEY_SIZE = 8;
    
    /*
     * counter size in int
     */
    public static final int COUNTER_SIZE = 4;

    public int[] matrix = new int[MATRIX_SIZE];
    
    protected static int LittleEndianToInt(byte[] bs, int i) 
    {
        return 
                ((bs[i    ] & 0xff)      ) | 
                ((bs[i + 1] & 0xff) <<  8) | 
                ((bs[i + 2] & 0xff) << 16) | 
                ((bs[i + 3] & 0xff) << 24);
    }

    protected static void IntToLittleEndian(int n, byte[] bs, int off) 
    {
        bs[  off] = (byte)(n       );
        bs[++off] = (byte)(n >>>  8);
        bs[++off] = (byte)(n >>> 16);
        bs[++off] = (byte)(n >>> 24);
    }
    
    protected static int ROTATE(int v, int c) 
    {
        return (v << c) | (v >>> (32 - c));
    }
    
    protected static void QuarterRound(int[] x, int a, int b, int c, int d) 
    {
        x[a] += x[b]; x[d] = ROTATE(x[d] ^ x[a], 16);
        x[c] += x[d]; x[b] = ROTATE(x[b] ^ x[c], 12);
        x[a] += x[b]; x[d] = ROTATE(x[d] ^ x[a],  8);
        x[c] += x[d]; x[b] = ROTATE(x[b] ^ x[c],  7);
    }

    public void Init()
    {
        java.util.Arrays.fill(this.matrix, 0, MATRIX_SIZE, (byte) 0);
    }
    
    public void Destroy()
    {
        java.util.Arrays.fill(this.matrix, 0, MATRIX_SIZE, (byte) 0);
    }
    
    public void Keysetup(byte[] k)
    {
        // convert magic number to string: "expand 32-byte k"
        this.matrix[ 0] = 0x61707865;
        this.matrix[ 1] = 0x3320646e;
        this.matrix[ 2] = 0x79622d32;
        this.matrix[ 3] = 0x6b206574;
        
        this.matrix[ 4] = LittleEndianToInt(k,  0);
        this.matrix[ 5] = LittleEndianToInt(k,  4);
        this.matrix[ 6] = LittleEndianToInt(k,  8);
        this.matrix[ 7] = LittleEndianToInt(k, 12);
        this.matrix[ 8] = LittleEndianToInt(k, 16);
        this.matrix[ 9] = LittleEndianToInt(k, 20);
        this.matrix[10] = LittleEndianToInt(k, 24);
        this.matrix[11] = LittleEndianToInt(k, 28);
    }
    public void Ivsetup(byte[] iv)
    {
        this.matrix[12] = 0;
        this.matrix[13] = 0;
        this.matrix[14] = LittleEndianToInt(iv, 0);
        this.matrix[15] = LittleEndianToInt(iv, 4);
    }
    public void Encrypt(byte[] dst, byte[] src, int len) 
    {
        int[] x = new int[MATRIX_SIZE];
        byte[] output = new byte[BLOCK_SIZE];
        int i, dpos = 0, spos = 0;

        while (len > 0) 
        {
            System.arraycopy(this.matrix, 0, x, 0, MATRIX_SIZE);
            for (i = TRANSFORM_TIMES; i > 0; i -= 2) 
            {
                QuarterRound(x, 0, 4,  8, 12);
                QuarterRound(x, 1, 5,  9, 13);
                QuarterRound(x, 2, 6, 10, 14);
                QuarterRound(x, 3, 7, 11, 15);
                QuarterRound(x, 0, 5, 10, 15);
                QuarterRound(x, 1, 6, 11, 12);
                QuarterRound(x, 2, 7,  8, 13);
                QuarterRound(x, 3, 4,  9, 14);
            }
            for (i = MATRIX_SIZE; i-- > 0;) x[i] += this.matrix[i];
            for (i = MATRIX_SIZE; i-- > 0;) IntToLittleEndian(x[i], output, 4 * i);

            // (1) check block count is 32-bit vs 64-bit; (2) java int is signed!
            this.matrix[12] += 1;
            if (this.matrix[12] <= 0) 
            {
                this.matrix[13] += 1;
            }
            if (len <= BLOCK_SIZE) 
            {
                for (i = len; i-- > 0;) 
                {
                    dst[i + dpos] = (byte) (src[i + spos] ^ output[i]);
                }
                return;
            }
            for (i = BLOCK_SIZE; i-- > 0;) 
            {
                dst[i + dpos] = (byte) (src[i + spos] ^ output[i]);
            }
            len -= BLOCK_SIZE;
            spos += BLOCK_SIZE;
            dpos += BLOCK_SIZE;
        }
    }
    
    public void Decrypt(byte[] dst, byte[] src, int len) 
    {
        this.Encrypt(dst, src, len);
    }
}
