/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.algorithm;

@SuppressWarnings("unused")
public class mmHex
{
    public static void ByteToHex(byte n, char[] pBytes)
    {
        pBytes[0] = Character.forDigit((n >> 4) & 0xF, 16);
        pBytes[1] = Character.forDigit((n & 0xF), 16);
    }

    public static int HexCharToDigit(char hHexChar)
    {
        return Character.digit(hHexChar, 16);
    }

    public static byte HexToByte(String hHexString)
    {
        int hDigit0 = HexCharToDigit(hHexString.charAt(0));
        int hDigit1 = HexCharToDigit(hHexString.charAt(1));
        return (byte) ((hDigit0 << 4) + hDigit1);
    }

    @SuppressWarnings("ForLoopReplaceableByForEach")
    public static String EncodeHexString(byte[] pByteArray)
    {
        char[] pBytes = new char[2];
        StringBuilder pBuilder = new StringBuilder();
        for (int i = 0; i < pByteArray.length; i++)
        {
            ByteToHex(pByteArray[i], pBytes);
            pBuilder.append(pBytes);
        }
        return pBuilder.toString();
    }

    public static byte[] DecodeHexString(String hHexString)
    {
        if (hHexString.length() % 2 == 1)
        {
            return null;
        }

        byte[] pByteArray = new byte[hHexString.length() / 2];
        for (int i = 0; i < hHexString.length(); i += 2)
        {
            pByteArray[i / 2] = HexToByte(hHexString.substring(i, i + 2));
        }
        return pByteArray;
    }
}
