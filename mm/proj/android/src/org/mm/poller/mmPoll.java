/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.poller;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;

//we catch all Exception e but print nothing but catch the error code.
@SuppressWarnings({"Convert2Diamond", "unused"})
public class mmPoll
{
    // java poll event.
    public static final int OP_READABLE = SelectionKey.OP_READ;
    public static final int OP_WRITABLE = SelectionKey.OP_WRITE;
    // java special.
    public static final int OP_ECONNECT = SelectionKey.OP_CONNECT;
    public static final int OP_ACCEPTOR = SelectionKey.OP_ACCEPT;
    
    public Vector<mmPollEvent> pe = new Vector<mmPollEvent>();
    public Selector impl = null;

    public void Init()
    {
        this.pe.clear();
        try 
        {
            this.impl = Selector.open();
        } 
        catch (IOException e) 
        {
            // real Exception.
            mmLogger.LogE("exception: " + e.toString());
        }
    }
    public void Destroy()
    {
        this.pe.clear();
        if(null != this.impl)
        {
            try 
            {
                this.impl.close();
            } 
            catch (IOException e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
            }
            this.impl = null;
        }
    }
    public int AddEvent(AbstractSelectableChannel fd, int mask, Object ud)
    {
        if(mmOSSocket.MM_INVALID_SOCKET == fd)
        {
            // socket is a invalid.
            return -1;
        }
        int rt = 0;
        int events = 0;
        if (0 != (mask & mmPollEvent.MM_PE_READABLE)) events |= OP_READABLE;
        if (0 != (mask & mmPollEvent.MM_PE_WRITABLE)) events |= OP_WRITABLE;
        if (0 != (mask & mmPollEvent.MM_PE_ECONNECT)) events |= OP_ECONNECT;
        if (0 != (mask & mmPollEvent.MM_PE_ACCEPTOR)) events |= OP_ACCEPTOR;
        try 
        {
            fd.register(this.impl, events, ud);
        } 
        catch (ClosedChannelException e) 
        {
            // do nothing here.we only need the finally code.
            rt = -1;
        }
        return rt;
    }
    public int ModEvent(AbstractSelectableChannel fd, int mask, Object ud)
    {
        if(mmOSSocket.MM_INVALID_SOCKET == fd)
        {
            // socket is a invalid.
            return -1;
        }
        int rt = 0;
        int events = 0;
        if (0 != (mask & mmPollEvent.MM_PE_READABLE)) events |= OP_READABLE;
        if (0 != (mask & mmPollEvent.MM_PE_WRITABLE)) events |= OP_WRITABLE;
        if (0 != (mask & mmPollEvent.MM_PE_ECONNECT)) events |= OP_ECONNECT;
        if (0 != (mask & mmPollEvent.MM_PE_ACCEPTOR)) events |= OP_ACCEPTOR;
        // mod current poll event.
        SelectionKey sk = fd.keyFor(this.impl);
        if(null != sk)
        {
            sk.interestOps(events);
        }
        else
        {
            rt = -1;
        }
        return rt;
    }
    public int DelEvent(AbstractSelectableChannel fd, int mask, Object ud)
    {
        if(mmOSSocket.MM_INVALID_SOCKET == fd)
        {
            // socket is a invalid.
            return -1;
        }
        int rt = 0;
        int events = 0;     
        if (0 != (mask & mmPollEvent.MM_PE_READABLE)) events |= OP_READABLE;
        if (0 != (mask & mmPollEvent.MM_PE_WRITABLE)) events |= OP_WRITABLE;
        if (0 != (mask & mmPollEvent.MM_PE_ECONNECT)) events |= OP_ECONNECT;
        if (0 != (mask & mmPollEvent.MM_PE_ACCEPTOR)) events |= OP_ACCEPTOR;
        // del current poll event.
        SelectionKey sk = fd.keyFor(this.impl);
        if(null != sk)
        {
            int evt = sk.interestOps();
            sk.interestOps(evt & (~events));
        }
        else
        {
            rt = -1;
        }
        return rt;
    }
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public int WaitEvent(int milliseconds)
    {
        int n = 0;
        int i = 0;
        do
        {
            try 
            {
                n = this.impl.select(milliseconds);
                mmErrno.SetCode(0);
            }
            catch (ClosedSelectorException e) 
            {
                // do nothing here.we only need the finally code.
                n = -1;
                mmErrno.SetCode(mmErrno.MM_ENETDOWN);
                break;
            }
            catch (IllegalArgumentException e) 
            {
                // do nothing here.we only need the finally code.
                n = -1;
                mmErrno.SetCode(mmErrno.MM_EINVAL);
                break;
            }
            catch (IOException e) 
            {
                // do nothing here.we only need the finally code.
                n = -1;
                mmErrno.SetCode(mmErrno.MM_EIO);
                break;
            }
            catch (Exception e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
                n = -1;
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
            // length is zero or select occur error.break here.
            if( 0 == n || -1 == n )
            {
                // we have none event.
                break;
            }
            if(null ==  this.impl)
            {
                // we have none event.
                n = -1;
                break;
            }
            this.pe.setSize(n);
            Set<SelectionKey> pe = this.impl.selectedKeys();
            Iterator<SelectionKey> it = pe.iterator();
            while (it.hasNext())
            {
                SelectionKey ee = it.next();
                int events = ee.interestOps();
                int mask = 0;
                //
                if (0 != (events & OP_READABLE))  mask |= mmPollEvent.MM_PE_READABLE;
                if (0 != (events & OP_WRITABLE))  mask |= mmPollEvent.MM_PE_WRITABLE;
                if (0 != (events & OP_ECONNECT))  mask |= mmPollEvent.MM_PE_ECONNECT;
                if (0 != (events & OP_ACCEPTOR))  mask |= mmPollEvent.MM_PE_ACCEPTOR;
                //
                mmPollEvent elem = new mmPollEvent();
                elem.s = ee.attachment();
                elem.mask = mask;
                this.pe.set(i, elem);
                ++i;
                //
                it.remove();
            }
        }while(false);
        return n;
    }   
    public String Api()
    {
        return "java.nio.channels.Selector";
    }
}
