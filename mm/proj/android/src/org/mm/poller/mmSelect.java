/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.poller;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.spi.AbstractSelectableChannel;

import org.mm.core.mmErrno;
import org.mm.core.mmLogger;
import org.mm.core.mmOSSocket;

@SuppressWarnings("unused")
public class mmSelect
{   
    public Selector impl = null;
    
    public mmPollEvent poll_event;
    public AbstractSelectableChannel fd;// fd.
    
    public void Init()
    {
        try 
        {
            this.impl = Selector.open();
        } 
        catch (IOException e) 
        {
            // real Exception.
            mmLogger.LogE("exception: " + e.toString());
        }
        this.poll_event.mask = mmPollEvent.MM_PE_NONEABLE;
        this.poll_event.s = null;
        this.fd = (AbstractSelectableChannel) mmOSSocket.MM_INVALID_SOCKET;
    }
    public void Destroy()
    {
        if(null != this.impl)
        {
            try 
            {
                this.impl.close();
            } 
            catch (IOException e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
            }
            this.impl = null;
        }
        this.poll_event.mask = mmPollEvent.MM_PE_NONEABLE;
        this.poll_event.s = null;
        this.fd = (AbstractSelectableChannel) mmOSSocket.MM_INVALID_SOCKET;
    }
    //////////////////////////////////////////////////////////////////////////
    public void SetFd(AbstractSelectableChannel fd)
    {
        this.fd = fd;
    }
    //////////////////////////////////////////////////////////////////////////
    public int AddEvent(int mask, Object ud)
    {
        if(mmOSSocket.MM_INVALID_SOCKET == this.fd)
        {
            // socket is a invalid.
            return -1;
        }
        int rt = 0;
        int events = 0;
        if (0 != (mask & mmPollEvent.MM_PE_READABLE)) events |= mmPoll.OP_READABLE;
        if (0 != (mask & mmPollEvent.MM_PE_WRITABLE)) events |= mmPoll.OP_WRITABLE;
        if (0 != (mask & mmPollEvent.MM_PE_ECONNECT)) events |= mmPoll.OP_ECONNECT;
        if (0 != (mask & mmPollEvent.MM_PE_ACCEPTOR)) events |= mmPoll.OP_ACCEPTOR;
        try 
        {
            this.fd.register(this.impl, events, ud);
        } 
        catch (ClosedChannelException e) 
        {
            // do nothing here.we only need the finally code.
            rt = -1;
        }
        return rt;
    }
    public int ModEvent(int mask, Object ud)
    {
        if(mmOSSocket.MM_INVALID_SOCKET == this.fd)
        {
            // socket is a invalid.
            return -1;
        }
        int rt = 0;
        int events = 0;
        if (0 != (mask & mmPollEvent.MM_PE_READABLE)) events |= mmPoll.OP_READABLE;
        if (0 != (mask & mmPollEvent.MM_PE_WRITABLE)) events |= mmPoll.OP_WRITABLE;
        if (0 != (mask & mmPollEvent.MM_PE_ECONNECT)) events |= mmPoll.OP_ECONNECT;
        if (0 != (mask & mmPollEvent.MM_PE_ACCEPTOR)) events |= mmPoll.OP_ACCEPTOR;
        // mod current poll event.
        SelectionKey sk = this.fd.keyFor(this.impl);
        if(null != sk)
        {
            sk.interestOps(events);
        }
        else
        {
            rt = -1;
        }
        return rt;
    }
    public int DelEvent(int mask, Object ud)
    {
        if(mmOSSocket.MM_INVALID_SOCKET == this.fd)
        {
            // socket is a invalid.
            return -1;
        }
        int rt = 0;
        int events = 0;     
        if (0 != (mask & mmPollEvent.MM_PE_READABLE)) events |= mmPoll.OP_READABLE;
        if (0 != (mask & mmPollEvent.MM_PE_WRITABLE)) events |= mmPoll.OP_WRITABLE;
        if (0 != (mask & mmPollEvent.MM_PE_ECONNECT)) events |= mmPoll.OP_ECONNECT;
        if (0 != (mask & mmPollEvent.MM_PE_ACCEPTOR)) events |= mmPoll.OP_ACCEPTOR;
        // del current poll event.
        SelectionKey sk = this.fd.keyFor(this.impl);
        if(null != sk)
        {
            int evt = sk.interestOps();
            sk.interestOps(evt & (~events));
        }
        else
        {
            rt = -1;
        }
        return rt;
    }
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public int WaitEvent(int milliseconds)
    {
        int n = 0;
        do
        {
            try 
            {
                n = this.impl.select(milliseconds);
                mmErrno.SetCode(0);
            }
            catch (ClosedSelectorException e) 
            {
                // do nothing here.we only need the finally code.
                n = -1;
                mmErrno.SetCode(mmErrno.MM_ENETDOWN);
                break;
            }
            catch (IllegalArgumentException e) 
            {
                // do nothing here.we only need the finally code.
                n = -1;
                mmErrno.SetCode(mmErrno.MM_EINVAL);
                break;
            }
            catch (IOException e) 
            {
                // do nothing here.we only need the finally code.
                n = -1;
                mmErrno.SetCode(mmErrno.MM_EIO);
                break;
            }
            catch (Exception e) 
            {
                // real Exception.
                mmLogger.LogE("exception: " + e.toString());
                n = -1;
                mmErrno.SetCode(mmErrno.MM_EINVAL);
            }
            // length is zero or select occur error.break here.
            if( 0 == n || -1 == n )
            {
                // we have none event.
                break;
            }
            if(null ==  this.impl)
            {
                // we have none event.
                n = -1;
                break;
            }
            SelectionKey ee = this.fd.keyFor(this.impl);
            int events = ee.interestOps();
            int mask = 0;
            //
            if (0 != (events & mmPoll.OP_READABLE))  mask |= mmPollEvent.MM_PE_READABLE;
            if (0 != (events & mmPoll.OP_WRITABLE))  mask |= mmPollEvent.MM_PE_WRITABLE;
            if (0 != (events & mmPoll.OP_ECONNECT))  mask |= mmPollEvent.MM_PE_ECONNECT;
            if (0 != (events & mmPoll.OP_ACCEPTOR))  mask |= mmPollEvent.MM_PE_ACCEPTOR;
            
            this.poll_event.s = ee.attachment();
            this.poll_event.mask = mask;
        }while(false);
        return n;
    }
    //////////////////////////////////////////////////////////////////////////
    public String Api()
    {
        return "java.nio.channels.Selector";
    }
}
