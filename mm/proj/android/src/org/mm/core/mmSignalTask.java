/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

@SuppressWarnings("unused")
public class mmSignalTask
{
    // signal task default success nearby time milliseconds.
    public static final int MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC = 0;
    // signal task default failure nearby time milliseconds.
    public static final int MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC = 200;
    
    public static class mmSignalTaskCallback
    {
        // factor for condition wait.return code for 0 is fire event -1 for condition wait.
        public int Factor( Object obj) 
        {
            return -1;
        }
        // handle for fire event.return code result 0 for success -1 for failure.
        public int Handle( Object obj)
        {
            return 0;
        }
        
        public Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
    }
    
    public mmSignalTaskCallback callback = new mmSignalTaskCallback();
    public mmThread signal_thread = new mmThread(); 
    public mmThread.Mutex signal_locker = new mmThread.Mutex();
    public mmThread.Cond signal_cond = new mmThread.Cond(this.signal_locker);   
    public int msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC ms.
    public int msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC ms.
    public int msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
    public int state = mmThread.MM_TS_CLOSED;// mm_thread_state_t,default is MM_TS_CLOSED(0)

    public void Init()
    {
        this.callback.Init();
        this.signal_thread.Init();
        this.signal_locker.Init();
        this.signal_cond.Init();
        this.msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
        this.msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
        this.msec_delay = 0;
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Destroy()
    {
        this.callback.Destroy();
        this.signal_thread.Destroy();
        this.signal_locker.Destroy();
        this.signal_cond.Destroy();
        this.msec_success_nearby = MM_SIGNAL_TASK_SUCCESS_NEARBY_MSEC;
        this.msec_failure_nearby = MM_SIGNAL_TASK_FAILURE_NEARBY_MSEC;
        this.msec_delay = 0;
        this.state = mmThread.MM_TS_CLOSED;
    }
    //////////////////////////////////////////////////////////////////////////
    // lock to make sure the knot is thread safe.
    public void Lock()
    {
        this.signal_locker.Lock();
    }
    // unlock knot.
    public void Unlock()
    {
        this.signal_locker.Unlock();
    }
    public void SetCallback(mmSignalTaskCallback callback)
    {
        // android assertions are unreliable.
        // assert null != callback : "you can not assign null callback.";
        this.callback = callback;
    }
    public void SetSuccessNearby(int nearby)
    {
        this.msec_success_nearby = nearby;
    }
    public void SetFailureNearby(int nearby)
    {
        this.msec_failure_nearby = nearby;
    }
    //////////////////////////////////////////////////////////////////////////
    // wait.
    @SuppressWarnings({"UnusedAssignment", "UnnecessaryContinue"})
    public void SignalWait()
    {
        int code = -1;
        int _nearby_time = 0;
        // android assertions are unreliable.
        // assert null != this.callback : "this.callback is a null.";
        if ( 0 < this.msec_delay )
        {
            mmTime.mmMSleep(this.msec_delay);
        }
        while(mmThread.MM_TS_MOTION == this.state)
        {
            // factor
            code = this.callback.Factor(this);
            if (0 == code)
            {
                this.signal_locker.Lock();
                this.signal_cond.Await();
                this.signal_locker.Unlock();
                // the condition is timeout invalid.we enter a new process.
                // we must make sure the code == 0.
                continue;
            }
            // fire event.
            code = this.callback.Handle(this);
            _nearby_time = 0 == code ? this.msec_success_nearby : this.msec_failure_nearby;
            // some os pthread timedwait impl precision is low.
            // MM_MSEC_PER_SEC check can avoid some precision problem
            // but will extend the application shutdown time.
            if (0 == _nearby_time)
            {
                // next loop immediately.
                continue;
            }
            else if(mmTime.MM_MSEC_PER_SEC < _nearby_time)
            {
                // timedwait a while.
                this.signal_locker.Lock();
                this.signal_cond.Timewait(_nearby_time);
                this.signal_locker.Unlock();
            }
            else
            {
                // msleep a while.
                mmTime.mmMSleep(_nearby_time);
            }
        }
    }
    // signal.
    public void Signal()
    {
        this.signal_locker.Lock();
        this.signal_cond.Signal();
        this.signal_locker.Unlock();
    }
    //////////////////////////////////////////////////////////////////////////
    // start wait thread.
    public void Start()
    {
        this.state = mmThread.MM_TS_FINISH == this.state ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        this.signal_thread.Start(new __static_SignalTask_PollWaitThread(this));
    }
    // interrupt wait thread.
    public void Interrupt()
    {
        this.state = mmThread.MM_TS_CLOSED;
        this.Signal();
    }
    // shutdown wait thread.
    public void Shutdown()
    {
        this.state = mmThread.MM_TS_FINISH;
        this.Signal();
    }
    // join wait thread.
    public void Join()
    {
        this.signal_thread.Join();
    }
    //////////////////////////////////////////////////////////////////////////
    @SuppressWarnings("UnusedAssignment")
    static final class __static_SignalTask_PollWaitThread implements Runnable
    {
        __static_SignalTask_PollWaitThread(mmSignalTask _p)
        {
            this.p = _p;
        }
        public mmSignalTask p = null;
        @Override
        public void run() 
        {
            p.SignalWait();
        }       
    }
}
