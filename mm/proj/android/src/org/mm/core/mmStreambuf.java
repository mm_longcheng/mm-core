/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

//output sequence (put)
//+++++-------
//|    |      |
//0  pptr   size
///////////////////////
//input  sequence (get)
//+++++-------
//|    |      |
//0  gptr   size
@SuppressWarnings("unused")
public class mmStreambuf
{
    public static final int MM_STREAMBUF_PAGE_SIZE = 1024;
    
    // output sequence (put)
    public int pptr = 0;
    // input  sequence (get)
    public int gptr = 0;

    // max size for this streambuf.default is MM_STREAMBUF_PAGE_SIZE = 1024
    public int size = 0;
    public byte[] buff = null;// buffer for real data.
    
    public void Init()
    {
        this.pptr = 0;
        this.gptr = 0;
        this.size = MM_STREAMBUF_PAGE_SIZE;
        this.buff = new byte[MM_STREAMBUF_PAGE_SIZE];
    }
    public void Destroy()
    {
        this.pptr = 0;
        this.gptr = 0;
        this.size = 0;
        this.buff = null;
    }
    
    // copy q to p.
    public void CopyFrom(mmStreambuf q)
    {
        byte[] buff = new byte[q.size];
        System.arraycopy(q.buff,0,buff,0,q.size);
        this.buff = buff;
        this.size = q.size;
        // setp
        this.pptr = q.pptr;
        // setg
        this.gptr = q.gptr;
    }
    // add max streambuf size.do not call this function if you known what it do.
    public void AddSize(int size)
    {
        int cursz = this.pptr - this.gptr;
        byte[] buff = new byte[this.size + size];
        System.arraycopy(this.buff,this.gptr,buff,this.gptr,cursz);
        this.buff = buff;
        this.size += size;
    }
    // rmv max streambuf size.do not call this function if you known what it do.
    public void RmvSize(int size)
    {
        int cursz = this.pptr - this.gptr;
        byte[] buff = new byte[this.size - size];
        System.arraycopy(this.buff,this.gptr,buff,this.gptr,cursz);
        this.buff = buff;
        this.size -= size;
    }
    public void Removeget()
    {
        int rsize = this.gptr;
        if (0 < rsize)
        {
            int wr = this.pptr;
            System.arraycopy(this.buff, rsize, this.buff, 0, this.pptr - this.gptr);
            wr -= rsize;
            // setp
            this.pptr = wr;
            // setg
            this.gptr = 0;
        }
    }
    @SuppressWarnings("UnusedAssignment")
    public void RemovegetSize(int rsize)
    {
        int rdmax = 0;
        this.Clearget();
        rdmax = this.pptr - this.gptr;
        if (rdmax < rsize)
        {
            rsize = rdmax;
        }
        if (0 < rsize)
        {
            int wr = this.pptr;
            System.arraycopy(this.buff, rsize, this.buff, 0, this.pptr - this.gptr);
            wr -= rsize;
            // setp
            this.pptr = wr;
            // setg
            this.gptr = 0;
        }
    }
    public void Clearget()
    {
        // setg
        this.gptr = 0;
    }

    public void Removeput()
    {
        int wsize = this.pptr;
        if (0 < wsize)
        {
            int rd = this.gptr;
            System.arraycopy(this.buff, wsize, this.buff, 0, this.pptr - this.gptr);
            rd -= wsize;
            // setp
            this.pptr = 0;
            // setg
            this.gptr = rd;
        }
    }
    @SuppressWarnings("UnusedAssignment")
    public void RemoveputSize(int wsize)
    {
        int wrmax = 0;
        this.Clearput();
        wrmax = this.pptr - this.gptr;
        if (wrmax < wsize)
        {
            wsize = wrmax;
        }
        if (0 < wsize)
        {
            int rd = this.gptr;
            System.arraycopy(this.buff, wsize, this.buff, 0, this.pptr - this.gptr);
            rd -= wsize;
            // setp
            this.pptr = 0;
            // setg
            this.gptr = rd;
        }
    }
    public void Clearput()
    {
        // setp
        this.pptr = 0;
    }

    public int Getsize()
    {
        return this.gptr;
    }
    public int Putsize()
    {
        return this.pptr;   
    }

    // size for input and output interval.
    public int Size()
    {
        return this.pptr - this.gptr;
    }

    // set get pointer to new pointer.
    public void SetGPtr(int new_ptr)
    {
        // setg
        this.gptr = new_ptr;
    }
    // set put pointer to new pointer.
    public void SetPPtr(int new_ptr)
    {
        // setp
        this.pptr = new_ptr;
    }
    // set get and put pointer to zero.
    public void Reset()
    {
        // setp
        this.pptr = 0;
        // setg
        this.gptr = 0;
    }

    // get get pointer(offset).
    public int GPtr()
    {
        return this.gptr;
    }
    // get put pointer(offset).
    public int PPtr()
    {
        return this.pptr;
    }
    //////////////////////////////////////////////////////////////////////////
    // get data s + offset length is n.and gbump n.
    public int Sgetn(byte[] s, int o, int n)
    {
        while( this.gptr + n > this.size )
        {
            this.OverflowSgetn();
        }
        System.arraycopy(this.buff, this.gptr, s, o, n);
        this.gptr += n;
        return n;
    }
    // put data s + offset length is n.and pbump n.
    public int Sputn(byte[] s, int o, int n)
    {
        while( this.pptr + n > this.size )
        {
            this.OverflowSputn();
        }
        System.arraycopy(s, o, this.buff, this.pptr, n);
        this.pptr += n;
        return n;
    }
    // gptr += n
    public void Gbump( int n )
    {
        this.gptr += n;
    }
    // pptr += n
    public void Pbump( int n )
    {
        this.pptr += n;
    }
    //////////////////////////////////////////////////////////////////////////
    public void OverflowSgetn()
    {
        if(0 < this.gptr)
        {
            // if gptr have g data.we try remove for free space.
            this.Removeget();
        }
        else
        {
            // if gptr no data we just add page size.
            this.AddSize(MM_STREAMBUF_PAGE_SIZE);
        }
    }
    public void OverflowSputn()
    {
        this.AddSize(MM_STREAMBUF_PAGE_SIZE);
    }
    // try decrease.
    // cursz = p->pptr - p->gptr;
    // fresz = p->size - cursz;
    // fresz > n && (p->size)(1/4) > page && (p->size)(3/4) < empty_size
    public void TryDecrease(int n)
    {
        int cursz = this.pptr - this.gptr;
        int fresz = this.size - cursz;
        int ssize = fresz - n;// the surplus size for decrease.
        if (fresz > n && MM_STREAMBUF_PAGE_SIZE < ssize)
        {
            int sz_1_4 = this.size / 4;
            int sz_3_4 = sz_1_4 * 3;
            int sz_sub = sz_1_4 / MM_STREAMBUF_PAGE_SIZE;
            if (sz_sub > 0 && sz_3_4 < ssize)
            {
                int sz_rel = sz_sub * MM_STREAMBUF_PAGE_SIZE;
                // here first removeget.
                this.Removeget();
                // rmv surplus size.
                this.RmvSize(sz_rel);
            }
            else
            {
                // only removeget.
                this.Removeget();
            }
        }
        else
        {
            // only removeget.
            this.Removeget();
        }
    }
    // try increase.
    public void TryIncrease(int n)
    {
        if ( this.pptr + n > this.size )
        {
             int dsz = this.pptr + n - this.size;
             int asz = dsz / MM_STREAMBUF_PAGE_SIZE;
             int bsz = dsz % MM_STREAMBUF_PAGE_SIZE;
             int nsz = ( asz + ( 0 == bsz ? 0 : 1 ) );
             this.AddSize(nsz * MM_STREAMBUF_PAGE_SIZE);
        }
    }
    // aligned streambuf memory if need sputn n size buffer before.
    public void AlignedMemory(int n)
    {
        // if current pptr + n not enough for target n.we make a aligned memory.
        if ( this.pptr + n > this.size )
        {
            int cursz = this.pptr - this.gptr;
            // the free size is enough and memmove size is <  MM_STREAMBUF_PAGE_SIZE.
            if ( this.size - cursz >= n && MM_STREAMBUF_PAGE_SIZE > cursz)
            {
                this.TryDecrease(n);
            } 
            else
            {
                this.TryIncrease(n);
            }
        }
    }
}
