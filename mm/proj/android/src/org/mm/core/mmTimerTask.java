/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("unused")
public class mmTimerTask
{
    // timer task default nearby time milliseconds.
    public static final int MM_TIME_TASK_NEARBY_MSEC = 200;
    
    public static class mmTimerTaskCallback
    {
        public void Handle(mmTimerTask p) {}
        
        public Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
    }
    
    public Timer task_thread = new Timer();// check thread.
    public mmTimerTaskCallback callback = new mmTimerTaskCallback();
    public int nearby_time = MM_TIME_TASK_NEARBY_MSEC;// nearby_time milliseconds for each time wait.default is MM_TIME_TASK_NEARBY_MSEC ms.
    public int msec_delay = 0;// delay milliseconds before first callback.default is 0 ms.
    public int state = mmThread.MM_TS_CLOSED;// mm_thread_state_t,default is MM_TS_CLOSED(0)

    public void Init()
    {
        this.callback.Init();
        this.nearby_time = MM_TIME_TASK_NEARBY_MSEC;
        this.msec_delay = 0;
        this.state = mmThread.MM_TS_CLOSED;
    }
    public void Destroy()
    {
        this.callback.Destroy();
        this.nearby_time = 0;
        this.msec_delay = 0;
        this.state = mmThread.MM_TS_CLOSED;
    }
    //////////////////////////////////////////////////////////////////////////
    // do not assign when rbtree have some elem.
    public void SetCallback(mmTimerTaskCallback callback)
    {
        assert null != callback : "you can not assign null callback.";
        this.callback = callback;
    }
    public void SetNearbyTime(int nearby_time)
    {
        this.nearby_time = nearby_time;
    }
    //////////////////////////////////////////////////////////////////////////
    // sync.
    public void Handle()
    {
        
    }
    //////////////////////////////////////////////////////////////////////////
    // start thread.
    public void Start()
    {
        this.state = mmThread.MM_TS_FINISH == this.state ? mmThread.MM_TS_CLOSED : mmThread.MM_TS_MOTION;
        this.task_thread.schedule(new __TimerTaskDefault(this), this.msec_delay);
    }
    // interrupt thread.
    public void Interrupt()
    {
        this.state = mmThread.MM_TS_CLOSED;
        this.task_thread.cancel();
    }
    // shutdown thread.
    public void Shutdown()
    {
        this.state = mmThread.MM_TS_FINISH;
        this.task_thread.cancel();
    }
    // join thread.
    public void Join()
    {
        // java.util. Timer not need join at all.
    }
    //////////////////////////////////////////////////////////////////////////
    public String Api()
    {
        return "java.util.Timer";
    }
    //////////////////////////////////////////////////////////////////////////
    @SuppressWarnings("UnusedAssignment")
    static final class __TimerTaskDefault extends TimerTask
    {
        public mmTimerTask obj = null;
        
        __TimerTaskDefault(mmTimerTask _p)
        {
            this.obj = _p;
        }
        @Override
        public void run() 
        {
            this.obj.callback.Handle(this.obj);
            if(mmThread.MM_TS_MOTION == this.obj.state)
            {
                this.obj.task_thread.schedule(new __TimerTaskDefault(this.obj), this.obj.nearby_time);
            }           
        }
    }
}
