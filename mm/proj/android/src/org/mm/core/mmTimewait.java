/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

//we catch all Exception e but print nothing but catch the error code.
@SuppressWarnings("unused")
public class mmTimewait
{
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public static int mmTimewait_MSecNearby(mmThread.Cond signal_cond, mmThread.Mutex signal_mutex, int _nearby_time)
    {
        int rt = 0;
        // some os pthread timedwait impl precision is low.
        // MM_MSEC_PER_SEC check can avoid some precision problem
        // but will extend the application shutdown time.
        if (0 == _nearby_time)
        {
            // next loop immediately.
            rt = 0;
        }
        else if(mmTime.MM_MSEC_PER_SEC < _nearby_time)
        {
            signal_mutex.Lock();
            rt = signal_cond.Timewait(_nearby_time);        
            signal_mutex.Unlock();
        }
        else
        {
            // msleep a while.
            mmTime.mmMSleep(_nearby_time);
            rt = 0;
        }
        return rt;
    }
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public static int mmTimewait_USecNearby(mmThread.Cond signal_cond, mmThread.Mutex signal_mutex, int _nearby_time)
    {
        int rt = 0;
        // some os pthread timedwait impl precision is low.
        // MM_MSEC_PER_SEC check can avoid some precision problem
        // but will extend the application shutdown time.
        if (0 == _nearby_time)
        {
            // next loop immediately.
            rt = 0;
        }
        else if(mmTime.MM_USEC_PER_SEC < _nearby_time)
        {
            signal_mutex.Lock();
            rt = signal_cond.Timewait(_nearby_time / mmTime.MM_MSEC_PER_SEC);       
            signal_mutex.Unlock();
        }
        else
        {
            // usleep a while.
            mmTime.mmUSleep(_nearby_time);
            rt = 0;
        }
        return rt;
    }
}
