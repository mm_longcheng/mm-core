/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

import java.util.TimeZone;

//we catch all Exception e but print nothing but catch the error code.
@SuppressWarnings("unused")
public class mmTime
{
    public static final int MM_MSEC_PER_SEC = 1000;
    public static final int MM_USEC_PER_SEC = 1000000;
    public static final int MM_NSEC_PER_SEC = 1000000000;
    
    public static void mmSSleep(long seconds)
    {
        try 
        {
            Thread.sleep(seconds * 1000);
        } 
        catch (Exception e) 
        {
            // do nothing here.we only need the finally code.
        }
    }
    public static void mmMSleep(long milliseconds)
    {
        try 
        {
            Thread.sleep(milliseconds);
        } 
        catch (Exception e) 
        {
            // do nothing here.we only need the finally code.
        }
    }
    public static void mmUSleep(long microseconds)
    {
        try 
        {
            long a = microseconds / mmTime.MM_MSEC_PER_SEC;
            long b = microseconds % mmTime.MM_MSEC_PER_SEC;
            Thread.sleep(a, (int)(b * mmTime.MM_MSEC_PER_SEC));
        } 
        catch (Exception e) 
        {
            // do nothing here.we only need the finally code.
        }
    }
    public static int mmGettimeofday(mmTimeval tp, mmTimezone tz)
    {
        long millisecond = System.currentTimeMillis();
        if (null != tp)
        {
            tp.tv_sec  = (long) ( millisecond / mmTime.MM_MSEC_PER_SEC);
            tp.tv_usec = (long) ((millisecond % mmTime.MM_MSEC_PER_SEC) * mmTime.MM_MSEC_PER_SEC);
        }
        if (null != tz)
        {
            TimeZone _tz = TimeZone.getDefault();
            
            tz.tz_dsttime = _tz.getDSTSavings();
            tz.tz_minuteswest = _tz.getOffset( millisecond );
        }       
        return 0;
    }
}
