/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

import java.net.ServerSocket;
import java.net.Socket;

//we catch all Exception e but print nothing but catch the error code.
@SuppressWarnings("unused")
public class mmOSSocket
{
    public static final Object MM_INVALID_SOCKET = null;    
    // linux socket.h
    public static final int MM_AF_INET4 = 2;
    public static final int MM_AF_INET6 = 10;
    // linux socket.h
    public static final int SOCK_STREAM = 1;
    public static final int SOCK_DGRAM  = 2;
    public static final int SOCK_ROW    = 3;
    
    public static final int MM_READ_SHUTDOWN    = 0x00;
    public static final int MM_SEND_SHUTDOWN    = 0x01;
    public static final int MM_BOTH_SHUTDOWN    = 0x02;
    
    public static final int MM_NONBLOCK = 0;
    public static final int MM_BLOCKING = 1;
    
    public static final int MM_KEEPALIVE_INACTIVE = 0;// not keepalive.
    public static final int MM_KEEPALIVE_ACTIVATE = 1;// mod keepalive.
    
    public static void Keepalive(Socket socket,int keepalive)
    {
        try 
        {
            socket.setKeepAlive(MM_KEEPALIVE_ACTIVATE == keepalive);
        } 
        catch (Exception e) 
        {
            // do nothing here.we only need the finally code.
        }
    }
    public static int SetReuseAddress(ServerSocket socket,boolean value)
    {
        int rt = -1;
        try 
        {
            socket.setReuseAddress(value);
            rt = 0;
        } 
        catch (Exception e) 
        {
            // do nothing here.we only need the finally code.
        }
        return rt;
        
    }
}
