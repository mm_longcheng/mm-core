/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

// java pool element implement is not need, we use java default memory manage system.
// here we just port c api here.
@SuppressWarnings("unused")
public class mmPoolElement
{
    public static final int MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT = 512;
    public static final float MM_POOL_ELEMENT_RATE_TRIM_BORDER_DEFAULT = 0.75f;
    public static final float MM_POOL_ELEMENT_RATE_TRIM_NUMBER_DEFAULT = 0.25f; 
    
    public Object ProduceType()
    {
        Object t = null;
        try 
        {
            t = class_factor.newInstance();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return t;
    }
    public void RecycleType(Object u)
    {
        // nothing to do.
    }
    
    public static class mmPoolElementCallback
    {
        public void Produce(Object obj, Object u, Object v){ }
        public void Recycle(Object obj, Object u, Object v){ }
        
        public Object obj = null;// weak ref. user data for callback.
        
        public void Init()
        {
            this.obj = null;
        }
        public void Destroy()
        {
            this.obj = null;
        }
        @Override  
        public Object clone()  
        {  
            mmPoolElementCallback o = null;
            try 
            {
                o = (mmPoolElementCallback)super.clone();
            } 
            catch (CloneNotSupportedException e) 
            {
                // print stack trace.
                e.printStackTrace();
            }
            if(null != o)
            {
                o.obj = this.obj;
            }
            return o;
        } 
    }
    
    public mmPoolElementCallback callback = new mmPoolElementCallback();
    public int chunk_size = MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT;
    public mmSpinlock locker = new mmSpinlock();

    public Class<?> class_factor = null;
    
    public void Init()
    {
        this.callback.Init();
        this.chunk_size = MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT;
        this.locker.Lock();
        this.class_factor = null;
    }
    public void Destroy()
    {
        this.callback.Destroy();
        this.chunk_size = MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT;
        this.locker.Destroy();
        this.class_factor = null;
    }
    public void Lock()
    {
        this.locker.Lock();
    }
    public void Unlock()
    {
        this.locker.Unlock();
    }
    public void SetElementType(Class<?> class_factor)
    {
        this.class_factor = class_factor;
    }
    // can not change at pool using.
    public void SetChunkSize(int chunk_size)
    {
        this.chunk_size = chunk_size;
    }
    public void SetCallback(mmPoolElementCallback callback)
    {
        // assert(NULL != callback && "you can not assign null callback.");
        this.callback = callback;
    }
    public Object Produce()
    {
        Object v = this.ProduceType();
        this.callback.Produce(this, this.callback.obj, v);
        return v;
    }
    public void Recycle(Object v) 
    {
        this.callback.Recycle(this, this.callback.obj, v);
        // v = null;
    }
}
