/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

//we catch all Exception e but print nothing but catch the error code.

// pthread behaver.
@SuppressWarnings("unused")
public class mmThread
{
    public ReentrantLock cond_locker = new ReentrantLock();
    public Condition cond_not_null = cond_locker.newCondition();
    
    public static class Mutex
    {
        public ReentrantLock impl = new ReentrantLock();
        public void Init()
        {

        }
        public void Destroy()
        {

        }
        public void Lock()
        {
            this.impl.lock();
        }
        public void Unlock()
        {
            this.impl.unlock();
        }
    }
    public static class Cond
    {
        @SuppressWarnings("UnusedAssignment")
        public Condition impl = null;
        public Cond(Mutex m)
        {
            this.impl = m.impl.newCondition();
        }
        public void Init()
        {
            
        }
        public void Destroy()
        {
            
        }
        public void Signal()
        {
            this.impl.signal();
        }
        public void Await()
        {
            try 
            {
                this.impl.await();
            } 
            catch (Exception e1) 
            {
                // Do nothing here. We only need the finally code.
            }
        }
        public int Timewait(long milliseconds)
        {
            int code = -1;
            try 
            {
                boolean r = this.impl.await(milliseconds, TimeUnit.MILLISECONDS);
                code = r ? 0 : 1;
            } 
            catch (Exception e1) 
            {
                // Do nothing here. We only need the finally code.
            }
            return code;
        }
    }
    
    // thread have some interface.
    //  init         state = MM_TS_CLOSED
    //  start        state = MM_TS_FINISH == state ? MM_TS_CLOSED : MM_TS_MOTION;
    //  interrupt    state = MM_TS_CLOSED
    //  shutdown     state = MM_TS_FINISH
    //  join         state = state
    //  destroy      state = MM_TS_CLOSED
    public static final int MM_TS_CLOSED = 0;// thread not start or be closed or be interrupt.
    public static final int MM_TS_MOTION = 1;// thread is running.
    public static final int MM_TS_FINISH = 2;// application is termination and can not restart.
    
    public Thread work_thread = null;
    
    public void Init()
    {
        this.work_thread = null;
    }
    public void Destroy()
    {
        this.work_thread = null;
    }
    public void Start(Runnable runnable)
    {
        this.work_thread = new Thread(runnable);
        this.work_thread.start();
    }
    public void Join()
    {
        try 
        {
            this.work_thread.join();
        } 
        catch (Exception e) 
        {
            // Do nothing here. We only need the finally code.
        }
    }
    
    public static long mmCurrentThreadId()
    {
        return Thread.currentThread().getId();
    }
}
