/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;


@SuppressWarnings({"unused", "PointlessBitwiseExpression", "PointlessArithmeticExpression"})
public class mmByte
{
    // little endian machine => little endian byte order.
    public static void LLInt16EncodeBytes(byte[] buffer, int offset, short i)
    {
        buffer[offset + 1] = (byte)((i >>  8) & 0xFF);
        buffer[offset + 0] = (byte)((i >>  0) & 0xFF);
    }
    
    public static short LLInt16DecodeBytes(byte[] buffer, int offset) 
    {
        short value = 0;
        value |= ((int)(buffer[offset + 1] & 0xFF)) <<  8;
        value |= ((int)(buffer[offset + 0] & 0xFF)) <<  0;
        return value;
    }
    public static void LLInt32EncodeBytes(byte[] buffer, int offset, int i)
    {
        buffer[offset + 3] = (byte)((i >> 24) & 0xFF);
        buffer[offset + 2] = (byte)((i >> 16) & 0xFF);
        buffer[offset + 1] = (byte)((i >>  8) & 0xFF);
        buffer[offset + 0] = (byte)((i >>  0) & 0xFF);
    }
    
    public static int LLInt32DecodeBytes(byte[] buffer, int offset) 
    {
        int value = 0;
        value |= ((int)(buffer[offset + 3] & 0xFF)) << 24;
        value |= ((int)(buffer[offset + 2] & 0xFF)) << 16;
        value |= ((int)(buffer[offset + 1] & 0xFF)) <<  8;
        value |= ((int)(buffer[offset + 0] & 0xFF)) <<  0;
        return value;
    }
    public static void LLInt64EncodeBytes(byte[] buffer, int offset, long i)
    {
        buffer[offset + 7] = (byte)((i >> 56) & 0xFF);
        buffer[offset + 6] = (byte)((i >> 48) & 0xFF);
        buffer[offset + 5] = (byte)((i >> 40) & 0xFF);
        buffer[offset + 4] = (byte)((i >> 32) & 0xFF);
        buffer[offset + 3] = (byte)((i >> 24) & 0xFF);
        buffer[offset + 2] = (byte)((i >> 16) & 0xFF);
        buffer[offset + 1] = (byte)((i >>  8) & 0xFF);
        buffer[offset + 0] = (byte)((i >>  0) & 0xFF);
    }
    
    public static long LLInt64DecodeBytes(byte[] buffer, int offset) 
    {
        long value = 0;
        value |= ((long)(buffer[offset + 7] & 0xFF)) << 56;
        value |= ((long)(buffer[offset + 6] & 0xFF)) << 48;
        value |= ((long)(buffer[offset + 5] & 0xFF)) << 40;
        value |= ((long)(buffer[offset + 4] & 0xFF)) << 32;
        value |= ((long)(buffer[offset + 3] & 0xFF)) << 24;
        value |= ((long)(buffer[offset + 2] & 0xFF)) << 16;
        value |= ((long)(buffer[offset + 1] & 0xFF)) <<  8;
        value |= ((long)(buffer[offset + 0] & 0xFF)) <<  0;
        return value;
    }
}
