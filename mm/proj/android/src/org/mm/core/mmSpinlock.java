/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings("unused")
public class mmSpinlock
{
    @SuppressWarnings("Convert2Diamond")
    public AtomicReference<Thread> atomic = new AtomicReference<Thread>();
    
    public void Init()
    {
        // here nothing for init.
    }
    public void Destroy()
    {
        // here nothing for destroy.
    }
    @SuppressWarnings("StatementWithEmptyBody")
    public void Lock()
    {
        Thread current = Thread.currentThread();
        while(!this.atomic.compareAndSet(null, current))
        {
            // busy-wait-loop.
        }
    }
    public boolean Trylock()
    {
        Thread current = Thread.currentThread();
        return this.atomic.compareAndSet(null, current);
    }
    public void Unlock()
    {
        Thread current = Thread.currentThread();
        this.atomic.compareAndSet(current, null);
    }
}
