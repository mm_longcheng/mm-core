/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

@SuppressWarnings("unused")
public class mmTimeval
{   
    public long tv_sec = 0;         /* seconds */
    public long tv_usec = 0;        /* and microseconds */
    
    public void Init()
    {       
        this.tv_sec = 0;
        this.tv_usec = 0;
    }
    public void Destroy()
    {
        this.tv_sec = 0;
        this.tv_usec = 0;
    }
    public void Normalize()
    {
        this.tv_sec += (this.tv_usec / mmTime.MM_USEC_PER_SEC);
        this.tv_usec = (this.tv_usec % mmTime.MM_USEC_PER_SEC);
        // make sure the 0 < tv_usec,if p->tv_sec * MM_USEC_PER_SEC + p->tv_usec > 0;
        if ( 0 < this.tv_sec && 0 > this.tv_usec )
        {
            this.tv_sec --;
            this.tv_usec += mmTime.MM_USEC_PER_SEC;
        }
    }
    public boolean Equal(mmTimeval q)
    {
        return (this.tv_sec == q.tv_sec && this.tv_usec == q.tv_usec);
    }
    public boolean Greater(mmTimeval q)
    {
        return (this.tv_sec > q.tv_sec || (this.tv_sec == q.tv_sec && this.tv_usec > q.tv_usec));
    }
    public boolean GreaterOrEqual(mmTimeval q)
    {
        return this.Greater(q) || this.Equal(q);
    }
    public boolean Less(mmTimeval q)
    {
        return !this.GreaterOrEqual(q);
    }
    public boolean LessOrEqual(mmTimeval q)
    {
        return !this.Greater(q);
    }
    public void Add(mmTimeval q)
    {
        this.tv_sec += q.tv_sec;
        this.tv_usec += q.tv_usec;
        this.Normalize();
    }
    public void Sub(mmTimeval q)
    {
        this.tv_sec -= q.tv_sec;
        this.tv_usec -= q.tv_usec;
        this.Normalize();
    }
    // reference.
    public void USec(long[] timecode)
    {
        timecode[0] = (this.tv_sec * mmTime.MM_USEC_PER_SEC) + (this.tv_usec);
    }
    // return reference.
    public long ToUSec()
    {
        return (this.tv_sec * mmTime.MM_USEC_PER_SEC) + (this.tv_usec);
    }
}
