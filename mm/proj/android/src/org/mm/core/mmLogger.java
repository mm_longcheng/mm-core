/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

import java.util.TreeMap;

@SuppressWarnings("unused")
public class mmLogger
{
    public static final int MM_LOG_UNKNOWN =  0;
    public static final int MM_LOG_FATAL   =  1;
    public static final int MM_LOG_CRIT    =  2;
    public static final int MM_LOG_ERROR   =  3;
    public static final int MM_LOG_ALERT   =  4;
    public static final int MM_LOG_WARNING =  5;
    public static final int MM_LOG_NOTICE  =  6;
    public static final int MM_LOG_INFO    =  7;
    public static final int MM_LOG_TRACE   =  8;
    public static final int MM_LOG_DEBUG   =  9;
    public static final int MM_LOG_VERBOSE = 10;

    public static class mmLevelMark
    {
        public String m; // mark
        public String n; // name
        public mmLevelMark(String _m, String _n)
        {
            this.m = _m;
            this.n = _n;
        }
    }
    public static TreeMap<Integer, mmLevelMark> const_level_mark = new TreeMap<Integer, mmLevelMark>() 
    {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
    
        {
            this.put(MM_LOG_UNKNOWN, new mmLevelMark("U","unknown") );
            this.put(MM_LOG_FATAL  , new mmLevelMark("F","fatal"  ) );
            this.put(MM_LOG_CRIT   , new mmLevelMark("C","crit"   ) );
            this.put(MM_LOG_ERROR  , new mmLevelMark("E","error"  ) );
            this.put(MM_LOG_ALERT  , new mmLevelMark("A","alert"  ) );
            this.put(MM_LOG_WARNING, new mmLevelMark("W","warning") );
            this.put(MM_LOG_NOTICE , new mmLevelMark("N","notice" ) );
            this.put(MM_LOG_INFO   , new mmLevelMark("I","info"   ) );
            this.put(MM_LOG_TRACE  , new mmLevelMark("T","trace"  ) );
            this.put(MM_LOG_DEBUG  , new mmLevelMark("D","debug"  ) );
            this.put(MM_LOG_VERBOSE, new mmLevelMark("V","verbose") );
        }
    };

    public static abstract class mmLoggerCallback
    {
        public abstract void EventLogger(mmLogger o, int lvl, String message);
    }
    
    public static class mmLoggerCallbackPrintf extends mmLoggerCallback
    {
        public mmThread.Mutex mutex = new mmThread.Mutex();

        @Override
        public void EventLogger(mmLogger o, int lvl, String message) 
        {
            mutex.Lock();

            // [ 8 V ]
            mmLevelMark lm = mmLoggerLevelMark(lvl);
            System.out.println(" " + lvl + " " + lm.m + " "+ message);

            mutex.Unlock();
        }
    }
    
    public static mmLoggerCallback pLoggerCallbackPrintf = new mmLoggerCallbackPrintf();

    public mmLoggerCallback callback = pLoggerCallbackPrintf;
    //weak ref for obj.
    public Object obj;
    
    private volatile static mmLogger _instance = null;

    static public mmLogger Instance()
    {
        if (null == _instance)
        {  
            synchronized (mmLogger.class)
            {
                if (null == _instance)
                {
                    _instance = new mmLogger();
                }
            }
        }
        return _instance;
    }
    public static void SetCallback(mmLoggerCallback callback, Object obj)
    {
        // android assertions are unreliable.
        // assert null != callback : "you can not assign null callback.";
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback = callback;
        gLogger.obj = obj;
    }
    public static mmLevelMark mmLoggerLevelMark(int lvl)
    {
        if (MM_LOG_FATAL <= lvl && MM_LOG_VERBOSE >= lvl)
        {
            return const_level_mark.get(lvl);
        }
        else
        {
            return const_level_mark.get(MM_LOG_UNKNOWN);
        }
    }
    // logger function.
    static public void LogU(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_UNKNOWN, message);
    }
    static public void LogF(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_FATAL, message);
    }
    static public void LogC(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_CRIT, message);
    }
    static public void LogE(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_ERROR, message);
    }
    static public void LogA(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_ALERT, message);
    }
    static public void LogW(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_WARNING, message);
    }
    static public void LogN(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_NOTICE, message);
    }
    static public void LogI(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_INFO, message);
    }
    static public void LogT(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_TRACE, message);
    }
    static public void LogD(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_DEBUG, message);
    }
    static public void LogV(String message)
    {
        mmLogger gLogger = mmLogger.Instance();
        gLogger.callback.EventLogger(gLogger, MM_LOG_VERBOSE, message);
    }
}
