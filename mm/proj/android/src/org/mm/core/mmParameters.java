/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.core;

@SuppressWarnings("unused")
public class mmParameters
{
    public static class mmServerInfo
    {
        public String node;
        public int port;
        public int length;
    }
    public static class mmClientInfo
    {
        public String node;
        public int port;
    }
    // 192.168.111.123-65535(2) 192.168.111.123-65535[2]
    public static void Server(String parameters, mmServerInfo server_info)
    {
        parameters = parameters.replace('-', ' ');
        parameters = parameters.replace('(', ' ');
        parameters = parameters.replace(')', ' ');
        parameters = parameters.replace('[', ' ');
        parameters = parameters.replace(']', ' ');
        String[] arr = parameters.split(" ");
        server_info.node = arr[0];
        server_info.port = Integer.parseInt(arr[1]);
        server_info.length = Integer.parseInt(arr[2]);      
    }
    // 192.168.111.123-65535
    public static void Client(String parameters, mmClientInfo client_info)
    {
        parameters = parameters.replace('-', ' ');
        String[] arr = parameters.split(" ");
        client_info.node = arr[0];
        client_info.port = Integer.parseInt(arr[1]);        
    }
}
