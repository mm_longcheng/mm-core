APP_ABI := arm64-v8a armeabi-v7a

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libmm_core_static
APP_MODULES += libmm_core_shared

APP_MODULES += libmm_net_static
APP_MODULES += libmm_net_shared

