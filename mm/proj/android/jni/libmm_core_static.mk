LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libmm_core_static
LOCAL_MODULE_FILENAME := libmm_core_static
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DMM_STATIC_DLL
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += cpufeatures
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/os/$(MM_PLATFORM)
LOCAL_C_INCLUDES += $(ANDROID_NDK)/sources/cpufeatures 
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/algorithm
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/atomic
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/container
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/core
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/graphics
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/math
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/parse
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/random
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/os/$(MM_PLATFORM)/core
# MY_SOURCES_PATH += $(LOCAL_PATH)/$(MM_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################