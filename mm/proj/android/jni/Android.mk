LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libmm_core_static.mk
include $(LOCAL_PATH)/libmm_core_shared.mk

include $(LOCAL_PATH)/libmm_net_static.mk
include $(LOCAL_PATH)/libmm_net_shared.mk

$(call import-module,android/cpufeatures)