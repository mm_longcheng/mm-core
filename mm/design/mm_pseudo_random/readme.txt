http://xoshiro.di.unimi.it/

Introduction
This page describes some new pseudorandom number generators (PRNGs) we (David Blackman and I) have been working on recently, and a shootout comparing them with other generators. Details about the generators can be found in our paper. Information about my previous xorshift-based generators can be found here, but they have been entirely superseded by the new ones, which are faster and better. As part of our study, we developed a very strong test for Hamming-weight dependencies which gave a number of surprising results.

64-bit Generators
xoshiro256** (XOR/shift/rotate) is our all-purpose, rock-solid generator (not a cryptographically secure generator, though, like all PRNGs in these pages). It has excellent (sub-ns) speed, a state space (256 bits) that is large enough for any parallel application, and it passes all tests we are aware of.

If, however, one has to generate only 64-bit floating-point numbers (by extracting the upper 53 bits) xoshiro256+ is a slightly (≈15%) faster generator with analogous statistical properties. For general usage, one has to consider that its lowest bits have low linear complexity and will fail linearity tests; however, low linear complexity can have hardly any impact in practice, and certainly has no impact at all if you generate floating-point numbers using the upper bits (we computed a precise estimate of the linear complexity of the lowest bits).

If you are tight on space, xoroshiro128** (XOR/rotate/shift/rotate) and xoroshiro128+ have the same speed and use half of the space; the same comments apply. They are suitable only for low-scale parallel applications; moreover, xoroshiro128+ exhibits a mild dependency in Hamming weights that generates a failure after 8 TB of output in our test. We believe this slight bias cannot affect any application.

Finally, if for any reason (which reason?) you need more state, we provide in the same vein xoshiro512** / xoshiro512+ and xoroshiro1024** / xoroshiro1024* (see the paper).

All generators, being based on linear recurrences, provide jump functions that make it possible to simulate any number of calls to the next-state function in constant time, once a suitable jump polynomial has been computed. We provide ready-made jump functions for a number of calls equal to the square root of the period, to make it easy generating non-overlapping sequences for parallel computations.

We suggest to use a SplitMix64 to initialize the state of our generators starting from a 64-bit seed, as research has shown that initialization must be performed with a generator radically different in nature from the one initialized to avoid correlation on similar seeds.

32-bit Generators
xoshiro128** is our 32-bit all-purpose, rock-solid generator, whereas xoshiro128+ is its counterpart for floating-point generation. They are the 32-bit counterpart of xoshiro256** and xoshiro256+, so similar comments apply. Their state is too small for large-scale parallelism: their intended usage is inside embedded hardware or GPUs. For an even smaller scale, you can use xoroshiro64** and xoroshiro64*. We not believe at this point in time 32-bit generator with a larger state can be of any use (but there 32-bit xoroshiro generators of much larger size).

All 32-bit generators pass all tests we are aware of, with the exception of linearity tests (binary rank and linear complexity) for xoshiro128+ and xoroshiro64*: in this case, due to the smaller number of output bits the low linear complexity of the lowest bits is sufficient to trigger BigCrush tests when the output is bit-reversed. Analogously to the 64-bit case, generating 32-bit floating-point number using the upper bits will not use any of the bits with low linear complexity.

16-bit Generators
We do not suggest any particular 16-bit generator, but it is possible to design relatively good ones using our techniques. For example, Parallax has embedded in their Propeller 2 microcontroller multiple 16-bit xoroshiro32++ generators (for information about the ++ scrambler, see the paper).

﻿A PRNG Shootout
We provide here a shootout of a few recent 64-bit PRNGs that are quite widely used. The purpose is that of providing a consistent, reproducible assessment of two properties of the generators: speed and quality. The code used to perform the tests and all the output from statistical test suites is available for download.

﻿Speed
The speed reported in this page is the time required to emit 64 random bits, and the number of clock cycles required to generate a byte (thanks to the PAPI library). If a generator is 32-bit in nature, we glue two consecutive outputs. Note that we do not report results using GPUs or SSE instructions: for that to be meaningful, we should have implementations for all generators. Otherwise, with suitable hardware support we could just use AES in counter mode and get 64 secure bits in 1.12ns. The tests were performed on an Intel® Core™ i7-7700 CPU @ 3.60GHz (Kaby Lake).

A few caveats:

Timings are taken running a generator for billions of times in a loop; but this is not the way you use generators.
There is some looping overhead, which is about 0.12 ns, but subtracting it from the timings is not going to be particularly meaningful due to instruction rescheduling, etc.
Relative speed might be different on different CPUs and on different scenarios.
Code has been compiled using gcc's -fno-move-loop-invariants and -fno-unroll-loops options. These options are essential to get a sensible result: without them, the compiler can move outside the testing loop constant loads (e.g., multiplicative constants) and may perform different loop unrolling depending on the generator. For this reason, we cannot provide timings with clang: there are at the time of this writing no such options. If you find timings that are significantly better than those shown here on comparable hardware, they are likely to be unreliable and just due to compiler artifacts.
To ease replicability, I distribute a harness performing the measurement. You just have to define a next() function and include the harness. But the only realistic suggestion is to try different generators in your application and see what happens.

﻿Quality
This is probably the more elusive property of a PRNG. Here quality is measured using the powerful BigCrush suite of tests. BigCrush is part of TestU01, a monumental framework for testing PRNGs developed by Pierre L'Ecuyer and Richard Simard (“TestU01: A C library for empirical testing of random number generators”, ACM Trans. Math. Softw. 33(4), Article 22, 2007).

We run BigCrush starting from 100 equispaced points of the state space of the generator and collect failures—tests in which the p-value statistics is outside the interval [0.001..0.999]. A failure is systematic if it happens at all points.

Note that TestU01 is a 32-bit test suite. Thus, two 32-bit integer values are passed to the test suite for each generated 64-bit value. Floating point numbers are generated instead by dividing the unsigned output of the generator by 264. Since this implies a bias towards the high bits (which is anyway a known characteristic of TestU01), we run the test suite also on the reverse generator. More detail about the whole process can be found in this paper.

Beside BigCrush, we analyzed our generators using a test for Hamming-weight dependencies described in our paper. As we already remarked, our only generator failing the test (but only after 8 TB of output) is xoroshiro128+.

We report the period of each generator and its footprint in bits: a generator gives “bang-for-the-buck” if the base-2 logarithm of the period is close to the footprint. Note that the footprint has been always padded to a multiple of 64, and it can be significantly larger than expected because of padding and cyclic access indices.





xorshift128+ for lua 5.4 google ...