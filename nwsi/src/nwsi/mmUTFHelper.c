/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUTFHelper.h"

#include "unicode/mmUTF16.h"

MM_EXPORT_NWSI
int
mmUTF_Unicode2CharCount(
    int                                            hCodePoint)
{
    return (hCodePoint >= MM_MIN_SUPPLEMENTARY_CODE_POINT ? 2 : 1);
}

MM_EXPORT_NWSI
int
mmUTF_GetUnicode4Length(
    const mmUInt32_t*                              t,
    size_t                                         size,
    size_t                                         o,
    int                                            s,
    int                                            n)
{
    int hCMax = (int)size;
    int hCMin = 0;

    int bCount = 0;
    int hIndex = 0;
    int hCharCount = 0;
    int hLength = 0;

    mmUInt32_t hUnicode = 0;

    bCount = (int)n;
    hIndex = (int)(o + (0 < s ? 0 : s));
    while(0 < bCount && (hCMin <= hIndex && hIndex < hCMax))
    {
        hUnicode = t[hIndex];
        hCharCount = mmUTF_Unicode2CharCount(hUnicode);
        bCount -= hCharCount;
        hIndex += s;
        hLength += (0 > bCount) ? 0 : 1;
    }

    return hLength;
}

MM_EXPORT_NWSI
int
mmUTF_GetUnicode2Length(
    const mmUInt32_t*                              t,
    size_t                                         size,
    size_t                                         o,
    int                                            s,
    int                                            n)
{
    int hCMax = (int)size;
    int hCMin = 0;

    int bCount = 0;
    int hIndex = 0;
    int hCharCount = 0;
    int hLength = 0;

    mmUInt32_t hUnicode = 0;

    bCount = (int)n;
    hIndex = (int)(o + (0 < s ? 0 : s));
    while(0 < bCount && (hCMin <= hIndex && hIndex < hCMax))
    {
        hUnicode = t[hIndex];
        hCharCount = mmUTF_Unicode2CharCount(hUnicode);
        bCount -= 1;
        hIndex += s;
        hLength += hCharCount;
    }

    return hLength;
}

MM_EXPORT_NWSI
int
mmUTF_GetUnicodeRangeLength(
    size_t                                         size,
    size_t                                         o,
    int                                            s,
    int                                            n)
{
    // clamp to [0, size]
    size_t l = (s > 0) ? (size - o) : o;
    return (l > n) ? n : (int)l;
}

MM_EXPORT_NWSI
size_t
mmUTF_GetUtf16OffsetLNormalise(
    const mmUInt16_t*                              s,
    size_t                                         z,
    size_t                                         o)
{
    if (0 < o && o < z)
    {
        mmUInt16_t c = s[o];
        int n = (U16_IS_SURROGATE(c) && U16_IS_TRAIL(c)) ? 1 : 0;
        return o - n;
    }
    else
    {
        return o;
    }
}

MM_EXPORT_NWSI
size_t
mmUTF_GetUtf16OffsetRNormalise(
    const mmUInt16_t*                              s,
    size_t                                         z,
    size_t                                         o)
{
    if (0 < o && o < z)
    {
        mmUInt16_t c = s[o];
        int n = (U16_IS_SURROGATE(c) && U16_IS_LEAD(c)) ? 1 : 0;
        return o + n;
    }
    else
    {
        return o;
    }
}
