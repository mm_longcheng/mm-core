/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEventSurface.h"

MM_EXPORT_NWSI void mmEventDraggingArgs_Reset(struct mmEventDraggingArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}
MM_EXPORT_NWSI void mmEventKeypadArgs_Reset(struct mmEventKeypadArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}

MM_EXPORT_NWSI void mmEventKeypadStatusArgs_Reset(struct mmEventKeypadStatusArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}

MM_EXPORT_NWSI void mmEventCursorArgs_Reset(struct mmEventCursorArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}

MM_EXPORT_NWSI void mmEventTouchsArgs_Reset(struct mmEventTouchsArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}

MM_EXPORT_NWSI void mmEventMetricsArgs_Reset(struct mmEventMetricsArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}

MM_EXPORT_NWSI void mmEventUpdateArgs_Reset(struct mmEventUpdateArgs* p)
{
    mmEventArgs_Reset(&p->hSuper);
    p->content = NULL;
}
