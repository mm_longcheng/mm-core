/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmClipboardProvider_h__
#define __mmClipboardProvider_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmUtf16String.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmClipboardProvider
{
    void(*SetByteBuffer)(
        struct mmClipboardProvider*                p, 
        const char*                                pMimeType, 
        struct mmByteBuffer*                       pByteBuffer);

    void(*GetByteBuffer)(
        struct mmClipboardProvider*                p,
        const char*                                pMimeType,
        struct mmByteBuffer*                       pByteBuffer);
};
MM_EXPORT_NWSI void mmClipboardProvider_Init(struct mmClipboardProvider* p);
MM_EXPORT_NWSI void mmClipboardProvider_Destroy(struct mmClipboardProvider* p);

struct mmClipboard
{
    struct mmClipboardProvider* pProvider;
};

MM_EXPORT_NWSI void mmClipboard_Init(struct mmClipboard* p);
MM_EXPORT_NWSI void mmClipboard_Destroy(struct mmClipboard* p);
MM_EXPORT_NWSI void mmClipboard_SetProvider(struct mmClipboard* p, struct mmClipboardProvider* pProvider);

/* Note: bytes is a weak reference. */
MM_EXPORT_NWSI void mmClipboard_SetBytes(struct mmClipboard* p, const char* mimeType, struct mmByteBuffer* bytes);
/* Note: bytes is a weak reference. */
MM_EXPORT_NWSI void mmClipboard_GetBytes(struct mmClipboard* p, const char* mimeType, struct mmByteBuffer* bytes);

/* Note: text is a weak string. */
MM_EXPORT_NWSI void mmClipboard_SetText(struct mmClipboard* p, struct mmUtf16String* text);
/* Note: text is a weak string. */
MM_EXPORT_NWSI void mmClipboard_GetText(struct mmClipboard* p, struct mmUtf16String* text);

#include "core/mmSuffix.h"

#endif//__mmClipboardProvider_h__
