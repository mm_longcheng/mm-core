/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPureDynlibLoader_h__
#define __mmPureDynlibLoader_h__

#include "core/mmCore.h"

#include "core/mmDynlib.h"

#include "core/mmString.h"

#include "container/mmRbtreeString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmPureDynlibLoader
{
    struct mmString hDynlibFolder;
    struct mmString hDynlibPrefix;
    struct mmString hDynlibSuffix;
    struct mmRbtreeStringVpt hDynlibMap;
};

MM_EXPORT_NWSI void mmPureDynlibLoader_Init(struct mmPureDynlibLoader* p);
MM_EXPORT_NWSI void mmPureDynlibLoader_Destroy(struct mmPureDynlibLoader* p);

MM_EXPORT_NWSI void mmPureDynlibLoader_OnFinishLaunching(struct mmPureDynlibLoader* p);
MM_EXPORT_NWSI void mmPureDynlibLoader_OnBeforeTerminate(struct mmPureDynlibLoader* p);

// default is ""
MM_EXPORT_NWSI void mmPureDynlibLoader_SetDynlibFolder(struct mmPureDynlibLoader* p, const char* _dynlibFolder);
// default is ""
MM_EXPORT_NWSI void mmPureDynlibLoader_SetDynlibPrefix(struct mmPureDynlibLoader* p, const char* _dynlibPrefix);
// default is ""
MM_EXPORT_NWSI void mmPureDynlibLoader_SetDynlibSuffix(struct mmPureDynlibLoader* p, const char* _dynlibSuffix);

MM_EXPORT_NWSI void mmPureDynlibLoader_AcquireDynlib(struct mmPureDynlibLoader* p, const char* _dynlibName);
MM_EXPORT_NWSI void mmPureDynlibLoader_ReleaseDynlib(struct mmPureDynlibLoader* p, const char* _dynlibName);
MM_EXPORT_NWSI void mmPureDynlibLoader_ClearDynlib(struct mmPureDynlibLoader* p);

MM_EXPORT_NWSI void mmPureDynlibLoader_GetDynlibRealName(struct mmPureDynlibLoader* p, const char* _dynlibName, struct mmString* _realName);

#include "core/mmSuffix.h"

#endif//__mmPureDynlibLoader_h__
