/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextParagraphFormat_h__
#define __mmTextParagraphFormat_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"

#include "mmTextFormat.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmTextParagraphFormat
{
    // master text format.
    struct mmTextFormat hMasterFormat;
    // font features tag. only support all of range.
    struct mmRbtreeU32U32 hFontFeatures;

    // default Natural
    int hTextAlignment;
    // default Top
    int hParagraphAlignment;
    // default BreakByWordWrapping.
    int hLineBreakMode;
    // default WritingDirectionNatural.
    int hWritingDirection;
    // default 0.0f.
    float hFirstLineHeadIndent;
    // default 0.0f.
    float hHeadIndent;
    // default 0.0f.
    float hTailIndent;
    // default 0.0f.
    float hLineSpacingAddition;
    // default 1.0f.
    float hLineSpacingMultiple;

    /*- Shadow -*/
    // (x, y) default (0.0f, 0.0f)
    float hShadowOffset[2];
    // default 0, not shadow, 3 generally.
    float hShadowBlur;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    float hShadowColor[4];
    
    // default LineCapButt.
    int hLineCap;
    // default LineJoinMiter
    int hLineJoin;
    // default is 10.0
    float hLineMiterLimit;

    // If add this flag, we not need explicitly set hWindowSize and hLayoutRect.
    // An appropriate width and height will be calculated internally.
    // Only suitable for single line text, uniform font size format for full text.
    // default 0
    int hAlignBaseline;
    // single line descent.
    float hLineDescent;
    // suitable size.
    float hSuitableSize[2];

    // TextWindow constraint dimensions box.
    // window size.
    // (w, h) default { 40.0f, 20.0f, },
    float hWindowSize[2];

    // TextLayout constraint dimensions box.
    // layout rect.
    // (x, y, w, h) default { 0.0f, 0.0f, 40.0f, 20.0f, },
    float hLayoutRect[4];

    // font display density. default 1.0.
    float hDisplayDensity;
    
    /*- EditBox -*/
    // caret index. default 0.
    mmUInt64_t hCaretIndex;
    // selection range [l, r]. default [0, 0].
    mmUInt64_t hSelection[2];
    // caret wrapping. 0 not 1 wrapping. default 0.
    int hCaretWrapping;
    // caret status. 0 hide 1 show. default 0.
    int hCaretStatus;
    // selection status. 0 release 1 capture. default 0.
    int hSelectionStatus;
    // caret width. default 1.
    float hCaretWidth;
    // default rgba { 1.0f, 1.0f, 1.0f, 1.0f, },
    float hCaretColor[4];
    // capture color. default ARGB 0xFF4080FF ( 64, 128, 255).
    float hSelectionCaptureColor[4];
    // release color. default ARGB 0xFFDCDCDC (220, 220, 220).
    float hSelectionReleaseColor[4];
    
    // object for platform implement.
    void* pObject;
};

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_Init(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_Destroy(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_Reset(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_SetObject(
    struct mmTextParagraphFormat*                  p, 
    void*                                          pObject);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_SetMasterFormat(
    struct mmTextParagraphFormat*                  p, 
    const struct mmTextFormat*                     hFormat);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_SetFontFeature(
    struct mmTextParagraphFormat*                  p, 
    mmUInt32_t                                     hTag, 
    mmUInt32_t                                     hParameter);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_RmvFontFeature(
    struct mmTextParagraphFormat*                  p, 
    mmUInt32_t                                     hTag);

MM_EXPORT_NWSI 
mmUInt32_t 
mmTextParagraphFormat_GetFontFeature(
    struct mmTextParagraphFormat*                  p, 
    mmUInt32_t                                     hTag);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_ClearFontFeature(
    struct mmTextParagraphFormat*                  p);

struct mmTextHitMetrics
{
    // [x, y, w, h]
    float rect[4];

    // text offset
    mmUInt32_t offset;

    // text length
    mmUInt32_t length;

    int bidiLevel;
    int isText;
    int isTrimmed;
};

MM_EXPORT_NWSI 
void 
mmTextHitMetrics_Reset(
    struct mmTextHitMetrics*                       p);

#include "core/mmSuffix.h"

#endif//__mmTextParagraphFormat_h__
