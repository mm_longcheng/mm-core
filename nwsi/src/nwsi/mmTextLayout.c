/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextLayout.h"

#include "nwsi/mmTextParagraphFormatNative.h"
#include "nwsi/mmTextSpannableNative.h"

#include "core/mmAlloc.h"

#include "graphics/mmColor.h"

MM_EXPORT_NWSI
void
mmTextLayout_Init(
    struct mmTextLayout*                           p)
{
    mmTextParagraphFormat_Init(&p->hParagraphFormat);
    mmTextSpannable_Init(&p->hTextSpannable);
    mmTextUtf16String_Init(&p->hText);
    mmTextParagraph_Init(&p->hTextParagraph);
    mmTextRenderTarget_Init(&p->hTextRenderTarget);
    mmTextDrawingContext_Init(&p->hDrawingContext);
    p->pGraphicsFactory = NULL;
    
    mmTextParagraph_SetTextParagraphFormat(&p->hTextParagraph, &p->hParagraphFormat);
    mmTextParagraph_SetTextSpannable(&p->hTextParagraph, &p->hTextSpannable);
    mmTextParagraph_SetText(&p->hTextParagraph, &p->hText);
    mmTextParagraph_SetDrawingContext(&p->hTextParagraph, &p->hDrawingContext);

    mmTextSpannable_SetMasterFormat(&p->hTextSpannable, &p->hParagraphFormat.hMasterFormat);
    mmTextRenderTarget_SetTextParagraphFormat(&p->hTextRenderTarget, &p->hParagraphFormat);
    mmTextDrawingContext_SetTextParagraphFormat(&p->hDrawingContext, &p->hParagraphFormat);
}

MM_EXPORT_NWSI
void
mmTextLayout_Destroy(
    struct mmTextLayout*                           p)
{
    p->pGraphicsFactory = NULL;
    mmTextDrawingContext_Destroy(&p->hDrawingContext);
    mmTextRenderTarget_Destroy(&p->hTextRenderTarget);
    mmTextParagraph_Destroy(&p->hTextParagraph);
    mmTextUtf16String_Destroy(&p->hText);
    mmTextSpannable_Destroy(&p->hTextSpannable);
    mmTextParagraphFormat_Destroy(&p->hParagraphFormat);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetGraphicsFactory(
    struct mmTextLayout*                           p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    p->pGraphicsFactory = pGraphicsFactory;
    mmTextRenderTarget_SetGraphicsFactory(&p->hTextRenderTarget, pGraphicsFactory);
    mmTextDrawingContext_SetGraphicsFactory(&p->hDrawingContext, pGraphicsFactory);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetMasterFormat(
    struct mmTextLayout*                           p,
    const struct mmTextFormat*                     pMasterFormat)
{
    mmTextParagraphFormat_SetMasterFormat(&p->hParagraphFormat, pMasterFormat);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetText(
    struct mmTextLayout*                           p,
    const struct mmUtf16String*                    str)
{
    struct mmUtf16String* pString = &p->hText.hString;
    mmUtf16String_Reset(pString);
    mmUtf16String_Assign(pString, str);
    mmTextLayout_UpdateText(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetTexts(
    struct mmTextLayout*                           p,
    const mmUInt16_t*                              s)
{
    struct mmUtf16String* pString = &p->hText.hString;
    mmUtf16String_Reset(pString);
    mmUtf16String_Assigns(pString, s);
    mmTextLayout_UpdateText(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetTextsn(
    struct mmTextLayout*                           p,
    const mmUInt16_t*                              s,
    size_t                                         n)
{
    struct mmUtf16String* pString = &p->hText.hString;
    mmUtf16String_Reset(pString);
    mmUtf16String_Assignsn(pString, s, n);
    mmTextLayout_UpdateText(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetTextWeak(
    struct mmTextLayout*                           p,
    const struct mmUtf16String*                    str)
{
    struct mmUtf16String* pString = &p->hText.hString;
    mmUtf16String_Reset(pString);
    mmUtf16String_MakeWeak(pString, str);
    mmTextLayout_UpdateText(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetTextWeaks(
    struct mmTextLayout*                           p,
    const mmUInt16_t*                              s)
{
    struct mmUtf16String* pString = &p->hText.hString;
    mmUtf16String_Reset(pString);
    mmUtf16String_MakeWeaks(pString, s);
    mmTextLayout_UpdateText(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetTextWeaksn(
    struct mmTextLayout*                           p,
    const mmUInt16_t*                              s,
    size_t                                         n)
{
    struct mmUtf16String* pString = &p->hText.hString;
    mmUtf16String_Reset(pString);
    mmUtf16String_MakeWeaksn(pString, s, n);
    mmTextLayout_UpdateText(p);
}

// default "Helvetica"
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeFontFamilyName(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    const mmUInt16_t*                              pFontFamilyName)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hFontFamilyName),
        length = mmMemberSizeof(struct mmTextFormat, hFontFamilyName),
    };
    struct mmRange r = { o, l, };
    struct mmUtf16String hFontFamilyName;
    mmUtf16String_MakeWeaks(&hFontFamilyName, pFontFamilyName);
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hFontFamilyName);
}

// default 12.0
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeFontSize(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    float                                          hFontSize)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hFontSize),
        length = mmMemberSizeof(struct mmTextFormat, hFontSize),
    };
    struct mmRange r = { o, l, };
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hFontSize);
}

// mmFontStyle_t default is mmFontStyleNormal.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeFontStyle(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    int                                            hFontStyle)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hFontStyle),
        length = mmMemberSizeof(struct mmTextFormat, hFontStyle),
    };
    struct mmRange r = { o, l, };
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hFontStyle);
}

// default is mmFontWeightNormal bold is mmFontWeightBold.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeFontWeight(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    int                                            hFontWeight)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hFontWeight),
        length = mmMemberSizeof(struct mmTextFormat, hFontWeight),
    };
    struct mmRange r = { o, l, };
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hFontWeight);
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeForegroundColor(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    mmUInt32_t                                     hForegroundColor)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hForegroundColor),
        length = mmMemberSizeof(struct mmTextFormat, hForegroundColor),
    };
    struct mmRange r = { o, l, };
    float hForegroundColorArray[4] = { 0 };
    mmColorFromARGB(hForegroundColorArray, hForegroundColor);
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, hForegroundColorArray);
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 0.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeBackgroundColor(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    mmUInt32_t                                     hBackgroundColor)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hBackgroundColor),
        length = mmMemberSizeof(struct mmTextFormat, hBackgroundColor),
    };
    struct mmRange r = { o, l, };
    float hBackgroundColorArray[4] = { 0 };
    mmColorFromARGB(hBackgroundColorArray, hBackgroundColor);
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, hBackgroundColorArray);
}

// -3 fill and Stroke 3. default 0, not Stroke.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeStrokeWidth(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    float                                          hStrokeWidth)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hStrokeWidth),
        length = mmMemberSizeof(struct mmTextFormat, hStrokeWidth),
    };
    struct mmRange r = { o, l, };
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hStrokeWidth);
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeStrokeColor(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    mmUInt32_t                                     hStrokeColor)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hStrokeColor),
        length = mmMemberSizeof(struct mmTextFormat, hStrokeColor),
    };
    struct mmRange r = { o, l, };
    float hStrokeColorArray[4] = { 0 };
    mmColorFromARGB(hStrokeColorArray, hStrokeColor);
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, hStrokeColorArray);
}

// 0 is not underline. -1 is default Width.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeUnderlineWidth(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    float                                          hUnderlineWidth)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hUnderlineWidth),
        length = mmMemberSizeof(struct mmTextFormat, hUnderlineWidth),
    };
    struct mmRange r = { o, l, };
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hUnderlineWidth);
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeUnderlineColor(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    mmUInt32_t                                     hUnderlineColor)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hUnderlineColor),
        length = mmMemberSizeof(struct mmTextFormat, hUnderlineColor),
    };
    struct mmRange r = { o, l, };
    float hUnderlineColorArray[4] = { 0 };
    mmColorFromARGB(hUnderlineColorArray, hUnderlineColor);
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, hUnderlineColorArray);
}

// 0 is not strikethru. -1 is default Width.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeStrikeThruWidth(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    float                                          hStrikeThruWidth)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hStrikeThruWidth),
        length = mmMemberSizeof(struct mmTextFormat, hStrikeThruWidth),
    };
    struct mmRange r = { o, l, };
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, &hStrikeThruWidth);
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeStrikeThruColor(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    mmUInt32_t                                     hStrikeThruColor)
{
    enum
    {
        offset = mmOffsetof(struct mmTextFormat, hStrikeThruColor),
        length = mmMemberSizeof(struct mmTextFormat, hStrikeThruColor),
    };
    struct mmRange r = { o, l, };
    float hStrikeThruColorArray[4] = { 0 };
    mmColorFromARGB(hStrikeThruColorArray, hStrikeThruColor);
    mmTextSpannable_SetParameter(&p->hTextSpannable, &r, offset, length, hStrikeThruColorArray);
}

// default Natural
MM_EXPORT_NWSI
void
mmTextLayout_SetParagraphTextAlignment(
    struct mmTextLayout*                           p,
    int                                            hTextAlignment)
{
    p->hParagraphFormat.hTextAlignment = hTextAlignment;
}

// default Top
MM_EXPORT_NWSI
void
mmTextLayout_SetParagraphAlignment(
    struct mmTextLayout*                           p,
    int                                            hParagraphAlignment)
{
    p->hParagraphFormat.hParagraphAlignment = hParagraphAlignment;
}

// default BreakByWordWrapping.
MM_EXPORT_NWSI
void
mmTextLayout_SetLineBreakMode(
    struct mmTextLayout*                           p,
    int                                            hLineBreakMode)
{
    p->hParagraphFormat.hLineBreakMode = hLineBreakMode;
}

// default WritingDirectionNatural.
MM_EXPORT_NWSI
void
mmTextLayout_SetWritingDirection(
    struct mmTextLayout*                           p,
    int                                            hWritingDirection)
{
    p->hParagraphFormat.hWritingDirection = hWritingDirection;
}

// default 0.0f.
MM_EXPORT_NWSI
void
mmTextLayout_SetFirstLineHeadIndent(
    struct mmTextLayout*                           p,
    float                                          hFirstLineHeadIndent)
{
    p->hParagraphFormat.hFirstLineHeadIndent = hFirstLineHeadIndent;
}

// default 0.0f.
MM_EXPORT_NWSI
void
mmTextLayout_SetHeadIndent(
    struct mmTextLayout*                           p,
    float                                          hHeadIndent)
{
    p->hParagraphFormat.hHeadIndent = hHeadIndent;
}

// default 0.0f.
MM_EXPORT_NWSI
void
mmTextLayout_SetTailIndent(
    struct mmTextLayout*                           p,
    float                                          hTailIndent)
{
    p->hParagraphFormat.hTailIndent = hTailIndent;
}

// default 0.0f.
MM_EXPORT_NWSI
void
mmTextLayout_SetLineSpacingAddition(
    struct mmTextLayout*                           p,
    float                                          hLineSpacingAddition)
{
    p->hParagraphFormat.hLineSpacingAddition = hLineSpacingAddition;
}

// default 1.0f.
MM_EXPORT_NWSI
void
mmTextLayout_SetLineSpacingMultiple(
    struct mmTextLayout*                           p,
    float                                          hLineSpacingMultiple)
{
    p->hParagraphFormat.hLineSpacingMultiple = hLineSpacingMultiple;
}

/*- Shadow -*/
// (x, y) default (0.0f, 0.0f)
MM_EXPORT_NWSI
void
mmTextLayout_SetShadowOffset(
    struct mmTextLayout*                           p,
    float                                          hShadowOffsetX,
    float                                          hShadowOffsetY)
{
    p->hParagraphFormat.hShadowOffset[0] = hShadowOffsetX;
    p->hParagraphFormat.hShadowOffset[1] = hShadowOffsetY;
}

// default 0, not shadow, 3 generally.
MM_EXPORT_NWSI
void
mmTextLayout_SetShadowBlur(
    struct mmTextLayout*                           p,
    float                                          hShadowBlur)
{
    p->hParagraphFormat.hShadowBlur = hShadowBlur;
}

// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetShadowColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hShadowColor)
{
    mmColorFromARGB(p->hParagraphFormat.hShadowColor, hShadowColor);
}

// default LineCapButt.
MM_EXPORT_NWSI
void
mmTextLayout_SetLineCap(
    struct mmTextLayout*                           p,
    int                                            hLineCap)
{
    p->hParagraphFormat.hLineCap = hLineCap;
}

// default LineJoinMiter
MM_EXPORT_NWSI
void
mmTextLayout_SetLineJoin(
    struct mmTextLayout*                           p,
    int                                            hLineJoin)
{
    p->hParagraphFormat.hLineJoin = hLineJoin;
}

// default is 10.0
MM_EXPORT_NWSI
void
mmTextLayout_SetLineMiterLimit(
    struct mmTextLayout*                           p,
    float                                          hLineMiterLimit)
{
    p->hParagraphFormat.hLineMiterLimit = hLineMiterLimit;
}

MM_EXPORT_NWSI
void
mmTextLayout_SetWindowSize(
    struct mmTextLayout*                           p,
    float                                          hWindowSizeW,
    float                                          hWindowSizeH)
{
    p->hParagraphFormat.hWindowSize[0] = hWindowSizeW;
    p->hParagraphFormat.hWindowSize[1] = hWindowSizeH;
}

MM_EXPORT_NWSI
void
mmTextLayout_SetLayoutRect(
    struct mmTextLayout*                           p,
    float                                          x,
    float                                          y,
    float                                          w,
    float                                          h)
{
    p->hParagraphFormat.hLayoutRect[0] = x;
    p->hParagraphFormat.hLayoutRect[1] = y;
    p->hParagraphFormat.hLayoutRect[2] = w;
    p->hParagraphFormat.hLayoutRect[3] = h;
}

MM_EXPORT_NWSI
void
mmTextLayout_SetDisplayDensity(
    struct mmTextLayout*                           p,
    float                                          hDisplayDensity)
{
    p->hParagraphFormat.hDisplayDensity = hDisplayDensity;
}

// caret index. default 0.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretIndex(
    struct mmTextLayout*                           p,
    size_t                                         hCaretIndex)
{
    p->hParagraphFormat.hCaretIndex = hCaretIndex;
}

// selection range [l, r]. default [0, 0].
MM_EXPORT_NWSI
void
mmTextLayout_SetSelection(
    struct mmTextLayout*                           p,
    size_t                                         l,
    size_t                                         r)
{
    p->hParagraphFormat.hSelection[0] = l;
    p->hParagraphFormat.hSelection[1] = r;
}

MM_EXPORT_NWSI
void
mmTextLayout_SetCaretWrapping(
    struct mmTextLayout*                           p,
    int                                            hCaretWrapping)
{
    p->hParagraphFormat.hCaretWrapping = hCaretWrapping;
}

// caret status. 0 hide 1 show.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretStatus(
    struct mmTextLayout*                           p,
    int                                            hCaretStatus)
{
    p->hParagraphFormat.hCaretStatus = hCaretStatus;
}

// selection status. 0 release 1 capture.
MM_EXPORT_NWSI
void
mmTextLayout_SetSelectionStatus(
    struct mmTextLayout*                           p,
    int                                            hSelectionStatus)
{
    p->hParagraphFormat.hSelectionStatus = hSelectionStatus;
}

// caret width. default 1.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretWidth(
    struct mmTextLayout*                           p,
    float                                          hCaretWidth)
{
    p->hParagraphFormat.hCaretWidth = hCaretWidth;
}

// default rgba { 1.0f, 1.0f, 1.0f, 1.0f, },
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hCaretColor)
{
    mmColorFromARGB(p->hParagraphFormat.hCaretColor, hCaretColor);
}

// capture color. default ARGB #FF4080FF ( 64, 128, 255).
MM_EXPORT_NWSI
void
mmTextLayout_SetSelectionCaptureColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hSelectionCaptureColor)
{
    mmColorFromARGB(p->hParagraphFormat.hSelectionCaptureColor, hSelectionCaptureColor);
}

// release color. default ARGB #FFDCDCDC (220, 220, 220).
MM_EXPORT_NWSI
void
mmTextLayout_SetSelectionReleaseColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hSelectionReleaseColor)
{
    mmColorFromARGB(p->hParagraphFormat.hSelectionReleaseColor, hSelectionReleaseColor);
}

// default "Helvetica"
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterFontFamilyName(
    struct mmTextLayout*                           p,
    const mmUInt16_t*                              pFontFamilyName)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmUtf16String_Assigns(&pMasterFormat->hFontFamilyName, pFontFamilyName);
}

// default 12.0
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterFontSize(
    struct mmTextLayout*                           p,
    float                                          hFontSize)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    pMasterFormat->hFontSize = hFontSize;
}

// mmFontStyle_t default is mmFontStyleNormal.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterFontStyle(
    struct mmTextLayout*                           p,
    int                                            hFontStyle)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    pMasterFormat->hFontStyle = hFontStyle;
}

// default is mmFontWeightNormal bold is mmFontWeightBold.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterFontWeight(
    struct mmTextLayout*                           p,
    int                                            hFontWeight)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    pMasterFormat->hFontWeight = hFontWeight;
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterForegroundColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hForegroundColor)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmColorFromARGB(pMasterFormat->hForegroundColor, hForegroundColor);
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 0.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterBackgroundColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hBackgroundColor)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmColorFromARGB(pMasterFormat->hBackgroundColor, hBackgroundColor);
}

// -3 fill and Stroke 3. default 0, not Stroke.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterStrokeWidth(
    struct mmTextLayout*                           p,
    float                                          hStrokeWidth)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    pMasterFormat->hStrokeWidth = hStrokeWidth;
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterStrokeColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hStrokeColor)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmColorFromARGB(pMasterFormat->hStrokeColor, hStrokeColor);
}

// 0 is not underline. -1 is default Width.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterUnderlineWidth(
    struct mmTextLayout*                           p,
    float                                          hUnderlineWidth)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    pMasterFormat->hUnderlineWidth = hUnderlineWidth;
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterUnderlineColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hUnderlineColor)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmColorFromARGB(pMasterFormat->hUnderlineColor, hUnderlineColor);
}

// 0 is not strikethru. -1 is default Width.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterStrikeThruWidth(
    struct mmTextLayout*                           p,
    float                                          hStrikeThruWidth)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    pMasterFormat->hStrikeThruWidth = hStrikeThruWidth;
}

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterStrikeThruColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hStrikeThruColor)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmColorFromARGB(pMasterFormat->hStrikeThruColor, hStrikeThruColor);
}

// set Master Parameter reference pointer.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterParameter(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    struct mmTextFormat* pMasterFormat = &p->hParagraphFormat.hMasterFormat;
    mmTextFormat_SetValue(pMasterFormat, o, l, v);
}

// set Range Parameter reference pointer.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeParameter(
    struct mmTextLayout*                           p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    mmTextSpannable_SetParameter(&p->hTextSpannable, r, o, l, v);
}

MM_EXPORT_NWSI
void
mmTextLayout_SetRangeProperty(
    struct mmTextLayout*                           p,
    const struct mmRange*                          r,
    enum mmTextProperty_t                          t,
    const void*                                    v)
{
    mmTextSpannable_SetProperty(&p->hTextSpannable, r, t, v);
}

MM_EXPORT_NWSI
void
mmTextLayout_ToggleCaretStatus(
    struct mmTextLayout*                           p)
{
    struct mmTextParagraphFormat* pParagraphFormat = &p->hParagraphFormat;
    pParagraphFormat->hCaretStatus += 1;
    pParagraphFormat->hCaretStatus &= 1;
}

MM_EXPORT_NWSI
void
mmTextLayout_HandleSelection(
    struct mmTextLayout*                           p)
{
    struct mmTextParagraphFormat* pParagraphFormat = &p->hParagraphFormat;
    struct mmRange r;
    r.o = pParagraphFormat->hSelection[0];
    r.l = pParagraphFormat->hSelection[1] - r.o;
    if (0 != r.l)
    {
        enum
        {
            offset = mmOffsetof(struct mmTextFormat, hBackgroundColor),
            length = mmMemberSizeof(struct mmTextFormat, hBackgroundColor),
        };
        const void* pCapture = pParagraphFormat->hSelectionCaptureColor;
        const void* pRelease = pParagraphFormat->hSelectionReleaseColor;
        const void* v = (1 == pParagraphFormat->hSelectionStatus) ? pCapture : pRelease;
        mmTextSpannable_SetAttribute(&p->hTextSpannable, &r, offset, length, v);
    }
}

MM_EXPORT_NWSI
void
mmTextLayout_OnFinishLaunching(
    struct mmTextLayout*                           p)
{
    mmTextUtf16String_NativeEncode(&p->hText);
    mmTextParagraphFormat_NativeEncode(&p->hParagraphFormat);
    mmTextSpannable_NativeEncode(&p->hTextSpannable);

    mmTextUtf16String_OnFinishLaunching(&p->hText);
    mmTextParagraph_OnFinishLaunching(&p->hTextParagraph);
    mmTextRenderTarget_OnFinishLaunching(&p->hTextRenderTarget);
    mmTextDrawingContext_OnFinishLaunching(&p->hDrawingContext);

    mmTextDrawingContext_PrepareRenderTarget(&p->hDrawingContext, &p->hTextRenderTarget);
    mmTextParagraph_UpdateDrawingContext(&p->hTextParagraph);
}

MM_EXPORT_NWSI
void
mmTextLayout_OnBeforeTerminate(
    struct mmTextLayout*                           p)
{
    mmTextDrawingContext_DiscardRenderTarget(&p->hDrawingContext);
    
    mmTextDrawingContext_OnBeforeTerminate(&p->hDrawingContext);
    mmTextRenderTarget_OnBeforeTerminate(&p->hTextRenderTarget);
    mmTextParagraph_OnBeforeTerminate(&p->hTextParagraph);
    mmTextUtf16String_OnBeforeTerminate(&p->hText);

    // For API complete quick test.
    //
    // mmTextSpannable_NativeDecode(&p->hTextSpannable);
    // mmTextParagraphFormat_NativeDecode(&p->hParagraphFormat);
    // mmTextUtf16String_NativeDecode(&p->hText);
}

MM_EXPORT_NWSI
void
mmTextLayout_SpannableReset(
    struct mmTextLayout*                           p)
{
    mmTextSpannable_ResetParameters(&p->hTextSpannable);
}

MM_EXPORT_NWSI
void
mmTextLayout_UpdateParagraph(
    struct mmTextLayout*                           p)
{
    mmTextParagraphFormat_NativeEncode(&p->hParagraphFormat);
    mmTextSpannable_NativeEncode(&p->hTextSpannable);
    mmTextUtf16String_NativeEncode(&p->hText);

    mmTextParagraph_OnBeforeTerminate(&p->hTextParagraph);
    mmTextParagraph_OnFinishLaunching(&p->hTextParagraph);

    mmTextParagraph_UpdateDrawingContext(&p->hTextParagraph);
}

MM_EXPORT_NWSI
void
mmTextLayout_UpdateTextRange(
    struct mmTextLayout*                           p)
{
    size_t hTextLength = mmTextUtf16String_Size(&p->hText);
    struct mmRange hRange = { 0, hTextLength, };
    mmTextSpannable_SetMasterRange(&p->hTextSpannable, &hRange);
}

MM_EXPORT_NWSI
void
mmTextLayout_UpdateText(
    struct mmTextLayout*                           p)
{
    mmTextLayout_UpdateTextRange(p);
    mmTextSpannable_UpdateAttributes(&p->hTextSpannable);
    mmTextLayout_HandleSelection(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_UpdateSize(
    struct mmTextLayout*                           p)
{
    mmTextLayout_OnBeforeTerminate(p);
    mmTextLayout_OnFinishLaunching(p);
}

MM_EXPORT_NWSI
void
mmTextLayout_Draw(
    struct mmTextLayout*                           p)
{
     mmTextParagraph_Draw(&p->hTextParagraph);
}

MM_EXPORT_NWSI
const void*
mmTextLayout_BufferLock(
    struct mmTextLayout*                           p)
{
    return mmTextRenderTarget_BufferLock(&p->hTextRenderTarget);
}

MM_EXPORT_NWSI
void
mmTextLayout_BufferUnLock(
    struct mmTextLayout*                           p,
    const void*                                    pBuffer)
{
    mmTextRenderTarget_BufferUnLock(&p->hTextRenderTarget, pBuffer);
}

MM_EXPORT_NWSI
void
mmTextLayout_HitTestPoint(
    const struct mmTextLayout*                     p,
    float                                          point[2],
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics)
{
    mmTextParagraph_HitTestPoint(
        &p->hTextParagraph, 
        point, 
        isTrailingHit, 
        isInside, 
        pMetrics);
}

MM_EXPORT_NWSI
void
mmTextLayout_HitTestTextPosition(
    const struct mmTextLayout*                     p,
    mmUInt32_t                                     offset,
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics)
{
    mmTextParagraph_HitTestTextPosition(
        &p->hTextParagraph, 
        offset, 
        isTrailingHit, 
        point, 
        pMetrics);
}

