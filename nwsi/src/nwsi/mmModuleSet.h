#ifndef __mmModuleSet_h__
#define __mmModuleSet_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"
#include "container/mmListVpt.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmEventUpdate;

struct mmModuleMetadata
{
    // type name
    const char* TypeName;

    // type size
    size_t TypeSize;

    // typedef void(*Produce)(void* p);
    void* Produce;

    // typedef void(*Recycle)(void* p);
    void* Recycle;

    // void(*OnUpdate)(void* p, struct mmEventUpdate* content);
    void* OnUpdate;

    // void(*OnStart)(void* p);
    void* OnStart;
    // void(*OnInterrupt)(void* p);
    void* OnInterrupt;
    // void(*OnShutdown)(void* p);
    void* OnShutdown;
    // void(*OnJoin)(void* p);
    void* OnJoin;

    // void(*OnFinishLaunching)(void* p);
    void* OnFinishLaunching;
    // void(*OnBeforeTerminate)(void* p);
    void* OnBeforeTerminate;

    // void(*OnEnterBackground)(void* p);
    void* OnEnterBackground;
    // void(*OnEnterForeground)(void* p);
    void* OnEnterForeground;
};

struct mmModule
{
    // metadata
    const struct mmModuleMetadata* metadata;
    
    // object handler.
    void* object;
};
MM_EXPORT_NWSI void mmModule_Init(struct mmModule* p);
MM_EXPORT_NWSI void mmModule_Destroy(struct mmModule* p);

struct mmModuleSet
{
    struct mmRbtreeStringVpt rbtree;
    struct mmListVpt list;
    void* object;
};
MM_EXPORT_NWSI void mmModuleSet_Init(struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_Destroy(struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_AddType(struct mmModuleSet* p, const struct mmModuleMetadata* m);
MM_EXPORT_NWSI void mmModuleSet_RmvType(struct mmModuleSet* p, const struct mmModuleMetadata* m);

MM_EXPORT_NWSI void mmModuleSet_SetObject(struct mmModuleSet* p, void* object);
MM_EXPORT_NWSI struct mmModule* mmModuleSet_GetModule(const struct mmModuleSet* p, const char* pTypeName);

MM_EXPORT_NWSI void mmModuleSet_OnSetObject(const struct mmModuleSet* p, void* object);

MM_EXPORT_NWSI void mmModuleSet_OnUpdate(const struct mmModuleSet* p, struct mmEventUpdate* content);

MM_EXPORT_NWSI void mmModuleSet_OnStart(const struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_OnInterrupt(const struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_OnShutdown(const struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_OnJoin(const struct mmModuleSet* p);

MM_EXPORT_NWSI void mmModuleSet_OnFinishLaunching(const struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_OnBeforeTerminate(const struct mmModuleSet* p);

MM_EXPORT_NWSI void mmModuleSet_OnEnterBackground(const struct mmModuleSet* p);
MM_EXPORT_NWSI void mmModuleSet_OnEnterForeground(const struct mmModuleSet* p);

#include "core/mmSuffix.h"

#endif//__mmModuleSet_h__
