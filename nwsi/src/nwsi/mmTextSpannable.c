/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextSpannable.h"

#include "nwsi/mmTextSpannableNative.h"

#include "core/mmAlloc.h"

#include "math/mmMath.h"

static
void
__static_mmTextSpannable_AppendMasterFormat(
    struct mmTextSpannable*                        p);

static
void
__static_mmTextSpannable_UpdateMasterFormat(
    struct mmTextSpannable*                        p);

static
void
__static_mmTextSpannable_SimplifyRange(
    struct mmTextSpannable*                        p,
    struct mmListVpt*                              list,
    struct mmListHead*                             sl,
    struct mmListHead*                             sr);

static
void
__static_mmTextSpannable_ResetList(
    struct mmTextSpannable*                        p,
    struct mmListVpt*                              list);

static
void
__static_mmTextSpannable_SetValueList(
    struct mmTextSpannable*                        p,
    struct mmListVpt*                              list,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v);

static
void
__static_mmTextSpannable_CopyValues(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI
void
mmTextSpan_Init(
    struct mmTextSpan*                             p)
{
    mmRange_Init(&p->hRange);
    mmTextFormat_Init(&p->hFormat);
}

MM_EXPORT_NWSI
void
mmTextSpan_Destroy(
    struct mmTextSpan*                             p)
{
    mmRange_Destroy(&p->hRange);
    mmTextFormat_Destroy(&p->hFormat);
}

MM_EXPORT_NWSI
void
mmTextSpan_Reset(
    struct mmTextSpan*                             p)
{
    mmRange_Reset(&p->hRange);
    mmTextFormat_Reset(&p->hFormat);
}

MM_EXPORT_NWSI
void
mmTextSpan_Assign(
    struct mmTextSpan*                             p,
    const struct mmTextSpan*                       q)
{
    mmRange_Assign(&p->hRange, &q->hRange);
    mmTextFormat_Assign(&p->hFormat, &q->hFormat);
}

MM_EXPORT_NWSI
void
mmTextSpannable_Init(
    struct mmTextSpannable*                        p)
{
    p->pMasterFormat = NULL;
    mmRange_Init(&p->hMasterRange);
    mmListVpt_Init(&p->hParameters);
    mmListVpt_Init(&p->hAttributes);
    p->hParameterNumber = 0;
    p->pObject = NULL;

    mmTextSpannable_NativeInit(p);
}

MM_EXPORT_NWSI
void
mmTextSpannable_Destroy(
    struct mmTextSpannable*                        p)
{
    mmTextSpannable_Clear(p);

    mmTextSpannable_NativeDestroy(p);

    p->pObject = NULL;
    p->hParameterNumber = 0;
    mmListVpt_Destroy(&p->hAttributes);
    mmListVpt_Destroy(&p->hParameters);
    mmRange_Destroy(&p->hMasterRange);
    p->pMasterFormat = NULL;
}

MM_EXPORT_NWSI
void
mmTextSpannable_Clear(
    struct mmTextSpannable*                        p)
{
    __static_mmTextSpannable_ResetList(p, &p->hAttributes);
    __static_mmTextSpannable_ResetList(p, &p->hParameters);
    p->hParameterNumber = 0;
}

MM_EXPORT_NWSI
void
mmTextSpannable_SetObject(
    struct mmTextSpannable*                        p,
    void*                                          pObject)
{
    p->pObject = pObject;
}

MM_EXPORT_NWSI
void
mmTextSpannable_SetMasterFormat(
    struct mmTextSpannable*                        p,
    const struct mmTextFormat*                     pMasterFormat)
{
    p->pMasterFormat = pMasterFormat;
}

MM_EXPORT_NWSI
void
mmTextSpannable_SetMasterRange(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          pRange)
{
    mmRange_Assign(&p->hMasterRange, pRange);
}

MM_EXPORT_NWSI
void
mmTextSpannable_SetParameter(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    __static_mmTextSpannable_SetValueList(
        p, 
        &p->hParameters, 
        r, o, l, v);

    p->hParameterNumber++;
}

MM_EXPORT_NWSI
void
mmTextSpannable_SetProperty(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          r,
    enum mmTextProperty_t                          t,
    const void*                                    v)
{
    const struct mmRange* pts;
    assert(0 <= t && t < MM_ARRAY_SIZE(mmTextPropertys) && "Property type is invalid.");
    pts = &mmTextPropertys[t];
    mmTextSpannable_SetParameter(p, r, pts->o, pts->l, v);
}

MM_EXPORT_NWSI
void
mmTextSpannable_SetAttribute(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    __static_mmTextSpannable_SetValueList(
        p, 
        &p->hAttributes, 
        r, o, l, v);
}

MM_EXPORT_NWSI
void
mmTextSpannable_GetMaxFontSize(
    const struct mmTextSpannable*                  p,
    const struct mmRange*                          r,
    float*                                         pMaxFontSize)
{
    float size = 0.0f;
    
    size_t pl = r->o;
    size_t pr = r->l + pl;
    size_t ql = 0;
    size_t qr = 0;

    struct mmTextSpan* e = NULL;
    
    const struct mmListVpt* list = &p->hAttributes;
    struct mmListHead* sl = list->l.next;
    struct mmListHead* sr = list->l.prev;

    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);

        e = (struct mmTextSpan*)it->v;

        ql = e->hRange.o;
        qr = e->hRange.l + ql;

        if (qr <= pl)
        {
            // e ====
            // u     xxxx
            sl = curr;
            continue;
        }
        if (pr <= ql)
        {
            // e     ====
            // u xxxx
            sr = curr;
            break;
        }
        if (pl <= ql && qr <= pr)
        {
            // e  ====
            // u xxxxxx
            size = mmMathMax(size, e->hFormat.hFontSize);
            continue;
        }
        if (pl <= ql && qr > pr)
        {
            // e   ====
            // u xxxx
            size = mmMathMax(size, e->hFormat.hFontSize);
            continue;
        }
        if (pl > ql && qr <= pr)
        {
            // e ====
            // u   xxxx
            size = mmMathMax(size, e->hFormat.hFontSize);
            continue;
        }
        if (pl > ql && qr > pr)
        {
            // e ======
            // u  xxxx
            size = mmMathMax(size, e->hFormat.hFontSize);
            continue;
        }
    }
    
    (*pMaxFontSize) = size;
}

MM_EXPORT_NWSI
void
mmTextSpannable_UpdateAttributes(
    struct mmTextSpannable*                        p)
{
    __static_mmTextSpannable_UpdateMasterFormat(p);
    __static_mmTextSpannable_ResetList(p, &p->hAttributes);
    __static_mmTextSpannable_CopyValues(p);
}

MM_EXPORT_NWSI
void
mmTextSpannable_ResetParameters(
    struct mmTextSpannable*                        p)
{
    p->hParameterNumber = 0;
    __static_mmTextSpannable_ResetList(p, &p->hParameters);
    __static_mmTextSpannable_AppendMasterFormat(p);
}

MM_EXPORT_NWSI
void
mmTextSpannable_Simplify(
    struct mmTextSpannable*                        p)
{
    struct mmListHead* cl = p->hAttributes.l.next;
    struct mmListHead* cr = p->hAttributes.l.prev;
    
    __static_mmTextSpannable_SimplifyRange(p, &p->hAttributes, cl, cr);
}

MM_EXPORT_NWSI
struct mmTextSpan*
mmTextSpannable_SpanProduce(
    struct mmTextSpannable*                        p)
{
    struct mmTextSpan* e = (struct mmTextSpan*)mmMalloc(sizeof(struct mmTextSpan));
    mmTextSpan_Init(e);
    return e;
}

MM_EXPORT_NWSI
void
mmTextSpannable_SpanRecycle(
    struct mmTextSpannable*                        p,
    struct mmTextSpan*                             e)
{
    mmTextSpan_Destroy(e);
    mmFree(e);
}

static
void
__static_mmTextSpannable_AppendMasterFormat(
    struct mmTextSpannable*                        p)
{
    struct mmTextSpan* e;
    e = mmTextSpannable_SpanProduce(p);
    mmTextFormat_Assign(&e->hFormat, p->pMasterFormat);
    mmRange_Assign(&e->hRange, &p->hMasterRange);
    mmListVpt_AddTail(&p->hParameters, e);
}

static
void
__static_mmTextSpannable_UpdateMasterFormat(
    struct mmTextSpannable*                        p)
{
    if (0 == p->hParameters.size)
    {
        // when the hParameters size is zero.
        // we need append the Master Format first.
        __static_mmTextSpannable_AppendMasterFormat(p);
    }

    if (1 == p->hParameters.size &&
        0 == p->hParameterNumber)
    {
        // only have MasterFormat.
        struct mmTextSpan* e = NULL;

        struct mmListHead* next = NULL;
        struct mmListVptIterator* it = NULL;
        next = p->hParameters.l.next;

        it = mmList_Entry(next, struct mmListVptIterator, n);
        e = (struct mmTextSpan*)it->v;
        mmTextFormat_Assign(&e->hFormat, p->pMasterFormat);
        mmRange_Assign(&e->hRange, &p->hMasterRange);
    }
}

static 
void 
__static_mmTextSpannable_SimplifyRange(
    struct mmTextSpannable*                        p, 
    struct mmListVpt*                              list,
    struct mmListHead*                             sl,
    struct mmListHead*                             sr)
{
    struct mmTextSpan* e = NULL;
    struct mmTextSpan* u = NULL;
    
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    struct mmListVptIterator* ik = NULL;
    next = sl;
    while (next != &list->l && next->next != &list->l && next != sr)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmTextSpan*)it->v;
        
        ik = mmList_Entry(next, struct mmListVptIterator, n);
        u = (struct mmTextSpan*)ik->v;
        if(mmTextFormat_Equals(&e->hFormat, &u->hFormat))
        {
            u->hRange.l = e->hRange.l + u->hRange.l;
            u->hRange.o = e->hRange.o;
            mmListVpt_Erase(list, it);
            mmTextSpannable_SpanRecycle(p, e);
        }
    }
}

static
void
__static_mmTextSpannable_ResetList(
    struct mmTextSpannable*                        p,
    struct mmListVpt*                              list)
{
    struct mmTextSpan* e = NULL;

    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmTextSpan*)it->v;
        mmListVpt_Erase(list, it);
        mmTextSpannable_SpanRecycle(p, e);
    }
}

static
void
__static_mmTextSpannable_SetValueList(
    struct mmTextSpannable*                        p,
    struct mmListVpt*                              list,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    size_t pl = r->o;
    size_t pr = r->l + pl;
    size_t ql = 0;
    size_t qr = 0;

    struct mmTextSpan* e = NULL;
    struct mmTextSpan* u = NULL;
    struct mmTextSpan* w = NULL;

    struct mmListHead* sl = list->l.next;
    struct mmListHead* sr = list->l.prev;

    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);

        e = (struct mmTextSpan*)it->v;

        ql = e->hRange.o;
        qr = e->hRange.l + ql;

        if (qr <= pl)
        {
            // e ====
            // u     xxxx
            sl = curr;
            continue;
        }
        if (pr <= ql)
        {
            // e     ====
            // u xxxx
            sr = curr;
            break;
        }
        if (pl <= ql && qr <= pr)
        {
            // e  ====
            // u xxxxxx
            mmTextFormat_SetValue(&e->hFormat, o, l, v);
            continue;
        }
        if (pl <= ql && qr > pr)
        {
            // e   ====
            // u xxxx
            u = mmTextSpannable_SpanProduce(p);
            mmTextSpan_Assign(u, e);
            mmListVpt_InsertPrev(list, it, u);

            mmTextFormat_SetValue(&u->hFormat, o, l, v);

            e->hRange.o = pr;
            e->hRange.l = qr - pr;

            u->hRange.o = ql;
            u->hRange.l = pr - ql;
            continue;
        }
        if (pl > ql && qr <= pr)
        {
            // e ====
            // u   xxxx
            u = mmTextSpannable_SpanProduce(p);
            mmTextSpan_Assign(u, e);
            mmListVpt_InsertNext(list, it, u);

            mmTextFormat_SetValue(&u->hFormat, o, l, v);

            u->hRange.o = pl;
            u->hRange.l = qr - pl;

            e->hRange.o = ql;
            e->hRange.l = pl - ql;
            continue;
        }
        if (pl > ql && qr > pr)
        {
            // e ======
            // u  xxxx
            w = mmTextSpannable_SpanProduce(p);
            mmTextSpan_Assign(w, e);
            mmListVpt_InsertPrev(list, it, w);

            u = mmTextSpannable_SpanProduce(p);
            mmTextSpan_Assign(u, e);
            mmListVpt_InsertNext(list, it, u);

            mmTextFormat_SetValue(&e->hFormat, o, l, v);

            w->hRange.o = ql;
            w->hRange.l = pl - ql;

            e->hRange.o = pl;
            e->hRange.l = pr - pl;

            u->hRange.o = pr;
            u->hRange.l = qr - pr;
            continue;
        }
    }

    __static_mmTextSpannable_SimplifyRange(p, list, sl, sr);
}

static
void
__static_mmTextSpannable_CopyValues(
    struct mmTextSpannable*                        p)
{
    struct mmTextSpan* e = NULL;
    struct mmTextSpan* u = NULL;

    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    next = p->hParameters.l.next;
    while (next != &p->hParameters.l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmTextSpan*)it->v;

        u = mmTextSpannable_SpanProduce(p);
        mmTextSpan_Assign(u, e);
        mmListVpt_AddTail(&p->hAttributes, u);
    }
}
