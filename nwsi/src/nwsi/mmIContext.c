/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmIContext.h"

static void __static_mmIContext_OnStart(struct mmIContext* p)
{

}
static void __static_mmIContext_OnInterrupt(struct mmIContext* p)
{

}
static void __static_mmIContext_OnShutdown(struct mmIContext* p)
{

}
static void __static_mmIContext_OnJoin(struct mmIContext* p)
{

}

static void __static_mmIContext_OnFinishLaunching(struct mmIContext* p)
{

}
static void __static_mmIContext_OnBeforeTerminate(struct mmIContext* p)
{

}

static void __static_mmIContext_OnEnterBackground(struct mmIContext* p)
{

}
static void __static_mmIContext_OnEnterForeground(struct mmIContext* p)
{

}

MM_EXPORT_NWSI void mmIContext_Init(struct mmIContext* p)
{
    p->OnStart = &__static_mmIContext_OnStart;
    p->OnInterrupt = &__static_mmIContext_OnInterrupt;
    p->OnShutdown = &__static_mmIContext_OnShutdown;
    p->OnJoin = &__static_mmIContext_OnJoin;

    p->OnFinishLaunching = &__static_mmIContext_OnFinishLaunching;
    p->OnBeforeTerminate = &__static_mmIContext_OnBeforeTerminate;
    
    p->OnFinishLaunching = &__static_mmIContext_OnEnterBackground;
    p->OnBeforeTerminate = &__static_mmIContext_OnEnterForeground;
}
MM_EXPORT_NWSI void mmIContext_Destroy(struct mmIContext* p)
{
    p->OnStart = &__static_mmIContext_OnStart;
    p->OnInterrupt = &__static_mmIContext_OnInterrupt;
    p->OnShutdown = &__static_mmIContext_OnShutdown;
    p->OnJoin = &__static_mmIContext_OnJoin;

    p->OnFinishLaunching = &__static_mmIContext_OnFinishLaunching;
    p->OnBeforeTerminate = &__static_mmIContext_OnBeforeTerminate;
    
    p->OnFinishLaunching = &__static_mmIContext_OnEnterBackground;
    p->OnBeforeTerminate = &__static_mmIContext_OnEnterForeground;
}
