/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmContextMaster_h__
#define __mmContextMaster_h__

#include "core/mmCore.h"
#include "core/mmArgument.h"
#include "core/mmEventSet.h"

#include "nwsi/mmIContext.h"
#include "nwsi/mmDevice.h"
#include "nwsi/mmPackageAssets.h"
#include "nwsi/mmAppIdentifier.h"
#include "nwsi/mmSecurityStore.h"
#include "nwsi/mmGraphicsFactory.h"
#include "nwsi/mmFontsSource.h"
#include "nwsi/mmModuleSet.h"
#include "nwsi/mmClipboard.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern const char* mmContextMaster_EventBattery;
MM_EXPORT_NWSI extern const char* mmContextMaster_EventLocation;
MM_EXPORT_NWSI extern const char* mmContextMaster_EventSensor;
MM_EXPORT_NWSI extern const char* mmContextMaster_EventSignal;

MM_EXPORT_NWSI extern const char* mmContextMaster_EventOrientation;

MM_EXPORT_NWSI extern const char* mmContextMaster_EventEnterBackground;
MM_EXPORT_NWSI extern const char* mmContextMaster_EventEnterForeground;

struct mmContextMaster
{
    struct mmIContext hSuper;
    
    struct mmArgument hArgument;
    struct mmFileContext hFileContext;
    
    struct mmDevice hDevice;
    struct mmPackageAssets hPackageAssets;
    struct mmAppIdentifier hAppIdentifier;
    struct mmSecurityStore hSecurityStore;
    struct mmGraphicsFactory hGraphicsFactory;

    struct mmFontsSource hFontsSource;

    // Module data Manager.
    struct mmModuleSet hModuleSet;
    
    // this member is event drive.
    struct mmEventSet hEventSet;

    // clipboard.
    struct mmClipboard hClipboard;

    void* pAppHandler;
    
    // mm_thread_state_t, default is MM_TS_CLOSED(0)
    mmSInt8_t hState;
};

MM_EXPORT_NWSI void mmContextMaster_Init(struct mmContextMaster* p);
MM_EXPORT_NWSI void mmContextMaster_Destroy(struct mmContextMaster* p);

MM_EXPORT_NWSI void mmContextMaster_SetArgument(struct mmContextMaster* p, int argc, const char** argv);

MM_EXPORT_NWSI void mmContextMaster_SetAppHandler(struct mmContextMaster* p, void* pAppHandler);
MM_EXPORT_NWSI void mmContextMaster_SetModuleName(struct mmContextMaster* p, const char* pModuleName);
MM_EXPORT_NWSI void mmContextMaster_SetShaderCacheFolder(struct mmContextMaster* p, const char* pFolder);
MM_EXPORT_NWSI void mmContextMaster_SetRenderSystemName(struct mmContextMaster* p, const char* pRenderSystemName);
MM_EXPORT_NWSI void mmContextMaster_SetTextureCacheEnabled(struct mmContextMaster* p, mmBool_t hTextureCacheEnabled);
MM_EXPORT_NWSI void mmContextMaster_SetClipboardProvider(struct mmContextMaster* p, struct mmClipboardProvider* pClipboardProvider);
// virtual function for super interface implement.
MM_EXPORT_NWSI void mmContextMaster_OnStart(struct mmIContext* pSuper);
MM_EXPORT_NWSI void mmContextMaster_OnInterrupt(struct mmIContext* pSuper);
MM_EXPORT_NWSI void mmContextMaster_OnShutdown(struct mmIContext* pSuper);
MM_EXPORT_NWSI void mmContextMaster_OnJoin(struct mmIContext* pSuper);

MM_EXPORT_NWSI void mmContextMaster_OnFinishLaunching(struct mmIContext* pSuper);
MM_EXPORT_NWSI void mmContextMaster_OnBeforeTerminate(struct mmIContext* pSuper);

MM_EXPORT_NWSI void mmContextMaster_OnEnterBackground(struct mmIContext* pSuper);
MM_EXPORT_NWSI void mmContextMaster_OnEnterForeground(struct mmIContext* pSuper);

#include "core/mmSuffix.h"

#endif//__mmContextMaster_h__
