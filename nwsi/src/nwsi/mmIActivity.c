/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmIActivity.h"

static void __static_mmIActivity_SetNativePointer(struct mmIActivity* p, void* pNativePointer)
{

}

static void __static_mmIActivity_SetContext(struct mmIActivity* p, struct mmContextMaster* pContextMaster)
{

}
static void __static_mmIActivity_SetSurface(struct mmIActivity* p, struct mmSurfaceMaster* pSurfaceMaster)
{

}

static void __static_mmIActivity_OnDragging(struct mmIActivity* p, struct mmEventDragging* content)
{

}

static void __static_mmIActivity_OnKeypadPressed(struct mmIActivity* p, struct mmEventKeypad* content)
{

}
static void __static_mmIActivity_OnKeypadRelease(struct mmIActivity* p, struct mmEventKeypad* content)
{

}
static void __static_mmIActivity_OnKeypadStatus(struct mmIActivity* p, struct mmEventKeypadStatus* content)
{

}

static void __static_mmIActivity_OnCursorBegan(struct mmIActivity* p, struct mmEventCursor* content)
{

}
static void __static_mmIActivity_OnCursorMoved(struct mmIActivity* p, struct mmEventCursor* content)
{

}
static void __static_mmIActivity_OnCursorEnded(struct mmIActivity* p, struct mmEventCursor* content)
{

}
static void __static_mmIActivity_OnCursorBreak(struct mmIActivity* p, struct mmEventCursor* content)
{

}
static void __static_mmIActivity_OnCursorWheel(struct mmIActivity* p, struct mmEventCursor* content)
{

}

static void __static_mmIActivity_OnTouchsBegan(struct mmIActivity* p, struct mmEventTouchs* content)
{

}
static void __static_mmIActivity_OnTouchsMoved(struct mmIActivity* p, struct mmEventTouchs* content)
{

}
static void __static_mmIActivity_OnTouchsEnded(struct mmIActivity* p, struct mmEventTouchs* content)
{

}
static void __static_mmIActivity_OnTouchsBreak(struct mmIActivity* p, struct mmEventTouchs* content)
{

}

static void __static_mmIActivity_OnSurfacePrepare(struct mmIActivity* p, struct mmEventMetrics* content)
{

}
static void __static_mmIActivity_OnSurfaceDiscard(struct mmIActivity* p, struct mmEventMetrics* content)
{

}
static void __static_mmIActivity_OnSurfaceChanged(struct mmIActivity* p, struct mmEventMetrics* content)
{

}

static void __static_mmIActivity_OnGetText(struct mmIActivity* p, struct mmEventEditText* content)
{

}
static void __static_mmIActivity_OnSetText(struct mmIActivity* p, struct mmEventEditText* content)
{

}
static void __static_mmIActivity_OnGetTextEditRect(struct mmIActivity* p, double rect[4])
{

}
static void __static_mmIActivity_OnInsertTextUtf16(struct mmIActivity* p, struct mmEventEditString* content)
{

}
static void __static_mmIActivity_OnReplaceTextUtf16(struct mmIActivity* p, struct mmEventEditString* content)
{

}

static void __static_mmIActivity_OnInjectCopy(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnInjectCut(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnInjectPaste(struct mmIActivity* p)
{

}

static void __static_mmIActivity_OnDisplayOneFrame(struct mmIActivity* p, struct mmEventUpdate* content)
{

}

static void __static_mmIActivity_OnStart(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnInterrupt(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnShutdown(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnJoin(struct mmIActivity* p)
{

}

static void __static_mmIActivity_OnFinishLaunching(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnBeforeTerminate(struct mmIActivity* p)
{

}

static void __static_mmIActivity_OnEnterBackground(struct mmIActivity* p)
{

}
static void __static_mmIActivity_OnEnterForeground(struct mmIActivity* p)
{

}

MM_EXPORT_NWSI void mmIActivity_Init(struct mmIActivity* p)
{
    p->SetNativePointer = &__static_mmIActivity_SetNativePointer;
    p->SetContext = &__static_mmIActivity_SetContext;
    p->SetSurface = &__static_mmIActivity_SetSurface;
    p->OnDragging = &__static_mmIActivity_OnDragging;
    p->OnKeypadPressed = &__static_mmIActivity_OnKeypadPressed;
    p->OnKeypadRelease = &__static_mmIActivity_OnKeypadRelease;
    p->OnKeypadStatus = &__static_mmIActivity_OnKeypadStatus;
    p->OnCursorBegan = &__static_mmIActivity_OnCursorBegan;
    p->OnCursorMoved = &__static_mmIActivity_OnCursorMoved;
    p->OnCursorEnded = &__static_mmIActivity_OnCursorEnded;
    p->OnCursorBreak = &__static_mmIActivity_OnCursorBreak;
    p->OnCursorWheel = &__static_mmIActivity_OnCursorWheel;
    p->OnTouchsBegan = &__static_mmIActivity_OnTouchsBegan;
    p->OnTouchsMoved = &__static_mmIActivity_OnTouchsMoved;
    p->OnTouchsEnded = &__static_mmIActivity_OnTouchsEnded;
    p->OnTouchsBreak = &__static_mmIActivity_OnTouchsBreak;
    p->OnSurfacePrepare = &__static_mmIActivity_OnSurfacePrepare;
    p->OnSurfaceDiscard = &__static_mmIActivity_OnSurfaceDiscard;
    p->OnSurfaceChanged = &__static_mmIActivity_OnSurfaceChanged;
    p->OnGetText = &__static_mmIActivity_OnGetText;
    p->OnSetText = &__static_mmIActivity_OnSetText;
    p->OnGetTextEditRect = &__static_mmIActivity_OnGetTextEditRect;
    p->OnInsertTextUtf16 = &__static_mmIActivity_OnInsertTextUtf16;
    p->OnReplaceTextUtf16 = &__static_mmIActivity_OnReplaceTextUtf16;
    p->OnInjectCopy = &__static_mmIActivity_OnInjectCopy;
    p->OnInjectCut = &__static_mmIActivity_OnInjectCut;
    p->OnInjectPaste = &__static_mmIActivity_OnInjectPaste;
    p->OnDisplayOneFrame = &__static_mmIActivity_OnDisplayOneFrame;
    p->OnStart = &__static_mmIActivity_OnStart;
    p->OnInterrupt = &__static_mmIActivity_OnInterrupt;
    p->OnShutdown = &__static_mmIActivity_OnShutdown;
    p->OnJoin = &__static_mmIActivity_OnJoin;
    p->OnFinishLaunching = &__static_mmIActivity_OnFinishLaunching;
    p->OnBeforeTerminate = &__static_mmIActivity_OnBeforeTerminate;
    p->OnEnterBackground = &__static_mmIActivity_OnEnterBackground;
    p->OnEnterForeground = &__static_mmIActivity_OnEnterForeground;
}
MM_EXPORT_NWSI void mmIActivity_Destroy(struct mmIActivity* p)
{
    p->SetNativePointer = &__static_mmIActivity_SetNativePointer;
    p->SetContext = &__static_mmIActivity_SetContext;
    p->SetSurface = &__static_mmIActivity_SetSurface;
    p->OnDragging = &__static_mmIActivity_OnDragging;
    p->OnKeypadPressed = &__static_mmIActivity_OnKeypadPressed;
    p->OnKeypadRelease = &__static_mmIActivity_OnKeypadRelease;
    p->OnKeypadStatus = &__static_mmIActivity_OnKeypadStatus;
    p->OnCursorBegan = &__static_mmIActivity_OnCursorBegan;
    p->OnCursorMoved = &__static_mmIActivity_OnCursorMoved;
    p->OnCursorEnded = &__static_mmIActivity_OnCursorEnded;
    p->OnCursorBreak = &__static_mmIActivity_OnCursorBreak;
    p->OnCursorWheel = &__static_mmIActivity_OnCursorWheel;
    p->OnTouchsBegan = &__static_mmIActivity_OnTouchsBegan;
    p->OnTouchsMoved = &__static_mmIActivity_OnTouchsMoved;
    p->OnTouchsEnded = &__static_mmIActivity_OnTouchsEnded;
    p->OnTouchsBreak = &__static_mmIActivity_OnTouchsBreak;
    p->OnSurfacePrepare = &__static_mmIActivity_OnSurfacePrepare;
    p->OnSurfaceDiscard = &__static_mmIActivity_OnSurfaceDiscard;
    p->OnSurfaceChanged = &__static_mmIActivity_OnSurfaceChanged;
    p->OnGetText = &__static_mmIActivity_OnGetText;
    p->OnSetText = &__static_mmIActivity_OnSetText;
    p->OnGetTextEditRect = &__static_mmIActivity_OnGetTextEditRect;
    p->OnInsertTextUtf16 = &__static_mmIActivity_OnInsertTextUtf16;
    p->OnReplaceTextUtf16 = &__static_mmIActivity_OnReplaceTextUtf16;
    p->OnInjectCopy = &__static_mmIActivity_OnInjectCopy;
    p->OnInjectCut = &__static_mmIActivity_OnInjectCut;
    p->OnInjectPaste = &__static_mmIActivity_OnInjectPaste;
    p->OnDisplayOneFrame = &__static_mmIActivity_OnDisplayOneFrame;
    p->OnStart = &__static_mmIActivity_OnStart;
    p->OnInterrupt = &__static_mmIActivity_OnInterrupt;
    p->OnShutdown = &__static_mmIActivity_OnShutdown;
    p->OnJoin = &__static_mmIActivity_OnJoin;
    p->OnFinishLaunching = &__static_mmIActivity_OnFinishLaunching;
    p->OnBeforeTerminate = &__static_mmIActivity_OnBeforeTerminate;
    p->OnEnterBackground = &__static_mmIActivity_OnEnterBackground;
    p->OnEnterForeground = &__static_mmIActivity_OnEnterForeground;
}
