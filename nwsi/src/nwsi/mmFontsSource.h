/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFontsSource_h__
#define __mmFontsSource_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmLogger.h"

#include "container/mmRbtreeString.h"
#include "container/mmListString.h"
#include "container/mmVectorValue.h"

#include "dish/mmFileContext.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmFontsData
{
    struct mmString lang;
    struct mmString style;
    struct mmString source;
    struct mmString variant;
    float weight;
};

MM_EXPORT_NWSI void mmFontsData_Init(struct mmFontsData* p);
MM_EXPORT_NWSI void mmFontsData_Destroy(struct mmFontsData* p);
// Copy q to p.
MM_EXPORT_NWSI void mmFontsData_CopyFrom(struct mmFontsData* p, struct mmFontsData* q);

struct mmFontsSource;

struct mmFontsDocumentBase
{
    void(*Analysis)(struct mmFontsDocumentBase* p, struct mmFontsSource* _pFontsLang, const char* _fileName);
};

struct mmFontsDocumentCreator
{
    struct mmFontsDocumentBase*(*Produce)(void);
    void(*Recycle)(struct mmFontsDocumentBase* m);
};

MM_EXPORT_NWSI extern const char* mmFontsSource_FontsLanguage_zh_Hans;
MM_EXPORT_NWSI extern const char* mmFontsSource_FontsLanguage_zh_Hant;
MM_EXPORT_NWSI extern const char* mmFontsSource_FontsLanguage_ja;
MM_EXPORT_NWSI extern const char* mmFontsSource_FontsLanguage_ko;

struct mmFontsSource
{
    struct mmRbtreeStringVpt hCreatorMap;
    struct mmRbtreeStringVpt hLangs;
    struct mmRbtreeStringVpt hFontsDocuments;
    struct mmListString hFontsDirectorys;
    
    struct mmString hDefaultEtc;
    struct mmString hDefaultLanguage;
    struct mmString hDefaultLanguageSourceFile;
    struct mmString hDefaultLanguageSourceFonts;
    
    // weak ref.
    struct mmFileContext* pFileContext;
};

MM_EXPORT_NWSI void mmFontsSource_Init(struct mmFontsSource* p);
MM_EXPORT_NWSI void mmFontsSource_Destroy(struct mmFontsSource* p);

MM_EXPORT_NWSI void mmFontsSource_SetFileContext(struct mmFontsSource* p, struct mmFileContext* _pFileContext);

MM_EXPORT_NWSI void mmFontsSource_SetDefaultEtc(struct mmFontsSource* p, const char* _path);
MM_EXPORT_NWSI void mmFontsSource_SetDefaultLanguage(struct mmFontsSource* p, const char* _language);

MM_EXPORT_NWSI void mmFontsSource_SetDefaultLanguageSourceFile(struct mmFontsSource* p, const char* _file);
MM_EXPORT_NWSI void mmFontsSource_SetDefaultLanguageSourceFonts(struct mmFontsSource* p, const char* _path);

MM_EXPORT_NWSI void mmFontsSource_OnFinishLaunching(struct mmFontsSource* p);
MM_EXPORT_NWSI void mmFontsSource_OnBeforeTerminate(struct mmFontsSource* p);

MM_EXPORT_NWSI void mmFontsSource_Register(struct mmFontsSource* p, const char* _fileName, const struct mmFontsDocumentCreator* _pCreator);
MM_EXPORT_NWSI void mmFontsSource_Unregister(struct mmFontsSource* p, const char* _fileName);

MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_Produce(struct mmFontsSource* p, const char* _fileName);
MM_EXPORT_NWSI void mmFontsSource_Recycle(struct mmFontsSource* p, const char* _fileName, struct mmFontsDocumentBase* u);

MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_AddFontsDocument(struct mmFontsSource* p, const char* _fileName);
MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_GetFontsDocument(struct mmFontsSource* p, const char* _fileName);
MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_GetFontsDocumentInstance(struct mmFontsSource* p, const char* _fileName);
MM_EXPORT_NWSI void mmFontsSource_RmvFontsDocument(struct mmFontsSource* p, const char* _fileName);
MM_EXPORT_NWSI void mmFontsSource_ClearFontsDocument(struct mmFontsSource* p);

MM_EXPORT_NWSI struct mmVectorValue* mmFontsSource_AddSources(struct mmFontsSource* p, const char* lang);
MM_EXPORT_NWSI struct mmVectorValue* mmFontsSource_GetSources(struct mmFontsSource* p, const char* lang);
MM_EXPORT_NWSI struct mmVectorValue* mmFontsSource_GetSourcesInstance(struct mmFontsSource* p, const char* lang);
MM_EXPORT_NWSI void mmFontsSource_ClearSources(struct mmFontsSource* p);

MM_EXPORT_NWSI void mmFontsSource_AddSource(struct mmFontsSource* p, const char* lang, struct mmFontsData* source);
MM_EXPORT_NWSI struct mmFontsData* mmFontsSource_GetSource(struct mmFontsSource* p, const char* lang, size_t index);

MM_EXPORT_NWSI struct mmFontsData* mmFontsSource_GetSourceDefault(struct mmFontsSource* p, const char* lang);

MM_EXPORT_NWSI void mmFontsSource_AddDefaultFonts(struct mmFontsSource* p, const char* path);
MM_EXPORT_NWSI void mmFontsSource_RmvDefaultFonts(struct mmFontsSource* p, const char* path);
MM_EXPORT_NWSI void mmFontsSource_ClearDefaultFonts(struct mmFontsSource* p);
MM_EXPORT_NWSI const struct mmListString* mmFontsSource_GetDefaultFonts(struct mmFontsSource* p);

MM_EXPORT_NWSI void mmFontsSource_AnalysisFontsDocumentFiles(struct mmFontsSource* p);
MM_EXPORT_NWSI void mmFontsSource_AnalysisFontsDefault(struct mmFontsSource* p);

#include "core/mmSuffix.h"

#endif//__mmFontsSource_h__
