/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmClipboard.h"

static 
void 
__static_mmClipboardProvider_SetByteBuffer(
    struct mmClipboardProvider*                    p, 
    const char*                                    pMimeType, 
    struct mmByteBuffer*                           pByteBuffer)
{

}

static 
void 
__static_mmClipboardProvider_GetByteBuffer(
    struct mmClipboardProvider*                    p, 
    const char*                                    pMimeType, 
    struct mmByteBuffer*                           pByteBuffer)
{

}

MM_EXPORT_NWSI void mmClipboardProvider_Init(struct mmClipboardProvider* p)
{
    p->SetByteBuffer = &__static_mmClipboardProvider_SetByteBuffer;
    p->GetByteBuffer = &__static_mmClipboardProvider_GetByteBuffer;
}
MM_EXPORT_NWSI void mmClipboardProvider_Destroy(struct mmClipboardProvider* p)
{
    p->SetByteBuffer = &__static_mmClipboardProvider_SetByteBuffer;
    p->GetByteBuffer = &__static_mmClipboardProvider_GetByteBuffer;
}

MM_EXPORT_NWSI void mmClipboard_Init(struct mmClipboard* p)
{
    p->pProvider = NULL;
}
MM_EXPORT_NWSI void mmClipboard_Destroy(struct mmClipboard* p)
{
    p->pProvider = NULL;
}
MM_EXPORT_NWSI void mmClipboard_SetProvider(struct mmClipboard* p, struct mmClipboardProvider* pProvider)
{
    p->pProvider = pProvider;
}
MM_EXPORT_NWSI void mmClipboard_SetBytes(struct mmClipboard* p, const char* mimeType, struct mmByteBuffer* bytes)
{
    if (NULL != p->pProvider)
    {
        (*(p->pProvider->SetByteBuffer))(p->pProvider, mimeType, bytes);
    }
}
MM_EXPORT_NWSI void mmClipboard_GetBytes(struct mmClipboard* p, const char* mimeType, struct mmByteBuffer* bytes)
{
    if (NULL != p->pProvider)
    {
        (*(p->pProvider->GetByteBuffer))(p->pProvider, mimeType, bytes);
    }
}
MM_EXPORT_NWSI void mmClipboard_SetText(struct mmClipboard* p, struct mmUtf16String* text)
{
    if (NULL != p->pProvider)
    {
        struct mmByteBuffer bytes;
        bytes.buffer = (mmUInt8_t*)mmUtf16String_Data(text);
        bytes.offset = 0;
        bytes.length = (size_t)mmUtf16String_Size(text) * sizeof(mmUInt16_t);
        (*(p->pProvider->SetByteBuffer))(p->pProvider, "text/plain", &bytes);
    }
}
MM_EXPORT_NWSI void mmClipboard_GetText(struct mmClipboard* p, struct mmUtf16String* text)
{
    if (NULL != p->pProvider)
    {
        const mmUInt16_t* s = NULL;
        size_t n = 0;
        struct mmByteBuffer bytes;
        (*(p->pProvider->GetByteBuffer))(p->pProvider, "text/plain", &bytes);
        s = (const mmUInt16_t*)(bytes.buffer + bytes.offset);
        n = bytes.length / sizeof(mmUInt16_t);
        mmUtf16String_MakeWeaksn(text, s, n);
    }
}