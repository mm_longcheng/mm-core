/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextParagraphFormat.h"

#include "core/mmAlloc.h"

#include "nwsi/mmTextParagraphFormatNative.h"

#include "math/mmVector2.h"
#include "math/mmVector4.h"

#include "graphics/mmColor.h"
#include "graphics/mmGeometry.h"

MM_EXPORT_NWSI
void
mmTextParagraphFormat_Init(
    struct mmTextParagraphFormat*                  p)
{
    mmTextFormat_Init(&p->hMasterFormat);
    mmRbtreeU32U32_Init(&p->hFontFeatures);
    p->pObject = NULL;

    mmTextParagraphFormat_NativeInit(p);

    mmTextParagraphFormat_Reset(p);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_Destroy(
    struct mmTextParagraphFormat*                  p)
{
    mmTextParagraphFormat_Reset(p);

    mmTextParagraphFormat_NativeDestroy(p);

    p->pObject = NULL;
    mmTextFormat_Destroy(&p->hMasterFormat);
    mmRbtreeU32U32_Destroy(&p->hFontFeatures);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_Reset(
    struct mmTextParagraphFormat*                  p)
{
    mmTextFormat_Reset(&p->hMasterFormat);
    mmRbtreeU32U32_Clear(&p->hFontFeatures);

    p->hTextAlignment = mmTextAlignmentNatural;
    p->hParagraphAlignment = mmTextParagraphAlignmentTop;
    p->hLineBreakMode = mmTextLineBreakByWordWrapping;
    p->hWritingDirection = mmTextWritingDirectionNatural;

    p->hFirstLineHeadIndent = 0.0f;
    p->hHeadIndent = 0.0f;
    p->hTailIndent = 0.0f;
    p->hLineSpacingAddition = 0.0f;
    p->hLineSpacingMultiple = 1.0f;

    mmVec2Make(p->hShadowOffset, 0.0f, 0.0f);
    p->hShadowBlur = 0.0f;
    mmColorMake(p->hShadowColor, 0.0f, 0.0f, 0.0f, 1.0f);

    p->hLineCap = mmLineCapButt;
    p->hLineJoin = mmLineJoinMiter;
    p->hLineMiterLimit = 10.0f;

    p->hAlignBaseline = 0;
    p->hLineDescent = 0.0f;
    mmVec2Make(p->hSuitableSize, 0.0f, 0.0f);

    mmVec2Make(p->hWindowSize, 40.0f, 20.0f);
    mmVec4Make(p->hLayoutRect, 0.0f, 0.0f, 40.0f, 20.0f);
    
    p->hDisplayDensity = 1.0f;
    
    p->hCaretIndex = 0;
    p->hSelection[0] = 0;
    p->hSelection[1] = 0;
    p->hCaretWrapping = 0;
    p->hCaretStatus = 0;
    p->hSelectionStatus = 0;
    p->hCaretWidth = 1.0f;
    mmColorMake(p->hCaretColor, 1.0f, 1.0f, 1.0f, 1.0f);
    mmColorFromARGB(p->hSelectionCaptureColor, 0xFF4080FF);
    mmColorFromARGB(p->hSelectionReleaseColor, 0xFFDCDCDC);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_SetObject(
    struct mmTextParagraphFormat*                  p,
    void*                                          pObject)
{
    p->pObject = pObject;
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_SetMasterFormat(
    struct mmTextParagraphFormat*                  p,
    const struct mmTextFormat*                     hFormat)
{
    mmTextFormat_Assign(&p->hMasterFormat, hFormat);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_SetFontFeature(
    struct mmTextParagraphFormat*                  p,
    mmUInt32_t                                     hTag,
    mmUInt32_t                                     hParameter)
{
    mmRbtreeU32U32_Set(&p->hFontFeatures, hTag, hParameter);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_RmvFontFeature(
    struct mmTextParagraphFormat*                  p,
    mmUInt32_t                                     hTag)
{
    mmRbtreeU32U32_Rmv(&p->hFontFeatures, hTag);
}

MM_EXPORT_NWSI
mmUInt32_t
mmTextParagraphFormat_GetFontFeature(
    struct mmTextParagraphFormat*                  p,
    mmUInt32_t                                     hTag)
{
    return *mmRbtreeU32U32_Get(&p->hFontFeatures, hTag);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_ClearFontFeature(
    struct mmTextParagraphFormat*                  p)
{
    mmRbtreeU32U32_Clear(&p->hFontFeatures);
}

MM_EXPORT_NWSI
void
mmTextHitMetrics_Reset(
    struct mmTextHitMetrics*                       p)
{
    mmMemset(p, 0, sizeof(struct mmTextHitMetrics));
}
