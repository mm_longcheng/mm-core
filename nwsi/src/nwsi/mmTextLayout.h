/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextLayout_h__
#define __mmTextLayout_h__

#include "core/mmCore.h"

#include "nwsi/mmTextFormat.h"
#include "nwsi/mmTextParagraphFormat.h"
#include "nwsi/mmTextSpannable.h"
#include "nwsi/mmTextUtf16String.h"
#include "nwsi/mmTextParagraph.h"
#include "nwsi/mmTextRenderTarget.h"
#include "nwsi/mmTextDrawingContext.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmGraphicsFactory;

struct mmTextLayout
{
    struct mmTextParagraphFormat hParagraphFormat;
    struct mmTextSpannable hTextSpannable;
    struct mmTextUtf16String hText;
    struct mmTextParagraph hTextParagraph;
    struct mmTextRenderTarget hTextRenderTarget;
    struct mmTextDrawingContext hDrawingContext;
    struct mmGraphicsFactory* pGraphicsFactory;
};

MM_EXPORT_NWSI 
void 
mmTextLayout_Init(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_Destroy(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetGraphicsFactory(
    struct mmTextLayout*                           p, 
    struct mmGraphicsFactory*                      pGraphicsFactory);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterFormat(
    struct mmTextLayout*                           p, 
    const struct mmTextFormat*                     pMasterFormat);

/*
 * @brief String Api.
 */

MM_EXPORT_NWSI 
void 
mmTextLayout_SetText(
    struct mmTextLayout*                           p, 
    const struct mmUtf16String*                    str);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetTexts(
    struct mmTextLayout*                           p, 
    const mmUInt16_t*                              s);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetTextsn(
    struct mmTextLayout*                           p, 
    const mmUInt16_t*                              s, 
    size_t                                         n);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetTextWeak(
    struct mmTextLayout*                           p, 
    const struct mmUtf16String*                    str);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetTextWeaks(
    struct mmTextLayout*                           p, 
    const mmUInt16_t*                              s);

MM_EXPORT_NWSI 
void 
mmTextLayout_SetTextWeaksn(
    struct mmTextLayout*                           p, 
    const mmUInt16_t*                              s, 
    size_t                                         n);

/*
 * @brief Range Api.
 */

// default "Helvetica"
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeFontFamilyName(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    const mmUInt16_t*                              pFontFamilyName);

// default 12.0
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeFontSize(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    float                                          hFontSize);

// mmFontStyle_t default is mmFontStyleNormal.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeFontStyle(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    int                                            hFontStyle);

// default is mmFontWeightNormal bold is mmFontWeightBold.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeFontWeight(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    int                                            hFontWeight);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeForegroundColor(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    mmUInt32_t                                     hForegroundColor);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 0.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeBackgroundColor(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    mmUInt32_t                                     hBackgroundColor);

// -3 fill and Stroke 3. default 0, not Stroke.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeStrokeWidth(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    float                                          hStrokeWidth);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeStrokeColor(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    mmUInt32_t                                     hStrokeColor);

// 0 is not underline. -1 is default Width.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeUnderlineWidth(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    float                                          hUnderlineWidth);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeUnderlineColor(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    mmUInt32_t                                     hUnderlineColor);

// 0 is not strikethru. -1 is default Width.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeStrikeThruWidth(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    float                                          hStrikeThruWidth);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetRangeStrikeThruColor(
    struct mmTextLayout*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    mmUInt32_t                                     hStrikeThruColor);

/*
 * @brief Paragraph Api.
 */

// default Natural
MM_EXPORT_NWSI 
void 
mmTextLayout_SetParagraphTextAlignment(
    struct mmTextLayout*                           p, 
    int                                            hTextAlignment);

// default Top
MM_EXPORT_NWSI 
void 
mmTextLayout_SetParagraphAlignment(
    struct mmTextLayout*                           p, 
    int                                            hParagraphAlignment);

// default BreakByWordWrapping.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLineBreakMode(
    struct mmTextLayout*                           p, 
    int                                            hLineBreakMode);

// default WritingDirectionNatural.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetWritingDirection(
    struct mmTextLayout*                           p, 
    int                                            hWritingDirection);

// default 0.0f.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetFirstLineHeadIndent(
    struct mmTextLayout*                           p, 
    float                                          hFirstLineHeadIndent);

// default 0.0f.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetHeadIndent(
    struct mmTextLayout*                           p, 
    float                                          hHeadIndent);

// default 0.0f.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetTailIndent(
    struct mmTextLayout*                           p, 
    float                                          hTailIndent);

// default 0.0f.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLineSpacingAddition(
    struct mmTextLayout*                           p, 
    float                                          hLineSpacingAddition);

// default 1.0f.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLineSpacingMultiple(
    struct mmTextLayout*                           p, 
    float                                          hLineSpacingMultiple);

/*- Shadow -*/
// (x, y) default (0.0f, 0.0f)
MM_EXPORT_NWSI 
void 
mmTextLayout_SetShadowOffset(
    struct mmTextLayout*                           p, 
    float                                          hShadowOffsetX, 
    float                                          hShadowOffsetY);

// default 0, not shadow, 3 generally.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetShadowBlur(
    struct mmTextLayout*                           p, 
    float                                          hShadowBlur);

// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetShadowColor(
    struct mmTextLayout*                           p, 
    mmUInt32_t                                     hShadowColor);

// default LineCapButt.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLineCap(
    struct mmTextLayout*                           p, 
    int                                            hLineCap);

// default LineJoinMiter
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLineJoin(
    struct mmTextLayout*                           p, 
    int                                            hLineJoin);

// default is 10.0
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLineMiterLimit(
    struct mmTextLayout*                           p, 
    float                                          hLineMiterLimit);

// TextWindow constraint dimensions box.
// window size.
// (w, h) default { FLT_MAX, FLT_MAX, },
MM_EXPORT_NWSI 
void 
mmTextLayout_SetWindowSize(
    struct mmTextLayout*                           p, 
    float                                          hWindowSizeW, 
    float                                          hWindowSizeH);

// TextLayout constraint dimensions box.
// layout rect.
// (x, y, w, h) default { -FLT_MAX, -FLT_MAX, FLT_MAX, FLT_MAX, },
MM_EXPORT_NWSI 
void 
mmTextLayout_SetLayoutRect(
    struct mmTextLayout*                           p, 
    float                                          x, 
    float                                          y, 
    float                                          w, 
    float                                          h);

// font display density. default 1.0.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetDisplayDensity(
    struct mmTextLayout*                           p, 
    float                                          hDisplayDensity);

/*
 * @brief EditBox Api.
 */

// caret index. default 0.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretIndex(
    struct mmTextLayout*                           p,
    size_t                                         hCaretIndex);

// selection range [l, r]. default [0, 0].
MM_EXPORT_NWSI
void
mmTextLayout_SetSelection(
    struct mmTextLayout*                           p,
    size_t                                         l,
    size_t                                         r);

// caret wrapping. 0 not 1 wrapping. default 0.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretWrapping(
    struct mmTextLayout*                           p,
    int                                            hCaretWrapping);

// caret status. 0 hide 1 show.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretStatus(
    struct mmTextLayout*                           p,
    int                                            hCaretStatus);

// selection status. 0 release 1 capture.
MM_EXPORT_NWSI
void
mmTextLayout_SetSelectionStatus(
    struct mmTextLayout*                           p,
    int                                            hSelectionStatus);

// caret width. default 1.
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretWidth(
    struct mmTextLayout*                           p,
    float                                          hCaretWidth);

// default rgba { 1.0f, 1.0f, 1.0f, 1.0f, },
MM_EXPORT_NWSI
void
mmTextLayout_SetCaretColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hCaretColor);

// capture color. default ARGB #FF4080FF ( 64, 128, 255).
MM_EXPORT_NWSI
void
mmTextLayout_SetSelectionCaptureColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hSelectionCaptureColor);

// release color. default ARGB #FFDCDCDC (220, 220, 220).
MM_EXPORT_NWSI
void
mmTextLayout_SetSelectionReleaseColor(
    struct mmTextLayout*                           p,
    mmUInt32_t                                     hSelectionReleaseColor);

/*
 * @brief MasterFormat Api.
 */

// default "Helvetica"
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterFontFamilyName(
    struct mmTextLayout*                           p, 
    const mmUInt16_t*                              pFontFamilyName);

// default 12.0
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterFontSize(
    struct mmTextLayout*                           p, 
    float                                          hFontSize);

// mmFontStyle_t default is mmFontStyleNormal.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterFontStyle(
    struct mmTextLayout*                           p, 
    int                                            hFontStyle);

// default is mmFontWeightNormal bold is mmFontWeightBold.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterFontWeight(
    struct mmTextLayout*                           p, 
    int                                            hFontWeight);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterForegroundColor(
    struct mmTextLayout*                           p, 
    mmUInt32_t                                     hForegroundColor);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 0.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterBackgroundColor(
    struct mmTextLayout*                           p, 
    mmUInt32_t                                     hBackgroundColor);

// -3 fill and Stroke 3. default 0, not Stroke.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterStrokeWidth(
    struct mmTextLayout*                           p, 
    float                                          hStrokeWidth);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterStrokeColor(
    struct mmTextLayout*                           p, 
    mmUInt32_t                                     hStrokeColor);

// 0 is not underline. -1 is default Width.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterUnderlineWidth(
    struct mmTextLayout*                           p, 
    float                                          hUnderlineWidth);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterUnderlineColor(
    struct mmTextLayout*                           p, 
    mmUInt32_t                                     hUnderlineColor);

// 0 is not strikethru. -1 is default Width.
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterStrikeThruWidth(
    struct mmTextLayout*                           p, 
    float                                          hStrikeThruWidth);

// 0xAARRGGBB
// default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
MM_EXPORT_NWSI 
void 
mmTextLayout_SetMasterStrikeThruColor(
    struct mmTextLayout*                           p, 
    mmUInt32_t                                     hStrikeThruColor);

// set Master Parameter reference pointer.
MM_EXPORT_NWSI
void
mmTextLayout_SetMasterParameter(
    struct mmTextLayout*                           p,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v);

// set Range Parameter reference pointer.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeParameter(
    struct mmTextLayout*                           p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v);

// set Range Property reference pointer.
MM_EXPORT_NWSI
void
mmTextLayout_SetRangeProperty(
    struct mmTextLayout*                           p,
    const struct mmRange*                          r,
    enum mmTextProperty_t                          t,
    const void*                                    v);

MM_EXPORT_NWSI
void
mmTextLayout_ToggleCaretStatus(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI
void
mmTextLayout_HandleSelection(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_OnFinishLaunching(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_OnBeforeTerminate(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI
void
mmTextLayout_SpannableReset(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_UpdateParagraph(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI
void
mmTextLayout_UpdateTextRange(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI
void
mmTextLayout_UpdateText(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_UpdateSize(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_Draw(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
const void* 
mmTextLayout_BufferLock(
    struct mmTextLayout*                           p);

MM_EXPORT_NWSI 
void 
mmTextLayout_BufferUnLock(
    struct mmTextLayout*                           p, 
    const void*                                    pBuffer);

MM_EXPORT_NWSI
void
mmTextLayout_HitTestPoint(
    const struct mmTextLayout*                     p,
    float                                          point[2],
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics);

MM_EXPORT_NWSI
void
mmTextLayout_HitTestTextPosition(
    const struct mmTextLayout*                     p,
    mmUInt32_t                                     offset,
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics);

#include "core/mmSuffix.h"

#endif//__mmTextLayout_h__
