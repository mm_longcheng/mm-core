/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextSpannable_h__
#define __mmTextSpannable_h__

#include "mmTextFormat.h"

#include "math/mmRange.h"

#include "container/mmListVpt.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmTextSpan
{
    struct mmRange hRange;
    struct mmTextFormat hFormat;
};

MM_EXPORT_NWSI 
void 
mmTextSpan_Init(
    struct mmTextSpan*                             p);

MM_EXPORT_NWSI 
void 
mmTextSpan_Destroy(
    struct mmTextSpan*                             p);

MM_EXPORT_NWSI 
void 
mmTextSpan_Reset(
    struct mmTextSpan*                             p);

MM_EXPORT_NWSI 
void 
mmTextSpan_Assign(
    struct mmTextSpan*                             p,
    const struct mmTextSpan*                       q);

struct mmTextSpannable
{
    const struct mmTextFormat* pMasterFormat;
    struct mmRange hMasterRange;
    struct mmListVpt hParameters;
    struct mmListVpt hAttributes;
    size_t hParameterNumber;
    // object for platform implement.
    void* pObject;
};

MM_EXPORT_NWSI 
void 
mmTextSpannable_Init(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_Destroy(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_Clear(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_SetObject(
    struct mmTextSpannable*                        p, 
    void*                                          pObject);

MM_EXPORT_NWSI 
void 
mmTextSpannable_SetMasterFormat(
    struct mmTextSpannable*                        p, 
    const struct mmTextFormat*                     pMasterFormat);

MM_EXPORT_NWSI
void
mmTextSpannable_SetMasterRange(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          pRange);

// set value to Parameters
// r range
// o mmTextFormat member offset mmOffsetof(struct mmTextFormat, <member>)
// l size for value
// v pointer for value
MM_EXPORT_NWSI
void
mmTextSpannable_SetParameter(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v);

// set value to Parameters
// r range
// t enum mmTextFormatProperty_t type 
// v pointer for value
MM_EXPORT_NWSI
void
mmTextSpannable_SetProperty(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          r,
    enum mmTextProperty_t                          t,
    const void*                                    v);

// set value to Attributes
// r range
// o mmTextFormat member offset mmOffsetof(struct mmTextFormat, <member>)
// l size for value
// v pointer for value
MM_EXPORT_NWSI
void
mmTextSpannable_SetAttribute(
    struct mmTextSpannable*                        p,
    const struct mmRange*                          r,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v);

MM_EXPORT_NWSI
void
mmTextSpannable_GetMaxFontSize(
    const struct mmTextSpannable*                  p,
    const struct mmRange*                          r,
    float*                                         pMaxFontSize);

MM_EXPORT_NWSI
void
mmTextSpannable_UpdateAttributes(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI
void
mmTextSpannable_ResetParameters(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_Simplify(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
struct mmTextSpan* 
mmTextSpannable_SpanProduce(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_SpanRecycle(
    struct mmTextSpannable*                        p, 
    struct mmTextSpan*                             e);

#include "core/mmSuffix.h"

#endif//__mmTextSpannable_h__
