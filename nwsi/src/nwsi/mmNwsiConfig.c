/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNwsiConfig.h"

#include "core/mmValueTransform.h"

#include "parse/mmParseINI.h"

#include "dish/mmFileContext.h"

MM_EXPORT_NWSI void mmNwsiConfig_Init(struct mmNwsiConfig* p)
{
    mmString_Init(&p->hConfigPath);
    p->pFileContext = NULL;
    p->DesignDensity = 2.0;
    p->AbsoluteDensity = 1.0;
    p->DesignCanvasSize[0] = 1080.0;
    p->DesignCanvasSize[1] = 1920.0;

    mmString_Assigns(&p->hConfigPath, "config/mm-nwsi.config");
}
MM_EXPORT_NWSI void mmNwsiConfig_Destroy(struct mmNwsiConfig* p)
{
    mmString_Destroy(&p->hConfigPath);
    p->pFileContext = NULL;
    p->DesignDensity = 2.0;
    p->AbsoluteDensity = 1.0;
    p->DesignCanvasSize[0] = 1080.0;
    p->DesignCanvasSize[1] = 1920.0;
}

MM_EXPORT_NWSI void mmNwsiConfig_SetFileContext(struct mmNwsiConfig* p, struct mmFileContext* pFileContext)
{
    p->pFileContext = pFileContext;
}

MM_EXPORT_NWSI void mmNwsiConfig_SetConfigPath(struct mmNwsiConfig* p, const char* pConfigPath)
{
    mmString_Assigns(&p->hConfigPath, pConfigPath);
}

MM_EXPORT_NWSI void mmNwsiConfig_Analysis(struct mmNwsiConfig* p)
{
    mmUInt8_t* buffer = NULL;
    size_t length = 0;

    const char* pValue = NULL;

    struct mmByteBuffer hByteBuffer;

    struct mmParseINI hINI;

    mmByteBuffer_Init(&hByteBuffer);
    mmParseINI_Init(&hINI);

    mmFileContext_AcquireFileByteBuffer(p->pFileContext, mmString_CStr(&p->hConfigPath), &hByteBuffer);
    buffer = hByteBuffer.buffer + hByteBuffer.offset;
    length = hByteBuffer.length;

    mmParseINI_AnalysisBuffer(&hINI, buffer, length);

    pValue = mmParseINI_GetValue(&hINI, "DesignDensity");
    mmValue_StringToDouble(pValue, &p->DesignDensity);

    pValue = mmParseINI_GetValue(&hINI, "AbsoluteDensity");
    mmValue_StringToDouble(pValue, &p->AbsoluteDensity);

    pValue = mmParseINI_GetValue(&hINI, "DesignCanvasSize.w");
    mmValue_StringToDouble(pValue, &p->DesignCanvasSize[0]);
    pValue = mmParseINI_GetValue(&hINI, "DesignCanvasSize.h");
    mmValue_StringToDouble(pValue, &p->DesignCanvasSize[1]);

    mmFileContext_ReleaseFileByteBuffer(p->pFileContext, &hByteBuffer);

    mmParseINI_Destroy(&hINI);
    mmByteBuffer_Destroy(&hByteBuffer);
}

MM_EXPORT_NWSI void mmNwsiConfig_OnFinishLaunching(struct mmNwsiConfig* p)
{
    mmNwsiConfig_Analysis(p);
}
MM_EXPORT_NWSI void mmNwsiConfig_OnBeforeTerminate(struct mmNwsiConfig* p)
{

}