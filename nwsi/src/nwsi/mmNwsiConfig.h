/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNwsiConfig_h__
#define __mmNwsiConfig_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmFileContext;

struct mmNwsiConfig
{
    // config path. "config/mm-nwsi.config" default.
    struct mmString hConfigPath;

    // file context.
    struct mmFileContext* pFileContext;

    // value.

    // Design density, default is 1.0.
    double DesignDensity;

    // Auto layout absolute density, default is 1.0.
    double AbsoluteDensity;

    // Design logical canvas size(Design Pixel).
    // (w, h) default is (378.0, 672.0).
    double DesignCanvasSize[2];
};

MM_EXPORT_NWSI void mmNwsiConfig_Init(struct mmNwsiConfig* p);
MM_EXPORT_NWSI void mmNwsiConfig_Destroy(struct mmNwsiConfig* p);

MM_EXPORT_NWSI void mmNwsiConfig_SetFileContext(struct mmNwsiConfig* p, struct mmFileContext* pFileContext);
// Default is "config/mm-nwsi.config".
MM_EXPORT_NWSI void mmNwsiConfig_SetConfigPath(struct mmNwsiConfig* p, const char* pConfigPath);

MM_EXPORT_NWSI void mmNwsiConfig_Analysis(struct mmNwsiConfig* p);

MM_EXPORT_NWSI void mmNwsiConfig_OnFinishLaunching(struct mmNwsiConfig* p);
MM_EXPORT_NWSI void mmNwsiConfig_OnBeforeTerminate(struct mmNwsiConfig* p);

#include "core/mmSuffix.h"

#endif//__mmNwsiConfig_h__
