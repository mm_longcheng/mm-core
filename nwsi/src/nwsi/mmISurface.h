/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmISurface_h__
#define __mmISurface_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

// Coordinate system
//     origin At left top
//     x      Positive axis lift to right
//     y      Positive axis top  to bottom

// Drag content.
struct mmEventDragging
{
    // The url for drag vector.
    struct mmVectorValue context;

    // The point for drop.
    // Point(x, y)
    double point[2];
};
MM_EXPORT_NWSI void mmEventDragging_Init(struct mmEventDragging* p);
MM_EXPORT_NWSI void mmEventDragging_Destroy(struct mmEventDragging* p);
MM_EXPORT_NWSI void mmEventDragging_Reset(struct mmEventDragging* p);
MM_EXPORT_NWSI void mmEventDragging_AlignedMemory(struct mmEventDragging* p, size_t size);
MM_EXPORT_NWSI void mmEventDragging_SetForIndex(struct mmEventDragging* p, size_t index, const char* url);
MM_EXPORT_NWSI void mmEventDragging_GetForIndex(struct mmEventDragging* p, size_t index, struct mmString* url);
MM_EXPORT_NWSI struct mmString* mmEventDragging_GetForIndexReference(struct mmEventDragging* p, size_t index);
MM_EXPORT_NWSI void mmEventDragging_SetPoint(struct mmEventDragging* p, double x, double y);

enum mmActiveStatusType
{
    // Background.
    mmActiveStatusBackground = 0,
    // Foreground.
    mmActiveStatusForeground = 1,
};
struct mmEventActiveStatus
{
    // Status type. enum mmActiveStatusType.
    mmUInt32_t state;
};
MM_EXPORT_NWSI void mmEventActiveStatus_Init(struct mmEventActiveStatus* p);
MM_EXPORT_NWSI void mmEventActiveStatus_Destroy(struct mmEventActiveStatus* p);
MM_EXPORT_NWSI void mmEventActiveStatus_Reset(struct mmEventActiveStatus* p);

// Keypad content.
struct mmEventKeypad
{
    /* modifier mask */
    mmUInt32_t modifier_mask;
    
    /* Key code. */
    int key;
    
    /* Text length. */
    int length;
    /* Text buffer. */
    mmUInt16_t text[4];
    
    /* Return value.
     *     0 Need to handle keyboard event.
     *     1 Event has been processed.
     */
    mmUInt32_t handle;
};
MM_EXPORT_NWSI void mmEventKeypad_Init(struct mmEventKeypad* p);
MM_EXPORT_NWSI void mmEventKeypad_Destroy(struct mmEventKeypad* p);
MM_EXPORT_NWSI void mmEventKeypad_Reset(struct mmEventKeypad* p);

struct mmEventCursor
{
    /* modifier mask */
    mmUInt32_t modifier_mask;
    /* button mask */
    mmUInt32_t button_mask;
    /* button id */
    mmUInt32_t button_id;
    /* Absolute position x */
    double abs_x;
    /* Absolute position y */
    double abs_y;
    /* Relative position x */
    double rel_x;
    /* Relative position y */
    double rel_y;
    /* wheel scrolling delta x */
    double wheel_dx;
    /* wheel scrolling delta y */
    double wheel_dy;
    /* The UTC time of this fix, in milliseconds since January 1, 1970.
     * The time (in ms) when event trigger.
     */
    mmUInt64_t timestamp;
    /* Return value.
     *     0 Need to handle keyboard pop up.
     *     1 Event has been processed.
     */
    mmUInt32_t handle;
};
MM_EXPORT_NWSI void mmEventCursor_Init(struct mmEventCursor* p);
MM_EXPORT_NWSI void mmEventCursor_Destroy(struct mmEventCursor* p);
MM_EXPORT_NWSI void mmEventCursor_Reset(struct mmEventCursor* p);

// Touch phase type.
enum mmTouchPhaseType
{
    // Touch began.
    mmTouchBegan = 0,
    // Touch move.
    mmTouchMoved = 1,
    // Touch end.
    mmTouchEnded = 2,
    // Touch break, cancel.
    mmTouchBreak = 3,
};

// Touch point.
struct mmEventTouch
{
    // Motion event weak ref.
    uintptr_t motion_id;
    // The number of times the finger was tapped for this given touch.
    int tap_count;
    // The phase of the touch.TouchPhase touch began, moved, ended, or was canceled.
    int phase;
    // modifier mask
    mmUInt32_t modifier_mask;
    // button mask
    mmUInt32_t button_mask;
    // Absolute position x.
    double abs_x;
    // Absolute position y.
    double abs_y;
    // Relative position x.
    double rel_x;
    // Relative position y.
    double rel_y;
    // The force of the touch, where a value of 1.0 represents the 
    // force of an average touch (predetermined by the system, not user-specific).
    double force_value;
    // The maximum possible force for a touch.
    double force_maximum;
    // The major radius (in points) of the touch.
    double major_radius;
    // The minor radius (in points) of the touch.
    double minor_radius;
    // The area size value, (0, 1) an abstract metric.
    double size_value;
    // The UTC time of this fix, in milliseconds since January 1, 1970.
    // The time (in ms) when event trigger.
    mmUInt64_t timestamp;
};
MM_EXPORT_NWSI void mmEventTouch_Init(struct mmEventTouch* p);
MM_EXPORT_NWSI void mmEventTouch_Destroy(struct mmEventTouch* p);
MM_EXPORT_NWSI void mmEventTouch_Reset(struct mmEventTouch* p);

// Touch point set.
struct mmEventTouchs
{
    // The touch vector.
    struct mmVectorValue context;
};
MM_EXPORT_NWSI void mmEventTouchs_Init(struct mmEventTouchs* p);
MM_EXPORT_NWSI void mmEventTouchs_Destroy(struct mmEventTouchs* p);
MM_EXPORT_NWSI void mmEventTouchs_Reset(struct mmEventTouchs* p);
MM_EXPORT_NWSI void mmEventTouchs_AlignedMemory(struct mmEventTouchs* p, size_t size);
MM_EXPORT_NWSI void mmEventTouchs_SetForIndex(struct mmEventTouchs* p, size_t index, struct mmEventTouch* touch);
MM_EXPORT_NWSI void mmEventTouchs_GetForIndex(struct mmEventTouchs* p, size_t index, struct mmEventTouch* touch);
MM_EXPORT_NWSI struct mmEventTouch* mmEventTouchs_GetForIndexReference(struct mmEventTouchs* p, size_t index);

enum mmEditTextType
{
    mmEditTextWhole = (1u << 0u),// whole data is valid, size and capacity is invalid.
    mmEditTextRsize = (1u << 1u),// resize size data is valid.
    mmEditTextCsize = (1u << 2u),// capacity size data is valid.
    mmEditTextRange = (1u << 3u),// only range data is valid.
    mmEditTextCaret = (1u << 4u),// only caret data is valid.
    mmEditTextDirty = (1u << 5u),// text is dirty, only SetText valid.
};

// Text content data.
struct mmEventEditText
{
    // text utf16 character weak reference pointer.
    mmUInt16_t* t;
    // c string utf32 character size.
    size_t size;
    // c string utf32 character capacity.
    size_t capacity;
    // current cursor position.
    size_t c;
    // lift  Selection position.
    size_t l;
    // right Selection position.
    size_t r;
    // mmEventTextType.
    int v;
};
MM_EXPORT_NWSI void mmEventEditText_Init(struct mmEventEditText* p);
MM_EXPORT_NWSI void mmEventEditText_Destroy(struct mmEventEditText* p);
MM_EXPORT_NWSI void mmEventEditText_Reset(struct mmEventEditText* p);

// String content data.
struct mmEventEditString
{
    // string pointer.
    mmUInt16_t* s;
    
    // string lenght.
    size_t l;
    
    // Replace character offset. Replace API only.
    size_t o;
    
    // Replace character number. Replace API only.
    size_t n;
};
MM_EXPORT_NWSI void mmEventEditString_Init(struct mmEventEditString* p);
MM_EXPORT_NWSI void mmEventEditString_Destroy(struct mmEventEditString* p);
MM_EXPORT_NWSI void mmEventEditString_Reset(struct mmEventEditString* p);

enum mmKeypadStatusType
{
    mmKeypadStatusShow = 0,// show keypad soft input.
    mmKeypadStatusHide = 1,// hide keypad soft input.
};

enum mmKeypadAnimationCurve
{
    mmKeypadAnimationCurveEaseInOut = 0,
    mmKeypadAnimationCurveEaseIn    = 1,
    mmKeypadAnimationCurveEaseOut   = 2,
    mmKeypadAnimationCurveLinear    = 3,
};

struct mmEventKeypadStatus
{
    // The surface pointer.
    void* surface;

    // Layer:
    //     screen <- window trigger window attach to screen.
    //     extern           extern screen brothers.
    //
    // Rect(x, y, w, h)
    double screen_rect[4];
    double safety_area[4];
    double keypad_rect[4];
    double window_rect[4];
    
    // Animation duration for this event.
    double animation_duration;
    // enum mmKeypadAnimationCurve.
    // Some time the value can be out of enum.
    int animation_curve;
    
    // Status type. mmSurfaceKeypadStatusType.
    mmUInt32_t state;
    
    // Return value.
    //     0 Need to handle keyboard occlusion.
    //     1 Event has been processed.
    mmUInt32_t handle;
};
MM_EXPORT_NWSI void mmEventKeypadStatus_Init(struct mmEventKeypadStatus* p);
MM_EXPORT_NWSI void mmEventKeypadStatus_Destroy(struct mmEventKeypadStatus* p);
MM_EXPORT_NWSI void mmEventKeypadStatus_Reset(struct mmEventKeypadStatus* p);

// Surface metrics.
struct mmEventMetrics
{
    // The surface pointer.
    void* surface;

    // canvas size.
    //
    // Size(w, h)
    double canvas_size[2];

    // window size.
    //
    // Size(w, h)
    double window_size[2];

    // safety area.
    //
    // Rect(x, y, w, h)
    double safety_area[4];
};
MM_EXPORT_NWSI void mmEventMetrics_Init(struct mmEventMetrics* p);
MM_EXPORT_NWSI void mmEventMetrics_Destroy(struct mmEventMetrics* p);
MM_EXPORT_NWSI void mmEventMetrics_Reset(struct mmEventMetrics* p);

// Surface frame update.
struct mmEventUpdate
{
    // frame number.
    mmUInt64_t number;

    // average fps.
    double average;

    // index [0x00, 0x3F].
    mmUInt8_t index;

    // Time for frame interval, second(s).
    double interval;
};
MM_EXPORT_NWSI void mmEventUpdate_Init(struct mmEventUpdate* p);
MM_EXPORT_NWSI void mmEventUpdate_Destroy(struct mmEventUpdate* p);
MM_EXPORT_NWSI void mmEventUpdate_Reset(struct mmEventUpdate* p);

struct mmEventSurfaceContent
{
    struct mmEventDragging hDragging;
    struct mmEventKeypad hKeypad;
    struct mmEventKeypadStatus hKeypadStatus;
    struct mmEventCursor hCursor;
    struct mmEventTouchs hTouchs;
    struct mmEventMetrics hMetrics;
    struct mmEventEditText hEditText;
    struct mmEventEditString hEditString;
};
MM_EXPORT_NWSI void mmEventSurfaceContent_Init(struct mmEventSurfaceContent* p);
MM_EXPORT_NWSI void mmEventSurfaceContent_Destroy(struct mmEventSurfaceContent* p);
MM_EXPORT_NWSI void mmEventSurfaceContent_Reset(struct mmEventSurfaceContent* p);

struct mmISurface;

enum mmKeypadType
{
    mmKeypadTypeDefault               =  0, /* Default for any.            */
    mmKeypadTypeASCIICapable          =  1, /* ASCII [A, z]                */
    mmKeypadTypeNumbersAndPunctuation =  2, /* [0, 9] [!, @, #, $, %, ...] */
    mmKeypadTypeUrl                   =  3, /* URL special ".com"          */
    mmKeypadTypeNumberPad             =  4, /* [0, 9]                      */
    mmKeypadTypePhonePad              =  5, /* [0, 9] [+, *, #]            */
    mmKeypadTypeNamePhonePad          =  6, /* [A, z]                      */
    mmKeypadTypeEmailAddress          =  7, /* [A, z] [@, .]               */
    mmKeypadTypeDecimalPad            =  8, /* [0, 9] [.]                  */
    mmKeypadTypeTwitter               =  9, /* Twitter style               */
    mmKeypadTypeWebSearch             = 10, /* [A, z] special Go action.   */
    mmKeypadTypeASCIICapableNumberPad = 11, /* [A, z] [0, 9]               */
};

enum mmKeypadAppearance
{
    mmKeypadAppearanceDefault         =  0, /* Default.    */
    mmKeypadAppearanceDark            =  1, /* Dark theme  */
    mmKeypadAppearanceLight           =  2, /* Light theme */
};

enum mmKeypadReturnKey
{
    mmKeypadReturnKeyDefault          =  0, /* Default.         */
    mmKeypadReturnKeyGo               =  1, /* Go               */
    mmKeypadReturnKeyGoogle           =  2, /* Google(Search)   */
    mmKeypadReturnKeyJoin             =  3, /* Join             */
    mmKeypadReturnKeyNext             =  4, /* Next             */
    mmKeypadReturnKeyRoute            =  5, /* Route            */
    mmKeypadReturnKeySearch           =  6, /* Search           */
    mmKeypadReturnKeySend             =  7, /* Send             */
    mmKeypadReturnKeyYahoo            =  8, /* Yahoo(Search)    */
    mmKeypadReturnKeyDone             =  9, /* Done             */
    mmKeypadReturnKeyEmergencyCall    = 10, /* EmergencyCall    */
    mmKeypadReturnKeyContinue         = 11, /* Continue         */
};

struct mmISurfaceKeypad
{
    void(*OnSetKeypadType)(struct mmISurface* pISurface, int hType);
    void(*OnSetKeypadAppearance)(struct mmISurface* pISurface, int hAppearance);
    void(*OnSetKeypadReturnKey)(struct mmISurface* pISurface, int hReturnKey);
    void(*OnKeypadStatusShow)(struct mmISurface* pISurface);
    void(*OnKeypadStatusHide)(struct mmISurface* pISurface);
};

MM_EXPORT_NWSI void mmISurfaceKeypad_Init(struct mmISurfaceKeypad* p);
MM_EXPORT_NWSI void mmISurfaceKeypad_Destroy(struct mmISurfaceKeypad* p);
MM_EXPORT_NWSI void mmISurfaceKeypad_Reset(struct mmISurfaceKeypad* p);

struct mmISurface
{
    void(*OnDragging)(struct mmISurface* p, struct mmEventDragging* content);

    void(*OnKeypadPressed)(struct mmISurface* p, struct mmEventKeypad* content);
    void(*OnKeypadRelease)(struct mmISurface* p, struct mmEventKeypad* content);
    void(*OnKeypadStatus)(struct mmISurface* p, struct mmEventKeypadStatus* content);

    void(*OnCursorBegan)(struct mmISurface* p, struct mmEventCursor* content);
    void(*OnCursorMoved)(struct mmISurface* p, struct mmEventCursor* content);    
    void(*OnCursorEnded)(struct mmISurface* p, struct mmEventCursor* content);
    void(*OnCursorBreak)(struct mmISurface* p, struct mmEventCursor* content);
    void(*OnCursorWheel)(struct mmISurface* p, struct mmEventCursor* content);

    void(*OnTouchsBegan)(struct mmISurface* p, struct mmEventTouchs* content);
    void(*OnTouchsMoved)(struct mmISurface* p, struct mmEventTouchs* content);
    void(*OnTouchsEnded)(struct mmISurface* p, struct mmEventTouchs* content);
    void(*OnTouchsBreak)(struct mmISurface* p, struct mmEventTouchs* content);

    void(*OnSurfacePrepare)(struct mmISurface* p, struct mmEventMetrics* content);
    void(*OnSurfaceDiscard)(struct mmISurface* p, struct mmEventMetrics* content);
    void(*OnSurfaceChanged)(struct mmISurface* p, struct mmEventMetrics* content);

    void(*OnGetText)(struct mmISurface* p, struct mmEventEditText* content);
    void(*OnSetText)(struct mmISurface* p, struct mmEventEditText* content);
    void(*OnGetTextEditRect)(struct mmISurface* p, double rect[4]);
    void(*OnInsertTextUtf16)(struct mmISurface* p, struct mmEventEditString* content);
    void(*OnReplaceTextUtf16)(struct mmISurface* p, struct mmEventEditString* content);
    
    void(*OnInjectCopy)(struct mmISurface* p);
    void(*OnInjectCut)(struct mmISurface* p);
    void(*OnInjectPaste)(struct mmISurface* p);
    
    void(*OnUpdate)(struct mmISurface* p);
    void(*OnUpdateImmediately)(struct mmISurface* p);
    void(*OnTimewait)(struct mmISurface* p);
    void(*OnTimewake)(struct mmISurface* p);
    
    void(*OnStart)(struct mmISurface* p);
    void(*OnInterrupt)(struct mmISurface* p);
    void(*OnShutdown)(struct mmISurface* p);
    void(*OnJoin)(struct mmISurface* p);
    
    void(*OnFinishLaunching)(struct mmISurface* p);
    void(*OnBeforeTerminate)(struct mmISurface* p);
    
    void(*OnEnterBackground)(struct mmISurface* p);
    void(*OnEnterForeground)(struct mmISurface* p);
};

MM_EXPORT_NWSI void mmISurface_Init(struct mmISurface* p);
MM_EXPORT_NWSI void mmISurface_Destroy(struct mmISurface* p);

#include "core/mmSuffix.h"

#endif//__mmISurface_h__
