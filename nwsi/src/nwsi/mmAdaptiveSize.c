#include "mmAdaptiveSize.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmLogger.h"
#include "core/mmStringUtils.h"

MM_EXPORT_NWSI void mmAdaptiveSize_Init(struct mmAdaptiveSize* p)
{
    p->hDisplayDensity = 1.0;

    p->hDisplayFrameSize[0] = 378.0;
    p->hDisplayFrameSize[1] = 672.0;

    p->hDesignDensity = 2.0;
    p->hAbsoluteDensity = 1.0;

    p->hDesignCanvasSize[0] = 720.0;
    p->hDesignCanvasSize[1] = 1280.0;

    p->hPhysicalViewSize[0] = 378.0;
    p->hPhysicalViewSize[1] = 672.0;

    p->hPhysicalSafeRect[0] = 0.0;
    p->hPhysicalSafeRect[1] = 0.0;
    p->hPhysicalSafeRect[2] = 378.0;
    p->hPhysicalSafeRect[3] = 672.0;

    p->hActuallyViewSize[0] = 720.0;
    p->hActuallyViewSize[1] = 1280.0;

    p->hActuallySafeRect[0] = 0.0;
    p->hActuallySafeRect[1] = 0.0;
    p->hActuallySafeRect[2] = 720.0;
    p->hActuallySafeRect[3] = 1280.0;

    p->hTransformCanvas = 1.0;
    p->hTransformWindow = 1.0;

    p->hViewSizeAspectRatio = 0.5625;
    p->hSafeRectAspectRatio = 0.5625;
}
MM_EXPORT_NWSI void mmAdaptiveSize_Destroy(struct mmAdaptiveSize* p)
{
    p->hDisplayDensity = 1.0;

    p->hDisplayFrameSize[0] = 378.0;
    p->hDisplayFrameSize[1] = 672.0;

    p->hDesignDensity = 2.0;
    p->hAbsoluteDensity = 1.0;

    p->hDesignCanvasSize[0] = 720.0;
    p->hDesignCanvasSize[1] = 1280.0;

    p->hPhysicalViewSize[0] = 378.0;
    p->hPhysicalViewSize[1] = 672.0;

    p->hPhysicalSafeRect[0] = 0.0;
    p->hPhysicalSafeRect[1] = 0.0;
    p->hPhysicalSafeRect[2] = 378.0;
    p->hPhysicalSafeRect[3] = 672.0;

    p->hActuallyViewSize[0] = 720.0;
    p->hActuallyViewSize[1] = 640.0;

    p->hActuallySafeRect[0] = 0.0;
    p->hActuallySafeRect[1] = 0.0;
    p->hActuallySafeRect[2] = 720.0;
    p->hActuallySafeRect[3] = 1280.0;

    p->hTransformCanvas = 1.0;
    p->hTransformWindow = 1.0;

    p->hViewSizeAspectRatio = 0.5625;
    p->hSafeRectAspectRatio = 0.5625;
}
MM_EXPORT_NWSI void mmAdaptiveSize_SetDisplayDensity(struct mmAdaptiveSize* p, double hDisplayDensity)
{
    p->hDisplayDensity = hDisplayDensity;
}
MM_EXPORT_NWSI void mmAdaptiveSize_SetAbsoluteDensity(struct mmAdaptiveSize* p, double hAbsoluteDensity)
{
    p->hAbsoluteDensity = hAbsoluteDensity;
}
MM_EXPORT_NWSI void mmAdaptiveSize_SetDisplayFrameSize(struct mmAdaptiveSize* p, double hDisplayFrameSize[2])
{
    p->hDisplayFrameSize[0] = hDisplayFrameSize[0];
    p->hDisplayFrameSize[1] = hDisplayFrameSize[1];
}

MM_EXPORT_NWSI void mmAdaptiveSize_SetDesignDensity(struct mmAdaptiveSize* p, double hDesignDensity)
{
    p->hDesignDensity = hDesignDensity;
}
MM_EXPORT_NWSI void mmAdaptiveSize_SetDesignCanvasSize(struct mmAdaptiveSize* p, double hDesignCanvasSize[2])
{
    p->hDesignCanvasSize[0] = hDesignCanvasSize[0];
    p->hDesignCanvasSize[1] = hDesignCanvasSize[1];
}

MM_EXPORT_NWSI void mmAdaptiveSize_SetPhysicalSafeRect(struct mmAdaptiveSize* p, double hPhysicalSafeRect[4])
{
    p->hPhysicalSafeRect[0] = hPhysicalSafeRect[0];
    p->hPhysicalSafeRect[1] = hPhysicalSafeRect[1];
    p->hPhysicalSafeRect[2] = hPhysicalSafeRect[2];
    p->hPhysicalSafeRect[3] = hPhysicalSafeRect[3];
}

MM_EXPORT_NWSI void mmAdaptiveSize_UpdatePhysicalViewSize(struct mmAdaptiveSize* p)
{
    p->hPhysicalViewSize[0] = p->hDisplayFrameSize[0] / p->hDisplayDensity;
    p->hPhysicalViewSize[1] = p->hDisplayFrameSize[1] / p->hDisplayDensity;
}

MM_EXPORT_NWSI void mmAdaptiveSize_AdjustBaseX(struct mmAdaptiveSize* p)
{
    // x base.

    double dw = p->hDesignCanvasSize[0] / p->hDesignDensity;
    double sx = dw / p->hPhysicalSafeRect[2];
    double sw = sx * p->hDesignDensity;

    p->hActuallyViewSize[0] = p->hPhysicalViewSize[0] * sw;
    p->hActuallyViewSize[1] = p->hPhysicalViewSize[1] * sw;

    p->hActuallySafeRect[0] = p->hPhysicalSafeRect[0] * sw;
    p->hActuallySafeRect[1] = p->hPhysicalSafeRect[1] * sw;
    p->hActuallySafeRect[2] = p->hPhysicalSafeRect[2] * sw;
    p->hActuallySafeRect[3] = p->hPhysicalSafeRect[3] * sw;

    p->hTransformCanvas = sw;
    p->hTransformWindow = p->hTransformCanvas / p->hDisplayDensity;

    p->hViewSizeAspectRatio = p->hActuallyViewSize[0] / p->hActuallyViewSize[1];
    p->hSafeRectAspectRatio = p->hActuallySafeRect[2] / p->hActuallySafeRect[3];
}
MM_EXPORT_NWSI void mmAdaptiveSize_AdjustBaseY(struct mmAdaptiveSize* p)
{
    // y base.

    double dh = p->hDesignCanvasSize[1] / p->hDesignDensity;
    double sy = dh / p->hPhysicalSafeRect[3];
    double sh = sy * p->hDesignDensity;

    p->hActuallyViewSize[0] = p->hPhysicalViewSize[0] * sh;
    p->hActuallyViewSize[1] = p->hPhysicalViewSize[1] * sh;

    p->hActuallySafeRect[0] = p->hPhysicalSafeRect[0] * sh;
    p->hActuallySafeRect[1] = p->hPhysicalSafeRect[1] * sh;
    p->hActuallySafeRect[2] = p->hPhysicalSafeRect[2] * sh;
    p->hActuallySafeRect[3] = p->hPhysicalSafeRect[3] * sh;

    p->hTransformCanvas = sh;
    p->hTransformWindow = p->hTransformCanvas / p->hDisplayDensity;

    p->hViewSizeAspectRatio = p->hActuallyViewSize[0] / p->hActuallyViewSize[1];
    p->hSafeRectAspectRatio = p->hActuallySafeRect[2] / p->hActuallySafeRect[3];
}

MM_EXPORT_NWSI void mmAdaptiveSize_AdjustAutoRelatively(struct mmAdaptiveSize* p)
{
    double dw = p->hDesignCanvasSize[0] / p->hDesignDensity;
    double sx = dw / p->hPhysicalSafeRect[2];
    double sw = sx * p->hDesignDensity;
    double jy = sw * p->hPhysicalSafeRect[3];

    if (jy > p->hDesignCanvasSize[1])
    {
        // x base.
        mmAdaptiveSize_AdjustBaseX(p);
    }
    else
    {
        // y base.
        mmAdaptiveSize_AdjustBaseY(p);
    }
}

MM_EXPORT_NWSI void mmAdaptiveSize_AdjustAutoAbsolute(struct mmAdaptiveSize* p)
{
    double dw = p->hDesignCanvasSize[0] / p->hAbsoluteDensity;
    double dh = p->hDesignCanvasSize[1] / p->hAbsoluteDensity;

    if (p->hPhysicalSafeRect[2] > dw && p->hPhysicalSafeRect[3] > dh)
    {
        p->hActuallyViewSize[0] = p->hPhysicalViewSize[0] * p->hDesignDensity;
        p->hActuallyViewSize[1] = p->hPhysicalViewSize[1] * p->hDesignDensity;

        p->hActuallySafeRect[0] = p->hPhysicalSafeRect[0] * p->hDesignDensity;
        p->hActuallySafeRect[1] = p->hPhysicalSafeRect[1] * p->hDesignDensity;
        p->hActuallySafeRect[2] = p->hPhysicalSafeRect[2] * p->hDesignDensity;
        p->hActuallySafeRect[3] = p->hPhysicalSafeRect[3] * p->hDesignDensity;

        p->hTransformCanvas = p->hDesignDensity;
        p->hTransformWindow = p->hTransformCanvas / p->hDisplayDensity;

        p->hViewSizeAspectRatio = p->hActuallyViewSize[0] / p->hActuallyViewSize[1];
        p->hSafeRectAspectRatio = p->hActuallySafeRect[2] / p->hActuallySafeRect[3];
    }
    else
    {
        mmAdaptiveSize_AdjustAutoRelatively(p);
    }
}

MM_EXPORT_NWSI void mmAdaptiveSize_OnFinishLaunching(struct mmAdaptiveSize* p)
{
    mmAdaptiveSize_UpdatePhysicalViewSize(p);
}
MM_EXPORT_NWSI void mmAdaptiveSize_OnBeforeTerminate(struct mmAdaptiveSize* p)
{
    // do nothing here.
}

MM_EXPORT_NWSI void mmAdaptiveSize_LoggerInfomation(struct mmAdaptiveSize* p, mmUInt32_t lvl)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    char hString0[128] = { 0 };
    char hString1[128] = { 0 };
    char hString2[128] = { 0 };
    char hString3[128] = { 0 };
    char hString4[128] = { 0 };
    char hString5[128] = { 0 };

    mmDoubleSizeToString(p->hDisplayFrameSize, hString0);
    mmDoubleSizeToString(p->hDesignCanvasSize, hString1);
    mmDoubleSizeToString(p->hPhysicalViewSize, hString2);
    mmDoubleRectToString(p->hPhysicalSafeRect, hString3);
    mmDoubleSizeToString(p->hActuallyViewSize, hString4);
    mmDoubleRectToString(p->hActuallySafeRect, hString5);

    //                              Window ----------------------------+----------------------------------
    //                              Window              hDisplayDensity: 1.000000
    //                              Window            hDisplayFrameSize: (378.00, 672.00)
    //                              Window               hDesignDensity: 2.000000
    //                              Window             hAbsoluteDensity: 1.000000
    //                              Window            hDesignCanvasSize: (720.00, 1280.00)
    //                              Window            hPhysicalViewSize: (378.00, 672.00)
    //                              Window            hPhysicalSafeRect: (0.00, 0.00, 378.00, 672.00)
    //                              Window            hActuallyViewSize: (720.00, 1280.00)
    //                              Window            hActuallySafeRect: (0.00, 0.00, 720.00, 1280.00)
    //                              Window             hTransformCanvas: 1.904762
    //                              Window             hTransformWindow: 1.904762
    //                              Window         hViewSizeAspectRatio: 0.562500
    //                              Window         hSafeRectAspectRatio: 0.562500
    //                              Window ----------------------------+----------------------------------

    mmLogger_Message(gLogger, lvl, "Window ----------------------------+----------------------------------");
    mmLogger_Message(gLogger, lvl, "Window              hDisplayDensity: %lf", p->hDisplayDensity);
    mmLogger_Message(gLogger, lvl, "Window            hDisplayFrameSize: %s", hString0);
    mmLogger_Message(gLogger, lvl, "Window               hDesignDensity: %lf", p->hDesignDensity);
    mmLogger_Message(gLogger, lvl, "Window             hAbsoluteDensity: %lf", p->hAbsoluteDensity);
    mmLogger_Message(gLogger, lvl, "Window            hDesignCanvasSize: %s", hString1);
    mmLogger_Message(gLogger, lvl, "Window            hPhysicalViewSize: %s", hString2);
    mmLogger_Message(gLogger, lvl, "Window            hPhysicalSafeRect: %s", hString3);
    mmLogger_Message(gLogger, lvl, "Window            hActuallyViewSize: %s", hString4);
    mmLogger_Message(gLogger, lvl, "Window            hActuallySafeRect: %s", hString5);
    mmLogger_Message(gLogger, lvl, "Window             hTransformCanvas: %lf", p->hTransformCanvas);
    mmLogger_Message(gLogger, lvl, "Window             hTransformWindow: %lf", p->hTransformWindow);
    mmLogger_Message(gLogger, lvl, "Window         hViewSizeAspectRatio: %lf", p->hViewSizeAspectRatio);
    mmLogger_Message(gLogger, lvl, "Window         hSafeRectAspectRatio: %lf", p->hSafeRectAspectRatio);
    mmLogger_Message(gLogger, lvl, "Window ----------------------------+----------------------------------");
}
