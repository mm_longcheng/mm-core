/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextFormat.h"

#include "core/mmAlloc.h"

#include "graphics/mmColor.h"
#include "graphics/mmGeometry.h"

#include <float.h>

MM_EXPORT_NWSI const int mmFontWeightThin          = 100;      // 100    Thin
MM_EXPORT_NWSI const int mmFontWeightExtraLight    = 200;      // 200    Extra Light
MM_EXPORT_NWSI const int mmFontWeightLight         = 300;      // 300    Light
MM_EXPORT_NWSI const int mmFontWeightNormal        = 400;      // 400    Normal
MM_EXPORT_NWSI const int mmFontWeightMedium        = 500;      // 500    Medium
MM_EXPORT_NWSI const int mmFontWeightSemiBold      = 600;      // 600    Semi Bold
MM_EXPORT_NWSI const int mmFontWeightBold          = 700;      // 700    Bold
MM_EXPORT_NWSI const int mmFontWeightExtraBold     = 800;      // 800    Extra Bold
MM_EXPORT_NWSI const int mmFontWeightExtraBlack    = 900;      // 900    Black

MM_EXPORT_NWSI const mmUInt16_t mmFontFamilyNameDefault[10] = { 'H', 'e', 'l', 'v', 'e', 't', 'i', 'c', 'a', 0, };

static const size_t gTextFormatSize0 = sizeof(struct mmUtf16String);
static const size_t gTextFormatSize1 = sizeof(struct mmTextFormat) - sizeof(struct mmUtf16String);

#define mmMakeTextPropertyRange(n) \
{ mmOffsetof(struct mmTextFormat, n), mmMemberSizeof(struct mmTextFormat, n) }

MM_EXPORT_NWSI const struct mmRange mmTextPropertys[12] =
{
    mmMakeTextPropertyRange(hFontFamilyName),
    mmMakeTextPropertyRange(hFontSize),
    mmMakeTextPropertyRange(hFontStyle),
    mmMakeTextPropertyRange(hFontWeight),
    mmMakeTextPropertyRange(hForegroundColor),
    mmMakeTextPropertyRange(hBackgroundColor),
    mmMakeTextPropertyRange(hStrokeWidth),
    mmMakeTextPropertyRange(hStrokeColor),
    mmMakeTextPropertyRange(hUnderlineWidth),
    mmMakeTextPropertyRange(hUnderlineColor),
    mmMakeTextPropertyRange(hStrikeThruWidth),
    mmMakeTextPropertyRange(hStrikeThruColor),
};

MM_EXPORT_NWSI
void
mmTextFormat_Init(
    struct mmTextFormat*                           p)
{
    mmUtf16String_Init(&p->hFontFamilyName);
    mmTextFormat_Reset(p);
}

MM_EXPORT_NWSI
void
mmTextFormat_Destroy(
    struct mmTextFormat*                           p)
{
    mmTextFormat_Reset(p);
    mmUtf16String_Destroy(&p->hFontFamilyName);
}

MM_EXPORT_NWSI
void
mmTextFormat_Reset(
    struct mmTextFormat*                           p)
{
    mmUtf16String_Reset(&p->hFontFamilyName);
    mmUtf16String_Assigns(&p->hFontFamilyName, mmFontFamilyNameDefault);
    p->hFontSize = 12.0f;
    p->hFontStyle = mmFontStyleNormal;
    p->hFontWeight = mmFontWeightNormal;

    mmColorMake(p->hForegroundColor, 0.0f, 0.0f, 0.0f, 1.0f);
    mmColorMake(p->hBackgroundColor, 0.0f, 0.0f, 0.0f, 0.0f);

    p->hStrokeWidth = 0.0f;
    mmColorMake(p->hStrokeColor, 0.0f, 0.0f, 0.0f, 1.0f);

    p->hUnderlineWidth = 0.0f;
    mmColorMake(p->hUnderlineColor, 0.0f, 0.0f, 0.0f, 1.0f);

    p->hStrikeThruWidth = 0.0f;
    mmColorMake(p->hStrikeThruColor, 0.0f, 0.0f, 0.0f, 1.0f);
}

MM_EXPORT_NWSI
int
mmTextFormat_Equals(
    const struct mmTextFormat*                     p,
    const struct mmTextFormat*                     q)
{
    return (0 == mmTextFormat_Compare(p, q));
}

MM_EXPORT_NWSI
void
mmTextFormat_Assign(
    struct mmTextFormat*                           p,
    const struct mmTextFormat*                     q)
{
    mmUtf16String_Reset(&p->hFontFamilyName);
    mmUtf16String_Assign(&p->hFontFamilyName, &q->hFontFamilyName);
    mmMemcpy((char*)p + gTextFormatSize0, (char*)q + gTextFormatSize0, gTextFormatSize1);
}

MM_EXPORT_NWSI
void
mmTextFormat_SetValue(
    struct mmTextFormat*                           p,
    size_t                                         o,
    size_t                                         l,
    const void*                                    v)
{
    if (o == mmOffsetof(struct mmTextFormat, hFontFamilyName))
    {
        struct mmUtf16String* pValue = (struct mmUtf16String*)v;
        mmUtf16String_Reset(&p->hFontFamilyName);
        mmUtf16String_Assign(&p->hFontFamilyName, pValue);
    }
    else
    {
        mmMemcpy(((mmUInt8_t*)p) + o, v, l);
    }
}

MM_EXPORT_NWSI
void
mmTextFormat_SetProperty(
    struct mmTextFormat*                           p,
    enum mmTextProperty_t                          t,
    const void*                                    v)
{
    const struct mmRange* pts;
    assert(0 <= t && t < MM_ARRAY_SIZE(mmTextPropertys) && "Property type is invalid.");
    pts = &mmTextPropertys[t];
    mmTextFormat_SetValue(p, pts->o, pts->l, v);
}

MM_EXPORT_NWSI
int
mmTextFormat_Compare(
    const struct mmTextFormat*                     p,
    const struct mmTextFormat*                     q)
{
    int c = mmUtf16String_Compare(&p->hFontFamilyName, &q->hFontFamilyName);
    if (0 != c) 
    {
        return c; 
    }
    else
    {
        return mmMemcmp((char*)p + gTextFormatSize0, (char*)q + gTextFormatSize0, gTextFormatSize1);
    }
}

MM_EXPORT_NWSI
int
mmTextFormat_IsFontEquals(
    const struct mmTextFormat*                     p,
    const struct mmTextFormat*                     q)
{
    int b1 = (0 == mmUtf16String_Compare(&p->hFontFamilyName, &q->hFontFamilyName));
    int b2 = (p->hFontSize == q->hFontSize);
    int b3 = (p->hFontStyle == q->hFontStyle);
    int b4 = (p->hFontWeight == q->hFontWeight);
    return b1 && b2 && b3 && b4;
}
