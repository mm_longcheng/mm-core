/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSurfaceMaster_h__
#define __mmSurfaceMaster_h__

#include "core/mmCore.h"
#include "core/mmFrameTimer.h"
#include "core/mmEventSet.h"

#include "nwsi/mmISurface.h"
#include "nwsi/mmIActivity.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventDragging;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventKeypadPressed;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventKeypadRelease;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventKeypadStatus;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventCursorBegan;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventCursorMoved;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventCursorEnded;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventCursorBreak;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventCursorWheel;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventTouchsBegan;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventTouchsMoved;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventTouchsEnded;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventTouchsBreak;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventSurfacePrepare;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventSurfaceDiscard;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventSurfaceChanged;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventInjectCopy;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventInjectCut;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventInjectPaste;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventUpdatedDisplay;

MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventEnterBackground;
MM_EXPORT_NWSI extern const char* mmSurfaceMaster_EventEnterForeground;

struct mmContextMaster;

struct mmSurfaceMaster
{
    struct mmISurface hSuper;
    
    struct mmFrameScheduler hFrameScheduler;
    
    struct mmEventUpdate hUpdate;
    
    // this member is event drive.
    struct mmEventSet hEventSet;
    
    struct mmContextMaster* pContextMaster;
    struct mmIActivity* pIActivity;
    struct mmISurfaceKeypad* pISurfaceKeypad;
    
    // weak ref.
    void* pViewSurface;
    
    // default is 60.
    double hDisplayFrequency;

    // default is 1.0.
    double hDisplayDensity;
    
    // Physical display frame Size (Display Pixel).
    // (w, h)
    double hDisplayFrameSize[2];

    // Physical logical view size of the device (System Point).
    // (w, h)
    double hPhysicalViewSize[2];
    // Physical logical safe rect of the device (System Point).
    // (x, y, w, h)
    double hPhysicalSafeRect[4];

    // mm_thread_state_t, default is MM_TS_CLOSED(0)
    mmSInt8_t hState;
};

MM_EXPORT_NWSI void mmSurfaceMaster_Init(struct mmSurfaceMaster* p);
MM_EXPORT_NWSI void mmSurfaceMaster_Destroy(struct mmSurfaceMaster* p);

MM_EXPORT_NWSI void mmSurfaceMaster_SetContextMaster(struct mmSurfaceMaster* p, struct mmContextMaster* pContextMaster);
MM_EXPORT_NWSI void mmSurfaceMaster_SetViewSurface(struct mmSurfaceMaster* p, void* pViewSurface);
MM_EXPORT_NWSI void mmSurfaceMaster_SetIActivity(struct mmSurfaceMaster* p, struct mmIActivity* pIActivity);
MM_EXPORT_NWSI void mmSurfaceMaster_SetISurfaceKeypad(struct mmSurfaceMaster* p, struct mmISurfaceKeypad* pISurfaceKeypad);
MM_EXPORT_NWSI void mmSurfaceMaster_SetDisplayFrequency(struct mmSurfaceMaster* p, double hDisplayFrequency);
MM_EXPORT_NWSI void mmSurfaceMaster_SetDisplayDensity(struct mmSurfaceMaster* p, double hDisplayDensity);
MM_EXPORT_NWSI void mmSurfaceMaster_SetDisplayFrameSize(struct mmSurfaceMaster* p, double hDisplayFrameSize[2]);
MM_EXPORT_NWSI void mmSurfaceMaster_SetPhysicalSafeRect(struct mmSurfaceMaster* p, double hPhysicalSafeRect[4]);
MM_EXPORT_NWSI void mmSurfaceMaster_SetSmoothMode(struct mmSurfaceMaster* p, int mode);

MM_EXPORT_NWSI void mmSurfaceMaster_LoggerInfomation(struct mmSurfaceMaster* p, mmUInt32_t lvl, const char* pWindowName);

MM_EXPORT_NWSI void mmSurfaceMaster_OnDisplayOneFrame(struct mmSurfaceMaster* p, double interval);

// virtual function for super interface implement.
MM_EXPORT_NWSI void mmSurfaceMaster_OnDragging(struct mmISurface* pSuper, struct mmEventDragging* content);

MM_EXPORT_NWSI void mmSurfaceMaster_OnKeypadPressed(struct mmISurface* pSuper, struct mmEventKeypad* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnKeypadRelease(struct mmISurface* pSuper, struct mmEventKeypad* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnKeypadStatus(struct mmISurface* pSuper, struct mmEventKeypadStatus* content);

MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorBegan(struct mmISurface* pSuper, struct mmEventCursor* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorMoved(struct mmISurface* pSuper, struct mmEventCursor* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorEnded(struct mmISurface* pSuper, struct mmEventCursor* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorBreak(struct mmISurface* pSuper, struct mmEventCursor* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorWheel(struct mmISurface* pSuper, struct mmEventCursor* content);

MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsBegan(struct mmISurface* pSuper, struct mmEventTouchs* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsMoved(struct mmISurface* pSuper, struct mmEventTouchs* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsEnded(struct mmISurface* pSuper, struct mmEventTouchs* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsBreak(struct mmISurface* pSuper, struct mmEventTouchs* content);

MM_EXPORT_NWSI void mmSurfaceMaster_OnSurfacePrepare(struct mmISurface* pSuper, struct mmEventMetrics* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnSurfaceDiscard(struct mmISurface* pSuper, struct mmEventMetrics* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnSurfaceChanged(struct mmISurface* pSuper, struct mmEventMetrics* content);

MM_EXPORT_NWSI void mmSurfaceMaster_OnGetText(struct mmISurface* pSuper, struct mmEventEditText* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnSetText(struct mmISurface* pSuper, struct mmEventEditText* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnGetTextEditRect(struct mmISurface* pSuper, double rect[4]);
MM_EXPORT_NWSI void mmSurfaceMaster_OnInsertTextUtf16(struct mmISurface* pSuper, struct mmEventEditString* content);
MM_EXPORT_NWSI void mmSurfaceMaster_OnReplaceTextUtf16(struct mmISurface* pSuper, struct mmEventEditString* content);

MM_EXPORT_NWSI void mmSurfaceMaster_OnInjectCopy(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnInjectCut(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnInjectPaste(struct mmISurface* pSuper);

MM_EXPORT_NWSI void mmSurfaceMaster_OnUpdate(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnUpdateImmediately(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnTimewait(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnTimewake(struct mmISurface* pSuper);

MM_EXPORT_NWSI void mmSurfaceMaster_OnStart(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnInterrupt(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnShutdown(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnJoin(struct mmISurface* pSuper);

MM_EXPORT_NWSI void mmSurfaceMaster_OnFinishLaunching(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnBeforeTerminate(struct mmISurface* pSuper);

MM_EXPORT_NWSI void mmSurfaceMaster_OnEnterBackground(struct mmISurface* pSuper);
MM_EXPORT_NWSI void mmSurfaceMaster_OnEnterForeground(struct mmISurface* pSuper);

#include "core/mmSuffix.h"

#endif//__mmSurfaceMaster_h__
