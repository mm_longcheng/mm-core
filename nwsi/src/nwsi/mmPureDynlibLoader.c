/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPureDynlibLoader.h"

#include "core/mmLogger.h"
#include "core/mmFilePath.h"

MM_EXPORT_NWSI void mmPureDynlibLoader_Init(struct mmPureDynlibLoader* p)
{
    mmString_Init(&p->hDynlibFolder);
    mmString_Init(&p->hDynlibPrefix);
    mmString_Init(&p->hDynlibSuffix);
    mmRbtreeStringVpt_Init(&p->hDynlibMap);
    
    mmString_Assigns(&p->hDynlibFolder, "");
    mmString_Assigns(&p->hDynlibPrefix, "");
    mmString_Assigns(&p->hDynlibSuffix, "");
}
MM_EXPORT_NWSI void mmPureDynlibLoader_Destroy(struct mmPureDynlibLoader* p)
{
    mmString_Destroy(&p->hDynlibFolder);
    mmString_Destroy(&p->hDynlibPrefix);
    mmString_Destroy(&p->hDynlibSuffix);
}

MM_EXPORT_NWSI void mmPureDynlibLoader_OnFinishLaunching(struct mmPureDynlibLoader* p)
{
    
}
MM_EXPORT_NWSI void mmPureDynlibLoader_OnBeforeTerminate(struct mmPureDynlibLoader* p)
{
    
}
// default is ""
MM_EXPORT_NWSI void mmPureDynlibLoader_SetDynlibFolder(struct mmPureDynlibLoader* p, const char* _dynlibFolder)
{
    mmString_Assigns(&p->hDynlibFolder, _dynlibFolder);
    mmDirectoryNoneSuffix(&p->hDynlibFolder, mmString_CStr(&p->hDynlibFolder));
}
// default is ""
MM_EXPORT_NWSI void mmPureDynlibLoader_SetDynlibPrefix(struct mmPureDynlibLoader* p, const char* _dynlibPrefix)
{
    mmString_Assigns(&p->hDynlibPrefix, _dynlibPrefix);
}
// default is ""
MM_EXPORT_NWSI void mmPureDynlibLoader_SetDynlibSuffix(struct mmPureDynlibLoader* p, const char* _dynlibSuffix)
{
    mmString_Assigns(&p->hDynlibSuffix, _dynlibSuffix);
}

MM_EXPORT_NWSI void mmPureDynlibLoader_AcquireDynlib(struct mmPureDynlibLoader* p, const char* _dynlibName)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmString _realName;
    struct mmString _weakKey;
    struct mmRbtreeStringVptIterator* it = NULL;
    
    mmString_Init(&_realName);
    mmString_MakeWeaks(&_weakKey, _dynlibName);
    it = mmRbtreeStringVpt_GetIterator(&p->hDynlibMap, &_weakKey);
    if(NULL == it)
    {
        MM_DYNLIB_HANDLE _dynlib = (MM_DYNLIB_HANDLE)MM_DYNLIB_LOAD(mmString_CStr(&_realName));
        if (_dynlib)
        {
            mmRbtreeStringVpt_Set(&p->hDynlibMap, &_weakKey, _dynlib);
            mmLogger_LogI(gLogger, "%s %d %s succeed.", __FUNCTION__, __LINE__, _dynlibName);
        }
        else
        {
            mmLogger_LogW(gLogger, "%s %d %s failure.", __FUNCTION__, __LINE__, _dynlibName);
        }
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d %s already load.", __FUNCTION__, __LINE__, _dynlibName);
    }
    
    mmString_Destroy(&_realName);
}
MM_EXPORT_NWSI void mmPureDynlibLoader_ReleaseDynlib(struct mmPureDynlibLoader* p, const char* _dynlibName)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmString _realName;
    struct mmString _weakKey;
    struct mmRbtreeStringVptIterator* it = NULL;
    
    mmString_Init(&_realName);
    mmString_MakeWeaks(&_weakKey, _dynlibName);
    it = mmRbtreeStringVpt_GetIterator(&p->hDynlibMap, &_weakKey);
    if(NULL != it)
    {
        MM_DYNLIB_HANDLE _dynlib = (MM_DYNLIB_HANDLE)it->v;
        if (MM_DYNLIB_UNLOAD(_dynlib))
        {
            mmLogger_LogW(gLogger, "%s %d %s failure.", __FUNCTION__, __LINE__, _dynlibName);
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d %s succeed.", __FUNCTION__, __LINE__, _dynlibName);
        }
        mmRbtreeStringVpt_Erase(&p->hDynlibMap, it);
    }
    else
    {
        mmLogger_LogW(gLogger, "%s %d %s not exist or not load in to system.", __FUNCTION__, __LINE__, _dynlibName);
    }
    
    mmString_Destroy(&_realName);
}
MM_EXPORT_NWSI void mmPureDynlibLoader_ClearDynlib(struct mmPureDynlibLoader* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    MM_DYNLIB_HANDLE _dynlib = NULL;
    //
    n = mmRb_First(&p->hDynlibMap.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        _dynlib = (MM_DYNLIB_HANDLE)it->v;
        if (MM_DYNLIB_UNLOAD(_dynlib))
        {
            mmLogger_LogW(gLogger, "%s %d %s failure.", __FUNCTION__, __LINE__, mmString_CStr(&it->k));
        }
        else
        {
            mmLogger_LogI(gLogger, "%s %d %s succeed.", __FUNCTION__, __LINE__, mmString_CStr(&it->k));
        }
        mmRbtreeStringVpt_Erase(&p->hDynlibMap, it);
    }
    
    mmLogger_LogI(gLogger, "%s %d succeed.", __FUNCTION__, __LINE__);
}

MM_EXPORT_NWSI void mmPureDynlibLoader_GetDynlibRealName(struct mmPureDynlibLoader* p, const char* _dynlibName, struct mmString* _realName)
{
    mmString_Assigns(_realName, "");
    
    mmString_Append(_realName, &p->hDynlibFolder);
    mmString_Appends(_realName, (0 == mmString_Size(&p->hDynlibFolder)) ? "" : "/");
    mmString_Append(_realName, &p->hDynlibPrefix);
    mmString_Appends(_realName, _dynlibName);
    mmString_Appends(_realName, MM_BUILD_SUFFIX);
    mmString_Append(_realName, &p->hDynlibSuffix);
}

