/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2022-2022 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIHandler_h__
#define __mmIHandler_h__

#include "core/mmCore.h"
#include "core/mmMetadata.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmIHandleDragging
{
    // void(*OnDragging)(void* p, struct mmEventDragging* content);
    void* OnDragging;
};

struct mmIHandleSoftboard
{
    // void(*OnKeypadStatus)(void* p, struct mmEventKeypadStatus* content);
    void* OnKeypadStatus;
};

struct mmIHandleKeypad
{
    // void(*OnKeypadPressed)(void* p, struct mmEventKeypad* content);
    void* OnKeypadPressed;
    // void(*OnKeypadRelease)(void* p, struct mmEventKeypad* content);
    void* OnKeypadRelease;
};

struct mmIHandleCursor
{
    // void(*OnCursorBegan)(void* p, struct mmEventCursor* content);
    void* OnCursorBegan;
    // void(*OnCursorMoved)(void* p, struct mmEventCursor* content);
    void* OnCursorMoved;
    // void(*OnCursorEnded)(void* p, struct mmEventCursor* content);
    void* OnCursorEnded;
    // void(*OnCursorBreak)(void* p, struct mmEventCursor* content);
    void* OnCursorBreak;
    // void(*OnCursorWheel)(void* p, struct mmEventCursor* content);
    void* OnCursorWheel;
};

struct mmIHandleTouchs
{
    // void(*OnTouchsBegan)(void* p, struct mmEventTouchs* content);
    void* OnTouchsBegan;
    // void(*OnTouchsMoved)(void* p, struct mmEventTouchs* content);
    void* OnTouchsMoved;
    // void(*OnTouchsEnded)(void* p, struct mmEventTouchs* content);
    void* OnTouchsEnded;
    // void(*OnTouchsBreak)(void* p, struct mmEventTouchs* content);
    void* OnTouchsBreak;
};

struct mmIHandleSurface
{
    // void(*OnSurfacePrepare)(void* p, struct mmEventMetrics* content);
    void* OnSurfacePrepare;
    // void(*OnSurfaceDiscard)(void* p, struct mmEventMetrics* content);
    void* OnSurfaceDiscard;
    // void(*OnSurfaceChanged)(void* p, struct mmEventMetrics* content);
    void* OnSurfaceChanged;
};

struct mmIHandleEditor
{
    // void(*OnGetText)(void* p, struct mmEventEditText* content);
    void* OnGetText;
    // void(*OnSetText)(void* p, struct mmEventEditText* content);
    void* OnSetText;
    // void(*OnGetTextEditRect)(void* p, double rect[4]);
    void* OnGetTextEditRect;
    // void(*OnInsertTextUtf16)(void* p, struct mmEventEditString* content);
    void* OnInsertTextUtf16;
    // void(*OnReplaceTextUtf16)(void* p, struct mmEventEditString* content);
    void* OnReplaceTextUtf16;
    
    // void(*OnInjectCopy)(void* p);
    void* OnInjectCopy;
    // void(*OnInjectCut)(void* p);
    void* OnInjectCut;
    // void(*OnInjectPaste)(void* p);
    void* OnInjectPaste;
};

struct mmIHandleActivity
{
    // void(*OnStart)(void* p);
    void* OnStart;
    // void(*OnInterrupt)(void* p);
    void* OnInterrupt;
    // void(*OnShutdown)(void* p);
    void* OnShutdown;
    // void(*OnJoin)(void* p);
    void* OnJoin;
    
    // void(*OnFinishLaunching)(void* p);
    void* OnFinishLaunching;
    // void(*OnBeforeTerminate)(void* p);
    void* OnBeforeTerminate;
    
    // void(*OnEnterBackground)(void* p);
    void* OnEnterBackground;
    // void(*OnEnterForeground)(void* p);
    void* OnEnterForeground;
};

struct mmIHandleScheduler
{
    // void(*OnUpdate)(void* p);
    void* OnUpdate;
    // void(*OnTimewait)(void* p);
    void* OnTimewait;
    // void(*OnTimewake)(void* p);
    void* OnTimewake;
};

struct mmIHandleUpdater
{
    // void(*OnDisplayOneFrame)(void* p, struct mmEventUpdate* content);
    void* OnDisplayOneFrame;
};

struct mmIHandleKeypadProvider
{
    // void(*OnSetKeypadType)(void* p, int hType);
    void* OnSetKeypadType;
    // void(*OnSetKeypadAppearance)(void* p, int hAppearance);
    void* OnSetKeypadAppearance;
    // void(*OnSetKeypadReturnKey)(void* p, int hReturnKey);
    void* OnSetKeypadReturnKey;
    // void(*OnKeypadStatusShow)(void* p);
    void* OnKeypadStatusShow;
    // void(*OnKeypadStatusHide)(void* p);
    void* OnKeypadStatusHide;
};

struct mmMetadataActivity
{
    const struct mmMetaAllocator* Allocator;
    const struct mmIHandleActivity* Activity;
    const struct mmIHandleDragging* Dragging;
    const struct mmIHandleKeypad* Keypad;
    const struct mmIHandleCursor* Cursor;
    const struct mmIHandleTouchs* Touchs;
    const struct mmIHandleEditor* Editor;
    const struct mmIHandleSurface* Surface;
    const struct mmIHandleUpdater* Updater;
};

struct mmMetadataContext
{
    const struct mmMetaAllocator* Allocator;
    const struct mmIHandleActivity* Activity;
};

struct mmMetadataSurface
{
    const struct mmMetaAllocator* Allocator;
    const struct mmIHandleActivity* Activity;
    const struct mmIHandleDragging* Dragging;
    const struct mmIHandleKeypad* Keypad;
    const struct mmIHandleCursor* Cursor;
    const struct mmIHandleTouchs* Touchs;
    const struct mmIHandleEditor* Editor;
    const struct mmIHandleSurface* Surface;
    const struct mmIHandleScheduler* Scheduler;
};

MM_EXPORT_NWSI void mmMetadataSurface_Init(struct mmMetadataSurface* p);
MM_EXPORT_NWSI void mmMetadataSurface_Destroy(struct mmMetadataSurface* p);

#include "core/mmSuffix.h"

#endif//__mmIHandler_h__
