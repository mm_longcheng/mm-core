/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextFormat_h__
#define __mmTextFormat_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "math/mmRange.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

// The default is mmTextAlignmentNatural.
enum mmTextAlignment_t
{
    mmTextAlignmentLeft      = 0,
    mmTextAlignmentCenter    = 1,
    mmTextAlignmentRight     = 2,
    mmTextAlignmentJustified = 3,
    mmTextAlignmentNatural   = 4,
};

// The default is mmTextParagraphAlignmentTop.
enum mmTextParagraphAlignment_t
{
    mmTextParagraphAlignmentTop    = 0,
    mmTextParagraphAlignmentCenter = 1,
    mmTextParagraphAlignmentBottom = 2,
};

// The default is mmTextLineBreakByWordWrapping.
enum mmTextLineBreakMode_t
{
    mmTextLineBreakByWordWrapping       = 0,
    mmTextLineBreakByCharWrapping       = 1,
    mmTextLineBreakByClipping           = 2,
    mmTextLineBreakByTruncatingHead     = 3,
    mmTextLineBreakByTruncatingTail     = 4,
    mmTextLineBreakByTruncatingMiddle   = 5,
};

// The default is mmTextWritingDirectionNatural.
enum mmTextWritingDirection_t
{
    mmTextWritingDirectionNatural       = -1,
    mmTextWritingDirectionLeftToRight   = 0,
    mmTextWritingDirectionRightToLeft   = 1,
};

// default is mmFontStyleNormal.
enum mmFontStyle_t
{
    mmFontStyleNormal   = 0,
    mmFontStyleItalic   = 1,
    mmFontStyleOblique  = 2,
};

MM_EXPORT_NWSI extern const int mmFontWeightThin;            // 100    Thin
MM_EXPORT_NWSI extern const int mmFontWeightExtraLight;      // 200    Extra Light
MM_EXPORT_NWSI extern const int mmFontWeightLight;           // 300    Light
MM_EXPORT_NWSI extern const int mmFontWeightNormal;          // 400    Normal
MM_EXPORT_NWSI extern const int mmFontWeightMedium;          // 500    Medium
MM_EXPORT_NWSI extern const int mmFontWeightSemiBold;        // 600    Semi Bold
MM_EXPORT_NWSI extern const int mmFontWeightBold;            // 700    Bold
MM_EXPORT_NWSI extern const int mmFontWeightExtraBold;       // 800    Extra Bold
MM_EXPORT_NWSI extern const int mmFontWeightExtraBlack;      // 900    Black

// utf16 "Helvetica" Font family name default.
MM_EXPORT_NWSI extern const mmUInt16_t mmFontFamilyNameDefault[10];

enum mmTextProperty_t
{
    mmTextPropertyFontFamilyName  =  0,
    mmTextPropertyFontSize        =  1,
    mmTextPropertyFontStyle       =  2,
    mmTextPropertyFontWeight      =  3,
    mmTextPropertyForegroundColor =  4,
    mmTextPropertyBackgroundColor =  5,
    mmTextPropertyStrokeWidth     =  6,
    mmTextPropertyStrokeColor     =  7,
    mmTextPropertyUnderlineWidth  =  8,
    mmTextPropertyUnderlineColor  =  9,
    mmTextPropertyStrikeThruWidth = 10,
    mmTextPropertyStrikeThruColor = 11,
};

MM_EXPORT_NWSI extern const struct mmRange mmTextPropertys[12];

struct mmTextFormat
{
    // "Helvetica",
    struct mmUtf16String hFontFamilyName;
    // 12.0 font point size.
    float hFontSize;
    // mmFontStyle_t default is mmFontStyleNormal.
    int hFontStyle;
    // default is mmFontWeightNormal bold is mmFontWeightBold.
    int hFontWeight;

    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    float hForegroundColor[4];
    // default rgba { 0.0f, 0.0f, 0.0f, 0.0f, }
    float hBackgroundColor[4];

    // -3 fill and Stroke 3. default 0, not Stroke.
    float hStrokeWidth;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    float hStrokeColor[4];

    // 0 is not underline. -1 is default Width.
    float hUnderlineWidth;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    float hUnderlineColor[4];

    // 0 is not strikethru. -1 is default Width.
    float hStrikeThruWidth;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    float hStrikeThruColor[4];
};

MM_EXPORT_NWSI 
void 
mmTextFormat_Init(
    struct mmTextFormat*                           p);

MM_EXPORT_NWSI 
void 
mmTextFormat_Destroy(
    struct mmTextFormat*                           p);

MM_EXPORT_NWSI 
void 
mmTextFormat_Reset(
    struct mmTextFormat*                           p);

MM_EXPORT_NWSI 
int 
mmTextFormat_Equals(
    const struct mmTextFormat*                     p, 
    const struct mmTextFormat*                     q);

MM_EXPORT_NWSI 
void 
mmTextFormat_Assign(
    struct mmTextFormat*                           p, 
    const struct mmTextFormat*                     q);

// o mmTextFormat member offset mmOffsetof(struct mmTextFormat, <member>)
// l mmTextFormat member length mmMemberSizeof(struct mmTextFormat, <member>)
// v pointer for value
MM_EXPORT_NWSI 
void 
mmTextFormat_SetValue(
    struct mmTextFormat*                           p, 
    size_t                                         o, 
    size_t                                         l, 
    const void*                                    v);

// t enum mmTextFormatProperty_t type 
// v pointer for value
MM_EXPORT_NWSI 
void 
mmTextFormat_SetProperty(
    struct mmTextFormat*                           p, 
    enum mmTextProperty_t                          t,
    const void*                                    v);

// 0 is same.
MM_EXPORT_NWSI 
int 
mmTextFormat_Compare(
    const struct mmTextFormat*                     p, 
    const struct mmTextFormat*                     q);

MM_EXPORT_NWSI 
int 
mmTextFormat_IsFontEquals(
    const struct mmTextFormat*                     p, 
    const struct mmTextFormat*                     q);

/*
 * @brief Use for TextFormat set attribute.
 *
 * @param p          Type struct mmTextFormat*
 * @param n          The name for member.
 * @param v          The pointer for value.
 */
#define mmTextFormat_SetAttribute(p, n, v)      \
mmTextFormat_SetValue((p),                      \
    mmOffsetof(struct mmTextFormat, n),         \
    mmMemberSizeof(struct mmTextFormat, n), (v))

#include "core/mmSuffix.h"

#endif//__mmTextFormat_h__
