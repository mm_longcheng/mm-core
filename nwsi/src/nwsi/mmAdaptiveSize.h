#ifndef __mmAdaptiveSize_h__
#define __mmAdaptiveSize_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmAdaptiveSize
{
    // default is 1.0.
    double hDisplayDensity;
    // Physical display frame Size (Display Pixel).
    // (w, h)
    double hDisplayFrameSize[2];

    // default is 1.0.
    double hDesignDensity;
    // Auto layout absolute density, default is 1.0.
    double hAbsoluteDensity;
    // Design logical canvas size(Design Pixel).
    // (w, h)
    double hDesignCanvasSize[2];

    // Physical logical view size of the device (System Point).
    // (w, h)
    double hPhysicalViewSize[2];
    // Physical logical safe rect of the device (System Point).
    // (x, y, w, h)
    double hPhysicalSafeRect[4];

    // Actually adjust the logical view size (GUI Pixel).
    // (w, h)
    double hActuallyViewSize[2];
    // Actually adjust the logical safe rect (GUI Pixel).
    // (x, y, w, h)
    double hActuallySafeRect[4];

    // Actually adjust scale, x and y is the same.
    // PhysicalView --> ActuallyView.
    double hTransformCanvas;

    // Actually adjust scale, x and y is the same.
    // DisplayFrame --> ActuallyView.
    double hTransformWindow;

    // ViewSize Aspect ratio w / h
    double hViewSizeAspectRatio;

    // SafeRect Aspect ratio w / h
    double hSafeRectAspectRatio;
};

MM_EXPORT_NWSI void mmAdaptiveSize_Init(struct mmAdaptiveSize* p);
MM_EXPORT_NWSI void mmAdaptiveSize_Destroy(struct mmAdaptiveSize* p);

MM_EXPORT_NWSI void mmAdaptiveSize_SetDisplayDensity(struct mmAdaptiveSize* p, double hDisplayDensity);
MM_EXPORT_NWSI void mmAdaptiveSize_SetAbsoluteDensity(struct mmAdaptiveSize* p, double hAbsoluteDensity);
MM_EXPORT_NWSI void mmAdaptiveSize_SetDisplayFrameSize(struct mmAdaptiveSize* p, double hDisplayFrameSize[2]);

MM_EXPORT_NWSI void mmAdaptiveSize_SetDesignDensity(struct mmAdaptiveSize* p, double hDesignDensity);
MM_EXPORT_NWSI void mmAdaptiveSize_SetDesignCanvasSize(struct mmAdaptiveSize* p, double hDesignCanvasSize[2]);

MM_EXPORT_NWSI void mmAdaptiveSize_SetPhysicalSafeRect(struct mmAdaptiveSize* p, double hPhysicalSafeRect[4]);

MM_EXPORT_NWSI void mmAdaptiveSize_UpdatePhysicalViewSize(struct mmAdaptiveSize* p);

MM_EXPORT_NWSI void mmAdaptiveSize_AdjustBaseX(struct mmAdaptiveSize* p);
MM_EXPORT_NWSI void mmAdaptiveSize_AdjustBaseY(struct mmAdaptiveSize* p);
MM_EXPORT_NWSI void mmAdaptiveSize_AdjustAutoRelatively(struct mmAdaptiveSize* p);
MM_EXPORT_NWSI void mmAdaptiveSize_AdjustAutoAbsolute(struct mmAdaptiveSize* p);

MM_EXPORT_NWSI void mmAdaptiveSize_OnFinishLaunching(struct mmAdaptiveSize* p);
MM_EXPORT_NWSI void mmAdaptiveSize_OnBeforeTerminate(struct mmAdaptiveSize* p);

MM_EXPORT_NWSI void mmAdaptiveSize_LoggerInfomation(struct mmAdaptiveSize* p, mmUInt32_t lvl);

#include "core/mmSuffix.h"

#endif//__mmAdaptiveSize_h__
