/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAppManifest_h__
#define __mmAppManifest_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmAppManifest
{
    struct mmString AssetsRootFolderPath;
    struct mmString AssetsRootFolderBase;
    struct mmString AssetsRootSourcePath;
    struct mmString AssetsRootSourceBase;
    struct mmString ShaderCacheFolder;
    struct mmString TextureCacheFolder;
    struct mmString PluginFolder;
    struct mmString DynlibFolder;
    struct mmString RenderSystemName;
    struct mmString LoggerFileName;
    struct mmString LoggerPathName;
    double DisplayFrequency;
    double FrameScale;
    mmUInt32_t AssetsRootType;
    mmUInt32_t LoggerLevel;
    mmUInt32_t CanvasRect[4];
};

MM_EXPORT_NWSI void mmAppManifest_Init(struct mmAppManifest* p);
MM_EXPORT_NWSI void mmAppManifest_Destroy(struct mmAppManifest* p);

MM_EXPORT_NWSI void mmAppManifest_Analysis(struct mmAppManifest* p, const char* pFileName);

#include "core/mmSuffix.h"

#endif//__mmAppManifest_h__
