/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmContextInterface_h__
#define __mmContextInterface_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmIContext
{
    void(*OnStart)(struct mmIContext* p);
    void(*OnInterrupt)(struct mmIContext* p);
    void(*OnShutdown)(struct mmIContext* p);
    void(*OnJoin)(struct mmIContext* p);

    void(*OnFinishLaunching)(struct mmIContext* p);
    void(*OnBeforeTerminate)(struct mmIContext* p);
    
    void(*OnEnterBackground)(struct mmIContext* p);
    void(*OnEnterForeground)(struct mmIContext* p);
};

MM_EXPORT_NWSI void mmIContext_Init(struct mmIContext* p);
MM_EXPORT_NWSI void mmIContext_Destroy(struct mmIContext* p);

#include "core/mmSuffix.h"

#endif//__mmContextInterface_h__
