/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFontsSource.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmByte.h"

#include <assert.h>

MM_EXPORT_NWSI void mmFontsData_Init(struct mmFontsData* p)
{
    mmString_Init(&p->lang);
    mmString_Init(&p->style);
    mmString_Init(&p->source);
    mmString_Init(&p->variant);
    p->weight = 0.0f;
    
    mmString_Assigns(&p->lang, "");
    mmString_Assigns(&p->style, "");
    mmString_Assigns(&p->source, "");
    mmString_Assigns(&p->variant, "");
}
MM_EXPORT_NWSI void mmFontsData_Destroy(struct mmFontsData* p)
{
    mmString_Destroy(&p->lang);
    mmString_Destroy(&p->style);
    mmString_Destroy(&p->source);
    mmString_Destroy(&p->variant);
    p->weight = 0.0f;
}

MM_EXPORT_NWSI void mmFontsData_CopyFrom(struct mmFontsData* p, struct mmFontsData* q)
{
    mmString_Assign(&p->lang, &q->lang);
    mmString_Assign(&p->style, &q->style);
    mmString_Assign(&p->source, &q->source);
    mmString_Assign(&p->variant, &q->variant);
    p->weight = q->weight;
}

MM_EXPORT_NWSI const char* mmFontsSource_FontsLanguage_zh_Hans = "zh-Hans";
MM_EXPORT_NWSI const char* mmFontsSource_FontsLanguage_zh_Hant = "zh-Hant";
MM_EXPORT_NWSI const char* mmFontsSource_FontsLanguage_ja = "ja";
MM_EXPORT_NWSI const char* mmFontsSource_FontsLanguage_ko = "ko";

MM_EXPORT_NWSI void mmFontsSource_Init(struct mmFontsSource* p)
{
    mmRbtreeStringVpt_Init(&p->hCreatorMap);
    mmRbtreeStringVpt_Init(&p->hLangs);
    mmRbtreeStringVpt_Init(&p->hFontsDocuments);
    mmListString_Init(&p->hFontsDirectorys);
    
    mmString_Init(&p->hDefaultEtc);
    mmString_Init(&p->hDefaultLanguage);
    mmString_Init(&p->hDefaultLanguageSourceFile);
    mmString_Init(&p->hDefaultLanguageSourceFonts);
    
    p->pFileContext = NULL;
    
    mmString_Assigns(&p->hDefaultEtc, "/etc");
    mmString_Assigns(&p->hDefaultLanguage, mmFontsSource_FontsLanguage_zh_Hans);
    mmString_Assigns(&p->hDefaultLanguageSourceFile, "");
    mmString_Assigns(&p->hDefaultLanguageSourceFonts, "");
}
MM_EXPORT_NWSI void mmFontsSource_Destroy(struct mmFontsSource* p)
{
    mmFontsSource_ClearFontsDocument(p);
    mmFontsSource_ClearSources(p);
    
    mmRbtreeStringVpt_Destroy(&p->hCreatorMap);
    mmRbtreeStringVpt_Destroy(&p->hLangs);
    mmRbtreeStringVpt_Destroy(&p->hFontsDocuments);
    mmListString_Destroy(&p->hFontsDirectorys);
    
    mmString_Destroy(&p->hDefaultEtc);
    mmString_Destroy(&p->hDefaultLanguage);
    mmString_Destroy(&p->hDefaultLanguageSourceFile);
    mmString_Destroy(&p->hDefaultLanguageSourceFonts);
    
    p->pFileContext = NULL;
}

MM_EXPORT_NWSI void mmFontsSource_SetFileContext(struct mmFontsSource* p, struct mmFileContext* _pFileContext)
{
    p->pFileContext = _pFileContext;
}

MM_EXPORT_NWSI void mmFontsSource_SetDefaultEtc(struct mmFontsSource* p, const char* _path)
{
    mmString_Assigns(&p->hDefaultEtc, _path);
}
MM_EXPORT_NWSI void mmFontsSource_SetDefaultLanguage(struct mmFontsSource* p, const char* _language)
{
    mmString_Assigns(&p->hDefaultLanguage, _language);
}

MM_EXPORT_NWSI void mmFontsSource_SetDefaultLanguageSourceFile(struct mmFontsSource* p, const char* _file)
{
    mmString_Assigns(&p->hDefaultLanguageSourceFile, _file);
}
MM_EXPORT_NWSI void mmFontsSource_SetDefaultLanguageSourceFonts(struct mmFontsSource* p, const char* _path)
{
    mmString_Assigns(&p->hDefaultLanguageSourceFonts, _path);
}

MM_EXPORT_NWSI void mmFontsSource_OnFinishLaunching(struct mmFontsSource* p)
{
    mmFontsSource_AnalysisFontsDefault(p);
}
MM_EXPORT_NWSI void mmFontsSource_OnBeforeTerminate(struct mmFontsSource* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI void mmFontsSource_Register(struct mmFontsSource* p, const char* _fileName, const struct mmFontsDocumentCreator* _pCreator)
{
    struct mmString _weakName;
    mmString_MakeWeaks(&_weakName, _fileName);
    mmRbtreeStringVpt_Set(&p->hCreatorMap, &_weakName, (void*)_pCreator);
}
MM_EXPORT_NWSI void mmFontsSource_Unregister(struct mmFontsSource* p, const char* _fileName)
{
    struct mmString _weakName;
    mmString_MakeWeaks(&_weakName, _fileName);
    mmRbtreeStringVpt_Rmv(&p->hCreatorMap, &_weakName);
}

MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_Produce(struct mmFontsSource* p, const char* _fileName)
{
    struct mmFontsDocumentBase* u = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString _weakName;
    
    mmString_MakeWeaks(&_weakName, _fileName);
    it = mmRbtreeStringVpt_GetIterator(&p->hCreatorMap, &_weakName);
    if(NULL != it)
    {
        const struct mmFontsDocumentCreator* _pCreator = (const struct mmFontsDocumentCreator*)it->v;
        u = (*(_pCreator->Produce))();
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        u = NULL;
        mmLogger_LogF(gLogger, "%s %d _fileName: %s is invalid.", __FUNCTION__, __LINE__, _fileName);
    }
    return u;
}
MM_EXPORT_NWSI void mmFontsSource_Recycle(struct mmFontsSource* p, const char* _fileName, struct mmFontsDocumentBase* u)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString _weakName;
    
    mmString_MakeWeaks(&_weakName, _fileName);
    it = mmRbtreeStringVpt_GetIterator(&p->hCreatorMap, &_weakName);
    if(NULL != it)
    {
        const struct mmFontsDocumentCreator* _pCreator = (const struct mmFontsDocumentCreator*)it->v;
        (*(_pCreator->Recycle))(u);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogF(gLogger, "%s %d _fileName: %s is invalid.", __FUNCTION__, __LINE__, _fileName);
    }
}

MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_AddFontsDocument(struct mmFontsSource* p, const char* _fileName)
{
    struct mmString _weakName;
    struct mmFontsDocumentBase* u = mmFontsSource_GetFontsDocument(p, _fileName);
    if(NULL == u)
    {
        u = mmFontsSource_Produce(p, _fileName);
        mmString_MakeWeaks(&_weakName, _fileName);
        mmRbtreeStringVpt_Set(&p->hFontsDocuments, &_weakName, u);
    }
    return u;
}
MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_GetFontsDocument(struct mmFontsSource* p, const char* _fileName)
{
    struct mmFontsDocumentBase* u = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString _weakName;
    
    mmString_MakeWeaks(&_weakName, _fileName);
    it = mmRbtreeStringVpt_GetIterator(&p->hFontsDocuments, &_weakName);
    if(NULL != it)
    {
        u = (struct mmFontsDocumentBase*)it->v;
    }
    return u;
}
MM_EXPORT_NWSI struct mmFontsDocumentBase* mmFontsSource_GetFontsDocumentInstance(struct mmFontsSource* p, const char* _fileName)
{
    struct mmFontsDocumentBase* u = mmFontsSource_GetFontsDocument(p, _fileName);
    if(NULL == u)
    {
        u = mmFontsSource_AddFontsDocument(p, _fileName);
    }
    return u;
}
MM_EXPORT_NWSI void mmFontsSource_RmvFontsDocument(struct mmFontsSource* p, const char* _fileName)
{
    struct mmFontsDocumentBase* u = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString _weakName;
    
    mmString_MakeWeaks(&_weakName, _fileName);
    it = mmRbtreeStringVpt_GetIterator(&p->hFontsDocuments, &_weakName);
    if(NULL != it)
    {
        u = (struct mmFontsDocumentBase*)it->v;
        mmFontsSource_Recycle(p, _fileName, u);
        mmRbtreeStringVpt_Erase(&p->hFontsDocuments, it);
    }
}
MM_EXPORT_NWSI void mmFontsSource_ClearFontsDocument(struct mmFontsSource* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFontsDocumentBase* u = NULL;
    //
    n = mmRb_First(&p->hFontsDocuments.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        u = (struct mmFontsDocumentBase*)(it->v);
        mmFontsSource_Recycle(p, mmString_CStr(&it->k), u);
        mmRbtreeStringVpt_Erase(&p->hFontsDocuments, it);
    }
}

MM_EXPORT_NWSI struct mmVectorValue* mmFontsSource_AddSources(struct mmFontsSource* p, const char* lang)
{
    struct mmString _weakName;
    struct mmVectorValue* u = mmFontsSource_GetSources(p, lang);
    if(NULL == u)
    {
        struct mmVectorValueAllocator hValueAllocator;

        hValueAllocator.Produce = &mmFontsData_Init;
        hValueAllocator.Recycle = &mmFontsData_Destroy;
        
        u = (struct mmVectorValue*)mmMalloc(sizeof(struct mmVectorValue));
        mmVectorValue_Init(u);
        mmVectorValue_SetElement(u, sizeof(struct mmFontsData));
        mmVectorValue_SetAllocator(u, &hValueAllocator);
        mmString_MakeWeaks(&_weakName, lang);
        mmRbtreeStringVpt_Set(&p->hLangs, &_weakName, u);
    }
    return u;
}
MM_EXPORT_NWSI struct mmVectorValue* mmFontsSource_GetSources(struct mmFontsSource* p, const char* lang)
{
    struct mmVectorValue* u = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString _weakName;
    
    mmString_MakeWeaks(&_weakName, lang);
    it = mmRbtreeStringVpt_GetIterator(&p->hLangs, &_weakName);
    if(NULL != it)
    {
        u = (struct mmVectorValue*)it->v;
    }
    return u;
}
MM_EXPORT_NWSI struct mmVectorValue* mmFontsSource_GetSourcesInstance(struct mmFontsSource* p, const char* lang)
{
    struct mmVectorValue* u = mmFontsSource_GetSources(p, lang);
    if(NULL == u)
    {
        u = mmFontsSource_AddSources(p, lang);
    }
    return u;
}
MM_EXPORT_NWSI void mmFontsSource_ClearSources(struct mmFontsSource* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmVectorValue* u = NULL;
    //
    n = mmRb_First(&p->hLangs.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        u = (struct mmVectorValue*)(it->v);
        mmVectorValue_Destroy(u);
        mmFree(u);
        mmRbtreeStringVpt_Erase(&p->hLangs, it);
    }
}

MM_EXPORT_NWSI void mmFontsSource_AddSource(struct mmFontsSource* p, const char* lang, struct mmFontsData* source)
{
    size_t index = 0;
    struct mmFontsData* value = NULL;
    struct mmVectorValue* sources = NULL;
    
    sources = mmFontsSource_GetSourcesInstance(p, lang);
    index = sources->size;
    mmVectorValue_Resize(sources, index + 1);
    value = (struct mmFontsData*)mmVectorValue_At(sources, index);
    mmFontsData_CopyFrom(value, source);
}
MM_EXPORT_NWSI struct mmFontsData* mmFontsSource_GetSource(struct mmFontsSource* p, const char* lang, size_t index)
{
    struct mmFontsData* value = NULL;
    struct mmVectorValue* sources = NULL;
    
    sources = mmFontsSource_GetSourcesInstance(p, lang);
    if(index < sources->size)
    {
        value = (struct mmFontsData*)mmVectorValue_At(sources, index);
    }
    return value;
}

MM_EXPORT_NWSI struct mmFontsData* mmFontsSource_GetSourceDefault(struct mmFontsSource* p, const char* lang)
{
    return mmFontsSource_GetSource(p, lang, 0);
}

MM_EXPORT_NWSI void mmFontsSource_AddDefaultFonts(struct mmFontsSource* p, const char* path)
{
    struct mmString _weakName;
    mmString_MakeWeaks(&_weakName, path);
    mmListString_AddTail(&p->hFontsDirectorys, &_weakName);
}
MM_EXPORT_NWSI void mmFontsSource_RmvDefaultFonts(struct mmFontsSource* p, const char* path)
{
    struct mmListHead* pos = NULL;
    struct mmListStringIterator* it = NULL;

    pos = p->hFontsDirectorys.l.next;
    while (pos != &p->hFontsDirectorys.l)
    {
        struct mmListHead* curr = pos;
        pos = pos->next;
        it = mmList_Entry(curr, struct mmListStringIterator, n);

        if(0 == mmString_CompareCStr(&it->v, path))
        {
            mmListString_Erase(&p->hFontsDirectorys, it);
        }
    }
}
MM_EXPORT_NWSI void mmFontsSource_ClearDefaultFonts(struct mmFontsSource* p)
{
    mmListString_Clear(&p->hFontsDirectorys);
}
MM_EXPORT_NWSI const struct mmListString* mmFontsSource_GetDefaultFonts(struct mmFontsSource* p)
{
    return &p->hFontsDirectorys;
}

MM_EXPORT_NWSI void mmFontsSource_AnalysisFontsDocumentFiles(struct mmFontsSource* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFontsDocumentBase* u = NULL;
    //
    n = mmRb_First(&p->hFontsDocuments.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        u = (struct mmFontsDocumentBase*)(it->v);
        (*(u->Analysis))(u, p, mmString_CStr(&it->k));
    }
}
MM_EXPORT_NWSI void mmFontsSource_AnalysisFontsDefault(struct mmFontsSource* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFontsDocumentBase* u = NULL;
    
    struct mmVectorValue* _sources = NULL;
    
    struct mmFontsData* pSource = NULL;
    
    struct mmString _FullEtcFileName;
    
    mmLogger_LogI(gLogger, "%s %d begin.", __FUNCTION__, __LINE__);
    
    mmString_Init(&_FullEtcFileName);

    n = mmRb_First(&p->hFontsDocuments.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        u = (struct mmFontsDocumentBase*)(it->v);
        
        mmString_Assign(&_FullEtcFileName, &p->hDefaultEtc);
        mmString_Appends(&_FullEtcFileName, "/");
        mmString_Append(&_FullEtcFileName, &it->k);

        mmLogger_LogD(gLogger, "%s %d etc_file: %s.", __FUNCTION__, __LINE__, mmString_CStr(&_FullEtcFileName));

        (*(u->Analysis))(u, p, mmString_CStr(&it->k));

        _sources = mmFontsSource_GetSources(p, mmString_CStr(&p->hDefaultLanguage));
        if (0 != _sources->size)
        {
            break;
        }
    }

    pSource = mmFontsSource_GetSourceDefault(p, mmString_CStr(&p->hDefaultLanguage));
    if (NULL != pSource)
    {
        struct mmString _FullTTFFileName;
        struct mmString _Fonts;
        
        struct mmListHead* pos = NULL;
        struct mmListStringIterator* it = NULL;
        
        mmString_Init(&_FullTTFFileName);
        mmString_Init(&_Fonts);

        pos = p->hFontsDirectorys.l.next;
        while (pos != &p->hFontsDirectorys.l)
        {
            struct mmListHead* curr = pos;
            pos = pos->next;
            it = mmList_Entry(curr, struct mmListStringIterator, n);

            mmString_Assign(&_FullTTFFileName, &it->v);
            mmString_Appends(&_FullTTFFileName, "/");
            mmString_Append(&_FullTTFFileName, &pSource->source);
            
            mmLogger_LogD(gLogger, "%s %d ttf_file: %s.", __FUNCTION__, __LINE__, mmString_CStr(&_FullTTFFileName));
            
            if (0 != mmFileContext_IsFileExists(p->pFileContext, mmString_CStr(&_FullTTFFileName)))
            {
                mmFontsSource_SetDefaultLanguageSourceFile(p, mmString_CStr(&pSource->source));
                mmFontsSource_SetDefaultLanguageSourceFonts(p, mmString_CStr(&it->v));

                mmLogger_LogI(gLogger, "%s %d fonts: %s source: %s.", __FUNCTION__, __LINE__, mmString_CStr(&it->v), mmString_CStr(&pSource->source));
                break;
            }
        }
        
        mmString_Destroy(&_FullTTFFileName);
        mmString_Destroy(&_Fonts);
    }
    
    mmString_Destroy(&_FullEtcFileName);

    mmLogger_LogI(gLogger, "%s %d end.", __FUNCTION__, __LINE__);
}
