/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIActivity_h__
#define __mmIActivity_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmContextMaster;
struct mmSurfaceMaster;

struct mmEventDragging;
struct mmEventKeypad;
struct mmEventKeypadStatus;
struct mmEventCursor;
struct mmEventTouchs;
struct mmEventMetrics;
struct mmEventEditText;
struct mmEventEditString;
struct mmEventUpdate;

struct mmIActivity
{
    void(*SetNativePointer)(struct mmIActivity* p, void* pNativePointer);
    void(*SetContext)(struct mmIActivity* p, struct mmContextMaster* pContextMaster);
    void(*SetSurface)(struct mmIActivity* p, struct mmSurfaceMaster* pSurfaceMaster);
    
    void(*OnDragging)(struct mmIActivity* p, struct mmEventDragging* content);

    void(*OnKeypadPressed)(struct mmIActivity* p, struct mmEventKeypad* content);
    void(*OnKeypadRelease)(struct mmIActivity* p, struct mmEventKeypad* content);
    void(*OnKeypadStatus)(struct mmIActivity* p, struct mmEventKeypadStatus* content);

    void(*OnCursorBegan)(struct mmIActivity* p, struct mmEventCursor* content);
    void(*OnCursorMoved)(struct mmIActivity* p, struct mmEventCursor* content);
    void(*OnCursorEnded)(struct mmIActivity* p, struct mmEventCursor* content);
    void(*OnCursorBreak)(struct mmIActivity* p, struct mmEventCursor* content);
    void(*OnCursorWheel)(struct mmIActivity* p, struct mmEventCursor* content);

    void(*OnTouchsBegan)(struct mmIActivity* p, struct mmEventTouchs* content);
    void(*OnTouchsMoved)(struct mmIActivity* p, struct mmEventTouchs* content);
    void(*OnTouchsEnded)(struct mmIActivity* p, struct mmEventTouchs* content);
    void(*OnTouchsBreak)(struct mmIActivity* p, struct mmEventTouchs* content);

    void(*OnSurfacePrepare)(struct mmIActivity* p, struct mmEventMetrics* content);
    void(*OnSurfaceDiscard)(struct mmIActivity* p, struct mmEventMetrics* content);
    void(*OnSurfaceChanged)(struct mmIActivity* p, struct mmEventMetrics* content);

    void(*OnGetText)(struct mmIActivity* p, struct mmEventEditText* content);
    void(*OnSetText)(struct mmIActivity* p, struct mmEventEditText* content);
    void(*OnGetTextEditRect)(struct mmIActivity* p, double rect[4]);
    void(*OnInsertTextUtf16)(struct mmIActivity* p, struct mmEventEditString* content);
    void(*OnReplaceTextUtf16)(struct mmIActivity* p, struct mmEventEditString* content);

    void(*OnInjectCopy)(struct mmIActivity* p);
    void(*OnInjectCut)(struct mmIActivity* p);
    void(*OnInjectPaste)(struct mmIActivity* p);

    void(*OnDisplayOneFrame)(struct mmIActivity* p, struct mmEventUpdate* content);

    void(*OnStart)(struct mmIActivity* p);
    void(*OnInterrupt)(struct mmIActivity* p);
    void(*OnShutdown)(struct mmIActivity* p);
    void(*OnJoin)(struct mmIActivity* p);

    void(*OnFinishLaunching)(struct mmIActivity* p);
    void(*OnBeforeTerminate)(struct mmIActivity* p);
    
    void(*OnEnterBackground)(struct mmIActivity* p);
    void(*OnEnterForeground)(struct mmIActivity* p);
};
MM_EXPORT_NWSI void mmIActivity_Init(struct mmIActivity* p);
MM_EXPORT_NWSI void mmIActivity_Destroy(struct mmIActivity* p);

#include "core/mmSuffix.h"

#endif//__mmIActivity_h__
