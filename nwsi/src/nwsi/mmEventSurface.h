/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEventSurface_h__
#define __mmEventSurface_h__

#include "core/mmCore.h"

#include "core/mmEventSet.h"

#include "nwsi/mmISurface.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmEventDraggingArgs
{
    struct mmEventArgs hSuper;
    struct mmEventDragging* content;
};
MM_EXPORT_NWSI void mmEventDraggingArgs_Reset(struct mmEventDraggingArgs* p);

struct mmEventKeypadArgs
{
    struct mmEventArgs hSuper;
    struct mmEventKeypad* content;
};
MM_EXPORT_NWSI void mmEventKeypadArgs_Reset(struct mmEventKeypadArgs* p);

struct mmEventKeypadStatusArgs
{
    struct mmEventArgs hSuper;
    struct mmEventKeypadStatus* content;
};
MM_EXPORT_NWSI void mmEventKeypadStatusArgs_Reset(struct mmEventKeypadStatusArgs* p);

struct mmEventCursorArgs
{
    struct mmEventArgs hSuper;
    struct mmEventCursor* content;
};
MM_EXPORT_NWSI void mmEventCursorArgs_Reset(struct mmEventCursorArgs* p);

struct mmEventTouchsArgs
{
    struct mmEventArgs hSuper;
    struct mmEventTouchs* content;
};
MM_EXPORT_NWSI void mmEventTouchsArgs_Reset(struct mmEventTouchsArgs* p);

struct mmEventMetricsArgs
{
    struct mmEventArgs hSuper;
    struct mmEventMetrics* content;
};
MM_EXPORT_NWSI void mmEventMetricsArgs_Reset(struct mmEventMetricsArgs* p);

struct mmEventUpdateArgs
{
    struct mmEventArgs hSuper;
    struct mmEventUpdate* content;
};
MM_EXPORT_NWSI void mmEventUpdateArgs_Reset(struct mmEventUpdateArgs* p);

#include "core/mmSuffix.h"

#endif//__mmEventSurface_h__
