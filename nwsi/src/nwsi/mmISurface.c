/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmISurface.h"

#include "core/mmAlloc.h"

MM_EXPORT_NWSI void mmEventDragging_Init(struct mmEventDragging* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(&p->context);
    mmMemset(p->point, 0, sizeof(double) * 2);

    // vector value initialization.
    hValueAllocator.Produce = &mmString_Init;
    hValueAllocator.Recycle = &mmString_Destroy;
    mmVectorValue_SetAllocator(&p->context, &hValueAllocator);
    mmVectorValue_SetElement(&p->context, sizeof(struct mmString));
}
MM_EXPORT_NWSI void mmEventDragging_Destroy(struct mmEventDragging* p)
{
    mmVectorValue_Destroy(&p->context);
    mmMemset(p->point, 0, sizeof(double) * 2);
}
MM_EXPORT_NWSI void mmEventDragging_Reset(struct mmEventDragging* p)
{
    mmVectorValue_Clear(&p->context);
    mmMemset(p->point, 0, sizeof(double) * 2);
}
MM_EXPORT_NWSI void mmEventDragging_AlignedMemory(struct mmEventDragging* p, size_t size)
{
    mmVectorValue_AlignedMemory(&p->context, size);
}
MM_EXPORT_NWSI void mmEventDragging_SetForIndex(struct mmEventDragging* p, size_t index, const char* url)
{
    struct mmString* item = (struct mmString*)mmVectorValue_At(&p->context, index);
    mmString_Assigns(item, url);
}
MM_EXPORT_NWSI void mmEventDragging_GetForIndex(struct mmEventDragging* p, size_t index, struct mmString* url)
{
    struct mmString* item = (struct mmString*)mmVectorValue_At(&p->context, index);
    mmString_Assign(url, item);
}
MM_EXPORT_NWSI struct mmString* mmEventDragging_GetForIndexReference(struct mmEventDragging* p, size_t index)
{
    return (struct mmString*)mmVectorValue_At(&p->context, index);
}
MM_EXPORT_NWSI void mmEventDragging_SetPoint(struct mmEventDragging* p, double x, double y)
{
    p->point[0] = x;
    p->point[1] = y;
}

MM_EXPORT_NWSI void mmEventActiveStatus_Init(struct mmEventActiveStatus* p)
{
    p->state = mmActiveStatusBackground;
}
MM_EXPORT_NWSI void mmEventActiveStatus_Destroy(struct mmEventActiveStatus* p)
{
    p->state = mmActiveStatusBackground;
}
MM_EXPORT_NWSI void mmEventActiveStatus_Reset(struct mmEventActiveStatus* p)
{
    p->state = mmActiveStatusBackground;
}

MM_EXPORT_NWSI void mmEventKeypad_Init(struct mmEventKeypad* p)
{
    p->modifier_mask = 0;
    p->key = 0;
    p->length = 0;
    mmMemset(p->text, 0, sizeof(p->text));
    p->handle = 0;
}
MM_EXPORT_NWSI void mmEventKeypad_Destroy(struct mmEventKeypad* p)
{
    p->modifier_mask = 0;
    p->key = 0;
    p->length = 0;
    mmMemset(p->text, 0, sizeof(p->text));
    p->handle = 0;
}
MM_EXPORT_NWSI void mmEventKeypad_Reset(struct mmEventKeypad* p)
{
    p->modifier_mask = 0;
    p->key = 0;
    p->length = 0;
    mmMemset(p->text, 0, sizeof(p->text));
    p->handle = 0;
}

MM_EXPORT_NWSI void mmEventCursor_Init(struct mmEventCursor* p)
{
    p->modifier_mask = 0;
    p->button_mask = 0;
    p->button_id = 0;
    p->abs_x = 0.0;
    p->abs_y = 0.0;
    p->rel_x = 0.0;
    p->rel_y = 0.0;
    p->wheel_dx = 0.0;
    p->wheel_dy = 0.0;
    p->timestamp = 0;
    p->handle = 0;
}
MM_EXPORT_NWSI void mmEventCursor_Destroy(struct mmEventCursor* p)
{
    p->modifier_mask = 0;
    p->button_mask = 0;
    p->button_id = 0;
    p->abs_x = 0.0;
    p->abs_y = 0.0;
    p->rel_x = 0.0;
    p->rel_y = 0.0;
    p->wheel_dx = 0.0;
    p->wheel_dy = 0.0;
    p->timestamp = 0;
    p->handle = 0;
}
MM_EXPORT_NWSI void mmEventCursor_Reset(struct mmEventCursor* p)
{
    p->modifier_mask = 0;
    p->button_mask = 0;
    p->button_id = 0;
    p->abs_x = 0.0;
    p->abs_y = 0.0;
    p->rel_x = 0.0;
    p->rel_y = 0.0;
    p->wheel_dx = 0.0;
    p->wheel_dy = 0.0;
    p->timestamp = 0;
    p->handle = 0;
}

MM_EXPORT_NWSI void mmEventTouch_Init(struct mmEventTouch* p)
{
    p->motion_id = 0;
    p->tap_count = 0;
    p->phase = mmTouchBegan;
    p->modifier_mask = 0;
    p->button_mask = 0;
    p->abs_x = 0.0;
    p->abs_y = 0.0;
    p->rel_x = 0.0;
    p->rel_y = 0.0;
    p->force_value = 1.0;
    p->force_maximum = 1.0;
    p->major_radius = 1.0;
    p->minor_radius = 1.0;
    p->size_value = 1.0;
    p->timestamp = 0;
}
MM_EXPORT_NWSI void mmEventTouch_Destroy(struct mmEventTouch* p)
{
    p->motion_id = 0;
    p->tap_count = 0;
    p->phase = mmTouchBegan;
    p->modifier_mask = 0;
    p->button_mask = 0;
    p->abs_x = 0.0;
    p->abs_y = 0.0;
    p->rel_x = 0.0;
    p->rel_y = 0.0;
    p->force_value = 1.0;
    p->force_maximum = 1.0;
    p->major_radius = 1.0;
    p->minor_radius = 1.0;
    p->size_value = 1.0;
    p->timestamp = 0;
}
MM_EXPORT_NWSI void mmEventTouch_Reset(struct mmEventTouch* p)
{
    p->motion_id = 0;
    p->tap_count = 0;
    p->phase = mmTouchBegan;
    p->modifier_mask = 0;
    p->button_mask = 0;
    p->abs_x = 0.0;
    p->abs_y = 0.0;
    p->rel_x = 0.0;
    p->rel_y = 0.0;
    p->force_value = 1.0;
    p->force_maximum = 1.0;
    p->major_radius = 1.0;
    p->minor_radius = 1.0;
    p->size_value = 1.0;
    p->timestamp = 0;
}

MM_EXPORT_NWSI void mmEventTouchs_Init(struct mmEventTouchs* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(&p->context);
    
    // vector value initialization.
    hValueAllocator.Produce = &mmEventTouch_Init;
    hValueAllocator.Recycle = &mmEventTouch_Destroy;
    mmVectorValue_SetAllocator(&p->context, &hValueAllocator);
    mmVectorValue_SetElement(&p->context, sizeof(struct mmEventTouch));
}
MM_EXPORT_NWSI void mmEventTouchs_Destroy(struct mmEventTouchs* p)
{
    mmVectorValue_Destroy(&p->context);
}
MM_EXPORT_NWSI void mmEventTouchs_Reset(struct mmEventTouchs* p)
{
    mmVectorValue_Clear(&p->context);
}
MM_EXPORT_NWSI void mmEventTouchs_AlignedMemory(struct mmEventTouchs* p, size_t size)
{
    mmVectorValue_AlignedMemory(&p->context, size);
}
MM_EXPORT_NWSI void mmEventTouchs_SetForIndex(struct mmEventTouchs* p, size_t index, struct mmEventTouch* touch)
{
    struct mmEventTouch* item = (struct mmEventTouch*)mmVectorValue_At(&p->context, index);
    *item = *touch;
}
MM_EXPORT_NWSI void mmEventTouchs_GetForIndex(struct mmEventTouchs* p, size_t index, struct mmEventTouch* touch)
{
    struct mmEventTouch* item = (struct mmEventTouch*)mmVectorValue_At(&p->context, index);
    *touch = *item;
}
MM_EXPORT_NWSI struct mmEventTouch* mmEventTouchs_GetForIndexReference(struct mmEventTouchs* p, size_t index)
{
    return (struct mmEventTouch*)mmVectorValue_At(&p->context, index);
}

MM_EXPORT_NWSI void mmEventEditText_Init(struct mmEventEditText* p)
{
    p->t = NULL;
    p->size = 0;
    p->capacity = 0;
    p->c = 0;
    p->l = 0;
    p->r = 0;
    p->v = mmEditTextWhole;
}
MM_EXPORT_NWSI void mmEventEditText_Destroy(struct mmEventEditText* p)
{
    p->t = NULL;
    p->size = 0;
    p->capacity = 0;
    p->c = 0;
    p->l = 0;
    p->r = 0;
    p->v = mmEditTextWhole;
}
MM_EXPORT_NWSI void mmEventEditText_Reset(struct mmEventEditText* p)
{
    p->t = NULL;
    p->size = 0;
    p->capacity = 0;
    p->c = 0;
    p->l = 0;
    p->r = 0;
    p->v = mmEditTextWhole;
}

MM_EXPORT_NWSI void mmEventEditString_Init(struct mmEventEditString* p)
{
    p->s = NULL;
    p->l = 0;
    p->o = 0;
    p->n = 0;
}
MM_EXPORT_NWSI void mmEventEditString_Destroy(struct mmEventEditString* p)
{
    p->s = NULL;
    p->l = 0;
    p->o = 0;
    p->n = 0;
}
MM_EXPORT_NWSI void mmEventEditString_Reset(struct mmEventEditString* p)
{
    p->s = NULL;
    p->l = 0;
    p->o = 0;
    p->n = 0;
}

MM_EXPORT_NWSI void mmEventKeypadStatus_Init(struct mmEventKeypadStatus* p)
{
    p->surface = NULL;
    mmMemset(p->screen_rect, 0, sizeof(double) * 4);
    mmMemset(p->safety_area, 0, sizeof(double) * 4);
    mmMemset(p->keypad_rect, 0, sizeof(double) * 4);
    mmMemset(p->window_rect, 0, sizeof(double) * 4);
    p->animation_duration = 0.0;
    p->animation_curve = mmKeypadAnimationCurveLinear;
    p->state = mmKeypadStatusShow;
    p->handle = 0;
}
MM_EXPORT_NWSI void mmEventKeypadStatus_Destroy(struct mmEventKeypadStatus* p)
{
    p->surface = NULL;
    mmMemset(p->screen_rect, 0, sizeof(double) * 4);
    mmMemset(p->safety_area, 0, sizeof(double) * 4);
    mmMemset(p->keypad_rect, 0, sizeof(double) * 4);
    mmMemset(p->window_rect, 0, sizeof(double) * 4);
    p->animation_duration = 0.0;
    p->animation_curve = mmKeypadAnimationCurveLinear;
    p->state = mmKeypadStatusShow;
    p->handle = 0;
}
MM_EXPORT_NWSI void mmEventKeypadStatus_Reset(struct mmEventKeypadStatus* p)
{
    p->surface = NULL;
    mmMemset(p->screen_rect, 0, sizeof(double) * 4);
    mmMemset(p->safety_area, 0, sizeof(double) * 4);
    mmMemset(p->keypad_rect, 0, sizeof(double) * 4);
    mmMemset(p->window_rect, 0, sizeof(double) * 4);
    p->animation_duration = 0.0;
    p->animation_curve = mmKeypadAnimationCurveLinear;
    p->state = mmKeypadStatusShow;
    p->handle = 0;
}

MM_EXPORT_NWSI void mmEventMetrics_Init(struct mmEventMetrics* p)
{
    p->surface = NULL;
    mmMemset(p->canvas_size, 0, sizeof(double) * 2);
    mmMemset(p->window_size, 0, sizeof(double) * 2);
    mmMemset(p->safety_area, 0, sizeof(double) * 4);
}
MM_EXPORT_NWSI void mmEventMetrics_Destroy(struct mmEventMetrics* p)
{
    mmMemset(p->safety_area, 0, sizeof(double) * 4);
    mmMemset(p->window_size, 0, sizeof(double) * 2);
    mmMemset(p->canvas_size, 0, sizeof(double) * 2);
    p->surface = NULL;
}
MM_EXPORT_NWSI void mmEventMetrics_Reset(struct mmEventMetrics* p)
{
    p->surface = NULL;
    mmMemset(p->canvas_size, 0, sizeof(double) * 2);
    mmMemset(p->window_size, 0, sizeof(double) * 2);
    mmMemset(p->safety_area, 0, sizeof(double) * 4);
}

MM_EXPORT_NWSI void mmEventUpdate_Init(struct mmEventUpdate* p)
{
    p->number = 0;
    p->average = 0.0;
    p->index = 0;
    p->interval = 0.0;
}
MM_EXPORT_NWSI void mmEventUpdate_Destroy(struct mmEventUpdate* p)
{
    p->interval = 0.0;
    p->index = 0;
    p->average = 0.0;
    p->number = 0;
}
MM_EXPORT_NWSI void mmEventUpdate_Reset(struct mmEventUpdate* p)
{
    p->number = 0;
    p->average = 0.0;
    p->index = 0;
    p->interval = 0.0;
}

MM_EXPORT_NWSI void mmEventSurfaceContent_Init(struct mmEventSurfaceContent* p)
{
    mmEventDragging_Init(&p->hDragging);
    mmEventKeypad_Init(&p->hKeypad);
    mmEventKeypadStatus_Init(&p->hKeypadStatus);
    mmEventCursor_Init(&p->hCursor);
    mmEventTouchs_Init(&p->hTouchs);
    mmEventMetrics_Init(&p->hMetrics);
    mmEventEditText_Init(&p->hEditText);
    mmEventEditString_Init(&p->hEditString);
}

MM_EXPORT_NWSI void mmEventSurfaceContent_Destroy(struct mmEventSurfaceContent* p)
{
    mmEventDragging_Destroy(&p->hDragging);
    mmEventKeypad_Destroy(&p->hKeypad);
    mmEventKeypadStatus_Destroy(&p->hKeypadStatus);
    mmEventCursor_Destroy(&p->hCursor);
    mmEventTouchs_Destroy(&p->hTouchs);
    mmEventMetrics_Destroy(&p->hMetrics);
    mmEventEditText_Destroy(&p->hEditText);
    mmEventEditString_Destroy(&p->hEditString);
}
MM_EXPORT_NWSI void mmEventSurfaceContent_Reset(struct mmEventSurfaceContent* p)
{
    mmEventDragging_Reset(&p->hDragging);
    mmEventKeypad_Reset(&p->hKeypad);
    mmEventKeypadStatus_Reset(&p->hKeypadStatus);
    mmEventCursor_Reset(&p->hCursor);
    mmEventTouchs_Reset(&p->hTouchs);
    mmEventMetrics_Reset(&p->hMetrics);
    mmEventEditText_Reset(&p->hEditText);
}

static void __static_mmISurfaceKeypad_OnSetKeypadType(struct mmISurface* pISurface, int hType)
{
    
}
static void __static_mmISurfaceKeypad_OnSetKeypadAppearance(struct mmISurface* pISurface, int hAppearance)
{
    
}
static void __static_mmISurfaceKeypad_OnSetKeypadReturnKey(struct mmISurface* pISurface, int hReturnKey)
{
    
}
static void __static_mmISurfaceKeypad_OnKeypadStatusShow(struct mmISurface* pISurface)
{

}
static void __static_mmISurfaceKeypad_OnKeypadStatusHide(struct mmISurface* pISurface)
{

}
MM_EXPORT_NWSI void mmISurfaceKeypad_Init(struct mmISurfaceKeypad* p)
{
    p->OnSetKeypadType = &__static_mmISurfaceKeypad_OnSetKeypadType;
    p->OnSetKeypadAppearance = &__static_mmISurfaceKeypad_OnSetKeypadAppearance;
    p->OnSetKeypadReturnKey = &__static_mmISurfaceKeypad_OnSetKeypadReturnKey;
    p->OnKeypadStatusShow = &__static_mmISurfaceKeypad_OnKeypadStatusShow;
    p->OnKeypadStatusHide = &__static_mmISurfaceKeypad_OnKeypadStatusHide;
}
MM_EXPORT_NWSI void mmISurfaceKeypad_Destroy(struct mmISurfaceKeypad* p)
{
    p->OnSetKeypadType = &__static_mmISurfaceKeypad_OnSetKeypadType;
    p->OnSetKeypadAppearance = &__static_mmISurfaceKeypad_OnSetKeypadAppearance;
    p->OnSetKeypadReturnKey = &__static_mmISurfaceKeypad_OnSetKeypadReturnKey;
    p->OnKeypadStatusShow = &__static_mmISurfaceKeypad_OnKeypadStatusShow;
    p->OnKeypadStatusHide = &__static_mmISurfaceKeypad_OnKeypadStatusHide;
}
MM_EXPORT_NWSI void mmISurfaceKeypad_Reset(struct mmISurfaceKeypad* p)
{
    p->OnSetKeypadType = &__static_mmISurfaceKeypad_OnSetKeypadType;
    p->OnSetKeypadAppearance = &__static_mmISurfaceKeypad_OnSetKeypadAppearance;
    p->OnSetKeypadReturnKey = &__static_mmISurfaceKeypad_OnSetKeypadReturnKey;
    p->OnKeypadStatusShow = &__static_mmISurfaceKeypad_OnKeypadStatusShow;
    p->OnKeypadStatusHide = &__static_mmISurfaceKeypad_OnKeypadStatusHide;
}
//
static void __static_mmISurface_OnDragging(struct mmISurface* p, struct mmEventDragging* content)
{
    
}

static void __static_mmISurface_OnKeypadPressed(struct mmISurface* p, struct mmEventKeypad* content)
{
    
}
static void __static_mmISurface_OnKeypadRelease(struct mmISurface* p, struct mmEventKeypad* content)
{
    
}
static void __static_mmISurface_OnKeypadStatus(struct mmISurface* p, struct mmEventKeypadStatus* content)
{
    
}

static void __static_mmISurface_OnCursorBegan(struct mmISurface* p, struct mmEventCursor* content)
{
    
}
static void __static_mmISurface_OnCursorMoved(struct mmISurface* p, struct mmEventCursor* content)
{
    
}
static void __static_mmISurface_OnCursorEnded(struct mmISurface* p, struct mmEventCursor* content)
{
    
}
static void __static_mmISurface_OnCursorBreak(struct mmISurface* p, struct mmEventCursor* content)
{
    
}
static void __static_mmISurface_OnCursorWheel(struct mmISurface* p, struct mmEventCursor* content)
{
    
}

static void __static_mmISurface_OnTouchsBegan(struct mmISurface* p, struct mmEventTouchs* content)
{
    
}
static void __static_mmISurface_OnTouchsMoved(struct mmISurface* p, struct mmEventTouchs* content)
{
    
}
static void __static_mmISurface_OnTouchsEnded(struct mmISurface* p, struct mmEventTouchs* content)
{
    
}
static void __static_mmISurface_OnTouchsBreak(struct mmISurface* p, struct mmEventTouchs* content)
{
    
}

static void __static_mmISurface_OnSurfacePrepare(struct mmISurface* p, struct mmEventMetrics* content)
{

}

static void __static_mmISurface_OnSurfaceDiscard(struct mmISurface* p, struct mmEventMetrics* content)
{

}

static void __static_mmISurface_OnSurfaceChanged(struct mmISurface* p, struct mmEventMetrics* content)
{
    
}

static void __static_mmISurface_OnGetText(struct mmISurface* p, struct mmEventEditText* content)
{

}
static void __static_mmISurface_OnSetText(struct mmISurface* p, struct mmEventEditText* content)
{

}
static void __static_mmISurface_OnGetTextEditRect(struct mmISurface* p, double rect[4])
{

}
static void __static_mmISurface_OnInsertTextUtf16(struct mmISurface* p, struct mmEventEditString* content)
{

}
static void __static_mmISurface_OnReplaceTextUtf16(struct mmISurface* p, struct mmEventEditString* content)
{

}
static void __static_mmISurface_OnInjectCopy(struct mmISurface* p)
{

}
static void __static_mmISurface_OnInjectCut(struct mmISurface* p)
{

}
static void __static_mmISurface_OnInjectPaste(struct mmISurface* p)
{

}

static void __static_mmISurface_OnUpdate(struct mmISurface* p)
{

}
static void __static_mmISurface_OnUpdateImmediately(struct mmISurface* p)
{

}
static void __static_mmISurface_OnTimewait(struct mmISurface* p)
{

}
static void __static_mmISurface_OnTimewake(struct mmISurface* p)
{

}

static void __static_mmISurface_OnStart(struct mmISurface* p)
{

}
static void __static_mmISurface_OnInterrupt(struct mmISurface* p)
{

}
static void __static_mmISurface_OnShutdown(struct mmISurface* p)
{

}
static void __static_mmISurface_OnJoin(struct mmISurface* p)
{

}

static void __static_mmISurface_OnFinishLaunching(struct mmISurface* p)
{

}
static void __static_mmISurface_OnBeforeTerminate(struct mmISurface* p)
{

}

static void __static_mmISurface_OnEnterBackground(struct mmISurface* p)
{
    
}
static void __static_mmISurface_OnEnterForeground(struct mmISurface* p)
{
    
}

MM_EXPORT_NWSI void mmISurface_Init(struct mmISurface* p)
{    
    p->OnDragging = &__static_mmISurface_OnDragging;
    
    p->OnKeypadPressed = &__static_mmISurface_OnKeypadPressed;
    p->OnKeypadRelease = &__static_mmISurface_OnKeypadRelease;
    p->OnKeypadStatus = &__static_mmISurface_OnKeypadStatus;
    
    p->OnCursorBegan = &__static_mmISurface_OnCursorBegan;
    p->OnCursorMoved = &__static_mmISurface_OnCursorMoved;
    p->OnCursorEnded = &__static_mmISurface_OnCursorEnded;
    p->OnCursorBreak = &__static_mmISurface_OnCursorBreak;
    p->OnCursorWheel = &__static_mmISurface_OnCursorWheel;
    
    p->OnTouchsBegan = &__static_mmISurface_OnTouchsBegan;
    p->OnTouchsMoved = &__static_mmISurface_OnTouchsMoved;
    p->OnTouchsEnded = &__static_mmISurface_OnTouchsEnded;
    p->OnTouchsBreak = &__static_mmISurface_OnTouchsBreak;
    
    p->OnSurfacePrepare = &__static_mmISurface_OnSurfacePrepare;
    p->OnSurfaceDiscard = &__static_mmISurface_OnSurfaceDiscard;
    p->OnSurfaceChanged = &__static_mmISurface_OnSurfaceChanged;

    p->OnGetText = &__static_mmISurface_OnGetText;
    p->OnSetText = &__static_mmISurface_OnSetText;
    p->OnGetTextEditRect = &__static_mmISurface_OnGetTextEditRect;
    p->OnInsertTextUtf16 = &__static_mmISurface_OnInsertTextUtf16;
    p->OnReplaceTextUtf16 = &__static_mmISurface_OnReplaceTextUtf16;

    p->OnInjectCopy = &__static_mmISurface_OnInjectCopy;
    p->OnInjectCut = &__static_mmISurface_OnInjectCut;
    p->OnInjectPaste = &__static_mmISurface_OnInjectPaste;
    
    p->OnUpdate = &__static_mmISurface_OnUpdate;
    p->OnUpdateImmediately = &__static_mmISurface_OnUpdateImmediately;
    p->OnTimewait = &__static_mmISurface_OnTimewait;
    p->OnTimewake = &__static_mmISurface_OnTimewake;
    
    p->OnStart = &__static_mmISurface_OnStart;
    p->OnInterrupt = &__static_mmISurface_OnInterrupt;
    p->OnShutdown = &__static_mmISurface_OnShutdown;
    p->OnJoin = &__static_mmISurface_OnJoin;

    p->OnFinishLaunching = &__static_mmISurface_OnFinishLaunching;
    p->OnBeforeTerminate = &__static_mmISurface_OnBeforeTerminate;
    
    p->OnEnterBackground = &__static_mmISurface_OnEnterBackground;
    p->OnEnterForeground = &__static_mmISurface_OnEnterForeground;
}
MM_EXPORT_NWSI void mmISurface_Destroy(struct mmISurface* p)
{
    p->OnDragging = &__static_mmISurface_OnDragging;

    p->OnKeypadPressed = &__static_mmISurface_OnKeypadPressed;
    p->OnKeypadRelease = &__static_mmISurface_OnKeypadRelease;
    p->OnKeypadStatus = &__static_mmISurface_OnKeypadStatus;

    p->OnCursorBegan = &__static_mmISurface_OnCursorBegan;
    p->OnCursorMoved = &__static_mmISurface_OnCursorMoved;    
    p->OnCursorEnded = &__static_mmISurface_OnCursorEnded;
    p->OnCursorBreak = &__static_mmISurface_OnCursorBreak;
    p->OnCursorWheel = &__static_mmISurface_OnCursorWheel;

    p->OnTouchsBegan = &__static_mmISurface_OnTouchsBegan;
    p->OnTouchsMoved = &__static_mmISurface_OnTouchsMoved;
    p->OnTouchsEnded = &__static_mmISurface_OnTouchsEnded;
    p->OnTouchsBreak = &__static_mmISurface_OnTouchsBreak;

    p->OnSurfacePrepare = &__static_mmISurface_OnSurfacePrepare;
    p->OnSurfaceDiscard = &__static_mmISurface_OnSurfaceDiscard;
    p->OnSurfaceChanged = &__static_mmISurface_OnSurfaceChanged;

    p->OnGetText = &__static_mmISurface_OnGetText;
    p->OnSetText = &__static_mmISurface_OnSetText;
    p->OnGetTextEditRect = &__static_mmISurface_OnGetTextEditRect;
    p->OnInsertTextUtf16 = &__static_mmISurface_OnInsertTextUtf16;
    p->OnReplaceTextUtf16 = &__static_mmISurface_OnReplaceTextUtf16;

    p->OnInjectCopy = &__static_mmISurface_OnInjectCopy;
    p->OnInjectCut = &__static_mmISurface_OnInjectCut;
    p->OnInjectPaste = &__static_mmISurface_OnInjectPaste;
    
    p->OnUpdate = &__static_mmISurface_OnUpdate;
    p->OnUpdateImmediately = &__static_mmISurface_OnUpdateImmediately;
    p->OnTimewait = &__static_mmISurface_OnTimewait;
    p->OnTimewake = &__static_mmISurface_OnTimewake;
    
    p->OnStart = &__static_mmISurface_OnStart;
    p->OnInterrupt = &__static_mmISurface_OnInterrupt;
    p->OnShutdown = &__static_mmISurface_OnShutdown;
    p->OnJoin = &__static_mmISurface_OnJoin;

    p->OnFinishLaunching = &__static_mmISurface_OnFinishLaunching;
    p->OnBeforeTerminate = &__static_mmISurface_OnBeforeTerminate;
    
    p->OnEnterBackground = &__static_mmISurface_OnEnterBackground;
    p->OnEnterForeground = &__static_mmISurface_OnEnterForeground;
}
