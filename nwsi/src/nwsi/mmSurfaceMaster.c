/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSurfaceMaster.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"

#include "parse/mmParseINI.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmEventSurface.h"

static void __static_mmSurfaceMaster_OnSurfacePrepare(struct mmSurfaceMaster* obj, struct mmEventArgs* args);
static void __static_mmSurfaceMaster_OnSurfaceDiscard(struct mmSurfaceMaster* obj, struct mmEventArgs* args);
static void __static_mmSurfaceMaster_OnSurfaceChanged(struct mmSurfaceMaster* obj, struct mmEventArgs* args);

static void __static_mmSurfaceMaster_AdaptiveTimerUnit_UpdateDisplay(void* obj, double interval);

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventDragging = "EventDragging";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventKeypadPressed = "EventKeypadPressed";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventKeypadRelease = "EventKeypadRelease";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventKeypadStatus = "EventKeypadStatus";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventCursorBegan = "EventCursorBegan";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventCursorMoved = "EventCursorMoved";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventCursorEnded = "EventCursorEnded";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventCursorBreak = "EventCursorBreak";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventCursorWheel = "EventCursorWheel";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventTouchsBegan = "EventTouchsBegan";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventTouchsMoved = "EventTouchsMoved";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventTouchsEnded = "EventTouchsEnded";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventTouchsBreak = "EventTouchsBreak";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventSurfacePrepare = "EventSurfacePrepare";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventSurfaceDiscard = "EventSurfaceDiscard";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventSurfaceChanged = "EventSurfaceChanged";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventInjectCopy = "EventInjectCopy";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventInjectCut = "EventInjectCut";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventInjectPaste = "EventInjectPaste";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventUpdatedDisplay = "EventUpdatedDisplay";

MM_EXPORT_NWSI const char* mmSurfaceMaster_EventEnterBackground = "EventEnterBackground";
MM_EXPORT_NWSI const char* mmSurfaceMaster_EventEnterForeground = "EventEnterForeground";

MM_EXPORT_NWSI void mmSurfaceMaster_Init(struct mmSurfaceMaster* p)
{
    mmISurface_Init(&p->hSuper);
    mmFrameScheduler_Init(&p->hFrameScheduler);
    mmEventUpdate_Init(&p->hUpdate);
    mmEventSet_Init(&p->hEventSet);

    p->pContextMaster = NULL;
    p->pIActivity = NULL;
    p->pISurfaceKeypad = NULL;

    p->pViewSurface = NULL;
    p->hDisplayFrequency = 60.0;
    p->hDisplayDensity = 1.0;
    mmMemset(p->hDisplayFrameSize, 0, sizeof(double) * 2);
    mmMemset(p->hPhysicalViewSize, 0, sizeof(double) * 2);
    mmMemset(p->hPhysicalSafeRect, 0, sizeof(double) * 4);
    p->hState = MM_TS_CLOSED;
    
    p->hSuper.OnDragging = &mmSurfaceMaster_OnDragging;
    p->hSuper.OnKeypadPressed = &mmSurfaceMaster_OnKeypadPressed;
    p->hSuper.OnKeypadRelease = &mmSurfaceMaster_OnKeypadRelease;
    p->hSuper.OnKeypadStatus = &mmSurfaceMaster_OnKeypadStatus;
    p->hSuper.OnCursorBegan = &mmSurfaceMaster_OnCursorBegan;
    p->hSuper.OnCursorMoved = &mmSurfaceMaster_OnCursorMoved;
    p->hSuper.OnCursorEnded = &mmSurfaceMaster_OnCursorEnded;
    p->hSuper.OnCursorBreak = &mmSurfaceMaster_OnCursorBreak;
    p->hSuper.OnCursorWheel = &mmSurfaceMaster_OnCursorWheel;
    p->hSuper.OnTouchsBegan = &mmSurfaceMaster_OnTouchsBegan;
    p->hSuper.OnTouchsMoved = &mmSurfaceMaster_OnTouchsMoved;
    p->hSuper.OnTouchsEnded = &mmSurfaceMaster_OnTouchsEnded;
    p->hSuper.OnTouchsBreak = &mmSurfaceMaster_OnTouchsBreak;
    p->hSuper.OnSurfacePrepare = &mmSurfaceMaster_OnSurfacePrepare;
    p->hSuper.OnSurfaceDiscard = &mmSurfaceMaster_OnSurfaceDiscard;
    p->hSuper.OnSurfaceChanged = &mmSurfaceMaster_OnSurfaceChanged;
    p->hSuper.OnGetText = &mmSurfaceMaster_OnGetText;
    p->hSuper.OnSetText = &mmSurfaceMaster_OnSetText;
    p->hSuper.OnGetTextEditRect = &mmSurfaceMaster_OnGetTextEditRect;
    p->hSuper.OnInsertTextUtf16 = &mmSurfaceMaster_OnInsertTextUtf16;
    p->hSuper.OnReplaceTextUtf16 = &mmSurfaceMaster_OnReplaceTextUtf16;
    p->hSuper.OnInjectCopy = &mmSurfaceMaster_OnInjectCopy;
    p->hSuper.OnInjectCut = &mmSurfaceMaster_OnInjectCut;
    p->hSuper.OnInjectPaste = &mmSurfaceMaster_OnInjectPaste;
    p->hSuper.OnUpdate = &mmSurfaceMaster_OnUpdate;
    p->hSuper.OnUpdateImmediately = &mmSurfaceMaster_OnUpdateImmediately;
    p->hSuper.OnTimewait = &mmSurfaceMaster_OnTimewait;
    p->hSuper.OnTimewake = &mmSurfaceMaster_OnTimewake;
    p->hSuper.OnStart = &mmSurfaceMaster_OnStart;
    p->hSuper.OnInterrupt = &mmSurfaceMaster_OnInterrupt;
    p->hSuper.OnShutdown = &mmSurfaceMaster_OnShutdown;
    p->hSuper.OnJoin = &mmSurfaceMaster_OnJoin;
    p->hSuper.OnFinishLaunching = &mmSurfaceMaster_OnFinishLaunching;
    p->hSuper.OnBeforeTerminate = &mmSurfaceMaster_OnBeforeTerminate;
    p->hSuper.OnEnterBackground = &mmSurfaceMaster_OnEnterBackground;
    p->hSuper.OnEnterForeground = &mmSurfaceMaster_OnEnterForeground;
}
MM_EXPORT_NWSI void mmSurfaceMaster_Destroy(struct mmSurfaceMaster* p)
{
    p->hState = MM_TS_CLOSED;
    mmMemset(p->hPhysicalSafeRect, 0, sizeof(double) * 4);
    mmMemset(p->hPhysicalViewSize, 0, sizeof(double) * 2);
    mmMemset(p->hDisplayFrameSize, 0, sizeof(double) * 2);
    p->hDisplayDensity = 1.0;
    p->hDisplayFrequency = 60.0;
    p->pViewSurface = NULL;

    p->pISurfaceKeypad = NULL;
    p->pIActivity = NULL;
    p->pContextMaster = NULL;
    mmEventSet_Destroy(&p->hEventSet);
    mmEventUpdate_Destroy(&p->hUpdate);
    mmFrameScheduler_Destroy(&p->hFrameScheduler);
    mmISurface_Destroy(&p->hSuper);
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetContextMaster(struct mmSurfaceMaster* p, struct mmContextMaster* pContextMaster)
{
    p->pContextMaster = pContextMaster;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetViewSurface(struct mmSurfaceMaster* p, void* pViewSurface)
{
    p->pViewSurface = pViewSurface;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetIActivity(struct mmSurfaceMaster* p, struct mmIActivity* pIActivity)
{
    p->pIActivity = pIActivity;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetISurfaceKeypad(struct mmSurfaceMaster* p, struct mmISurfaceKeypad* pISurfaceKeypad)
{
    p->pISurfaceKeypad = pISurfaceKeypad;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetDisplayFrequency(struct mmSurfaceMaster* p, double hDisplayFrequency)
{
    p->hDisplayFrequency = hDisplayFrequency;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetDisplayDensity(struct mmSurfaceMaster* p, double hDisplayDensity)
{
    p->hDisplayDensity = hDisplayDensity;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetDisplayFrameSize(struct mmSurfaceMaster* p, double hDisplayFrameSize[2])
{
    p->hDisplayFrameSize[0] = hDisplayFrameSize[0];
    p->hDisplayFrameSize[1] = hDisplayFrameSize[1];
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetPhysicalSafeRect(struct mmSurfaceMaster* p, double hPhysicalSafeRect[4])
{
    p->hPhysicalSafeRect[0] = hPhysicalSafeRect[0];
    p->hPhysicalSafeRect[1] = hPhysicalSafeRect[1];
    p->hPhysicalSafeRect[2] = hPhysicalSafeRect[2];
    p->hPhysicalSafeRect[3] = hPhysicalSafeRect[3];

    p->hPhysicalViewSize[0] = p->hDisplayFrameSize[0] / p->hDisplayDensity;
    p->hPhysicalViewSize[1] = p->hDisplayFrameSize[1] / p->hDisplayDensity;
}
MM_EXPORT_NWSI void mmSurfaceMaster_SetSmoothMode(struct mmSurfaceMaster* p, int mode)
{
    mmFrameScheduler_SetSmoothMode(&p->hFrameScheduler, mode);
}

MM_EXPORT_NWSI void mmSurfaceMaster_LoggerInfomation(struct mmSurfaceMaster* p, mmUInt32_t lvl, const char* pWindowName)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    char hString0[128] = { 0 };
    char hString2[128] = { 0 };
    char hString3[128] = { 0 };

    mmDoubleSizeToString(p->hDisplayFrameSize, hString0);
    mmDoubleSizeToString(p->hPhysicalViewSize, hString2);
    mmDoubleRectToString(p->hPhysicalSafeRect, hString3);

    //                              Window ----------------------------+----------------------------------
    //                              Window                  pWindowName: <WindowName>
    //                              Window              hDisplayDensity: 1.000000
    //                              Window            hDisplayFrameSize: (378.00, 672.00)
    //                              Window            hPhysicalViewSize: (378.00, 672.00)
    //                              Window            hPhysicalSafeRect: (0.00, 0.00, 378.00, 672.00)
    //                              Window ----------------------------+----------------------------------

    mmLogger_Message(gLogger, lvl, "Window ----------------------------+----------------------------------");
    mmLogger_Message(gLogger, lvl, "Window                  pWindowName: %s", pWindowName);
    mmLogger_Message(gLogger, lvl, "Window              hDisplayDensity: %lf", p->hDisplayDensity);
    mmLogger_Message(gLogger, lvl, "Window            hDisplayFrameSize: %s", hString0);
    mmLogger_Message(gLogger, lvl, "Window            hPhysicalViewSize: %s", hString2);
    mmLogger_Message(gLogger, lvl, "Window            hPhysicalSafeRect: %s", hString3);
    mmLogger_Message(gLogger, lvl, "Window ----------------------------+----------------------------------");
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnDisplayOneFrame(struct mmSurfaceMaster* p, double interval)
{    
    struct mmEventUpdate* pUpdate = &p->hUpdate;
    struct mmEventUpdateArgs hArgs;

    pUpdate->interval = interval;
    
    (*(p->pIActivity->OnDisplayOneFrame))(p->pIActivity, pUpdate);

    mmEventUpdateArgs_Reset(&hArgs);
    hArgs.content = pUpdate;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventUpdatedDisplay, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnDragging(struct mmISurface* pSuper, struct mmEventDragging* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventDraggingArgs hArgs;
    
    (*(p->pIActivity->OnDragging))(p->pIActivity, content);

    mmEventDraggingArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventDragging, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnKeypadPressed(struct mmISurface* pSuper, struct mmEventKeypad* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventKeypadArgs hArgs;
    
    (*(p->pIActivity->OnKeypadPressed))(p->pIActivity, content);

    mmEventKeypadArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventKeypadPressed, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnKeypadRelease(struct mmISurface* pSuper, struct mmEventKeypad* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventKeypadArgs hArgs;
    
    (*(p->pIActivity->OnKeypadRelease))(p->pIActivity, content);
    
    mmEventKeypadArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventKeypadRelease, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnKeypadStatus(struct mmISurface* pSuper, struct mmEventKeypadStatus* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventKeypadStatusArgs hArgs;
    
    (*(p->pIActivity->OnKeypadStatus))(p->pIActivity, content);

    mmEventKeypadStatusArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventKeypadStatus, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorBegan(struct mmISurface* pSuper, struct mmEventCursor* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventCursorArgs hArgs;
    
    (*(p->pIActivity->OnCursorBegan))(p->pIActivity, content);
    
    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventCursorBegan, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorMoved(struct mmISurface* pSuper, struct mmEventCursor* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventCursorArgs hArgs;

    (*(p->pIActivity->OnCursorMoved))(p->pIActivity, content);
    
    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventCursorMoved, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorEnded(struct mmISurface* pSuper, struct mmEventCursor* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventCursorArgs hArgs;

    (*(p->pIActivity->OnCursorEnded))(p->pIActivity, content);
    
    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventCursorEnded, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorBreak(struct mmISurface* pSuper, struct mmEventCursor* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventCursorArgs hArgs;

    (*(p->pIActivity->OnCursorBreak))(p->pIActivity, content);

    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventCursorBreak, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnCursorWheel(struct mmISurface* pSuper, struct mmEventCursor* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventCursorArgs hArgs;

    (*(p->pIActivity->OnCursorWheel))(p->pIActivity, content);

    mmEventCursorArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventCursorWheel, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsBegan(struct mmISurface* pSuper, struct mmEventTouchs* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventTouchsArgs hArgs;
    
    (*(p->pIActivity->OnTouchsBegan))(p->pIActivity, content);

    mmEventTouchsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventTouchsBegan, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsMoved(struct mmISurface* pSuper, struct mmEventTouchs* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventTouchsArgs hArgs;

    (*(p->pIActivity->OnTouchsMoved))(p->pIActivity, content);

    mmEventTouchsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventTouchsMoved, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsEnded(struct mmISurface* pSuper, struct mmEventTouchs* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventTouchsArgs hArgs;

    (*(p->pIActivity->OnTouchsEnded))(p->pIActivity, content);

    mmEventTouchsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventTouchsEnded, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnTouchsBreak(struct mmISurface* pSuper, struct mmEventTouchs* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventTouchsArgs hArgs;

    (*(p->pIActivity->OnTouchsBreak))(p->pIActivity, content);

    mmEventTouchsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventTouchsBreak, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnSurfacePrepare(struct mmISurface* pSuper, struct mmEventMetrics* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventMetricsArgs hArgs;

    mmEventMetricsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventSurfacePrepare, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnSurfaceDiscard(struct mmISurface* pSuper, struct mmEventMetrics* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventMetricsArgs hArgs;

    mmEventMetricsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventSurfaceDiscard, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnSurfaceChanged(struct mmISurface* pSuper, struct mmEventMetrics* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventMetricsArgs hArgs;

    mmEventMetricsArgs_Reset(&hArgs);
    hArgs.content = content;
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventSurfaceChanged, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnGetText(struct mmISurface* pSuper, struct mmEventEditText* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    (*(p->pIActivity->OnGetText))(p->pIActivity, content);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnSetText(struct mmISurface* pSuper, struct mmEventEditText* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    (*(p->pIActivity->OnSetText))(p->pIActivity, content);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnGetTextEditRect(struct mmISurface* pSuper, double rect[4])
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    (*(p->pIActivity->OnGetTextEditRect))(p->pIActivity, rect);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnInsertTextUtf16(struct mmISurface* pSuper, struct mmEventEditString* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);

    (*(p->pIActivity->OnInsertTextUtf16))(p->pIActivity, content);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnReplaceTextUtf16(struct mmISurface* pSuper, struct mmEventEditString* content)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);

    (*(p->pIActivity->OnReplaceTextUtf16))(p->pIActivity, content);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnInjectCopy(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventArgs hArgs;
    
    (*(p->pIActivity->OnInjectCopy))(p->pIActivity);

    mmEventArgs_Reset(&hArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventInjectCopy, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnInjectCut(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventArgs hArgs;
    
    (*(p->pIActivity->OnInjectCut))(p->pIActivity);

    mmEventArgs_Reset(&hArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventInjectCut, &hArgs);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnInjectPaste(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventArgs hArgs;
    
    (*(p->pIActivity->OnInjectPaste))(p->pIActivity);

    mmEventArgs_Reset(&hArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventInjectPaste, &hArgs);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnUpdate(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    mmFrameScheduler_Update(&p->hFrameScheduler);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnUpdateImmediately(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    mmFrameScheduler_UpdateImmediately(&p->hFrameScheduler);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnTimewait(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    mmFrameScheduler_Timewait(&p->hFrameScheduler);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnTimewake(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);    
    
    mmFrameScheduler_Timewake(&p->hFrameScheduler);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnStart(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    p->hState = MM_TS_FINISH == p->hState ? MM_TS_CLOSED : MM_TS_MOTION;
    mmFrameScheduler_Start(&p->hFrameScheduler);
    (*(p->pIActivity->OnStart))(p->pIActivity);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnInterrupt(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    p->hState = MM_TS_CLOSED;
    (*(p->pIActivity->OnInterrupt))(p->pIActivity);
    mmFrameScheduler_Interrupt(&p->hFrameScheduler);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnShutdown(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    p->hState = MM_TS_FINISH;
    (*(p->pIActivity->OnShutdown))(p->pIActivity);
    mmFrameScheduler_Shutdown(&p->hFrameScheduler);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnJoin(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    
    (*(p->pIActivity->OnJoin))(p->pIActivity);
    mmFrameScheduler_Join(&p->hFrameScheduler);    
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnFinishLaunching(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    //struct mmContextMaster* pContextMaster = p->pContextMaster;
    struct mmEventSet* pEventSet = &p->hEventSet;

    struct mmLogger* gLogger = mmLogger_Instance();
    
    struct mmFrameTimerCallback hFrameTimerCallback;

    hFrameTimerCallback.obj = (void*)p;
    hFrameTimerCallback.Update = &__static_mmSurfaceMaster_AdaptiveTimerUnit_UpdateDisplay;
    mmFrameScheduler_SetFrequency(&p->hFrameScheduler, p->hDisplayFrequency);
    mmFrameScheduler_SetBackground(&p->hFrameScheduler, MM_FRAME_TIMER_BACKGROUND_PAUSED);
    mmFrameScheduler_SetActive(&p->hFrameScheduler, 1);
    mmFrameScheduler_SetCallback(&p->hFrameScheduler, &hFrameTimerCallback);

    mmLogger_LogI(gLogger, "%s %d DisplayFrequency: %lf.", __FUNCTION__, __LINE__, p->hDisplayFrequency);

    // Surface life cycle must less Activity.
    mmEventSet_SubscribeEvent(pEventSet,
        mmSurfaceMaster_EventSurfacePrepare,
        &__static_mmSurfaceMaster_OnSurfacePrepare, p);

    mmEventSet_SubscribeEvent(pEventSet,
        mmSurfaceMaster_EventSurfaceDiscard,
        &__static_mmSurfaceMaster_OnSurfaceDiscard, p);

    mmEventSet_SubscribeEvent(pEventSet,
        mmSurfaceMaster_EventSurfaceChanged,
        &__static_mmSurfaceMaster_OnSurfaceChanged, p);

    (*(p->pIActivity->OnFinishLaunching))(p->pIActivity);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnBeforeTerminate(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventSet* pEventSet = &p->hEventSet;

    (*(p->pIActivity->OnBeforeTerminate))(p->pIActivity);
        
    mmEventSet_UnSubscribeEvent(pEventSet,
        mmSurfaceMaster_EventSurfaceChanged,
        &__static_mmSurfaceMaster_OnSurfaceChanged, p);

    mmEventSet_UnSubscribeEvent(pEventSet,
        mmSurfaceMaster_EventSurfaceDiscard,
        &__static_mmSurfaceMaster_OnSurfaceDiscard, p);

    mmEventSet_UnSubscribeEvent(pEventSet,
        mmSurfaceMaster_EventSurfacePrepare,
        &__static_mmSurfaceMaster_OnSurfacePrepare, p);
}

MM_EXPORT_NWSI void mmSurfaceMaster_OnEnterBackground(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventArgs hEventArgs;
    struct mmLogger* gLogger = mmLogger_Instance();

    mmLogger_LogI(gLogger, "mmSurfaceMaster.OnEnterBackground.");

    mmEventArgs_Reset(&hEventArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventEnterBackground, &hEventArgs);

    mmFrameScheduler_EnterBackground(&p->hFrameScheduler);    

    (*(p->pIActivity->OnEnterBackground))(p->pIActivity);
}
MM_EXPORT_NWSI void mmSurfaceMaster_OnEnterForeground(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(pSuper);
    struct mmEventArgs hEventArgs;
    struct mmLogger* gLogger = mmLogger_Instance();

    (*(p->pIActivity->OnEnterForeground))(p->pIActivity);        

    mmFrameScheduler_EnterForeground(&p->hFrameScheduler);

    mmEventArgs_Reset(&hEventArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmSurfaceMaster_EventEnterForeground, &hEventArgs);

    mmLogger_LogI(gLogger, "mmSurfaceMaster.OnEnterForeground.");
}

static void __static_mmSurfaceMaster_OnSurfacePrepare(struct mmSurfaceMaster* p, struct mmEventArgs* args)
{
    struct mmEventMetricsArgs* pArgs = (struct mmEventMetricsArgs*)(args);
    (*(p->pIActivity->OnSurfacePrepare))(p->pIActivity, pArgs->content);
}
static void __static_mmSurfaceMaster_OnSurfaceDiscard(struct mmSurfaceMaster* p, struct mmEventArgs* args)
{
    struct mmEventMetricsArgs* pArgs = (struct mmEventMetricsArgs*)(args);
    (*(p->pIActivity->OnSurfaceDiscard))(p->pIActivity, pArgs->content);
}
static void __static_mmSurfaceMaster_OnSurfaceChanged(struct mmSurfaceMaster* p, struct mmEventArgs* args)
{
    struct mmEventMetricsArgs* pArgs = (struct mmEventMetricsArgs*)(args);
    (*(p->pIActivity->OnSurfaceChanged))(p->pIActivity, pArgs->content);
}

static void __static_mmSurfaceMaster_AdaptiveTimerUnit_UpdateDisplay(void* obj, double interval)
{
    struct mmFrameTimer* unit = (struct mmFrameTimer*)(obj);
    struct mmSurfaceMaster* p = (struct mmSurfaceMaster*)(unit->callback.obj);
    struct mmFrameStats* status = &unit->status;
    struct mmEventUpdate* pUpdate = &p->hUpdate;
    pUpdate->number = status->number;
    pUpdate->average = status->average;
    pUpdate->index = status->index;
    mmSurfaceMaster_OnDisplayOneFrame(p, interval);
}

