/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmAppManifest.h"

#include "core/mmLogger.h"
#include "core/mmValueTransform.h"

#include "parse/mmParseINI.h"

#include "dish/mmFileContext.h"

MM_EXPORT_NWSI void mmAppManifest_Init(struct mmAppManifest* p)
{
    mmString_Init(&p->AssetsRootFolderPath);
    mmString_Init(&p->AssetsRootFolderBase);
    mmString_Init(&p->AssetsRootSourcePath);
    mmString_Init(&p->AssetsRootSourceBase);
    mmString_Init(&p->ShaderCacheFolder);
    mmString_Init(&p->TextureCacheFolder);
    mmString_Init(&p->PluginFolder);
    mmString_Init(&p->DynlibFolder);
    mmString_Init(&p->RenderSystemName);
    mmString_Init(&p->LoggerFileName);
    mmString_Init(&p->LoggerPathName);
    p->DisplayFrequency = 60.0;
    p->FrameScale = 1.0;
    p->AssetsRootType = MM_FILE_ASSETS_TYPE_FOLDER;
    p->LoggerLevel = MM_LOG_INFO;
    p->CanvasRect[0] = 0;
    p->CanvasRect[1] = 0;
    p->CanvasRect[2] = 378;
    p->CanvasRect[3] = 672;

    mmString_Assigns(&p->AssetsRootFolderPath, "./resources");
    mmString_Assigns(&p->AssetsRootFolderBase, "assets");
    mmString_Assigns(&p->AssetsRootSourcePath, "");
    mmString_Assigns(&p->AssetsRootSourceBase, "");
    mmString_Assigns(&p->ShaderCacheFolder, "shader");
    mmString_Assigns(&p->TextureCacheFolder, "texture");
    mmString_Assigns(&p->PluginFolder, "");
    mmString_Assigns(&p->DynlibFolder, "");
    mmString_Assigns(&p->RenderSystemName, "OpenGL ES 2.x Rendering Subsystem");
    mmString_Assigns(&p->LoggerFileName, "nwsi");
    mmString_Assigns(&p->LoggerPathName, "log");
}
MM_EXPORT_NWSI void mmAppManifest_Destroy(struct mmAppManifest* p)
{
    mmString_Destroy(&p->AssetsRootFolderPath);
    mmString_Destroy(&p->AssetsRootFolderBase);
    mmString_Destroy(&p->AssetsRootSourcePath);
    mmString_Destroy(&p->AssetsRootSourceBase);
    mmString_Destroy(&p->ShaderCacheFolder);
    mmString_Destroy(&p->TextureCacheFolder);
    mmString_Destroy(&p->PluginFolder);
    mmString_Destroy(&p->DynlibFolder);
    mmString_Destroy(&p->RenderSystemName);
    mmString_Destroy(&p->LoggerFileName);
    mmString_Destroy(&p->LoggerPathName);
    p->DisplayFrequency = 60.0;
    p->FrameScale = 1.0;
    p->AssetsRootType = MM_FILE_ASSETS_TYPE_FOLDER;
    p->LoggerLevel = MM_LOG_INFO;
    p->CanvasRect[0] = 0;
    p->CanvasRect[1] = 0;
    p->CanvasRect[2] = 378;
    p->CanvasRect[3] = 672;
}

MM_EXPORT_NWSI void mmAppManifest_Analysis(struct mmAppManifest* p, const char* pFileName)
{
    const char* pValue = NULL;

    struct mmParseINI hINI;

    mmParseINI_Init(&hINI);
    mmParseINI_SetFileName(&hINI, pFileName);
    mmParseINI_Analysis(&hINI);

    pValue = mmParseINI_GetValue(&hINI, "AssetsRootFolderPath");
    mmString_Assigns(&p->AssetsRootFolderPath, pValue);

    pValue = mmParseINI_GetValue(&hINI, "AssetsRootFolderBase");
    mmString_Assigns(&p->AssetsRootFolderBase, pValue);

    pValue = mmParseINI_GetValue(&hINI, "AssetsRootSourcePath");
    mmString_Assigns(&p->AssetsRootSourcePath, pValue);

    pValue = mmParseINI_GetValue(&hINI, "AssetsRootSourceBase");
    mmString_Assigns(&p->AssetsRootSourceBase, pValue);

    pValue = mmParseINI_GetValue(&hINI, "ShaderCacheFolder");
    mmString_Assigns(&p->ShaderCacheFolder, pValue);

    pValue = mmParseINI_GetValue(&hINI, "TextureCacheFolder");
    mmString_Assigns(&p->TextureCacheFolder, pValue);

    pValue = mmParseINI_GetValue(&hINI, "PluginFolder");
    mmString_Assigns(&p->PluginFolder, pValue);

    pValue = mmParseINI_GetValue(&hINI, "DynlibFolder");
    mmString_Assigns(&p->DynlibFolder, pValue);

    pValue = mmParseINI_GetValue(&hINI, "RenderSystemName");
    mmString_Assigns(&p->RenderSystemName, pValue);

    pValue = mmParseINI_GetValue(&hINI, "LoggerFileName");
    mmString_Assigns(&p->LoggerFileName, pValue);

    pValue = mmParseINI_GetValue(&hINI, "LoggerPathName");
    mmString_Assigns(&p->LoggerPathName, pValue);

    pValue = mmParseINI_GetValue(&hINI, "DisplayFrequency");
    mmValue_AToDouble(pValue, &p->DisplayFrequency);

    pValue = mmParseINI_GetValue(&hINI, "FrameScale");
    mmValue_AToDouble(pValue, &p->FrameScale);

    pValue = mmParseINI_GetValue(&hINI, "AssetsRootType");
    mmValue_AToUInt32(pValue, &p->AssetsRootType);

    pValue = mmParseINI_GetValue(&hINI, "LoggerLevel");
    mmValue_AToUInt32(pValue, &p->LoggerLevel);

    pValue = mmParseINI_GetValue(&hINI, "CanvasRect.x");
    mmValue_AToUInt32(pValue, &p->CanvasRect[0]);

    pValue = mmParseINI_GetValue(&hINI, "CanvasRect.y");
    mmValue_AToUInt32(pValue, &p->CanvasRect[1]);

    pValue = mmParseINI_GetValue(&hINI, "CanvasRect.w");
    mmValue_AToUInt32(pValue, &p->CanvasRect[2]);

    pValue = mmParseINI_GetValue(&hINI, "CanvasRect.h");
    mmValue_AToUInt32(pValue, &p->CanvasRect[3]);

    mmParseINI_Destroy(&hINI);
}
