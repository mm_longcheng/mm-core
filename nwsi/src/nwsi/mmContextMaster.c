/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmContextMaster.h"

#include "core/mmAlloc.h"
#include "core/mmThread.h"
#include "core/mmLogger.h"
#include "core/mmLoggerManager.h"
#include "core/mmContext.h"
#include "core/mmTimeCache.h"
#include "core/mmContext.h"
#include "core/mmFilePath.h"

MM_EXPORT_NWSI const char* mmContextMaster_EventBattery = "EventBattery";
MM_EXPORT_NWSI const char* mmContextMaster_EventLocation = "EventLocation";
MM_EXPORT_NWSI const char* mmContextMaster_EventSensor = "EventSensor";
MM_EXPORT_NWSI const char* mmContextMaster_EventSignal = "EventSignal";

MM_EXPORT_NWSI const char* mmContextMaster_EventOrientation = "EventOrientation";

MM_EXPORT_NWSI const char* mmContextMaster_EventEnterBackground = "EventEnterBackground";
MM_EXPORT_NWSI const char* mmContextMaster_EventEnterForeground = "EventEnterForeground";

MM_EXPORT_NWSI void mmContextMaster_Init(struct mmContextMaster* p)
{
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();

    mmIContext_Init(&p->hSuper);
    mmArgument_Init(&p->hArgument);
    mmFileContext_Init(&p->hFileContext);
    mmDevice_Init(&p->hDevice);
    mmPackageAssets_Init(&p->hPackageAssets);
    mmAppIdentifier_Init(&p->hAppIdentifier);
    mmSecurityStore_Init(&p->hSecurityStore);
    mmGraphicsFactory_Init(&p->hGraphicsFactory);
    mmFontsSource_Init(&p->hFontsSource);
    mmModuleSet_Init(&p->hModuleSet);
    mmEventSet_Init(&p->hEventSet);
    mmClipboard_Init(&p->hClipboard);
    p->pAppHandler = NULL;
    p->hState = MM_TS_CLOSED;
    
    mmContext_Init(gContext);
    mmTimeCache_Init(gTimeCache);
    
    mmGraphicsFactory_SetPackageAssets(&p->hGraphicsFactory, &p->hPackageAssets);

    mmFontsSource_SetFileContext(&p->hFontsSource, &p->hFileContext);

    mmModuleSet_SetObject(&p->hModuleSet, p);
    
    p->hSuper.OnStart = &mmContextMaster_OnStart;
    p->hSuper.OnInterrupt = &mmContextMaster_OnInterrupt;
    p->hSuper.OnShutdown = &mmContextMaster_OnShutdown;
    p->hSuper.OnJoin = &mmContextMaster_OnJoin;
    p->hSuper.OnFinishLaunching = &mmContextMaster_OnFinishLaunching;
    p->hSuper.OnBeforeTerminate = &mmContextMaster_OnBeforeTerminate;
    p->hSuper.OnEnterBackground = &mmContextMaster_OnEnterBackground;
    p->hSuper.OnEnterForeground = &mmContextMaster_OnEnterForeground;
}
MM_EXPORT_NWSI void mmContextMaster_Destroy(struct mmContextMaster* p)
{
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    
    mmTimeCache_Destroy(gTimeCache);
    mmContext_Destroy(gContext);
    
    p->hState = MM_TS_CLOSED;
    p->pAppHandler = NULL;

    mmClipboard_Destroy(&p->hClipboard);
    mmEventSet_Destroy(&p->hEventSet);
    mmModuleSet_Destroy(&p->hModuleSet);
    mmFontsSource_Destroy(&p->hFontsSource);
    mmGraphicsFactory_Destroy(&p->hGraphicsFactory);
    mmSecurityStore_Destroy(&p->hSecurityStore);
    mmAppIdentifier_Destroy(&p->hAppIdentifier);
    mmPackageAssets_Destroy(&p->hPackageAssets);
    mmDevice_Destroy(&p->hDevice);
    mmFileContext_Destroy(&p->hFileContext);
    mmArgument_Destroy(&p->hArgument);
    mmIContext_Destroy(&p->hSuper);
}

MM_EXPORT_NWSI void mmContextMaster_SetArgument(struct mmContextMaster* p, int argc, const char** argv)
{
    p->hArgument.argc = argc;
    p->hArgument.argv = argv;
}

MM_EXPORT_NWSI void mmContextMaster_SetAppHandler(struct mmContextMaster* p, void* pAppHandler)
{
    p->pAppHandler = pAppHandler;
    mmPackageAssets_SetAppHandler(&p->hPackageAssets, p->pAppHandler);
}
MM_EXPORT_NWSI void mmContextMaster_SetModuleName(struct mmContextMaster* p, const char* pModuleName)
{
    mmPackageAssets_SetModuleName(&p->hPackageAssets, pModuleName);
}
MM_EXPORT_NWSI void mmContextMaster_SetShaderCacheFolder(struct mmContextMaster* p, const char* pFolder)
{
    //mmOgreSystem_SetShaderCacheFolder(&p->hOgreSystem, pFolder);
}

MM_EXPORT_NWSI void mmContextMaster_SetRenderSystemName(struct mmContextMaster* p, const char* pRenderSystemName)
{
    //mmOgreSystem_SetRenderSystemName(&p->hOgreSystem, pRenderSystemName);
}
MM_EXPORT_NWSI void mmContextMaster_SetTextureCacheEnabled(struct mmContextMaster* p, mmBool_t hTextureCacheEnabled)
{
    //mmCEGUISystem_SetTextureCacheEnabled(&p->hCEGUISystem, hTextureCacheEnabled);
}
MM_EXPORT_NWSI void mmContextMaster_SetClipboardProvider(struct mmContextMaster* p, struct mmClipboardProvider* pClipboardProvider)
{
    mmClipboard_SetProvider(&p->hClipboard, pClipboardProvider);
}
MM_EXPORT_NWSI void mmContextMaster_OnStart(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    
    mmLogger_LogI(gLogger, "%s begin %d.", __FUNCTION__, __LINE__);

    p->hState = MM_TS_FINISH == p->hState ? MM_TS_CLOSED : MM_TS_MOTION;
    mmContext_Start(gContext);
    mmTimeCache_Start(gTimeCache);
    mmModuleSet_OnStart(&p->hModuleSet);

    mmLogger_LogI(gLogger, "%s end %d.", __FUNCTION__, __LINE__);
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
MM_EXPORT_NWSI void mmContextMaster_OnInterrupt(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    
    p->hState = MM_TS_CLOSED;
    mmModuleSet_OnInterrupt(&p->hModuleSet);
    mmTimeCache_Interrupt(gTimeCache);
    mmContext_Interrupt(gContext);
    
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
MM_EXPORT_NWSI void mmContextMaster_OnShutdown(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    
    p->hState = MM_TS_FINISH;
    mmModuleSet_OnShutdown(&p->hModuleSet);
    mmTimeCache_Shutdown(gTimeCache);
    mmContext_Shutdown(gContext);
    
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}
MM_EXPORT_NWSI void mmContextMaster_OnJoin(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContext* gContext = mmContext_Instance();
    struct mmTimeCache* gTimeCache = mmTimeCache_Instance();
    
    mmModuleSet_OnJoin(&p->hModuleSet);
    mmTimeCache_Join(gTimeCache);
    mmContext_Join(gContext);
    
    mmLogger_LogI(gLogger, "%s %d.", __FUNCTION__, __LINE__);
}

MM_EXPORT_NWSI void mmContextMaster_OnFinishLaunching(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    struct mmPackageAssets* pPackageAssets = &p->hPackageAssets;
    
    const char* pAssetsRootFolderPath = mmString_CStr(&pPackageAssets->hAssetsRootFolderPath);
    const char* pAssetsRootFolderBase = mmString_CStr(&pPackageAssets->hAssetsRootFolderBase);
    const char* pAssetsRootSourcePath = mmString_CStr(&pPackageAssets->hAssetsRootSourcePath);
    const char* pAssetsRootSourceBase = mmString_CStr(&pPackageAssets->hAssetsRootSourceBase);
        
    mmFileContext_SetAssetsRootFolder(&p->hFileContext, pAssetsRootFolderPath, pAssetsRootFolderBase);
    mmFileContext_SetAssetsRootSource(&p->hFileContext, pAssetsRootSourcePath, pAssetsRootSourceBase);
    
    mmDevice_OnFinishLaunching(&p->hDevice);

    mmPackageAssets_OnFinishLaunching(&p->hPackageAssets);

    mmAppIdentifier_SetAppId(&p->hAppIdentifier, mmString_CStr(&pPackageAssets->hPackageName));
    mmAppIdentifier_OnFinishLaunching(&p->hAppIdentifier);
    
    mmSecurityStore_SetAppId(&p->hSecurityStore, mmString_CStr(&pPackageAssets->hPackageName));
    mmSecurityStore_OnFinishLaunching(&p->hSecurityStore);

    mmGraphicsFactory_OnFinishLaunching(&p->hGraphicsFactory);

    mmFontsSource_OnFinishLaunching(&p->hFontsSource);

    mmModuleSet_OnFinishLaunching(&p->hModuleSet);
}
MM_EXPORT_NWSI void mmContextMaster_OnBeforeTerminate(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    mmModuleSet_OnBeforeTerminate(&p->hModuleSet);
    
    mmFontsSource_OnBeforeTerminate(&p->hFontsSource);
    
    mmGraphicsFactory_OnBeforeTerminate(&p->hGraphicsFactory);

    mmSecurityStore_OnBeforeTerminate(&p->hSecurityStore);
    mmAppIdentifier_OnBeforeTerminate(&p->hAppIdentifier);
    mmPackageAssets_OnBeforeTerminate(&p->hPackageAssets);
    mmDevice_OnBeforeTerminate(&p->hDevice);
}
MM_EXPORT_NWSI void mmContextMaster_OnEnterBackground(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);
    
    mmModuleSet_OnEnterBackground(&p->hModuleSet);

    struct mmEventArgs hEventArgs;
    mmEventArgs_Reset(&hEventArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmContextMaster_EventEnterBackground, &hEventArgs);
}
MM_EXPORT_NWSI void mmContextMaster_OnEnterForeground(struct mmIContext* pSuper)
{
    struct mmContextMaster* p = (struct mmContextMaster*)(pSuper);

    mmModuleSet_OnEnterForeground(&p->hModuleSet);
    
    struct mmEventArgs hEventArgs;
    mmEventArgs_Reset(&hEventArgs);
    mmEventSet_FireEvent(&p->hEventSet, mmContextMaster_EventEnterForeground, &hEventArgs);
}
