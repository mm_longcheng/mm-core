#include "mmModuleSet.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include <assert.h>

MM_EXPORT_NWSI void mmModule_Init(struct mmModule* p)
{
    p->metadata = NULL;
    p->object = NULL;
}
MM_EXPORT_NWSI void mmModule_Destroy(struct mmModule* p)
{
    p->object = NULL;
    p->metadata = NULL;
}

static void mmModuleSet_OnHandlerEventNext(const struct mmModuleSet* p, size_t f);
static void mmModuleSet_OnHandlerEventPrev(const struct mmModuleSet* p, size_t f);

MM_EXPORT_NWSI void mmModuleSet_Init(struct mmModuleSet* p)
{
    mmRbtreeStringVpt_Init(&p->rbtree);
    mmListVpt_Init(&p->list);
    p->object = NULL;
}
MM_EXPORT_NWSI void mmModuleSet_Destroy(struct mmModuleSet* p)
{
    assert(0 == p->rbtree.size && "you must remove all data before destroy.");
    p->object = NULL;
    mmListVpt_Destroy(&p->list);
    mmRbtreeStringVpt_Destroy(&p->rbtree);
}
MM_EXPORT_NWSI void mmModuleSet_AddType(struct mmModuleSet* p, const struct mmModuleMetadata* m)
{
    struct mmString hWeakKey;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmString_MakeWeaks(&hWeakKey, m->TypeName);
    it = mmRbtreeStringVpt_GetIterator(&p->rbtree, &hWeakKey);
    if (NULL == it)
    {
        typedef void(*ProduceFuncType)(void* e);
        struct mmModule* pModule = (struct mmModule*)mmMalloc(m->TypeSize);
        (*((ProduceFuncType)(m->Produce)))(pModule);
        pModule->object = p->object;
        mmRbtreeStringVpt_Set(&p->rbtree, &hWeakKey, pModule);
        mmListVpt_AddTail(&p->list, (void*)pModule);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d you can not add type twice.", __FUNCTION__, __LINE__);
    }
}

MM_EXPORT_NWSI void mmModuleSet_RmvType(struct mmModuleSet* p, const struct mmModuleMetadata* m)
{
    struct mmString hWeakKey;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmString_MakeWeaks(&hWeakKey, m->TypeName);
    it = mmRbtreeStringVpt_GetIterator(&p->rbtree, &hWeakKey);
    if (NULL != it)
    {
        typedef void(*RecycleFuncType)(void* e);
        struct mmModule* pModule = (struct mmModule*)(it->v);
        mmListVpt_Remove(&p->list, pModule);
        mmRbtreeStringVpt_Erase(&p->rbtree, it);
        (*((RecycleFuncType)(m->Recycle)))(pModule);
        mmFree(pModule);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogW(gLogger, "%s %d you can not rmv type does not exist.", __FUNCTION__, __LINE__);
    }
}
MM_EXPORT_NWSI void mmModuleSet_SetObject(struct mmModuleSet* p, void* object)
{
    p->object = object;
    mmModuleSet_OnSetObject(p, object);
}
MM_EXPORT_NWSI struct mmModule* mmModuleSet_GetModule(const struct mmModuleSet* p, const char* pTypeName)
{
    struct mmModule* u = NULL;
    struct mmString hWeakKey;
    struct mmRbtreeStringVptIterator* it = NULL;
    mmString_MakeWeaks(&hWeakKey, pTypeName);
    it = mmRbtreeStringVpt_GetIterator(&p->rbtree, &hWeakKey);
    if (NULL != it)
    {
        u = (struct mmModule*)(it->v);
    }
    return u;
}

MM_EXPORT_NWSI void mmModuleSet_OnSetObject(const struct mmModuleSet* p, void* object)
{
    struct mmModule* u = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        u = (struct mmModule*)(it->v);
        u->object = object;
    }
}

MM_EXPORT_NWSI void mmModuleSet_OnUpdate(const struct mmModuleSet* p, struct mmEventUpdate* content)
{
    typedef void(*EventFuncType)(void* p, struct mmEventUpdate* content);
    const struct mmModuleMetadata* metadata = NULL;
    struct mmModule* u = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    const struct mmListVpt* list = &p->list;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        u = (struct mmModule*)it->v;
        metadata = u->metadata;
        assert(NULL != metadata && "metadata is invalid.");
        (*((EventFuncType)(metadata->OnUpdate)))(u, content);
    }
}

MM_EXPORT_NWSI void mmModuleSet_OnStart(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnStart),
    };
    mmModuleSet_OnHandlerEventNext(p, func);
}
MM_EXPORT_NWSI void mmModuleSet_OnInterrupt(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnInterrupt),
    };
    mmModuleSet_OnHandlerEventPrev(p, func);
}
MM_EXPORT_NWSI void mmModuleSet_OnShutdown(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnShutdown),
    };
    mmModuleSet_OnHandlerEventPrev(p, func);
}
MM_EXPORT_NWSI void mmModuleSet_OnJoin(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnJoin),
    };
    mmModuleSet_OnHandlerEventPrev(p, func);
}

MM_EXPORT_NWSI void mmModuleSet_OnFinishLaunching(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnFinishLaunching),
    };
    mmModuleSet_OnHandlerEventNext(p, func);
}
MM_EXPORT_NWSI void mmModuleSet_OnBeforeTerminate(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnBeforeTerminate),
    };
    mmModuleSet_OnHandlerEventPrev(p, func);
}

MM_EXPORT_NWSI void mmModuleSet_OnEnterBackground(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnEnterBackground),
    };
    mmModuleSet_OnHandlerEventNext(p, func);
}
MM_EXPORT_NWSI void mmModuleSet_OnEnterForeground(const struct mmModuleSet* p)
{
    enum
    {
        func = mmOffsetof(struct mmModuleMetadata, OnEnterForeground),
    };
    mmModuleSet_OnHandlerEventPrev(p, func);
}

static void mmModuleSet_OnHandlerEventNext(const struct mmModuleSet* p, size_t f)
{
    typedef void(*EventFuncType)(void* p);
    const struct mmModuleMetadata* metadata = NULL;
    EventFuncType func = NULL;
    struct mmModule* u = NULL;
    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    const struct mmListVpt* list = &p->list;
    next = list->l.next;
    while (next != &list->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        u = (struct mmModule*)(it->v);
        metadata = u->metadata;
        assert(NULL != metadata && "metadata is invalid.");
        func = (EventFuncType)(*((EventFuncType*)((char*)metadata + f)));
        assert(NULL != func && "func is invalid.");
        (*(func))(u);
    }
}

static void mmModuleSet_OnHandlerEventPrev(const struct mmModuleSet* p, size_t f)
{
    typedef void(*EventFuncType)(void* p);
    const struct mmModuleMetadata* metadata = NULL;
    EventFuncType func = NULL;
    struct mmModule* u = NULL;
    struct mmListHead* prev = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;
    const struct mmListVpt* list = &p->list;
    prev = list->l.next;
    while (prev != &list->l)
    {
        curr = prev;
        prev = prev->prev;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        u = (struct mmModule*)(it->v);
        metadata = u->metadata;
        assert(NULL != metadata && "metadata is invalid.");
        func = (EventFuncType)(*((EventFuncType*)((char*)metadata + f)));
        assert(NULL != func && "func is invalid.");
        (*(func))(u);
    }
}
