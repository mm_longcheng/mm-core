/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextFormatNative_h__
#define __mmTextFormatNative_h__

#include "core/mmCore.h"

#include "nwsi/mmTextFormat.h"
#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmTextFormat_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hFontFamilyName;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hFontSize;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hFontStyle;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hFontWeight;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hForegroundColor;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hBackgroundColor;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hStrokeWidth;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hStrokeColor;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hUnderlineWidth;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hUnderlineColor;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hStrikeThruWidth;
MM_EXPORT_NWSI extern jfieldID mmTextFormat_Field_hStrikeThruColor;

MM_EXPORT_NWSI 
void 
mmTextFormat_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextFormat_DetachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextFormat_Decode(
    JNIEnv*                                        env, 
    struct mmTextFormat*                           content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextFormat_Encode(
    JNIEnv*                                        env, 
    struct mmTextFormat*                           content, 
    jobject                                        obj);

#include "core/mmSuffix.h"

#endif//__mmTextFormatNative_h__
