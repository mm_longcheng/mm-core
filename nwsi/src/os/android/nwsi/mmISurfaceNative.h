/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmISurfaceNative_h__
#define __mmISurfaceNative_h__

#include "core/mmCore.h"

#include "nwsi/mmISurface.h"
#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmEventKeypad_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmEventKeypad_Field_modifier_mask;
MM_EXPORT_NWSI extern jfieldID mmEventKeypad_Field_key;
MM_EXPORT_NWSI extern jfieldID mmEventKeypad_Field_length;
MM_EXPORT_NWSI extern jfieldID mmEventKeypad_Field_text;
MM_EXPORT_NWSI extern jfieldID mmEventKeypad_Field_handle;

MM_EXPORT_NWSI void mmEventKeypad_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmEventKeypad_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmEventKeypad_Decode(JNIEnv* env, struct mmEventKeypad* content, jobject obj);
MM_EXPORT_NWSI void mmEventKeypad_Encode(JNIEnv* env, struct mmEventKeypad* content, jobject obj);
MM_EXPORT_NWSI void mmEventKeypad_EncodeHandle(JNIEnv* env, struct mmEventKeypad* content, jobject obj);

MM_EXPORT_NWSI extern jclass mmEventCursor_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_modifier_mask;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_button_mask;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_button_id;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_abs_x;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_abs_y;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_rel_x;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_rel_y;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_wheel_dx;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_wheel_dy;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_timestamp;
MM_EXPORT_NWSI extern jfieldID mmEventCursor_Field_handle;

MM_EXPORT_NWSI void mmEventCursor_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmEventCursor_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmEventCursor_Decode(JNIEnv* env, struct mmEventCursor* content, jobject obj);
MM_EXPORT_NWSI void mmEventCursor_Encode(JNIEnv* env, struct mmEventCursor* content, jobject obj);
MM_EXPORT_NWSI void mmEventCursor_EncodeHandle(JNIEnv* env, struct mmEventCursor* content, jobject obj);


MM_EXPORT_NWSI extern jclass mmEventTouch_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_motion_id;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_tap_count;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_phase;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_modifier_mask;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_button_mask;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_abs_x;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_abs_y;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_rel_x;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_rel_y;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_force_value;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_force_maximum;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_major_radius;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_minor_radius;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_size_value;
MM_EXPORT_NWSI extern jfieldID mmEventTouch_Field_timestamp;

MM_EXPORT_NWSI void mmEventTouch_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmEventTouch_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmEventTouch_Decode(JNIEnv* env, struct mmEventTouch* content, jobject obj);
MM_EXPORT_NWSI void mmEventTouch_Encode(JNIEnv* env, struct mmEventTouch* content, jobject obj);

MM_EXPORT_NWSI extern jclass mmEventTouchs_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmEventTouchs_Field_max_size;
MM_EXPORT_NWSI extern jfieldID mmEventTouchs_Field_size;
MM_EXPORT_NWSI extern jfieldID mmEventTouchs_Field_touchs;
MM_EXPORT_NWSI extern jmethodID mmEventTouchs_Method_AlignedMemory;

MM_EXPORT_NWSI void mmEventTouchs_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmEventTouchs_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmEventTouchs_Decode(JNIEnv* env, struct mmEventTouchs* content, jobject obj);
MM_EXPORT_NWSI void mmEventTouchs_Encode(JNIEnv* env, struct mmEventTouchs* content, jobject obj);

MM_EXPORT_NWSI extern jclass mmEventKeypadStatus_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_surface;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_screen_rect;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_safety_area;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_keypad_rect;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_window_rect;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_animation_duration;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_animation_curve;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_state;
MM_EXPORT_NWSI extern jfieldID mmEventKeypadStatus_Field_handle;

MM_EXPORT_NWSI void mmEventKeypadStatus_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmEventKeypadStatus_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmEventKeypadStatus_Decode(JNIEnv* env, struct mmEventKeypadStatus* content, jobject obj);
MM_EXPORT_NWSI void mmEventKeypadStatus_Encode(JNIEnv* env, struct mmEventKeypadStatus* content, jobject obj);
MM_EXPORT_NWSI void mmEventKeypadStatus_EncodeHandle(JNIEnv* env, struct mmEventKeypadStatus* content, jobject obj);

MM_EXPORT_NWSI extern jclass mmEventMetrics_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmEventMetrics_Field_surface;
MM_EXPORT_NWSI extern jfieldID mmEventMetrics_Field_canvas_size;
MM_EXPORT_NWSI extern jfieldID mmEventMetrics_Field_window_size;
MM_EXPORT_NWSI extern jfieldID mmEventMetrics_Field_safety_area;

MM_EXPORT_NWSI void mmEventMetrics_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmEventMetrics_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmEventMetrics_Decode(JNIEnv* env, struct mmEventMetrics* content, jobject obj);
MM_EXPORT_NWSI void mmEventMetrics_Encode(JNIEnv* env, struct mmEventMetrics* content, jobject obj);

#include "core/mmSuffix.h"

#endif//__mmISurfaceNative_h__
