/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextUtf16String.h"

#include "dish/mmJavaVMEnv.h"

MM_EXPORT_NWSI
void
mmTextUtf16String_Init(
    struct mmTextUtf16String*                      p)
{
    mmUtf16String_Init(&p->hString);
    p->pObject = NULL;

    mmTextUtf16String_NativeInit(p);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_Destroy(
    struct mmTextUtf16String*                      p)
{
    mmTextUtf16String_NativeDestroy(p);

    p->pObject = NULL;
    mmUtf16String_Destroy(&p->hString);
}

MM_EXPORT_NWSI
size_t
mmTextUtf16String_Size(
    const struct mmTextUtf16String*                p)
{
    return mmUtf16String_Size(&p->hString);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_OnFinishLaunching(
    struct mmTextUtf16String*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);

    // mmTextUtf16String_Encode(env, p, jNativePtr);

    (*env)->CallVoidMethod(env, jNativePtr, mmTextUtf16String_Method_OnFinishLaunching);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_OnBeforeTerminate(
    struct mmTextUtf16String*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    
    (*env)->CallVoidMethod(env, jNativePtr, mmTextUtf16String_Method_OnBeforeTerminate);

    // mmTextUtf16String_Decode(env, p, jNativePtr);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_NativeInit(
    struct mmTextUtf16String*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_NewObject = (*env)->NewObject(env, mmTextUtf16String_ObjectClass, mmTextUtf16String_Method_NewObject);
    jobject jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    p->pObject = jNativePtr;
    (*env)->CallVoidMethod(env, jNativePtr, mmTextUtf16String_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_NativeDestroy(
    struct mmTextUtf16String*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    (*env)->CallVoidMethod(env, jNativePtr, mmTextUtf16String_Method_Destroy);
    (*env)->DeleteGlobalRef(env, jNativePtr);
    p->pObject = NULL;
}

MM_EXPORT_NWSI
void
mmTextUtf16String_NativeEncode(
    struct mmTextUtf16String*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    mmTextUtf16String_Encode(env, p, jNativePtr);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_NativeDecode(
    struct mmTextUtf16String*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    mmTextUtf16String_Decode(env, p, jNativePtr);
}

MM_EXPORT_NWSI jclass mmTextUtf16String_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextUtf16String_Field_buffer = NULL;
MM_EXPORT_NWSI jfieldID mmTextUtf16String_Field_offset = NULL;
MM_EXPORT_NWSI jfieldID mmTextUtf16String_Field_length = NULL;
MM_EXPORT_NWSI jmethodID mmTextUtf16String_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextUtf16String_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmTextUtf16String_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmTextUtf16String_Method_OnFinishLaunching = NULL;
MM_EXPORT_NWSI jmethodID mmTextUtf16String_Method_OnBeforeTerminate = NULL;

MM_EXPORT_NWSI
void
mmTextUtf16String_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextUtf16String");
    mmTextUtf16String_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextUtf16String_Field_buffer = (*env)->GetFieldID(env, object_class, "buffer", "[C");
    mmTextUtf16String_Field_offset = (*env)->GetFieldID(env, object_class, "offset", "I");
    mmTextUtf16String_Field_length = (*env)->GetFieldID(env, object_class, "length", "I");
    mmTextUtf16String_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmTextUtf16String_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmTextUtf16String_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmTextUtf16String_Method_OnFinishLaunching = (*env)->GetMethodID(env, object_class, "OnFinishLaunching", "()V");
    mmTextUtf16String_Method_OnBeforeTerminate = (*env)->GetMethodID(env, object_class, "OnBeforeTerminate", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextUtf16String_Field_buffer = NULL;
    mmTextUtf16String_Field_offset = NULL;
    mmTextUtf16String_Field_length = NULL;
    mmTextUtf16String_Method_NewObject = NULL;
    mmTextUtf16String_Method_Init = NULL;
    mmTextUtf16String_Method_Destroy = NULL;
    mmTextUtf16String_Method_OnFinishLaunching = NULL;
    mmTextUtf16String_Method_OnBeforeTerminate = NULL;

    (*env)->DeleteGlobalRef(env, mmTextUtf16String_ObjectClass);
    mmTextUtf16String_ObjectClass = NULL;
}

MM_EXPORT_NWSI
void
mmTextUtf16String_Decode(
    JNIEnv*                                        env,
    struct mmTextUtf16String*                      content,
    jobject                                        obj)
{
    jcharArray j_buffer = (jcharArray)(*env)->GetObjectField(env, obj, mmTextUtf16String_Field_buffer);

    struct mmUtf16String* pString = &content->hString;

    mmUtf16String_Reset(pString);

    jint offset = (*env)->GetIntField(env, obj, mmTextUtf16String_Field_offset);
    jint length = (*env)->GetIntField(env, obj, mmTextUtf16String_Field_length);

    jchar* buffer = (*env)->GetCharArrayElements(env, j_buffer, NULL);
    
    mmUtf16String_Assignsn(pString, buffer + offset, length);

    (*env)->ReleaseCharArrayElements(env, j_buffer, buffer, 0);

    (*env)->DeleteLocalRef(env, j_buffer);
}

MM_EXPORT_NWSI
void
mmTextUtf16String_Encode(
    JNIEnv*                                        env,
    struct mmTextUtf16String*                      content,
    jobject                                        obj)
{
    struct mmUtf16String* pString = &content->hString;

    jint offset = (jint)0;
    jint length = (jint)mmUtf16String_Size(pString);

    const jchar* buffer = (const jchar*)mmUtf16String_CStr(pString);

    jcharArray j_buffer = (*env)->NewCharArray(env, length);

    (*env)->SetCharArrayRegion(env, j_buffer, offset, length, buffer);
    (*env)->SetIntField(env, obj, mmTextUtf16String_Field_offset, offset);
    (*env)->SetIntField(env, obj, mmTextUtf16String_Field_length, length);
    (*env)->SetObjectField(env, obj, mmTextUtf16String_Field_buffer, j_buffer);

    (*env)->DeleteLocalRef(env, j_buffer);
}