/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmISurfaceNative.h"

#include "core/mmAlloc.h"

MM_EXPORT_NWSI jclass mmEventKeypad_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypad_Field_modifier_mask = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypad_Field_key = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypad_Field_length = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypad_Field_text = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypad_Field_handle = NULL;

MM_EXPORT_NWSI void mmEventKeypad_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmISurface$mmEventKeypad");
    mmEventKeypad_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmEventKeypad_Field_modifier_mask = (*env)->GetFieldID(env, object_class, "modifier_mask", "I");
    mmEventKeypad_Field_key = (*env)->GetFieldID(env, object_class, "key", "I");
    mmEventKeypad_Field_length = (*env)->GetFieldID(env, object_class, "length", "I");
    mmEventKeypad_Field_text = (*env)->GetFieldID(env, object_class, "text", "[C");
    mmEventKeypad_Field_handle = (*env)->GetFieldID(env, object_class, "handle", "I");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmEventKeypad_DetachVirtualMachine(JNIEnv* env)
{
    mmEventKeypad_Field_modifier_mask = NULL;
    mmEventKeypad_Field_key = NULL;
    mmEventKeypad_Field_length = NULL;
    mmEventKeypad_Field_text = NULL;
    mmEventKeypad_Field_handle = NULL;

    (*env)->DeleteGlobalRef(env, mmEventKeypad_ObjectClass);
    mmEventKeypad_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmEventKeypad_Decode(JNIEnv* env, struct mmEventKeypad* content, jobject obj)
{
    jcharArray j_text = (jcharArray)(*env)->GetObjectField(env, obj, mmEventKeypad_Field_text);

    content->modifier_mask = (*env)->GetIntField(env, obj, mmEventKeypad_Field_modifier_mask);
    content->key = (*env)->GetIntField(env, obj, mmEventKeypad_Field_key);
    content->length = (*env)->GetIntField(env, obj, mmEventKeypad_Field_length);
    (*env)->GetCharArrayRegion(env, j_text, 0, 4, content->text);
    content->handle = (*env)->GetIntField(env, obj, mmEventKeypad_Field_handle);

    (*env)->DeleteLocalRef(env, j_text);
}
MM_EXPORT_NWSI void mmEventKeypad_Encode(JNIEnv* env, struct mmEventKeypad* content, jobject obj)
{
    jcharArray j_text = (jfloatArray)(*env)->GetObjectField(env, obj, mmEventKeypad_Field_text);

    (*env)->SetIntField(env, obj, mmEventKeypad_Field_modifier_mask, content->modifier_mask);
    (*env)->SetIntField(env, obj, mmEventKeypad_Field_key, content->key);
    (*env)->SetIntField(env, obj, mmEventKeypad_Field_length, content->length);
    (*env)->SetCharArrayRegion(env, j_text, 0, 4, content->text);
    (*env)->SetIntField(env, obj, mmEventKeypad_Field_handle, content->handle);

    (*env)->DeleteLocalRef(env, j_text);
}
MM_EXPORT_NWSI void mmEventKeypad_EncodeHandle(JNIEnv* env, struct mmEventKeypad* content, jobject obj)
{
    (*env)->SetIntField(env, obj, mmEventKeypad_Field_handle, content->handle);
}

MM_EXPORT_NWSI jclass mmEventCursor_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_modifier_mask = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_button_mask = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_button_id = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_abs_x = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_abs_y = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_rel_x = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_rel_y = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_wheel_dx = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_wheel_dy = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_timestamp = NULL;
MM_EXPORT_NWSI jfieldID mmEventCursor_Field_handle = NULL;

MM_EXPORT_NWSI void mmEventCursor_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmISurface$mmEventCursor");
    mmEventCursor_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmEventCursor_Field_modifier_mask = (*env)->GetFieldID(env, object_class, "modifier_mask", "I");
    mmEventCursor_Field_button_mask = (*env)->GetFieldID(env, object_class, "button_mask", "I");
    mmEventCursor_Field_button_id = (*env)->GetFieldID(env, object_class, "button_id", "I");
    mmEventCursor_Field_abs_x = (*env)->GetFieldID(env, object_class, "abs_x", "D");
    mmEventCursor_Field_abs_y = (*env)->GetFieldID(env, object_class, "abs_y", "D");
    mmEventCursor_Field_rel_x = (*env)->GetFieldID(env, object_class, "rel_x", "D");
    mmEventCursor_Field_rel_y = (*env)->GetFieldID(env, object_class, "rel_y", "D");
    mmEventCursor_Field_wheel_dx = (*env)->GetFieldID(env, object_class, "wheel_dx", "D");
    mmEventCursor_Field_wheel_dy = (*env)->GetFieldID(env, object_class, "wheel_dy", "D");
    mmEventCursor_Field_timestamp = (*env)->GetFieldID(env, object_class, "timestamp", "J");
    mmEventCursor_Field_handle = (*env)->GetFieldID(env, object_class, "handle", "I");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmEventCursor_DetachVirtualMachine(JNIEnv* env)
{
    mmEventCursor_Field_modifier_mask = NULL;
    mmEventCursor_Field_button_mask = NULL;
    mmEventCursor_Field_button_id = NULL;
    mmEventCursor_Field_abs_x = NULL;
    mmEventCursor_Field_abs_y = NULL;
    mmEventCursor_Field_rel_x = NULL;
    mmEventCursor_Field_rel_y = NULL;
    mmEventCursor_Field_wheel_dx = NULL;
    mmEventCursor_Field_wheel_dy = NULL;
    mmEventCursor_Field_timestamp = NULL;
    mmEventCursor_Field_handle = NULL;

    (*env)->DeleteGlobalRef(env, mmEventCursor_ObjectClass);
    mmEventCursor_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmEventCursor_Decode(JNIEnv* env, struct mmEventCursor* content, jobject obj)
{
    content->modifier_mask = (*env)->GetIntField(env, obj, mmEventCursor_Field_modifier_mask);
    content->button_mask = (*env)->GetIntField(env, obj, mmEventCursor_Field_button_mask);
    content->button_id = (*env)->GetIntField(env, obj, mmEventCursor_Field_button_id);
    content->abs_x = (*env)->GetDoubleField(env, obj, mmEventCursor_Field_abs_x);
    content->abs_y = (*env)->GetDoubleField(env, obj, mmEventCursor_Field_abs_y);
    content->rel_x = (*env)->GetDoubleField(env, obj, mmEventCursor_Field_rel_x);
    content->rel_y = (*env)->GetDoubleField(env, obj, mmEventCursor_Field_rel_y);
    content->wheel_dx = (*env)->GetDoubleField(env, obj, mmEventCursor_Field_wheel_dx);
    content->wheel_dy = (*env)->GetDoubleField(env, obj, mmEventCursor_Field_wheel_dy);
    content->timestamp = (*env)->GetLongField(env, obj, mmEventCursor_Field_timestamp);
    content->handle = (*env)->GetIntField(env, obj, mmEventCursor_Field_handle);
}
MM_EXPORT_NWSI void mmEventCursor_Encode(JNIEnv* env, struct mmEventCursor* content, jobject obj)
{
    (*env)->SetIntField(env, obj, mmEventCursor_Field_modifier_mask, content->modifier_mask);
    (*env)->SetIntField(env, obj, mmEventCursor_Field_button_mask, content->button_mask);
    (*env)->SetIntField(env, obj, mmEventCursor_Field_button_id, content->button_id);
    (*env)->SetDoubleField(env, obj, mmEventCursor_Field_abs_x, content->abs_x);
    (*env)->SetDoubleField(env, obj, mmEventCursor_Field_abs_y, content->abs_y);
    (*env)->SetDoubleField(env, obj, mmEventCursor_Field_rel_x, content->rel_x);
    (*env)->SetDoubleField(env, obj, mmEventCursor_Field_rel_y, content->rel_y);
    (*env)->SetDoubleField(env, obj, mmEventCursor_Field_wheel_dx, content->wheel_dx);
    (*env)->SetDoubleField(env, obj, mmEventCursor_Field_wheel_dy, content->wheel_dy);
    (*env)->SetLongField(env, obj, mmEventCursor_Field_timestamp, content->timestamp);
    (*env)->SetIntField(env, obj, mmEventCursor_Field_handle, content->handle);
}
MM_EXPORT_NWSI void mmEventCursor_EncodeHandle(JNIEnv* env, struct mmEventCursor* content, jobject obj)
{
    (*env)->SetIntField(env, obj, mmEventCursor_Field_handle, content->handle);
}

MM_EXPORT_NWSI jclass mmEventTouch_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_motion_id = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_tap_count = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_phase = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_modifier_mask = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_button_mask = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_abs_x = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_abs_y = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_rel_x = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_rel_y = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_force_value = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_force_maximum = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_major_radius = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_minor_radius = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_size_value = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouch_Field_timestamp = NULL;

MM_EXPORT_NWSI void mmEventTouch_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmISurface$mmEventTouch");
    mmEventTouch_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmEventTouch_Field_motion_id = (*env)->GetFieldID(env, object_class, "motion_id", "J");
    mmEventTouch_Field_tap_count = (*env)->GetFieldID(env, object_class, "tap_count", "I");
    mmEventTouch_Field_phase = (*env)->GetFieldID(env, object_class, "phase", "I");
    mmEventTouch_Field_modifier_mask = (*env)->GetFieldID(env, object_class, "modifier_mask", "I");
    mmEventTouch_Field_button_mask = (*env)->GetFieldID(env, object_class, "button_mask", "I");
    mmEventTouch_Field_abs_x = (*env)->GetFieldID(env, object_class, "abs_x", "D");
    mmEventTouch_Field_abs_y = (*env)->GetFieldID(env, object_class, "abs_y", "D");
    mmEventTouch_Field_rel_x = (*env)->GetFieldID(env, object_class, "rel_x", "D");
    mmEventTouch_Field_rel_y = (*env)->GetFieldID(env, object_class, "rel_y", "D");
    mmEventTouch_Field_force_value = (*env)->GetFieldID(env, object_class, "force_value", "D");
    mmEventTouch_Field_force_maximum = (*env)->GetFieldID(env, object_class, "force_maximum", "D");
    mmEventTouch_Field_major_radius = (*env)->GetFieldID(env, object_class, "major_radius", "D");
    mmEventTouch_Field_minor_radius = (*env)->GetFieldID(env, object_class, "minor_radius", "D");
    mmEventTouch_Field_size_value = (*env)->GetFieldID(env, object_class, "size_value", "D");
    mmEventTouch_Field_timestamp = (*env)->GetFieldID(env, object_class, "timestamp", "J");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmEventTouch_DetachVirtualMachine(JNIEnv* env)
{
    mmEventTouch_Field_motion_id = NULL;
    mmEventTouch_Field_tap_count = NULL;
    mmEventTouch_Field_phase = NULL;
    mmEventTouch_Field_modifier_mask = NULL;
    mmEventTouch_Field_button_mask = NULL;
    mmEventTouch_Field_abs_x = NULL;
    mmEventTouch_Field_abs_y = NULL;
    mmEventTouch_Field_rel_x = NULL;
    mmEventTouch_Field_rel_y = NULL;
    mmEventTouch_Field_force_value = NULL;
    mmEventTouch_Field_force_maximum = NULL;
    mmEventTouch_Field_major_radius = NULL;
    mmEventTouch_Field_minor_radius = NULL;
    mmEventTouch_Field_size_value = NULL;
    mmEventTouch_Field_timestamp = NULL;

    (*env)->DeleteGlobalRef(env, mmEventTouch_ObjectClass);
    mmEventTouch_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmEventTouch_Decode(JNIEnv* env, struct mmEventTouch* content, jobject obj)
{
    content->motion_id = (*env)->GetLongField(env, obj, mmEventTouch_Field_motion_id);
    content->tap_count = (*env)->GetIntField(env, obj, mmEventTouch_Field_tap_count);
    content->phase = (*env)->GetIntField(env, obj, mmEventTouch_Field_phase);
    content->modifier_mask = (*env)->GetIntField(env, obj, mmEventTouch_Field_modifier_mask);
    content->button_mask = (*env)->GetIntField(env, obj, mmEventTouch_Field_button_mask);
    content->abs_x = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_abs_x);
    content->abs_y = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_abs_y);
    content->rel_x = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_rel_x);
    content->rel_y = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_rel_y);
    content->force_value = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_force_value);
    content->force_maximum = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_force_maximum);
    content->major_radius = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_major_radius);
    content->minor_radius = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_minor_radius);
    content->size_value = (*env)->GetDoubleField(env, obj, mmEventTouch_Field_size_value);
    content->timestamp = (*env)->GetLongField(env, obj, mmEventTouch_Field_timestamp);
}
MM_EXPORT_NWSI void mmEventTouch_Encode(JNIEnv* env, struct mmEventTouch* content, jobject obj)
{
    (*env)->SetLongField(env, obj, mmEventTouch_Field_motion_id, content->motion_id);
    (*env)->SetIntField(env, obj, mmEventTouch_Field_tap_count, content->tap_count);
    (*env)->SetIntField(env, obj, mmEventTouch_Field_phase, content->phase);
    (*env)->SetIntField(env, obj, mmEventTouch_Field_modifier_mask, content->modifier_mask);
    (*env)->SetIntField(env, obj, mmEventTouch_Field_button_mask, content->button_mask);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_abs_x, content->abs_x);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_abs_y, content->abs_y);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_rel_x, content->rel_x);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_rel_y, content->rel_y);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_force_value, content->force_value);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_force_maximum, content->force_maximum);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_major_radius, content->major_radius);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_minor_radius, content->minor_radius);
    (*env)->SetDoubleField(env, obj, mmEventTouch_Field_size_value, content->size_value);
    (*env)->SetLongField(env, obj, mmEventTouch_Field_timestamp, content->timestamp);
}

MM_EXPORT_NWSI jclass mmEventTouchs_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouchs_Field_max_size = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouchs_Field_size = NULL;
MM_EXPORT_NWSI jfieldID mmEventTouchs_Field_touchs = NULL;
MM_EXPORT_NWSI jmethodID mmEventTouchs_Method_AlignedMemory = NULL;

MM_EXPORT_NWSI void mmEventTouchs_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmISurface$mmEventTouchs");
    mmEventTouchs_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmEventTouchs_Field_max_size = (*env)->GetFieldID(env, object_class, "max_size", "I");
    mmEventTouchs_Field_size = (*env)->GetFieldID(env, object_class, "size", "I");
    mmEventTouchs_Field_touchs = (*env)->GetFieldID(env, object_class, "touchs", "[Lorg/mm/nwsi/mmISurface$mmEventTouch;");
    mmEventTouchs_Method_AlignedMemory = (*env)->GetMethodID(env, object_class, "AlignedMemory", "(I)V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmEventTouchs_DetachVirtualMachine(JNIEnv* env)
{
    mmEventTouchs_Field_max_size = NULL;
    mmEventTouchs_Field_size = NULL;
    mmEventTouchs_Field_touchs = NULL;
    mmEventTouchs_Method_AlignedMemory = NULL;

    (*env)->DeleteGlobalRef(env, mmEventTouchs_ObjectClass);
    mmEventTouchs_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmEventTouchs_Decode(JNIEnv* env, struct mmEventTouchs* content, jobject obj)
{
    int i = 0;
    struct mmEventTouch* u = NULL;
    size_t size = 0;

    size = (*env)->GetIntField(env, obj, mmEventTouchs_Field_size);

    mmEventTouchs_AlignedMemory(content, size);

    jobjectArray j_touchs = (jobjectArray)(*env)->GetObjectField(env, obj, mmEventTouchs_Field_touchs);
    for (i = 0; i < size; ++i)
    {
        jobject j_u = (*env)->GetObjectArrayElement(env, j_touchs, i);
        u = mmEventTouchs_GetForIndexReference(content, i);
        mmEventTouch_Decode(env, u, j_u);
        (*env)->DeleteLocalRef(env, j_u);
    }
    (*env)->DeleteLocalRef(env, j_touchs);
}
MM_EXPORT_NWSI void mmEventTouchs_Encode(JNIEnv* env, struct mmEventTouchs* content, jobject obj)
{
    int i = 0;
    struct mmEventTouch* u = NULL;
    size_t size = 0;

    size = content->context.size;

    (*env)->CallVoidMethod(env, obj, mmEventTouchs_Method_AlignedMemory, size);

    jobjectArray j_touchs = (jobjectArray)(*env)->GetObjectField(env, obj, mmEventTouchs_Field_touchs);
    for (i = 0; i < size; ++i)
    {
        jobject j_u = (*env)->AllocObject(env, mmEventTouch_ObjectClass);
        u = mmEventTouchs_GetForIndexReference(content, i);
        mmEventTouch_Encode(env, u, j_u);
        (*env)->SetObjectArrayElement(env, j_touchs, i, j_u);
        (*env)->DeleteLocalRef(env, j_u);
    }
    (*env)->DeleteLocalRef(env, j_touchs);
}

MM_EXPORT_NWSI jclass mmEventKeypadStatus_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_surface = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_screen_rect = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_safety_area = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_keypad_rect = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_window_rect = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_animation_duration = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_animation_curve = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_state = NULL;
MM_EXPORT_NWSI jfieldID mmEventKeypadStatus_Field_handle = NULL;

MM_EXPORT_NWSI void mmEventKeypadStatus_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmISurface$mmEventKeypadStatus");
    mmEventKeypadStatus_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmEventKeypadStatus_Field_surface = (*env)->GetFieldID(env, object_class, "surface", "Ljava/lang/Object;");
    mmEventKeypadStatus_Field_screen_rect = (*env)->GetFieldID(env, object_class, "screen_rect", "[D");
    mmEventKeypadStatus_Field_safety_area = (*env)->GetFieldID(env, object_class, "safety_area", "[D");
    mmEventKeypadStatus_Field_keypad_rect = (*env)->GetFieldID(env, object_class, "keypad_rect", "[D");
    mmEventKeypadStatus_Field_window_rect = (*env)->GetFieldID(env, object_class, "window_rect", "[D");
    mmEventKeypadStatus_Field_animation_duration = (*env)->GetFieldID(env, object_class, "animation_duration", "D");
    mmEventKeypadStatus_Field_animation_curve = (*env)->GetFieldID(env, object_class, "animation_curve", "I");
    mmEventKeypadStatus_Field_state = (*env)->GetFieldID(env, object_class, "state", "I");
    mmEventKeypadStatus_Field_handle = (*env)->GetFieldID(env, object_class, "handle", "I");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmEventKeypadStatus_DetachVirtualMachine(JNIEnv* env)
{
    mmEventKeypadStatus_Field_surface = NULL;
    mmEventKeypadStatus_Field_screen_rect = NULL;
    mmEventKeypadStatus_Field_safety_area = NULL;
    mmEventKeypadStatus_Field_keypad_rect = NULL;
    mmEventKeypadStatus_Field_window_rect = NULL;
    mmEventKeypadStatus_Field_animation_duration = NULL;
    mmEventKeypadStatus_Field_animation_curve = NULL;
    mmEventKeypadStatus_Field_state = NULL;
    mmEventKeypadStatus_Field_handle = NULL;

    (*env)->DeleteGlobalRef(env, mmEventKeypadStatus_ObjectClass);
    mmEventKeypadStatus_ObjectClass = NULL;    
}

MM_EXPORT_NWSI void mmEventKeypadStatus_Decode(JNIEnv* env, struct mmEventKeypadStatus* content, jobject obj)
{
    jobject j_surface = (jobjectArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_surface);

    jdoubleArray j_screen_rect = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_screen_rect);
    jdoubleArray j_safety_area = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_safety_area);
    jdoubleArray j_keypad_rect = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_keypad_rect);
    jdoubleArray j_window_rect = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_window_rect);

    content->surface = (void*)j_surface;
    (*env)->GetDoubleArrayRegion(env, j_screen_rect, 0, 4, content->screen_rect);
    (*env)->GetDoubleArrayRegion(env, j_safety_area, 0, 4, content->safety_area);
    (*env)->GetDoubleArrayRegion(env, j_keypad_rect, 0, 4, content->keypad_rect);
    (*env)->GetDoubleArrayRegion(env, j_window_rect, 0, 4, content->window_rect);
    content->animation_duration = (*env)->GetDoubleField(env, obj, mmEventKeypadStatus_Field_animation_duration);
    content->animation_curve = (*env)->GetIntField(env, obj, mmEventKeypadStatus_Field_animation_curve);
    content->state = (*env)->GetIntField(env, obj, mmEventKeypadStatus_Field_state);
    content->handle = (*env)->GetIntField(env, obj, mmEventKeypadStatus_Field_handle);

    (*env)->DeleteLocalRef(env, j_screen_rect);
    (*env)->DeleteLocalRef(env, j_safety_area);
    (*env)->DeleteLocalRef(env, j_keypad_rect);
    (*env)->DeleteLocalRef(env, j_window_rect);

    (*env)->DeleteLocalRef(env, j_surface);
}
MM_EXPORT_NWSI void mmEventKeypadStatus_Encode(JNIEnv* env, struct mmEventKeypadStatus* content, jobject obj)
{
    jdoubleArray j_screen_rect = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_screen_rect);
    jdoubleArray j_safety_area = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_safety_area);
    jdoubleArray j_keypad_rect = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_keypad_rect);
    jdoubleArray j_window_rect = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventKeypadStatus_Field_window_rect);

    (*env)->SetObjectField(env, obj, mmEventKeypadStatus_Field_surface, (jobject)content->surface);
    (*env)->SetDoubleArrayRegion(env, j_screen_rect, 0, 4, content->screen_rect);
    (*env)->SetDoubleArrayRegion(env, j_safety_area, 0, 4, content->safety_area);
    (*env)->SetDoubleArrayRegion(env, j_keypad_rect, 0, 4, content->keypad_rect);
    (*env)->SetDoubleArrayRegion(env, j_window_rect, 0, 4, content->window_rect);
    (*env)->SetDoubleField(env, obj, mmEventKeypadStatus_Field_animation_duration, content->animation_duration);
    (*env)->SetIntField(env, obj, mmEventKeypadStatus_Field_animation_curve, content->animation_curve);
    (*env)->SetIntField(env, obj, mmEventKeypadStatus_Field_state, content->state);
    (*env)->SetIntField(env, obj, mmEventKeypadStatus_Field_handle, content->handle);

    (*env)->DeleteLocalRef(env, j_screen_rect);
    (*env)->DeleteLocalRef(env, j_safety_area);
    (*env)->DeleteLocalRef(env, j_keypad_rect);
    (*env)->DeleteLocalRef(env, j_window_rect);
}
MM_EXPORT_NWSI void mmEventKeypadStatus_EncodeHandle(JNIEnv* env, struct mmEventKeypadStatus* content, jobject obj)
{
    (*env)->SetIntField(env, obj, mmEventKeypadStatus_Field_handle, content->handle);
}

MM_EXPORT_NWSI jclass mmEventMetrics_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmEventMetrics_Field_surface = NULL;
MM_EXPORT_NWSI jfieldID mmEventMetrics_Field_canvas_size = NULL;
MM_EXPORT_NWSI jfieldID mmEventMetrics_Field_window_size = NULL;
MM_EXPORT_NWSI jfieldID mmEventMetrics_Field_safety_area = NULL;

MM_EXPORT_NWSI void mmEventMetrics_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmISurface$mmEventMetrics");
    mmEventMetrics_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmEventMetrics_Field_surface = (*env)->GetFieldID(env, object_class, "surface", "Ljava/lang/Object;");
    mmEventMetrics_Field_canvas_size = (*env)->GetFieldID(env, object_class, "canvas_size", "[D");
    mmEventMetrics_Field_window_size = (*env)->GetFieldID(env, object_class, "window_size", "[D");
    mmEventMetrics_Field_safety_area = (*env)->GetFieldID(env, object_class, "safety_area", "[D");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmEventMetrics_DetachVirtualMachine(JNIEnv* env)
{
    mmEventMetrics_Field_surface = NULL;
    mmEventMetrics_Field_canvas_size = NULL;
    mmEventMetrics_Field_window_size = NULL;
    mmEventMetrics_Field_safety_area = NULL;

    (*env)->DeleteGlobalRef(env, mmEventMetrics_ObjectClass);
    mmEventMetrics_ObjectClass = NULL;    
}

MM_EXPORT_NWSI void mmEventMetrics_Decode(JNIEnv* env, struct mmEventMetrics* content, jobject obj)
{
    jobject j_surface = (jobjectArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_surface);

    jdoubleArray j_canvas_size = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_canvas_size);
    jdoubleArray j_window_size = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_window_size);
    jdoubleArray j_safety_area = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_safety_area);

    content->surface = (void*)j_surface;
    (*env)->GetDoubleArrayRegion(env, j_canvas_size, 0, 2, content->canvas_size);
    (*env)->GetDoubleArrayRegion(env, j_window_size, 0, 2, content->window_size);
    (*env)->GetDoubleArrayRegion(env, j_safety_area, 0, 4, content->safety_area);

    (*env)->DeleteLocalRef(env, j_canvas_size);
    (*env)->DeleteLocalRef(env, j_window_size);
    (*env)->DeleteLocalRef(env, j_safety_area);

    (*env)->DeleteLocalRef(env, j_surface);
}
MM_EXPORT_NWSI void mmEventMetrics_Encode(JNIEnv* env, struct mmEventMetrics* content, jobject obj)
{
    jdoubleArray j_canvas_size = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_canvas_size);
    jdoubleArray j_window_size = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_window_size);
    jdoubleArray j_safety_area = (jdoubleArray)(*env)->GetObjectField(env, obj, mmEventMetrics_Field_safety_area);

    (*env)->SetObjectField(env, obj, mmEventMetrics_Field_surface, (jobject)content->surface);
    (*env)->SetDoubleArrayRegion(env, j_canvas_size, 0, 2, content->canvas_size);
    (*env)->SetDoubleArrayRegion(env, j_window_size, 0, 2, content->window_size);
    (*env)->SetDoubleArrayRegion(env, j_safety_area, 0, 4, content->safety_area);

    (*env)->DeleteLocalRef(env, j_canvas_size);
    (*env)->DeleteLocalRef(env, j_window_size);
    (*env)->DeleteLocalRef(env, j_safety_area);
}