/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNativeApplication_h__
#define __mmNativeApplication_h__

#include "core/mmCore.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmSurfaceMaster.h"

#include "nwsi/mmIActivity.h"

#include "nwsi/mmNativeClipboard.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmNativeApplication_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmNativeApplication_Field_hNativePtr;

MM_EXPORT_NWSI void mmNativeApplication_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmNativeApplication_DetachVirtualMachine(JNIEnv* env);

struct mmNativeApplication
{
    struct mmContextMaster hContextMaster;
    struct mmSurfaceMaster hSurfaceMaster;
    
    struct mmNativeClipboard hNativeClipboard;

    struct mmString hRenderSystemName;
    
    struct mmString hLoggerFileName;
    mmUInt32_t hLoggerLevel;
    
    struct mmIActivity* pIActivity;

    jobject jNativePtr;
    jobject jActivityPtr;
};

MM_EXPORT_NWSI void mmNativeApplication_Init(struct mmNativeApplication* p);
MM_EXPORT_NWSI void mmNativeApplication_Destroy(struct mmNativeApplication* p);

MM_EXPORT_NWSI void mmNativeApplication_SetAppHandler(struct mmNativeApplication* p, void* pAppHandler);
// default is "mm.nwsi"
MM_EXPORT_NWSI void mmNativeApplication_SetModuleName(struct mmNativeApplication* p, const char* pModuleName);
// Render system name.
MM_EXPORT_NWSI void mmNativeApplication_SetRenderSystemName(struct mmNativeApplication* p, const char* pRenderSystemName);

// default is "nwsi"
MM_EXPORT_NWSI void mmNativeApplication_SetLoggerFileName(struct mmNativeApplication* p, const char* hLoggerFileName);
// default is MM_LOG_INFO
MM_EXPORT_NWSI void mmNativeApplication_SetLoggerLevel(struct mmNativeApplication* p, mmUInt32_t hLoggerLevel);
// default is 60.0
MM_EXPORT_NWSI void mmNativeApplication_SetDisplayFrequency(struct mmNativeApplication* p, double hDisplayFrequency);

MM_EXPORT_NWSI void mmNativeApplication_SetActivityMaster(struct mmNativeApplication* p, struct mmIActivity* pIActivity);

MM_EXPORT_NWSI void mmNativeApplication_OnFinishLaunching(struct mmNativeApplication* p);
MM_EXPORT_NWSI void mmNativeApplication_OnBeforeTerminate(struct mmNativeApplication* p);

MM_EXPORT_NWSI void mmNativeApplication_OnEnterBackground(struct mmNativeApplication* p);
MM_EXPORT_NWSI void mmNativeApplication_OnEnterForeground(struct mmNativeApplication* p);

MM_EXPORT_NWSI void mmNativeApplication_OnStart(struct mmNativeApplication* p);
MM_EXPORT_NWSI void mmNativeApplication_OnInterrupt(struct mmNativeApplication* p);
MM_EXPORT_NWSI void mmNativeApplication_OnShutdown(struct mmNativeApplication* p);
MM_EXPORT_NWSI void mmNativeApplication_OnJoin(struct mmNativeApplication* p);

#include "core/mmSuffix.h"

#endif//__mmNativeApplication_h__
