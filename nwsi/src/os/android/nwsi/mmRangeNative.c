/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRangeNative.h"

#include "core/mmAlloc.h"

MM_EXPORT_NWSI jclass mmRange_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmRange_Field_o = NULL;
MM_EXPORT_NWSI jfieldID mmRange_Field_l = NULL;

MM_EXPORT_NWSI void mmRange_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/math/mmRange");
    mmRange_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmRange_Field_o = (*env)->GetFieldID(env, object_class, "o", "I");
    mmRange_Field_l = (*env)->GetFieldID(env, object_class, "l", "I");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmRange_DetachVirtualMachine(JNIEnv* env)
{
    mmRange_Field_o = NULL;
    mmRange_Field_l = NULL;

    (*env)->DeleteGlobalRef(env, mmRange_ObjectClass);
    mmRange_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmRange_Decode(JNIEnv* env, struct mmRange* content, jobject obj)
{
    content->o = (size_t)(*env)->GetIntField(env, obj, mmRange_Field_o);
    content->l = (size_t)(*env)->GetIntField(env, obj, mmRange_Field_l);
}
MM_EXPORT_NWSI void mmRange_Encode(JNIEnv* env, struct mmRange* content, jobject obj)
{
    (*env)->SetIntField(env, obj, mmRange_Field_o, (int)content->o);
    (*env)->SetIntField(env, obj, mmRange_Field_l, (int)content->l);
}
