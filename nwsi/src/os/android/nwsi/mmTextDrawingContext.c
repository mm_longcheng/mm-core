/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextDrawingContext.h"
#include "mmTextRenderTarget.h"
#include "mmGraphicsFactory.h"

#include "nwsi/mmTextParagraphFormat.h"

#include "dish/mmJavaVMEnv.h"

MM_EXPORT_NWSI
void
mmTextDrawingContext_Init(
    struct mmTextDrawingContext*                   p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    p->pRenderTarget = NULL;
    p->pBitmap = NULL;
    p->hPass = 0;

    jobject j_NewObject = (*env)->NewObject(env, mmTextDrawingContext_ObjectClass, mmTextDrawingContext_Method_NewObject);
    p->jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_Destroy(
    struct mmTextDrawingContext*                   p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_Destroy);
    (*env)->DeleteGlobalRef(env, p->jNativePtr);
    p->jNativePtr = NULL;

    (*env)->DeleteGlobalRef(env, (jobject)p->pBitmap);
    (*env)->DeleteGlobalRef(env, (jobject)p->pRenderTarget);

    p->hPass = 0;
    p->pBitmap = NULL;
    p->pRenderTarget = NULL;
    p->pParagraphFormat = NULL;
    p->pGraphicsFactory = NULL;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetGraphicsFactory(
    struct mmTextDrawingContext*                   p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_SetGraphicsFactory, pGraphicsFactory->jNativePtr);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetTextParagraphFormat(
    struct mmTextDrawingContext*                   p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_obj = (jobject)(pParagraphFormat->pObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_SetTextParagraphFormat, j_obj);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetRenderTarget(
    struct mmTextDrawingContext*                   p,
    void*                                          pRenderTarget)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->DeleteGlobalRef(env, (jobject)p->pRenderTarget);
    p->pRenderTarget = (*env)->NewGlobalRef(env, (jobject)pRenderTarget);

    jobject j_obj = (jobject)(pRenderTarget);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_SetRenderTarget, j_obj);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetBitmap(
    struct mmTextDrawingContext*                   p,
    void*                                          pBitmap)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->DeleteGlobalRef(env, (jobject)p->pBitmap);
    p->pBitmap = (*env)->NewGlobalRef(env, (jobject)pBitmap);

    jobject j_obj = (jobject)(pBitmap);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_SetBitmap, j_obj);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetPass(
    struct mmTextDrawingContext*                   p,
    int                                            hPass)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    p->hPass = hPass;

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_SetPass, (jint)p->hPass);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_PrepareRenderTarget(
    struct mmTextDrawingContext*                   p,
    struct mmTextRenderTarget*                     pRenderTarget)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject obj = pRenderTarget->jNativePtr;

    jobject j_pBitmap = (*env)->GetObjectField(env, obj, mmTextRenderTarget_Field_pBitmap);
    jobject j_pCanvas = (*env)->GetObjectField(env, obj, mmTextRenderTarget_Field_pCanvas);

    mmTextDrawingContext_SetRenderTarget(p, j_pCanvas);
    mmTextDrawingContext_SetBitmap(p, j_pBitmap);

    (*env)->DeleteLocalRef(env, j_pBitmap);
    (*env)->DeleteLocalRef(env, j_pCanvas);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_DiscardRenderTarget(
    struct mmTextDrawingContext*                   p)
{
    mmTextDrawingContext_SetRenderTarget(p, NULL);
    mmTextDrawingContext_SetBitmap(p, NULL);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_OnFinishLaunching(
    struct mmTextDrawingContext*                   p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_OnFinishLaunching);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_OnBeforeTerminate(
    struct mmTextDrawingContext*                   p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextDrawingContext_Method_OnBeforeTerminate);
}

MM_EXPORT_NWSI jclass mmTextDrawingContext_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_SetGraphicsFactory = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_SetTextParagraphFormat = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_SetRenderTarget = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_SetBitmap = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_SetPass = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_OnFinishLaunching = NULL;
MM_EXPORT_NWSI jmethodID mmTextDrawingContext_Method_OnBeforeTerminate = NULL;

MM_EXPORT_NWSI
void
mmTextDrawingContext_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextDrawingContext");
    mmTextDrawingContext_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextDrawingContext_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmTextDrawingContext_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmTextDrawingContext_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmTextDrawingContext_Method_SetGraphicsFactory = (*env)->GetMethodID(env, object_class, "SetGraphicsFactory", "(Lorg/mm/nwsi/mmGraphicsFactory;)V");
    mmTextDrawingContext_Method_SetTextParagraphFormat = (*env)->GetMethodID(env, object_class, "SetTextParagraphFormat", "(Lorg/mm/nwsi/mmTextParagraphFormat;)V");
    mmTextDrawingContext_Method_SetRenderTarget = (*env)->GetMethodID(env, object_class, "SetRenderTarget", "(Ljava/lang/Object;)V");
    mmTextDrawingContext_Method_SetBitmap = (*env)->GetMethodID(env, object_class, "SetBitmap", "(Ljava/lang/Object;)V");
    mmTextDrawingContext_Method_SetPass = (*env)->GetMethodID(env, object_class, "SetPass", "(I)V");
    mmTextDrawingContext_Method_OnFinishLaunching = (*env)->GetMethodID(env, object_class, "OnFinishLaunching", "()V");
    mmTextDrawingContext_Method_OnBeforeTerminate = (*env)->GetMethodID(env, object_class, "OnBeforeTerminate", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextDrawingContext_Method_NewObject = NULL;
    mmTextDrawingContext_Method_Init = NULL;
    mmTextDrawingContext_Method_Destroy = NULL;
    mmTextDrawingContext_Method_SetGraphicsFactory = NULL;
    mmTextDrawingContext_Method_SetTextParagraphFormat = NULL;
    mmTextDrawingContext_Method_SetRenderTarget = NULL;
    mmTextDrawingContext_Method_SetBitmap = NULL;
    mmTextDrawingContext_Method_SetPass = NULL;
    mmTextDrawingContext_Method_OnFinishLaunching = NULL;
    mmTextDrawingContext_Method_OnBeforeTerminate = NULL;

    (*env)->DeleteGlobalRef(env, mmTextDrawingContext_ObjectClass);
    mmTextDrawingContext_ObjectClass = NULL;
}