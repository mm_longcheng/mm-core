/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmGraphicsFactory.h"

#include "nwsi/mmPackageAssets.h"
#include "nwsi/mmJavaNativeAndroid.h"

#include "core/mmByte.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "dish/mmJavaVMEnv.h"
#include "dish/mmFileContext.h"

static
void*
mmGraphicsFactory_CreateTypeface(
    struct mmGraphicsFactory*                      p,
    jmethodID                                      method,
    const char*                                    pFontPath)
{
    jobject j_pHandle = NULL;
    jobject j_pHandlePtr = NULL;
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pFontPath = (*env)->NewStringUTF(env, pFontPath);

    j_pHandle = (*env)->CallObjectMethod(
        env, 
        p->jNativePtr, 
        method, 
        j_pFontPath);

    mmJavaNative_CreateGlobalRef(&j_pHandlePtr, env, j_pHandle);

    (*env)->DeleteLocalRef(env, j_pFontPath);
    (*env)->DeleteLocalRef(env, j_pHandle);

    return (void*)j_pHandlePtr;
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_DeleteTypeface(
    struct mmGraphicsFactory*                      p,
    void*                                          pHandle)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();
    jobject j_pHandlePtr = (jobject)(pHandle);
    (*env)->CallVoidMethod(
        env, 
        p->jNativePtr, 
        mmGraphicsFactory_Method_DeleteTypeface, 
        j_pHandlePtr);

    mmJavaNative_DeleteGlobalRef(&j_pHandlePtr, env);
}

static
int
mmGraphicsFactory_CreateFontTypeface(
    struct mmGraphicsFactory*                      p,
    jmethodID                                      method,
    const char*                                    pFontFamily,
    const char*                                    pFontPath,
    const char*                                    pRealPath)
{
    int err = MM_UNKNOWN;

    do
    {
        void* pHandle;

        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        struct mmFontResource* u;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL != it)
        {
            err = MM_SUCCESS;
            break;
        }

        pHandle = mmGraphicsFactory_CreateTypeface(p, method, pFontPath);
        if (NULL == pHandle)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_CreateTypeface failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        u = (struct mmFontResource*)mmMalloc(sizeof(struct mmFontResource));
        mmFontResource_Init(u);
        mmString_Assign(&u->hFamily, &hWeakFamily);
        mmString_Assigns(&u->hPath, pFontPath);
        mmString_Assigns(&u->hRealPath, pRealPath);
        u->pHandle = (void*)pHandle;
        mmRbtreeStringVpt_Set(&p->hFonts, &hWeakFamily, u);
        err = MM_SUCCESS;
    } while (0);

    return err;
}

static
int
mmGraphicsFactory_DeleteFontTypeface(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily)
{
    int err = MM_UNKNOWN;

    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        void* pHandle;

        struct mmFontResource* u;

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL == it)
        {
            err = MM_SUCCESS;
            break;
        }

        u = (struct mmFontResource*)it->v;

        pHandle = u->pHandle;

        mmRbtreeStringVpt_Rmv(&p->hFonts, &hWeakFamily);
        mmFontResource_Destroy(u);
        mmFree(u);

        // The number of fonts added indicates success. Zero indicates failure. 
        // To get extended error information, call GetLastError.
        mmGraphicsFactory_DeleteTypeface(p, pHandle);
        err = MM_SUCCESS;
    } while (0);

    return err;
}

MM_EXPORT_NWSI
void
mmFontResource_Init(
    struct mmFontResource*                         p)
{
    mmString_Init(&p->hFamily);
    mmString_Init(&p->hPath);
    mmString_Init(&p->hRealPath);
    p->pHandle = NULL;
}

MM_EXPORT_NWSI
void
mmFontResource_Destroy(
    struct mmFontResource*                         p)
{
    p->pHandle = NULL;
    mmString_Destroy(&p->hRealPath);
    mmString_Destroy(&p->hPath);
    mmString_Destroy(&p->hFamily);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_Init(
    struct mmGraphicsFactory*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    mmRbtreeStringVpt_Init(&p->hFonts);

    p->pPackageAssets = NULL;

    jobject j_NewObject = (*env)->NewObject(env, mmGraphicsFactory_ObjectClass, mmGraphicsFactory_Method_NewObject);
    p->jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmGraphicsFactory_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);

    mmGraphicsFactory_SetNativePtr(env, (jobject)p->jNativePtr, p);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_Destroy(
    struct mmGraphicsFactory*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    assert(0 == p->hFonts.size && "assets is not destroy complete.");

    mmGraphicsFactory_SetNativePtr(env, (jobject)p->jNativePtr, NULL);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmGraphicsFactory_Method_Destroy);
    (*env)->DeleteGlobalRef(env, p->jNativePtr);
    p->jNativePtr = NULL;

    p->pPackageAssets = NULL;

    mmRbtreeStringVpt_Destroy(&p->hFonts);
}

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_SetPackageAssets(
    struct mmGraphicsFactory*                      p,
    struct mmPackageAssets*                        pPackageAssets)
{
    p->pPackageAssets = pPackageAssets;
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_OnFinishLaunching(
    struct mmGraphicsFactory*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_obj = (jobject)((NULL == p->pPackageAssets) ? NULL : p->pPackageAssets->jNativePtr);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmGraphicsFactory_Method_SetPackageAssets, j_obj);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmGraphicsFactory_Method_OnFinishLaunching);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_OnBeforeTerminate(
    struct mmGraphicsFactory*                      p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmGraphicsFactory_Method_OnBeforeTerminate);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmGraphicsFactory_Method_SetPackageAssets, NULL);
}

MM_EXPORT_NWSI
int
mmGraphicsFactory_LoadFontFromResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath)
{
    int err = MM_UNKNOWN;
    int hAssetsType;
    const char* pRealPath;
    struct mmString hRealPath;
    mmString_Init(&hRealPath);
    hAssetsType = mmFileContext_GetFileAssetsPath(pFileContext, pFontPath, &hRealPath);
    pRealPath = mmString_CStr(&hRealPath);
    switch (hAssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        err = mmGraphicsFactory_CreateFontTypeface(
            p, 
            mmGraphicsFactory_Method_CreateTypefaceFromFolder, 
            pFontFamily, 
            pFontPath, 
            pRealPath);
    }
    break;

    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        err = mmGraphicsFactory_CreateFontTypeface(
            p, 
            mmGraphicsFactory_Method_CreateTypefaceFromSource, 
            pFontFamily, 
            pFontPath, 
            pRealPath);
    }
    break;

    default:
    {
        // Not support assets type.
        err = MM_FAILURE;
    }
    break;
    }
    mmString_Destroy(&hRealPath);
    return err;
}

MM_EXPORT_NWSI
int
mmGraphicsFactory_UnloadFontByResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath)
{
    int err = MM_UNKNOWN;
    int hAssetsType;
    hAssetsType = mmFileContext_GetFileAssetsType(pFileContext, pFontPath);
    switch (hAssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        err = mmGraphicsFactory_DeleteFontTypeface(p, pFontFamily);
    }
    break;

    default:
    {
        // Not support assets type.
        err = MM_FAILURE;
    }
    break;
    }
    return err;
}

MM_EXPORT_NWSI
const struct mmFontResource*
mmGraphicsFactory_GetFontResourceByFamily(
    const struct mmGraphicsFactory*                p,
    const char*                                    pFontFamily)
{
    const struct mmFontResource* u = NULL;
    struct mmString hWeakFamily;
    struct mmRbtreeStringVptIterator* it;
    mmString_MakeWeaks(&hWeakFamily, pFontFamily);
    it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
    if (NULL != it)
    {
        u = (const struct mmFontResource*)it->v;
    }
    else
    {
        u = NULL;
    }
    return u;
}

MM_EXPORT_NWSI jclass mmGraphicsFactory_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmGraphicsFactory_Field_hNativePtr = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_SetPackageAssets = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_OnFinishLaunching = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_OnBeforeTerminate = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_CreateTypefaceFromFolder = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_CreateTypefaceFromSource = NULL;
MM_EXPORT_NWSI jmethodID mmGraphicsFactory_Method_DeleteTypeface = NULL;

MM_EXPORT_NWSI
void
mmGraphicsFactory_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmGraphicsFactory");
    mmGraphicsFactory_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmGraphicsFactory_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");
    mmGraphicsFactory_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmGraphicsFactory_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmGraphicsFactory_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmGraphicsFactory_Method_SetPackageAssets = (*env)->GetMethodID(env, object_class, "SetPackageAssets", "(Lorg/mm/nwsi/mmPackageAssets;)V");
    mmGraphicsFactory_Method_OnFinishLaunching = (*env)->GetMethodID(env, object_class, "OnFinishLaunching", "()V");
    mmGraphicsFactory_Method_OnBeforeTerminate = (*env)->GetMethodID(env, object_class, "OnBeforeTerminate", "()V");
    mmGraphicsFactory_Method_CreateTypefaceFromFolder = (*env)->GetMethodID(env, object_class, "CreateTypefaceFromFolder", "(Ljava/lang/String;)Ljava/lang/Object;");
    mmGraphicsFactory_Method_CreateTypefaceFromSource = (*env)->GetMethodID(env, object_class, "CreateTypefaceFromSource", "(Ljava/lang/String;)Ljava/lang/Object;");
    mmGraphicsFactory_Method_DeleteTypeface = (*env)->GetMethodID(env, object_class, "DeleteTypeface", "(Ljava/lang/Object;)V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmGraphicsFactory_Field_hNativePtr = NULL;
    mmGraphicsFactory_Method_NewObject = NULL;
    mmGraphicsFactory_Method_Init = NULL;
    mmGraphicsFactory_Method_Destroy = NULL;
    mmGraphicsFactory_Method_SetPackageAssets = NULL;
    mmGraphicsFactory_Method_OnFinishLaunching = NULL;
    mmGraphicsFactory_Method_OnBeforeTerminate = NULL;
    mmGraphicsFactory_Method_CreateTypefaceFromFolder = NULL;
    mmGraphicsFactory_Method_CreateTypefaceFromSource = NULL;
    mmGraphicsFactory_Method_DeleteTypeface = NULL;

    (*env)->DeleteGlobalRef(env, mmGraphicsFactory_ObjectClass);
    mmGraphicsFactory_ObjectClass = NULL;
}

MM_EXPORT_NWSI struct mmGraphicsFactory* mmGraphicsFactory_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmGraphicsFactory_Field_hNativePtr);
    return (struct mmGraphicsFactory*)j_hNativePtr;
}
MM_EXPORT_NWSI void mmGraphicsFactory_SetNativePtr(JNIEnv* env, jobject obj, struct mmGraphicsFactory* p)
{
    jlong j_hNativePtr = (jlong)p;
    (*env)->SetLongField(env, obj, mmGraphicsFactory_Field_hNativePtr, j_hNativePtr);
}

JNIEXPORT jobject JNICALL Java_org_mm_nwsi_mmGraphicsFactory_NativeGetFontHandleByFamily(JNIEnv* env, jobject obj, jstring pFamily)
{
    struct mmGraphicsFactory* p = mmGraphicsFactory_GetNativePtr(env, obj);

    jobject j_Handle = NULL;
    const char* j_Family_c_str = (*env)->GetStringUTFChars(env, pFamily, NULL);
    const char* r_Family_c_str = NULL == j_Family_c_str ? "" : j_Family_c_str;

    const struct mmFontResource* u = NULL;
    u = mmGraphicsFactory_GetFontResourceByFamily(p, r_Family_c_str);
    j_Handle = (NULL == u) ? NULL : (jobject)u->pHandle;

    (*env)->ReleaseStringUTFChars(env, pFamily, j_Family_c_str);

    return j_Handle;
}
