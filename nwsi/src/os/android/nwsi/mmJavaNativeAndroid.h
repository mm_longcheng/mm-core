/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmJavaNativeAndroid_h__
#define __mmJavaNativeAndroid_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI void mmJavaNative_JObjectInit(jobject* j);
MM_EXPORT_NWSI void mmJavaNative_JObjectDestroy(jobject* j);
MM_EXPORT_NWSI void mmJavaNative_CreateGlobalRef(jobject* j, JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmJavaNative_DeleteGlobalRef(jobject* j, JNIEnv* env);
MM_EXPORT_NWSI void mmJavaNative_SetLongField(JNIEnv* env, jobject obj, jfieldID field, jlong value);

// java.lang.Integer
MM_EXPORT_NWSI extern jclass mmJavaLangInteger_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmJavaLangInteger_Method_NewObject;
MM_EXPORT_NWSI extern jmethodID mmJavaLangInteger_Method_intValue;
MM_EXPORT_NWSI void mmJavaLangInteger_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaLangInteger_DetachVirtualMachine(JNIEnv* env);

// java.util.Iterator
MM_EXPORT_NWSI extern jclass mmJavaUtilIterator_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilIterator_Method_hasNext;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilIterator_Method_next;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilIterator_Method_remove;
MM_EXPORT_NWSI void mmJavaUtilIterator_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaUtilIterator_DetachVirtualMachine(JNIEnv* env);

// java.util.LinkedList
MM_EXPORT_NWSI extern jclass mmJavaUtilLinkedList_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilLinkedList_Method_iterator;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilLinkedList_Method_add;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilLinkedList_Method_clear;
MM_EXPORT_NWSI void mmJavaUtilLinkedList_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaUtilLinkedList_DetachVirtualMachine(JNIEnv* env);

// java.util.Map$Entry
MM_EXPORT_NWSI extern jclass mmJavaUtilMapEntry_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilMapEntry_Method_getKey;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilMapEntry_Method_getValue;
MM_EXPORT_NWSI void mmJavaUtilMapEntry_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaUtilMapEntry_DetachVirtualMachine(JNIEnv* env);

// java.util.Set
MM_EXPORT_NWSI extern jclass mmJavaUtilSet_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilSet_Method_iterator;
MM_EXPORT_NWSI void mmJavaUtilSet_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaUtilSet_DetachVirtualMachine(JNIEnv* env);

// java.util.TreeMap
MM_EXPORT_NWSI extern jclass mmJavaUtilTreeMap_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilTreeMap_Method_entrySet;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilTreeMap_Method_put;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilTreeMap_Method_get;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilTreeMap_Method_remove;
MM_EXPORT_NWSI extern jmethodID mmJavaUtilTreeMap_Method_clear;
MM_EXPORT_NWSI void mmJavaUtilTreeMap_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaUtilTreeMap_DetachVirtualMachine(JNIEnv* env);

// android.graphics.Matrix
MM_EXPORT_NWSI extern jclass mmAndroidGraphicsMatrix_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmAndroidGraphicsMatrix_Method_NewObject;
MM_EXPORT_NWSI extern jmethodID mmAndroidGraphicsMatrix_Method_setValues;
MM_EXPORT_NWSI void mmAndroidGraphicsMatrix_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmAndroidGraphicsMatrix_DetachVirtualMachine(JNIEnv* env);

MM_EXPORT_NWSI void mmJavaNativeAndroid_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmJavaNativeAndroid_DetachVirtualMachine(JNIEnv* env);

#include "core/mmSuffix.h"

#endif//__mmJavaNativeAndroid_h__
