/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNwsiNative.h"

#include <jni.h>

#include "dish/mmJavaVMEnv.h"

#include "mmJavaNativeAndroid.h"

#include "mmNativeApplication.h"
#include "mmDevice.h"
#include "mmLocale.h"
#include "mmSecurityStore.h"
#include "mmPackageAssets.h"
#include "mmAppIdentifier.h"
#include "mmNativeClipboard.h"
#include "mmISurfaceNative.h"
#include "mmUIViewSurfaceMaster.h"

#include "mmRangeNative.h"
#include "mmTextFormatNative.h"
#include "mmTextParagraphFormatNative.h"
#include "mmTextSpannableNative.h"
#include "mmTextUtf16String.h"
#include "mmTextParagraph.h"
#include "mmTextRenderTarget.h"
#include "mmTextDrawingContext.h"
#include "mmGraphicsFactory.h"

#ifndef MM_STATIC_NWSI
JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    mmNwsi_AttachVirtualMachine(vm);
    
    return JNI_VERSION_1_4;
}
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved)
{
    mmNwsi_DetachVirtualMachine(vm);
}
#endif//MM_STATIC_NWSI

MM_EXPORT_NWSI void mmNwsi_AttachVirtualMachine(JavaVM* vm)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    mmJavaNativeAndroid_AttachVirtualMachine(env);

    mmNativeApplication_AttachVirtualMachine(env);
    mmDevice_AttachVirtualMachine(env);
    mmLocale_AttachVirtualMachine(env);
    mmSecurityStore_AttachVirtualMachine(env);
    mmPackageAssets_AttachVirtualMachine(env);
    mmAppIdentifier_AttachVirtualMachine(env);
    mmUIViewSurfaceMaster_AttachVirtualMachine(env);
    mmNativeClipboard_AttachVirtualMachine(env);

    mmEventKeypad_AttachVirtualMachine(env);
    mmEventCursor_AttachVirtualMachine(env);
    mmEventTouch_AttachVirtualMachine(env);
    mmEventTouchs_AttachVirtualMachine(env);
    mmEventKeypadStatus_AttachVirtualMachine(env);
    mmEventMetrics_AttachVirtualMachine(env);

    mmRange_AttachVirtualMachine(env);
    mmTextFormat_AttachVirtualMachine(env);
    mmTextParagraphFormat_AttachVirtualMachine(env);
    mmTextHitMetrics_AttachVirtualMachine(env);
    mmTextSpan_AttachVirtualMachine(env);
    mmTextSpannable_AttachVirtualMachine(env);
    mmTextUtf16String_AttachVirtualMachine(env);
    mmTextParagraph_AttachVirtualMachine(env);
    mmTextRenderTarget_AttachVirtualMachine(env);
    mmTextDrawingContext_AttachVirtualMachine(env);
    mmGraphicsFactory_AttachVirtualMachine(env);
}
MM_EXPORT_NWSI void mmNwsi_DetachVirtualMachine(JavaVM* vm)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    mmJavaNativeAndroid_DetachVirtualMachine(env);

    mmNativeApplication_DetachVirtualMachine(env);
    mmDevice_DetachVirtualMachine(env);
    mmLocale_DetachVirtualMachine(env);
    mmSecurityStore_DetachVirtualMachine(env);
    mmPackageAssets_DetachVirtualMachine(env);
    mmAppIdentifier_DetachVirtualMachine(env);
    mmUIViewSurfaceMaster_DetachVirtualMachine(env);
    mmNativeClipboard_DetachVirtualMachine(env);

    mmEventKeypad_DetachVirtualMachine(env);
    mmEventCursor_DetachVirtualMachine(env);
    mmEventTouch_DetachVirtualMachine(env);
    mmEventTouchs_DetachVirtualMachine(env);
    mmEventKeypadStatus_DetachVirtualMachine(env);
    mmEventMetrics_DetachVirtualMachine(env);

    mmRange_DetachVirtualMachine(env);
    mmTextFormat_DetachVirtualMachine(env);
    mmTextParagraphFormat_DetachVirtualMachine(env);
    mmTextHitMetrics_DetachVirtualMachine(env);
    mmTextSpan_DetachVirtualMachine(env);
    mmTextSpannable_DetachVirtualMachine(env);
    mmTextUtf16String_DetachVirtualMachine(env);
    mmTextParagraph_DetachVirtualMachine(env);
    mmTextRenderTarget_DetachVirtualMachine(env);
    mmTextDrawingContext_DetachVirtualMachine(env);
    mmGraphicsFactory_DetachVirtualMachine(env);
}