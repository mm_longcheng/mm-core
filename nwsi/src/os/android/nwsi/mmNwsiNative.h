/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmNwsiNative_h__
#define __mmNwsiNative_h__

#include "core/mmCore.h"

#include <jni.h>

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI void mmNwsi_AttachVirtualMachine(JavaVM* vm);
MM_EXPORT_NWSI void mmNwsi_DetachVirtualMachine(JavaVM* vm);

#ifndef MM_STATIC_NWSI
static mmInline void mmNwsiNative_AttachVirtualMachine(JavaVM* vm)
{

}
static mmInline void mmNwsiNative_DetachVirtualMachine(JavaVM* vm)
{

}
#else
static mmInline void mmNwsiNative_AttachVirtualMachine(JavaVM* vm)
{
    mmNwsi_AttachVirtualMachine(vm);
}
static mmInline void mmNwsiNative_DetachVirtualMachine(JavaVM* vm)
{
    mmNwsi_DetachVirtualMachine(vm);
}
#endif//MM_STATIC_NWSI

#include "core/mmSuffix.h"

#endif//__mmNwsiNative_h__
