/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextRenderTarget.h"
#include "mmTextDrawingContext.h"
#include "mmGraphicsFactory.h"

#include "mmJavaNativeAndroid.h"

#include "nwsi/mmTextParagraphFormat.h"

#include "dish/mmJavaVMEnv.h"

#include <assert.h>

MM_EXPORT_NWSI
void
mmTextRenderTarget_Init(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    p->jByteBuffer = NULL;

    jobject j_NewObject = (*env)->NewObject(env, mmTextRenderTarget_ObjectClass, mmTextRenderTarget_Method_NewObject);
    p->jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_Destroy(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_Destroy);
    (*env)->DeleteGlobalRef(env, p->jNativePtr);
    p->jNativePtr = NULL;

    assert(NULL == p->jByteBuffer && "jByteBuffer must be a null.");
    p->jByteBuffer = NULL;

    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetGraphicsFactory(
    struct mmTextRenderTarget*                     p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    p->pGraphicsFactory = pGraphicsFactory;

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_SetGraphicsFactory, pGraphicsFactory->jNativePtr);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetTextParagraphFormat(
    struct mmTextRenderTarget*                     p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    p->pParagraphFormat = pParagraphFormat;

    jobject j_obj = (jobject)(pParagraphFormat->pObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_SetTextParagraphFormat, j_obj);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_OnFinishLaunching(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_OnFinishLaunching);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_OnBeforeTerminate(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_OnBeforeTerminate);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BeginDraw(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_BeginDraw);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetTransform(
    struct mmTextRenderTarget*                     p,
    struct mmAffineTransform*                      hTransform)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_Matrix = (*env)->NewObject(env, mmAndroidGraphicsMatrix_ObjectClass, mmAndroidGraphicsMatrix_Method_NewObject);
    jfloatArray j_Values = (*env)->NewFloatArray(env, 9);

    jfloat hValues[9] = { 0 };
    hValues[0] = hTransform->a; hValues[1] = hTransform->b; hValues[2] = 0.0f;
    hValues[3] = hTransform->c; hValues[4] = hTransform->d; hValues[5] = 0.0f;
    hValues[6] = hTransform->x; hValues[7] = hTransform->y; hValues[8] = 1.0f;

    (*env)->SetFloatArrayRegion(env, j_Values, 0, 9, hValues);

    (*env)->CallVoidMethod(env, j_Matrix, mmAndroidGraphicsMatrix_Method_setValues, j_Values);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_SetTransform, j_Matrix);

    (*env)->DeleteLocalRef(env, j_Values);
    (*env)->DeleteLocalRef(env, j_Matrix);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_Clear(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_Clear);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_EndDraw(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_EndDraw);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BeginTransparency(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_BeginTransparency);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_EndTransparency(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_EndTransparency);
}

MM_EXPORT_NWSI
const void*
mmTextRenderTarget_BufferLock(
    struct mmTextRenderTarget*                     p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_pBuffer = (*env)->CallObjectMethod(env, p->jNativePtr, mmTextRenderTarget_Method_BufferLock);
    p->jByteBuffer = (jbyteArray)(j_pBuffer);
    if (NULL == p->jByteBuffer)
    {
        return NULL;
    }
    else
    {
        return (*env)->GetByteArrayElements(env, p->jByteBuffer, NULL);
    }
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BufferUnLock(
    struct mmTextRenderTarget*                     p,
    const void*                                    pBuffer)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    if (NULL != pBuffer)
    {
        (*env)->CallVoidMethod(env, p->jNativePtr, mmTextRenderTarget_Method_BufferUnLock, p->jByteBuffer);
        (*env)->ReleaseByteArrayElements(env, p->jByteBuffer, (jbyteArray)pBuffer, 0);
        (*env)->DeleteLocalRef(env, p->jByteBuffer);
        p->jByteBuffer = NULL;
    }
    else
    {
        p->jByteBuffer = NULL;
    }
}

MM_EXPORT_NWSI jclass mmTextRenderTarget_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextRenderTarget_Field_pBitmap = NULL;
MM_EXPORT_NWSI jfieldID mmTextRenderTarget_Field_pCanvas = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_SetGraphicsFactory = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_SetTextParagraphFormat = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_OnFinishLaunching = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_OnBeforeTerminate = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_BeginDraw = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_SetTransform = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_Clear = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_EndDraw = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_BeginTransparency = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_EndTransparency = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_BufferLock = NULL;
MM_EXPORT_NWSI jmethodID mmTextRenderTarget_Method_BufferUnLock = NULL;

MM_EXPORT_NWSI
void
mmTextRenderTarget_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextRenderTarget");
    mmTextRenderTarget_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextRenderTarget_Field_pBitmap = (*env)->GetFieldID(env, object_class, "pBitmap", "Landroid/graphics/Bitmap;");
    mmTextRenderTarget_Field_pCanvas = (*env)->GetFieldID(env, object_class, "pCanvas", "Landroid/graphics/Canvas;");
    mmTextRenderTarget_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmTextRenderTarget_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmTextRenderTarget_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmTextRenderTarget_Method_SetGraphicsFactory = (*env)->GetMethodID(env, object_class, "SetGraphicsFactory", "(Lorg/mm/nwsi/mmGraphicsFactory;)V");
    mmTextRenderTarget_Method_SetTextParagraphFormat = (*env)->GetMethodID(env, object_class, "SetTextParagraphFormat", "(Lorg/mm/nwsi/mmTextParagraphFormat;)V");
    mmTextRenderTarget_Method_OnFinishLaunching = (*env)->GetMethodID(env, object_class, "OnFinishLaunching", "()V");
    mmTextRenderTarget_Method_OnBeforeTerminate = (*env)->GetMethodID(env, object_class, "OnBeforeTerminate", "()V");
    mmTextRenderTarget_Method_BeginDraw = (*env)->GetMethodID(env, object_class, "BeginDraw", "()V");
    mmTextRenderTarget_Method_SetTransform = (*env)->GetMethodID(env, object_class, "SetTransform", "(Landroid/graphics/Matrix;)V");
    mmTextRenderTarget_Method_Clear = (*env)->GetMethodID(env, object_class, "Clear", "()V");
    mmTextRenderTarget_Method_EndDraw = (*env)->GetMethodID(env, object_class, "EndDraw", "()V");
    mmTextRenderTarget_Method_BeginTransparency = (*env)->GetMethodID(env, object_class, "BeginTransparency", "()V");
    mmTextRenderTarget_Method_EndTransparency = (*env)->GetMethodID(env, object_class, "EndTransparency", "()V");
    mmTextRenderTarget_Method_BufferLock = (*env)->GetMethodID(env, object_class, "BufferLock", "()[B");
    mmTextRenderTarget_Method_BufferUnLock = (*env)->GetMethodID(env, object_class, "BufferUnLock", "([B)V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextRenderTarget_Field_pBitmap = NULL;
    mmTextRenderTarget_Field_pCanvas = NULL;
    mmTextRenderTarget_Method_NewObject = NULL;
    mmTextRenderTarget_Method_Init = NULL;
    mmTextRenderTarget_Method_Destroy = NULL;
    mmTextRenderTarget_Method_SetGraphicsFactory = NULL;
    mmTextRenderTarget_Method_SetTextParagraphFormat = NULL;
    mmTextRenderTarget_Method_OnFinishLaunching = NULL;
    mmTextRenderTarget_Method_OnBeforeTerminate = NULL;
    mmTextRenderTarget_Method_BeginDraw = NULL;
    mmTextRenderTarget_Method_SetTransform = NULL;
    mmTextRenderTarget_Method_Clear = NULL;
    mmTextRenderTarget_Method_EndDraw = NULL;
    mmTextRenderTarget_Method_BeginTransparency = NULL;
    mmTextRenderTarget_Method_EndTransparency = NULL;
    mmTextRenderTarget_Method_BufferLock = NULL;
    mmTextRenderTarget_Method_BufferUnLock = NULL;

    (*env)->DeleteGlobalRef(env, mmTextRenderTarget_ObjectClass);
    mmTextRenderTarget_ObjectClass = NULL;
}