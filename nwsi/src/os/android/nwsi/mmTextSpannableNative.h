/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextSpannableNative_h__
#define __mmTextSpannableNative_h__

#include "core/mmCore.h"

#include "nwsi/mmTextSpannable.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmTextSpan_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmTextSpan_Field_hRange;
MM_EXPORT_NWSI extern jfieldID mmTextSpan_Field_hFormat;
MM_EXPORT_NWSI extern jmethodID mmTextSpan_Method_NewObject;

MM_EXPORT_NWSI 
void 
mmTextSpan_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextSpan_DetachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextSpan_Decode(
    JNIEnv*                                        env, 
    struct mmTextSpan*                             content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextSpan_Encode(
    JNIEnv*                                        env, 
    struct mmTextSpan*                             content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextSpannable_NativeInit(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_NativeDestroy(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_NativeEncode(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI 
void 
mmTextSpannable_NativeDecode(
    struct mmTextSpannable*                        p);

MM_EXPORT_NWSI extern jclass mmTextSpannable_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmTextSpannable_Field_hAttributes;
MM_EXPORT_NWSI extern jfieldID mmTextSpannable_Field_pObject;
MM_EXPORT_NWSI extern jmethodID mmTextSpannable_Method_NewObject;
MM_EXPORT_NWSI extern jmethodID mmTextSpannable_Method_Init;
MM_EXPORT_NWSI extern jmethodID mmTextSpannable_Method_Destroy;
MM_EXPORT_NWSI extern jmethodID mmTextSpannable_Method_AddSpan;
MM_EXPORT_NWSI extern jmethodID mmTextSpannable_Method_ClearSpan;

MM_EXPORT_NWSI 
void 
mmTextSpannable_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextSpannable_DetachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextSpannable_Decode(
    JNIEnv*                                        env, 
    struct mmTextSpannable*                        content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextSpannable_Encode(
    JNIEnv*                                        env, 
    struct mmTextSpannable*                        content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextSpannable_DecodeSpans(
    JNIEnv*                                        env, 
    struct mmTextSpannable*                        content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextSpannable_EncodeSpans(
    JNIEnv*                                        env, 
    struct mmTextSpannable*                        content, 
    jobject                                        obj);

#include "core/mmPrefix.h"

MM_EXPORT_NWSI struct mmTextSpannable* mmTextSpannable_GetNativePtr(JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmTextSpannable_SetNativePtr(JNIEnv* env, jobject obj, struct mmTextSpannable* p);

#include "core/mmSuffix.h"

#include "core/mmSuffix.h"

#endif//__mmTextSpannableNative_h__
