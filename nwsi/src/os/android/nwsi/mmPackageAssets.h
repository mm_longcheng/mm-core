/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPackageAssets_h__
#define __mmPackageAssets_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmPackageAssets_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmPackageAssets_Field_hNativePtr;

MM_EXPORT_NWSI void mmPackageAssets_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmPackageAssets_DetachVirtualMachine(JNIEnv* env);

struct mmPackageAssets
{
    struct mmString hModuleName;
    
    struct mmString hPackageName;
    struct mmString hVersionName;
    struct mmString hVersionCode;
    struct mmString hDisplayName;
    
    struct mmString hHomeDirectory;

    struct mmString hInternalFilesDirectory;
    struct mmString hInternalCacheDirectory;
    struct mmString hExternalFilesDirectory;
    struct mmString hExternalCacheDirectory;

    struct mmString hPluginFolder;
    struct mmString hDynlibFolder;
    
    struct mmString hShaderCacheFolder;
    struct mmString hTextureCacheFolder;

    struct mmString hLoggerPathName;
    struct mmString hLoggerPath;
    
    struct mmString hAssetsRootFolderPath;
    struct mmString hAssetsRootFolderBase;
    
    struct mmString hAssetsRootSourcePath;
    struct mmString hAssetsRootSourceBase;
    
    void* pAppHandler;
    void* pAssetManager;

    jobject jAssetManagerPtr;
    
    // object for platform implement.
    void* jNativePtr;
};

MM_EXPORT_NWSI void mmPackageAssets_Init(struct mmPackageAssets* p);
MM_EXPORT_NWSI void mmPackageAssets_Destroy(struct mmPackageAssets* p);

MM_EXPORT_NWSI void mmPackageAssets_SetAppHandler(struct mmPackageAssets* p, void* pAppHandler);
MM_EXPORT_NWSI void mmPackageAssets_SetAssetManager(struct mmPackageAssets* p, void* pAssetManager);
MM_EXPORT_NWSI void mmPackageAssets_SetModuleName(struct mmPackageAssets* p, const char* pModuleName);
MM_EXPORT_NWSI void mmPackageAssets_SetPluginFolder(struct mmPackageAssets* p, const char* pPluginFolder);
MM_EXPORT_NWSI void mmPackageAssets_SetDynlibFolder(struct mmPackageAssets* p, const char* pDynlibFolder);
MM_EXPORT_NWSI void mmPackageAssets_SetShaderCacheFolder(struct mmPackageAssets* p, const char* pShaderCacheFolder);
MM_EXPORT_NWSI void mmPackageAssets_SetTextureCacheFolder(struct mmPackageAssets* p, const char* pTextureCacheFolder);
MM_EXPORT_NWSI void mmPackageAssets_SetLoggerPathName(struct mmPackageAssets* p, const char* pLoggerPathName);
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootFolder(struct mmPackageAssets* p, const char* pPath, const char* pBase);
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootSource(struct mmPackageAssets* p, const char* pPath, const char* pBase);

MM_EXPORT_NWSI void mmPackageAssets_AttachNativeResource(struct mmPackageAssets* p);
MM_EXPORT_NWSI void mmPackageAssets_DetachNativeResource(struct mmPackageAssets* p);

MM_EXPORT_NWSI void mmPackageAssets_OnFinishLaunching(struct mmPackageAssets* p);
MM_EXPORT_NWSI void mmPackageAssets_OnBeforeTerminate(struct mmPackageAssets* p);

// Utility function.
MM_EXPORT_NWSI 
void
mmPackageAssets_MakeShaderCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname);

MM_EXPORT_NWSI
void
mmPackageAssets_MakeTextureCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname);

// jni interface.
MM_EXPORT_NWSI struct mmPackageAssets* mmPackageAssets_GetNativePtr(JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmPackageAssets_SetNativePtr(JNIEnv* env, jobject obj, struct mmPackageAssets* p);

#include "core/mmSuffix.h"

#endif//__mmPackageAssets_h__

