/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextParagraphFormatNative_h__
#define __mmTextParagraphFormatNative_h__

#include "core/mmCore.h"

#include "nwsi/mmTextParagraphFormat.h"
#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_NativeInit(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_NativeDestroy(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_NativeEncode(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_NativeDecode(
    struct mmTextParagraphFormat*                  p);

MM_EXPORT_NWSI extern jclass mmTextParagraphFormat_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hMasterFormat;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hFontFeatures;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hTextAlignment;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hParagraphAlignment;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineBreakMode;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hWritingDirection;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hFirstLineHeadIndent;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hHeadIndent;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hTailIndent;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineSpacingAddition;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineSpacingMultiple;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hShadowOffset;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hShadowBlur;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hShadowColor;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineCap;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineJoin;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineMiterLimit;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hAlignBaseline;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLineDescent;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hSuitableSize;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hWindowSize;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hLayoutRect;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hDisplayDensity;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hCaretIndex;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hSelection;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hCaretWrapping;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hCaretStatus;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hSelectionStatus;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hCaretWidth;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hCaretColor;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hSelectionCaptureColor;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_hSelectionReleaseColor;
MM_EXPORT_NWSI extern jfieldID mmTextParagraphFormat_Field_pObject;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_NewObject;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_Init;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_Destroy;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_SetFontFeature;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_RmvFontFeature;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_GetFontFeature;
MM_EXPORT_NWSI extern jmethodID mmTextParagraphFormat_Method_ClearFontFeature;

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_DetachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_Decode(
    JNIEnv*                                        env, 
    struct mmTextParagraphFormat*                  content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_Encode(
    JNIEnv*                                        env, 
    struct mmTextParagraphFormat*                  content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_DecodeFontFeatures(
    JNIEnv*                                        env, 
    struct mmTextParagraphFormat*                  content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextParagraphFormat_EncodeFontFeatures(
    JNIEnv*                                        env, 
    struct mmTextParagraphFormat*                  content, 
    jobject                                        obj);

MM_EXPORT_NWSI struct mmTextParagraphFormat* mmTextParagraphFormat_GetNativePtr(JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmTextParagraphFormat_SetNativePtr(JNIEnv* env, jobject obj, struct mmTextParagraphFormat* p);
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmTextParagraphFormat_NativeUpdateSuitableData(JNIEnv* env, jobject obj);

MM_EXPORT_NWSI extern jclass mmTextHitMetrics_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmTextHitMetrics_Field_rect;
MM_EXPORT_NWSI extern jfieldID mmTextHitMetrics_Field_offset;
MM_EXPORT_NWSI extern jfieldID mmTextHitMetrics_Field_length;
MM_EXPORT_NWSI extern jfieldID mmTextHitMetrics_Field_bidiLevel;
MM_EXPORT_NWSI extern jfieldID mmTextHitMetrics_Field_isText;
MM_EXPORT_NWSI extern jfieldID mmTextHitMetrics_Field_isTrimmed;
MM_EXPORT_NWSI extern jmethodID mmTextHitMetrics_Method_NewObject;

MM_EXPORT_NWSI 
void 
mmTextHitMetrics_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextHitMetrics_DetachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextHitMetrics_Decode(
    JNIEnv*                                        env, 
    struct mmTextHitMetrics*                       content, 
    jobject                                        obj);

MM_EXPORT_NWSI 
void 
mmTextHitMetrics_Encode(
    JNIEnv*                                        env, 
    struct mmTextHitMetrics*                       content, 
    jobject                                        obj);

#include "core/mmSuffix.h"

#endif//__mmTextParagraphFormatNative_h__
