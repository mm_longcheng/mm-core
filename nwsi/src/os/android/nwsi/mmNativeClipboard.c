/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNativeClipboard.h"

#include "core/mmAlloc.h"
#include "core/mmByte.h"
#include "core/mmLogger.h"
#include "core/mmUtf16String.h"

#include "nwsi/mmJavaNativeAndroid.h"

#include "dish/mmJavaVMEnv.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI jclass mmNativeClipboard_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmNativeClipboard_Field_hNativePtr = NULL;
MM_EXPORT_NWSI jmethodID mmNativeClipboard_Method_SetClipboardText = NULL;
MM_EXPORT_NWSI jmethodID mmNativeClipboard_Method_GetClipboardText = NULL;

MM_EXPORT_NWSI void mmNativeClipboard_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmNativeClipboard");
    mmNativeClipboard_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmNativeClipboard_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");
    mmNativeClipboard_Method_SetClipboardText = (*env)->GetMethodID(env, object_class, "SetClipboardText", "(Ljava/lang/String;)V");
    mmNativeClipboard_Method_GetClipboardText = (*env)->GetMethodID(env, object_class, "GetClipboardText", "()Ljava/lang/String;");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmNativeClipboard_DetachVirtualMachine(JNIEnv* env)
{
    mmNativeClipboard_Field_hNativePtr = NULL;
    mmNativeClipboard_Method_SetClipboardText = NULL;
    mmNativeClipboard_Method_GetClipboardText = NULL;

    (*env)->DeleteGlobalRef(env, mmNativeClipboard_ObjectClass);
    mmNativeClipboard_ObjectClass = NULL;
}

static 
void 
__static_mmNativeClipboard_SetByteBuffer(
    struct mmClipboardProvider*                    pSuper,
    const char*                                    pMimeType,
    struct mmByteBuffer*                           pByteBuffer)
{
    do
    {
        JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

        mmUInt8_t* buffer = pByteBuffer->buffer + pByteBuffer->offset;
        size_t length = pByteBuffer->length;
        size_t size = length / sizeof(jchar);

        struct mmNativeClipboard* p = (struct mmNativeClipboard*)(pSuper);

        jobject obj = p->jClipboardPtr;

        if (0 != strcmp("text/plain", pMimeType))
        {
            break;
        }
        
        if(NULL == buffer || 0 == length)
        {
            break;
        }

        if(NULL == obj)
        {
            break;
        }

        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, length);
        mmMemcpy(p->hStreambuf.buff, buffer, length);

        jstring j_text = (*env)->NewString(env, (const jchar*)p->hStreambuf.buff, size);
        (*env)->CallVoidMethod(env, obj, mmNativeClipboard_Method_SetClipboardText, j_text);
        (*env)->DeleteLocalRef(env, j_text);

    }while(0);
}

static 
void 
__static_mmNativeClipboard_GetByteBuffer(
    struct mmClipboardProvider*                    pSuper,
    const char*                                    pMimeType,
    struct mmByteBuffer*                           pByteBuffer)
{
    do
    {
        JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

        struct mmNativeClipboard* p = (struct mmNativeClipboard*)(pSuper);

        jobject obj = p->jClipboardPtr;

        if (0 != strcmp("text/plain", pMimeType))
        {
            break;
        }

        if(NULL == obj)
        {
            break;
        }

        jstring j_text = (jstring)(*env)->CallObjectMethod(env, obj, mmNativeClipboard_Method_GetClipboardText);
        const jchar* j_text_c_str = (*env)->GetStringChars(env, j_text, NULL);
        const jchar* r_text_c_str = NULL == j_text_c_str ? mmUtf16StringEmpty : j_text_c_str;
        jsize size = (*env)->GetStringLength(env, j_text);
        size_t length = size * sizeof(jchar);

        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, length);
        mmMemcpy(p->hStreambuf.buff, r_text_c_str, length);

        (*env)->ReleaseStringChars(env, obj, j_text_c_str);

        (*env)->DeleteLocalRef(env, j_text);

        pByteBuffer->buffer = p->hStreambuf.buff;
        pByteBuffer->offset = 0;
        pByteBuffer->length = length;
    }while(0);
}

MM_EXPORT_NWSI void mmNativeClipboard_Init(struct mmNativeClipboard* p)
{
    mmStreambuf_Init(&p->hStreambuf);

    p->hSuper.SetByteBuffer = &__static_mmNativeClipboard_SetByteBuffer;
    p->hSuper.GetByteBuffer = &__static_mmNativeClipboard_GetByteBuffer;

    mmJavaNative_JObjectInit(&p->jClipboardPtr);
}
MM_EXPORT_NWSI void mmNativeClipboard_Destroy(struct mmNativeClipboard* p)
{
    mmJavaNative_JObjectDestroy(&p->jClipboardPtr);

    p->hSuper.SetByteBuffer = &__static_mmNativeClipboard_SetByteBuffer;
    p->hSuper.GetByteBuffer = &__static_mmNativeClipboard_GetByteBuffer;

    mmStreambuf_Destroy(&p->hStreambuf);
}

MM_EXPORT_NWSI void mmNativeClipboard_SetJavaObject(struct mmNativeClipboard* p, JNIEnv* env, jobject obj)
{
    mmJavaNative_SetLongField(env, p->jClipboardPtr, mmNativeClipboard_Field_hNativePtr, 0);
    mmJavaNative_DeleteGlobalRef(&p->jClipboardPtr, env);
    mmJavaNative_CreateGlobalRef(&p->jClipboardPtr, env, obj);
    mmJavaNative_SetLongField(env, obj, mmNativeClipboard_Field_hNativePtr,  (jlong)p);
}
