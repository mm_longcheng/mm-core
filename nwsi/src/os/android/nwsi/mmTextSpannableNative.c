/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextSpannableNative.h"
#include "mmRangeNative.h"
#include "mmTextFormatNative.h"

#include "mmJavaNativeAndroid.h"

#include "core/mmAlloc.h"

#include "dish/mmJavaVMEnv.h"

MM_EXPORT_NWSI jclass mmTextSpan_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextSpan_Field_hRange = NULL;
MM_EXPORT_NWSI jfieldID mmTextSpan_Field_hFormat = NULL;
MM_EXPORT_NWSI jmethodID mmTextSpan_Method_NewObject = NULL;

MM_EXPORT_NWSI
void
mmTextSpan_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextSpan");
    mmTextSpan_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmTextSpan_Field_hRange = (*env)->GetFieldID(env, object_class, "hRange", "Lorg/mm/math/mmRange;");
    mmTextSpan_Field_hFormat = (*env)->GetFieldID(env, object_class, "hFormat", "Lorg/mm/nwsi/mmTextFormat;");
    mmTextSpan_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextSpan_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextSpan_Field_hRange = NULL;
    mmTextSpan_Field_hFormat = NULL;
    mmTextSpan_Method_NewObject = NULL;

    (*env)->DeleteGlobalRef(env, mmTextSpan_ObjectClass);
    mmTextSpan_ObjectClass = NULL;
}

MM_EXPORT_NWSI
void
mmTextSpan_Decode(
    JNIEnv*                                        env,
    struct mmTextSpan*                             content,
    jobject                                        obj)
{
    jobject j_hRange = (*env)->GetObjectField(env, obj, mmTextSpan_Field_hRange);
    jobject j_hFormat = (*env)->GetObjectField(env, obj, mmTextSpan_Field_hFormat);

    mmRange_Decode(env, &content->hRange, j_hRange);
    mmTextFormat_Decode(env, &content->hFormat, j_hFormat);

    (*env)->DeleteLocalRef(env, j_hRange);
    (*env)->DeleteLocalRef(env, j_hFormat);
}

MM_EXPORT_NWSI
void
mmTextSpan_Encode(
    JNIEnv*                                        env,
    struct mmTextSpan*                             content,
    jobject                                        obj)
{
    jobject j_hRange = (*env)->GetObjectField(env, obj, mmTextSpan_Field_hRange);
    jobject j_hFormat = (*env)->GetObjectField(env, obj, mmTextSpan_Field_hFormat);

    mmRange_Encode(env, &content->hRange, j_hRange);
    mmTextFormat_Encode(env, &content->hFormat, j_hFormat);

    (*env)->DeleteLocalRef(env, j_hRange);
    (*env)->DeleteLocalRef(env, j_hFormat);
}

MM_EXPORT_NWSI
void
mmTextSpannable_NativeInit(
    struct mmTextSpannable*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_NewObject = (*env)->NewObject(env, mmTextSpannable_ObjectClass, mmTextSpannable_Method_NewObject);
    jobject jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    p->pObject = jNativePtr;
    (*env)->CallVoidMethod(env, jNativePtr, mmTextSpannable_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);
}

MM_EXPORT_NWSI
void
mmTextSpannable_NativeDestroy(
    struct mmTextSpannable*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    (*env)->CallVoidMethod(env, jNativePtr, mmTextSpannable_Method_Destroy);
    (*env)->DeleteGlobalRef(env, jNativePtr);
    p->pObject = NULL;
}

MM_EXPORT_NWSI
void
mmTextSpannable_NativeEncode(
    struct mmTextSpannable*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    mmTextSpannable_Encode(env, p, jNativePtr);
}

MM_EXPORT_NWSI
void
mmTextSpannable_NativeDecode(
    struct mmTextSpannable*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    mmTextSpannable_Decode(env, p, jNativePtr);
}

MM_EXPORT_NWSI jclass mmTextSpannable_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextSpannable_Field_hAttributes = NULL;
MM_EXPORT_NWSI jfieldID mmTextSpannable_Field_pObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextSpannable_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextSpannable_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmTextSpannable_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmTextSpannable_Method_AddSpan = NULL;
MM_EXPORT_NWSI jmethodID mmTextSpannable_Method_ClearSpan = NULL;

MM_EXPORT_NWSI
void
mmTextSpannable_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextSpannable");
    mmTextSpannable_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextSpannable_Field_hAttributes = (*env)->GetFieldID(env, object_class, "hAttributes", "Ljava/util/LinkedList;");
    mmTextSpannable_Field_pObject = (*env)->GetFieldID(env, object_class, "pObject", "J");
    mmTextSpannable_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmTextSpannable_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmTextSpannable_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmTextSpannable_Method_AddSpan = (*env)->GetMethodID(env, object_class, "AddSpan", "(Lorg/mm/nwsi/mmTextSpan;)V");
    mmTextSpannable_Method_ClearSpan = (*env)->GetMethodID(env, object_class, "ClearSpan", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextSpannable_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextSpannable_Field_hAttributes = NULL;
    mmTextSpannable_Field_pObject = NULL;
    mmTextSpannable_Method_NewObject = NULL;
    mmTextSpannable_Method_Init = NULL;
    mmTextSpannable_Method_Destroy = NULL;
    mmTextSpannable_Method_AddSpan = NULL;
    mmTextSpannable_Method_ClearSpan = NULL;

    (*env)->DeleteGlobalRef(env, mmTextSpannable_ObjectClass);
    mmTextSpannable_ObjectClass = NULL;
}

MM_EXPORT_NWSI
void
mmTextSpannable_Decode(
    JNIEnv*                                        env,
    struct mmTextSpannable*                        content,
    jobject                                        obj)
{
    mmTextSpannable_DecodeSpans(env, content, obj);
}

MM_EXPORT_NWSI
void
mmTextSpannable_Encode(
    JNIEnv*                                        env,
    struct mmTextSpannable*                        content,
    jobject                                        obj)
{
    mmTextSpannable_EncodeSpans(env, content, obj);
}

MM_EXPORT_NWSI
void
mmTextSpannable_DecodeSpans(
    JNIEnv*                                        env,
    struct mmTextSpannable*                        content,
    jobject                                        obj)
{
    jobject j_hAttributes = (*env)->GetObjectField(env, obj, mmTextSpannable_Field_hAttributes);

    mmTextSpannable_Clear(content);

    struct mmListVpt* pAttributes = &content->hAttributes;

    jobject j_pIteratorObj = (*env)->CallObjectMethod(env, j_hAttributes, mmJavaUtilLinkedList_Method_iterator);

    while ((*env)->CallBooleanMethod(env, j_pIteratorObj, mmJavaUtilIterator_Method_hasNext)) 
    {
        jobject j_pEntryObj = (*env)->CallObjectMethod(env, j_pIteratorObj, mmJavaUtilIterator_Method_next);

        struct mmTextSpan* e = mmTextSpannable_SpanProduce(content);
        mmTextSpan_Decode(env, e, j_pEntryObj);
        mmListVpt_AddTail(pAttributes, e);

        (*env)->DeleteLocalRef(env, j_pEntryObj);
    }

    (*env)->DeleteLocalRef(env, j_pIteratorObj);

    (*env)->DeleteLocalRef(env, j_hAttributes);
}

MM_EXPORT_NWSI
void
mmTextSpannable_EncodeSpans(
    JNIEnv*                                        env,
    struct mmTextSpannable*                        content,
    jobject                                        obj)
{
    jobject j_hAttributes = (*env)->GetObjectField(env, obj, mmTextSpannable_Field_hAttributes);

    struct mmListVpt* pAttributes = &content->hAttributes;

    struct mmTextSpan* e = NULL;

    struct mmListHead* next = NULL;
    struct mmListHead* curr = NULL;
    struct mmListVptIterator* it = NULL;

    jobject j_obj = NULL;

    (*env)->CallVoidMethod(env, j_hAttributes, mmJavaUtilLinkedList_Method_clear);

    next = pAttributes->l.next;
    while (next != &pAttributes->l)
    {
        curr = next;
        next = next->next;
        it = mmList_Entry(curr, struct mmListVptIterator, n);
        e = (struct mmTextSpan*)it->v;

        j_obj = (*env)->NewObject(env, mmTextSpan_ObjectClass, mmTextSpan_Method_NewObject);
        mmTextSpan_Encode(env, e, j_obj);
        (*env)->CallBooleanMethod(env, j_hAttributes, mmJavaUtilLinkedList_Method_add, j_obj);
        (*env)->DeleteLocalRef(env, j_obj);
    }

    (*env)->DeleteLocalRef(env, j_hAttributes);
}

MM_EXPORT_NWSI struct mmTextSpannable* mmTextSpannable_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmTextSpannable_Field_pObject);
    return (struct mmTextSpannable*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmTextSpannable_SetNativePtr(JNIEnv* env, jobject obj, struct mmTextSpannable* p)
{
    jlong j_hNativePtr = (jlong)p;
    (*env)->SetLongField(env, obj, mmTextSpannable_Field_pObject, j_hNativePtr);
}
