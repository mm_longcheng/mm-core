/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextParagraphFormatNative.h"
#include "mmTextFormatNative.h"

#include "mmJavaNativeAndroid.h"

#include "core/mmAlloc.h"

#include "dish/mmJavaVMEnv.h"

MM_EXPORT_NWSI
void
mmTextParagraphFormat_NativeInit(
    struct mmTextParagraphFormat*                  p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_NewObject = (*env)->NewObject(env, mmTextParagraphFormat_ObjectClass, mmTextParagraphFormat_Method_NewObject);
    jobject jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    p->pObject = jNativePtr;
    (*env)->CallVoidMethod(env, jNativePtr, mmTextParagraphFormat_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_NativeDestroy(
    struct mmTextParagraphFormat*                  p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    (*env)->CallVoidMethod(env, jNativePtr, mmTextParagraphFormat_Method_Destroy);
    (*env)->DeleteGlobalRef(env, jNativePtr);
    p->pObject = NULL;
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_NativeEncode(
    struct mmTextParagraphFormat*                  p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    mmTextParagraphFormat_Encode(env, p, jNativePtr);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_NativeDecode(
    struct mmTextParagraphFormat*                  p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject jNativePtr = (jobject)(p->pObject);
    mmTextParagraphFormat_Decode(env, p, jNativePtr);
}

MM_EXPORT_NWSI jclass mmTextParagraphFormat_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hMasterFormat = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hFontFeatures = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hTextAlignment = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hParagraphAlignment = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineBreakMode = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hWritingDirection = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hFirstLineHeadIndent = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hHeadIndent = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hTailIndent = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineSpacingAddition = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineSpacingMultiple = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hShadowOffset = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hShadowBlur = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hShadowColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineCap = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineJoin = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineMiterLimit = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hAlignBaseline = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLineDescent = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hSuitableSize = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hWindowSize = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hLayoutRect = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hDisplayDensity = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hCaretIndex = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hSelection = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hCaretWrapping = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hCaretStatus = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hSelectionStatus = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hCaretWidth = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hCaretColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hSelectionCaptureColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_hSelectionReleaseColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextParagraphFormat_Field_pObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_SetFontFeature = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_RmvFontFeature = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_GetFontFeature = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraphFormat_Method_ClearFontFeature = NULL;

MM_EXPORT_NWSI
void
mmTextParagraphFormat_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextParagraphFormat");
    mmTextParagraphFormat_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextParagraphFormat_Field_hMasterFormat = (*env)->GetFieldID(env, object_class, "hMasterFormat", "Lorg/mm/nwsi/mmTextFormat;");
    mmTextParagraphFormat_Field_hFontFeatures = (*env)->GetFieldID(env, object_class, "hFontFeatures", "Ljava/util/TreeMap;");
    mmTextParagraphFormat_Field_hTextAlignment = (*env)->GetFieldID(env, object_class, "hTextAlignment", "I");
    mmTextParagraphFormat_Field_hParagraphAlignment = (*env)->GetFieldID(env, object_class, "hParagraphAlignment", "I");
    mmTextParagraphFormat_Field_hLineBreakMode = (*env)->GetFieldID(env, object_class, "hLineBreakMode", "I");
    mmTextParagraphFormat_Field_hWritingDirection = (*env)->GetFieldID(env, object_class, "hWritingDirection", "I");
    mmTextParagraphFormat_Field_hFirstLineHeadIndent = (*env)->GetFieldID(env, object_class, "hFirstLineHeadIndent", "F");
    mmTextParagraphFormat_Field_hHeadIndent = (*env)->GetFieldID(env, object_class, "hHeadIndent", "F");
    mmTextParagraphFormat_Field_hTailIndent = (*env)->GetFieldID(env, object_class, "hTailIndent", "F");
    mmTextParagraphFormat_Field_hLineSpacingAddition = (*env)->GetFieldID(env, object_class, "hLineSpacingAddition", "F");
    mmTextParagraphFormat_Field_hLineSpacingMultiple = (*env)->GetFieldID(env, object_class, "hLineSpacingMultiple", "F");
    mmTextParagraphFormat_Field_hShadowOffset = (*env)->GetFieldID(env, object_class, "hShadowOffset", "[F");
    mmTextParagraphFormat_Field_hShadowBlur = (*env)->GetFieldID(env, object_class, "hShadowBlur", "F");
    mmTextParagraphFormat_Field_hShadowColor = (*env)->GetFieldID(env, object_class, "hShadowColor", "[F");
    mmTextParagraphFormat_Field_hLineCap = (*env)->GetFieldID(env, object_class, "hLineCap", "I");
    mmTextParagraphFormat_Field_hLineJoin = (*env)->GetFieldID(env, object_class, "hLineJoin", "I");
    mmTextParagraphFormat_Field_hLineMiterLimit = (*env)->GetFieldID(env, object_class, "hLineMiterLimit", "F");
    mmTextParagraphFormat_Field_hAlignBaseline = (*env)->GetFieldID(env, object_class, "hAlignBaseline", "I");
    mmTextParagraphFormat_Field_hLineDescent = (*env)->GetFieldID(env, object_class, "hLineDescent", "F");
    mmTextParagraphFormat_Field_hSuitableSize = (*env)->GetFieldID(env, object_class, "hSuitableSize", "[F");
    mmTextParagraphFormat_Field_hWindowSize = (*env)->GetFieldID(env, object_class, "hWindowSize", "[F");
    mmTextParagraphFormat_Field_hLayoutRect = (*env)->GetFieldID(env, object_class, "hLayoutRect", "[F");
    mmTextParagraphFormat_Field_hDisplayDensity = (*env)->GetFieldID(env, object_class, "hDisplayDensity", "F");
    mmTextParagraphFormat_Field_hCaretIndex = (*env)->GetFieldID(env, object_class, "hCaretIndex", "J");
    mmTextParagraphFormat_Field_hSelection = (*env)->GetFieldID(env, object_class, "hSelection", "[J");
    mmTextParagraphFormat_Field_hCaretWrapping = (*env)->GetFieldID(env, object_class, "hCaretWrapping", "I");
    mmTextParagraphFormat_Field_hCaretStatus = (*env)->GetFieldID(env, object_class, "hCaretStatus", "I");
    mmTextParagraphFormat_Field_hSelectionStatus = (*env)->GetFieldID(env, object_class, "hSelectionStatus", "I");
    mmTextParagraphFormat_Field_hCaretWidth = (*env)->GetFieldID(env, object_class, "hCaretWidth", "F");
    mmTextParagraphFormat_Field_hCaretColor = (*env)->GetFieldID(env, object_class, "hCaretColor", "[F");
    mmTextParagraphFormat_Field_hSelectionCaptureColor = (*env)->GetFieldID(env, object_class, "hSelectionCaptureColor", "[F");
    mmTextParagraphFormat_Field_hSelectionReleaseColor = (*env)->GetFieldID(env, object_class, "hSelectionReleaseColor", "[F");
    mmTextParagraphFormat_Field_pObject = (*env)->GetFieldID(env, object_class, "pObject", "J");
    mmTextParagraphFormat_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmTextParagraphFormat_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmTextParagraphFormat_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmTextParagraphFormat_Method_SetFontFeature = (*env)->GetMethodID(env, object_class, "SetFontFeature", "(II)V");
    mmTextParagraphFormat_Method_RmvFontFeature = (*env)->GetMethodID(env, object_class, "RmvFontFeature", "(I)V");
    mmTextParagraphFormat_Method_GetFontFeature = (*env)->GetMethodID(env, object_class, "GetFontFeature", "(I)I");
    mmTextParagraphFormat_Method_ClearFontFeature = (*env)->GetMethodID(env, object_class, "ClearFontFeature", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextParagraphFormat_Field_hMasterFormat = NULL;
    mmTextParagraphFormat_Field_hFontFeatures = NULL;
    mmTextParagraphFormat_Field_hTextAlignment = NULL;
    mmTextParagraphFormat_Field_hParagraphAlignment = NULL;
    mmTextParagraphFormat_Field_hLineBreakMode = NULL;
    mmTextParagraphFormat_Field_hWritingDirection = NULL;
    mmTextParagraphFormat_Field_hFirstLineHeadIndent = NULL;
    mmTextParagraphFormat_Field_hHeadIndent = NULL;
    mmTextParagraphFormat_Field_hTailIndent = NULL;
    mmTextParagraphFormat_Field_hLineSpacingAddition = NULL;
    mmTextParagraphFormat_Field_hLineSpacingMultiple = NULL;
    mmTextParagraphFormat_Field_hShadowOffset = NULL;
    mmTextParagraphFormat_Field_hShadowBlur = NULL;
    mmTextParagraphFormat_Field_hShadowColor = NULL;
    mmTextParagraphFormat_Field_hLineCap = NULL;
    mmTextParagraphFormat_Field_hLineJoin = NULL;
    mmTextParagraphFormat_Field_hLineMiterLimit = NULL;
    mmTextParagraphFormat_Field_hAlignBaseline = NULL;
    mmTextParagraphFormat_Field_hLineDescent = NULL;
    mmTextParagraphFormat_Field_hSuitableSize = NULL;
    mmTextParagraphFormat_Field_hWindowSize = NULL;
    mmTextParagraphFormat_Field_hLayoutRect = NULL;
    mmTextParagraphFormat_Field_hDisplayDensity = NULL;
    mmTextParagraphFormat_Field_hCaretIndex = NULL;
    mmTextParagraphFormat_Field_hSelection = NULL;
    mmTextParagraphFormat_Field_hCaretWrapping = NULL;
    mmTextParagraphFormat_Field_hCaretStatus = NULL;
    mmTextParagraphFormat_Field_hSelectionStatus = NULL;
    mmTextParagraphFormat_Field_hCaretWidth = NULL;
    mmTextParagraphFormat_Field_hCaretColor = NULL;
    mmTextParagraphFormat_Field_hSelectionCaptureColor = NULL;
    mmTextParagraphFormat_Field_hSelectionReleaseColor = NULL;
    mmTextParagraphFormat_Field_pObject = NULL;
    mmTextParagraphFormat_Method_NewObject = NULL;
    mmTextParagraphFormat_Method_Init = NULL;
    mmTextParagraphFormat_Method_Destroy = NULL;
    mmTextParagraphFormat_Method_SetFontFeature = NULL;
    mmTextParagraphFormat_Method_RmvFontFeature = NULL;
    mmTextParagraphFormat_Method_GetFontFeature = NULL;
    mmTextParagraphFormat_Method_ClearFontFeature = NULL;

    (*env)->DeleteGlobalRef(env, mmTextParagraphFormat_ObjectClass);
    mmTextParagraphFormat_ObjectClass = NULL;
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_Decode(
    JNIEnv*                                        env,
    struct mmTextParagraphFormat*                  content,
    jobject                                        obj)
{
    jobject j_hMasterFormat = (*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hMasterFormat);
    jfloatArray j_hShadowOffset = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hShadowOffset);
    jfloatArray j_hShadowColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hShadowColor);
    jfloatArray j_hSuitableSize = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSuitableSize);
    jfloatArray j_hWindowSize = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hWindowSize);
    jfloatArray j_hLayoutRect = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hLayoutRect);
    jlongArray j_hSelection = (jlongArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSelection);
    jfloatArray j_hCaretColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hCaretColor);
    jfloatArray j_hSelectionCaptureColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSelectionCaptureColor);
    jfloatArray j_hSelectionReleaseColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSelectionReleaseColor);

    mmTextFormat_Decode(env, &content->hMasterFormat, j_hMasterFormat);
    mmTextParagraphFormat_DecodeFontFeatures(env, content, obj);
    content->hTextAlignment = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hTextAlignment);
    content->hParagraphAlignment = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hParagraphAlignment);
    content->hLineBreakMode = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hLineBreakMode);
    content->hWritingDirection = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hWritingDirection);
    content->hFirstLineHeadIndent = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hFirstLineHeadIndent);
    content->hHeadIndent = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hHeadIndent);
    content->hTailIndent = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hTailIndent);
    content->hLineSpacingAddition = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hLineSpacingAddition);
    content->hLineSpacingMultiple = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hLineSpacingMultiple);
    (*env)->GetFloatArrayRegion(env, j_hShadowOffset, 0, 2, content->hShadowOffset);
    content->hShadowBlur = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hShadowBlur);
    (*env)->GetFloatArrayRegion(env, j_hShadowColor, 0, 4, content->hShadowColor);
    content->hLineCap = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hLineCap);
    content->hLineJoin = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hLineJoin);
    content->hLineMiterLimit = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hLineMiterLimit);
    content->hAlignBaseline = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hAlignBaseline);
    content->hLineDescent = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hLineDescent);
    (*env)->GetFloatArrayRegion(env, j_hSuitableSize, 0, 2, content->hSuitableSize);
    (*env)->GetFloatArrayRegion(env, j_hWindowSize, 0, 2, content->hWindowSize);
    (*env)->GetFloatArrayRegion(env, j_hLayoutRect, 0, 4, content->hLayoutRect);
    content->hDisplayDensity = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hDisplayDensity);
    content->hCaretIndex = (*env)->GetLongField(env, obj, mmTextParagraphFormat_Field_hCaretIndex);
    (*env)->GetLongArrayRegion(env, j_hSelection, 0, 2, (jlong*)content->hSelection);
    content->hCaretWrapping = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hCaretWrapping);
    content->hCaretStatus = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hCaretStatus);
    content->hSelectionStatus = (*env)->GetIntField(env, obj, mmTextParagraphFormat_Field_hSelectionStatus);
    content->hCaretWidth = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hCaretWidth);
    (*env)->GetFloatArrayRegion(env, j_hCaretColor, 0, 4, content->hCaretColor);
    (*env)->GetFloatArrayRegion(env, j_hSelectionCaptureColor, 0, 4, content->hSelectionCaptureColor);
    (*env)->GetFloatArrayRegion(env, j_hSelectionReleaseColor, 0, 4, content->hSelectionReleaseColor);

    (*env)->DeleteLocalRef(env, j_hMasterFormat);
    (*env)->DeleteLocalRef(env, j_hShadowOffset);
    (*env)->DeleteLocalRef(env, j_hShadowColor);
    (*env)->DeleteLocalRef(env, j_hSuitableSize);
    (*env)->DeleteLocalRef(env, j_hWindowSize);
    (*env)->DeleteLocalRef(env, j_hLayoutRect);
    (*env)->DeleteLocalRef(env, j_hSelection);
    (*env)->DeleteLocalRef(env, j_hCaretColor);
    (*env)->DeleteLocalRef(env, j_hSelectionCaptureColor);
    (*env)->DeleteLocalRef(env, j_hSelectionReleaseColor);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_Encode(
    JNIEnv*                                        env,
    struct mmTextParagraphFormat*                  content,
    jobject                                        obj)
{
    jobject j_hMasterFormat = (*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hMasterFormat);
    jfloatArray j_hShadowOffset = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hShadowOffset);
    jfloatArray j_hShadowColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hShadowColor);
    jfloatArray j_hSuitableSize = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSuitableSize);
    jfloatArray j_hWindowSize = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hWindowSize);
    jfloatArray j_hLayoutRect = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hLayoutRect);
    jlongArray j_hSelection = (jlongArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSelection);
    jfloatArray j_hCaretColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hCaretColor);
    jfloatArray j_hSelectionCaptureColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSelectionCaptureColor);
    jfloatArray j_hSelectionReleaseColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSelectionReleaseColor);

    mmTextFormat_Encode(env, &content->hMasterFormat, j_hMasterFormat);
    mmTextParagraphFormat_EncodeFontFeatures(env, content, obj);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hTextAlignment, content->hTextAlignment);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hParagraphAlignment, content->hParagraphAlignment);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hLineBreakMode, content->hLineBreakMode);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hWritingDirection, content->hWritingDirection);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hFirstLineHeadIndent, content->hFirstLineHeadIndent);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hHeadIndent, content->hHeadIndent);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hTailIndent, content->hTailIndent);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hLineSpacingAddition, content->hLineSpacingAddition);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hLineSpacingMultiple, content->hLineSpacingMultiple);
    (*env)->SetFloatArrayRegion(env, j_hShadowOffset, 0, 2, content->hShadowOffset);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hShadowBlur, content->hShadowBlur);
    (*env)->SetFloatArrayRegion(env, j_hShadowColor, 0, 4, content->hShadowColor);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hLineCap, content->hLineCap);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hLineJoin, content->hLineJoin);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hLineMiterLimit, content->hLineMiterLimit);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hAlignBaseline, content->hAlignBaseline);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hLineDescent, content->hLineDescent);
    (*env)->SetFloatArrayRegion(env, j_hSuitableSize, 0, 2, content->hSuitableSize);
    (*env)->SetFloatArrayRegion(env, j_hWindowSize, 0, 2, content->hWindowSize);
    (*env)->SetFloatArrayRegion(env, j_hLayoutRect, 0, 4, content->hLayoutRect);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hDisplayDensity, content->hDisplayDensity);
    (*env)->SetLongField(env, obj, mmTextParagraphFormat_Field_hCaretIndex, content->hCaretIndex);
    (*env)->SetLongArrayRegion(env, j_hSelection, 0, 2, (jlong*)content->hSelection);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hCaretWrapping, content->hCaretWrapping);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hCaretStatus, content->hCaretStatus);
    (*env)->SetIntField(env, obj, mmTextParagraphFormat_Field_hSelectionStatus, content->hSelectionStatus);
    (*env)->SetFloatField(env, obj, mmTextParagraphFormat_Field_hCaretWidth, content->hCaretWidth);
    (*env)->SetFloatArrayRegion(env, j_hCaretColor, 0, 4, content->hCaretColor);
    (*env)->SetFloatArrayRegion(env, j_hSelectionCaptureColor, 0, 4, content->hSelectionCaptureColor);
    (*env)->SetFloatArrayRegion(env, j_hSelectionReleaseColor, 0, 4, content->hSelectionReleaseColor);

    (*env)->DeleteLocalRef(env, j_hMasterFormat);
    (*env)->DeleteLocalRef(env, j_hShadowOffset);
    (*env)->DeleteLocalRef(env, j_hShadowColor);
    (*env)->DeleteLocalRef(env, j_hSuitableSize);
    (*env)->DeleteLocalRef(env, j_hWindowSize);
    (*env)->DeleteLocalRef(env, j_hLayoutRect);
    (*env)->DeleteLocalRef(env, j_hSelection);
    (*env)->DeleteLocalRef(env, j_hCaretColor);
    (*env)->DeleteLocalRef(env, j_hSelectionCaptureColor);
    (*env)->DeleteLocalRef(env, j_hSelectionReleaseColor);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_DecodeFontFeatures(
    JNIEnv*                                        env,
    struct mmTextParagraphFormat*                  content,
    jobject                                        obj)
{
    jobject j_hFontFeatures = (*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hFontFeatures);

    mmTextParagraphFormat_ClearFontFeature(content);

    struct mmRbtreeU32U32* pFontFeatures = &content->hFontFeatures;

    jobject j_pSetObj = (*env)->CallObjectMethod(env, j_hFontFeatures, mmJavaUtilTreeMap_Method_entrySet);
    jobject j_pIteratorObj = (*env)->CallObjectMethod(env, j_pSetObj, mmJavaUtilSet_Method_iterator);

    while ((*env)->CallBooleanMethod(env, j_pIteratorObj, mmJavaUtilIterator_Method_hasNext)) 
    {
        jobject j_pEntryObj = (*env)->CallObjectMethod(env, j_pIteratorObj, mmJavaUtilIterator_Method_next);

        jobject j_pKeyObj = (*env)->CallObjectMethod(env, j_pEntryObj, mmJavaUtilMapEntry_Method_getKey);

        if (j_pKeyObj == NULL)
        {
            (*env)->DeleteLocalRef(env, j_pEntryObj);
            continue;
        }
        jobject j_pValueObj = (*env)->CallObjectMethod(env, j_pEntryObj, mmJavaUtilMapEntry_Method_getValue);
        if (j_pValueObj == NULL)
        {
            (*env)->DeleteLocalRef(env, j_pKeyObj);
            (*env)->DeleteLocalRef(env, j_pEntryObj);
            continue;
        }
        jint hKey = (*env)->CallIntMethod(env, j_pKeyObj, mmJavaLangInteger_Method_intValue);
        jint hValue = (*env)->CallIntMethod(env, j_pValueObj, mmJavaLangInteger_Method_intValue);

        mmRbtreeU32U32_Set(pFontFeatures, hKey, hValue);

        (*env)->DeleteLocalRef(env, j_pValueObj);
        (*env)->DeleteLocalRef(env, j_pKeyObj);
        (*env)->DeleteLocalRef(env, j_pEntryObj);
    }

    (*env)->DeleteLocalRef(env, j_pIteratorObj);
    (*env)->DeleteLocalRef(env, j_pSetObj);

    (*env)->DeleteLocalRef(env, j_hFontFeatures);
}

MM_EXPORT_NWSI
void
mmTextParagraphFormat_EncodeFontFeatures(
    JNIEnv*                                        env,
    struct mmTextParagraphFormat*                  content,
    jobject                                        obj)
{
    jobject j_hFontFeatures = (*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hFontFeatures);

    struct mmRbtreeU32U32* pFontFeatures = &content->hFontFeatures;

    struct mmRbNode* n = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;
    
    (*env)->CallVoidMethod(env, j_hFontFeatures, mmJavaUtilTreeMap_Method_clear);
    
    jobject j_pKeyObj = NULL;
    jobject j_pValueObj = NULL;

    n = mmRb_First(&pFontFeatures->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32U32Iterator, n);
        n = mmRb_Next(n);

        j_pKeyObj = (*env)->NewObject(env, mmJavaLangInteger_ObjectClass, mmJavaLangInteger_Method_NewObject, (jint)it->k);
        j_pValueObj = (*env)->NewObject(env, mmJavaLangInteger_ObjectClass, mmJavaLangInteger_Method_NewObject, (jint)it->v);

        (*env)->CallObjectMethod(env, j_hFontFeatures, mmJavaUtilTreeMap_Method_put, j_pKeyObj, j_pValueObj);

        (*env)->DeleteLocalRef(env, j_pKeyObj);
        (*env)->DeleteLocalRef(env, j_pValueObj);
    }

    (*env)->DeleteLocalRef(env, j_hFontFeatures);
}

MM_EXPORT_NWSI struct mmTextParagraphFormat* mmTextParagraphFormat_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmTextParagraphFormat_Field_pObject);
    return (struct mmTextParagraphFormat*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmTextParagraphFormat_SetNativePtr(JNIEnv* env, jobject obj, struct mmTextParagraphFormat* p)
{
    jlong j_hNativePtr = (jlong)p;
    (*env)->SetLongField(env, obj, mmTextParagraphFormat_Field_pObject, j_hNativePtr);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmTextParagraphFormat_NativeUpdateSuitableData(JNIEnv* env, jobject obj)
{
    struct mmTextParagraphFormat* p = mmTextParagraphFormat_GetNativePtr(env, obj);

    jfloatArray j_hSuitableSize = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hSuitableSize);
    jfloatArray j_hWindowSize = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hWindowSize);
    jfloatArray j_hLayoutRect = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextParagraphFormat_Field_hLayoutRect);

    p->hLineDescent = (*env)->GetFloatField(env, obj, mmTextParagraphFormat_Field_hLineDescent);
    (*env)->GetFloatArrayRegion(env, j_hSuitableSize, 0, 2, p->hSuitableSize);
    (*env)->GetFloatArrayRegion(env, j_hWindowSize, 0, 2, p->hWindowSize);
    (*env)->GetFloatArrayRegion(env, j_hLayoutRect, 0, 4, p->hLayoutRect);

    (*env)->DeleteLocalRef(env, j_hSuitableSize);
    (*env)->DeleteLocalRef(env, j_hWindowSize);
    (*env)->DeleteLocalRef(env, j_hLayoutRect);
}

MM_EXPORT_NWSI jclass mmTextHitMetrics_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextHitMetrics_Field_rect = NULL;
MM_EXPORT_NWSI jfieldID mmTextHitMetrics_Field_offset = NULL;
MM_EXPORT_NWSI jfieldID mmTextHitMetrics_Field_length = NULL;
MM_EXPORT_NWSI jfieldID mmTextHitMetrics_Field_bidiLevel = NULL;
MM_EXPORT_NWSI jfieldID mmTextHitMetrics_Field_isText = NULL;
MM_EXPORT_NWSI jfieldID mmTextHitMetrics_Field_isTrimmed = NULL;
MM_EXPORT_NWSI jmethodID mmTextHitMetrics_Method_NewObject = NULL;

MM_EXPORT_NWSI
void
mmTextHitMetrics_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextHitMetrics");
    mmTextHitMetrics_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextHitMetrics_Field_rect = (*env)->GetFieldID(env, object_class, "rect", "[F");
    mmTextHitMetrics_Field_offset = (*env)->GetFieldID(env, object_class, "offset", "I");
    mmTextHitMetrics_Field_length = (*env)->GetFieldID(env, object_class, "length", "I");
    mmTextHitMetrics_Field_bidiLevel = (*env)->GetFieldID(env, object_class, "bidiLevel", "I");
    mmTextHitMetrics_Field_isText = (*env)->GetFieldID(env, object_class, "isText", "I");
    mmTextHitMetrics_Field_isTrimmed = (*env)->GetFieldID(env, object_class, "isTrimmed", "I");
    mmTextHitMetrics_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextHitMetrics_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextHitMetrics_Field_rect = NULL;
    mmTextHitMetrics_Field_offset = NULL;
    mmTextHitMetrics_Field_length = NULL;
    mmTextHitMetrics_Field_bidiLevel = NULL;
    mmTextHitMetrics_Field_isText = NULL;
    mmTextHitMetrics_Field_isTrimmed = NULL;
    mmTextHitMetrics_Method_NewObject = NULL;

    (*env)->DeleteGlobalRef(env, mmTextHitMetrics_ObjectClass);
    mmTextHitMetrics_ObjectClass = NULL;
}

MM_EXPORT_NWSI
void
mmTextHitMetrics_Decode(
    JNIEnv*                                        env,
    struct mmTextHitMetrics*                       content,
    jobject                                        obj)
{
    jfloatArray j_rect = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextHitMetrics_Field_rect);

    (*env)->GetFloatArrayRegion(env, j_rect, 0, 4, content->rect);
    content->offset = (*env)->GetIntField(env, obj, mmTextHitMetrics_Field_offset);
    content->length = (*env)->GetIntField(env, obj, mmTextHitMetrics_Field_length);
    content->bidiLevel = (*env)->GetIntField(env, obj, mmTextHitMetrics_Field_bidiLevel);
    content->isText = (*env)->GetIntField(env, obj, mmTextHitMetrics_Field_isText);
    content->isTrimmed = (*env)->GetIntField(env, obj, mmTextHitMetrics_Field_isTrimmed);

    (*env)->DeleteLocalRef(env, j_rect);
}

MM_EXPORT_NWSI
void
mmTextHitMetrics_Encode(
    JNIEnv*                                        env,
    struct mmTextHitMetrics*                       content,
    jobject                                        obj)
{
    jfloatArray j_rect = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextHitMetrics_Field_rect);

    (*env)->SetFloatArrayRegion(env, j_rect, 0, 4, content->rect);
    (*env)->SetIntField(env, obj, mmTextHitMetrics_Field_offset, content->offset);
    (*env)->SetIntField(env, obj, mmTextHitMetrics_Field_length, content->length);
    (*env)->SetIntField(env, obj, mmTextHitMetrics_Field_bidiLevel, content->bidiLevel);
    (*env)->SetIntField(env, obj, mmTextHitMetrics_Field_isText, content->isText);
    (*env)->SetIntField(env, obj, mmTextHitMetrics_Field_isTrimmed, content->isTrimmed);

    (*env)->DeleteLocalRef(env, j_rect);
}
