/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextFormatNative.h"

#include "core/mmAlloc.h"

MM_EXPORT_NWSI jclass mmTextFormat_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hFontFamilyName = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hFontSize = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hFontStyle = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hFontWeight = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hForegroundColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hBackgroundColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hStrokeWidth = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hStrokeColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hUnderlineWidth = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hUnderlineColor = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hStrikeThruWidth = NULL;
MM_EXPORT_NWSI jfieldID mmTextFormat_Field_hStrikeThruColor = NULL;

MM_EXPORT_NWSI
void
mmTextFormat_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextFormat");
    mmTextFormat_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextFormat_Field_hFontFamilyName = (*env)->GetFieldID(env, object_class, "hFontFamilyName", "Ljava/lang/String;");
    mmTextFormat_Field_hFontSize = (*env)->GetFieldID(env, object_class, "hFontSize", "F");
    mmTextFormat_Field_hFontStyle = (*env)->GetFieldID(env, object_class, "hFontStyle", "I");
    mmTextFormat_Field_hFontWeight = (*env)->GetFieldID(env, object_class, "hFontWeight", "I");
    mmTextFormat_Field_hForegroundColor = (*env)->GetFieldID(env, object_class, "hForegroundColor", "[F");
    mmTextFormat_Field_hBackgroundColor = (*env)->GetFieldID(env, object_class, "hBackgroundColor", "[F");
    mmTextFormat_Field_hStrokeWidth = (*env)->GetFieldID(env, object_class, "hStrokeWidth", "F");
    mmTextFormat_Field_hStrokeColor = (*env)->GetFieldID(env, object_class, "hStrokeColor", "[F");
    mmTextFormat_Field_hUnderlineWidth = (*env)->GetFieldID(env, object_class, "hUnderlineWidth", "F");
    mmTextFormat_Field_hUnderlineColor = (*env)->GetFieldID(env, object_class, "hUnderlineColor", "[F");
    mmTextFormat_Field_hStrikeThruWidth = (*env)->GetFieldID(env, object_class, "hStrikeThruWidth", "F");
    mmTextFormat_Field_hStrikeThruColor = (*env)->GetFieldID(env, object_class, "hStrikeThruColor", "[F");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextFormat_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextFormat_Field_hFontFamilyName = NULL;
    mmTextFormat_Field_hFontSize = NULL;
    mmTextFormat_Field_hFontStyle = NULL;
    mmTextFormat_Field_hFontWeight = NULL;
    mmTextFormat_Field_hForegroundColor = NULL;
    mmTextFormat_Field_hBackgroundColor = NULL;
    mmTextFormat_Field_hStrokeWidth = NULL;
    mmTextFormat_Field_hStrokeColor = NULL;
    mmTextFormat_Field_hUnderlineWidth = NULL;
    mmTextFormat_Field_hUnderlineColor = NULL;
    mmTextFormat_Field_hStrikeThruWidth = NULL;
    mmTextFormat_Field_hStrikeThruColor = NULL;

    (*env)->DeleteGlobalRef(env, mmTextFormat_ObjectClass);
    mmTextFormat_ObjectClass = NULL;
}

MM_EXPORT_NWSI
void
mmTextFormat_Decode(
    JNIEnv*                                        env,
    struct mmTextFormat*                           content,
    jobject                                        obj)
{
    jobject j_pFontFamilyName = (jobject)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hFontFamilyName);
    jfloatArray j_hForegroundColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hForegroundColor);
    jfloatArray j_hBackgroundColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hBackgroundColor);
    jfloatArray j_hStrokeColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hStrokeColor);
    jfloatArray j_hUnderlineColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hUnderlineColor);
    jfloatArray j_hStrikeThruColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hStrikeThruColor);

    const jchar* j_pFontFamilyName_Chars = (*env)->GetStringChars(env, j_pFontFamilyName, NULL);
    const mmUInt16_t* r_pFontFamilyName_Chars = (NULL == j_pFontFamilyName_Chars) ? mmUtf16StringEmpty : j_pFontFamilyName_Chars;

    mmUtf16String_Assigns(&content->hFontFamilyName, r_pFontFamilyName_Chars);
    content->hFontSize = (*env)->GetFloatField(env, obj, mmTextFormat_Field_hFontSize);
    content->hFontStyle = (*env)->GetIntField(env, obj, mmTextFormat_Field_hFontStyle);
    content->hFontWeight = (*env)->GetIntField(env, obj, mmTextFormat_Field_hFontWeight);
    (*env)->GetFloatArrayRegion(env, j_hForegroundColor, 0, 4, content->hForegroundColor);
    (*env)->GetFloatArrayRegion(env, j_hBackgroundColor, 0, 4, content->hBackgroundColor);
    content->hStrokeWidth = (*env)->GetFloatField(env, obj, mmTextFormat_Field_hStrokeWidth);
    (*env)->GetFloatArrayRegion(env, j_hStrokeColor, 0, 4, content->hStrokeColor);
    content->hUnderlineWidth = (*env)->GetFloatField(env, obj, mmTextFormat_Field_hUnderlineWidth);
    (*env)->GetFloatArrayRegion(env, j_hUnderlineColor, 0, 4, content->hUnderlineColor);
    content->hStrikeThruWidth = (*env)->GetFloatField(env, obj, mmTextFormat_Field_hStrikeThruWidth);
    (*env)->GetFloatArrayRegion(env, j_hStrikeThruColor, 0, 4, content->hStrikeThruColor);

    (*env)->ReleaseStringChars(env, j_pFontFamilyName, j_pFontFamilyName_Chars);

    (*env)->DeleteLocalRef(env, j_pFontFamilyName);
    (*env)->DeleteLocalRef(env, j_hForegroundColor);
    (*env)->DeleteLocalRef(env, j_hBackgroundColor);
    (*env)->DeleteLocalRef(env, j_hStrokeColor);
    (*env)->DeleteLocalRef(env, j_hUnderlineColor);
    (*env)->DeleteLocalRef(env, j_hStrikeThruColor);
}

MM_EXPORT_NWSI
void
mmTextFormat_Encode(
    JNIEnv*                                        env,
    struct mmTextFormat*                           content,
    jobject                                        obj)
{
    const jchar* r_pFontFamilyName_Chars = (const jchar*)mmUtf16String_CStr(&content->hFontFamilyName);
    jsize r_pFontFamilyName_Size = (jsize)mmUtf16String_Size(&content->hFontFamilyName);

    jfloatArray j_hForegroundColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hForegroundColor);
    jfloatArray j_hBackgroundColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hBackgroundColor);
    jfloatArray j_hStrokeColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hStrokeColor);
    jfloatArray j_hUnderlineColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hUnderlineColor);
    jfloatArray j_hStrikeThruColor = (jfloatArray)(*env)->GetObjectField(env, obj, mmTextFormat_Field_hStrikeThruColor);

    jstring j_pFontFamilyName = (*env)->NewString(env, r_pFontFamilyName_Chars, r_pFontFamilyName_Size);

    (*env)->SetObjectField(env, obj, mmTextFormat_Field_hFontFamilyName, j_pFontFamilyName);
    (*env)->SetFloatField(env, obj, mmTextFormat_Field_hFontSize, content->hFontSize);
    (*env)->SetIntField(env, obj, mmTextFormat_Field_hFontStyle, content->hFontStyle);
    (*env)->SetIntField(env, obj, mmTextFormat_Field_hFontWeight, content->hFontWeight);
    (*env)->SetFloatArrayRegion(env, j_hForegroundColor, 0, 4, content->hForegroundColor);
    (*env)->SetFloatArrayRegion(env, j_hBackgroundColor, 0, 4, content->hBackgroundColor);
    (*env)->SetFloatField(env, obj, mmTextFormat_Field_hStrokeWidth, content->hStrokeWidth);
    (*env)->SetFloatArrayRegion(env, j_hStrokeColor, 0, 4, content->hStrokeColor);
    (*env)->SetFloatField(env, obj, mmTextFormat_Field_hUnderlineWidth, content->hUnderlineWidth);
    (*env)->SetFloatArrayRegion(env, j_hUnderlineColor, 0, 4, content->hUnderlineColor);
    (*env)->SetFloatField(env, obj, mmTextFormat_Field_hStrikeThruWidth, content->hStrikeThruWidth);
    (*env)->SetFloatArrayRegion(env, j_hStrikeThruColor, 0, 4, content->hStrikeThruColor);

    (*env)->DeleteLocalRef(env, j_pFontFamilyName);
    (*env)->DeleteLocalRef(env, j_hForegroundColor);
    (*env)->DeleteLocalRef(env, j_hBackgroundColor);
    (*env)->DeleteLocalRef(env, j_hStrokeColor);
    (*env)->DeleteLocalRef(env, j_hUnderlineColor);
    (*env)->DeleteLocalRef(env, j_hStrikeThruColor);
}
