/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDevice.h"

#include "core/mmLogger.h"

#include <sys/utsname.h>

MM_EXPORT_NWSI jclass mmDevice_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmDevice_Field_hNativePtr = NULL;

MM_EXPORT_NWSI void mmDevice_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmDevice");
    mmDevice_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmDevice_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmDevice_DetachVirtualMachine(JNIEnv* env)
{
    (*env)->DeleteGlobalRef(env, mmDevice_ObjectClass);
    mmDevice_ObjectClass = NULL;

    mmDevice_Field_hNativePtr = NULL;
}

MM_EXPORT_NWSI void mmDevice_Init(struct mmDevice* p)
{
    mmString_Init(&p->hName);
    mmString_Init(&p->hModel);
    mmString_Init(&p->hLocalizedModel);
    mmString_Init(&p->hSystemName);
    mmString_Init(&p->hSystemVersion);
    
    mmString_Init(&p->hSysName);
    mmString_Init(&p->hNodeName);
    mmString_Init(&p->hRelease);
    mmString_Init(&p->hVersion);
    mmString_Init(&p->hMachine);
    
    mmString_Assigns(&p->hName, "");
    mmString_Assigns(&p->hModel, "");
    mmString_Assigns(&p->hLocalizedModel, "");
    mmString_Assigns(&p->hSystemName, "");
    mmString_Assigns(&p->hSystemVersion, "");
    
    mmString_Assigns(&p->hSysName, "");
    mmString_Assigns(&p->hNodeName, "");
    mmString_Assigns(&p->hRelease, "");
    mmString_Assigns(&p->hVersion, "");
    mmString_Assigns(&p->hMachine, "");
}
MM_EXPORT_NWSI void mmDevice_Destroy(struct mmDevice* p)
{
    mmString_Destroy(&p->hName);
    mmString_Destroy(&p->hModel);
    mmString_Destroy(&p->hLocalizedModel);
    mmString_Destroy(&p->hSystemName);
    mmString_Destroy(&p->hSystemVersion);
    
    mmString_Destroy(&p->hSysName);
    mmString_Destroy(&p->hNodeName);
    mmString_Destroy(&p->hRelease);
    mmString_Destroy(&p->hVersion);
    mmString_Destroy(&p->hMachine);
}

MM_EXPORT_NWSI void mmDevice_OnFinishLaunching(struct mmDevice* p)
{
    struct utsname systemInfo;
    
    // uname.
    uname(&systemInfo);

    mmString_Assigns(&p->hLocalizedModel, mmString_CStr(&p->hModel));
    
    mmString_Assigns(&p->hSysName, systemInfo.sysname);
    mmString_Assigns(&p->hNodeName, systemInfo.nodename);
    mmString_Assigns(&p->hRelease, systemInfo.release);
    mmString_Assigns(&p->hVersion, systemInfo.version);
    mmString_Assigns(&p->hMachine, systemInfo.machine);
}
MM_EXPORT_NWSI void mmDevice_OnBeforeTerminate(struct mmDevice* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI struct mmDevice* mmDevice_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmDevice_Field_hNativePtr);
    return (struct mmDevice*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmDevice_SetNativePtr(JNIEnv* env, jobject obj, struct mmDevice* p)
{
    jlong j_hNativePtr = (jlong)p;
    (*env)->SetLongField(env, obj, mmDevice_Field_hNativePtr, j_hNativePtr);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmDevice_NativeSetName(JNIEnv* env, jobject obj, jstring pName)
{
    struct mmDevice* p = mmDevice_GetNativePtr(env, obj);

    const char* j_Name_c_str = (*env)->GetStringUTFChars(env, pName, NULL);
    const char* r_Name_c_str = NULL == j_Name_c_str ? "" : j_Name_c_str;
    mmString_Assigns(&p->hName, r_Name_c_str);
    (*env)->ReleaseStringUTFChars(env, pName, j_Name_c_str);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmDevice_NativeSetModel(JNIEnv* env, jobject obj, jstring pModel)
{
    struct mmDevice* p = mmDevice_GetNativePtr(env, obj);

    const char* j_Model_c_str = (*env)->GetStringUTFChars(env, pModel, NULL);
    const char* r_Model_c_str = NULL == j_Model_c_str ? "" : j_Model_c_str;
    mmString_Assigns(&p->hModel, r_Model_c_str);
    (*env)->ReleaseStringUTFChars(env, pModel, j_Model_c_str);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmDevice_NativeSetSystemName(JNIEnv* env, jobject obj, jstring pSystemName)
{
    struct mmDevice* p = mmDevice_GetNativePtr(env, obj);

    const char* j_SystemName_c_str = (*env)->GetStringUTFChars(env, pSystemName, NULL);
    const char* r_SystemName_c_str = NULL == j_SystemName_c_str ? "" : j_SystemName_c_str;
    mmString_Assigns(&p->hSystemName, r_SystemName_c_str);
    (*env)->ReleaseStringUTFChars(env, pSystemName, j_SystemName_c_str);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmDevice_NativeSetSystemVersion(JNIEnv* env, jobject obj, jstring pSystemVersion)
{
    struct mmDevice* p = mmDevice_GetNativePtr(env, obj);

    const char* j_SystemVersion_c_str = (*env)->GetStringUTFChars(env, pSystemVersion, NULL);
    const char* r_SystemVersion_c_str = NULL == j_SystemVersion_c_str ? "" : j_SystemVersion_c_str;
    mmString_Assigns(&p->hSystemVersion, r_SystemVersion_c_str);
    (*env)->ReleaseStringUTFChars(env, pSystemVersion, j_SystemVersion_c_str);
}
