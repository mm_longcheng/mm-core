/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


#include "mmAppIdentifier.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "dish/mmJavaVMEnv.h"

#include "nwsi/mmJavaNativeAndroid.h"

MM_EXPORT_NWSI jclass mmAppIdentifier_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmAppIdentifier_Field_hNativePtr = NULL;
MM_EXPORT_NWSI jmethodID mmAppIdentifier_Method_GenerateRandomUUID = NULL;
MM_EXPORT_NWSI jmethodID mmAppIdentifier_Method_GetUUIDDeviceBytes = NULL;
MM_EXPORT_NWSI jmethodID mmAppIdentifier_Method_GetUUIDAppBytes = NULL;

MM_EXPORT_NWSI void mmAppIdentifier_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmAppIdentifier");
    mmAppIdentifier_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmAppIdentifier_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");
    mmAppIdentifier_Method_GenerateRandomUUID = (*env)->GetStaticMethodID(env, object_class, "GenerateRandomUUID", "([B)V");
    mmAppIdentifier_Method_GetUUIDDeviceBytes = (*env)->GetMethodID(env, object_class, "GetUUIDDeviceBytes", "(Ljava/lang/String;[B)V");
    mmAppIdentifier_Method_GetUUIDAppBytes = (*env)->GetMethodID(env, object_class, "GetUUIDAppBytes", "(Ljava/lang/String;[B)V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmAppIdentifier_DetachVirtualMachine(JNIEnv* env)
{
    (*env)->DeleteGlobalRef(env, mmAppIdentifier_ObjectClass);
    mmAppIdentifier_ObjectClass = NULL;

    mmAppIdentifier_Field_hNativePtr = NULL;
    mmAppIdentifier_Method_GenerateRandomUUID = NULL;
    mmAppIdentifier_Method_GetUUIDDeviceBytes = NULL;
    mmAppIdentifier_Method_GetUUIDAppBytes = NULL;
}

MM_EXPORT_NWSI void mmAppIdentifier_GenerateRandomUUID(char hUUIDBytes[64])
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jbyteArray j_UUIDBytes = (*env)->NewByteArray(env, 64);

    (*env)->CallStaticVoidMethod(env, mmAppIdentifier_ObjectClass, mmAppIdentifier_Method_GenerateRandomUUID, j_UUIDBytes);

    mmMemset(hUUIDBytes, 0, 64);
    (*env)->GetByteArrayRegion(env, j_UUIDBytes, 0, 64, (jbyte*)hUUIDBytes);

    (*env)->DeleteLocalRef(env, j_UUIDBytes);
}

MM_EXPORT_NWSI void mmAppIdentifier_Init(struct mmAppIdentifier* p)
{
    mmString_Init(&p->hAppId);

    mmString_Init(&p->hUUIDDevice);
    mmString_Init(&p->hUUIDApp);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    mmString_Assigns(&p->hAppId, "");

    mmString_Assigns(&p->hUUIDDevice, "");
    mmString_Assigns(&p->hUUIDApp, "");

    mmJavaNative_JObjectInit(&p->jNativePtr);
}
MM_EXPORT_NWSI void mmAppIdentifier_Destroy(struct mmAppIdentifier* p)
{
    mmJavaNative_JObjectDestroy(&p->jNativePtr);

    mmString_Destroy(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    mmString_Destroy(&p->hUUIDDevice);
    mmString_Destroy(&p->hUUIDApp);
}

MM_EXPORT_NWSI void mmAppIdentifier_SetAppId(struct mmAppIdentifier* p, const char* pAppId)
{
    mmString_Assigns(&p->hAppId, pAppId);
}

MM_EXPORT_NWSI void mmAppIdentifier_OnFinishLaunching(struct mmAppIdentifier* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    mmAppIdentifier_GetUUIDDeviceString(p, &p->hUUIDDevice);
    mmAppIdentifier_GetUUIDAppString(p, &p->hUUIDApp);
    
    mmLogger_LogI(gLogger, "AppIdentifier AppIdentifier.OnFinishLaunching");
    mmLogger_LogI(gLogger, "AppIdentifier ---------------------+----------------------------------");
    mmLogger_LogI(gLogger, "AppIdentifier           hUUIDDevice: %s", mmString_CStr(&p->hUUIDDevice));
    mmLogger_LogI(gLogger, "AppIdentifier              hUUIDApp: %s", mmString_CStr(&p->hUUIDApp));
    mmLogger_LogI(gLogger, "AppIdentifier ---------------------+----------------------------------");
}
MM_EXPORT_NWSI void mmAppIdentifier_OnBeforeTerminate(struct mmAppIdentifier* p)
{
    // nothing here.
}

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceBytes(struct mmAppIdentifier* p, char hUUIDBytes[64])
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pAppId = (*env)->NewStringUTF(env, mmString_CStr(&p->hAppId));

    jbyteArray j_UUIDBytes = (*env)->NewByteArray(env, 64);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmAppIdentifier_Method_GetUUIDDeviceBytes, j_pAppId, j_UUIDBytes);

    mmMemset(hUUIDBytes, 0, 64);
    (*env)->GetByteArrayRegion(env, j_UUIDBytes, 0, 64, (jbyte*)hUUIDBytes);

    (*env)->DeleteLocalRef(env, j_UUIDBytes);

    (*env)->DeleteLocalRef(env, j_pAppId);
}
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppBytes(struct mmAppIdentifier* p, char hUUIDBytes[64])
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pAppId = (*env)->NewStringUTF(env, mmString_CStr(&p->hAppId));

    jbyteArray j_UUIDBytes = (*env)->NewByteArray(env, 64);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmAppIdentifier_Method_GetUUIDAppBytes, j_pAppId, j_UUIDBytes);

    mmMemset(hUUIDBytes, 0, 64);
    (*env)->GetByteArrayRegion(env, j_UUIDBytes, 0, 64, (jbyte*)hUUIDBytes);

    (*env)->DeleteLocalRef(env, j_UUIDBytes);

    (*env)->DeleteLocalRef(env, j_pAppId);
}

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceString(struct mmAppIdentifier* p, struct mmString* pUUIDString)
{
    char hUUIDBytes[64] = { 0 };
    mmAppIdentifier_GetUUIDDeviceBytes(p, hUUIDBytes);
    mmString_Assigns(pUUIDString, hUUIDBytes);
}
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppString(struct mmAppIdentifier* p, struct mmString* pUUIDString)
{
    char hUUIDBytes[64] = { 0 };
    mmAppIdentifier_GetUUIDAppBytes(p, hUUIDBytes);
    mmString_Assigns(pUUIDString, hUUIDBytes);
}


MM_EXPORT_NWSI struct mmAppIdentifier* mmAppIdentifier_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmAppIdentifier_Field_hNativePtr);
    return (struct mmAppIdentifier*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmAppIdentifier_SetNativePtr(JNIEnv* env, jobject obj, struct mmAppIdentifier* p)
{
    mmJavaNative_SetLongField(env, p->jNativePtr, mmAppIdentifier_Field_hNativePtr, 0);
    mmJavaNative_DeleteGlobalRef(&p->jNativePtr, env);
    mmJavaNative_CreateGlobalRef(&p->jNativePtr, env, obj);
    mmJavaNative_SetLongField(env, obj, mmAppIdentifier_Field_hNativePtr,  (jlong)p);
}
