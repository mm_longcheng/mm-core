/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPackageAssets.h"

#include "core/mmLogger.h"
#include "core/mmFilePath.h"

#include "dish/mmJavaVMEnv.h"

#include "nwsi/mmJavaNativeAndroid.h"

MM_EXPORT_NWSI jclass mmPackageAssets_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmPackageAssets_Field_hNativePtr = NULL;

MM_EXPORT_NWSI void mmPackageAssets_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmPackageAssets");
    mmPackageAssets_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmPackageAssets_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmPackageAssets_DetachVirtualMachine(JNIEnv* env)
{
    (*env)->DeleteGlobalRef(env, mmPackageAssets_ObjectClass);
    mmPackageAssets_ObjectClass = NULL;

    mmPackageAssets_Field_hNativePtr = NULL;
}

// JNI interface.
MM_EXPORT_NWSI void mmPackageAssets_SetJavaObject(struct mmPackageAssets* p, JNIEnv* env, jobject pPackageAssets)
{
    jlong j_hNativePtr = (jlong)p;
    jobject obj = pPackageAssets;
    (*env)->SetLongField(env, obj, mmPackageAssets_Field_hNativePtr, j_hNativePtr);
}

MM_EXPORT_NWSI void mmPackageAssets_Init(struct mmPackageAssets* p)
{
    mmString_Init(&p->hModuleName);
    
    mmString_Init(&p->hPackageName);
    mmString_Init(&p->hVersionName);
    mmString_Init(&p->hVersionCode);
    mmString_Init(&p->hDisplayName);
    
    mmString_Init(&p->hHomeDirectory);

    mmString_Init(&p->hInternalFilesDirectory);
    mmString_Init(&p->hInternalCacheDirectory);
    mmString_Init(&p->hExternalFilesDirectory);
    mmString_Init(&p->hExternalCacheDirectory);

    mmString_Init(&p->hPluginFolder);
    mmString_Init(&p->hDynlibFolder);

    mmString_Init(&p->hShaderCacheFolder);
    mmString_Init(&p->hTextureCacheFolder);

    mmString_Init(&p->hLoggerPathName);
    mmString_Init(&p->hLoggerPath);

    mmString_Init(&p->hAssetsRootFolderPath);
    mmString_Init(&p->hAssetsRootFolderBase);
    
    mmString_Init(&p->hAssetsRootSourcePath);
    mmString_Init(&p->hAssetsRootSourceBase);
    
    p->pAppHandler = NULL;
    p->pAssetManager = NULL;

    p->jAssetManagerPtr = NULL;

    p->jNativePtr = NULL;

    // Initial value.
    mmString_Assigns(&p->hModuleName, "mm.nwsi");
    
    mmString_Assigns(&p->hPackageName, "");
    mmString_Assigns(&p->hVersionName, "");
    mmString_Assigns(&p->hVersionCode, "");
    mmString_Assigns(&p->hDisplayName, "");
    
    mmString_Assigns(&p->hHomeDirectory, "");

    mmString_Assigns(&p->hInternalFilesDirectory, "");
    mmString_Assigns(&p->hInternalCacheDirectory, "");
    mmString_Assigns(&p->hExternalFilesDirectory, "");
    mmString_Assigns(&p->hExternalCacheDirectory, "");

    mmString_Assigns(&p->hPluginFolder, "");
    mmString_Assigns(&p->hDynlibFolder, "");

    mmString_Assigns(&p->hShaderCacheFolder, "shader");
    mmString_Assigns(&p->hTextureCacheFolder, "texture");

    mmString_Assigns(&p->hLoggerPathName, "log");
    mmString_Assigns(&p->hLoggerPath, "");
    
    mmString_Assigns(&p->hAssetsRootFolderPath, "");
    mmString_Assigns(&p->hAssetsRootFolderBase, "");
    
    mmString_Assigns(&p->hAssetsRootSourcePath, "");
    mmString_Assigns(&p->hAssetsRootSourceBase, "");

    mmJavaNative_JObjectInit(&p->jAssetManagerPtr);

    mmJavaNative_JObjectInit(&p->jNativePtr);
}
MM_EXPORT_NWSI void mmPackageAssets_Destroy(struct mmPackageAssets* p)
{
    mmJavaNative_JObjectDestroy(&p->jNativePtr);

    mmJavaNative_JObjectDestroy(&p->jAssetManagerPtr);

    mmString_Destroy(&p->hModuleName);
    
    mmString_Destroy(&p->hPackageName);
    mmString_Destroy(&p->hVersionName);
    mmString_Destroy(&p->hVersionCode);
    mmString_Destroy(&p->hDisplayName);
    
    mmString_Destroy(&p->hHomeDirectory);

    mmString_Destroy(&p->hInternalFilesDirectory);
    mmString_Destroy(&p->hInternalCacheDirectory);
    mmString_Destroy(&p->hExternalFilesDirectory);
    mmString_Destroy(&p->hExternalCacheDirectory);
    
    mmString_Destroy(&p->hPluginFolder);
    mmString_Destroy(&p->hDynlibFolder);

    mmString_Destroy(&p->hShaderCacheFolder);
    mmString_Destroy(&p->hTextureCacheFolder);

    mmString_Destroy(&p->hLoggerPathName);
    mmString_Destroy(&p->hLoggerPath);

    mmString_Destroy(&p->hAssetsRootFolderPath);
    mmString_Destroy(&p->hAssetsRootFolderBase);
    
    mmString_Destroy(&p->hAssetsRootSourcePath);
    mmString_Destroy(&p->hAssetsRootSourceBase);
    
    p->pAppHandler = NULL;
    p->pAssetManager = NULL;

    p->jAssetManagerPtr = NULL;

    p->jNativePtr = NULL;
}

MM_EXPORT_NWSI void mmPackageAssets_SetAppHandler(struct mmPackageAssets* p, void* pAppHandler)
{
    p->pAppHandler = pAppHandler;
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetManager(struct mmPackageAssets* p, void* pAssetManager)
{
    p->pAssetManager = pAssetManager;
}
MM_EXPORT_NWSI void mmPackageAssets_SetModuleName(struct mmPackageAssets* p, const char* pModuleName)
{
    mmString_Assigns(&p->hModuleName, pModuleName);
}
MM_EXPORT_NWSI void mmPackageAssets_SetPluginFolder(struct mmPackageAssets* p, const char* pPluginFolder)
{
    mmString_Assigns(&p->hPluginFolder, pPluginFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetDynlibFolder(struct mmPackageAssets* p, const char* pDynlibFolder)
{
    mmString_Assigns(&p->hDynlibFolder, pDynlibFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetShaderCacheFolder(struct mmPackageAssets* p, const char* pShaderCacheFolder)
{
    mmString_Assigns(&p->hShaderCacheFolder, pShaderCacheFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetTextureCacheFolder(struct mmPackageAssets* p, const char* pTextureCacheFolder)
{
    mmString_Assigns(&p->hTextureCacheFolder, pTextureCacheFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetLoggerPathName(struct mmPackageAssets* p, const char* pLoggerPathName)
{
    mmString_Assigns(&p->hLoggerPathName, pLoggerPathName);
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootFolder(struct mmPackageAssets* p, const char* pPath, const char* pBase)
{
    mmString_Assigns(&p->hAssetsRootFolderPath, pPath);
    mmString_Assigns(&p->hAssetsRootFolderBase, pBase);
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootSource(struct mmPackageAssets* p, const char* pPath, const char* pBase)
{
    mmString_Assigns(&p->hAssetsRootSourcePath, pPath);
    mmString_Assigns(&p->hAssetsRootSourceBase, pBase);
}

MM_EXPORT_NWSI void mmPackageAssets_AttachNativeResource(struct mmPackageAssets* p)
{
    // android default assets directory.
    //
    // InternalFilesDirectory /data/data/<PackageName>/files
    // InternalCacheDirectory /data/data/<PackageName>/cache
    // ExternalFilesDirectory /mnt/sdcard/Android/data/<PackageName>/files
    // ExternalCacheDirectory /mnt/sdcard/Android/data/<PackageName>/cache
    // LoggerPath             /mnt/sdcard/Android/data/<PackageName>/cache/log
    // AssetsPath             <BundlePath>/assets
    //
    // Note: /mnt/sdcard is device different.

    mmString_Assign(&p->hLoggerPath, &p->hExternalCacheDirectory);
    mmString_Appends(&p->hLoggerPath, "/");
    mmString_Append(&p->hLoggerPath, &p->hLoggerPathName);

    // mkdir
    mmMkdirIfNotExist(mmString_CStr(&p->hLoggerPath));
}
MM_EXPORT_NWSI void mmPackageAssets_DetachNativeResource(struct mmPackageAssets* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI void mmPackageAssets_OnFinishLaunching(struct mmPackageAssets* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // logger information.
    mmLogger_LogI(gLogger, "Assets mmPackageAssets.OnFinishLaunching");
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
    mmLogger_LogI(gLogger, "Assets                pPackageName : %s", mmString_CStr(&p->hPackageName));
    mmLogger_LogI(gLogger, "Assets                pVersionName : %s", mmString_CStr(&p->hVersionName));
    mmLogger_LogI(gLogger, "Assets                pVersionCode : %s", mmString_CStr(&p->hVersionCode));
    mmLogger_LogI(gLogger, "Assets                pDisplayName : %s", mmString_CStr(&p->hDisplayName));
    mmLogger_LogI(gLogger, "Assets               pPluginFolder : %s", mmString_CStr(&p->hPluginFolder));
    mmLogger_LogI(gLogger, "Assets               pDynlibFolder : %s", mmString_CStr(&p->hDynlibFolder));
    mmLogger_LogI(gLogger, "Assets              hHomeDirectory : %s", mmString_CStr(&p->hHomeDirectory));
    mmLogger_LogI(gLogger, "Assets          pShaderCacheFolder : %s", mmString_CStr(&p->hShaderCacheFolder));
    mmLogger_LogI(gLogger, "Assets         pTextureCacheFolder : %s", mmString_CStr(&p->hTextureCacheFolder));
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
    mmLogger_LogI(gLogger, "Assets     pInternalFilesDirectory : %s", mmString_CStr(&p->hInternalFilesDirectory));
    mmLogger_LogI(gLogger, "Assets     pInternalCacheDirectory : %s", mmString_CStr(&p->hInternalCacheDirectory));
    mmLogger_LogI(gLogger, "Assets     pExternalFilesDirectory : %s", mmString_CStr(&p->hExternalFilesDirectory));
    mmLogger_LogI(gLogger, "Assets     pExternalCacheDirectory : %s", mmString_CStr(&p->hExternalCacheDirectory));
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
}
MM_EXPORT_NWSI void mmPackageAssets_OnBeforeTerminate(struct mmPackageAssets* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI
void
mmPackageAssets_MakeShaderCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &p->hExternalCacheDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &p->hShaderCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}

MM_EXPORT_NWSI
void
mmPackageAssets_MakeTextureCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &p->hExternalCacheDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &p->hTextureCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}

MM_EXPORT_NWSI struct mmPackageAssets* mmPackageAssets_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmPackageAssets_Field_hNativePtr);
    return (struct mmPackageAssets*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmPackageAssets_SetNativePtr(JNIEnv* env, jobject obj, struct mmPackageAssets* p)
{
    mmJavaNative_SetLongField(env, p->jNativePtr, mmPackageAssets_Field_hNativePtr, 0);
    mmJavaNative_DeleteGlobalRef(&p->jNativePtr, env);
    mmJavaNative_CreateGlobalRef(&p->jNativePtr, env, obj);
    mmJavaNative_SetLongField(env, obj, mmPackageAssets_Field_hNativePtr,  (jlong)p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetAssetManager(JNIEnv* env, jobject obj, jobject pAssetManager)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    mmJavaNative_DeleteGlobalRef(&p->jAssetManagerPtr, env);
    mmJavaNative_CreateGlobalRef(&p->jAssetManagerPtr, env, pAssetManager);

    p->pAssetManager = (void*)p->jAssetManagerPtr;
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetPackageName(JNIEnv* env, jobject obj, jstring pPackageName)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_PackageName_c_str = (*env)->GetStringUTFChars(env, pPackageName, NULL);
    const char* r_PackageName_c_str = NULL == j_PackageName_c_str ? "" : j_PackageName_c_str;
    mmString_Assigns(&p->hPackageName, r_PackageName_c_str);
    (*env)->ReleaseStringUTFChars(env, pPackageName, j_PackageName_c_str);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetVersionName(JNIEnv* env, jobject obj, jstring pVersionName)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_VersionName_c_str = (*env)->GetStringUTFChars(env, pVersionName, NULL);
    const char* r_VersionName_c_str = NULL == j_VersionName_c_str ? "" : j_VersionName_c_str;
    mmString_Assigns(&p->hVersionName, r_VersionName_c_str);
    (*env)->ReleaseStringUTFChars(env, pVersionName, j_VersionName_c_str);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetVersionCode(JNIEnv* env, jobject obj, jstring pVersionCode)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_VersionCode_c_str = (*env)->GetStringUTFChars(env, pVersionCode, NULL);
    const char* r_VersionCode_c_str = NULL == j_VersionCode_c_str ? "" : j_VersionCode_c_str;
    mmString_Assigns(&p->hVersionCode, r_VersionCode_c_str);
    (*env)->ReleaseStringUTFChars(env, pVersionCode, j_VersionCode_c_str);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetDisplayName(JNIEnv* env, jobject obj, jstring pDisplayName)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_DisplayName_c_str = (*env)->GetStringUTFChars(env, pDisplayName, NULL);
    const char* r_DisplayName_c_str = NULL == j_DisplayName_c_str ? "" : j_DisplayName_c_str;
    mmString_Assigns(&p->hDisplayName, r_DisplayName_c_str);
    (*env)->ReleaseStringUTFChars(env, pDisplayName, j_DisplayName_c_str);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetHomeDirectory(JNIEnv* env, jobject obj, jstring pHomeDirectory)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_HomeDirectory_c_str = (*env)->GetStringUTFChars(env, pHomeDirectory, NULL);
    const char* r_HomeDirectory_c_str = NULL == j_HomeDirectory_c_str ? "" : j_HomeDirectory_c_str;
    mmString_Assigns(&p->hHomeDirectory, r_HomeDirectory_c_str);
    (*env)->ReleaseStringUTFChars(env, pHomeDirectory, j_HomeDirectory_c_str);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetInternalFilesDirectory(JNIEnv* env, jobject obj, jstring pInternalFilesDirectory)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_InternalFilesDirectory_c_str = (*env)->GetStringUTFChars(env, pInternalFilesDirectory, NULL);
    const char* r_InternalFilesDirectory_c_str = NULL == j_InternalFilesDirectory_c_str ? "" : j_InternalFilesDirectory_c_str;
    mmString_Assigns(&p->hInternalFilesDirectory, r_InternalFilesDirectory_c_str);
    (*env)->ReleaseStringUTFChars(env, pInternalFilesDirectory, j_InternalFilesDirectory_c_str);    
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetInternalCacheDirectory(JNIEnv* env, jobject obj, jstring pInternalCacheDirectory)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_InternalCacheDirectory_c_str = (*env)->GetStringUTFChars(env, pInternalCacheDirectory, NULL);
    const char* r_InternalCacheDirectory_c_str = NULL == j_InternalCacheDirectory_c_str ? "" : j_InternalCacheDirectory_c_str;
    mmString_Assigns(&p->hInternalCacheDirectory, r_InternalCacheDirectory_c_str);
    (*env)->ReleaseStringUTFChars(env, pInternalCacheDirectory, j_InternalCacheDirectory_c_str); 
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetExternalFilesDirectory(JNIEnv* env, jobject obj, jstring pExternalFilesDirectory)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_ExternalFilesDirectory_c_str = (*env)->GetStringUTFChars(env, pExternalFilesDirectory, NULL);
    const char* r_ExternalFilesDirectory_c_str = NULL == j_ExternalFilesDirectory_c_str ? "" : j_ExternalFilesDirectory_c_str;
    mmString_Assigns(&p->hExternalFilesDirectory, r_ExternalFilesDirectory_c_str);
    (*env)->ReleaseStringUTFChars(env, pExternalFilesDirectory, j_ExternalFilesDirectory_c_str); 
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetExternalCacheDirectory(JNIEnv* env, jobject obj, jstring pExternalCacheDirectory)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_ExternalCacheDirectory_c_str = (*env)->GetStringUTFChars(env, pExternalCacheDirectory, NULL);
    const char* r_ExternalCacheDirectory_c_str = NULL == j_ExternalCacheDirectory_c_str ? "" : j_ExternalCacheDirectory_c_str;
    mmString_Assigns(&p->hExternalCacheDirectory, r_ExternalCacheDirectory_c_str);
    (*env)->ReleaseStringUTFChars(env, pExternalCacheDirectory, j_ExternalCacheDirectory_c_str); 
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetPluginFolder(JNIEnv* env, jobject obj, jstring pPluginFolder)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_PluginFolder_c_str = (*env)->GetStringUTFChars(env, pPluginFolder, NULL);
    const char* r_PluginFolder_c_str = NULL == j_PluginFolder_c_str ? "" : j_PluginFolder_c_str;
    mmString_Assigns(&p->hPluginFolder, r_PluginFolder_c_str);
    (*env)->ReleaseStringUTFChars(env, pPluginFolder, j_PluginFolder_c_str); 
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetDynlibFolder(JNIEnv* env, jobject obj, jstring pDynlibFolder)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_DynlibFolder_c_str = (*env)->GetStringUTFChars(env, pDynlibFolder, NULL);
    const char* r_DynlibFolder_c_str = NULL == j_DynlibFolder_c_str ? "" : j_DynlibFolder_c_str;
    mmString_Assigns(&p->hDynlibFolder, r_DynlibFolder_c_str);
    (*env)->ReleaseStringUTFChars(env, pDynlibFolder, j_DynlibFolder_c_str); 
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetLoggerPathName(JNIEnv* env, jobject obj, jstring pLoggerPathName)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_LoggerPathName_c_str = (*env)->GetStringUTFChars(env, pLoggerPathName, NULL);
    const char* r_LoggerPathName_c_str = NULL == j_LoggerPathName_c_str ? "" : j_LoggerPathName_c_str;
    mmString_Assigns(&p->hExternalCacheDirectory, r_LoggerPathName_c_str);
    (*env)->ReleaseStringUTFChars(env, pLoggerPathName, j_LoggerPathName_c_str); 
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetAssetsRootFolder(JNIEnv* env, jobject obj, jstring pAssetsRootFolderPath, jstring pAssetsRootFolderBase)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_AssetsRootFolderPath_c_str = (*env)->GetStringUTFChars(env, pAssetsRootFolderPath, NULL);
    const char* j_AssetsRootFolderBase_c_str = (*env)->GetStringUTFChars(env, pAssetsRootFolderBase, NULL);
    const char* r_AssetsRootFolderPath_c_str = NULL == j_AssetsRootFolderPath_c_str ? "" : j_AssetsRootFolderPath_c_str;    
    const char* r_AssetsRootFolderBase_c_str = NULL == j_AssetsRootFolderBase_c_str ? "" : j_AssetsRootFolderBase_c_str;
    mmString_Assigns(&p->hAssetsRootFolderPath, r_AssetsRootFolderPath_c_str);
    mmString_Assigns(&p->hAssetsRootFolderBase, r_AssetsRootFolderBase_c_str);
    (*env)->ReleaseStringUTFChars(env, pAssetsRootFolderPath, j_AssetsRootFolderPath_c_str); 
    (*env)->ReleaseStringUTFChars(env, pAssetsRootFolderBase, j_AssetsRootFolderBase_c_str); 
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeSetAssetsRootSource(JNIEnv* env, jobject obj, jstring pAssetsRootSourcePath, jstring pAssetsRootSourceBase)
{
    struct mmPackageAssets* p = mmPackageAssets_GetNativePtr(env, obj);

    const char* j_AssetsRootSourcePath_c_str = (*env)->GetStringUTFChars(env, pAssetsRootSourcePath, NULL);
    const char* j_AssetsRootSourceBase_c_str = (*env)->GetStringUTFChars(env, pAssetsRootSourceBase, NULL);
    const char* r_AssetsRootSourcePath_c_str = NULL == j_AssetsRootSourcePath_c_str ? "" : j_AssetsRootSourcePath_c_str;
    const char* r_AssetsRootSourceBase_c_str = NULL == j_AssetsRootSourceBase_c_str ? "" : j_AssetsRootSourceBase_c_str;
    mmString_Assigns(&p->hAssetsRootSourcePath, r_AssetsRootSourcePath_c_str);
    mmString_Assigns(&p->hAssetsRootSourceBase, r_AssetsRootSourceBase_c_str);
    (*env)->ReleaseStringUTFChars(env, pAssetsRootSourcePath, j_AssetsRootSourcePath_c_str); 
    (*env)->ReleaseStringUTFChars(env, pAssetsRootSourceBase, j_AssetsRootSourceBase_c_str); 
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeOnFinishLaunching(JNIEnv* env, jobject obj)
{

}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmPackageAssets_NativeOnBeforeTerminate(JNIEnv* env, jobject obj)
{

}
