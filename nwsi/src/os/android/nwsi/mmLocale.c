/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmLocale.h"

#include "dish/mmJavaVMEnv.h"

#define BUFFER_SIZE 64

MM_EXPORT_NWSI jclass mmLocale_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmLocale_Method_LanguageCountry = NULL;

MM_EXPORT_NWSI void mmLocale_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmLocale");
    mmLocale_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmLocale_Method_LanguageCountry = (*env)->GetStaticMethodID(env, object_class, "LanguageCountry", "([B[B)V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmLocale_DetachVirtualMachine(JNIEnv* env)
{
    mmLocale_Method_LanguageCountry = NULL;

    (*env)->DeleteGlobalRef(env, mmLocale_ObjectClass);
    mmLocale_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmLocale_LanguageCountry(struct mmString* pLanguage, struct mmString* pCountry)
{
    jbyte cLanguage[32] = { 0 };
    jbyte cCountry[32] = { 0 };

    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jbyteArray j_Language = (*env)->NewByteArray(env, 32);
    jbyteArray j_Country = (*env)->NewByteArray(env, 32);

    (*env)->CallStaticVoidMethod(env, mmLocale_ObjectClass, mmLocale_Method_LanguageCountry, j_Language, j_Country);

    (*env)->GetByteArrayRegion(env, j_Language, 0, 32, cLanguage);
    (*env)->GetByteArrayRegion(env, j_Country, 0, 32, cCountry);
    mmString_Assigns(pLanguage, (const char*)cLanguage);
    mmString_Assigns(pCountry, (const char*)cCountry);

    (*env)->DeleteLocalRef(env, j_Language);
    (*env)->DeleteLocalRef(env, j_Country);
}
