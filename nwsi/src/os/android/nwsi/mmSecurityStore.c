/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSecurityStore.h"

#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"

#include "algorithm/mmMd5.h"

#include "dish/mmJavaVMEnv.h"

#include "nwsi/mmJavaNativeAndroid.h"

MM_EXPORT_NWSI jclass mmSecurityStore_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmSecurityStore_Field_hNativePtr = NULL;
MM_EXPORT_NWSI jmethodID mmSecurityStore_Method_Insert = NULL;
MM_EXPORT_NWSI jmethodID mmSecurityStore_Method_Update = NULL;
MM_EXPORT_NWSI jmethodID mmSecurityStore_Method_Select = NULL;
MM_EXPORT_NWSI jmethodID mmSecurityStore_Method_Delete = NULL;

MM_EXPORT_NWSI void mmSecurityStore_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmSecurityStore");
    mmSecurityStore_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmSecurityStore_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");
    mmSecurityStore_Method_Insert = (*env)->GetMethodID(env, object_class, "Insert", "(Ljava/lang/String;[B)V");
    mmSecurityStore_Method_Update = (*env)->GetMethodID(env, object_class, "Update", "(Ljava/lang/String;[B)V");
    mmSecurityStore_Method_Select = (*env)->GetMethodID(env, object_class, "Select", "(Ljava/lang/String;[B)V");
    mmSecurityStore_Method_Delete = (*env)->GetMethodID(env, object_class, "Delete", "(Ljava/lang/String;)V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmSecurityStore_DetachVirtualMachine(JNIEnv* env)
{
    (*env)->DeleteGlobalRef(env, mmSecurityStore_ObjectClass);
    mmSecurityStore_ObjectClass = NULL;

    mmSecurityStore_Field_hNativePtr = NULL;
    mmSecurityStore_Method_Insert = NULL;
    mmSecurityStore_Method_Update = NULL;
    mmSecurityStore_Method_Select = NULL;
    mmSecurityStore_Method_Delete = NULL;
}

MM_EXPORT_NWSI void mmSecurityStore_Init(struct mmSecurityStore* p)
{
    mmString_Init(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    mmString_Assigns(&p->hAppId, "");

    mmJavaNative_JObjectInit(&p->jNativePtr);
}
MM_EXPORT_NWSI void mmSecurityStore_Destroy(struct mmSecurityStore* p)
{
    mmJavaNative_JObjectDestroy(&p->jNativePtr);

    mmString_Destroy(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;
}

MM_EXPORT_NWSI void mmSecurityStore_SetAppId(struct mmSecurityStore* p, const char* pAppId)
{
    mmString_Assigns(&p->hAppId, pAppId);
}

MM_EXPORT_NWSI void mmSecurityStore_OnFinishLaunching(struct mmSecurityStore* p)
{

}
MM_EXPORT_NWSI void mmSecurityStore_OnBeforeTerminate(struct mmSecurityStore* p)
{

}

MM_EXPORT_NWSI void mmSecurityStore_Insert(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pMember = (*env)->NewStringUTF(env, pMember);

    jbyteArray j_pSecret = (*env)->NewByteArray(env, 64);

    (*env)->SetByteArrayRegion(env, j_pSecret, 0, 64, (const jbyte*)pSecret);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmSecurityStore_Method_Insert, j_pMember, j_pSecret);

    (*env)->DeleteLocalRef(env, j_pSecret);

    (*env)->DeleteLocalRef(env, j_pMember);
}
MM_EXPORT_NWSI void mmSecurityStore_Update(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pMember = (*env)->NewStringUTF(env, pMember);

    jbyteArray j_pSecret = (*env)->NewByteArray(env, 64);

    (*env)->SetByteArrayRegion(env, j_pSecret, 0, 64, (const jbyte*)pSecret);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmSecurityStore_Method_Update, j_pMember, j_pSecret);

    (*env)->DeleteLocalRef(env, j_pSecret);

    (*env)->DeleteLocalRef(env, j_pMember);
}
MM_EXPORT_NWSI void mmSecurityStore_Select(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pMember = (*env)->NewStringUTF(env, pMember);

    jbyteArray j_pSecret = (*env)->NewByteArray(env, 64);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmSecurityStore_Method_Select, j_pMember, j_pSecret);

    mmMemset(pSecret, 0, 64);
    (*env)->GetByteArrayRegion(env, j_pSecret, 0, 64, (jbyte*)pSecret);

    (*env)->DeleteLocalRef(env, j_pSecret);

    (*env)->DeleteLocalRef(env, j_pMember);
}
MM_EXPORT_NWSI void mmSecurityStore_Delete(struct mmSecurityStore* p, const char* pMember)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jstring j_pMember = (*env)->NewStringUTF(env, pMember);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmSecurityStore_Method_Delete, j_pMember);

    (*env)->DeleteLocalRef(env, j_pMember);
}

static void mmSecurityStore_Md5ToString(md5_uint8 hDigest[16], char hBuffer[64])
{
    int i;
    char* v = hBuffer;
    for (i = 0; i < 16; i++)
    {
        (*v++) = mmHexDigit[(hDigest[i] >> 4) & 0x0F];
        (*v++) = mmHexDigit[(hDigest[i]     ) & 0x0F];
    }
    (*v++) = 0;
}

// java -> c

// jni interface.
MM_EXPORT_NWSI struct mmSecurityStore* mmSecurityStore_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmSecurityStore_Field_hNativePtr);
    return (struct mmSecurityStore*)j_hNativePtr;
}
MM_EXPORT_NWSI void mmSecurityStore_SetNativePtr(JNIEnv* env, jobject obj, struct mmSecurityStore* p)
{
    mmJavaNative_SetLongField(env, p->jNativePtr, mmSecurityStore_Field_hNativePtr, 0);
    mmJavaNative_DeleteGlobalRef(&p->jNativePtr, env);
    mmJavaNative_CreateGlobalRef(&p->jNativePtr, env, obj);
    (*env)->SetLongField(env, obj, mmSecurityStore_Field_hNativePtr, (jlong)p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmSecurityStore_NativeGetSecretKey(JNIEnv* env, jobject obj, jstring pAppId, jobject pUUIDBytes, jobject pSecretKey)
{
    jbyte cUUIDBytes[64] = { 0 };
    const char* j_pAppId_Chars = (*env)->GetStringUTFChars(env, pAppId, NULL);
    const char* r_pAppId_Chars = (NULL == j_pAppId_Chars) ? "" : j_pAppId_Chars;

    jbyteArray j_pUUIDBytes = (jbyteArray)(pUUIDBytes);

    jbyteArray j_pSecretKey = (jbyteArray)(pSecretKey);

    // Note: the md5 buffer need 64, not 32. the sprintf need '\0' for append.
    char hBuffer[64] = { 0 };
    md5_uint8 hDigest[16] = { 0 };
    struct mmString hKeyString;
    struct mmMd5Context hMd5;

    mmString_Init(&hKeyString);

    (*env)->GetByteArrayRegion(env, j_pUUIDBytes, 0, 64, (jbyte*)cUUIDBytes);

    mmString_Assigns(&hKeyString, r_pAppId_Chars);
    mmString_Appendsn(&hKeyString, (const char*)cUUIDBytes, 64);
    mmString_Appends(&hKeyString, "org.mm.nwsi");

    mmMd5_Starts(&hMd5);
    mmMd5_Update(&hMd5, (md5_uint8*)mmString_Data(&hKeyString), mmString_Size(&hKeyString));
    mmMd5_Finish(&hMd5, hDigest);

    mmSecurityStore_Md5ToString(hDigest, (char*)hBuffer);

    (*env)->SetByteArrayRegion(env, j_pSecretKey, 0, 32, (const jbyte*)hBuffer);

    mmString_Destroy(&hKeyString);

    (*env)->ReleaseStringUTFChars(env, pAppId, j_pAppId_Chars);
}
