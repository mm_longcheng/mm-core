/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmJavaNativeAndroid.h"

#include "dish/mmJavaVMEnv.h"

MM_EXPORT_NWSI void mmJavaNative_JObjectInit(jobject* j)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();
    mmJavaNative_CreateGlobalRef(j, env, NULL);
}

MM_EXPORT_NWSI void mmJavaNative_JObjectDestroy(jobject* j)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();
    mmJavaNative_DeleteGlobalRef(j, env);
}

MM_EXPORT_NWSI void mmJavaNative_CreateGlobalRef(jobject* j, JNIEnv* env, jobject obj)
{
    if (NULL != obj)
    {
        (*j) = (jobject)(*env)->NewGlobalRef(env, obj);
    }
    else
    {
        (*j) = (jobject)NULL;
    }
}

MM_EXPORT_NWSI void mmJavaNative_DeleteGlobalRef(jobject* j, JNIEnv* env)
{
    if (NULL != (*j))
    {
        (*env)->DeleteGlobalRef(env, (*j));
        (*j) = NULL;
    }
}
MM_EXPORT_NWSI void mmJavaNative_SetLongField(JNIEnv* env, jobject obj, jfieldID field, jlong value)
{
    if (NULL != obj)
    {
        (*env)->SetLongField(env, obj, field, value);
    }
}

MM_EXPORT_NWSI jclass mmJavaLangInteger_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmJavaLangInteger_Method_NewObject;
MM_EXPORT_NWSI jmethodID mmJavaLangInteger_Method_intValue = NULL;
MM_EXPORT_NWSI void mmJavaLangInteger_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "java/lang/Integer");
    mmJavaLangInteger_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmJavaLangInteger_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "(I)V");
    mmJavaLangInteger_Method_intValue = (*env)->GetMethodID(env, object_class, "intValue", "()I");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmJavaLangInteger_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaLangInteger_Method_NewObject = NULL;
    mmJavaLangInteger_Method_intValue = NULL;

    (*env)->DeleteGlobalRef(env, mmJavaLangInteger_ObjectClass);
    mmJavaLangInteger_ObjectClass = NULL;
}

MM_EXPORT_NWSI jclass mmJavaUtilIterator_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilIterator_Method_hasNext = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilIterator_Method_next = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilIterator_Method_remove = NULL;
MM_EXPORT_NWSI void mmJavaUtilIterator_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "java/util/Iterator");
    mmJavaUtilIterator_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmJavaUtilIterator_Method_hasNext = (*env)->GetMethodID(env, object_class, "hasNext", "()Z");
    mmJavaUtilIterator_Method_next = (*env)->GetMethodID(env, object_class, "next", "()Ljava/lang/Object;");
    mmJavaUtilIterator_Method_remove = (*env)->GetMethodID(env, object_class, "remove", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmJavaUtilIterator_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaUtilIterator_Method_hasNext = NULL;
    mmJavaUtilIterator_Method_next = NULL;
    mmJavaUtilIterator_Method_remove = NULL;

    (*env)->DeleteGlobalRef(env, mmJavaUtilIterator_ObjectClass);
    mmJavaUtilIterator_ObjectClass = NULL;
}

MM_EXPORT_NWSI jclass mmJavaUtilLinkedList_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilLinkedList_Method_iterator = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilLinkedList_Method_add = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilLinkedList_Method_clear = NULL;
MM_EXPORT_NWSI void mmJavaUtilLinkedList_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "java/util/LinkedList");
    mmJavaUtilLinkedList_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmJavaUtilLinkedList_Method_iterator = (*env)->GetMethodID(env, object_class, "iterator", "()Ljava/util/Iterator;");
    mmJavaUtilLinkedList_Method_add = (*env)->GetMethodID(env, object_class, "add", "(Ljava/lang/Object;)Z");
    mmJavaUtilLinkedList_Method_clear = (*env)->GetMethodID(env, object_class, "clear", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmJavaUtilLinkedList_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaUtilLinkedList_Method_iterator = NULL;
    mmJavaUtilLinkedList_Method_add = NULL;
    mmJavaUtilLinkedList_Method_clear = NULL;

    (*env)->DeleteGlobalRef(env, mmJavaUtilLinkedList_ObjectClass);
    mmJavaUtilLinkedList_ObjectClass = NULL;
}

MM_EXPORT_NWSI jclass mmJavaUtilMapEntry_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilMapEntry_Method_getKey = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilMapEntry_Method_getValue = NULL;
MM_EXPORT_NWSI void mmJavaUtilMapEntry_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "java/util/Map$Entry");
    mmJavaUtilMapEntry_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmJavaUtilMapEntry_Method_getKey = (*env)->GetMethodID(env, object_class, "getKey", "()Ljava/lang/Object;");
    mmJavaUtilMapEntry_Method_getValue = (*env)->GetMethodID(env, object_class, "getValue", "()Ljava/lang/Object;");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmJavaUtilMapEntry_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaUtilMapEntry_Method_getKey = NULL;
    mmJavaUtilMapEntry_Method_getValue = NULL;

    (*env)->DeleteGlobalRef(env, mmJavaUtilMapEntry_ObjectClass);
    mmJavaUtilMapEntry_ObjectClass = NULL;
}

MM_EXPORT_NWSI jclass mmJavaUtilSet_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilSet_Method_iterator = NULL;
MM_EXPORT_NWSI void mmJavaUtilSet_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "java/util/Set");
    mmJavaUtilSet_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmJavaUtilSet_Method_iterator = (*env)->GetMethodID(env, object_class, "iterator", "()Ljava/util/Iterator;");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmJavaUtilSet_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaUtilSet_Method_iterator = NULL;

    (*env)->DeleteGlobalRef(env, mmJavaUtilSet_ObjectClass);
    mmJavaUtilSet_ObjectClass = NULL;
}

MM_EXPORT_NWSI jclass mmJavaUtilTreeMap_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilTreeMap_Method_entrySet = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilTreeMap_Method_put = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilTreeMap_Method_get = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilTreeMap_Method_remove = NULL;
MM_EXPORT_NWSI jmethodID mmJavaUtilTreeMap_Method_clear = NULL;
MM_EXPORT_NWSI void mmJavaUtilTreeMap_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "java/util/TreeMap");
    mmJavaUtilTreeMap_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmJavaUtilTreeMap_Method_entrySet = (*env)->GetMethodID(env, object_class, "entrySet", "()Ljava/util/Set;");
    mmJavaUtilTreeMap_Method_put = (*env)->GetMethodID(env, object_class, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
    mmJavaUtilTreeMap_Method_get = (*env)->GetMethodID(env, object_class, "get", "(Ljava/lang/Object;)Ljava/lang/Object;");
    mmJavaUtilTreeMap_Method_remove = (*env)->GetMethodID(env, object_class, "remove", "(Ljava/lang/Object;)Ljava/lang/Object;");
    mmJavaUtilTreeMap_Method_clear = (*env)->GetMethodID(env, object_class, "clear", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmJavaUtilTreeMap_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaUtilTreeMap_Method_entrySet = NULL;
    mmJavaUtilTreeMap_Method_put = NULL;
    mmJavaUtilTreeMap_Method_get = NULL;
    mmJavaUtilTreeMap_Method_remove = NULL;
    mmJavaUtilTreeMap_Method_clear = NULL;

    (*env)->DeleteGlobalRef(env, mmJavaUtilTreeMap_ObjectClass);
    mmJavaUtilTreeMap_ObjectClass = NULL;
}

// android.graphics.Matrix
MM_EXPORT_NWSI jclass mmAndroidGraphicsMatrix_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmAndroidGraphicsMatrix_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmAndroidGraphicsMatrix_Method_setValues = NULL;
MM_EXPORT_NWSI void mmAndroidGraphicsMatrix_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "android/graphics/Matrix");
    mmAndroidGraphicsMatrix_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmAndroidGraphicsMatrix_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmAndroidGraphicsMatrix_Method_setValues = (*env)->GetMethodID(env, object_class, "setValues", "([F)V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmAndroidGraphicsMatrix_DetachVirtualMachine(JNIEnv* env)
{
    mmAndroidGraphicsMatrix_Method_NewObject = NULL;
    mmAndroidGraphicsMatrix_Method_setValues = NULL;

     (*env)->DeleteGlobalRef(env, mmAndroidGraphicsMatrix_ObjectClass);
    mmAndroidGraphicsMatrix_ObjectClass = NULL;
}

MM_EXPORT_NWSI void mmJavaNativeAndroid_AttachVirtualMachine(JNIEnv* env)
{
    mmJavaLangInteger_AttachVirtualMachine(env);

    mmJavaUtilIterator_AttachVirtualMachine(env);
    mmJavaUtilLinkedList_AttachVirtualMachine(env);
    mmJavaUtilMapEntry_AttachVirtualMachine(env);
    mmJavaUtilSet_AttachVirtualMachine(env);
    mmJavaUtilTreeMap_AttachVirtualMachine(env);

    mmAndroidGraphicsMatrix_AttachVirtualMachine(env);
}
MM_EXPORT_NWSI void mmJavaNativeAndroid_DetachVirtualMachine(JNIEnv* env)
{
    mmJavaLangInteger_DetachVirtualMachine(env);
    
    mmJavaUtilIterator_DetachVirtualMachine(env);
    mmJavaUtilLinkedList_DetachVirtualMachine(env);
    mmJavaUtilMapEntry_DetachVirtualMachine(env);
    mmJavaUtilSet_DetachVirtualMachine(env);
    mmJavaUtilTreeMap_DetachVirtualMachine(env);

    mmAndroidGraphicsMatrix_DetachVirtualMachine(env);
}

