/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextParagraph_h__
#define __mmTextParagraph_h__

#include "core/mmCore.h"

#include "nwsi/mmTextSpannable.h"
#include "nwsi/mmTextParagraphFormat.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

struct mmTextParagraphFormat;
struct mmTextSpannable;
struct mmTextUtf16String;
struct mmTextDrawingContext;

struct mmTextParagraph
{
    struct mmTextParagraphFormat* pParagraphFormat;
    struct mmTextSpannable* pTextSpannable;
    struct mmTextUtf16String* pText;
    struct mmTextDrawingContext* pDrawingContext;

    jobject jNativePtr;
};

MM_EXPORT_NWSI 
void mmTextParagraph_Init(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_Destroy(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_SetTextParagraphFormat(
    struct mmTextParagraph*                        p, 
    struct mmTextParagraphFormat*                  pParagraphFormat);

MM_EXPORT_NWSI 
void mmTextParagraph_SetTextSpannable(
    struct mmTextParagraph*                        p, 
    struct mmTextSpannable*                        pTextSpannable);

MM_EXPORT_NWSI 
void mmTextParagraph_SetText(
    struct mmTextParagraph*                        p, 
    struct mmTextUtf16String*                      pText);

MM_EXPORT_NWSI 
void mmTextParagraph_SetDrawingContext(
    struct mmTextParagraph*                        p, 
    struct mmTextDrawingContext*                   pDrawingContext);

MM_EXPORT_NWSI 
void mmTextParagraph_OnFinishLaunching(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_OnBeforeTerminate(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_UpdateDrawingContext(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_UpdateTextImplement(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_UpdateTextFormat(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_UpdateTextLayout(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_UpdateFontFeature(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_UpdateTextSpannable(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_DeleteTextFormat(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_DeleteTextLayout(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void mmTextParagraph_Draw(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_HitTestPoint(
    const struct mmTextParagraph*                  p, 
    float                                          point[2], 
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics);

MM_EXPORT_NWSI 
void 
mmTextParagraph_HitTestTextPosition(
    const struct mmTextParagraph*                  p, 
    mmUInt32_t                                     offset, 
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics);

MM_EXPORT_NWSI extern jclass mmTextParagraph_ObjectClass;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_NewObject;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_Init;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_Destroy;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_SetTextParagraphFormat;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_SetTextSpannable;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_SetText;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_SetDrawingContext;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_OnFinishLaunching;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_OnBeforeTerminate;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_UpdateTextImplement;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_UpdateTextFormat;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_UpdateTextLayout;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_UpdateFontFeature;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_UpdateTextSpannable;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_DeleteTextFormat;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_DeleteTextLayout;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_Draw;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_HitTestPoint;
MM_EXPORT_NWSI extern jmethodID mmTextParagraph_Method_HitTestTextPosition;

MM_EXPORT_NWSI 
void 
mmTextParagraph_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmTextParagraph_DetachVirtualMachine(
    JNIEnv*                                        env);

#include "core/mmSuffix.h"

#endif//__mmTextParagraph_h__
