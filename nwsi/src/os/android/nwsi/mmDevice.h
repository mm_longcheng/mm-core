/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDevice_h__
#define __mmDevice_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmDevice_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmDevice_Field_hNativePtr;

MM_EXPORT_NWSI void mmDevice_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmDevice_DetachVirtualMachine(JNIEnv* env);

struct mmDevice
{
    // Device information.
    struct mmString hName;           /* e.g. "My iPhone"                */
    struct mmString hModel;          /* e.g. @"iPhone", @"iPod touch"   */
    struct mmString hLocalizedModel; /* localized version of model      */
    struct mmString hSystemName;     /* e.g. @"iOS"                     */
    struct mmString hSystemVersion;  /* e.g. @"4.0"                     */
    
    // Hardware information. struct utsname for uname.
    struct mmString hSysName;        /* [XSI] Name of OS                */
    struct mmString hNodeName;       /* [XSI] Name of this network node */
    struct mmString hRelease;        /* [XSI] Release level             */
    struct mmString hVersion;        /* [XSI] Version level             */
    struct mmString hMachine;        /* [XSI] Hardware type             */
};

MM_EXPORT_NWSI void mmDevice_Init(struct mmDevice* p);
MM_EXPORT_NWSI void mmDevice_Destroy(struct mmDevice* p);

MM_EXPORT_NWSI void mmDevice_OnFinishLaunching(struct mmDevice* p);
MM_EXPORT_NWSI void mmDevice_OnBeforeTerminate(struct mmDevice* p);

// jni interface.
MM_EXPORT_NWSI struct mmDevice* mmDevice_GetNativePtr(JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmDevice_SetNativePtr(JNIEnv* env, jobject obj, struct mmDevice* p);

#include "core/mmSuffix.h"

#endif//__mmDevice_h__
