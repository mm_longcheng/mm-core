/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNativeApplication.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmLoggerManager.h"

#include "dish/mmJavaVMEnv.h"

#include "nwsi/mmPackageAssets.h"

#include "nwsi/mmJavaNativeAndroid.h"

MM_EXPORT_NWSI jclass mmNativeApplication_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmNativeApplication_Field_hNativePtr = NULL;

MM_EXPORT_NWSI void mmNativeApplication_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmNativeApplication");
    mmNativeApplication_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmNativeApplication_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmNativeApplication_DetachVirtualMachine(JNIEnv* env)
{
    (*env)->DeleteGlobalRef(env, mmNativeApplication_ObjectClass);
    mmNativeApplication_ObjectClass = NULL;

    mmNativeApplication_Field_hNativePtr = NULL;
}

MM_EXPORT_NWSI void mmNativeApplication_Init(struct mmNativeApplication* p)
{
    mmContextMaster_Init(&p->hContextMaster);
    mmSurfaceMaster_Init(&p->hSurfaceMaster);

    mmNativeClipboard_Init(&p->hNativeClipboard);

    mmString_Init(&p->hRenderSystemName);
    
    mmString_Init(&p->hLoggerFileName);
    p->hLoggerLevel = MM_LOG_INFO;
    p->pIActivity = NULL;
    
    mmContextMaster_SetClipboardProvider(&p->hContextMaster, &p->hNativeClipboard.hSuper);

    mmSurfaceMaster_SetContextMaster(&p->hSurfaceMaster, &p->hContextMaster);
    // Android platform limit frame rate by os DisplayLink.
    mmSurfaceMaster_SetSmoothMode(&p->hSurfaceMaster, MM_TRUE);

    mmString_Assigns(&p->hRenderSystemName, "OpenGL ES 2.x Rendering Subsystem");
    mmString_Assigns(&p->hLoggerFileName, "nwsi");

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;
    p->jActivityPtr = NULL;

    mmJavaNative_JObjectInit(&p->jActivityPtr);
}
MM_EXPORT_NWSI void mmNativeApplication_Destroy(struct mmNativeApplication* p)
{
    mmJavaNative_JObjectDestroy(&p->jActivityPtr);

    p->jActivityPtr = NULL;
    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    p->pIActivity = NULL;
    p->hLoggerLevel = MM_LOG_INFO;
    mmString_Destroy(&p->hLoggerFileName);
    
    mmString_Destroy(&p->hRenderSystemName);
    
    mmNativeClipboard_Destroy(&p->hNativeClipboard);

    mmSurfaceMaster_Destroy(&p->hSurfaceMaster);
    mmContextMaster_Destroy(&p->hContextMaster);
}

MM_EXPORT_NWSI void mmNativeApplication_SetAppHandler(struct mmNativeApplication* p, void* pAppHandler)
{
    mmContextMaster_SetAppHandler(&p->hContextMaster, pAppHandler);
}

MM_EXPORT_NWSI void mmNativeApplication_SetModuleName(struct mmNativeApplication* p, const char* pModuleName)
{
    mmContextMaster_SetModuleName(&p->hContextMaster, pModuleName);
}
MM_EXPORT_NWSI void mmNativeApplication_SetRenderSystemName(struct mmNativeApplication* p, const char* pRenderSystemName)
{
    mmString_Assigns(&p->hRenderSystemName, pRenderSystemName);
}
MM_EXPORT_NWSI void mmNativeApplication_SetLoggerFileName(struct mmNativeApplication* p, const char* pLoggerFileName)
{
    mmString_Assigns(&p->hLoggerFileName, pLoggerFileName);
}
MM_EXPORT_NWSI void mmNativeApplication_SetLoggerLevel(struct mmNativeApplication* p, mmUInt32_t hLoggerLevel)
{
    p->hLoggerLevel = hLoggerLevel;
}
MM_EXPORT_NWSI void mmNativeApplication_SetDisplayFrequency(struct mmNativeApplication* p, double hDisplayFrequency)
{
    mmSurfaceMaster_SetDisplayFrequency(&p->hSurfaceMaster, hDisplayFrequency);
}

MM_EXPORT_NWSI void mmNativeApplication_SetActivityMaster(struct mmNativeApplication* p, struct mmIActivity* pIActivity)
{
    p->pIActivity = pIActivity;
    (*(p->pIActivity->SetContext))(p->pIActivity, &p->hContextMaster);
    (*(p->pIActivity->SetSurface))(p->pIActivity, &p->hSurfaceMaster);
    mmSurfaceMaster_SetIActivity(&p->hSurfaceMaster, p->pIActivity);
}

MM_EXPORT_NWSI void mmNativeApplication_OnFinishLaunching(struct mmNativeApplication* p)
{
    struct mmPackageAssets* pPackageAssets = &p->hContextMaster.hPackageAssets;
    // struct mmOgrePluginLoader* pOgrePluginLoader = &p->hContextMaster.hOgrePluginLoader;
    // struct mmPureDynlibLoader* pPureDynlibLoader = &p->hContextMaster.hPureDynlibLoader;
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    
    mmPackageAssets_AttachNativeResource(pPackageAssets);
    
    // load plug-in first.
    // cegui need MODULE_DIR_VAR_NAME to location MODULE *.so use dlopen.
    // MODULE_DIR_VAR_NAME will auto append "/".
    // setenv(MODULE_DIR_VAR_NAME, mmString_CStr(&pPackageAssets->hPackageName), 1);

    // mmOgrePluginLoader_SetPluginFolder(pOgrePluginLoader, mmString_CStr(&pPackageAssets->hPluginFolder));
    // mmOgrePluginLoader_SetPluginPrefix(pOgrePluginLoader, "lib");
    // mmOgrePluginLoader_SetPluginSuffix(pOgrePluginLoader, ".so");

    // mmPureDynlibLoader_SetDynlibFolder(pPureDynlibLoader, mmString_CStr(&pPackageAssets->hDynlibFolder));
    // mmPureDynlibLoader_SetDynlibPrefix(pPureDynlibLoader, "lib");
    // mmPureDynlibLoader_SetDynlibSuffix(pPureDynlibLoader, ".so");
    
    mmLoggerManager_SetFileName(gLoggerManager, mmString_CStr(&p->hLoggerFileName));
    mmLoggerManager_SetFileFopenMode(gLoggerManager, "wb");
    mmLoggerManager_SetLoggerPath(gLoggerManager, mmString_CStr(&pPackageAssets->hLoggerPath));
    mmLoggerManager_SetLoggerLevel(gLoggerManager, p->hLoggerLevel);
    mmLoggerManager_SetIsConsole(gLoggerManager, 1);
    mmLoggerManager_SetIsImmediately(gLoggerManager, 1);
    mmLoggerManager_FOpen(gLoggerManager);
    
    mmContextMaster_SetRenderSystemName(&p->hContextMaster, mmString_CStr(&p->hRenderSystemName));
    mmContextMaster_SetShaderCacheFolder(&p->hContextMaster, mmString_CStr(&pPackageAssets->hShaderCacheFolder));
    mmContextMaster_SetTextureCacheEnabled(&p->hContextMaster, MM_TRUE);
    
    // mmOgrePlugin_Register(&p->hContextMaster.hOgrePluginLoader);
    
    mmContextMaster_OnFinishLaunching(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnBeforeTerminate(struct mmNativeApplication* p)
{
    struct mmPackageAssets* pPackageAssets = &p->hContextMaster.hPackageAssets;
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    
    mmContextMaster_OnBeforeTerminate(&p->hContextMaster.hSuper);
    
    // mmOgrePlugin_Unregister(&p->hContextMaster.hOgrePluginLoader);
    
    mmLoggerManager_Flush(gLoggerManager);
    mmLoggerManager_Close(gLoggerManager);
    
    mmPackageAssets_DetachNativeResource(pPackageAssets);
}

MM_EXPORT_NWSI void mmNativeApplication_OnEnterBackground(struct mmNativeApplication* p)
{
    mmContextMaster_OnEnterBackground(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnEnterForeground(struct mmNativeApplication* p)
{
    mmContextMaster_OnEnterForeground(&p->hContextMaster.hSuper);
}

MM_EXPORT_NWSI void mmNativeApplication_OnStart(struct mmNativeApplication* p)
{
    mmContextMaster_OnStart(&p->hContextMaster.hSuper);
    mmSurfaceMaster_OnStart(&p->hSurfaceMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnInterrupt(struct mmNativeApplication* p)
{
    mmSurfaceMaster_OnInterrupt(&p->hSurfaceMaster.hSuper);
    mmContextMaster_OnInterrupt(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnShutdown(struct mmNativeApplication* p)
{
    mmSurfaceMaster_OnShutdown(&p->hSurfaceMaster.hSuper);
    mmContextMaster_OnShutdown(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnJoin(struct mmNativeApplication* p)
{
    mmSurfaceMaster_OnJoin(&p->hSurfaceMaster.hSuper);
    mmContextMaster_OnJoin(&p->hContextMaster.hSuper);
}

// java -> c

MM_EXPORT_NWSI struct mmNativeApplication* mmNativeApplication_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmNativeApplication_Field_hNativePtr);
    return (struct mmNativeApplication*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmNativeApplication_SetNativePtr(JNIEnv* env, jobject obj, struct mmNativeApplication* p)
{
    jlong j_hNativePtr = (jlong)p;
    (*env)->SetLongField(env, obj, mmNativeApplication_Field_hNativePtr, j_hNativePtr);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeAlloc(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = (struct mmNativeApplication*)mmMalloc(sizeof(struct mmNativeApplication));
    p->jNativePtr = (jobject)(*env)->NewGlobalRef(env, obj);
    mmNativeApplication_SetNativePtr(env, obj, p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeDealloc(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_SetNativePtr(env, obj, NULL);
    (*env)->DeleteGlobalRef(env, p->jNativePtr);
    mmFree(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeInit(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_Init(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeDestroy(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_Destroy(p);
}

JNIEXPORT jlong JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeGetContextMaster(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    return (jlong)&p->hContextMaster;
}
JNIEXPORT jlong JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeGetSurfaceMaster(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    return (jlong)&p->hSurfaceMaster;
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetActivity(JNIEnv* env, jobject obj, jobject pActivity)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    mmJavaNative_DeleteGlobalRef(&p->jActivityPtr, env);
    mmJavaNative_CreateGlobalRef(&p->jActivityPtr, env, pActivity);

    mmNativeApplication_SetAppHandler(p, (void*)p->jActivityPtr);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetClipboard(JNIEnv* env, jobject obj, jobject pClipboard)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    mmNativeClipboard_SetJavaObject(&p->hNativeClipboard, env, pClipboard);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetDevice(JNIEnv* env, jobject obj, jobject pDevice)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    struct mmContextMaster* pContextMaster = &p->hContextMaster;
    struct mmDevice* rDevice = &pContextMaster->hDevice;
    mmDevice_SetNativePtr(env, pDevice, rDevice);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetPackageAssets(JNIEnv* env, jobject obj, jobject pPackageAssets)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    struct mmContextMaster* pContextMaster = &p->hContextMaster;
    struct mmPackageAssets* rPackageAssets = &pContextMaster->hPackageAssets;
    mmPackageAssets_SetNativePtr(env, pPackageAssets, rPackageAssets);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetAppIdentifier(JNIEnv* env, jobject obj, jobject pAppIdentifier)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    struct mmContextMaster* pContextMaster = &p->hContextMaster;
    struct mmAppIdentifier* rAppIdentifier = &pContextMaster->hAppIdentifier;
    mmAppIdentifier_SetNativePtr(env, pAppIdentifier, rAppIdentifier);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetSecurityStore(JNIEnv* env, jobject obj, jobject pSecurityStore)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    struct mmContextMaster* pContextMaster = &p->hContextMaster;
    struct mmSecurityStore* rSecurityStore = &pContextMaster->hSecurityStore;
    mmSecurityStore_SetNativePtr(env, pSecurityStore, rSecurityStore);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetModuleName(JNIEnv* env, jobject obj, jstring pModuleName)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    const char* j_ModuleName_c_str = (*env)->GetStringUTFChars(env, pModuleName, NULL);
    const char* r_ModuleName_c_str = NULL == j_ModuleName_c_str ? "" : j_ModuleName_c_str;
    mmNativeApplication_SetModuleName(p, r_ModuleName_c_str);
    (*env)->ReleaseStringUTFChars(env, pModuleName, j_ModuleName_c_str);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetRenderSystemName(JNIEnv* env, jobject obj, jstring pRenderSystemName)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    const char* j_RenderSystemName_c_str = (*env)->GetStringUTFChars(env, pRenderSystemName, NULL);
    const char* r_RenderSystemName_c_str = NULL == j_RenderSystemName_c_str ? "" : j_RenderSystemName_c_str;
    mmNativeApplication_SetRenderSystemName(p, r_RenderSystemName_c_str);
    (*env)->ReleaseStringUTFChars(env, pRenderSystemName, j_RenderSystemName_c_str);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetLoggerFileName(JNIEnv* env, jobject obj, jstring pLoggerFileName)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    const char* j_LoggerFileName_c_str = (*env)->GetStringUTFChars(env, pLoggerFileName, NULL);
    const char* r_LoggerFileName_c_str = NULL == j_LoggerFileName_c_str ? "" : j_LoggerFileName_c_str;
    mmNativeApplication_SetLoggerFileName(p, r_LoggerFileName_c_str);
    (*env)->ReleaseStringUTFChars(env, pLoggerFileName, j_LoggerFileName_c_str);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetLoggerLevel(JNIEnv* env, jobject obj, jint LoggerLevel)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);

    mmNativeApplication_SetLoggerLevel(p, LoggerLevel);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeSetActivityMaster(JNIEnv* env, jobject obj, jlong IActivity)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    struct mmIActivity* pIActivity = (struct mmIActivity*)IActivity;
    mmNativeApplication_SetActivityMaster(p, pIActivity);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnFinishLaunching(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnFinishLaunching(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnBeforeTerminate(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnBeforeTerminate(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnEnterBackground(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnEnterBackground(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnEnterForeground(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnEnterForeground(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnStart(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnStart(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnInterrupt(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnInterrupt(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnShutdown(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnShutdown(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmNativeApplication_NativeOnJoin(JNIEnv* env, jobject obj)
{
    struct mmNativeApplication* p = mmNativeApplication_GetNativePtr(env, obj);
    mmNativeApplication_OnJoin(p);
}
