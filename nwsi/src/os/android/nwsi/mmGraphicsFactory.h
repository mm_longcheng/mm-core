/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmGraphicsFactory_h__
#define __mmGraphicsFactory_h__

#include "core/mmCore.h"

#include "container/mmRbtreeString.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

struct mmPackageAssets;
struct mmFileContext;
struct mmByteBuffer;

struct mmFontResource
{
    struct mmString hFamily;
    struct mmString hPath;
    struct mmString hRealPath;
    void* pHandle;
};

MM_EXPORT_NWSI
void
mmFontResource_Init(
    struct mmFontResource*                         p);

MM_EXPORT_NWSI
void
mmFontResource_Destroy(
    struct mmFontResource*                         p);

struct mmGraphicsFactory
{
    // <string, struct mmFontResource*>
    struct mmRbtreeStringVpt hFonts;

    // weak reference.
    struct mmPackageAssets* pPackageAssets;

    jobject jNativePtr;
};

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_Init(
    struct mmGraphicsFactory*                      p);

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_Destroy(
    struct mmGraphicsFactory*                      p);

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_SetPackageAssets(
    struct mmGraphicsFactory*                      p,
    struct mmPackageAssets*                        pPackageAssets);

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_OnFinishLaunching(
    struct mmGraphicsFactory*                      p);

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_OnBeforeTerminate(
    struct mmGraphicsFactory*                      p);

MM_EXPORT_NWSI
int
mmGraphicsFactory_LoadFontFromResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath);

MM_EXPORT_NWSI
int
mmGraphicsFactory_UnloadFontByResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath);

MM_EXPORT_NWSI
const struct mmFontResource*
mmGraphicsFactory_GetFontResourceByFamily(
    const struct mmGraphicsFactory*                p,
    const char*                                    pFontFamily);

MM_EXPORT_NWSI extern jclass mmGraphicsFactory_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmGraphicsFactory_Field_hNativePtr;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_NewObject;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_Init;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_Destroy;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_SetPackageAssets;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_OnFinishLaunching;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_OnBeforeTerminate;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_CreateTypefaceFromFolder;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_CreateTypefaceFromSource;
MM_EXPORT_NWSI extern jmethodID mmGraphicsFactory_Method_DeleteTypeface;

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_AttachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_DetachVirtualMachine(
    JNIEnv*                                        env);

MM_EXPORT_NWSI struct mmGraphicsFactory* mmGraphicsFactory_GetNativePtr(JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmGraphicsFactory_SetNativePtr(JNIEnv* env, jobject obj, struct mmGraphicsFactory* p);

#include "core/mmSuffix.h"

#endif//__mmGraphicsFactory_h__
