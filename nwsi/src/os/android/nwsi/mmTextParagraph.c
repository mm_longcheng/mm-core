/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextParagraph.h"
#include "mmTextDrawingContext.h"
#include "mmTextUtf16String.h"

#include "mmTextParagraphFormatNative.h"
#include "mmTextSpannableNative.h"

#include "nwsi/mmTextParagraphFormat.h"
#include "nwsi/mmTextSpannable.h"

#include "core/mmAlloc.h"
#include "dish/mmJavaVMEnv.h"

MM_EXPORT_NWSI
void mmTextParagraph_Init(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    p->pParagraphFormat = NULL;
    p->pTextSpannable = NULL;
    p->pText = NULL;
    p->pDrawingContext = NULL;
    p->jNativePtr = NULL;

    jobject j_NewObject = (*env)->NewObject(env, mmTextParagraph_ObjectClass, mmTextParagraph_Method_NewObject);
    p->jNativePtr = (*env)->NewGlobalRef(env, j_NewObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_Init);
    (*env)->DeleteLocalRef(env, j_NewObject);
}

MM_EXPORT_NWSI
void mmTextParagraph_Destroy(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_Destroy);
    (*env)->DeleteGlobalRef(env, p->jNativePtr);
    p->jNativePtr = NULL;

    p->pParagraphFormat = NULL;
    p->pTextSpannable = NULL;
    p->pText = NULL;
    p->pDrawingContext = NULL;
    p->jNativePtr = NULL;
}

MM_EXPORT_NWSI
void mmTextParagraph_SetTextParagraphFormat(
    struct mmTextParagraph*                        p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_obj = (jobject)(pParagraphFormat->pObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_SetTextParagraphFormat, j_obj);
    p->pParagraphFormat = pParagraphFormat;
	mmTextParagraphFormat_SetNativePtr(env, j_obj, pParagraphFormat);
}

MM_EXPORT_NWSI
void mmTextParagraph_SetTextSpannable(
    struct mmTextParagraph*                        p,
    struct mmTextSpannable*                        pTextSpannable)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_obj = (jobject)(pTextSpannable->pObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_SetTextSpannable, j_obj);
    p->pTextSpannable = pTextSpannable;
	mmTextSpannable_SetNativePtr(env, j_obj, pTextSpannable);
}

MM_EXPORT_NWSI
void mmTextParagraph_SetText(
    struct mmTextParagraph*                        p,
    struct mmTextUtf16String*                      pText)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    jobject j_obj = (jobject)(pText->pObject);
    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_SetText, j_obj);
    p->pText = pText;
}

MM_EXPORT_NWSI
void mmTextParagraph_SetDrawingContext(
    struct mmTextParagraph*                        p,
    struct mmTextDrawingContext*                   pDrawingContext)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_SetDrawingContext, pDrawingContext->jNativePtr);
    p->pDrawingContext = pDrawingContext;
}

MM_EXPORT_NWSI
void mmTextParagraph_OnFinishLaunching(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_OnFinishLaunching);
}

MM_EXPORT_NWSI
void mmTextParagraph_OnBeforeTerminate(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_OnBeforeTerminate);
}

MM_EXPORT_NWSI
void mmTextParagraph_UpdateDrawingContext(
    struct mmTextParagraph*                        p)
{
    
}

MM_EXPORT_NWSI
void mmTextParagraph_UpdateTextImplement(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_UpdateTextImplement);
}

MM_EXPORT_NWSI
void mmTextParagraph_UpdateTextFormat(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_UpdateTextFormat);
}

MM_EXPORT_NWSI
void mmTextParagraph_UpdateTextLayout(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_UpdateTextLayout);
}

MM_EXPORT_NWSI
void mmTextParagraph_UpdateFontFeature(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_UpdateFontFeature);
}

MM_EXPORT_NWSI
void mmTextParagraph_UpdateTextSpannable(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_UpdateTextSpannable);
}

MM_EXPORT_NWSI
void mmTextParagraph_DeleteTextFormat(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_DeleteTextFormat);
}

MM_EXPORT_NWSI
void mmTextParagraph_DeleteTextLayout(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_DeleteTextLayout);
}

MM_EXPORT_NWSI
void mmTextParagraph_Draw(
    struct mmTextParagraph*                        p)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_Draw);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_HitTestPoint(
    const struct mmTextParagraph*                  p, 
    float                                          point[2], 
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    int value[2] = { 0 };

    jfloatArray j_point = (*env)->NewFloatArray(env, 2);
    jintArray j_value = (*env)->NewIntArray(env, 2);
    jobject j_pMetrics = (*env)->NewObject(env, mmTextHitMetrics_ObjectClass, mmTextHitMetrics_Method_NewObject);

    (*env)->SetFloatArrayRegion(env, j_point, 0, 2, point);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_HitTestPoint, j_point, j_value, j_pMetrics);

    (*env)->GetIntArrayRegion(env, j_value, 0, 2, value);

    (*isTrailingHit) = value[0];
    (*isInside) = value[1];

    mmTextHitMetrics_Decode(env, pMetrics, j_pMetrics);

    (*env)->DeleteLocalRef(env, j_pMetrics);
    (*env)->DeleteLocalRef(env, j_value);
    (*env)->DeleteLocalRef(env, j_point);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_HitTestTextPosition(
    const struct mmTextParagraph*                  p, 
    mmUInt32_t                                     offset, 
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics)
{
    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    int value[2];
    jfloatArray j_point = (*env)->NewFloatArray(env, 2);
    jintArray j_value = (*env)->NewIntArray(env, 2);
    jobject j_pMetrics = (*env)->NewObject(env, mmTextHitMetrics_ObjectClass, mmTextHitMetrics_Method_NewObject);

    value[0] = (int)offset;
    value[1] = (int)isTrailingHit;

    (*env)->SetIntArrayRegion(env, j_value, 0, 2, value);

    (*env)->CallVoidMethod(env, p->jNativePtr, mmTextParagraph_Method_HitTestTextPosition, j_value, j_point, j_pMetrics);

    mmTextHitMetrics_Decode(env, pMetrics, j_pMetrics);

    (*env)->GetFloatArrayRegion(env, j_point, 0, 2, point);

    (*env)->DeleteLocalRef(env, j_pMetrics);
    (*env)->DeleteLocalRef(env, j_value);
    (*env)->DeleteLocalRef(env, j_point);
}


MM_EXPORT_NWSI jclass mmTextParagraph_ObjectClass = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_NewObject = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_Init = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_Destroy = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_SetTextParagraphFormat = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_SetTextSpannable = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_SetText = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_SetDrawingContext = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_OnFinishLaunching = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_OnBeforeTerminate = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_UpdateTextImplement = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_UpdateTextFormat = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_UpdateTextLayout = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_UpdateFontFeature = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_UpdateTextSpannable = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_DeleteTextFormat = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_DeleteTextLayout = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_Draw = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_HitTestPoint = NULL;
MM_EXPORT_NWSI jmethodID mmTextParagraph_Method_HitTestTextPosition = NULL;

MM_EXPORT_NWSI
void
mmTextParagraph_AttachVirtualMachine(
    JNIEnv*                                        env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmTextParagraph");
    mmTextParagraph_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);
    
    mmTextParagraph_Method_NewObject = (*env)->GetMethodID(env, object_class, "<init>", "()V");
    mmTextParagraph_Method_Init = (*env)->GetMethodID(env, object_class, "Init", "()V");
    mmTextParagraph_Method_Destroy = (*env)->GetMethodID(env, object_class, "Destroy", "()V");
    mmTextParagraph_Method_SetTextParagraphFormat = (*env)->GetMethodID(env, object_class, "SetTextParagraphFormat", "(Lorg/mm/nwsi/mmTextParagraphFormat;)V");
    mmTextParagraph_Method_SetTextSpannable = (*env)->GetMethodID(env, object_class, "SetTextSpannable", "(Lorg/mm/nwsi/mmTextSpannable;)V");
    mmTextParagraph_Method_SetText = (*env)->GetMethodID(env, object_class, "SetText", "(Lorg/mm/nwsi/mmTextUtf16String;)V");
    mmTextParagraph_Method_SetDrawingContext = (*env)->GetMethodID(env, object_class, "SetDrawingContext", "(Lorg/mm/nwsi/mmTextDrawingContext;)V");
    mmTextParagraph_Method_OnFinishLaunching = (*env)->GetMethodID(env, object_class, "OnFinishLaunching", "()V");
    mmTextParagraph_Method_OnBeforeTerminate = (*env)->GetMethodID(env, object_class, "OnBeforeTerminate", "()V");
    mmTextParagraph_Method_UpdateTextImplement = (*env)->GetMethodID(env, object_class, "UpdateTextImplement", "()V");
    mmTextParagraph_Method_UpdateTextFormat = (*env)->GetMethodID(env, object_class, "UpdateTextFormat", "()V");
    mmTextParagraph_Method_UpdateTextLayout = (*env)->GetMethodID(env, object_class, "UpdateTextLayout", "()V");
    mmTextParagraph_Method_UpdateFontFeature = (*env)->GetMethodID(env, object_class, "UpdateFontFeature", "()V");
    mmTextParagraph_Method_UpdateTextSpannable = (*env)->GetMethodID(env, object_class, "UpdateTextSpannable", "()V");
    mmTextParagraph_Method_DeleteTextFormat = (*env)->GetMethodID(env, object_class, "DeleteTextFormat", "()V");
    mmTextParagraph_Method_DeleteTextLayout = (*env)->GetMethodID(env, object_class, "DeleteTextLayout", "()V");
    mmTextParagraph_Method_Draw = (*env)->GetMethodID(env, object_class, "Draw", "()V");
    mmTextParagraph_Method_HitTestPoint = (*env)->GetMethodID(env, object_class, "HitTestPoint", "([F[ILorg/mm/nwsi/mmTextHitMetrics;)V");
    mmTextParagraph_Method_HitTestTextPosition = (*env)->GetMethodID(env, object_class, "HitTestTextPosition", "([I[FLorg/mm/nwsi/mmTextHitMetrics;)V");

    (*env)->DeleteLocalRef(env, object_class);
}

MM_EXPORT_NWSI
void
mmTextParagraph_DetachVirtualMachine(
    JNIEnv*                                        env)
{
    mmTextParagraph_Method_NewObject = NULL;
    mmTextParagraph_Method_Init = NULL;
    mmTextParagraph_Method_Destroy = NULL;
    mmTextParagraph_Method_SetTextParagraphFormat = NULL;
    mmTextParagraph_Method_SetTextSpannable = NULL;
    mmTextParagraph_Method_SetText = NULL;
    mmTextParagraph_Method_SetDrawingContext = NULL;
    mmTextParagraph_Method_OnFinishLaunching = NULL;
    mmTextParagraph_Method_OnBeforeTerminate = NULL;
    mmTextParagraph_Method_UpdateTextImplement = NULL;
    mmTextParagraph_Method_UpdateTextFormat = NULL;
    mmTextParagraph_Method_UpdateTextLayout = NULL;
    mmTextParagraph_Method_UpdateFontFeature = NULL;
    mmTextParagraph_Method_UpdateTextSpannable = NULL;
    mmTextParagraph_Method_DeleteTextFormat = NULL;
    mmTextParagraph_Method_DeleteTextLayout = NULL;
    mmTextParagraph_Method_Draw = NULL;
    mmTextParagraph_Method_HitTestPoint = NULL;
    mmTextParagraph_Method_HitTestTextPosition = NULL;

    (*env)->DeleteGlobalRef(env, mmTextParagraph_ObjectClass);
    mmTextParagraph_ObjectClass = NULL;
}