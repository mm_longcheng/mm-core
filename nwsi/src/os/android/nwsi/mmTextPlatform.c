/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextPlatform.h"

#include "unicode/mmUBidi.h"

#include "nwsi/mmUTFHelper.h"

#include <jni.h>

MM_EXPORT_NWSI
int
mmTextPlatform_GetTextBaseDirection(
    const mmUInt16_t*                              pWText,
    size_t                                         hLength)
{
    return mmUBidi_GetBaseDirection((const UChar*)pWText, (int32_t)hLength);
}

JNIEXPORT 
jint 
JNICALL 
Java_org_mm_nwsi_mmUBidi_NativeGetBaseDirection(
    JNIEnv*                                        env, 
    jobject                                        obj, 
    jobject                                        text, 
    jint                                           length)
{
    int hBaseDirection = 0;

    jchar* pWText = (*env)->GetCharArrayElements(env, text, NULL);
    
    hBaseDirection = mmTextPlatform_GetTextBaseDirection((const mmUInt16_t*)pWText, (size_t)length);

    (*env)->ReleaseCharArrayElements(env, text, pWText, 0);

    return hBaseDirection;
}

JNIEXPORT 
jint 
JNICALL 
Java_org_mm_nwsi_mmTextPlatform_NativeGetUtf16OffsetLNormalise(
    JNIEnv*                                        env, 
    jobject                                        obj, 
    jobject                                        s, 
    jint                                           z,
    jint                                           o)
{
    int offset = 0;

    jchar* pWText = (*env)->GetCharArrayElements(env, s, NULL);
    
    offset = mmUTF_GetUtf16OffsetLNormalise((const mmUInt16_t*)pWText, (size_t)z, (size_t)o);

    (*env)->ReleaseCharArrayElements(env, s, pWText, 0);

    return offset;
}

JNIEXPORT 
jint 
JNICALL 
Java_org_mm_nwsi_mmTextPlatform_NativeGetUtf16OffsetRNormalise(
    JNIEnv*                                        env, 
    jobject                                        obj, 
    jobject                                        s, 
    jint                                           z,
    jint                                           o)
{
    int offset = 0;

    jchar* pWText = (*env)->GetCharArrayElements(env, s, NULL);
    
    offset = mmUTF_GetUtf16OffsetRNormalise((const mmUInt16_t*)pWText, (size_t)z, (size_t)o);

    (*env)->ReleaseCharArrayElements(env, s, pWText, 0);

    return offset;
}

