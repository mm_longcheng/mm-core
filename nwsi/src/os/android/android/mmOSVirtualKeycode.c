/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOSVirtualKeycode.h"

#include "core/mmKeyCode.h"

#include <android/keycodes.h>

#include <jni.h>

static const mmUInt8_t kVKeycodeMap[512] =
{
    [AKEYCODE_UNKNOWN                       ] = MM_KC_UNKNOWN,
    [AKEYCODE_SOFT_LEFT                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_SOFT_RIGHT                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_HOME                          ] = MM_KC_HOME,
    [AKEYCODE_BACK                          ] = MM_KC_ESCAPE,
    [AKEYCODE_CALL                          ] = MM_KC_UNKNOWN,
    [AKEYCODE_ENDCALL                       ] = MM_KC_UNKNOWN,
    [AKEYCODE_0                             ] = MM_KC_0,          // '0'
    [AKEYCODE_1                             ] = MM_KC_1,          // '1'
    [AKEYCODE_2                             ] = MM_KC_2,          // '2'
    [AKEYCODE_3                             ] = MM_KC_3,          // '3'
    [AKEYCODE_4                             ] = MM_KC_4,          // '4'
    [AKEYCODE_5                             ] = MM_KC_5,          // '5'        
    [AKEYCODE_6                             ] = MM_KC_6,          // '6'
    [AKEYCODE_7                             ] = MM_KC_7,          // '7'         
    [AKEYCODE_8                             ] = MM_KC_8,          // '8'
    [AKEYCODE_9                             ] = MM_KC_9,          // '9'
    [AKEYCODE_STAR                          ] = MM_KC_MULTIPLY,   // '*'
    [AKEYCODE_POUND                         ] = MM_KC_UNKNOWN,    // '#'
    [AKEYCODE_DPAD_UP                       ] = MM_KC_UP,
    [AKEYCODE_DPAD_DOWN                     ] = MM_KC_DOWN,
    [AKEYCODE_DPAD_LEFT                     ] = MM_KC_LEFT,
    [AKEYCODE_DPAD_RIGHT                    ] = MM_KC_RIGHT,
    [AKEYCODE_DPAD_CENTER                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_VOLUME_UP                     ] = MM_KC_VOLUMEUP,
    [AKEYCODE_VOLUME_DOWN                   ] = MM_KC_VOLUMEDOWN,
    [AKEYCODE_POWER                         ] = MM_KC_POWER,
    [AKEYCODE_CAMERA                        ] = MM_KC_UNKNOWN,
    [AKEYCODE_CLEAR                         ] = MM_KC_UNKNOWN,
    [AKEYCODE_A                             ] = MM_KC_A,
    [AKEYCODE_B                             ] = MM_KC_B,
    [AKEYCODE_C                             ] = MM_KC_C,
    [AKEYCODE_D                             ] = MM_KC_D,
    [AKEYCODE_E                             ] = MM_KC_E,
    [AKEYCODE_F                             ] = MM_KC_F,
    [AKEYCODE_G                             ] = MM_KC_G,
    [AKEYCODE_H                             ] = MM_KC_H,
    [AKEYCODE_I                             ] = MM_KC_I,
    [AKEYCODE_J                             ] = MM_KC_J,
    [AKEYCODE_K                             ] = MM_KC_K,
    [AKEYCODE_L                             ] = MM_KC_L,
    [AKEYCODE_M                             ] = MM_KC_M,
    [AKEYCODE_N                             ] = MM_KC_N,
    [AKEYCODE_O                             ] = MM_KC_O,
    [AKEYCODE_P                             ] = MM_KC_P,
    [AKEYCODE_Q                             ] = MM_KC_Q,
    [AKEYCODE_R                             ] = MM_KC_R,
    [AKEYCODE_S                             ] = MM_KC_S,
    [AKEYCODE_T                             ] = MM_KC_T,
    [AKEYCODE_U                             ] = MM_KC_U,
    [AKEYCODE_V                             ] = MM_KC_V,
    [AKEYCODE_W                             ] = MM_KC_W,
    [AKEYCODE_X                             ] = MM_KC_X,
    [AKEYCODE_Y                             ] = MM_KC_Y,
    [AKEYCODE_Z                             ] = MM_KC_Z,
    [AKEYCODE_COMMA                         ] = MM_KC_COMMA,      // ','
    [AKEYCODE_PERIOD                        ] = MM_KC_PERIOD,     // '.'
    [AKEYCODE_ALT_LEFT                      ] = MM_KC_LMENU,
    [AKEYCODE_ALT_RIGHT                     ] = MM_KC_RMENU,
    [AKEYCODE_SHIFT_LEFT                    ] = MM_KC_LSHIFT,
    [AKEYCODE_SHIFT_RIGHT                   ] = MM_KC_RSHIFT,
    [AKEYCODE_TAB                           ] = MM_KC_TAB,
    [AKEYCODE_SPACE                         ] = MM_KC_SPACE,
    [AKEYCODE_SYM                           ] = MM_KC_UNKNOWN,
    [AKEYCODE_EXPLORER                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_ENVELOPE                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_ENTER                         ] = MM_KC_RETURN,
    [AKEYCODE_DEL                           ] = MM_KC_BACKSPACE,
    [AKEYCODE_GRAVE                         ] = MM_KC_GRAVE,
    [AKEYCODE_MINUS                         ] = MM_KC_MINUS,
    [AKEYCODE_EQUALS                        ] = MM_KC_EQUALS,
    [AKEYCODE_LEFT_BRACKET                  ] = MM_KC_LBRACKET,
    [AKEYCODE_RIGHT_BRACKET                 ] = MM_KC_RBRACKET,
    [AKEYCODE_BACKSLASH                     ] = MM_KC_BACKSLASH,
    [AKEYCODE_SEMICOLON                     ] = MM_KC_SEMICOLON,
    [AKEYCODE_APOSTROPHE                    ] = MM_KC_APOSTROPHE,
    [AKEYCODE_SLASH                         ] = MM_KC_SLASH,
    [AKEYCODE_AT                            ] = MM_KC_AT,
    [AKEYCODE_NUM                           ] = MM_KC_UNKNOWN,
    [AKEYCODE_HEADSETHOOK                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_FOCUS                         ] = MM_KC_UNKNOWN,
    [AKEYCODE_PLUS                          ] = MM_KC_ADD,
    [AKEYCODE_MENU                          ] = MM_KC_UNKNOWN,
    [AKEYCODE_NOTIFICATION                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_SEARCH                        ] = MM_KC_WEBSEARCH,
    [AKEYCODE_MEDIA_PLAY_PAUSE              ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_STOP                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_NEXT                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_PREVIOUS                ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_REWIND                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_FAST_FORWARD            ] = MM_KC_UNKNOWN,
    [AKEYCODE_MUTE                          ] = MM_KC_MUTE,
    [AKEYCODE_PAGE_UP                       ] = MM_KC_PGUP,
    [AKEYCODE_PAGE_DOWN                     ] = MM_KC_PGDN,
    [AKEYCODE_PICTSYMBOLS                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_SWITCH_CHARSET                ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_A                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_B                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_C                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_X                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_Y                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_Z                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_L1                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_R1                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_L2                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_R2                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_THUMBL                 ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_THUMBR                 ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_START                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_SELECT                 ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_MODE                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_ESCAPE                        ] = MM_KC_ESCAPE,
    [AKEYCODE_FORWARD_DEL                   ] = MM_KC_DELETE,
    [AKEYCODE_CTRL_LEFT                     ] = MM_KC_LCONTROL,
    [AKEYCODE_CTRL_RIGHT                    ] = MM_KC_RCONTROL,
    [AKEYCODE_CAPS_LOCK                     ] = MM_KC_CAPSLOCK,
    [AKEYCODE_SCROLL_LOCK                   ] = MM_KC_SCROLL,
    [AKEYCODE_META_LEFT                     ] = MM_KC_LWIN,
    [AKEYCODE_META_RIGHT                    ] = MM_KC_RWIN,
    [AKEYCODE_FUNCTION                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_SYSRQ                         ] = MM_KC_SYSRQ,
    [AKEYCODE_BREAK                         ] = MM_KC_PAUSE,
    [AKEYCODE_MOVE_HOME                     ] = MM_KC_HOME,
    [AKEYCODE_MOVE_END                      ] = MM_KC_END,
    [AKEYCODE_INSERT                        ] = MM_KC_INSERT,
    [AKEYCODE_FORWARD                       ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_PLAY                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_PAUSE                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_CLOSE                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_EJECT                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_RECORD                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_F1                            ] = MM_KC_F1,
    [AKEYCODE_F2                            ] = MM_KC_F2,
    [AKEYCODE_F3                            ] = MM_KC_F3,
    [AKEYCODE_F4                            ] = MM_KC_F4,
    [AKEYCODE_F5                            ] = MM_KC_F5,
    [AKEYCODE_F6                            ] = MM_KC_F6,
    [AKEYCODE_F7                            ] = MM_KC_F7,
    [AKEYCODE_F8                            ] = MM_KC_F8,
    [AKEYCODE_F9                            ] = MM_KC_F9,
    [AKEYCODE_F10                           ] = MM_KC_F10,
    [AKEYCODE_F11                           ] = MM_KC_F11,
    [AKEYCODE_F12                           ] = MM_KC_F12,
    [AKEYCODE_NUM_LOCK                      ] = MM_KC_NUMLOCK,
    [AKEYCODE_NUMPAD_0                      ] = MM_KC_NUMPAD0,
    [AKEYCODE_NUMPAD_1                      ] = MM_KC_NUMPAD1,
    [AKEYCODE_NUMPAD_2                      ] = MM_KC_NUMPAD2,
    [AKEYCODE_NUMPAD_3                      ] = MM_KC_NUMPAD3,
    [AKEYCODE_NUMPAD_4                      ] = MM_KC_NUMPAD4,
    [AKEYCODE_NUMPAD_5                      ] = MM_KC_NUMPAD5,
    [AKEYCODE_NUMPAD_6                      ] = MM_KC_NUMPAD6,
    [AKEYCODE_NUMPAD_7                      ] = MM_KC_NUMPAD7,
    [AKEYCODE_NUMPAD_8                      ] = MM_KC_NUMPAD8,
    [AKEYCODE_NUMPAD_9                      ] = MM_KC_NUMPAD9,
    [AKEYCODE_NUMPAD_DIVIDE                 ] = MM_KC_NUMPADSLASH,  // '/'
    [AKEYCODE_NUMPAD_MULTIPLY               ] = MM_KC_NUMPADSTAR,   // '*'
    [AKEYCODE_NUMPAD_SUBTRACT               ] = MM_KC_NUMPADMINUS,  // '-'
    [AKEYCODE_NUMPAD_ADD                    ] = MM_KC_NUMPADPLUS,   // '+'
    [AKEYCODE_NUMPAD_DOT                    ] = MM_KC_NUMPADPERIOD, // '.'
    [AKEYCODE_NUMPAD_COMMA                  ] = MM_KC_NUMPADCOMMA,  // ','
    [AKEYCODE_NUMPAD_ENTER                  ] = MM_KC_NUMPADENTER,
    [AKEYCODE_NUMPAD_EQUALS                 ] = MM_KC_NUMPADEQUALS, // '='
    [AKEYCODE_NUMPAD_LEFT_PAREN             ] = MM_KC_UNKNOWN,      // '('
    [AKEYCODE_NUMPAD_RIGHT_PAREN            ] = MM_KC_UNKNOWN,      // ')'
    [AKEYCODE_VOLUME_MUTE                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_INFO                          ] = MM_KC_UNKNOWN,
    [AKEYCODE_CHANNEL_UP                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_CHANNEL_DOWN                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_ZOOM_IN                       ] = MM_KC_UNKNOWN,
    [AKEYCODE_ZOOM_OUT                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV                            ] = MM_KC_UNKNOWN,
    [AKEYCODE_WINDOW                        ] = MM_KC_UNKNOWN,
    [AKEYCODE_GUIDE                         ] = MM_KC_UNKNOWN,
    [AKEYCODE_DVR                           ] = MM_KC_UNKNOWN,
    [AKEYCODE_BOOKMARK                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_CAPTIONS                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_SETTINGS                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_POWER                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_STB_POWER                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_STB_INPUT                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_AVR_POWER                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_AVR_INPUT                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_PROG_RED                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_PROG_GREEN                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_PROG_YELLOW                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_PROG_BLUE                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_APP_SWITCH                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_1                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_2                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_3                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_4                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_5                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_6                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_7                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_8                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_9                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_10                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_11                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_12                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_13                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_14                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_15                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_BUTTON_16                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_LANGUAGE_SWITCH               ] = MM_KC_UNKNOWN,
    [AKEYCODE_MANNER_MODE                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_3D_MODE                       ] = MM_KC_UNKNOWN,
            
    // ICE_CREAM_SANDWICH_MR1
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    [AKEYCODE_CONTACTS                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_CALENDAR                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_MUSIC                         ] = MM_KC_UNKNOWN,
    [AKEYCODE_CALCULATOR                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_ZENKAKU_HANKAKU               ] = MM_KC_UNKNOWN,
            
    // JELLY_BEAN
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
    [AKEYCODE_EISU                          ] = MM_KC_UNKNOWN,
    [AKEYCODE_MUHENKAN                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_KATAKANA_HIRAGANA             ] = MM_KC_UNKNOWN,
    [AKEYCODE_YEN                           ] = MM_KC_UNKNOWN,
    [AKEYCODE_RO                            ] = MM_KC_UNKNOWN,
    [AKEYCODE_KANA                          ] = MM_KC_UNKNOWN,
    [AKEYCODE_ASSIST                        ] = MM_KC_UNKNOWN,
            
    // JELLY_BEAN_MR2
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
    [AKEYCODE_BRIGHTNESS_DOWN               ] = MM_KC_UNKNOWN,
    [AKEYCODE_BRIGHTNESS_UP                 ] = MM_KC_UNKNOWN,

    // KITKAT
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
    [AKEYCODE_MEDIA_AUDIO_TRACK             ] = MM_KC_UNKNOWN,

    // KITKAT_WATCH
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH)
    [AKEYCODE_SLEEP                         ] = MM_KC_SLEEP,
    [AKEYCODE_WAKEUP                        ] = MM_KC_WAKE,
            
    // L
    // (Build.VERSION.SDK_INT >= 21)
    [AKEYCODE_PAIRING                       ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_TOP_MENU                ] = MM_KC_UNKNOWN,
    [AKEYCODE_11                            ] = MM_KC_UNKNOWN,
    [AKEYCODE_12                            ] = MM_KC_UNKNOWN,
    [AKEYCODE_LAST_CHANNEL                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_DATA_SERVICE               ] = MM_KC_UNKNOWN,
    [AKEYCODE_VOICE_ASSIST                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_RADIO_SERVICE              ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_TELETEXT                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_NUMBER_ENTRY               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_TERRESTRIAL_ANALOG         ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_TERRESTRIAL_DIGITAL        ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_SATELLITE                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_SATELLITE_BS               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_SATELLITE_CS               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_SATELLITE_SERVICE          ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_NETWORK                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_ANTENNA_CABLE              ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_HDMI_1               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_HDMI_2               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_HDMI_3               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_HDMI_4               ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_COMPOSITE_1          ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_COMPOSITE_2          ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_COMPONENT_1          ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_COMPONENT_2          ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_INPUT_VGA_1                ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_AUDIO_DESCRIPTION          ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_AUDIO_DESCRIPTION_MIX_UP   ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_AUDIO_DESCRIPTION_MIX_DOWN ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_ZOOM_MODE                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_CONTENTS_MENU              ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_MEDIA_CONTEXT_MENU         ] = MM_KC_UNKNOWN,
    [AKEYCODE_TV_TIMER_PROGRAMMING          ] = MM_KC_UNKNOWN,
    [AKEYCODE_HELP                          ] = MM_KC_UNKNOWN,
            
    // M
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
    [AKEYCODE_NAVIGATE_PREVIOUS             ] = MM_KC_UNKNOWN,
    [AKEYCODE_NAVIGATE_NEXT                 ] = MM_KC_UNKNOWN,
    [AKEYCODE_NAVIGATE_IN                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_NAVIGATE_OUT                  ] = MM_KC_UNKNOWN,

    // N
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
    [AKEYCODE_STEM_PRIMARY                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_STEM_1                        ] = MM_KC_UNKNOWN,
    [AKEYCODE_STEM_2                        ] = MM_KC_UNKNOWN,
    [AKEYCODE_STEM_3                        ] = MM_KC_UNKNOWN,
    [AKEYCODE_DPAD_UP_LEFT                  ] = MM_KC_UNKNOWN,
    [AKEYCODE_DPAD_DOWN_LEFT                ] = MM_KC_UNKNOWN,
    [AKEYCODE_DPAD_UP_RIGHT                 ] = MM_KC_UNKNOWN,
    [AKEYCODE_DPAD_DOWN_RIGHT               ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_SKIP_FORWARD            ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_SKIP_BACKWARD           ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_STEP_FORWARD            ] = MM_KC_UNKNOWN,
    [AKEYCODE_MEDIA_STEP_BACKWARD           ] = MM_KC_UNKNOWN,
    [AKEYCODE_SOFT_SLEEP                    ] = MM_KC_UNKNOWN,
    [AKEYCODE_CUT                           ] = MM_KC_UNKNOWN,
    [AKEYCODE_COPY                          ] = MM_KC_UNKNOWN,
    [AKEYCODE_PASTE                         ] = MM_KC_UNKNOWN,

    // N_MR1
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1)
    [AKEYCODE_SYSTEM_NAVIGATION_UP          ] = MM_KC_UNKNOWN,
    [AKEYCODE_SYSTEM_NAVIGATION_DOWN        ] = MM_KC_UNKNOWN,
    [AKEYCODE_SYSTEM_NAVIGATION_LEFT        ] = MM_KC_UNKNOWN,
    [AKEYCODE_SYSTEM_NAVIGATION_RIGHT       ] = MM_KC_UNKNOWN,

    // P
    // (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
    [AKEYCODE_ALL_APPS                      ] = MM_KC_UNKNOWN,
    [AKEYCODE_REFRESH                       ] = MM_KC_UNKNOWN,

    [AKEYCODE_THUMBS_UP                     ] = MM_KC_UNKNOWN,
    [AKEYCODE_THUMBS_DOWN                   ] = MM_KC_UNKNOWN,
    [AKEYCODE_PROFILE_SWITCH                ] = MM_KC_UNKNOWN,
};

MM_EXPORT_NWSI int mmOSVirtualKeycodeTranslate(int keycode)
{
    if (0 <= keycode && keycode < 512)
    {
        return kVKeycodeMap[keycode];
    }
    else
    {
        return MM_KC_UNKNOWN;
    }
}

JNIEXPORT jint JNICALL Java_org_mm_nwsi_mmKeyCode_NativeOSVirtualKeycodeTranslate(JNIEnv* env, jobject obj, jint keycode)
{
    return mmOSVirtualKeycodeTranslate(keycode);
}