/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "core/mmOSContext.h"
#include "core/mmAlloc.h"
#include "core/mmASCIICode.h"
#include "core/mmKeyCode.h"
#include "core/mmUtf16String.h"

#include "dish/mmJavaVMEnv.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmJavaNativeAndroid.h"
#include "nwsi/mmUTFHelper.h"
#include "nwsi/mmISurfaceNative.h"

#include "mmUIViewSurfaceMaster.h"

#include <android/configuration.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <android/native_activity.h>
#include <android/native_window_jni.h>

static
void
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster);

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(struct mmUIViewSurfaceMaster* p, int hSafeArea[4]);
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(struct mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadEnter(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadLeave(struct mmUIViewSurfaceMaster* p);

static
void
__static_mmUIViewSurfaceMaster_OnUpdateMask(
    struct mmUIViewSurfaceMaster*                  p,
    int                                            phase,
    mmUInt32_t                                     button_id);

static void __static_mmUIViewSurfaceMaster_OnTouchesDataHandler(struct mmUIViewSurfaceMaster* p, JNIEnv* env, jobject pContent, mmUInt32_t button_id);

static void __static_mmUIViewSurfaceMaster_TextDeleteSurrounding(struct mmUIViewSurfaceMaster* p, int hLLength, int hRLength);

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pISurface, int hType);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pISurface, int hAppearance);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pISurface, int hReturnKey);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pISurface);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pISurface);

MM_EXPORT_NWSI jclass mmUIViewSurfaceMaster_ObjectClass = NULL;
MM_EXPORT_NWSI jfieldID mmUIViewSurfaceMaster_Field_hNativePtr = NULL;
MM_EXPORT_NWSI jmethodID mmUIViewSurfaceMaster_Method_OnSetKeypadType = NULL;
MM_EXPORT_NWSI jmethodID mmUIViewSurfaceMaster_Method_OnSetKeypadAppearance = NULL;
MM_EXPORT_NWSI jmethodID mmUIViewSurfaceMaster_Method_OnSetKeypadReturnKey = NULL;
MM_EXPORT_NWSI jmethodID mmUIViewSurfaceMaster_Method_OnKeypadStatusShow = NULL;
MM_EXPORT_NWSI jmethodID mmUIViewSurfaceMaster_Method_OnKeypadStatusHide = NULL;

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_AttachVirtualMachine(JNIEnv* env)
{
    jclass object_class = (*env)->FindClass(env, "org/mm/nwsi/mmUIViewSurfaceMaster");
    mmUIViewSurfaceMaster_ObjectClass = (jclass)(*env)->NewGlobalRef(env, object_class);

    mmUIViewSurfaceMaster_Field_hNativePtr = (*env)->GetFieldID(env, object_class, "hNativePtr", "J");
    mmUIViewSurfaceMaster_Method_OnSetKeypadType = (*env)->GetMethodID(env, object_class, "OnSetKeypadType", "(I)V");
    mmUIViewSurfaceMaster_Method_OnSetKeypadAppearance = (*env)->GetMethodID(env, object_class, "OnSetKeypadAppearance", "(I)V");
    mmUIViewSurfaceMaster_Method_OnSetKeypadReturnKey = (*env)->GetMethodID(env, object_class, "OnSetKeypadReturnKey", "(I)V");
    mmUIViewSurfaceMaster_Method_OnKeypadStatusShow = (*env)->GetMethodID(env, object_class, "OnKeypadStatusShow", "()V");
    mmUIViewSurfaceMaster_Method_OnKeypadStatusHide = (*env)->GetMethodID(env, object_class, "OnKeypadStatusHide", "()V");

    (*env)->DeleteLocalRef(env, object_class);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_DetachVirtualMachine(JNIEnv* env)
{
    (*env)->DeleteGlobalRef(env, mmUIViewSurfaceMaster_ObjectClass);
    mmUIViewSurfaceMaster_ObjectClass = NULL;

    mmUIViewSurfaceMaster_Field_hNativePtr = NULL;
    mmUIViewSurfaceMaster_Method_OnSetKeypadType = NULL;
    mmUIViewSurfaceMaster_Method_OnSetKeypadAppearance = NULL;
    mmUIViewSurfaceMaster_Method_OnSetKeypadReturnKey = NULL;
    mmUIViewSurfaceMaster_Method_OnKeypadStatusShow = NULL;
    mmUIViewSurfaceMaster_Method_OnKeypadStatusHide = NULL;
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Init(struct mmUIViewSurfaceMaster* p)
{
    mmEventSurfaceContent_Init(&p->hSurfaceContent);
    mmISurfaceKeypad_Init(&p->hISurfaceKeypad);

    mmUtf16String_Init(&p->hComposingText);

    p->pSurfaceMaster = NULL;
    p->pISurface = NULL;

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;
    p->jSurface = NULL;

    p->hModifierMask = 0;
    p->hButtonMask = 0;

    p->hDisplayDensity = 1.0;

    mmJavaNative_JObjectInit(&p->jSurface);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Destroy(struct mmUIViewSurfaceMaster* p)
{
    mmJavaNative_JObjectDestroy(&p->jSurface);

    p->hDisplayDensity = 1.0;
	
    p->hButtonMask = 0;
    p->hModifierMask = 0;

    p->jSurface = NULL;
    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    p->pISurface = NULL;
    p->pSurfaceMaster = NULL;

    mmUtf16String_Destroy(&p->hComposingText);

    mmISurfaceKeypad_Destroy(&p->hISurfaceKeypad);
    mmEventSurfaceContent_Destroy(&p->hSurfaceContent);
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetSurfaceMaster(struct mmUIViewSurfaceMaster* p, struct mmSurfaceMaster* pSurfaceMaster)
{
    void* pViewSurfaceMaster = (void*)p;

    p->pSurfaceMaster = pSurfaceMaster;
    p->pISurface = &p->pSurfaceMaster->hSuper;

    mmSurfaceMaster_SetViewSurface(pSurfaceMaster, pViewSurfaceMaster);
    mmSurfaceMaster_SetISurfaceKeypad(pSurfaceMaster, &p->hISurfaceKeypad);
}

static 
void 
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    pMetrics->surface = surface;
    pMetrics->canvas_size[0] = pSurfaceMaster->hDisplayFrameSize[0];
    pMetrics->canvas_size[1] = pSurfaceMaster->hDisplayFrameSize[1];
    pMetrics->window_size[0] = pSurfaceMaster->hPhysicalViewSize[0];
    pMetrics->window_size[1] = pSurfaceMaster->hPhysicalViewSize[1];
    pMetrics->safety_area[0] = pSurfaceMaster->hPhysicalSafeRect[0];
    pMetrics->safety_area[1] = pSurfaceMaster->hPhysicalSafeRect[1];
    pMetrics->safety_area[2] = pSurfaceMaster->hPhysicalSafeRect[2];
    pMetrics->safety_area[3] = pSurfaceMaster->hPhysicalSafeRect[3];
}

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(struct mmUIViewSurfaceMaster* p, int hSafeArea[4])
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

        struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

        jobject jAssetManager = NULL;
        jobject jSurface = NULL;
        AAssetManager* pAAssetManager = NULL;
        ANativeWindow* pANativeWindow = NULL;

        double hDisplayDensity = 0.0;

        const char* pWindowName = "";
        unsigned int w = 0;
        unsigned int h = 0;

        double hDisplayFrameSize[2];
        double hPhysicalSafeRect[4];

        struct mmEventMetrics* pMetrics = NULL;

        if (NULL == p->pSurfaceMaster)
        {
            mmLogger_LogI(gLogger, "%s %d pSurfaceMaster is null.", __FUNCTION__, __LINE__);
            break;
        }

        jAssetManager = (jobject)(pPackageAssets->pAssetManager);
        if(NULL == jAssetManager)
        {
            mmLogger_LogI(gLogger, "%s %d jAssetManager is null.", __FUNCTION__, __LINE__);
            break;
        }

        jSurface = (jobject)(p->jSurface);
        if(NULL == jSurface)
        {
            mmLogger_LogI(gLogger, "%s %d jSurface is null.", __FUNCTION__, __LINE__);
            break;
        }

        pAAssetManager = AAssetManager_fromJava(env, jAssetManager);
        if(NULL == pAAssetManager)
        {
            mmLogger_LogI(gLogger, "%s %d pAAssetManager is null.", __FUNCTION__, __LINE__);
            break;
        }

        pANativeWindow = ANativeWindow_fromSurface(env, jSurface);
        if(NULL == pANativeWindow)
        {
            mmLogger_LogI(gLogger, "%s %d pANativeWindow is null.", __FUNCTION__, __LINE__);
            break;
        }

        pMetrics = &p->hSurfaceContent.hMetrics;

        hDisplayDensity = p->hDisplayDensity;

        pWindowName = mmString_CStr(&pPackageAssets->hPackageName);
        w = (unsigned int)ANativeWindow_getWidth(pANativeWindow);
        h = (unsigned int)ANativeWindow_getHeight(pANativeWindow);

        hDisplayFrameSize[0] = (double)w;
        hDisplayFrameSize[1] = (double)h;

        // Safe rect.
        hPhysicalSafeRect[0] = (double)((double)hSafeArea[0] / hDisplayDensity);
        hPhysicalSafeRect[1] = (double)((double)hSafeArea[1] / hDisplayDensity);
        hPhysicalSafeRect[2] = (double)((double)hSafeArea[2] / hDisplayDensity);
        hPhysicalSafeRect[3] = (double)((double)hSafeArea[3] / hDisplayDensity);

        mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
        mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
        mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);
    
        // Logger infomation.
        mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.PrepareSwapchain");
        mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_INFO, pWindowName);

        // OnSurfacePrepare event.
        __static_mmSurfaceContentMetricsAssignment(pMetrics, (void*)p->jSurface, p->pSurfaceMaster);
        (*(p->pISurface->OnSurfacePrepare))(p->pISurface, pMetrics);

    }while(0);
}
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(struct mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;
    __static_mmSurfaceContentMetricsAssignment(pMetrics, (void*)p->jSurface, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceDiscard))(p->pISurface, pMetrics);

    mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.DiscardSwapchain");
}

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(struct mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_ThreadEnter(struct mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_ThreadLeave(struct mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}

static
void
__static_mmUIViewSurfaceMaster_OnUpdateMask(
    struct mmUIViewSurfaceMaster*                  p,
    int                                            phase,
    mmUInt32_t                                     button_id)
{
	p->hModifierMask = 0;

    if (MM_MB_UNKNOWN != button_id)
    {
        switch (phase)
        {
        case mmTouchBegan:
            p->hButtonMask |=  (1 << button_id);
            break;
        case mmTouchEnded:
        case mmTouchBreak:
            p->hButtonMask &= ~(1 << button_id);
            break;
        default:
            break;
        }
    }
}

static void __static_mmUIViewSurfaceMaster_OnTouchesDataHandler(struct mmUIViewSurfaceMaster* p, JNIEnv* env, jobject pContent, mmUInt32_t button_id)
{
    struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;

    mmEventTouchs_Decode(env, pTouchs, pContent);


    if(0 < pTouchs->context.size)
    {
        struct mmEventTouch* t = NULL;
        t = mmEventTouchs_GetForIndexReference(pTouchs, 0);
		
		__static_mmUIViewSurfaceMaster_OnUpdateMask(p, t->phase, button_id);
		
		p->hModifierMask = t->modifier_mask;

        pCursor->modifier_mask = (int)p->hModifierMask;
        pCursor->button_mask = (int)p->hButtonMask;
        pCursor->button_id = (int)button_id;
        pCursor->abs_x = (double)t->abs_x;
        pCursor->abs_y = (double)t->abs_y;
        pCursor->rel_x = (double)t->rel_x;
        pCursor->rel_y = (double)t->rel_y;
        pCursor->wheel_dx = (double)0.0;
        pCursor->wheel_dx = (double)0.0;
        pCursor->timestamp = (mmUInt64_t)t->timestamp;
    }
}

static void __static_mmUIViewSurfaceMaster_TextDeleteSurrounding(struct mmUIViewSurfaceMaster* p, int hLLength, int hRLength)
{
    size_t hRangeSize = 0;
    size_t hNewUTF16Size = 0;
    size_t hTextSize = 0;
    size_t hLPos = 0;
    size_t hRPos = 0;

    mmUInt16_t* pArrays = NULL;
    mmUInt16_t* pDstBuffer = NULL;
    mmUInt16_t* pSrcBuffer = NULL;

    size_t hLength = 0;

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    hTextSize = pEditText->size;
    hLPos = pEditText->l;
    hRPos = pEditText->r;

    hRangeSize = hRPos - hLPos;
    hNewUTF16Size = hTextSize - hLLength - hRLength;

    pEditText->capacity = hNewUTF16Size;
    pEditText->v = mmEditTextCsize;
    (*(p->pISurface->OnSetText))(p->pISurface, pEditText);

    pArrays = pEditText->t;

    // Note: the mmUInt16_t + i, buffer offset unit is 4.
    
    hLength = (hTextSize - (hRPos + hRLength)) * sizeof(mmUInt16_t);
    pDstBuffer = pArrays + (hRPos);
    pSrcBuffer = pArrays + (hRPos + hRLength);
    mmMemmove(pDstBuffer, pSrcBuffer, hLength);

    hLength = (hTextSize - (hLPos + hRLength)) * sizeof(mmUInt16_t);
    pDstBuffer = pArrays + (hLPos - hLLength);
    pSrcBuffer = pArrays + (hLPos);
    mmMemmove(pDstBuffer, pSrcBuffer, hLength);

    pEditText->l -= hLLength;
    pEditText->r -= hLLength;
    pEditText->c -= hLLength;
    pEditText->size = hNewUTF16Size;
    pEditText->v = mmEditTextRange | mmEditTextCaret | mmEditTextRsize | mmEditTextDirty;

    (*(p->pISurface->OnSetText))(p->pISurface, pEditText);
}

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pISurface, int hType)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmUIViewSurfaceMaster_Method_OnSetKeypadType, hType);
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pISurface, int hAppearance)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmUIViewSurfaceMaster_Method_OnSetKeypadAppearance, hAppearance);
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pISurface, int hReturnKey)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmUIViewSurfaceMaster_Method_OnSetKeypadReturnKey, hReturnKey);
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pISurface)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmUIViewSurfaceMaster_Method_OnKeypadStatusShow);
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pISurface)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    JNIEnv* env = mmJavaVMEnvCurrentThreadJNIEnv();

    (*env)->CallVoidMethod(env, p->jNativePtr, mmUIViewSurfaceMaster_Method_OnKeypadStatusHide);
}

// java -> c
#include "core/mmPrefix.h"

MM_EXPORT_NWSI struct mmUIViewSurfaceMaster* mmUIViewSurfaceMaster_GetNativePtr(JNIEnv* env, jobject obj)
{
    jlong j_hNativePtr = (*env)->GetLongField(env, obj, mmUIViewSurfaceMaster_Field_hNativePtr);
    return (struct mmUIViewSurfaceMaster*)j_hNativePtr;
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetNativePtr(JNIEnv* env, jobject obj, struct mmUIViewSurfaceMaster* p)
{
    jlong j_hNativePtr = (jlong)p;
    (*env)->SetLongField(env, obj, mmUIViewSurfaceMaster_Field_hNativePtr, j_hNativePtr);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeAlloc(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)mmMalloc(sizeof(struct mmUIViewSurfaceMaster));
    p->jNativePtr = (jobject)(*env)->NewGlobalRef(env, obj);
    mmUIViewSurfaceMaster_SetNativePtr(env, obj, p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeDealloc(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    mmUIViewSurfaceMaster_SetNativePtr(env, obj, NULL);
    (*env)->DeleteGlobalRef(env, p->jNativePtr);
    mmFree(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeInit(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    mmUIViewSurfaceMaster_Init(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeDestroy(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    mmUIViewSurfaceMaster_Destroy(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeThreadPeekMessage(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    __static_mmUIViewSurfaceMaster_ThreadPeekMessage(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeThreadEnter(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    __static_mmUIViewSurfaceMaster_ThreadEnter(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeThreadLeave(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    __static_mmUIViewSurfaceMaster_ThreadLeave(p);
}


JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnUpdate(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnUpdate))(p->pISurface);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnTimewait(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnTimewait))(p->pISurface);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnTimewake(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnTimewake))(p->pISurface);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnStart(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnStart))(p->pISurface);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnInterrupt(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnInterrupt))(p->pISurface);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnShutdown(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnShutdown))(p->pISurface);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnJoin(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnJoin))(p->pISurface);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSurfaceComplete(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    (*(p->pISurface->OnFinishLaunching))(p->pISurface);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnFinishLaunching(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    mmISurfaceKeypad_Reset(&p->hISurfaceKeypad);
    p->hISurfaceKeypad.OnSetKeypadType = &__static_mmUIViewSurfaceMaster_OnSetKeypadType;
    p->hISurfaceKeypad.OnSetKeypadAppearance = &__static_mmUIViewSurfaceMaster_OnSetKeypadAppearance;
    p->hISurfaceKeypad.OnSetKeypadReturnKey = &__static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey;
    p->hISurfaceKeypad.OnKeypadStatusShow = &__static_mmUIViewSurfaceMaster_OnKeypadStatusShow;
    p->hISurfaceKeypad.OnKeypadStatusHide = &__static_mmUIViewSurfaceMaster_OnKeypadStatusHide;
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnBeforeTerminate(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    mmISurfaceKeypad_Reset(&p->hISurfaceKeypad);

    (*(p->pISurface->OnBeforeTerminate))(p->pISurface);

    __static_mmUIViewSurfaceMaster_DiscardSwapchain(p);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnEnterBackground(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnEnterBackground))(p->pISurface);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnEnterForeground(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    (*(p->pISurface->OnEnterForeground))(p->pISurface);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeSetSurfaceMaster(JNIEnv* env, jobject obj, jlong hSurfaceMaster)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    mmUIViewSurfaceMaster_SetSurfaceMaster(p, (struct mmSurfaceMaster*)hSurfaceMaster);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeSetDisplayDensity(JNIEnv* env, jobject obj, jdouble hDisplayDensity)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    p->hDisplayDensity = hDisplayDensity;
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnKeypadPressed(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

    mmEventKeypad_Decode(env, pKeypad, pContent);

    (*(p->pISurface->OnKeypadPressed))(p->pISurface, pKeypad);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnKeypadRelease(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

    mmEventKeypad_Decode(env, pKeypad, pContent);

    (*(p->pISurface->OnKeypadRelease))(p->pISurface, pKeypad);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnKeypadStatus(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventKeypadStatus* pKeypadStatus = &p->hSurfaceContent.hKeypadStatus;

    mmEventKeypadStatus_Decode(env, pKeypadStatus, pContent);

    (*(p->pISurface->OnKeypadStatus))(p->pISurface, pKeypadStatus);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnTouchsBegan(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;

    __static_mmUIViewSurfaceMaster_OnTouchesDataHandler(p, env, pContent, MM_MB_LBUTTON);

    (*(p->pISurface->OnTouchsBegan))(p->pISurface, pTouchs);
    (*(p->pISurface->OnCursorBegan))(p->pISurface, pCursor);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnTouchsMoved(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;

    __static_mmUIViewSurfaceMaster_OnTouchesDataHandler(p, env, pContent, MM_MB_UNKNOWN);

    (*(p->pISurface->OnTouchsMoved))(p->pISurface, pTouchs);
    (*(p->pISurface->OnCursorMoved))(p->pISurface, pCursor);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnTouchsEnded(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;

    __static_mmUIViewSurfaceMaster_OnTouchesDataHandler(p, env, pContent, MM_MB_LBUTTON);

    (*(p->pISurface->OnTouchsEnded))(p->pISurface, pTouchs);
    (*(p->pISurface->OnCursorEnded))(p->pISurface, pCursor);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnTouchsBreak(JNIEnv* env, jobject obj, jobject pContent)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;

    __static_mmUIViewSurfaceMaster_OnTouchesDataHandler(p, env, pContent, MM_MB_LBUTTON);

    (*(p->pISurface->OnTouchsBreak))(p->pISurface, pTouchs);
    (*(p->pISurface->OnCursorBreak))(p->pISurface, pCursor);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeSetSurface(JNIEnv* env, jobject obj, jobject pSurface)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    mmJavaNative_DeleteGlobalRef(&p->jSurface, env);
    mmJavaNative_CreateGlobalRef(&p->jSurface, env, pSurface);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSurfaceCreated(JNIEnv* env, jobject obj, jobject hSafeArea)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    int c_hSafeArea[4];

    jintArray j_hSafeArea = (jintArray)(hSafeArea);
    (*env)->GetIntArrayRegion(env, j_hSafeArea, 0, 4, c_hSafeArea);

    __static_mmUIViewSurfaceMaster_PrepareSwapchain(p, c_hSafeArea);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSurfaceDestroyed(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    __static_mmUIViewSurfaceMaster_DiscardSwapchain(p);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSurfaceChanged(JNIEnv* env, jobject obj, jobject hSafeArea, jobject hViewSize)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

    // Note: window_size is DisplayFrameSize.
    // We temporarily use unpacked data.
    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;

    double hDisplayFrameSize[2];
    double hPhysicalSafeRect[4];

    const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);

    double hDisplayDensity = p->hDisplayDensity;

    int c_hViewSize[2];
    int c_hSafeArea[4];

    jintArray j_hViewSize = (jintArray)(hViewSize);
    jintArray j_hSafeArea = (jintArray)(hSafeArea);

    (*env)->GetIntArrayRegion(env, j_hViewSize, 0, 2, c_hViewSize);
    (*env)->GetIntArrayRegion(env, j_hSafeArea, 0, 4, c_hSafeArea);

    // Display frame size.
    hDisplayFrameSize[0] = (double)c_hViewSize[0];
    hDisplayFrameSize[1] = (double)c_hViewSize[1];

    // Safe rect.
    hPhysicalSafeRect[0] = (double)c_hSafeArea[0] / hDisplayDensity;
    hPhysicalSafeRect[1] = (double)c_hSafeArea[1] / hDisplayDensity;
    hPhysicalSafeRect[2] = (double)c_hSafeArea[2] / hDisplayDensity;
    hPhysicalSafeRect[3] = (double)c_hSafeArea[3] / hDisplayDensity;

    mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
    mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
    mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);
    
    // Logger infomation.
    mmLogger_LogT(gLogger, "Window mmUIViewSurfaceMaster.NativeOnSurfaceChanged");
    mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_TRACE, pWindowName);

    // Fire event for size change.
    __static_mmSurfaceContentMetricsAssignment(pMetrics, (void*)p->jSurface, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceChanged))(p->pISurface, pMetrics);

    int iw = c_hViewSize[0];
    int ih = c_hViewSize[1];
    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnWindowDidResize %ux%u.", iw, ih);
}

// EditBox
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetTextEditRect(JNIEnv* env, jobject obj, jobject hRect)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    double c_Rect[4] = { 0 };
    jdoubleArray j_rect = (jdoubleArray)(hRect);
    (*(p->pISurface->OnGetTextEditRect))(p->pISurface, c_Rect);
    (*env)->SetDoubleArrayRegion(env, j_rect, 0, 4, c_Rect);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnInsertTextUtf16(JNIEnv* env, jobject obj, jobject s, jint o, jint l)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    jcharArray j_s = (jcharArray)(s);
    jchar* jarr_s = (*env)->GetCharArrayElements(env, j_s, NULL);

    struct mmEventEditString* pEditString = &p->hSurfaceContent.hEditString;

    pEditString->s = (mmUInt16_t*)(jarr_s + o);
    pEditString->l = l;
    pEditString->o = 0;
    pEditString->n = 0;
    (*(p->pISurface->OnInsertTextUtf16))(p->pISurface, pEditString);

    (*env)->ReleaseCharArrayElements(env, j_s, jarr_s, 0);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnCommitText(JNIEnv* env, jobject obj, jstring pText, jint hNewCursorPosition)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;
    struct mmEventEditString* pEditString = &p->hSurfaceContent.hEditString;

    const jchar* j_pText_Chars = (*env)->GetStringChars(env, pText, NULL);
    const mmUInt16_t* r_pText_Chars = (NULL == j_pText_Chars) ? mmUtf16StringEmpty : j_pText_Chars;

    // Commit text to the text box and set the new cursor position.
    // 
    // This method removes the contents of the currently composing text and replaces it with the passed CharSequence, 
    // and then moves the cursor according to newCursorPosition. If there is no composing text when this method is called, 
    // the new text is inserted at the cursor position, removing text inside the selection if any. This behaves like 
    // calling setComposingText(text, newCursorPosition) then finishComposingText().

    mmUInt16_t* pBuffer = (mmUInt16_t*)r_pText_Chars;
    size_t hLength = (size_t)(*env)->GetStringLength(env, pText);

    size_t hCaret = 0;
    size_t hSize0 = 0;
    size_t hSize1 = 0;
    int hInsertNumber = 0;
    int hIgnoreNumber = 0;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);
    hSize0 = pEditText->size - mmUtf16String_Size(&p->hComposingText);

    size_t hMarkedSize = mmUtf16String_Size(&p->hComposingText);
    size_t hRPos = pEditText->c;
    size_t hLPos = hRPos - hMarkedSize;
    // mmUInt16_t* pComposingBuffer = (mmUInt16_t*)mmUtf16String_Data(&p->hComposingText);
    // __static_mmUIViewSurfaceMaster_ReplaceRange(p, hLPos, hRPos, pComposingBuffer, 0);
    pEditString->s = NULL;
    pEditString->l = 0;
    pEditString->o = hLPos;
    pEditString->n = hMarkedSize;
    (*(p->pISurface->OnReplaceTextUtf16))(p->pISurface, pEditString);

    pEditText->c = hLPos;
    pEditText->v = mmEditTextCaret;
    (*(p->pISurface->OnSetText))(p->pISurface, pEditText);

    pEditString->s = pBuffer;
    pEditString->l = hLength;
    pEditString->o = 0;
    pEditString->n = 0;
    (*(p->pISurface->OnInsertTextUtf16))(p->pISurface, pEditString);

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);
    hSize1 = pEditText->size;

    hInsertNumber = (int)(hSize1 - hSize0);
    hIgnoreNumber = (int)(hLength - hInsertNumber);
    hIgnoreNumber = (hIgnoreNumber < 0) ? 0 : hIgnoreNumber;

    mmUtf16String_Clear(&p->hComposingText);

    // The new cursor position around the text, in Java characters. 
    //     If >  0, this is relative to the end of the text - 1; 
    //     if <= 0, this is relative to the start of the text. 
    //
    // So a value of 1 will always advance the cursor to the position after 
    // the full text being inserted. Note that this means you can't position 
    // the cursor within the text, because the editor can make modifications 
    // to the text you are providing so it is not possible to correctly specify 
    // locations there.

    int hCursorOffset = 0;
    int hInsertOffset = 0;

    int hU2RangeL = 0;
    int hU2RangeSize = 0;
    int hU2LengthC = 0;

    if(0 < hNewCursorPosition)
    {
        hCursorOffset = (int)(hNewCursorPosition - 1);
        hInsertOffset = (int)(hLPos + hLength);

        hU2RangeL = hInsertOffset;
        hU2RangeSize = hCursorOffset;
        hU2LengthC = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2RangeL, +1, hU2RangeSize);
    }
    else
    {
        hCursorOffset = (int)(-hNewCursorPosition);
        hInsertOffset = (int)(hLPos);

        hU2RangeL = hInsertOffset;
        hU2RangeSize = hCursorOffset;
        hU2LengthC = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2RangeL, -1, hU2RangeSize);
    }

    hCaret = hU2RangeL + hU2LengthC - hIgnoreNumber;

    pEditText->l = hCaret;
    pEditText->r = hCaret;
    pEditText->c = hCaret;
    pEditText->v = mmEditTextRange | mmEditTextCaret;
    (*(p->pISurface->OnSetText))(p->pISurface, pEditText);

    (*env)->ReleaseStringChars(env, pText, j_pText_Chars);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnDeleteSurroundingText(JNIEnv* env, jobject obj, jint hBeforeLength, jint hAfterLength)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    int hU2LLength = mmUTF_GetUnicodeRangeLength(pEditText->size, pEditText->l, -1, hBeforeLength);
    int hU2RLength = mmUTF_GetUnicodeRangeLength(pEditText->size, pEditText->r, +1, hAfterLength);

    __static_mmUIViewSurfaceMaster_TextDeleteSurrounding(p, hU2LLength, hU2RLength);

    mmUtf16String_Clear(&p->hComposingText);
}

JNIEXPORT jchar JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetCursorCapsUtf16(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    return (jchar)pEditText->t[pEditText->c];
}
JNIEXPORT jstring JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetSelectedText(JNIEnv* env, jobject obj, jint hFlags)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    size_t hLength = pEditText->r - pEditText->l;
    size_t hOffset = pEditText->l;
    mmUInt16_t* pBuffer = pEditText->t;

    const jchar* chars = (const jchar*)pBuffer + hOffset;
    jsize size = (jsize)hLength;

    chars = (NULL == chars) ? (const jchar*)mmStringEmpty : chars;

    return (*env)->NewString(env, chars, size);
}
JNIEXPORT jstring JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetTextAfterCursor(JNIEnv* env, jobject obj, jint n, jint hFlags)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    int hU2RLength = mmUTF_GetUnicodeRangeLength(pEditText->size, pEditText->r, +1, n);

    size_t hLength = hU2RLength;
    size_t hOffset = pEditText->r;
    mmUInt16_t* pBuffer = pEditText->t;

    const jchar* chars = (const jchar*)pBuffer + hOffset;
    jsize size = (jsize)hLength;

    chars = (NULL == chars) ? (const jchar*)mmStringEmpty : chars;

    return (*env)->NewString(env, chars, size);
}
JNIEXPORT jstring JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetTextBeforeCursor(JNIEnv* env, jobject obj, jint n, jint hFlags)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    int hU2LLength = mmUTF_GetUnicodeRangeLength(pEditText->size, pEditText->l, -1, n);

    size_t hLength = hU2LLength;
    size_t hOffset = pEditText->l - hLength;
    mmUInt16_t* pBuffer = pEditText->t;

    const jchar* chars = (const jchar*)pBuffer + hOffset;
    jsize size = (jsize)hLength;

    chars = (NULL == chars) ? mmUtf16StringEmpty : chars;

    return (*env)->NewString(env, chars, size);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSetSelection(JNIEnv* env, jobject obj, jint hStart, jint hEnd)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    int hU2RangeL = 0;
    int hU2RangeR = 0;
    int hU2RangeSize = 0;
    int hU2LengthS = 0;
    int hU2LengthE = 0;
    int hU2Offset = 0;

    if(hStart < hEnd)
    {
        hU2RangeL = hStart;
        hU2RangeR = hEnd;
    }
    else
    {
        hU2RangeL = hEnd;
        hU2RangeR = hStart;
    }

    hU2RangeSize = hU2RangeR - hU2RangeL;
    hU2Offset = 0;
    hU2LengthS = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2Offset, +1, hU2RangeL);
    hU2Offset = hU2LengthS;
    hU2LengthE = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2Offset, +1, hU2RangeSize);

    pEditText->l = hU2LengthS;
    pEditText->r = hU2LengthS + hU2LengthE;
    pEditText->v = mmEditTextRange;
    (*(p->pISurface->OnSetText))(p->pISurface, pEditText);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetSelection(JNIEnv* env, jobject obj, jobject hRange)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    int hU2RangeL = 0;
    int hU2RangeR = 0;
    int hU2RangeSize = 0;
    int hU2LengthS = 0;
    int hU2LengthE = 0;
    int hU2Offset = 0;

    hU2RangeL = (int)pEditText->l;
    hU2RangeR = (int)pEditText->r;
    hU2RangeSize = hU2RangeR - hU2RangeL;
    hU2Offset = 0;
    hU2LengthS = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2Offset, +1, hU2RangeL);
    hU2Offset = hU2LengthS;
    hU2LengthE = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2Offset, +1, hU2RangeSize);

    jintArray j_hRange = (jintArray)(hRange);

    jint jarr_hRange[2];
    jarr_hRange[0] = hU2LengthS;
    jarr_hRange[1] = hU2LengthS + hU2LengthE;

    (*env)->SetIntArrayRegion(env, j_hRange, 0, 2, jarr_hRange);
}

JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetSize(JNIEnv* env, jobject obj, jobject hSize)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextCsize | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    jintArray j_hSize = (jintArray)(hSize);

    jint jarr_hSize[2];
    jarr_hSize[0] = pEditText->size;
    jarr_hSize[1] = pEditText->capacity;

    (*env)->SetIntArrayRegion(env, j_hSize, 0, 2, jarr_hSize);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetTextSelection(JNIEnv* env, jobject obj, jobject hTextRange)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextRange;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    jintArray j_hTextRange = (jintArray)(hTextRange);

    jint jarr_hTextRange[2];
    jarr_hTextRange[0] = pEditText->l;
    jarr_hTextRange[1] = pEditText->r;

    (*env)->SetIntArrayRegion(env, j_hTextRange, 0, 2, jarr_hTextRange);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnGetTextSize(JNIEnv* env, jobject obj, jobject hTextSize)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextCsize | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    jintArray j_hTextSize = (jintArray)(hTextSize);

    jint jarr_hTextSize[2];
    jarr_hTextSize[0] = pEditText->size;
    jarr_hTextSize[1] = pEditText->capacity;

    (*env)->SetIntArrayRegion(env, j_hTextSize, 0, 2, jarr_hTextSize);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSetComposingRegion(JNIEnv* env, jobject obj, jint hStart, jint hEnd)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    int hU2RangeL = 0;
    int hU2RangeR = 0;
    int hU2RangeSize = 0;
    int hU2LengthS = 0;
    int hU2LengthE = 0;
    int hU2Offset = 0;

    if(hStart < hEnd)
    {
        hU2RangeL = hStart;
        hU2RangeR = hEnd;
    }
    else
    {
        hU2RangeL = hEnd;
        hU2RangeR = hStart;
    }

    hU2RangeSize = hU2RangeR - hU2RangeL;
    hU2Offset = 0;
    hU2LengthS = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2Offset, +1, hU2RangeL);
    hU2Offset = hU2LengthS;
    hU2LengthE = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2Offset, +1, hU2RangeSize);

    size_t hComposingLength = hU2LengthE;
    size_t hComposingOffset = hU2LengthS;
    mmUInt16_t* pBuffer = pEditText->t;

    mmUtf16String_Resize(&p->hComposingText, hComposingLength);
    mmMemcpy((void*)mmUtf16String_Data(&p->hComposingText), pBuffer + hComposingOffset, hComposingLength * sizeof(mmUInt16_t));
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnSetComposingText(JNIEnv* env, jobject obj, jstring pText, jint hNewCursorPosition)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;
    struct mmEventEditString* pEditString = &p->hSurfaceContent.hEditString;

    const jchar* j_pText_Chars = (*env)->GetStringChars(env, pText, NULL);
    const mmUInt16_t* r_pText_Chars = (NULL == j_pText_Chars) ? mmUtf16StringEmpty : j_pText_Chars;

    mmUInt16_t* pBuffer = (mmUInt16_t*)r_pText_Chars;
    size_t hLength = (size_t)(*env)->GetStringLength(env, pText);

    size_t hCaret = 0;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

    size_t hMarkedSize = mmUtf16String_Size(&p->hComposingText);
    size_t hRPos = pEditText->c;
    size_t hLPos = hRPos - hMarkedSize;
    // __static_mmUIViewSurfaceMaster_ReplaceRange(p, hLPos, hRPos, pBuffer, hLength);
    pEditString->s = pBuffer;
    pEditString->l = hLength;
    pEditString->o = hLPos;
    pEditString->n = hMarkedSize;
    (*(p->pISurface->OnReplaceTextUtf16))(p->pISurface, pEditString);

    // cache the current ComposingText.
    mmUtf16String_Resize(&p->hComposingText, hLength);
    mmMemcpy((void*)mmUtf16String_Data(&p->hComposingText), pBuffer, hLength * sizeof(mmUInt16_t));

    // newCursorPosition The new cursor position around the text. 
    //     If > 0, this is relative to the end of the text - 1; 
    //     if <= 0, this is relative to the start of the text. 
    //
    // So a value of 1 will always advance you to the position after the full 
    // text being inserted. Note that this means you can't position the cursor 
    // within the text, because the editor can make modifications to the text 
    // you are providing so it is not possible to correctly specify locations there.

    int hCursorOffset = 0;
    int hInsertOffset = 0;

    int hU2RangeL = 0;
    int hU2RangeSize = 0;
    int hU2LengthC = 0;

    if(0 < hNewCursorPosition)
    {
        hCursorOffset = (int)(hNewCursorPosition - 1);
        hInsertOffset = (int)(hLPos + hLength);

        hU2RangeL = hInsertOffset;
        hU2RangeSize = hCursorOffset;
        hU2LengthC = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2RangeL, +1, hU2RangeSize);
    }
    else
    {
        hCursorOffset = (int)(-hNewCursorPosition);
        hInsertOffset = (int)(hLPos);

        hU2RangeL = hInsertOffset;
        hU2RangeSize = hCursorOffset;
        hU2LengthC = mmUTF_GetUnicodeRangeLength(pEditText->size, hU2RangeL, -1, hU2RangeSize);
    }

    hCaret = hU2RangeL + hU2LengthC;

    pEditText->l = hCaret;
    pEditText->r = hCaret;
    pEditText->c = hCaret;
    pEditText->v = mmEditTextRange | mmEditTextCaret | mmEditTextDirty;
    (*(p->pISurface->OnSetText))(p->pISurface, pEditText);

    (*env)->ReleaseStringChars(env, pText, j_pText_Chars);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnFinishComposingText(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    mmUtf16String_Clear(&p->hComposingText);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnDeleteBackwardText(JNIEnv* env, jobject obj)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;
    mmMemset(pKeypad->text, 0, sizeof(mmUInt16_t) * 4);
    pKeypad->modifier_mask = 0;
    pKeypad->key = MM_KC_BACKSPACE;
    pKeypad->length = 1;
    pKeypad->text[0] = MM_ASCII_BS;
    pKeypad->handle = 0;
    (*(p->pISurface->OnKeypadPressed))(p->pISurface, pKeypad);
    (*(p->pISurface->OnKeypadRelease))(p->pISurface, pKeypad);
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnPerformContextMenuAction(JNIEnv* env, jobject obj, jint hActionId)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    // android.R.id.selectAll           Constant Value: 16908319 (0x0102001f)
    // android.R.id.cut                 Constant Value: 16908320 (0x01020020)
    // android.R.id.copy                Constant Value: 16908321 (0x01020021)
    // android.R.id.paste               Constant Value: 16908322 (0x01020022)
    // android.R.id.copyUrl             Constant Value: 16908323 (0x01020023)
    // android.R.id.switchInputMethod   Constant Value: 16908324 (0x01020024)
    // android.R.id.startSelectingText  Constant Value: 16908328 (0x01020028)
    // android.R.id.stopSelectingText   Constant Value: 16908329 (0x01020029)

    switch(hActionId)
    {
    case 0x0102001f:
        {
            // selectAll
            struct mmEventEditText* pEditText = &p->hSurfaceContent.hEditText;

            pEditText->v = mmEditTextRsize;
            (*(p->pISurface->OnGetText))(p->pISurface, pEditText);

            pEditText->l = 0;
            pEditText->r = pEditText->size;
            pEditText->v = mmEditTextRange;
            (*(p->pISurface->OnSetText))(p->pISurface, pEditText);
        }
        break;
    case 0x01020020:
        {
            // cut
            (*(p->pISurface->OnInjectCut))(p->pISurface);
        }
        break;
    case 0x01020021:
        {
            // copy
            (*(p->pISurface->OnInjectCopy))(p->pISurface);
        }
        break;
    case 0x01020022:
        {
            // paste
            (*(p->pISurface->OnInjectPaste))(p->pISurface);
        }
        break;
    default:
        break;
    }
}
JNIEXPORT void JNICALL Java_org_mm_nwsi_mmUIViewSurfaceMaster_NativeOnPerformEditorAction(JNIEnv* env, jobject obj, jint hEditorAction)
{
    struct mmUIViewSurfaceMaster* p = mmUIViewSurfaceMaster_GetNativePtr(env, obj);

    // EditorInfo.IME_ACTION_UNSPECIFIED Constant Value: 0 (0x00000000)
    // EditorInfo.IME_ACTION_NONE        Constant Value: 1 (0x00000001)
    // EditorInfo.IME_ACTION_GO          Constant Value: 2 (0x00000002)
    // EditorInfo.IME_ACTION_SEARCH      Constant Value: 3 (0x00000003)
    // EditorInfo.IME_ACTION_SEND        Constant Value: 4 (0x00000004)
    // EditorInfo.IME_ACTION_NEXT        Constant Value: 5 (0x00000005)
    // EditorInfo.IME_ACTION_DONE        Constant Value: 6 (0x00000006)
    // EditorInfo.IME_ACTION_PREVIOUS    Constant Value: 7 (0x00000007)

    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;
    mmMemset(pKeypad->text, 0, sizeof(mmUInt16_t) * 4);
    pKeypad->modifier_mask = 0;
    pKeypad->key = MM_KC_RETURN;
    pKeypad->length = 1;
    pKeypad->text[0] = MM_ASCII_CR;
    pKeypad->handle = 0;
    (*(p->pISurface->OnKeypadPressed))(p->pISurface, pKeypad);
    (*(p->pISurface->OnKeypadRelease))(p->pISurface, pKeypad);
}

#include "core/mmSuffix.h"
