/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewSurfaceMaster_h__
#define __mmUIViewSurfaceMaster_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "nwsi/mmISurface.h"
#include "nwsi/mmSurfaceMaster.h"

#include "nwsi/mmNwsiExport.h"

#include <jni.h>

#include "core/mmPrefix.h"

MM_EXPORT_NWSI extern jclass mmUIViewSurfaceMaster_ObjectClass;
MM_EXPORT_NWSI extern jfieldID mmUIViewSurfaceMaster_Field_hNativePtr;
MM_EXPORT_NWSI extern jmethodID mmUIViewSurfaceMaster_Method_OnSetKeypadType;
MM_EXPORT_NWSI extern jmethodID mmUIViewSurfaceMaster_Method_OnSetKeypadAppearance;
MM_EXPORT_NWSI extern jmethodID mmUIViewSurfaceMaster_Method_OnSetKeypadReturnKey;
MM_EXPORT_NWSI extern jmethodID mmUIViewSurfaceMaster_Method_OnKeypadStatusShow;
MM_EXPORT_NWSI extern jmethodID mmUIViewSurfaceMaster_Method_OnKeypadStatusHide;

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_AttachVirtualMachine(JNIEnv* env);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_DetachVirtualMachine(JNIEnv* env);

struct mmSurfaceMaster;
struct mmSurfaceInterface;

struct mmUIViewSurfaceMaster
{
    struct mmEventSurfaceContent hSurfaceContent;
    struct mmISurfaceKeypad hISurfaceKeypad;

    struct mmUtf16String hComposingText;

    // weak ref.
    struct mmSurfaceMaster* pSurfaceMaster;
    // weak ref.
    struct mmISurface* pISurface;

    jobject jNativePtr;
    jobject jSurface;

    mmUInt32_t hModifierMask;
    mmUInt32_t hButtonMask;

    double hDisplayDensity;
};

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Init(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Destroy(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetSurfaceMaster(struct mmUIViewSurfaceMaster* p, struct mmSurfaceMaster* pSurfaceMaster);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_JSurfaceNativeCreateGlobalRef(struct mmUIViewSurfaceMaster* p, JNIEnv* env, jobject obj);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_JSurfaceNativeDeleteGlobalRef(struct mmUIViewSurfaceMaster* p, JNIEnv* env);

#include "core/mmSuffix.h"

#endif//__mmUIViewSurfaceMaster_h__

