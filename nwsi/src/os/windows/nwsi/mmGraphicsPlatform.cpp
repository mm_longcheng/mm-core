/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmGraphicsPlatform.h"

MM_EXPORT_NWSI
void
mmIUnknownSafeRelease(
    IUnknown*                                      p)
{
    if (NULL != p)
    {
        p->Release();
    }
}

MM_EXPORT_NWSI
void
mmGraphicsPlatform_MakeD2D1ColorF(
    D2D1_COLOR_F*                                  p,
    float                                          pColor[4])
{
    p->r = pColor[0];
    p->g = pColor[1];
    p->b = pColor[2];
    p->a = pColor[3];
}

MM_EXPORT_NWSI
void
mmGraphicsPlatform_MakeD2D1Vector4F(
    D2D1_VECTOR_4F*                                p,
    float                                          pColor[4])
{
    p->x = pColor[0];
    p->y = pColor[1];
    p->z = pColor[2];
    p->w = pColor[3];
}

MM_EXPORT_NWSI
D2D1_LINE_JOIN
mmGraphicsPlatform_GetLineJoin(
    int                                            hLineJoin)
{
    static const D2D1_LINE_JOIN gLineJoinTable[] =
    {
        D2D1_LINE_JOIN_MITER        , // 0 mmLineJoinMiter
        D2D1_LINE_JOIN_ROUND        , // 1 mmLineJoinRound
        D2D1_LINE_JOIN_BEVEL        , // 2 mmLineJoinBevel
    };
    if (0 <= hLineJoin && hLineJoin < MM_ARRAY_SIZE(gLineJoinTable))
    {
        return gLineJoinTable[hLineJoin];
    }
    else
    {
        return D2D1_LINE_JOIN_MITER;
    }
}

MM_EXPORT_NWSI
D2D1_CAP_STYLE
mmGraphicsPlatform_GetCapStyle(
    int                                            hLineCap)
{
    static const D2D1_CAP_STYLE gStyleTable[] =
    {
        D2D1_CAP_STYLE_FLAT     , // 0 mmLineCapButt
        D2D1_CAP_STYLE_ROUND    , // 1 mmLineCapRound
        D2D1_CAP_STYLE_SQUARE   , // 2 mmLineCapSquare
    };
    if (0 <= hLineCap && hLineCap < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hLineCap];
    }
    else
    {
        return D2D1_CAP_STYLE_FLAT;
    }
}
