/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUnicodeWindows.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"

MM_EXPORT_NWSI UINT mmUnicodeWindows_CodePage(void)
{
    // need to convert to narrow (OEM or ANSI) codepage so that fstream can use it 
    // properly on international systems.
#if MM_PLATFORM == MM_PLATFORM_WIN32
    // Note, that on legacy CRT versions codepage for narrow CRT file functions can be changed using 
    // SetFileApisANSI/OEM, but on modern runtimes narrow pathes are always widened using ANSI codepage.
    // We suppose that on such runtimes file APIs codepage is left in default, ANSI state.
    UINT hCodePage = AreFileApisANSI() ? CP_ACP : CP_OEMCP;
#elif MM_PLATFORM == MM_PLATFORM_WINRT
    // Runtime is modern, narrow calls are widened inside CRT using CP_ACP codepage.
    UINT hCodePage = CP_ACP;
#endif
    return hCodePage;
}
MM_EXPORT_NWSI int mmUnicodeWindows_ParamToWideChar(WPARAM wParam, UINT hCodePage, wchar_t wbstr[2])
{
#ifdef _UNICODE
    wbstr[0] = (wchar_t)wParam;
    wbstr[1] = '\0';
    return 2;
#else
    char mbstr[3] = { 0 };
    BYTE hiByte = (BYTE)(wParam >> 8);
    BYTE loByte = (BYTE)(wParam & 0x000000FF);
    if (0 == hiByte)
    {
        mbstr[0] = loByte;
        mbstr[1] = '\0';
    }
    else
    {
        mbstr[0] = hiByte;
        mbstr[1] = loByte;
        mbstr[2] = '\0';
    }
    mmMemset(wbstr, 0, sizeof(wchar_t) * 2);
    return MultiByteToWideChar(hCodePage, 0, mbstr, -1, wbstr, 2);
#endif//_UNICODE
}
MM_EXPORT_NWSI int mmUnicodeWindows_ParamToMultiByte(WPARAM wParam, UINT hCodePage, char mbstr[3])
{

#ifdef _UNICODE
    wchar_t wbstr[2] = { 0 };
    wbstr[0] = (wchar_t)wParam;
    wbstr[1] = '\0';
    mmMemset(mbstr, 0, sizeof(char) * 3);
    return WideCharToMultiByte(hCodePage, 0, wbstr, -1, mbstr, (int)3, NULL, NULL);
#else
    BYTE hiByte = (BYTE)(wParam >> 8);
    BYTE loByte = (BYTE)(wParam & 0x000000FF);
    if (0 == hiByte)
    {
        mbstr[0] = loByte;
        mbstr[1] = '\0';
        return 1;
    }
    else
    {
        mbstr[0] = hiByte;
        mbstr[1] = loByte;
        mbstr[2] = '\0';
        return 2;
    }
#endif//_UNICODE

}

MM_EXPORT_NWSI int mmUnicodeWindows_WCharToString(const WCHAR* wText, struct mmString* sText)
{
    int hCode = -1;

    do
    {
        UINT codepage = mmUnicodeWindows_CodePage();

        char* data = NULL;
        int size = 0;
        const int wlength = (const int)(wcslen(wText));
        /* Use default flags */
        int length = WideCharToMultiByte(codepage, 0, wText, wlength, NULL, 0, NULL, NULL);
        if (length <= 0)
        {
            hCode = 1;
            mmString_Clear(sText);
            break;
        }

        // success
        mmString_Resize(sText, length);
        data = (char*)mmString_Data(sText);
        size = (int)mmString_Size(sText);

        /* Use default flags */
        length = WideCharToMultiByte(codepage, 0, wText, wlength, data, size, NULL, NULL);
        if (length <= 0)
        {
            hCode = 1;
            mmString_Clear(sText);
            break;
        }

        hCode = MM_SUCCESS;
    } while (0);

    return hCode;
}

MM_EXPORT_NWSI int mmUnicodeWindows_TCharToString(const TCHAR* tText, struct mmString* sText)
{
#ifdef _UNICODE
    return mmUnicodeWindows_WCharToString(tText, sText);
#else
    return mmString_Assigns(sText, tText);
#endif//_UNICODE
}