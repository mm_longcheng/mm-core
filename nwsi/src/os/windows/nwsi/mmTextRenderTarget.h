/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextRenderTarget_h__
#define __mmTextRenderTarget_h__

#include "core/mmCore.h"

#include "math/mmAffineTransform.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmGraphicsFactory;
struct mmTextParagraphFormat;
struct mmTextDrawingContext;

struct ID2D1RenderTarget;
struct IWICBitmap;
struct IWICBitmapLock;

struct mmTextRenderTarget
{
    struct mmGraphicsFactory* pGraphicsFactory;
    struct mmTextParagraphFormat* pParagraphFormat;

    struct ID2D1RenderTarget* pD2DRenderTarget;
    struct IWICBitmap* pWICBitmap;
    struct IWICBitmapLock* pBitmapLock;
};

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_Init(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_Destroy(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_SetGraphicsFactory(
    struct mmTextRenderTarget*                     p, 
    struct mmGraphicsFactory*                      pGraphicsFactory);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_SetTextParagraphFormat(
    struct mmTextRenderTarget*                     p, 
    struct mmTextParagraphFormat*                  pParagraphFormat);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_OnFinishLaunching(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_OnBeforeTerminate(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_BeginDraw(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_SetTransform(
    struct mmTextRenderTarget*                     p, 
    struct mmAffineTransform*                      hTransform);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_Clear(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_EndDraw(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_BeginTransparency(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_EndTransparency(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
const void* 
mmTextRenderTarget_BufferLock(
    struct mmTextRenderTarget*                     p);

MM_EXPORT_NWSI 
void 
mmTextRenderTarget_BufferUnLock(
    struct mmTextRenderTarget*                     p, 
    const void*                                    pBuffer);

#include "core/mmSuffix.h"

#endif//__mmTextRenderTarget_h__
