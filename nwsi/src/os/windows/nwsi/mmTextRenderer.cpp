/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextRenderer.h"
#include "mmTextDrawingContext.h"
#include "mmTextDrawingEffect.h"
#include "mmTextPlatform.h"

#include "mmGraphicsPlatform.h"
#include "mmGraphicsFactory.h"

#include "core/mmLogger.h"

#include "nwsi/mmTextParagraphFormat.h"

#include <d2d1.h>
#include <dwrite.h>

class mmTextRenderer : public IDWriteTextRenderer
{
public:
    ULONG hRefCount;
public:
    mmTextRenderer(void);
public:
    // IUnknowns
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(
        /* [in] */ REFIID riid,
        /* [iid_is][out] */ _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject);

    virtual ULONG STDMETHODCALLTYPE AddRef(void);

    virtual ULONG STDMETHODCALLTYPE Release(void);
public:
    // IDWriteTextRenderer
    STDMETHOD(DrawGlyphRun)(
        _In_opt_ void* clientDrawingContext,
        FLOAT baselineOriginX,
        FLOAT baselineOriginY,
        DWRITE_MEASURING_MODE measuringMode,
        _In_ DWRITE_GLYPH_RUN const* glyphRun,
        _In_ DWRITE_GLYPH_RUN_DESCRIPTION const* glyphRunDescription,
        _In_opt_ IUnknown* clientDrawingEffect
        );

    STDMETHOD(DrawUnderline)(
        _In_opt_ void* clientDrawingContext,
        FLOAT baselineOriginX,
        FLOAT baselineOriginY,
        _In_ DWRITE_UNDERLINE const* underline,
        _In_opt_ IUnknown* clientDrawingEffect
        );

    STDMETHOD(DrawStrikethrough)(
        _In_opt_ void* clientDrawingContext,
        FLOAT baselineOriginX,
        FLOAT baselineOriginY,
        _In_ DWRITE_STRIKETHROUGH const* strikethrough,
        _In_opt_ IUnknown* clientDrawingEffect
        );

    STDMETHOD(DrawInlineObject)(
        _In_opt_ void* clientDrawingContext,
        FLOAT originX,
        FLOAT originY,
        _In_ IDWriteInlineObject* inlineObject,
        BOOL isSideways,
        BOOL isRightToLeft,
        _In_opt_ IUnknown* clientDrawingEffect
        );

public:
    // IDWritePixelSnapping
    STDMETHOD(IsPixelSnappingDisabled)(
        _In_opt_ void* clientDrawingContext,
        _Out_ BOOL* isDisabled
        );

    STDMETHOD(GetCurrentTransform)(
        _In_opt_ void* clientDrawingContext,
        _Out_ DWRITE_MATRIX* transform
        );

    STDMETHOD(GetPixelsPerDip)(
        _In_opt_ void* clientDrawingContext,
        _Out_ FLOAT* pixelsPerDip
        );
public:
    void GeometryDrawingRectangle(
        struct mmTextDrawingContext* pDrawingContext,
        const D2D1_RECT_F& hRectangle,
        FLOAT baselineOriginX,
        FLOAT baselineOriginY,
        const D2D1_COLOR_F& hForegroundColor);

    void GeometryDrawingEffect(
        struct mmTextDrawingContext* pDrawingContext,
        ID2D1Geometry* pGeometry,
        struct mmTextFormat* pTextFormat);
};

MM_EXPORT_NWSI
HRESULT
mmTextFactory_CreateTextRenderer(
    struct IDWriteTextRenderer**                   pTextRenderer)
{
    (*pTextRenderer) = new mmTextRenderer;
    (*pTextRenderer)->AddRef();
    return S_OK;
}

mmTextRenderer::mmTextRenderer(void)
    : hRefCount(0)
{

}

HRESULT STDMETHODCALLTYPE mmTextRenderer::QueryInterface(
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject)
{
    if (riid == IID_IUnknown || 
        riid == __uuidof(IDWritePixelSnapping) ||
        riid == __uuidof(IDWriteTextRenderer))
    {
        *ppvObject = this;
        this->AddRef();
        return NOERROR;
    }
    else
    {
        *ppvObject = NULL;
        return E_NOINTERFACE;
    }
}

ULONG STDMETHODCALLTYPE mmTextRenderer::AddRef(void)
{
    return ++this->hRefCount;
}

ULONG STDMETHODCALLTYPE mmTextRenderer::Release(void)
{
    // Decrement the object's internal counter.
    if (0 == --this->hRefCount)
    {
        delete this;
    }
    return this->hRefCount;
}

HRESULT mmTextRenderer::DrawGlyphRun(
    void *clientDrawingContext, FLOAT baselineOriginX, FLOAT baselineOriginY,
    DWRITE_MEASURING_MODE measuringMode, DWRITE_GLYPH_RUN const *glyphRun,
    DWRITE_GLYPH_RUN_DESCRIPTION const *glyphRunDescription, IUnknown *clientDrawingEffect)
{
    HRESULT hr = S_OK;

    ID2D1PathGeometry* pPathGeometry = NULL;
    ID2D1GeometrySink* pGeometrySink = NULL;
    ID2D1TransformedGeometry* pTransformedGeometry = NULL;
    mmIDrawingEffect* pDrawingEffect = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == clientDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d clientDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == clientDrawingEffect)
        {
            mmLogger_LogT(gLogger, "%s %d clientDrawingEffect is null.", __FUNCTION__, __LINE__);
            break;
        }

        hr = clientDrawingEffect->QueryInterface(__uuidof(mmIDrawingEffect), (void**)(&pDrawingEffect));
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d QueryInterface failure.", __FUNCTION__, __LINE__);
            break;
        }
        struct mmTextFormat* pTextFormat = NULL;
        pDrawingEffect->GetTextFormat(&pTextFormat);
        if (NULL == pTextFormat)
        {
            mmLogger_LogT(gLogger, "%s %d GetTextFormat failure.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextDrawingContext* pDrawingContext = (struct mmTextDrawingContext*)clientDrawingContext;
        struct mmGraphicsFactory* pGraphicsFactory = pDrawingContext->pGraphicsFactory;
        if (NULL == pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        ID2D1Factory* pD2DFactory = pGraphicsFactory->pD2DFactory;
        if (NULL == pD2DFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        // Initialize a matrix to translate the origin of the glyph run.
        D2D1::Matrix3x2F const matrix = D2D1::Matrix3x2F(
            1.0f, 0.0f,
            0.0f, 1.0f,
            baselineOriginX, baselineOriginY
        );

        // Create the path geometry.
        hr = pD2DFactory->CreatePathGeometry(
            &pPathGeometry
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreatePathGeometry failure.", __FUNCTION__, __LINE__);
            break;
        }

        // Write to the path geometry using the geometry sink.
        hr = pPathGeometry->Open(
            &pGeometrySink
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d pGeometrySink->Open failure.", __FUNCTION__, __LINE__);
            break;
        }

        // Get the glyph run outline geometries back from DirectWrite and place them within the
        // geometry sink.
        hr = glyphRun->fontFace->GetGlyphRunOutline(
            glyphRun->fontEmSize,
            glyphRun->glyphIndices,
            glyphRun->glyphAdvances,
            glyphRun->glyphOffsets,
            glyphRun->glyphCount,
            glyphRun->isSideways,
            glyphRun->bidiLevel % 2,
            pGeometrySink
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d GetGlyphRunOutline failure.", __FUNCTION__, __LINE__);
            break;
        }

        // Close the geometry sink
        hr = pGeometrySink->Close();
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d pGeometrySink->Close failure.", __FUNCTION__, __LINE__);
            break;
        }

        // Create the transformed geometry
        hr = pD2DFactory->CreateTransformedGeometry(
            pPathGeometry,
            &matrix,
            &pTransformedGeometry
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateTransformedGeometry failure.", __FUNCTION__, __LINE__);
            break;
        }

        this->GeometryDrawingEffect(
            pDrawingContext,
            pTransformedGeometry,
            pTextFormat);
    } while (0);

    mmIUnknownSafeRelease(pDrawingEffect);
    mmIUnknownSafeRelease(pPathGeometry);
    mmIUnknownSafeRelease(pGeometrySink);
    mmIUnknownSafeRelease(pTransformedGeometry);

    return hr;
}

HRESULT mmTextRenderer::DrawUnderline(
    _In_opt_ void* clientDrawingContext,
    FLOAT baselineOriginX,
    FLOAT baselineOriginY,
    _In_ DWRITE_UNDERLINE const* underline,
    _In_opt_ IUnknown* clientDrawingEffect
)
{
    HRESULT hr = S_OK;

    mmIDrawingEffect* pDrawingEffect = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == clientDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d clientDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextDrawingContext* pDrawingContext = (struct mmTextDrawingContext*)clientDrawingContext;
        struct mmTextParagraphFormat* pParagraphFormat = pDrawingContext->pParagraphFormat;

        if (NULL == pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        float hDisplayDensity = pParagraphFormat->hDisplayDensity;

        switch (pDrawingContext->hPass)
        {
        case 1:
        {
            // pass 1
            //     Background x
            //     Underline  o
            //     Foreground x
            //     Stroke     x
            //     StrikeThru x

            if (NULL == clientDrawingEffect)
            {
                mmLogger_LogT(gLogger, "%s %d clientDrawingEffect is null.", __FUNCTION__, __LINE__);
                break;
            }

            hr = clientDrawingEffect->QueryInterface(__uuidof(mmIDrawingEffect), (void**)(&pDrawingEffect));
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d QueryInterface failure.", __FUNCTION__, __LINE__);
                break;
            }
            struct mmTextFormat* pTextFormat = NULL;
            pDrawingEffect->GetTextFormat(&pTextFormat);
            if (NULL == pTextFormat)
            {
                mmLogger_LogT(gLogger, "%s %d GetTextFormat failure.", __FUNCTION__, __LINE__);
                break;
            }

            float hUnderlineWidth = (float)fabs(pTextFormat->hUnderlineWidth * hDisplayDensity);
            FLOAT hThickness = (-1.0f == pTextFormat->hUnderlineWidth) ? underline->thickness : hUnderlineWidth;

            D2D1_RECT_F hRectangle = D2D1::RectF(
                0,
                underline->offset,
                underline->width,
                underline->offset + hThickness
            );

            D2D1_COLOR_F hUnderlineColor;
            mmGraphicsPlatform_MakeD2D1ColorF(&hUnderlineColor, pTextFormat->hUnderlineColor);

            this->GeometryDrawingRectangle(
                pDrawingContext,
                hRectangle,
                baselineOriginX,
                baselineOriginY,
                hUnderlineColor);
        }
        break;

        default:
        break;
        }
    } while (0);

    mmIUnknownSafeRelease(pDrawingEffect);

    return hr;
}

HRESULT mmTextRenderer::DrawStrikethrough(
    _In_opt_ void* clientDrawingContext,
    FLOAT baselineOriginX,
    FLOAT baselineOriginY,
    _In_ DWRITE_STRIKETHROUGH const* strikethrough,
    _In_opt_ IUnknown* clientDrawingEffect
)
{
    HRESULT hr = S_OK;

    mmIDrawingEffect* pDrawingEffect = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == clientDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d clientDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextDrawingContext* pDrawingContext = (struct mmTextDrawingContext*)clientDrawingContext;
        struct mmTextParagraphFormat* pParagraphFormat = pDrawingContext->pParagraphFormat;

        if (NULL == pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        float hDisplayDensity = pParagraphFormat->hDisplayDensity;

        switch (pDrawingContext->hPass)
        {
        case 2:
        {
            // pass 2
            //     Background x
            //     Underline  x
            //     Foreground o
            //     Stroke     o
            //     StrikeThru o

            if (NULL == clientDrawingEffect)
            {
                mmLogger_LogT(gLogger, "%s %d clientDrawingEffect is null.", __FUNCTION__, __LINE__);
                break;
            }

            hr = clientDrawingEffect->QueryInterface(__uuidof(mmIDrawingEffect), (void**)(&pDrawingEffect));
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d QueryInterface failure.", __FUNCTION__, __LINE__);
                break;
            }
            struct mmTextFormat* pTextFormat = NULL;
            pDrawingEffect->GetTextFormat(&pTextFormat);
            if (NULL == pTextFormat)
            {
                mmLogger_LogT(gLogger, "%s %d GetTextFormat failure.", __FUNCTION__, __LINE__);
                break;
            }

            float hStrikeThruWidth = (float)fabs(pTextFormat->hStrikeThruWidth * hDisplayDensity);
            FLOAT hThickness = (-1.0f == pTextFormat->hStrikeThruWidth) ? strikethrough->thickness : hStrikeThruWidth;

            D2D1_RECT_F hRectangle = D2D1::RectF(
                0,
                strikethrough->offset,
                strikethrough->width,
                strikethrough->offset + hThickness
            );

            D2D1_COLOR_F hStrikeThruColor;
            mmGraphicsPlatform_MakeD2D1ColorF(&hStrikeThruColor, pTextFormat->hStrikeThruColor);

            this->GeometryDrawingRectangle(
                pDrawingContext,
                hRectangle,
                baselineOriginX,
                baselineOriginY,
                hStrikeThruColor);
        }
        break;

        default:
        break;
        }
    } while (0);

    mmIUnknownSafeRelease(pDrawingEffect);

    return hr;
}

HRESULT mmTextRenderer::DrawInlineObject(
    _In_opt_ void* clientDrawingContext,
    FLOAT originX,
    FLOAT originY,
    _In_ IDWriteInlineObject* inlineObject,
    BOOL isSideways,
    BOOL isRightToLeft,
    _In_opt_ IUnknown* clientDrawingEffect
)
{
    HRESULT hr = S_OK;

    hr = inlineObject->Draw(
        clientDrawingContext,
        this,
        originX,
        originY,
        isSideways,
        isRightToLeft,
        clientDrawingEffect
    );

    return hr;
}

// IDWritePixelSnapping
HRESULT mmTextRenderer::IsPixelSnappingDisabled(
    _In_opt_ void* clientDrawingContext,
    _Out_ BOOL* isDisabled
)
{
    *isDisabled = FALSE;
    return S_OK;
}

HRESULT mmTextRenderer::GetCurrentTransform(
    _In_opt_ void* clientDrawingContext,
    _Out_ DWRITE_MATRIX* transform
)
{
    HRESULT hr = S_FALSE;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmTextDrawingContext* pDrawingContext = (struct mmTextDrawingContext*)clientDrawingContext;
        if (NULL == pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        ID2D1RenderTarget* pD2DRenderTarget = (ID2D1RenderTarget*)pDrawingContext->pRenderTarget;
        if (NULL == pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        pD2DRenderTarget->GetTransform(reinterpret_cast<D2D1_MATRIX_3X2_F*>(transform));

        hr = S_OK;
    } while (0);

    return hr;
}

HRESULT mmTextRenderer::GetPixelsPerDip(
    _In_opt_ void* clientDrawingContext,
    _Out_ FLOAT* pixelsPerDip
)
{
    HRESULT hr = S_FALSE;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmTextDrawingContext* pDrawingContext = (struct mmTextDrawingContext*)clientDrawingContext;
        if (NULL == pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        ID2D1RenderTarget* pD2DRenderTarget = (ID2D1RenderTarget*)pDrawingContext->pRenderTarget;
        if (NULL == pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        float x, y;

        pD2DRenderTarget->GetDpi(&x, &y);

        // (x + y) / (2 * 96)
        *pixelsPerDip = (FLOAT)(x + y) / 192.0f;

        hr = S_OK;
    } while (0);

    return hr;
}


void mmTextRenderer::GeometryDrawingRectangle(
    struct mmTextDrawingContext* pDrawingContext,
    const D2D1_RECT_F& hRectangle,
    FLOAT baselineOriginX,
    FLOAT baselineOriginY,
    const D2D1_COLOR_F& hForegroundColor)
{
    HRESULT hr = S_OK;

    ID2D1SolidColorBrush* pBrushForegroundColor = NULL;
    ID2D1RectangleGeometry* pRectangleGeometry = NULL;
    ID2D1TransformedGeometry* pTransformedGeometry = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (0.0f == hForegroundColor.a)
        {
            break;
        }

        ID2D1RenderTarget* pD2DRenderTarget = (ID2D1RenderTarget*)pDrawingContext->pRenderTarget;
        if (NULL == pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmGraphicsFactory* pGraphicsFactory = pDrawingContext->pGraphicsFactory;
        if (NULL == pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        ID2D1Factory* pD2DFactory = pGraphicsFactory->pD2DFactory;
        if (NULL == pD2DFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        hr = pD2DRenderTarget->CreateSolidColorBrush(hForegroundColor, &pBrushForegroundColor);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateSolidColorBrush failure.", __FUNCTION__, __LINE__);
            break;
        }

        // Initialize a matrix to translate the origin of the underline
        D2D1::Matrix3x2F const matrix = D2D1::Matrix3x2F(
            1.0f, 0.0f,
            0.0f, 1.0f,
            baselineOriginX, baselineOriginY
        );

        hr = pD2DFactory->CreateRectangleGeometry(
            &hRectangle,
            &pRectangleGeometry
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateRectangleGeometry failure.", __FUNCTION__, __LINE__);
            break;
        }

        hr = pD2DFactory->CreateTransformedGeometry(
            pRectangleGeometry,
            &matrix,
            &pTransformedGeometry
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateTransformedGeometry failure.", __FUNCTION__, __LINE__);
            break;
        }

        pD2DRenderTarget->FillGeometry(pTransformedGeometry, pBrushForegroundColor);
    } while (0);

    mmIUnknownSafeRelease(pBrushForegroundColor);
    mmIUnknownSafeRelease(pRectangleGeometry);
    mmIUnknownSafeRelease(pTransformedGeometry);
}

void mmTextRenderer::GeometryDrawingEffect(
    struct mmTextDrawingContext* pDrawingContext,
    ID2D1Geometry* pGeometry,
    struct mmTextFormat* pTextFormat)
{
    HRESULT hr = S_OK;

    ID2D1SolidColorBrush* pBrushForegroundColor = NULL;
    ID2D1SolidColorBrush* pBrushStrokeColor = NULL;
    ID2D1StrokeStyle* pStrokeStyle = NULL;

    D2D1_STROKE_STYLE_PROPERTIES hStrokeStyleProperties;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }

        ID2D1RenderTarget* pD2DRenderTarget = (ID2D1RenderTarget*)pDrawingContext->pRenderTarget;
        if (NULL == pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmGraphicsFactory* pGraphicsFactory = pDrawingContext->pGraphicsFactory;
        if (NULL == pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        ID2D1Factory* pD2DFactory = pGraphicsFactory->pD2DFactory;
        if (NULL == pD2DFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = pDrawingContext->pParagraphFormat;
        if (NULL == pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        switch (pDrawingContext->hPass)
        {
        case 2:
            // pass 2
            //     Background x
            //     Underline  x
            //     Foreground o
            //     Stroke     o
            //     StrikeThru o

            if (0.0f >= pTextFormat->hStrokeWidth && 0.0f < pTextFormat->hForegroundColor[3])
            {
                D2D1_COLOR_F hForegroundColor;
                mmGraphicsPlatform_MakeD2D1ColorF(&hForegroundColor, pTextFormat->hForegroundColor);

                hr = pD2DRenderTarget->CreateSolidColorBrush(hForegroundColor, &pBrushForegroundColor);
                if (FAILED(hr))
                {
                    mmLogger_LogT(gLogger, "%s %d CreateSolidColorBrush failure.", __FUNCTION__, __LINE__);
                    break;
                }

                pD2DRenderTarget->FillGeometry(pGeometry, pBrushForegroundColor);
            }

            if (0.0f != pTextFormat->hStrokeWidth && 0.0f < pTextFormat->hStrokeColor[3])
            {
                D2D1_COLOR_F hStrokeColor;
                mmGraphicsPlatform_MakeD2D1ColorF(&hStrokeColor, pTextFormat->hStrokeColor);

                hr = pD2DRenderTarget->CreateSolidColorBrush(hStrokeColor, &pBrushStrokeColor);
                if (FAILED(hr))
                {
                    mmLogger_LogT(gLogger, "%s %d CreateSolidColorBrush failure.", __FUNCTION__, __LINE__);
                    break;
                }

                float hDisplayDensity = pParagraphFormat->hDisplayDensity;
                UINT32 hLineCap = mmGraphicsPlatform_GetCapStyle(pParagraphFormat->hLineCap);
                UINT32 hLineJoin = mmGraphicsPlatform_GetLineJoin(pParagraphFormat->hLineJoin);
                FLOAT hLineMiterLimit = pParagraphFormat->hLineMiterLimit;

                hStrokeStyleProperties.startCap = (D2D1_CAP_STYLE)hLineCap;
                hStrokeStyleProperties.endCap = (D2D1_CAP_STYLE)hLineCap;
                hStrokeStyleProperties.dashCap = (D2D1_CAP_STYLE)hLineCap;
                hStrokeStyleProperties.lineJoin = (D2D1_LINE_JOIN)hLineJoin;
                hStrokeStyleProperties.miterLimit = (FLOAT)hLineMiterLimit;
                hStrokeStyleProperties.dashStyle = (D2D1_DASH_STYLE)D2D1_DASH_STYLE_SOLID;
                hStrokeStyleProperties.dashOffset = (FLOAT)0.0f;

                hr = pD2DFactory->CreateStrokeStyle(
                    hStrokeStyleProperties,
                    NULL,
                    0,
                    &pStrokeStyle
                );
                if (FAILED(hr))
                {
                    mmLogger_LogT(gLogger, "%s %d CreateStrokeStyle failure.", __FUNCTION__, __LINE__);
                    break;
                }

                FLOAT hStrokeWidth = (FLOAT)fabs(pTextFormat->hStrokeWidth * hDisplayDensity);
                pD2DRenderTarget->DrawGeometry(pGeometry, pBrushStrokeColor, hStrokeWidth, pStrokeStyle);
            }
        break;

        default:
        break;
        }
    } while (0);

    mmIUnknownSafeRelease(pBrushForegroundColor);
    mmIUnknownSafeRelease(pBrushStrokeColor);
    mmIUnknownSafeRelease(pStrokeStyle);
}
