/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRegistry_h__
#define __mmRegistry_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI int mmRegistry_KeyInsert(HKEY hKey, const char* pSubKey, const char* pField, struct mmString* pValue);
MM_EXPORT_NWSI int mmRegistry_KeyUpdate(HKEY hKey, const char* pSubKey, const char* pField, struct mmString* pValue);
MM_EXPORT_NWSI int mmRegistry_KeySelect(HKEY hKey, const char* pSubKey, const char* pField, struct mmString* pValue);
MM_EXPORT_NWSI int mmRegistry_KeyDelete(HKEY hKey, const char* pSubKey, const char* pField);

// HKEY_CURRENT_USER "Software\\mm\\<pAppId>\\<pGroup>"
MM_EXPORT_NWSI void mmRegistry_SubKey(const char* pAppId, const char* pGroup, char hSubKey[128]);

MM_EXPORT_NWSI int mmRegistry_Insert(const char* pAppId, const char* pGroup, const char* pField, struct mmString* pValue);
MM_EXPORT_NWSI int mmRegistry_Update(const char* pAppId, const char* pGroup, const char* pField, struct mmString* pValue);
MM_EXPORT_NWSI int mmRegistry_Select(const char* pAppId, const char* pGroup, const char* pField, struct mmString* pValue);
MM_EXPORT_NWSI int mmRegistry_Delete(const char* pAppId, const char* pGroup, const char* pField);

#include "core/mmSuffix.h"

#endif//__mmDeviceUUID_h__
