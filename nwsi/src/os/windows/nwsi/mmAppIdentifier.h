/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmAppIdentifier_h__
#define __mmAppIdentifier_h__

#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI void mmAppIdentifier_GenerateRandomUUID(char hUUIDBytes[64]);

struct mmAppIdentifier
{
    struct mmString hAppId;          /* App Identifier                  */
    
    struct mmString hUUIDDevice;     /* UUID Device                     */
    struct mmString hUUIDApp;        /* UUID App                        */
};

MM_EXPORT_NWSI void mmAppIdentifier_Init(struct mmAppIdentifier* p);
MM_EXPORT_NWSI void mmAppIdentifier_Destroy(struct mmAppIdentifier* p);

MM_EXPORT_NWSI void mmAppIdentifier_SetAppId(struct mmAppIdentifier* p, const char* pAppId);

MM_EXPORT_NWSI void mmAppIdentifier_OnFinishLaunching(struct mmAppIdentifier* p);
MM_EXPORT_NWSI void mmAppIdentifier_OnBeforeTerminate(struct mmAppIdentifier* p);

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceBytes(struct mmAppIdentifier* p, char hUUIDBytes[64]);
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppBytes(struct mmAppIdentifier* p, char hUUIDBytes[64]);

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceString(struct mmAppIdentifier* p, struct mmString* pUUIDString);
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppString(struct mmAppIdentifier* p, struct mmString* pUUIDString);

#include "core/mmSuffix.h"

#endif//__mmAppIdentifier_h__
