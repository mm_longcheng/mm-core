/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextPlatform.h"

#include "nwsi/mmTextFormat.h"

#include "unicode/mmUBidi.h"

MM_EXPORT_NWSI
DWRITE_FONT_WEIGHT
mmTextPlatform_GetFontWeight(
    int                                            hWeight)
{
    return (DWRITE_FONT_WEIGHT)hWeight;
}

MM_EXPORT_NWSI
DWRITE_FONT_STYLE
mmTextPlatform_GetFontStyle(
    int                                            hStyle)
{
    static const DWRITE_FONT_STYLE gStyleTable[] =
    {
        DWRITE_FONT_STYLE_NORMAL    , // 0 mmFontStyleNormal
        DWRITE_FONT_STYLE_ITALIC    , // 1 mmFontStyleItalic
        DWRITE_FONT_STYLE_OBLIQUE   , // 2 mmFontStyleOblique
    };
    if (0 <= hStyle && hStyle < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hStyle];
    }
    else
    {
        return DWRITE_FONT_STYLE_NORMAL;
    }
}

MM_EXPORT_NWSI
FLOAT
mmTextPlatform_GetFontSize(
    float                                          hSize)
{
    return (FLOAT)hSize;
}

MM_EXPORT_NWSI
void
mmTextPlatform_GetSystemDefaultLocaleName(
    WCHAR                                          hLocaleName[32])
{
    LCID lcid = GetSystemDefaultLCID();
    GetSystemDefaultLocaleName(hLocaleName, lcid);
}

MM_EXPORT_NWSI
int
mmTextPlatform_GetTextBaseDirection(
    const mmUInt16_t*                              pWText,
    size_t                                         hLength)
{
    return mmUBidi_GetBaseDirection((const UChar*)pWText, (int32_t)hLength);
}

MM_EXPORT_NWSI
DWRITE_TEXT_ALIGNMENT
mmTextPlatform_GetTextAlignment(
    int                                            hTextAlignment,
    int                                            hBaseDirection)
{
    static const DWRITE_TEXT_ALIGNMENT gAlignmentTable[] =
    {
        DWRITE_TEXT_ALIGNMENT_LEADING       , // 0 mmTextAlignmentLeft
        DWRITE_TEXT_ALIGNMENT_CENTER        , // 1 mmTextAlignmentCenter
        DWRITE_TEXT_ALIGNMENT_TRAILING      , // 2 mmTextAlignmentRight
        DWRITE_TEXT_ALIGNMENT_JUSTIFIED     , // 3 mmTextAlignmentJustified
    };

    if(mmTextAlignmentNatural == hTextAlignment)
    {
        // UBIDI_NEUTRAL will use left to right TextAlignment.
        UBiDiDirection hDirection = (UBiDiDirection)hBaseDirection;
        return (UBIDI_RTL == hDirection) ? DWRITE_TEXT_ALIGNMENT_TRAILING : DWRITE_TEXT_ALIGNMENT_LEADING;
    }

    if (0 <= hTextAlignment && hTextAlignment < MM_ARRAY_SIZE(gAlignmentTable))
    {
        return gAlignmentTable[hTextAlignment];
    }
    else
    {
        // default fallback LTR.
        return DWRITE_TEXT_ALIGNMENT_LEADING;
    }
}

MM_EXPORT_NWSI
DWRITE_PARAGRAPH_ALIGNMENT
mmTextPlatform_GetTextParagraphAlignment(
    int                                            hParagraphAlignment)
{
    static const DWRITE_PARAGRAPH_ALIGNMENT gAlignmentTable[] =
    {
        DWRITE_PARAGRAPH_ALIGNMENT_NEAR       , // 0 mmTextParagraphAlignmentTop
        DWRITE_PARAGRAPH_ALIGNMENT_CENTER     , // 1 mmTextParagraphAlignmentCenter
        DWRITE_PARAGRAPH_ALIGNMENT_FAR        , // 2 mmTextParagraphAlignmentBottom
    };

    if (0 <= hParagraphAlignment && hParagraphAlignment < MM_ARRAY_SIZE(gAlignmentTable))
    {
        return gAlignmentTable[hParagraphAlignment];
    }
    else
    {
        return DWRITE_PARAGRAPH_ALIGNMENT_NEAR;
    }
}

MM_EXPORT_NWSI
DWRITE_WORD_WRAPPING
mmTextPlatform_GetTextWordWrapping(
    int                                            hLineBreakMode)
{
    static const DWRITE_WORD_WRAPPING gModeTable[] =
    {
        DWRITE_WORD_WRAPPING_WRAP           , // 0 mmTextLineBreakByWordWrapping
        DWRITE_WORD_WRAPPING_CHARACTER      , // 1 mmTextLineBreakByCharWrapping
        DWRITE_WORD_WRAPPING_NO_WRAP        , // 2 mmTextLineBreakByClipping
        DWRITE_WORD_WRAPPING_NO_WRAP        , // 3 mmTextLineBreakByTruncatingHead
        DWRITE_WORD_WRAPPING_NO_WRAP        , // 4 mmTextLineBreakByTruncatingTail
        DWRITE_WORD_WRAPPING_NO_WRAP        , // 5 mmTextLineBreakByTruncatingMiddle
    };

    if (0 <= hLineBreakMode && hLineBreakMode < MM_ARRAY_SIZE(gModeTable))
    {
        return gModeTable[hLineBreakMode];
    }
    else
    {
        return DWRITE_WORD_WRAPPING_WRAP;
    }
}

MM_EXPORT_NWSI
DWRITE_READING_DIRECTION
mmTextPlatform_GetReadingDirection(
    int                                            hWritingDirection,
    int                                            hBaseDirection)
{
    static const DWRITE_READING_DIRECTION gModeTable[] =
    {
        DWRITE_READING_DIRECTION_LEFT_TO_RIGHT      , // 0 mmTextWritingDirectionLeftToRight
        DWRITE_READING_DIRECTION_RIGHT_TO_LEFT      , // 1 mmTextWritingDirectionRightToLeft
    };

    if (mmTextWritingDirectionNatural == hWritingDirection)
    {
        // UBIDI_NEUTRAL will use left to right WritingDirection.
        UBiDiDirection hDirection = (UBiDiDirection)hBaseDirection;
        return (UBIDI_RTL == hDirection) ? DWRITE_READING_DIRECTION_RIGHT_TO_LEFT : DWRITE_READING_DIRECTION_LEFT_TO_RIGHT;
    }

    if (0 <= hWritingDirection && hWritingDirection < MM_ARRAY_SIZE(gModeTable))
    {
        return gModeTable[hWritingDirection];
    }
    else
    {
        // default fallback LTR.
        return DWRITE_READING_DIRECTION_LEFT_TO_RIGHT;
    }
}