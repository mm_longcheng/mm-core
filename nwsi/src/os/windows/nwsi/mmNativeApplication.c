/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNativeApplication.h"

#include "core/mmLogger.h"
#include "core/mmLoggerManager.h"

#include "nwsi/mmPackageAssets.h"

//#include "nwsi/mmOgrePluginRegister.h"

//static const char* MODULE_DIR_VAR_NAME = "CEGUI_MODULE_DIR";
//
//// port api.
//static int setenv(const char* name, const char* value, int overwrite)
//{
//    int errcode = 0;
//    if (!overwrite) 
//    {
//        size_t envsize = 0;
//        errcode = getenv_s(&envsize, NULL, 0, name);
//        if (errcode || envsize) return errcode;
//    }
//    return _putenv_s(name, value);
//}

MM_EXPORT_NWSI void mmNativeApplication_Init(struct mmNativeApplication* p)
{
    mmContextMaster_Init(&p->hContextMaster);
    mmSurfaceMaster_Init(&p->hSurfaceMaster);
    
    mmNativeClipboard_Init(&p->hNativeClipboard);

    mmString_Init(&p->hRenderSystemName);
    
    mmString_Init(&p->hLoggerFileName);
    p->hLoggerLevel = MM_LOG_INFO;
    p->pIActivity = NULL;
    
    mmContextMaster_SetClipboardProvider(&p->hContextMaster, &p->hNativeClipboard.hSuper);

    mmSurfaceMaster_SetContextMaster(&p->hSurfaceMaster, &p->hContextMaster);
    // windows platform limit frame rate by os MainLoop.
    mmSurfaceMaster_SetSmoothMode(&p->hSurfaceMaster, MM_FALSE);

    mmString_Assigns(&p->hRenderSystemName, "OpenGL ES 2.x Rendering Subsystem");
    mmString_Assigns(&p->hLoggerFileName, "nwsi");
}
MM_EXPORT_NWSI void mmNativeApplication_Destroy(struct mmNativeApplication* p)
{
    p->pIActivity = NULL;
    p->hLoggerLevel = MM_LOG_INFO;
    mmString_Destroy(&p->hLoggerFileName);
    
    mmString_Destroy(&p->hRenderSystemName);
    
    mmNativeClipboard_Destroy(&p->hNativeClipboard);

    mmSurfaceMaster_Destroy(&p->hSurfaceMaster);
    mmContextMaster_Destroy(&p->hContextMaster);
}

MM_EXPORT_NWSI void mmNativeApplication_SetAppHandler(struct mmNativeApplication* p, void* pAppHandler)
{
    mmContextMaster_SetAppHandler(&p->hContextMaster, pAppHandler);
}

MM_EXPORT_NWSI void mmNativeApplication_SetModuleName(struct mmNativeApplication* p, const char* pModuleName)
{
    mmContextMaster_SetModuleName(&p->hContextMaster, pModuleName);
}
MM_EXPORT_NWSI void mmNativeApplication_SetRenderSystemName(struct mmNativeApplication* p, const char* pRenderSystemName)
{
    mmString_Assigns(&p->hRenderSystemName, pRenderSystemName);
}
MM_EXPORT_NWSI void mmNativeApplication_SetLoggerFileName(struct mmNativeApplication* p, const char* pLoggerFileName)
{
    mmString_Assigns(&p->hLoggerFileName, pLoggerFileName);
}
MM_EXPORT_NWSI void mmNativeApplication_SetLoggerLevel(struct mmNativeApplication* p, mmUInt32_t hLoggerLevel)
{
    p->hLoggerLevel = hLoggerLevel;
}
MM_EXPORT_NWSI void mmNativeApplication_SetDisplayFrequency(struct mmNativeApplication* p, double hDisplayFrequency)
{
    mmSurfaceMaster_SetDisplayFrequency(&p->hSurfaceMaster, hDisplayFrequency);
}
MM_EXPORT_NWSI void mmNativeApplication_SetActivityMaster(struct mmNativeApplication* p, struct mmIActivity* pIActivity)
{
    p->pIActivity = pIActivity;
    (*(p->pIActivity->SetContext))(p->pIActivity, &p->hContextMaster);
    (*(p->pIActivity->SetSurface))(p->pIActivity, &p->hSurfaceMaster);
    mmSurfaceMaster_SetIActivity(&p->hSurfaceMaster, p->pIActivity);
}

MM_EXPORT_NWSI void mmNativeApplication_OnFinishLaunching(struct mmNativeApplication* p)
{
    struct mmPackageAssets* pPackageAssets = &p->hContextMaster.hPackageAssets;
    //struct mmOgrePluginLoader* pOgrePluginLoader = &p->hContextMaster.hOgrePluginLoader;
    //struct mmPureDynlibLoader* pPureDynlibLoader = &p->hContextMaster.hPureDynlibLoader;
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    
    mmPackageAssets_AttachNativeResource(pPackageAssets);
    
    //// load plug-in first.
    //// cegui need MODULE_DIR_VAR_NAME to location MODULE *.so use dlopen.
    //// MODULE_DIR_VAR_NAME will auto append "/".
    //// std::string hPackageName = pPackageAssets->hPackageName.s;
    //setenv(MODULE_DIR_VAR_NAME, mmString_CStr(&pPackageAssets->hPackageName), 1);

    //mmOgrePluginLoader_SetPluginFolder(pOgrePluginLoader, mmString_CStr(&pPackageAssets->hPluginFolder));
    //mmOgrePluginLoader_SetPluginPrefix(pOgrePluginLoader, "lib");
    //mmOgrePluginLoader_SetPluginSuffix(pOgrePluginLoader, ".dll");
    
    //mmPureDynlibLoader_SetDynlibFolder(pPureDynlibLoader, mmString_CStr(&pPackageAssets->hDynlibFolder));
    //mmPureDynlibLoader_SetDynlibPrefix(pPureDynlibLoader, "");
    //mmPureDynlibLoader_SetDynlibSuffix(pPureDynlibLoader, "");
    
    mmLoggerManager_SetFileName(gLoggerManager, mmString_CStr(&p->hLoggerFileName));
    mmLoggerManager_SetFileFopenMode(gLoggerManager, "wb");
    mmLoggerManager_SetLoggerPath(gLoggerManager, mmString_CStr(&pPackageAssets->hLoggerPath));
    mmLoggerManager_SetLoggerLevel(gLoggerManager, p->hLoggerLevel);
    mmLoggerManager_SetIsConsole(gLoggerManager, 1);
    mmLoggerManager_SetIsImmediately(gLoggerManager, 1);
    mmLoggerManager_FOpen(gLoggerManager);
    
    mmContextMaster_SetRenderSystemName(&p->hContextMaster, mmString_CStr(&p->hRenderSystemName));
    mmContextMaster_SetShaderCacheFolder(&p->hContextMaster, mmString_CStr(&pPackageAssets->hShaderCacheFolder));
    mmContextMaster_SetTextureCacheEnabled(&p->hContextMaster, MM_FALSE);

    //mmOgrePlugin_Register(&p->hContextMaster.hOgrePluginLoader);
    
    mmContextMaster_OnFinishLaunching(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnBeforeTerminate(struct mmNativeApplication* p)
{
    struct mmPackageAssets* pPackageAssets = &p->hContextMaster.hPackageAssets;
    struct mmLoggerManager* gLoggerManager = mmLoggerManager_Instance();
    
    mmContextMaster_OnBeforeTerminate(&p->hContextMaster.hSuper);
    
    //mmOgrePlugin_Unregister(&p->hContextMaster.hOgrePluginLoader);
    
    mmLoggerManager_Flush(gLoggerManager);
    mmLoggerManager_Close(gLoggerManager);
    
    mmPackageAssets_DetachNativeResource(pPackageAssets);
}

MM_EXPORT_NWSI void mmNativeApplication_OnEnterBackground(struct mmNativeApplication* p)
{
    mmContextMaster_OnEnterBackground(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnEnterForeground(struct mmNativeApplication* p)
{
    mmContextMaster_OnEnterForeground(&p->hContextMaster.hSuper);
}

MM_EXPORT_NWSI void mmNativeApplication_OnStart(struct mmNativeApplication* p)
{
    mmContextMaster_OnStart(&p->hContextMaster.hSuper);
    mmSurfaceMaster_OnStart(&p->hSurfaceMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnInterrupt(struct mmNativeApplication* p)
{
    mmSurfaceMaster_OnInterrupt(&p->hSurfaceMaster.hSuper);
    mmContextMaster_OnInterrupt(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnShutdown(struct mmNativeApplication* p)
{
    mmSurfaceMaster_OnShutdown(&p->hSurfaceMaster.hSuper);
    mmContextMaster_OnShutdown(&p->hContextMaster.hSuper);
}
MM_EXPORT_NWSI void mmNativeApplication_OnJoin(struct mmNativeApplication* p)
{
    mmSurfaceMaster_OnJoin(&p->hSurfaceMaster.hSuper);
    mmContextMaster_OnJoin(&p->hContextMaster.hSuper);
}