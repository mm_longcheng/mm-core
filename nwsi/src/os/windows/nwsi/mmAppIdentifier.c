/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


#include "mmAppIdentifier.h"
#include "mmRegistry.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include <TChar.h>
#include "Objbase.h"

#pragma comment(lib,"Ole32.lib")

static const char* MM_APP_IDENTIFIER_PREFS_GROUP = "AppIdentifier";
static const char* MM_APP_IDENTIFIER_PREFS_FIELD_UUID_DEVICE = "UUIDDevice";
static const char* MM_APP_IDENTIFIER_PREFS_FIELD_UUID_APP = "UUIDApp";

static void mmAppIdentifier_UUIDToBytes(struct mmString* pUUID, char hUUIDBytes[64])
{
    mmMemset(hUUIDBytes, 0, 64);
    mmMemcpy(hUUIDBytes, mmString_Data(pUUID), mmString_Size(pUUID));
}

static void mmAppIdentifier_GenerateUUIDDevice(const char* pAppId, char hUUIDBytes[64])
{
    // HKEY_LOCAL_MACHINE
    static const char* Device_GUIDSubKey = "SOFTWARE\\Microsoft\\Cryptography";
    static const char* Device_GUIDValueName = "MachineGuid";

    const char* pGroup = MM_APP_IDENTIFIER_PREFS_GROUP;
    const char* pField = MM_APP_IDENTIFIER_PREFS_FIELD_UUID_DEVICE;

    int hCode = 0;
    struct mmString hUUID;

    mmString_Init(&hUUID);

    do
    {
        hCode = mmRegistry_Select(pAppId, pGroup, pField, &hUUID);
        if (MM_SUCCESS == hCode)
        {
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            break;
        }

        hCode = mmRegistry_KeySelect(HKEY_LOCAL_MACHINE, Device_GUIDSubKey, Device_GUIDValueName, &hUUID);
        if (0 == hCode)
        {
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            mmRegistry_Insert(pAppId, pGroup, pField, &hUUID);
            break;
        }

        {
            mmAppIdentifier_GenerateRandomUUID(hUUIDBytes);
            mmString_Assigns(&hUUID, hUUIDBytes);
            mmRegistry_Insert(pAppId, pGroup, pField, &hUUID);
        }
    } while (0);

    mmString_Destroy(&hUUID);
}

static void mmAppIdentifier_GenerateUUIDAppBytes(const char* pAppId, char hUUIDBytes[64])
{
    const char* pGroup = MM_APP_IDENTIFIER_PREFS_GROUP;
    const char* pField = MM_APP_IDENTIFIER_PREFS_FIELD_UUID_APP;

    struct mmString hUUID;


    int hCode = 0;

    mmString_Init(&hUUID);

    hCode = mmRegistry_Select(pAppId, pGroup, pField, &hUUID);
    if (MM_SUCCESS != hCode)
    {
        mmAppIdentifier_GenerateRandomUUID(hUUIDBytes);
        mmString_Assigns(&hUUID, hUUIDBytes);
        mmRegistry_Insert(pAppId, pGroup, pField, &hUUID);
    }
    else
    {
        mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
    }

    mmString_Destroy(&hUUID);
}

MM_EXPORT_NWSI void mmAppIdentifier_GenerateRandomUUID(char hUUIDBytes[64])
{
    GUID hGUID;
    unsigned char* d4 = hGUID.Data4;
    CoCreateGuid(&hGUID);

    // 856f5555-8064-43e9-8b7e-d04c5a9d6a9a
    // xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx (8-4-4-4-12)
    mmSprintf(hUUIDBytes, "%08X-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
        hGUID.Data1,
        hGUID.Data2,
        hGUID.Data3,
        d4[0], d4[1],
        d4[2], d4[3], d4[4], d4[5], d4[6], d4[7]);
}

MM_EXPORT_NWSI void mmAppIdentifier_Init(struct mmAppIdentifier* p)
{
    mmString_Init(&p->hAppId);

    mmString_Init(&p->hUUIDDevice);
    mmString_Init(&p->hUUIDApp);
    
    mmString_Assigns(&p->hAppId, "");
    
    mmString_Assigns(&p->hUUIDDevice, "");
    mmString_Assigns(&p->hUUIDApp, "");
}
MM_EXPORT_NWSI void mmAppIdentifier_Destroy(struct mmAppIdentifier* p)
{
    mmString_Destroy(&p->hAppId);
    
    mmString_Destroy(&p->hUUIDDevice);
    mmString_Destroy(&p->hUUIDApp);
}

MM_EXPORT_NWSI void mmAppIdentifier_SetAppId(struct mmAppIdentifier* p, const char* pAppId)
{
    mmString_Assigns(&p->hAppId, pAppId);
}

MM_EXPORT_NWSI void mmAppIdentifier_OnFinishLaunching(struct mmAppIdentifier* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    mmAppIdentifier_GetUUIDDeviceString(p, &p->hUUIDDevice);
    mmAppIdentifier_GetUUIDAppString(p, &p->hUUIDApp);
    
    mmLogger_LogI(gLogger, "AppIdentifier AppIdentifier.OnFinishLaunching");
    mmLogger_LogI(gLogger, "AppIdentifier ---------------------+----------------------------------");
    mmLogger_LogI(gLogger, "AppIdentifier           hUUIDDevice: %s", mmString_CStr(&p->hUUIDDevice));
    mmLogger_LogI(gLogger, "AppIdentifier              hUUIDApp: %s", mmString_CStr(&p->hUUIDApp));
    mmLogger_LogI(gLogger, "AppIdentifier ---------------------+----------------------------------");
}
MM_EXPORT_NWSI void mmAppIdentifier_OnBeforeTerminate(struct mmAppIdentifier* p)
{

}

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceBytes(struct mmAppIdentifier* p, char hUUIDBytes[64])
{
    mmAppIdentifier_GenerateUUIDDevice(mmString_CStr(&p->hAppId), hUUIDBytes);
}
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppBytes(struct mmAppIdentifier* p, char hUUIDBytes[64])
{
    mmAppIdentifier_GenerateUUIDAppBytes(mmString_CStr(&p->hAppId), hUUIDBytes);
}

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceString(struct mmAppIdentifier* p, struct mmString* pUUIDString)
{
    char hUUIDBytes[64] = { 0 };
    mmAppIdentifier_GetUUIDDeviceBytes(p, hUUIDBytes);
    mmString_Assigns(pUUIDString, hUUIDBytes);
}
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppString(struct mmAppIdentifier* p, struct mmString* pUUIDString)
{
    char hUUIDBytes[64] = { 0 };
    mmAppIdentifier_GetUUIDAppBytes(p, hUUIDBytes);
    mmString_Assigns(pUUIDString, hUUIDBytes);
}