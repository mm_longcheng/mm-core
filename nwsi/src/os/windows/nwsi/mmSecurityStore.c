/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSecurityStore.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "algorithm/mmBase64.h"

#include "nwsi/mmRegistry.h"

#include <Wincrypt.h>

#pragma comment(lib, "crypt32.lib")

static const char* MM_SECURITY_PREFS_GROUP = "SecurityInfo";

static void mmSecurityStore_EncryptBytes(struct mmString* i, struct mmString* o)
{
    DATA_BLOB hDataI;
    DATA_BLOB hDataO;

    char* pBase64Buffer = NULL;
    size_t pBase64Length = 0;

    hDataI.pbData = (BYTE*)mmString_CStr(i);
    hDataI.cbData = (DWORD)mmString_Size(i);

    hDataO.pbData = NULL;
    hDataO.cbData = 0;

    if (!CryptProtectData(&hDataI, NULL, NULL, NULL, NULL, 0, &hDataO))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "EncryptBytes failure.");
    }
    else
    {
        pBase64Length = mmBase64_EncodeLength((size_t)(hDataO.cbData));
        pBase64Buffer = mmMalloc(pBase64Length);
        mmBase64_Encode(pBase64Buffer, (const char*)hDataO.pbData, (int)(hDataO.cbData));

        mmString_Assignsn(o, pBase64Buffer, pBase64Length);
    }

    mmFree(pBase64Buffer);
    LocalFree(hDataO.pbData);
}

static void mmSecurityStore_DecryptBytes(struct mmString* i, struct mmString* o)
{
    DATA_BLOB hDataI;
    DATA_BLOB hDataO;

    size_t uLength = 0;
    const char* uBuffer = NULL;
    char* pBase64Buffer = NULL;
    size_t pBase64Length = 0;

    hDataO.pbData = NULL;
    hDataO.cbData = 0;

    uLength = mmString_Size(i);
    uBuffer = mmString_CStr(i);
    pBase64Length = mmBase64_DecodeLength(uLength);
    pBase64Buffer = (char*)mmMalloc(pBase64Length);
    mmBase64_Decode(pBase64Buffer, uBuffer, uLength);

    hDataI.pbData = (BYTE*)pBase64Buffer;
    hDataI.cbData = (DWORD)pBase64Length;

    if (!CryptUnprotectData(&hDataI, NULL, NULL, NULL, NULL, 0, &hDataO))
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogI(gLogger, "DecryptBytes failure.");
    }
    else
    {
        mmString_Assignsn(o, hDataO.pbData, hDataO.cbData);
    }

    mmFree(pBase64Buffer);
    LocalFree(hDataO.pbData);
}

MM_EXPORT_NWSI void mmSecurityStore_Init(struct mmSecurityStore* p)
{
    mmString_Init(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    mmString_Assigns(&p->hAppId, "");
}
MM_EXPORT_NWSI void mmSecurityStore_Destroy(struct mmSecurityStore* p)
{
    mmString_Destroy(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;
}

MM_EXPORT_NWSI void mmSecurityStore_SetAppId(struct mmSecurityStore* p, const char* pAppId)
{
    mmString_Assigns(&p->hAppId, pAppId);
}

MM_EXPORT_NWSI void mmSecurityStore_OnFinishLaunching(struct mmSecurityStore* p)
{
    // nothing
}
MM_EXPORT_NWSI void mmSecurityStore_OnBeforeTerminate(struct mmSecurityStore* p)
{
    // nothing
}

MM_EXPORT_NWSI void mmSecurityStore_Insert(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    struct mmString hValue;
    struct mmString hBytes;

    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    
    mmString_MakeWeaks(&hValue, pSecret);

    mmString_Init(&hBytes);

    mmSecurityStore_EncryptBytes(&hValue, &hBytes);

    mmRegistry_Insert(mmString_CStr(&p->hAppId), pGroup, pField, &hBytes);

    mmString_Destroy(&hBytes);
}
MM_EXPORT_NWSI void mmSecurityStore_Update(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    struct mmString hValue;
    struct mmString hBytes;

    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    
    mmString_MakeWeaks(&hValue, pSecret);

    mmString_Init(&hBytes);

    mmSecurityStore_EncryptBytes(&hValue, &hBytes);

    mmRegistry_Update(mmString_CStr(&p->hAppId), pGroup, pField, &hBytes);

    mmString_Destroy(&hBytes);
}
MM_EXPORT_NWSI void mmSecurityStore_Select(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    struct mmString hValue;
    struct mmString hBytes;

    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    
    mmString_Init(&hValue);
    mmString_Init(&hBytes);

    mmRegistry_Select(mmString_CStr(&p->hAppId), pGroup, pField, &hBytes);

    mmSecurityStore_DecryptBytes(&hBytes, &hValue);

    mmMemset(pSecret, 0, 64);
    mmMemcpy(pSecret, mmString_CStr(&hValue), mmString_Size(&hValue));
    
    mmString_Destroy(&hBytes);
    mmString_Destroy(&hValue);
}
MM_EXPORT_NWSI void mmSecurityStore_Delete(struct mmSecurityStore* p, const char* pMember)
{
    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    mmRegistry_Delete(mmString_CStr(&p->hAppId), pGroup, pField);
}

