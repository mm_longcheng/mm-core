/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmNativeClipboard.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "nwsi/mmUTFHelper.h"

static 
void 
__static_mmNativeClipboard_SetByteBuffer(
    struct mmClipboardProvider*                    pSuper,
    const char*                                    pMimeType,
    struct mmByteBuffer*                           pByteBuffer)
{
    int hIsClipboardOpened = 0;

    do
    {
        HGLOBAL pHMemory = NULL;
        LPTSTR pCodeString = NULL;
        size_t hHMemeryLength = 0;
        HANDLE pHandle = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmUInt8_t* buffer = pByteBuffer->buffer + pByteBuffer->offset;
        size_t length = pByteBuffer->length;

        struct mmNativeClipboard* p = (struct mmNativeClipboard*)(pSuper);

        if (0 != strcmp("text/plain", pMimeType))
        {
            break;
        }

        if (NULL == buffer || 0 == length)
        {
            break;
        }

        if (!OpenClipboard(NULL))
        {
            // can not open the clipboard.
            mmLogger_LogW(gLogger, "mmNativeClipboard.SetByteBuffer OpenClipboard failure.");
            break;
        }

        hIsClipboardOpened = 1;

        if (!EmptyClipboard())
        {
            // can not empty current clipboard.
            mmLogger_LogW(gLogger, "mmNativeClipboard.SetByteBuffer EmptyClipboard failure.");
            break;
        }

        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, length);
        mmMemcpy(p->hStreambuf.buff, buffer, length);

        // when we copy unicode string into clipboard we must copy the \0\0 terminator together.
        // so len is (character size + 1) * sizeof(mmUInt16_t).
        hHMemeryLength = (length + sizeof(mmUInt16_t));
        pHMemory = GlobalAlloc(GMEM_MOVEABLE, hHMemeryLength);
        if (NULL == pHMemory)
        {
            // can not allocate GMEM_MOVEABLE Memory.
            mmLogger_LogW(gLogger, "mmNativeClipboard.SetByteBuffer GlobalAlloc Memory failure.");
            break;
        }

        pCodeString = (LPTSTR)GlobalLock(pHMemory);
        if (NULL == pCodeString)
        {
            // lock the GMEM_MOVEABLE Memory failure.
            mmLogger_LogW(gLogger, "mmNativeClipboard.SetByteBuffer GlobalLock Memory failure.");
            break;
        }

        // copy memery.
        mmMemcpy(pCodeString, p->hStreambuf.buff, length);

        // memset \0\0 terminator.
        mmMemset(pCodeString + length, 0, sizeof(mmUInt16_t));

        // unlock the GMEM_MOVEABLE Memory.
        GlobalUnlock(pHMemory);

        // Assign to clipboard data.
        pHandle = SetClipboardData(CF_UNICODETEXT, pHMemory);
        if (NULL == pHandle)
        {
            mmLogger_LogW(gLogger, "mmNativeClipboard.SetByteBuffer SetClipboardData failure.");
            break;
        }

        mmLogger_LogV(gLogger, "mmNativeClipboard.SetByteBuffer success.");
    } while (0);

    if (1 == hIsClipboardOpened)
    {
        // close clipboard.
        CloseClipboard();
    }
}

static 
void 
__static_mmNativeClipboard_GetByteBuffer(
    struct mmClipboardProvider*                    pSuper,
    const char*                                    pMimeType,
    struct mmByteBuffer*                           pByteBuffer)
{
    int hIsClipboardOpened = 0;

    do
    {
        HGLOBAL pHMemory = NULL;
        LPTSTR pCodeString = NULL;
        size_t hCodeStringLength = 0;
        size_t length = 0;

        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmNativeClipboard* p = (struct mmNativeClipboard*)(pSuper);

        if (0 != strcmp("text/plain", pMimeType))
        {
            break;
        }

        if (!OpenClipboard(NULL))
        {
            mmLogger_LogW(gLogger, "mmNativeClipboard.GetByteBuffer OpenClipboard failure.");
            break;
        }

        hIsClipboardOpened = 1;

        if (!IsClipboardFormatAvailable(CF_UNICODETEXT))
        {
            mmLogger_LogW(gLogger, "mmNativeClipboard.GetByteBuffer IsClipboardFormatAvailable failure.");
            break;
        }

        pHMemory = (HGLOBAL)GetClipboardData(CF_UNICODETEXT);
        if (NULL == pHMemory)
        {
            mmLogger_LogW(gLogger, "mmNativeClipboard.GetByteBuffer GetClipboardData failure.");
            break;
        }

        pCodeString = (LPTSTR)GlobalLock(pHMemory);
        if (NULL == pCodeString)
        {
            // lock the GMEM_MOVEABLE Memory failure.
            mmLogger_LogW(gLogger, "mmNativeClipboard.GetByteBuffer GlobalLock Memory failure.");
            break;
        }

        // copy memery.
        hCodeStringLength = wcslen((const wchar_t*)pCodeString);
        length = hCodeStringLength * sizeof(mmUInt16_t);

        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, length);
        mmMemcpy(p->hStreambuf.buff, pCodeString, length);

        // unlock the GMEM_MOVEABLE Memory.
        GlobalUnlock(pHMemory);

        pByteBuffer->buffer = p->hStreambuf.buff;
        pByteBuffer->offset = 0;
        pByteBuffer->length = length;

        mmLogger_LogV(gLogger, "mmNativeClipboard.GetByteBuffer success.");
    } while (0);

    if (1 == hIsClipboardOpened)
    {
        // close clipboard.
        CloseClipboard();
    }
}

MM_EXPORT_NWSI void mmNativeClipboard_Init(struct mmNativeClipboard* p)
{
    mmStreambuf_Init(&p->hStreambuf);

    p->hSuper.SetByteBuffer = &__static_mmNativeClipboard_SetByteBuffer;
    p->hSuper.GetByteBuffer = &__static_mmNativeClipboard_GetByteBuffer;
}
MM_EXPORT_NWSI void mmNativeClipboard_Destroy(struct mmNativeClipboard* p)
{
    p->hSuper.SetByteBuffer = &__static_mmNativeClipboard_SetByteBuffer;
    p->hSuper.GetByteBuffer = &__static_mmNativeClipboard_GetByteBuffer;

    mmStreambuf_Destroy(&p->hStreambuf);
}
