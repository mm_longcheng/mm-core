/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextParagraph.h"
#include "mmTextDrawingContext.h"
#include "mmTextUtf16String.h"
#include "mmTextDrawingEffect.h"
#include "mmTextRenderer.h"
#include "mmTextPlatform.h"

#include "mmGraphicsPlatform.h"
#include "mmGraphicsFactory.h"

#include "core/mmLogger.h"
#include "core/mmAlloc.h"

#include "math/mmMath.h"
#include "math/mmVector2.h"
#include "math/mmVector4.h"

#include "nwsi/mmUTFHelper.h"

#include <assert.h>

#include <d2d1.h>
#include <d2d1_1.h>
#include <d2d1effects.h>

#include <wincodec.h>
#include <dwrite.h>
#include <dwrite_3.h>

#pragma comment(lib,"dxguid.lib")

static 
void 
__static_mmTextParagraph_UpdateTextSpan(
    struct mmTextParagraph*                        p, 
    struct mmTextSpan*                             pTextSpan);

static 
void 
__static_mmTextParagraph_DrawText(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_DrawTextSpan(
    struct mmTextParagraph*                        p, 
    struct mmTextSpan*                             pTextSpan);

static
void
__static_mmTextParagraph_DrawCaret(
    struct mmTextParagraph*                        p,
    ID2D1RenderTarget*                             pD2DRenderTarget);

MM_EXPORT_NWSI
void
mmTextParagraph_Init(
    struct mmTextParagraph*                        p)
{
    p->pParagraphFormat = NULL;
    p->pTextSpannable = NULL;
    p->pText = NULL;
    p->pDrawingContext = NULL;
    p->pDWriteTextFormat = NULL;
    p->pDWriteTextLayout = NULL;
    p->pDWriteTextRenderer = NULL;

    mmTextFactory_CreateTextRenderer(&p->pDWriteTextRenderer);
}

MM_EXPORT_NWSI
void
mmTextParagraph_Destroy(
    struct mmTextParagraph*                        p)
{
    mmIUnknownSafeRelease(p->pDWriteTextRenderer);

    mmTextParagraph_DeleteTextLayout(p);
    mmTextParagraph_DeleteTextFormat(p);

    p->pParagraphFormat = NULL;
    p->pTextSpannable = NULL;
    p->pText = NULL;
    p->pDrawingContext = NULL;
    p->pDWriteTextFormat = NULL;
    p->pDWriteTextLayout = NULL;
    p->pDWriteTextRenderer = NULL;
}

MM_EXPORT_NWSI
void
mmTextParagraph_SetTextParagraphFormat(
    struct mmTextParagraph*                        p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    p->pParagraphFormat = pParagraphFormat;
}

MM_EXPORT_NWSI
void
mmTextParagraph_SetTextSpannable(
    struct mmTextParagraph*                        p,
    struct mmTextSpannable*                        pTextSpannable)
{
    p->pTextSpannable = pTextSpannable;
}

MM_EXPORT_NWSI
void
mmTextParagraph_SetText(
    struct mmTextParagraph*                        p,
    struct mmTextUtf16String*                      pText)
{
    p->pText = pText;
}

MM_EXPORT_NWSI
void
mmTextParagraph_SetDrawingContext(
    struct mmTextParagraph*                        p,
    struct mmTextDrawingContext*                   pDrawingContext)
{
    p->pDrawingContext = pDrawingContext;
}

MM_EXPORT_NWSI
void
mmTextParagraph_OnFinishLaunching(
    struct mmTextParagraph*                        p)
{
    mmTextParagraph_UpdateTextImplement(p);
    mmTextParagraph_UpdateTextFormat(p);
    mmTextParagraph_UpdateTextLayout(p);
    mmTextParagraph_UpdateFontFeature(p);
    mmTextParagraph_UpdateTextSpannable(p);
}

MM_EXPORT_NWSI
void
mmTextParagraph_OnBeforeTerminate(
    struct mmTextParagraph*                        p)
{
    mmTextParagraph_DeleteTextLayout(p);
    mmTextParagraph_DeleteTextFormat(p);
}

MM_EXPORT_NWSI
void
mmTextParagraph_UpdateDrawingContext(
    struct mmTextParagraph*                        p)
{

}

MM_EXPORT_NWSI
void
mmTextParagraph_UpdateTextImplement(
    struct mmTextParagraph*                        p)
{

}

MM_EXPORT_NWSI
void
mmTextParagraph_UpdateTextFormat(
    struct mmTextParagraph*                        p)
{
    HRESULT hr = S_OK;

    IDWriteInlineObject* pInlineObject = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        struct mmGraphicsFactory* pGraphicsFactory = p->pDrawingContext->pGraphicsFactory;
        if (NULL == pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;

        struct IDWriteFontCollection1* pFontCollection = pGraphicsFactory->pFontCollection;
        struct IDWriteFactory* pDWriteFactory = pGraphicsFactory->pDWriteFactory;
        if (NULL == pDWriteFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        const WCHAR* pFontFamilyName = (const WCHAR*)mmUtf16String_CStr(&pMasterFormat->hFontFamilyName);
        DWRITE_FONT_WEIGHT hFontWeight = mmTextPlatform_GetFontWeight(pMasterFormat->hFontWeight);
        DWRITE_FONT_STYLE hFontStyle = mmTextPlatform_GetFontStyle(pMasterFormat->hFontStyle);
        FLOAT hFontSize = mmTextPlatform_GetFontSize(pMasterFormat->hFontSize);
        FLOAT hFontPixelSize = hFontSize * pParagraphFormat->hDisplayDensity;

        WCHAR hLocaleName[32] = { 0 };
        mmTextPlatform_GetSystemDefaultLocaleName(hLocaleName);

        // Font collection (NULL sets it to use the system font collection).
        hr = pDWriteFactory->CreateTextFormat(
            pFontFamilyName,            // Font family name
            pFontCollection,            // Font collection
            hFontWeight,                // Font weight
            hFontStyle,                 // Font style
            DWRITE_FONT_STRETCH_NORMAL, // Font stretch
            hFontPixelSize,             // Font size
            hLocaleName,                // Locale name
            &p->pDWriteTextFormat       // IDWriteTextFormat
        );

        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateTextFormat failure.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmUtf16String* pString = &p->pText->hString;
        size_t length = mmUtf16String_Size(pString);
        const mmUtf16_t* buffer = mmUtf16String_Data(pString);
        int hBaseDirection = mmTextPlatform_GetTextBaseDirection((const mmUInt16_t*)buffer, length);

        DWRITE_TEXT_ALIGNMENT hTextAlignment = mmTextPlatform_GetTextAlignment(
            pParagraphFormat->hTextAlignment,
            hBaseDirection);
        hr = p->pDWriteTextFormat->SetTextAlignment(hTextAlignment);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d SetTextAlignment failure.", __FUNCTION__, __LINE__);
            break;
        }

        DWRITE_PARAGRAPH_ALIGNMENT hParagraphAlignment = mmTextPlatform_GetTextParagraphAlignment(
            pParagraphFormat->hParagraphAlignment);
        hr = p->pDWriteTextFormat->SetParagraphAlignment(hParagraphAlignment);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d SetParagraphAlignment failure.", __FUNCTION__, __LINE__);
            break;
        }

        DWRITE_WORD_WRAPPING hWordWrapping = mmTextPlatform_GetTextWordWrapping(
            pParagraphFormat->hLineBreakMode);
        hr = p->pDWriteTextFormat->SetWordWrapping(hWordWrapping);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d SetWordWrapping failure.", __FUNCTION__, __LINE__);
            break;
        }

        if (pParagraphFormat->hLineBreakMode >= mmTextLineBreakByTruncatingHead &&
            pParagraphFormat->hLineBreakMode <= mmTextLineBreakByTruncatingMiddle)
        {
            hr = pDWriteFactory->CreateEllipsisTrimmingSign(p->pDWriteTextFormat, &pInlineObject);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d CreateEllipsisTrimmingSign failure.", __FUNCTION__, __LINE__);
                break;
            }

            DWRITE_TRIMMING hTrimming = { DWRITE_TRIMMING_GRANULARITY_CHARACTER, 1, 10, };
            hr = p->pDWriteTextFormat->SetTrimming(&hTrimming, pInlineObject);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetTrimming failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
        
        DWRITE_READING_DIRECTION hReadingDirection = mmTextPlatform_GetReadingDirection(
            pParagraphFormat->hWritingDirection,
            hBaseDirection);
        hr = p->pDWriteTextFormat->SetReadingDirection(hReadingDirection);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d SetReadingDirection failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    mmIUnknownSafeRelease(pInlineObject);
}

MM_EXPORT_NWSI
void
mmTextParagraph_UpdateTextLayout(
    struct mmTextParagraph*                        p)
{
    HRESULT hr = S_OK;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        struct mmGraphicsFactory* pGraphicsFactory = p->pDrawingContext->pGraphicsFactory;
        if (NULL == pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;

        struct IDWriteFactory* pDWriteFactory = pGraphicsFactory->pDWriteFactory;
        if (NULL == pDWriteFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        // hLayoutRect Parameters already include dpi scaling.
        FLOAT hLayoutW = pParagraphFormat->hLayoutRect[2];
        FLOAT hLayoutH = pParagraphFormat->hLayoutRect[3];

        struct mmUtf16String* pString = &p->pText->hString;
        size_t length = mmUtf16String_Size(pString);
        const mmUtf16_t* buffer = mmUtf16String_Data(pString);

        if (1 == pParagraphFormat->hAlignBaseline)
        {
            float wsize[2];
            FLOAT minWidth;
            DWRITE_WORD_WRAPPING hWrapping;
            DWRITE_TEXT_METRICS textMetrics;
            DWRITE_LINE_METRICS lineMetrics;
            UINT32 actualLineCount;

            // Create a text layout using the text format.
            hr = pDWriteFactory->CreateTextLayout(
                (const WCHAR*)buffer,           // The string to be laid out and formatted.
                (UINT32)length,                 // The length of the string.
                p->pDWriteTextFormat,           // The text format to apply to the string (contains font information, etc).
                0,                              // The width of the layout box.
                0,                              // The height of the layout box.
                &p->pDWriteTextLayout           // The IDWriteTextLayout interface pointer.
            );
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d CreateTextLayout failure.", __FUNCTION__, __LINE__);
                break;
            }

            // storage current wrapping setting.
            hWrapping = p->pDWriteTextFormat->GetWordWrapping();
            // set no wrap for single line layout.
            hr = p->pDWriteTextLayout->SetWordWrapping(DWRITE_WORD_WRAPPING_NO_WRAP);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetWordWrapping failure.", __FUNCTION__, __LINE__);
                break;
            }

            hr = p->pDWriteTextLayout->GetMetrics(&textMetrics);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d GetMetrics failure.", __FUNCTION__, __LINE__);
                break;
            }

            hr = p->pDWriteTextLayout->DetermineMinWidth(&minWidth);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d DetermineMinWidth failure.", __FUNCTION__, __LINE__);
                break;
            }

            wsize[0] = mmMathMax((float)minWidth, (float)textMetrics.width);
            wsize[0] = hLayoutW <= 0.0f ? wsize[0] : hLayoutW;

            hr = p->pDWriteTextLayout->SetMaxWidth(wsize[0]);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d DetermineMinWidth failure.", __FUNCTION__, __LINE__);
                break;
            }

            // here only single line.
            // when the string is empty the line count at least 1.
            hr = p->pDWriteTextLayout->GetLineMetrics(&lineMetrics, 1, &actualLineCount);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d GetLineMetrics failure.", __FUNCTION__, __LINE__);
                break;
            }

            wsize[1] = mmMathMax((float)lineMetrics.height, (float)textMetrics.height);
            wsize[1] = hLayoutH <= 0.0f ? wsize[1] : hLayoutH;

            hr = p->pDWriteTextLayout->SetMaxHeight(wsize[1]);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d DetermineMinWidth failure.", __FUNCTION__, __LINE__);
                break;
            }

            // recovery wrapping setting.
            hr = p->pDWriteTextLayout->SetWordWrapping(hWrapping);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetWordWrapping failure.", __FUNCTION__, __LINE__);
                break;
            }

            pParagraphFormat->hLineDescent = lineMetrics.height - lineMetrics.baseline;
            mmVec2Assign(pParagraphFormat->hSuitableSize, wsize);

            mmVec2Make(pParagraphFormat->hWindowSize, wsize[0], wsize[1]);
            mmVec4Make(pParagraphFormat->hLayoutRect, 0, 0, wsize[0], wsize[1]);
        }
        else
        {
            float wsize[2];
            DWRITE_LINE_METRICS* lineMetricsPtr;
            DWRITE_LINE_METRICS lineMetrics;
            UINT32 actualLineCount;

            // Create a text layout using the text format.
            hr = pDWriteFactory->CreateTextLayout(
                (const WCHAR*)buffer,           // The string to be laid out and formatted.
                (UINT32)length,                 // The length of the string.
                p->pDWriteTextFormat,           // The text format to apply to the string (contains font information, etc).
                hLayoutW,                       // The width of the layout box.
                hLayoutH,                       // The height of the layout box.
                &p->pDWriteTextLayout           // The IDWriteTextLayout interface pointer.
            );
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d CreateTextLayout failure.", __FUNCTION__, __LINE__);
                break;
            }

            mmVec2Assign(wsize, pParagraphFormat->hWindowSize);

            // first get the actual line count.
            hr = p->pDWriteTextLayout->GetLineMetrics(NULL, 0, &actualLineCount);
            if (FAILED(hr) && HRESULT_FROM_WIN32(ERROR_INSUFFICIENT_BUFFER) != hr)
            {
                mmLogger_LogT(gLogger, "%s %d GetLineMetrics failure.", __FUNCTION__, __LINE__);
                break;
            }

            // when the string is empty the line count at least 1.
            if (0 == actualLineCount)
            {
                // line count is 0.
                break;
            }

            if (1 == actualLineCount)
            {
                hr = p->pDWriteTextLayout->GetLineMetrics(&lineMetrics, 1, &actualLineCount);
                if (FAILED(hr))
                {
                    mmLogger_LogT(gLogger, "%s %d GetLineMetrics failure.", __FUNCTION__, __LINE__);
                    break;
                }
            }
            else
            {
                lineMetricsPtr = (DWRITE_LINE_METRICS*)mmMalloc(sizeof(DWRITE_LINE_METRICS) * actualLineCount);
                if (NULL == lineMetricsPtr)
                {
                    mmLogger_LogE(gLogger, "%s %d lineMetricsPtr is invalid.", __FUNCTION__, __LINE__);
                    break;
                }

                hr = p->pDWriteTextLayout->GetLineMetrics(lineMetricsPtr, actualLineCount, &actualLineCount);
                // use the first line metrics value.
                lineMetrics = lineMetricsPtr[0];
                mmFree(lineMetricsPtr);

                if (FAILED(hr))
                {
                    mmLogger_LogT(gLogger, "%s %d GetLineMetrics failure.", __FUNCTION__, __LINE__);
                    break;
                }
            }

            pParagraphFormat->hLineDescent = lineMetrics.height - lineMetrics.baseline;
            mmVec2Assign(pParagraphFormat->hSuitableSize, wsize);
        }

    } while (0);
}

MM_EXPORT_NWSI
void
mmTextParagraph_UpdateFontFeature(
    struct mmTextParagraph*                        p)
{
    HRESULT hr = S_OK;

    IDWriteTypography* pTypography = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        struct mmGraphicsFactory* pGraphicsFactory = p->pDrawingContext->pGraphicsFactory;
        if (NULL == pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct IDWriteFactory* pDWriteFactory = pGraphicsFactory->pDWriteFactory;
        if (NULL == pDWriteFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmRbtreeU32U32* pFontFeatures = &pParagraphFormat->hFontFeatures;

        if (0 == pFontFeatures->size)
        {
            break;
        }

        hr = pDWriteFactory->CreateTypography(&pTypography);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateTypography failure.", __FUNCTION__, __LINE__);
            break;
        }

        DWRITE_FONT_FEATURE hFontFeature;

        struct mmRbNode* n = NULL;
        struct mmRbtreeU32U32Iterator* it = NULL;
        //
        n = mmRb_First(&pFontFeatures->rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeU32U32Iterator, n);
            n = mmRb_Next(n);

            // Set the stylistic set.
            hFontFeature.nameTag = (DWRITE_FONT_FEATURE_TAG)it->k;
            hFontFeature.parameter = (UINT32)it->v;
            // ignore the result.
            pTypography->AddFontFeature(hFontFeature);
        }

        struct mmUtf16String* pString = &p->pText->hString;
        size_t length = mmUtf16String_Size(pString);

        DWRITE_TEXT_RANGE hTextRange = { 0, (UINT32)length, };

        hr = p->pDWriteTextLayout->SetTypography(pTypography, hTextRange);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d SetTypography failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    mmIUnknownSafeRelease(pTypography);
}

MM_EXPORT_NWSI
void
mmTextParagraph_UpdateTextSpannable(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pTextSpannable)
        {
            mmLogger_LogT(gLogger, "%s %d pTextSpannable is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pDWriteTextLayout)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteTextLayout is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmListVpt* pAttributes = &p->pTextSpannable->hAttributes;

        if (0 == pAttributes->size)
        {
            break;
        }

        struct mmTextSpan* e = NULL;

        struct mmListHead* next = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        next = pAttributes->l.next;
        while (next != &pAttributes->l)
        {
            curr = next;
            next = next->next;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            e = (struct mmTextSpan*)it->v;

            __static_mmTextParagraph_UpdateTextSpan(p, e);
        }
    } while (0);
}

MM_EXPORT_NWSI
void
mmTextParagraph_DeleteTextFormat(
    struct mmTextParagraph*                        p)
{
    mmIUnknownSafeRelease(p->pDWriteTextFormat);
    p->pDWriteTextFormat = NULL;
}

MM_EXPORT_NWSI
void
mmTextParagraph_DeleteTextLayout(
    struct mmTextParagraph*                        p)
{
    mmIUnknownSafeRelease(p->pDWriteTextLayout);
    p->pDWriteTextLayout = NULL;
}

MM_EXPORT_NWSI
void
mmTextParagraph_Draw(
    struct mmTextParagraph*                        p)
{
    HRESULT hr = S_OK;

    ID2D1DeviceContext* pD2D1DeviceContext = NULL;

    ID2D1Bitmap* pBitmap = NULL;
    ID2D1Effect* pShadowEffect = NULL;
    ID2D1Effect* pAffineTransformEffect = NULL;
    ID2D1Effect* pCompositeEffect = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pDWriteTextLayout)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteTextLayout is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pDWriteTextRenderer)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteTextRenderer is null.", __FUNCTION__, __LINE__);
            break;
        }

        ID2D1RenderTarget* pD2DRenderTarget = (ID2D1RenderTarget*)p->pDrawingContext->pRenderTarget;
        if (NULL == pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        float hDisplayDensity = pParagraphFormat->hDisplayDensity;
        FLOAT hOriginX = pParagraphFormat->hLayoutRect[0];
        FLOAT hOriginY = pParagraphFormat->hLayoutRect[1];
        FLOAT hShadowOffsetX = pParagraphFormat->hShadowOffset[0] * hDisplayDensity;
        FLOAT hShadowOffsetY = pParagraphFormat->hShadowOffset[1] * hDisplayDensity;

        D2D1_MATRIX_3X2_F hMatrix = D2D1::Matrix3x2F::Identity();
        D2D1_COLOR_F hClearColor = D2D1::ColorF(0.0f, 0.0f, 0.0f, 0.0f);

        pD2DRenderTarget->BeginDraw();

        pD2DRenderTarget->SetTransform(hMatrix);

        pD2DRenderTarget->Clear(hClearColor);

        mmTextDrawingContext_SetPass(p->pDrawingContext, 1);
        p->pDWriteTextLayout->Draw(p->pDrawingContext, p->pDWriteTextRenderer, hOriginX, hOriginY);

        mmTextDrawingContext_SetPass(p->pDrawingContext, 2);
        p->pDWriteTextLayout->Draw(p->pDrawingContext, p->pDWriteTextRenderer, hOriginX, hOriginY);

        pD2DRenderTarget->EndDraw();

        // Shadow Transparent can not decide skip the following process.
        // We have to handle hBackgroundColor.
        // (0.0f == pParagraphFormat->hShadowBlur || 0.0f == pParagraphFormat->hShadowColor[3])
        // can not break here.

        IWICBitmap* pWICBitmap = (IWICBitmap*)p->pDrawingContext->pBitmap;
        if (NULL == pWICBitmap)
        {
            mmLogger_LogT(gLogger, "%s %d pWICBitmap is null.", __FUNCTION__, __LINE__);
            break;
        }

        FLOAT hShadowBlur = pParagraphFormat->hShadowBlur * hDisplayDensity;
        D2D1_MATRIX_3X2_F hShadowMatrix = D2D1::Matrix3x2F::Translation(hShadowOffsetX, hShadowOffsetY);
        D2D1_VECTOR_4F hShadowColor4F;
        mmGraphicsPlatform_MakeD2D1Vector4F(&hShadowColor4F, pParagraphFormat->hShadowColor);

        hr = pD2DRenderTarget->QueryInterface(&pD2D1DeviceContext);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d QueryInterface pD2D1DeviceContext failure.", __FUNCTION__, __LINE__);
            break;
        }

        hr = pD2DRenderTarget->CreateBitmapFromWicBitmap(pWICBitmap, &pBitmap);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateBitmapFromWicBitmap failure.", __FUNCTION__, __LINE__);
            break;
        }

        hr = pD2D1DeviceContext->CreateEffect(CLSID_D2D1Shadow, &pShadowEffect);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateEffect pShadowEffect failure.", __FUNCTION__, __LINE__);
            break;
        }
        hr = pD2D1DeviceContext->CreateEffect(CLSID_D2D12DAffineTransform, &pAffineTransformEffect);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateEffect pAffineTransformEffect failure.", __FUNCTION__, __LINE__);
            break;
        }
        hr = pD2D1DeviceContext->CreateEffect(CLSID_D2D1Composite, &pCompositeEffect);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateEffect pCompositeEffect failure.", __FUNCTION__, __LINE__);
            break;
        }
        pShadowEffect->SetInput(0, pBitmap);
        pShadowEffect->SetValue(D2D1_SHADOW_PROP_BLUR_STANDARD_DEVIATION, hShadowBlur);
        pShadowEffect->SetValue(D2D1_SHADOW_PROP_COLOR, hShadowColor4F);

        pAffineTransformEffect->SetInputEffect(0, pShadowEffect);
        pAffineTransformEffect->SetValue(D2D1_2DAFFINETRANSFORM_PROP_TRANSFORM_MATRIX, hShadowMatrix);

        pCompositeEffect->SetInputEffect(0, pAffineTransformEffect);
        pCompositeEffect->SetInput(1, pBitmap);

        pD2D1DeviceContext->BeginDraw();

        pD2D1DeviceContext->Clear(hClearColor);

        mmTextDrawingContext_SetPass(p->pDrawingContext, 0);
        __static_mmTextParagraph_DrawText(p);

        pD2D1DeviceContext->DrawImage(pCompositeEffect);

        __static_mmTextParagraph_DrawCaret(p, pD2DRenderTarget);

        pD2D1DeviceContext->EndDraw();
    } while (0);

    mmIUnknownSafeRelease(pD2D1DeviceContext);
    mmIUnknownSafeRelease(pBitmap);
    mmIUnknownSafeRelease(pShadowEffect);
    mmIUnknownSafeRelease(pAffineTransformEffect);
    mmIUnknownSafeRelease(pCompositeEffect);
}

MM_EXPORT_NWSI
void
mmTextParagraph_HitTestPoint(
    const struct mmTextParagraph*                  p,
    float                                          point[2],
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        (*isTrailingHit) = 0;
        (*isInside) = 0;
        mmTextHitMetrics_Reset(pMetrics);

        if (NULL == p->pDWriteTextLayout)
        {

            mmLogger_LogT(gLogger, "%s %d pDWriteTextLayout is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        {
            struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
            FLOAT hOriginX = pParagraphFormat->hLayoutRect[0];
            FLOAT hOriginY = pParagraphFormat->hLayoutRect[1];

            FLOAT pointX = point[0] - hOriginX;
            FLOAT pointY = point[1] - hOriginY;

            BOOL _isTrailingHit;
            BOOL _isInside;
            DWRITE_HIT_TEST_METRICS _hitTestMetrics;

            p->pDWriteTextLayout->HitTestPoint(
                pointX,
                pointY,
                &_isTrailingHit,
                &_isInside,
                &_hitTestMetrics);

            (*isTrailingHit) = _isTrailingHit;
            (*isInside) = _isInside;

            pMetrics->rect[0] = _hitTestMetrics.left;
            pMetrics->rect[1] = _hitTestMetrics.top;
            pMetrics->rect[2] = _hitTestMetrics.width;
            pMetrics->rect[3] = _hitTestMetrics.height;
            pMetrics->offset = _hitTestMetrics.textPosition;
            pMetrics->length = _hitTestMetrics.length;
            pMetrics->bidiLevel = _hitTestMetrics.bidiLevel;
            pMetrics->isText = _hitTestMetrics.isText;
            pMetrics->isTrimmed = _hitTestMetrics.isTrimmed;
        }
    } while (0);
}

MM_EXPORT_NWSI
void
mmTextParagraph_HitTestTextPosition(
    const struct mmTextParagraph*                  p,
    mmUInt32_t                                     offset,
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        point[0] = 0;
        point[1] = 0;
        mmTextHitMetrics_Reset(pMetrics);

        if (NULL == p->pDWriteTextLayout)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteTextLayout is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        {
            struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
            FLOAT hOriginX = pParagraphFormat->hLayoutRect[0];
            FLOAT hOriginY = pParagraphFormat->hLayoutRect[1];
            FLOAT pointX = point[0] - hOriginX;
            FLOAT pointY = point[1] - hOriginY;

            DWRITE_HIT_TEST_METRICS _hitTestMetrics;

            p->pDWriteTextLayout->HitTestTextPosition(
                (UINT32)offset,
                (BOOL)isTrailingHit,
                &pointX,
                &pointY,
                &_hitTestMetrics);

            point[0] = pointX + hOriginX;
            point[1] = pointY + hOriginY;

            pMetrics->rect[0] = _hitTestMetrics.left;
            pMetrics->rect[1] = _hitTestMetrics.top;
            pMetrics->rect[2] = _hitTestMetrics.width;
            pMetrics->rect[3] = _hitTestMetrics.height;
            pMetrics->offset = _hitTestMetrics.textPosition;
            pMetrics->length = _hitTestMetrics.length;
            pMetrics->bidiLevel = _hitTestMetrics.bidiLevel;
            pMetrics->isText = _hitTestMetrics.isText;
            pMetrics->isTrimmed = _hitTestMetrics.isTrimmed;
        }
    } while (0);
}

static
void
__static_mmTextParagraph_UpdateTextSpan(
    struct mmTextParagraph*                        p,
    struct mmTextSpan*                             pTextSpan)
{
    HRESULT hr = S_OK;

    mmIDrawingEffect* pEffect = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmTextFormat* pTextFormat = &pTextSpan->hFormat;
        struct mmRange* pRange = &pTextSpan->hRange;

        if (NULL == p->pDWriteTextLayout)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteTextLayout is null.", __FUNCTION__, __LINE__);
            break;
        }

        hr = mmTextFactory_CreateDrawingEffect(&pEffect);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d mmTextDXFactory_CreateDrawingEffect failure.", __FUNCTION__, __LINE__);
            break;
        }

        pEffect->SetTextFormat(pTextFormat);

        // (index, length)
        DWRITE_TEXT_RANGE hTextRange = { (UINT32)pRange->o, (UINT32)pRange->l, };

        hr = p->pDWriteTextLayout->SetDrawingEffect(pEffect, hTextRange);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d SetDrawingEffect failure.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextFormat* pMasterFormat = &p->pParagraphFormat->hMasterFormat;

        if (0 != mmUtf16String_Compare(&pTextFormat->hFontFamilyName, &pMasterFormat->hFontFamilyName))
        {
            const WCHAR* pFontFamilyName = (const WCHAR*)mmUtf16String_CStr(&pTextFormat->hFontFamilyName);
            hr = p->pDWriteTextLayout->SetFontFamilyName(pFontFamilyName, hTextRange);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetFontFamilyName failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        if (pTextFormat->hFontSize != pMasterFormat->hFontSize)
        {
            FLOAT hFontSize = mmTextPlatform_GetFontSize(pTextFormat->hFontSize);
            FLOAT hFontPixelSize = hFontSize * p->pParagraphFormat->hDisplayDensity;
            hr = p->pDWriteTextLayout->SetFontSize(hFontPixelSize, hTextRange);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetFontSize failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        if (pTextFormat->hFontStyle != pMasterFormat->hFontStyle)
        {
            DWRITE_FONT_STYLE hFontStyle = mmTextPlatform_GetFontStyle(pTextFormat->hFontStyle);
            hr = p->pDWriteTextLayout->SetFontStyle(hFontStyle, hTextRange);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetFontStyle failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        if (pTextFormat->hFontWeight != pMasterFormat->hFontWeight)
        {
            DWRITE_FONT_WEIGHT hFontWeight = mmTextPlatform_GetFontWeight(pTextFormat->hFontWeight);
            hr = p->pDWriteTextLayout->SetFontWeight(hFontWeight, hTextRange);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetFontWeight failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        if (0.0f != pTextFormat->hUnderlineWidth)
        {
            hr = p->pDWriteTextLayout->SetUnderline(TRUE, hTextRange);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetUnderline failure.", __FUNCTION__, __LINE__);
                break;
            }
        }

        if (0.0f != pTextFormat->hStrikeThruWidth)
        {
            hr = p->pDWriteTextLayout->SetStrikethrough(TRUE, hTextRange);
            if (FAILED(hr))
            {
                mmLogger_LogT(gLogger, "%s %d SetStrikethrough failure.", __FUNCTION__, __LINE__);
                break;
            }
        }
    } while (0);

    mmIUnknownSafeRelease(pEffect);
}

static
void
__static_mmTextParagraph_DrawText(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pTextSpannable)
        {
            mmLogger_LogT(gLogger, "%s %d pTextSpannable is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmListVpt* pAttributes = &p->pTextSpannable->hAttributes;

        if (0 == pAttributes->size)
        {
            break;
        }

        struct mmTextSpan* e = NULL;

        struct mmListHead* next = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        next = pAttributes->l.next;
        while (next != &pAttributes->l)
        {
            curr = next;
            next = next->next;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            e = (struct mmTextSpan*)it->v;

            __static_mmTextParagraph_DrawTextSpan(p, e);
        }
    } while (0);
}

static
void
__static_mmTextParagraph_DrawTextSpan(
    struct mmTextParagraph*                        p,
    struct mmTextSpan*                             pTextSpan)
{
    HRESULT hr = S_OK;

    ID2D1SolidColorBrush* pBrushBackgroundColor = NULL;
    DWRITE_HIT_TEST_METRICS* pHitTestMetrics = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmRange* pRange = &pTextSpan->hRange;
        if (0 == pRange->l)
        {
            // length is 0;
            break;
        }
        struct mmTextFormat* pTextFormat = &pTextSpan->hFormat;
        if (0.0f == pTextFormat->hBackgroundColor[3])
        {
            // hBackgroundColor is Transparency;
            break;
        }

        struct IDWriteTextLayout* pDWriteTextLayout = p->pDWriteTextLayout;
        if (NULL == pDWriteTextLayout)
        {
            mmLogger_LogT(gLogger, "%s %d pDWriteTextLayout is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        ID2D1RenderTarget* pD2DRenderTarget = (ID2D1RenderTarget*)p->pDrawingContext->pRenderTarget;
        if (NULL == pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        FLOAT hOriginX = pParagraphFormat->hLayoutRect[0];
        FLOAT hOriginY = pParagraphFormat->hLayoutRect[1];

        // Determine actual number of hit-test ranges
        UINT32 hActualHitTestMetricsCount = 0;
        pDWriteTextLayout->HitTestTextRange(
            (UINT32)pRange->o,          // textPosition
            (UINT32)pRange->l,          // textLength
            hOriginX,                   // originX
            hOriginY,                   // originY
            NULL,                       // hitTestMetrics
            0,                          // maxHitTestMetricsCount
            &hActualHitTestMetricsCount // actualHitTestMetricsCount
        );

        if (0 == hActualHitTestMetricsCount)
        {
            // hActualHitTestMetricsCount is 0;
            break;
        }

        // Draw the ranges BackgroundColor behind the text.
        D2D1_COLOR_F hBackgroundColor;
        mmGraphicsPlatform_MakeD2D1ColorF(&hBackgroundColor, pTextFormat->hBackgroundColor);

        hr = pD2DRenderTarget->CreateSolidColorBrush(hBackgroundColor, &pBrushBackgroundColor);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateSolidColorBrush failure.", __FUNCTION__, __LINE__);
            break;
        }

        pHitTestMetrics = (DWRITE_HIT_TEST_METRICS*)mmMalloc(sizeof(DWRITE_HIT_TEST_METRICS) * hActualHitTestMetricsCount);
        if (NULL == pHitTestMetrics)
        {
            mmLogger_LogT(gLogger, "%s %d pHitTestMetrics is null.", __FUNCTION__, __LINE__);
            break;
        }

        // actual hit test data.
        pDWriteTextLayout->HitTestTextRange(
            (UINT32)pRange->o,          // textPosition
            (UINT32)pRange->l,          // textLength
            hOriginX,                   // originX
            hOriginY,                   // originY
            pHitTestMetrics,            // hitTestMetrics
            hActualHitTestMetricsCount, // maxHitTestMetricsCount
            &hActualHitTestMetricsCount // actualHitTestMetricsCount
        );

        // Note that an ideal layout will return fractional values,
        // so you may see slivers between the selection ranges due
        // to the per-primitive antialiasing of the edges unless
        // it is disabled (better for performance anyway).

        D2D1_ANTIALIAS_MODE hAntialiasMode = pD2DRenderTarget->GetAntialiasMode();
        pD2DRenderTarget->SetAntialiasMode(D2D1_ANTIALIAS_MODE_ALIASED);

        size_t i = 0;
        for (i = 0; i < hActualHitTestMetricsCount; ++i)
        {
            const DWRITE_HIT_TEST_METRICS* htm = &pHitTestMetrics[i];
            D2D1_RECT_F hHighlightRect =
            {
                (htm->left),
                (htm->top),
                (htm->left + htm->width),
                (htm->top + htm->height),
            };
            pD2DRenderTarget->FillRectangle(hHighlightRect, pBrushBackgroundColor);
        }

        pD2DRenderTarget->SetAntialiasMode(hAntialiasMode);

    } while (0);

    mmIUnknownSafeRelease(pBrushBackgroundColor);
    mmFree(pHitTestMetrics);
}

static
void
__static_mmTextParagraph_DrawCaret(
    struct mmTextParagraph*                        p,
    ID2D1RenderTarget*                             pD2DRenderTarget)
{
    HRESULT hr = S_OK;

    ID2D1SolidColorBrush* pBrushCaretColor = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;

        if (0 == pParagraphFormat->hCaretStatus)
        {
            break;
        }

        D2D1_COLOR_F hCaretColor;
        mmGraphicsPlatform_MakeD2D1ColorF(&hCaretColor, pParagraphFormat->hCaretColor);

        hr = pD2DRenderTarget->CreateSolidColorBrush(hCaretColor, &pBrushCaretColor);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateSolidColorBrush failure.", __FUNCTION__, __LINE__);
            break;
        }

        D2D1_RECT_F hRect;
        float point[2];
        struct mmTextHitMetrics metrics;
        int isTrailingHit = pParagraphFormat->hCaretWrapping;
        mmUInt32_t offset = (mmUInt32_t)pParagraphFormat->hCaretIndex - isTrailingHit;
        float hDisplayDensity = pParagraphFormat->hDisplayDensity;
        float hCaretW = pParagraphFormat->hCaretWidth * hDisplayDensity;
        float hCaretHalfW = hCaretW / 2.0f;

        // Normalise to front index.
        const struct mmUtf16String* pString = &p->pText->hString;
        const mmUInt16_t* ss = mmUtf16String_Data(pString);
        size_t sz = mmUtf16String_Size(pString);
        offset = (mmUInt32_t)mmUTF_GetUtf16OffsetLNormalise(ss, sz, offset);

        mmTextParagraph_HitTestTextPosition(p, offset, isTrailingHit, point, &metrics);

        hRect.left   = point[0] - hCaretHalfW;
        hRect.top    = point[1];
        hRect.right  = point[0] + hCaretHalfW;
        hRect.bottom = point[1] + metrics.rect[3];

        D2D1_ANTIALIAS_MODE hAntialiasMode = pD2DRenderTarget->GetAntialiasMode();
        pD2DRenderTarget->SetAntialiasMode(D2D1_ANTIALIAS_MODE_ALIASED);

        pD2DRenderTarget->FillRectangle(hRect, pBrushCaretColor);

        pD2DRenderTarget->SetAntialiasMode(hAntialiasMode);
    } while (0);
}
