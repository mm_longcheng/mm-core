/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIDisplayMetrics_h__
#define __mmUIDisplayMetrics_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

/* 
 * hWnd
 * 
 * Handle to the window whose device context is to be retrieved. 
 * If this value is NULL, GetDC retrieves the device context for the entire screen.
 */
MM_EXPORT_NWSI double mmUIDisplayMetrics_GetDisplayDensity(HWND hWnd);

/* 
 * hWnd
 * 
 * Handle to the window whose device context is to be retrieved. 
 * If this value is NULL, GetDC retrieves the device context for the entire screen.
 */
MM_EXPORT_NWSI void mmUIDisplayMetrics_GetWindowDpi(HWND hWnd, FLOAT* dpiX, FLOAT* dpiY);

/* 
 * pDriver
 * 
 * pDriver default can be "DISPLAY".
 */
MM_EXPORT_NWSI void mmUIDisplayMetrics_GetDriverDpi(const char* pDriver, FLOAT* dpiX, FLOAT* dpiY);

#include "core/mmSuffix.h"

#endif//__mmUIDisplayMetrics_h__
