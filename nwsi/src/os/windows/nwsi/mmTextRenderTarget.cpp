/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextRenderTarget.h"
#include "mmTextDrawingContext.h"
#include "mmGraphicsPlatform.h"
#include "mmGraphicsFactory.h"
#include "mmUIDisplayMetrics.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "nwsi/mmTextParagraphFormat.h"

#include <d2d1.h>
#include <dwrite.h>
#include <wincodec.h>

#include <assert.h>

MM_EXPORT_NWSI
void
mmTextRenderTarget_Init(
    struct mmTextRenderTarget*                     p)
{
    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    p->pD2DRenderTarget = NULL;
    p->pWICBitmap = NULL;
    p->pBitmapLock = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_Destroy(
    struct mmTextRenderTarget*                     p)
{
    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    p->pD2DRenderTarget = NULL;
    p->pWICBitmap = NULL;
    p->pBitmapLock = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetGraphicsFactory(
    struct mmTextRenderTarget*                     p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    p->pGraphicsFactory = pGraphicsFactory;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetTextParagraphFormat(
    struct mmTextRenderTarget*                     p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    p->pParagraphFormat = pParagraphFormat;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_OnFinishLaunching(
    struct mmTextRenderTarget*                     p)
{
    HRESULT hr = S_OK;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pGraphicsFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pGraphicsFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        IWICImagingFactory* pWICImagingFactory = p->pGraphicsFactory->pWICImagingFactory;
        if (NULL == pWICImagingFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pWICImagingFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        ID2D1Factory* pD2DFactory = p->pGraphicsFactory->pD2DFactory;
        if (NULL == pD2DFactory)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DFactory is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        if (NULL == pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        FLOAT hDpiX, hDpiY;
        // pD2DFactory->GetDesktopDpi(&hDpiX, &hDpiY);
        // GetDesktopDpi is deprecated.
        mmUIDisplayMetrics_GetWindowDpi(NULL, &hDpiX, &hDpiY);

        UINT hWindowSizeW = (UINT)ceilf(pParagraphFormat->hWindowSize[0]);
        UINT hWindowSizeH = (UINT)ceilf(pParagraphFormat->hWindowSize[1]);
        // Avoid empty rectangles.
        hWindowSizeW = (0 == hWindowSizeW) ? 1 : hWindowSizeW;
        hWindowSizeH = (0 == hWindowSizeH) ? 1 : hWindowSizeH;
        // GUID_WICPixelFormat32bppPRGBA;
        // GUID_WICPixelFormat32bppPBGRA;
        hr = pWICImagingFactory->CreateBitmap(
            hWindowSizeW,
            hWindowSizeH,
            GUID_WICPixelFormat32bppPRGBA,
            WICBitmapCacheOnDemand,
            &p->pWICBitmap);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateBitmap failure.", __FUNCTION__, __LINE__);
            break;
        }

        D2D1_PIXEL_FORMAT hPixelFormat;
        // DXGI_FORMAT_R8G8B8A8_UNORM;
        // DXGI_FORMAT_B8G8R8A8_UNORM;
        hPixelFormat.format = DXGI_FORMAT_R8G8B8A8_UNORM;
        hPixelFormat.alphaMode = D2D1_ALPHA_MODE_PREMULTIPLIED;

        /*
         * https://docs.microsoft.com/zh-cn/windows/win32/api/d2d1/ns-d2d1-d2d1_render_target_properties?redirectedfrom=MSDN
         *
         * Using Default DPI Settings
         *
         * To use the default DPI, set dpiX and dpiY to 0. 
         * The default DPI varies depending on the render target:
         *
         * For a compatible render target, the default DPI is the DPI of 
         * the parent render target.
         *
         * For a ID2D1HwndRenderTarget, the default DPI is the system DPI 
         * obtained from the render target's ID2D1Factory.
         *
         * For other render targets, the default DPI is 96.
         *
         * To use the default DPI setting, both dpiX and dpiY must be set to 0. 
         * Setting only one value to 0 causes an E_INVALIDARG error when 
         * attempting to create a render target.
         */

        D2D1_RENDER_TARGET_PROPERTIES hRenderTargetProperties;
        hRenderTargetProperties.type = D2D1_RENDER_TARGET_TYPE_DEFAULT;
        hRenderTargetProperties.pixelFormat = hPixelFormat;
        hRenderTargetProperties.dpiX = hDpiX;
        hRenderTargetProperties.dpiY = hDpiY;
        hRenderTargetProperties.usage = D2D1_RENDER_TARGET_USAGE_NONE;
        hRenderTargetProperties.minLevel = D2D1_FEATURE_LEVEL_DEFAULT;

        hr = pD2DFactory->CreateWicBitmapRenderTarget(
            p->pWICBitmap,
            hRenderTargetProperties,
            &p->pD2DRenderTarget);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d CreateWicBitmapRenderTarget failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_OnBeforeTerminate(
    struct mmTextRenderTarget*                     p)
{
    mmIUnknownSafeRelease(p->pD2DRenderTarget);
    mmIUnknownSafeRelease(p->pWICBitmap);

    p->pD2DRenderTarget = NULL;
    p->pWICBitmap = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BeginDraw(
    struct mmTextRenderTarget*                     p)
{
    assert(p->pD2DRenderTarget && "pD2DRenderTarget is a null.");
    p->pD2DRenderTarget->BeginDraw();
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetTransform(
    struct mmTextRenderTarget*                     p,
    struct mmAffineTransform*                      hTransform)
{
    assert(p->pD2DRenderTarget && "pD2DRenderTarget is a null.");
    D2D1_MATRIX_3X2_F hMatrix;
    hMatrix.m11 = hTransform->a; hMatrix.m12 = hTransform->b;
    hMatrix.m21 = hTransform->c; hMatrix.m22 = hTransform->d;
    hMatrix.dx  = hTransform->x; hMatrix.dy  = hTransform->y;
    p->pD2DRenderTarget->SetTransform(hMatrix);
}

// mmColorBlackTransparent
MM_EXPORT_NWSI
void
mmTextRenderTarget_Clear(
    struct mmTextRenderTarget*                     p)
{
    assert(p->pD2DRenderTarget && "pD2DRenderTarget is a null.");
    D2D1_COLOR_F hClearColor = D2D1::ColorF(0.0f, 0.0f, 0.0f, 0.0f);
    p->pD2DRenderTarget->Clear(hClearColor);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_EndDraw(
    struct mmTextRenderTarget*                     p)
{
    assert(p->pD2DRenderTarget && "pD2DRenderTarget is a null.");
    p->pD2DRenderTarget->EndDraw();
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BeginTransparency(
    struct mmTextRenderTarget*                     p)
{

}

MM_EXPORT_NWSI
void
mmTextRenderTarget_EndTransparency(
    struct mmTextRenderTarget*                     p)
{

}

MM_EXPORT_NWSI
const void*
mmTextRenderTarget_BufferLock(
    struct mmTextRenderTarget*                     p)
{
    HRESULT hr = S_OK;

    BYTE* pBuffer = NULL;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pD2DRenderTarget)
        {
            mmLogger_LogT(gLogger, "%s %d pD2DRenderTarget is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->pWICBitmap)
        {
            mmLogger_LogT(gLogger, "%s %d pWICBitmap is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        if (NULL == pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        UINT hWindowSizeW = (UINT)ceilf(pParagraphFormat->hWindowSize[0]);
        UINT hWindowSizeH = (UINT)ceilf(pParagraphFormat->hWindowSize[1]);
        // Avoid empty rectangles.
        hWindowSizeW = (0 == hWindowSizeW) ? 1 : hWindowSizeW;
        hWindowSizeH = (0 == hWindowSizeH) ? 1 : hWindowSizeH;

        WICRect hWICRect = { (INT)0, (INT)0, (INT)hWindowSizeW, (INT)hWindowSizeH, };
        hr = p->pWICBitmap->Lock(&hWICRect, WICBitmapLockRead, &p->pBitmapLock);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d pWICBitmap->Lock failure.", __FUNCTION__, __LINE__);
            break;
        }

        UINT hBufferSize = 0;
        hr = p->pBitmapLock->GetDataPointer(&hBufferSize, &pBuffer);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d pBitmapLock->GetDataPointer failure.", __FUNCTION__, __LINE__);
            break;
        }
    } while (0);

    return pBuffer;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BufferUnLock(
    struct mmTextRenderTarget*                     p,
    const void*                                    pBuffer)
{
    if (NULL != pBuffer)
    {
        mmIUnknownSafeRelease(p->pBitmapLock);
        p->pBitmapLock = NULL;
    }
    else
    {
        p->pBitmapLock = NULL;
    }
}

