/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextDrawingContext.h"
#include "mmTextRenderTarget.h"

MM_EXPORT_NWSI
void
mmTextDrawingContext_Init(
    struct mmTextDrawingContext*                   p)
{
    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    p->pRenderTarget = NULL;
    p->pBitmap = NULL;
    p->hPass = 0;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_Destroy(
    struct mmTextDrawingContext*                   p)
{
    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    p->pRenderTarget = NULL;
    p->pBitmap = NULL;
    p->hPass = 0;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetGraphicsFactory(
    struct mmTextDrawingContext*                   p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    p->pGraphicsFactory = pGraphicsFactory;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetTextParagraphFormat(
    struct mmTextDrawingContext*                   p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    p->pParagraphFormat = pParagraphFormat;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetRenderTarget(
    struct mmTextDrawingContext*                   p,
    void*                                          pRenderTarget)
{
    p->pRenderTarget = pRenderTarget;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetBitmap(
    struct mmTextDrawingContext*                   p,
    void*                                          pBitmap)
{
    p->pBitmap = pBitmap;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_SetPass(
    struct mmTextDrawingContext*                   p,
    int                                            hPass)
{
    p->hPass = hPass;
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_PrepareRenderTarget(
    struct mmTextDrawingContext*                   p,
    struct mmTextRenderTarget*                     pRenderTarget)
{
    mmTextDrawingContext_SetRenderTarget(p, pRenderTarget->pD2DRenderTarget);
    mmTextDrawingContext_SetBitmap(p, pRenderTarget->pWICBitmap);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_DiscardRenderTarget(
    struct mmTextDrawingContext*                   p)
{
    mmTextDrawingContext_SetRenderTarget(p, NULL);
    mmTextDrawingContext_SetBitmap(p, NULL);
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_OnFinishLaunching(
    struct mmTextDrawingContext*                   p)
{
    
}

MM_EXPORT_NWSI
void
mmTextDrawingContext_OnBeforeTerminate(
    struct mmTextDrawingContext*                   p)
{

}