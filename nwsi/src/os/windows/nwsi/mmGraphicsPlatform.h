/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmGraphicsPlatform_h__
#define __mmGraphicsPlatform_h__

#include "core/mmCore.h"

#include <d2d1.h>
#include <d2d1_1.h>

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI 
void 
mmIUnknownSafeRelease(
    IUnknown*                                      p);

MM_EXPORT_NWSI 
void 
mmGraphicsPlatform_MakeD2D1ColorF(
    D2D1_COLOR_F*                                  p, 
    float                                          pColor[4]);

MM_EXPORT_NWSI 
void 
mmGraphicsPlatform_MakeD2D1Vector4F(
    D2D1_VECTOR_4F*                                p, 
    float                                          pColor[4]);

MM_EXPORT_NWSI 
D2D1_LINE_JOIN 
mmGraphicsPlatform_GetLineJoin(
    int                                            hLineJoin);

MM_EXPORT_NWSI 
D2D1_CAP_STYLE 
mmGraphicsPlatform_GetCapStyle(
    int                                            hLineCap);

#include "core/mmSuffix.h"

#endif//__mmGraphicsPlatform_h__
