/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmPackageAssets.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmString.h"
#include "core/mmFilePath.h"

#include "mmUnicodeWindows.h"

#include <shlobj.h>

#pragma comment(lib,"Version.lib")

static void __static_mmPackageAssets_ModuleFileName(struct mmString* pModuleFileName)
{
    struct mmString hBuffer;

    // try to determine the application's path
    DWORD bufsize = 256;
    DWORD retval = 0;
    const char* resolved = NULL;
    const char* pPath = NULL;

    mmString_Init(&hBuffer);

    do
    {
        mmString_Resize(&hBuffer, bufsize);
        retval = GetModuleFileNameA(NULL, (char*)mmString_Data(&hBuffer), bufsize);
        if (retval == 0)
        {
            // failed
            break;
        }

        if (retval < bufsize)
        {
            // operation was successful.
            resolved = mmString_Data(&hBuffer);
            break;
        }
        else
        {
            // buffer was too small, grow buffer and try again
            bufsize <<= 1;
        }
    } while (!resolved);

    pPath = (NULL == resolved) ? "" : resolved;
    mmString_Assigns(pModuleFileName, pPath);

    mmString_Destroy(&hBuffer);
}
static void __static_mmPackageAssets_VerQueryValue(LPBYTE lpVersionInfo, DWORD hLangCharset, const char* pKey, struct mmString* pValue)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    UINT uLength = 0;
    const char* pValueString = NULL;
    char prQueryKey[64] = { 0 };
    mmSprintf(prQueryKey, "\\StringFileInfo\\%08lx\\%s", hLangCharset, pKey);
    if (FALSE == VerQueryValueA(lpVersionInfo, prQueryKey, (LPVOID*)&pValueString, &uLength))
    {
        mmLogger_LogW(gLogger, "mmPackageAssets.VerQueryValue VerQueryValueA %s failure.", pKey);
    }
    else
    {
        mmString_Assigns(pValue, pValueString);
    }
}
static __static_mmPackageAssets_Version(struct mmPackageAssets* p)
{
    UINT uLength = 0;

    VS_FIXEDFILEINFO* pFixedFileInfo = NULL;

    DWORD* pTransTable = NULL;
    DWORD  hLangCharset = 0;

    DWORD dwDummy;
    DWORD dwFVISize = 0;
    LPBYTE lpVersionInfo = NULL;

    struct mmString pModuleFileName;

    struct mmLogger* gLogger = mmLogger_Instance();

    mmString_Init(&pModuleFileName);

    do
    {
        __static_mmPackageAssets_ModuleFileName(&pModuleFileName);

        dwFVISize = GetFileVersionInfoSizeA(mmString_CStr(&pModuleFileName), &dwDummy);
        lpVersionInfo = (LPBYTE)mmMalloc(dwFVISize);
        if (FALSE == GetFileVersionInfoA(mmString_CStr(&pModuleFileName), 0, dwFVISize, lpVersionInfo))
        {
            mmLogger_LogW(gLogger, "mmPackageAssets.Version GetFileVersionInfoA failure.");
            break;
        }

        if (FALSE == VerQueryValueA(lpVersionInfo, "\\", (LPVOID*)&pFixedFileInfo, &uLength))
        {
            mmLogger_LogW(gLogger, "mmPackageAssets.Version VerQueryValueA FixedFileInfo failure.");
        }
        else
        {
            char hFixedVersion[128] = { 0 };

            mmSprintf(hFixedVersion, "%u.%u.%u.%u",
                HIWORD(pFixedFileInfo->dwProductVersionMS),
                LOWORD(pFixedFileInfo->dwProductVersionMS),
                HIWORD(pFixedFileInfo->dwProductVersionLS),
                LOWORD(pFixedFileInfo->dwProductVersionLS));

            mmLogger_LogI(gLogger, "mmPackageAssets.Version FixedVersion: %s", hFixedVersion);
        }

        if (FALSE == VerQueryValueA(lpVersionInfo, "\\VarFileInfo\\Translation", (LPVOID*)&pTransTable, &uLength))
        {
            mmLogger_LogW(gLogger, "mmPackageAssets.Version VerQueryValueA Translation failure.");
            break;
        }

        hLangCharset = MAKELONG(HIWORD(pTransTable[0]), LOWORD(pTransTable[0]));

        __static_mmPackageAssets_VerQueryValue(lpVersionInfo, hLangCharset,   "InternalName", &p->hPackageName);
        __static_mmPackageAssets_VerQueryValue(lpVersionInfo, hLangCharset, "ProductVersion", &p->hVersionName);
        __static_mmPackageAssets_VerQueryValue(lpVersionInfo, hLangCharset,    "FileVersion", &p->hVersionCode);
        __static_mmPackageAssets_VerQueryValue(lpVersionInfo, hLangCharset,    "ProductName", &p->hDisplayName);
    } while (0);

    if (NULL != lpVersionInfo)
    {
        mmFree(lpVersionInfo);
        lpVersionInfo = NULL;
    }

    mmString_Destroy(&pModuleFileName);
}

static void __static_mmPackageAssets_MyDocumentsPath(struct mmString* pMyDocumentsPath)
{
    TCHAR tPath[MAX_PATH] = { 0 };
    // My Documents
    SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, tPath);
    mmUnicodeWindows_TCharToString(tPath, pMyDocumentsPath);
}
static void __static_mmPackageAssets_WindowsFontsPath(struct mmString* pWindowsFontsPath)
{
    TCHAR tPath[MAX_PATH] = { 0 };
    // My Documents
    SHGetFolderPath(NULL, CSIDL_FONTS | CSIDL_FLAG_CREATE, NULL, 0, tPath);
    mmUnicodeWindows_TCharToString(tPath, pWindowsFontsPath);
}
static void __static_mmPackageAssets_ApplicationModelPath(struct mmString* pApplicationModelPath)
{
    struct mmString hBuffer;

    mmString_Init(&hBuffer);

#if MM_PLATFORM == MM_PLATFORM_WIN32
    {
        // try to determine the application's path
        DWORD bufsize = 256;
        const char* resolved = NULL;
        const char* pPath = NULL;

        do
        {
            mmString_Resize(&hBuffer, bufsize);
            DWORD retval = GetModuleFileNameA(NULL, (char*)mmString_Data(&hBuffer), bufsize);
            if (retval == 0)
            {
                // failed
                break;
            }

            if (retval < bufsize)
            {
                // operation was successful.
                resolved = mmString_Data(&hBuffer);
                break;
            }
            else
            {
                // buffer was too small, grow buffer and try again
                bufsize <<= 1;
            }
        } while (!resolved);

        pPath = (NULL == resolved) ? "" : resolved;
        mmString_Assigns(pApplicationModelPath, pPath);

        if (mmString_Empty(pApplicationModelPath))
        {
            // fall back to current working directory.
            mmString_Assigns(pApplicationModelPath, ".");
        }
        else
        {
            // need to strip the application filename from the path
            size_t lpos = mmString_FindLastOf(pApplicationModelPath, '\\');
            if (mmStringNpos != lpos)
            {
                mmString_Substr(pApplicationModelPath, pApplicationModelPath, 0, lpos);
            }
        }
    }
#elif MM_PLATFORM == MM_PLATFORM_WINRT
    {
        const WCHAR* wText = Windows::ApplicationModel::Package::Current->InstalledLocation->Path->Data();
        int hCode = mmUnicodeWindows_WCharToString(wText, pApplicationModelPath);
        if (0 != hCode)
        {
            // fall back to current working directory.
            mmString_Assigns(pApplicationModelPath, ".");
        }
}
#endif

    mmString_Destroy(&hBuffer);
}

MM_EXPORT_NWSI void mmPackageAssets_Init(struct mmPackageAssets* p)
{
    mmString_Init(&p->hModuleName);
    
    mmString_Init(&p->hPackageName);
    mmString_Init(&p->hVersionName);
    mmString_Init(&p->hVersionCode);
    mmString_Init(&p->hDisplayName);
    
    mmString_Init(&p->hHomeDirectory);

    mmString_Init(&p->hInternalFilesDirectory);
    mmString_Init(&p->hInternalCacheDirectory);
    mmString_Init(&p->hExternalFilesDirectory);
    mmString_Init(&p->hExternalCacheDirectory);
    
    mmString_Init(&p->hPluginFolder);
    mmString_Init(&p->hDynlibFolder);

    mmString_Init(&p->hShaderCacheFolder);
    mmString_Init(&p->hTextureCacheFolder);

    mmString_Init(&p->hLoggerPathName);
    mmString_Init(&p->hLoggerPath);

    mmString_Init(&p->hAssetsRootFolderPath);
    mmString_Init(&p->hAssetsRootFolderBase);
    
    mmString_Init(&p->hAssetsRootSourcePath);
    mmString_Init(&p->hAssetsRootSourceBase);
    
    p->pAppHandler = NULL;
    p->pAssetManager = NULL;
   
    // Initial value.
    mmString_Assigns(&p->hModuleName, "mm.nwsi");
    
    mmString_Assigns(&p->hPackageName, "");
    mmString_Assigns(&p->hVersionName, "");
    mmString_Assigns(&p->hVersionCode, "");
    mmString_Assigns(&p->hDisplayName, "");
    
    mmString_Assigns(&p->hHomeDirectory, "");

    mmString_Assigns(&p->hInternalFilesDirectory, "");
    mmString_Assigns(&p->hInternalCacheDirectory, "");
    mmString_Assigns(&p->hExternalFilesDirectory, "");
    mmString_Assigns(&p->hExternalCacheDirectory, "");
    
    mmString_Assigns(&p->hPluginFolder, "");
    mmString_Assigns(&p->hDynlibFolder, "");

    mmString_Assigns(&p->hShaderCacheFolder, "shader");
    mmString_Assigns(&p->hTextureCacheFolder, "texture");

    mmString_Assigns(&p->hLoggerPathName, "log");
    mmString_Assigns(&p->hLoggerPath, "");
    
    mmString_Assigns(&p->hAssetsRootFolderPath, "./resources");
    mmString_Assigns(&p->hAssetsRootFolderBase, "assets");
    
    mmString_Assigns(&p->hAssetsRootSourcePath, "");
    mmString_Assigns(&p->hAssetsRootSourceBase, "");
}
MM_EXPORT_NWSI void mmPackageAssets_Destroy(struct mmPackageAssets* p)
{
    mmString_Destroy(&p->hModuleName);
    
    mmString_Destroy(&p->hPackageName);
    mmString_Destroy(&p->hVersionName);
    mmString_Destroy(&p->hVersionCode);
    mmString_Destroy(&p->hDisplayName);
    
    mmString_Destroy(&p->hHomeDirectory);

    mmString_Destroy(&p->hInternalFilesDirectory);
    mmString_Destroy(&p->hInternalCacheDirectory);
    mmString_Destroy(&p->hExternalFilesDirectory);
    mmString_Destroy(&p->hExternalCacheDirectory);

    mmString_Destroy(&p->hPluginFolder);
    mmString_Destroy(&p->hDynlibFolder);

    mmString_Destroy(&p->hShaderCacheFolder);
    mmString_Destroy(&p->hTextureCacheFolder);

    mmString_Destroy(&p->hLoggerPathName);
    mmString_Destroy(&p->hLoggerPath);

    mmString_Destroy(&p->hAssetsRootFolderPath);
    mmString_Destroy(&p->hAssetsRootFolderBase);
    
    mmString_Destroy(&p->hAssetsRootSourcePath);
    mmString_Destroy(&p->hAssetsRootSourceBase);
    
    p->pAppHandler = NULL;
    p->pAssetManager = NULL;
}

MM_EXPORT_NWSI void mmPackageAssets_SetAppHandler(struct mmPackageAssets* p, void* pAppHandler)
{
    p->pAppHandler = pAppHandler;
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetManager(struct mmPackageAssets* p, void* pAssetManager)
{
    p->pAssetManager = pAssetManager;
}
MM_EXPORT_NWSI void mmPackageAssets_SetModuleName(struct mmPackageAssets* p, const char* pModuleName)
{
    mmString_Assigns(&p->hModuleName, pModuleName);
}
MM_EXPORT_NWSI void mmPackageAssets_SetPluginFolder(struct mmPackageAssets* p, const char* pPluginFolder)
{
    mmString_Assigns(&p->hPluginFolder, pPluginFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetDynlibFolder(struct mmPackageAssets* p, const char* pDynlibFolder)
{
    mmString_Assigns(&p->hDynlibFolder, pDynlibFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetShaderCacheFolder(struct mmPackageAssets* p, const char* pShaderCacheFolder)
{
    mmString_Assigns(&p->hShaderCacheFolder, pShaderCacheFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetTextureCacheFolder(struct mmPackageAssets* p, const char* pTextureCacheFolder)
{
    mmString_Assigns(&p->hTextureCacheFolder, pTextureCacheFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetLoggerPathName(struct mmPackageAssets* p, const char* pLoggerPathName)
{
    mmString_Assigns(&p->hLoggerPathName, pLoggerPathName);
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootFolder(struct mmPackageAssets* p, const char* pPath, const char* pBase)
{
    mmString_Assigns(&p->hAssetsRootFolderPath, pPath);
    mmString_Assigns(&p->hAssetsRootFolderBase, pBase);
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootSource(struct mmPackageAssets* p, const char* pPath, const char* pBase)
{
    mmString_Assigns(&p->hAssetsRootSourcePath, pPath);
    mmString_Assigns(&p->hAssetsRootSourceBase, pBase);
}

MM_EXPORT_NWSI void mmPackageAssets_AttachNativeResource(struct mmPackageAssets* p)
{
    struct mmString hMyDocumentsPath;
    struct mmString hApplicationModelPath;
    struct mmString hMyDocumentsApplication;
    struct mmString hData;

    mmString_Init(&hMyDocumentsPath);
    mmString_Init(&hApplicationModelPath);
    mmString_Init(&hMyDocumentsApplication);
    mmString_Init(&hData);

    // windows default assets directory.
    //
    // InternalFilesDirectory <MyDocuments>/files
    // InternalCacheDirectory <MyDocuments>/cache
    // ExternalFilesDirectory data/files
    // ExternalCacheDirectory data/cache
    // LoggerPath             data/cache/log
    // AssetsPath             <BundlePath>/assets

    __static_mmPackageAssets_MyDocumentsPath(&hMyDocumentsPath);
    __static_mmPackageAssets_ApplicationModelPath(&hApplicationModelPath);

    mmString_ReplaceChar(&hMyDocumentsPath, '\\', '/');
    mmString_ReplaceChar(&hApplicationModelPath, '\\', '/');

    mmString_Append(&hMyDocumentsApplication, &hMyDocumentsPath);
    mmString_Appends(&hMyDocumentsApplication, "/");
    mmString_Append(&hMyDocumentsApplication, &p->hModuleName);

    mmString_Append(&hData, &hApplicationModelPath);
    mmString_Appends(&hData, "/");
    mmString_Appends(&hData, "data");

    mmString_Assign(&p->hHomeDirectory, &hData);

    mmString_Assign(&p->hInternalFilesDirectory, &hMyDocumentsApplication);
    mmString_Appends(&p->hInternalFilesDirectory, "/files");

    mmString_Assign(&p->hInternalCacheDirectory, &hMyDocumentsApplication);
    mmString_Appends(&p->hInternalCacheDirectory, "/cache");

    mmString_Assign(&p->hExternalFilesDirectory, &hData);
    mmString_Appends(&p->hExternalFilesDirectory, "/files");

    mmString_Assign(&p->hExternalCacheDirectory, &hData);
    mmString_Appends(&p->hExternalCacheDirectory, "/cache");

    mmString_Assign(&p->hLoggerPath, &p->hExternalCacheDirectory);
    mmString_Appends(&p->hLoggerPath, "/");
    mmString_Append(&p->hLoggerPath, &p->hLoggerPathName);

    mmMkdirIfNotExist(mmString_CStr(&hMyDocumentsApplication));
    mmMkdirIfNotExist(mmString_CStr(&hData));
    mmMkdirIfNotExist(mmString_CStr(&p->hInternalFilesDirectory));
    mmMkdirIfNotExist(mmString_CStr(&p->hInternalCacheDirectory));
    mmMkdirIfNotExist(mmString_CStr(&p->hExternalFilesDirectory));
    mmMkdirIfNotExist(mmString_CStr(&p->hExternalCacheDirectory));
    mmMkdirIfNotExist(mmString_CStr(&p->hLoggerPath));

    __static_mmPackageAssets_Version(p);

    mmString_Destroy(&hMyDocumentsPath);
    mmString_Destroy(&hApplicationModelPath);
    mmString_Destroy(&hMyDocumentsApplication);
    mmString_Destroy(&hData);
}
MM_EXPORT_NWSI void mmPackageAssets_DetachNativeResource(struct mmPackageAssets* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI void mmPackageAssets_OnFinishLaunching(struct mmPackageAssets* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // logger information.
    mmLogger_LogI(gLogger, "Assets mmPackageAssets.OnFinishLaunching");
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
    mmLogger_LogI(gLogger, "Assets                pPackageName : %s", mmString_CStr(&p->hPackageName));
    mmLogger_LogI(gLogger, "Assets                pVersionName : %s", mmString_CStr(&p->hVersionName));
    mmLogger_LogI(gLogger, "Assets                pVersionCode : %s", mmString_CStr(&p->hVersionCode));
    mmLogger_LogI(gLogger, "Assets                pDisplayName : %s", mmString_CStr(&p->hDisplayName));
    mmLogger_LogI(gLogger, "Assets               pPluginFolder : %s", mmString_CStr(&p->hPluginFolder));
    mmLogger_LogI(gLogger, "Assets               pDynlibFolder : %s", mmString_CStr(&p->hDynlibFolder));
    mmLogger_LogI(gLogger, "Assets              hHomeDirectory : %s", mmString_CStr(&p->hHomeDirectory));
    mmLogger_LogI(gLogger, "Assets          pShaderCacheFolder : %s", mmString_CStr(&p->hShaderCacheFolder));
    mmLogger_LogI(gLogger, "Assets         pTextureCacheFolder : %s", mmString_CStr(&p->hTextureCacheFolder));
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
    mmLogger_LogI(gLogger, "Assets     pInternalFilesDirectory : %s", mmString_CStr(&p->hInternalFilesDirectory));
    mmLogger_LogI(gLogger, "Assets     pInternalCacheDirectory : %s", mmString_CStr(&p->hInternalCacheDirectory));
    mmLogger_LogI(gLogger, "Assets     pExternalFilesDirectory : %s", mmString_CStr(&p->hExternalFilesDirectory));
    mmLogger_LogI(gLogger, "Assets     pExternalCacheDirectory : %s", mmString_CStr(&p->hExternalCacheDirectory));
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
}
MM_EXPORT_NWSI void mmPackageAssets_OnBeforeTerminate(struct mmPackageAssets* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI
void
mmPackageAssets_MakeShaderCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &p->hExternalCacheDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &p->hShaderCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}

MM_EXPORT_NWSI
void
mmPackageAssets_MakeTextureCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &p->hExternalCacheDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &p->hTextureCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}
