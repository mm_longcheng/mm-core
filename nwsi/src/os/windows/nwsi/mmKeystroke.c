/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmKeystroke.h"

MM_EXPORT_NWSI void mmKeystrokeMake(struct mmKeystroke* p, WPARAM wParam, LPARAM lParam)
{
    p->vkCode = LOWORD(wParam);                                       // virtual-key code

    p->scanCode = LOBYTE(HIWORD(lParam));                             // scan code
    p->scanCodeE0 = (HIWORD(lParam) & KF_EXTENDED) == KF_EXTENDED;    // extended-key flag, 1 if scancode has 0xE0 prefix

    p->upFlag = (HIWORD(lParam) & KF_UP) == KF_UP;                    // transition-state flag, 1 on keyup
    p->repeatFlag = (HIWORD(lParam) & KF_REPEAT) == KF_REPEAT;        // previous key-state flag, 1 on autorepeat
    p->repeatCount = LOWORD(lParam);                                  // repeat count, > 0 if several keydown messages was combined into one message

    p->altDownFlag = (HIWORD(lParam) & KF_ALTDOWN) == KF_ALTDOWN;     // ALT key was pressed

    p->dlgModeFlag = (HIWORD(lParam) & KF_DLGMODE) == KF_DLGMODE;     // dialog box is active
    p->menuModeFlag = (HIWORD(lParam) & KF_MENUMODE) == KF_MENUMODE;  // menu is active
}

MM_EXPORT_NWSI int mmKeystrokeToUnicode(struct mmKeystroke* p)
{
    static WCHAR deadKey = 0;

    WCHAR buff[3] = { 0, 0, 0 };
    int ascii = 0;

    BYTE keyState[256] = { 0 };
    HKL  layout = GetKeyboardLayout(0);
    if (0 == GetKeyboardState(keyState))
    {
        return 0;
    }
    // This parameter must be 1 if a menu is active, zero otherwise.
    UINT wFlags = p->menuModeFlag ? 1: 0;
    ascii = ToUnicodeEx((UINT)p->vkCode, (UINT)p->scanCode, keyState, buff, 3, wFlags, layout);
    if (ascii == 1 && deadKey != '\0')
    {
        // A dead key is stored and we have just converted a character key
        // Combine the two into a single character
        WCHAR wcBuff[3] = { buff[0], deadKey, '\0' };
        WCHAR out[3] = { 0 };

        deadKey = '\0';
        if (FoldStringW(MAP_PRECOMPOSED, (LPWSTR)wcBuff, 3, (LPWSTR)out, 3))
        {
            return out[0];
        }
    }
    else if (ascii == 1)
    {
        // We have a single character
        deadKey = '\0';
        return buff[0];
    }
    else if (ascii == 2)
    {
        // Convert a non-combining diacritical mark into a combining diacritical mark
        // Combining versions range from 0x300 to 0x36F; only 5 (for French) have been mapped below
        // http://www.fileformat.info/info/unicode/block/combining_diacritical_marks/images.htm
        switch (buff[0])
        {
        case 0x5E: // Circumflex accent: в
            deadKey = 0x302;
            break;
        case 0x60: // Grave accent: а
            deadKey = 0x300;
            break;
        case 0xA8: // Diaeresis: ь
            deadKey = 0x308;
            break;
        case 0xB4: // Acute accent: й
            deadKey = 0x301;
            break;
        case 0xB8: // Cedilla: з
            deadKey = 0x327;
            break;
        default:
            deadKey = buff[0];
            break;
        }
    }
    return 0;
}

MM_EXPORT_NWSI int mmKeystrokeToCharacters(struct mmKeystroke* p, mmUInt16_t text[4])
{
    int ascii = 0;

    BYTE keyState[256] = { 0 };
    HKL  layout = GetKeyboardLayout(0);
    if (0 == GetKeyboardState(keyState))
    {
        return 0;
    }
    else
    {
        // This parameter must be 1 if a menu is active, zero otherwise.
        UINT wFlags = p->menuModeFlag ? 1 : 0;
        ascii = ToUnicodeEx((UINT)p->vkCode, (UINT)p->scanCode, keyState, (LPWSTR)text, 4, wFlags, layout);
        return (-1 == ascii) ? 0 : ascii;
    }
}
