/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextUtf16String_h__
#define __mmTextUtf16String_h__

#include "core/mmCore.h"
#include "core/mmUtf16String.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmTextUtf16String
{
    struct mmUtf16String hString;
};

MM_EXPORT_NWSI 
void 
mmTextUtf16String_Init(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_Destroy(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
size_t 
mmTextUtf16String_Size(
    const struct mmTextUtf16String*                p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_OnFinishLaunching(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_OnBeforeTerminate(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_NativeInit(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_NativeDestroy(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_NativeEncode(
    struct mmTextUtf16String*                      p);

MM_EXPORT_NWSI 
void 
mmTextUtf16String_NativeDecode(
    struct mmTextUtf16String*                      p);

#include "core/mmSuffix.h"

#endif//__mmTextUtf16String_h__
