/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmKeystroke_h__
#define __mmKeystroke_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

// https://docs.microsoft.com/en-us/windows/win32/inputdev/about-keyboard-input#keystroke-message-flags
struct mmKeystroke
{
    WORD vkCode;       // virtual-key code

    BYTE scanCode;     // scan code
    BOOL scanCodeE0;   // extended-key flag, 1 if scancode has 0xE0 prefix

    BOOL upFlag;       // transition-state flag, 1 on keyup
    BOOL repeatFlag;   // previous key-state flag, 1 on autorepeat
    WORD repeatCount;  // repeat count, > 0 if several keydown messages was combined into one message

    BOOL altDownFlag;  // ALT key was pressed

    BOOL dlgModeFlag;  // dialog box is active
    BOOL menuModeFlag; // menu is active
};

MM_EXPORT_NWSI void mmKeystrokeMake(struct mmKeystroke* p, WPARAM wParam, LPARAM lParam);
MM_EXPORT_NWSI int mmKeystrokeToUnicode(struct mmKeystroke* p);
MM_EXPORT_NWSI int mmKeystrokeToCharacters(struct mmKeystroke* p, mmUInt16_t text[4]);

#include "core/mmSuffix.h"

#endif//__mmKeystroke_h__
