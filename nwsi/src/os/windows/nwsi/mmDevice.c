/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDevice.h"

#include "core/mmLogger.h"

// GetVersionEx
#pragma warning(disable: 4996)

MM_EXPORT_NWSI void mmDevice_Init(struct mmDevice* p)
{
    mmString_Init(&p->hName);
    mmString_Init(&p->hModel);
    mmString_Init(&p->hLocalizedModel);
    mmString_Init(&p->hSystemName);
    mmString_Init(&p->hSystemVersion);
    
    mmString_Init(&p->hSysName);
    mmString_Init(&p->hNodeName);
    mmString_Init(&p->hRelease);
    mmString_Init(&p->hVersion);
    mmString_Init(&p->hMachine);
    
    mmString_Assigns(&p->hName, "");
    mmString_Assigns(&p->hModel, "");
    mmString_Assigns(&p->hLocalizedModel, "");
    mmString_Assigns(&p->hSystemName, "");
    mmString_Assigns(&p->hSystemVersion, "");
    
    mmString_Assigns(&p->hSysName, "");
    mmString_Assigns(&p->hNodeName, "");
    mmString_Assigns(&p->hRelease, "");
    mmString_Assigns(&p->hVersion, "");
    mmString_Assigns(&p->hMachine, "");
}
MM_EXPORT_NWSI void mmDevice_Destroy(struct mmDevice* p)
{
    mmString_Destroy(&p->hName);
    mmString_Destroy(&p->hModel);
    mmString_Destroy(&p->hLocalizedModel);
    mmString_Destroy(&p->hSystemName);
    mmString_Destroy(&p->hSystemVersion);
    
    mmString_Destroy(&p->hSysName);
    mmString_Destroy(&p->hNodeName);
    mmString_Destroy(&p->hRelease);
    mmString_Destroy(&p->hVersion);
    mmString_Destroy(&p->hMachine);
}

MM_EXPORT_NWSI void mmDevice_OnFinishLaunching(struct mmDevice* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    BOOL hCode0 = FALSE;
    OSVERSIONINFOEXA hOSInfo;

    char hVersion[128] = { 0 };
    char hRelease[64] = { 0 };

    BOOL hCode1 = FALSE;
    DWORD hNameLength = 128;
    char hName[128] = { 0 };

    SYSTEM_INFO hSystemInfo;
    const char* pSystemMachine = "";

    memset(&hOSInfo, 0, sizeof(OSVERSIONINFOEX));
    hOSInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    hCode0 = GetVersionExA((LPOSVERSIONINFOA)&hOSInfo);
    if (hCode0)
    {
        sprintf(hVersion, "%u.%u.%u %u.%u",
            hOSInfo.dwMajorVersion,
            hOSInfo.dwMinorVersion,
            hOSInfo.dwBuildNumber,
            hOSInfo.wServicePackMajor,
            hOSInfo.wServicePackMinor);

        sprintf(hRelease, "%u", hOSInfo.dwBuildNumber);
    }
    else
    {
        sprintf(hVersion, "Unknown Version");
        sprintf(hRelease, "%u", hOSInfo.dwBuildNumber);

        mmLogger_LogW(gLogger, "GetVersionEx failure.");
    }

    hCode1 = GetComputerNameA(hName, &hNameLength);
    if (!hCode1)
    {
        sprintf(hName, "Unknown Computer Name");
        mmLogger_LogW(gLogger, "GetComputerName failure.");
    }

    // Copy the hardware information to the SYSTEM_INFO structure.
    GetNativeSystemInfo(&hSystemInfo);
    if (hSystemInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64 || 
        hSystemInfo.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64)
    {
        pSystemMachine = "x64";
    }
    else
    {
        pSystemMachine = "x32";
    }

    mmString_Assigns(&p->hName, hName);
    mmString_Assigns(&p->hModel, "");
    mmString_Assigns(&p->hLocalizedModel, "");
    mmString_Assigns(&p->hSystemName, "Windows");
    mmString_Assigns(&p->hSystemVersion, hVersion);

    mmString_Assigns(&p->hSysName, "Windows");
    mmString_Assigns(&p->hNodeName, hName);
    mmString_Assigns(&p->hRelease, hRelease);
    mmString_Assigns(&p->hVersion, hVersion);
    mmString_Assigns(&p->hMachine, pSystemMachine);
}
MM_EXPORT_NWSI void mmDevice_OnBeforeTerminate(struct mmDevice* p)
{
    // need do nothing here.
}

