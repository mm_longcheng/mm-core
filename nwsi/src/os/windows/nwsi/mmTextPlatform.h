/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextPlatform_h__
#define __mmTextPlatform_h__

#include "core/mmCore.h"

#include <d2d1.h>
#include <dwrite.h>

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_NWSI 
DWRITE_FONT_WEIGHT 
mmTextPlatform_GetFontWeight(
    int                                            hWeight);

MM_EXPORT_NWSI 
DWRITE_FONT_STYLE 
mmTextPlatform_GetFontStyle(
    int                                            hStyle);

MM_EXPORT_NWSI 
FLOAT 
mmTextPlatform_GetFontSize(
    float                                          hSize);

MM_EXPORT_NWSI 
void 
mmTextPlatform_GetSystemDefaultLocaleName(
    WCHAR                                          hLocaleName[32]);

MM_EXPORT_NWSI 
int 
mmTextPlatform_GetTextBaseDirection(
    const mmUInt16_t*                              pWText, 
    size_t                                         hLength);

MM_EXPORT_NWSI 
DWRITE_TEXT_ALIGNMENT 
mmTextPlatform_GetTextAlignment(
    int                                            hTextAlignment, 
    int                                            hBaseDirection);

MM_EXPORT_NWSI 
DWRITE_PARAGRAPH_ALIGNMENT 
mmTextPlatform_GetTextParagraphAlignment(
    int                                            hParagraphAlignment);

MM_EXPORT_NWSI 
DWRITE_WORD_WRAPPING 
mmTextPlatform_GetTextWordWrapping(
    int                                            hLineBreakMode);

MM_EXPORT_NWSI 
DWRITE_READING_DIRECTION 
mmTextPlatform_GetReadingDirection(
    int                                            hWritingDirection, 
    int                                            hBaseDirection);

#include "core/mmSuffix.h"

#endif//__mmTextPlatform_h__
