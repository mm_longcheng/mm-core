/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


#include "mmRegistry.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

MM_EXPORT_NWSI int mmRegistry_KeyInsert(HKEY hKey, const char* pSubKey, const char* pField, struct mmString* pValue)
{
    int hCode = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        DWORD hError = 0;
        HKEY hSubKeyHandle = 0;
        DWORD hFieldType = REG_SZ;

        hError = RegCreateKeyA(hKey, pSubKey, &hSubKeyHandle);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegCreateKeyA pSubKey: %s failure: %u.", pSubKey, hErrorCode);
            hCode = 1;
            break;
        }

        hError = RegSetValueExA(hSubKeyHandle, pField, 0, hFieldType, (BYTE*)(mmString_CStr(pValue)), (DWORD)mmString_Size(pValue));
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegSetValueExA hFieldType: %s failure: %u.", hFieldType, hErrorCode);
            RegCloseKey(hSubKeyHandle);
            hCode = 2;
            break;
        }

        RegCloseKey(hSubKeyHandle);

        hCode = MM_SUCCESS;
    } while (0);
    return hCode;
}
MM_EXPORT_NWSI int mmRegistry_KeyUpdate(HKEY hKey, const char* pSubKey, const char* pField, struct mmString* pValue)
{
    int hCode = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        DWORD hError = 0;
        HKEY hSubKeyHandle = 0;
        DWORD hFieldType = REG_SZ;

        hError = RegOpenKeyExA(hKey, pSubKey, 0, KEY_SET_VALUE, &hSubKeyHandle);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegOpenKeyExA pSubKey: %s failure: %u.", pSubKey, hErrorCode);
            hCode = 1;
            break;
        }
        hError = RegSetValueExA(hSubKeyHandle, pField, 0, hFieldType, (BYTE*)(mmString_CStr(pValue)), (DWORD)mmString_Size(pValue));
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegSetValueExA hFieldType: %s failure: %u.", hFieldType, hErrorCode);
            RegCloseKey(hSubKeyHandle);
            hCode = 2;
            break;
        }

        RegCloseKey(hSubKeyHandle);

        hCode = MM_SUCCESS;
    } while (0);
    return hCode;
}
MM_EXPORT_NWSI int mmRegistry_KeySelect(HKEY hKey, const char* pSubKey, const char* pField, struct mmString* pValue)
{
    int hCode = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        DWORD hError = 0;

        HKEY hSubKeyHandle = 0;
        DWORD hFieldType = REG_SZ;
        DWORD hFieldSize = 0;
        char* hFieldData = NULL;

        hError = RegOpenKeyExA(hKey, pSubKey, 0, KEY_QUERY_VALUE, &hSubKeyHandle);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogI(gLogger, "RegOpenKeyExA pSubKey: %s not exist: %u.", pSubKey, hErrorCode);
            hCode = 1;
            break;
        }
        RegQueryValueExA(hSubKeyHandle, pField, NULL, &hFieldType, (LPBYTE)NULL, &hFieldSize);
        hFieldData = mmMalloc(hFieldSize);
        hError = RegQueryValueExA(hSubKeyHandle, pField, NULL, &hFieldType, (LPBYTE)hFieldData, &hFieldSize);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogI(gLogger, "RegQueryValueExA pField: %s not exist: %u.", pField, hErrorCode);
            RegCloseKey(hSubKeyHandle);
            mmFree(hFieldData);
            hCode = 2;
            break;
        }

        mmString_Assignsn(pValue, (const char*)hFieldData, hFieldSize);
        RegCloseKey(hSubKeyHandle);
        mmFree(hFieldData);

        hCode = MM_SUCCESS;
    } while (0);

    return hCode;
}
MM_EXPORT_NWSI int mmRegistry_KeyDelete(HKEY hKey, const char* pSubKey, const char* pField)
{
    int hCode = MM_UNKNOWN;

    struct mmLogger* gLogger = mmLogger_Instance();

    DWORD hError = 0;

    hError = RegDeleteKeyValueA(hKey, pSubKey, pField);
    if (ERROR_SUCCESS != hError)
    {
        DWORD hErrorCode = GetLastError();
        mmLogger_LogE(gLogger, "RegDeleteKeyValueA pSubKey: %s pField: %s failure: %u.", pSubKey, pField, hErrorCode);
        hCode = 1;
    }
    else
    {
        hCode = MM_SUCCESS;
    }
    return hCode;
}

MM_EXPORT_NWSI void mmRegistry_SubKey(const char* pAppId, const char* pGroup, char pSubKey[128])
{
    mmSprintf(pSubKey, "Software\\mm\\%s\\%s", pAppId, pGroup);
}

MM_EXPORT_NWSI int mmRegistry_Insert(const char* pAppId, const char* pGroup, const char* pField, struct mmString* pValue)
{
    int hCode = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        DWORD hError = 0;
        HKEY hSubKeyHandle = 0;
        DWORD hFieldType = REG_SZ;

        char pSubKey[128] = { 0 };
        mmRegistry_SubKey(pAppId, pGroup, pSubKey);

        hError = RegCreateKeyA(HKEY_CURRENT_USER, pSubKey, &hSubKeyHandle);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegCreateKeyA pSubKey: %s failure: %u.", pSubKey, hErrorCode);
            hCode = 1;
            break;
        }

        hError = RegSetValueExA(hSubKeyHandle, pField, 0, hFieldType, (BYTE*)(mmString_CStr(pValue)), (DWORD)mmString_Size(pValue));
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegSetValueExA hFieldType: %s failure: %u.", hFieldType, hErrorCode);
            RegCloseKey(hSubKeyHandle);
            hCode = 2;
            break;
        }

        RegCloseKey(hSubKeyHandle);

        hCode = MM_SUCCESS;
    } while (0);
    return hCode;
}
MM_EXPORT_NWSI int mmRegistry_Update(const char* pAppId, const char* pGroup, const char* pField, struct mmString* pValue)
{
    int hCode = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        DWORD hError = 0;
        HKEY hSubKeyHandle = 0;
        DWORD hFieldType = REG_SZ;

        char pSubKey[128] = { 0 };
        mmRegistry_SubKey(pAppId, pGroup, pSubKey);

        hError = RegOpenKeyExA(HKEY_CURRENT_USER, pSubKey, 0, KEY_SET_VALUE, &hSubKeyHandle);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegOpenKeyExA pSubKey: %s failure: %u.", pSubKey, hErrorCode);
            hCode = 1;
            break;
        }
        hError = RegSetValueExA(hSubKeyHandle, pField, 0, hFieldType, (BYTE*)(mmString_CStr(pValue)), (DWORD)mmString_Size(pValue));
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogE(gLogger, "RegSetValueExA hFieldType: %s failure: %u.", hFieldType, hErrorCode);
            RegCloseKey(hSubKeyHandle);
            hCode = 2;
            break;
        }

        RegCloseKey(hSubKeyHandle);

        hCode = MM_SUCCESS;
    } while (0);
    return hCode;
}
MM_EXPORT_NWSI int mmRegistry_Select(const char* pAppId, const char* pGroup, const char* pField, struct mmString* pValue)
{
    int hCode = MM_UNKNOWN;
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        DWORD hError = 0;

        HKEY hSubKeyHandle = 0;
        DWORD hFieldType = REG_SZ;
        DWORD hFieldSize = 0;
        char* hFieldData = NULL;

        char pSubKey[128] = { 0 };
        mmRegistry_SubKey(pAppId, pGroup, pSubKey);

        hError = RegOpenKeyExA(HKEY_CURRENT_USER, pSubKey, 0, KEY_QUERY_VALUE, &hSubKeyHandle);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogI(gLogger, "RegOpenKeyExA pSubKey: %s not exist: %u.", pSubKey, hErrorCode);
            hCode = 1;
            break;
        }
        RegQueryValueExA(hSubKeyHandle, pField, NULL, &hFieldType, (LPBYTE)NULL, &hFieldSize);
        hFieldData = mmMalloc(hFieldSize);
        hError = RegQueryValueExA(hSubKeyHandle, pField, NULL, &hFieldType, (LPBYTE)hFieldData, &hFieldSize);
        if (ERROR_SUCCESS != hError)
        {
            DWORD hErrorCode = GetLastError();
            mmLogger_LogI(gLogger, "RegQueryValueExA pField: %s not exist: %u.", pField, hErrorCode);
            RegCloseKey(hSubKeyHandle);
            mmFree(hFieldData);
            hCode = 2;
            break;
        }

        mmString_Assignsn(pValue, (const char*)hFieldData, hFieldSize);
        RegCloseKey(hSubKeyHandle);
        mmFree(hFieldData);

        hCode = MM_SUCCESS;
    } while (0);

    return hCode;
}
MM_EXPORT_NWSI int mmRegistry_Delete(const char* pAppId, const char* pGroup, const char* pField)
{
    int hCode = MM_UNKNOWN;

    struct mmLogger* gLogger = mmLogger_Instance();

    DWORD hError = 0;

    char pSubKey[128] = { 0 };
    mmRegistry_SubKey(pAppId, pGroup, pSubKey);

    hError = RegDeleteKeyValueA(HKEY_CURRENT_USER, pSubKey, pField);
    if (ERROR_SUCCESS != hError)
    {
        DWORD hErrorCode = GetLastError();
        mmLogger_LogE(gLogger, "RegDeleteKeyValueA pSubKey: %s pField: %s failure: %u.", pSubKey, pField, hErrorCode);
        hCode = 1;
    }
    else
    {
        hCode = MM_SUCCESS;
    }
    return hCode;
}
