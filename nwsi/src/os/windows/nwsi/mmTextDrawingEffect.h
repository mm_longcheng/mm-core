/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextDrawingEffect_h__
#define __mmTextDrawingEffect_h__

#include "core/mmCore.h"

#include <combaseapi.h>

#include "nwsi/mmNwsiExport.h"

struct mmTextFormat;

DECLARE_INTERFACE_IID_(mmIDrawingEffect, IUnknown, "2a7d4190-3a9c-4473-9e60-961e7bb407f2")
{
    STDMETHOD(GetTextFormat)(struct mmTextFormat** c) PURE;
    STDMETHOD(SetTextFormat)(struct mmTextFormat* c) PURE;
};

#include "core/mmPrefix.h"

MM_EXPORT_NWSI 
HRESULT 
mmTextFactory_CreateDrawingEffect(
    struct mmIDrawingEffect**                      pEffect);

#include "core/mmSuffix.h"

#endif//__mmTextDrawingEffect_h__
