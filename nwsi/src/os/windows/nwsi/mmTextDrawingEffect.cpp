/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextDrawingEffect.h"
#include "nwsi/mmTextFormat.h"

class mmTextDrawingEffect : public mmIDrawingEffect
{
public:
    ULONG hRefCount;

    struct mmTextFormat* pTextFormat;
public:
    mmTextDrawingEffect(void);

    STDMETHOD(GetTextFormat)(struct mmTextFormat** c);
    STDMETHOD(SetTextFormat)(struct mmTextFormat* c);
public:
    // IUnknowns
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(
        /* [in] */ REFIID riid,
        /* [iid_is][out] */ _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject);

    virtual ULONG STDMETHODCALLTYPE AddRef(void);

    virtual ULONG STDMETHODCALLTYPE Release(void);
};

MM_EXPORT_NWSI
HRESULT
mmTextFactory_CreateDrawingEffect(
    struct mmIDrawingEffect**                      pEffect)
{
    (*pEffect) = new mmTextDrawingEffect;
    (*pEffect)->AddRef();
    return S_OK;
}

mmTextDrawingEffect::mmTextDrawingEffect(void)
    : hRefCount(0)
    , pTextFormat(NULL)
{

}

HRESULT mmTextDrawingEffect::GetTextFormat(struct mmTextFormat** v)
{
    *v = this->pTextFormat;
    return (NULL != this->pTextFormat) ? S_OK : E_POINTER;
}

HRESULT mmTextDrawingEffect::SetTextFormat(struct mmTextFormat* v)
{
    this->pTextFormat = v;
    return S_OK;
}


HRESULT STDMETHODCALLTYPE mmTextDrawingEffect::QueryInterface(
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ _COM_Outptr_ void __RPC_FAR *__RPC_FAR *ppvObject)
{
    if (riid == IID_IUnknown || 
        riid == __uuidof(mmIDrawingEffect))
    {
        *ppvObject = this;
        this->AddRef();
        return NOERROR;
    }
    else
    {
        *ppvObject = NULL;
        return E_NOINTERFACE;
    }
}

ULONG STDMETHODCALLTYPE mmTextDrawingEffect::AddRef(void)
{
    return ++this->hRefCount;
}

ULONG STDMETHODCALLTYPE mmTextDrawingEffect::Release(void)
{
    // Decrement the object's internal counter.
    if (0 == --this->hRefCount)
    {
        delete this;
    }
    return this->hRefCount;
}
