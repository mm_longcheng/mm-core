/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmGraphicsFactory.h"
#include "mmGraphicsPlatform.h"

#include "core/mmLogger.h"
#include "core/mmUtf16String.h"
#include "core/mmUTFConvert.h"
#include "core/mmByte.h"
#include "core/mmAlloc.h"

#include "dish/mmFileContext.h"

#include <d2d1.h>
#include <dwrite.h>
#include <dwrite_3.h>
#include <wincodec.h>

#pragma comment(lib,"D2d1.lib")
#pragma comment(lib,"Dwrite.lib")
#pragma comment(lib,"Windowscodecs.lib")

static
HRESULT
mmGraphicsFactory_UpdateFontCollection(
    struct mmGraphicsFactory*                      p)
{
    HRESULT hr = S_OK;

    do
    {
        struct IDWriteFontFile* pFontFile = NULL;
        struct mmFontResource* u;
        struct mmRbNode* n = NULL;
        struct mmRbtreeStrVptIterator* it = NULL;

        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDWriteFactory)
        {
            mmLogger_LogT(gLogger, "%s %d"
                " pDWriteFactory is null.",
                __FUNCTION__, __LINE__);
            hr = E_POINTER;
            break;
        }

        mmIUnknownSafeRelease(p->pFontCollection);
        mmIUnknownSafeRelease(p->pFontSet);
        mmIUnknownSafeRelease(p->pFontSetBuilder2);

        p->pFontCollection = NULL;
        p->pFontSet = NULL;
        p->pFontSetBuilder2 = NULL;

        hr = p->pDWriteFactory->CreateFontSetBuilder(&p->pFontSetBuilder2);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CreateFontSetBuilder failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        n = mmRb_First(&p->hFonts.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeStrVptIterator, n);
            n = mmRb_Next(n);
            u = (struct mmFontResource*)it->v;
            pFontFile = (struct IDWriteFontFile*)u->pHandle;
            hr = p->pFontSetBuilder2->AddFontFile(pFontFile);
            if (FAILED(hr))
            {
                mmLogger_LogE(gLogger, "%s %d"
                    " AddFontFile failure.",
                    __FUNCTION__, __LINE__);
                break;
            }
        }

        hr = p->pFontSetBuilder2->CreateFontSet(&p->pFontSet);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CreateFontSet failure.",
                __FUNCTION__, __LINE__);
            break;
        }

        hr = p->pDWriteFactory->CreateFontCollectionFromFontSet(p->pFontSet, &p->pFontCollection);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CreateFontCollectionFromFontSet failure.",
                __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return hr;
}

static
int
mmGraphicsFactory_LoadFontFromFolder(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily,
    const char*                                    pFontPath,
    const char*                                    pRealPath)
{
    int err = MM_UNKNOWN;

    do
    {
        HRESULT hr;
        BOOL hErr;
        WIN32_FILE_ATTRIBUTE_DATA hAttr;
        const FILETIME* pLastWriteTime;
        struct IDWriteFontFile* pFontFile;

        const mmUtf16_t* pUtf16Path;
        struct mmString hWeakPath;
        struct mmUtf16String hUtf16Path;

        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        struct mmFontResource* u;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL != it)
        {
            err = MM_SUCCESS;
            break;
        }

        if (NULL == p->pDWriteFactory)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pDWriteFactory is a null.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        mmString_MakeWeaks(&hWeakPath, pRealPath);
        mmUtf16String_Init(&hUtf16Path);
        mmUTF_8to16(&hWeakPath, &hUtf16Path);
        pUtf16Path = mmUtf16String_CStr(&hUtf16Path);
        
        hErr = GetFileAttributesExW((const WCHAR*)pUtf16Path, GetFileExInfoStandard, &hAttr);
        pLastWriteTime = hErr ? &hAttr.ftLastWriteTime : NULL;
        hr = p->pDWriteFactory->CreateFontFileReference(
            (const WCHAR*)pUtf16Path,
            pLastWriteTime,
            &pFontFile);

        mmUtf16String_Destroy(&hUtf16Path);

        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CreateFontFileReference failure.", 
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        u = (struct mmFontResource*)mmMalloc(sizeof(struct mmFontResource));
        mmFontResource_Init(u);
        mmString_Assign(&u->hFamily, &hWeakFamily);
        mmString_Assigns(&u->hPath, pFontPath);
        mmString_Assigns(&u->hRealPath, pRealPath);
        u->pHandle = (void*)pFontFile;
        mmRbtreeStringVpt_Set(&p->hFonts, &hWeakFamily, u);

        hr = mmGraphicsFactory_UpdateFontCollection(p);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_UpdateFontCollection failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = MM_SUCCESS;
    } while (0);

    return err;
}

static
int
mmGraphicsFactory_UnloadFontByFolder(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily)
{
    int err = MM_UNKNOWN;

    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        HRESULT hr;
        struct IDWriteFontFile* pFontFile;

        const mmUtf16_t* pUtf16Path;
        struct mmUtf16String hUtf16Path;

        struct mmFontResource* u;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL == it)
        {
            err = MM_SUCCESS;
            break;
        }

        u = (struct mmFontResource*)it->v;

        mmUtf16String_Init(&hUtf16Path);
        mmUTF_8to16(&u->hRealPath, &hUtf16Path);
        pUtf16Path = mmUtf16String_CStr(&hUtf16Path);
        pFontFile = (struct IDWriteFontFile*)u->pHandle;

        mmRbtreeStringVpt_Rmv(&p->hFonts, &hWeakFamily);
        mmFontResource_Destroy(u);
        mmFree(u);

        mmIUnknownSafeRelease(pFontFile);

        mmUtf16String_Destroy(&hUtf16Path);
        if (NULL == pFontFile)
        {
            mmLogger_LogW(gLogger, "%s %d"
                " pFontFile is a null.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        if (NULL == pFontFile)
        {
            mmLogger_LogW(gLogger, "%s %d"
                " pFontFile is a null.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        hr = mmGraphicsFactory_UpdateFontCollection(p);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_UpdateFontCollection failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = MM_SUCCESS;

    } while (0);

    return err;
}

static
int
mmGraphicsFactory_LoadFontFromSource(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily,
    const char*                                    pFontPath,
    const char*                                    pRealPath,
    struct mmByteBuffer*                           pBytes)
{
    int err = MM_UNKNOWN;

    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        HRESULT hr;
        struct IDWriteFontFile* pFontFile;

        const void* pData;
        UINT32 hSize;

        struct mmFontResource* u;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL != it)
        {
            err = MM_SUCCESS;
            break;
        }

        if (NULL == pBytes->buffer || 0 == pBytes->length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pBytes is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        if (NULL == p->pMemoryFontFileLoader)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pMemoryFontFileLoader is a null.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        pData = (const void*)(pBytes->buffer + pBytes->offset);
        hSize = (UINT32)(pBytes->length);

        hr = p->pMemoryFontFileLoader->CreateInMemoryFontFileReference(
            p->pDWriteFactory, 
            pData, 
            hSize, 
            NULL, 
            &pFontFile);

        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CreateInMemoryFontFileReference failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        u = (struct mmFontResource*)mmMalloc(sizeof(struct mmFontResource));
        mmFontResource_Init(u);
        mmString_Assign(&u->hFamily, &hWeakFamily);
        mmString_Assigns(&u->hPath, pFontPath);
        mmString_Assigns(&u->hRealPath, pRealPath);
        u->pHandle = (void*)pFontFile;
        mmRbtreeStringVpt_Set(&p->hFonts, &hWeakFamily, u);

        hr = mmGraphicsFactory_UpdateFontCollection(p);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_UpdateFontCollection failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = MM_SUCCESS;
    } while (0);

    return err;
}

static
int
mmGraphicsFactory_UnloadFontBySource(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily)
{
    int err = MM_UNKNOWN;

    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        HRESULT hr;
        struct IDWriteFontFile* pFontFile;

        struct mmFontResource* u;

        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL == it)
        {
            err = MM_SUCCESS;
            break;
        }

        u = (struct mmFontResource*)it->v;

        pFontFile = (struct IDWriteFontFile*)u->pHandle;

        mmRbtreeStringVpt_Rmv(&p->hFonts, &hWeakFamily);
        mmFontResource_Destroy(u);
        mmFree(u);

        mmIUnknownSafeRelease(pFontFile);

        if (NULL == pFontFile)
        {
            mmLogger_LogW(gLogger, "%s %d"
                " pFontFile is a null.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        hr = mmGraphicsFactory_UpdateFontCollection(p);
        if (FAILED(hr))
        {
            mmLogger_LogE(gLogger, "%s %d"
                " mmGraphicsFactory_UpdateFontCollection failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        err = MM_SUCCESS;

    } while (0);

    return err;
}

static
HRESULT
mmGraphicsFactory_FontLoaderPrepare(
    struct mmGraphicsFactory*                      p)
{
    HRESULT hr = S_OK;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDWriteFactory)
        {
            mmLogger_LogT(gLogger, "%s %d"
                " pDWriteFactory is null.", 
                __FUNCTION__, __LINE__);
            hr = E_POINTER;
            break;
        }

        hr = p->pDWriteFactory->CreateInMemoryFontFileLoader(&p->pMemoryFontFileLoader);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " CreateInMemoryFontFileLoader failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

        hr = p->pDWriteFactory->RegisterFontFileLoader(p->pMemoryFontFileLoader);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " RegisterFontFileLoader MemoryFontFileLoader failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

    } while (0);

    return hr;
}

static
void
mmGraphicsFactory_FontLoaderDiscard(
    struct mmGraphicsFactory*                      p)
{
    do
    {
        HRESULT hr = S_OK;
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pDWriteFactory)
        {
            mmLogger_LogT(gLogger, "%s %d"
                " pDWriteFactory is null.", 
                __FUNCTION__, __LINE__);
            break;
        }

        hr = p->pDWriteFactory->UnregisterFontFileLoader(p->pMemoryFontFileLoader);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " UnregisterFontFileLoader MemoryFontFileLoader failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

        mmIUnknownSafeRelease(p->pMemoryFontFileLoader);
        p->pMemoryFontFileLoader = NULL;

    } while (0);
}

MM_EXPORT_NWSI
void
mmFontResource_Init(
    struct mmFontResource*                         p)
{
    mmString_Init(&p->hFamily);
    mmString_Init(&p->hPath);
    mmString_Init(&p->hRealPath);
    p->pHandle = NULL;
}

MM_EXPORT_NWSI
void
mmFontResource_Destroy(
    struct mmFontResource*                         p)
{
    p->pHandle = NULL;
    mmString_Destroy(&p->hRealPath);
    mmString_Destroy(&p->hPath);
    mmString_Destroy(&p->hFamily);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_Init(
    struct mmGraphicsFactory*                      p)
{
    mmRbtreeStringVpt_Init(&p->hFonts);

    p->pPackageAssets = NULL;

    p->pD2DFactory = NULL;
    p->pDWriteFactory = NULL;
    p->pWICImagingFactory = NULL;

    p->pMemoryFontFileLoader = NULL;
    p->pFontSetBuilder2 = NULL;
    p->pFontSet = NULL;
    p->pFontCollection = NULL;
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_Destroy(
    struct mmGraphicsFactory*                      p)
{
    assert(0 == p->hFonts.size && "assets is not destroy complete.");

    p->pFontCollection = NULL;
    p->pFontSet = NULL;
    p->pFontSetBuilder2 = NULL;
    p->pMemoryFontFileLoader = NULL;

    p->pWICImagingFactory = NULL;
    p->pDWriteFactory = NULL;
    p->pD2DFactory = NULL;

    p->pPackageAssets = NULL;

    mmRbtreeStringVpt_Destroy(&p->hFonts);
}

MM_EXPORT_NWSI 
void 
mmGraphicsFactory_SetPackageAssets(
    struct mmGraphicsFactory*                      p,
    struct mmPackageAssets*                        pPackageAssets)
{
    p->pPackageAssets = pPackageAssets;
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_OnFinishLaunching(
    struct mmGraphicsFactory*                      p)
{
    HRESULT hr = S_OK;

    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        // COM Initialize.
        CoInitialize(NULL);

        hr = D2D1CreateFactory(
            D2D1_FACTORY_TYPE_SINGLE_THREADED,
            reinterpret_cast<IUnknown**>(&p->pD2DFactory)
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " D2D1CreateFactory failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

        hr = DWriteCreateFactory(
            DWRITE_FACTORY_TYPE_SHARED,
            __uuidof(IDWriteFactory7),
            reinterpret_cast<IUnknown**>(&p->pDWriteFactory)
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " DWriteCreateFactory failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

        hr = CoCreateInstance(
            CLSID_WICImagingFactory,
            NULL,
            CLSCTX_INPROC_SERVER,
            IID_IWICImagingFactory,
            (LPVOID*)&p->pWICImagingFactory
        );
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " CoCreateInstance failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

        hr = mmGraphicsFactory_FontLoaderPrepare(p);
        if (FAILED(hr))
        {
            mmLogger_LogT(gLogger, "%s %d"
                " mmGraphicsFactory_FontLoaderPrepare failure.", 
                __FUNCTION__, __LINE__);
            break;
        }

    } while (0);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_OnBeforeTerminate(
    struct mmGraphicsFactory*                      p)
{
    mmGraphicsFactory_FontLoaderDiscard(p);

    mmIUnknownSafeRelease(p->pDWriteFactory);
    mmIUnknownSafeRelease(p->pD2DFactory);
    mmIUnknownSafeRelease(p->pWICImagingFactory);

    // COM Uninitialize.
    CoUninitialize();

    p->pD2DFactory = NULL;
    p->pDWriteFactory = NULL;
    p->pWICImagingFactory = NULL;
}

MM_EXPORT_NWSI
int
mmGraphicsFactory_LoadFontFromResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath)
{
    int err = MM_UNKNOWN;
    int hAssetsType;
    const char* pRealPath;
    struct mmString hRealPath;
    mmString_Init(&hRealPath);

    hAssetsType = mmFileContext_GetFileAssetsPath(
        pFileContext, 
        pFontPath, 
        &hRealPath);

    pRealPath = mmString_CStr(&hRealPath);

    switch (hAssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        err = mmGraphicsFactory_LoadFontFromFolder(
            p, 
            pFontFamily, 
            pFontPath, 
            pRealPath);
    }
    break;

    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        struct mmByteBuffer hBytes;

        mmFileContext_AcquireFileByteBuffer(
            pFileContext, 
            pFontPath, 
            &hBytes);

        err = mmGraphicsFactory_LoadFontFromSource(
            p, 
            pFontFamily, 
            pFontPath, 
            pRealPath, 
            &hBytes);

        mmFileContext_ReleaseFileByteBuffer(
            pFileContext, 
            &hBytes);
    }
    break;

    default:
    {
        // Not support assets type.
        err = MM_FAILURE;
    }
    break;
    }
    mmString_Destroy(&hRealPath);
    return err;
}

MM_EXPORT_NWSI
int
mmGraphicsFactory_UnloadFontByResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath)
{
    int err = MM_UNKNOWN;
    int hAssetsType;

    hAssetsType = mmFileContext_GetFileAssetsType(
        pFileContext, 
        pFontPath);

    switch (hAssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        err = mmGraphicsFactory_UnloadFontByFolder(p, pFontFamily);
    }
    break;

    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        err = mmGraphicsFactory_UnloadFontBySource(p, pFontFamily);
    }
    break;

    default:
    {
        // Not support assets type.
        err = MM_FAILURE;
    }
    break;
    }
    return err;
}

MM_EXPORT_NWSI
const struct mmFontResource*
mmGraphicsFactory_GetFontResourceByFamily(
    const struct mmGraphicsFactory*                p,
    const char*                                    pFontFamily)
{
    const struct mmFontResource* u = NULL;
    struct mmString hWeakFamily;
    struct mmRbtreeStringVptIterator* it;
    mmString_MakeWeaks(&hWeakFamily, pFontFamily);
    it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
    if (NULL != it)
    {
        u = (const struct mmFontResource*)it->v;
    }
    else
    {
        u = NULL;
    }
    return u;
}

