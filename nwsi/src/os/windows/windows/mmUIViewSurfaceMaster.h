/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUIViewSurfaceMaster_h__
#define __mmUIViewSurfaceMaster_h__

#include "core/mmCore.h"

#include "nwsi/mmUIViewDefineWindows.h"
#include "nwsi/mmISurface.h"
#include "nwsi/mmSurfaceMaster.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

#define mmUIViewSurfaceMaster_X_DEFAULT 0
#define mmUIViewSurfaceMaster_Y_DEFAULT 0
// 720  * 0.525 = 378
#define mmUIViewSurfaceMaster_W_DEFAULT 378
// 1280 * 0.525 = 672
#define mmUIViewSurfaceMaster_H_DEFAULT 672

struct mmUIViewSurfaceMaster
{
    struct mmEventSurfaceContent hSurfaceContent;
    struct mmISurfaceKeypad hISurfaceKeypad;
    
    // weak ref.
    struct mmSurfaceMaster* pSurfaceMaster;
    
    // weak ref.
    struct mmISurface* pISurface;
    
    double hDisplayDensity;

    int hOPosition[2];
    mmUInt32_t hModifierMask;
    mmUInt32_t hButtonMask;

    double hTextEditRect[4];

    CHAR szTitle[MM_MAX_LOADSTRING];

    HWND hWnd;
    HINSTANCE hInstance;
    HACCEL hAcceleratorTable;
    int nCmdShow;

    BOOL bMenu;
    DWORD dwExStyle;
    DWORD dwStyle;

    HWND hWndParent;

    int hWindowX;
    int hWindowY;
    int hWindowW;
    int hWindowH;

    int hClientX;
    int hClientY;
    int hClientW;
    int hClientH;
};

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Init(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Destroy(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetSurfaceMaster(struct mmUIViewSurfaceMaster* p, struct mmSurfaceMaster* pSurfaceMaster);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetInstance(struct mmUIViewSurfaceMaster* p, HINSTANCE hInstance);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetCommandShow(struct mmUIViewSurfaceMaster* p, int nCmdShow);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetClientRect(struct mmUIViewSurfaceMaster* p, int x, int y, int w, int h);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetWndParent(struct mmUIViewSurfaceMaster* p, HWND hWndParent);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetAcceleratorTable(struct mmUIViewSurfaceMaster* p, HACCEL hAcceleratorTable);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetDisplayDensity(struct mmUIViewSurfaceMaster* p, double hDisplayDensity);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnUpdate(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnTimewait(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnTimewake(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnDisplayLinkUpdate(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnUpdateSize(struct mmUIViewSurfaceMaster* p, int w, int h);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnFinishLaunching(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnBeforeTerminate(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnEnterBackground(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnEnterForeground(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnStart(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnInterrupt(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnShutdown(struct mmUIViewSurfaceMaster* p);
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnJoin(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI ATOM mmUIViewSurfaceMaster_RegisterClass(HINSTANCE hInstance);
MM_EXPORT_NWSI BOOL mmUIViewSurfaceMaster_UnregisterClass(HINSTANCE hInstance);

#include "core/mmSuffix.h"

#endif//__mmUIViewSurfaceMaster_h__
