/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUIViewSurfaceMaster.h"
#include "mmOSVirtualKeycode.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmKeyCode.h"

#include "nwsi/mmContextMaster.h"
#include "nwsi/mmUnicodeWindows.h"
#include "nwsi/mmUIDisplayMetrics.h"
#include "nwsi/mmKeystroke.h"

#include <windowsx.h>

#pragma   comment(lib, "imm32.lib ")

#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL 0x020A
#define __WM_REALMOUSELAST WM_MOUSEWHEEL
#else
#define __WM_REALMOUSELAST WM_MOUSELAST
#endif // WM_MOUSEWHEEL
//
#define GET_HIWORD(param) ((unsigned short)HIWORD(param))
#define GET_LOWORD(param) ((unsigned short)LOWORD(param))

static CHAR __mmUIViewSurfaceMaster_szWindowClass[] = "mmUIViewSurfaceMaster";

static
void
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster);

static mmUInt32_t __static_mmOSModifierMask(unsigned short flags);

LRESULT CALLBACK __static_mmUIViewSurfaceMaster_WndProc(HWND, UINT, WPARAM, LPARAM);

static void __static_mmUIViewSurfaceMaster_PrepareSurfaceView(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_DiscardSurfaceView(struct mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_ShowWindow(struct mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadEnter(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadLeave(struct mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(struct mmUIViewSurfaceMaster* p);

static 
void 
__static_mmUIViewSurfaceMaster_OnUpdateMask(
    struct mmUIViewSurfaceMaster*                  p, 
    WPARAM                                         wParam, 
    LPARAM                                         lParam, 
    int                                            phase, 
    mmUInt32_t                                     button_id);

static
void
__static_mmUIViewSurfaceMaster_OnCursorDataHandler(
    struct mmUIViewSurfaceMaster*                  p,
    WPARAM                                         wParam,
    LPARAM                                         lParam,
    int                                            phase,
    mmUInt32_t                                     button_id);

static 
void 
__static_mmUIViewSurfaceMaster_OnTextDataHandler(
    struct mmUIViewSurfaceMaster*                  p, 
    WPARAM                                         wParam, 
    LPARAM                                         lParam, 
    struct mmKeystroke*                            hKeystroke, 
    int                                            phase);

static void __static_mmUIViewSurfaceMaster_OnUpdateSizeCache(struct mmUIViewSurfaceMaster* p, int x, int y, int w, int h);
static void __static_mmUIViewSurfaceMaster_OnSizeChange(struct mmUIViewSurfaceMaster* p, int cw, int ch);

static void __static_mmUIViewSurfaceMaster_ImmKeypadStatusShow(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ImmKeypadStatusHide(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ImmKeypadStatusUpdate(struct mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pISurface, int hType);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pISurface, int hAppearance);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pISurface, int hReturnKey);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pISurface);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pISurface);

static void __static_mmUIViewSurfaceMaster_OnKeypadInsertTextUtf16(struct mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_OnShortcutKey(struct mmUIViewSurfaceMaster* p);

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Init(struct mmUIViewSurfaceMaster* p)
{
    mmEventSurfaceContent_Init(&p->hSurfaceContent);
    mmISurfaceKeypad_Init(&p->hISurfaceKeypad);
            
    p->pSurfaceMaster = NULL;
    p->pISurface = NULL;
    
    p->hDisplayDensity = 1.0;

    mmMemset(p->hOPosition, 0, sizeof(p->hOPosition));
    p->hModifierMask = 0;
    p->hButtonMask = 0;

    mmMemset(p->hTextEditRect, 0, sizeof(double) * 4);

    mmMemset(p->szTitle, 0, sizeof(CHAR) * MM_MAX_LOADSTRING);
    p->hWnd = NULL;
    p->hInstance = NULL;
    p->hAcceleratorTable = NULL;
    p->nCmdShow = 0;

    p->bMenu = FALSE;
    p->dwExStyle = WS_EX_ACCEPTFILES;
    p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_CHILDWINDOW;

    p->hWndParent = NULL;

    p->hWindowX = 0;
    p->hWindowY = 0;
    p->hWindowW = 0;
    p->hWindowH = 0;

    p->hClientX = mmUIViewSurfaceMaster_X_DEFAULT;
    p->hClientY = mmUIViewSurfaceMaster_Y_DEFAULT;
    p->hClientW = mmUIViewSurfaceMaster_W_DEFAULT;
    p->hClientH = mmUIViewSurfaceMaster_H_DEFAULT;
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_Destroy(struct mmUIViewSurfaceMaster* p)
{
    p->hClientX = mmUIViewSurfaceMaster_X_DEFAULT;
    p->hClientY = mmUIViewSurfaceMaster_Y_DEFAULT;
    p->hClientW = mmUIViewSurfaceMaster_W_DEFAULT;
    p->hClientH = mmUIViewSurfaceMaster_H_DEFAULT;

    p->hWindowX = 0;
    p->hWindowY = 0;
    p->hWindowW = 0;
    p->hWindowH = 0;

    p->hWndParent = NULL;

    p->dwExStyle = WS_EX_ACCEPTFILES;
    p->dwStyle = WS_VISIBLE | WS_CLIPCHILDREN | WS_CHILDWINDOW;
    p->bMenu = FALSE;

    p->nCmdShow = 0;
    p->hAcceleratorTable = NULL;
    p->hInstance = NULL;
    p->hWnd = NULL;
    mmMemset(p->szTitle, 0, sizeof(CHAR) * MM_MAX_LOADSTRING);

    mmMemset(p->hTextEditRect, 0, sizeof(double) * 4);

    p->hButtonMask = 0;
    p->hModifierMask = 0;
    mmMemset(p->hOPosition, 0, sizeof(p->hOPosition));

    p->hDisplayDensity = 1.0;

    p->pSurfaceMaster = NULL;
    p->pISurface = NULL;
        
    mmISurfaceKeypad_Destroy(&p->hISurfaceKeypad);
    mmEventSurfaceContent_Destroy(&p->hSurfaceContent);
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetSurfaceMaster(struct mmUIViewSurfaceMaster* p, struct mmSurfaceMaster* pSurfaceMaster)
{
    void* pViewSurfaceMaster = (void*)p;

    p->pSurfaceMaster = pSurfaceMaster;
    p->pISurface = &p->pSurfaceMaster->hSuper;

    mmSurfaceMaster_SetViewSurface(pSurfaceMaster, pViewSurfaceMaster);
    mmSurfaceMaster_SetISurfaceKeypad(pSurfaceMaster, &p->hISurfaceKeypad);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetInstance(struct mmUIViewSurfaceMaster* p, HINSTANCE hInstance)
{
    p->hInstance = hInstance;
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetCommandShow(struct mmUIViewSurfaceMaster* p, int nCmdShow)
{
    p->nCmdShow = nCmdShow;
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetClientRect(struct mmUIViewSurfaceMaster* p, int x, int y, int w, int h)
{
    p->hClientX = x;
    p->hClientY = y;
    p->hClientW = w;
    p->hClientH = h;
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetWndParent(struct mmUIViewSurfaceMaster* p, HWND hWndParent)
{
    p->hWndParent = hWndParent;
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetAcceleratorTable(struct mmUIViewSurfaceMaster* p, HACCEL hAcceleratorTable)
{
    p->hAcceleratorTable = hAcceleratorTable;
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_SetDisplayDensity(struct mmUIViewSurfaceMaster* p, double hDisplayDensity)
{
    p->hDisplayDensity = hDisplayDensity;
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnUpdate(struct mmUIViewSurfaceMaster* p)
{
    (*(p->pISurface->OnUpdate))(p->pISurface);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnTimewait(struct mmUIViewSurfaceMaster* p)
{
    (*(p->pISurface->OnTimewait))(p->pISurface);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnTimewake(struct mmUIViewSurfaceMaster* p)
{
    (*(p->pISurface->OnTimewake))(p->pISurface);
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnDisplayLinkUpdate(struct mmUIViewSurfaceMaster* p)
{
    // Update.
    (*(p->pISurface->OnUpdate))(p->pISurface);
    // PeekMessage.
    __static_mmUIViewSurfaceMaster_ThreadPeekMessage(p);
    // Timewait.
    (*(p->pISurface->OnTimewait))(p->pISurface);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnUpdateSize(struct mmUIViewSurfaceMaster* p, int w, int h)
{
    UINT uFlags = SWP_NOZORDER | SWP_NOACTIVATE;
    __static_mmUIViewSurfaceMaster_OnUpdateSizeCache(p, p->hClientX, p->hClientY, w, h);
    SetWindowPos(p->hWnd, NULL, p->hWindowX, p->hWindowY, p->hWindowW, p->hWindowH, uFlags);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnFinishLaunching(struct mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

    mmSprintf(p->szTitle, "%s", mmString_CStr(&pPackageAssets->hPackageName));

    __static_mmUIViewSurfaceMaster_PrepareSurfaceView(p);
    __static_mmUIViewSurfaceMaster_ShowWindow(p);

    __static_mmUIViewSurfaceMaster_ThreadEnter(p);

    __static_mmUIViewSurfaceMaster_PrepareSwapchain(p);

    mmISurfaceKeypad_Reset(&p->hISurfaceKeypad);
    p->hISurfaceKeypad.OnSetKeypadType = &__static_mmUIViewSurfaceMaster_OnSetKeypadType;
    p->hISurfaceKeypad.OnSetKeypadAppearance = &__static_mmUIViewSurfaceMaster_OnSetKeypadAppearance;
    p->hISurfaceKeypad.OnSetKeypadReturnKey = &__static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey;
    p->hISurfaceKeypad.OnKeypadStatusShow = &__static_mmUIViewSurfaceMaster_OnKeypadStatusShow;
    p->hISurfaceKeypad.OnKeypadStatusHide = &__static_mmUIViewSurfaceMaster_OnKeypadStatusHide;

    (*(p->pISurface->OnFinishLaunching))(p->pISurface);

    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnFinishLaunching %ux%u.", p->hClientW, p->hClientH);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnBeforeTerminate(struct mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    (*(p->pISurface->OnBeforeTerminate))(p->pISurface);

    mmISurfaceKeypad_Reset(&p->hISurfaceKeypad);

    __static_mmUIViewSurfaceMaster_DiscardSwapchain(p);

    __static_mmUIViewSurfaceMaster_ThreadLeave(p);

    __static_mmUIViewSurfaceMaster_DiscardSurfaceView(p);

    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnBeforeTerminate.");
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnEnterBackground(struct mmUIViewSurfaceMaster* p)
{
    (*(p->pISurface->OnEnterBackground))(p->pISurface);
}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnEnterForeground(struct mmUIViewSurfaceMaster* p)
{
    (*(p->pISurface->OnEnterForeground))(p->pISurface);
}

MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnStart(struct mmUIViewSurfaceMaster* p)
{

}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnInterrupt(struct mmUIViewSurfaceMaster* p)
{

}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnShutdown(struct mmUIViewSurfaceMaster* p)
{

}
MM_EXPORT_NWSI void mmUIViewSurfaceMaster_OnJoin(struct mmUIViewSurfaceMaster* p)
{

}

MM_EXPORT_NWSI ATOM mmUIViewSurfaceMaster_RegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXA wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = __static_mmUIViewSurfaceMaster_WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);;
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = __mmUIViewSurfaceMaster_szWindowClass;
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    return RegisterClassExA(&wcex);
}
MM_EXPORT_NWSI BOOL mmUIViewSurfaceMaster_UnregisterClass(HINSTANCE hInstance)
{
    return UnregisterClassA(__mmUIViewSurfaceMaster_szWindowClass, hInstance);
}

static void __static_mmUIViewSurfaceMaster_PrepareSurfaceView(struct mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    RECT rc;

    SetRect(&rc, p->hClientX, p->hClientY, p->hClientX + p->hClientW, p->hClientY + p->hClientH);

    AdjustWindowRectEx(&rc, p->dwStyle, p->bMenu, p->dwExStyle);

    p->hWindowX = rc.left;
    p->hWindowY = rc.top;
    p->hWindowW = rc.right - rc.left;
    p->hWindowH = rc.bottom - rc.top;

    p->hWindowX = p->hWindowX < 0 ? 0 : p->hWindowX;
    p->hWindowY = p->hWindowY < 0 ? 0 : p->hWindowY;

    p->hWnd = CreateWindowExA(
        p->dwExStyle,
        __mmUIViewSurfaceMaster_szWindowClass, p->szTitle,
        p->dwStyle,
        p->hWindowX, p->hWindowY,
        p->hWindowW, p->hWindowH,
        p->hWndParent,
        NULL,
        p->hInstance,
        NULL);

    if (p->hWnd)
    {
        mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster CreateWindowEx success.");

        mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster Window((%d, %d), (%d, %d))", 
            p->hWindowX, p->hWindowY, p->hWindowW, p->hWindowH);

        mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster Client((%d, %d), (%d, %d))", 
            p->hClientX, p->hClientY, p->hClientW, p->hClientH);

        SetWindowLongPtr(p->hWnd, GWLP_USERDATA, (LONG_PTR)p);
    }
    else
    {
        mmLogger_LogF(gLogger, "mmUIViewSurfaceMaster CreateWindowEx failure.");

        mmLogger_LogF(gLogger, "mmUIViewSurfaceMaster Window((%d, %d), (%d, %d))", 
            p->hWindowX, p->hWindowY, p->hWindowW, p->hWindowH);

        mmLogger_LogF(gLogger, "mmUIViewSurfaceMaster Client((%d, %d), (%d, %d))", 
            p->hClientX, p->hClientY, p->hClientW, p->hClientH);
    }
}
static void __static_mmUIViewSurfaceMaster_DiscardSurfaceView(struct mmUIViewSurfaceMaster* p)
{
    if (NULL != p->hWnd)
    {
        DestroyWindow(p->hWnd);
        p->hWnd = NULL;
    }
}

static void __static_mmUIViewSurfaceMaster_ShowWindow(struct mmUIViewSurfaceMaster* p)
{
    ShowWindow(p->hWnd, p->nCmdShow);
    UpdateWindow(p->hWnd);
}

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(struct mmUIViewSurfaceMaster* p)
{
    MSG msg;
    // Peek message use PM_REMOVE.
    while (PeekMessage(&msg, p->hWnd, (UINT)0U, (UINT)0U, PM_REMOVE))
    {
        if (!TranslateAccelerator(msg.hwnd, p->hAcceleratorTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
}
static void __static_mmUIViewSurfaceMaster_ThreadEnter(struct mmUIViewSurfaceMaster* p)
{

}
static void __static_mmUIViewSurfaceMaster_ThreadLeave(struct mmUIViewSurfaceMaster* p)
{

}

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(struct mmUIViewSurfaceMaster* p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;

        RECT hClientRect;

        double hDisplayDensity = p->hDisplayDensity;

        const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);
        unsigned int w = 0;
        unsigned int h = 0;

        double hDisplayFrameSize[2];
        double hPhysicalSafeRect[4];

        struct mmEventMetrics* pMetrics = NULL;

        if (NULL == p->pSurfaceMaster)
        {
            mmLogger_LogI(gLogger, "%s %d pSurfaceMaster is null.", __FUNCTION__, __LINE__);
            break;
        }

        if (NULL == p->hWnd)
        {
            mmLogger_LogI(gLogger, "%s %d hWnd is null.", __FUNCTION__, __LINE__);
            break;
        }

        pMetrics = &p->hSurfaceContent.hMetrics;

        GetClientRect(p->hWnd, &hClientRect);

        w = (unsigned int)(hClientRect.right - hClientRect.left);
        h = (unsigned int)(hClientRect.bottom - hClientRect.top);

        hDisplayFrameSize[0] = (double)w;
        hDisplayFrameSize[1] = (double)h;

        // Safe rect.
        hPhysicalSafeRect[0] = (double)0.0;
        hPhysicalSafeRect[1] = (double)0.0;
        hPhysicalSafeRect[2] = (double)(hDisplayFrameSize[0] / hDisplayDensity);
        hPhysicalSafeRect[3] = (double)(hDisplayFrameSize[1] / hDisplayDensity);

        mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
        mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
        mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);

        // Logger infomation.
        mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.PrepareSwapchain");
        mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_INFO, pWindowName);

        // OnSurfacePrepare event.
        __static_mmSurfaceContentMetricsAssignment(pMetrics, p->hWnd, p->pSurfaceMaster);
        (*(p->pISurface->OnSurfacePrepare))(p->pISurface, pMetrics);

    } while (0);
}

static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(struct mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;
    __static_mmSurfaceContentMetricsAssignment(pMetrics, p->hWnd, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceDiscard))(p->pISurface, pMetrics);

    mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.DiscardSwapchain");
}

static
void
__static_mmUIViewSurfaceMaster_OnUpdateMask(
    struct mmUIViewSurfaceMaster*                  p,
    WPARAM                                         wParam,
    LPARAM                                         lParam,
    int                                            phase,
    mmUInt32_t                                     button_id)
{
    unsigned short flags = GET_LOWORD(wParam);
    p->hModifierMask = __static_mmOSModifierMask(flags);

    if (MM_MB_UNKNOWN != button_id)
    {
        switch (phase)
        {
        case mmTouchBegan:
            p->hButtonMask |=  (1 << button_id);
            break;
        case mmTouchEnded:
        case mmTouchBreak:
            p->hButtonMask &= ~(1 << button_id);
            break;
        default:
            break;
        }
    }
}

static 
void 
__static_mmUIViewSurfaceMaster_OnCursorDataHandler(
    struct mmUIViewSurfaceMaster*                  p, 
    WPARAM                                         wParam, 
    LPARAM                                         lParam, 
    int                                            phase, 
    mmUInt32_t                                     button_id)
{
    struct mmEventTouchs* pTouchs = NULL;
    struct mmEventCursor* pCursor = NULL;
    struct mmEventTouch* t = NULL;

    struct timeval tv;

    mmUInt64_t timestamp = 0;

    double abs_x = 0.0;
    double abs_y = 0.0;

    double rel_x = 0.0;
    double rel_y = 0.0;

    double wheel_dx = 0.0;
    double wheel_dy = 0.0;

    double hDisplayDensity = p->hDisplayDensity;

    short w_h_val = (short)GET_HIWORD(wParam);
    short w_l_val = (short)GET_LOWORD(wParam);

    double dt_y = ((double)w_h_val) / ((double)WHEEL_DELTA);

    int px_x = GET_X_LPARAM(lParam);
    int px_y = GET_Y_LPARAM(lParam);

    px_x = px_x < 0 ? 0 : (px_x > p->hClientW ? p->hClientW : px_x);
    px_y = px_y < 0 ? 0 : (px_y > p->hClientH ? p->hClientH : px_y);

    abs_x = (double)(px_x / hDisplayDensity);
    abs_y = (double)(px_y / hDisplayDensity);

    wheel_dx = 0.0f;
    wheel_dy = (double)(dt_y);

    rel_x = (double)((px_x - p->hOPosition[0]) / hDisplayDensity);
    rel_y = (double)((px_y - p->hOPosition[1]) / hDisplayDensity);

    p->hOPosition[0] = px_x;
    p->hOPosition[1] = px_y;

    __static_mmUIViewSurfaceMaster_OnUpdateMask(p, wParam, lParam, phase, button_id);

    mmGettimeofday(&tv, NULL);
    timestamp = (mmUInt64_t)(mmTimeval_ToUSec(&tv) / 1000);

    pTouchs = &p->hSurfaceContent.hTouchs;
    pCursor = &p->hSurfaceContent.hCursor;

    mmEventTouchs_AlignedMemory(pTouchs, 1);
    t = mmEventTouchs_GetForIndexReference(pTouchs, 0);

    t->motion_id = (uintptr_t)(1);
    t->tap_count = (int)1;
    t->phase = (int)phase;
    t->modifier_mask = (mmUInt32_t)p->hModifierMask;
    t->button_mask = (mmUInt32_t)p->hButtonMask;
    t->abs_x = (double)(abs_x);
    t->abs_y = (double)(abs_y);
    t->rel_x = (double)(rel_x);
    t->rel_y = (double)(rel_y);
    t->force_value = (double)0.25;
    t->force_maximum = (double)1.0;
    t->major_radius = (double)1.0;
    t->minor_radius = (double)1.0;
    t->size_value = (double)0.04;
    t->timestamp = (mmUInt64_t)timestamp;

    pCursor->modifier_mask = (mmUInt32_t)p->hModifierMask;
    pCursor->button_mask = (mmUInt32_t)p->hButtonMask;
    pCursor->button_id = (mmUInt32_t)button_id;
    pCursor->abs_x = (double)(abs_x);
    pCursor->abs_y = (double)(abs_y);
    pCursor->rel_x = (double)(rel_x);
    pCursor->rel_y = (double)(rel_y);
    pCursor->wheel_dx = (double)(wheel_dx);
    pCursor->wheel_dy = (double)(wheel_dy);
    pCursor->timestamp = (mmUInt64_t)timestamp;
}

static
void
__static_mmUIViewSurfaceMaster_OnTextDataHandler(
    struct mmUIViewSurfaceMaster*                  p,
    WPARAM                                         wParam,
    LPARAM                                         lParam,
    struct mmKeystroke*                            hKeystroke,
    int                                            phase)
{
    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

    mmUInt32_t keyCode = (mmUInt32_t)wParam;
    int key = mmOSVirtualKeycodeTranslate((mmUInt32_t)keyCode);
    mmUInt32_t modifierKey = (mmUInt32_t)mmKeyCodeToModifierKey(key);

    mmMemset(pKeypad->text, 0, sizeof(mmUInt16_t) * 4);

    switch (phase)
    {
    case 0:
        p->hModifierMask |=  (modifierKey);
        break;
    case 1:
        p->hModifierMask &= ~(modifierKey);
        break;
    default:
        break;
    }

    pKeypad->modifier_mask = p->hModifierMask;
    pKeypad->key = key;
    pKeypad->length = mmKeystrokeToCharacters(hKeystroke, pKeypad->text);
    pKeypad->handle = 0;
}

static void __static_mmUIViewSurfaceMaster_OnUpdateSizeCache(struct mmUIViewSurfaceMaster* p, int x, int y, int w, int h)
{
    RECT rc;

    mmUIViewSurfaceMaster_SetClientRect(p, x, y, w, h);

    SetRect(&rc, p->hClientX, p->hClientY, p->hClientW, p->hClientH);

    AdjustWindowRectEx(&rc, p->dwStyle, p->bMenu, p->dwExStyle);

    p->hWindowX = rc.left;
    p->hWindowY = rc.top;
    p->hWindowW = rc.right - rc.left;
    p->hWindowH = rc.bottom - rc.top;

    p->hWindowX = p->hWindowX < 0 ? 0 : p->hWindowX;
    p->hWindowY = p->hWindowY < 0 ? 0 : p->hWindowY;
}

static void __static_mmUIViewSurfaceMaster_OnSizeChange(struct mmUIViewSurfaceMaster* p, int cw, int ch)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;

    double hDisplayDensity = p->hDisplayDensity;

    double hDisplayFrameSize[2];
    double hPhysicalSafeRect[4];

    const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);

    __static_mmUIViewSurfaceMaster_OnUpdateSizeCache(p, p->hClientX, p->hClientY, cw, ch);

    // Display frame size.
    hDisplayFrameSize[0] = (double)cw;
    hDisplayFrameSize[1] = (double)ch;

    // Safe rect.
    hPhysicalSafeRect[0] = (double)0.0;
    hPhysicalSafeRect[1] = (double)0.0;
    hPhysicalSafeRect[2] = (double)(hDisplayFrameSize[0] / hDisplayDensity);
    hPhysicalSafeRect[3] = (double)(hDisplayFrameSize[1] / hDisplayDensity);

    mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
    mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
    mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);

    // Logger infomation.
    mmLogger_LogT(gLogger, "Window mmUIViewSurfaceMaster.NativeOnSurfaceChanged");
    mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_TRACE, pWindowName);

    // Fire event for size change.
    __static_mmSurfaceContentMetricsAssignment(pMetrics, p->hWnd, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceChanged))(p->pISurface, pMetrics);
}

static void __static_mmUIViewSurfaceMaster_ImmKeypadStatusShow(struct mmUIViewSurfaceMaster* p)
{
    HIMC hIMC = ImmGetContext(p->hWnd);
    if (NULL != hIMC)
    {
        ImmSetOpenStatus(hIMC, TRUE);
        ImmReleaseContext(p->hWnd, hIMC);
    }
}
static void __static_mmUIViewSurfaceMaster_ImmKeypadStatusHide(struct mmUIViewSurfaceMaster* p)
{
    HIMC hIMC = ImmGetContext(p->hWnd);
    if (NULL != hIMC)
    {
        ImmSetOpenStatus(hIMC, FALSE);
        ImmReleaseContext(p->hWnd, hIMC);
    }
}
static void __static_mmUIViewSurfaceMaster_ImmKeypadStatusUpdate(struct mmUIViewSurfaceMaster* p)
{
    HIMC hIMC = ImmGetContext(p->hWnd);
    if (NULL != hIMC)
    {
        COMPOSITIONFORM Composition;

        (*(p->pISurface->OnGetTextEditRect))(p->pISurface, p->hTextEditRect);

        double hDisplayDensity = p->hDisplayDensity;

        double hImmX = (p->hTextEditRect[0]                      ) * hDisplayDensity;
        double hImmY = (p->hTextEditRect[1] + p->hTextEditRect[3]) * hDisplayDensity;

        Composition.dwStyle = CFS_FORCE_POSITION;
        Composition.ptCurrentPos.x = (LONG)hImmX;
        Composition.ptCurrentPos.y = (LONG)hImmY;
        ImmSetCompositionWindow(hIMC, &Composition);
        ImmReleaseContext(p->hWnd, hIMC);
    }
}

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pISurface, int hType)
{
    // not implement.
    // struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    // struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pISurface, int hAppearance)
{
    // not implement.
    // struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    // struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pISurface, int hReturnKey)
{
    // not implement.
    // struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    // struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pISurface)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    __static_mmUIViewSurfaceMaster_ImmKeypadStatusShow(p);
    __static_mmUIViewSurfaceMaster_ImmKeypadStatusUpdate(p);
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pISurface)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pISurface;
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);

    __static_mmUIViewSurfaceMaster_ImmKeypadStatusHide(p);
}

static
void
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    pMetrics->surface = surface;
    pMetrics->canvas_size[0] = pSurfaceMaster->hDisplayFrameSize[0];
    pMetrics->canvas_size[1] = pSurfaceMaster->hDisplayFrameSize[1];
    pMetrics->window_size[0] = pSurfaceMaster->hPhysicalViewSize[0];
    pMetrics->window_size[1] = pSurfaceMaster->hPhysicalViewSize[1];
    pMetrics->safety_area[0] = pSurfaceMaster->hPhysicalSafeRect[0];
    pMetrics->safety_area[1] = pSurfaceMaster->hPhysicalSafeRect[1];
    pMetrics->safety_area[2] = pSurfaceMaster->hPhysicalSafeRect[2];
    pMetrics->safety_area[3] = pSurfaceMaster->hPhysicalSafeRect[3];
}

static mmUInt32_t __static_mmOSModifierMask(unsigned short flags)
{
    // MK_LBUTTON  0x0001
    // MK_RBUTTON  0x0002
    // MK_SHIFT    0x0004
    // MK_CONTROL  0x0008
    // MK_MBUTTON  0x0010
    // MK_XBUTTON1 0x0020
    // MK_XBUTTON2 0x0040

    mmUInt32_t mask = 0;

    int option  = (GetKeyState(VK_MENU)) & 0x8000;
    int command = (GetKeyState(VK_LWIN) | GetKeyState(VK_RWIN)) & 0x8000;
    int capital = (GetKeyState(VK_CAPITAL)) & 1;
    int numlock = (GetKeyState(VK_NUMLOCK)) & 1;

    if (flags &   MK_SHIFT) mask |= MM_MODIFIER_SHIFT;
    if (flags & MK_CONTROL) mask |= MM_MODIFIER_CONTROL;
    if (option ) mask |= MM_MODIFIER_OPTION;
    if (command) mask |= MM_MODIFIER_COMMAND;
    if (capital) mask |= MM_MODIFIER_CAPITAL;
    if (numlock) mask |= MM_MODIFIER_NUMLOCK;

    return mask;
}

static void __static_mmUIViewSurfaceMaster_OnKeypadInsertTextUtf16(struct mmUIViewSurfaceMaster* p)
{
    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

    if (0 != pKeypad->length && pKeypad->key != MM_KC_BACKSPACE)
    {
        struct mmEventEditString* pEditString = &p->hSurfaceContent.hEditString;
        pEditString->s = pKeypad->text;
        pEditString->l = pKeypad->length;
        pEditString->o = 0;
        pEditString->n = 0;
        (*(p->pISurface->OnInsertTextUtf16))(p->pISurface, pEditString);
    }
}

static void __static_mmUIViewSurfaceMaster_OnShortcutKey(struct mmUIViewSurfaceMaster* p)
{
    struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

    // If it is a shortcut key, then the text insert event will not triggered.
    // The text content is invalid, we just assign the text length = 0.

    if (p->hModifierMask & MM_MODIFIER_CONTROL)
    {
        switch (pKeypad->key)
        {
        case MM_KC_C:
            (*(p->pISurface->OnInjectCopy))(p->pISurface);
            pKeypad->length = 0;
            break;
        case MM_KC_X:
            (*(p->pISurface->OnInjectCut))(p->pISurface);
            pKeypad->length = 0;
            break;
        case MM_KC_V:
            (*(p->pISurface->OnInjectPaste))(p->pISurface);
            pKeypad->length = 0;
            break;
        default:
            break;
        }
    }
}

LRESULT CALLBACK __static_mmUIViewSurfaceMaster_WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    struct mmUIViewSurfaceMaster* p = (struct mmUIViewSurfaceMaster*)GetWindowLongPtr(hWnd, GWLP_USERDATA);

    if (NULL == p)
    {
        // if user data pointer is NULL, use default proc.
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    switch (message)
    {
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc;

        // When drop window, the loop of PeekMessage will not break.
        // Here we trigger an update manual for refresh draw content.
        (*(p->pISurface->OnUpdateImmediately))(p->pISurface);

        hdc = BeginPaint(hWnd, &ps);
        // TODO: Add any drawing code using hdc here...
        EndPaint(hWnd, &ps);
    }
    break;

    case WM_DESTROY:
    {
        PostQuitMessage(0);
    }
    break;

    case WM_DROPFILES:
    {
        HDROP hDrop = (HDROP)wParam;
        char hUrl[MAX_PATH] = { 0 };
        POINT hPpt;
        UINT fCount = 0;
        UINT index = 0;

        struct mmEventDragging* pContentDrag = &p->hSurfaceContent.hDragging;

        double hDisplayDensity = p->hDisplayDensity;

        fCount = DragQueryFileA(hDrop, 0xFFFFFFFF, NULL, 0);
        DragQueryPoint(hDrop, &hPpt);

        double x = (double)(hPpt.x / hDisplayDensity);
        double y = (double)(hPpt.y / hDisplayDensity);

        mmEventDragging_Reset(pContentDrag);
        mmEventDragging_AlignedMemory(pContentDrag, fCount);
        mmEventDragging_SetPoint(pContentDrag, (double)x, (double)y);

        for (index = 0; index < fCount; ++index)
        {
            DragQueryFileA(hDrop, index, hUrl, MAX_PATH);
            mmEventDragging_SetForIndex(pContentDrag, index, hUrl);
        }

        (*(p->pISurface->OnDragging))(p->pISurface, pContentDrag);

        mmEventDragging_Reset(pContentDrag);

        DragFinish(hDrop);
    }
    break;

    case WM_SIZE:
    {
        // https://docs.microsoft.com/en-us/windows/win32/winmsg/wm-size

        // Size change.
        if (FALSE == IsIconic(hWnd))
        {
            struct mmLogger* gLogger = mmLogger_Instance();

            int iw = LOWORD(lParam); // width of client area 
            int ih = HIWORD(lParam); // Hight of client area 
            __static_mmUIViewSurfaceMaster_OnSizeChange(p, iw, ih);
 
            mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnWindowDidResize %ux%u.", iw, ih);
        }
    }
    break;

    case WM_ACTIVATE:
    {
        if (LOWORD(wParam) != WA_INACTIVE)
        {
            mmUIViewSurfaceMaster_OnEnterForeground(p);
        }
        else
        {
            mmUIViewSurfaceMaster_OnEnterBackground(p);
        }
    }
    break;

    case WM_MOVE:
    {
        // do nothing now.
        // mmPrintf("WM_MOVE\n");
    }
    break;

    case WM_DISPLAYCHANGE:
    {
        // do nothing now.
        // mmPrintf("WM_DISPLAYCHANGE\n");
    }
    break;

    case WM_GETMINMAXINFO:
    {
        // Prevent the window from going smaller than some minimu size
        ((MINMAXINFO*)lParam)->ptMinTrackSize.x = 160;
        ((MINMAXINFO*)lParam)->ptMinTrackSize.y = 100;
    }
    break;

    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
    {
        // https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-keydown
        //
        // 0-15  The repeat count for the current message. The value is the number of times the Keystroke 
        //       is autorepeated as a result of the user holding down the key. If the Keystroke is held long 
        //       enough, multiple messages are sent. However, the repeat count is not cumulative.
        // 
        // 16-23 The scan code. The value depends on the OEM.
        // 
        // 24    Indicates whether the key is an extended key, such as the right-hand ALT and CTRL keys that 
        //       appear on an enhanced 101- or 102-key keyboard. The value is 1 if it is an extended key; 
        //       otherwise, it is 0.
        // 
        // 25-28 Reserved; do not use.
        // 
        // 29    The context code. The value is always 0 for a WM_KEYDOWN message.
        // 
        // 30    The previous key state. The value is 1 if the key is down before the message is sent, or it 
        //       is zero if the key is up.
        // 
        // 31    The transition state. The value is always 0 for a WM_KEYDOWN message.        

        struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

        struct mmKeystroke hKeystroke;

        mmKeystrokeMake(&hKeystroke, wParam, lParam);

        __static_mmUIViewSurfaceMaster_OnTextDataHandler(p, wParam, lParam, &hKeystroke, 0);

        (*(p->pISurface->OnKeypadPressed))(p->pISurface, pKeypad);

        __static_mmUIViewSurfaceMaster_OnShortcutKey(p);

        __static_mmUIViewSurfaceMaster_OnKeypadInsertTextUtf16(p);
    }
    break;

    case WM_KEYUP:
    case WM_SYSKEYUP:
    {
        // https://docs.microsoft.com/en-us/windows/win32/inputdev/wm-keyup
        //
        // 0-15  The repeat count for the current message. The value is the number of times the Keystroke is 
        //       autorepeated as a result of the user holding down the key. The repeat count is always 1 for 
        //       a WM_KEYUP message.
        // 
        // 16-23 The scan code. The value depends on the OEM.
        // 
        // 24    Indicates whether the key is an extended key, such as the right-hand ALT and CTRL keys that 
        //       appear on an enhanced 101- or 102-key keyboard. The value is 1 if it is an extended key; 
        //       otherwise, it is 0.
        // 
        // 25-28 Reserved; do not use.
        // 
        // 29    The context code. The value is always 0 for a WM_KEYUP message.
        // 
        // 30    The previous key state. The value is always 1 for a WM_KEYUP message.
        // 
        // 31    The transition state. The value is always 1 for a WM_KEYUP message.

        struct mmEventKeypad* pKeypad = &p->hSurfaceContent.hKeypad;

        struct mmKeystroke hKeystroke;

        mmKeystrokeMake(&hKeystroke, wParam, lParam);

        __static_mmUIViewSurfaceMaster_OnTextDataHandler(p, wParam, lParam, &hKeystroke, 1);

        (*(p->pISurface->OnKeypadRelease))(p->pISurface, pKeypad);
    }
    break;

    case WM_IME_CHAR:
    {
        // https://docs.microsoft.com/en-us/windows/win32/intl/wm-ime-char
        // 
        // 0-15  Repeat count. Since the first byte and second byte are continuous, this is always 1.
        //
        // 16-23 Scan code for a complete Asian character.
        //
        // 24    Extended key.
        //
        // 25-28 Not used.
        //
        // 29    Context code.
        //
        // 30    Previous key state.
        //
        // 31    Transition state.

        struct mmEventEditString* pEditString = &p->hSurfaceContent.hEditString;

        wchar_t wbstr[2] = { 0 };
        UINT hCodePage = mmUnicodeWindows_CodePage();
        int bytes = mmUnicodeWindows_ParamToWideChar(wParam, hCodePage, wbstr);
        int n = bytes / sizeof(uint16_t);
        // Fire text event.
        pEditString->s = (mmUInt16_t*)wbstr;
        pEditString->l = n;
        pEditString->o = 0;
        pEditString->n = 0;
        (*(p->pISurface->OnInsertTextUtf16))(p->pISurface, pEditString);
    }
    break;

    case WM_IME_COMPOSITION:
    {
        // mmPrintf("WM_IME_COMPOSITION\n");
    }
    break;

    case WM_IME_STARTCOMPOSITION:
    {
        __static_mmUIViewSurfaceMaster_ImmKeypadStatusUpdate(p);
    }
    break;

    case WM_IME_ENDCOMPOSITION:
    {
        // mmPrintf("WM_IME_ENDCOMPOSITION\n");
    }
    break;

    case WM_MOUSEHOVER:
    {
        // mmPrintf("WM_MOUSEHOVER\n");
    }
    break;

    case WM_MOUSELEAVE:
    {
        // mmPrintf("WM_MOUSELEAVE\n");
    }
    break;

    case WM_MOUSEMOVE:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-mousemove
        //
        // wParam
        //     Indicates whether various virtual keys are down. 
        //         This parameter can be one or more of the following values.
        //
        //         0x0001 MK_LBUTTON;
        //         0x0002 MK_RBUTTON;
        //         0x0004 MK_SHIFT;
        //         0x0008 MK_CONTROL;
        //         0x0010 MK_MBUTTON;
        //         0x0020 MK_XBUTTON1;
        //         0x0040 MK_XBUTTON2;
        //
        // lParam
        //     The low  - order word specifies the x - coordinate of the cursor.
        //         The coordinate is relative to the upper - left corner of the client area.
        //     The high - order word specifies the y - coordinate of the cursor.
        //         The coordinate is relative to the upper - left corner of the client area.
        //

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchMoved, MM_MB_UNKNOWN);

        (*(p->pISurface->OnTouchsMoved))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorMoved))(p->pISurface, pCursor);
    }
    break;

    case WM_MOUSEWHEEL:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-mousewheel
        //
        // wParam
        //
        //    The high - order word indicates the distance the wheel is rotated, 
        //        expressed in multiples or divisions of WHEEL_DELTA, which is 120. 
        //        A positive value indicates that the wheel was rotated forward, away from the user; 
        //        a negative value indicates that the wheel was rotated backward, toward the user.
        //
        //    The low - order word indicates whether various virtual keys are down.
        //        This parameter can be one or more of the following values.
        //
        //         0x0001 MK_LBUTTON;
        //         0x0002 MK_RBUTTON;
        //         0x0004 MK_SHIFT;
        //         0x0008 MK_CONTROL;
        //         0x0010 MK_MBUTTON;
        //         0x0020 MK_XBUTTON1;
        //         0x0040 MK_XBUTTON2;

        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchMoved, MM_MB_UNKNOWN);

        (*(p->pISurface->OnCursorWheel))(p->pISurface, pCursor);
    }
    break;

    case WM_LBUTTONDOWN:
    case WM_LBUTTONDBLCLK:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-lbuttondown
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-lbuttondblclk

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchBegan, MM_MB_LBUTTON);

        (*(p->pISurface->OnTouchsBegan))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorBegan))(p->pISurface, pCursor);
    }
    break;

    case WM_MBUTTONDOWN:
    case WM_MBUTTONDBLCLK:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-mbuttondown
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-mbuttondblclk

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchBegan, MM_MB_MBUTTON);

        (*(p->pISurface->OnTouchsBegan))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorBegan))(p->pISurface, pCursor);
    }
    break;

    case WM_RBUTTONDOWN:
    case WM_RBUTTONDBLCLK:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-rbuttondown
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-rbuttondblclk

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchBegan, MM_MB_RBUTTON);

        (*(p->pISurface->OnTouchsBegan))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorBegan))(p->pISurface, pCursor);
    }
    break;

    case WM_XBUTTONDOWN:
    case WM_XBUTTONDBLCLK:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-xbuttondown
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-xbuttondblclk
        //
        // wParam
        //     0x0001 MK_LBUTTON;
        //     0x0002 MK_RBUTTON;
        //     0x0004 MK_SHIFT;
        //     0x0008 MK_CONTROL;
        //     0x0010 MK_MBUTTON;
        //     0x0020 MK_XBUTTON1;
        //     0x0040 MK_XBUTTON2;
        //
        // lParam
        //     The low  - order word specifies the x - coordinate of the cursor.
        //         The coordinate is relative to the upper - left corner of the client area.
        //     The high - order word specifies the y - coordinate of the cursor.
        //         The coordinate is relative to the upper - left corner of the client area.

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        // XBUTTON1 0x0001
        // XBUTTON2 0x0002
        unsigned short buttons = GET_HIWORD(wParam);
        int bn = (buttons && XBUTTON1) ? 1 : 2;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchBegan, MM_MB_MBUTTON + bn);

        (*(p->pISurface->OnTouchsBegan))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorBegan))(p->pISurface, pCursor);
    }
    break;

    case WM_LBUTTONUP:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-lbuttonup

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchEnded, MM_MB_LBUTTON);

        (*(p->pISurface->OnTouchsEnded))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorEnded))(p->pISurface, pCursor);
    }
    break;

    case WM_MBUTTONUP:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-mbuttonup

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchEnded, MM_MB_MBUTTON);

        (*(p->pISurface->OnTouchsEnded))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorEnded))(p->pISurface, pCursor);
    }
    break;

    case WM_RBUTTONUP:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-rbuttonup

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchEnded, MM_MB_RBUTTON);

        (*(p->pISurface->OnTouchsEnded))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorEnded))(p->pISurface, pCursor);
    }
    break;

    case WM_XBUTTONUP:
    {
        // https://docs.microsoft.com/zh-cn/windows/win32/inputdev/wm-xbuttonup
        //
        // wParam
        //     0x0001 MK_LBUTTON;
        //     0x0002 MK_RBUTTON;
        //     0x0004 MK_SHIFT;
        //     0x0008 MK_CONTROL;
        //     0x0010 MK_MBUTTON;
        //     0x0020 MK_XBUTTON1;
        //     0x0040 MK_XBUTTON2;
        //
        // lParam
        //     The low  - order word specifies the x - coordinate of the cursor.
        //         The coordinate is relative to the upper - left corner of the client area.
        //     The high - order word specifies the y - coordinate of the cursor.
        //         The coordinate is relative to the upper - left corner of the client area.

        struct mmEventTouchs* pTouchs = &p->hSurfaceContent.hTouchs;
        struct mmEventCursor* pCursor = &p->hSurfaceContent.hCursor;

        // XBUTTON1 0x0001
        // XBUTTON2 0x0002
        unsigned short buttons = GET_HIWORD(wParam);
        int bn = (buttons && XBUTTON1) ? 1 : 2;

        __static_mmUIViewSurfaceMaster_OnCursorDataHandler(p, wParam, lParam, mmTouchEnded, MM_MB_MBUTTON + bn);

        (*(p->pISurface->OnTouchsEnded))(p->pISurface, pTouchs);
        (*(p->pISurface->OnCursorEnded))(p->pISurface, pCursor);
    }
    break;

    default:
    {
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    break;
    }
    return 0;
}