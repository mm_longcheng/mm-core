/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOSVirtualKeycode.h"

#include "core/mmKeyCode.h"

static const mmUInt8_t kVKeycodeMap[256] =
{
    [VK_LBUTTON            ] = MM_KC_UNKNOWN   , // 0x01
    [VK_RBUTTON            ] = MM_KC_UNKNOWN   , // 0x02
    [VK_CANCEL             ] = MM_KC_UNKNOWN   , // 0x03
    [VK_MBUTTON            ] = MM_KC_UNKNOWN   , // 0x04    /* NOT contiguous with L & RBUTTON */

    [VK_XBUTTON1           ] = MM_KC_UNKNOWN   , // 0x05    /* NOT contiguous with L & RBUTTON */
    [VK_XBUTTON2           ] = MM_KC_UNKNOWN   , // 0x06    /* NOT contiguous with L & RBUTTON */

    [VK_BACK               ] = MM_KC_BACK      , // 0x08
    [VK_TAB                ] = MM_KC_TAB       , // 0x09

    [VK_CLEAR              ] = MM_KC_UNKNOWN   , // 0x0C
    [VK_RETURN             ] = MM_KC_RETURN    , // 0x0D

    [VK_SHIFT              ] = MM_KC_LSHIFT    , // 0x10
    [VK_CONTROL            ] = MM_KC_LCONTROL  , // 0x11
    [VK_MENU               ] = MM_KC_LMENU     , // 0x12
    [VK_PAUSE              ] = MM_KC_PAUSE     , // 0x13
    [VK_CAPITAL            ] = MM_KC_CAPITAL   , // 0x14

    [VK_KANA               ] = MM_KC_KANA      , // 0x15

    [VK_JUNJA              ] = MM_KC_UNKNOWN   , // 0x17
    [VK_FINAL              ] = MM_KC_UNKNOWN   , // 0x18
    [VK_HANJA              ] = MM_KC_UNKNOWN   , // 0x19

    [VK_ESCAPE             ] = MM_KC_ESCAPE    , // 0x1B

    [VK_CONVERT            ] = MM_KC_UNKNOWN   , // 0x1C
    [VK_NONCONVERT         ] = MM_KC_UNKNOWN   , // 0x1D
    [VK_ACCEPT             ] = MM_KC_UNKNOWN   , // 0x1E
    [VK_MODECHANGE         ] = MM_KC_UNKNOWN   , // 0x1F

    [VK_SPACE              ] = MM_KC_SPACE     , // 0x20
    [VK_PRIOR              ] = MM_KC_PGUP      , // 0x21
    [VK_NEXT               ] = MM_KC_PGDN      , // 0x22
    [VK_END                ] = MM_KC_END       , // 0x23
    [VK_HOME               ] = MM_KC_HOME      , // 0x24
    [VK_LEFT               ] = MM_KC_LEFT      , // 0x25
    [VK_UP                 ] = MM_KC_UP        , // 0x26
    [VK_RIGHT              ] = MM_KC_RIGHT     , // 0x27
    [VK_DOWN               ] = MM_KC_DOWN      , // 0x28
    [VK_SELECT             ] = MM_KC_UNKNOWN   , // 0x29
    [VK_PRINT              ] = MM_KC_UNKNOWN   , // 0x2A
    [VK_EXECUTE            ] = MM_KC_UNKNOWN   , // 0x2B
    [VK_SNAPSHOT           ] = MM_KC_SYSRQ     , // 0x2C
    [VK_INSERT             ] = MM_KC_INSERT    , // 0x2D
    [VK_DELETE             ] = MM_KC_DELETE    , // 0x2E
    [VK_HELP               ] = MM_KC_UNKNOWN   , // 0x2F

    ['0'                   ] = MM_KC_0         , // 0x30
    ['1'                   ] = MM_KC_1         , // 0x31
    ['2'                   ] = MM_KC_2         , // 0x32
    ['3'                   ] = MM_KC_3         , // 0x33
    ['4'                   ] = MM_KC_4         , // 0x34
    ['5'                   ] = MM_KC_5         , // 0x35
    ['6'                   ] = MM_KC_6         , // 0x36
    ['7'                   ] = MM_KC_7         , // 0x37
    ['8'                   ] = MM_KC_8         , // 0x38
    ['9'                   ] = MM_KC_9         , // 0x39

    ['A'                   ] = MM_KC_A         , // 0x41
    ['B'                   ] = MM_KC_B         , // 0x42
    ['C'                   ] = MM_KC_C         , // 0x43
    ['D'                   ] = MM_KC_D         , // 0x44
    ['E'                   ] = MM_KC_E         , // 0x45
    ['F'                   ] = MM_KC_F         , // 0x46
    ['G'                   ] = MM_KC_G         , // 0x47
    ['H'                   ] = MM_KC_H         , // 0x48
    ['I'                   ] = MM_KC_I         , // 0x49
    ['J'                   ] = MM_KC_J         , // 0x4A
    ['K'                   ] = MM_KC_K         , // 0x4B
    ['L'                   ] = MM_KC_L         , // 0x4C
    ['M'                   ] = MM_KC_M         , // 0x4D
    ['N'                   ] = MM_KC_N         , // 0x4E
    ['O'                   ] = MM_KC_O         , // 0x4F
    ['P'                   ] = MM_KC_P         , // 0x50
    ['Q'                   ] = MM_KC_Q         , // 0x51
    ['R'                   ] = MM_KC_R         , // 0x52
    ['S'                   ] = MM_KC_S         , // 0x53
    ['T'                   ] = MM_KC_T         , // 0x54
    ['U'                   ] = MM_KC_U         , // 0x55
    ['V'                   ] = MM_KC_V         , // 0x56
    ['W'                   ] = MM_KC_W         , // 0x57
    ['X'                   ] = MM_KC_X         , // 0x58
    ['Y'                   ] = MM_KC_Y         , // 0x59
    ['Z'                   ] = MM_KC_Z         , // 0x5A

    [VK_LWIN               ] = MM_KC_LWIN      , // 0x5B
    [VK_RWIN               ] = MM_KC_RWIN      , // 0x5C
    [VK_APPS               ] = MM_KC_APPS      , // 0x5D

    [VK_SLEEP              ] = MM_KC_SLEEP     , // 0x5F

    [VK_NUMPAD0            ] = MM_KC_NUMPAD0   , // 0x60
    [VK_NUMPAD1            ] = MM_KC_NUMPAD1   , // 0x61
    [VK_NUMPAD2            ] = MM_KC_NUMPAD2   , // 0x62
    [VK_NUMPAD3            ] = MM_KC_NUMPAD3   , // 0x63
    [VK_NUMPAD4            ] = MM_KC_NUMPAD4   , // 0x64
    [VK_NUMPAD5            ] = MM_KC_NUMPAD5   , // 0x65
    [VK_NUMPAD6            ] = MM_KC_NUMPAD6   , // 0x66
    [VK_NUMPAD7            ] = MM_KC_NUMPAD7   , // 0x67
    [VK_NUMPAD8            ] = MM_KC_NUMPAD8   , // 0x68
    [VK_NUMPAD9            ] = MM_KC_NUMPAD9   , // 0x69

    [VK_MULTIPLY           ] = MM_KC_MULTIPLY  , // 0x6A
    [VK_ADD                ] = MM_KC_ADD       , // 0x6B
    [VK_SEPARATOR          ] = MM_KC_UNKNOWN   , // 0x6C
    [VK_SUBTRACT           ] = MM_KC_SUBTRACT  , // 0x6D
    [VK_DECIMAL            ] = MM_KC_DECIMAL   , // 0x6E
    [VK_DIVIDE             ] = MM_KC_DIVIDE    , // 0x6F

    [VK_F1                 ] = MM_KC_F1        , // 0x70
    [VK_F2                 ] = MM_KC_F2        , // 0x71
    [VK_F3                 ] = MM_KC_F3        , // 0x72
    [VK_F4                 ] = MM_KC_F4        , // 0x73
    [VK_F5                 ] = MM_KC_F5        , // 0x74
    [VK_F6                 ] = MM_KC_F6        , // 0x75
    [VK_F7                 ] = MM_KC_F7        , // 0x76
    [VK_F8                 ] = MM_KC_F8        , // 0x77
    [VK_F9                 ] = MM_KC_F9        , // 0x78
    [VK_F10                ] = MM_KC_F10       , // 0x79
    [VK_F11                ] = MM_KC_F11       , // 0x7A
    [VK_F12                ] = MM_KC_F12       , // 0x7B
    [VK_F13                ] = MM_KC_F13       , // 0x7C
    [VK_F14                ] = MM_KC_F14       , // 0x7D
    [VK_F15                ] = MM_KC_F15       , // 0x7E
    [VK_F16                ] = MM_KC_UNKNOWN   , // 0x7F
    [VK_F17                ] = MM_KC_UNKNOWN   , // 0x80
    [VK_F18                ] = MM_KC_UNKNOWN   , // 0x81
    [VK_F19                ] = MM_KC_UNKNOWN   , // 0x82
    [VK_F20                ] = MM_KC_UNKNOWN   , // 0x83
    [VK_F21                ] = MM_KC_UNKNOWN   , // 0x84
    [VK_F22                ] = MM_KC_UNKNOWN   , // 0x85
    [VK_F23                ] = MM_KC_UNKNOWN   , // 0x86
    [VK_F24                ] = MM_KC_UNKNOWN   , // 0x87

    [VK_NAVIGATION_VIEW    ] = MM_KC_UNKNOWN   , // 0x88    reserved
    [VK_NAVIGATION_MENU    ] = MM_KC_UNKNOWN   , // 0x89    reserved
    [VK_NAVIGATION_UP      ] = MM_KC_UNKNOWN   , // 0x8A    reserved
    [VK_NAVIGATION_DOWN    ] = MM_KC_UNKNOWN   , // 0x8B    reserved
    [VK_NAVIGATION_LEFT    ] = MM_KC_UNKNOWN   , // 0x8C    reserved
    [VK_NAVIGATION_RIGHT   ] = MM_KC_UNKNOWN   , // 0x8D    reserved
    [VK_NAVIGATION_ACCEPT  ] = MM_KC_UNKNOWN   , // 0x8E    reserved
    [VK_NAVIGATION_CANCEL  ] = MM_KC_UNKNOWN   , // 0x8F    reserved

    [VK_NUMLOCK            ] = MM_KC_NUMLOCK   , // 0x90
    [VK_SCROLL             ] = MM_KC_SCROLL    , // 0x91

    [VK_OEM_FJ_JISHO       ] = MM_KC_UNKNOWN   , // 0x92    'Dictionary' key
    [VK_OEM_FJ_MASSHOU     ] = MM_KC_UNKNOWN   , // 0x93    'Unregister word' key
    [VK_OEM_FJ_TOUROKU     ] = MM_KC_UNKNOWN   , // 0x94    'Register word' key
    [VK_OEM_FJ_LOYA        ] = MM_KC_UNKNOWN   , // 0x95    'Left OYAYUBI' key
    [VK_OEM_FJ_ROYA        ] = MM_KC_UNKNOWN   , // 0x96    'Right OYAYUBI' key

    [VK_LSHIFT             ] = MM_KC_LSHIFT    , // 0xA0
    [VK_RSHIFT             ] = MM_KC_RSHIFT    , // 0xA1
    [VK_LCONTROL           ] = MM_KC_LCONTROL  , // 0xA2
    [VK_RCONTROL           ] = MM_KC_RCONTROL  , // 0xA3
    [VK_LMENU              ] = MM_KC_LMENU     , // 0xA4
    [VK_RMENU              ] = MM_KC_RMENU     , // 0xA5

    [VK_BROWSER_BACK       ] = MM_KC_UNKNOWN   , // 0xA6
    [VK_BROWSER_FORWARD    ] = MM_KC_UNKNOWN   , // 0xA7
    [VK_BROWSER_REFRESH    ] = MM_KC_UNKNOWN   , // 0xA8
    [VK_BROWSER_STOP       ] = MM_KC_UNKNOWN   , // 0xA9
    [VK_BROWSER_SEARCH     ] = MM_KC_UNKNOWN   , // 0xAA
    [VK_BROWSER_FAVORITES  ] = MM_KC_UNKNOWN   , // 0xAB
    [VK_BROWSER_HOME       ] = MM_KC_UNKNOWN   , // 0xAC

    [VK_VOLUME_MUTE        ] = MM_KC_UNKNOWN   , // 0xAD
    [VK_VOLUME_DOWN        ] = MM_KC_UNKNOWN   , // 0xAE
    [VK_VOLUME_UP          ] = MM_KC_UNKNOWN   , // 0xAF
    [VK_MEDIA_NEXT_TRACK   ] = MM_KC_UNKNOWN   , // 0xB0
    [VK_MEDIA_PREV_TRACK   ] = MM_KC_UNKNOWN   , // 0xB1
    [VK_MEDIA_STOP         ] = MM_KC_UNKNOWN   , // 0xB2
    [VK_MEDIA_PLAY_PAUSE   ] = MM_KC_UNKNOWN   , // 0xB3
    [VK_LAUNCH_MAIL        ] = MM_KC_UNKNOWN   , // 0xB4
    [VK_LAUNCH_MEDIA_SELECT] = MM_KC_UNKNOWN   , // 0xB5
    [VK_LAUNCH_APP1        ] = MM_KC_UNKNOWN   , // 0xB6
    [VK_LAUNCH_APP2        ] = MM_KC_UNKNOWN   , // 0xB7

    [VK_OEM_1              ] = MM_KC_SEMICOLON , // 0xBA    ';:' for US     (;:)
    [VK_OEM_PLUS           ] = MM_KC_EQUALS    , // 0xBB    '+' any country (=+)
    [VK_OEM_COMMA          ] = MM_KC_COMMA     , // 0xBC    ',' any country (,<)
    [VK_OEM_MINUS          ] = MM_KC_MINUS     , // 0xBD    '-' any country (-_)
    [VK_OEM_PERIOD         ] = MM_KC_PERIOD    , // 0xBE    '.' any country (.>)
    [VK_OEM_2              ] = MM_KC_SLASH     , // 0xBF    '/?' for US     (/?)
    [VK_OEM_3              ] = MM_KC_GRAVE     , // 0xC0    '`~' for US     (`~)

    /*
     * 0xC3 - 0xDA : Gamepad input
     */

    [VK_OEM_4              ] = MM_KC_LBRACKET  , // 0xDB    '[{' for US     ([{)
    [VK_OEM_5              ] = MM_KC_BACKSLASH , // 0xDC    '\|' for US     (\|)
    [VK_OEM_6              ] = MM_KC_RBRACKET  , // 0xDD    ']}' for US     (]})
    [VK_OEM_7              ] = MM_KC_APOSTROPHE, // 0xDE    ''"' for US     ('")
    [VK_OEM_8              ] = MM_KC_UNKNOWN   , // 0xDF

    [VK_OEM_AX             ] = MM_KC_UNKNOWN   , // 0xE1    'AX' key on Japanese AX kbd
    [VK_OEM_102            ] = MM_KC_UNKNOWN   , // 0xE2    "<>" or "\|" on RT 102-key kbd.
    [VK_ICO_HELP           ] = MM_KC_UNKNOWN   , // 0xE3    Help key on ICO
    [VK_ICO_00             ] = MM_KC_UNKNOWN   , // 0xE4    00 key on ICO

    [VK_PROCESSKEY         ] = MM_KC_UNKNOWN   , // 0xE5

    [VK_ICO_CLEAR          ] = MM_KC_UNKNOWN   , // 0xE6

    [VK_PACKET             ] = MM_KC_UNKNOWN   , // 0xE7

    [VK_OEM_RESET          ] = MM_KC_UNKNOWN   , // 0xE9
    [VK_OEM_JUMP           ] = MM_KC_UNKNOWN   , // 0xEA
    [VK_OEM_PA1            ] = MM_KC_UNKNOWN   , // 0xEB
    [VK_OEM_PA2            ] = MM_KC_UNKNOWN   , // 0xEC
    [VK_OEM_PA3            ] = MM_KC_UNKNOWN   , // 0xED
    [VK_OEM_WSCTRL         ] = MM_KC_UNKNOWN   , // 0xEE
    [VK_OEM_CUSEL          ] = MM_KC_UNKNOWN   , // 0xEF
    [VK_OEM_ATTN           ] = MM_KC_UNKNOWN   , // 0xF0
    [VK_OEM_FINISH         ] = MM_KC_UNKNOWN   , // 0xF1
    [VK_OEM_COPY           ] = MM_KC_UNKNOWN   , // 0xF2
    [VK_OEM_AUTO           ] = MM_KC_UNKNOWN   , // 0xF3
    [VK_OEM_ENLW           ] = MM_KC_UNKNOWN   , // 0xF4
    [VK_OEM_BACKTAB        ] = MM_KC_UNKNOWN   , // 0xF5

    [VK_ATTN               ] = MM_KC_UNKNOWN   , // 0xF6
    [VK_CRSEL              ] = MM_KC_UNKNOWN   , // 0xF7
    [VK_EXSEL              ] = MM_KC_UNKNOWN   , // 0xF8
    [VK_EREOF              ] = MM_KC_UNKNOWN   , // 0xF9
    [VK_PLAY               ] = MM_KC_UNKNOWN   , // 0xFA
    [VK_ZOOM               ] = MM_KC_UNKNOWN   , // 0xFB
    [VK_NONAME             ] = MM_KC_UNKNOWN   , // 0xFC
    [VK_PA1                ] = MM_KC_UNKNOWN   , // 0xFD
    [VK_OEM_CLEAR          ] = MM_KC_UNKNOWN   , // 0xFE
};

MM_EXPORT_NWSI int mmOSVirtualKeycodeTranslate(int keycode)
{
    if (0 <= keycode && keycode < 256)
    {
        return kVKeycodeMap[keycode];
    }
    else
    {
        return MM_KC_UNKNOWN;
    }
}
