/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmObjcARC_h__
#define __mmObjcARC_h__

#if __has_feature(objc_arc)

// objc arc mode not need explicit manager memory.
#define mmObjc_Autorelease(p)
#define mmObjc_Dealloc(p)
#define mmObjc_Release(p)
#define mmObjc_Retain(p)

#else

// objc none arc mode must explicit manager memory.
#define mmObjc_Autorelease(p) [p autorelease]
#define mmObjc_Dealloc(p) [p dealloc]
#define mmObjc_Release(p) [p release]
#define mmObjc_Retain(p) [p retain]

#endif

#endif//__mmObjcARC_h__
