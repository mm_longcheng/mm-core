/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmLocale.h"

#import <Foundation/NSString.h>
#import <Foundation/NSLocale.h>

MM_EXPORT_NWSI void mmLocale_LanguageCountry(struct mmString* pLanguage, struct mmString* pCountry)
{
    // char code[3]={0};
    // NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    // NSArray* languages = [defaults objectForKey:@"AppleLanguages"];
    // NSString* currentLanguage = [languages objectAtIndex:0];
    //
    // // get the current language code.(such as English is "en", Chinese is "zh" and so on)
    // NSDictionary* temp = [NSLocale componentsFromLocaleIdentifier:currentLanguage];
    // NSString* languageCode = [temp objectForKey:NSLocaleLanguageCode];
    // [languageCode getCString:code maxLength:3 encoding:NSASCIIStringEncoding];
    // code[2]='\0';
    
    
    // Reachability* reachability = nil;
    
    NSLocale* pLocale = [NSLocale currentLocale];
    NSString* pLanguageCode = [pLocale languageCode];
    NSString* pCountryCode = [pLocale countryCode];
    
    mmString_Assigns(pLanguage, [pLanguageCode UTF8String]);
    mmString_Assigns(pCountry, [pCountryCode UTF8String]);
}

