/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmDevice.h"

#include "mmOSDevice.h"

MM_EXPORT_NWSI void mmDevice_Init(struct mmDevice* p)
{
    mmString_Init(&p->hName);
    mmString_Init(&p->hModel);
    mmString_Init(&p->hLocalizedModel);
    mmString_Init(&p->hSystemName);
    mmString_Init(&p->hSystemVersion);
    
    mmString_Init(&p->hSysName);
    mmString_Init(&p->hNodeName);
    mmString_Init(&p->hRelease);
    mmString_Init(&p->hVersion);
    mmString_Init(&p->hMachine);

    mmString_Assigns(&p->hName, "");
    mmString_Assigns(&p->hModel, "");
    mmString_Assigns(&p->hLocalizedModel, "");
    mmString_Assigns(&p->hSystemName, "");
    mmString_Assigns(&p->hSystemVersion, "");
    
    mmString_Assigns(&p->hSysName, "");
    mmString_Assigns(&p->hNodeName, "");
    mmString_Assigns(&p->hRelease, "");
    mmString_Assigns(&p->hVersion, "");
    mmString_Assigns(&p->hMachine, "");
}
MM_EXPORT_NWSI void mmDevice_Destroy(struct mmDevice* p)
{
    mmString_Destroy(&p->hName);
    mmString_Destroy(&p->hModel);
    mmString_Destroy(&p->hLocalizedModel);
    mmString_Destroy(&p->hSystemName);
    mmString_Destroy(&p->hSystemVersion);
    
    mmString_Destroy(&p->hSysName);
    mmString_Destroy(&p->hNodeName);
    mmString_Destroy(&p->hRelease);
    mmString_Destroy(&p->hVersion);
    mmString_Destroy(&p->hMachine);
}

MM_EXPORT_NWSI void mmDevice_OnFinishLaunching(struct mmDevice* p)
{
    mmOSDeviceInformation(p);
}
MM_EXPORT_NWSI void mmDevice_OnBeforeTerminate(struct mmDevice* p)
{
    // need do nothing here.
}

