/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextRenderTarget.h"

#include "nwsi/mmGraphicsPlatform.h"
#include "nwsi/mmTextDrawingContext.h"

#include "core/mmAlloc.h"

MM_EXPORT_NWSI
void
mmTextRenderTarget_Init(
    struct mmTextRenderTarget*                     p)
{
    p->pGraphicsFactory = NULL;
    p->pParagraphFormat = NULL;
    
    mmTextBitmap_Init(&p->hBitmap);
    p->rColorSpace = NULL;
    p->rContext = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_Destroy(
    struct mmTextRenderTarget*                     p)
{
    p->rContext = NULL;
    p->rColorSpace = NULL;
    mmTextBitmap_Destroy(&p->hBitmap);
    
    p->pParagraphFormat = NULL;
    p->pGraphicsFactory = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetGraphicsFactory(
    struct mmTextRenderTarget*                     p,
    struct mmGraphicsFactory*                      pGraphicsFactory)
{
    p->pGraphicsFactory = pGraphicsFactory;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetTextParagraphFormat(
    struct mmTextRenderTarget*                     p,
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    p->pParagraphFormat = pParagraphFormat;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_OnFinishLaunching(
    struct mmTextRenderTarget*                     p)
{
    if(NULL != p->pParagraphFormat)
    {
        size_t hWindowSizeW = 0;
        size_t hWindowSizeH = 0;
        size_t hBitsPerComponent = 8;
        size_t hBytesPerRow = 0;
        uint32_t hBitmapInfo = 0;
        
        hBitmapInfo |= kCGImageAlphaPremultipliedLast;
        hBitmapInfo |= kCGBitmapByteOrder32Big;
        
        hWindowSizeW = (size_t)ceilf(p->pParagraphFormat->hWindowSize[0]);
        hWindowSizeH = (size_t)ceilf(p->pParagraphFormat->hWindowSize[1]);
        // Avoid empty rectangles.
        hWindowSizeW = (0 == hWindowSizeW) ? 1 : hWindowSizeW;
        hWindowSizeH = (0 == hWindowSizeH) ? 1 : hWindowSizeH;

        hBytesPerRow = hWindowSizeW * 4;
        
        p->hBitmap.hBitmapW = hWindowSizeW;
        p->hBitmap.hBitmapH = hWindowSizeH;
        p->hBitmap.hBitsPerComponent = hBitsPerComponent;
        p->hBitmap.hBytesPerRow = hBytesPerRow;
        p->hBitmap.hBitmapInfo = hBitmapInfo;
        mmTextBitmap_OnFinishLaunching(&p->hBitmap);
        
        p->rColorSpace = CGColorSpaceCreateDeviceRGB();
        
        p->rContext = CGBitmapContextCreate(
            p->hBitmap.pBuffer,
            hWindowSizeW,
            hWindowSizeH,
            hBitsPerComponent,
            hBytesPerRow,
            p->rColorSpace,
            hBitmapInfo);
    }
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_OnBeforeTerminate(
    struct mmTextRenderTarget*                     p)
{
    mmCFTypeRefSafeRelease(p->rContext);
    mmCFTypeRefSafeRelease(p->rColorSpace);
    mmTextBitmap_OnBeforeTerminate(&p->hBitmap);
    
    p->rColorSpace = NULL;
    p->rContext = NULL;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BeginDraw(
    struct mmTextRenderTarget*                     p)
{
    assert(nil != p->rContext && "rContext is a nil.");
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_SetTransform(
    struct mmTextRenderTarget*                     p,
    struct mmAffineTransform*                      hTransform)
{
    assert(nil != p->rContext && "rContext is a nil.");

    CGAffineTransform hMatrix =
    {
        hTransform->a, hTransform->b,
        hTransform->c, hTransform->d,
        hTransform->x, hTransform->y,
    };
    CGContextSetTextMatrix(p->rContext, hMatrix);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_Clear(
    struct mmTextRenderTarget*                     p)
{
    assert(nil != p->rContext && "rContext is a nil.");
    mmTextBitmap_Clear(&p->hBitmap);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_EndDraw(
    struct mmTextRenderTarget*                     p)
{
    assert(nil != p->rContext && "rContext is a nil.");
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BeginTransparency(
    struct mmTextRenderTarget*                     p)
{
    assert(nil != p->rContext && "rContext is a nil.");
    assert(NULL != p->pParagraphFormat && "pParagraphFormat is a nil.");
    struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
    CGFloat hWindowSizeW = pParagraphFormat->hWindowSize[0];
    CGFloat hWindowSizeH = pParagraphFormat->hWindowSize[1];
    CGRect hRextRect = CGRectMake(0, 0, hWindowSizeW, hWindowSizeH);
    CGContextBeginTransparencyLayerWithRect(p->rContext, hRextRect, NULL);
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_EndTransparency(
    struct mmTextRenderTarget*                     p)
{
    assert(nil != p->rContext && "rContext is a nil.");
    CGContextEndTransparencyLayer(p->rContext);
}

MM_EXPORT_NWSI
const void*
mmTextRenderTarget_BufferLock(
    struct mmTextRenderTarget*                     p)
{
    return p->hBitmap.pBuffer;
}

MM_EXPORT_NWSI
void
mmTextRenderTarget_BufferUnLock(
    struct mmTextRenderTarget*                     p,
    const void*                                    pBuffer)
{
    // need do nothing.
}

