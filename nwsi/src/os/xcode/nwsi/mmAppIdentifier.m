/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


#import "mmAppIdentifier.h"

#import "core/mmAlloc.h"
#import "core/mmLogger.h"

#import "nwsi/mmKeyChain.h"
#import "nwsi/mmObjcARC.h"

#import <Foundation/Foundation.h>

static const char* MM_APP_IDENTIFIER_PREFS_TEAM_APPID = "org.mm.nwsi";

static const char* MM_APP_IDENTIFIER_PREFS_GROUP = "AppIdentifier";
static const char* MM_APP_IDENTIFIER_PREFS_FIELD_UUID_DEVICE = "UUIDDevice";
static const char* MM_APP_IDENTIFIER_PREFS_FIELD_UUID_APP = "UUIDApp";

static void mmAppIdentifier_SaveCurrentUUID(NSUserDefaults* pUserDefaults, NSString* pField, struct mmString* pUUID)
{
    NSString* pUUIDString = [[NSString alloc] initWithUTF8String:mmString_CStr(pUUID)];
    [pUserDefaults setObject:pUUIDString forKey:pField];
    [pUserDefaults synchronize];
    mmObjc_Release(pUUIDString);
}

static void mmAppIdentifier_UUIDToBytes(struct mmString* pUUID, char hUUIDBytes[64])
{
    mmMemset(hUUIDBytes, 0, 64);
    mmMemcpy(hUUIDBytes, mmString_CStr(pUUID), mmString_Size(pUUID));
}

static void mmAppIdentifier_GenerateUUIDDevice(const char* pAppId, char hUUIDBytes[64])
{
    const char* pTeamAppId = MM_APP_IDENTIFIER_PREFS_TEAM_APPID;
    
    const char* pGroup = MM_APP_IDENTIFIER_PREFS_GROUP;
    const char* pField = MM_APP_IDENTIFIER_PREFS_FIELD_UUID_DEVICE;

    int hCode = -1;
    
    struct mmString hUUID;
    struct mmString hAppIdentifierPrefix;
    struct mmString hAccess;

    mmString_Init(&hUUID);
    mmString_Init(&hAppIdentifierPrefix);
    mmString_Init(&hAccess);
    
    mmKeyChain_GetAppIdentifierPrefix(&hAppIdentifierPrefix);
    mmString_Assign(&hAccess, &hAppIdentifierPrefix);
    mmString_Appends(&hAccess, pAppId);

    NSString* pGroupString = [[NSString alloc] initWithUTF8String:pGroup];
    NSString* pFieldString = [[NSString alloc] initWithUTF8String:pField];

    NSUserDefaults* pUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:pGroupString];

    do
    {
        NSString* pUUIDString = [pUserDefaults objectForKey:pFieldString];
        if (nil != pUUIDString)
        {
            const char* pUUID = [pUUIDString UTF8String];
            mmString_Assigns(&hUUID, pUUID);
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            break;
        }
        
        hCode = mmKeyChain_Select(pAppId, pGroup, pField, &hUUID);
        if(0 == hCode)
        {
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            mmAppIdentifier_SaveCurrentUUID(pUserDefaults, pFieldString, &hUUID);
            break;
        }

        if(mmString_Empty(&hAppIdentifierPrefix))
        {
            hCode = mmKeyChain_Select(pTeamAppId, pGroup, pField, &hUUID);
        }
        else
        {
            hCode = mmKeyChain_SharedSelect(mmString_CStr(&hAccess), pTeamAppId, pGroup, pField, &hUUID);
        }
        
        if(0 == hCode)
        {
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            mmKeyChain_Insert(pAppId, pGroup, pField, &hUUID);
            mmAppIdentifier_SaveCurrentUUID(pUserDefaults, pFieldString, &hUUID);
            break;
        }
        
        {
            mmAppIdentifier_GenerateRandomUUID(hUUIDBytes);

            mmString_Assigns(&hUUID, hUUIDBytes);
            
            if(mmString_Empty(&hAppIdentifierPrefix))
            {
                mmKeyChain_Insert(pTeamAppId, pGroup, pField, &hUUID);
            }
            else
            {
                mmKeyChain_SharedInsert(mmString_CStr(&hAccess), pTeamAppId, pGroup, pField, &hUUID);
            }
            
            mmKeyChain_Insert(pAppId, pGroup, pField, &hUUID);
            mmAppIdentifier_SaveCurrentUUID(pUserDefaults, pFieldString, &hUUID);
        }
    } while(0);

    mmObjc_Release(pFieldString);
    mmObjc_Release(pGroupString);

    mmString_Destroy(&hAccess);
    mmString_Destroy(&hAppIdentifierPrefix);
    mmString_Destroy(&hUUID);
}

static void mmAppIdentifier_GenerateUUIDAppBytes(const char* pAppId, char hUUIDBytes[64])
{
    const char* pGroup = MM_APP_IDENTIFIER_PREFS_GROUP;
    const char* pField = MM_APP_IDENTIFIER_PREFS_FIELD_UUID_APP;
    
    int hCode = -1;
    
    struct mmString hUUID;
    
    mmString_Init(&hUUID);

    NSString* pGroupString = [[NSString alloc] initWithUTF8String:pGroup];
    NSString* pFieldString = [[NSString alloc] initWithUTF8String:pField];
    
    NSUserDefaults* pUserDefaults = [[NSUserDefaults alloc] initWithSuiteName:pGroupString];

    do
    {
        NSString* pUUIDString = [pUserDefaults objectForKey:pFieldString];
        if (nil != pUUIDString)
        {
            const char* pUUID = [pUUIDString UTF8String];
            mmString_Assigns(&hUUID, pUUID);
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            break;
        }
        
        hCode = mmKeyChain_Select(pAppId, pGroup, pField, &hUUID);
        if(0 == hCode)
        {
            mmAppIdentifier_UUIDToBytes(&hUUID, hUUIDBytes);
            mmAppIdentifier_SaveCurrentUUID(pUserDefaults, pFieldString, &hUUID);
            break;
        }
        
        {
            mmAppIdentifier_GenerateRandomUUID(hUUIDBytes);

            mmString_Assigns(&hUUID, hUUIDBytes);
            mmKeyChain_Insert(pAppId, pGroup, pField, &hUUID);
            mmAppIdentifier_SaveCurrentUUID(pUserDefaults, pFieldString, &hUUID);
        }
    } while(0);

    mmObjc_Release(pFieldString);
    mmObjc_Release(pGroupString);

    mmString_Destroy(&hUUID);
}


MM_EXPORT_NWSI void mmAppIdentifier_GenerateRandomUUID(char hUUID[64])
{
    // length = 36
    // FECAABC7-341B-48FA-9910-26D7D908D95C
    CFUUIDRef pUUID = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef pUUIDCFString = CFUUIDCreateString(kCFAllocatorDefault, pUUID);
    const char* pCString = CFStringGetCStringPtr(pUUIDCFString, kCFStringEncodingUTF8);
    size_t hCStringLength = mmStrlen(pCString);
    hCStringLength = hCStringLength < 36 ? hCStringLength : hCStringLength;
    mmMemset(hUUID, 0, 64);
    mmMemcpy(hUUID, pCString, hCStringLength);
    CFRelease(pUUIDCFString);
    CFRelease(pUUID);
}

MM_EXPORT_NWSI void mmAppIdentifier_Init(struct mmAppIdentifier* p)
{
    mmString_Init(&p->hAppId);

    mmString_Init(&p->hUUIDDevice);
    mmString_Init(&p->hUUIDApp);
    
    mmString_Assigns(&p->hAppId, "");
    
    mmString_Assigns(&p->hUUIDDevice, "");
    mmString_Assigns(&p->hUUIDApp, "");
}
MM_EXPORT_NWSI void mmAppIdentifier_Destroy(struct mmAppIdentifier* p)
{
    mmString_Destroy(&p->hAppId);
    
    mmString_Destroy(&p->hUUIDDevice);
    mmString_Destroy(&p->hUUIDApp);
}

MM_EXPORT_NWSI void mmAppIdentifier_SetAppId(struct mmAppIdentifier* p, const char* pAppId)
{
    mmString_Assigns(&p->hAppId, pAppId);
}

MM_EXPORT_NWSI void mmAppIdentifier_OnFinishLaunching(struct mmAppIdentifier* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    mmAppIdentifier_GetUUIDDeviceString(p, &p->hUUIDDevice);
    mmAppIdentifier_GetUUIDAppString(p, &p->hUUIDApp);
    
    mmLogger_LogI(gLogger, "AppIdentifier AppIdentifier.OnFinishLaunching");
    mmLogger_LogI(gLogger, "AppIdentifier ---------------------+----------------------------------");
    mmLogger_LogI(gLogger, "AppIdentifier           hUUIDDevice: %s", mmString_CStr(&p->hUUIDDevice));
    mmLogger_LogI(gLogger, "AppIdentifier              hUUIDApp: %s", mmString_CStr(&p->hUUIDApp));
    mmLogger_LogI(gLogger, "AppIdentifier ---------------------+----------------------------------");
}
MM_EXPORT_NWSI void mmAppIdentifier_OnBeforeTerminate(struct mmAppIdentifier* p)
{

}

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceBytes(struct mmAppIdentifier* p, char hUUIDBytes[64])
{
    mmAppIdentifier_GenerateUUIDDevice(mmString_CStr(&p->hAppId), hUUIDBytes);
}
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppBytes(struct mmAppIdentifier* p, char hUUIDBytes[64])
{
    mmAppIdentifier_GenerateUUIDAppBytes(mmString_CStr(&p->hAppId), hUUIDBytes);
}

MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDDeviceString(struct mmAppIdentifier* p, struct mmString* pUUIDString)
{
    char hUUIDBytes[64] = { 0 };
    mmAppIdentifier_GetUUIDDeviceBytes(p, hUUIDBytes);
    mmString_Assigns(pUUIDString, hUUIDBytes);
}
MM_EXPORT_NWSI void mmAppIdentifier_GetUUIDAppString(struct mmAppIdentifier* p, struct mmString* pUUIDString)
{
    char hUUIDBytes[64] = { 0 };
    mmAppIdentifier_GetUUIDAppBytes(p, hUUIDBytes);
    mmString_Assigns(pUUIDString, hUUIDBytes);
}
