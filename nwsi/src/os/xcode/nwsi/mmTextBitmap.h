/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextBitmap_h__
#define __mmTextBitmap_h__

#include "core/mmCore.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmTextBitmap
{
    size_t hBitmapW;
    size_t hBitmapH;
    size_t hBitsPerComponent;
    size_t hBytesPerRow;
    
    uint32_t hBitmapInfo;
    
    size_t hLength;
    uint8_t* pBuffer;
};

MM_EXPORT_NWSI 
void 
mmTextBitmap_Init(
    struct mmTextBitmap*                           p);

MM_EXPORT_NWSI 
void 
mmTextBitmap_Destroy(
    struct mmTextBitmap*                           p);

MM_EXPORT_NWSI 
void 
mmTextBitmap_OnFinishLaunching(
    struct mmTextBitmap*                           p);

MM_EXPORT_NWSI 
void 
mmTextBitmap_OnBeforeTerminate(
    struct mmTextBitmap*                           p);

MM_EXPORT_NWSI 
void 
mmTextBitmap_Clear(
    struct mmTextBitmap*                           p);

#include "core/mmSuffix.h"

#endif//__mmTextBitmap_h__
