/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmPackageAssets.h"

#import "core/mmLogger.h"

#import "core/mmFilePath.h"

#import "nwsi/mmObjcARC.h"

#import <Foundation/NSBundle.h>
#import <Foundation/NSPathUtilities.h>

MM_EXPORT_NWSI void mmPackageAssets_Init(struct mmPackageAssets* p)
{
    mmString_Init(&p->hModuleName);
    
    mmString_Init(&p->hPackageName);
    mmString_Init(&p->hVersionName);
    mmString_Init(&p->hVersionCode);
    mmString_Init(&p->hDisplayName);
    
    mmString_Init(&p->hHomeDirectory);

    mmString_Init(&p->hInternalFilesDirectory);
    mmString_Init(&p->hInternalCacheDirectory);
    mmString_Init(&p->hExternalFilesDirectory);
    mmString_Init(&p->hExternalCacheDirectory);
    
    mmString_Init(&p->hPluginFolder);
    mmString_Init(&p->hDynlibFolder);

    mmString_Init(&p->hShaderCacheFolder);
    mmString_Init(&p->hTextureCacheFolder);

    mmString_Init(&p->hLoggerPathName);
    mmString_Init(&p->hLoggerPath);

    mmString_Init(&p->hAssetsRootFolderPath);
    mmString_Init(&p->hAssetsRootFolderBase);
    
    mmString_Init(&p->hAssetsRootSourcePath);
    mmString_Init(&p->hAssetsRootSourceBase);
    
    p->pAppHandler = NULL;
    p->pAssetManager = NULL;
   
    // Initial value.
    mmString_Assigns(&p->hModuleName, "mm.nwsi");
    
    mmString_Assigns(&p->hPackageName, "");
    mmString_Assigns(&p->hVersionName, "");
    mmString_Assigns(&p->hVersionCode, "");
    mmString_Assigns(&p->hDisplayName, "");
    
    mmString_Assigns(&p->hHomeDirectory, "");

    mmString_Assigns(&p->hInternalFilesDirectory, "");
    mmString_Assigns(&p->hInternalCacheDirectory, "");
    mmString_Assigns(&p->hExternalFilesDirectory, "");
    mmString_Assigns(&p->hExternalCacheDirectory, "");
    
    mmString_Assigns(&p->hPluginFolder, "");
    mmString_Assigns(&p->hDynlibFolder, "");

    mmString_Assigns(&p->hShaderCacheFolder, "shader");
    mmString_Assigns(&p->hTextureCacheFolder, "texture");

    mmString_Assigns(&p->hLoggerPathName, "log");
    mmString_Assigns(&p->hLoggerPath, "");
    
    mmString_Assigns(&p->hAssetsRootFolderPath, "");
    mmString_Assigns(&p->hAssetsRootFolderBase, "");
    
    mmString_Assigns(&p->hAssetsRootSourcePath, "");
    mmString_Assigns(&p->hAssetsRootSourceBase, "");
}
MM_EXPORT_NWSI void mmPackageAssets_Destroy(struct mmPackageAssets* p)
{
    mmString_Destroy(&p->hModuleName);
    
    mmString_Destroy(&p->hPackageName);
    mmString_Destroy(&p->hVersionName);
    mmString_Destroy(&p->hVersionCode);
    mmString_Destroy(&p->hDisplayName);
    
    mmString_Destroy(&p->hHomeDirectory);

    mmString_Destroy(&p->hInternalFilesDirectory);
    mmString_Destroy(&p->hInternalCacheDirectory);
    mmString_Destroy(&p->hExternalFilesDirectory);
    mmString_Destroy(&p->hExternalCacheDirectory);
    
    mmString_Destroy(&p->hPluginFolder);
    mmString_Destroy(&p->hDynlibFolder);

    mmString_Destroy(&p->hShaderCacheFolder);
    mmString_Destroy(&p->hTextureCacheFolder);

    mmString_Destroy(&p->hLoggerPathName);
    mmString_Destroy(&p->hLoggerPath);

    mmString_Destroy(&p->hAssetsRootFolderPath);
    mmString_Destroy(&p->hAssetsRootFolderBase);
    
    mmString_Destroy(&p->hAssetsRootSourcePath);
    mmString_Destroy(&p->hAssetsRootSourceBase);
    
    p->pAppHandler = NULL;
    p->pAssetManager = NULL;
}

MM_EXPORT_NWSI void mmPackageAssets_SetAppHandler(struct mmPackageAssets* p, void* pAppHandler)
{
    p->pAppHandler = pAppHandler;
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetManager(struct mmPackageAssets* p, void* pAssetManager)
{
    p->pAssetManager = pAssetManager;
}
MM_EXPORT_NWSI void mmPackageAssets_SetModuleName(struct mmPackageAssets* p, const char* pModuleName)
{
    mmString_Assigns(&p->hModuleName, pModuleName);
}
MM_EXPORT_NWSI void mmPackageAssets_SetPluginFolder(struct mmPackageAssets* p, const char* pPluginFolder)
{
    mmString_Assigns(&p->hPluginFolder, pPluginFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetDynlibFolder(struct mmPackageAssets* p, const char* pDynlibFolder)
{
    mmString_Assigns(&p->hDynlibFolder, pDynlibFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetShaderCacheFolder(struct mmPackageAssets* p, const char* pShaderCacheFolder)
{
    mmString_Assigns(&p->hShaderCacheFolder, pShaderCacheFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetTextureCacheFolder(struct mmPackageAssets* p, const char* pTextureCacheFolder)
{
    mmString_Assigns(&p->hTextureCacheFolder, pTextureCacheFolder);
}
MM_EXPORT_NWSI void mmPackageAssets_SetLoggerPathName(struct mmPackageAssets* p, const char* pLoggerPathName)
{
    mmString_Assigns(&p->hLoggerPathName, pLoggerPathName);
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootFolder(struct mmPackageAssets* p, const char* pPath, const char* pBase)
{
    mmString_Assigns(&p->hAssetsRootFolderPath, pPath);
    mmString_Assigns(&p->hAssetsRootFolderBase, pBase);
}
MM_EXPORT_NWSI void mmPackageAssets_SetAssetsRootSource(struct mmPackageAssets* p, const char* pPath, const char* pBase)
{
    mmString_Assigns(&p->hAssetsRootSourcePath, pPath);
    mmString_Assigns(&p->hAssetsRootSourceBase, pBase);
}

MM_EXPORT_NWSI void mmPackageAssets_AttachNativeResource(struct mmPackageAssets* p)
{
    NSBundle* pBundle = [NSBundle mainBundle];
    NSDictionary* pDictionary = [pBundle infoDictionary];
    
    NSString* pResourcePath = [pBundle resourcePath];

    NSString* pHomeDirectory = NSHomeDirectory();
    
    NSString* pPackageName = [pDictionary objectForKey:@"CFBundleIdentifier"];
    NSString* pVersionName = [pDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString* pVersionCode = [pDictionary objectForKey:@"CFBundleVersion"];
    
    NSString* pBundleDisplayName = [pDictionary objectForKey:@"CFBundleDisplayName"];
    NSString* pBundleName = [pDictionary objectForKey:@"CFBundleName"];
    NSString* pDisplayName = (nil == pBundleDisplayName) ? pBundleName : pBundleDisplayName;
    
    NSArray<NSString*>* pArr0 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray<NSString*>* pArr1 = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSArray<NSString*>* pArr2 = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    
    NSString* pDocumentPath = pArr0.firstObject;
    NSString* pCachePath = pArr1.firstObject;
    NSString* pApplicationSupportPath = pArr2.firstObject;

    // ios default assets directory.
    //
    // InternalFilesDirectory Library/Application Support/<ModuleName>/files
    // InternalCacheDirectory Library/Caches/<ModuleName>/cache
    // ExternalFilesDirectory Document/<ModuleName>/files
    // ExternalCacheDirectory Document/<ModuleName>/cache
    // LoggerPath             Document/<ModuleName>/cache/log
    // AssetsPath             <BundlePath>/assets
    
    // Writable directory.
    NSString* pModuleName = [[NSString alloc] initWithUTF8String:mmString_CStr(&p->hModuleName)];
    NSString* pLoggerPathName = [[NSString alloc] initWithUTF8String:mmString_CStr(&p->hLoggerPathName)];
    NSString* pInternalPath0 = [[NSString alloc] initWithString:pApplicationSupportPath];
    NSString* pInternalPath1 = [[NSString alloc] initWithString:pCachePath];
    NSString* pExternalPath0 = [[NSString alloc] initWithString:pDocumentPath];
    NSString* pInternalModulePath0 = [[NSString alloc] initWithFormat:@"%@/%@", pInternalPath0, pModuleName];
    NSString* pInternalModulePath1 = [[NSString alloc] initWithFormat:@"%@/%@", pInternalPath1, pModuleName];
    NSString* pExternalModulePath0 = [[NSString alloc] initWithFormat:@"%@/%@", pExternalPath0, pModuleName];
    NSString* pInternalFilesDirectory = [[NSString alloc] initWithFormat:@"%@/files", pInternalModulePath0];
    NSString* pInternalCacheDirectory = [[NSString alloc] initWithFormat:@"%@/cache", pInternalModulePath1];
    NSString* pExternalFilesDirectory = [[NSString alloc] initWithFormat:@"%@/files", pExternalModulePath0];
    NSString* pExternalCacheDirectory = [[NSString alloc] initWithFormat:@"%@/cache", pExternalModulePath0];
    // Assets.
    NSString* pAssetsRootFolderPath = [[NSString alloc] initWithString:pResourcePath];
    NSString* pAssetsRootFolderBase = [[NSString alloc] initWithUTF8String:"assets"];
    // Logger.
    NSString* pLoggerPath = [[NSString alloc] initWithFormat:@"%@/%@", pExternalCacheDirectory, pLoggerPathName];
    
    mmMkdirIfNotExist([pInternalPath0 UTF8String]);
    mmMkdirIfNotExist([pInternalModulePath0 UTF8String]);
    mmMkdirIfNotExist([pInternalModulePath1 UTF8String]);
    mmMkdirIfNotExist([pExternalModulePath0 UTF8String]);
    mmMkdirIfNotExist([pInternalFilesDirectory UTF8String]);
    mmMkdirIfNotExist([pInternalCacheDirectory UTF8String]);
    mmMkdirIfNotExist([pExternalFilesDirectory UTF8String]);
    mmMkdirIfNotExist([pExternalCacheDirectory UTF8String]);
    mmMkdirIfNotExist([pLoggerPath UTF8String]);
    
    mmString_Assigns(&p->hPackageName, [pPackageName UTF8String]);
    mmString_Assigns(&p->hVersionName, [pVersionName UTF8String]);
    mmString_Assigns(&p->hVersionCode, [pVersionCode UTF8String]);
    mmString_Assigns(&p->hDisplayName, [pDisplayName UTF8String]);
    
    mmString_Assigns(&p->hHomeDirectory, [pHomeDirectory UTF8String]);

    mmString_Assigns(&p->hInternalFilesDirectory, [pInternalFilesDirectory UTF8String]);
    mmString_Assigns(&p->hInternalCacheDirectory, [pInternalCacheDirectory UTF8String]);
    mmString_Assigns(&p->hExternalFilesDirectory, [pExternalFilesDirectory UTF8String]);
    mmString_Assigns(&p->hExternalCacheDirectory, [pExternalCacheDirectory UTF8String]);
    
    mmString_Assigns(&p->hLoggerPath, [pLoggerPath UTF8String]);
    
    mmString_Assigns(&p->hAssetsRootFolderPath, [pAssetsRootFolderPath UTF8String]);
    mmString_Assigns(&p->hAssetsRootFolderBase, [pAssetsRootFolderBase UTF8String]);
    
    mmString_Assigns(&p->hAssetsRootSourcePath, "");
    mmString_Assigns(&p->hAssetsRootSourceBase, "");
    
    mmObjc_Release(pModuleName);
    mmObjc_Release(pLoggerPathName);
    mmObjc_Release(pInternalPath0);
    mmObjc_Release(pInternalPath1);
    mmObjc_Release(pExternalPath0);
    mmObjc_Release(pInternalModulePath0);
    mmObjc_Release(pInternalModulePath1);
    mmObjc_Release(pExternalModulePath0);
    mmObjc_Release(pInternalFilesDirectory);
    mmObjc_Release(pInternalCacheDirectory);
    mmObjc_Release(pExternalFilesDirectory);
    mmObjc_Release(pExternalCacheDirectory);
    
    mmObjc_Release(pAssetsRootFolderPath);
    mmObjc_Release(pAssetsRootFolderBase);
    
    mmObjc_Release(pLoggerPath);
}
MM_EXPORT_NWSI void mmPackageAssets_DetachNativeResource(struct mmPackageAssets* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI void mmPackageAssets_OnFinishLaunching(struct mmPackageAssets* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    // logger information.
    mmLogger_LogI(gLogger, "Assets mmPackageAssets.OnFinishLaunching");
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
    mmLogger_LogI(gLogger, "Assets                pPackageName : %s", mmString_CStr(&p->hPackageName));
    mmLogger_LogI(gLogger, "Assets                pVersionName : %s", mmString_CStr(&p->hVersionName));
    mmLogger_LogI(gLogger, "Assets                pVersionCode : %s", mmString_CStr(&p->hVersionCode));
    mmLogger_LogI(gLogger, "Assets                pDisplayName : %s", mmString_CStr(&p->hDisplayName));
    mmLogger_LogI(gLogger, "Assets               pPluginFolder : %s", mmString_CStr(&p->hPluginFolder));
    mmLogger_LogI(gLogger, "Assets               pDynlibFolder : %s", mmString_CStr(&p->hDynlibFolder));
    mmLogger_LogI(gLogger, "Assets              hHomeDirectory : %s", mmString_CStr(&p->hHomeDirectory));
    mmLogger_LogI(gLogger, "Assets          pShaderCacheFolder : %s", mmString_CStr(&p->hShaderCacheFolder));
    mmLogger_LogI(gLogger, "Assets         pTextureCacheFolder : %s", mmString_CStr(&p->hTextureCacheFolder));
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
    mmLogger_LogI(gLogger, "Assets     pInternalFilesDirectory : %s", mmString_CStr(&p->hInternalFilesDirectory));
    mmLogger_LogI(gLogger, "Assets     pInternalCacheDirectory : %s", mmString_CStr(&p->hInternalCacheDirectory));
    mmLogger_LogI(gLogger, "Assets     pExternalFilesDirectory : %s", mmString_CStr(&p->hExternalFilesDirectory));
    mmLogger_LogI(gLogger, "Assets     pExternalCacheDirectory : %s", mmString_CStr(&p->hExternalCacheDirectory));
    mmLogger_LogI(gLogger, "Assets ----------------------------+----------------------------------");
}
MM_EXPORT_NWSI void mmPackageAssets_OnBeforeTerminate(struct mmPackageAssets* p)
{
    // need do nothing here.
}

MM_EXPORT_NWSI
void
mmPackageAssets_MakeShaderCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &p->hExternalCacheDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &p->hShaderCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}

MM_EXPORT_NWSI
void
mmPackageAssets_MakeTextureCachePath(
    struct mmPackageAssets*                        p,
    const char*                                    pathname,
    struct mmString*                               fullname)
{
    mmString_Assign(fullname, &p->hExternalCacheDirectory);
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Append(fullname, &p->hTextureCacheFolder);
    mmMkdirIfNotExist(mmString_CStr(fullname));
    mmDirectoryHaveSuffix(fullname, mmString_CStr(fullname));
    mmString_Appends(fullname, pathname);
}
