/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmGraphicsFactory.h"
#include "mmGraphicsPlatform.h"
#include "mmURLString.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

#include "dish/mmFileContext.h"

#include <CoreText/CoreText.h>

static
int
mmGraphicsFactory_LoadFontFromFolder(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily,
    const char*                                    pFontPath,
    const char*                                    pRealPath)
{
    int err = MM_UNKNOWN;

    CFErrorRef rError = NULL;
    CFStringRef rRealPath = NULL;
    CFURLRef rURL = NULL;
    
    do
    {
        bool hError = 0;

        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        struct mmFontResource* u;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL != it)
        {
            err = MM_SUCCESS;
            break;
        }
        
        rRealPath = CFStringCreateWithCString(
            kCFAllocatorDefault,
            pRealPath,
            kCFStringEncodingUTF8);
        if (NULL == rRealPath)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CFStringCreateWithCString failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        rURL = CFURLCreateWithFileSystemPath(
            kCFAllocatorDefault,
            rRealPath,
            kCFURLPOSIXPathStyle,
            FALSE);
        if (NULL == rURL)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CFURLCreateWithString failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        hError = CTFontManagerRegisterFontsForURL(
            rURL,
            kCTFontManagerScopeProcess,
            &rError);
        if (false == hError)
        {
            char cError[256] = { 0 };
            CFStringRef hErrorDesc = CFErrorCopyDescription(rError);
            CFStringGetCString(hErrorDesc, cError, 256, kCFStringEncodingUTF8);
            mmCFTypeRefSafeRelease(hErrorDesc);
            mmLogger_LogE(gLogger, "%s %d"
                " CTFontManagerRegisterFontsForURL failure. Error: %s",
                __FUNCTION__, __LINE__, cError);
            err = MM_FAILURE;
            break;
        }

        u = (struct mmFontResource*)mmMalloc(sizeof(struct mmFontResource));
        mmFontResource_Init(u);
        mmString_Assign(&u->hFamily, &hWeakFamily);
        mmString_Assigns(&u->hPath, pFontPath);
        mmString_Assigns(&u->hRealPath, pRealPath);
        u->pHandle = (void*)NULL;
        mmRbtreeStringVpt_Set(&p->hFonts, &hWeakFamily, u);
        err = MM_SUCCESS;
    } while (0);

    mmCFTypeRefSafeRelease(rURL);
    mmCFTypeRefSafeRelease(rRealPath);
    mmCFTypeRefSafeRelease(rError);
    
    return err;
}

static
int
mmGraphicsFactory_UnloadFontByFolder(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily)
{
    int err = MM_UNKNOWN;

    CFErrorRef rError = NULL;
    CFStringRef rRealPath = NULL;
    CFURLRef rURL = NULL;
    
    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        bool hError = 0;
        const char* pRealPath = "";
        struct mmString hRealPath;
        
        struct mmFontResource* u;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL == it)
        {
            err = MM_SUCCESS;
            break;
        }

        u = (struct mmFontResource*)it->v;

        mmString_Init(&hRealPath);
        mmString_Assign(&hRealPath, &u->hRealPath);

        mmRbtreeStringVpt_Rmv(&p->hFonts, &hWeakFamily);
        mmFontResource_Destroy(u);
        mmFree(u);

        pRealPath = mmString_CStr(&hRealPath);
        rRealPath = CFStringCreateWithCString(
            kCFAllocatorDefault,
            pRealPath,
            kCFStringEncodingUTF8);
        mmString_Destroy(&hRealPath);
        if (NULL == rRealPath)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CFStringCreateWithCString failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        rURL = CFURLCreateWithFileSystemPath(
            kCFAllocatorDefault,
            rRealPath,
            kCFURLPOSIXPathStyle,
            FALSE);
        if (NULL == rURL)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CFURLCreateWithString failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        hError = CTFontManagerUnregisterFontsForURL(
            rURL,
            kCTFontManagerScopeProcess,
            &rError);
        if (false == hError)
        {
            char cError[256] = { 0 };
            CFStringRef hErrorDesc = CFErrorCopyDescription(rError);
            CFStringGetCString(hErrorDesc, cError, 256, kCFStringEncodingUTF8);
            mmCFTypeRefSafeRelease(hErrorDesc);
            mmLogger_LogE(gLogger, "%s %d"
                " CTFontManagerUnregisterFontsForURL failure. Error: %s",
                __FUNCTION__, __LINE__, cError);
            err = MM_FAILURE;
            break;
        }
        
        err = MM_SUCCESS;
    } while (0);
    
    mmCFTypeRefSafeRelease(rURL);
    mmCFTypeRefSafeRelease(rRealPath);
    mmCFTypeRefSafeRelease(rError);

    return err;
}

static
int
mmGraphicsFactory_LoadFontFromSource(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily,
    const char*                                    pFontPath,
    const char*                                    pRealPath,
    struct mmByteBuffer*                           pBytes)
{
    int err = MM_UNKNOWN;

    CFErrorRef rError = NULL;
    CFDataRef rData = NULL;
    CGDataProviderRef rDataProvider = NULL;
    
    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        const UInt8* bytes = NULL;
        CFIndex length = 0;
        CGFontRef rFont = NULL;
        bool hError = 0;

        struct mmFontResource* u;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL != it)
        {
            err = MM_SUCCESS;
            break;
        }

        if (NULL == pBytes->buffer || 0 == pBytes->length)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " pBytes is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }

        bytes = (const UInt8*)(pBytes->buffer + pBytes->offset);
        length = (CFIndex)pBytes->length;
        rData = CFDataCreate(
            kCFAllocatorDefault,
            bytes,
            length);
        if (NULL == rData)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CFDataCreateWithBytesNoCopy failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        rDataProvider = CGDataProviderCreateWithCFData(rData);
        if (NULL == rDataProvider)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CGDataProviderCreateWithCFData failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        rFont = CGFontCreateWithDataProvider(rDataProvider);
        if (NULL == rFont)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " CGFontCreateWithDataProvider failure.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        hError = CTFontManagerRegisterGraphicsFont(rFont, &rError);
        if (false == hError)
        {
            char cError[256] = { 0 };
            CFStringRef hErrorDesc = CFErrorCopyDescription(rError);
            CFStringGetCString(hErrorDesc, cError, 256, kCFStringEncodingUTF8);
            mmCFTypeRefSafeRelease(hErrorDesc);
            mmLogger_LogE(gLogger, "%s %d"
                " CTFontManagerRegisterGraphicsFont failure. Error: %s",
                __FUNCTION__, __LINE__, cError);
            err = MM_FAILURE;
            
            // Note: Here we must release cFont.
            mmCFTypeRefSafeRelease(rFont);
            rFont = NULL;
            break;
        }

        u = (struct mmFontResource*)mmMalloc(sizeof(struct mmFontResource));
        mmFontResource_Init(u);
        mmString_Assign(&u->hFamily, &hWeakFamily);
        mmString_Assigns(&u->hPath, pFontPath);
        mmString_Assigns(&u->hRealPath, pRealPath);
        u->pHandle = (void*)rFont;
        mmRbtreeStringVpt_Set(&p->hFonts, &hWeakFamily, u);
        err = MM_SUCCESS;
    } while (0);

    mmCFTypeRefSafeRelease(rDataProvider);
    mmCFTypeRefSafeRelease(rData);
    mmCFTypeRefSafeRelease(rError);
    
    return err;
}

static
int
mmGraphicsFactory_UnloadFontBySource(
    struct mmGraphicsFactory*                      p,
    const char*                                    pFontFamily)
{
    int err = MM_UNKNOWN;

    CFErrorRef rError = NULL;
    
    do
    {
        struct mmString hWeakFamily;
        struct mmRbtreeStringVptIterator* it;

        CGFontRef rFont = NULL;
        bool hError = 0;

        struct mmFontResource* u;
        
        struct mmLogger* gLogger = mmLogger_Instance();

        mmString_MakeWeaks(&hWeakFamily, pFontFamily);
        it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
        if (NULL == it)
        {
            err = MM_SUCCESS;
            break;
        }

        u = (struct mmFontResource*)it->v;

        rFont = (CGFontRef)u->pHandle;

        mmRbtreeStringVpt_Rmv(&p->hFonts, &hWeakFamily);
        mmFontResource_Destroy(u);
        mmFree(u);

        if (NULL == rFont)
        {
            mmLogger_LogE(gLogger, "%s %d"
                " rFont is invalid.",
                __FUNCTION__, __LINE__);
            err = MM_FAILURE;
            break;
        }
        
        hError = CTFontManagerUnregisterGraphicsFont(rFont, &rError);
        mmCFTypeRefSafeRelease(rFont);
        if (0 == hError)
        {
            char cError[256] = { 0 };
            CFStringRef hErrorDesc = CFErrorCopyDescription(rError);
            CFStringGetCString(hErrorDesc, cError, 256, kCFStringEncodingUTF8);
            mmCFTypeRefSafeRelease(hErrorDesc);
            mmLogger_LogE(gLogger, "%s %d"
                " CTFontManagerUnregisterGraphicsFont failure. Error: %s",
                __FUNCTION__, __LINE__, cError);
            err = MM_FAILURE;
            break;
        }
        
        err = MM_SUCCESS;
        
    } while (0);
    
    mmCFTypeRefSafeRelease(rError);

    return err;
}

MM_EXPORT_NWSI
void
mmFontResource_Init(
    struct mmFontResource*                         p)
{
    mmString_Init(&p->hFamily);
    mmString_Init(&p->hPath);
    mmString_Init(&p->hRealPath);
    p->pHandle = NULL;
}

MM_EXPORT_NWSI
void
mmFontResource_Destroy(
    struct mmFontResource*                         p)
{
    p->pHandle = NULL;
    mmString_Destroy(&p->hRealPath);
    mmString_Destroy(&p->hPath);
    mmString_Destroy(&p->hFamily);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_Init(
    struct mmGraphicsFactory*                      p)
{
    mmRbtreeStringVpt_Init(&p->hFonts);

    p->pPackageAssets = NULL;
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_Destroy(
    struct mmGraphicsFactory*                      p)
{
    p->pPackageAssets = NULL;

    mmRbtreeStringVpt_Destroy(&p->hFonts);
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_SetPackageAssets(
    struct mmGraphicsFactory*                      p,
    struct mmPackageAssets*                        pPackageAssets)
{
    p->pPackageAssets = pPackageAssets;
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_OnFinishLaunching(
    struct mmGraphicsFactory*                      p)
{
    
}

MM_EXPORT_NWSI
void
mmGraphicsFactory_OnBeforeTerminate(
    struct mmGraphicsFactory*                      p)
{

}

MM_EXPORT_NWSI
int
mmGraphicsFactory_LoadFontFromResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath)
{
    int err = MM_UNKNOWN;
    int hAssetsType;
    const char* pRealPath;
    struct mmString hRealPath;
    
    mmString_Init(&hRealPath);
    
    hAssetsType = mmFileContext_GetFileAssetsPath(
        pFileContext,
        pFontPath,
        &hRealPath);
    
    pRealPath = mmString_CStr(&hRealPath);
    
    switch (hAssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        err = mmGraphicsFactory_LoadFontFromFolder(
            p,
            pFontFamily,
            pFontPath,
            pRealPath);
    }
    break;
    
    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        struct mmByteBuffer hBytes;
        
        mmFileContext_AcquireFileByteBuffer(
            pFileContext,
            pFontPath,
            &hBytes);
        
        err = mmGraphicsFactory_LoadFontFromSource(
            p,
            pFontFamily,
            pFontPath,
            pRealPath,
            &hBytes);
        
        mmFileContext_ReleaseFileByteBuffer(
            pFileContext,
            &hBytes);
    }
    break;

    default:
    {
        // Not support assets type.
        err = MM_FAILURE;
    }
    break;
    }
    mmString_Destroy(&hRealPath);
    return err;
}

MM_EXPORT_NWSI
int
mmGraphicsFactory_UnloadFontByResource(
    struct mmGraphicsFactory*                      p,
    struct mmFileContext*                          pFileContext,
    const char*                                    pFontFamily,
    const char*                                    pFontPath)
{
    int err = MM_UNKNOWN;
    int hAssetsType;
    
    hAssetsType = mmFileContext_GetFileAssetsType(
        pFileContext,
        pFontPath);
    
    switch (hAssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        err = mmGraphicsFactory_UnloadFontByFolder(p, pFontFamily);
    }
    break;
    
    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        err = mmGraphicsFactory_UnloadFontBySource(p, pFontFamily);
    }
    break;

    default:
    {
        // Not support assets type.
        err = MM_FAILURE;
    }
    break;
    }
    return err;
}

MM_EXPORT_NWSI
const struct mmFontResource*
mmGraphicsFactory_GetFontResourceByFamily(
    const struct mmGraphicsFactory*                p,
    const char*                                    pFontFamily)
{
    const struct mmFontResource* u = NULL;
    struct mmString hWeakFamily;
    struct mmRbtreeStringVptIterator* it;
    mmString_MakeWeaks(&hWeakFamily, pFontFamily);
    it = mmRbtreeStringVpt_GetIterator(&p->hFonts, &hWeakFamily);
    if (NULL != it)
    {
        u = (const struct mmFontResource*)it->v;
    }
    else
    {
        u = NULL;
    }
    return u;
}

