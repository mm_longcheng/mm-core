/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSecurityStore.h"

#include "core/mmAlloc.h"

#include "nwsi/mmKeyChain.h"

static const char* MM_SECURITY_PREFS_GROUP = "SecurityInfo";

MM_EXPORT_NWSI void mmSecurityStore_Init(struct mmSecurityStore* p)
{
    mmString_Init(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;

    mmString_Assigns(&p->hAppId, "");
}
MM_EXPORT_NWSI void mmSecurityStore_Destroy(struct mmSecurityStore* p)
{
    mmString_Destroy(&p->hAppId);

    // Note: The jNativePtr is control by Native Allocator. Can not at reset here.
    // p->jNativePtr = NULL;
}

MM_EXPORT_NWSI void mmSecurityStore_SetAppId(struct mmSecurityStore* p, const char* pAppId)
{
    mmString_Assigns(&p->hAppId, pAppId);
}

MM_EXPORT_NWSI void mmSecurityStore_OnFinishLaunching(struct mmSecurityStore* p)
{
    // nothing
}
MM_EXPORT_NWSI void mmSecurityStore_OnBeforeTerminate(struct mmSecurityStore* p)
{
    // nothing
}

MM_EXPORT_NWSI void mmSecurityStore_Insert(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    struct mmString hValue;
    mmString_MakeWeaks(&hValue, pSecret);
    mmKeyChain_Insert(mmString_CStr(&p->hAppId), pGroup, pField, &hValue);
}
MM_EXPORT_NWSI void mmSecurityStore_Update(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    struct mmString hValue;
    mmString_MakeWeaks(&hValue, pSecret);
    mmKeyChain_Update(mmString_CStr(&p->hAppId), pGroup, pField, &hValue);
}
MM_EXPORT_NWSI void mmSecurityStore_Select(struct mmSecurityStore* p, const char* pMember, char pSecret[64])
{
    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    struct mmString hValue;
    mmString_Init(&hValue);
    mmKeyChain_Select(mmString_CStr(&p->hAppId), pGroup, pField, &hValue);
    mmMemset(pSecret, 0, 64);
    mmMemcpy(pSecret, mmString_Data(&hValue), mmString_Size(&hValue));
    mmString_Destroy(&hValue);
}
MM_EXPORT_NWSI void mmSecurityStore_Delete(struct mmSecurityStore* p, const char* pMember)
{
    const char* pGroup = MM_SECURITY_PREFS_GROUP;
    const char* pField = pMember;
    mmKeyChain_Delete(mmString_CStr(&p->hAppId), pGroup, pField);
}
