/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextBitmap.h"

#include "core/mmAlloc.h"

MM_EXPORT_NWSI
void
mmTextBitmap_Init(
    struct mmTextBitmap*                           p)
{
    p->hBitmapW = 0;
    p->hBitmapH = 0;
    p->hBitsPerComponent = 0;
    p->hBytesPerRow = 0;

    p->hBitmapInfo = 0;

    p->hLength = 0;
    p->pBuffer = NULL;
}

MM_EXPORT_NWSI
void
mmTextBitmap_Destroy(
    struct mmTextBitmap*                           p)
{
    p->hBitmapW = 0;
    p->hBitmapH = 0;
    p->hBitsPerComponent = 0;
    p->hBytesPerRow = 0;
    
    p->hBitmapInfo = 0;
    
    p->hLength = 0;
    p->pBuffer = NULL;
}

MM_EXPORT_NWSI
void
mmTextBitmap_OnFinishLaunching(
    struct mmTextBitmap*                           p)
{
    size_t hByteNumber = p->hBitsPerComponent / 2;
    p->hLength = p->hBitmapW * p->hBitmapH * hByteNumber;
    p->pBuffer = (uint8_t*)mmMalloc(p->hLength);
}

MM_EXPORT_NWSI
void
mmTextBitmap_OnBeforeTerminate(
    struct mmTextBitmap*                           p)
{
    mmFree(p->pBuffer);
    p->pBuffer = NULL;
    p->hLength = 0;
}

MM_EXPORT_NWSI
void
mmTextBitmap_Clear(
    struct mmTextBitmap*                           p)
{
    mmMemset(p->pBuffer, 0, p->hLength);
}
