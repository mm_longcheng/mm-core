/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTextParagraph_h__
#define __mmTextParagraph_h__

#include "core/mmCore.h"

#include "math/mmAffineTransform.h"

#include "nwsi/mmTextParagraphFormat.h"
#include "nwsi/mmTextSpannable.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmTextUtf16String;
struct mmTextDrawingContext;

struct mmTextParagraph
{
    struct mmTextParagraphFormat* pParagraphFormat;
    struct mmTextSpannable* pTextSpannable;
    struct mmTextUtf16String* pText;
    struct mmTextDrawingContext* pDrawingContext;
    // (x, y, w, h)
    double hDrawingRect[4];
    // text frame origin(x, y)
    double hOrigin[2];
    
    void* pMasterFont;
    void* pAttributedStringPtr;
    void* pFramesetter;
    void* pFrame;
    void* pLineRanges;
    void* pLineOrigins;
    
    int pipeline;
};

MM_EXPORT_NWSI 
void 
mmTextParagraph_Init(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_Destroy(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetTextParagraphFormat(
    struct mmTextParagraph*                        p, 
    struct mmTextParagraphFormat*                  pParagraphFormat);

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetTextSpannable(
    struct mmTextParagraph*                        p, 
    struct mmTextSpannable*                        pTextSpannable);

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetText(
    struct mmTextParagraph*                        p, 
    struct mmTextUtf16String*                      pText);

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetDrawingContext(
    struct mmTextParagraph*                        p, 
    struct mmTextDrawingContext*                   pDrawingContext);

MM_EXPORT_NWSI 
void 
mmTextParagraph_OnFinishLaunching(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_OnBeforeTerminate(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateDrawingContext(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextImplement(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextFormat(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextLayout(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateFontFeature(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextSpannable(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_DeleteTextFormat(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_DeleteTextLayout(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI 
void 
mmTextParagraph_Draw(
    struct mmTextParagraph*                        p);

MM_EXPORT_NWSI
void
mmTextParagraph_HitTestPoint(
    const struct mmTextParagraph*                  p,
    float                                          point[2],
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics);

MM_EXPORT_NWSI
void
mmTextParagraph_HitTestTextPosition(
    const struct mmTextParagraph*                  p,
    mmUInt32_t                                     offset,
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics);

#include "core/mmSuffix.h"

#endif//__mmTextParagraph_h__
