/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmKeyChain_h__
#define __mmKeyChain_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

// Note: Info.plist add < key / value > ["AppIdentifierPrefix" -> $(AppIdentifierPrefix)]
MM_EXPORT_NWSI
void
mmKeyChain_GetAppIdentifierPrefix(
    struct mmString*                               pAppIdentifierPrefix);

MM_EXPORT_NWSI
int
mmKeyChain_SharedInsert(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue);

MM_EXPORT_NWSI
int
mmKeyChain_SharedUpdate(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue);

MM_EXPORT_NWSI
int
mmKeyChain_SharedSelect(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue);

MM_EXPORT_NWSI
int
mmKeyChain_SharedDelete(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField);

MM_EXPORT_NWSI
int
mmKeyChain_Insert(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue);

MM_EXPORT_NWSI
int
mmKeyChain_Update(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue);

MM_EXPORT_NWSI
int
mmKeyChain_Select(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue);

MM_EXPORT_NWSI
int
mmKeyChain_Delete(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField);

#include "core/mmSuffix.h"

#endif//__mmKeyChain_h__
