/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmGraphicsPlatform.h"

#include "graphics/mmColor.h"

#include "unicode/mmUString.h"
#include "unicode/mmUBidi.h"

#include "nwsi/mmTextFormat.h"

MM_EXPORT_NWSI
void
mmCFTypeRefSafeRelease(
    CFTypeRef                                      o)
{
    if(NULL != o)
    {
        CFRelease(o);
    }
}

MM_EXPORT_NWSI
CGColorRef
mmGraphicsPlatform_MakeCGColor(
    CGColorSpaceRef                                rColorSpace,
    float                                          hColor[4])
{
    // CGFloat float is different.
    CGFloat c[4] = { 0 };
    c[0] = hColor[0];
    c[1] = hColor[1];
    c[2] = hColor[2];
    c[3] = hColor[3];
    return CGColorCreate(rColorSpace, c);
}

MM_EXPORT_NWSI
void
mmGraphicsPlatform_MakeCGFloatColor(
    CGFloat                                        c[4],
    float                                          hColor[4])
{
    c[0] = hColor[0];
    c[1] = hColor[1];
    c[2] = hColor[2];
    c[3] = hColor[3];
}

MM_EXPORT_NWSI
CGLineJoin
mmGraphicsPlatform_GetLineJoin(
    int                                            hLineJoin)
{
    static const CGLineJoin gStyleTable[] =
    {
        kCGLineJoinMiter   , // 0 mmLineJoinMiter
        kCGLineJoinRound   , // 1 mmLineJoinRound
        kCGLineJoinBevel   , // 2 mmLineJoinBevel
    };
    if (0 <= hLineJoin && hLineJoin < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hLineJoin];
    }
    else
    {
        return kCGLineJoinMiter;
    }
}

MM_EXPORT_NWSI
CGLineCap
mmGraphicsPlatform_GetCapStyle(
    int                                            hLineCap)
{
    static const CGLineCap gStyleTable[] =
    {
        kCGLineCapButt     , // 0 mmLineCapButt
        kCGLineCapRound    , // 1 mmLineCapRound
        kCGLineCapSquare   , // 2 mmLineCapSquare
    };
    if (0 <= hLineCap && hLineCap < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hLineCap];
    }
    else
    {
        return kCGLineCapButt;
    }
}

MM_EXPORT_NWSI
CGFloat
mmGraphicsPlatform_GetCGFloot(
    float                                          hValue)
{
    return (hValue == FLT_MAX) ? CGFLOAT_MAX : hValue;
}

