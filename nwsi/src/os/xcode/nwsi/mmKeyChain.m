/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/


#import "mmKeyChain.h"

#import <CoreFoundation/CoreFoundation.h>
#import <Security/Security.h>
#import <Foundation/Foundation.h>

MM_EXPORT_NWSI
void
mmKeyChain_GetAppIdentifierPrefix(
    struct mmString*                               pAppIdentifierPrefix)
{
    // "7GZ827U656." Note that appIdentifierPrefix ends with a period.
    NSBundle* pBundle = [NSBundle mainBundle];
    NSString* rAppIdentifierPrefix = [pBundle objectForInfoDictionaryKey:@"AppIdentifierPrefix"];
    const char* cPrefix = (nil == rAppIdentifierPrefix) ? "" : [rAppIdentifierPrefix UTF8String];
    mmString_Assigns(pAppIdentifierPrefix, cPrefix);
}

MM_EXPORT_NWSI
int
mmKeyChain_SharedInsert(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);
    
    CFStringRef rAccess = CFStringCreateWithCString(kCFAllocatorDefault, pAccess, kCFStringEncodingUTF8);
    
    CFDataRef rValueData = CFDataCreate(kCFAllocatorDefault, (const UInt8*)mmString_Data(pValue), (CFIndex)mmString_Size(pValue));
    
    const void* rQueryKeys[] =
    {
        kSecClass,
        kSecAttrService,
        kSecAttrLabel,
        kSecAttrAccount,
        kSecAttrSynchronizable,
        kSecAttrAccessGroup,
        kSecValueData,
    };
    const void* rQueryVals[] =
    {
        kSecClassGenericPassword,
        rAppId,
        rGroup,
        rField,
        kCFBooleanFalse,
        rAccess,
        rValueData,
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    hStatus = SecItemAdd(rQuery, NULL);
    
    if (hStatus != noErr)
    {
        hCode = 1;
    }
    else
    {
        hCode = 0;
    }
    
    CFRelease(rQuery);
    
    CFRelease(rValueData);
    
    CFRelease(rAccess);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_SharedUpdate(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);

    CFStringRef rAccess = CFStringCreateWithCString(kCFAllocatorDefault, pAccess, kCFStringEncodingUTF8);
    
    CFDataRef rValueData = CFDataCreate(kCFAllocatorDefault, (const UInt8*)mmString_Data(pValue), (CFIndex)mmString_Size(pValue));
    
    const void* rQueryKeys[] =
    {
        kSecClass,
        kSecAttrService,
        kSecAttrLabel,
        kSecAttrAccount,
        kSecAttrSynchronizable,
        kSecAttrAccessGroup,
    };
    const void* rQueryVals[] =
    {
        kSecClassGenericPassword,
        rAppId,
        rGroup,
        rField,
        kCFBooleanFalse,
        rAccess,
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    const void* rValueKeys[] = { kSecValueData, };
    const void* rValueVals[] = { rValueData, };
    CFDictionaryRef rValue = CFDictionaryCreate(
        kCFAllocatorDefault,
        rValueKeys,
        rValueVals,
        sizeof(rValueKeys) / sizeof(rValueKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    hStatus = SecItemUpdate(rQuery, rValue);
    
    if (hStatus != noErr)
    {
        hCode = 1;
    }
    else
    {
        hCode = 0;
    }
    
    CFRelease(rValue);
    CFRelease(rQuery);
    
    CFRelease(rValueData);
    
    CFRelease(rAccess);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_SharedSelect(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);

    CFStringRef rAccess = CFStringCreateWithCString(kCFAllocatorDefault, pAccess, kCFStringEncodingUTF8);
    
    const void* rQueryKeys[] =
    {
        kSecClass,
        kSecAttrService,
        kSecAttrLabel,
        kSecAttrAccount,
        kSecAttrSynchronizable,
        kSecAttrAccessGroup,
        kSecMatchLimit,
        kSecReturnData,
    };
    const void* rQueryVals[] =
    {
        kSecClassGenericPassword,
        rAppId,
        rGroup,
        rField,
        kCFBooleanFalse,
        rAccess,
        kSecMatchLimitOne,
        kCFBooleanTrue,
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    CFDataRef pResultData = nil;
    
    hStatus = SecItemCopyMatching(rQuery, (CFTypeRef*)&pResultData);
    
    if (hStatus != noErr)
    {
        mmString_Clear(pValue);
        hCode = 1;
    }
    else
    {
        CFIndex hResultLength = CFDataGetLength(pResultData);
        const UInt8* pResultBuffer = CFDataGetBytePtr(pResultData);
        mmString_Assignsn(pValue, (const char*)pResultBuffer, (size_t)hResultLength);
        hCode = 0;
    }
    
    if(nil != pResultData)
    {
        CFRelease(pResultData);
        pResultData = nil;
    }
    
    CFRelease(rQuery);
    
    CFRelease(rAccess);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_SharedDelete(
    const char*                                    pAccess,
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);

    CFStringRef rAccess = CFStringCreateWithCString(kCFAllocatorDefault, pAccess, kCFStringEncodingUTF8);
    
    const void* rQueryKeys[] =
    {
        kSecClass,
        kSecAttrService,
        kSecAttrLabel,
        kSecAttrAccount,
        kSecAttrSynchronizable,
        kSecAttrAccessGroup,
        kSecReturnAttributes,
    };
    const void* rQueryVals[] =
    {
        kSecClassGenericPassword,
        rAppId,
        rGroup,
        rField,
        kCFBooleanFalse,
        rAccess,
        kCFBooleanTrue,
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    hStatus = SecItemDelete(rQuery);
    
    if (hStatus != noErr)
    {
        hCode = 1;
    }
    else
    {
        hCode = 0;
    }
    
    CFRelease(rQuery);
    
    CFRelease(rAccess);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_Insert(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);
    
    CFDataRef rValueData = CFDataCreate(kCFAllocatorDefault, (const UInt8*)mmString_Data(pValue), (CFIndex)mmString_Size(pValue));
    
    const void* rQueryKeys[] = 
    { 
        kSecClass, 
        kSecAttrService, 
        kSecAttrLabel, 
        kSecAttrAccount, 
        kSecAttrSynchronizable, 
        kSecValueData, 
    };
    const void* rQueryVals[] = 
    { 
        kSecClassGenericPassword, 
        rAppId, 
        rGroup, 
        rField, 
        kCFBooleanFalse, 
        rValueData, 
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    hStatus = SecItemAdd(rQuery, NULL);
    
    if (hStatus != noErr)
    {
        hCode = 1;
    }
    else
    {
        hCode = 0;
    }
    
    CFRelease(rQuery);
    
    CFRelease(rValueData);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_Update(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);
    
    CFDataRef rValueData = CFDataCreate(kCFAllocatorDefault, (const UInt8*)mmString_Data(pValue), (CFIndex)mmString_Size(pValue));
    
    const void* rQueryKeys[] = 
    { 
        kSecClass, 
        kSecAttrService, 
        kSecAttrLabel, 
        kSecAttrAccount, 
        kSecAttrSynchronizable, 
    };
    const void* rQueryVals[] = 
    { 
        kSecClassGenericPassword, 
        rAppId, 
        rGroup, 
        rField, 
        kCFBooleanFalse, 
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    const void* rValueKeys[] = { kSecValueData, };
    const void* rValueVals[] = { rValueData, };
    CFDictionaryRef rValue = CFDictionaryCreate(
        kCFAllocatorDefault,
        rValueKeys,
        rValueVals,
        sizeof(rValueKeys) / sizeof(rValueKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    hStatus = SecItemUpdate(rQuery, rValue);
    
    if (hStatus != noErr)
    {
        hCode = 1;
    }
    else
    {
        hCode = 0;
    }
    
    CFRelease(rValue);
    CFRelease(rQuery);
    
    CFRelease(rValueData);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_Select(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField,
    struct mmString*                               pValue)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);
    
    const void* rQueryKeys[] = 
    { 
        kSecClass, 
        kSecAttrService, 
        kSecAttrLabel, 
        kSecAttrAccount, 
        kSecAttrSynchronizable,
        kSecMatchLimit,
        kSecReturnData, 
    };
    const void* rQueryVals[] = 
    { 
        kSecClassGenericPassword, 
        rAppId, 
        rGroup, 
        rField, 
        kCFBooleanFalse,
        kSecMatchLimitOne,
        kCFBooleanTrue, 
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    CFDataRef pResultData = nil;
    
    hStatus = SecItemCopyMatching(rQuery, (CFTypeRef*)&pResultData);
    
    if (hStatus != noErr)
    {
        mmString_Clear(pValue);
        hCode = 1;
    }
    else
    {
        CFIndex hResultLength = CFDataGetLength(pResultData);
        const UInt8* pResultBuffer = CFDataGetBytePtr(pResultData);
        mmString_Assignsn(pValue, (const char*)pResultBuffer, (size_t)hResultLength);
        hCode = 0;
    }
    
    if(nil != pResultData)
    {
        CFRelease(pResultData);
        pResultData = nil;
    }
    
    CFRelease(rQuery);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}

MM_EXPORT_NWSI
int
mmKeyChain_Delete(
    const char*                                    pAppId,
    const char*                                    pGroup,
    const char*                                    pField)
{
    OSStatus hStatus = noErr;
    int hCode = -1;
    CFStringRef rAppId = CFStringCreateWithCString(kCFAllocatorDefault, pAppId, kCFStringEncodingUTF8);
    CFStringRef rGroup = CFStringCreateWithCString(kCFAllocatorDefault, pGroup, kCFStringEncodingUTF8);
    CFStringRef rField = CFStringCreateWithCString(kCFAllocatorDefault, pField, kCFStringEncodingUTF8);
    
    const void* rQueryKeys[] = 
    { 
        kSecClass, 
        kSecAttrService, 
        kSecAttrLabel, 
        kSecAttrAccount, 
        kSecAttrSynchronizable, 
        kSecReturnAttributes, 
    };
    const void* rQueryVals[] = 
    { 
        kSecClassGenericPassword, 
        rAppId, 
        rGroup, 
        rField, 
        kCFBooleanFalse, 
        kCFBooleanTrue, 
    };

    CFDictionaryRef rQuery = CFDictionaryCreate(
        kCFAllocatorDefault,
        rQueryKeys,
        rQueryVals,
        sizeof(rQueryKeys) / sizeof(rQueryKeys[0]),
        &kCFTypeDictionaryKeyCallBacks,
        &kCFTypeDictionaryValueCallBacks);
    
    hStatus = SecItemDelete(rQuery);
    
    if (hStatus != noErr)
    {
        hCode = 1;
    }
    else
    {
        hCode = 0;
    }
    
    CFRelease(rQuery);
    
    CFRelease(rField);
    CFRelease(rGroup);
    CFRelease(rAppId);
    
    return hCode;
}
