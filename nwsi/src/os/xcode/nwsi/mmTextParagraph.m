/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmTextParagraph.h"
#import "mmOSTextPlatform.h"
#import "mmOSGraphicsPlatform.h"

#import "nwsi/mmTextUtf16String.h"
#import "nwsi/mmTextDrawingContext.h"
#import "nwsi/mmTextPlatform.h"
#import "nwsi/mmGraphicsPlatform.h"
#import "nwsi/mmTextBitmap.h"
#import "nwsi/mmTextPlatform.h"
#import "nwsi/mmUTFHelper.h"

#import "core/mmLogger.h"
#import "core/mmAlloc.h"

#import "math/mmRectangle.h"
#import "math/mmVector2.h"
#import "math/mmVector4.h"

#import "graphics/mmColor.h"

#import <CoreText/CoreText.h>

static 
CFIndex 
__static_mmTextParagraph_GetLineIndexForVertical(
    const struct mmTextParagraph*                  p,
    float                                          y);

static 
CFIndex 
__static_mmTextParagraph_GetLineIndexForOffset(
    const struct mmTextParagraph*                  p,
    CFIndex                                        offset);

static
void
__static_mmTextParagraph_GetLineCaretRange(
    CTLineRef                                      rLine,
    CGFloat                                        lx,
    int                                            index[2],
    CGFloat                                        range[2]);

static 
void 
__static_mmTextParagraph_UpdatePipeline(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_UpdateLineRange(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_UpdateTransformCTM(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_UpdateTextSpanFormat(
    struct mmTextParagraph*                        p, 
    struct mmTextSpan*                             pTextSpan);

static 
void 
__static_mmTextParagraph_RmvAttributeShadow(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_AddAttributeShadow(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_RmvAttributeBackground(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_AddAttributeBackground(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_RmvAttributeFormat(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_AddAttributeFormatMaster(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_AddAttributeFormatSpannable(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_AddAttributeFormat(
    struct mmTextParagraph*                        p);

static 
void 
__static_mmTextParagraph_UpdateTextSpan(
    struct mmTextParagraph*                        p, 
    struct mmTextSpan*                             pTextSpan);

static
void
__static_mmTextParagraph_DrawCaret(
    struct mmTextParagraph*                        p,
    CGContextRef                                   rContext);

MM_EXPORT_NWSI 
void 
mmTextParagraph_Init(
    struct mmTextParagraph*                        p)
{
    p->pParagraphFormat = NULL;
    p->pTextSpannable = NULL;
    p->pText = NULL;
    p->pDrawingContext = NULL;
    mmMemset(p->hDrawingRect, 0, sizeof(p->hDrawingRect));
    mmMemset(p->hOrigin, 0, sizeof(p->hOrigin));
    
    p->pMasterFont = NULL;
    p->pAttributedStringPtr = NULL;
    p->pFramesetter = NULL;
    p->pFrame = NULL;
    p->pLineRanges = NULL;
    p->pLineOrigins = NULL;
    
    p->pipeline = 0;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_Destroy(
    struct mmTextParagraph*                        p)
{
    p->pParagraphFormat = NULL;
    p->pTextSpannable = NULL;
    p->pText = NULL;
    p->pDrawingContext = NULL;
    mmMemset(p->hDrawingRect, 0, sizeof(p->hDrawingRect));
    mmMemset(p->hOrigin, 0, sizeof(p->hOrigin));
    
    p->pMasterFont = NULL;
    p->pAttributedStringPtr = NULL;
    p->pFramesetter = NULL;
    p->pFrame = NULL;
    p->pLineRanges = NULL;
    p->pLineOrigins = NULL;
    
    p->pipeline = 0;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetTextParagraphFormat(
    struct mmTextParagraph*                        p, 
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    p->pParagraphFormat = pParagraphFormat;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetTextSpannable(
    struct mmTextParagraph*                        p, 
    struct mmTextSpannable*                        pTextSpannable)
{
    p->pTextSpannable = pTextSpannable;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetText(
    struct mmTextParagraph*                        p, 
    struct mmTextUtf16String*                      pText)
{
    p->pText = pText;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_SetDrawingContext(
    struct mmTextParagraph*                        p, 
    struct mmTextDrawingContext*                   pDrawingContext)
{
    p->pDrawingContext = pDrawingContext;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_OnFinishLaunching(
    struct mmTextParagraph*                        p)
{
    mmTextParagraph_UpdateTextImplement(p);
    mmTextParagraph_UpdateTextFormat(p);
    mmTextParagraph_UpdateFontFeature(p);
    mmTextParagraph_UpdateTextSpannable(p);
    mmTextParagraph_UpdateTextLayout(p);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_OnBeforeTerminate(
    struct mmTextParagraph*                        p)
{
    mmTextParagraph_DeleteTextLayout(p);
    mmTextParagraph_DeleteTextFormat(p);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateDrawingContext(
    struct mmTextParagraph*                        p)
{
    __static_mmTextParagraph_UpdatePipeline(p);
    __static_mmTextParagraph_UpdateLineRange(p);
    __static_mmTextParagraph_UpdateTransformCTM(p);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextImplement(
    struct mmTextParagraph*                        p)
{
    
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextFormat(
    struct mmTextParagraph*                        p)
{
    struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
    struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
    
    // o ParagraphStyle
    // o Font
    // o Foreground
    // o Background
    // o Stroke
    // o Underline
    // o StrikeThru
    // o Shadow
    
    struct mmUtf16String* pTextString = &p->pText->hString;
    size_t hTextLength = mmUtf16String_Size(pTextString);
    const mmUtf16_t* pTextBuffer = mmUtf16String_Data(pTextString);
    int hBaseDirection = mmTextPlatform_GetTextBaseDirection(pTextBuffer, hTextLength);
    // Font
    NSObject* pMasterFont = mmTextPlatform_MakeFont(pMasterFormat, pParagraphFormat->hDisplayDensity);
    // MutableParagraphStyle
    NSObject* pParagraphStyle = mmTextPlatform_MakeParagraphStyle(pParagraphFormat, hBaseDirection);
    
    NSMutableDictionary* pAttributedStringDictionary = [NSMutableDictionary dictionary];
    [pAttributedStringDictionary setObject:pParagraphStyle forKey:NSParagraphStyleAttributeName];
    [pAttributedStringDictionary setObject:pMasterFont forKey:NSFontAttributeName];
    if(0 != mmColorCompare(pMasterFormat->hForegroundColor, mmTextPlatform_ColorBlack))
    {
        NSObject* pForegroundColor = mmTextPlatform_MakeColor(pMasterFormat->hForegroundColor);
        [pAttributedStringDictionary setObject:pForegroundColor forKey:NSForegroundColorAttributeName];
    }
    if(0 != mmColorCompare(pMasterFormat->hBackgroundColor, mmTextPlatform_ColorEmpty))
    {
        NSObject* pBackgroundColor = mmTextPlatform_MakeColor(pMasterFormat->hBackgroundColor);
        [pAttributedStringDictionary setObject:pBackgroundColor forKey:NSBackgroundColorAttributeName];
    }
    if(0.0 != pMasterFormat->hStrokeWidth)
    {
        float hStrokeWidth = pMasterFormat->hStrokeWidth * pParagraphFormat->hDisplayDensity;
        NSObject* pStrokeColor = mmTextPlatform_MakeColor(pMasterFormat->hStrokeColor);
        [pAttributedStringDictionary setObject:@(hStrokeWidth) forKey:NSStrokeWidthAttributeName];
        [pAttributedStringDictionary setObject:pStrokeColor forKey:NSStrokeColorAttributeName];
    }
    if(0.0 != pMasterFormat->hUnderlineWidth)
    {
        // ios not support Underline thickness.
        NSObject* pUnderlineColor = mmTextPlatform_MakeColor(pMasterFormat->hUnderlineColor);
        [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
        [pAttributedStringDictionary setObject:pUnderlineColor forKey:NSUnderlineColorAttributeName];
    }
    if(0.0 != pMasterFormat->hStrikeThruWidth)
    {
        // // ios not support StrikeThru thickness.
        NSObject* pStrikeThruColor = mmTextPlatform_MakeColor(pMasterFormat->hStrikeThruColor);
        [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSStrikethroughStyleAttributeName];
        [pAttributedStringDictionary setObject:pStrikeThruColor forKey:NSStrikethroughColorAttributeName];
    }
    if (0.0 != pParagraphFormat->hShadowBlur)
    {
        NSObject* pShadow = mmTextPlatform_MakeShadow(pParagraphFormat);
        [pAttributedStringDictionary setObject:pShadow forKey:NSShadowAttributeName];
    }
    
    NSString* pString = [NSString alloc];
    pString = [pString initWithCharactersNoCopy:(unichar*)pTextBuffer
                                         length:(NSUInteger)hTextLength
                                   freeWhenDone:NO];
    
    NSMutableAttributedString* pAttributedString = nil;
    pAttributedString = [NSMutableAttributedString alloc];
    pAttributedString = [pAttributedString initWithString:pString
                                               attributes:pAttributedStringDictionary];
    
    p->pMasterFont = (__bridge_retained void*)pMasterFont;
    p->pAttributedStringPtr = (__bridge_retained void*)pAttributedString;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextLayout(
    struct mmTextParagraph*                        p)
{
    CGMutablePathRef rFramePath = NULL;
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        NSObject* pMasterFont = (__bridge id)p->pMasterFont;
        if(nil == pMasterFont)
        {
            mmLogger_LogT(gLogger, "%s %d pMasterFont is nil.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        if (NULL == pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is nil.", __FUNCTION__, __LINE__);
            break;
        }
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
        
        float* hLayoutRect = pParagraphFormat->hLayoutRect;
        CGFloat hRectX = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[0]);
        CGFloat hRectY = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[1]);
        CGFloat hRectW = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[2]);
        CGFloat hRectH = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[3]);
        
        float hLayoutW = pParagraphFormat->hLayoutRect[2];
        float hLayoutH = pParagraphFormat->hLayoutRect[3];

        CFMutableAttributedStringRef rMutableAttributedString = (__bridge CFMutableAttributedStringRef)(pAttributedString);
        CTFramesetterRef rFramesetter = CTFramesetterCreateWithAttributedString(rMutableAttributedString);
        
        CFRange hRange = CFRangeMake(0, pAttributedString.length);
        
        CGSize hSize;
        NSStringDrawingOptions hOptions = 0;
        // We just need LineFragmentOrigin.
        hOptions |= NSStringDrawingUsesLineFragmentOrigin;
        // hOptions |= NSStringDrawingUsesFontLeading;
        // hOptions |= NSStringDrawingUsesDeviceMetrics;
        CGRect hDrawingRect;
        
        // master font descent.
        CGFloat hDescent = -mmTextPlatform_GetFontDescent(pMasterFont);
        
        if (pParagraphFormat->hAlignBaseline)
        {
            float wsize[2];
            CGFloat hPaddingWidth;
            
            hSize = CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX);
            hDrawingRect = [pAttributedString boundingRectWithSize:hSize
                                                           options:hOptions
                                                           context:nil];
            
            // boundingRectWithSize not included italic.
            // We padding width here. Avoid characters being cropped.
            hPaddingWidth = mmTextPlatform_GetFontSuitablePaddingWidth(
                pMasterFormat->hFontSize,
                pMasterFormat->hFontStyle);
            
            wsize[0] = (float)(hDrawingRect.size.width + hPaddingWidth);
            wsize[1] = (float)(hDrawingRect.size.height);

            wsize[0] = hLayoutW <= 0.0f ? wsize[0] : hLayoutW;
            wsize[1] = hLayoutH <= 0.0f ? wsize[1] : hLayoutH;

            pParagraphFormat->hLineDescent = hDescent;
            mmVec2Assign(pParagraphFormat->hSuitableSize, wsize);
            
            mmVec2Make(pParagraphFormat->hWindowSize, wsize[0], wsize[1]);
            mmVec4Make(pParagraphFormat->hLayoutRect, 0, 0, wsize[0], wsize[1]);
            
            hRectW = (CGFloat)wsize[0];
            hRectH = (CGFloat)wsize[1];
        }
        else
        {
            float wsize[2];
            
            hSize = CGSizeMake(hRectW, hRectH);
            hDrawingRect = [pAttributedString boundingRectWithSize:hSize
                                                           options:hOptions
                                                           context:nil];
            
            mmVec2Assign(wsize, pParagraphFormat->hWindowSize);
            
            pParagraphFormat->hLineDescent = hDescent;
            mmVec2Assign(pParagraphFormat->hSuitableSize, wsize);
            
            hRectW = (CGFloat)wsize[0];
            hRectH = (CGFloat)wsize[1];
        }
        p->hDrawingRect[0] = (double)hDrawingRect.origin.x;
        p->hDrawingRect[1] = (double)hDrawingRect.origin.y;
        p->hDrawingRect[2] = (double)hDrawingRect.size.width;
        p->hDrawingRect[3] = (double)hDrawingRect.size.height;
        
        CFRange fitRange;
        CGSize constraints = CGSizeMake(hRectW, CGFLOAT_MAX);
        CGSize sSize = CTFramesetterSuggestFrameSizeWithConstraints(
            rFramesetter,
            hRange,
            NULL,
            constraints,
            &fitRange);
        
        // we must use max height make sure the CTFrameRef hava correct number of rows.
        // hRectH sometimes less than sSize.height and the hLineCount will be zero.
        CGFloat sSizeH = hRectH < sSize.height ? sSize.height : hRectH;
        
        CGMutablePathRef rFramePath = CGPathCreateMutable();
        CGRect hFramePathRect = CGRectMake(0, 0, hRectW, sSizeH);
        CGPathAddRect(rFramePath, NULL, hFramePathRect);
    
        CTFrameRef rFrame = CTFramesetterCreateFrame(rFramesetter, hRange, rFramePath, NULL);
        
        CFArrayRef rLines = CTFrameGetLines(rFrame);
        CFIndex hLineCount = CFArrayGetCount(rLines);
        
        CGPoint* pLineRanges = (CGPoint*)mmMalloc(sizeof(CGPoint) * hLineCount);
        mmMemset(pLineRanges, 0, sizeof(CGPoint) * hLineCount);
        CGPoint* pLineOrigins = (CGPoint*)mmMalloc(sizeof(CGPoint) * hLineCount);
        mmMemset(pLineOrigins, 0, sizeof(CGPoint) * hLineCount);
        
        CTFrameGetLineOrigins(rFrame, CFRangeMake(0, 0), pLineOrigins);
        
        mmCFTypeRefSafeRelease(rFramePath);
        
        p->pFramesetter = (void*)rFramesetter;
        p->pFrame = (void*)rFrame;
        p->pLineRanges = pLineRanges;
        p->pLineOrigins = pLineOrigins;

        /*- update origin position -*/
        // Note: We must use the drawing size.
        CGFloat hhDrawingRectH = (CGFloat)p->hDrawingRect[3];
        // ParagraphAlignment
        CGFloat hHalfH = (hRectH - hhDrawingRectH) * 0.5f;
        p->hOrigin[0] = hRectX;
        p->hOrigin[1] = hRectY + hHalfH * pParagraphFormat->hParagraphAlignment;
    } while(0);
    
    mmCFTypeRefSafeRelease(rFramePath);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateFontFeature(
    struct mmTextParagraph*                        p)
{
    
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_UpdateTextSpannable(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pTextSpannable)
        {
            mmLogger_LogT(gLogger, "%s %d pTextSpannable is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmListVpt* pAttributes = &p->pTextSpannable->hAttributes;

        if (0 == pAttributes->size)
        {
            break;
        }

        struct mmTextSpan* e = NULL;

        struct mmListHead* next = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        
        [pAttributedString beginEditing];
        
        next = pAttributes->l.next;
        while (next != &pAttributes->l)
        {
            curr = next;
            next = next->next;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            e = (struct mmTextSpan*)it->v;

            __static_mmTextParagraph_UpdateTextSpan(p, e);
        }
        
        [pAttributedString endEditing];
    } while (0);
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_DeleteTextFormat(
    struct mmTextParagraph*                        p)
{
    NSMutableAttributedString* pAttributedString = (__bridge_transfer id)p->pAttributedStringPtr;
    pAttributedString = nil;
    
    p->pAttributedStringPtr = NULL;
    
    NSObject* pFont = (__bridge_transfer id)p->pMasterFont;
    pFont = nil;
    
    p->pMasterFont = NULL;
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_DeleteTextLayout(
    struct mmTextParagraph*                        p)
{
    if (NULL != p->pLineOrigins)
    {
        mmFree(p->pLineOrigins);
        p->pLineOrigins = NULL;
    }
    
    if (NULL != p->pLineRanges)
    {
        mmFree(p->pLineRanges);
        p->pLineRanges = NULL;
    }
    
    if (NULL != p->pFrame)
    {
        CTFrameRef rFrame = (CTFrameRef)p->pFrame;
        mmCFTypeRefSafeRelease(rFrame);
        p->pFrame = NULL;
    }
    
    if (NULL != p->pFramesetter)
    {
        CTFramesetterRef rFramesetter = (CTFramesetterRef)p->pFramesetter;
        mmCFTypeRefSafeRelease(rFramesetter);
        p->pFramesetter = NULL;
    }
}

MM_EXPORT_NWSI 
void 
mmTextParagraph_Draw(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        if (NULL == p->pAttributedStringPtr)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedStringPtr is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pFrame)
        {
            mmLogger_LogT(gLogger, "%s %d pFrame is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CGContextRef rContext = (CGContextRef)(p->pDrawingContext->pRenderTarget);
        if (NULL == rContext)
        {
            mmLogger_LogT(gLogger, "%s %d rContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        struct mmTextBitmap* pBitmap = (struct mmTextBitmap*)(p->pDrawingContext->pBitmap);
        if (NULL == pBitmap)
        {
            mmLogger_LogT(gLogger, "%s %d pBitmap is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        float* hLayoutRect = pParagraphFormat->hLayoutRect;
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        
        CGRect hTextRect;
        CGFloat hRectX = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[0]);
        CGFloat hRectY = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[1]);
        CGFloat hRectW = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[2]);
        CGFloat hRectH = (CGFloat)mmGraphicsPlatform_GetCGFloot(hLayoutRect[3]);
        
        hTextRect = CGRectMake(hRectX, hRectY, hRectW, hRectH);
        hTextRect.origin.x += p->hOrigin[0];
        hTextRect.origin.y += p->hOrigin[1];
        
        CGFloat hWindowSizeW = (CGFloat)(pParagraphFormat->hWindowSize[0]);
        CGFloat hWindowSizeH = (CGFloat)(pParagraphFormat->hWindowSize[1]);
        CGRect hWindowRect = CGRectMake(0, 0, hWindowSizeW, hWindowSizeH);
        
        CGAffineTransform hMatrix = CGAffineTransformIdentity;
        
        void* ctx = mmOSCGContextPushContext(rContext);
        
        mmTextBitmap_Clear(pBitmap);
        
        CGContextSetTextMatrix(rContext, hMatrix);
        
        CGContextBeginTransparencyLayerWithRect(rContext, hWindowRect, NULL);
        
        if (0 == p->pipeline)
        {
            mmTextDrawingContext_SetPass(p->pDrawingContext, 1);
            [pAttributedString drawInRect:hTextRect];
        }
        else
        {
            mmTextDrawingContext_SetPass(p->pDrawingContext, 0);
            [pAttributedString beginEditing];
            __static_mmTextParagraph_RmvAttributeFormat(p);
            __static_mmTextParagraph_RmvAttributeShadow(p);
            __static_mmTextParagraph_AddAttributeBackground(p);
            [pAttributedString endEditing];
            [pAttributedString drawInRect:hTextRect];
            
            mmTextDrawingContext_SetPass(p->pDrawingContext, 1);
            [pAttributedString beginEditing];
            __static_mmTextParagraph_AddAttributeFormat(p);
            __static_mmTextParagraph_AddAttributeShadow(p);
            __static_mmTextParagraph_RmvAttributeBackground(p);
            [pAttributedString endEditing];
            [pAttributedString drawInRect:hTextRect];
            
            // Restore the complete configuration.
            [pAttributedString beginEditing];
            __static_mmTextParagraph_AddAttributeBackground(p);
            [pAttributedString endEditing];
        }
        
        __static_mmTextParagraph_DrawCaret(p, rContext);
        
        CGContextEndTransparencyLayer(rContext);
        
        mmOSCGContextPopContext(ctx);
    } while(0);
}

MM_EXPORT_NWSI
void
mmTextParagraph_HitTestPoint(
    const struct mmTextParagraph*                  p,
    float                                          point[2],
    int*                                           isTrailingHit,
    int*                                           isInside,
    struct mmTextHitMetrics*                       pMetrics)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        (*isTrailingHit) = 0;
        (*isInside) = 0;
        mmTextHitMetrics_Reset(pMetrics);
        
        if (NULL == p->pFrame)
        {
            mmLogger_LogT(gLogger, "%s %d pFrame is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CTFrameRef rFrame = (CTFrameRef)p->pFrame;
        CFArrayRef rLines = CTFrameGetLines(rFrame);
        CFIndex hLineCount = CFArrayGetCount(rLines);

        CGFloat lx = (CGFloat)point[0] - p->hOrigin[0];
        CGFloat ly = (CGFloat)point[1] - p->hOrigin[1];
        CGFloat vertical = (CGFloat)point[1];
        
        // Note: Line range included the vertical alignment transform.
        CFIndex line = __static_mmTextParagraph_GetLineIndexForVertical(p, vertical);
        if ((CFIndex)(-1) == line || line >= hLineCount)
        {
            mmLogger_LogT(gLogger, "%s %d line is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        CTLineRef rLine = (CTLineRef)CFArrayGetValueAtIndex(rLines, line);
        if (NULL == rLine)
        {
            mmLogger_LogT(gLogger, "%s %d rLine is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CGPoint* pLineOrigins = (CGPoint*)p->pLineOrigins;
        CGPoint* pLineOrigin = &pLineOrigins[line];
        
        lx = lx - pLineOrigin->x;
        
        CGPoint position = CGPointMake(lx, ly);
        CFIndex hOffsetC = CTLineGetStringIndexForPosition(rLine, position);

        CGFloat secondaryOffset = 0;
        // surrogate pairs, only lead part will return correct value.
        // trail part and at end of line, will return 0.
        CGFloat c = CTLineGetOffsetForStringIndex(rLine, hOffsetC, &secondaryOffset);

        int hOffsetL, hOffsetR;
        CGFloat l, r;
        
        int index[2];
        CGFloat range[2];
        __static_mmTextParagraph_GetLineCaretRange(rLine, lx, index, range);
        
        hOffsetL = (int)(index[0]);
        hOffsetR = (int)(index[1]);
        l = (CGFloat)range[0];
        r = (CGFloat)range[1];
        
        CGPoint* pLineRanges = (CGPoint*)p->pLineRanges;
        CGPoint* pLineRange = &pLineRanges[line];
        
        CGFloat t = pLineRange->x;
        CGFloat w = r - l;
        CGFloat h = pLineRange->y - pLineRange->x;
        
        float dx = (float)(p->hDrawingRect[0] + p->hOrigin[0]);
        float dy = (float)(p->hDrawingRect[1] + p->hOrigin[1]);
        float dw = (float)(p->hDrawingRect[2]);
        float dh = (float)(p->hDrawingRect[3]);
        
        CGPoint lpos = CGPointMake(point[0], point[1]);
        CGRect oRect = CGRectMake(l, t, w, h);
        CGRect lRect = CGRectMake(dx, dy, dw, dh);

        pMetrics->rect[0] = l;
        pMetrics->rect[1] = t;
        pMetrics->rect[2] = w;
        pMetrics->rect[3] = h;
        pMetrics->offset = hOffsetL;
        pMetrics->length = hOffsetR - hOffsetL;
        pMetrics->bidiLevel = 0;//ubidi_getLevelAt(pBiDi, (int32_t)hOffsetL);
        pMetrics->isText = CGRectContainsPoint(oRect, lpos) ? 1 : 0;
        pMetrics->isTrimmed = 0;

        (*isTrailingHit) = (c > lx) ? 1 : 0;
        (*isInside) = CGRectContainsPoint(lRect, lpos) ? 1 : 0;
    
    } while(0);
}

MM_EXPORT_NWSI
void
mmTextParagraph_HitTestTextPosition(
    const struct mmTextParagraph*                  p,
    mmUInt32_t                                     offset,
    int                                            isTrailingHit,
    float                                          point[2],
    struct mmTextHitMetrics*                       pMetrics)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        point[0] = 0;
        point[1] = 0;
        mmTextHitMetrics_Reset(pMetrics);
        
        if (NULL == p->pFrame)
        {
            mmLogger_LogT(gLogger, "%s %d pFrame is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CTFrameRef rFrame = (CTFrameRef)p->pFrame;
        CFArrayRef rLines = CTFrameGetLines(rFrame);
        CFIndex hLineCount = CFArrayGetCount(rLines);
        
        CFIndex line = __static_mmTextParagraph_GetLineIndexForOffset(p, offset);
        if ((CFIndex)(-1) == line || line >= hLineCount)
        {
            mmLogger_LogT(gLogger, "%s %d line is invalid.", __FUNCTION__, __LINE__);
            break;
        }
        CTLineRef rLine = (CTLineRef)CFArrayGetValueAtIndex(rLines, line);
        if (NULL == rLine)
        {
            mmLogger_LogT(gLogger, "%s %d rLine is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CFIndex hOffsetC = (CFIndex)offset;

        CGFloat secondaryOffset = 0;
        // surrogate pairs, only lead part will return correct value.
        // trail part and at end of line, will return 0.
        CGFloat c = CTLineGetOffsetForStringIndex(rLine, hOffsetC, &secondaryOffset);
        
        int hOffsetL, hOffsetR;
        CGFloat l, r;
        
        int index[2];
        CGFloat range[2];
        __static_mmTextParagraph_GetLineCaretRange(rLine, c, index, range);
        
        CGPoint* pLineOrigins = (CGPoint*)p->pLineOrigins;
        CGPoint* pLineOrigin = &pLineOrigins[line];
        
        hOffsetL = (int)index[0];
        hOffsetR = (int)index[1];
        l = (CGFloat)(range[0] + pLineOrigin->x);
        r = (CGFloat)(range[1]);
        
        CGPoint* pLineRanges = (CGPoint*)p->pLineRanges;
        CGPoint* pLineRange = &pLineRanges[line];
        
        CGFloat t = pLineRange->x;
        CGFloat w = r - l;
        CGFloat h = pLineRange->y - pLineRange->x;
        
        point[0] = (1 == isTrailingHit) ? r : l;
        point[1] = t;
        
        CGPoint lpos = CGPointMake(point[0], point[1]);
        CGRect oRect = CGRectMake(l, t, w, h);

        pMetrics->rect[0] = l;
        pMetrics->rect[1] = t;
        pMetrics->rect[2] = w;
        pMetrics->rect[3] = h;
        pMetrics->offset = hOffsetL;
        pMetrics->length = hOffsetR - hOffsetL;
        pMetrics->bidiLevel = 0;//ubidi_getLevelAt(pBiDi, (int32_t)hOffsetL);
        pMetrics->isText = CGRectContainsPoint(oRect, lpos) ? 1 : 0;
        pMetrics->isTrimmed = 0;
        
    } while(0);
}

static 
CFIndex 
__static_mmTextParagraph_GetLineIndexForVertical(
    const struct mmTextParagraph*                  p,
    float                                          y)
{
    CFIndex line = (CFIndex)(-1);
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pFrame)
        {
            mmLogger_LogT(gLogger, "%s %d pFrame is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pLineRanges)
        {
            mmLogger_LogT(gLogger, "%s %d pLineRanges is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CTFrameRef rFrame = (CTFrameRef)p->pFrame;
        CFArrayRef rLines = CTFrameGetLines(rFrame);
        CFIndex hLineCount = CFArrayGetCount(rLines);
        
        CGPoint* pLineRanges = (CGPoint*)p->pLineRanges;
        
        // line range is Small to large ordered interval.
        // BinarySerach for line index.
        CFIndex o = 0;
        CFIndex l = hLineCount;
        
        CFIndex mid = (CFIndex)(-1);
        CFIndex left = o;
        CFIndex right = o + l - 1;

        if (0 == l)
        {
            line = (CFIndex)(-1);
        }
        else
        {
            while (left <= right && (CFIndex)(-1) != right)
            {
                // Avoid overflow problems
                mid = left + (right - left) / 2;

                CGPoint* pLineRange = &pLineRanges[mid];
                CGFloat t = pLineRange->x;
                CGFloat b = pLineRange->y;
                
                if (y < t)
                {
                    right = mid - 1;
                }
                else if (y > b)
                {
                    left = mid + 1;
                }
                else
                {
                    //(t <= y && y <= b)
                    line = mid;
                    break;
                }
            }
        }
        
        // If not surrounded, find the closest one.
        line = ((CFIndex)(-1) == line) ? mid : line;

    } while(0);
    
    return line;
}

static 
CFIndex 
__static_mmTextParagraph_GetLineIndexForOffset(
    const struct mmTextParagraph*                  p,
    CFIndex                                        offset)
{
    CFIndex line = (CFIndex)(-1);
    
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pFrame)
        {
            mmLogger_LogT(gLogger, "%s %d pFrame is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CTFrameRef rFrame = (CTFrameRef)p->pFrame;
        CFArrayRef rLines = CTFrameGetLines(rFrame);
        CFIndex hLineCount = CFArrayGetCount(rLines);
        
        CFIndex idx = 0;
        CFIndex mid = 0;
        for (idx = 0;idx < hLineCount;++idx)
        {
            CTLineRef rLine = (CTLineRef)CFArrayGetValueAtIndex(rLines, idx);
            
            CFRange hLineRange = CTLineGetStringRange(rLine);
            
            if (hLineRange.location <= offset && offset < hLineRange.location + hLineRange.length)
            {
                line = idx;
                break;
            }
            
            mid = idx;
        }
        
        // If not surrounded, find the closest one.
        line = ((CFIndex)(-1) == line) ? mid : line;

    } while(0);
    
    return line;
}

static 
void 
__static_mmTextParagraph_GetLineCaretRange(
    CTLineRef                                      rLine,
    CGFloat                                        lx,
    int                                            index[2], 
    CGFloat                                        range[2])
{
    __block CFIndex charIndexl = 0;
    __block CFIndex charIndexr = 0;
    __block double offsetl = 0;
    __block double offsetr = 0;
    __block bool inside = NO;
    
    // The block to invoke once for each logical caret edge in the line,
    // in left-to-right visual order. The block's offset parameter is
    // relative to the line origin. The block's leadingEdge parameter
    // specifies logical order.
    CTLineEnumerateCaretOffsets(rLine, ^(double offset, CFIndex charIndex, bool leadingEdge, bool* stop)
    {
        // printf("offset: %f charIndex:%ld leadingEdge:%d\n", offset, charIndex, (int)leadingEdge);
        
        CFIndex idx = charIndex + (leadingEdge ? 0 : 1);
        if (offset > lx)
        {
            offsetr = offset;
            charIndexr = idx;
            (*stop) = YES;
            inside = YES;
        }
        else
        {
            offsetl = offset;
            charIndexl = idx;
            
            offsetr = offset;
            charIndexr = idx;
        }
    });
    
    charIndexr += (inside ? 0 : 1);
    
    index[0] = (int)charIndexl;
    index[1] = (int)charIndexr;
    range[0] = (CGFloat)offsetl;
    range[1] = (CGFloat)offsetr;
}

static 
void 
__static_mmTextParagraph_UpdatePipeline(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
        
        p->pipeline = 0;
        
        if(0 != mmColorCompare(pMasterFormat->hBackgroundColor, mmTextPlatform_ColorEmpty))
        {
            p->pipeline |= 1;
            break;
        }

        struct mmListVpt* pAttributes = &p->pTextSpannable->hAttributes;
        
        if (0 == pAttributes->size)
        {
            break;
        }

        struct mmTextSpan* e = NULL;

        struct mmListHead* next = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        next = pAttributes->l.next;
        while (next != &pAttributes->l)
        {
            curr = next;
            next = next->next;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            e = (struct mmTextSpan*)it->v;
            
            if(0 != mmColorCompare(e->hFormat.hBackgroundColor, pMasterFormat->hBackgroundColor))
            {
                p->pipeline |= 1;
                break;
            }
        }
    } while(0);
}

static 
void 
__static_mmTextParagraph_UpdateLineRange(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pFrame)
        {
            mmLogger_LogT(gLogger, "%s %d pFrame is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pLineRanges)
        {
            mmLogger_LogT(gLogger, "%s %d pLineRanges is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        CTFrameRef rFrame = (CTFrameRef)p->pFrame;
        CFArrayRef rLines = CTFrameGetLines(rFrame);
        CFIndex hLineCount = CFArrayGetCount(rLines);
        
        CGPoint* pLineRanges = (CGPoint*)p->pLineRanges;
        float hDisplayDensity = p->pParagraphFormat->hDisplayDensity;
        
        CGFloat hOffsetY = 0;
        CGFloat hOriginY = p->hOrigin[1];
        
        CFIndex idx = 0;
        for (idx = 0;idx < hLineCount;++idx)
        {
            CTLineRef rLine = (CTLineRef)CFArrayGetValueAtIndex(rLines, idx);
            
            CFRange sr = CTLineGetStringRange(rLine);
            
            CGPoint* hLineRange = &pLineRanges[idx];
            
            struct mmRange r = { sr.location, sr.length };
            float hMaxFontSize = 0.0f;
            mmTextSpannable_GetMaxFontSize(p->pTextSpannable, &r, &hMaxFontSize);
            
            CGFloat t = hOffsetY + hOriginY;
            CGFloat h = mmTextPlatform_GetDrawLineSpacing((CGFloat)(hMaxFontSize * hDisplayDensity));
            CGFloat b = t + h;
            
            hOffsetY += h;
            
            hLineRange->x = t;
            hLineRange->y = b;
        }
    } while(0);
}

static 
void 
__static_mmTextParagraph_UpdateTransformCTM(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        if (NULL == p->pDrawingContext)
        {
            mmLogger_LogT(gLogger, "%s %d pDrawingContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        CGContextRef rContext = (CGContextRef)(p->pDrawingContext->pRenderTarget);
        if (NULL == rContext)
        {
            mmLogger_LogT(gLogger, "%s %d rContext is null.", __FUNCTION__, __LINE__);
            break;
        }
        if (NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        CGFloat hWindowSizeH = (CGFloat)(p->pParagraphFormat->hWindowSize[1]);
        CGAffineTransform hTransformCTM = { 1, 0, 0, -1, 0, hWindowSizeH, };

        CGAffineTransform hCTM = CGContextGetCTM(rContext);

        if (CGAffineTransformEqualToTransform(hTransformCTM, hCTM))
        {
            // CTM Transform is same.
            break;
        }

        CGAffineTransform hRCTM = CGAffineTransformInvert(hCTM);
        CGAffineTransform hFCTM = CGAffineTransformConcat(hRCTM, hTransformCTM);
        CGContextConcatCTM(rContext, hFCTM);

    } while(0);
}

static 
void 
__static_mmTextParagraph_UpdateTextSpanFormat(
    struct mmTextParagraph*                        p, 
    struct mmTextSpan*                             pTextSpan)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        
        if(NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        // o Foreground
        // x Background
        // o Stroke
        // o Underline
        // o StrikeThru
        
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
        struct mmTextFormat* pTextFormat = &pTextSpan->hFormat;
        struct mmRange* pRange = &pTextSpan->hRange;
        
        NSMutableDictionary* pAttributedStringDictionary = [NSMutableDictionary dictionary];
        if(!mmTextFormat_IsFontEquals(pMasterFormat, pTextFormat))
        {
            NSObject* pFont = mmTextPlatform_MakeFont(pTextFormat, pParagraphFormat->hDisplayDensity);
            [pAttributedStringDictionary setObject:pFont forKey:NSFontAttributeName];
        }
        if(0 != mmColorCompare(pTextFormat->hForegroundColor, pMasterFormat->hForegroundColor))
        {
            NSObject* pForegroundColor = mmTextPlatform_MakeColor(pTextFormat->hForegroundColor);
            [pAttributedStringDictionary setObject:pForegroundColor forKey:NSForegroundColorAttributeName];
        }
        if(pTextFormat->hStrokeWidth != pMasterFormat->hStrokeWidth)
        {
            float hStrokeWidth = pTextFormat->hStrokeWidth * pParagraphFormat->hDisplayDensity;
            NSObject* pStrokeColor = mmTextPlatform_MakeColor(pTextFormat->hStrokeColor);
            [pAttributedStringDictionary setObject:@(hStrokeWidth) forKey:NSStrokeWidthAttributeName];
            [pAttributedStringDictionary setObject:pStrokeColor forKey:NSStrokeColorAttributeName];
        }
        if(pTextFormat->hUnderlineWidth != pMasterFormat->hUnderlineWidth)
        {
            NSObject* pUnderlineColor = mmTextPlatform_MakeColor(pTextFormat->hUnderlineColor);
            [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
            [pAttributedStringDictionary setObject:pUnderlineColor forKey:NSUnderlineColorAttributeName];
        }
        if(pTextFormat->hStrikeThruWidth != pMasterFormat->hStrikeThruWidth)
        {
            NSObject* pStrikeThruColor = mmTextPlatform_MakeColor(pTextFormat->hStrikeThruColor);
            [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSStrikethroughStyleAttributeName];
            [pAttributedStringDictionary setObject:pStrikeThruColor forKey:NSStrikethroughColorAttributeName];
        }
        
        if(0 < [pAttributedStringDictionary count])
        {
            NSRange hRange = NSMakeRange((NSUInteger)pRange->o, (NSUInteger)pRange->l);
            [pAttributedString addAttributes:pAttributedStringDictionary range:hRange];
        }
    } while (0);
}

static 
void 
__static_mmTextParagraph_RmvAttributeShadow(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        if(NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is NULL.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmUtf16String* pTextString = &p->pText->hString;
        size_t hTextLength = mmUtf16String_Size(pTextString);
        NSRange hRange = NSMakeRange((NSUInteger)0, (NSUInteger)hTextLength);

        [pAttributedString removeAttribute:NSShadowAttributeName range:hRange];
    } while(0);
}

static 
void 
__static_mmTextParagraph_AddAttributeShadow(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        if(NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is NULL.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        
        struct mmUtf16String* pTextString = &p->pText->hString;
        size_t hTextLength = mmUtf16String_Size(pTextString);
        NSRange hRange = NSMakeRange((NSUInteger)0, (NSUInteger)hTextLength);
        
        if (0.0 != pParagraphFormat->hShadowBlur)
        {
            NSObject* pShadow = mmTextPlatform_MakeShadow(pParagraphFormat);
            [pAttributedString addAttribute:NSShadowAttributeName value:pShadow range:hRange];
        }
    } while(0);
}

static 
void 
__static_mmTextParagraph_RmvAttributeBackground(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        if(NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is NULL.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmUtf16String* pTextString = &p->pText->hString;
        size_t hTextLength = mmUtf16String_Size(pTextString);
        NSRange hRange = NSMakeRange((NSUInteger)0, (NSUInteger)hTextLength);
        
        [pAttributedString removeAttribute:NSBackgroundColorAttributeName range:hRange];
    } while(0);
}

static 
void 
__static_mmTextParagraph_AddAttributeBackground(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        if(NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is NULL.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pTextSpannable)
        {
            mmLogger_LogT(gLogger, "%s %d pTextSpannable is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
        
        struct mmUtf16String* pTextString = &p->pText->hString;
        size_t hTextLength = mmUtf16String_Size(pTextString);
        NSRange hRange = NSMakeRange((NSUInteger)0, (NSUInteger)hTextLength);

        // applay MasterFormat.
        if(0 != mmColorCompare(pMasterFormat->hBackgroundColor, mmTextPlatform_ColorEmpty))
        {
            NSObject* pBackgroundColor = mmTextPlatform_MakeColor(pMasterFormat->hBackgroundColor);
            [pAttributedString addAttribute:NSBackgroundColorAttributeName value:pBackgroundColor range:hRange];
        }

        // applay Spannable Format.
        {
            struct mmListVpt* pAttributes = &p->pTextSpannable->hAttributes;
            
            if (0 == pAttributes->size)
            {
                break;
            }

            struct mmTextFormat* pTextFormat = NULL;
            struct mmTextSpan* e = NULL;

            struct mmListHead* next = NULL;
            struct mmListHead* curr = NULL;
            struct mmListVptIterator* it = NULL;
            next = pAttributes->l.next;
            while (next != &pAttributes->l)
            {
                curr = next;
                next = next->next;
                it = mmList_Entry(curr, struct mmListVptIterator, n);
                e = (struct mmTextSpan*)it->v;

                pTextFormat = &e->hFormat;
                if(0 != mmColorCompare(pTextFormat->hBackgroundColor, pMasterFormat->hBackgroundColor))
                {
                    NSRange hRange = NSMakeRange((NSUInteger)e->hRange.o, (NSUInteger)e->hRange.l);
                    NSObject* pBackgroundColor = mmTextPlatform_MakeColor(pTextFormat->hBackgroundColor);
                    [pAttributedString addAttribute:NSBackgroundColorAttributeName value:pBackgroundColor range:hRange];
                }
            }
        }
    } while(0);
}

static 
void 
__static_mmTextParagraph_RmvAttributeFormat(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        if(NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is NULL.", __FUNCTION__, __LINE__);
            break;
        }
        
        // o Foreground
        // x Background
        // o Stroke
        // o Underline
        // o StrikeThru
        
        struct mmUtf16String* pTextString = &p->pText->hString;
        size_t hTextLength = mmUtf16String_Size(pTextString);
        NSRange hRange = NSMakeRange((NSUInteger)0, (NSUInteger)hTextLength);
        
        // Foreground default is black, we need make for clear color.
        // Can't simply remove it
        NSObject* pCleanColor = mmTextPlatformCleanColor();
        [pAttributedString addAttribute:NSForegroundColorAttributeName value:pCleanColor range:hRange];
        
        [pAttributedString removeAttribute:NSStrokeColorAttributeName range:hRange];
        [pAttributedString removeAttribute:NSStrokeWidthAttributeName range:hRange];
        
        [pAttributedString removeAttribute:NSUnderlineStyleAttributeName range:hRange];
        [pAttributedString removeAttribute:NSUnderlineColorAttributeName range:hRange];
        
        [pAttributedString removeAttribute:NSStrikethroughStyleAttributeName range:hRange];
        [pAttributedString removeAttribute:NSStrikethroughColorAttributeName range:hRange];
    } while(0);
}

static 
void 
__static_mmTextParagraph_AddAttributeFormatMaster(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }
        if(NULL == p->pText)
        {
            mmLogger_LogT(gLogger, "%s %d pText is NULL.", __FUNCTION__, __LINE__);
            break;
        }
        
        if (NULL == p->pTextSpannable)
        {
            mmLogger_LogT(gLogger, "%s %d pTextSpannable is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
        
        struct mmUtf16String* pTextString = &p->pText->hString;
        size_t hTextLength = mmUtf16String_Size(pTextString);
        NSRange hRange = NSMakeRange((NSUInteger)0, (NSUInteger)hTextLength);

        NSMutableDictionary* pAttributedStringDictionary = [NSMutableDictionary dictionary];
        
        // o Foreground
        // x Background
        // o Stroke
        // o Underline
        // o StrikeThru

        // applay MasterFormat.
        if(0 != mmColorCompare(pMasterFormat->hForegroundColor, mmTextPlatform_ColorBlack))
        {
            NSObject* pForegroundColor = mmTextPlatform_MakeColor(pMasterFormat->hForegroundColor);
            [pAttributedStringDictionary setObject:pForegroundColor forKey:NSForegroundColorAttributeName];
        }
        if(0.0 != pMasterFormat->hStrokeWidth)
        {
            float hStrokeWidth = pMasterFormat->hStrokeWidth * pParagraphFormat->hDisplayDensity;
            NSObject* pStrokeColor = mmTextPlatform_MakeColor(pMasterFormat->hStrokeColor);
            [pAttributedStringDictionary setObject:@(hStrokeWidth) forKey:NSStrokeWidthAttributeName];
            [pAttributedStringDictionary setObject:pStrokeColor forKey:NSStrokeColorAttributeName];
        }
        if(0.0 != pMasterFormat->hUnderlineWidth)
        {
            // ios not support Underline thickness.
            NSObject* pUnderlineColor = mmTextPlatform_MakeColor(pMasterFormat->hUnderlineColor);
            [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
            [pAttributedStringDictionary setObject:pUnderlineColor forKey:NSUnderlineColorAttributeName];
        }
        if(0.0 != pMasterFormat->hStrikeThruWidth)
        {
            // ios not support StrikeThru thickness.
            NSObject* pStrikeThruColor = mmTextPlatform_MakeColor(pMasterFormat->hStrikeThruColor);
            [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSStrikethroughStyleAttributeName];
            [pAttributedStringDictionary setObject:pStrikeThruColor forKey:NSStrikethroughColorAttributeName];
        }
        
        [pAttributedString addAttributes:pAttributedStringDictionary range:hRange];
    } while(0);
}

static 
void 
__static_mmTextParagraph_AddAttributeFormatSpannable(
    struct mmTextParagraph*                        p)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if (NULL == p->pTextSpannable)
        {
            mmLogger_LogT(gLogger, "%s %d pTextSpannable is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        // o Foreground
        // x Background
        // o Stroke
        // o Underline
        // o StrikeThru
        
        // applay Spannable Format.
        struct mmListVpt* pAttributes = &p->pTextSpannable->hAttributes;
        
        if (0 == pAttributes->size)
        {
            break;
        }

        struct mmTextSpan* e = NULL;

        struct mmListHead* next = NULL;
        struct mmListHead* curr = NULL;
        struct mmListVptIterator* it = NULL;
        next = pAttributes->l.next;
        while (next != &pAttributes->l)
        {
            curr = next;
            next = next->next;
            it = mmList_Entry(curr, struct mmListVptIterator, n);
            e = (struct mmTextSpan*)it->v;

            __static_mmTextParagraph_UpdateTextSpanFormat(p, e);
        }
    } while(0);
}

static 
void 
__static_mmTextParagraph_AddAttributeFormat(
    struct mmTextParagraph*                        p)
{
    __static_mmTextParagraph_AddAttributeFormatMaster(p);
    __static_mmTextParagraph_AddAttributeFormatSpannable(p);
}

static 
void 
__static_mmTextParagraph_UpdateTextSpan(
    struct mmTextParagraph*                        p, 
    struct mmTextSpan*                             pTextSpan)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        NSMutableAttributedString* pAttributedString = (__bridge id)p->pAttributedStringPtr;
        if(nil == pAttributedString)
        {
            mmLogger_LogT(gLogger, "%s %d pAttributedString is nil.", __FUNCTION__, __LINE__);
            break;
        }

        if(NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        struct mmTextFormat* pMasterFormat = &pParagraphFormat->hMasterFormat;
        struct mmTextFormat* pTextFormat = &pTextSpan->hFormat;
        struct mmRange* pRange = &pTextSpan->hRange;
        
        // o Font
        // o Foreground
        // o Background
        // o Stroke
        // o Underline
        // o StrikeThru

        NSMutableDictionary* pAttributedStringDictionary = [NSMutableDictionary dictionary];
        if(!mmTextFormat_IsFontEquals(pMasterFormat, pTextFormat))
        {
            NSObject* pFont = mmTextPlatform_MakeFont(pTextFormat, pParagraphFormat->hDisplayDensity);
            [pAttributedStringDictionary setObject:pFont forKey:NSFontAttributeName];
        }
        if(0 != mmColorCompare(pTextFormat->hForegroundColor, pMasterFormat->hForegroundColor))
        {
            NSObject* pForegroundColor = mmTextPlatform_MakeColor(pTextFormat->hForegroundColor);
            [pAttributedStringDictionary setObject:pForegroundColor forKey:NSForegroundColorAttributeName];
        }
        if(0 != mmColorCompare(pTextFormat->hBackgroundColor, pMasterFormat->hBackgroundColor))
        {
            NSObject* pBackgroundColor = mmTextPlatform_MakeColor(pTextFormat->hBackgroundColor);
            [pAttributedStringDictionary setObject:pBackgroundColor forKey:NSBackgroundColorAttributeName];
        }
        if(pTextFormat->hStrokeWidth != pMasterFormat->hStrokeWidth)
        {
            float hStrokeWidth = pTextFormat->hStrokeWidth * pParagraphFormat->hDisplayDensity;
            NSObject* pStrokeColor = mmTextPlatform_MakeColor(pTextFormat->hStrokeColor);
            [pAttributedStringDictionary setObject:@(hStrokeWidth) forKey:NSStrokeWidthAttributeName];
            [pAttributedStringDictionary setObject:pStrokeColor forKey:NSStrokeColorAttributeName];
        }
        if(pTextFormat->hUnderlineWidth != pMasterFormat->hUnderlineWidth)
        {
            NSObject* pUnderlineColor = mmTextPlatform_MakeColor(pTextFormat->hUnderlineColor);
            [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSUnderlineStyleAttributeName];
            [pAttributedStringDictionary setObject:pUnderlineColor forKey:NSUnderlineColorAttributeName];
        }
        if(pTextFormat->hStrikeThruWidth != pMasterFormat->hStrikeThruWidth)
        {
            NSObject* pStrikeThruColor = mmTextPlatform_MakeColor(pTextFormat->hStrikeThruColor);
            [pAttributedStringDictionary setObject:@(NSUnderlineStyleSingle) forKey:NSStrikethroughStyleAttributeName];
            [pAttributedStringDictionary setObject:pStrikeThruColor forKey:NSStrikethroughColorAttributeName];
        }

        if(0 < [pAttributedStringDictionary count])
        {
            NSRange hRange = NSMakeRange((NSUInteger)pRange->o, (NSUInteger)pRange->l);
            [pAttributedString addAttributes:pAttributedStringDictionary range:hRange];
        }
    } while (0);
}

static
void
__static_mmTextParagraph_DrawCaret(
    struct mmTextParagraph*                        p,
    CGContextRef                                   rContext)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        
        if(NULL == p->pParagraphFormat)
        {
            mmLogger_LogT(gLogger, "%s %d pParagraphFormat is null.", __FUNCTION__, __LINE__);
            break;
        }

        struct mmTextParagraphFormat* pParagraphFormat = p->pParagraphFormat;
        
        if (0 == pParagraphFormat->hCaretStatus)
        {
            break;
        }
        
        CGRect rect;
        CGFloat hCaretColor[4];
        float point[2];
        struct mmTextHitMetrics metrics;
        int isTrailingHit = pParagraphFormat->hCaretWrapping;
        mmUInt32_t offset = (mmUInt32_t)pParagraphFormat->hCaretIndex - isTrailingHit;
        float hDisplayDensity = pParagraphFormat->hDisplayDensity;
		float hCaretW = pParagraphFormat->hCaretWidth * hDisplayDensity;
		float hCaretHalfW = hCaretW / 2.0f;

        // Normalise to front index.
        const struct mmUtf16String* pString = &p->pText->hString;
        const mmUInt16_t* ss = mmUtf16String_Data(pString);
        size_t sz = mmUtf16String_Size(pString);
        offset = (mmUInt32_t)mmUTF_GetUtf16OffsetLNormalise(ss, sz, offset);
        
        mmTextParagraph_HitTestTextPosition(p, offset, isTrailingHit, point, &metrics);
        
        rect.origin.x = point[0] - hCaretHalfW;
        rect.origin.y = point[1];
        rect.size.width = hCaretW;
        rect.size.height = metrics.rect[3];
        
        mmGraphicsPlatform_MakeCGFloatColor(hCaretColor, pParagraphFormat->hCaretColor);
        
        CGContextSaveGState(rContext);
        
        CGContextSetFillColor(rContext, hCaretColor);
        CGContextFillRect(rContext, rect);
        
        CGContextRestoreGState(rContext);
    } while(0);
}
