/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSecurityStore_h__
#define __mmSecurityStore_h__

#include "core/mmCore.h"
#include "core/mmString.h"

#include "nwsi/mmNwsiExport.h"

#include "core/mmPrefix.h"

struct mmSecurityStore
{
    struct mmString hAppId;          /* App Identifier                  */
};

MM_EXPORT_NWSI void mmSecurityStore_Init(struct mmSecurityStore* p);
MM_EXPORT_NWSI void mmSecurityStore_Destroy(struct mmSecurityStore* p);

MM_EXPORT_NWSI void mmSecurityStore_SetAppId(struct mmSecurityStore* p, const char* pAppId);

MM_EXPORT_NWSI void mmSecurityStore_OnFinishLaunching(struct mmSecurityStore* p);
MM_EXPORT_NWSI void mmSecurityStore_OnBeforeTerminate(struct mmSecurityStore* p);

MM_EXPORT_NWSI void mmSecurityStore_Insert(struct mmSecurityStore* p, const char* pMember, char pSecret[64]);
MM_EXPORT_NWSI void mmSecurityStore_Update(struct mmSecurityStore* p, const char* pMember, char pSecret[64]);
MM_EXPORT_NWSI void mmSecurityStore_Select(struct mmSecurityStore* p, const char* pMember, char pSecret[64]);
MM_EXPORT_NWSI void mmSecurityStore_Delete(struct mmSecurityStore* p, const char* pMember);

#include "core/mmSuffix.h"

#endif//__mmSecurityStore_h__
