/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTextPlatform.h"

#include "graphics/mmColor.h"

#include "unicode/mmUString.h"
#include "unicode/mmUBidi.h"

#include "nwsi/mmTextFormat.h"

static const CGFloat gCoreTextFontWeightTable[9] =
{
   -0.8,    //    UIFontWeightUltraLight    -0.800000
   -0.6,    //    UIFontWeightThin          -0.600000
   -0.4,    //    UIFontWeightLight         -0.400000
    0.0,    //    UIFontWeightRegular        0.000000
    0.23,   //    UIFontWeightMedium         0.230000
    0.3,    //    UIFontWeightSemibold       0.300000
    0.4,    //    UIFontWeightBold           0.400000
    0.56,   //    UIFontWeightHeavy          0.560000
    0.62,   //    UIFontWeightBlack          0.620000
};

MM_EXPORT_NWSI const float mmTextPlatform_ColorEmpty[4] = { 0.0f, 0.0f, 0.0f, 0.0f, };
MM_EXPORT_NWSI const float mmTextPlatform_ColorBlack[4] = { 0.0f, 0.0f, 0.0f, 1.0f, };

MM_EXPORT_NWSI
CGFloat
mmTextPlatform_GetFontWeight(
    int                                            hWeight)
{
    CGFloat w = 0.0;
    int i = hWeight / 100;
    if(i < 1)
    {
        // (-inf, 1)
        w = -0.8;
    }
    else if(1 <= i && i <= 9)
    {
        // [1, 9]
        w = gCoreTextFontWeightTable[i - 1];
    }
    else
    {
        // (9, +inf)
        w = 0.62;
    }
    return w;
}

MM_EXPORT_NWSI
const char*
mmTextPlatform_GetFontStyle(
    int                                            hStyle)
{
    static const char* gStyleTable[] =
    {
        "normal"    , // 0 mmFontStyleNormal
        "italic"    , // 1 mmFontStyleItalic
        "oblique"   , // 2 mmFontStyleOblique
    };
    if (0 <= hStyle && hStyle < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hStyle];
    }
    else
    {
        return gStyleTable[0];
    }
}

MM_EXPORT_NWSI
CGFloat
mmTextPlatform_GetFontSize(
    float                                          hSize)
{
    return (CGFloat)hSize;
}

MM_EXPORT_NWSI
int
mmTextPlatform_GetTextBaseDirection(
    const mmUInt16_t*                              pWText,
    size_t                                         hLength)
{
    return mmUBidi_GetBaseDirection((const UChar*)pWText, (int32_t)hLength);
}

MM_EXPORT_NWSI
CGFloat
mmTextPlatform_GetFontObliqueSkewX(
    int                                            hFontStyle)
{
    return (mmFontStyleOblique == hFontStyle) ? 0.25 : 0.0;
}

MM_EXPORT_NWSI
CGFloat
mmTextPlatform_GetFontSuitablePaddingWidth(
    float                                          hFontSize,
    int                                            hFontStyle)
{
    return (mmFontStyleNormal != hFontStyle) ? hFontSize * 0.25 : 0.0;
}
