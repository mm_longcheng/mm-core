/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmOSTextPlatform.h"

#import "graphics/mmColor.h"

#import "unicode/mmUString.h"
#import "unicode/mmUBidi.h"

#import "nwsi/mmTextFormat.h"
#import "nwsi/mmGraphicsPlatform.h"
#import "nwsi/mmTextPlatform.h"

MM_EXPORT_NWSI 
UIColor* 
mmTextPlatformCleanColor(void)
{
    return UIColor.clearColor;
}

MM_EXPORT_NWSI 
NSTextAlignment 
mmTextPlatform_GetTextAlignment(
    int                                            hTextAlignment, 
    int                                            hBaseDirection)
{
    static const NSTextAlignment gAlignmentTable[] =
    {
        NSTextAlignmentLeft         , // 0 mmTextAlignmentLeft
        NSTextAlignmentCenter       , // 1 mmTextAlignmentCenter
        NSTextAlignmentRight        , // 2 mmTextAlignmentRight
        NSTextAlignmentJustified    , // 3 mmTextAlignmentJustified
        NSTextAlignmentNatural      , // 4 mmTextAlignmentNatural
    };
    if (0 <= hTextAlignment && hTextAlignment < MM_ARRAY_SIZE(gAlignmentTable))
    {
        return gAlignmentTable[hTextAlignment];
    }
    else
    {
        return NSTextAlignmentNatural;
    }
}

MM_EXPORT_NWSI 
int 
mmTextPlatform_GetTextParagraphAlignment(
    int                                            hTextVAlignment)
{
    return hTextVAlignment;
}

MM_EXPORT_NWSI 
NSLineBreakMode 
mmTextPlatform_GetTextWordWrapping(
    int                                            hLineBreakMode)
{
    static const NSLineBreakMode gStyleTable[] =
    {
        NSLineBreakByWordWrapping           , // 0 mmTextLineBreakByWordWrapping
        NSLineBreakByCharWrapping           , // 1 mmTextLineBreakByCharWrapping
        NSLineBreakByClipping               , // 2 mmTextLineBreakByClipping
        NSLineBreakByTruncatingHead         , // 3 mmTextLineBreakByTruncatingHead
        NSLineBreakByTruncatingTail         , // 4 mmTextLineBreakByTruncatingTail
        NSLineBreakByTruncatingMiddle       , // 5 mmTextLineBreakByTruncatingMiddle
    };
    if (0 <= hLineBreakMode && hLineBreakMode < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hLineBreakMode];
    }
    else
    {
        return NSLineBreakByWordWrapping;
    }
}

MM_EXPORT_NWSI 
NSWritingDirection 
mmTextPlatform_GetWritingDirection(
    int                                            hWritingDirection)
{
    static const NSWritingDirection gStyleTable[] =
    {
        NSWritingDirectionLeftToRight       , // 0 mmTextWritingDirectionLeftToRight
        NSWritingDirectionRightToLeft       , // 1 mmTextWritingDirectionRightToLeft
    };
    if (0 <= hWritingDirection && hWritingDirection < MM_ARRAY_SIZE(gStyleTable))
    {
        return gStyleTable[hWritingDirection];
    }
    else
    {
        return NSWritingDirectionNatural;
    }
}

MM_EXPORT_NWSI
uint32_t
mmTextPlatform_GetFontSymbolic(
    int                                            hFontStyle)
{
    return (mmFontStyleItalic == hFontStyle) ? UIFontDescriptorTraitItalic : 0;
}

MM_EXPORT_NWSI 
UIFont* 
mmTextPlatform_MakeFont(
    struct mmTextFormat*                           pTextFormat, 
    float                                          hDisplayDensity)
{
    UIFont* pFont = nil;
    
    do
    {
        // FontFamily
        size_t hULength = mmUtf16String_Size(&pTextFormat->hFontFamilyName);
        const unichar* hUBuffer = (const unichar*)mmUtf16String_CStr(&pTextFormat->hFontFamilyName);
        NSString* pFontFamily = [NSString stringWithCharacters:hUBuffer length:(NSUInteger)hULength];
        // FontWeight
        CGFloat hFontWeight = mmTextPlatform_GetFontWeight(pTextFormat->hFontWeight);
        uint32_t hFontSymbolic = mmTextPlatform_GetFontSymbolic(pTextFormat->hFontStyle);
        NSMutableDictionary* pFontTraitsDictionary = [NSMutableDictionary dictionary];
        [pFontTraitsDictionary setObject:@(hFontWeight) forKey:UIFontWeightTrait];
        [pFontTraitsDictionary setObject:@(hFontSymbolic) forKey:UIFontSymbolicTrait];
        // FontSize
        CGFloat hFontSize = mmTextPlatform_GetFontSize(pTextFormat->hFontSize);
        CGFloat hFontPixelSize = hFontSize * hDisplayDensity;
        // FontStyle
        CGFloat hObliqueSkewX = mmTextPlatform_GetFontObliqueSkewX(pTextFormat->hFontStyle);
        CGAffineTransform hFontTransform = CGAffineTransformMake(1, 0, hObliqueSkewX, 1, 0, 0);
        NSValue* pTransformValue = [NSValue value:&hFontTransform withObjCType:@encode(CGAffineTransform)];
        // FontDescriptor
        NSMutableDictionary* pDictionary = [NSMutableDictionary dictionary];
        [pDictionary setObject:pFontFamily forKey:UIFontDescriptorFamilyAttribute];
        [pDictionary setObject:pFontTraitsDictionary forKey:UIFontDescriptorTraitsAttribute];
        [pDictionary setObject:pTransformValue forKey:UIFontDescriptorMatrixAttribute];
        [pDictionary setObject:@(hFontPixelSize) forKey:UIFontDescriptorSizeAttribute];
        UIFontDescriptor* pDescriptor = [UIFontDescriptor fontDescriptorWithFontAttributes:pDictionary];
        // Font
        pFont = [UIFont fontWithDescriptor:pDescriptor size:hFontPixelSize];
        if (nil != pFont)
        {
            break;
        }
        pFont = [UIFont systemFontOfSize:hFontPixelSize weight:(UIFontWeight)hFontWeight];
        if (nil != pFont)
        {
            break;
        }
        pFont = [UIFont systemFontOfSize:hFontPixelSize];
    } while(0);
    
    return pFont;
}


MM_EXPORT_NWSI 
UIColor* 
mmTextPlatform_MakeColor(
    float                                          hColor[4])
{
    UIColor* pColor = [UIColor colorWithRed:(CGFloat)hColor[0]
                                      green:(CGFloat)hColor[1]
                                       blue:(CGFloat)hColor[2]
                                      alpha:(CGFloat)hColor[3]];
    return pColor;
}

MM_EXPORT_NWSI 
NSShadow* 
mmTextPlatform_MakeShadow(
    struct mmTextParagraphFormat*                  pParagraphFormat)
{
    float hDisplayDensity = pParagraphFormat->hDisplayDensity;
    float hOffsetX = pParagraphFormat->hShadowOffset[0] * hDisplayDensity;
    float hOffsetY = pParagraphFormat->hShadowOffset[1] * hDisplayDensity;
    CGSize hShadowOffset = CGSizeMake(hOffsetX, -hOffsetY);
    NSObject* pShadowColor = mmTextPlatform_MakeColor(pParagraphFormat->hShadowColor);
    NSShadow* pShadow = [[NSShadow alloc] init];
    pShadow.shadowBlurRadius = pParagraphFormat->hShadowBlur * hDisplayDensity;
    pShadow.shadowOffset = hShadowOffset;
    pShadow.shadowColor = (UIColor*)pShadowColor;
    return pShadow;
}

MM_EXPORT_NWSI
NSMutableParagraphStyle*
mmTextPlatform_MakeParagraphStyle(
    struct mmTextParagraphFormat*                  pParagraphFormat,
    int                                            hBaseDirection)
{
    float hDisplayDensity = pParagraphFormat->hDisplayDensity;
    NSMutableParagraphStyle* pStyle = [[NSMutableParagraphStyle alloc] init];
    pStyle.alignment = mmTextPlatform_GetTextAlignment(pParagraphFormat->hTextAlignment, hBaseDirection);
    pStyle.lineBreakMode = mmTextPlatform_GetTextWordWrapping(pParagraphFormat->hLineBreakMode);
    pStyle.baseWritingDirection = mmTextPlatform_GetWritingDirection(pParagraphFormat->hWritingDirection);
    pStyle.firstLineHeadIndent = pParagraphFormat->hFirstLineHeadIndent;
    pStyle.headIndent = pParagraphFormat->hHeadIndent * hDisplayDensity;
    pStyle.tailIndent = pParagraphFormat->hTailIndent * hDisplayDensity;
    pStyle.lineSpacing = pParagraphFormat->hLineSpacingAddition * hDisplayDensity;
    pStyle.lineHeightMultiple = pParagraphFormat->hLineSpacingMultiple;
    return pStyle;
}

MM_EXPORT_NWSI
CGFloat
mmTextPlatform_GetDrawLineSpacing(
    CGFloat                                        hFontSize)
{
    // iOS DrawLineSpacing = round(1.15 * hFontSize) + 0;
    return round(1.15 * hFontSize);
}

MM_EXPORT_NWSI
CGFloat
mmTextPlatform_GetFontDescent(
    NSObject*                                      pFontObject)
{
    UIFont* pFont = (UIFont*)pFontObject;
    return (nil == pFont) ? 0 : pFont.descender;
}
