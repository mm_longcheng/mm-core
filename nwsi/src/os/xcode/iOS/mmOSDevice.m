/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmOSDevice.h"

#import <UIKit/UIDevice.h>
#import <Foundation/NSString.h>

#import <sys/utsname.h>

MM_EXPORT_NWSI void mmOSDeviceInformation(struct mmDevice* p)
{
    struct utsname systemInfo;
    
    UIDevice* _CurrentDevice = [UIDevice currentDevice];
    
    // e.g. "My iPhone"
    NSString* name = [_CurrentDevice name];
    // e.g. @"iPhone", @"iPod touch"
    NSString* model = [_CurrentDevice model];
    // localized version of model @"iPhone"
    NSString* localizedModel = [_CurrentDevice localizedModel];
    // e.g. @"iOS"
    NSString* systemName = [_CurrentDevice systemName];
    // e.g. @"4.0"
    NSString* systemVersion = [_CurrentDevice systemVersion];
    
    // uname.
    uname(&systemInfo);
    
    mmString_Assigns(&p->hName, [name UTF8String]);
    mmString_Assigns(&p->hModel, [model UTF8String]);
    mmString_Assigns(&p->hLocalizedModel, [localizedModel UTF8String]);
    mmString_Assigns(&p->hSystemName, [systemName UTF8String]);
    mmString_Assigns(&p->hSystemVersion, [systemVersion UTF8String]);
    
    mmString_Assigns(&p->hSysName, systemInfo.sysname);
    mmString_Assigns(&p->hNodeName, systemInfo.nodename);
    mmString_Assigns(&p->hRelease, systemInfo.release);
    mmString_Assigns(&p->hVersion, systemInfo.version);
    mmString_Assigns(&p->hMachine, systemInfo.machine);
}


