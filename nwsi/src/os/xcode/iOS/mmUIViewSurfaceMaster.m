/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmUIViewSurfaceMaster.h"
#import "mmUITextInput.h"

#import "nwsi/mmObjcARC.h"

#import "nwsi/mmISurface.h"
#import "nwsi/mmContextMaster.h"
#import "nwsi/mmSurfaceMaster.h"

#import "core/mmTime.h"
#import "core/mmTimeval.h"
#import "core/mmThread.h"
#import "core/mmLogger.h"
#import "core/mmKeyCode.h"
#import "core/mmASCIICode.h"
#import "core/mmAlloc.h"
#import "core/mmUtf16String.h"

#import <QuartzCore/CAMetalLayer.h>

@interface mmUIViewSurfaceMaster()
{
    struct mmEventSurfaceContent hSurfaceContent;
    struct mmISurfaceKeypad hISurfaceKeypad;
    
    // weak ref.
    struct mmSurfaceMaster* pSurfaceMaster;
    
    // weak ref.
    struct mmISurface* pISurface;
    
    struct mmUtf16String hUTF16String;
    
    // Text edit rect (x, y, w, h).
    double hTextEditRect[4];
    
    // cache value.
    CGSize hViewSize;
    CGRect hSafeRect;
    
    mmUInt32_t hModifierMask;
    mmUInt32_t hButtonMask;

    double hDisplayDensity;
}

@property (strong, nonatomic) CADisplayLink* displayLink;

@property (strong, nonatomic) NSString* markedText;
@property (strong, nonatomic) mmUITextRange* markedRange;
@property (strong, nonatomic) NSDictionary<NSAttributedStringKey, id>* markedStyles;

@property (strong, nonatomic) NSDictionary* selectorToIndex;

@end

@implementation mmUIViewSurfaceMaster

static
void
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster);

static void __static_mmUIViewSurfaceMaster_Init(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_Destroy(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadEnter(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadLeave(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_OnSizeChange(mmUIViewSurfaceMaster* p, CGFloat fw, CGFloat fh);

static void __static_mmUIViewSurfaceMaster_AttachNotification(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_DetachNotification(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pSuper, int hType);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pSuper, int hAppearance);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pSuper, int hReturnKey);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pSuper);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pSuper);

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Initialization code.
    }
    return self;
}
- (void)dealloc
{
    // Destroy code.
    
    // when Application Terminate, the dealloc may not trigger.
    // we Terminate at ApplicationWillTerminate event.
    // explicit external call.
    mmObjc_Dealloc(super);
}

-(id)initWithCoder:(NSCoder*)pCoder
{
    self = [super initWithCoder:pCoder];
    if (self)
    {
        // Initialization code.
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    struct mmLogger* gLogger = mmLogger_Instance();
    // Update code.
    
    CGSize hViewSize = self.bounds.size;
    CGRect hSafeRect = self.safeAreaLayoutGuide.layoutFrame;
    
    unsigned int iw = (unsigned int)hViewSize.width;
    unsigned int ih = (unsigned int)hViewSize.height;
    
    // ios Soft keyboard input method, will trigger layoutSubviews every chinese text.
    if (!CGRectEqualToRect(self->hSafeRect, hSafeRect) ||
        !CGSizeEqualToSize(self->hViewSize, hViewSize))
    {
        CGFloat fw = hViewSize.width;
        CGFloat fh = hViewSize.height;
        
        // We can only modify the width and height, not the position,
        // otherwise the input method soft keyboard animation will be incorrect.
        CGRect hMetalFrame = self.frame;
        self.frame = CGRectMake(hMetalFrame.origin.x, hMetalFrame.origin.y, fw, fh);
        
        __static_mmUIViewSurfaceMaster_OnSizeChange(self, fw, fh);

        self->hSafeRect = hSafeRect;
        self->hViewSize = hViewSize;
        
        mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.layoutSubviews %ux%u.", iw, ih);
    }
    else
    {
        mmLogger_LogV(gLogger, "mmUIViewSurfaceMaster.layoutSubviews %ux%u.", iw, ih);
    }
}

- (void)Init
{
    __static_mmUIViewSurfaceMaster_Init(self);
}

- (void)Destroy
{
    __static_mmUIViewSurfaceMaster_Destroy(self);
}

- (void)SetSurfaceMaster:(struct mmSurfaceMaster*)pSurfaceMaster
{
    void* pViewSurfaceMaster = (__bridge void*)self;
    
    self->pSurfaceMaster = pSurfaceMaster;
    self->pISurface = &self->pSurfaceMaster->hSuper;
    
    mmSurfaceMaster_SetViewSurface(pSurfaceMaster, pViewSurfaceMaster);
    mmSurfaceMaster_SetISurfaceKeypad(pSurfaceMaster, &self->hISurfaceKeypad);
}

- (void*)GetSurfaceCALayer
{
    return (__bridge void*)self.layer;
}

- (void)OnFinishLaunching
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    mmMemset(self->hTextEditRect, 0, sizeof(double) * 4);
    
    self->hViewSize = CGSizeZero;
    self->hSafeRect = CGRectZero;
    
    self.markedRange = [[mmUITextRange alloc] init];
    
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    
    UIScreen* pUIScreen = [UIScreen mainScreen];
    CGFloat hScreenScale = [pUIScreen scale];
    [self setContentScaleFactor:hScreenScale];
    self->hDisplayDensity = self.contentScaleFactor;
    
    // The MultipleTouch.
    [self setMultipleTouchEnabled:YES];
    
    [self setKeyboardType:UIKeyboardTypeDefault];
    [self setKeyboardAppearance:UIKeyboardAppearanceDefault];
    [self setReturnKeyType:UIReturnKeyDefault];
    
    //   0     1     2        3
    // [copy, cut, paste, selectAll]
    self.selectorToIndex = @
    {
        NSStringFromSelector(@selector(copy:))     : [NSNumber numberWithInt:0],
        NSStringFromSelector(@selector(cut:))      : [NSNumber numberWithInt:1],
        NSStringFromSelector(@selector(paste:))    : [NSNumber numberWithInt:2],
        NSStringFromSelector(@selector(selectAll:)): [NSNumber numberWithInt:3],
    };
    
    // High priority // (default = NSDefaultRunLoopMode)
    [self setDisplayLink:[CADisplayLink displayLinkWithTarget:self selector:@selector(OnDisplayLinkUpdate:)]];
    [self.displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    
    self->hISurfaceKeypad.OnSetKeypadType = &__static_mmUIViewSurfaceMaster_OnSetKeypadType;
    self->hISurfaceKeypad.OnSetKeypadAppearance = &__static_mmUIViewSurfaceMaster_OnSetKeypadAppearance;
    self->hISurfaceKeypad.OnSetKeypadReturnKey = &__static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey;
    self->hISurfaceKeypad.OnKeypadStatusShow = &__static_mmUIViewSurfaceMaster_OnKeypadStatusShow;
    self->hISurfaceKeypad.OnKeypadStatusHide = &__static_mmUIViewSurfaceMaster_OnKeypadStatusHide;
    
    __static_mmUIViewSurfaceMaster_AttachNotification(self);
    
    __static_mmUIViewSurfaceMaster_ThreadEnter(self);
    
    __static_mmUIViewSurfaceMaster_PrepareSwapchain(self);
    
    (*(self->pISurface->OnFinishLaunching))(self->pISurface);
    
    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnFinishLaunching %ux%u.", (unsigned int)w, (unsigned int)h);
}
- (void)OnBeforeTerminate
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    (*(self->pISurface->OnBeforeTerminate))(self->pISurface);
    
    __static_mmUIViewSurfaceMaster_DiscardSwapchain(self);
    
    __static_mmUIViewSurfaceMaster_ThreadLeave(self);
    
    __static_mmUIViewSurfaceMaster_DetachNotification(self);
    
    mmISurfaceKeypad_Reset(&self->hISurfaceKeypad);
    
    [self.displayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
    [self.displayLink invalidate];
    mmObjc_Dealloc(self.displayLink);
    self.displayLink = nil;
    
    mmObjc_Dealloc(self.selectorToIndex);
    self.selectorToIndex = nil;
    
    mmObjc_Dealloc(self.markedRange);
    self.markedRange = nil;
    
    self->hSafeRect = CGRectZero;
    self->hViewSize = CGSizeZero;
    
    mmMemset(self->hTextEditRect, 0, sizeof(double) * 4);
    
    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnBeforeTerminate.");
}

- (void)OnEnterBackground
{
    (*(self->pISurface->OnEnterBackground))(self->pISurface);
}
- (void)OnEnterForeground
{
    (*(self->pISurface->OnEnterForeground))(self->pISurface);
}

- (void)OnDisplayLinkUpdate:(CADisplayLink*)displayLink
{
    // Update.
    (*(self->pISurface->OnUpdate))(self->pISurface);
    // PeekMessage.
    __static_mmUIViewSurfaceMaster_ThreadPeekMessage(self);
    // Timewait.
    (*(self->pISurface->OnTimewait))(self->pISurface);
}

- (void)OnUIKeyboardNotification:(NSNotification*)pNotification status:(mmUInt32_t)state
{
    struct mmEventKeypadStatus* pKeypadStatus = &self->hSurfaceContent.hKeypadStatus;
    
    NSDictionary* pUserInfo = [pNotification userInfo];
    
    CGRect hKeyboardRectE = [[pUserInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    double hAnimationDuration = [[pUserInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSInteger hAnimationCurve = [[pUserInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect hLocalKeypadRectE = [self convertRect:hKeyboardRectE fromView:self];
    CGRect hWindowFrame = self.frame;
    CGRect hWindowSafeArea = self.safeAreaLayoutGuide.layoutFrame;
    CGRect hMainScreenRect = [[UIScreen mainScreen] bounds];
    
    pKeypadStatus->surface = (__bridge void*)(self);
    
    pKeypadStatus->screen_rect[0] = hMainScreenRect.origin.x;      // x
    pKeypadStatus->screen_rect[1] = hMainScreenRect.origin.y;      // y
    pKeypadStatus->screen_rect[2] = hMainScreenRect.size.width;    // width
    pKeypadStatus->screen_rect[3] = hMainScreenRect.size.height;   // height
    
    pKeypadStatus->safety_area[0] = hWindowSafeArea.origin.x;      // x
    pKeypadStatus->safety_area[1] = hWindowSafeArea.origin.y;      // y
    pKeypadStatus->safety_area[2] = hWindowSafeArea.size.width;    // width
    pKeypadStatus->safety_area[3] = hWindowSafeArea.size.height;   // height
    
    pKeypadStatus->keypad_rect[0] = hLocalKeypadRectE.origin.x;    // x
    pKeypadStatus->keypad_rect[1] = hLocalKeypadRectE.origin.y;    // y
    pKeypadStatus->keypad_rect[2] = hLocalKeypadRectE.size.width;  // width
    pKeypadStatus->keypad_rect[3] = hLocalKeypadRectE.size.height; // height
    
    pKeypadStatus->window_rect[0] = hWindowFrame.origin.x;         // x
    pKeypadStatus->window_rect[1] = hWindowFrame.origin.y;         // y
    pKeypadStatus->window_rect[2] = hWindowFrame.size.width;       // width
    pKeypadStatus->window_rect[3] = hWindowFrame.size.height;      // height
    
    pKeypadStatus->animation_duration = hAnimationDuration;
    pKeypadStatus->animation_curve = (int)hAnimationCurve;
    
    pKeypadStatus->state = state;
    pKeypadStatus->handle = 0;
    
    (*(self->pISurface->OnKeypadStatus))(self->pISurface, pKeypadStatus);
}

- (void)OnUIKeyboardHandleAnimationStatus:(NSNotification*)pNotification
{
    struct mmEventKeypadStatus* pKeypadStatus = &self->hSurfaceContent.hKeypadStatus;
    
    if(0 == pKeypadStatus->handle)
    {
        double hAnimationDuration = pKeypadStatus->animation_duration;
        UIViewAnimationCurve hAnimationCurve = (UIViewAnimationCurve)pKeypadStatus->animation_curve;
        
        double y1 = (double)((pKeypadStatus->keypad_rect[1]           ));
        double y2 = (double)((self->hTextEditRect[1] + self->hTextEditRect[3]));
        double y3 = y1 - y2;
        double y4 = 0 > y3 ? y3 : 0.0;
        
        // Although I prefer the following method, it is not officially recommended.
        //
        // CGRect hEAGL2ViewFrame;
        // [UIView beginAnimations:nil context:nullptr];
        // [UIView setAnimationDelegate:self];
        // [UIView setAnimationDuration:hAnimationDuration];
        // [UIView setAnimationCurve:(UIViewAnimationCurve)hAnimationCurve];
        // [UIView setAnimationBeginsFromCurrentState:YES];
        // hEAGL2ViewFrame = self.viewEAGL2.frame;
        // hEAGL2ViewFrame.origin.y = y4;
        // self.viewEAGL2.frame = hEAGL2ViewFrame;
        // [UIView commitAnimations];
        
        /* The UIView commitAnimations series functions is not recommend now. */
        /* Deprecated in iOS 13.0. Please use the block-based animation API instead. */

        UIViewAnimationOptions hOptions = 0;
        switch(hAnimationCurve)
        {
        case UIViewAnimationCurveEaseInOut:
            hOptions = UIViewAnimationOptionCurveEaseInOut;
            break;
        case UIViewAnimationCurveEaseIn:
            hOptions = UIViewAnimationOptionCurveEaseIn;
            break;
        case UIViewAnimationCurveEaseOut:
            hOptions = UIViewAnimationOptionCurveEaseOut;
            break;
        case UIViewAnimationCurveLinear:
            hOptions = UIViewAnimationOptionCurveLinear;
            break;
        default:
            hOptions = 0;
            break;
        }
        
        // Some of time, we need user interaction at animation.
        // If need this, open the next line.
        //
        // hOptions |= UIViewAnimationOptionAllowUserInteraction;
        
        [UIView animateWithDuration:hAnimationDuration
                              delay:0.0
                            options:hOptions
                         animations:^
        {
            CGRect hViewFrame;
            hViewFrame = self.frame;
            hViewFrame.origin.y = y4;
            self.frame = hViewFrame;
        }
                         completion:^(BOOL finished)
        {
            mmMemset(self->hTextEditRect, 0, sizeof(double) * 4);
            (*(self->pISurface->OnGetTextEditRect))(self->pISurface, self->hTextEditRect);
            
            [self OnMenuControllerShow];
        }];
    }
}

#pragma mark NSNotification - handler function.

- (void)OnUIKeyboardWillShow:(NSNotification*)notification
{
    [self OnUIKeyboardNotification:notification status:mmKeypadStatusShow];
    
    // Handler the return value.
    [self OnUIKeyboardHandleAnimationStatus:notification];
}
- (void)OnUIKeyboardWillHide:(NSNotification*)notification
{   
    [self OnUIKeyboardNotification:notification status:mmKeypadStatusHide];
    
    // Handler the return value.
    [self OnUIKeyboardHandleAnimationStatus:notification];
}
- (void)OnApplicationDidEnterBackground:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    [self OnEnterBackground];
}
- (void)OnApplicationWillEnterForeground:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    [self OnEnterForeground];
}
#pragma mark -

#pragma mark UIMenuController - Member function.

- (void)OnMenuControllerShow
{
    UIMenuController* pMenuController = [UIMenuController sharedMenuController];

    CGRect rect;

    rect.origin.x    = (CGFloat)(self->hTextEditRect[0]);
    rect.origin.y    = (CGFloat)(self->hTextEditRect[1]);
    rect.size.width  = (CGFloat)(self->hTextEditRect[2]);
    rect.size.height = (CGFloat)(self->hTextEditRect[3]);

    BOOL hAnimated = (YES != pMenuController.isMenuVisible);
    [pMenuController setTargetRect:rect inView:self];
    [pMenuController setMenuVisible:YES animated:hAnimated];
}
- (void)OnMenuControllerHide
{
    UIMenuController* pMenuController = [UIMenuController sharedMenuController];
    BOOL hAnimated = (NO != pMenuController.isMenuVisible);
    [pMenuController setMenuVisible:NO animated:hAnimated];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    static const int MaxActionNumber = 4;
    
    // For quick debug.
    // NSLog(@"action: %@", NSStringFromSelector(action));
    
    NSString* name = NSStringFromSelector(action);
    NSNumber* indexObject = [self.selectorToIndex objectForKey:name];
    mmObjc_Dealloc(name);
    if(nil != indexObject)
    {
        int index = indexObject.intValue;
        
        struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
        
        pEditText->v = mmEditTextWhole | mmEditTextRsize;
        (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
        
        int textLength = (int)(pEditText->size);
        int textSelect = (int)(pEditText->r - pEditText->l);
        
        UIPasteboard* pBboard = [UIPasteboard generalPasteboard];
        int buffLength = (int)(pBboard.string.length);
        
        BOOL hFlags[MaxActionNumber] = { 0 };
        hFlags[0] = (0 < textSelect);
        hFlags[1] = (0 < textSelect);
        hFlags[2] = (0 < buffLength);
        hFlags[3] = (textSelect != textLength);
        
        return hFlags[index];
    }
    else
    {
        return [super canPerformAction:action withSender:sender];
    }
}
- (void)copy:(nullable id)sender
{
    // NSLog(@"mmUIViewSurfaceMaster.copy");
    (*(self->pISurface->OnInjectCopy))(self->pISurface);
}
- (void)cut:(nullable id)sender
{
    // NSLog(@"mmUIViewSurfaceMaster.cut");
    (*(self->pISurface->OnInjectCut))(self->pISurface);
}
- (void)paste:(nullable id)sender
{
    // NSLog(@"mmUIViewSurfaceMaster.paste");
    (*(self->pISurface->OnInjectPaste))(self->pISurface);
}
- (void)selectAll:(nullable id)sender
{
    // NSLog(@"mmUIViewSurfaceMaster.selectAll");
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    pEditText->l = 0;
    pEditText->r = pEditText->size;
    pEditText->v = mmEditTextRange;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
    
    // The default UIMenuController behavior is disappear after "selectAll" action.
    // But most of time, we need show UIMenuController .
    [self OnMenuControllerShow];
}

#pragma mark -

#pragma mark - mmUIViewSurfaceMaster Member function.

- (void)OnUpdateMask:(UIEvent *)event withPhase:(int)phase maskKey:(mmUInt32_t)button_id
{
    self->hModifierMask = 0;

    if (MM_MB_UNKNOWN != button_id)
    {
        switch (phase)
        {
        case mmTouchBegan:
            self->hButtonMask |=  (1 << button_id);
            break;
        case mmTouchEnded:
        case mmTouchBreak:
            self->hButtonMask &= ~(1 << button_id);
            break;
        default:
            break;
        }
    }
}

- (void)OnTouchesDataHandler:(NSSet<UITouch *> *)touches
                   withEvent:(nullable UIEvent *)event
                   withPhase:(int)phase
                     maskKey:(mmUInt32_t)button_id
{
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    
    struct timeval tv;
    mmUInt64_t timestamp = 0;
    
    struct mmEventTouch* t = NULL;
    size_t i = 0;
    
    double force_value = 0.0;
    double force_maximum = 0.0;
    
    NSEnumerator* enumerator = nil;
    UITouch* touch = nil;
    
    UIView* view = nil;
    CGPoint _locationInView;
    CGPoint _locationInViewReal;
    CGPoint _previousLocationInView;
    CGPoint _previousLocationInViewReal;
    
    CGFloat _abs_x = 0.0f;
    CGFloat _abs_y = 0.0f;
    CGFloat _rel_x = 0.0f;
    CGFloat _rel_y = 0.0f;
    
    NSInteger count = [touches count];
    
    [self OnUpdateMask:event withPhase:phase maskKey:button_id];
    
    mmEventTouchs_AlignedMemory(pTouchs, (size_t)count);
    
    mmGettimeofday(&tv, NULL);
    timestamp = (mmUInt64_t)(mmTimeval_ToUSec(&tv) / 1000);
    
    enumerator = [touches objectEnumerator];
    while (touch = [enumerator nextObject])
    {
        t = mmEventTouchs_GetForIndexReference(pTouchs, i);
        
        view = [touch view];
        _locationInView = [touch locationInView: view];
        _locationInViewReal.x = _locationInView.x;
        _locationInViewReal.y = _locationInView.y;
        
        _previousLocationInView = [touch previousLocationInView: view];
        _previousLocationInViewReal.x = _previousLocationInView.x;
        _previousLocationInViewReal.y = _previousLocationInView.y;
        
        force_value = (double)touch.force;
        force_maximum = (double)touch.maximumPossibleForce;
        
        // When comparing the specs of the iPhone 11, iPhone 11 Pro, and iPhone 11 Pro Max
        // I noticed 3D Touch has officially been replaced with Haptic Touch.
        //
        // UITouch.force UITouch.maximumPossibleForce value may 0.
        //
        // clamp
        force_maximum = (0 >= force_maximum) ? 1.0 : force_maximum;
        force_value = force_value <= 0.0 ? 0.25 * force_maximum : force_value;
        force_value = force_value >= force_maximum ? 0.25 * force_maximum : force_value;
        
        _abs_x = _locationInViewReal.x;
        _abs_y = _locationInViewReal.y;
        _rel_x = _locationInViewReal.x - _previousLocationInViewReal.x;
        _rel_y = _locationInViewReal.y - _previousLocationInViewReal.y;
        
        t->motion_id     = (uintptr_t)((__bridge void *)touch);
        t->tap_count     = (int)touch.tapCount;
        t->phase         = (int)phase;
        t->modifier_mask = (int)self->hModifierMask;
        t->button_mask   = (int)self->hButtonMask;
        
        t->abs_x         = (double)(_abs_x);
        t->abs_y         = (double)(_abs_y);
        t->rel_x         = (double)(_rel_x);
        t->rel_y         = (double)(_rel_y);

        t->force_value   = (double)force_value;
        t->force_maximum = (double)force_maximum;
        t->major_radius  = (double)[touch majorRadius];
        t->minor_radius  = (double)[touch majorRadius];
        t->size_value    = (double)(t->major_radius * t->minor_radius);
        
        t->timestamp     = (mmUInt64_t)timestamp;
        
        // After testing, the last locationInView is the current previousLocationInView.
        // NSLog(@"location (%f, %f) (%f, %f)", _abs_x, _abs_y, _rel_x, _rel_y);
        
        // NSLog(@"motion_id: %" PRIuPTR " force_value: %lf force_maximum: %lf major_radius: %lf",
        //       t->motion_id, t->force_value, t->force_maximum, t->major_radius);
        
        ++i;
    }
    
    if (0 < count)
    {
        // trigger simulation cursor event.
        t = mmEventTouchs_GetForIndexReference(pTouchs, 0);
        
        pCursor->modifier_mask = (int)t->modifier_mask;
        pCursor->button_mask = (int)t->button_mask;
        pCursor->button_id = (int)button_id;
        pCursor->abs_x = (double)t->abs_x;
        pCursor->abs_y = (double)t->abs_y;
        pCursor->rel_x = (double)t->rel_x;
        pCursor->rel_y = (double)t->rel_y;
        pCursor->wheel_dx = (double)0.0;
        pCursor->wheel_dy = (double)0.0;
        pCursor->timestamp = (mmUInt64_t)timestamp;
    }
}

#pragma mark -

#pragma mark UIView - UIGestureRecognizer

- (void)setGestureRecognizers:(NSArray<__kindof UIGestureRecognizer *> *)gestureRecognizers
{
    // We don't trigger any gestures.
}
- (NSArray<__kindof UIGestureRecognizer *> *)gestureRecognizers
{
    // We don't trigger any gestures.
    return nil;
}

// called when the recognizer attempts to transition out of UIGestureRecognizerStatePossible
// if a touch hit-tested to this view will be cancelled as a result of gesture recognition
// returns YES by default. return NO to cause the gesture recognizer to transition to
// UIGestureRecognizerStateFailed
// subclasses may override to prevent recognition of particular gestures. for example, UISlider
// prevents swipes parallel to the slider that start in the thumb
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer API_AVAILABLE(ios(6.0))
{
    // We don't trigger any gestures.
    return NO;
}

#pragma mark -

// Pass the touches to the superview
#pragma mark UIView - Touchs Delegate

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    
    NSInteger count = [touches count];
    
    if (0 < count)
    {
        [self OnTouchesDataHandler:touches
                         withEvent:event
                         withPhase:mmTouchBegan
                           maskKey:MM_MB_LBUTTON];
        
        (*(self->pISurface->OnTouchsBegan))(self->pISurface, pTouchs);
        (*(self->pISurface->OnCursorBegan))(self->pISurface, pCursor);
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    
    NSInteger count = [touches count];
    
    if (0 < count)
    {
        [self OnTouchesDataHandler:touches
                         withEvent:event
                         withPhase:mmTouchMoved
                           maskKey:MM_MB_UNKNOWN];
        
        (*(self->pISurface->OnTouchsMoved))(self->pISurface, pTouchs);
        (*(self->pISurface->OnCursorMoved))(self->pISurface, pCursor);
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    
    NSInteger count = [touches count];
    
    if (0 < count)
    {
        [self OnTouchesDataHandler:touches
                         withEvent:event
                         withPhase:mmTouchEnded
                           maskKey:MM_MB_LBUTTON];
        
        (*(self->pISurface->OnTouchsEnded))(self->pISurface, pTouchs);
        (*(self->pISurface->OnCursorEnded))(self->pISurface, pCursor);
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event
{
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    
    NSInteger count = [touches count];
    
    if (0 < count)
    {
        [self OnTouchesDataHandler:touches
                         withEvent:event
                         withPhase:mmTouchBreak
                           maskKey:MM_MB_LBUTTON];
        
        (*(self->pISurface->OnTouchsBreak))(self->pISurface, pTouchs);
        (*(self->pISurface->OnCursorBreak))(self->pISurface, pCursor);
    }
}

#pragma mark -

#pragma mark - UIView - Responder

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)becomeFirstResponder
{
    // clear the TextInput MarkedText.
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    // clear the TextInput MarkedText.
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
    return [super resignFirstResponder];
}

#pragma mark -

#pragma mark UITextInputTraits Protocol Methods

// default is UIKeyboardTypeDefault
@synthesize keyboardType;
// default is UIKeyboardAppearanceDefault
@synthesize keyboardAppearance;
// default is UIReturnKeyDefault
@synthesize returnKeyType;

#pragma mark -

#pragma mark UIKeyInput Protocol Methods

- (BOOL)hasText
{
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    return (0 != pEditText->size);
}
- (void)insertText:(NSString *)text
{
    // NSLog(@"mmUIViewSurfaceMaster.insertText %@", text);
    
    // clear the TextInput MarkedText.
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
    
    // No matter what type of UIReturnKey.
    // The return key fire a "\n" "insertText" event.
    //
    // At IOS simulator. The "Cammand" "CTRL" "ALT" control key will not trigger this event.
    NSUInteger length = text.length;
    NSRange hRange = NSMakeRange(0, length);
    mmUtf16String_Resize(&self->hUTF16String, length);
    mmUInt16_t* buffer = (mmUInt16_t*)mmUtf16String_Data(&self->hUTF16String);
    [text getCharacters:(unichar *)buffer range:hRange];

    struct mmEventEditString* pEditString = &self->hSurfaceContent.hEditString;
    
    pEditString->s = buffer;
    pEditString->l = length;
    pEditString->o = 0;
    pEditString->n = 0;
    (*(self->pISurface->OnInsertTextUtf16))(self->pISurface, pEditString);
    
    // If insert any text, we hide the UIMenuController.
    [self OnMenuControllerHide];
}
- (void)deleteBackward
{
    // NSLog(@"mmUIViewSurfaceMaster.deleteBackward");
    
    // clear the TextInput MarkedText.
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
    
    struct mmEventKeypad* pKeypad = &self->hSurfaceContent.hKeypad;
    mmMemset(pKeypad->text, 0, sizeof(mmUInt16_t) * 4);
    pKeypad->modifier_mask = 0;
    pKeypad->key = MM_KC_BACKSPACE;
    pKeypad->length = 1;
    pKeypad->text[0] = MM_ASCII_BS;
    pKeypad->handle = 0;
    (*(self->pISurface->OnKeypadPressed))(self->pISurface, pKeypad);
    (*(self->pISurface->OnKeypadRelease))(self->pISurface, pKeypad);
    
    // If delete back any ward, we hide the UIMenuController.
    [self OnMenuControllerHide];
}

#pragma mark -

#pragma mark - UIView - UITextInput

/* Methods for manipulating text. */
- (nullable NSString *)textInRange:(UITextRange *)range
{
    // A substring of a document that falls within the specified range.
    
    // NSLog(@"mmUIViewSurfaceMaster.textInRange");
    mmUITextRange* pRange = (mmUITextRange*)range;
    NSString* text = nil;
    
    size_t l = 0;
    size_t r = 0;
    size_t size = 0;
    
    mmUInt16_t* buffer = NULL;
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    l = pRange.l.pos;
    r = pRange.r.pos;
    size = r - l;
    
    buffer = pEditText->t + l;
    
    // build the NSString with Copy, make sure the life cycle safe.
    text = [NSString stringWithCharacters:(const unichar *)buffer
                                   length:(NSUInteger)size];
    mmObjc_Autorelease(text);
    
    // NSLog(@"mmUIViewSurfaceMaster.textInRange (%d %d) (%@)",
    //      (int)pRange.l.pos, (int)pRange.r.pos, text);
    
    return text;
}

- (void)replaceRange:(UITextRange *)range withText:(NSString *)text
{
    // Replaces the text in a document that is in the specified range.
    
    // NSLog(@"mmUIViewSurfaceMaster.replaceRange");
    mmUITextRange* pRange = (mmUITextRange*)range;
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    struct mmEventEditString* pEditString = &self->hSurfaceContent.hEditString;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    NSUInteger length = text.length;
    NSRange hRange = NSMakeRange(0, length);
    mmUtf16String_Resize(&self->hUTF16String, length);
    mmUInt16_t* buffer = (mmUInt16_t*)mmUtf16String_Data(&self->hUTF16String);
    [text getCharacters:(unichar *)buffer range:hRange];
    
    size_t hLPos = (size_t)pRange.l.pos;
    size_t hRPos = (size_t)pRange.r.pos;
    size_t hMarkedSize = hRPos - hLPos;
    pEditString->s = buffer;
    pEditString->l = length;
    pEditString->o = hLPos;
    pEditString->n = hMarkedSize;
    (*(self->pISurface->OnReplaceTextUtf16))(self->pISurface, pEditString);
    
    pEditText->c = (size_t)(hRPos);
    pEditText->v = mmEditTextCaret | mmEditTextDirty;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
}

/* Text may have a selection, either zero-length (a caret) or ranged.  Editing operations are
 * always performed on the text from this selection.  nil corresponds to no selection. */

// @synthesize selectedTextRange;
- (void)setSelectedTextRange:(UITextRange *)aSelectedTextRange;
{
    // NSLog(@"mmUIViewSurfaceMaster.setSelectedTextRange");
    mmUITextRange* pRange = (mmUITextRange*)aSelectedTextRange;
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);

    pEditText->l = (size_t)pRange.l.pos;
    pEditText->r = (size_t)pRange.r.pos;
    pEditText->v = mmEditTextRange;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
}
- (UITextRange *)selectedTextRange;
{
    // If the text range has a length, it indicates the currently selected text.
    // If it has zero length, it indicates the caret (insertion point).
    // If the text-range object is nil, it indicates that there is no current selection.
    
    // NSLog(@"mmUIViewSurfaceMaster.selectedTextRange");
    mmUITextRange* pRange = [[mmUITextRange alloc] init];
    mmObjc_Autorelease(pRange);
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    if (0 == pEditText->l && 0 == pEditText->r)
    {
        pRange.l.pos = (NSUInteger)pEditText->c;
        pRange.r.pos = (NSUInteger)pEditText->c;
    }
    else
    {
        pRange.l.pos = (NSUInteger)pEditText->l;
        pRange.r.pos = (NSUInteger)pEditText->r;
    }
    
    return pRange;
}

/* If text can be selected, it can be marked. Marked text represents provisionally
 * inserted text that has yet to be confirmed by the user.  It requires unique visual
 * treatment in its display.  If there is any marked text, the selection, whether a
 * caret or an extended range, always resides witihin.
 *
 * Setting marked text either replaces the existing marked text or, if none is present,
 * inserts it from the current selection. */

// Nil if no marked text.
// @synthesize markedTextRange;
- (void)setMarkedTextRange:(UITextRange *)markedTextRange;
{
    // NSLog(@"mmUIViewSurfaceMaster.setMarkedTextRange");
    mmUITextRange* pRange = (mmUITextRange*)markedTextRange;
    
    self.markedRange.l.pos = pRange.l.pos;
    self.markedRange.r.pos = pRange.r.pos;
}

- (UITextRange *)markedTextRange;
{
    // NSLog(@"mmUIViewSurfaceMaster.markedTextRange");
    return (nil == self.markedText) ? nil : self.markedRange;
}

// Describes how the marked text should be drawn.
// @synthesize markedTextStyle;
- (void)setMarkedTextStyle:(NSDictionary<NSAttributedStringKey, id> *)markedTextStyle;
{
    // NSLog(@"mmUIViewSurfaceMaster.setMarkedTextStyle");
    self.markedStyles = markedTextStyle;
}
- (NSDictionary<NSAttributedStringKey, id> *)markedTextStyle;
{
    // NSLog(@"mmUIViewSurfaceMaster.markedTextStyle");
    return self.markedStyles;
}

// selectedRange is a range within the markedText
- (void)setMarkedText:(nullable NSString *)markedText
        selectedRange:(NSRange)selectedRange
{
    // NSLog(@"mmUIViewSurfaceMaster.setMarkedText (%@) (%d %d)", markedText,
    //      (int)selectedRange.location, (int)selectedRange.length);
    
    NSUInteger length = markedText.length;
    NSRange hRange = NSMakeRange(0, length);
    mmUtf16String_Resize(&self->hUTF16String, length);
    mmUInt16_t* buffer = (mmUInt16_t*)mmUtf16String_Data(&self->hUTF16String);
    [markedText getCharacters:(unichar *)buffer range:hRange];
    
    mmUITextRange* pRange = (mmUITextRange*)self.markedRange;
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    struct mmEventEditString* pEditString = &self->hSurfaceContent.hEditString;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);

    size_t hMarkedSize = (nil == self.markedText) ? 0 : self.markedText.length;
    size_t hRPos = pEditText->c;
    size_t hLPos = hRPos - hMarkedSize;
    pEditString->s = buffer;
    pEditString->l = length;
    pEditString->o = hLPos;
    pEditString->n = hMarkedSize;
    (*(self->pISurface->OnReplaceTextUtf16))(self->pISurface, pEditString);
    
    pRange.l.pos = hLPos;
    pRange.r.pos = hLPos + length;
    
    pEditText->c = (size_t)(hLPos + length);
    pEditText->v = mmEditTextCaret | mmEditTextDirty;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
    
    // cache.
    mmObjc_Dealloc(self.markedText);
    self.markedText = markedText;
}
- (void)unmarkText
{
    // NSLog(@"mmUIViewSurfaceMaster.unmarkText");
    mmUITextRange* pRange = (mmUITextRange*)self.markedRange;
    pRange.l.pos = 0;
    pRange.r.pos = 0;
    
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
}

/* The end and beginning of the the text document. */
@synthesize beginningOfDocument;
@synthesize endOfDocument;

/* Methods for creating ranges and positions. */
- (nullable UITextRange *)textRangeFromPosition:(UITextPosition *)fromPosition
                                     toPosition:(UITextPosition *)toPosition
{
    // An object that represents the range between fromPosition and toPosition.
    
    mmUITextPosition* pLPos = (mmUITextPosition*)fromPosition;
    mmUITextPosition* pRPos = (mmUITextPosition*)toPosition;
    
    mmUITextRange* pRange = [[mmUITextRange alloc] init];
    mmObjc_Autorelease(pRange);

    // l <= r is easy for use.
    pRange.l.pos = pLPos.pos < pRPos.pos ? pLPos.pos : pRPos.pos;
    pRange.r.pos = pLPos.pos > pRPos.pos ? pLPos.pos : pRPos.pos;
    
    // NSLog(@"mmUIViewSurfaceMaster.textRangeFromPosition (%d %d)",
    //      (int)pRange.l.pos, (int)pRange.r.pos);
    
    return pRange;
}
- (nullable UITextPosition *)positionFromPosition:(UITextPosition *)position
                                           offset:(NSInteger)offset
{
    // A custom UITextPosition object that represents the location in a document
    // that is at the specified offset from position. Return nil if the computed
    // text position is less than 0 or greater than the length of the backing string.
    
    mmUITextPosition* pOPos = (mmUITextPosition*)position;
    NSUInteger pos = pOPos.pos + offset;
    
    // NSLog(@"mmUIViewSurfaceMaster.positionFromPosition (%d)", (int)pos);
    
    if ((NSUInteger)(-1) == pos)
    {
        return nil;
    }
    else
    {
        mmUITextPosition* pNPos = [[mmUITextPosition alloc] init];
        mmObjc_Autorelease(pNPos);
        pNPos.pos = pos;
        
        return pNPos;
    }
}
- (nullable UITextPosition *)positionFromPosition:(UITextPosition *)position
                                      inDirection:(UITextLayoutDirection)direction
                                           offset:(NSInteger)offset
{
    // UITextLayoutDirectionRight = 2,
    // UITextLayoutDirectionLeft,
    // UITextLayoutDirectionUp,
    // UITextLayoutDirectionDown
    return nil;
}

/* Simple evaluation of positions */
- (NSComparisonResult)comparePosition:(UITextPosition *)position
                           toPosition:(UITextPosition *)other
{
    mmUITextPosition* f = (mmUITextPosition*)position;
    mmUITextPosition* t = (mmUITextPosition*)other;
    if(f == nil && t != nil)
    {
        return NSOrderedAscending;
    }
    if(f != nil && t == nil)
    {
        return NSOrderedDescending;
    }
    if(f == nil && t == nil)
    {
        return NSOrderedSame;
    }
    
    if(f.pos < t.pos)
    {
        return NSOrderedAscending;
    }
    if(f.pos > t.pos)
    {
        return NSOrderedDescending;
    }
    
    if(f.pos == t.pos)
    {
        return NSOrderedSame;
    }
    
    return NSOrderedSame;
}
- (NSInteger)offsetFromPosition:(UITextPosition *)from
                     toPosition:(UITextPosition *)toPosition
{
    mmUITextPosition* f = (mmUITextPosition*)from;
    mmUITextPosition* t = (mmUITextPosition*)toPosition;
    return t.pos - f.pos;
}

/* A system-provied input delegate is assigned when the system is interested in input changes. */
@synthesize inputDelegate;

/* A tokenizer must be provided to inform the text input system about text units of varying granularity. */
@synthesize tokenizer;

/* Layout questions. */
- (nullable UITextPosition *)positionWithinRange:(UITextRange *)range
                             farthestInDirection:(UITextLayoutDirection)direction
{
    // UITextLayoutDirectionRight = 2,
    // UITextLayoutDirectionLeft,
    // UITextLayoutDirectionUp,
    // UITextLayoutDirectionDown
    return nil;
}
- (nullable UITextRange *)characterRangeByExtendingPosition:(UITextPosition *)position
                                                inDirection:(UITextLayoutDirection)direction
{
    // UITextLayoutDirectionRight = 2,
    // UITextLayoutDirectionLeft,
    // UITextLayoutDirectionUp,
    // UITextLayoutDirectionDown
    return nil;
}

/* Writing direction */
- (NSWritingDirection)baseWritingDirectionForPosition:(UITextPosition *)position
                                          inDirection:(UITextStorageDirection)direction
{
    return UITextWritingDirectionNatural;
}
- (void)setBaseWritingDirection:(NSWritingDirection)writingDirection
                       forRange:(UITextRange *)range
{
    // need do nothing here.
}

/* Geometry used to provide, for example, a correction rect. */
- (CGRect)firstRectForRange:(UITextRange *)range
{
    return CGRectMake(0, 0, 0, 0);
}
- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    return CGRectMake(0, 0, 0, 0);
}
// Returns an array of UITextSelectionRects
- (NSArray<UITextSelectionRect *> *)selectionRectsForRange:(UITextRange *)range API_AVAILABLE(ios(6.0))
{
    NSArray<UITextSelectionRect *>* pArray = [[NSArray<UITextSelectionRect *> alloc] init];
    mmObjc_Autorelease(pArray);
    return pArray;
}

/* Hit testing. */
- (nullable UITextPosition *)closestPositionToPoint:(CGPoint)point
{
    return nil;
}
- (nullable UITextPosition *)closestPositionToPoint:(CGPoint)point
                                        withinRange:(UITextRange *)range
{
    return nil;
}
- (nullable UITextRange *)characterRangeAtPoint:(CGPoint)point
{
    return nil;
}

#pragma mark -

#pragma mark - CAMetalLayer

/** Indicates that the view wants to draw using the backing layer instead of using drawRect:.  */
-(BOOL) wantsUpdateLayer
{
    return YES;
}

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunguarded-availability-new"
#endif

/** Returns a Metal-compatible layer. */
+(Class) layerClass
{
    return [CAMetalLayer class];
}

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#pragma mark -

#pragma mark - UIView - static mmUIViewSurfaceMaster

static 
void 
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface, 
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    pMetrics->surface = surface;
    pMetrics->canvas_size[0] = pSurfaceMaster->hDisplayFrameSize[0];
    pMetrics->canvas_size[1] = pSurfaceMaster->hDisplayFrameSize[1];
    pMetrics->window_size[0] = pSurfaceMaster->hPhysicalViewSize[0];
    pMetrics->window_size[1] = pSurfaceMaster->hPhysicalViewSize[1];
    pMetrics->safety_area[0] = pSurfaceMaster->hPhysicalSafeRect[0];
    pMetrics->safety_area[1] = pSurfaceMaster->hPhysicalSafeRect[1];
    pMetrics->safety_area[2] = pSurfaceMaster->hPhysicalSafeRect[2];
    pMetrics->safety_area[3] = pSurfaceMaster->hPhysicalSafeRect[3];
}

static void __static_mmUIViewSurfaceMaster_Init(mmUIViewSurfaceMaster* p)
{
    mmEventSurfaceContent_Init(&p->hSurfaceContent);
    mmISurfaceKeypad_Init(&p->hISurfaceKeypad);
    p->pSurfaceMaster = NULL;
    p->pISurface = NULL;
    mmUtf16String_Init(&p->hUTF16String);
    mmMemset(p->hTextEditRect, 0, sizeof(p->hTextEditRect));
    p->hViewSize = CGSizeZero;
    p->hSafeRect = CGRectZero;
    p->hModifierMask = 0;
    p->hButtonMask = 0;
    p->hDisplayDensity = 1.0;
    
    p.displayLink = nil;
    p.markedText = nil;
    p.markedRange = nil;
    p.markedStyles = nil;
    p.selectorToIndex = nil;
}
static void __static_mmUIViewSurfaceMaster_Destroy(mmUIViewSurfaceMaster* p)
{
    p.selectorToIndex = nil;
    p.markedStyles = nil;
    p.markedRange = nil;
    p.markedText = nil;
    p.displayLink = nil;
    
    p->hDisplayDensity = 1.0;
    p->hButtonMask = 0;
    p->hModifierMask = 0;
    p->hSafeRect = CGRectZero;
    p->hViewSize = CGSizeZero;
    mmMemset(p->hTextEditRect, 0, sizeof(p->hTextEditRect));
    mmUtf16String_Destroy(&p->hUTF16String);
    p->pISurface = NULL;
    p->pSurfaceMaster = NULL;
    mmISurfaceKeypad_Destroy(&p->hISurfaceKeypad);
    mmEventSurfaceContent_Destroy(&p->hSurfaceContent);
}

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_ThreadEnter(mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_ThreadLeave(mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(mmUIViewSurfaceMaster* p)
{
    do
    {
        struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        double hDisplayDensity = p->hDisplayDensity;
        
        const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);
        unsigned int w = 0;
        unsigned int h = 0;
        
        double hDisplayFrameSize[2];
        double hPhysicalSafeRect[4];
        
        CGSize hViewSize = p.bounds.size;
        CGRect hSafeRect = p.safeAreaLayoutGuide.layoutFrame;
        
        struct mmEventMetrics* pMetrics = NULL;
        
        if (NULL == p->pSurfaceMaster)
        {
            mmLogger_LogI(gLogger, "%s %d pSurfaceMaster is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        w = (unsigned int)hViewSize.width;
        h = (unsigned int)hViewSize.height;
        
        p->hSafeRect = hSafeRect;
        p->hViewSize = hViewSize;
        
        pMetrics = &p->hSurfaceContent.hMetrics;
        
        hDisplayFrameSize[0] = (double)w * hDisplayDensity;
        hDisplayFrameSize[1] = (double)h * hDisplayDensity;
        
        // Safe rect.
        hPhysicalSafeRect[0] = (double)hSafeRect.origin.x;
        hPhysicalSafeRect[1] = (double)hSafeRect.origin.y;
        hPhysicalSafeRect[2] = (double)hSafeRect.size.width;
        hPhysicalSafeRect[3] = (double)hSafeRect.size.height;

        mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
        mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
        mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);

        // Logger infomation.
        mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.PrepareSwapchain");
        mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_INFO, pWindowName);
        
        // OnSurfacePrepare event.
        __static_mmSurfaceContentMetricsAssignment(pMetrics, (__bridge void*)p, p->pSurfaceMaster);
        (*(p->pISurface->OnSurfacePrepare))(p->pISurface, pMetrics);

    }while(0);
}
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;
    __static_mmSurfaceContentMetricsAssignment(pMetrics, (__bridge void*)p, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceDiscard))(p->pISurface, pMetrics);

    mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.DiscardSwapchain");
}

static void __static_mmUIViewSurfaceMaster_OnSizeChange(mmUIViewSurfaceMaster* p, CGFloat fw, CGFloat fh)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;

    double hDisplayDensity = p->hDisplayDensity;
    double hDisplayFrameSize[2];
    double hPhysicalSafeRect[4];
    
    const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);
    
    CGRect hSafeRect = p.safeAreaLayoutGuide.layoutFrame;

    // Display frame size.
    hDisplayFrameSize[0] = (double)(fw * hDisplayDensity);
    hDisplayFrameSize[1] = (double)(fh * hDisplayDensity);

    // Safe rect.
    hPhysicalSafeRect[0] = (double)hSafeRect.origin.x;
    hPhysicalSafeRect[1] = (double)hSafeRect.origin.y;
    hPhysicalSafeRect[2] = (double)hSafeRect.size.width;
    hPhysicalSafeRect[3] = (double)hSafeRect.size.height;
    
    mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
    mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
    mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);

    // Logger infomation.
    mmLogger_LogT(gLogger, "Window mmUIViewSurfaceMaster.NativeOnSurfaceChanged");
    mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_TRACE, pWindowName);
    
    // Fire event for size change.
    __static_mmSurfaceContentMetricsAssignment(pMetrics, (__bridge void*)p, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceChanged))(p->pISurface, pMetrics);
}

static void __static_mmUIViewSurfaceMaster_AttachNotification(mmUIViewSurfaceMaster* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify addObserver:p selector:@selector(OnUIKeyboardWillShow:)
                    name:UIKeyboardWillShowNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnUIKeyboardWillHide:)
                    name:UIKeyboardWillHideNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationDidEnterBackground:)
                    name:UIApplicationDidEnterBackgroundNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnApplicationWillEnterForeground:)
                    name:UIApplicationWillEnterForegroundNotification
                  object:nil];
}
static void __static_mmUIViewSurfaceMaster_DetachNotification(mmUIViewSurfaceMaster* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify removeObserver:p name:UIKeyboardWillShowNotification object:nil];
    [pNotify removeObserver:p name:UIKeyboardWillHideNotification object:nil];
    [pNotify removeObserver:p name:UIApplicationDidEnterBackgroundNotification object:nil];
    [pNotify removeObserver:p name:UIApplicationWillEnterForegroundNotification object:nil];
}

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pSuper, int hType)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    // mmSurfaceKeypadType -> UIKeyboardType Map is same.
    [p setKeyboardType:(UIKeyboardType)hType];
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pSuper, int hAppearance)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    // mmSurfaceKeypadAppearance -> UIKeyboardAppearance Map is same.
    [p setKeyboardAppearance:(UIKeyboardAppearance)hAppearance];
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pSuper, int hReturnKey)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    // mmSurfaceKeypadReturnKey -> UIReturnKeyType Map is same.
    [p setReturnKeyType:(UIReturnKeyType)hReturnKey];
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    mmMemset(p->hTextEditRect, 0, sizeof(double) * 4);
    (*(p->pISurface->OnGetTextEditRect))(p->pISurface, p->hTextEditRect);
    
    [p becomeFirstResponder];
    [p OnMenuControllerShow];
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    mmMemset(p->hTextEditRect, 0, sizeof(double) * 4);
    
    [p OnMenuControllerHide];
    [p resignFirstResponder];
}

#pragma mark -

@end
