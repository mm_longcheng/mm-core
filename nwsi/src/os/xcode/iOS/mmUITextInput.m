/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmUITextInput.h"

#import "nwsi/mmObjcARC.h"


#pragma mark - UIView - mmUITextPosition

@implementation mmUITextPosition

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Initialization code.
        self.pos = 0;
    }
    return self;
}
- (void)dealloc
{
    // Destroy code.
    mmObjc_Dealloc(super);
}

@end

#pragma mark -

#pragma mark - UIView - mmUITextRange

@implementation mmUITextRange

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Initialization code.
        self.l = [[mmUITextPosition alloc] init];
        self.r = [[mmUITextPosition alloc] init];
    }
    return self;
}
- (void)dealloc
{
    // Destroy code.
    mmObjc_Dealloc(self.l);
    mmObjc_Dealloc(self.r);
    
    mmObjc_Dealloc(super);
}

//  Whether the range is zero-length.
- (BOOL)isEmpty
{
    return (0 == self.r.pos - self.l.pos);
}
- (UITextPosition *)start
{
    return self.l;
}
- (UITextPosition *)end
{
    return self.r;
}

#pragma mark -

@end
