/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmOSVirtualKeycode.h"

#include "core/mmKeyCode.h"

#include <Carbon/Carbon.h>

static const mmUInt8_t kVKeycodeMap[256] =
{
    [kVK_ANSI_A              ] = MM_KC_A            , // = 0x00,
    [kVK_ANSI_S              ] = MM_KC_S            , // = 0x01,
    [kVK_ANSI_D              ] = MM_KC_D            , // = 0x02,
    [kVK_ANSI_F              ] = MM_KC_F            , // = 0x03,
    [kVK_ANSI_H              ] = MM_KC_H            , // = 0x04,
    [kVK_ANSI_G              ] = MM_KC_G            , // = 0x05,
    [kVK_ANSI_Z              ] = MM_KC_Z            , // = 0x06,
    [kVK_ANSI_X              ] = MM_KC_X            , // = 0x07,
    [kVK_ANSI_C              ] = MM_KC_C            , // = 0x08,
    [kVK_ANSI_V              ] = MM_KC_V            , // = 0x09,
    [kVK_ANSI_B              ] = MM_KC_B            , // = 0x0B,
    [kVK_ANSI_Q              ] = MM_KC_Q            , // = 0x0C,
    [kVK_ANSI_W              ] = MM_KC_W            , // = 0x0D,
    [kVK_ANSI_E              ] = MM_KC_E            , // = 0x0E,
    [kVK_ANSI_R              ] = MM_KC_R            , // = 0x0F,
    [kVK_ANSI_Y              ] = MM_KC_Y            , // = 0x10,
    [kVK_ANSI_T              ] = MM_KC_T            , // = 0x11,
    [kVK_ANSI_1              ] = MM_KC_1            , // = 0x12,
    [kVK_ANSI_2              ] = MM_KC_2            , // = 0x13,
    [kVK_ANSI_3              ] = MM_KC_3            , // = 0x14,
    [kVK_ANSI_4              ] = MM_KC_4            , // = 0x15,
    [kVK_ANSI_6              ] = MM_KC_6            , // = 0x16,
    [kVK_ANSI_5              ] = MM_KC_5            , // = 0x17,
    [kVK_ANSI_Equal          ] = MM_KC_EQUALS       , // = 0x18,
    [kVK_ANSI_9              ] = MM_KC_9            , // = 0x19,
    [kVK_ANSI_7              ] = MM_KC_7            , // = 0x1A,
    [kVK_ANSI_Minus          ] = MM_KC_MINUS        , // = 0x1B,
    [kVK_ANSI_8              ] = MM_KC_8            , // = 0x1C,
    [kVK_ANSI_0              ] = MM_KC_0            , // = 0x1D,
    [kVK_ANSI_RightBracket   ] = MM_KC_RBRACKET     , // = 0x1E,
    [kVK_ANSI_O              ] = MM_KC_O            , // = 0x1F,
    [kVK_ANSI_U              ] = MM_KC_U            , // = 0x20,
    [kVK_ANSI_LeftBracket    ] = MM_KC_LBRACKET     , // = 0x21,
    [kVK_ANSI_I              ] = MM_KC_I            , // = 0x22,
    [kVK_ANSI_P              ] = MM_KC_P            , // = 0x23,
    [kVK_ANSI_L              ] = MM_KC_L            , // = 0x25,
    [kVK_ANSI_J              ] = MM_KC_J            , // = 0x26,
    [kVK_ANSI_Quote          ] = MM_KC_APOSTROPHE   , // = 0x27,
    [kVK_ANSI_K              ] = MM_KC_K            , // = 0x28,
    [kVK_ANSI_Semicolon      ] = MM_KC_SEMICOLON    , // = 0x29,
    [kVK_ANSI_Backslash      ] = MM_KC_BACKSLASH    , // = 0x2A,
    [kVK_ANSI_Comma          ] = MM_KC_COMMA        , // = 0x2B,
    [kVK_ANSI_Slash          ] = MM_KC_SLASH        , // = 0x2C,
    [kVK_ANSI_N              ] = MM_KC_N            , // = 0x2D,
    [kVK_ANSI_M              ] = MM_KC_M            , // = 0x2E,
    [kVK_ANSI_Period         ] = MM_KC_PERIOD       , // = 0x2F,
    [kVK_ANSI_Grave          ] = MM_KC_GRAVE        , // = 0x32,
    [kVK_ANSI_KeypadDecimal  ] = MM_KC_NUMPADPERIOD , // = 0x41,
    [kVK_ANSI_KeypadMultiply ] = MM_KC_NUMPADSTAR   , // = 0x43,
    [kVK_ANSI_KeypadPlus     ] = MM_KC_NUMPADPLUS   , // = 0x45,
    [kVK_ANSI_KeypadClear    ] = MM_KC_UNKNOWN      , // = 0x47,
    [kVK_ANSI_KeypadDivide   ] = MM_KC_NUMPADSLASH  , // = 0x4B,
    [kVK_ANSI_KeypadEnter    ] = MM_KC_NUMPADENTER  , // = 0x4C,
    [kVK_ANSI_KeypadMinus    ] = MM_KC_NUMPADMINUS  , // = 0x4E,
    [kVK_ANSI_KeypadEquals   ] = MM_KC_NUMPADEQUALS , // = 0x51,
    [kVK_ANSI_Keypad0        ] = MM_KC_NUMPAD0      , // = 0x52,
    [kVK_ANSI_Keypad1        ] = MM_KC_NUMPAD1      , // = 0x53,
    [kVK_ANSI_Keypad2        ] = MM_KC_NUMPAD2      , // = 0x54,
    [kVK_ANSI_Keypad3        ] = MM_KC_NUMPAD3      , // = 0x55,
    [kVK_ANSI_Keypad4        ] = MM_KC_NUMPAD4      , // = 0x56,
    [kVK_ANSI_Keypad5        ] = MM_KC_NUMPAD5      , // = 0x57,
    [kVK_ANSI_Keypad6        ] = MM_KC_NUMPAD6      , // = 0x58,
    [kVK_ANSI_Keypad7        ] = MM_KC_NUMPAD7      , // = 0x59,
    [kVK_ANSI_Keypad8        ] = MM_KC_NUMPAD8      , // = 0x5B,
    [kVK_ANSI_Keypad9        ] = MM_KC_NUMPAD9      , // = 0x5C
    
    [kVK_Return              ] = MM_KC_RETURN       , // = 0x24,
    [kVK_Tab                 ] = MM_KC_TAB          , // = 0x30,
    [kVK_Space               ] = MM_KC_SPACE        , // = 0x31,
    [kVK_Delete              ] = MM_KC_BACKSPACE    , // = 0x33,
    [kVK_Escape              ] = MM_KC_ESCAPE       , // = 0x35,
    [kVK_Command             ] = MM_KC_LWIN         , // = 0x37,
    [kVK_Shift               ] = MM_KC_LSHIFT       , // = 0x38,
    [kVK_CapsLock            ] = MM_KC_CAPITAL      , // = 0x39,
    [kVK_Option              ] = MM_KC_LMENU        , // = 0x3A,
    [kVK_Control             ] = MM_KC_LCONTROL     , // = 0x3B,
    [kVK_RightShift          ] = MM_KC_RSHIFT       , // = 0x3C,
    [kVK_RightOption         ] = MM_KC_RMENU        , // = 0x3D,
    [kVK_RightControl        ] = MM_KC_RCONTROL     , // = 0x3E,
    [kVK_Function            ] = MM_KC_UNKNOWN      , // = 0x3F,
    [kVK_F17                 ] = MM_KC_UNKNOWN      , // = 0x40,
    [kVK_VolumeUp            ] = MM_KC_VOLUMEUP     , // = 0x48,
    [kVK_VolumeDown          ] = MM_KC_VOLUMEDOWN   , // = 0x49,
    [kVK_Mute                ] = MM_KC_MUTE         , // = 0x4A,
    [kVK_F18                 ] = MM_KC_UNKNOWN      , // = 0x4F,
    [kVK_F19                 ] = MM_KC_UNKNOWN      , // = 0x50,
    [kVK_F20                 ] = MM_KC_UNKNOWN      , // = 0x5A,
    [kVK_F5                  ] = MM_KC_F5           , // = 0x60,
    [kVK_F6                  ] = MM_KC_F6           , // = 0x61,
    [kVK_F7                  ] = MM_KC_F7           , // = 0x62,
    [kVK_F3                  ] = MM_KC_F3           , // = 0x63,
    [kVK_F8                  ] = MM_KC_F8           , // = 0x64,
    [kVK_F9                  ] = MM_KC_F9           , // = 0x65,
    [kVK_F11                 ] = MM_KC_F11          , // = 0x67,
    [kVK_F13                 ] = MM_KC_F13          , // = 0x69,
    [kVK_F16                 ] = MM_KC_UNKNOWN      , // = 0x6A,
    [kVK_F14                 ] = MM_KC_F14          , // = 0x6B,
    [kVK_F10                 ] = MM_KC_F10          , // = 0x6D,
    [kVK_F12                 ] = MM_KC_F12          , // = 0x6F,
    [kVK_F15                 ] = MM_KC_F15          , // = 0x71,
    [kVK_Help                ] = MM_KC_UNKNOWN      , // = 0x72,
    [kVK_Home                ] = MM_KC_HOME         , // = 0x73,
    [kVK_PageUp              ] = MM_KC_PGUP         , // = 0x74,
    [kVK_ForwardDelete       ] = MM_KC_DELETE       , // = 0x75,
    [kVK_F4                  ] = MM_KC_F4           , // = 0x76,
    [kVK_End                 ] = MM_KC_END          , // = 0x77,
    [kVK_F2                  ] = MM_KC_F2           , // = 0x78,
    [kVK_PageDown            ] = MM_KC_PGDN         , // = 0x79,
    [kVK_F1                  ] = MM_KC_F1           , // = 0x7A,
    [kVK_LeftArrow           ] = MM_KC_LEFTARROW    , // = 0x7B,
    [kVK_RightArrow          ] = MM_KC_RIGHTARROW   , // = 0x7C,
    [kVK_DownArrow           ] = MM_KC_DOWNARROW    , // = 0x7D,
    [kVK_UpArrow             ] = MM_KC_UPARROW      , // = 0x7E
    
    [kVK_ISO_Section         ] = MM_KC_UNKNOWN      , // = 0x0A
    
    [kVK_JIS_Yen             ] = MM_KC_YEN          , // = 0x5D,
    [kVK_JIS_Underscore      ] = MM_KC_UNKNOWN      , // = 0x5E,
    [kVK_JIS_KeypadComma     ] = MM_KC_UNKNOWN      , // = 0x5F,
    [kVK_JIS_Eisu            ] = MM_KC_UNKNOWN      , // = 0x66,
    [kVK_JIS_Kana            ] = MM_KC_KANA         , // = 0x68
};

MM_EXPORT_NWSI int mmOSVirtualKeycodeTranslate(int keycode)
{
    if (0 <= keycode && keycode < 256)
    {
        return kVKeycodeMap[keycode];
    }
    else
    {
        return MM_KC_UNKNOWN;
    }
}
