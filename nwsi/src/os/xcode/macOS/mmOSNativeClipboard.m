/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmOSNativeClipboard.h"

#import "core/mmAlloc.h"
#import "core/mmLogger.h"
#import "core/mmStreambuf.h"

#import "nwsi/mmNativeClipboard.h"
#import "nwsi/mmObjcARC.h"

#import <AppKit/AppKit.h>

MM_EXPORT_NWSI
void
mmOSNativeClipboard_SetByteBuffer(
    struct mmClipboardProvider*                    pSuper,
    const char*                                    pMimeType,
    struct mmByteBuffer*                           pByteBuffer)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        mmUInt8_t* buffer = pByteBuffer->buffer + pByteBuffer->offset;
        size_t length = pByteBuffer->length;

        struct mmNativeClipboard* p = (struct mmNativeClipboard*)(pSuper);

        if (0 != strcmp("text/plain", pMimeType))
        {
            break;
        }

        if (NULL == buffer || 0 == length)
        {
            break;
        }

        size_t size = length / sizeof(mmUInt16_t);
        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, length);
        mmMemcpy(p->hStreambuf.buff, buffer, length);
        
        NSPasteboard* pBboard = [NSPasteboard generalPasteboard];
        if(nil == pBboard)
        {
            break;
        }
        
        NSString* pString = [NSString stringWithCharacters:(const unichar *)buffer
                                                    length:size];
        
        mmObjc_Autorelease(pString);
        
        [pBboard clearContents];
        [pBboard writeObjects:@[pString]];
        
        mmLogger_LogV(gLogger, "mmNativeClipboard.SetByteBuffer success.");
    } while (0);
}

MM_EXPORT_NWSI
void
mmOSNativeClipboard_GetByteBuffer(
    struct mmClipboardProvider*                    pSuper,
    const char*                                    pMimeType,
    struct mmByteBuffer*                           pByteBuffer)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();

        struct mmNativeClipboard* p = (struct mmNativeClipboard*)(pSuper);

        if (0 != strcmp("text/plain", pMimeType))
        {
            break;
        }

        NSPasteboard* pBboard = [NSPasteboard generalPasteboard];
        if(nil == pBboard)
        {
            break;
        }
        
        NSString* pString = [pBboard stringForType:NSPasteboardTypeString];
        if(nil == pString)
        {
            break;
        }
        
        NSUInteger size = pString.length;
        size_t length = size * sizeof(mmUInt16_t);
        
        mmStreambuf_Reset(&p->hStreambuf);
        mmStreambuf_AlignedMemory(&p->hStreambuf, length);
        mmUInt16_t* buffer = (mmUInt16_t*)p->hStreambuf.buff;
        
        NSRange hRange = NSMakeRange(0, size);
        [pString getCharacters:(unichar *)buffer range:hRange];

        pByteBuffer->buffer = p->hStreambuf.buff;
        pByteBuffer->offset = 0;
        pByteBuffer->length = length;

        mmLogger_LogV(gLogger, "mmNativeClipboard.GetByteBuffer success.");
    } while (0);
}

