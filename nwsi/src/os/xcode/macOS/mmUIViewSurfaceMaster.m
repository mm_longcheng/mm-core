/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmUIViewSurfaceMaster.h"
#import "mmOSVirtualKeycode.h"

#import "nwsi/mmObjcARC.h"
#import "nwsi/mmISurface.h"
#import "nwsi/mmContextMaster.h"
#import "nwsi/mmSurfaceMaster.h"

#import "core/mmAlloc.h"
#import "core/mmLogger.h"
#import "core/mmKeyCode.h"
#import "core/mmUtf16String.h"
#import "core/mmKeyCode.h"
#import "core/mmASCIICode.h"

#import <QuartzCore/CAMetalLayer.h>
#import <AppKit/NSWindow.h>

@interface mmUIViewSurfaceMaster()
{
    struct mmEventSurfaceContent hSurfaceContent;
    struct mmISurfaceKeypad hISurfaceKeypad;
    
    // weak ref.
    struct mmSurfaceMaster* pSurfaceMaster;
    
    // weak ref.
    struct mmISurface* pISurface;
    
    struct mmUtf16String hUTF16String;
    
    // Text edit rect (x, y, w, h).
    double hTextEditRect[4];
    
    // cache value.
    CGSize hViewSize;
    CGRect hSafeRect;
    
    mmUInt32_t hModifierMask;
    mmUInt32_t hButtonMask;
    CGFloat hForceValue;
    CGFloat hOPosition[2];
    CGFloat hWheelDelta[2];
    
    NSUInteger hMarkedTextRange[2];

    double hDisplayDensity;
    
    CVDisplayLinkRef displayLink;
}

@property (strong, nonatomic) NSTrackingArea* trackingArea;
@property (strong, nonatomic) NSMutableAttributedString* markedText;

@property (strong, nonatomic) NSSet* selectors;

@end

@implementation mmUIViewSurfaceMaster

static CVReturn mmCVDisplayLinkOnDisplayLinkUpdate(
    CVDisplayLinkRef                               displayLink,
    const CVTimeStamp*                             now,
    const CVTimeStamp*                             outputTime,
    CVOptionFlags                                  flagsIn,
    CVOptionFlags*                                 flagsOut,
    void*                                          target);

static
void
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster);

static mmUInt32_t __static_mmOSModifierMask(NSEventModifierFlags flags);

static void __static_mmUIViewSurfaceMaster_Init(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_Destroy(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadEnter(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_ThreadLeave(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_OnSizeChange(mmUIViewSurfaceMaster* p, CGFloat fw, CGFloat fh);

static void __static_mmUIViewSurfaceMaster_AttachNotification(mmUIViewSurfaceMaster* p);
static void __static_mmUIViewSurfaceMaster_DetachNotification(mmUIViewSurfaceMaster* p);

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pSuper, int hType);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pSuper, int hAppearance);
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pSuper, int hReturnKey);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pSuper);
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pSuper);

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Initialization code.
        
        // Back the view with a layer created by the makeBackingLayer method.
        self.wantsLayer = YES;
    }
    return self;
}
- (void)dealloc
{
    // Destroy code.
    
    // when Application Terminate, the dealloc may not trigger.
    // we Terminate at ApplicationWillTerminate event.
    // explicit external call.
    mmObjc_Dealloc(super);
}

-(id)initWithCoder:(NSCoder*)pCoder
{
    self = [super initWithCoder:pCoder];
    if (self)
    {
        // Initialization code.
        
        // Back the view with a layer created by the makeBackingLayer method.
        self.wantsLayer = YES;
    }
    return self;
}

- (void)Init
{
    __static_mmUIViewSurfaceMaster_Init(self);
}

- (void)Destroy
{
    __static_mmUIViewSurfaceMaster_Destroy(self);
}

- (void)SetSurfaceMaster:(struct mmSurfaceMaster*)pSurfaceMaster
{
    void* pViewSurfaceMaster = (__bridge void*)self;
    
    self->pSurfaceMaster = pSurfaceMaster;
    self->pISurface = &self->pSurfaceMaster->hSuper;
    
    mmSurfaceMaster_SetViewSurface(pSurfaceMaster, pViewSurfaceMaster);
    mmSurfaceMaster_SetISurfaceKeypad(pSurfaceMaster, &self->hISurfaceKeypad);
}

- (void*)GetSurfaceCALayer
{
    return (__bridge void*)self.layer;
}

- (void)OnFinishLaunching
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    assert(nil != self.layer && "layer is null.");
    
    self->hViewSize = CGSizeZero;
    self->hSafeRect = CGRectZero;
    
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    
    self->hDisplayDensity = self.layer.contentsScale;

    [self PrepareTrackingAreas];
    
    // NOTE: kUTTypeURL corresponds to NSPasteboardTypeURL but is available
    //       on 10.7 without having been deprecated yet
    [self registerForDraggedTypes:@[(__bridge NSString*) kUTTypeURL]];
    
    self.selectors = [[NSSet alloc] initWithObjects:
                      NSStringFromSelector(@selector(deleteForward:)),
                      NSStringFromSelector(@selector(deleteBackward:)),
                      nil];
    
    // DisplayLink
    CVDisplayLinkCreateWithActiveCGDisplays(&self->displayLink);
    CVDisplayLinkSetOutputCallback(self->displayLink, &mmCVDisplayLinkOnDisplayLinkUpdate, (__bridge void*)self);
    
    self->hISurfaceKeypad.OnSetKeypadType = &__static_mmUIViewSurfaceMaster_OnSetKeypadType;
    self->hISurfaceKeypad.OnSetKeypadAppearance = &__static_mmUIViewSurfaceMaster_OnSetKeypadAppearance;
    self->hISurfaceKeypad.OnSetKeypadReturnKey = &__static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey;
    self->hISurfaceKeypad.OnKeypadStatusShow = &__static_mmUIViewSurfaceMaster_OnKeypadStatusShow;
    self->hISurfaceKeypad.OnKeypadStatusHide = &__static_mmUIViewSurfaceMaster_OnKeypadStatusHide;
    
    __static_mmUIViewSurfaceMaster_AttachNotification(self);
    
    __static_mmUIViewSurfaceMaster_ThreadEnter(self);
    
    __static_mmUIViewSurfaceMaster_PrepareSwapchain(self);
    
    (*(self->pISurface->OnFinishLaunching))(self->pISurface);
    
    CVDisplayLinkStart(self->displayLink);
    
    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnFinishLaunching %ux%u.", (unsigned int)w, (unsigned int)h);
}
- (void)OnBeforeTerminate
{
    struct mmLogger* gLogger = mmLogger_Instance();
    
    (*(self->pISurface->OnBeforeTerminate))(self->pISurface);
    
    __static_mmUIViewSurfaceMaster_DiscardSwapchain(self);
    
    __static_mmUIViewSurfaceMaster_ThreadLeave(self);
    
    __static_mmUIViewSurfaceMaster_DetachNotification(self);
    
    mmISurfaceKeypad_Reset(&self->hISurfaceKeypad);
    
    mmObjc_Dealloc(self.selectors);
    self.selectors = nil;
    
    CVDisplayLinkRelease(self->displayLink);
    self->displayLink = NULL;
    
    [self unregisterDraggedTypes];
    [self DiscardTrackingAreas];
    
    self->hSafeRect = CGRectZero;
    self->hViewSize = CGSizeZero;
    
    mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnBeforeTerminate.");
}

- (void)OnEnterBackground
{
    (*(self->pISurface->OnEnterBackground))(self->pISurface);
}
- (void)OnEnterForeground
{
    (*(self->pISurface->OnEnterForeground))(self->pISurface);
}

- (void)OnDisplayLinkUpdate:(id)object
{
    // Main thread here.
    // unsigned long int tid = mmCurrentThreadId();
    // printf("OnDisplayLinkUpdate thread id: %ld\n", tid);
    
    // Update.
    (*(self->pISurface->OnUpdate))(self->pISurface);
    // PeekMessage.
    __static_mmUIViewSurfaceMaster_ThreadPeekMessage(self);
    // Timewait.
    (*(self->pISurface->OnTimewait))(self->pISurface);
}

- (void)PrepareTrackingAreas
{
    NSTrackingAreaOptions options = 0;
    options |= NSTrackingMouseEnteredAndExited;
    options |= NSTrackingMouseMoved;
    options |= NSTrackingActiveInKeyWindow;
    options |= NSTrackingEnabledDuringMouseDrag;
    options |= NSTrackingCursorUpdate;
    options |= NSTrackingInVisibleRect;
    options |= NSTrackingAssumeInside;

    self.trackingArea = [[NSTrackingArea alloc] initWithRect:[self bounds]
                                                     options:options
                                                       owner:self
                                                    userInfo:nil];
    
    [self addTrackingArea:self.trackingArea];
}

- (void)DiscardTrackingAreas
{
    if (self.trackingArea != nil)
    {
        [self removeTrackingArea:self.trackingArea];
        mmObjc_Dealloc(self.trackingArea);
        self.trackingArea = nil;
    }
}

- (void)OnUpdateMask:(NSEvent *)event withPhase:(int)phase maskKey:(mmUInt32_t)button_id
{
    self->hModifierMask = __static_mmOSModifierMask(event.modifierFlags);

    if (MM_MB_UNKNOWN != button_id)
    {
        switch (phase)
        {
        case mmTouchBegan:
            self->hButtonMask |=  (1 << button_id);
            break;
        case mmTouchEnded:
        case mmTouchBreak:
            self->hButtonMask &= ~(1 << button_id);
            break;
        default:
            break;
        }
    }
}

- (void)OnTouchesDataHandler:(NSEvent *)event withPhase:(int)phase maskKey:(mmUInt32_t)button_id
{
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    
    struct mmEventTouch* t = NULL;
    
    double abs_x = 0.0;
    double abs_y = 0.0;
    double rel_x = 0.0;
    double rel_y = 0.0;
    
    mmUInt64_t timestamp;
    struct timeval tv;
    
    double force_value = self->hForceValue;
    double force_maximum = 1.0;
    
    CGSize bsize = self.bounds.size;
    CGFloat hClientW = bsize.width;
    CGFloat hClientH = bsize.height;

    CGFloat px_x = +event.locationInWindow.x;
    CGFloat px_y = -event.locationInWindow.y + hClientH;
    
    px_x = px_x < 0 ? 0 : (px_x > hClientW ? hClientW : px_x);
    px_y = px_y < 0 ? 0 : (px_y > hClientH ? hClientH : px_y);
    
    abs_x = (double)(px_x);
    abs_y = (double)(px_y);
    
    rel_x = (double)((px_x - self->hOPosition[0]));
    rel_y = (double)((px_y - self->hOPosition[1]));
    
    self->hOPosition[0] = px_x;
    self->hOPosition[1] = px_y;
    
    // clamp
    force_maximum = (0 >= force_maximum) ? 1.0 : force_maximum;
    force_value = force_value <= 0.0 ? 0.25 * force_maximum : force_value;
    force_value = force_value >= force_maximum ? 0.25 * force_maximum : force_value;
    
    mmGettimeofday(&tv, NULL);
    timestamp = (mmUInt64_t)(mmTimeval_ToUSec(&tv) / 1000);
    
    [self OnUpdateMask:event withPhase:phase maskKey:button_id];
    
    mmEventTouchs_AlignedMemory(pTouchs, 1);
    t = mmEventTouchs_GetForIndexReference(pTouchs, 0);

    t->motion_id = (uintptr_t)(1);
    t->tap_count = (int)1;
    t->phase = (int)phase;
    t->modifier_mask = (mmUInt32_t)self->hModifierMask;
    t->button_mask = (mmUInt32_t)self->hButtonMask;
    t->abs_x = (double)(abs_x);
    t->abs_y = (double)(abs_y);
    t->rel_x = (double)(rel_x);
    t->rel_y = (double)(rel_y);
    t->force_value = (double)force_value;
    t->force_maximum = (double)force_maximum;
    t->major_radius = (double)1.0;
    t->minor_radius = (double)1.0;
    t->size_value = (double)0.04;
    t->timestamp = (mmUInt64_t)timestamp;
    
    pCursor->modifier_mask = (mmUInt32_t)self->hModifierMask;
    pCursor->button_mask = (mmUInt32_t)self->hButtonMask;
    pCursor->button_id = (mmUInt32_t)button_id;
    pCursor->abs_x = (double)(abs_x);
    pCursor->abs_y = (double)(abs_y);
    pCursor->rel_x = (double)(rel_x);
    pCursor->rel_y = (double)(rel_y);
    pCursor->wheel_dx = (double)(self->hWheelDelta[0]);
    pCursor->wheel_dy = (double)(self->hWheelDelta[1]);
    pCursor->timestamp = (mmUInt64_t)timestamp;
}

- (void)OnTextDataHandler:(NSEvent *)event
{
    struct mmEventKeypad* pKeypad = &self->hSurfaceContent.hKeypad;
    
    self->hModifierMask = __static_mmOSModifierMask([event modifierFlags]);
    
    unsigned short keyCode = [event keyCode];
    NSString* characters = [event characters];
    characters = (nil == characters) ? @"" : characters;
    NSUInteger length = characters.length;
    length = length > 3 ? 3 : length;
    mmUInt16_t* buffer = (mmUInt16_t*)pKeypad->text;
    NSRange hRange = NSMakeRange(0, length);
    mmMemset(buffer, 0, sizeof(mmUInt16_t) * 4);
    pKeypad->modifier_mask = self->hModifierMask;
    pKeypad->length = (int)length;
    [characters getCharacters:(unichar *)buffer range:hRange];
    pKeypad->key = (int)mmOSVirtualKeycodeTranslate((int)keyCode);
    pKeypad->handle = 0;
}

#pragma mark -

#pragma mark - NSNotification window

- (void)OnWindowDidResize:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmLogger* gLogger = mmLogger_Instance();
    // Update code.
    
    CGSize hViewSize = self.bounds.size;
    CGRect hSafeRect = CGRectMake(0, 0, hViewSize.width, hViewSize.height);
    
    unsigned int iw = (unsigned int)hViewSize.width;
    unsigned int ih = (unsigned int)hViewSize.height;
    
    // ios Soft keyboard input method, will trigger layoutSubviews every chinese text.
    if (!CGRectEqualToRect(self->hSafeRect, hSafeRect) ||
        !CGSizeEqualToSize(self->hViewSize, hViewSize))
    {
        CGFloat fw = hViewSize.width;
        CGFloat fh = hViewSize.height;
        
        // We can only modify the width and height, not the position,
        // otherwise the input method soft keyboard animation will be incorrect.
        CGRect hMetalFrame = self.frame;
        self.frame = CGRectMake(hMetalFrame.origin.x, hMetalFrame.origin.y, fw, fh);
        
        __static_mmUIViewSurfaceMaster_OnSizeChange(self, fw, fh);

        self->hSafeRect = hSafeRect;
        self->hViewSize = hViewSize;
        
        mmLogger_LogI(gLogger, "mmUIViewSurfaceMaster.OnWindowDidResize %ux%u.", iw, ih);
    }
    else
    {
        mmLogger_LogV(gLogger, "mmUIViewSurfaceMaster.OnWindowDidResize %ux%u.", iw, ih);
    }
}

- (void)OnWindowDidMove:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
}

- (void)OnWindowDidMiniaturize:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    [self OnEnterBackground];
}

- (void)OnWindowDidDeminiaturize:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    [self OnEnterForeground];
}

- (void)OnWindowDidBecomeKey:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    [self OnEnterForeground];
}

- (void)OnWindowDidResignKey:(NSNotification *)notification
{
    // printf("%s \n", __FUNCTION__);
    [self OnEnterBackground];
}

#pragma mark -

#pragma mark - super virtual

- (void)drawRect:(NSRect)dirtyRect
{
    // printf("%s \n", __FUNCTION__);
    
    [super drawRect:dirtyRect];
}

- (void)updateTrackingAreas
{
    // printf("%s \n", __FUNCTION__);
    
    [self DiscardTrackingAreas];
    [self PrepareTrackingAreas];
    
    [super updateTrackingAreas];
}

- (BOOL)canBecomeKeyView
{
    return YES;
}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

#pragma mark - NSEvent cursor

- (void)cursorUpdate:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    [super cursorUpdate:event];
}

#pragma mark -

#pragma mark - NSEvent mouse

- (BOOL)acceptsFirstMouse:(NSEvent *)event
{
    return YES;
}

- (void)mouseDown:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchBegan maskKey:MM_MB_LBUTTON];
    
    (*(self->pISurface->OnTouchsBegan))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorBegan))(self->pISurface, pCursor);

    [super mouseDown:event];
}

- (void)mouseDragged:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchMoved maskKey:MM_MB_UNKNOWN];
    
    (*(self->pISurface->OnTouchsMoved))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorMoved))(self->pISurface, pCursor);
    
    [super mouseDragged:event];
}

- (void)mouseUp:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchEnded maskKey:MM_MB_LBUTTON];
    
    (*(self->pISurface->OnTouchsEnded))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorEnded))(self->pISurface, pCursor);
    
    [super mouseUp:event];
}

- (void)mouseMoved:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchMoved maskKey:MM_MB_UNKNOWN];
    
    (*(self->pISurface->OnTouchsMoved))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorMoved))(self->pISurface, pCursor);
    
    [super mouseMoved:event];
}

- (void)rightMouseDown:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchBegan maskKey:MM_MB_RBUTTON];
    
    (*(self->pISurface->OnTouchsBegan))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorBegan))(self->pISurface, pCursor);
    
    [super rightMouseDown:event];
}

- (void)rightMouseDragged:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchMoved maskKey:MM_MB_UNKNOWN];
    
    (*(self->pISurface->OnTouchsMoved))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorMoved))(self->pISurface, pCursor);
    
    [super rightMouseDragged:event];
}

- (void)rightMouseUp:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);

    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchEnded maskKey:MM_MB_RBUTTON];
    
    (*(self->pISurface->OnTouchsEnded))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorEnded))(self->pISurface, pCursor);

    [super rightMouseUp:event];
}

- (void)otherMouseDown:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);

    mmUInt32_t button_id = (mmUInt32_t)event.buttonNumber;
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchBegan maskKey:button_id];
    
    (*(self->pISurface->OnTouchsBegan))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorBegan))(self->pISurface, pCursor);

    [super otherMouseDown:event];
}

- (void)otherMouseDragged:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    mmUInt32_t button_id = (mmUInt32_t)event.buttonNumber;
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchMoved maskKey:button_id];
    
    (*(self->pISurface->OnTouchsMoved))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorMoved))(self->pISurface, pCursor);
    
    [super otherMouseDragged:event];
}

- (void)otherMouseUp:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    mmUInt32_t button_id = (mmUInt32_t)event.buttonNumber;
    
    struct mmEventTouchs* pTouchs = &self->hSurfaceContent.hTouchs;
    struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
    
    self->hForceValue = event.pressure;
    self->hWheelDelta[0] = 0.0;
    self->hWheelDelta[1] = 0.0;
    
    [self OnTouchesDataHandler:event withPhase:mmTouchEnded maskKey:button_id];
    
    (*(self->pISurface->OnTouchsEnded))(self->pISurface, pTouchs);
    (*(self->pISurface->OnCursorEnded))(self->pISurface, pCursor);
    
    [super otherMouseUp:event];
}

- (void)scrollWheel:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    
    double deltaX = (double)[event scrollingDeltaX];
    double deltaY = (double)[event scrollingDeltaY];

    if ([event hasPreciseScrollingDeltas])
    {
        deltaX *= 0.1;
        deltaY *= 0.1;
    }
    
    // printf("%s (%lf, %lf) \n", __FUNCTION__, deltaX, deltaY);
    
    if (fabs(deltaX) > 0.0 || fabs(deltaY) > 0.0)
    {
        struct mmEventCursor* pCursor = &self->hSurfaceContent.hCursor;
        
        self->hForceValue = 0.0;
        self->hWheelDelta[0] = deltaX;
        self->hWheelDelta[1] = deltaY;
        
        [self OnTouchesDataHandler:event withPhase:mmTouchMoved maskKey:MM_MB_UNKNOWN];
        
        (*(self->pISurface->OnCursorWheel))(self->pISurface, pCursor);
    }
    else
    {
        self->hForceValue = 0.0;
        self->hWheelDelta[0] = deltaX;
        self->hWheelDelta[1] = deltaY;
    }
    
    [super scrollWheel:event];
}

- (void)mouseExited:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    [super mouseExited:event];
}

- (void)mouseEntered:(NSEvent *)event
{
    // printf("%s \n", __FUNCTION__);
    [super mouseEntered:event];
}

#pragma mark -

#pragma mark - NSEvent key

- (void)keyDown:(NSEvent *)event
{
    // printf("%s keyCode: %d\n", __FUNCTION__, (int)event.keyCode);
    
    // const char* s = event.characters.UTF8String;
    // printf("%s keyCode: %d characters: %s\n", __FUNCTION__, (int)event.keyCode, s);

    struct mmEventKeypad* pKeypad = &self->hSurfaceContent.hKeypad;

    [self OnTextDataHandler:event];

    if ([self hasMarkedText])
    {
        // when at marked text, can not fire KeypadPressed event.
        
        // Trigger text event.
        [self interpretKeyEvents:@[event]];
    }
    else
    {
        (*(self->pISurface->OnKeypadPressed))(self->pISurface, pKeypad);
        
        // Trigger text event.
        [self interpretKeyEvents:@[event]];
    }
    
    // this line triggers system beep error, there's no beep without it.
    // [super keyDown:event];
}

- (void)keyUp:(NSEvent *)event
{
    // printf("%s keyCode: %d\n", __FUNCTION__, (int)event.keyCode);
    
    struct mmEventKeypad* pKeypad = &self->hSurfaceContent.hKeypad;

    [self OnTextDataHandler:event];

    (*(self->pISurface->OnKeypadRelease))(self->pISurface, pKeypad);
    
    // this line triggers system beep error, there's no beep without it.
    // [super keyUp:event];
}

- (void)flagsChanged:(NSEvent *)event
{
    // printf("%s keyCode: %d\n", __FUNCTION__, (int)event.keyCode);
    
    struct mmEventKeypad* pKeypad = &self->hSurfaceContent.hKeypad;

    NSEventModifierFlags modifierFlags = [event modifierFlags];
    unsigned short keyCode = [event keyCode];
    int key = mmOSVirtualKeycodeTranslate((int)keyCode);
    int modifierKey = mmKeyCodeToModifierKey(key);
    self->hModifierMask = __static_mmOSModifierMask(modifierFlags);

    mmUInt16_t* buffer = (mmUInt16_t*)pKeypad->text;
    mmMemset(buffer, 0, sizeof(mmUInt16_t) * 4);
    pKeypad->modifier_mask = self->hModifierMask;
    pKeypad->length = 0;
    pKeypad->key = key;
    pKeypad->handle = 0;

    if (self->hModifierMask & modifierKey)
    {
        (*(self->pISurface->OnKeypadPressed))(self->pISurface, pKeypad);
    }
    else
    {
        (*(self->pISurface->OnKeypadRelease))(self->pISurface, pKeypad);
    }
    
    [super flagsChanged:event];
}

#pragma mark -

#pragma mark - NSDragging dragging

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender
{
    return YES;
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
    // HACK: We don't know what to say here because we don't know what the
    //       application wants to do with the paths
    return NSDragOperationGeneric;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
    // printf("%s \n", __FUNCTION__);

    NSPasteboard* pasteboard = [sender draggingPasteboard];
    NSDictionary* options = @{NSPasteboardURLReadingFileURLsOnlyKey:@YES};
    NSArray* urls = [pasteboard readObjectsForClasses:@[[NSURL class]]
                                              options:options];
    const NSUInteger count = [urls count];
    if (0 < count)
    {
        struct mmEventDragging* pDragging = &self->hSurfaceContent.hDragging;
        
        CGSize bsize = self.bounds.size;
        // NOTE: The returned location uses base 0,1 not 0,0
        const NSPoint pos = [sender draggingLocation];
        double x = (pos.x);
        double y = (bsize.height - pos.y);
        
        const char* url = NULL;
        NSUInteger i = 0;
        
        mmEventDragging_Reset(pDragging);
        mmEventDragging_AlignedMemory(pDragging, count);
        mmEventDragging_SetPoint(pDragging, (double)x, (double)y);

        for (i = 0;i < count;++i)
        {
            url = [urls[i] fileSystemRepresentation];
            mmEventDragging_SetForIndex(pDragging, i, url);
        }
        
        (*(self->pISurface->OnDragging))(self->pISurface, pDragging);
        
        mmEventDragging_Reset(pDragging);
    }

    return YES;
}

#pragma mark -

#pragma mark - NSTextInputClient

- (void)insertText:(id)string replacementRange:(NSRange)replacementRange
{
    // NSLog(@"insertText %@", string);
    
    NSString* text = nil;

    if ([string isKindOfClass:[NSAttributedString class]])
    {
        text = [string string];
    }
    else
    {
        text = (NSString*)string;
    }
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    struct mmEventEditString* pEditString = &self->hSurfaceContent.hEditString;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize | mmEditTextCaret;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    NSUInteger length = text.length;
    NSRange hRange = NSMakeRange(0, length);
    mmUtf16String_Resize(&self->hUTF16String, length);
    mmUInt16_t* buffer = (mmUInt16_t*)mmUtf16String_Data(&self->hUTF16String);
    [text getCharacters:(unichar *)buffer range:hRange];
    
    // clear ComposingText.
    size_t hMarkedSize = (nil == self.markedText) ? 0 : self.markedText.length;
    size_t hRPos = pEditText->c;
    size_t hLPos = hRPos - hMarkedSize;
    pEditString->s = NULL;
    pEditString->l = 0;
    pEditString->o = hLPos;
    pEditString->n = hMarkedSize;
    (*(self->pISurface->OnReplaceTextUtf16))(self->pISurface, pEditString);

    pEditText->c = (size_t)(hLPos);
    pEditText->v = mmEditTextCaret;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
    
    // insert text.
    pEditString->s = buffer;
    pEditString->l = length;
    pEditString->o = 0;
    pEditString->n = 0;
    (*(self->pISurface->OnInsertTextUtf16))(self->pISurface, pEditString);
    
    // clear marked text.
    NSUInteger* hMarkedTextRange = self->hMarkedTextRange;
    hMarkedTextRange[0] = 0;
    hMarkedTextRange[1] = 0;
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
}

 - (void)doCommandBySelector:(SEL)selector
{
    // do nothing here.
    struct mmLogger* gLogger = mmLogger_Instance();
    NSString* name = NSStringFromSelector(selector);
    mmLogger_LogV(gLogger, "%s %d doCommandBySelector: %s", __FUNCTION__, __LINE__, name.UTF8String);
    if ([self.selectors containsObject:name])
    {
        // PerformSelector may cause a leak because its selector is unknown
        // [self performSelector:selector];
        
        IMP imp = [self methodForSelector:selector];
        void (*func)(id, SEL, id) = (void*)imp;
        assert(NULL != func && "func is a null.");
        func(self, selector, nil);
    }
    mmObjc_Dealloc(name);
}

- (void)setMarkedText:(id)string
        selectedRange:(NSRange)selectedRange
     replacementRange:(NSRange)replacementRange
{
    // NSLog(@"setMarkedText (%@) (%d %d) (%d %d)", string,
    //       (int)selectedRange.location, (int)selectedRange.length,
    //       (int)replacementRange.location, (int)replacementRange.length);

    NSMutableAttributedString* markedText = nil;

    if ([string isKindOfClass:[NSAttributedString class]])
    {
        markedText = [[NSMutableAttributedString alloc] initWithAttributedString:string];
    }
    else
    {
        markedText = [[NSMutableAttributedString alloc] initWithString:string];
    }
    
    NSString* text = markedText.mutableString;
    NSUInteger length = text.length;
    NSRange hRange = NSMakeRange(0, length);
    mmUtf16String_Resize(&self->hUTF16String, length);
    mmUInt16_t* buffer = (mmUInt16_t*)mmUtf16String_Data(&self->hUTF16String);
    [text getCharacters:(unichar *)buffer range:hRange];
    
    NSUInteger* hMarkedTextRange = self->hMarkedTextRange;

    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    struct mmEventEditString* pEditString = &self->hSurfaceContent.hEditString;

    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    size_t hMarkedSize = (nil == self.markedText) ? 0 : self.markedText.length;
    size_t hRPos = pEditText->c;
    size_t hLPos = hRPos - hMarkedSize;
    pEditString->s = buffer;
    pEditString->l = length;
    pEditString->o = hLPos;
    pEditString->n = hMarkedSize;
    (*(self->pISurface->OnReplaceTextUtf16))(self->pISurface, pEditString);
    
    hMarkedTextRange[0] = hLPos;
    hMarkedTextRange[1] = hLPos + length;
    
    pEditText->c = (size_t)(hLPos + length);
    pEditText->v = mmEditTextCaret | mmEditTextDirty;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
    
    // cache
    mmObjc_Dealloc(self.markedText);
    self.markedText = markedText;
}

- (void)unmarkText
{
    // NSLog(@"unmarkText");
    NSUInteger* hMarkedTextRange = self->hMarkedTextRange;
    hMarkedTextRange[0] = 0;
    hMarkedTextRange[1] = 0;
    
    mmObjc_Dealloc(self.markedText);
    self.markedText = nil;
}

- (NSRange)selectedRange
{
    // NSLog(@"selectedRange");
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextWhole | mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    NSUInteger loc = (NSUInteger)(pEditText->l);
    NSUInteger len = (NSUInteger)(pEditText->r - pEditText->l);
    // NSLog(@"selectedRange (%ld, %ld)", loc, len);
    return NSMakeRange(loc, len);
}

- (NSRange)markedRange
{
    // NSLog(@"markedRange");
    if (((nil != self.markedText)) &&([self.markedText length] > 0))
    {
        NSUInteger* r = self->hMarkedTextRange;
        // NSLog(@"markedRange (%ld, %ld)", r[0], r[1] - r[0]);
        return NSMakeRange(r[0], r[1] - r[0]);
    }
    else
    {
        // NSLog(@"markedRange (%ld, %ld)", NSNotFound, (unsigned long)0);
        return NSMakeRange(NSNotFound, 0);
    }
}

- (BOOL)hasMarkedText
{
    // NSLog(@"hasMarkedText");
    return (nil != self.markedText) && ([self.markedText length] > 0);
}

- (nullable NSAttributedString *)attributedSubstringForProposedRange:(NSRange)range
                                                         actualRange:(nullable NSRangePointer)actualRange
{
    // NSLog(@"attributedSubstringForProposedRange");
    return nil;
}

- (NSArray<NSAttributedStringKey> *)validAttributesForMarkedText;
{
    // NSLog(@"validAttributesForMarkedText");
    return [NSArray array];
}

- (NSRect)firstRectForCharacterRange:(NSRange)range
                         actualRange:(nullable NSRangePointer)actualRange
{
    // NSLog(@"firstRectForCharacterRange");
    
    // Input Method Manager Alternative column position positioning.
    
    NSRect rect;
    
    CGSize bsize = self.bounds.size;
    NSRect frame = self.window.frame;
    
    CGFloat hClientH = bsize.height;
    
    mmMemset(self->hTextEditRect, 0, sizeof(double) * 4);
    (*(self->pISurface->OnGetTextEditRect))(self->pISurface, self->hTextEditRect);
    
    CGFloat x = self->hTextEditRect[0];
    CGFloat y = self->hTextEditRect[1];
    CGFloat w = self->hTextEditRect[2];
    CGFloat h = self->hTextEditRect[3];
    
    // Flip position left bottom.
    y = hClientH - y - h;

    rect.origin.x    = (CGFloat)(x + frame.origin.x);
    rect.origin.y    = (CGFloat)(y + frame.origin.y);
    rect.size.width  = (CGFloat)(w);
    rect.size.height = (CGFloat)(h);
    
    return rect;
}

- (NSUInteger)characterIndexForPoint:(NSPoint)point
{
    // NSLog(@"characterIndexForPoint");
    return 0;
}

#pragma mark -

#pragma mark - NSResponder Command

- (void)deleteForward:(nullable id)sender
{
    // NSLog(@"deleteForward");
}

- (void)deleteBackward:(nullable id)sender
{
    // NSLog(@"deleteBackward");
}

#pragma mark -

#pragma mark - Menu Controller

- (void)copy:(nullable id)sender
{
    // NSLog(@"copy");
    (*(self->pISurface->OnInjectCopy))(self->pISurface);
}
- (void)cut:(nullable id)sender
{
    // NSLog(@"cut");
    (*(self->pISurface->OnInjectCut))(self->pISurface);
}
- (void)paste:(nullable id)sender
{
    // NSLog(@"paste");
    (*(self->pISurface->OnInjectPaste))(self->pISurface);
}

- (void)selectAll:(nullable id)sender
{
    // NSLog(@"selectAll");
    
    struct mmEventEditText* pEditText = &self->hSurfaceContent.hEditText;
    
    pEditText->v = mmEditTextRsize;
    (*(self->pISurface->OnGetText))(self->pISurface, pEditText);
    
    pEditText->l = 0;
    pEditText->r = pEditText->size;
    pEditText->v = mmEditTextRange;
    (*(self->pISurface->OnSetText))(self->pISurface, pEditText);
}

#pragma mark -

#pragma mark - CAMetalLayer

/** Indicates that the view wants to draw using the backing layer instead of using drawRect:.  */
-(BOOL) wantsUpdateLayer
{
    return YES;
}

/** Returns a Metal-compatible layer. */
+(Class)layerClass
{
    return [CAMetalLayer class];
}

/** If the wantsLayer property is set to YES, this method will be invoked to return a layer instance. */
-(CALayer*)makeBackingLayer
{
    CALayer* layer = [self.class.layerClass layer];
    CGSize viewScale = [self convertSizeToBacking: CGSizeMake(1.0, 1.0)];
    layer.contentsScale = MIN(viewScale.width, viewScale.height);
    return layer;
}

#pragma mark -

#pragma mark - UIView - static mmUIViewSurfaceMaster

static CVReturn mmCVDisplayLinkOnDisplayLinkUpdate(
    CVDisplayLinkRef                               displayLink,
    const CVTimeStamp*                             now,
    const CVTimeStamp*                             outputTime,
    CVOptionFlags                                  flagsIn,
    CVOptionFlags*                                 flagsOut,
    void*                                          target)
{
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)target;
    
    // It is not the MainSurface thread.
    // unsigned long int tid = mmCurrentThreadId();
    // printf("OnDisplayLinkUpdate thread id: %ld\n", tid);
    
    const dispatch_block_t block = ^
    {
        [p OnDisplayLinkUpdate:nil];
    };
    dispatch_async(dispatch_get_main_queue(), block);
    
    return kCVReturnSuccess;
}

static
void
__static_mmSurfaceContentMetricsAssignment(
    struct mmEventMetrics*                         pMetrics,
    void*                                          surface,
    struct mmSurfaceMaster*                        pSurfaceMaster)
{
    pMetrics->surface = surface;
    pMetrics->canvas_size[0] = pSurfaceMaster->hDisplayFrameSize[0];
    pMetrics->canvas_size[1] = pSurfaceMaster->hDisplayFrameSize[1];
    pMetrics->window_size[0] = pSurfaceMaster->hPhysicalViewSize[0];
    pMetrics->window_size[1] = pSurfaceMaster->hPhysicalViewSize[1];
    pMetrics->safety_area[0] = pSurfaceMaster->hPhysicalSafeRect[0];
    pMetrics->safety_area[1] = pSurfaceMaster->hPhysicalSafeRect[1];
    pMetrics->safety_area[2] = pSurfaceMaster->hPhysicalSafeRect[2];
    pMetrics->safety_area[3] = pSurfaceMaster->hPhysicalSafeRect[3];
}

static mmUInt32_t __static_mmOSModifierMask(NSEventModifierFlags flags)
{
    mmUInt32_t mask = 0;
    
    if (flags & NSEventModifierFlagShift      ) mask |= MM_MODIFIER_SHIFT;
    if (flags & NSEventModifierFlagControl    ) mask |= MM_MODIFIER_CONTROL;
    if (flags & NSEventModifierFlagOption     ) mask |= MM_MODIFIER_OPTION;
    if (flags & NSEventModifierFlagCommand    ) mask |= MM_MODIFIER_COMMAND;
    if (flags & NSEventModifierFlagCapsLock   ) mask |= MM_MODIFIER_CAPITAL;
    if (flags & NSEventModifierFlagNumericPad ) mask |= MM_MODIFIER_NUMLOCK;

    return mask;
}

static void __static_mmUIViewSurfaceMaster_Init(mmUIViewSurfaceMaster* p)
{
    mmEventSurfaceContent_Init(&p->hSurfaceContent);
    mmISurfaceKeypad_Init(&p->hISurfaceKeypad);
    p->pSurfaceMaster = NULL;
    p->pISurface = NULL;
    mmUtf16String_Init(&p->hUTF16String);
    mmMemset(p->hTextEditRect, 0, sizeof(p->hTextEditRect));
    p->hViewSize = CGSizeZero;
    p->hSafeRect = CGRectZero;
    p->hModifierMask = 0;
    p->hButtonMask = 0;
    p->hForceValue = 0;
    mmMemset(p->hOPosition, 0, sizeof(p->hOPosition));
    mmMemset(p->hWheelDelta, 0, sizeof(p->hWheelDelta));
    mmMemset(p->hMarkedTextRange, 0, sizeof(p->hMarkedTextRange));
    p->hDisplayDensity = 1.0;
    p->displayLink = NULL;
    
    p.trackingArea = nil;
    p.markedText = nil;
}
static void __static_mmUIViewSurfaceMaster_Destroy(mmUIViewSurfaceMaster* p)
{
    p.markedText = nil;
    p.trackingArea = nil;
    
    p->displayLink = NULL;
    p->hDisplayDensity = 1.0;
    mmMemset(p->hMarkedTextRange, 0, sizeof(p->hMarkedTextRange));
    mmMemset(p->hWheelDelta, 0, sizeof(p->hWheelDelta));
    mmMemset(p->hOPosition, 0, sizeof(p->hOPosition));
    p->hForceValue = 0;
    p->hButtonMask = 0;
    p->hModifierMask = 0;
    p->hSafeRect = CGRectZero;
    p->hViewSize = CGSizeZero;
    mmMemset(p->hTextEditRect, 0, sizeof(p->hTextEditRect));
    mmUtf16String_Destroy(&p->hUTF16String);
    p->pISurface = NULL;
    p->pSurfaceMaster = NULL;
    mmISurfaceKeypad_Destroy(&p->hISurfaceKeypad);
    mmEventSurfaceContent_Destroy(&p->hSurfaceContent);
}

static void __static_mmUIViewSurfaceMaster_ThreadPeekMessage(mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_ThreadEnter(mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_ThreadLeave(mmUIViewSurfaceMaster* p)
{
    // need do nothing here.
}

static void __static_mmUIViewSurfaceMaster_PrepareSwapchain(mmUIViewSurfaceMaster* p)
{
    do
    {
        struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
        struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
        
        struct mmLogger* gLogger = mmLogger_Instance();
        
        double hDisplayDensity = p->hDisplayDensity;
        
        const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);
        unsigned int w = 0;
        unsigned int h = 0;
        
        double hDisplayFrameSize[2];
        double hPhysicalSafeRect[4];
        
        CGSize hViewSize = p.bounds.size;
        CGRect hSafeRect = CGRectMake(0, 0, hViewSize.width, hViewSize.height);
        
        struct mmEventMetrics* pMetrics = NULL;
        
        if (NULL == p->pSurfaceMaster)
        {
            mmLogger_LogI(gLogger, "%s %d pSurfaceMaster is null.", __FUNCTION__, __LINE__);
            break;
        }
        
        w = (unsigned int)hViewSize.width;
        h = (unsigned int)hViewSize.height;
        
        p->hSafeRect = hSafeRect;
        p->hViewSize = hViewSize;
        
        pMetrics = &p->hSurfaceContent.hMetrics;
        
        hDisplayFrameSize[0] = (double)w * hDisplayDensity;
        hDisplayFrameSize[1] = (double)h * hDisplayDensity;
        
        // Safe rect.
        hPhysicalSafeRect[0] = (double)hSafeRect.origin.x;
        hPhysicalSafeRect[1] = (double)hSafeRect.origin.y;
        hPhysicalSafeRect[2] = (double)hSafeRect.size.width;
        hPhysicalSafeRect[3] = (double)hSafeRect.size.height;

        mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
        mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
        mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);

        // Logger infomation.
        mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.PrepareSwapchain");
        mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_INFO, pWindowName);
        
        // OnSurfacePrepare event.
        __static_mmSurfaceContentMetricsAssignment(pMetrics, (__bridge void*)p, p->pSurfaceMaster);
        (*(p->pISurface->OnSurfacePrepare))(p->pISurface, pMetrics);

    }while(0);
}
static void __static_mmUIViewSurfaceMaster_DiscardSwapchain(mmUIViewSurfaceMaster* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;
    __static_mmSurfaceContentMetricsAssignment(pMetrics, (__bridge void*)p, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceDiscard))(p->pISurface, pMetrics);

    mmLogger_LogI(gLogger, "Window mmUIViewSurfaceMaster.DiscardSwapchain");
}

static void __static_mmUIViewSurfaceMaster_OnSizeChange(mmUIViewSurfaceMaster* p, CGFloat fw, CGFloat fh)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    struct mmContextMaster* pContextMaster = p->pSurfaceMaster->pContextMaster;
    struct mmPackageAssets* pPackageAssets = &pContextMaster->hPackageAssets;
    struct mmEventMetrics* pMetrics = &p->hSurfaceContent.hMetrics;

    double hDisplayDensity = p->hDisplayDensity;
    double hDisplayFrameSize[2];
    double hPhysicalSafeRect[4];
    
    const char* pWindowName = mmString_CStr(&pPackageAssets->hPackageName);
    
    CGRect hSafeRect = CGRectMake(0, 0, fw, fh);

    // Display frame size.
    hDisplayFrameSize[0] = (double)(fw * hDisplayDensity);
    hDisplayFrameSize[1] = (double)(fh * hDisplayDensity);

    // Safe rect.
    hPhysicalSafeRect[0] = (double)hSafeRect.origin.x;
    hPhysicalSafeRect[1] = (double)hSafeRect.origin.y;
    hPhysicalSafeRect[2] = (double)hSafeRect.size.width;
    hPhysicalSafeRect[3] = (double)hSafeRect.size.height;
    
    mmSurfaceMaster_SetDisplayDensity(p->pSurfaceMaster, hDisplayDensity);
    mmSurfaceMaster_SetDisplayFrameSize(p->pSurfaceMaster, hDisplayFrameSize);
    mmSurfaceMaster_SetPhysicalSafeRect(p->pSurfaceMaster, hPhysicalSafeRect);

    // Logger infomation.
    mmLogger_LogT(gLogger, "Window mmUIViewSurfaceMaster.NativeOnSurfaceChanged");
    mmSurfaceMaster_LoggerInfomation(p->pSurfaceMaster, MM_LOG_TRACE, pWindowName);
    
    // Fire event for size change.
    __static_mmSurfaceContentMetricsAssignment(pMetrics, (__bridge void*)p, p->pSurfaceMaster);
    (*(p->pISurface->OnSurfaceChanged))(p->pISurface, pMetrics);
}

static void __static_mmUIViewSurfaceMaster_AttachNotification(mmUIViewSurfaceMaster* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify addObserver:p selector:@selector(OnWindowDidResize:)
                    name:NSWindowDidResizeNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnWindowDidMove:)
                    name:NSWindowDidMoveNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnWindowDidMiniaturize:)
                    name:NSWindowDidMiniaturizeNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnWindowDidDeminiaturize:)
                    name:NSWindowDidDeminiaturizeNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnWindowDidBecomeKey:)
                    name:NSWindowDidBecomeKeyNotification
                  object:nil];
    
    [pNotify addObserver:p selector:@selector(OnWindowDidResignKey:)
                    name:NSWindowDidResignKeyNotification
                  object:nil];
}
static void __static_mmUIViewSurfaceMaster_DetachNotification(mmUIViewSurfaceMaster* p)
{
    NSNotificationCenter* pNotify = [NSNotificationCenter defaultCenter];
    
    [pNotify removeObserver:p name:NSWindowDidResizeNotification object:nil];
    [pNotify removeObserver:p name:NSWindowDidMoveNotification object:nil];
    [pNotify removeObserver:p name:NSWindowDidMiniaturizeNotification object:nil];
    [pNotify removeObserver:p name:NSWindowDidDeminiaturizeNotification object:nil];
    [pNotify removeObserver:p name:NSWindowDidBecomeKeyNotification object:nil];
    [pNotify removeObserver:p name:NSWindowDidResignKeyNotification object:nil];
}

static void __static_mmUIViewSurfaceMaster_OnSetKeypadType(struct mmISurface* pSuper, int hType)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadAppearance(struct mmISurface* pSuper, int hAppearance)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_OnSetKeypadReturnKey(struct mmISurface* pSuper, int hReturnKey)
{
    // need do nothing here.
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusShow(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    mmMemset(p->hTextEditRect, 0, sizeof(double) * 4);
    (*(p->pISurface->OnGetTextEditRect))(p->pISurface, p->hTextEditRect);
    
    [p becomeFirstResponder];
    // [p OnMenuControllerShow];
}
static void __static_mmUIViewSurfaceMaster_OnKeypadStatusHide(struct mmISurface* pSuper)
{
    struct mmSurfaceMaster* pSurfaceMaster = (struct mmSurfaceMaster*)pSuper;
    mmUIViewSurfaceMaster* p = (__bridge mmUIViewSurfaceMaster*)(pSurfaceMaster->pViewSurface);
    
    mmMemset(p->hTextEditRect, 0, sizeof(double) * 4);
    
    // [p OnMenuControllerHide];
    [p resignFirstResponder];
}

#pragma mark -

@end
