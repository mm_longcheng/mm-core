/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#import "mmOSDevice.h"

#import <AppKit/AppKit.h>
#import <sys/utsname.h>

static void mmOSDeviceSysctlbyname(const char* name, struct mmString* value)
{
    size_t len = 0;
    sysctlbyname(name, NULL, &len, NULL, 0);
    if (0 != len)
    {
        char* data = NULL;
        mmString_Resize(value, len);
        data = (char*)mmString_Data(value);
        sysctlbyname(name, data, &len, NULL, 0);
    }
}

MM_EXPORT_NWSI void mmOSDeviceInformation(struct mmDevice* p)
{
    struct utsname systemInfo;
    
    struct mmString hValue;

    char opversion[64] = { 0 };
    const char* osversion = "";
    char hSystemName[64] = { 0 };
    NSHost* host = [NSHost currentHost];
    NSProcessInfo* pProcessInfo = NSProcessInfo.processInfo;
    NSOperatingSystemVersion osv = [pProcessInfo operatingSystemVersion];
    NSString* hostname = [host name];
    
    // uname.
    uname(&systemInfo);
    
    mmString_Init(&hValue);
    
    // "20F71"
    mmOSDeviceSysctlbyname("kern.osversion", &hValue);
    osversion = mmString_CStr(&hValue);
    // "11.4.0"
    mmSprintf(opversion, "%ld.%ld.%ld", osv.majorVersion, osv.minorVersion, osv.patchVersion);
    mmSprintf(hSystemName, "macOS %s (%s)", opversion, osversion);
    
    // "My's MacBook-Pro.local"
    mmString_Assigns(&p->hName, [hostname UTF8String]);
    // "MacBookPro12,1"
    mmOSDeviceSysctlbyname("hw.model", &p->hModel);
    // "MacBookPro12,1"
    mmString_Assign(&p->hLocalizedModel, &p->hModel);
    // macOS 11.4 (20F71)
    mmString_Assigns(&p->hSystemName, hSystemName);
    // "11.4.0"
    mmString_Assigns(&p->hSystemVersion, opversion);
    
    mmString_Assigns(&p->hSysName, systemInfo.sysname);
    mmString_Assigns(&p->hNodeName, systemInfo.nodename);
    mmString_Assigns(&p->hRelease, systemInfo.release);
    mmString_Assigns(&p->hVersion, systemInfo.version);
    mmString_Assigns(&p->hMachine, systemInfo.machine);
    
    mmString_Destroy(&hValue);
}

