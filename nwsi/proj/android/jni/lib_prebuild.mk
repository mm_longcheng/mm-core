LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libminizip_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-libx/build/zlib/proj_$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libminizip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_core_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/mm/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_core_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_dish_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_dish_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_unicode_shared
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_unicode_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libmm_unicode_static
LOCAL_SRC_FILES := $(MM_HOME)/mm-core/dish/proj/$(MM_PLATFORM)/obj/local/$(TARGET_ARCH_ABI)/libmm_unicode_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################