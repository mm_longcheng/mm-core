/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

@SuppressWarnings({"unused"})
public class mmCryptAES
{
    private static final String TAG = mmCryptAES.class.getSimpleName();

    /**
     * Encryption Algorithm
     */
    private static final String MM_KEY_ALGORITHM = "AES";

    /**
     * AES key length, 32 bytes, range: 16-32 bytes
     */
    public static final int MM_SECRET_KEY_LENGTH = 32;

    /**
     * Character Encoding
     */
    private static final String MM_CHARSET_UTF8 = "utf-8";

    /**
     * When the key length is less than 16 bytes, the default padding bits
     */
    private static final String MM_DEFAULT_VALUE = "0";
    /**
     * Encryption and decryption algorithm/working mode/filling method
     */
    private static final String MM_CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";

    /**
     * Decode Base64 string into byte array.
     */
    public static byte[] Base64Decode(String pData)
    {
        return Base64.decode(pData, Base64.NO_WRAP);
    }

    /**
     * Encode byte array into Base64 string.
     */
    public static String Base64Encode(byte[] pData)
    {
        return Base64.encodeToString(pData, Base64.NO_WRAP);
    }

    /**
     * If the AES key is less than the length of {@code length},
     * the key will be supplemented to ensure the security of the key.
     *
     * @param secretKey secret key
     * @param length    The length of the key
     * @param text      Default supplemented text
     * @return key
     */
    @SuppressWarnings("SameParameterValue")
    private static String MakeKey(String secretKey, int length, String text)
    {
        // Get key length
        int strLen = secretKey.length();
        // Determine whether the length is less than the expected length
        if (strLen < length)
        {
            // Complementary digits
            StringBuilder builder = new StringBuilder();
            // Add the key to the builder
            builder.append(secretKey);
            // Add default text
            for (int i = 0; i < length - strLen; i++)
            {
                builder.append(text);
            }
            // 赋值
            secretKey = builder.toString();
        }
        return secretKey;
    }

    /**
     * Use password to get AES key
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static SecretKeySpec GetSecretKey(String secretKey)
    {
        try
        {
            secretKey = MakeKey(secretKey, MM_SECRET_KEY_LENGTH, MM_DEFAULT_VALUE);
            byte[] pKey = secretKey.getBytes(MM_CHARSET_UTF8);
            return new SecretKeySpec(pKey, MM_KEY_ALGORITHM);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * AES encrypt
     *
     * @param data      Content to be encrypted
     * @param secretKey Encryption password, length: 16 or 32 characters
     * @return Returns the encrypted data after Base64 transcoding
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static String Encrypt(String data, String secretKey)
    {
        try
        {
            // Create a cipher
            Cipher cipher = Cipher.getInstance(MM_CIPHER_ALGORITHM);
            // Initialize as an encryption cipher
            cipher.init(Cipher.ENCRYPT_MODE, GetSecretKey(secretKey));

            // 1 byte for IV size byte is approximately 16.
            byte[] pIV = cipher.getIV();
            byte[] pBN = cipher.doFinal(data.getBytes(MM_CHARSET_UTF8));
            int hIVSize = pIV.length;
            int hBNSize = pBN.length;
            int hSESize = hIVSize + hBNSize + 1;
            byte[] pBytes = new byte[hSESize];
            pBytes[0] = (byte)pIV.length;
            System.arraycopy(pIV, 0, pBytes, 1, hIVSize);
            System.arraycopy(pBN, 0, pBytes, 1 + hIVSize, hBNSize);

            // Encode the encrypted data with Base64
            return Base64Encode(pBytes);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * AES decrypt
     *
     * @param base64Data Encrypted ciphertext Base64 string
     * @param secretKey  Decryption key, length: 16 or 32 characters
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static String Decrypt(String base64Data, String secretKey)
    {
        try
        {
            // Base64 decode the encrypted data
            byte[] pBytes = Base64Decode(base64Data);

            // 1 byte for IV size byte is approximately 16.
            int hSESize = pBytes.length;
            int hIVSize = pBytes[0];
            int hBNSize = hSESize - hIVSize - 1;
            byte[] pIV = new byte[hIVSize];
            byte[] pBN = new byte[hBNSize];
            System.arraycopy(pBytes, 1, pIV, 0, hIVSize);
            System.arraycopy(pBytes, 1 + hIVSize, pBN, 0, hBNSize);

            IvParameterSpec pIVSpec = new IvParameterSpec(pIV);

            // Create a cipher
            Cipher cipher = Cipher.getInstance(MM_CIPHER_ALGORITHM);
            // Set to decrypt mode
            cipher.init(Cipher.DECRYPT_MODE, GetSecretKey(secretKey), pIVSpec);
            // Perform decryption operation
            byte[] result = cipher.doFinal(pBN);
            return new String(result, MM_CHARSET_UTF8);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}