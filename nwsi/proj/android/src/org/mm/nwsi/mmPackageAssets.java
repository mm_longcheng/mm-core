/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import java.io.File;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

@SuppressWarnings({"unused", "JavaJniMissingFunction"})
public class mmPackageAssets
{
    // ===========================================================
    // Fields
    // ===========================================================
    private static final String TAG = mmPackageAssets.class.getSimpleName();
    
    // weak reference.
    public long hNativePtr = 0;
    
    // weak reference.
    public Activity pActivity = null;
    
    // weak reference.
    public ApplicationInfo pApplicationInfo = null;
    // weak reference.
    public PackageManager pPackageManager = null;
    // weak reference.
    public PackageInfo pPackageInfo = null;
    // weak reference.
    public AssetManager pAssetManager = null;
    
    public String pPackageName = "";
    public String pVersionName = "";
    public String pVersionCode = "";
    public String pDisplayName = "";
    
    public String pHomeDirectory = "";

    public String pInternalFilesDirectory = "";
    public String pInternalCacheDirectory = "";
    public String pExternalFilesDirectory = "";
    public String pExternalCacheDirectory = "";
    
    public String pPluginFolder = "";
    public String pDynlibFolder = "";

    public String pAssetsSource = "";
    public String pShaderCacheFolder = "";
    
    public int hOrientation = 0;
    public int hGLESVersion = 0;
    
    public int hDisplayRotation = 0;
    public float hDisplayRefreshRate = 0.0f;
    
    public mmPackageAssets()
    {

    }

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetActivity(Activity activity)
    {
        this.pActivity = activity;
    }
    public Activity GetActivity()
    {
        return this.pActivity;
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    @TargetApi(Build.VERSION_CODES.P)
    public String GetVersionCode()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) 
        {
            // Deprecated in API level 16
            int hVersionCode = this.pPackageInfo.versionCode;
            return Integer.toString(hVersionCode);
        }
        else
        {
            long hVersionCode =  this.pPackageInfo.getLongVersionCode();
            return Long.toString(hVersionCode);
        }
    }
    
    @SuppressWarnings("ConstantConditions")
    public void OnFinishLaunching()
    {
        do
        {
            if (null == this.pActivity)
            {
                Log.e(TAG, TAG + " pActivity is a null.");
                break;
            }
            if (0 == this.hNativePtr)
            {
                Log.e(TAG, TAG + " hNativePtr is 0.");
                break;
            }            
            
            File pFilesDir = this.pActivity.getFilesDir();
            File pCacheDir = this.pActivity.getCacheDir();
            File pExternalFilesDir = this.pActivity.getExternalFilesDir(null);
            File pExternalCacheDir = this.pActivity.getExternalCacheDir();

            if (null == pFilesDir)
            {
                Log.e(TAG, TAG + " pFilesDir is a null.");
                break;
            }
            if (null == pCacheDir)
            {
                Log.e(TAG, TAG + " pCacheDir is a null.");
                break;
            }
            if (null == pExternalFilesDir)
            {
                Log.i(TAG, TAG + " pExternalFilesDir is a null.");
                break;
            }
            if (null == pExternalCacheDir)
            {
                Log.i(TAG, TAG + " pExternalCacheDir is a null.");
                break;
            }
            
            this.pApplicationInfo = this.pActivity.getApplicationInfo();
            this.pPackageName = this.pApplicationInfo.packageName;
            this.pPluginFolder = this.pApplicationInfo.nativeLibraryDir;
            this.pDynlibFolder = this.pApplicationInfo.nativeLibraryDir;
            this.pPackageManager = this.pActivity.getPackageManager();

            File pHomeDirectoryDir = pExternalFilesDir.getParentFile();

            if (null == pHomeDirectoryDir)
            {
                Log.i(TAG, TAG + " pHomeDirectoryDir is a null.");
                break;
            }

            try
            {
                this.pPackageInfo = this.pPackageManager.getPackageInfo(this.pPackageName, 0);
            }
            catch (NameNotFoundException e)
            {
                e.printStackTrace();
            }
            
            if(null == this.pPackageInfo)
            {
                Log.i(TAG, TAG + " pPackageInfo is a null.");
                break;
            }

            this.pHomeDirectory = pHomeDirectoryDir.getAbsolutePath();

            this.pInternalFilesDirectory = pFilesDir.getAbsolutePath();
            this.pInternalCacheDirectory = pCacheDir.getAbsolutePath();
            this.pExternalFilesDirectory = pExternalFilesDir.getAbsolutePath();
            this.pExternalCacheDirectory = pExternalCacheDir.getAbsolutePath();
            
            this.pAssetsSource = this.pApplicationInfo.sourceDir;
            this.pAssetManager = this.pActivity.getAssets();
            
            Resources pResources = this.pActivity.getResources();
            Configuration pConfiguration = pResources.getConfiguration();
            this.hOrientation = pConfiguration.orientation;
            
            Object pActivityService = this.pActivity.getSystemService(Context.ACTIVITY_SERVICE);
            ActivityManager pActivityManager = (ActivityManager)pActivityService;
            ConfigurationInfo info = pActivityManager.getDeviceConfigurationInfo();
            this.hGLESVersion = info.reqGlEsVersion;
            
            Object pWindowService = this.pActivity.getSystemService(Context.WINDOW_SERVICE);
            WindowManager pWindowManager = (WindowManager)pWindowService;
            Display pDefaultDisplay = pWindowManager.getDefaultDisplay();
            this.hDisplayRefreshRate = pDefaultDisplay.getRefreshRate();
            this.hDisplayRotation = pDefaultDisplay.getRotation();
            
            int hLabelRes = this.pPackageInfo.applicationInfo.labelRes;
            this.pVersionName = this.pPackageInfo.versionName;
            this.pVersionCode = this.GetVersionCode();
            this.pDisplayName = pResources.getString(hLabelRes);
            
            // logger information.
            Log.i(TAG, "Assets mmPackageAssets.OnFinishLaunching");
            Log.i(TAG, "Assets ----------------------------+----------------------------------");
            Log.i(TAG, "Assets               pAssetsSource : " + this.pAssetsSource);
            Log.i(TAG, "Assets                hOrientation : " + this.hOrientation);
            Log.i(TAG, "Assets                hGLESVersion : " + String.format("%08x", this.hGLESVersion));
            Log.i(TAG, "Assets         hDisplayRefreshRate : " + this.hDisplayRefreshRate);
            Log.i(TAG, "Assets            hDisplayRotation : " + this.hDisplayRotation);
            Log.i(TAG, "Assets ----------------------------+----------------------------------");
            
            // Native API.
            this.NativeSetAssetManager(this.pAssetManager);
            
            this.NativeSetPackageName(this.pPackageName);
            this.NativeSetVersionName(this.pVersionName);
            this.NativeSetVersionCode(this.pVersionCode);
            this.NativeSetDisplayName(this.pDisplayName);
            
            this.NativeSetPluginFolder(this.pPluginFolder);
            this.NativeSetDynlibFolder(this.pDynlibFolder);

            this.NativeSetHomeDirectory(this.pHomeDirectory);

            this.NativeSetInternalFilesDirectory(this.pInternalFilesDirectory);
            this.NativeSetInternalCacheDirectory(this.pInternalCacheDirectory);
            this.NativeSetExternalFilesDirectory(this.pExternalFilesDirectory);
            this.NativeSetExternalCacheDirectory(this.pExternalCacheDirectory);
            
            // set assets root.
            this.NativeSetAssetsRootSource(this.pAssetsSource, "assets");
        }while(false);
        
        this.NativeOnFinishLaunching();
    }
    public void OnBeforeTerminate()
    {
        this.NativeOnBeforeTerminate();
        
        if (null != this.pActivity)
        {
            this.pApplicationInfo = null;
            this.pPackageManager = null;
            this.pPackageInfo = null;
            this.pAssetManager = null;
            
            this.pPackageName = "";
            this.pVersionName = "";
            this.pVersionCode = "";
            this.pDisplayName = "";
            
            this.pInternalFilesDirectory = "";
            this.pInternalCacheDirectory = "";
            this.pExternalFilesDirectory = "";
            this.pExternalCacheDirectory = "";
            
            this.pAssetsSource = "";
            this.pShaderCacheFolder = "";
            
            this.hOrientation = 0;
            this.hGLESVersion = 0;
            
            this.hDisplayRotation = 0;
            this.hDisplayRefreshRate = 0.0f;
        }
    }
    
    // ===========================================================
    // Methods
    // ===========================================================
    public ApplicationInfo GetApplicationInfo() 
    {
        return this.pApplicationInfo;
    }
    
    public PackageManager GetPackageManager() 
    {
        return this.pPackageManager;
    }
    
    public PackageInfo GetPackageInfo() 
    {
        return this.pPackageInfo;
    }
     
    public String GetPackageName() 
    {
        return this.pPackageName;
    }
    
    public String GetAssetsSource() 
    {
        return this.pAssetsSource;
    }

    public String GetInternalFilesDirectory() 
    {
        return this.pInternalFilesDirectory;
    }
    public String GetInternalCacheDirectory() 
    {
        return this.pInternalCacheDirectory;
    }
    public String GetExternalFilesDirectory() 
    {
        return this.pExternalFilesDirectory;
    }
    public String GetExternalCacheDirectory() 
    {
        return this.pExternalCacheDirectory;
    }
    public String GetShaderCacheFolder() 
    {
        return this.pShaderCacheFolder;  
    }
    
    public AssetManager GetAssetManager() 
    {
        return this.pAssetManager;
    }
    public int GetOrientation() 
    {
        return this.hOrientation;
    }
    public int GetGLESVersion() 
    {
        return this.hGLESVersion;
    }
    // the angle = rotation * 90
    public int GetDefaultDisplayRotation() 
    {
        return this.hDisplayRotation;
    }
    public float GetDefaultDisplayRefreshRate() 
    {
        return this.hDisplayRefreshRate;
    } 
    // static 
    public static String GetCurrentLanguage()
    {
        Locale pLocale = Locale.getDefault();
        return pLocale.getLanguage();
    }
    
    public static String GetDeviceModel()
    {
        return Build.MODEL;
    }
    
    // ===========================================================
    // native java -> c
    // ===========================================================
    public native void NativeSetAssetManager(AssetManager pAssetManager);
    
    public native void NativeSetPackageName(String pPackageName);
    public native void NativeSetVersionName(String pVersionName);
    public native void NativeSetVersionCode(String pVersionCode);
    public native void NativeSetDisplayName(String pDisplayName);

    public native void NativeSetHomeDirectory(String pHomeDirectory);

    public native void NativeSetInternalFilesDirectory(String pInternalFilesDirectory);
    public native void NativeSetInternalCacheDirectory(String pInternalCacheDirectory);
    public native void NativeSetExternalFilesDirectory(String pExternalFilesDirectory);
    public native void NativeSetExternalCacheDirectory(String pExternalCacheDirectory);

    public native void NativeSetPluginFolder(String pPluginFolder);
    public native void NativeSetDynlibFolder(String pDynlibFolder);
    public native void NativeSetLoggerPathName(String pLoggerPathName);
    
    public native void NativeSetAssetsRootFolder(String pAssetsRootFolderPath, String pAssetsRootFolderBase);
    public native void NativeSetAssetsRootSource(String pAssetsRootSourcePath, String pAssetsRootSourceBase);
    
    public native void NativeOnFinishLaunching();
    public native void NativeOnBeforeTerminate();
}