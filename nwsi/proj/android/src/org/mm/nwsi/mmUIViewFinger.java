/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.mm.nwsi.mmISurface.mmEventTouch;

import android.util.Log;

@SuppressWarnings("unused")
public class mmUIViewFinger
{
    private static final String TAG = mmUIViewFinger.class.getSimpleName();
    
    @SuppressWarnings("Convert2Diamond")
    public TreeMap<Long, mmEventTouch> hFingerCache = new TreeMap<Long, mmEventTouch>();

    public mmUIViewFinger()
    {

    }

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }

    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }

    public void SetFinger(long motion_id, mmEventTouch touch)
    {
        mmEventTouch e = new mmEventTouch();

        // Copy Value.
        e.motion_id = touch.motion_id;
        e.tap_count = touch.tap_count;
        e.phase = touch.phase;
        e.modifier_mask = touch.modifier_mask;
        e.button_mask = touch.button_mask;
        e.abs_x = touch.abs_x;
        e.abs_y = touch.abs_y;
        e.rel_x = touch.rel_x;
        e.rel_y = touch.rel_y;
        e.force_value = touch.force_value;
        e.force_maximum = touch.force_maximum;
        e.major_radius = touch.major_radius;
        e.minor_radius = touch.minor_radius;
        e.size_value = touch.size_value;
        e.timestamp = touch.timestamp;

        this.hFingerCache.put(motion_id, e);
    }
    public mmEventTouch GetFinger(long motion_id)
    {
        return this.hFingerCache.get(motion_id);
    }
    public void ClearFinger()
    {
        this.hFingerCache.clear();
    }
    @SuppressWarnings({"WhileLoopReplaceableByForEach", "UnusedAssignment"})
    public void ClearFingerValue()
    {
        mmEventTouch e = null;
        Map.Entry<Long, mmEventTouch> pEntry = null;
        Iterator<Map.Entry<Long, mmEventTouch>> it = this.hFingerCache.entrySet().iterator();
        while (it.hasNext())
        {
            pEntry = it.next();
            e = pEntry.getValue();

            // Can not clear e.motion_id.
            e.tap_count = 0;
            e.phase = mmISurface.mmTouchBegan;
            e.modifier_mask = 0;
            e.button_mask = 0;
            e.abs_x = 0;
            e.abs_y = 0;
            e.rel_x = 0;
            e.rel_y = 0;
            e.force_value = 0.25;
            e.force_maximum = 1.0;
            e.major_radius = 1;
            e.minor_radius = 1;
            e.size_value = 1;
            e.timestamp = 0;
        }
    }
    public void SupplementCacheFinger(mmEventTouch e)
    {
        // Previous Touch.
        mmEventTouch f = this.GetFinger(e.motion_id);
        // Completing relative coordinates.
        if(null == f)
        {
            e.rel_x = 0;
            e.rel_y = 0;
        }
        else
        {
            e.rel_x = e.abs_x - f.abs_x;
            e.rel_y = e.abs_y - f.abs_y;
        }
        // Current Touch.
        this.SetFinger(e.motion_id, e);
    }
}