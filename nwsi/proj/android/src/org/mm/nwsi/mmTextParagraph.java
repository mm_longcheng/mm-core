package org.mm.nwsi;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;

import org.mm.core.mmLogger;
import org.mm.math.mmRange;

import java.text.Bidi;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@SuppressWarnings({"unused"})
public class mmTextParagraph
{
    private static final String TAG = mmTextParagraph.class.getSimpleName();

    public mmTextParagraphFormat pParagraphFormat = null;
    public mmTextSpannable pTextSpannable = null;
    public mmTextUtf16String pText = null;
    public mmTextDrawingContext pDrawingContext = null;
    
    public final TextPaint pTextPaint = new TextPaint();
    public final SpannableStringBuilder pSpannableStringBuilder = new SpannableStringBuilder();
    public StaticLayout pLayout = null;
    public Bidi pBidi = null;
    public float[] hOrigin = new float[]{ 0.0f, 0.0f, };

    public void Init()
    {

    }
    public void Destroy()
    {

    }

    public void SetTextParagraphFormat(mmTextParagraphFormat pParagraphFormat)
    {
        this.pParagraphFormat = pParagraphFormat;
    }
    public void SetTextSpannable(mmTextSpannable pTextSpannable)
    {
        this.pTextSpannable = pTextSpannable;
        this.pTextSpannable.SetMasterFormat(this.pParagraphFormat.hMasterFormat);
    }

    public void SetText(mmTextUtf16String pText)
    {
        this.pText = pText;
    }
    public void SetDrawingContext(mmTextDrawingContext pDrawingContext)
    {
        this.pDrawingContext = pDrawingContext;
    }

    public void OnFinishLaunching()
    {
        this.UpdateTextImplement();
        this.UpdateTextFormat();
        this.UpdateFontFeature();
        this.UpdateTextSpannable();
        this.UpdateTextLayout();
    }

    public void OnBeforeTerminate()
    {
        this.DeleteTextFormat();
        this.DeleteTextLayout();
    }

    public void UpdateTextImplement()
    {
        this.pSpannableStringBuilder.clear();
        this.pSpannableStringBuilder.append(this.pText);
    }

    public void UpdateTextFormat()
    {
        if (null != this.pParagraphFormat)
        {
            mmTextFormat pMasterFormat = this.pParagraphFormat.hMasterFormat;
            mmGraphicsFactory pGraphicsFactory = this.pDrawingContext.pGraphicsFactory;

            float hFontSize = mmTextPlatform.GetFontSize(pMasterFormat.hFontSize);
            float hFontPixelSize = hFontSize * this.pParagraphFormat.hDisplayDensity;
            float hObliqueSkewX = mmTextPlatform.GetFontObliqueSkewX(pMasterFormat.hFontStyle);
            Typeface pTypeface = mmTextPlatform.MakeTypeface(pGraphicsFactory, pMasterFormat);
            Paint.Cap hStrokeCap = mmGraphicsPlatform.GetCapStyle(this.pParagraphFormat.hLineCap);
            Paint.Join hLineJoin = mmGraphicsPlatform.GetLineJoin(this.pParagraphFormat.hLineJoin);
            float hLineMiterLimit = this.pParagraphFormat.hLineMiterLimit;

            this.pTextPaint.setTypeface(pTypeface);
            this.pTextPaint.setTextSkewX(hObliqueSkewX);
            this.pTextPaint.setTextSize(hFontPixelSize);

            this.pTextPaint.setStrokeCap(hStrokeCap);
            this.pTextPaint.setStrokeJoin(hLineJoin);
            this.pTextPaint.setStrokeMiter(hLineMiterLimit);

            mmTextFormat hMasterFormat = this.pParagraphFormat.hMasterFormat;
            int hForegroundColor = mmGraphicsPlatform.ColorMake(hMasterFormat.hForegroundColor);
            this.pTextPaint.setColor(hForegroundColor);
            this.pTextPaint.setStrokeWidth(0.0f);
            this.pTextPaint.setStyle(TextPaint.Style.STROKE);

            this.pTextPaint.setAntiAlias(true);
        }
        else
        {
            mmLogger.LogT(TAG + " UpdateTextFormat pParagraphFormat is null.");
        }
    }

    @SuppressWarnings("ConstantConditions")
    public void UpdateTextLayout()
    {
        do
        {
            if (null == this.pParagraphFormat)
            {
                mmLogger.LogT(TAG + " UpdateTextLayout pParagraphFormat is null.");
                break;
            }

            if (null == this.pText)
            {
                mmLogger.LogT(TAG + " UpdateTextLayout pText is null.");
                break;
            }

            String s = new String(this.pText.buffer, this.pText.offset, this.pText.length);
            this.pBidi = new Bidi(s, 0);

            float hLayoutW = this.pParagraphFormat.hLayoutRect[2];
            float hLayoutH = this.pParagraphFormat.hLayoutRect[3];

            if (1 == this.pParagraphFormat.hAlignBaseline)
            {
                float[] wsize = new float[2];
                Paint.FontMetrics fm = this.pTextPaint.getFontMetrics();
                wsize[0] = this.pTextPaint.measureText(s);
                wsize[1] = fm.descent - fm.ascent + fm.leading;

                wsize[0] = hLayoutW <= 0.0f ? wsize[0] : hLayoutW;
                wsize[1] = hLayoutH <= 0.0f ? wsize[1] : hLayoutH;

                this.pParagraphFormat.hLineDescent = fm.descent;
                this.pParagraphFormat.hSuitableSize[0] = wsize[0];
                this.pParagraphFormat.hSuitableSize[1] = wsize[1];

                this.pParagraphFormat.hWindowSize[0] = wsize[0];
                this.pParagraphFormat.hWindowSize[1] = wsize[1];

                this.pParagraphFormat.hLayoutRect[0] = 0.0f;
                this.pParagraphFormat.hLayoutRect[1] = 0.0f;
                this.pParagraphFormat.hLayoutRect[2] = wsize[0];
                this.pParagraphFormat.hLayoutRect[3] = wsize[1];

                this.pParagraphFormat.NativeUpdateSuitableData();
            }
            else
            {
                float[] wsize = new float[2];
                Paint.FontMetrics fm = this.pTextPaint.getFontMetrics();
                wsize[0] = this.pParagraphFormat.hWindowSize[0];
                wsize[1] = this.pParagraphFormat.hWindowSize[1];
                this.pParagraphFormat.hLineDescent = fm.descent;
                this.pParagraphFormat.hSuitableSize[0] = wsize[0];
                this.pParagraphFormat.hSuitableSize[1] = wsize[1];
            }

            if(Build.VERSION_CODES.M <= Build.VERSION.SDK_INT)
            {
                Layout_VersionM();
            }
            else
            {
                Layout_VersionFallback();
            }

        } while(false);
    }
    @SuppressWarnings("UnusedAssignment")
    public void UpdateFontFeature()
    {
        if(null != this.pParagraphFormat)
        {
            TreeMap<Integer, Integer> hFontFeatures = this.pParagraphFormat.hFontFeatures;

            TreeMap.Entry<Integer, Integer> n = null;
            Iterator<TreeMap.Entry<Integer, Integer>> it = null;
            Set<Map.Entry<Integer, Integer>> s = hFontFeatures.entrySet();
            it = s.iterator();
            while (it.hasNext())
            {
                n = it.next();
                Integer hTag = n.getKey();
                Integer hParameter = n.getValue();
                mmLogger.LogT(TAG + " UpdateFontFeature hTag: " + hTag + " hParameter: " + hParameter);
            }
        }
        else
        {
            mmLogger.LogT(TAG + " UpdateFontFeature pParagraphFormat is null.");
        }
    }

    @SuppressWarnings({"WhileLoopReplaceableByForEach", "UnusedAssignment"})
    public void UpdateTextSpannable()
    {
        if (null != this.pTextSpannable)
        {
            LinkedList<mmTextSpan> hAttributes = this.pTextSpannable.hAttributes;

            this.pSpannableStringBuilder.clearSpans();

            mmTextSpan u = null;
            Iterator<mmTextSpan> it = hAttributes.iterator();
            while (it.hasNext())
            {
                u = it.next();
                this.UpdateTextSpan(u);
            }
        }
        else
        {
            mmLogger.LogT(TAG + " UpdateTextSpannable pTextSpannable is null.");
        }
    }

    public void DeleteTextFormat()
    {

    }

    public void DeleteTextLayout()
    {
        this.pBidi = null;
        this.pLayout = null;
    }

    @SuppressWarnings("ConstantConditions")
    public void Draw()
    {
        do
        {
            if(null == this.pLayout)
            {
                mmLogger.LogT(TAG + " Draw pLayout is null.");
                break;
            }
            if(null == this.pDrawingContext)
            {
                mmLogger.LogT(TAG + " Draw pDrawingContext is null.");
                break;
            }
            Canvas pCanvas = (Canvas)this.pDrawingContext.pRenderTarget;
            if(null == pCanvas)
            {
                mmLogger.LogT(TAG + " Draw pCanvas is null.");
                break;
            }
            mmTextParagraphFormat pParagraphFormat = this.pParagraphFormat;
            if(null == pParagraphFormat)
            {
                mmLogger.LogT(TAG + " Draw pParagraphFormat is null.");
                break;
            }

            float hOriginX = pParagraphFormat.hLayoutRect[0];
            float hOriginY = pParagraphFormat.hLayoutRect[1];
            float hLayoutRectH = pParagraphFormat.hLayoutRect[3];

            int hDrawingRectH = this.pLayout.getHeight();

            // ParagraphAlignment
            float hHalfH = (hLayoutRectH - hDrawingRectH) * 0.5f;
            hOriginY = hOriginY + pParagraphFormat.hParagraphAlignment * hHalfH;

            this.hOrigin[0] = hOriginX;
            this.hOrigin[1] = hOriginY;

            Matrix hTransform = new Matrix();
            
            pCanvas.save();
            
            pCanvas.setMatrix(hTransform);
            
            pCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            pCanvas.translate(hOriginX, hOriginY);

            this.EnableShadow(false);
            this.pTextPaint.setAntiAlias(false);

            // Background 0
            this.pDrawingContext.SetPass(0);
            this.DrawText(pCanvas);

            this.EnableShadow(true);
            this.pTextPaint.setAntiAlias(true);

            // Underline  1
            this.pDrawingContext.SetPass(1);
            this.DrawText(pCanvas);

            // Foreground 2
            this.pDrawingContext.SetPass(2);
            this.pLayout.draw(pCanvas);

            this.EnableShadow(false);

            // Stroke     3
            this.pDrawingContext.SetPass(3);
            this.pLayout.draw(pCanvas);

            // StrikeThru 4
            this.pDrawingContext.SetPass(4);
            this.DrawText(pCanvas);

            this.DrawCaret(pCanvas);

            pCanvas.restore();
        } while (false);
    }

    public void HitTestPoint(float[] point, int[] value, mmTextHitMetrics pMetrics)
    {
        // float            in  point[2] (x, y)
        // int              out value[2]
        //                      value[0] isTrailingHit
        //                      value[1] isInside
        // mmTextHitMetrics out pMetrics

        if (null == this.pLayout)
        {
            value[0] = 0;
            value[1] = 0;
            pMetrics.Reset();

            mmLogger.LogT(TAG + " HitTestPoint pLayout is null. point(" + point[0] + ", " + point[1] + ")");
        }
        else
        {
            float lx = point[0] - this.hOrigin[0];
            float ly = point[1] - this.hOrigin[1];
            int line = this.pLayout.getLineForVertical((int)ly);

            int hOffsetC = this.pLayout.getOffsetForHorizontal(line, lx);

            float c = this.pLayout.getPrimaryHorizontal(hOffsetC);

            int isTrailingHit;
            int idx0, idx1;

            if (c > lx)
            {
                // right side.
                idx0 = this.pLayout.getOffsetToLeftOf(hOffsetC);
                idx1 = hOffsetC;
                isTrailingHit = 1;
            }
            else
            {
                // left side.
                idx0 = hOffsetC;
                idx1 = this.pLayout.getOffsetToRightOf(hOffsetC);
                isTrailingHit = 0;
            }

            float hLineL = this.pLayout.getLineLeft(line);
            float hLineR = this.pLayout.getLineRight(line);

            float l = this.pLayout.getPrimaryHorizontal(idx0);
            float k = this.pLayout.getPrimaryHorizontal(idx1);
            int l1 = this.pLayout.getLineForOffset(idx0);
            int l2 = this.pLayout.getLineForOffset(idx1);
            // Careful getPrimaryHorizontal Wrap.
            float r = (l2 == l1) ? (k) : ((0 == k) ? hLineR : hLineL);

            float t = (float)this.pLayout.getLineTop(line);
            float b = (float)this.pLayout.getLineBottom(line);
            float w = r - l;
            float h = b - t;

            float lw = this.pLayout.getWidth();
            float lh = this.pLayout.getHeight();

            RectF oRect = new RectF(l, t, r, b);
            RectF lRect = new RectF(0, 0, lw, lh);

            pMetrics.rect[0] = l;
            pMetrics.rect[1] = t;
            pMetrics.rect[2] = w;
            pMetrics.rect[3] = h;
            pMetrics.offset = idx0;
            pMetrics.length = idx1 - idx0;
            pMetrics.bidiLevel = this.pBidi.getLevelAt(idx0);
            pMetrics.isText = oRect.contains(lx, ly) ? 1 : 0;
            pMetrics.isTrimmed = 0;

            isTrailingHit = (r >= lx) ? isTrailingHit : 1;

            value[0] = isTrailingHit;
            value[1] = lRect.contains(lx, ly) ? 1 : 0;
        }
    }

    public void HitTestTextPosition(int[] value, float[] point, mmTextHitMetrics pMetrics)
    {
        // int              in  value[2]
        //                      value[0] offset
        //                      value[1] isTrailingHit
        // float            out point[2] (x, y)
        // mmTextHitMetrics out pMetrics

        if (null == this.pLayout)
        {
            point[0] = 0;
            point[1] = 0;
            pMetrics.Reset();

            mmLogger.LogI(TAG + " HitTestTextPosition pLayout is null. value(" + value[0] + ", " + value[1] + ")");
        }
        else
        {
            int idx0 = value[0];
            int idx1 = this.pLayout.getOffsetToRightOf(idx0);
            int isTrailingHit = value[1];

            int line = this.pLayout.getLineForOffset(idx0);

            float hLineL = this.pLayout.getLineLeft(line);
            float hLineR = this.pLayout.getLineRight(line);

            float l = this.pLayout.getPrimaryHorizontal(idx0);
            float k = this.pLayout.getPrimaryHorizontal(idx1);
            int l1 = this.pLayout.getLineForOffset(idx0);
            int l2 = this.pLayout.getLineForOffset(idx1);
            // Careful getPrimaryHorizontal Wrap.
            float r = (l2 == l1) ? (k) : ((0 == k) ? hLineR : hLineL);

            float t = (float)this.pLayout.getLineTop(line);
            float b = (float)this.pLayout.getLineBottom(line);
            float w = r - l;
            float h = b - t;

            point[0] = (1 == isTrailingHit) ? r : l;
            point[1] = t;

            RectF oRect = new RectF(l, t, r, b);

            float lx = point[0] - this.hOrigin[0];
            float ly = point[1] - this.hOrigin[1];

            pMetrics.rect[0] = l;
            pMetrics.rect[1] = t;
            pMetrics.rect[2] = w;
            pMetrics.rect[3] = h;
            pMetrics.offset = idx0;
            pMetrics.length = idx1 - idx0;
            pMetrics.bidiLevel = this.pBidi.getLevelAt(idx0);
            pMetrics.isText = oRect.contains(lx, ly) ? 1 : 0;
            pMetrics.isTrimmed = 0;
        }
    }

    private void UpdateTextSpan(mmTextSpan pTextSpan)
    {
        mmRange pRange = pTextSpan.hRange;
        int rl = pRange.o;
        int rr = pRange.l + rl;
        int length = this.pSpannableStringBuilder.length();

        if ((0 <= rl && rl < length) && (0 <= rr && rr <= length))
        {
            mmTextDrawingEffect pFormatSpan = new mmTextDrawingEffect();
            pFormatSpan.SetDrawingContext(this.pDrawingContext);
            pFormatSpan.SetTextFormat(pTextSpan.hFormat);
            this.pSpannableStringBuilder.setSpan(pFormatSpan, rl, rr, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        else
        {
            mmLogger.LogI(TAG + " UpdateTextSpan [" + rl + ", " + rr + "] out of string range length: " + length);
        }
    }

    @SuppressWarnings({"WhileLoopReplaceableByForEach", "UnusedAssignment"})
    private void DrawText(Canvas pCanvas)
    {
        if (null != this.pTextSpannable)
        {
            LinkedList<mmTextSpan> hAttributes = this.pTextSpannable.hAttributes;

            mmTextSpan u = null;
            Iterator<mmTextSpan> it = hAttributes.iterator();
            while (it.hasNext())
            {
                u = it.next();
                this.DrawTextSpan(pCanvas, u);
            }
        }
        else
        {
            mmLogger.LogT(TAG + " DrawBackground pTextSpannable is null.");
        }
    }

    @SuppressWarnings("UnusedAssignment")
    private void DrawTextSpan(Canvas pCanvas, mmTextSpan pTextSpan)
    {
        mmRange pRange = pTextSpan.hRange;
        mmTextFormat pTextFormat = pTextSpan.hFormat;

        if (pRange.l > 0)
        {
            int ll = pRange.o;
            int lr = pRange.l + ll;
            int line0 = this.pLayout.getLineForOffset(ll);
            int line1 = this.pLayout.getLineForOffset(lr - 1);

            int i = 0;
            for (i = line0; i <= line1; ++i)
            {
                int hLineT = this.pLayout.getLineTop(i);
                int hLintB = this.pLayout.getLineBottom(i);

                float hLineL = this.pLayout.getLineLeft(i);
                float hLineR = this.pLayout.getLineRight(i);

                int hLintS = this.pLayout.getLineStart(i);
                int hLintE = this.pLayout.getLineEnd(i);

                int hBaseline = this.pLayout.getLineBaseline(i);

                int idx0 = Math.max(hLintS, ll);
                int idx1 = Math.min(hLintE, lr);

                float l = this.pLayout.getPrimaryHorizontal(idx0);
                float k = this.pLayout.getPrimaryHorizontal(idx1);
                int l1 = this.pLayout.getLineForOffset(idx0);
                int l2 = this.pLayout.getLineForOffset(idx1);
                // Careful getPrimaryHorizontal Wrap.
                float r = (l2 == l1) ? (k) : ((0 == k) ? hLineR : hLineL);

                switch (this.pDrawingContext.hPass)
                {
                    case 0:
                    {
                        // Background 0
                        if (0.0f != pTextFormat.hBackgroundColor[3])
                        {
                            int hColor = mmGraphicsPlatform.ColorMake(pTextFormat.hBackgroundColor);
                            RectF xrect = new RectF();
                            xrect.set(l, hLineT, r, hLintB);

                            this.pTextPaint.setColor(hColor);
                            this.pTextPaint.setStrokeWidth(0.0f);
                            this.pTextPaint.setStyle(TextPaint.Style.FILL);

                            pCanvas.drawRect(xrect, this.pTextPaint);
                        }
                    }
                    break;

                    case 1:
                    {
                        // Underline  1
                        if (0.0f != pTextFormat.hUnderlineWidth)
                        {
                            float hDisplayDensity = this.pParagraphFormat.hDisplayDensity;
                            float w = Math.abs(pTextFormat.hUnderlineWidth * hDisplayDensity);
                            float t = mmTextPaint.GetUnderlineThickness(this.pTextPaint);
                            float pos = mmTextPaint.GetUnderlinePosition(this.pTextPaint);
                            float thi = (-1.0f == pTextFormat.hUnderlineWidth) ? t : w;
                            int hColor = mmGraphicsPlatform.ColorMake(pTextFormat.hUnderlineColor);

                            float vPos = hBaseline + pos;
                            float vth = thi / 2.0f;
                            float ttp = vPos - vth;
                            float tbo = vPos + vth;

                            RectF hRectF = new RectF();
                            hRectF.set(l, ttp, r, tbo);

                            this.pTextPaint.setColor(hColor);
                            this.pTextPaint.setStrokeWidth(0.0f);
                            this.pTextPaint.setStyle(TextPaint.Style.FILL);

                            pCanvas.drawRect(hRectF, this.pTextPaint);
                        }
                    }
                    break;

                    case 4:
                    {
                        // StrikeThru 4
                        if (0.0f != pTextFormat.hStrikeThruWidth)
                        {
                            float hDisplayDensity = this.pParagraphFormat.hDisplayDensity;
                            float w = Math.abs(pTextFormat.hStrikeThruWidth * hDisplayDensity);
                            float t = mmTextPaint.GetStrikeThruThickness(this.pTextPaint);
                            float pos = mmTextPaint.GetStrikeThruPosition(this.pTextPaint);
                            float thi = (-1.0f == pTextFormat.hStrikeThruWidth) ? t : w;
                            int hColor = mmGraphicsPlatform.ColorMake(pTextFormat.hStrikeThruColor);

                            float vPos = hBaseline + pos;
                            float vth = thi / 2.0f;
                            float ttp = vPos - vth;
                            float tbo = vPos + vth;

                            RectF hRectF = new RectF();
                            hRectF.set(l, ttp, r, tbo);

                            this.pTextPaint.setColor(hColor);
                            this.pTextPaint.setStrokeWidth(0.0f);
                            this.pTextPaint.setStyle(TextPaint.Style.FILL);

                            pCanvas.drawRect(hRectF, this.pTextPaint);
                        }
                    }
                    break;

                    default:
                    break;
                }
            }
        }
    }

    private void EnableShadow(boolean hEnable)
    {
        if (hEnable)
        {
            if (0.0 != this.pParagraphFormat.hShadowBlur)
            {
                float hDisplayDensity = this.pParagraphFormat.hDisplayDensity;
                float hShadowRadius = this.pParagraphFormat.hShadowBlur * hDisplayDensity;
                float hShadowOffsetX = this.pParagraphFormat.hShadowOffset[0] * hDisplayDensity;
                float hShadowOffsetY = this.pParagraphFormat.hShadowOffset[1] * hDisplayDensity;
                int hShadowColor = mmGraphicsPlatform.ColorMake(this.pParagraphFormat.hShadowColor);
                this.pTextPaint.setShadowLayer(hShadowRadius, hShadowOffsetX, hShadowOffsetY, hShadowColor);
            }
        }
        else
        {
            this.pTextPaint.setShadowLayer(0.0f, 0.0f, 0.0f, 0x00000000);
        }
    }

    private void DrawCaret(Canvas pCanvas)
    {
        mmTextParagraphFormat pParagraphFormat = this.pParagraphFormat;
        if (1 == pParagraphFormat.hCaretStatus)
        {
            int[] value = new int[2];
            float[] point = new float[2];
            mmTextHitMetrics metrics = new mmTextHitMetrics();

            float hDisplayDensity = pParagraphFormat.hDisplayDensity;

            int isTrailingHit = pParagraphFormat.hCaretWrapping;
            int offset = (int)pParagraphFormat.hCaretIndex - isTrailingHit;
			float hCaretW = pParagraphFormat.hCaretWidth * hDisplayDensity;
			float hCaretHalfW = hCaretW / 2.0f;

            // Normalise to front index.
            // android can handle surrogate pairs correctly.
            // offset = (mmUInt32_t)mmUTF_GetUtf16OffsetLNormalise(ss, sz, offset);

            value[0] = offset;
            value[1] = isTrailingHit;
            this.HitTestTextPosition(value, point, metrics);

            int hColor = mmGraphicsPlatform.ColorMake(pParagraphFormat.hCaretColor);
            RectF rect = new RectF();

            rect.left   = point[0] - hCaretHalfW;
            rect.top    = point[1];
            rect.right  = point[0] + hCaretHalfW;
            rect.bottom = point[1] + metrics.rect[3];

            this.pTextPaint.setColor(hColor);
            this.pTextPaint.setStrokeWidth(0.0f);
            this.pTextPaint.setStyle(TextPaint.Style.FILL);

            pCanvas.drawRect(rect, this.pTextPaint);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void Layout_VersionM()
    {
        float hDisplayDensity = this.pParagraphFormat.hDisplayDensity;

        int hLayoutW = (int)this.pParagraphFormat.hLayoutRect[2];
        int hLayoutH = (int)this.pParagraphFormat.hLayoutRect[3];

        int hBaseDirection = mmUBidi.GetBaseDirection(this.pText.buffer, this.pText.length);

        Layout.Alignment hAlignment = mmTextPlatform.GetTextAlignment(this.pParagraphFormat.hTextAlignment, hBaseDirection);

        float hSpacingAddition = this.pParagraphFormat.hLineSpacingAddition * hDisplayDensity;
        float hSpacingMultiple = this.pParagraphFormat.hLineSpacingMultiple;

        final boolean hIncludePad = false;

        TextUtils.TruncateAt hTruncating = mmTextPlatform.GetTextWordTruncating(this.pParagraphFormat.hLineBreakMode);
        int hEllipsizedWidth = 0;

        int b = 0;
        int e = this.pText.length;

        StaticLayout.Builder pBuilder = StaticLayout.Builder.obtain(this.pSpannableStringBuilder, b, e, this.pTextPaint, hLayoutW);
        pBuilder.setAlignment(hAlignment);
        pBuilder.setLineSpacing(hSpacingAddition, hSpacingMultiple);
        pBuilder.setIncludePad(hIncludePad);
        pBuilder.setEllipsize(hTruncating);
        pBuilder.setEllipsizedWidth(hEllipsizedWidth);

        this.pLayout = pBuilder.build();
    }

    private void Layout_VersionFallback()
    {
        float hDisplayDensity = this.pParagraphFormat.hDisplayDensity;

        int hLayoutW = (int)this.pParagraphFormat.hLayoutRect[2];
        int hLayoutH = (int)this.pParagraphFormat.hLayoutRect[3];

        int hBaseDirection = mmUBidi.GetBaseDirection(this.pText.buffer, this.pText.length);

        Layout.Alignment hAlignment = mmTextPlatform.GetTextAlignment(this.pParagraphFormat.hTextAlignment, hBaseDirection);

        float hSpacingAddition = this.pParagraphFormat.hLineSpacingAddition * hDisplayDensity;
        float hSpacingMultiple = this.pParagraphFormat.hLineSpacingMultiple;

        final boolean hIncludePad = false;

        TextUtils.TruncateAt hTruncating = mmTextPlatform.GetTextWordTruncating(this.pParagraphFormat.hLineBreakMode);
        int hEllipsizedWidth = 0;

        int b = 0;
        int e = this.pText.length;

        this.pLayout = new StaticLayout(
            this.pSpannableStringBuilder,
            b,
            e,
            this.pTextPaint,
            hLayoutW,
            hAlignment,
            hSpacingMultiple,
            hSpacingAddition,
            hIncludePad,
            hTruncating,
            hEllipsizedWidth);
    }
}
