package org.mm.nwsi;

import android.graphics.Paint;
import android.os.Build;
import android.text.TextPaint;

import java.lang.reflect.Method;

import android.annotation.TargetApi;

@SuppressWarnings("unused")
public class mmTextPaint
{
    static public void SetUnderline(final TextPaint tp, int color, float underlineThickness)
    {
        if(Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT)
        {
            SetUnderline_VersionQ(tp, color, underlineThickness);
        }
        else
        {
            // Fallback.
            SetUnderline_VersionFallback(tp, color, underlineThickness);
        }
    }

    static public float GetUnderlineThickness(final TextPaint tp)
    {
        if(Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT)
        {
            return GetUnderlineThickness_VersionQ(tp);
        }
        else
        {
            // Fallback.
            return GetUnderlineThickness_VersionFallback(tp);
        }
    }

    static public float GetUnderlinePosition(final TextPaint tp)
    {
        if(Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT)
        {
            return GetUnderlinePosition_VersionQ(tp);
        }
        else
        {
            // Fallback.
            return GetUnderlinePosition_VersionFallback(tp);
        }
    }

    static public float GetStrikeThruThickness(final TextPaint tp)
    {
        if(Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT)
        {
            return GetStrikeThruThickness_VersionQ(tp);
        }
        else
        {
            // Fallback.
            return GetStrikeThruThickness_VersionFallback(tp);
        }
    }

    static public float GetStrikeThruPosition(final TextPaint tp)
    {
        if(Build.VERSION_CODES.Q <= Build.VERSION.SDK_INT)
        {
            return GetStrikeThruPosition_VersionQ(tp);
        }
        else
        {
            // Fallback.
            return GetStrikeThruPosition_VersionFallback(tp);
        }
    }

    @SuppressWarnings("JavaReflectionMemberAccess")
    static public void SetUnderline_VersionFallback(final TextPaint tp, int color, float underlineThickness)
    {
        try
        {
            final Method pMethod = TextPaint.class.getMethod("setUnderlineText", Integer.TYPE, Float.TYPE);
            pMethod.invoke(tp, color, underlineThickness);
        }
        catch (final Exception e)
        {
            tp.setUnderlineText(true);
        }
    }

    @TargetApi(Build.VERSION_CODES.Q)
    static public void SetUnderline_VersionQ(final TextPaint tp, int color, float underlineThickness)
    {
        tp.underlineThickness = underlineThickness;
        tp.underlineColor = color;
    }

    static public float GetUnderlineThickness_VersionFallback(final TextPaint tp)
    {
        Paint.FontMetrics pFontMetrics = tp.getFontMetrics();
        return (pFontMetrics.bottom - pFontMetrics.top) / 18.0f;
    }
    @TargetApi(Build.VERSION_CODES.Q)
    static public float GetUnderlineThickness_VersionQ(final TextPaint tp)
    {
        return tp.underlineThickness;
    }

    static public float GetUnderlinePosition_VersionFallback(final TextPaint tp)
    {
        Paint.FontMetrics pFontMetrics = tp.getFontMetrics();
        return pFontMetrics.descent * 0.5f * 1.15f;
    }
    @TargetApi(Build.VERSION_CODES.Q)
    static public float GetUnderlinePosition_VersionQ(final TextPaint tp)
    {
        return tp.getUnderlinePosition();
    }

    static public float GetStrikeThruThickness_VersionFallback(final TextPaint tp)
    {
        Paint.FontMetrics pFontMetrics = tp.getFontMetrics();
        return (pFontMetrics.bottom - pFontMetrics.top) / 18.0f;
    }
    @TargetApi(Build.VERSION_CODES.Q)
    static public float GetStrikeThruThickness_VersionQ(final TextPaint tp)
    {
        return tp.getStrikeThruThickness();
    }

    static public float GetStrikeThruPosition_VersionFallback(final TextPaint tp)
    {
        Paint.FontMetrics pFontMetrics = tp.getFontMetrics();
        return -pFontMetrics.descent * 1.15f;
    }
    @TargetApi(Build.VERSION_CODES.Q)
    static public float GetStrikeThruPosition_VersionQ(final TextPaint tp)
    {
        return tp.getStrikeThruPosition();
    }
}
