/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import java.util.Locale;

@SuppressWarnings("unused")
public class mmLocale
{
    private static final String TAG = mmLocale.class.getSimpleName();
    
    // ===========================================================
    // c --> java
    // ===========================================================
    public static void LanguageCountry(byte[] pLanguage, byte[] pCountry)
    {
        // locale api is thread safe.
        Locale locale = Locale.getDefault();
        String s_language = locale.getLanguage();
        String s_country = locale.getCountry();
        
        byte[] b_language = s_language.getBytes();
        byte[] b_country = s_country.getBytes();        

        System.arraycopy(b_language, 0, pLanguage, 0, b_language.length);
        System.arraycopy(b_country, 0, pCountry, 0, b_country.length);
    }
}