/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import org.mm.core.mmLogger;
import org.mm.core.mmLogger.mmLevelMark;
import org.mm.core.mmLogger.mmLoggerCallback;
import org.mm.core.mmThread;

import android.util.Log;

public class mmLoggerManager
{
    private static final String TAG = mmActivityMaster.class.getSimpleName();
    
    
    @SuppressWarnings("DuplicateBranchesInSwitch")
    public static void LoggerConsole(String section, int lvl, String message)
    {
        // [ 8 V ]
        mmLevelMark lm = mmLogger.mmLoggerLevelMark(lvl);              
        
        switch(lvl)
        {
        case mmLogger.MM_LOG_VERBOSE:
            Log.v(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        case mmLogger.MM_LOG_DEBUG:
            Log.d(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        case mmLogger.MM_LOG_INFO:
            Log.i(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        case mmLogger.MM_LOG_WARNING:
            Log.w(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        case mmLogger.MM_LOG_ERROR:
            Log.e(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        case mmLogger.MM_LOG_ALERT:
            Log.e(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        default:
            Log.v(section, " " + lvl + " " + lm.m + " "+ message);
            break;
        }
    }
    
    public static class mmLoggerCallbackAndroid extends mmLoggerCallback
    {
        public mmThread.Mutex mutex = new mmThread.Mutex();

        @SuppressWarnings("ConstantConditions")
        @Override
        public void EventLogger(mmLogger o, int lvl, String message) 
        {
            mutex.Lock();

            do
            {
                mmLoggerManager lm = (mmLoggerManager)(o.obj);
                if (null == lm || lvl > lm.logger_level)
                {
                    // logger filter.
                    break;
                }

                mmLoggerManager.LoggerConsole(lm.logger_section, lvl, message);
            }while(false);

            mutex.Unlock();
        }
    }
    
    public static mmLoggerCallback pLoggerCallbackAndroid = new mmLoggerCallbackAndroid();

    public String logger_section = "nwsi";
    // default MM_LOG_VERBOSE.
    public int logger_level = mmLogger.MM_LOG_VERBOSE;
    
    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetLoggerLevel(int hLoggerLevel)
    {
        this.logger_level = hLoggerLevel;
    }
    
    public void OnFinishLaunching()
    {
        mmLogger.SetCallback(mmLoggerManager.pLoggerCallbackAndroid, this);
    }
    public void OnBeforeTerminate()
    {
        mmLogger.SetCallback(mmLogger.pLoggerCallbackPrintf, null);
    }
}