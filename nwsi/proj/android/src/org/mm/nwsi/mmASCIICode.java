/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

@SuppressWarnings("unused")
public class mmASCIICode
{
    /*
    "Bin"     , "Oct", "Dec", "Hex", "(Short Name)(Name)"           ,
    ,0000 0000,     0,     0,  0x00, NUL (null)                     ,
    ,0000 0001,     1,     1,  0x01, SOH (start of headline)        ,
    ,0000 0010,     2,     2,  0x02, STX (start of text)            ,
    ,0000 0011,     3,     3,  0x03, ETX (end of text)              ,
    ,0000 0100,     4,     4,  0x04, EOT (end of transmission)      ,
    ,0000 0101,     5,     5,  0x05, ENQ (enquiry)                  ,
    ,0000 0110,     6,     6,  0x06, ACK (acknowledge)              ,
    ,0000 0111,     7,     7,  0x07, BEL (bell)                     ,
    ,0000 1000,    10,     8,  0x08, BS  (backspace)                ,
    ,0000 1001,    11,     9,  0x09, HT  (horizontal tab)           ,
    ,0000 1010,    12,    10,  0x0A, LF  (NL line feed, new line)   ,
    ,0000 1011,    13,    11,  0x0B, VT  (vertical tab)             ,
    ,0000 1100,    14,    12,  0x0C, FF  (NP form feed, new page)   ,
    ,0000 1101,    15,    13,  0x0D, CR  (carriage return)          ,
    ,0000 1110,    16,    14,  0x0E, SO  (shift out)                ,
    ,0000 1111,    17,    15,  0x0F, SI  (shift in)                 ,
    ,0001 0000,    20,    16,  0x10, DLE (data link escape)         ,
    ,0001 0001,    21,    17,  0x11, DC1 (device control 1)         ,
    ,0001 0010,    22,    18,  0x12, DC2 (device control 2)         ,
    ,0001 0011,    23,    19,  0x13, DC3 (device control 3)         ,
    ,0001 0100,    24,    20,  0x14, DC4 (device control 4)         ,
    ,0001 0101,    25,    21,  0x15, NAK (negative acknowledge)     ,
    ,0001 0110,    26,    22,  0x16, SYN (synchronous idle)         ,
    ,0001 0111,    27,    23,  0x17, ETB (end of trans. block)      ,
    ,0001 1000,    30,    24,  0x18, CAN (cancel)                   ,
    ,0001 1001,    31,    25,  0x19, EM  (end of medium)            ,
    ,0001 1010,    32,    26,  0x1A, SUB (substitute)               ,
    ,0001 1011,    33,    27,  0x1B, ESC (escape)                   ,
    ,0001 1100,    34,    28,  0x1C, FS  (file separator)           ,
    ,0001 1101,    35,    29,  0x1D, GS  (group separator)          ,
    ,0001 1110,    36,    30,  0x1E, RS  (record separator)         ,
    ,0001 1111,    37,    31,  0x1F, US  (unit separator)           ,
    ,0010 0000,    40,    32,  0x20,     (space)                    ,
    ,0010 0001,    41,    33,  0x21, !                              ,
    ,0010 0010,    42,    34,  0x22, "                              ,
    ,0010 0011,    43,    35,  0x23, #                              ,
    ,0010 0100,    44,    36,  0x24, $                              ,
    ,0010 0101,    45,    37,  0x25, %                              ,
    ,0010 0110,    46,    38,  0x26, &                              ,
    ,0010 0111,    47,    39,  0x27, '                              ,
    ,0010 1000,    50,    40,  0x28, (                              ,
    ,0010 1001,    51,    41,  0x29, )                              ,
    ,0010 1010,    52,    42,  0x2A, *                              ,
    ,0010 1011,    53,    43,  0x2B, +                              ,
    ,0010 1100,    54,    44,  0x2C, ,                              ,
    ,0010 1101,    55,    45,  0x2D, -                              ,
    ,0010 1110,    56,    46,  0x2E, .                              ,
    ,0010 1111,    57,    47,  0x2F, /                              ,
    ,0011 0000,    60,    48,  0x30, 0                              ,
    ,0011 0001,    61,    49,  0x31, 1                              ,
    ,0011 0010,    62,    50,  0x32, 2                              ,
    ,0011 0011,    63,    51,  0x33, 3                              ,
    ,0011 0100,    64,    52,  0x34, 4                              ,
    ,0011 0101,    65,    53,  0x35, 5                              ,
    ,0011 0110,    66,    54,  0x36, 6                              ,
    ,0011 0111,    67,    55,  0x37, 7                              ,
    ,0011 1000,    70,    56,  0x38, 8                              ,
    ,0011 1001,    71,    57,  0x39, 9                              ,
    ,0011 1010,    72,    58,  0x3A, :                              ,
    ,0011 1011,    73,    59,  0x3B, ;                              ,
    ,0011 1100,    74,    60,  0x3C, <                              ,
    ,0011 1101,    75,    61,  0x3D, =                              ,
    ,0011 1110,    76,    62,  0x3E, >                              ,
    ,0011 1111,    77,    63,  0x3F, ?                              ,
    ,0100 0000,   100,    64,  0x40, @                              ,
    ,0100 0001,   101,    65,  0x41, A                              ,
    ,0100 0010,   102,    66,  0x42, B                              ,
    ,0100 0011,   103,    67,  0x43, C                              ,
    ,0100 0100,   104,    68,  0x44, D                              ,
    ,0100 0101,   105,    69,  0x45, E                              ,
    ,0100 0110,   106,    70,  0x46, F                              ,
    ,0100 0111,   107,    71,  0x47, G                              ,
    ,0100 1000,   110,    72,  0x48, H                              ,
    ,0100 1001,   111,    73,  0x49, I                              ,
    ,0100 1010,   112,    74,  0x4A, J                              ,
    ,0100 1011,   113,    75,  0x4B, K                              ,
    ,0100 1100,   114,    76,  0x4C, L                              ,
    ,0100 1101,   115,    77,  0x4D, M                              ,
    ,0100 1110,   116,    78,  0x4E, N                              ,
    ,0100 1111,   117,    79,  0x4F, O                              ,
    ,0101 0000,   120,    80,  0x50, P                              ,
    ,0101 0001,   121,    81,  0x51, Q                              ,
    ,0101 0010,   122,    82,  0x52, R                              ,
    ,0101 0011,   123,    83,  0x53, S                              ,
    ,0101 0100,   124,    84,  0x54, T                              ,
    ,0101 0101,   125,    85,  0x55, U                              ,
    ,0101 0110,   126,    86,  0x56, V                              ,
    ,0101 0111,   127,    87,  0x57, W                              ,
    ,0101 1000,   130,    88,  0x58, X                              ,
    ,0101 1001,   131,    89,  0x59, Y                              ,
    ,0101 1010,   132,    90,  0x5A, Z                              ,
    ,0101 1011,   133,    91,  0x5B, [                              ,
    ,0101 1100,   134,    92,  0x5C, \                              ,
    ,0101 1101,   135,    93,  0x5D, ]                              ,
    ,0101 1110,   136,    94,  0x5E, ^                              ,
    ,0101 1111,   137,    95,  0x5F, _                              ,
    ,0110 0000,   140,    96,  0x60, `                              ,
    ,0110 0001,   141,    97,  0x61, a                              ,
    ,0110 0010,   142,    98,  0x62, b                              ,
    ,0110 0011,   143,    99,  0x63, c                              ,
    ,0110 0100,   144,   100,  0x64, d                              ,
    ,0110 0101,   145,   101,  0x65, e                              ,
    ,0110 0110,   146,   102,  0x66, f                              ,
    ,0110 0111,   147,   103,  0x67, g                              ,
    ,0110 1000,   150,   104,  0x68, h                              ,
    ,0110 1001,   151,   105,  0x69, i                              ,
    ,0110 1010,   152,   106,  0x6A, j                              ,
    ,0110 1011,   153,   107,  0x6B, k                              ,
    ,0110 1100,   154,   108,  0x6C, l                              ,
    ,0110 1101,   155,   109,  0x6D, m                              ,
    ,0110 1110,   156,   110,  0x6E, n                              ,
    ,0110 1111,   157,   111,  0x6F, o                              ,
    ,0111 0000,   160,   112,  0x70, p                              ,
    ,0111 0001,   161,   113,  0x71, q                              ,
    ,0111 0010,   162,   114,  0x72, r                              ,
    ,0111 0011,   163,   115,  0x73, s                              ,
    ,0111 0100,   164,   116,  0x74, t                              ,
    ,0111 0101,   165,   117,  0x75, u                              ,
    ,0111 0110,   166,   118,  0x76, v                              ,
    ,0111 0111,   167,   119,  0x77, w                              ,
    ,0111 1000,   170,   120,  0x78, x                              ,
    ,0111 1001,   171,   121,  0x79, y                              ,
    ,0111 1010,   172,   122,  0x7A, z                              ,
    ,0111 1011,   173,   123,  0x7B, {                              ,
    ,0111 1100,   174,   124,  0x7C, |                              ,
    ,0111 1101,   175,   125,  0x7D, }                              ,
    ,0111 1110,   176,   126,  0x7E, ~                              ,
    ,0111 1111,   177,   127,  0x7F, DEL (delete)                   ,
    */

    // Only [0x00, 0x7F] control code.
    public final static int MM_ASCII_NUL      = 0x00; /*, NUL (null)                     , */
    public final static int MM_ASCII_SOH      = 0x01; /*, SOH (start of headline)        , */
    public final static int MM_ASCII_STX      = 0x02; /*, STX (start of text)            , */
    public final static int MM_ASCII_ETX      = 0x03; /*, ETX (end of text)              , */
    public final static int MM_ASCII_EOT      = 0x04; /*, EOT (end of transmission)      , */
    public final static int MM_ASCII_ENQ      = 0x05; /*, ENQ (enquiry)                  , */
    public final static int MM_ASCII_ACK      = 0x06; /*, ACK (acknowledge)              , */
    public final static int MM_ASCII_BEL      = 0x07; /*, BEL (bell)                     , */
    public final static int MM_ASCII_BS       = 0x08; /*, BS  (backspace)                , */
    public final static int MM_ASCII_HT       = 0x09; /*, HT  (horizontal tab)           , */
    public final static int MM_ASCII_LF       = 0x0A; /*, LF  (NL line feed, new line)   , */
    public final static int MM_ASCII_VT       = 0x0B; /*, VT  (vertical tab)             , */
    public final static int MM_ASCII_FF       = 0x0C; /*, FF  (NP form feed, new page)   , */
    public final static int MM_ASCII_CR       = 0x0D; /*, CR  (carriage return)          , */
    public final static int MM_ASCII_SO       = 0x0E; /*, SO  (shift out)                , */
    public final static int MM_ASCII_SI       = 0x0F; /*, SI  (shift in)                 , */
    public final static int MM_ASCII_DLE      = 0x10; /*, DLE (data link escape)         , */
    public final static int MM_ASCII_DC1      = 0x11; /*, DC1 (device control 1)         , */
    public final static int MM_ASCII_DC2      = 0x12; /*, DC2 (device control 2)         , */
    public final static int MM_ASCII_DC3      = 0x13; /*, DC3 (device control 3)         , */
    public final static int MM_ASCII_DC4      = 0x14; /*, DC4 (device control 4)         , */
    public final static int MM_ASCII_NAK      = 0x15; /*, NAK (negative acknowledge)     , */
    public final static int MM_ASCII_SYN      = 0x16; /*, SYN (synchronous idle)         , */
    public final static int MM_ASCII_ETB      = 0x17; /*, ETB (end of trans. block)      , */
    public final static int MM_ASCII_CAN      = 0x18; /*, CAN (cancel)                   , */
    public final static int MM_ASCII_EM       = 0x19; /*, EM  (end of medium)            , */
    public final static int MM_ASCII_SUB      = 0x1A; /*, SUB (substitute)               , */
    public final static int MM_ASCII_ESC      = 0x1B; /*, ESC (escape)                   , */
    public final static int MM_ASCII_FS       = 0x1C; /*, FS  (file separator)           , */
    public final static int MM_ASCII_GS       = 0x1D; /*, GS  (group separator)          , */
    public final static int MM_ASCII_RS       = 0x1E; /*, RS  (record separator)         , */
    public final static int MM_ASCII_US       = 0x1F; /*, US  (unit separator)           , */
    public final static int MM_ASCII_DEL      = 0x7F; /*, DEL (delete)                   , */
}