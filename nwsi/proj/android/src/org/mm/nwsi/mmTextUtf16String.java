package org.mm.nwsi;

@SuppressWarnings({"unused"})
public class mmTextUtf16String implements CharSequence
{
    public char[] buffer = null;
    public int offset = 0;
    public int length = 0;

    public mmTextUtf16String()
    {
        this.buffer = null;
        this.offset = 0;
        this.length = 0;
    }
    public mmTextUtf16String(char[] buffer)
    {
        this.Assign(buffer);
    }

    public mmTextUtf16String(char[] buffer, int offset, int length)
    {
        this.Assign(buffer, offset, length);
    }

    public void Init()
    {

    }
    
    public void Destroy()
    {

    }
    
    public void OnFinishLaunching()
    {

    }
    
    public void OnBeforeTerminate()
    {

    }
    
    public void Assign(char[] buffer)
    {
        this.Assign(buffer, 0, buffer.length);
    }

    public void Assign(char[] buffer, int offset, int length)
    {
        this.buffer = buffer;
        this.offset = offset;
        this.length = length;
    }

    @Override
    public int length()
    {
        return this.length;
    }

    @Override
    public char charAt(int index)
    {
        return this.buffer[this.offset + index];
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public CharSequence subSequence(int start, int end)
    {
        mmTextUtf16String s = new mmTextUtf16String();
        s.buffer = this.buffer;
        s.offset = this.offset + start;
        s.length = end - start;
        return s;
    }
}
