/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

@SuppressLint("ObsoleteSdkInt")
@SuppressWarnings("unused")
public class mmUIFullScreenLayout
{
    private static final String TAG = mmUIFullScreenLayout.class.getSimpleName();

    static public boolean GetUIViewIsFullscreen(Activity pActivity, View pView)
    {
        if(Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT)
        {
            // Google official API.
            return GetUIViewIsFullscreen_Version16(pActivity, pView);
        }
        else
        {
            // Fallback support.
            return GetUIViewIsFullscreen_VersionFallback(pActivity, pView);
        }
    }
    @TargetApi(16)
    static public boolean GetUIViewIsFullscreen_Version16(Activity pActivity, View pView)
    {
        int hVisibility = pView.getSystemUiVisibility();
        return (0 != (hVisibility & View.SYSTEM_UI_FLAG_FULLSCREEN));
    }
    static public boolean GetUIViewIsFullscreen_VersionFallback(Activity pActivity, View pView)
    {
        Window pWindow = pActivity.getWindow();
        WindowManager.LayoutParams pAttributes = pWindow.getAttributes();
        return (0 != (pAttributes.flags & WindowManager.LayoutParams.FLAG_FULLSCREEN));
    }
    
    static public void SetUIWindowFullScreen(Window pWindow)
    {
        if(null != pWindow)
        {
            SetWindowFullScreen14(pWindow);
            SetWindowFullScreen19(pWindow);
            SetWindowFullScreen21(pWindow);
        }
    }
    
    // Note: If you need to set the full screen, you must open the notched screen properties.
    //       android:theme "res/values-v27/styles.xml"
    //       <item name="android:windowLayoutInDisplayCutoutMode">shortEdges</item>
    static public void SetUIViewFullScreen(View pView)
    {
        if(null != pView)
        {
            int[] pVisibility = new int[1];
            GetFullScreenUIVisibility14(pVisibility);
            GetFullScreenUIVisibility16(pVisibility);
            GetFullScreenUIVisibility19(pVisibility);
            pView.setSystemUiVisibility(pVisibility[0]);
        }
    }

    static public int GetUIViewFullScreenVisibility()
    {
        int[] pVisibility = new int[1];
        GetFullScreenUIVisibility14(pVisibility);
        GetFullScreenUIVisibility16(pVisibility);
        GetFullScreenUIVisibility19(pVisibility);
        return pVisibility[0];
    }

    @TargetApi(14)
    static public void SetWindowFullScreen14(Window pWindow)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            pWindow.requestFeature(Window.FEATURE_NO_TITLE);
            pWindow.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }
    
    @TargetApi(19)
    static public void SetWindowFullScreen19(Window pWindow)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            pWindow.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            pWindow.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }
    
    @TargetApi(21)
    static public void SetWindowFullScreen21(Window pWindow)
    {
        if(Build.VERSION.SDK_INT >= 21)
        {
            pWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            
            pWindow.setStatusBarColor(Color.TRANSPARENT);
            pWindow.setNavigationBarColor(Color.TRANSPARENT);
        }
    }
    
    @TargetApi(14)
    static public void GetFullScreenUIVisibility14(int[] pVisibility)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
        {
            // https://developer.android.google.cn/reference/android/view/View?hl=zh-cn#SYSTEM_UI_FLAG_HIDE_NAVIGATION
            //     There is a limitation: because navigation controls are so important, the least user interaction will cause
            //     them to reappear immediately. When this happens, both this flag and SYSTEM_UI_FLAG_FULLSCREEN
            //     will be cleared automatically, so that both elements reappear at the same time.
            //
            // Virtual navigation bar at low version is not safe area.
            // Version >= (API 19) have View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY flag, can use it for safe area.
            // But most of time, the bar area is not unstable, Adjustment the safety area is unworthy.
            // Worse still, after the navigation bar is hidden, the first click event will be blocked.
            //
            // So at API [14, 19), we not need hide the navigation bar.
            //
            // pVisibility[0] |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

            pVisibility[0] |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
            
            Log.i(TAG, TAG + " UIVisibility >=14.");
        }
    }
    @TargetApi(16)
    static public void GetFullScreenUIVisibility16(int[] pVisibility)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
        {
            pVisibility[0] |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

            // SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION will not hide the bar, just layout for drawable area.
            pVisibility[0] |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
            pVisibility[0] |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            pVisibility[0] |= View.SYSTEM_UI_FLAG_FULLSCREEN;

            Log.i(TAG, TAG + " UIVisibility >=16.");
        }
    }
    
    @TargetApi(19)
    static public void GetFullScreenUIVisibility19(int[] pVisibility)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            pVisibility[0] |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            // At API [19, +inf) the navigation bar can use for drawable area, but not safe area.
            pVisibility[0] |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            
            Log.i(TAG, TAG + " UIVisibility >=19.");
        }
    }
}