/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.mm.algorithm.mmHex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@SuppressWarnings({"unused", "JavadocReference"})
public class mmAppIdentifier
{
    private static final String TAG = mmAppIdentifier.class.getSimpleName();

    public static final String MM_APP_IDENTIFIER_INVALID_ANDROID_ID = "9774d56d682e549c";

    public static final String MM_APP_IDENTIFIER_PREFS_GROUP = "AppIdentifier";
    public static final String MM_APP_IDENTIFIER_PREFS_FIELD_UUID_DEVICE = "UUIDDevice";
    public static final String MM_APP_IDENTIFIER_PREFS_FIELD_UUID_APP = "UUIDApp";

    // weak reference.
    public long hNativePtr = 0;

    public Context pContext = null;

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }

    public void SetContext(Context pContext)
    {
        this.pContext = pContext;
    }

    public void OnFinishLaunching()
    {

    }

    public void OnBeforeTerminate()
    {

    }

    public static String GetSHA1(String s)
    {
        if(null != s)
        {
            String hSHA1 = null;
            try
            {
                byte[] pBytes = s.getBytes();
                MessageDigest digest = MessageDigest.getInstance("SHA-1");
                digest.update(pBytes);
                byte[] pDigest = digest.digest();
                hSHA1 = mmHex.EncodeHexString(pDigest);
            }
            catch (NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
            return hSHA1;
        }
        else
        {
            return null;
        }
    }

    @SuppressWarnings("UnusedAssignment")
    @SuppressLint({"HardwareIds"})
    public static String GetAndroidId(Context pContext)
    {
        String hAndroidId = null;
        ContentResolver pContentResolver = pContext.getContentResolver();
        hAndroidId = Secure.getString(pContentResolver, Secure.ANDROID_ID);

        // Use the Android ID unless it's broken, in which case
        // fallback on deviceId,
        // unless it's not available, then fallback on a random
        // number which we store to a prefs file
        hAndroidId = (mmAppIdentifier.MM_APP_IDENTIFIER_INVALID_ANDROID_ID.equals(hAndroidId)) ? "" : hAndroidId;

        return hAndroidId;
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    @SuppressLint({"HardwareIds", "MissingPermission"})
    public static String GetDeviceId(Context pContext)
    {
        String hDeviceId = null;
        try
        {
            TelephonyManager pTelephonyManager = (TelephonyManager)pContext.getSystemService(Context.TELEPHONY_SERVICE);
            hDeviceId = pTelephonyManager.getDeviceId();
        }
        catch (Exception e)
        {
            // Fallback implement.
        }

        hDeviceId = (null == hDeviceId) ? "" : hDeviceId;

        return hDeviceId;
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    @SuppressLint({"HardwareIds", "MissingPermission"})
    @TargetApi(Build.VERSION_CODES.O)
    public static String GetSerial()
    {
        String hSerial = null;
        if(Build.VERSION_CODES.O <= Build.VERSION.SDK_INT)
        {
            try
            {
                hSerial = Build.getSerial();
            }
            catch (Exception e)
            {
                // Fallback implement.
            }
        }
        else
        {
            hSerial = Build.SERIAL;
        }

        hSerial = (null != hSerial && !hSerial.equals(Build.UNKNOWN)) ? hSerial : "";

        return hSerial;
    }

    @SuppressWarnings("UnusedAssignment")
    public static String GetBuildFingerprint()
    {
        String hFingerprint = null;
        hFingerprint = mmAppIdentifier.GetSHA1(Build.FINGERPRINT);
        hFingerprint = (null == hFingerprint) ? "" : hFingerprint;
        return hFingerprint;
    }

    @SuppressLint("ApplySharedPref")
    public static void SaveCurrentUUID(SharedPreferences pPreferences, String pField, UUID pUUID)
    {
        // Write the value out to the Preferences file.
        SharedPreferences.Editor pEditor = pPreferences.edit();
        pEditor.putString(pField, pUUID.toString());
        pEditor.commit();
    }

    @SuppressWarnings({"CharsetObjectCanBeUsed", "UnusedAssignment"})
    public static UUID GenerateUUIDByUniqueString(SharedPreferences pPreferences, String hUniqueString)
    {
        UUID pUUID = null;

        try
        {
            byte[] hBytes = hUniqueString.getBytes("UTF-8");
            pUUID = UUID.nameUUIDFromBytes(hBytes);
        }
        catch (Exception e)
        {
            // Fallback implement.
            pUUID = UUID.randomUUID();
        }

        return pUUID;
    }

    public static void UUIDToBytes(UUID pUUID, byte[] pUUIDBytes)
    {
        if(null != pUUID)
        {
            String pUUIDString = pUUID.toString();
            byte[] pUUIDStringBytes = pUUIDString.getBytes();
            System.arraycopy(pUUIDStringBytes, 0, pUUIDBytes, 0, pUUIDStringBytes.length);
        }
    }

    /**
     * Returns a unique UUID for the current android device. As with all UUIDs,
     * this unique ID is "very highly likely" to be unique across all Android
     * devices. Much more so than ANDROID_ID is.
     *
     * The UUID is generated by using ANDROID_ID as the base key if appropriate,
     * falling back on TelephonyManager.getDeviceID() if ANDROID_ID is known to
     * be incorrect, and finally falling back on a random UUID that's persisted
     * to SharedPreferences if getDeviceID() does not return a usable value.
     *
     * In some rare circumstances, this ID may change. In particular, if the
     * device is factory reset a new device ID may be generated. In addition, if
     * a user upgrades their phone from certain buggy implementations of Android
     * 2.2 to a newer, non-buggy version of Android, the device ID may change.
     * Or, if a user uninstalls your app on a device that has neither a proper
     * Android ID nor a Device ID, this ID may change on reinstallation.
     *
     * Note that if the code falls back on using TelephonyManager.getDeviceId(),
     * the resulting ID will NOT change after a factory reset. Something to be
     * aware of.
     *
     * Works around a bug in Android 2.2 for many devices when using ANDROID_ID
     * directly.
     *
     * @see http://code.google.com/p/android/issues/detail?id=10603
     *
     * @param pUUIDBytes that may be used to uniquely identify your device for most
     *     purposes.
     */

    @SuppressWarnings({"StringBufferReplaceableByString", "UnnecessaryLocalVariable", "ConstantConditions"})
    public static void GenerateUUIDDevice(Context pContext, byte[] pUUIDBytes)
    {
        UUID pUUID = null;

        do
        {
            if(null == pContext)
            {
                break;
            }

            final String pGroup = mmAppIdentifier.MM_APP_IDENTIFIER_PREFS_GROUP;
            final String pField = mmAppIdentifier.MM_APP_IDENTIFIER_PREFS_FIELD_UUID_DEVICE;
            final SharedPreferences pPreferences = pContext.getSharedPreferences(pGroup, Context.MODE_PRIVATE);
            final String hUUIDString = pPreferences.getString(pField, null);
            if (null != hUUIDString)
            {
                // Use the ids previously computed and stored in the
                // prefs file
                pUUID = UUID.fromString(hUUIDString);
                break;
            }

            String hAndroidId = mmAppIdentifier.GetAndroidId(pContext);
            String hDeviceId = mmAppIdentifier.GetDeviceId(pContext);
            String hSerial = mmAppIdentifier.GetSerial();
            String hBuildFingerprint = mmAppIdentifier.GetBuildFingerprint();

            if (hAndroidId.isEmpty() && hDeviceId.isEmpty() && hSerial.isEmpty())
            {
                pUUID = UUID.randomUUID();
                mmAppIdentifier.SaveCurrentUUID(pPreferences, pField, pUUID);
                break;
            }

            {
                StringBuilder hBuilder = new StringBuilder();
                hBuilder.append(hAndroidId).append("-");
                hBuilder.append(hDeviceId).append("-");
                hBuilder.append(hBuildFingerprint).append("-");
                hBuilder.append(hSerial);

                String hUniqueString = hBuilder.toString();
                pUUID = mmAppIdentifier.GenerateUUIDByUniqueString(pPreferences, hUniqueString);
                mmAppIdentifier.SaveCurrentUUID(pPreferences, pField, pUUID);
            }
        }while(false);

        mmAppIdentifier.UUIDToBytes(pUUID, pUUIDBytes);
    }

    @SuppressWarnings({"StringBufferReplaceableByString", "UnnecessaryLocalVariable", "ConstantConditions"})
    public static void GenerateUUIDApp(Context pContext, String pAppId, byte[] pUUIDBytes)
    {
        UUID pUUID = null;

        do
        {
            if(null == pContext)
            {
                break;
            }

            final String pGroup = mmAppIdentifier.MM_APP_IDENTIFIER_PREFS_GROUP;
            final String pField = mmAppIdentifier.MM_APP_IDENTIFIER_PREFS_FIELD_UUID_APP;
            final SharedPreferences pPreferences = pContext.getSharedPreferences(pGroup, Context.MODE_PRIVATE);
            final String hUUIDString = pPreferences.getString(pField, null);
            if (null != hUUIDString)
            {
                // Use the ids previously computed and stored in the
                // prefs file
                pUUID = UUID.fromString(hUUIDString);
                break;
            }

            String hAndroidId = mmAppIdentifier.GetAndroidId(pContext);
            String hDeviceId = mmAppIdentifier.GetDeviceId(pContext);
            String hSerial = mmAppIdentifier.GetSerial();
            String hBuildFingerprint = mmAppIdentifier.GetBuildFingerprint();

            if (hAndroidId.isEmpty() && hDeviceId.isEmpty() && hSerial.isEmpty())
            {
                pUUID = UUID.randomUUID();
                mmAppIdentifier.SaveCurrentUUID(pPreferences, pField, pUUID);
                break;
            }

            {
                StringBuilder hBuilder = new StringBuilder();
                hBuilder.append(hAndroidId).append("-");
                hBuilder.append(hDeviceId).append("-");
                hBuilder.append(hBuildFingerprint).append("-");
                hBuilder.append(hSerial).append("-");
                hBuilder.append(pAppId);

                String hUniqueString = hBuilder.toString();
                pUUID = mmAppIdentifier.GenerateUUIDByUniqueString(pPreferences, hUniqueString);
                mmAppIdentifier.SaveCurrentUUID(pPreferences, pField, pUUID);
            }
        }while (false);

        mmAppIdentifier.UUIDToBytes(pUUID, pUUIDBytes);
    }

    public static void GenerateRandomUUID(byte[] pUUIDBytes)
    {
        UUID pUUID = UUID.randomUUID();
        mmAppIdentifier.UUIDToBytes(pUUID, pUUIDBytes);
    }

    public void GetUUIDDeviceBytes(String pAppId, byte[] pUUIDBytes)
    {
        mmAppIdentifier.GenerateUUIDDevice(this.pContext, pUUIDBytes);
    }

    public void GetUUIDAppBytes(String pAppId, byte[] pUUIDBytes)
    {
        mmAppIdentifier.GenerateUUIDApp(this.pContext, pAppId, pUUIDBytes);
    }
}