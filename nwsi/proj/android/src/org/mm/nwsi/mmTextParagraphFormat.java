package org.mm.nwsi;

import org.mm.graphics.mmGeometry;

import java.util.TreeMap;

@SuppressWarnings({"unused", "Convert2Diamond"})
public class mmTextParagraphFormat
{
    // master text format.
    public mmTextFormat hMasterFormat = new mmTextFormat();
    // font features tag. only support all of range.
    public TreeMap<Integer, Integer> hFontFeatures = new TreeMap<Integer, Integer>();

    // default Natural
    public int hTextAlignment = mmTextFormat.mmTextAlignmentNatural;
    // default Top
    public int hParagraphAlignment = mmTextFormat.mmTextParagraphAlignmentTop;
    // default BreakByWordWrapping.
    public int hLineBreakMode = mmTextFormat.mmTextLineBreakByWordWrapping;
    // default WritingDirectionNatural.
    public int hWritingDirection = mmTextFormat.mmTextWritingDirectionNatural;
    // default 0.0f.
    public float hFirstLineHeadIndent = 0.0f;
    // default 0.0f.
    public float hHeadIndent = 0.0f;
    // default 0.0f.
    public float hTailIndent = 0.0f;
    // default 0.0f.
    public float hLineSpacingAddition = 0.0f;
    // default 1.0f.
    public float hLineSpacingMultiple = 1.0f;

    /*- Shadow -*/
    // (x, y) default (0.0f, 0.0f)
    public float[] hShadowOffset = new float[]{ 0.0f, 0.0f, };
    // default 0, not shadow, 3 generally.
    public float hShadowBlur;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    public float[] hShadowColor = new float[]{ 0.0f, 0.0f, 0.0f, 1.0f, };

    // default LineCapButt.
    public int hLineCap = mmGeometry.mmLineCapButt;
    // default LineJoinMiter
    public int hLineJoin = mmGeometry.mmLineJoinMiter;
    // default is 10.0
    public float hLineMiterLimit = mmGeometry.mmLineMiterLimitDefault;

    // If add this flag, we not need explicitly set hWindowSize and hLayoutRect.
    // An appropriate width and height will be calculated internally.
    // Only suitable for single line text, uniform font size format for full text.
    // default 0
    public int hAlignBaseline = 0;
    // single line descent.
    public float hLineDescent = 0.0f;
    // suitable size.
    public float[] hSuitableSize = new float[]{ 0.0f, 0.0f, };;

    // TextWindow constraint dimensions box.
    // window size.
    // (w, h) default { 40.0f, 20.0f, },
    public float[] hWindowSize = new float[]{ 40.0f, 20.0f, };

    // TextLayout constraint dimensions box.
    // layout rect.
    // (x, y, w, h) default { 0.0f, 0.0f, 40.0f, 20.0f, },
    public float[] hLayoutRect =  new float[]{ 0.0f, 0.0f, 40.0f, 20.0f, };

    // font display density. default 1.0.
    public float hDisplayDensity = 1.0f;

    /*- EditBox -*/
    // caret index. default 0.
    public long hCaretIndex = 0;
    // selection range [l, r]. default [0, 0].
    public long[] hSelection = new long[] { 0, 0 };
	// caret wrapping. 0 not 1 wrapping. default 0.
    public int hCaretWrapping = 0;
    // caret status. 0 hide 1 show. default 0.
    public int hCaretStatus = 0;
    // selection status. 0 release 1 capture. default 0.
    public int hSelectionStatus = 0;
    // caret width. default 1.
    public float hCaretWidth = 1;
    // default rgba { 1.0f, 1.0f, 1.0f, 1.0f, },
    public float[] hCaretColor = new float[] { 1.0f, 1.0f, 1.0f, 1.0f, };
    // capture color. default ARGB 0xFF4080FF ( 64, 128, 255).
    public float[] hSelectionCaptureColor = new float[] {  64 / 255.f, 128 / 255.f, 255 / 255.f, 1.f, };
    // release color. default ARGB 0xFFDCDCDC (220, 220, 220).
    public float[] hSelectionReleaseColor = new float[] { 220 / 255.f, 220 / 220.f, 255 / 255.f, 1.f, };

    // object for platform implement.
    public long pObject = 0;
    
    public void Init()
    {
        this.hMasterFormat.Init();
        this.hFontFeatures.clear();
    }
    public void Destroy()
    {
        this.hMasterFormat.Destroy();
        this.hFontFeatures.clear();
    }

    public void SetFontFeature(int hTag, int hParameter)
    {
        this.hFontFeatures.put(hTag, hParameter);
    }
    public void RmvFontFeature(int hTag)
    {
        this.hFontFeatures.remove(hTag);
    }
    public int GetFontFeature(int hTag)
    {
        Integer u = this.hFontFeatures.get(hTag);
        return (null == u) ? 0 : u;
    }
    public void ClearFontFeature()
    {
        this.hFontFeatures.clear();
    }

    @SuppressWarnings("JavaJniMissingFunction")
    public native void NativeUpdateSuitableData();
}
