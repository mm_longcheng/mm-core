/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Resources;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.TreeMap;

@SuppressWarnings({"unused", "TryWithIdenticalCatches"})
public class mmUINotchScreen
{
    // https://dev.vivo.com.cn/documentCenter/doc/103
    // https://open.oppomobile.com/wiki/doc#id=10159
    // https://dev.mi.com/console/doc/detail?pId=1293
    // https://developer.huawei.com/consumer/cn/devservice/doc/50114?from=timeline

    public interface NotchScreenApiInterface
    {
        // is notch support.
        boolean IsNotchSupport();

        // radius[2] {width, height}
        void GetNotchSize(int[] hSize);

        // radius[2] {top, bottom}
        void GetCornerRadius(int[] hRadius);
    }

    public static class NotchScreenApiBase implements NotchScreenApiInterface
    {
        protected Activity pActivity = null;

        public NotchScreenApiBase()
        {

        }

        public void SetActivity(Activity pActivity)
        {
            this.pActivity = pActivity;
        }

        public Activity GetActivity()
        {
            return this.pActivity;
        }

        @Override
        public boolean IsNotchSupport()
        {
            return false;
        }

        // radius[2] {width, height}
        @Override
        public void GetNotchSize(int[] hSize)
        {
            hSize[0] = 0;
            hSize[1] = 0;
        }

        // radius[2] {top, bottom}
        @Override
        public void GetCornerRadius(int[] hRadius)
        {
            hRadius[0] = 0;
            hRadius[1] = 0;
        }
    }

    public static class NotchScreenApiManufacturer_VIVO extends NotchScreenApiBase
    {
        // height / width    19:9
        // Circle radius     24dp
        // Notch area        height 27dp width 100dp
        // status bar height 32dp
        public NotchScreenApiManufacturer_VIVO()
        {

        }

        @SuppressLint("PrivateApi")
        @Override
        public boolean IsNotchSupport()
        {
            boolean hIsNotch = false;
            try
            {
                // 0x00000020: Is there notch
                // 0x00000008: Is there rounded corners
                ClassLoader cl = this.pActivity.getClassLoader();
                Class<?> cls = cl.loadClass("android.util.FtFeature");
                Method method = cls.getMethod("isFeatureSupport", int.class);
                Object hIsNotchObj = method.invoke(cls,0x00000020);
                hIsNotch = null != hIsNotchObj && (Boolean) hIsNotchObj;
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (NoSuchMethodException e)
            {
                e.printStackTrace();
            }
            catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
                e.printStackTrace();
            }
            return hIsNotch;
        }

        @Override
        public void GetNotchSize(int[] hSize)
        {
            // VIVO is a special value. width 100dp height 27dp.
            hSize[0] = mmUIDisplayMetrics.Dp2PxInteger(this.pActivity, 100);
            hSize[1] = mmUIDisplayMetrics.Dp2PxInteger(this.pActivity, 27);
        }

        // radius[2] {top, bottom}
        @Override
        public void GetCornerRadius(int[] hRadius)
        {
            // VIVO is a special value. Corner radius 24dp.
            hRadius[0] = mmUIDisplayMetrics.Dp2PxInteger(this.pActivity, 24);
            hRadius[1] = mmUIDisplayMetrics.Dp2PxInteger(this.pActivity, 24);
        }
    }

    public static class NotchScreenApiManufacturer_OPPO extends NotchScreenApiBase
    {
        // height / width    19:9
        // Circle radius     80px
        // Notch area        height 324px width 80px
        // status bar height 80px
        public NotchScreenApiManufacturer_OPPO()
        {

        }

        @Override
        public boolean IsNotchSupport()
        {
            boolean hIsNotch = false;
            try
            {
                PackageManager pPackageManager = this.pActivity.getPackageManager();
                hIsNotch = pPackageManager.hasSystemFeature("com.oppo.feature.screen.heteromorphism");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return hIsNotch;
        }

        @Override
        public void GetNotchSize(int[] hSize)
        {
            // OPPO is a special value. width 324px height 80px.
            hSize[0] = 324;
            hSize[1] = 80;
        }

        // radius[2] {top, bottom}
        @Override
        public void GetCornerRadius(int[] hRadius)
        {
            // OPPO is a special value. Corner radius 80px.
            // Rounded corners are consistent with notch height.
            hRadius[0] = 80;
            hRadius[1] = 80;
        }
    }

    public static class NotchScreenApiManufacturer_HUAWEI extends NotchScreenApiBase
    {
        // height / width    19:9
        // Circle radius     API()
        // Notch area        height API() width API()
        // status bar height API()
        public NotchScreenApiManufacturer_HUAWEI()
        {

        }

        @Override
        public boolean IsNotchSupport()
        {
            boolean hIsNotch = false;
            try
            {
                ClassLoader cl = this.pActivity.getClassLoader();
                Class<?> cls = cl.loadClass("com.huawei.android.util.HwNotchSizeUtil");
                Method method = cls.getMethod("hasNotchInScreen");
                Object hIsNotchObj = method.invoke(cls);
                hIsNotch = null != hIsNotchObj && (Boolean) hIsNotchObj;
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (NoSuchMethodException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return hIsNotch;
        }

        @SuppressLint("PrivateApi")
        @Override
        public void GetNotchSize(int[] hSize)
        {
            int[] hNotchSize = null;

            try
            {
                ClassLoader cl = this.pActivity.getClassLoader();
                Class<?> cls = cl.loadClass("com.huawei.android.util.HwNotchSizeUtil");
                Method method = cls.getMethod("getNotchSize");
                hNotchSize = (int[]) method.invoke(cls);
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (NoSuchMethodException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            if(null != hNotchSize)
            {
                hSize[0] = hNotchSize[0];
                hSize[1] = hNotchSize[1];
            }
            else
            {
                // not valid notch.
                hSize[0] = 0;
                hSize[1] = 0;
            }
        }

        // radius[2] {top, bottom}
        @Override
        public void GetCornerRadius(int[] hRadius)
        {
            int[] hSize = new int[2];
            this.GetNotchSize(hSize);

            // HUAWEI not API for corner radius.
            hRadius[0] = hSize[1];
            hRadius[1] = hSize[1];
        }
    }

    public static class NotchScreenApiManufacturer_XIAOMI extends NotchScreenApiBase
    {
        // height / width    18:9 18.7:9
        // Circle radius     API()
        // Notch area        height API() width API()
        // status bar height API()
        public NotchScreenApiManufacturer_XIAOMI()
        {

        }

        @SuppressLint("PrivateApi")
        @Override
        public boolean IsNotchSupport()
        {
            boolean hIsNotch = false;
            try
            {
                ClassLoader cl = this.pActivity.getClassLoader();
                Class<?> cls = cl.loadClass("android.os.SystemProperties");
                Method method = cls.getMethod("getInt", String.class, int.class);
                Object hMiuiNotchObj = method.invoke(null, "ro.miui.notch", 0);
                int hMiuiNotch = (null == hMiuiNotchObj) ? 0 : (Integer) hMiuiNotchObj;
                hIsNotch = (1 == hMiuiNotch);
            }
            catch (ClassNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (NoSuchMethodException e)
            {
                e.printStackTrace();
            }
            catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
            catch (InvocationTargetException e)
            {
                e.printStackTrace();
            }
            return hIsNotch;
        }

        @Override
        public void GetNotchSize(int[] hSize)
        {
            Resources pResources = this.pActivity.getResources();
            int resourceWidthId = pResources.getIdentifier("notch_width", "dimen", "android");
            int resourceHeightId = pResources.getIdentifier("notch_height", "dimen", "android");
            if(resourceWidthId > 0 && resourceHeightId > 0)
            {
                hSize[0] = pResources.getDimensionPixelSize(resourceWidthId);
                hSize[1] = pResources.getDimensionPixelSize(resourceHeightId);
            }
            else
            {
                // not valid notch.
                hSize[0] = 0;
                hSize[1] = 0;
            }
        }

        // radius[2] {top, bottom}
        @Override
        public void GetCornerRadius(int[] hRadius)
        {
            Resources pResources = this.pActivity.getResources();
            int hResourceIdT = pResources.getIdentifier("rounded_corner_radius_top", "dimen", "android");
            if (hResourceIdT > 0)
            {
                hRadius[0] = pResources.getDimensionPixelSize(hResourceIdT);
            }
            else
            {
                hRadius[0] = 0;
            }

            int hResourceIdB = pResources.getIdentifier("rounded_corner_radius_bottom", "dimen", "android");
            if (hResourceIdB > 0)
            {
                hRadius[1] = pResources.getDimensionPixelSize(hResourceIdB);
            }
            else
            {
                hRadius[0] = 0;
            }
        }
    }

    public static final TreeMap<String, NotchScreenApiBase> const_VersionOManufacturerApi = new TreeMap<String, NotchScreenApiBase>()
    {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {
            this.put("huawei"  , new NotchScreenApiManufacturer_HUAWEI());
            this.put("xiaomi"  , new NotchScreenApiManufacturer_XIAOMI());
            this.put("vivo"    , new NotchScreenApiManufacturer_VIVO());
            this.put("oppo"    , new NotchScreenApiManufacturer_OPPO());
        }
    };

    static public NotchScreenApiBase GetManufacturerApiByName(String pManufacturer)
    {
        return const_VersionOManufacturerApi.get(pManufacturer);
    }
}
