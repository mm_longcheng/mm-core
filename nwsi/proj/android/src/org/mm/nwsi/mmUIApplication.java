package org.mm.nwsi;

import org.mm.core.mmLogger;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

@SuppressWarnings("unused")
public class mmUIApplication extends FrameLayout
{
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = mmUIApplication.class.getSimpleName();
    
    public mmDevice pDevice = null;
    public mmPackageAssets pPackageAssets = null;
    public mmAppIdentifier pAppIdentifier = null;
    public mmSecurityStore pSecurityStore = null;
    public mmNativeApplication pNativeApplication = null;
    public mmUIViewController pUIViewController = null;
    
    public mmActivityMaster pActivityMaster = null;
    public Activity pActivity = null;
    public String pModuleName = "mm.nwsi";
    public String pRenderSystemName = "OpenGL ES 2.x Rendering Subsystem";
    public String pLoggerFileName = "nwsi";
    public int hLoggerLevel = mmLogger.MM_LOG_INFO;

    // FullScreen.
    private boolean hUIFullScreenLayout = false;
    
    public mmUIApplication(Context context)
    {
        super(context);
    }
    public mmUIApplication(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    public mmUIApplication(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }
    
    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }

    public void SetActivity(Activity pActivity)
    {
        this.pActivity = pActivity;
    }
    public void SetActivityMaster(mmActivityMaster pActivityMaster)
    {
        this.pActivityMaster = pActivityMaster;
    }
    
    // default is "mm.nwsi"
    public void SetModuleName(String pModuleName)
    {
    	this.pModuleName = pModuleName;
    }
    // Render system name.
    public void SetRenderSystemName(String pRenderSystemName) 
    {
    	this.pRenderSystemName = pRenderSystemName;
    }

    // default is "nwsi"
    public void SetLoggerFileName(String pLoggerFileName) 
    {
    	this.pLoggerFileName = pLoggerFileName;
    }
    // default is MM_LOG_INFO
    public void SetLoggerLevel(int hLoggerLevel) 
    {
    	this.hLoggerLevel = hLoggerLevel;
    }

    public void SetUIFullScreenLayout(boolean hUIFullScreenLayout)
    {
        this.hUIFullScreenLayout = hUIFullScreenLayout;
    }

    static public void LoadLibrary(final String pLibraryName)
    {
        mmNativeLibrary.LoadLibrary(pLibraryName);
    }
    
    public void OnFinishLaunching()
    {
    	this.pDevice = new mmDevice();
    	this.pPackageAssets = new mmPackageAssets();
    	this.pAppIdentifier = new mmAppIdentifier();
    	this.pSecurityStore = new mmSecurityStore();
        this.pNativeApplication = new mmNativeApplication();
        this.pUIViewController = new mmUIViewController(this.pActivity);

        int w = FrameLayout.LayoutParams.MATCH_PARENT;
        int h = FrameLayout.LayoutParams.WRAP_CONTENT;
        FrameLayout.LayoutParams hLayoutParams = new FrameLayout.LayoutParams(w, h);
        
        this.pDevice.Init();
        this.pPackageAssets.Init();
        this.pAppIdentifier.Init();
        this.pSecurityStore.Init();
        this.pUIViewController.Init();
        this.pNativeApplication.Init();

        this.pDevice.SetContext(this.pActivity);
        this.pPackageAssets.SetActivity(this.pActivity);
        this.pAppIdentifier.SetContext(this.pActivity);
        this.pSecurityStore.SetActivity(this.pActivity);
        
    	this.pNativeApplication.SetModuleName(this.pModuleName);
        this.pNativeApplication.SetRenderSystemName(this.pRenderSystemName);
        this.pNativeApplication.SetLoggerFileName(this.pLoggerFileName);
        this.pNativeApplication.SetLoggerLevel(this.hLoggerLevel);
        this.pNativeApplication.SetActivity(this.pActivity);
        this.pNativeApplication.SetDevice(this.pDevice);
        this.pNativeApplication.SetPackageAssets(this.pPackageAssets);
        this.pNativeApplication.SetAppIdentifier(this.pAppIdentifier);
        this.pNativeApplication.SetSecurityStore(this.pSecurityStore);
        this.pNativeApplication.SetActivityMaster(this.pActivityMaster.hNativePtr);
        
        this.pUIViewController.SetActivity(this.pActivity);
        this.pUIViewController.SetNativeApplication(this.pNativeApplication);
        this.pUIViewController.SetUIFullScreenLayout(this.hUIFullScreenLayout);
        this.addView(this.pUIViewController, hLayoutParams);
        
        this.pDevice.OnFinishLaunching();
        this.pPackageAssets.OnFinishLaunching();
        this.pAppIdentifier.OnFinishLaunching();
        this.pSecurityStore.OnFinishLaunching();
        this.pNativeApplication.OnFinishLaunching();
        this.pUIViewController.OnFinishLaunching();
    }
    public void OnBeforeTerminate()
    {
    	this.pUIViewController.OnBeforeTerminate();
        this.pNativeApplication.OnBeforeTerminate();
        this.pSecurityStore.OnBeforeTerminate();
        this.pAppIdentifier.OnBeforeTerminate();
        this.pPackageAssets.OnBeforeTerminate();
        this.pDevice.OnBeforeTerminate();

        this.pUIViewController.SetActivity(null);
        this.pNativeApplication.SetActivity(null);
        this.pSecurityStore.SetActivity(null);
        this.pAppIdentifier.SetContext(null);
        this.pPackageAssets.SetActivity(null);
        this.pDevice.SetContext(null);

        this.pUIViewController.Destroy();
        this.pNativeApplication.Destroy();
        this.pAppIdentifier.Destroy();
        this.pPackageAssets.Destroy();
        this.pSecurityStore.Destroy();
        this.pDevice.Destroy();
        
        this.pUIViewController = null;
        this.pNativeApplication = null;
        this.pAppIdentifier = null;
        this.pPackageAssets = null;
        this.pSecurityStore = null;
        this.pDevice = null;
    }
    
    public void OnEnterBackground()
    {
    	this.pUIViewController.OnEnterBackground();
        this.pNativeApplication.OnEnterBackground();
    }
    public void OnEnterForeground()
    {
        this.pNativeApplication.OnEnterForeground();
        this.pUIViewController.OnEnterForeground();
    }
    
    public void OnStart()
    {
    	this.pNativeApplication.OnStart();
    	this.pUIViewController.OnStart();
    }
    public void OnInterrupt()
    {
    	this.pUIViewController.OnInterrupt();
        this.pNativeApplication.OnInterrupt();
    }
    public void OnShutdown()
    {
    	this.pUIViewController.OnShutdown();
        this.pNativeApplication.OnShutdown();
    }
    public void OnJoin()
    {
    	this.pUIViewController.OnJoin();
        this.pNativeApplication.OnJoin();
    }
}