package org.mm.nwsi;

import android.graphics.Typeface;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.UpdateAppearance;

import org.mm.core.mmLogger;

@SuppressWarnings({"unused"})
public class mmTextDrawingEffect extends CharacterStyle implements UpdateAppearance, ParcelableSpan
{
    private static final String TAG = mmTextDrawingEffect.class.getSimpleName();

    public static final int mmANTextFormatSpanTypeId = 100;

    public mmTextDrawingContext pDrawingContext = null;
    public mmTextFormat pTextFormat = null;

    public mmTextDrawingEffect()
    {

    }

    public void SetDrawingContext(mmTextDrawingContext pDrawingContext)
    {
        this.pDrawingContext = pDrawingContext;
    }

    public void SetTextFormat(mmTextFormat pTextFormat)
    {
        this.pTextFormat = pTextFormat;
    }

    public int getSpanTypeIdInternal()
    {
        return mmANTextFormatSpanTypeId;
    }

    public void writeToParcelInternal(Parcel dest, int flags)
    {

    }

    @Override
    public int getSpanTypeId()
    {
        return this.getSpanTypeIdInternal();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        this.writeToParcelInternal(dest, flags);
    }

    @SuppressWarnings({"ConstantConditions", "IfStatementWithIdenticalBranches"})
    @Override
    public void updateDrawState(final TextPaint tp)
    {
        do
        {
            if (null == this.pDrawingContext)
            {
                mmLogger.LogT(TAG + " updateDrawState pDrawingContext is null.");
                break;
            }
            if (null == this.pTextFormat)
            {
                mmLogger.LogT(TAG + " updateDrawState pTextFormat is null.");
                break;
            }
            mmTextParagraphFormat pParagraphFormat = this.pDrawingContext.pParagraphFormat;
            if (null == pParagraphFormat)
            {
                mmLogger.LogT(TAG + " updateDrawState pParagraphFormat is null.");
                break;
            }
            mmTextFormat hMasterFormat = pParagraphFormat.hMasterFormat;
            if (null == hMasterFormat)
            {
                mmLogger.LogT(TAG + " updateDrawState hMasterFormat is null.");
                break;
            }
            mmGraphicsFactory pGraphicsFactory = this.pDrawingContext.pGraphicsFactory;
            if (null == pGraphicsFactory)
            {
                mmLogger.LogT(TAG + " updateDrawState pGraphicsFactory is null.");
                break;
            }
            // Font style.
            if (!mmTextFormat.IsFontStyleEquals(hMasterFormat, this.pTextFormat))
            {
                Typeface pTypeface = mmTextPlatform.MakeTypeface(pGraphicsFactory, this.pTextFormat);
                float hObliqueSkewX = mmTextPlatform.GetFontObliqueSkewX(this.pTextFormat.hFontStyle);
                tp.setTypeface(pTypeface);
                tp.setTextSkewX(hObliqueSkewX);
            }
            // Font size.
            if (hMasterFormat.hFontSize != this.pTextFormat.hFontSize)
            {
                float hFontSize = mmTextPlatform.GetFontSize(this.pTextFormat.hFontSize);
                float hFontPixelSize = hFontSize * pParagraphFormat.hDisplayDensity;
                tp.setTextSize(hFontPixelSize);
            }

            switch (this.pDrawingContext.hPass)
            {
                case 2:
                {
                    // Foreground 2
                    if (0.0f != this.pTextFormat.hForegroundColor[3] && 0.0 >= this.pTextFormat.hStrokeWidth)
                    {
                        int hForegroundColor = mmGraphicsPlatform.ColorMake(this.pTextFormat.hForegroundColor);
                        tp.setColor(hForegroundColor);
                        tp.setStrokeWidth(0.0f);
                        tp.setStyle(TextPaint.Style.FILL);
                    }
                    else
                    {
                        tp.setColor(0x00000000);
                        tp.setStrokeWidth(0.0f);
                        tp.setStyle(TextPaint.Style.FILL);
                    }
                }
                break;

                case 3:
                {
                    // Stroke     3
                    if (0.0f != this.pTextFormat.hStrokeWidth)
                    {
                        // -3 is fill and stroke.
                        float hDisplayDensity = pParagraphFormat.hDisplayDensity;
                        float hStrokeWidth = Math.abs(this.pTextFormat.hStrokeWidth * hDisplayDensity);
                        int hStrokeColor = mmGraphicsPlatform.ColorMake(this.pTextFormat.hStrokeColor);
                        tp.setColor(hStrokeColor);
                        tp.setStrokeWidth(hStrokeWidth);
                        tp.setStyle(TextPaint.Style.STROKE);
                    }
                    else
                    {
                        tp.setColor(0x00000000);
                        tp.setStrokeWidth(0.0f);
                        tp.setStyle(TextPaint.Style.STROKE);
                    }
                }
                break;

                default:
                {
                    tp.setColor(0x00000000);
                    tp.setStrokeWidth(0.0f);
                    tp.setStyle(TextPaint.Style.STROKE);
                }
                break;
            }
        } while(false);
    }
}
