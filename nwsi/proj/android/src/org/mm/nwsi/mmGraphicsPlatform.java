package org.mm.nwsi;

import android.graphics.Color;
import android.graphics.Paint;

@SuppressWarnings({"unused"})
public class mmGraphicsPlatform
{
    static public Paint.Join GetLineJoin(int hLineJoin)
    {
        final Paint.Join[] gStyleTable =
        {
            Paint.Join.MITER   , // 0 mmLineJoinMiter
            Paint.Join.ROUND   , // 1 mmLineJoinRound
            Paint.Join.BEVEL   , // 2 mmLineJoinBevel
        };
        if (0 <= hLineJoin && hLineJoin < gStyleTable.length)
        {
            return gStyleTable[hLineJoin];
        }
        else
        {
            return Paint.Join.MITER;
        }
    }

    static public Paint.Cap GetCapStyle(int hLineCap)
    {
        final Paint.Cap[] gStyleTable =
        {
            Paint.Cap.BUTT     , // 0 mmLineCapButt
            Paint.Cap.ROUND    , // 1 mmLineCapRound
            Paint.Cap.SQUARE   , // 2 mmLineCapSquare
        };
        if (0 <= hLineCap && hLineCap < gStyleTable.length)
        {
            return gStyleTable[hLineCap];
        }
        else
        {
            return Paint.Cap.BUTT;
        }
    }
    
    static public int ColorMake(final float[] hColor)
    {
        int r = Math.round(hColor[0] * 255.0f);
        int g = Math.round(hColor[1] * 255.0f);
        int b = Math.round(hColor[2] * 255.0f);
        int a = Math.round(hColor[3] * 255.0f);
        return Color.argb(a, r, g, b);
    }
}
