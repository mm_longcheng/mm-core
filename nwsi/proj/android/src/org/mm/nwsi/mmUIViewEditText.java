/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import java.util.TreeMap;

import org.mm.core.mmLogger;
import org.mm.nwsi.mmISurface.mmEventKeypad;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;

@SuppressLint("ObsoleteSdkInt")
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class mmUIViewEditText extends View implements MenuItem.OnMenuItemClickListener
{
    private static final String TAG = mmUIViewEditText.class.getSimpleName();

    private mmUIInputConnection pEditInputConnection = null;
    
    private int hImeOption = EditorInfo.IME_ACTION_NEXT;
    private int hInputType = InputType.TYPE_CLASS_TEXT;    
    
    private int hKeypadType = mmISurface.mmKeypadTypeDefault;
    private int hKeypadAppearance = mmISurface.mmKeypadAppearanceDefault;
    private int hKeypadReturnKey = mmISurface.mmKeypadReturnKeyDefault;
    
    private int hImeReturnKeyType = EditorInfo.IME_ACTION_NONE;
    private int hImeInputType = EditorInfo.IME_ACTION_NONE;
    
    private mmUIViewSurfaceMaster pUIViewSurfaceMaster = null;
    private mmNativeClipboard pNativeClipboard = null;

    private mmUIEditTextActionMode pUIEditTextActionMode = null;
    private ActionMode pActionMode = null;

    public static final TreeMap<Integer, Integer> const_InputType = new TreeMap<Integer, Integer>()
    {
        // android.text.InputType
        //
        // TYPE_NULL                             Constant Value: 0           (0x00000000)
        // TYPE_MASK_CLASS                       Constant Value: 15          (0x0000000f)
        // TYPE_MASK_VARIATION                   Constant Value: 4080        (0x00000ff0)
        // TYPE_MASK_FLAGS                       Constant Value: 16773120    (0x00fff000)
        //
        // TYPE_CLASS_TEXT                       Constant Value: 1           (0x00000001)
        // TYPE_CLASS_NUMBER                     Constant Value: 2           (0x00000002)
        // TYPE_CLASS_PHONE                      Constant Value: 3           (0x00000003)
        // TYPE_CLASS_DATETIME                   Constant Value: 4           (0x00000004)
        //
        // TYPE_TEXT_VARIATION_NORMAL            Constant Value: 0           (0x00000000)
        // TYPE_TEXT_VARIATION_URI               Constant Value: 16          (0x00000010)
        // TYPE_TEXT_VARIATION_EMAIL_ADDRESS     Constant Value: 32          (0x00000020)
        // TYPE_TEXT_VARIATION_EMAIL_SUBJECT     Constant Value: 48          (0x00000030)
        // TYPE_TEXT_VARIATION_SHORT_MESSAGE     Constant Value: 64          (0x00000040)
        // TYPE_TEXT_VARIATION_LONG_MESSAGE      Constant Value: 80          (0x00000050)
        // TYPE_TEXT_VARIATION_PERSON_NAME       Constant Value: 96          (0x00000060)
        // TYPE_TEXT_VARIATION_POSTAL_ADDRESS    Constant Value: 112         (0x00000070)
        // TYPE_TEXT_VARIATION_PASSWORD          Constant Value: 128         (0x00000080)
        // TYPE_TEXT_VARIATION_VISIBLE_PASSWORD  Constant Value: 144         (0x00000090)
        // TYPE_TEXT_VARIATION_WEB_EDIT_TEXT     Constant Value: 160         (0x000000a0)
        // TYPE_TEXT_VARIATION_FILTER            Constant Value: 176         (0x000000b0)
        // TYPE_TEXT_VARIATION_PHONETIC          Constant Value: 192         (0x000000c0)
        // TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS Constant Value: 208         (0x000000d0)
        // TYPE_TEXT_VARIATION_WEB_PASSWORD      Constant Value: 224         (0x000000e0)
        // TYPE_NUMBER_VARIATION_NORMAL          Constant Value: 0           (0x00000000)
        // TYPE_NUMBER_VARIATION_PASSWORD        Constant Value: 16          (0x00000010)
        // TYPE_DATETIME_VARIATION_NORMAL        Constant Value: 0           (0x00000000)
        // TYPE_DATETIME_VARIATION_DATE          Constant Value: 16          (0x00000010)
        // TYPE_DATETIME_VARIATION_TIME          Constant Value: 32          (0x00000020)
        //
        // TYPE_NUMBER_FLAG_SIGNED               Constant Value: 4096        (0x00001000)
        // TYPE_NUMBER_FLAG_DECIMAL              Constant Value: 8192        (0x00002000)
        //
        // TYPE_TEXT_FLAG_CAP_CHARACTERS         Constant Value: 4096        (0x00001000)
        // TYPE_TEXT_FLAG_CAP_WORDS              Constant Value: 8192        (0x00002000)
        // TYPE_TEXT_FLAG_CAP_SENTENCES          Constant Value: 16384       (0x00004000)
        // TYPE_TEXT_FLAG_AUTO_CORRECT           Constant Value: 32768       (0x00008000)
        // TYPE_TEXT_FLAG_AUTO_COMPLETE          Constant Value: 65536       (0x00010000)
        // TYPE_TEXT_FLAG_MULTI_LINE             Constant Value: 131072      (0x00020000)
        // TYPE_TEXT_FLAG_IME_MULTI_LINE         Constant Value: 262144      (0x00040000)
        // TYPE_TEXT_FLAG_NO_SUGGESTIONS         Constant Value: 524288      (0x00080000)
        
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        {
            this.put(mmISurface.mmKeypadTypeDefault               , InputType.TYPE_CLASS_TEXT);
            this.put(mmISurface.mmKeypadTypeASCIICapable          , InputType.TYPE_CLASS_TEXT);
            this.put(mmISurface.mmKeypadTypeNumbersAndPunctuation , InputType.TYPE_CLASS_TEXT);
            this.put(mmISurface.mmKeypadTypeUrl                   , InputType.TYPE_CLASS_TEXT   | InputType.TYPE_TEXT_VARIATION_URI);
            this.put(mmISurface.mmKeypadTypeNumberPad             , InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
            this.put(mmISurface.mmKeypadTypePhonePad              , InputType.TYPE_CLASS_PHONE);
            this.put(mmISurface.mmKeypadTypeNamePhonePad          , InputType.TYPE_CLASS_TEXT   | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
            this.put(mmISurface.mmKeypadTypeEmailAddress          , InputType.TYPE_CLASS_TEXT   | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            this.put(mmISurface.mmKeypadTypeDecimalPad            , InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            this.put(mmISurface.mmKeypadTypeTwitter               , InputType.TYPE_CLASS_TEXT);
            this.put(mmISurface.mmKeypadTypeWebSearch             , InputType.TYPE_CLASS_TEXT   | InputType.TYPE_TEXT_VARIATION_URI);
            this.put(mmISurface.mmKeypadTypeASCIICapableNumberPad , InputType.TYPE_CLASS_TEXT);
        }
    };
    
    public static final TreeMap<Integer, Integer> const_ReturnType = new TreeMap<Integer, Integer>()
    {
        // EditorInfo
        //
        // IME_NULL                              Constant Value: 0           (0x00000000)
        // IME_MASK_ACTION                       Constant Value: 255         (0x000000ff)
        //
        // IME_ACTION_UNSPECIFIED                Constant Value: 0           (0x00000000)
        // IME_ACTION_NONE                       Constant Value: 1           (0x00000001)
        // IME_ACTION_GO                         Constant Value: 2           (0x00000002)
        // IME_ACTION_SEARCH                     Constant Value: 3           (0x00000003)
        // IME_ACTION_SEND                       Constant Value: 4           (0x00000004)
        // IME_ACTION_NEXT                       Constant Value: 5           (0x00000005)
        // IME_ACTION_DONE                       Constant Value: 6           (0x00000006)
        // IME_ACTION_PREVIOUS                   Constant Value: 7           (0x00000007)
        //
        // IME_FLAG_NO_PERSONALIZED_LEARNING     Constant Value: 16777216    (0x01000000)
        // IME_FLAG_NO_FULLSCREEN                Constant Value: 33554432    (0x02000000)
        // IME_FLAG_NAVIGATE_PREVIOUS            Constant Value: 67108864    (0x04000000)
        // IME_FLAG_NAVIGATE_NEXT                Constant Value: 134217728   (0x08000000)
        // IME_FLAG_NO_EXTRACT_UI                Constant Value: 268435456   (0x10000000)
        // IME_FLAG_NO_ACCESSORY_ACTION          Constant Value: 536870912   (0x20000000)
        // IME_FLAG_NO_ENTER_ACTION              Constant Value: 1073741824  (0x40000000)
        // IME_FLAG_FORCE_ASCII                  Constant Value: -2147483648 (0x80000000)
        
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        {
            this.put(mmISurface.mmKeypadReturnKeyDefault       , EditorInfo.IME_ACTION_UNSPECIFIED);
            this.put(mmISurface.mmKeypadReturnKeyGo            , EditorInfo.IME_ACTION_GO);
            this.put(mmISurface.mmKeypadReturnKeyGoogle        , EditorInfo.IME_ACTION_SEARCH);
            this.put(mmISurface.mmKeypadReturnKeyJoin          , EditorInfo.IME_ACTION_NONE);
            this.put(mmISurface.mmKeypadReturnKeyNext          , EditorInfo.IME_ACTION_NEXT);
            this.put(mmISurface.mmKeypadReturnKeyRoute         , EditorInfo.IME_ACTION_GO);
            this.put(mmISurface.mmKeypadReturnKeySearch        , EditorInfo.IME_ACTION_SEARCH);
            this.put(mmISurface.mmKeypadReturnKeySend          , EditorInfo.IME_ACTION_SEND);
            this.put(mmISurface.mmKeypadReturnKeyYahoo         , EditorInfo.IME_ACTION_SEARCH);
            this.put(mmISurface.mmKeypadReturnKeyDone          , EditorInfo.IME_ACTION_DONE);
            this.put(mmISurface.mmKeypadReturnKeyEmergencyCall , EditorInfo.IME_ACTION_GO);
            this.put(mmISurface.mmKeypadReturnKeyContinue      , EditorInfo.IME_ACTION_NEXT);
        }
    };

    static public int GetInputType(int hInputKey)
    {
        int _Type;
        Integer pInteger = const_InputType.get(hInputKey);
        if (null == pInteger)
        {
            _Type = InputType.TYPE_CLASS_TEXT;
        }
        else
        {
            _Type = pInteger;   
        }
        return _Type;
    }

    static public int GetReturnKeyType(int hReturnKey)
    {
        int _Type;
        Integer pInteger = const_ReturnType.get(hReturnKey);
        if (null == pInteger)
        {
            _Type = EditorInfo.IME_ACTION_UNSPECIFIED;
        }
        else
        {
            _Type = pInteger;
        }
        return _Type;
    }
    
    public mmUIViewEditText(Context context)
    {
        super(context);
    }
    public mmUIViewEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    public mmUIViewEditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }
    @TargetApi(21)
    public mmUIViewEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) 
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    
    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetUIViewSurfaceMaster(mmUIViewSurfaceMaster pUIViewSurfaceMaster)
    {
        this.pUIViewSurfaceMaster = pUIViewSurfaceMaster;
    }
    public void SetNativeClipboard(mmNativeClipboard hNativeClipboard)
    {
        this.pNativeClipboard = hNativeClipboard;
    }

    public void OnFinishLaunching()
    {
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        
        this.hImeInputType = GetInputType(this.hKeypadType);
        this.hImeReturnKeyType = GetReturnKeyType(this.hKeypadReturnKey);        

        this.pEditInputConnection = new mmUIInputConnection();
        this.pEditInputConnection.SetUIViewSurfaceMaster(this.pUIViewSurfaceMaster);
        this.pEditInputConnection.SetUIViewEditText(this);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            this.pUIEditTextActionMode = new mmUIEditTextActionMode();
            this.pUIEditTextActionMode.Init();
            this.pUIEditTextActionMode.SetUIViewSurfaceMaster(this.pUIViewSurfaceMaster);
            this.pUIEditTextActionMode.SetUIViewEditText(this);
            this.pUIEditTextActionMode.SetNativeClipboard(this.pNativeClipboard);
        }
    }
    public void OnBeforeTerminate()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            this.pUIEditTextActionMode.Destroy();
            this.pUIEditTextActionMode = null;
        }
        
        this.pEditInputConnection = null;
    }
    
    public void SetKeypadType(int hType)
    {
        this.hKeypadType = hType;
        this.hImeInputType = GetInputType(this.hKeypadType);
        
        mmLogger.LogD(TAG + " hKeypadType: " + this.hKeypadType);
    }
    public void SetKeypadAppearance(int hAppearance)
    {
        this.hKeypadAppearance = hAppearance;
        
        mmLogger.LogD(TAG + " hKeypadAppearance: " + this.hKeypadAppearance);
    }
    public void SetKeypadReturnKey(int hReturnKey)
    {
        this.hKeypadReturnKey = hReturnKey;
        this.hImeReturnKeyType = GetReturnKeyType(this.hKeypadReturnKey);
        
        mmLogger.LogD(TAG + " hKeypadReturnKey: " + this.hKeypadReturnKey);
    }
    
    public void OnUpdateSelection(InputMethodManager imm)
    {
        this.pEditInputConnection.OnUpdateSelection(imm, this);
    }
    
    public int OnKeypadPressed(int keyCode, KeyEvent event)
    {
        mmEventKeypad pKeypad = this.pUIViewSurfaceMaster.hKeypad;

        this.pUIViewSurfaceMaster.OnTextDataHandler(keyCode, event);

        // Native API.
        this.pUIViewSurfaceMaster.NativeOnKeypadPressed(pKeypad);

        return pKeypad.handle;
    }

    public int OnKeypadRelease(int keyCode, KeyEvent event)
    {
        mmEventKeypad pKeypad = this.pUIViewSurfaceMaster.hKeypad;

        this.pUIViewSurfaceMaster.OnTextDataHandler(keyCode, event);

        // Native API.
        this.pUIViewSurfaceMaster.NativeOnKeypadRelease(pKeypad);

        return pKeypad.handle;
    }
    
    @TargetApi(23)
    public void OnContextMenuShow()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            mmLogger.LogD(TAG + " OnShowContextMenu support floating ActionMode.");
            this.pActionMode = this.startActionMode(this.pUIEditTextActionMode, ActionMode.TYPE_FLOATING);
            this.pActionMode.invalidateContentRect();
        }
        else
        {
            mmLogger.LogD(TAG + " OnShowContextMenu not support floating ActionMode.");
        }
    }
    
    public void OnContextMenuHide()
    {
        if(null != this.pActionMode)
        {
            this.pActionMode.finish();
        }
    }

    public void OnContextMenuUpdate()
    {
        if(null != this.pActionMode)
        {
            this.pActionMode.invalidate();
        }
    }

    public void OnDestroyActionMode(ActionMode mode) 
    {
        this.pActionMode = null;
    }
    
    // First of all, we have to rewrite a method in the View, return true, 
    // is to make this View editable text state, return false by default.
    @Override
    public boolean onCheckIsTextEditor()
    {
        // Log.d(TAG, TAG + " onCheckIsTextEditor");
        return true;
    }

    // The second is to rewrite this method, you need to return an InputConnect object, 
    // this is a bridge with the input content of the input method.
    public InputConnection onCreateInputConnection(EditorInfo outAttrs)
    {
        mmLogger.LogD(TAG + " onCreateInputConnection");

        this.pEditInputConnection.OnUpdateSelectionRangeValue();
        
        // outAttrs The Options of input method we need to set.
        
        this.hImeOption = EditorInfo.IME_NULL;        
        this.hImeOption |= EditorInfo.IME_FLAG_NO_EXTRACT_UI;
        this.hImeOption |= EditorInfo.IME_FLAG_NO_FULLSCREEN;        
        this.hImeOption |= this.hImeReturnKeyType;        
        
        this.hInputType = InputType.TYPE_NULL;
        this.hInputType |= this.hImeInputType;
        
        outAttrs.imeOptions = this.hImeOption;
        outAttrs.inputType = this.hInputType;
        
        outAttrs.initialSelStart = this.pEditInputConnection.hRange[0];
        outAttrs.initialSelEnd = this.pEditInputConnection.hRange[1];
        
        return this.pEditInputConnection;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        int hScanCode = event.getScanCode();
        int hKeyCode = event.getKeyCode();
        int hUnicodeChar = event.getUnicodeChar();
        
        mmLogger.LogD(TAG + " onKeyDown " + hScanCode + + hKeyCode + + hUnicodeChar);

        if(0 != this.OnKeypadPressed(keyCode, event))
        {
            return super.onKeyDown(keyCode, event);
        }
        else
        {
            return true;
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        int hScanCode = event.getScanCode();
        int hKeyCode = event.getKeyCode();
        int hUnicodeChar = event.getUnicodeChar();

        mmLogger.LogD(TAG + " onKeyUp " + hScanCode + + hKeyCode + + hUnicodeChar);
        
        if(0 == this.OnKeypadRelease(keyCode, event))
        {
            return super.onKeyUp(keyCode, event);
        }
        else
        {
            return true;
        }
    }
    
    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event) 
    {
        mmLogger.LogI(TAG + " onKeyShortcut " + event.toString());
        
        if (event.hasModifiers(KeyEvent.META_CTRL_ON)) 
        {
            // Handle Ctrl-only shortcuts.
            switch (keyCode) 
            {
            case KeyEvent.KEYCODE_X:
                mmLogger.LogI(TAG + " onKeyShortcut cut.");
                this.pEditInputConnection.performContextMenuAction(android.R.id.cut);
                break;
            case KeyEvent.KEYCODE_C:
                mmLogger.LogI(TAG + " onKeyShortcut copy.");
                this.pEditInputConnection.performContextMenuAction(android.R.id.copy);
                break;
            case KeyEvent.KEYCODE_V:
                mmLogger.LogI(TAG + " onKeyShortcut paste.");
                this.pEditInputConnection.performContextMenuAction(android.R.id.paste);
                break;
            case KeyEvent.KEYCODE_A:
                mmLogger.LogI(TAG + " onKeyShortcut selectAll.");
                this.pEditInputConnection.performContextMenuAction(android.R.id.selectAll);
                break;
            default:
                break;
            }
        }
        
        return super.onKeyShortcut(keyCode, event);
    }
    
    // MenuItem.OnMenuItemClickListener
    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        int hItemId = item.getItemId();
        mmLogger.LogI(TAG + " onMenuItemClick ItemId: " + hItemId);
        this.pEditInputConnection.performContextMenuAction(hItemId);
        return false;
    }
}
