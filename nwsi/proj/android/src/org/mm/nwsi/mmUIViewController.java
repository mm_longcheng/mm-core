/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import org.mm.core.mmLogger;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

@SuppressWarnings("unused")
public class mmUIViewController extends FrameLayout implements View.OnSystemUiVisibilityChangeListener
{
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = mmUIViewController.class.getSimpleName();
    
    // strong reference.
    private mmUIViewSurfaceMaster pUIViewSurfaceMaster = null;
    
    // weak reference.
    private Activity pActivity = null;
    // weak reference.
    private mmNativeApplication pNativeApplication = null;

    // FullScreen.
    private boolean hUIFullScreenLayout = false;
    private int hUIVisibilityRestore = 0;
    private int hUIVisibilitySetting = 0;
    
    public mmUIViewController(Context context)
    {
        super(context);
    }
    public mmUIViewController(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    public mmUIViewController(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }
    @TargetApi(21)
    public mmUIViewController(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) 
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
    
    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    // ===========================================================
    // Member function.
    // ===========================================================
    public void SetActivity(Activity pActivity)
    {
        this.pActivity = pActivity;
    }

    public void SetUIFullScreenLayout(boolean hUIFullScreenLayout)
    {
        this.hUIFullScreenLayout = hUIFullScreenLayout;
    }

    public void SetNativeApplication(mmNativeApplication pNativeApplication)
    {
        this.pNativeApplication = pNativeApplication;
    }
    
    public void OnFinishLaunching()
    {
        int w = FrameLayout.LayoutParams.MATCH_PARENT;
        int h = FrameLayout.LayoutParams.WRAP_CONTENT;
        FrameLayout.LayoutParams hLayoutParams = new FrameLayout.LayoutParams(w, h);

        long hSurfaceMaster = this.pNativeApplication.NativeGetSurfaceMaster();
        mmNativeClipboard pNativeClipboard = this.pNativeApplication.pNativeClipboard;
        
        this.pUIViewSurfaceMaster = new mmUIViewSurfaceMaster(this.pActivity);
        this.pUIViewSurfaceMaster.Init();
        this.pUIViewSurfaceMaster.SetActivity(this.pActivity);
        this.pUIViewSurfaceMaster.SetSurfaceMaster(hSurfaceMaster);
        this.pUIViewSurfaceMaster.SetNativeClipboard(pNativeClipboard);
        this.pUIViewSurfaceMaster.OnFinishLaunching();
        this.addView(this.pUIViewSurfaceMaster, hLayoutParams);

        this.pUIViewSurfaceMaster.setOnSystemUiVisibilityChangeListener(this);
        
        // cache current value.
        this.hUIVisibilityRestore = this.pUIViewSurfaceMaster.getSystemUiVisibility();
        this.hUIVisibilitySetting = mmUIFullScreenLayout.GetUIViewFullScreenVisibility();

        this.UpdateFullScreenLayout();

        Log.i(TAG, TAG + " OnFinishLaunching success.");
    }
    public void OnBeforeTerminate()
    {
        this.removeView(this.pUIViewSurfaceMaster);

        this.pUIViewSurfaceMaster.setOnSystemUiVisibilityChangeListener(null);
        
        this.pUIViewSurfaceMaster.OnBeforeTerminate();
        this.pUIViewSurfaceMaster.Destroy();
        
        this.pUIViewSurfaceMaster = null;

        Log.i(TAG, TAG + " OnBeforeTerminate success.");
    }

    private void UpdateFullScreenLayout()
    {
        if(this.hUIFullScreenLayout)
        {
            this.pUIViewSurfaceMaster.setSystemUiVisibility(this.hUIVisibilitySetting);
        }
        else
        {
            this.pUIViewSurfaceMaster.setSystemUiVisibility(this.hUIVisibilityRestore);
        }
    }

    public void OnEnterBackground()
    {
        // UIViewSurfaceMaster OnEnterBackground only trigger by surface.
        // this.pUIViewSurfaceMaster.OnEnterBackground();
    }
    public void OnEnterForeground()
    {
        // UIViewSurfaceMaster OnEnterForeground only trigger by surface.
        // this.pUIViewSurfaceMaster.OnEnterForeground();
    }

    @SuppressWarnings({"Convert2Lambda", "Anonymous2MethodRef"})
    @Override
    public void onSystemUiVisibilityChange(int visibility)
    {
        // Note that system bars will only be "visible" if none of the
        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
        if (0 == (visibility & View.SYSTEM_UI_FLAG_FULLSCREEN))
        {
            // adjustments to your UI, such as showing the action bar or
            // other navigational controls.
            mmLogger.LogT(TAG + " onSystemUiVisibilityChange INACTIVE FULLSCREEN.");
        }
        else
        {
            // adjustments to your UI, such as hiding the action bar or
            // other navigational controls.
            mmLogger.LogT(TAG + " onSystemUiVisibilityChange ACTIVATE FULLSCREEN.");
        }

        if(this.hUIVisibilitySetting != visibility)
        {
            // Delayed restore our settings.
            Runnable pRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    mmUIViewController.this.UpdateFullScreenLayout();
                }
            };
            long delayMillis = 1000;
            this.postDelayed(pRunnable, delayMillis);
        }

        String hVisibilityString = String.format("0x%08X", visibility);
        mmLogger.LogT(TAG + " onSystemUiVisibilityChange hVisibility: " + hVisibilityString);
    }
    
    public void OnStart()
    {
        this.pUIViewSurfaceMaster.OnStart();
    }
    public void OnInterrupt()
    {
        this.pUIViewSurfaceMaster.OnInterrupt();
    }
    public void OnShutdown()
    {
        this.pUIViewSurfaceMaster.OnShutdown();
    }
    public void OnJoin()
    {
        this.pUIViewSurfaceMaster.OnJoin();
    }
}