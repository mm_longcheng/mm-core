/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;

@SuppressWarnings("unused")
public class mmNativeClipboard
{
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = mmNativeClipboard.class.getSimpleName();
    
    // weak reference.
    long hNativePtr = 0;
    // weak reference.
    public Activity pActivity = null;
    
    public void Init()
    {
        this.pActivity = null;

        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        this.pActivity = null;

        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetActivity(Activity pActivity)
    {
        this.pActivity = pActivity;
    }
    
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    void SetClipboardText(String text)
    {
        do
        {
            Object pClipboardService = this.pActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipboardManager pClipboard = (ClipboardManager)pClipboardService;
            if(null == pClipboard)
            {
                break;
            }
            
            
            ClipData pClipData = null;
            
            if(null == text)
            {
                break;
            }
            
            // Note: the CEGUI Clipboard only send utf8 Encoding buffer.
            pClipData = ClipData.newPlainText("text", text);
            pClipboard.setPrimaryClip(pClipData);
        }while(false);
    }

    @SuppressWarnings("ConstantConditions")
    String GetClipboardText()
    {
        String text = null;
        
        do
        {
            Object pClipboardService = this.pActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipboardManager pClipboard = (ClipboardManager)pClipboardService;
            if(null == pClipboard)
            {
                break;
            }
            
            ClipData pClipData = pClipboard.getPrimaryClip();
            if(null == pClipData)
            {
                break;
            }
            
            if(0 == pClipData.getItemCount())
            {
                break;
            }
            
            ClipData.Item pItem = pClipData.getItemAt(0);
            if(null == pItem)
            {
                break;
            }
            
            // Note: the CEGUI Clipboard only recv utf8 Encoding buffer.
            CharSequence pCharSequence = pItem.getText();
            if(null == pCharSequence)
            {
                break;
            }
            
            text = pCharSequence.toString();
        }while(false);
        
        return text;
    }
}