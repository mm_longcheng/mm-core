/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import org.mm.core.mmLogger;
import org.mm.nwsi.mmISurface.mmEventKeypad;
import org.mm.nwsi.mmISurface.mmEventKeypadStatus;
import org.mm.nwsi.mmISurface.mmEventTouch;
import org.mm.nwsi.mmISurface.mmEventTouchs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InputDevice;
import android.view.InputDevice.MotionRange;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

@SuppressWarnings({"unused", "JavaJniMissingFunction"})
public class mmUIViewSurfaceMaster extends FrameLayout implements SurfaceHolder.Callback
{
    // ===========================================================
    // Constants
    // ===========================================================
    private static final String TAG = mmUIViewSurfaceMaster.class.getSimpleName();

    // strong reference.
    public long hNativePtr = 0;
    // strong reference.
    public mmUIViewWindow hUIViewWindow = null;
    // strong reference.
    public mmUIViewEditText hUIViewEditText = null;
    // weak reference.
    public Surface pSurface = null;
    public boolean hSurfaceComplete = false;

    public mmEventTouchs hTouchs = new mmEventTouchs();
    public mmEventKeypad hKeypad = new mmEventKeypad();

    public mmUIViewFinger hUIViewFinger = new mmUIViewFinger();

    public mmUIViewRunnable hUIViewRunnable = new mmUIViewRunnable();
    public mmUIViewLayoutListener hUIViewLayoutListener = new mmUIViewLayoutListener();

    // strong reference.
    public Handler pHandler = null;
    
    public boolean hQueryComplete = false;

    public Activity pActivity = null;

    public int[] hViewSize = new int[2];
    public int[] hSafeArea = new int[4];
    
    public double hDisplayDensity = 1.0;

    private mmNativeClipboard pNativeClipboard = null;

    static public int mmOSModifierMask(int hModifiers)
    {
        int mask = 0;

        if(0 != (hModifiers & KeyEvent.META_SHIFT_ON      )) { mask |= mmKeyCode.MM_MODIFIER_SHIFT   ; }
        if(0 != (hModifiers & KeyEvent.META_SHIFT_LEFT_ON )) { mask |= mmKeyCode.MM_MODIFIER_SHIFT   ; }
        if(0 != (hModifiers & KeyEvent.META_SHIFT_RIGHT_ON)) { mask |= mmKeyCode.MM_MODIFIER_SHIFT   ; }
        if(0 != (hModifiers & KeyEvent.META_ALT_ON        )) { mask |= mmKeyCode.MM_MODIFIER_OPTION  ; }
        if(0 != (hModifiers & KeyEvent.META_ALT_LEFT_ON   )) { mask |= mmKeyCode.MM_MODIFIER_OPTION  ; }
        if(0 != (hModifiers & KeyEvent.META_ALT_RIGHT_ON  )) { mask |= mmKeyCode.MM_MODIFIER_OPTION  ; }
        if(0 != (hModifiers & KeyEvent.META_CTRL_ON       )) { mask |= mmKeyCode.MM_MODIFIER_CONTROL ; }
        if(0 != (hModifiers & KeyEvent.META_CTRL_LEFT_ON  )) { mask |= mmKeyCode.MM_MODIFIER_CONTROL ; }
        if(0 != (hModifiers & KeyEvent.META_CTRL_RIGHT_ON )) { mask |= mmKeyCode.MM_MODIFIER_CONTROL ; }
        if(0 != (hModifiers & KeyEvent.META_META_ON       )) { mask |= mmKeyCode.MM_MODIFIER_COMMAND ; }
        if(0 != (hModifiers & KeyEvent.META_META_LEFT_ON  )) { mask |= mmKeyCode.MM_MODIFIER_COMMAND ; }
        if(0 != (hModifiers & KeyEvent.META_META_RIGHT_ON )) { mask |= mmKeyCode.MM_MODIFIER_COMMAND ; }
        if(0 != (hModifiers & KeyEvent.META_SYM_ON        )) { mask |= mmKeyCode.MM_MODIFIER_SYM     ; }
        if(0 != (hModifiers & KeyEvent.META_FUNCTION_ON   )) { mask |= mmKeyCode.MM_MODIFIER_FUNCTION; }

        return mask;
    }

    public mmUIViewSurfaceMaster(Context context)
    {
        super(context);
    }
    public mmUIViewSurfaceMaster(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
    public mmUIViewSurfaceMaster(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }
    @TargetApi(21)
    public mmUIViewSurfaceMaster(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) 
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void Init()
    {
        this.NativeAlloc();
        
        this.hTouchs.Init();
        this.hKeypad.Init();

        this.hUIViewFinger.Init();

        this.hUIViewRunnable.Init();
        this.hUIViewLayoutListener.Init();
        
        this.NativeInit();

        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        this.NativeDestroy();

        this.hUIViewLayoutListener.Destroy();
        this.hUIViewRunnable.Destroy();

        this.hUIViewFinger.Destroy();
        
        this.hKeypad.Destroy();
        this.hTouchs.Destroy();
        
        this.NativeDealloc();

        Log.i(TAG, TAG + " Destroy succeed.");
    }

    // ===========================================================
    // Member function.
    // ===========================================================
    public void SetActivity(Activity pActivity)
    {
        this.pActivity = pActivity;
    }
    
    public Activity GetActivity()
    {
        return this.pActivity;
    }
    
    public void SetSurfaceMaster(long hSurfaceMaster)
    {
        this.NativeSetSurfaceMaster(hSurfaceMaster);
    }

    public void SetNativeClipboard(mmNativeClipboard pNativeClipboard)
    {
        this.pNativeClipboard = pNativeClipboard;
    }

    public void OnFinishLaunching()
    {
        Context pContext = this.getContext();
        
        this.hDisplayDensity = mmUIDisplayMetrics.GetDisplayDensity(this.pActivity);
        this.NativeSetDisplayDensity(this.hDisplayDensity);
        Log.i(TAG, TAG + " OnFinishLaunching hDisplayDensity: " + this.hDisplayDensity);

        int w = FrameLayout.LayoutParams.MATCH_PARENT;
        int h = FrameLayout.LayoutParams.WRAP_CONTENT;
        FrameLayout.LayoutParams hUIViewEditTextLayoutParams = new FrameLayout.LayoutParams(w, h);
        this.hUIViewEditText = new mmUIViewEditText(pContext);
        this.hUIViewEditText.Init();
        this.hUIViewEditText.SetUIViewSurfaceMaster(this);
        this.hUIViewEditText.SetNativeClipboard(this.pNativeClipboard);
        this.hUIViewEditText.SetKeypadType(mmISurface.mmKeypadTypeDefault);
        this.hUIViewEditText.SetKeypadAppearance(mmISurface.mmKeypadAppearanceDefault);
        this.hUIViewEditText.SetKeypadReturnKey(mmISurface.mmKeypadReturnKeyDefault);
        this.hUIViewEditText.OnFinishLaunching();
        this.addView(this.hUIViewEditText, hUIViewEditTextLayoutParams);
        
        this.hUIViewWindow = new mmUIViewWindow(pContext);
        SurfaceHolder pSurfaceHolder = this.hUIViewWindow.getHolder();
        pSurfaceHolder.addCallback(this);
        FrameLayout.LayoutParams hUIViewEAGL2LayoutParams = new FrameLayout.LayoutParams(w, h);
        this.hUIViewWindow.Init();
        this.hUIViewWindow.OnFinishLaunching();
        this.addView(this.hUIViewWindow, hUIViewEAGL2LayoutParams);
        
        // focus the hUIViewEditText.
        this.hUIViewEditText.requestFocus();
        
        // default hidden the soft input.
        Window pWindow = this.pActivity.getWindow();
        pWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        // Native API.
        this.NativeThreadEnter();

        // Native API.
        this.NativeOnFinishLaunching();
        
        this.pHandler = new Handler(Looper.getMainLooper());
        
        this.hUIViewRunnable.SetUIViewSurfaceMaster(this);
        this.hUIViewRunnable.SetHandler(this.pHandler);
        this.hUIViewRunnable.OnFinishLaunching();
        
        this.hUIViewLayoutListener.SetUIViewSurfaceMaster(this);
        this.hUIViewLayoutListener.OnFinishLaunching();
        
        Log.i(TAG, TAG + " OnFinishLaunching succeed.");
    }
    public void OnBeforeTerminate()
    {
        this.hUIViewLayoutListener.OnBeforeTerminate();
        
        this.hUIViewRunnable.OnBeforeTerminate();
        this.hUIViewRunnable.SetHandler(null);
        this.hUIViewRunnable.SetUIViewSurfaceMaster(null);
        
        this.pHandler = null;
        
        // Native API.
        this.NativeOnBeforeTerminate();

        // Native API.
        this.NativeThreadLeave();
        
        this.removeView(this.hUIViewWindow);
        this.hUIViewWindow.OnBeforeTerminate();
        this.hUIViewWindow.Destroy();
        this.hUIViewWindow = null;
        this.pSurface = null;
        
        this.removeView(this.hUIViewEditText);
        this.hUIViewEditText.OnBeforeTerminate();
        this.hUIViewEditText.Destroy();
        this.hUIViewEditText = null;
        
        Log.i(TAG, TAG + " OnBeforeTerminate succeed.");
    }
    
    public void OnEnterBackground()
    {
        // Native API.
        this.NativeOnEnterBackground();
    }
    public void OnEnterForeground()
    {
        // Native API.
        this.NativeOnEnterForeground();
    }

    public void OnStart()
    {
        // Native API.
        this.NativeOnStart();
    }
    public void OnInterrupt()
    {
        // Native API.
        this.NativeOnInterrupt();
    }
    public void OnShutdown()
    {
        // Native API.
        this.NativeOnShutdown();
    }
    public void OnJoin()
    {
        // Native API.
        this.NativeOnJoin();
    }

    public void OnDisplayLinkUpdate()
    {
        // Native API.
        this.NativeOnUpdate();
        
        this.NativeThreadPeekMessage();
        
        this.NativeOnTimewait();
    }

    public void OnTextDataHandler(int keyCode, KeyEvent event)
    {
        int hScanCode = event.getScanCode();
        int hKeyCode = event.getKeyCode();
        int hModifiers = event.getModifiers();
        String hCharacters = event.getCharacters();

        // hCharacters can be null.
        hCharacters = (null == hCharacters) ? "" : hCharacters;

        mmEventKeypad pKeypad = this.hKeypad;

        int length = hCharacters.length();
        length = Math.min(length, 3);

        int hVirtualKeycode = mmKeyCode.GetVirtualKeycode(hKeyCode);
        boolean hHandleKeyCode = mmKeyCode.HandleKeyCode(hKeyCode, hVirtualKeycode);

        java.util.Arrays.fill(pKeypad.text, 0, 4, (char)0);

        pKeypad.modifier_mask = mmOSModifierMask(hModifiers);
        pKeypad.key = hVirtualKeycode;
        pKeypad.length = length;
        hCharacters.getChars(0, length, pKeypad.text, 0);
        pKeypad.handle = hHandleKeyCode ? 1 : 0;
    }

    public void PrintSamples(MotionEvent ev) 
    {
        final int historySize = ev.getHistorySize();
        final int pointerCount = ev.getPointerCount();
        for (int h = 0; h < historySize; h++) 
        {
            Log.i(TAG, TAG + " history " + "h: " + h + " At time " + ev.getHistoricalEventTime(h) + ":");
            for (int p = 0; p < pointerCount; p++) 
            {
                int i = ev.getPointerId(p);
                float x = ev.getHistoricalX(p, h);
                float y = ev.getHistoricalY(p, h);
                Log.i(TAG, TAG + " " + "  pointer " + i + ": (" + x + ", " + y + ")");
            }
        }
        Log.i(TAG, TAG + " current " + "At time " + ev.getEventTime() + ":");
        for (int p = 0; p < pointerCount; p++) 
        {
            int i = ev.getPointerId(p);
            float x = ev.getX(p);
            float y = ev.getY(p);
            Log.i(TAG, TAG + " " + "  pointer " + i + ": (" + x + ", " + y + ")");
        }
    }

    @SuppressWarnings({"UnusedAssignment", "RedundantCast"})
    public void TouchEventOneFinger(MotionEvent pMotionEvent, int i, int n, int phase)
    {
        mmEventTouch e = this.hTouchs.GetForIndexReference(n);

        double hDisplayDensity = this.hDisplayDensity;

        double force_value = 0.0;
        double force_maximum = 0.0;
        
        InputDevice pDevice = pMotionEvent.getDevice();
        MotionRange pPressureRange = pDevice.getMotionRange(MotionEvent.AXIS_PRESSURE);
        force_value = (double)pMotionEvent.getPressure(i);
        force_maximum = (double)pPressureRange.getMax();

        // Some devices have no implement for this value, always returning 1.0.
        // We will fix it to normal value 0.25.
        force_maximum = (0 >= force_maximum) ? 1.0 : force_maximum;
        force_value = (force_maximum <= force_value) ? (0.25 * force_maximum) : force_value;
        
        e.motion_id = pMotionEvent.getPointerId(i);
        e.tap_count = pMotionEvent.getPointerCount();
        e.phase = phase;
        e.modifier_mask = 0;
        e.button_mask = pMotionEvent.getButtonState();
        e.abs_x = (double)(pMotionEvent.getX(i) / hDisplayDensity);
        e.abs_y = (double)(pMotionEvent.getY(i) / hDisplayDensity);
        e.rel_x = (double)0.0;
        e.rel_y = (double)0.0;
        e.force_value = (double)force_value;
        e.force_maximum = (double)force_maximum;
        e.major_radius = (double)(pMotionEvent.getTouchMajor(i) / hDisplayDensity);
        e.minor_radius = (double)(pMotionEvent.getTouchMinor(i) / hDisplayDensity);
        e.size_value = (double)pMotionEvent.getSize(i);
        e.timestamp = pMotionEvent.getEventTime();

        // supplement and cache finger data.
        this.hUIViewFinger.SupplementCacheFinger(e);
        
        // Log.i(TAG, TAG + "pressure range max:" + pPressureRange.getMax());
    }
    @SuppressWarnings("UnusedAssignment")
    public void TouchEventAllFinger(MotionEvent pMotionEvent, int phase)
    {
        int count = pMotionEvent.getPointerCount();
        int i = 0;
        for (i = 0; i < count; i++) 
        {
            this.TouchEventOneFinger(pMotionEvent, i, i, phase);
        }
    }

    // ===========================================================
    // Override function.
    // ===========================================================
    // SurfaceHolder.Callback
    @Override
    public void surfaceCreated(SurfaceHolder holder) 
    {
        Surface pSurface = holder.getSurface();
        if (null != pSurface && pSurface.isValid()) 
        {
            mmUISafeAreaLayout.GetSafeAreaLayoutGuide(this.pActivity, this, this.hSafeArea);

            this.hViewSize[0] = this.getWidth();
            this.hViewSize[1] = this.getHeight();

            if (this.hSurfaceComplete)
            {
                this.pSurface = pSurface;

                // Native API.
                this.NativeSetSurface(this.pSurface);
                this.NativeOnSurfaceCreated(this.hSafeArea);

                this.NativeOnEnterForeground();
            }
            else
            {
                this.pSurface = pSurface;

                // Native API.
                this.NativeSetSurface(this.pSurface);
                this.NativeOnSurfaceCreated(this.hSafeArea);

                this.NativeOnSurfaceComplete();

                this.NativeOnEnterForeground();

                this.hSurfaceComplete = true;
            }
        }

        mmLogger.LogI(TAG + " surfaceCreated.");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) 
    {
        if (null != this.pSurface && this.pSurface.isValid())
        {
            // Native API.
            this.NativeOnEnterBackground();

            this.NativeOnSurfaceDestroyed();
            this.NativeSetSurface(null);
            
            this.pSurface = null;
        }

        mmLogger.LogI(TAG + " surfaceDestroyed.");
    }

    @SuppressWarnings({"ConstantConditions", "RedundantCast"})
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format,final int width, final int height) 
    {
        String hFormatString = mmPixelFormat.PixelFormatToString(format);
        String hFormatWindowSizeString = "format:" + format  + "(" + hFormatString + ")" + " " + width + "x" + height;
        mmLogger.LogI(TAG + " surfaceChanged " + hFormatWindowSizeString);

        // Reference www.libsdl.org for android SDLActivity.java.
        do
        {
            if (null == this.pSurface)
            {
                // ignore this event.
                mmLogger.LogI(TAG + " Surface is not ready, ignore this event.");
                break;
            }
            
            double min = (double)Math.min(width, height);
            double max = (double)Math.max(width, height);
            if (max / min >= 1.20) 
            {
                int ro = this.pActivity.getRequestedOrientation();
                if ((ro == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT || 
                     ro == ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT) && width > height)
                {
                    mmLogger.LogI(TAG + " Not need handle event " + hFormatWindowSizeString);
                    break;
                }
                if ((ro == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE || 
                     ro == ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE) && width < height)
                {
                    mmLogger.LogI(TAG + " Not need handle event " + hFormatWindowSizeString);
                    break;
                }
            }
            else
            {
                mmLogger.LogI(TAG + " On such aspect-ratio. Could be a square resolution.");
            }

            if (this.hViewSize[0] == width && this.hViewSize[1] == height)
            {
                // the view size not change.
                break;
            }

            if(this.isInEditMode())
            {
                // edit mode not trigger SurfaceChanged event.
                break;
            }

            mmLogger.LogI(TAG + " NativeOnSurfaceChanged " + hFormatWindowSizeString);

            this.hViewSize[0] = width;
            this.hViewSize[1] = height;

            mmUISafeAreaLayout.GetSafeAreaLayoutGuide(this.pActivity, this, this.hSafeArea);

            // Native API.
            this.NativeOnSurfaceChanged(this.hSafeArea, this.hViewSize);
        }while(false);
    }

    // View
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) 
    {
        int hScanCode = event.getScanCode();
        int hKeyCode = event.getKeyCode();
        int hUnicodeChar = event.getUnicodeChar();
        
        mmLogger.LogD(TAG + " onKeyDown " + hScanCode + + hKeyCode + + hUnicodeChar);

        if(0 != this.hUIViewEditText.OnKeypadPressed(keyCode, event))
        {
            return super.onKeyDown(keyCode, event);
        }
        else
        {
            return true;
        }
    }
    @Override
    public boolean onKeyUp(int keyCode,  KeyEvent event) 
    {
        int hScanCode = event.getScanCode();
        int hKeyCode = event.getKeyCode();
        int hUnicodeChar = event.getUnicodeChar();

        mmLogger.LogD(TAG + " onKeyUp " + hScanCode + + hKeyCode + + hUnicodeChar);
        
        if(0 == this.hUIViewEditText.OnKeypadRelease(keyCode, event))
        {
            return super.onKeyUp(keyCode, event);
        }
        else
        {
            return true;
        }
    }
    @Override
    public boolean performClick() 
    {
        return super.performClick();
    }

    @SuppressWarnings("CommentedOutCode")
    @Override
    public boolean onTouchEvent(MotionEvent pMotionEvent)
    {
        // quick debug.
        //
        // Log.i(TAG, TAG + " onTouchEvent " + pMotionEvent.getAction());
        // this.PrintSamples(pMotionEvent);

        int hAction = pMotionEvent.getAction();

        switch (hAction & MotionEvent.ACTION_MASK)
        {
        case MotionEvent.ACTION_POINTER_DOWN:
            {
                // https://www.android-doc.com/reference/android/view/MotionEvent.html
                // Constant for getActionMasked(): A non-primary pointer has gone down.
                // 
                // We can only handle one finger at here.
                //
                // ACTION_POINTER_ID_SHIFT deprecated in API level 8. Renamed to ACTION_POINTER_INDEX_SHIFT.
                int index = hAction >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                int count = 1;
    
                this.hTouchs.AlignedMemory(count);
                this.TouchEventOneFinger(pMotionEvent, index, 0, mmISurface.mmTouchBegan);
    
                // Native API.
                this.NativeOnTouchsBegan(this.hTouchs);
            }
            break;

        case MotionEvent.ACTION_DOWN:
            {
                // there are only one finger on the screen.
                int index = 0;
                int count = 1;
    
                this.hTouchs.AlignedMemory(count);
                this.TouchEventOneFinger(pMotionEvent, index, 0, mmISurface.mmTouchBegan);
    
                // Native API.
                this.NativeOnTouchsBegan(this.hTouchs);
            }
            break;

        case MotionEvent.ACTION_MOVE:
            {
                int count = pMotionEvent.getPointerCount();
    
                this.hTouchs.AlignedMemory(count);
                this.TouchEventAllFinger(pMotionEvent, mmISurface.mmTouchMoved);
    
                // Native API.
                this.NativeOnTouchsMoved(this.hTouchs);
            }
            break;

        case MotionEvent.ACTION_POINTER_UP:
            {
                // https://www.android-doc.com/reference/android/view/MotionEvent.html
                // Constant for getActionMasked(): A non-primary pointer has gone down.
                // 
                // We can only handle one finger at here.
                //
                // ACTION_POINTER_ID_SHIFT deprecated in API level 8. Renamed to ACTION_POINTER_INDEX_SHIFT.
                int index = pMotionEvent.getAction() >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                int count = 1;
    
                this.hTouchs.AlignedMemory(count);
                this.TouchEventOneFinger(pMotionEvent, index, 0, mmISurface.mmTouchEnded);
    
                // Native API.
                this.NativeOnTouchsEnded(this.hTouchs);
            }
            break;

        case MotionEvent.ACTION_UP:
            {
                // there are only one finger on the screen.
                int index = 0;
                int count = 1;
    
                this.hTouchs.AlignedMemory(count);
                this.TouchEventOneFinger(pMotionEvent, index, 0, mmISurface.mmTouchEnded);
                
                // Native API.
                this.NativeOnTouchsEnded(this.hTouchs);
    
                // we clear finger at ACTION_UP.
                this.hUIViewFinger.ClearFingerValue();
                
                // onTouchEvent should call performClick when a click is detected.
                this.performClick();
            }
            break;

        case MotionEvent.ACTION_CANCEL:
            {
                int count = pMotionEvent.getPointerCount();
    
                this.hTouchs.AlignedMemory(count);
                this.TouchEventAllFinger(pMotionEvent, mmISurface.mmTouchBreak);
                
                // Native API.
                this.NativeOnTouchsBreak(this.hTouchs);
    
                // we clear finger at ACTION_CANCEL.
                this.hUIViewFinger.ClearFingerValue();
            }
            break;
        
        default:
            break;
        }

        return true;
    }
    @Override
    protected void onSizeChanged(int pNewSurfaceWidth, int pNewSurfaceHeight, int pOldSurfaceWidth, int pOldSurfaceHeight)
    {
        mmLogger.LogI(TAG + " onSizeChanged " + 
            "new: " + pNewSurfaceWidth + "x" + pNewSurfaceHeight + " " + 
            "old: " + pOldSurfaceWidth + "x" + pOldSurfaceHeight);
    }

    // ===========================================================
    // native c -> java
    // ===========================================================
    public void OnSetKeypadType(int hType)
    {
        this.hUIViewEditText.SetKeypadType(hType);
    }
    public void OnSetKeypadAppearance(int hAppearance)
    {
        this.hUIViewEditText.SetKeypadAppearance(hAppearance);
    }
    public void OnSetKeypadReturnKey(int hReturnKey)
    {
        this.hUIViewEditText.SetKeypadReturnKey(hReturnKey);
    }
    public void OnKeypadStatusShow()
    {
        mmLogger.LogT(TAG + " OnKeypadStatusShow.");
        Object pInputMethodService = this.pActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(null != pInputMethodService)
        {
            // focus the hUIViewEditText.
            this.hUIViewEditText.requestFocus();
            
            InputMethodManager imm = (InputMethodManager)pInputMethodService;
            imm.showSoftInput(this.hUIViewEditText, 0);
            // notify update selection.
            this.hUIViewEditText.OnUpdateSelection(imm);
            
            this.hUIViewEditText.OnContextMenuShow();
        }
    }
    public void OnKeypadStatusHide()
    {
        mmLogger.LogT(TAG + " OnKeypadStatusHide.");
        Object pInputMethodService = this.pActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(null != pInputMethodService)
        {
            this.hUIViewEditText.OnContextMenuHide();
            
            InputMethodManager imm = (InputMethodManager)pInputMethodService;
            imm.hideSoftInputFromWindow(this.hUIViewEditText.getWindowToken(), 0);
        }
    }

    // ===========================================================
    // native java -> c
    // ===========================================================
    public native void NativeAlloc();
    public native void NativeDealloc();
    public native void NativeInit();
    public native void NativeDestroy();
    
    public native void NativeSetSurfaceMaster(long hSurfaceMaster);
    public native void NativeSetDisplayDensity(double hDisplayDensity);

    public native void NativeThreadPeekMessage();
    public native void NativeThreadEnter();
    public native void NativeThreadLeave();
    
    public native void NativeOnUpdate();
    public native void NativeOnTimewait();
    public native void NativeOnTimewake();

    public native void NativeOnStart();
    public native void NativeOnInterrupt();
    public native void NativeOnShutdown();
    public native void NativeOnJoin();

    public native void NativeOnSurfaceComplete();
    public native void NativeOnFinishLaunching();
    public native void NativeOnBeforeTerminate();

    public native void NativeOnEnterBackground();
    public native void NativeOnEnterForeground();
    
    // Keypad Event.
    public native void NativeOnKeypadPressed(mmEventKeypad hContent);
    public native void NativeOnKeypadRelease(mmEventKeypad hContent);
    public native void NativeOnKeypadStatus(mmEventKeypadStatus hContent);

    // Touchs Event.
    public native void NativeOnTouchsBegan(mmEventTouchs hContent);
    public native void NativeOnTouchsMoved(mmEventTouchs hContent);
    public native void NativeOnTouchsEnded(mmEventTouchs hContent);
    public native void NativeOnTouchsBreak(mmEventTouchs hContent);

    public native void NativeSetSurface(Surface pSurface);
    public native void NativeOnSurfaceCreated(int[] hSafeArea);
    public native void NativeOnSurfaceDestroyed();
    public native void NativeOnSurfaceChanged(int[] hSafeArea, int[] hViewSize);
    
    // EditBox
    public native void NativeOnGetTextEditRect(double[] hRect);
    public native void NativeOnInsertTextUtf16(char[] s, int o, int l);
    public native void NativeOnCommitText(String pText, int hNewCursorPosition);
    public native void NativeOnDeleteSurroundingText(int hBeforeLength, int hAfterLength);
    public native char NativeOnGetCursorCapsUtf16();
    public native String NativeOnGetSelectedText(int hFlags);
    public native String NativeOnGetTextAfterCursor(int n, int hFlags);
    public native String NativeOnGetTextBeforeCursor(int n, int hFlags);
    public native void NativeOnSetSelection(int hStart, int hEnd);
    public native void NativeOnGetSelection(int[] hRange);
    public native void NativeOnGetSize(int[] hSize);
    public native void NativeOnGetTextSelection(int[] hTextRange);
    public native void NativeOnGetTextSize(int[] hTextSize);
    public native void NativeOnSetComposingRegion(int hStart, int hEnd);
    public native void NativeOnSetComposingText(String pText, int hNewCursorPosition);
    public native void NativeOnFinishComposingText();
    public native void NativeOnDeleteBackwardText();
    public native void NativeOnPerformContextMenuAction(int hActionId);
    public native void NativeOnPerformEditorAction(int hEditorAction);
}
