/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import java.util.List;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.DisplayCutout;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;

import org.mm.core.mmLogger;

@SuppressWarnings("unused")
public class mmUISafeAreaLayout
{
    private static final String TAG = mmUISafeAreaLayout.class.getSimpleName();

    public final static float MM_GENERAL_ASPECT_RATIO = 16.0f / 9.0f;

    // Note: This API can only get the correct value after the window is created.
    // hArea (x, y, w, h)
    static public void GetSafeAreaLayoutGuide(Activity pActivity, View pView, int[] hArea)
    {
        int[] hLocation = new int[2];
        // Get the absolute coordinates in the entire screen.
        pView.getLocationOnScreen(hLocation);

        // View.getGlobalVisibleRect Unstable.
        //
        // When the screen is horizontal, some devices will rotate when entering the background,
        // and the parameters are sometimes square.
        int hViewW = pView.getWidth();
        int hViewH = pView.getHeight();

        // Screen Location and View (Width, Height) for GlobalVisibleRect. Do not check scaling and rotation.
        Rect v = new Rect();
        v.left   = hLocation[0];
        v.top    = hLocation[1];
        v.right  = hLocation[0] + hViewW;
        v.bottom = hLocation[1] + hViewH;
        
        // When DisplayCutout not enable, will Get the NotNotch Screen SafeArea.
        int[] hSafeArea = new int[4];
        GetViewSafeArea(pActivity, pView, hSafeArea);

        int[] hGVisible = new int[4];
        hGVisible[0] = v.left;
        hGVisible[1] = v.top;
        hGVisible[2] = v.right - v.left;
        hGVisible[3] = v.bottom - v.top;

        mmLogger.LogD(TAG + " hSafeArea Area(" + hSafeArea[0] + ", " + hSafeArea[1]+ ", " + hSafeArea[2]+ ", " + hSafeArea[3] + ")");
        mmLogger.LogD(TAG + " hGVisible Area(" + hGVisible[0] + ", " + hGVisible[1]+ ", " + hGVisible[2]+ ", " + hGVisible[3] + ")");
        
        int sl = hSafeArea[0];
        int st = hSafeArea[1];
        int sr = hSafeArea[2] + hSafeArea[0];
        int sb = hSafeArea[3] + hSafeArea[1];
        
        if(v.intersect(sl, st, sr, sb))
        {
            hArea[0] = (v.left - hLocation[0]);
            hArea[1] = (v.top  - hLocation[1]);
            hArea[2] = (v.right - v.left);
            hArea[3] = (v.bottom - v.top);
        }
        else
        {
            hArea[0] = hGVisible[0];
            hArea[1] = hGVisible[1];
            hArea[2] = hGVisible[2];
            hArea[3] = hGVisible[3];
        }
    }
    
    static public boolean GetIsNotch(Activity pActivity)
    {
        if(Build.VERSION_CODES.P <= Build.VERSION.SDK_INT)
        {
            // Google official API.
            return GetIsNotch_VersionP(pActivity);
        }
        else if(Build.VERSION_CODES.O == Build.VERSION.SDK_INT)
        {
            // Manufacturer API.
            return GetIsNotch_VersionO(pActivity);
        }
        else
        {
            // Not notch support.
            return GetIsNotch_VersionFallback(pActivity);
        }
    }

    // hArea (x, y, w, h)
    static public void GetViewSafeArea(Activity pActivity, View pView, int[] hArea)
    {
        if(Build.VERSION_CODES.P <= Build.VERSION.SDK_INT)
        {
            // Google official API.
            GetViewSafeArea_VersionP(pActivity, pView, hArea);
        }
        else if(Build.VERSION_CODES.O == Build.VERSION.SDK_INT)
        {
            // Manufacturer API.
            GetViewSafeArea_VersionO(pActivity, pView, hArea);
        }
        else
        {
            // Not notch support.
            GetViewSafeArea_VersionFallback(pActivity, pView, hArea);
        }
    }

    public static boolean GetIsNotch_VersionFallback(Activity pActivity)
    {
        return false;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static boolean GetIsNotch_VersionO(Activity pActivity)
    {
        String hBrand = Build.BRAND.toLowerCase(Locale.ENGLISH);
        mmUINotchScreen.NotchScreenApiBase pNotchScreenApiBase = mmUINotchScreen.GetManufacturerApiByName(hBrand);
        if(null != pNotchScreenApiBase)
        {
            return pNotchScreenApiBase.IsNotchSupport();
        }
        else
        {
            return false;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @TargetApi(Build.VERSION_CODES.P)
    public static boolean GetIsNotch_VersionP(Activity pActivity)
    {
        // android.view.WindowInsets getRootWindowInsets API level 23
        boolean hIsNotch = false;

        do
        {
            Window pWindow = pActivity.getWindow();
            View pDecorView = pWindow.getDecorView();
            WindowInsets pWindowInsets = pDecorView.getRootWindowInsets();
            if(null == pWindowInsets)
            {
                break;
            }
            DisplayCutout pDisplayCutout = pWindowInsets.getDisplayCutout();
            if(null == pDisplayCutout)
            {
                break;
            }
            List<Rect> hListRects =  pDisplayCutout.getBoundingRects();
            int hRectsSize = hListRects.size();
            if(0 == hRectsSize)
            {
                break;
            }
            hIsNotch = true;
        }while(false);

        return hIsNotch;
    }

    @SuppressWarnings("UnusedAssignment")
    public static void GetViewSafeArea_VersionFallback(Activity pActivity, View pView, int[] hArea)
    {
        // Virtual navigation bar at low version is not safe area.
        // Version >= (API 19) have View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY flag, can conditional hide it.
        // But most of time, the bar area is not unstable, Adjustment the safety area is unworthy.
        //
        // When the device use physical navigation bar, the virtual navigation bar height will be 0.

        boolean hHasSoftNavigationBar = mmUIDisplayMetrics.GetIsHasSoftNavigationBar(pActivity);
        int hStatusBarHeight = mmUIDisplayMetrics.GetStatusBarHeight(pActivity);
        int hNavigationBarHeight = mmUIDisplayMetrics.GetNavigationBarHeight(pActivity);
        int hSoftNavigationBarHeight = hHasSoftNavigationBar ? hNavigationBarHeight : 0;
        int hExpandHeight = hStatusBarHeight + hSoftNavigationBarHeight;
        float hAspectRatio = 0.0f;
        int hTopHeight = 0;

        mmUIDisplayMetrics.GetDisplayMetricsAreaSize(pActivity, hArea);

        WindowManager pWindowManager = pActivity.getWindowManager();
        Display pDisplay = pWindowManager.getDefaultDisplay();
        int hRotation = pDisplay.getRotation();

        // ROTATION_0   Constant Value: 0 (0x00000000)
        // ROTATION_90  Constant Value: 1 (0x00000001)
        // ROTATION_180 Constant Value: 2 (0x00000002)
        // ROTATION_270 Constant Value: 3 (0x00000003)

        switch(hRotation)
        {
            case Surface.ROTATION_0:
            case Surface.ROTATION_180:
            {
                hAspectRatio = (float)(hArea[3] - hExpandHeight) / (float)(hArea[2]);
                hTopHeight = (hAspectRatio >= MM_GENERAL_ASPECT_RATIO) ? hStatusBarHeight : 0;

                hArea[1] = hArea[1] + hTopHeight;
                hArea[3] = hArea[3] - hSoftNavigationBarHeight - hTopHeight;
            }
            break;
            case Surface.ROTATION_90:
            case Surface.ROTATION_270:
            {
                // Counterclockwise 90
                // Counterclockwise 270
                hAspectRatio = (float)(hArea[2] - hExpandHeight) / (float)(hArea[3]);
                hTopHeight = (hAspectRatio >= MM_GENERAL_ASPECT_RATIO) ? hStatusBarHeight : 0;

                hArea[0] = hArea[0] + hTopHeight;
                hArea[2] = hArea[2] - hSoftNavigationBarHeight - hTopHeight;
            }
            default:
            break;
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static void GetViewSafeArea_VersionO(Activity pActivity, View pView, int[] hArea)
    {
        String hBrand = Build.BRAND.toLowerCase(Locale.ENGLISH);
        mmUINotchScreen.NotchScreenApiBase pNotchScreenApiBase = mmUINotchScreen.GetManufacturerApiByName(hBrand);
        if(null != pNotchScreenApiBase)
        {
            int[] hViewArea = new int[4];
            mmUIDisplayMetrics.GetDisplayMetricsAreaSize(pActivity, hViewArea);

            int[] hSize = new int[2];
            int[] hRadius = new int[2];

            pNotchScreenApiBase.SetActivity(pActivity);
            pNotchScreenApiBase.GetNotchSize(hSize);
            pNotchScreenApiBase.GetCornerRadius(hRadius);

            WindowManager pWindowManager = pActivity.getWindowManager();
            Display pDisplay = pWindowManager.getDefaultDisplay();
            int hRotation = pDisplay.getRotation();

            // ROTATION_0   Constant Value: 0 (0x00000000)
            // ROTATION_90  Constant Value: 1 (0x00000001)
            // ROTATION_180 Constant Value: 2 (0x00000002)
            // ROTATION_270 Constant Value: 3 (0x00000003)

            switch(hRotation)
            {
                case Surface.ROTATION_0:
                {
                    hArea[0] = 0;
                    hArea[1] = hSize[1];
                    hArea[2] = hViewArea[2];
                    hArea[3] = hViewArea[3] - hSize[1] - hRadius[1];
                }
                break;
                case Surface.ROTATION_90:
                {
                    // Counterclockwise 90
                    hArea[0] = hSize[1];
                    hArea[1] = 0;
                    hArea[2] = hViewArea[2] - hSize[1] - hRadius[1];
                    hArea[3] = hViewArea[3];
                }
                break;
                case Surface.ROTATION_180:
                {
                    hArea[0] = 0;
                    hArea[1] = hRadius[1];
                    hArea[2] = hViewArea[2];
                    hArea[3] = hViewArea[3] - hSize[1] - hRadius[1];
                }
                break;
                case Surface.ROTATION_270:
                {
                    // Counterclockwise 270
                    hArea[0] = hRadius[1];
                    hArea[1] = 0;
                    hArea[2] = hViewArea[2] - hSize[1] - hRadius[1];
                    hArea[3] = hViewArea[3];
                }
                default:
                break;
            }
        }
        else
        {
            // fallback version.
            GetViewSafeArea_VersionFallback(pActivity, pView, hArea);
        }
    }

    @SuppressWarnings("ConstantConditions")
    @TargetApi(Build.VERSION_CODES.P)
    public static void GetViewSafeArea_VersionP(Activity pActivity, View pView, int[] hArea)
    {
        // android.view.WindowInsets getRootWindowInsets API level 23
        do
        {
            Window pWindow = pActivity.getWindow();
            View pDecorView = pWindow.getDecorView();
            WindowInsets pWindowInsets = pDecorView.getRootWindowInsets();
            if(null == pWindowInsets)
            {
                // fallback version.
                GetViewSafeArea_VersionFallback(pActivity, pView, hArea);
                break;
            }
            DisplayCutout pDisplayCutout = pWindowInsets.getDisplayCutout();
            if(null == pDisplayCutout)
            {
                // fallback version.
                GetViewSafeArea_VersionFallback(pActivity, pView, hArea);
                break;
            }

            int[] hViewArea = new int[4];
            mmUIDisplayMetrics.GetDisplayMetricsAreaSize(pActivity, hViewArea);

            int hInsetL = pDisplayCutout.getSafeInsetLeft();
            int hInsetT = pDisplayCutout.getSafeInsetTop();
            int hInsetR = pDisplayCutout.getSafeInsetRight();
            int hInsetB = pDisplayCutout.getSafeInsetBottom();
            
            // Some manufacturers ignore the rounded corners on the other side.
            // If the value on the other side is 0, it is considered symmetric.
            
            // System bar height.
            int hStatusBarHeight = mmUIDisplayMetrics.GetStatusBarHeight(pActivity);
            int hNavigationBarHeight = mmUIDisplayMetrics.GetNavigationBarHeight(pActivity);
            
            // radius[2] {top, bottom}
            int[] hRadius = new int[2];
            
            WindowManager pWindowManager = pActivity.getWindowManager();
            Display pDisplay = pWindowManager.getDefaultDisplay();
            int hRotation = pDisplay.getRotation();

            // ROTATION_0   Constant Value: 0 (0x00000000)
            // ROTATION_90  Constant Value: 1 (0x00000001)
            // ROTATION_180 Constant Value: 2 (0x00000002)
            // ROTATION_270 Constant Value: 3 (0x00000003)

            switch(hRotation)
            {
                case Surface.ROTATION_0:
                {
                    hRadius[0] = Math.min(hInsetT, hStatusBarHeight);
                    hRadius[1] = Math.max(hInsetT, hNavigationBarHeight);
                    
                    hInsetT = hRadius[0];
                    hInsetB = (0 == hInsetB) ? hRadius[1] : hInsetB;
                }
                break;
                case Surface.ROTATION_90:
                {
                    // Counterclockwise 90
                    hRadius[0] = Math.min(hInsetL, hStatusBarHeight);
                    hRadius[1] = Math.max(hInsetL, hNavigationBarHeight);
                    
                    hInsetL = hRadius[0];
                    hInsetR = (0 == hInsetR) ? hRadius[1] : hInsetR;
                }
                break;
                case Surface.ROTATION_180:
                {
                    hRadius[0] = Math.min(hInsetB, hStatusBarHeight);
                    hRadius[1] = Math.max(hInsetB, hNavigationBarHeight);
                    
                    hInsetB = hRadius[0];
                    hInsetT = (0 == hInsetT) ? hRadius[1] : hInsetT;
                }
                break;
                case Surface.ROTATION_270:
                {
                    // Counterclockwise 270
                    hRadius[0] = Math.min(hInsetR, hStatusBarHeight);
                    hRadius[1] = Math.max(hInsetR, hNavigationBarHeight);
                    
                    hInsetR = hRadius[0];
                    hInsetL = (0 == hInsetL) ? hRadius[1] : hInsetL;
                }
                default:
                break;
            }
            
            hArea[0] = hInsetL;
            hArea[1] = hInsetT;
            hArea[2] = hViewArea[2] - hInsetR - hInsetL;
            hArea[3] = hViewArea[3] - hInsetB - hInsetT;
        }while(false);
    }
}