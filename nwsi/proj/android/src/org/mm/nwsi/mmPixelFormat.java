/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import java.util.TreeMap;

import android.graphics.PixelFormat;

public class mmPixelFormat
{
    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    public static final TreeMap<Integer, String> const_PixelFormat = new TreeMap<Integer, String>()
    {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        {
            this.put(PixelFormat.UNKNOWN  , "UNKNOWN");
            this.put(PixelFormat.A_8      , "A_8");
            this.put(PixelFormat.LA_88    , "LA_88");
            this.put(PixelFormat.L_8      , "L_8");
            this.put(PixelFormat.RGBA_4444, "RGBA_4444");
            this.put(PixelFormat.RGBA_5551, "RGBA_5551");
            this.put(PixelFormat.RGBA_8888, "RGBA_8888");
            this.put(PixelFormat.RGBX_8888, "RGBX_8888");
            this.put(PixelFormat.RGB_332  , "RGB_332");
            this.put(PixelFormat.RGB_565  , "RGB_565");
            this.put(PixelFormat.RGB_888  , "RGB_888");
        }
    };

    static public String PixelFormatToString(int format)
    {
        String s = const_PixelFormat.get(format);
        if (null == s || s.equals(""))
        {
            s = const_PixelFormat.get(PixelFormat.UNKNOWN);
        }
        return s;
    }
}