package org.mm.nwsi;

import android.graphics.Canvas;

import java.util.LinkedList;

@SuppressWarnings({"unused", "Convert2Diamond"})
public class mmGraphicsContext
{
    public static ThreadLocal<LinkedList<Canvas>> gThreadContext = new ThreadLocal<LinkedList<Canvas>>();

    public static LinkedList<Canvas> ThreadCanvasInstance()
    {
        LinkedList<Canvas> gThreadCanvas = gThreadContext.get();
        if (null == gThreadCanvas)
        {
            gThreadCanvas = new LinkedList<Canvas>();
            gThreadContext.set(gThreadCanvas);
        }
        return gThreadCanvas;
    }

    public static void PushContext(Canvas pCanvas)
    {
        LinkedList<Canvas> gThreadCanvas = ThreadCanvasInstance();
        gThreadCanvas.addLast(pCanvas);
    }

    public static void PopContext()
    {
        LinkedList<Canvas> gThreadCanvas = ThreadCanvasInstance();
        gThreadCanvas.removeLast();
    }

    public static Canvas GetCurrentContext()
    {
        LinkedList<Canvas> gThreadCanvas = ThreadCanvasInstance();
        return gThreadCanvas.getLast();
    }
}
