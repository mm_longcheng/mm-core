package org.mm.nwsi;

@SuppressWarnings({"unused"})
public class mmTextDrawingContext
{
    public mmGraphicsFactory pGraphicsFactory = null;
    public mmTextParagraphFormat pParagraphFormat = null;
    // object for platform implement.
    Object pRenderTarget = null;
    Object pBitmap = null;

    // Background 0
    // Underline  1
    // Foreground 2
    // Stroke     3
    // StrikeThru 4
    public int hPass = 0;

    public void Init()
    {
        this.pParagraphFormat = null;
        this.hPass = 0;
        this.pRenderTarget = null;
        this.pBitmap = null;
    }
    public void Destroy()
    {
        this.pParagraphFormat = null;
        this.hPass = 0;
        this.pRenderTarget = null;
        this.pBitmap = null;
    }

    public void SetGraphicsFactory(mmGraphicsFactory pGraphicsFactory)
    {
        this.pGraphicsFactory = pGraphicsFactory;
    }
    
    public void SetTextParagraphFormat(mmTextParagraphFormat pParagraphFormat)
    {
        this.pParagraphFormat = pParagraphFormat;
    }
    public void SetRenderTarget(Object pRenderTarget)
    {
        this.pRenderTarget = pRenderTarget;
    }
    public void SetBitmap(Object pBitmap)
    {
        this.pBitmap = pBitmap;
    }

    public void SetPass(int hPass)
    {
        this.hPass = hPass;
    }
    
    public void OnFinishLaunching()
    {

    }
    public void OnBeforeTerminate()
    {

    }
}
