package org.mm.nwsi;

@SuppressWarnings({"unused", "JavaDoc", "SpellCheckingInspection"})
public class mmUBidi
{
    /** Left-to-right text. This is a 0 value.
     * <ul>
     * <li>As return value for <code>ubidi_getDirection()</code>, it means
     *     that the source string contains no right-to-left characters, or
     *     that the source string is empty and the paragraph level is even.
     * <li> As return value for <code>ubidi_getBaseDirection()</code>, it
     *      means that the first strong character of the source string has
     *      a left-to-right direction.
     * </ul>
     * @stable ICU 2.0
     */
    public static final int UBIDI_LTR = 0;

    /** Right-to-left text. This is a 1 value.
     * <ul>
     * <li>As return value for <code>ubidi_getDirection()</code>, it means
     *     that the source string contains no left-to-right characters, or
     *     that the source string is empty and the paragraph level is odd.
     * <li> As return value for <code>ubidi_getBaseDirection()</code>, it
     *      means that the first strong character of the source string has
     *      a right-to-left direction.
     * </ul>
     * @stable ICU 2.0
     */
    public static final int UBIDI_RTL = 1;

    /** Mixed-directional text.
     * <p>As return value for <code>ubidi_getDirection()</code>, it means
     *    that the source string contains both left-to-right and
     *    right-to-left characters.
     * @stable ICU 2.0
     */
    public static final int UBIDI_MIXED = 2;

    /** No strongly directional text.
     * <p>As return value for <code>ubidi_getBaseDirection()</code>, it means
     *    that the source string is missing or empty, or contains neither left-to-right
     *    nor right-to-left characters.
     * @stable ICU 4.6
     */
    public static final int UBIDI_NEUTRAL = 3;

    public static int GetBaseDirection(char[] text, int length)
    {
        return NativeGetBaseDirection(text, length);
    }

    @SuppressWarnings("JavaJniMissingFunction")
    public static native int NativeGetBaseDirection(char[] text, int length);
}
