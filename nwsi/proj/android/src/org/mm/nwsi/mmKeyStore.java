/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

@SuppressWarnings({"unused"})
public class mmKeyStore
{
    private static final String TAG = mmKeyStore.class.getSimpleName();

    // Unit year.
    public static final int MM_MAX_EXPIRED_TIME = 1000;
    // x500Principal
    public static final String MM_X500_PRINCIPAL_NAME = "CN=MyKey, O=Android Authority";
    // KeyStore provider
    public static final String MM_KEYSTORE_PROVIDER = "AndroidKeyStore";

    public static final String MM_CIPHER_TRANSFORMATION = "RSA/ECB/PKCS1Padding";

    /* RSA */
    public static final String MM_ALGORITHM_RSA = "RSA";
    public static final int MM_RSA_KEY_SIZE = 2048;

    // RSA has a limit on the length of encrypted characters, so segmented encryption is required.
    public static final int MM_RSA_ENCRYPT_BLOCK = 244;
    public static final int MM_RSA_DECRYPT_BLOCK = 256;

    private KeyStore pKeyStore = null;
    private Context pContext = null;

    public mmKeyStore()
    {

    }

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }

    public void SetContext(Context pContext)
    {
        this.pContext = pContext;
    }

    public void OnFinishLaunching()
    {
        this.InstanceAndroidKeyStore();
    }

    public void OnBeforeTerminate()
    {
        this.pKeyStore = null;
    }

    public void InstanceAndroidKeyStore()
    {
        if(null == this.pKeyStore)
        {
            try
            {
                this.pKeyStore = KeyStore.getInstance("AndroidKeyStore");
                this.pKeyStore.load(null);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void CreateKeys_VersionFallback(String hAlias)
    {
        // not implement.
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void CreateKeys_Version18(String hAlias)
    {
        try
        {
            // Create new key
            Calendar start = Calendar.getInstance();
            Calendar end = Calendar.getInstance();
            end.add(Calendar.YEAR, mmKeyStore.MM_MAX_EXPIRED_TIME);

            X500Principal pX500Principal = new X500Principal(mmKeyStore.MM_X500_PRINCIPAL_NAME);

            KeyPairGeneratorSpec.Builder pSpecBuilder = new KeyPairGeneratorSpec.Builder(this.pContext);
            pSpecBuilder.setAlias(hAlias);
            pSpecBuilder.setSubject(pX500Principal);
            pSpecBuilder.setSerialNumber(BigInteger.ONE);
            pSpecBuilder.setStartDate(start.getTime());
            pSpecBuilder.setEndDate(end.getTime());

            KeyPairGeneratorSpec pSpec = pSpecBuilder.build();

            KeyPairGenerator pGenerator = KeyPairGenerator.getInstance(mmKeyStore.MM_ALGORITHM_RSA, mmKeyStore.MM_KEYSTORE_PROVIDER);
            pGenerator.initialize(pSpec);
            pGenerator.generateKeyPair();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void CreateKeys_Version23(String hAlias)
    {
        try
        {
            KeyPairGenerator pGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, mmKeyStore.MM_KEYSTORE_PROVIDER);

            KeyGenParameterSpec.Builder pBuilder = new KeyGenParameterSpec.Builder(hAlias, KeyProperties.PURPOSE_DECRYPT);
            pBuilder.setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512);
            pBuilder.setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1);
            pBuilder.setUserAuthenticationRequired(false);

            KeyGenParameterSpec pSpec = pBuilder.build();
            pGenerator.initialize(pSpec);
            pGenerator.generateKeyPair();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    public void CreateKeys(String hAlias)
    {
        try
        {
            if (TextUtils.isEmpty(hAlias))
            {
                return;
            }

            if (null == this.pKeyStore)
            {
                return;
            }

            if (this.pKeyStore.containsAlias(hAlias))
            {
                return;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                this.CreateKeys_Version23(hAlias);
            }
            else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            {
                this.CreateKeys_Version18(hAlias);
            }
            else
            {
                this.CreateKeys_VersionFallback(hAlias);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void DeleteKeys(String hAlias)
    {
        try
        {
            if (TextUtils.isEmpty(hAlias))
            {
                return;
            }

            if (null == this.pKeyStore)
            {
                return;
            }

            this.pKeyStore.deleteEntry(hAlias);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @SuppressWarnings({"ConstantConditions"})
    public String EncryptString(String hAlias, String hNeedEncryptWord)
    {
        String hEncryptWord = "";

        do
        {
            if (TextUtils.isEmpty(hNeedEncryptWord))
            {
                hEncryptWord = "";
                break;
            }

            if (TextUtils.isEmpty(hAlias))
            {
                hEncryptWord = "";
                break;
            }

            if (null == this.pKeyStore)
            {
                hEncryptWord = "";
                break;
            }

            try
            {
                Certificate pCertificate = this.pKeyStore.getCertificate(hAlias);
                if (null == pCertificate)
                {
                    hEncryptWord = hNeedEncryptWord;
                    break;
                }
                PublicKey pPublicKey = pCertificate.getPublicKey();
                Cipher pCipher = Cipher.getInstance(mmKeyStore.MM_CIPHER_TRANSFORMATION);
                pCipher.init(Cipher.ENCRYPT_MODE, pPublicKey);

                ByteArrayOutputStream pOut = new ByteArrayOutputStream();
                int hOffSet = 0;
                int hLength = hNeedEncryptWord.length();
                byte[] pBytes = hNeedEncryptWord.getBytes();
                for (int i = 0; hLength - hOffSet > 0; hOffSet = i * mmKeyStore.MM_RSA_ENCRYPT_BLOCK)
                {
                    byte[] pCache;
                    if (hLength - hOffSet > mmKeyStore.MM_RSA_ENCRYPT_BLOCK)
                    {
                        pCache = pCipher.doFinal(pBytes, hOffSet, mmKeyStore.MM_RSA_ENCRYPT_BLOCK);
                    }
                    else
                    {
                        pCache = pCipher.doFinal(pBytes, hOffSet, hLength - hOffSet);
                    }
                    pOut.write(pCache, 0, pCache.length);
                    ++i;
                }
                byte[] hEncryptWordBytes = pOut.toByteArray();
                pOut.close();

                hEncryptWord = Base64.encodeToString(hEncryptWordBytes, Base64.URL_SAFE);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Log.e(TAG, "EncryptWord error:" + e.getMessage());
            }
        } while(false);

        return hEncryptWord;
    }

    @SuppressWarnings({"CharsetObjectCanBeUsed", "ConstantConditions"})
    public String DecryptString(String hAlias, String hNeedDecryptWord)
    {
        String hDecryptWord = "";

        do
        {
            if (TextUtils.isEmpty(hNeedDecryptWord))
            {
                hDecryptWord = "";
                break;
            }

            if (TextUtils.isEmpty(hAlias))
            {
                hDecryptWord = "";
                break;
            }

            if (null == this.pKeyStore)
            {
                hDecryptWord = "";
                break;
            }

            try
            {
                PrivateKey pPrivateKey = (PrivateKey) this.pKeyStore.getKey(hAlias, null);
                if (null == pPrivateKey)
                {
                    hDecryptWord = hNeedDecryptWord;
                    break;
                }
                Cipher pCipher = Cipher.getInstance(mmKeyStore.MM_CIPHER_TRANSFORMATION);
                pCipher.init(Cipher.DECRYPT_MODE, pPrivateKey);

                ByteArrayOutputStream pOut = new ByteArrayOutputStream();
                int hOffSet = 0;
                byte[] encryptedData = Base64.decode(hNeedDecryptWord, Base64.URL_SAFE);
                int hLength = encryptedData.length;
                for (int i = 0; hLength - hOffSet > 0; hOffSet = i * mmKeyStore.MM_RSA_DECRYPT_BLOCK)
                {
                    byte[] pCache;
                    if (hLength - hOffSet > mmKeyStore.MM_RSA_DECRYPT_BLOCK)
                    {
                        pCache = pCipher.doFinal(encryptedData, hOffSet, mmKeyStore.MM_RSA_DECRYPT_BLOCK);
                    }
                    else
                    {
                        pCache = pCipher.doFinal(encryptedData, hOffSet, hLength - hOffSet);
                    }
                    pOut.write(pCache, 0, pCache.length);
                    ++i;
                }
                byte[] hDecryptWordBytes = pOut.toByteArray();
                pOut.close();

                hDecryptWord = new String(hDecryptWordBytes, 0, hDecryptWordBytes.length, "UTF-8");
            }
            catch (Exception e)
            {
                e.printStackTrace();
                Log.e(TAG, "DecryptWord error:" + e.getMessage());
            }
        } while(false);

        return hDecryptWord;
    }
}