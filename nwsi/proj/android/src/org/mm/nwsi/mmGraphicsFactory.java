package org.mm.nwsi;

import android.content.res.AssetManager;
import android.graphics.Typeface;

import org.mm.core.mmLogger;

@SuppressWarnings({"unused"})
public class mmGraphicsFactory
{
    mmPackageAssets pPackageAssets = null;
    // Reserved compatible. Empty now.
    // object for platform implement.
    public long hNativePtr = 0;

    public void Init()
    {

    }
    public void Destroy()
    {

    }

    public void SetPackageAssets(mmPackageAssets pPackageAssets)
    {
        this.pPackageAssets = pPackageAssets;
    }

    public void OnFinishLaunching()
    {

    }
    public void OnBeforeTerminate()
    {

    }

    public Object CreateTypefaceFromFolder(String pFontPath)
    {
        Object pTypeface = null;

        try
        {
            pTypeface = Typeface.createFromFile(pFontPath);
        }
        catch (Exception e)
        {
            mmLogger.LogE("exception: " + e);
        }

        return pTypeface;
    }

    public Object CreateTypefaceFromSource(String pFontPath)
    {
        Object pTypeface = null;

        try
        {
            mmPackageAssets pPackageAssets = this.pPackageAssets;
            AssetManager pAssetManager = pPackageAssets.pAssetManager;
            pTypeface = Typeface.createFromAsset(pAssetManager, pFontPath);
        }
        catch (Exception e)
        {
            mmLogger.LogE("exception: " + e);
        }

        return pTypeface;
    }

    public void DeleteTypeface(Object pTypeface)
    {
        // android Typeface not delete api for unload.
    }

	@SuppressWarnings("JavaJniMissingFunction")
    public native Typeface NativeGetFontHandleByFamily(String pFamily);
}
