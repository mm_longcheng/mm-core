/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.view.KeyEvent;

@SuppressWarnings("unused")
public class mmKeyCode
{
    // ===========================================================
    // Constants
    // ===========================================================
    public final static int MM_KC_UNKNOWN         = 0x00   ;
    public final static int MM_KC_ESCAPE          = 0x01   ;
    public final static int MM_KC_1               = 0x02   ;
    public final static int MM_KC_2               = 0x03   ;
    public final static int MM_KC_3               = 0x04   ;
    public final static int MM_KC_4               = 0x05   ;
    public final static int MM_KC_5               = 0x06   ;
    public final static int MM_KC_6               = 0x07   ;
    public final static int MM_KC_7               = 0x08   ;
    public final static int MM_KC_8               = 0x09   ;
    public final static int MM_KC_9               = 0x0A   ;
    public final static int MM_KC_0               = 0x0B   ;
    public final static int MM_KC_MINUS           = 0x0C   ; /* - on main keyboard */
    public final static int MM_KC_EQUALS          = 0x0D   ;
    public final static int MM_KC_BACK            = 0x0E   ; /* backspace */
    public final static int MM_KC_TAB             = 0x0F   ;
    public final static int MM_KC_Q               = 0x10   ;
    public final static int MM_KC_W               = 0x11   ;
    public final static int MM_KC_E               = 0x12   ;
    public final static int MM_KC_R               = 0x13   ;
    public final static int MM_KC_T               = 0x14   ;
    public final static int MM_KC_Y               = 0x15   ;
    public final static int MM_KC_U               = 0x16   ;
    public final static int MM_KC_I               = 0x17   ;
    public final static int MM_KC_O               = 0x18   ;
    public final static int MM_KC_P               = 0x19   ;
    public final static int MM_KC_LBRACKET        = 0x1A   ;
    public final static int MM_KC_RBRACKET        = 0x1B   ;
    public final static int MM_KC_RETURN          = 0x1C   ; /* Enter on main keyboard */
    public final static int MM_KC_LCONTROL        = 0x1D   ;
    public final static int MM_KC_A               = 0x1E   ;
    public final static int MM_KC_S               = 0x1F   ;
    public final static int MM_KC_D               = 0x20   ;
    public final static int MM_KC_F               = 0x21   ;
    public final static int MM_KC_G               = 0x22   ;
    public final static int MM_KC_H               = 0x23   ;
    public final static int MM_KC_J               = 0x24   ;
    public final static int MM_KC_K               = 0x25   ;
    public final static int MM_KC_L               = 0x26   ;
    public final static int MM_KC_SEMICOLON       = 0x27   ;
    public final static int MM_KC_APOSTROPHE      = 0x28   ;
    public final static int MM_KC_GRAVE           = 0x29   ; /* accent grave */
    public final static int MM_KC_LSHIFT          = 0x2A   ;
    public final static int MM_KC_BACKSLASH       = 0x2B   ;
    public final static int MM_KC_Z               = 0x2C   ;
    public final static int MM_KC_X               = 0x2D   ;
    public final static int MM_KC_C               = 0x2E   ;
    public final static int MM_KC_V               = 0x2F   ;
    public final static int MM_KC_B               = 0x30   ;
    public final static int MM_KC_N               = 0x31   ;
    public final static int MM_KC_M               = 0x32   ;
    public final static int MM_KC_COMMA           = 0x33   ;
    public final static int MM_KC_PERIOD          = 0x34   ; /* . on main keyboard */
    public final static int MM_KC_SLASH           = 0x35   ; /* / on main keyboard */
    public final static int MM_KC_RSHIFT          = 0x36   ;
    public final static int MM_KC_MULTIPLY        = 0x37   ; /* * on numeric keypad */
    public final static int MM_KC_LMENU           = 0x38   ; /* left Alt */
    public final static int MM_KC_SPACE           = 0x39   ;
    public final static int MM_KC_CAPITAL         = 0x3A   ;
    public final static int MM_KC_F1              = 0x3B   ;
    public final static int MM_KC_F2              = 0x3C   ;
    public final static int MM_KC_F3              = 0x3D   ;
    public final static int MM_KC_F4              = 0x3E   ;
    public final static int MM_KC_F5              = 0x3F   ;
    public final static int MM_KC_F6              = 0x40   ;
    public final static int MM_KC_F7              = 0x41   ;
    public final static int MM_KC_F8              = 0x42   ;
    public final static int MM_KC_F9              = 0x43   ;
    public final static int MM_KC_F10             = 0x44   ;
    public final static int MM_KC_NUMLOCK         = 0x45   ;
    public final static int MM_KC_SCROLL          = 0x46   ; /* Scroll Lock */
    public final static int MM_KC_NUMPAD7         = 0x47   ;
    public final static int MM_KC_NUMPAD8         = 0x48   ;
    public final static int MM_KC_NUMPAD9         = 0x49   ;
    public final static int MM_KC_SUBTRACT        = 0x4A   ; /* - on numeric keypad */
    public final static int MM_KC_NUMPAD4         = 0x4B   ;
    public final static int MM_KC_NUMPAD5         = 0x4C   ;
    public final static int MM_KC_NUMPAD6         = 0x4D   ;
    public final static int MM_KC_ADD             = 0x4E   ; /* + on numeric keypad */
    public final static int MM_KC_NUMPAD1         = 0x4F   ;
    public final static int MM_KC_NUMPAD2         = 0x50   ;
    public final static int MM_KC_NUMPAD3         = 0x51   ;
    public final static int MM_KC_NUMPAD0         = 0x52   ;
    public final static int MM_KC_DECIMAL         = 0x53   ; /* . on numeric keypad */
    public final static int MM_KC_OEM_102         = 0x56   ; /* <> or \| on RT 102-key keyboard (Non-U.S.) */
    public final static int MM_KC_F11             = 0x57   ;
    public final static int MM_KC_F12             = 0x58   ;
    public final static int MM_KC_F13             = 0x64   ; /*                     (NEC PC98) */
    public final static int MM_KC_F14             = 0x65   ; /*                     (NEC PC98) */
    public final static int MM_KC_F15             = 0x66   ; /*                     (NEC PC98) */
    public final static int MM_KC_KANA            = 0x70   ; /* (Japanese keyboard)            */
    public final static int MM_KC_ABNT_C1         = 0x73   ; /* /? on Brazilian keyboard */
    public final static int MM_KC_CONVERT         = 0x79   ; /* (Japanese keyboard)            */
    public final static int MM_KC_NOCONVERT       = 0x7B   ; /* (Japanese keyboard)            */
    public final static int MM_KC_YEN             = 0x7D   ; /* (Japanese keyboard)            */
    public final static int MM_KC_ABNT_C2         = 0x7E   ; /* Numpad . on Brazilian keyboard */
    public final static int MM_KC_NUMPADEQUALS    = 0x8D   ; /* = on numeric keypad (NEC PC98) */
    public final static int MM_KC_PREVTRACK       = 0x90   ; /* Previous Track (MM_KC_CIRCUMFLEX on Japanese keyboard) */
    public final static int MM_KC_AT              = 0x91   ; /*                     (NEC PC98) */
    public final static int MM_KC_COLON           = 0x92   ; /*                     (NEC PC98) */
    public final static int MM_KC_UNDERLINE       = 0x93   ; /*                     (NEC PC98) */
    public final static int MM_KC_KANJI           = 0x94   ; /* (Japanese keyboard)            */
    public final static int MM_KC_STOP            = 0x95   ; /*                     (NEC PC98) */
    public final static int MM_KC_AX              = 0x96   ; /*                     (Japan AX) */
    public final static int MM_KC_UNLABELED       = 0x97   ; /*                        (J3100) */
    public final static int MM_KC_NEXTTRACK       = 0x99   ; /* Next Track */
    public final static int MM_KC_NUMPADENTER     = 0x9C   ; /* Enter on numeric keypad */
    public final static int MM_KC_RCONTROL        = 0x9D   ;
    public final static int MM_KC_MUTE            = 0xA0   ; /* Mute */
    public final static int MM_KC_CALCULATOR      = 0xA1   ; /* Calculator */
    public final static int MM_KC_PLAYPAUSE       = 0xA2   ; /* Play / Pause */
    public final static int MM_KC_MEDIASTOP       = 0xA4   ; /* Media Stop */
    public final static int MM_KC_VOLUMEDOWN      = 0xAE   ; /* Volume - */
    public final static int MM_KC_VOLUMEUP        = 0xB0   ; /* Volume + */
    public final static int MM_KC_WEBHOME         = 0xB2   ; /* Web home */
    public final static int MM_KC_NUMPADCOMMA     = 0xB3   ; /* , on numeric keypad (NEC PC98) */
    public final static int MM_KC_DIVIDE          = 0xB5   ; /* / on numeric keypad */
    public final static int MM_KC_SYSRQ           = 0xB7   ;
    public final static int MM_KC_RMENU           = 0xB8   ; /* right Alt */
    public final static int MM_KC_PAUSE           = 0xC5   ; /* Pause */
    public final static int MM_KC_HOME            = 0xC7   ; /* Home on arrow keypad */
    public final static int MM_KC_UP              = 0xC8   ; /* UpArrow on arrow keypad */
    public final static int MM_KC_PRIOR           = 0xC9   ; /* PgUp on arrow keypad */
    public final static int MM_KC_LEFT            = 0xCB   ; /* LeftArrow on arrow keypad */
    public final static int MM_KC_RIGHT           = 0xCD   ; /* RightArrow on arrow keypad */
    public final static int MM_KC_END             = 0xCF   ; /* End on arrow keypad */
    public final static int MM_KC_DOWN            = 0xD0   ; /* DownArrow on arrow keypad */
    public final static int MM_KC_NEXT            = 0xD1   ; /* PgDn on arrow keypad */
    public final static int MM_KC_INSERT          = 0xD2   ; /* Insert on arrow keypad */
    public final static int MM_KC_DELETE          = 0xD3   ; /* Delete on arrow keypad */
    public final static int MM_KC_LWIN            = 0xDB   ; /* Left Windows key */
    public final static int MM_KC_RWIN            = 0xDC   ; /* Right Windows key */
    public final static int MM_KC_APPS            = 0xDD   ; /* AppMenu key */
    public final static int MM_KC_POWER           = 0xDE   ; /* System Power */
    public final static int MM_KC_SLEEP           = 0xDF   ; /* System Sleep */
    public final static int MM_KC_WAKE            = 0xE3   ; /* System Wake */
    public final static int MM_KC_WEBSEARCH       = 0xE5   ; /* Web Search */
    public final static int MM_KC_WEBFAVORITES    = 0xE6   ; /* Web Favorites */
    public final static int MM_KC_WEBREFRESH      = 0xE7   ; /* Web Refresh */
    public final static int MM_KC_WEBSTOP         = 0xE8   ; /* Web Stop */
    public final static int MM_KC_WEBFORWARD      = 0xE9   ; /* Web Forward */
    public final static int MM_KC_WEBBACK         = 0xEA   ; /* Web Back */
    public final static int MM_KC_MYCOMPUTER      = 0xEB   ; /* My Computer */
    public final static int MM_KC_MAIL            = 0xEC   ; /* Mail */
    public final static int MM_KC_MEDIASELECT     = 0xED   ; /* Media Select */

    /*
    *  Alternate names for keys, to facilitate transition from DOS.
    */
    public final static int MM_KC_BACKSPACE       = MM_KC_BACK        ;    /* backspace */
    public final static int MM_KC_NUMPADSTAR      = MM_KC_MULTIPLY    ;    /* * on numeric keypad */
    public final static int MM_KC_LALT            = MM_KC_LMENU       ;    /* left Alt */
    public final static int MM_KC_CAPSLOCK        = MM_KC_CAPITAL     ;    /* CapsLock */
    public final static int MM_KC_NUMPADMINUS     = MM_KC_SUBTRACT    ;    /* - on numeric keypad */
    public final static int MM_KC_NUMPADPLUS      = MM_KC_ADD         ;    /* + on numeric keypad */
    public final static int MM_KC_NUMPADPERIOD    = MM_KC_DECIMAL     ;    /* . on numeric keypad */
    public final static int MM_KC_NUMPADSLASH     = MM_KC_DIVIDE      ;    /* / on numeric keypad */
    public final static int MM_KC_RALT            = MM_KC_RMENU       ;    /* right Alt */
    public final static int MM_KC_UPARROW         = MM_KC_UP          ;    /* UpArrow on arrow keypad */
    public final static int MM_KC_PGUP            = MM_KC_PRIOR       ;    /* PgUp on arrow keypad */
    public final static int MM_KC_LEFTARROW       = MM_KC_LEFT        ;    /* LeftArrow on arrow keypad */
    public final static int MM_KC_RIGHTARROW      = MM_KC_RIGHT       ;    /* RightArrow on arrow keypad */
    public final static int MM_KC_DOWNARROW       = MM_KC_DOWN        ;    /* DownArrow on arrow keypad */
    public final static int MM_KC_PGDN            = MM_KC_NEXT        ;    /* PgDn on arrow keypad */

    /*
    *  Alternate names for keys originally not used on US keyboards.
    */
    public final static int MM_KC_CIRCUMFLEX      = MM_KC_PREVTRACK   ;    /* Japanese keyboard */

    /* Modifier key flags. */
    public final static int MM_MODIFIER_SHIFT    = 0x0001; /* bit Shift.                   */
    public final static int MM_MODIFIER_CONTROL  = 0x0002; /* bit Control.                 */
    public final static int MM_MODIFIER_OPTION   = 0x0003; /* bit Option or Alternate. Alt */
    public final static int MM_MODIFIER_COMMAND  = 0x0004; /* bit Command or Super. META.  */
    public final static int MM_MODIFIER_CAPITAL  = 0x0010; /* bit Caps Lock.               */
    public final static int MM_MODIFIER_NUMLOCK  = 0x0020; /* bit Number Lock.             */
    public final static int MM_MODIFIER_HELP     = 0x0040; /* bit Help.                    */
    public final static int MM_MODIFIER_FUNCTION = 0x0080; /* bit Function Fn.             */
    public final static int MM_MODIFIER_SYM      = 0x0100; /* bit SYM.                     */

    /* Mouse button. */
    public final static int MM_MB_UNKNOWN       =     -1; /* The unknown mouse button.     */
    public final static int MM_MB_LBUTTON       =      0; /* The left mouse button.        */
    public final static int MM_MB_RBUTTON       =      1; /* The right mouse button.       */
    public final static int MM_MB_MBUTTON       =      2; /* The middle mouse button.      */

    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    static public int GetVirtualKeycode(int hKeycode)
    {
        return NativeOSVirtualKeycodeTranslate(hKeycode);
    }

    @SuppressWarnings("UnusedAssignment")
    static public boolean HandleKeyCode(int pKeyCode, int hVirtualKeycode)
    {
        boolean hHandle = false;

        switch (pKeyCode) 
        {
        case KeyEvent.KEYCODE_HOME:
        case KeyEvent.KEYCODE_BACK:
        case KeyEvent.KEYCODE_MENU:
            hHandle = true;
            break;
        default:
            hHandle = (MM_KC_UNKNOWN != hVirtualKeycode);
            break;
        }
        
        return hHandle;
    }

    @SuppressWarnings("JavaJniMissingFunction")
    public static native int NativeOSVirtualKeycodeTranslate(int keycode);
}