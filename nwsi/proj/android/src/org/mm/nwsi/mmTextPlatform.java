package org.mm.nwsi;

import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.TextUtils;

import org.mm.core.mmLogger;

@SuppressWarnings({"unused"})
public class mmTextPlatform
{
    static public int GetFontWeight(int hWeight)
    {
        return hWeight;
    }

    static public int GetFontStyle(int hStyle)
    {
        final int[] gStyleTable =
        {
            Typeface.NORMAL     , // 0 mmFontStyleNormal
            Typeface.ITALIC     , // 1 mmFontStyleItalic
            Typeface.NORMAL     , // 2 mmFontStyleOblique
        };
        if (0 <= hStyle && hStyle < gStyleTable.length)
        {
            return gStyleTable[hStyle];
        }
        else
        {
            return Typeface.NORMAL;
        }
    }

    static public float GetFontSize(float hSize)
    {
        return hSize;
    }

    static public int GetTextBaseDirection(char[] pWText, int hLength)
    {
        return mmUBidi.GetBaseDirection(pWText, hLength);
    }

    @SuppressWarnings("ConstantConditions")
    static public Layout.Alignment GetTextAlignment(int hTextAlignment, int hBaseDirection)
    {
        Layout.Alignment hAlignment = Layout.Alignment.ALIGN_NORMAL;

        switch (hTextAlignment)
        {
        case mmTextFormat.mmTextAlignmentCenter:
        {
            hAlignment = Layout.Alignment.ALIGN_CENTER;
        }
        break;

        case mmTextFormat.mmTextAlignmentNatural:
        {
            hAlignment = Layout.Alignment.ALIGN_NORMAL;
        }
        break;

        case mmTextFormat.mmTextAlignmentLeft:
        case mmTextFormat.mmTextAlignmentRight:
        {
            // UBIDI_NEUTRAL will use left to right WritingDirection.
            boolean b0 = (mmUBidi.UBIDI_NEUTRAL == hBaseDirection) && (hTextAlignment == mmTextFormat.mmTextAlignmentLeft);
            boolean b1 = (mmUBidi.UBIDI_LTR     == hBaseDirection) && (hTextAlignment == mmTextFormat.mmTextAlignmentLeft);
            boolean b2 = (mmUBidi.UBIDI_RTL     == hBaseDirection) && (hTextAlignment == mmTextFormat.mmTextAlignmentRight);
            hAlignment = (b0 || b1 || b2) ? Layout.Alignment.ALIGN_NORMAL : Layout.Alignment.ALIGN_OPPOSITE;
        }
        break;

        default:
        break;
        }

        return hAlignment;
    }

    static public int GetTextParagraphAlignment(int hTextVAlignment)
    {
        return hTextVAlignment;
    }

    static public int GetTextWordWrapping(int hLineBreakMode)
    {
        return hLineBreakMode;
    }

    static public TextUtils.TruncateAt GetTextWordTruncating(int hLineBreakMode)
    {
        // nul is not Truncating.
        final TextUtils.TruncateAt[] gStyleTable =
        {
            null                            , // 0 mmTextLineBreakByWordWrapping
            null                            , // 1 mmTextLineBreakByCharWrapping
            null                            , // 2 mmTextLineBreakByClipping
            TextUtils.TruncateAt.START      , // 3 mmTextLineBreakByTruncatingHead
            TextUtils.TruncateAt.END        , // 4 mmTextLineBreakByTruncatingTail
            TextUtils.TruncateAt.MIDDLE     , // 5 mmTextLineBreakByTruncatingMiddle
        };
        if (0 <= hLineBreakMode && hLineBreakMode < gStyleTable.length)
        {
            return gStyleTable[hLineBreakMode];
        }
        else
        {
            return null;
        }
    }

    static public int GetWritingDirection(int hWritingDirection)
    {
        return hWritingDirection;
    }

    static public float GetFontObliqueSkewX(int hFontStyle)
    {
        return (mmTextFormat.mmFontStyleOblique == hFontStyle) ? -0.25f : 0.0f;
    }
    static public boolean GetFontStyleItalic(int hFontStyle)
    {
        return (mmTextFormat.mmFontStyleItalic == hFontStyle);
    }

    static public Typeface MakeTypeface(mmGraphicsFactory pGraphicsFactory, mmTextFormat pTextFormat)
    {
        if(Build.VERSION_CODES.P <= Build.VERSION.SDK_INT)
        {
            // Font weight API.
            return MakeTypeface_VersionP(pGraphicsFactory, pTextFormat);
        }
        else
        {
            // Fallback.
            return MakeTypeface_VersionFallback(pGraphicsFactory, pTextFormat);
        }
    }

    @TargetApi(Build.VERSION_CODES.P)
    static public Typeface MakeTypeface_VersionP(mmGraphicsFactory pGraphicsFactory, mmTextFormat pTextFormat)
    {
        Typeface pTypeface = null;
        // noinspection UnusedAssignment
        Typeface pTypefaceFamily = null;
        Typeface pTypefaceStyle = null;
        String hFontFamilyName = pTextFormat.hFontFamilyName;
        int hFontWeight = mmTextPlatform.GetFontWeight(pTextFormat.hFontWeight);
        int hFontStyle = mmTextPlatform.GetFontStyle(pTextFormat.hFontStyle);
        boolean hFontStyleItalic = mmTextPlatform.GetFontStyleItalic(pTextFormat.hFontStyle);
        // The font is package external resources. if not null.
        pTypefaceFamily = pGraphicsFactory.NativeGetFontHandleByFamily(hFontFamilyName);
        if (null == pTypefaceFamily)
        {
            // The font is system internal resources.
            try
            {
                pTypefaceStyle = Typeface.create(hFontFamilyName, hFontStyle);
            }
            catch (Exception e)
            {
                mmLogger.LogE("exception: " + e);
            }
        }
        else
        {
            // The font is package external resources.
            if (Typeface.NORMAL != hFontStyle)
            {
                // Additional extension parameters Style.
                try
                {
                    pTypefaceStyle = Typeface.create(pTypefaceFamily, hFontStyle);
                }
                catch (Exception e)
                {
                    mmLogger.LogE("exception: " + e);
                }
            }
            else
            {
                // copy reference.
                pTypefaceStyle = pTypefaceFamily;
            }
        }

        if (null == pTypefaceStyle)
        {
            // The family name is invalid. We fallback to default.
            try
            {
                pTypefaceStyle = Typeface.defaultFromStyle(hFontStyle);
            }
            catch (Exception e)
            {
                mmLogger.LogE("exception: " + e);
            }
        }

        if (null != pTypefaceStyle && mmTextFormat.mmFontWeightNormal != hFontWeight)
        {
            // Additional extension parameters Weight.
            try
            {
                pTypeface = Typeface.create(pTypefaceStyle, hFontWeight, hFontStyleItalic);
            }
            catch (Exception e)
            {
                mmLogger.LogE("exception: " + e);
            }
        }
        else
        {
            // copy reference.
            pTypeface = pTypefaceStyle;
        }

        if (null == pTypeface)
        {
            // Fallback to pTypefaceStyle.
            pTypeface = pTypefaceStyle;
        }

        if (null == pTypeface)
        {
            // Fallback for default Typeface.
            pTypeface = Typeface.DEFAULT;
        }

        return pTypeface;
    }
    static public Typeface MakeTypeface_VersionFallback(mmGraphicsFactory pGraphicsFactory, mmTextFormat pTextFormat)
    {
        Typeface pTypeface = null;
        // noinspection UnusedAssignment
        Typeface pTypefaceFamily = null;
        Typeface pTypefaceStyle = null;
        String hFontFamilyName = pTextFormat.hFontFamilyName;
        int hFontStyle = mmTextPlatform.GetFontStyle(pTextFormat.hFontStyle);
        // The font is package external resources. if not null.
        pTypefaceFamily = pGraphicsFactory.NativeGetFontHandleByFamily(hFontFamilyName);
        if (null == pTypefaceFamily)
        {
            // The font is system internal resources.
            try
            {
                pTypefaceStyle = Typeface.create(hFontFamilyName, hFontStyle);
            }
            catch (Exception e)
            {
                mmLogger.LogE("exception: " + e);
            }
        }
        else
        {
            // The font is package external resources.
            if (Typeface.NORMAL != hFontStyle)
            {
                // Additional extension parameters Style.
                try
                {
                    pTypefaceStyle = Typeface.create(pTypefaceFamily, hFontStyle);
                }
                catch (Exception e)
                {
                    mmLogger.LogE("exception: " + e);
                }
            }
            else
            {
                // copy reference.
                pTypefaceStyle = pTypefaceFamily;
            }
        }

        if (null == pTypefaceStyle)
        {
            // The family name is invalid. We fallback to default.
            try
            {
                pTypeface = Typeface.defaultFromStyle(hFontStyle);
            }
            catch (Exception e)
            {
                mmLogger.LogE("exception: " + e);
            }
        }
        else
        {
            // copy reference.
            pTypeface = pTypefaceStyle;
        }

        if (null == pTypeface)
        {
            // Fallback for default Typeface.
            pTypeface = Typeface.DEFAULT;
        }

        return pTypeface;
    }

    @SuppressWarnings("JavaJniMissingFunction")
    public static native int NativeGetUtf16OffsetLNormalise(char[] s, int z, int o);
    @SuppressWarnings("JavaJniMissingFunction")
    public static native int NativeGetUtf16OffsetRNormalise(char[] s, int z, int o);
}
