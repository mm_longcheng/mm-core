/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.app.Activity;
import android.util.Log;

@SuppressWarnings({"unused", "JavaJniMissingFunction"})
public class mmNativeApplication
{
    private static final String TAG = mmNativeApplication.class.getSimpleName();
    
    // strong reference.
    public long hNativePtr = 0;
    
    public mmLoggerManager pLoggerManager = new mmLoggerManager();
    public mmNativeClipboard pNativeClipboard = new mmNativeClipboard();

    public mmNativeApplication()
    {

    }

    public void Init()
    {
        this.pLoggerManager.Init();
        this.pNativeClipboard.Init();

        this.NativeAlloc();
        
        this.NativeInit();

        this.NativeSetClipboard(this.pNativeClipboard);

        Log.i(TAG, TAG + " Init succeed.");
    }

    public void Destroy()
    {
        this.NativeSetClipboard(null);
        this.NativeSetActivity(null);

        this.NativeDestroy();
        
        this.NativeDealloc();

        this.pNativeClipboard.Destroy();
        this.pLoggerManager.Destroy();

        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetActivity(Activity pActivity)
    {
        this.pNativeClipboard.SetActivity(pActivity);
        this.NativeSetActivity(pActivity);
    }
    
    public void SetDevice(mmDevice pDevice)
    {
        this.NativeSetDevice(pDevice);
    }
    public void SetPackageAssets(mmPackageAssets pPackageAssets)
    {
        this.NativeSetPackageAssets(pPackageAssets);
    }
    public void SetAppIdentifier(mmAppIdentifier pAppIdentifier)
    {
        this.NativeSetAppIdentifier(pAppIdentifier);
    }
    public void SetSecurityStore(mmSecurityStore pSecurityStore)
    {
        this.NativeSetSecurityStore(pSecurityStore);
    }
    // default is "mm.nwsi"
    public void SetModuleName(String pModuleName)
    {
        this.NativeSetModuleName(pModuleName);
    }
    // Render system name.
    public void SetRenderSystemName(String pRenderSystemName)
    {
        this.NativeSetRenderSystemName(pRenderSystemName);
    }

    // default is "nwsi"
    public void SetLoggerFileName(String pLoggerFileName)
    {
        this.NativeSetLoggerFileName(pLoggerFileName);
    }
    // default is MM_LOG_INFO
    public void SetLoggerLevel(int hLoggerLevel)
    {
        this.pLoggerManager.SetLoggerLevel(hLoggerLevel);
        this.NativeSetLoggerLevel(hLoggerLevel);
    }

    public void SetActivityMaster(long pActivityInterface)
    {
        this.NativeSetActivityMaster(pActivityInterface);
    }   

    public void OnFinishLaunching()
    {
        this.pLoggerManager.OnFinishLaunching();
        
        // Native API.
        this.NativeOnFinishLaunching();
    }
    public void OnBeforeTerminate()
    {
        // Native API.
        this.NativeOnBeforeTerminate();
        
        this.pLoggerManager.OnBeforeTerminate();
    }
    public void OnEnterBackground()
    {
        // Native API.
        this.NativeOnEnterBackground();
    }
    public void OnEnterForeground()
    {
        // Native API.
        this.NativeOnEnterForeground();
    }
    public void OnStart()
    {
        // Native API.
        this.NativeOnStart();
    }
    public void OnInterrupt()
    {
        // Native API.
        this.NativeOnInterrupt();
    }
    public void OnShutdown()
    {
        // Native API.
        this.NativeOnShutdown();
    }
    public void OnJoin()
    {
        // Native API.
        this.NativeOnJoin();
    }
    // ===========================================================
    // native java -> c
    // ===========================================================
    public native void NativeAlloc();
    public native void NativeDealloc();
    public native void NativeInit();
    public native void NativeDestroy();
    
    public native long NativeGetContextMaster();
    public native long NativeGetSurfaceMaster();

    public native void NativeSetActivity(Activity pActivity);
    public native void NativeSetClipboard(mmNativeClipboard pClipboard);
    
    public native void NativeSetDevice(mmDevice pDevice);
    public native void NativeSetPackageAssets(mmPackageAssets pPackageAssets);
    public native void NativeSetAppIdentifier(mmAppIdentifier pAppIdentifier);
    public native void NativeSetSecurityStore(mmSecurityStore pSecurityStore);

    // default is "mm.nwsi"
    public native void NativeSetModuleName(String pModuleName);
    // Render system name.
    public native void NativeSetRenderSystemName(String pRenderSystemName);

    // default is "nwsi"
    public native void NativeSetLoggerFileName(String pLoggerFileName);
    // default is MM_LOG_INFO
    public native void NativeSetLoggerLevel(int hLoggerLevel);

    public native void NativeSetActivityMaster(long pActivityInterface);
    
    public native void NativeOnFinishLaunching();
    public native void NativeOnBeforeTerminate();
    
    public native void NativeOnEnterBackground();
    public native void NativeOnEnterForeground();
    
    public native void NativeOnStart();
    public native void NativeOnInterrupt();
    public native void NativeOnShutdown();
    public native void NativeOnJoin();
}