/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import org.mm.core.mmLogger;
import org.mm.nwsi.mmISurface.mmEventKeypad;

import android.annotation.TargetApi;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputContentInfo;
import android.view.inputmethod.InputMethodManager;

public class mmUIInputConnection implements InputConnection
{
    private static final String TAG = mmUIInputConnection.class.getSimpleName();

    public mmUIViewSurfaceMaster pUIViewSurfaceMaster = null;
    private mmUIViewEditText pUIViewEditText = null;
    public char[] pKeycode = new char[1];
    int[] hRange = new int[2];
    
    public mmUIInputConnection()
    {

    }

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetUIViewSurfaceMaster(mmUIViewSurfaceMaster pUIViewSurfaceMaster)
    {
        this.pUIViewSurfaceMaster = pUIViewSurfaceMaster;
    }

    public void SetUIViewEditText(mmUIViewEditText pUIViewEditText)
    {
        this.pUIViewEditText = pUIViewEditText;
    }

    public void OnUpdateSelectionRangeValue()
    {
        this.pUIViewSurfaceMaster.NativeOnGetSelection(this.hRange);
    }
    
    @SuppressWarnings("UnusedAssignment")
    public void OnUpdateSelection(InputMethodManager imm, View pView)
    {
        int hSelectionL = -1;
        int hSelectionR = -1;
        int hCandL = -1;
        int hCandR = -1;
        
        this.OnUpdateSelectionRangeValue();
        
        hSelectionL = this.hRange[0];
        hSelectionR = this.hRange[1];
        
        imm.updateSelection(pView, hSelectionL, hSelectionR, hCandL, hCandR);
    }

    @Override
    public boolean beginBatchEdit()
    {
        // Tell the editor that you are starting a batch of editor operations.
        mmLogger.LogV(TAG + " beginBatchEdit");
        return true;
    }

    @Override
    public boolean clearMetaKeyStates(int states)
    {
        // Clear the given meta key pressed states in the given input connection.
        mmLogger.LogV(TAG + " clearMetaKeyStates");

        // META_ALT_MASK       Constant Value: 50      (0x00000032)
        // META_CTRL_MASK      Constant Value: 28672   (0x00007000)
        // META_META_MASK      Constant Value: 458752  (0x00070000)
        // META_SHIFT_MASK     Constant Value: 193     (0x000000c1)
        //
        // META_ALT_ON         Constant Value: 2       (0x00000002)
        // META_ALT_LEFT_ON    Constant Value: 16      (0x00000010)
        // META_ALT_RIGHT_ON   Constant Value: 32      (0x00000020)
        // META_SHIFT_ON       Constant Value: 1       (0x00000001)
        // META_SHIFT_LEFT_ON  Constant Value: 64      (0x00000040)
        // META_SHIFT_RIGHT_ON Constant Value: 128     (0x00000080)
        // META_SYM_ON         Constant Value: 4       (0x00000004)
        // META_FUNCTION_ON    Constant Value: 8       (0x00000008)
        // META_CTRL_ON        Constant Value: 4096    (0x00001000)
        // META_CTRL_LEFT_ON   Constant Value: 8192    (0x00002000)
        // META_CTRL_RIGHT_ON  Constant Value: 16384   (0x00004000)
        // META_META_ON        Constant Value: 65536   (0x00010000)
        // META_META_LEFT_ON   Constant Value: 131072  (0x00020000)
        // META_META_RIGHT_ON  Constant Value: 262144  (0x00040000)
        // META_CAPS_LOCK_ON   Constant Value: 1048576 (0x00100000)
        // META_NUM_LOCK_ON    Constant Value: 2097152 (0x00200000)
        // META_SCROLL_LOCK_ON Constant Value: 4194304 (0x00400000)
        return true;
    }

     // Added in API level 24
     @Override
     @TargetApi(24)
     public void closeConnection()
     {
         // Called by the system up to only once to notify that the system is about to
         // invalidate connection between the input method and the application.
         mmLogger.LogV(TAG + " closeConnection");
         
         this.finishComposingText();
     }
    
     @Override
     public boolean commitCompletion(CompletionInfo text)
     {
         // Commit a completion the user has selected from the possible ones previously reported
         // to InputMethodSession#displayCompletions or InputMethodManager#displayCompletions.
         mmLogger.LogV(TAG + " commitCompletion " + text);
         return true;
     }

     // Added in API level 25
     @TargetApi(25)
     @Override
     public boolean commitContent(InputContentInfo inputContentInfo, int flags, Bundle opts)
     {
         // Called by the input method to commit content such as a PNG image to the editor.
         mmLogger.LogV(TAG + " commitContent " + inputContentInfo + " " + flags + " " + opts);
         return true;
     }

    @Override
    public boolean commitCorrection(CorrectionInfo correctionInfo)
    {
        // Commit a correction automatically performed on the raw user's input.
        mmLogger.LogV(TAG + " commitCorrection " + correctionInfo);
        return true;
    }

    @Override
    public boolean commitText(CharSequence text, int newCursorPosition)
    {
        // Commit text to the text box and set the new cursor position.
        mmLogger.LogD(TAG + " commitText: \"" + text + "\" " + newCursorPosition);
        
        String pText = text.toString();
        this.pUIViewSurfaceMaster.NativeOnCommitText(pText, newCursorPosition);

        // when commitText, we need refresh menu content.
        this.pUIViewEditText.OnContextMenuUpdate();
        return true;
    }

    @Override
    public boolean deleteSurroundingText(int beforeLength, int afterLength)
    {
        // Delete beforeLength characters of text before the current cursor position, and delete
        // afterLength characters of text after the current cursor position, excluding the selection.
        mmLogger.LogD(TAG + " deleteSurroundingText: " + beforeLength + " " + afterLength);
        
        this.pUIViewSurfaceMaster.NativeOnDeleteSurroundingText(beforeLength, afterLength);
        return true;
    }

    // Added in API level 24
    @Override
    @TargetApi(24)
    public boolean deleteSurroundingTextInCodePoints(int beforeLength, int afterLength)
    {
        // A variant of deleteSurroundingText(int, int).
        mmLogger.LogD(TAG + " deleteSurroundingTextInCodePoints: " + beforeLength + " " + afterLength);
        return false;
    }

    @Override
    public boolean endBatchEdit()
    {
        // Tell the editor that you are done with a batch edit previously initiated with beginBatchEdit().
        mmLogger.LogV(TAG + " endBatchEdit");
        return true;
    }

    @Override
    public boolean finishComposingText()
    {
        // Have the text editor finish whatever composing text is currently active.
        mmLogger.LogD(TAG + " finishComposingText");
        
        this.pUIViewSurfaceMaster.NativeOnFinishComposingText();

        return true;
    }

    @Override
    public int getCursorCapsMode(int reqModes)
    {
        // Retrieve the current capitalization mode in effect at the current cursor position in the text.
        mmLogger.LogV(TAG + " getCursorCapsMode " + reqModes);

        char hUtf16 = this.pUIViewSurfaceMaster.NativeOnGetCursorCapsUtf16();
        String pUnicodeString = String.valueOf(hUtf16);
        return TextUtils.getCapsMode(pUnicodeString, 0, reqModes);
    }

    @Override
    public ExtractedText getExtractedText(ExtractedTextRequest request, int flags)
    {
        // Retrieve the current text in the input connection's editor, and monitor for any changes to it.
        mmLogger.LogV(TAG + " getExtractedText " + request + " " + flags);
        return null;
    }

     // Added in API level 24
     @Override
     @TargetApi(24)
     public Handler getHandler()
     {
         // Called by the InputMethodManager to enable application developers to specify a dedicated
         // Handler on which incoming IPC method calls from input methods will be dispatched.
         mmLogger.LogV(TAG + " getHandler ");
         return null;
     }

    @Override
    public CharSequence getSelectedText(int flags)
    {
        // Gets the selected text, if any.        
        String text = this.pUIViewSurfaceMaster.NativeOnGetSelectedText(flags);
        mmLogger.LogD(TAG + " getSelectedText " + flags + " \"" + text + "\"");
        return text;
    }

    @Override
    public CharSequence getTextAfterCursor(int n, int flags)
    {
        // Get n characters of text after the current cursor position.        
        String text = this.pUIViewSurfaceMaster.NativeOnGetTextAfterCursor(n, flags);
        mmLogger.LogD(TAG + " getTextAfterCursor " + n + " " + flags + " \"" + text + "\"");
        return text;
    }

    @Override
    public CharSequence getTextBeforeCursor(int n, int flags)
    {
        // Get n characters of text before the current cursor position.        
        String text =  this.pUIViewSurfaceMaster.NativeOnGetTextBeforeCursor(n, flags);
        mmLogger.LogD(TAG + " getTextBeforeCursor " + n + " " + flags + " \"" + text + "\"");
        return text;
    }

    @Override
    public boolean performContextMenuAction(int id)
    {
        // Perform a context menu action on the field.
        mmLogger.LogD(TAG + " performContextMenuAction " + id);
        
        // android.R.id.selectAll           Constant Value: 16908319 (0x0102001f)
        // android.R.id.cut                 Constant Value: 16908320 (0x01020020)
        // android.R.id.copy                Constant Value: 16908321 (0x01020021)
        // android.R.id.paste               Constant Value: 16908322 (0x01020022)
        // android.R.id.copyUrl             Constant Value: 16908323 (0x01020023)
        // android.R.id.switchInputMethod   Constant Value: 16908324 (0x01020024)
        // android.R.id.startSelectingText  Constant Value: 16908328 (0x01020028)
        // android.R.id.stopSelectingText   Constant Value: 16908329 (0x01020029)
        this.pUIViewSurfaceMaster.NativeOnPerformContextMenuAction(id);

        // when MenuItem clicked, we need refresh menu content.
        this.pUIViewEditText.OnContextMenuUpdate();
        return true;
    }

    @Override
    public boolean performEditorAction(int editorAction)
    {
        // Have the editor perform an action it has said it can do.
        mmLogger.LogD(TAG + " performEditorAction " + editorAction);
        
        // EditorInfo.IME_ACTION_UNSPECIFIED Constant Value: 0 (0x00000000)
        // EditorInfo.IME_ACTION_NONE        Constant Value: 1 (0x00000001)
        // EditorInfo.IME_ACTION_GO          Constant Value: 2 (0x00000002)
        // EditorInfo.IME_ACTION_SEARCH      Constant Value: 3 (0x00000003)
        // EditorInfo.IME_ACTION_SEND        Constant Value: 4 (0x00000004)
        // EditorInfo.IME_ACTION_NEXT        Constant Value: 5 (0x00000005)
        // EditorInfo.IME_ACTION_DONE        Constant Value: 6 (0x00000006)
        // EditorInfo.IME_ACTION_PREVIOUS    Constant Value: 7 (0x00000007)
        this.pUIViewSurfaceMaster.NativeOnPerformEditorAction(editorAction);
        return true;
    }

    @Override
    public boolean performPrivateCommand(String action, Bundle data)
    {
        // API to send private commands from an input method to its connected editor.
        mmLogger.LogV(TAG + " performPrivateCommand " + action + " " + data);
        return true;
    }

    @Override
    public boolean reportFullscreenMode(boolean enabled)
    {
        // Called back when the connected IME switches between fullscreen and normal modes.
        mmLogger.LogV(TAG + " reportFullscreenMode " + enabled);
        return true;
    }

    // Added in API level 21
    @Override
    @TargetApi(21)
    public boolean requestCursorUpdates(int cursorUpdateMode)
    {
        // Called by the input method to ask the editor for calling back
        // InputMethodManager#updateCursorAnchorInfo(android.view.View, CursorAnchorInfo)
        // to notify cursor/anchor locations.
        mmLogger.LogV(TAG + " requestCursorUpdates " + cursorUpdateMode);
        return true;
    }

    @Override
    public boolean sendKeyEvent(KeyEvent event)
    {
        // Send a key event to the process that is currently attached through this input connection.
        
        int hScanCode = event.getScanCode();
        int hKeyCode = event.getKeyCode();
        int hUnicodeChar = event.getUnicodeChar();
        
        mmLogger.LogD(TAG + " sendKeyEvent " + hScanCode + " " + hKeyCode + " " + hUnicodeChar);
        
        mmEventKeypad pKeypad = this.pUIViewSurfaceMaster.hKeypad;

        this.pUIViewSurfaceMaster.OnTextDataHandler(hKeyCode, event);
        
        int hAction = event.getAction();
        switch(hAction)
        {
        case KeyEvent.ACTION_DOWN:
            {
                // Native API.
                this.pUIViewSurfaceMaster.NativeOnKeypadPressed(pKeypad);

                // when key down, we need refresh menu content.
                this.pUIViewEditText.OnContextMenuUpdate();
            }
            break;
        case KeyEvent.ACTION_UP:
            {
                // Native API.
                this.pUIViewSurfaceMaster.NativeOnKeypadRelease(pKeypad);

                /*
                * 0 == hUnicode is control key event.
                * 0 != hUnicode May need to insert characters
                * this.pKeycode[0] = (char)hUnicode;
                * this.pUIViewSurfaceMaster.NativeOnInsertTextUtf16(this.pKeycode, 0, 1);
                */

                this.pUIViewSurfaceMaster.NativeOnFinishComposingText();
            }
            break;
        default:
            break;
        }
        
        return true;
    }

    @Override
    public boolean setComposingRegion(int start, int end)
    {
        // Mark a certain region of text as composing text.
        mmLogger.LogD(TAG + " setComposingRegion " + start + " " + end);
        
        this.pUIViewSurfaceMaster.NativeOnSetComposingRegion(start, end);
        return true;
    }

    @Override
    public boolean setComposingText(CharSequence text, int newCursorPosition)
    {
        // Replace the currently composing text with the given text, and set the new cursor position.
        mmLogger.LogD(TAG + " setComposingText " + text + " " + newCursorPosition);
        
        String pText = text.toString();
        this.pUIViewSurfaceMaster.NativeOnSetComposingText(pText, newCursorPosition);
        return true;
    }

    @Override
    public boolean setSelection(int start, int end)
    {
        // Set the selection of the text editor.
        mmLogger.LogD(TAG + " setSelection " + start + " " + end);
        
        this.pUIViewSurfaceMaster.NativeOnSetSelection(start, end);
        return true;
    }
}