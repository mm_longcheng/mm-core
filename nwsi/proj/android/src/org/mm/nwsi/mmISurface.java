/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

@SuppressWarnings("unused")
public class mmISurface
{
    // enum mmSurfaceActiveStatusType
    // Background.
    public final static int mmActiveStatusBackground = 0;
    // Foreground.
    public final static int mmActiveStatusForeground = 1;

    public static class mmEventKeypad
    {
        // modifier mask
        public int modifier_mask;

        /* Key code. */
        public int key;

        /* Text length. */
        public int length;
        /* Text buffer. */
        public char[] text = new char[4];

        // Return value.
        //     0 Need to handle keyboard occlusion.
        //     1 Event has been processed.
        public int handle;
        
        public void Init()
        {
            this.modifier_mask = 0;
            this.key = 0;
            this.length = 0;
            java.util.Arrays.fill(this.text, 0, 4, (char)0);
            this.handle = 0;
        }
        public void Destroy()
        {
            this.modifier_mask = 0;
            this.key = 0;
            this.length = 0;
            java.util.Arrays.fill(this.text, 0, 4, (char)0);
            this.handle = 0;
        }
        public void Reset()
        {
            this.modifier_mask = 0;
            this.key = 0;
            this.length = 0;
            java.util.Arrays.fill(this.text, 0, 4, (char)0);
            this.handle = 0;
        }
    }
    
    public static class mmEventCursor
    {
        // modifier mask
        public int modifier_mask;
        // button mask
        public int button_mask;
        // button id
        public int button_id;
        // Absolute position x.
        public double abs_x;
        // Absolute position y.
        public double abs_y;
        // Relative position x.
        public double rel_x;
        // Relative position y.
        public double rel_y;
        /* wheel scrolling delta x */
        public double wheel_dx;
        /* wheel scrolling delta y */
        public double wheel_dy;
        // The UTC time of this fix, in milliseconds since January 1, 1970.
        // The time (in ms) when event trigger.
        public long timestamp;
        
        // Return value.
        //     0 Need to handle keyboard pop up.
        //     1 Event has been processed.
        public int handle;
        
        public void Init()
        {
            this.modifier_mask = 0;
            this.button_mask = 0;
            this.button_id = 0;
            this.abs_x = 0.0;
            this.abs_y = 0.0;
            this.rel_x = 0.0;
            this.rel_y = 0.0;
            this.wheel_dx = 0.0;
            this.wheel_dy = 0.0;
            this.timestamp = 0;
            this.handle = 0;
        }
        public void Destroy()
        {
            this.modifier_mask = 0;
            this.button_mask = 0;
            this.button_id = 0;
            this.abs_x = 0.0;
            this.abs_y = 0.0;
            this.rel_x = 0.0;
            this.rel_y = 0.0;
            this.wheel_dx = 0.0;
            this.wheel_dy = 0.0;
            this.timestamp = 0;
            this.handle = 0;
        }
        public void Reset()
        {
            this.modifier_mask = 0;
            this.button_mask = 0;
            this.button_id = 0;
            this.abs_x = 0.0;
            this.abs_y = 0.0;
            this.rel_x = 0.0;
            this.rel_y = 0.0;
            this.wheel_dx = 0.0;
            this.wheel_dy = 0.0;
            this.timestamp = 0;
            this.handle = 0;
        }
    }
    
    // Touch phase type.
    // enum mmTouchPhaseType
    // Touch began.
    public final static int mmTouchBegan = 0;
    // Touch move.
    public final static int mmTouchMoved = 1;
    // Touch end.
    public final static int mmTouchEnded = 2;
    // Touch break, cancel.
    public final static int mmTouchBreak = 3;

    // Touch point.
    public static class mmEventTouch
    {
        // Motion event weak ref.
        public long motion_id;
        // The number of times the finger was tapped for this given touch.
        public int tap_count;
        // The phase of the touch.TouchPhase touch began, moved, ended, or was canceled.
        public int phase;
        // modifier mask
        public int modifier_mask;
        // button mask
        public int button_mask;
        // Absolute position x.
        public double abs_x;
        // Absolute position y.
        public double abs_y;
        // Relative position x.
        public double rel_x;
        // Relative position y.
        public double rel_y;
        // The force of the touch, where a value of 1.0 represents the 
        // force of an average touch (predetermined by the system, not user-specific).
        public double force_value;
        // The maximum possible force for a touch.
        public double force_maximum;
        // The major radius (in points) of the touch.
        public double major_radius;
        // The minor radius (in points) of the touch.
        public double minor_radius;
        // The area size value, (0, 1) an abstract metric.
        public double size_value;
        // The UTC time of this fix, in milliseconds since January 1, 1970.
        // The time (in ms) when event trigger.
        public long timestamp;
    
        public void Init()
        {
            this.motion_id = 0;
            this.tap_count = 0;
            this.phase = mmTouchBegan;
            this.modifier_mask = 0;
            this.button_mask = 0;
            this.abs_x = 0.0;
            this.abs_y = 0.0;
            this.rel_x = 0.0;
            this.rel_y = 0.0;
            this.force_value = 1.0;
            this.force_maximum = 1.0;
            this.major_radius = 1.0;
            this.minor_radius = 1.0;
            this.size_value = 1.0;
            this.timestamp = 0;
        }
        public void Destroy()
        {
            this.motion_id = 0;
            this.tap_count = 0;
            this.phase = mmTouchBegan;
            this.modifier_mask = 0;
            this.button_mask = 0;
            this.abs_x = 0.0;
            this.abs_y = 0.0;
            this.rel_x = 0.0;
            this.rel_y = 0.0;
            this.force_value = 1.0;
            this.force_maximum = 1.0;
            this.major_radius = 1.0;
            this.minor_radius = 1.0;
            this.size_value = 1.0;
            this.timestamp = 0;
        }
        public void Reset()
        {
            this.motion_id = 0;
            this.tap_count = 0;
            this.phase = mmTouchBegan;
            this.modifier_mask = 0;
            this.button_mask = 0;
            this.abs_x = 0.0;
            this.abs_y = 0.0;
            this.rel_x = 0.0;
            this.rel_y = 0.0;
            this.force_value = 1.0;
            this.force_maximum = 1.0;
            this.major_radius = 1.0;
            this.minor_radius = 1.0;
            this.size_value = 1.0;
            this.timestamp = 0;
        }
    }
    
    public static class mmEventTouchs
    {
        // max size.
        public int max_size = 0;
        // size of touchs.
        public int size = 0;
        // weak ref.
        public mmEventTouch[] touchs = null;

        public void Init()
        {
            this.max_size = 0;
            this.size = 0;
            this.touchs = null;
        }
        public void Destroy()
        {
            this.max_size = 0;
            this.size = 0;
            this.touchs = null;
        }
        public void Reset()
        {
            this.max_size = 0;
            this.size = 0;
            this.touchs = null;
        }

        @SuppressWarnings("UnusedAssignment")
        public void AlignedMemory(int size)
        {
            if(this.max_size < size)
            {
                int i = 0;
                this.max_size = size;
                this.touchs = new mmEventTouch[this.max_size];
                for (i = 0;i < this.max_size;++i)
                {
                    this.touchs[i] = new mmEventTouch();
                }
            }
            this.size = size;
        }
        public mmEventTouch GetForIndexReference(int index)
        {
            // assert (index < this.size);
            return this.touchs[index];
        }
    }
    
    // enum mmEditTextType
    @SuppressWarnings("PointlessBitwiseExpression")
    public final static int mmEditTextWhole = (1 << 0);// whole data is valid, size and capacity is invalid.
    public final static int mmEditTextRsize = (1 << 1);// resize size data is valid.
    public final static int mmEditTextCsize = (1 << 2);// capacity size data is valid.
    public final static int mmEditTextRange = (1 << 3);// only range data is valid.
    public final static int mmEditTextCaret = (1 << 4);// only caret data is valid.
    public final static int mmEditTextDirty = (1 << 5);// text is dirty, only SetText valid.
    
    // Text content data.
    public static class mmEventEditText
    {
        // text utf32 character weak reference pointer.
        public int[] t;
        // c string utf32 character size.
        public int size;
        // c string utf32 character capacity.
        public int capacity;
        // current cursor position.
        public int c;
        // lift  Selection position.
        public int l;
        // right Selection position.
        public int r;
        // mmEventTextType.
        public int v;
        
        public void Init()
        {
            this.t = null;
            this.size = 0;
            this.capacity = 0;
            this.c = 0;
            this.l = 0;
            this.r = 0;
            this.v = mmEditTextWhole;
        }
        public void Destroy()
        {
            this.t = null;
            this.size = 0;
            this.capacity = 0;
            this.c = 0;
            this.l = 0;
            this.r = 0;
            this.v = mmEditTextWhole;
        }
        public void Reset()
        {
            this.t = null;
            this.size = 0;
            this.capacity = 0;
            this.c = 0;
            this.l = 0;
            this.r = 0;
            this.v = mmEditTextWhole;
        }
    }

    // enum mmKeypadStatusType
    public final static int mmKeypadStatusShow = 0;// show keypad soft input.
    public final static int mmKeypadStatusHide = 1;// hide keypad soft input.

    // enum mmKeypadAnimationCurve
    public final static int mmKeypadAnimationCurveEaseInOut = 0;
    public final static int mmKeypadAnimationCurveEaseIn    = 1;
    public final static int mmKeypadAnimationCurveEaseOut   = 2;
    public final static int mmKeypadAnimationCurveLinear    = 3;

    @SuppressWarnings("RedundantCast")
    public static class mmEventKeypadStatus
    {
        // The surface pointer.
        public Object surface;

        // Layer:
        //     screen <- window trigger window attach to screen.
        //     extern           extern screen brothers.
        //
        // Rect(x, y, w, h)
        public double[] screen_rect = new double[4];
        public double[] safety_area = new double[4];
        public double[] keypad_rect = new double[4];
        public double[] window_rect = new double[4];
        
        // Animation duration for this event.
        public double animation_duration;
        // mmKeypadAnimationCurve.
        // Some time the value can be out of enum.
        public int animation_curve;
        
        // Status type. mmSurfaceKeypadStatusType.
        public int state;
        
        // Return value.
        //     0 Need to handle keyboard occlusion.
        //     1 Event has been processed.
        public int handle;

        public void Init()
        {
            this.surface = null;
            java.util.Arrays.fill(this.screen_rect, 0, 4, (double)0);
            java.util.Arrays.fill(this.safety_area, 0, 4, (double)0);
            java.util.Arrays.fill(this.keypad_rect, 0, 4, (double)0);
            java.util.Arrays.fill(this.window_rect, 0, 4, (double)0);
            this.animation_duration = 0.0;
            this.animation_curve = mmKeypadAnimationCurveLinear;
            this.state = mmKeypadStatusShow;
            this.handle = 0;
        }
        public void Destroy()
        {
            this.surface = null;
            java.util.Arrays.fill(this.screen_rect, 0, 4, (double)0);
            java.util.Arrays.fill(this.safety_area, 0, 4, (double)0);
            java.util.Arrays.fill(this.keypad_rect, 0, 4, (double)0);
            java.util.Arrays.fill(this.window_rect, 0, 4, (double)0);
            this.animation_duration = 0.0;
            this.animation_curve = mmKeypadAnimationCurveLinear;
            this.state = mmKeypadStatusShow;
            this.handle = 0;
        }
        public void Reset()
        {
            this.surface = null;
            java.util.Arrays.fill(this.screen_rect, 0, 4, (double)0);
            java.util.Arrays.fill(this.safety_area, 0, 4, (double)0);
            java.util.Arrays.fill(this.keypad_rect, 0, 4, (double)0);
            java.util.Arrays.fill(this.window_rect, 0, 4, (double)0);
            this.animation_duration = 0.0;
            this.animation_curve = mmKeypadAnimationCurveLinear;
            this.state = mmKeypadStatusShow;
            this.handle = 0;
        }
    }

    // Surface size change.
    @SuppressWarnings("RedundantCast")
    public static class mmEventMetrics
    {
        // The surface pointer.
        public Object surface;

        // canvas size.
        //
        // Size(w, h)
        public double[] canvas_size = new double[2];

        // window size.
        //
        // Size(w, h)
        public double[] window_size = new double[2];

        // safety area.
        //
        // Rect(x, y, w, h)
        public double[] safety_area = new double[4];

        public void Init()
        {
            this.surface = null;
            java.util.Arrays.fill(this.canvas_size, 0, 2, (double)0);
            java.util.Arrays.fill(this.window_size, 0, 2, (double)0);
            java.util.Arrays.fill(this.safety_area, 0, 4, (double)0);
        }
        public void Destroy()
        {
            this.surface = null;
            java.util.Arrays.fill(this.canvas_size, 0, 2, (double)0);
            java.util.Arrays.fill(this.window_size, 0, 2, (double)0);
            java.util.Arrays.fill(this.safety_area, 0, 4, (double)0);
        }
        public void Reset()
        {
            java.util.Arrays.fill(this.canvas_size, 0, 2, (double)0);
            java.util.Arrays.fill(this.window_size, 0, 2, (double)0);
            java.util.Arrays.fill(this.safety_area, 0, 4, (double)0);
        }
    }


    // enum mmKeypadType
    public final static int mmKeypadTypeDefault               =  0; /* Default for any.            */
    public final static int mmKeypadTypeASCIICapable          =  1; /* ASCII [A, z]                */
    public final static int mmKeypadTypeNumbersAndPunctuation =  2; /* [0, 9] [!, @, #, $, %, ...] */
    public final static int mmKeypadTypeUrl                   =  3; /* URL special ".com"          */
    public final static int mmKeypadTypeNumberPad             =  4; /* [0, 9]                      */
    public final static int mmKeypadTypePhonePad              =  5; /* [0, 9] [+, *, #]            */
    public final static int mmKeypadTypeNamePhonePad          =  6; /* [A, z]                      */
    public final static int mmKeypadTypeEmailAddress          =  7; /* [A, z] [@, .]               */
    public final static int mmKeypadTypeDecimalPad            =  8; /* [0, 9] [.]                  */
    public final static int mmKeypadTypeTwitter               =  9; /* Twitter style               */
    public final static int mmKeypadTypeWebSearch             = 10; /* [A, z] special Go action.   */
    public final static int mmKeypadTypeASCIICapableNumberPad = 11; /* [A, z] [0, 9]               */

    // enum mmKeypadAppearance
    public final static int mmKeypadAppearanceDefault         =  0; /* Default.    */
    public final static int mmKeypadAppearanceDark            =  1; /* Dark theme  */
    public final static int mmKeypadAppearanceLight           =  2; /* Light theme */

    // enum mmKeypadReturnKey
    public final static int mmKeypadReturnKeyDefault          =  0; /* Default.         */
    public final static int mmKeypadReturnKeyGo               =  1; /* Go               */
    public final static int mmKeypadReturnKeyGoogle           =  2; /* Google(Search)   */
    public final static int mmKeypadReturnKeyJoin             =  3; /* Join             */
    public final static int mmKeypadReturnKeyNext             =  4; /* Next             */
    public final static int mmKeypadReturnKeyRoute            =  5; /* Route            */
    public final static int mmKeypadReturnKeySearch           =  6; /* Search           */
    public final static int mmKeypadReturnKeySend             =  7; /* Send             */
    public final static int mmKeypadReturnKeyYahoo            =  8; /* Yahoo(Search)    */
    public final static int mmKeypadReturnKeyDone             =  9; /* Done             */
    public final static int mmKeypadReturnKeyEmergencyCall    = 10; /* EmergencyCall    */
    public final static int mmKeypadReturnKeyContinue         = 11; /* Continue         */

    public interface mmISurfaceKeypad
    {
        void OnSetKeypadType(int hType);
        void OnSetKeypadAppearance(int hAppearance);
        void OnSetKeypadReturnKey(int hReturnKey);
        void OnKeypadStatusShow();
        void OnKeypadStatusHide();
    }
}