package org.mm.nwsi;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;

import org.mm.core.mmLogger;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@SuppressWarnings("unused")
public class mmTextRenderTarget
{
    private static final String TAG = mmTextRenderTarget.class.getSimpleName();

    public mmGraphicsFactory pGraphicsFactory = null;
    public mmTextParagraphFormat pParagraphFormat = null;
    
    public Bitmap pBitmap = null;
    public Canvas pCanvas = null;
    ByteBuffer pByteBuffer = null;

    public void Init()
    {
        this.pBitmap = null;
        this.pCanvas = null;
        this.pParagraphFormat = null;
    }
    public void Destroy()
    {
        this.pBitmap = null;
        this.pCanvas = null;
        this.pParagraphFormat = null;
    }

    public void SetGraphicsFactory(mmGraphicsFactory pGraphicsFactory)
    {
        this.pGraphicsFactory = pGraphicsFactory;
    }
    
    public void SetTextParagraphFormat(mmTextParagraphFormat pParagraphFormat)
    {
        this.pParagraphFormat = pParagraphFormat;
    }

    @SuppressWarnings("ConstantConditions")
    public void OnFinishLaunching()
    {
        do
        {
            if(null == this.pParagraphFormat)
            {
                mmLogger.LogT(TAG + " OnFinishLaunching pParagraphFormat is null.");
                break;
            }
            int hWindowSizeW = (int)Math.ceil(this.pParagraphFormat.hWindowSize[0]);
            int hWindowSizeH = (int)Math.ceil(this.pParagraphFormat.hWindowSize[1]);
            if(0 > hWindowSizeW || 0 > hWindowSizeH)
            {
                mmLogger.LogT(TAG + " OnFinishLaunching hBitmapW or hBitmapH is invalid.");
                break;
            }
            // Avoid empty rectangles.
            hWindowSizeW = (0 == hWindowSizeW) ? 1 : hWindowSizeW;
            hWindowSizeH = (0 == hWindowSizeH) ? 1 : hWindowSizeH;

            // RGBA8888
            // hWindowSizeW * hWindowSizeH * 4;
            int hBufferSize = hWindowSizeW * hWindowSizeH * 4;
            this.pByteBuffer = ByteBuffer.allocate(hBufferSize);
            this.pByteBuffer.order(ByteOrder.nativeOrder());

            this.pBitmap = Bitmap.createBitmap(hWindowSizeW, hWindowSizeH, Bitmap.Config.ARGB_8888);
            if(null == this.pBitmap)
            {
                mmLogger.LogT(TAG + " OnFinishLaunching pBitmap is null.");
                break;
            }
            this.pCanvas = new Canvas(this.pBitmap);
            if(null == this.pCanvas)
            {
                mmLogger.LogT(TAG + " OnFinishLaunching pCanvas is null.");
                break;
            }
        }while(false);
    }
    public void OnBeforeTerminate()
    {
        this.pCanvas = null;
        this.pBitmap = null;
        this.pByteBuffer = null;
    }

    public void BeginDraw()
    {
        this.pCanvas.save();
        mmGraphicsContext.PushContext(this.pCanvas);
    }
    public void SetTransform(Matrix hTransform)
    {
        this.pCanvas.setMatrix(hTransform);
    }
    public void Clear()
    {
        this.pCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }
    public void EndDraw()
    {
        this.pCanvas.restore();
        mmGraphicsContext.PopContext();
    }
    public void BeginTransparency()
    {

    }
    public void EndTransparency()
    {

    }

    @SuppressWarnings("ConstantConditions")
    public byte[] BufferLock()
    {
        byte[] pBuffer = null;
        do
        {
            if (null == this.pBitmap)
            {
                mmLogger.LogT(TAG + " BufferLock pBitmap is null.");
                break;
            }
            if (null == this.pByteBuffer)
            {
                mmLogger.LogT(TAG + " BufferLock pBitmap is null.");
                break;
            }

            try
            {
                this.pByteBuffer.clear();
                this.pBitmap.copyPixelsToBuffer(this.pByteBuffer);
                pBuffer = pByteBuffer.array();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        } while(false);

        return pBuffer;
    }
    public void BufferUnLock(byte[] pByteBuffer)
    {
        // need do nothing.
    }
}
