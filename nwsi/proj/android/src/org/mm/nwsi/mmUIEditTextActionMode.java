/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import org.mm.core.mmLogger;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

@TargetApi(23)
public class mmUIEditTextActionMode extends ActionMode.Callback2
{
    private static final String TAG = mmUIEditTextActionMode.class.getSimpleName();
    
    private mmUIViewSurfaceMaster pUIViewSurfaceMaster = null;
    private mmUIViewEditText pUIViewEditText = null;
    private mmNativeClipboard pNativeClipboard = null;
    private final double[] hTextEditRect = new double[4];
    private final int[] hTextRange = new int[2];
    private final int[] hTextSize = new int[2];
    
    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }
    
    public void SetUIViewSurfaceMaster(mmUIViewSurfaceMaster pUIViewSurfaceMaster)
    {
        this.pUIViewSurfaceMaster = pUIViewSurfaceMaster;
    }
    
    public void SetUIViewEditText(mmUIViewEditText pUIViewEditText)
    {
        this.pUIViewEditText = pUIViewEditText;
    }

    public void SetNativeClipboard(mmNativeClipboard pNativeClipboard)
    {
        this.pNativeClipboard = pNativeClipboard;
    }

    @SuppressWarnings({"unused", "RedundantCast"})
    public void OnUpdateActionModeMenu(ActionMode mode, Menu menu)
    {
        final int MaxActionNumber = 4;
        //   0     1     2        3
        // [copy, cut, paste, selectAll]
        
        this.pUIViewSurfaceMaster.NativeOnGetTextSelection(this.hTextRange);
        this.pUIViewSurfaceMaster.NativeOnGetTextSize(this.hTextSize);

        String pClipboardText = this.pNativeClipboard.GetClipboardText();
        
        int textLength = (int)(this.hTextSize[0]);
        int textSelect = (int)(this.hTextRange[1] - this.hTextRange[0]);
        
        // pClipboardText maybe null.
        int buffLength = (int)(null == pClipboardText ? 0 : pClipboardText.length());
        
        mmLogger.LogD(TAG + " OnUpdateActionModeMenu " + textLength + " " + textSelect + " " + buffLength);
        
        boolean[] hFlags = new boolean[MaxActionNumber];
        hFlags[0] = (0 < textSelect);
        hFlags[1] = (0 < textSelect);
        hFlags[2] = (0 < buffLength);
        hFlags[3] = (textSelect != textLength);
        
        MenuItem pMenuItem0 = menu.findItem(android.R.id.copy);
        pMenuItem0.setVisible(hFlags[0]);
        
        MenuItem pMenuItem1 = menu.findItem(android.R.id.cut);
        pMenuItem1.setVisible(hFlags[1]);
        
        MenuItem pMenuItem2 = menu.findItem(android.R.id.paste);
        pMenuItem2.setVisible(hFlags[2]);
        
        MenuItem pMenuItem3 = menu.findItem(android.R.id.selectAll);
        pMenuItem3.setVisible(hFlags[3]);
    }
    
    // Called to refresh an action mode's action menu whenever it is invalidated.
    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) 
    {
        mmLogger.LogD(TAG + " onPrepareActionMode");
        this.OnUpdateActionModeMenu(mode, menu);
        return true;
    }

    // Called when an action mode is about to be exited and destroyed.
    @Override
    public void onDestroyActionMode(ActionMode mode) 
    {
        mmLogger.LogD(TAG + " onDestroyActionMode");
        this.pUIViewEditText.OnDestroyActionMode(mode);
    }

    // Called when action mode is first created.
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) 
    {
        mmLogger.LogD(TAG + " onCreateActionMode");
        
        int hMenuAction = MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT;
        
        MenuItem pMenuItem0 = menu.add(0, android.R.id.copy     , 0, "copy");
        pMenuItem0.setAlphabeticShortcut('c');
        pMenuItem0.setShowAsAction(hMenuAction);
        
        MenuItem pMenuItem1 = menu.add(0, android.R.id.cut      , 0, "cut");
        pMenuItem1.setAlphabeticShortcut('x');
        pMenuItem1.setShowAsAction(hMenuAction);
        
        MenuItem pMenuItem2 = menu.add(0, android.R.id.paste    , 0, "paste");
        pMenuItem2.setAlphabeticShortcut('v');
        pMenuItem2.setShowAsAction(hMenuAction);
        
        MenuItem pMenuItem3 = menu.add(0, android.R.id.selectAll, 0, "selectAll");
        pMenuItem3.setAlphabeticShortcut('a');
        pMenuItem3.setShowAsAction(hMenuAction);
        
        this.OnUpdateActionModeMenu(mode, menu);
        
        return true;
    }

    // Called to report a user click on an action button.
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) 
    {
        int hItemId = item.getItemId();
        mmLogger.LogD(TAG + " onActionItemClicked " + hItemId);
        this.pUIViewEditText.onMenuItemClick(item);
        return false;
    }
    
    @Override
    public void onGetContentRect(ActionMode mode, View view, Rect outRect) 
    {
        double hDisplayDensity = this.pUIViewSurfaceMaster.hDisplayDensity;

        java.util.Arrays.fill(this.hTextEditRect, 0, 4, (double)0);
        this.pUIViewSurfaceMaster.NativeOnGetTextEditRect(this.hTextEditRect);
        
        int l = (int)((this.hTextEditRect[0]                        ) * hDisplayDensity);
        int t = (int)((this.hTextEditRect[1]                        ) * hDisplayDensity);
        int r = (int)((this.hTextEditRect[0] + this.hTextEditRect[2]) * hDisplayDensity);
        int b = (int)((this.hTextEditRect[1] + this.hTextEditRect[3]) * hDisplayDensity);
        
        mmLogger.LogD(TAG + " onGetContentRect " + l + " " + t + " " + r + " " + b);
        
        outRect.set(l, t, r, b);
    }
}