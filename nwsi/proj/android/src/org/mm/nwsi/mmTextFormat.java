package org.mm.nwsi;

@SuppressWarnings({"unused"})
public class mmTextFormat
{
    // The default is mmTextAlignmentNatural.
    // enum mmTextAlignment_t
    public static final int mmTextAlignmentLeft      = 0;
    public static final int mmTextAlignmentCenter    = 1;
    public static final int mmTextAlignmentRight     = 2;
    public static final int mmTextAlignmentJustified = 3;
    public static final int mmTextAlignmentNatural   = 4;

    // The default is mmTextParagraphAlignmentTop.
    // enum mmTextParagraphAlignment_t
    public static final int mmTextParagraphAlignmentTop      = 0;
    public static final int mmTextParagraphAlignmentBottom   = 1;
    public static final int mmTextParagraphAlignmentCenter   = 2;

    // The default is mmTextLineBreakByWordWrapping.
    // enum mmTextLineBreakMode_t
    public static final int mmTextLineBreakByWordWrapping     = 0;
    public static final int mmTextLineBreakByCharWrapping     = 1;
    public static final int mmTextLineBreakByClipping         = 2;
    public static final int mmTextLineBreakByTruncatingHead   = 3;
    public static final int mmTextLineBreakByTruncatingTail   = 4;
    public static final int mmTextLineBreakByTruncatingMiddle = 5;

    // The default is mmTextWritingDirectionNatural.
    // enum mmTextWritingDirection_t
    public static final int mmTextWritingDirectionNatural     = -1;
    public static final int mmTextWritingDirectionLeftToRight = 0;
    public static final int mmTextWritingDirectionRightToLeft = 1;

    // default is mmFontStyleNormal.
    // enum mmFontStyle_t
    public static final int mmFontStyleNormal     = 0;
    public static final int mmFontStyleItalic     = 1;
    public static final int mmFontStyleOblique    = 2;

    // FontWeight
    public static final int mmFontWeightThin         = 100; // 100    Thin
    public static final int mmFontWeightExtraLight   = 200; // 200    Extra Light
    public static final int mmFontWeightLight        = 300; // 300    Light
    public static final int mmFontWeightNormal       = 400; // 400    Normal
    public static final int mmFontWeightMedium       = 500; // 500    Medium
    public static final int mmFontWeightSemiBold     = 600; // 600    Semi Bold
    public static final int mmFontWeightBold         = 700; // 700    Bold
    public static final int mmFontWeightExtraBold    = 800; // 800    Extra Bold
    public static final int mmFontWeightExtraBlack   = 900; // 900    Black

    // utf16 "Helvetica" Font family name default.
    public static final String mmFontFamilyNameDefault = "Helvetica";

    // "Helvetica",
    public String hFontFamilyName = mmFontFamilyNameDefault;
    // 12.0 font point size.
    public float hFontSize = 12.0f;
    // mmFontStyle_t default is mmFontStyleNormal.
    public int hFontStyle = mmFontStyleNormal;
    // default is mmFontWeightNormal bold is mmFontWeightBold.
    public int hFontWeight = mmFontWeightBold;

    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    public float[] hForegroundColor = new float[]{ 0.0f, 0.0f, 0.0f, 1.0f, };
    // default rgba { 0.0f, 0.0f, 0.0f, 0.0f, }
    public float[] hBackgroundColor = new float[]{ 0.0f, 0.0f, 0.0f, 0.0f, };

    // -3 fill and Stroke 3. default 0, not Stroke.
    public float hStrokeWidth = 0.0f;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    public float[] hStrokeColor = new float[]{ 0.0f, 0.0f, 0.0f, 1.0f, };

    // 0 is not underline.
    public float hUnderlineWidth = 0.0f;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    public float[] hUnderlineColor = new float[]{ 0.0f, 0.0f, 0.0f, 1.0f, };

    // 0 is not strikethru.
    public float hStrikeThruWidth = 0.0f;
    // default rgba { 0.0f, 0.0f, 0.0f, 1.0f, }
    public float[] hStrikeThruColor = new float[]{ 0.0f, 0.0f, 0.0f, 1.0f, };

    public void Init()
    {

    }
    public void Destroy()
    {

    }

    static public boolean IsFontEquals(mmTextFormat p, mmTextFormat q)
    {
        boolean b1 = (p.hFontFamilyName.equals(q.hFontFamilyName));
        boolean b2 = (p.hFontSize == q.hFontSize);
        boolean b3 = (p.hFontStyle == q.hFontStyle);
        boolean b4 = (p.hFontWeight == q.hFontWeight);
        return b1 && b2 && b3 && b4;
    }

    static public boolean IsFontStyleEquals(mmTextFormat p, mmTextFormat q)
    {
        boolean b1 = (p.hFontFamilyName.equals(q.hFontFamilyName));
        boolean b2 = (p.hFontStyle == q.hFontStyle);
        boolean b3 = (p.hFontWeight == q.hFontWeight);
        return b1 && b2 && b3;
    }
}
