/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.content.Context;
import android.util.Log;

@SuppressWarnings({"unused", "JavaJniMissingFunction"})
public class mmDevice
{
    private static final String TAG = mmDevice.class.getSimpleName();
    
    // weak reference.
    public long hNativePtr = 0;

    public Context pContext = null;

    public mmDevice()
    {

    }

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }

    public void SetContext(Context pContext)
    {
        this.pContext = pContext;
    }

    public void OnFinishLaunching()
    {
        Log.i(TAG, "Device mmDevice.OnFinishLaunching");
        Log.i(TAG, "Device ----------------------------+----------------------------------");
        Log.i(TAG, "Device                        BOARD: " + android.os.Build.BOARD);
        Log.i(TAG, "Device                   BOOTLOADER: " + android.os.Build.BOOTLOADER);
        Log.i(TAG, "Device                        BRAND: " + android.os.Build.BRAND);
        Log.i(TAG, "Device                       DEVICE: " + android.os.Build.DEVICE);
        Log.i(TAG, "Device                      DISPLAY: " + android.os.Build.DISPLAY);
        Log.i(TAG, "Device                  FINGERPRINT: " + android.os.Build.FINGERPRINT);
        Log.i(TAG, "Device                     HARDWARE: " + android.os.Build.HARDWARE);
        Log.i(TAG, "Device                         HOST: " + android.os.Build.HOST);
        Log.i(TAG, "Device                           ID: " + android.os.Build.ID);
        Log.i(TAG, "Device                 MANUFACTURER: " + android.os.Build.MANUFACTURER);
        Log.i(TAG, "Device                        MODEL: " + android.os.Build.MODEL);
        Log.i(TAG, "Device                      PRODUCT: " + android.os.Build.PRODUCT);
        Log.i(TAG, "Device                         TAGS: " + android.os.Build.TAGS);
        Log.i(TAG, "Device                         TIME: " + android.os.Build.TIME);
        Log.i(TAG, "Device                         TYPE: " + android.os.Build.TYPE);
        Log.i(TAG, "Device                         USER: " + android.os.Build.USER);
        Log.i(TAG, "Device                 RadioVersion: " + android.os.Build.getRadioVersion());
        Log.i(TAG, "Device ----------------------------+----------------------------------");
        Log.i(TAG, "Device                     CODENAME: " + android.os.Build.VERSION.CODENAME);
        Log.i(TAG, "Device                  INCREMENTAL: " + android.os.Build.VERSION.INCREMENTAL);
        Log.i(TAG, "Device                      RELEASE: " + android.os.Build.VERSION.RELEASE);
        Log.i(TAG, "Device                      SDK_INT: " + android.os.Build.VERSION.SDK_INT);
        Log.i(TAG, "Device ----------------------------+----------------------------------");
        
        String pName = android.os.Build.MODEL + "-" + android.os.Build.ID;
        
        this.NativeSetName(pName);
        this.NativeSetModel(android.os.Build.MODEL);
        this.NativeSetSystemName("Android");
        this.NativeSetSystemVersion(android.os.Build.VERSION.RELEASE);
    }
    public void OnBeforeTerminate()
    {
        Log.i(TAG, "Device mmDevice.OnBeforeTerminate");
    }

    // ===========================================================
    // native java -> c
    // ===========================================================
    public native void NativeSetName(String pName);
    public native void NativeSetModel(String pModel);
    public native void NativeSetSystemName(String pSystemName);
    public native void NativeSetSystemVersion(String pSystemVersion);
}