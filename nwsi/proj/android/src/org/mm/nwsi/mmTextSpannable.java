package org.mm.nwsi;

import java.util.LinkedList;

@SuppressWarnings({"unused", "Convert2Diamond"})
public class mmTextSpannable
{
    public mmTextFormat pMasterFormat = null;
    public LinkedList<mmTextSpan> hAttributes = new LinkedList<mmTextSpan>();

    // object for platform implement.
    public long pObject = 0;
    
    public void Init()
    {
        //this.hMasterFormat.Init();
        this.hAttributes.clear();
    }
    public void Destroy()
    {
        //this.hMasterFormat.Destroy();
        this.hAttributes.clear();
    }

    public void SetMasterFormat(mmTextFormat pMasterFormat)
    {
        this.pMasterFormat = pMasterFormat;
    }

    public void AddSpan(mmTextSpan u)
    {
        this.hAttributes.add(u);
    }
    public void ClearSpan()
    {
        this.hAttributes.clear();
    }
}
