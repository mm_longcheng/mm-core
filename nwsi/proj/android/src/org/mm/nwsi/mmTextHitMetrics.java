package org.mm.nwsi;


@SuppressWarnings({"unused"})
public class mmTextHitMetrics
{
    // [x, y, w, h]
    public float[] rect = new float[]{ 0.0f, 0.0f, 0.0f, 0.0f, };

    // text offset
    public int offset = 0;

    // text length
    public int length = 0;

    public int bidiLevel = 0;
    public int isText = 0;
    public int isTrimmed = 0;

    public void Reset()
    {
        java.util.Arrays.fill(this.rect, 0, 4, (float)0);
        this.offset = 0;
        this.length = 0;
        this.bidiLevel = 0;
        this.isText = 0;
        this.isTrimmed = 0;
    }
}
