/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.util.Log;

import org.mm.core.mmLogger;

@SuppressWarnings({"unused"})
public class mmSecurityStore
{
    private static final String TAG = mmSecurityStore.class.getSimpleName();

    public static final String MM_SECURITY_PREFS_GROUP = "SecurityInfo";

    // weak reference.
    public long hNativePtr = 0;

    public Activity pActivity = null;

    private final mmKeyStore pKeyStore = new mmKeyStore();

    private String pAppId = "";
    private String pSecretKey = "";

    public mmSecurityStore()
    {

    }

    public void Init()
    {
        Log.i(TAG, TAG + " Init succeed.");
    }
    public void Destroy()
    {
        Log.i(TAG, TAG + " Destroy succeed.");
    }

    public void SetActivity(Activity pActivity)
    {
        this.pActivity = pActivity;
    }
    
    public void OnFinishLaunching()
    {
        this.CreateKeyStore();
    }

    public void OnBeforeTerminate()
    {
        this.DeleteKeyStore();
    }

    @SuppressWarnings({"CharsetObjectCanBeUsed"})
    public void GenerateSecretKey()
    {
        ApplicationInfo pApplicationInfo = this.pActivity.getApplicationInfo();
        this.pAppId = pApplicationInfo.packageName;

        byte[] pUUIDBytes = new byte[64];
        mmAppIdentifier.GenerateUUIDApp(this.pActivity, this.pAppId, pUUIDBytes);

        byte[] pSecretKeyBytes = new byte[mmCryptAES.MM_SECRET_KEY_LENGTH];
        this.NativeGetSecretKey(this.pAppId, pUUIDBytes, pSecretKeyBytes);

        try
        {
            this.pSecretKey = new String(pSecretKeyBytes, 0, 32, "utf-8");
        }
        catch (Exception e)
        {
            mmLogger.LogE(TAG + " GenerateSecretKey use weak SecretKey.");
            this.pSecretKey = this.pAppId;
        }
    }

    public void CreateKeyStore()
    {
        this.GenerateSecretKey();

        this.pKeyStore.SetContext(this.pActivity);
        this.pKeyStore.OnFinishLaunching();
        this.pKeyStore.CreateKeys(this.pAppId);
    }
    public void DeleteKeyStore()
    {
        this.pKeyStore.OnBeforeTerminate();
    }

    @SuppressLint("ObsoleteSdkInt")
    public String Encrypt(String data, String secretKey)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
        {
            return this.Encrypt_Version18(data, secretKey);
        }
        else
        {
            return this.Encrypt_VersionFallback(data, secretKey);
        }
    }

    @SuppressLint("ObsoleteSdkInt")
    public String Decrypt(String base64Data, String secretKey)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
        {
            return this.Decrypt_Version18(base64Data, secretKey);
        }
        else
        {
            return this.Decrypt_VersionFallback(base64Data, secretKey);
        }
    }

    public String Encrypt_VersionFallback(String data, String secretKey)
    {
        return mmCryptAES.Encrypt(data, secretKey);
    }
    public String Encrypt_Version18(String data, String secretKey)
    {
        return this.pKeyStore.EncryptString(this.pAppId, data);
    }

    public String Decrypt_VersionFallback(String base64Data, String secretKey)
    {
        return mmCryptAES.Decrypt(base64Data, secretKey);
    }
    public String Decrypt_Version18(String base64Data, String secretKey)
    {
        return this.pKeyStore.DecryptString(this.pAppId, base64Data);
    }

    @SuppressLint("ApplySharedPref")
    @SuppressWarnings({"CharsetObjectCanBeUsed", "UnnecessaryLocalVariable"})
    public void Insert(String pMember, byte[] pSecret)
    {
        try
        {
            final String pGroup = mmSecurityStore.MM_SECURITY_PREFS_GROUP;
            final SharedPreferences pPreferences = this.pActivity.getSharedPreferences(pGroup, Context.MODE_PRIVATE);
            SharedPreferences.Editor pEditor = pPreferences.edit();

            String pSecretString = new String(pSecret, "utf-8");
            String pEncrypt = this.Encrypt(pSecretString, this.pSecretKey);

            pEditor.putString(pMember, pEncrypt);
            pEditor.commit();
        }
        catch (Exception e)
        {
            // nothing
        }
    }

    @SuppressLint("ApplySharedPref")
    @SuppressWarnings({"CharsetObjectCanBeUsed", "UnnecessaryLocalVariable"})
    public void Update(String pMember, byte[] pSecret)
    {
        try
        {
            final String pGroup = mmSecurityStore.MM_SECURITY_PREFS_GROUP;
            final SharedPreferences pPreferences = this.pActivity.getSharedPreferences(pGroup, Context.MODE_PRIVATE);
            SharedPreferences.Editor pEditor = pPreferences.edit();

            String pSecretString = new String(pSecret, "utf-8");
            String pEncrypt = this.Encrypt(pSecretString, this.pSecretKey);

            pEditor.putString(pMember, pEncrypt);
            pEditor.commit();
        }
        catch (Exception e)
        {
            // nothing
        }
    }

    @SuppressWarnings({"UnnecessaryLocalVariable"})
    public void Select(String pMember, byte[] pSecret)
    {
        try
        {
            final String pGroup = mmSecurityStore.MM_SECURITY_PREFS_GROUP;
            final SharedPreferences pPreferences = this.pActivity.getSharedPreferences(pGroup, Context.MODE_PRIVATE);
            String pEncrypt = pPreferences.getString(pMember, "");

            String pDecrypt = this.Decrypt(pEncrypt, this.pSecretKey);
            byte[] pDecryptBytes = pDecrypt.getBytes();

            java.util.Arrays.fill(pSecret, 0, 64, (byte)0);
            int hLength = Math.min(pDecryptBytes.length, 64);
            System.arraycopy(pDecryptBytes, 0, pSecret, 0, hLength);
        }
        catch (Exception e)
        {
            // nothing
        }
    }

    @SuppressLint("ApplySharedPref")
    @SuppressWarnings({"UnnecessaryLocalVariable"})
    public void Delete(String pMember)
    {
        try
        {
            final String pGroup = mmSecurityStore.MM_SECURITY_PREFS_GROUP;
            final SharedPreferences pPreferences = this.pActivity.getSharedPreferences(pGroup, Context.MODE_PRIVATE);
            SharedPreferences.Editor pEditor = pPreferences.edit();
            pEditor.remove(pMember);
            pEditor.commit();
        }
        catch (Exception e)
        {
            // nothing
        }
    }

    // pSecretKey[32]
    @SuppressWarnings("JavaJniMissingFunction")
    public native void NativeGetSecretKey(String pAppId, byte[] pUUIDBytes, byte[] pSecretKey);
}