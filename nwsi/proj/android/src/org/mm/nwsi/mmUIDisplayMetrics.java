/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2019-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

package org.mm.nwsi;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;

import java.lang.reflect.Method;

@SuppressWarnings("unused")
public class mmUIDisplayMetrics
{
    public static float GetDisplayDensity(Context pContext)
    {
        Resources pResources = pContext.getResources();
        DisplayMetrics pDisplayMetrics = pResources.getDisplayMetrics();
        return pDisplayMetrics.density;
    }
    
    // DP -> PX
    public static float Dp2Px(Context pContext, float hDpValue)
    {
        Resources pResources = pContext.getResources();
        DisplayMetrics pDisplayMetrics = pResources.getDisplayMetrics();
        float hDensity = pDisplayMetrics.density;
        return hDpValue * hDensity;
    }

    // PX -> DP
    public static float Px2Dp(Context pContext, float hPxValue)
    {
        Resources pResources = pContext.getResources();
        DisplayMetrics pDisplayMetrics = pResources.getDisplayMetrics();
        float hDensity = pDisplayMetrics.density;
        return hPxValue / hDensity;
    }

    // DP -> PX Integer
    public static int Dp2PxInteger(Context pContext, float hDpValue)
    {
        float hPxReal = Dp2Px(pContext, hDpValue);
        return (int)(hPxReal + 0.5f);
    }

    // PX -> DP Integer
    public static int Px2DpInteger(Context pContext, float hPxValue)
    {
        float hDpReal = Dp2Px(pContext, hPxValue);
        return (int)(hDpReal + 0.5f);
    }

    // size[4]  {x, y, w, h} Pixel
    public static void GetDisplayMetricsAreaSize(Activity pActivity, int[] hSize)
    {
        if(Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT)
        {
            // [17, Max] API.
            GetDisplayMetricsAreaSize_Version17(pActivity, hSize);
        }
        else if(Build.VERSION_CODES.ICE_CREAM_SANDWICH <= Build.VERSION.SDK_INT)
        {
            // [14, 17) API.
            GetDisplayMetricsAreaSize_Version14(pActivity, hSize);
        }
        else
        {
            // [Min, 14) API fallback.
            GetDisplayMetricsAreaSize_VersionFallback(pActivity, hSize);
        }
    }
    
    // get status bar height.
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public static int GetStatusBarHeight(Context pContext)
    {
        int hStatusBarHeight = 0;
        Resources pResources = pContext.getResources();
        int hResourceId = pResources.getIdentifier("status_bar_height", "dimen", "android");
        if (hResourceId > 0)
        {
            hStatusBarHeight = pResources.getDimensionPixelSize(hResourceId);
        }
        else
        {
            hStatusBarHeight = 0;
        }
        return hStatusBarHeight;
    }
    
    // get navigation bar height.
    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    public static int GetNavigationBarHeight(Context pContext)
    {
        int hNavigationBarHeight = 0;
        Resources pResources = pContext.getResources();
        int hResourceId = pResources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (hResourceId > 0)
        {
            hNavigationBarHeight = pResources.getDimensionPixelSize(hResourceId);
        }
        else
        {
            hNavigationBarHeight = 0;
        }
        return hNavigationBarHeight;
    }

    @SuppressLint("ObsoleteSdkInt")
    public static boolean GetIsHasSoftNavigationBar(Activity pActivity)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
        {
            return GetIsHasSoftNavigationBar_Version17(pActivity);
        }
        else
        {
            return GetIsHasSoftNavigationBar_VersionFallback(pActivity);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static int GetBottomInvalidHeight(Activity pActivity, View pView, int hNavigationBarHeight)
    {
        int hBottomInvalidHeight = 0;

        if(null == pActivity || null == pView)
        {
            hBottomInvalidHeight = 0;
        }
        else
        {
            int hUIVisibility = pView.getSystemUiVisibility();

            boolean hIsShowNavigation = (0 == (hUIVisibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION));

            WindowManager pWindowManager = pActivity.getWindowManager();
            Display pDisplay = pWindowManager.getDefaultDisplay();
            int hRotation = pDisplay.getRotation();

            // ROTATION_0   Constant Value: 0 (0x00000000)
            // ROTATION_90  Constant Value: 1 (0x00000001)
            // ROTATION_180 Constant Value: 2 (0x00000002)
            // ROTATION_270 Constant Value: 3 (0x00000003)

            switch(hRotation)
            {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                {
                    hBottomInvalidHeight = hIsShowNavigation ? hNavigationBarHeight : 0;
                }
                break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                {
                    // Counterclockwise 90
                    // Counterclockwise 270
                    hBottomInvalidHeight = 0;
                }
                default:
                break;
            }
        }

        return hBottomInvalidHeight;
    }

    public static boolean GetIsTablet(Context pContext)
    {
        Resources pResources = pContext.getResources();
        Configuration pConfiguration = pResources.getConfiguration();
        return (pConfiguration.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void GetDisplayMetricsAreaSize_Version17(Activity pActivity, int[] hSize)
    {
        DisplayMetrics pDisplayMetrics = new DisplayMetrics();
        WindowManager pWindowManager = pActivity.getWindowManager();
        Display pDisplay = pWindowManager.getDefaultDisplay();
        pDisplay.getRealMetrics(pDisplayMetrics);

        hSize[0] = 0;
        hSize[1] = 0;
        hSize[2] = pDisplayMetrics. widthPixels;
        hSize[3] = pDisplayMetrics.heightPixels;
    }

    @SuppressWarnings({"UnusedAssignment", "JavaReflectionMemberAccess"})
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public static void GetDisplayMetricsAreaSize_Version14(Activity pActivity, int[] hSize)
    {
        WindowManager pWindowManager = pActivity.getWindowManager();
        Display pDisplay = pWindowManager.getDefaultDisplay();

        int hPixelsW = 0;
        int hPixelsH = 0;

        try
        {
            Class<?> cls = Display.class;
            Method method1 = cls.getMethod("getRawWidth" );
            Method method2 = cls.getMethod("getRawHeight" );
            Object hPixelsWObj = method1.invoke(pDisplay);
            Object hPixelsHObj = method2.invoke(pDisplay);
            hPixelsW = (null == hPixelsWObj) ? 0 : (Integer) hPixelsWObj;
            hPixelsH = (null == hPixelsHObj) ? 0 : (Integer) hPixelsHObj;
        }
        catch (Exception e)
        {
            DisplayMetrics pDisplayMetrics = new DisplayMetrics();
            pDisplay.getMetrics(pDisplayMetrics);
            hPixelsW = pDisplayMetrics. widthPixels;
            hPixelsH = pDisplayMetrics.heightPixels;
        }

        hSize[0] = 0;
        hSize[1] = 0;
        hSize[2] = hPixelsW;
        hSize[3] = hPixelsH;
    }

    public static void GetDisplayMetricsAreaSize_VersionFallback(Activity pActivity, int[] hSize)
    {
        WindowManager pWindowManager = pActivity.getWindowManager();
        Display pDisplay = pWindowManager.getDefaultDisplay();
        DisplayMetrics pDisplayMetrics = new DisplayMetrics();
        pDisplay.getMetrics(pDisplayMetrics);

        hSize[0] = 0;
        hSize[1] = 0;
        hSize[2] = pDisplayMetrics. widthPixels;
        hSize[3] = pDisplayMetrics.heightPixels;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean GetIsHasSoftNavigationBar_Version17(Activity pActivity)
    {
        WindowManager pWindowManager = pActivity.getWindowManager();
        Display pDisplay = pWindowManager.getDefaultDisplay();

        DisplayMetrics hRealMetrics = new DisplayMetrics();
        DisplayMetrics hShowMetrics = new DisplayMetrics();
        pDisplay.getRealMetrics(hRealMetrics);
        pDisplay.getMetrics(hShowMetrics);

        int hRealH = hRealMetrics.heightPixels;
        int hRealW = hRealMetrics.widthPixels;

        int hShowH = hShowMetrics.heightPixels;
        int hShowW = hShowMetrics.widthPixels;

        return (hRealH > hShowH) || (hRealW > hShowW);
    }

    public static boolean GetIsHasSoftNavigationBar_VersionFallback(Activity pActivity)
    {
        ViewConfiguration pViewConfiguration = ViewConfiguration.get(pActivity);
        boolean hHasMenuKey = pViewConfiguration.hasPermanentMenuKey();
        boolean hHasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        return !hHasMenuKey && !hHasBackKey;
    }
}