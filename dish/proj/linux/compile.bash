#!/bin/sh

make -f makefile_calendar_shared $@;
make -f makefile_calendar_static $@;

make -f makefile_dish_shared $@;
make -f makefile_dish_static $@;

make -f makefile_entity_shared $@;
make -f makefile_entity_static $@;

make -f makefile_raptorq_shared $@;
make -f makefile_raptorq_static $@;

make -f makefile_fix32_shared $@;
make -f makefile_fix32_static $@;

make -f makefile_track_shared $@;
make -f makefile_track_static $@;

make -f makefile_rq_shared $@;
make -f makefile_rq_static $@;

