LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make
MM_PLATFORM  ?= android

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libmm_calendar_static.mk
include $(LOCAL_PATH)/libmm_calendar_shared.mk

include $(LOCAL_PATH)/libmm_sxwnl_static.mk
include $(LOCAL_PATH)/libmm_sxwnl_shared.mk

include $(LOCAL_PATH)/libmm_dish_static.mk
include $(LOCAL_PATH)/libmm_dish_shared.mk

include $(LOCAL_PATH)/libmm_entity_static.mk
include $(LOCAL_PATH)/libmm_entity_shared.mk

include $(LOCAL_PATH)/libmm_raptorq_static.mk
include $(LOCAL_PATH)/libmm_raptorq_shared.mk

include $(LOCAL_PATH)/libmm_fix32_static.mk
include $(LOCAL_PATH)/libmm_fix32_shared.mk

include $(LOCAL_PATH)/libmm_track_static.mk
include $(LOCAL_PATH)/libmm_track_shared.mk

include $(LOCAL_PATH)/libmm_rq_static.mk
include $(LOCAL_PATH)/libmm_rq_shared.mk

include $(LOCAL_PATH)/libmm_unicode_static.mk
include $(LOCAL_PATH)/libmm_unicode_shared.mk
