APP_ABI := arm64-v8a armeabi-v7a

APP_STL := c++_shared

APP_PLATFORM := android-16

APP_MODULES += libmm_calendar_static
APP_MODULES += libmm_calendar_shared

APP_MODULES += libmm_sxwnl_static
APP_MODULES += libmm_sxwnl_shared

APP_MODULES += libmm_dish_static
APP_MODULES += libmm_dish_shared

APP_MODULES += libmm_entity_static
APP_MODULES += libmm_entity_shared

APP_MODULES += libmm_raptorq_static
APP_MODULES += libmm_raptorq_shared

APP_MODULES += libmm_fix32_static
APP_MODULES += libmm_fix32_shared

APP_MODULES += libmm_track_static
APP_MODULES += libmm_track_shared

APP_MODULES += libmm_rq_static
APP_MODULES += libmm_rq_shared

APP_MODULES += libmm_unicode_static
APP_MODULES += libmm_unicode_shared
