/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqI.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

static void __static_mmRqI_AssemblyIdx(struct mmRqI* p, void* idx, struct mmRqPacket* packet, struct mmRqElem* e);

MM_EXPORT_RQ void mmRqI_Init(struct mmRqI* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmRqCallback_Init(&p->callback);

    mmPoolElement_Init(&p->pool_element);
    mmVectorVpt_Init(&p->vector_vpt);
    p->length = 0;

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->sampling_rate = 1;
    p->frame_rate = 1;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->overhead_decode = MM_RQ_I_OVERHEAD_DECODE_DEFAULT;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;

    p->u = NULL;

    hAllocator.Produce = &mmRqElem_Init;
    hAllocator.Recycle = &mmRqElem_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmRqElem));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);

    mmRqI_SetChunkSize(p, MM_RQ_I_CHUNK_SIZE_DEFAULT);
    mmRqI_SetLength(p, MM_RQ_I_LENGTH_DEFAULT);
}
MM_EXPORT_RQ void mmRqI_Destroy(struct mmRqI* p)
{
    mmRqI_Clear(p);
    //
    mmRqCallback_Destroy(&p->callback);

    mmPoolElement_Destroy(&p->pool_element);
    mmVectorVpt_Destroy(&p->vector_vpt);
    p->length = 0;

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->sampling_rate = 1;
    p->frame_rate = 1;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->overhead_decode = MM_RQ_I_OVERHEAD_DECODE_DEFAULT;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;

    p->u = NULL;
}
MM_EXPORT_RQ void mmRqI_Reset(struct mmRqI* p)
{
    // reset not need clear.
    //
    mmRqCallback_Reset(&p->callback);

    // reset not need handle pool_element.
    // reset not need handle vector_vpt.

    p->length = 0;

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->sampling_rate = 1;
    p->frame_rate = 1;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->overhead_decode = MM_RQ_I_OVERHEAD_DECODE_DEFAULT;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;

    p->u = NULL;
}
MM_EXPORT_RQ void mmRqI_ResetContext(struct mmRqI* p)
{
    // reset context not need reset p->length.

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;
}
MM_EXPORT_RQ void mmRqI_SetRate(struct mmRqI* p, mmUInt32_t sampling_rate, mmUInt32_t frame_rate)
{
    p->sampling_rate = sampling_rate;
    p->frame_rate = frame_rate;
    p->timestamp_interval = p->sampling_rate / p->frame_rate;
}
MM_EXPORT_RQ void mmRqI_SetTimestampInitial(struct mmRqI* p, mmUInt32_t timestamp_initial)
{
    p->timestamp_initial = timestamp_initial;
}
MM_EXPORT_RQ void mmRqI_SetLength(struct mmRqI* p, mmUInt32_t length)
{
    if (length != p->length)
    {
        p->length = length;
        mmVectorVpt_Resize(&p->vector_vpt, p->length);
    }
}
MM_EXPORT_RQ void mmRqI_SetChunkSize(struct mmRqI* p, mmUInt32_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool_element, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_RQ void mmRqI_SetOverhead(struct mmRqI* p, mmUInt32_t overhead)
{
    p->overhead_decode = overhead;
}
MM_EXPORT_RQ void mmRqI_SetCallback(struct mmRqI* p, struct mmRqCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = *callback;
}
MM_EXPORT_RQ void mmRqI_SetContext(struct mmRqI* p, void* u)
{
    p->u = u;
}

MM_EXPORT_RQ void mmRqI_Clear(struct mmRqI* p)
{
    mmPoolElement_ClearChunk(&p->pool_element);
    mmVectorVpt_Clear(&p->vector_vpt);
}
MM_EXPORT_RQ void mmRqI_AssemblySourced(struct mmRqI* p, void* idx, struct mmRqPacket* packet)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t timestamp = (mmUInt32_t)packet->phead.index_time;
    mmUInt32_t id = (timestamp - p->timestamp_initial) / p->timestamp_interval;
    mmUInt32_t n = id - p->scroll_index;

    p->packet_number++;

    if (n >= p->length)
    {
        // the package is out of range.
        p->discard++;
    }
    else
    {
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, n);
        if (NULL == nn)
        {
            mmUInt32_t index = n + 1;

            // pool_element produce.
            mmPoolElement_Lock(&p->pool_element);
            nn = (struct mmRqElem*)mmPoolElement_Produce(&p->pool_element);
            mmPoolElement_Unlock(&p->pool_element);
            // note: 
            // pool_element manager init and destroy.
            // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
            mmRqElem_Reset(nn);
            //
            mmVectorVpt_SetIndex(&p->vector_vpt, n, nn);
            p->max_index = p->max_index > index ? p->max_index : index;
            //
            mmRqElem_SetId(nn, id);
            mmRqElem_SetOverhead(nn, p->overhead_decode);
            mmRqElem_SetDecoder(nn, packet->phead.oti_common, packet->phead.oti_scheme);
        }

        assert(NULL != nn && "system memory is not enough.");

        __static_mmRqI_AssemblyIdx(p, idx, packet, nn);

        if (RQ_ELEM_STATE_FINISH == nn->state)
        {
            mmRqI_FeedbackSourced(p, idx);
        }
    }
}
MM_EXPORT_RQ void mmRqI_FeedbackSourced(struct mmRqI* p, void* idx)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t i = 0;
    mmUInt32_t d = p->finish_index - p->scroll_index;
    for (i = d; i < p->length; i++)
    {
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, i);
        if (NULL != nn && RQ_ELEM_STATE_FINISH == nn->state)
        {
            // fire event.
            (*(p->callback.Finish))(p, p->u, idx, nn);
        }
        else
        {
            break;
        }
    }
    if (0 != i)
    {
        // move
        p->finish_index += (i - d);
    }
}
MM_EXPORT_RQ void mmRqI_AssemblyPerfect(struct mmRqI* p, void* idx, mmUInt32_t id)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t n = id - p->scroll_index;

    do
    {
        if (n >= p->length)
        {
            // the package is out of range.
            break;
        }
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, n);
        if (NULL == nn)
        {
            // the package is invalid.
            break;
        }
        mmRqElem_SetState(nn, RQ_ELEM_STATE_SCROLL);
    } while (0);
}
MM_EXPORT_RQ void mmRqI_FeedbackPerfect(struct mmRqI* p, void* idx)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t i = 0;
    for (i = 0; i < p->length; i++)
    {
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, i);
        if (NULL != nn && RQ_ELEM_STATE_SCROLL == nn->state)
        {
            // fire event.
            (*(p->callback.Scroll))(p, p->u, idx, nn);
            // NULL for i.
            mmVectorVpt_SetIndex(&p->vector_vpt, i, NULL);
            // note: 
            // pool_element manager init and destroy.
            // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
            mmRqElem_Reset(nn);
            // pool_element recycle.
            mmPoolElement_Lock(&p->pool_element);
            mmPoolElement_Recycle(&p->pool_element, nn);
            mmPoolElement_Unlock(&p->pool_element);
        }
        else
        {
            break;
        }
    }
    if (0 != i)
    {
        // move
        p->max_index -= i;
        mmMemcpy(p->vector_vpt.arrays, p->vector_vpt.arrays + i, sizeof(struct mm_rq_o_elem*) * (p->max_index));
        mmMemset(p->vector_vpt.arrays + p->max_index, 0, sizeof(struct mm_rq_o_elem*) * (i));
        p->scroll_index += i;
    }
}

static void __static_mmRqI_AssemblyIdx(struct mmRqI* p, void* idx, struct mmRqPacket* packet, struct mmRqElem* e)
{
    mmUInt8_t sbn = 0;
    mmUInt32_t esi = 0;

    mmUInt32_t payload_id = packet->phead.payload_id;

    mmRaptorqContext_AssemblyPayloadId(payload_id, &sbn, &esi);

    do
    {
        if (RQ_ELEM_STATE_FINISH == e->state)
        {
            p->duplicate++;
            break;
        }
        if (MM_FALSE == mmRaptorqContext_AssemblySymbolValuable(&e->rq, esi, sbn))
        {
            // not valuable packet.
            p->duplicate++;
            break;
        }
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            int success = MM_FALSE;

            mmUInt16_t symbol_size = 0;
            mmUInt8_t* data = NULL;

            data = (mmUInt8_t*)(packet->bbuff.buffer + packet->bbuff.offset);

            success = mmRaptorqContext_DecoderAssemblySymbolsBuffer(&e->rq, (mmUInt8_t*)data, esi, sbn, &symbol_size);
            if (MM_TRUE != success)
            {
                mmLogger_LogE(gLogger, "%s %d adding symbol payload_id %d failed.", __FUNCTION__, __LINE__, payload_id);
                break;
            }

            // fire event.
            (*(p->callback.Handle))(p, p->u, idx, e, packet);

            mmRqElem_IncreaseEsiNumber(e, sbn);

            success = mmRaptorqContext_AssemblySourceComplete(&e->rq, e->overhead);
            if (MM_TRUE != success)
            {
                // assembly source not complete.
                break;
            }
        }
        {
            struct mmStreambuf* streambuf = &e->streambuf_buffer;
            int success = MM_FALSE;

            // assembly source complete.
            struct mmByteBuffer byte_buffer;

            mmUInt64_t transfer_length = mmRaptorqContext_TransferLength(&e->rq);

            mmStreambuf_Reset(streambuf);
            mmStreambuf_AlignedMemory(streambuf, (size_t)transfer_length);
            mmStreambuf_Pbump(streambuf, (size_t)transfer_length);

            byte_buffer.buffer = streambuf->buff;
            byte_buffer.offset = streambuf->gptr;
            byte_buffer.length = streambuf->pptr - streambuf->gptr;

            success = mmRqElem_ContextDecode(e, &byte_buffer);
            if (MM_TRUE != success)
            {
                // assembly source complete decode failed.
                e->overhead++;
                break;
            }
        }
        {
            // assembly source complete decode finish.
            mmRqElem_SetState(e, RQ_ELEM_STATE_FINISH);
        }
    } while (0);
}

