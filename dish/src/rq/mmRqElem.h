/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqElem_h__
#define __mmRqElem_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"

#include "container/mmBitset.h"
#include "container/mmRbtreeU32.h"

#include "raptorq/mmRaptorqContext.h"

#include "mmRqPacket.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

enum mmRqElemState_t
{
    RQ_ELEM_STATE_HANDLE = 0,
    RQ_ELEM_STATE_FINISH = 1,
    RQ_ELEM_STATE_SCROLL = 2,
};

struct mmRqElem
{
    struct mmRaptorqContext rq;
    struct mmStreambuf streambuf_buffer;
    struct mmStreambuf streambuf_packet;
    struct mmRbtreeU32U32 esi_num;
    mmUInt32_t id;
    mmUInt32_t overhead;
    mmUInt32_t state;
};

MM_EXPORT_RQ void mmRqElem_Init(struct mmRqElem* p);
MM_EXPORT_RQ void mmRqElem_Destroy(struct mmRqElem* p);
MM_EXPORT_RQ void mmRqElem_Reset(struct mmRqElem* p);
MM_EXPORT_RQ void mmRqElem_SetId(struct mmRqElem* p, mmUInt32_t id);
MM_EXPORT_RQ void mmRqElem_SetOverhead(struct mmRqElem* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqElem_SetState(struct mmRqElem* p, mmUInt32_t state);
MM_EXPORT_RQ void mmRqElem_SetDecoder(struct mmRqElem* p, mmUInt64_t oti_common, mmUInt32_t oti_scheme);
MM_EXPORT_RQ void mmRqElem_SetEncoder(struct mmRqElem* p, mmUInt64_t F, mmUInt16_t T, mmUInt16_t SS, mmUInt8_t Al, size_t WS);
MM_EXPORT_RQ int mmRqElem_ContextDecode(struct mmRqElem* p, struct mmByteBuffer* byte_buffer);
MM_EXPORT_RQ int mmRqElem_ContextEncode(struct mmRqElem* p, struct mmByteBuffer* byte_buffer);

MM_EXPORT_RQ void mmRqElem_SetEsiNumber(struct mmRqElem* p, mmUInt8_t sbn, mmUInt32_t esi_number);
MM_EXPORT_RQ mmUInt32_t mmRqElem_GetEsiNumber(struct mmRqElem* p, mmUInt8_t sbn);
MM_EXPORT_RQ void mmRqElem_ClearEsiNumber(struct mmRqElem* p);
MM_EXPORT_RQ void mmRqElem_IncreaseEsiNumber(struct mmRqElem* p, mmUInt8_t sbn);
MM_EXPORT_RQ void mmRqElem_DecreaseEsiNumber(struct mmRqElem* p, mmUInt8_t sbn);

#include "core/mmSuffix.h"

#endif//__mmRqElem_h__
