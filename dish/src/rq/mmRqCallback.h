/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqCallback_h__
#define __mmRqCallback_h__

#include "core/mmCore.h"

#include "mmRqPacket.h"

#include "mmRqElem.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

// obj is fire event target.
// u   is fire event target user weak data.
// e   function input idx weak data.
struct mmRqCallback
{
    void(*Handle)(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet);
    void(*Finish)(void* obj, void* u, void* idx, struct mmRqElem* e);
    void(*Scroll)(void* obj, void* u, void* idx, struct mmRqElem* e);
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_RQ void mmRqCallback_Init(struct mmRqCallback* p);
MM_EXPORT_RQ void mmRqCallback_Destroy(struct mmRqCallback* p);
MM_EXPORT_RQ void mmRqCallback_Reset(struct mmRqCallback* p);

#include "core/mmSuffix.h"

#endif//__mmRqCallback_h__
