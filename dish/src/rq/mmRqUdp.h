/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqUdp_h__
#define __mmRqUdp_h__

#include "core/mmCore.h"

#include "mmRqFountain.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

struct mmRqUdpMulcast
{
    mmAtomic_t locker;

    // microseconds. rq_o packet number update timecode.
    mmUInt64_t timecode_o_packet_number_update;
    // microseconds. rq_o packet indexp update timecode.
    mmUInt64_t timecode_o_packet_indexp_update;

    mmUInt64_t o_packet_number;
    mmUInt32_t o_packet_indexp;

    float expected_loss;
};
MM_EXPORT_RQ void mmRqUdpMulcast_Init(struct mmRqUdpMulcast* p);
MM_EXPORT_RQ void mmRqUdpMulcast_Destroy(struct mmRqUdpMulcast* p);

MM_EXPORT_RQ void mmRqUdpMulcast_Reset(struct mmRqUdpMulcast* p);

/* locker order is
*     locker
*/
MM_EXPORT_RQ void mmRqUdpMulcast_Lock(struct mmRqUdpMulcast* p);
MM_EXPORT_RQ void mmRqUdpMulcast_Unlock(struct mmRqUdpMulcast* p);

struct mmRqUdp
{
    struct mmRqFountain fountain_unicast;
    struct mmRqUdpMulcast udp_mulcast;
};

MM_EXPORT_RQ void mmRqUdp_Init(struct mmRqUdp* p);
MM_EXPORT_RQ void mmRqUdp_Destroy(struct mmRqUdp* p);
MM_EXPORT_RQ void mmRqUdp_Reset(struct mmRqUdp* p);

/* locker order is
*     fountain_unicast
*     udp_mulcast
*/
MM_EXPORT_RQ void mmRqUdp_Lock(struct mmRqUdp* p);
MM_EXPORT_RQ void mmRqUdp_Unlock(struct mmRqUdp* p);

#include "core/mmSuffix.h"

#endif//__mmRqUdp_h__
