/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqProtocol_h__
#define __mmRqProtocol_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

//  0                   1                   2                   3
//  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |               |T|M|U|A|P|R|S|F|                               |
// |     non       |P|U|R|C|S|S|Y|I|            ext                |
// |               |Y|L|G|K|H|T|N|N|                               |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

// mid is message id.
//   | n| T| M| U| A| P| R| S| F| e|
//   | o| P| U| R| C| S| S| Y| I| x|
//   | n| Y| L| G| K| H| T| N| N| t|

//   { 0, 0, 1, 0, 0, 1, 0, 0, 0 ,0 };
// mid is message id.
// pid is identity.
// sid is none.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_Mulcast = 0x00480000;

//   { 0, 0, 0, 0, 0, 1, 0, 0, 0 ,0 };
// mid is message id.
// pid is identity.
// sid is none.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_Unicast = 0x00080000;

//   { 0, 1, 0, 0, 0, 0, 0, 1, 0 ,0 };
// mid is message id.
// pid is none.
// sid is client timecode us.
// uid is unique_id.
static const mmUInt32_t mmRqMidUInt32_SynNone = 0x00820000;

//   { 0, 1, 0, 0, 1, 0, 0, 0, 0 ,0 };
// mid is message id.
// pid is identity.
// sid is l(mmUInt32_t) mulcast recv timestamp_initial, r(mmUInt32_t) mulcast timestamp_initial.
// uid is l(mmUInt32_t) unicast recv timestamp_initial, r(mmUInt32_t) unicast timestamp_initial.
static const mmUInt32_t mmRqMidUInt32_AckNone = 0x00900000;

//   { 0, 1, 0, 0, 0, 0, 0, 0, 1 ,0 };
// mid is message id.
// pid is identity.
// sid is none.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_FinNone = 0x00810000;

//   { 0, 1, 0, 0, 0, 0, 1, 0, 0 ,0 };
// mid is message id.
// pid is identity.
// sid is none.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_RstNone = 0x00840000;

//   { 0, 1, 0, 0, 1, 0, 0, 1, 0 ,0 };
// mid is message id.
// pid is identity.
// sid is none.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_SyncAck = 0x00920000;

//   { 0, 1, 0, 0, 0, 0, 0, 0, 0 ,0x01 };
// mid is message id.
// pid is identity.
// sid is client timecode us.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_PingReq = 0x00800001;

//   { 0, 1, 0, 0, 0, 0, 0, 0, 0 ,0x02 };
// mid is message id.
// pid is identity.
// sid is client timecode us.
// uid is none.
static const mmUInt32_t mmRqMidUInt32_PingRes = 0x00800002;

//   { 0, 1, 0, 0, 0, 0, 0, 0, 0 ,0x80 };
// mid is message id.
// pid is identity.
// sid is l(mmUInt32_t) mulcast recv message packet number, r(mmUInt32_t) unicast recv message packet number.
// uid is timecode us.
static const mmUInt32_t mmRqMidUInt32_LossUpd = 0x00800080;

//   { 0, 1, 0, 0, 0, 0, 0, 0, 0 ,0x81 };
// mid is message id.
// pid is identity.
// sid is l(mmUInt32_t) mulcast feedback perfect index, r(mmUInt32_t) unicast feedback perfect index.
// uid is timecode us.
static const mmUInt32_t mmRqMidUInt32_IdxsUpd = 0x00800081;

enum mmRqNetState_t
{
    MM_RQ_CLOSED      =  0,
    MM_RQ_LISTEN      =  1,
    MM_RQ_SYN_SEND    =  2,
    MM_RQ_SYN_RECV    =  3,
    MM_RQ_ESTABLISHED =  4,
    MM_RQ_FIN_WAIT_1  =  5,
    MM_RQ_FIN_WAIT_2  =  6,
    MM_RQ_CLOSE_WAIT  =  7,
    MM_RQ_CLOSING     =  8,
    MM_RQ_TIME_WAIT   =  9,
    MM_RQ_LAST_ACK    = 10,
};

// server mmRqMidUInt32_Mulcast mmRqMidUInt32_Unicast 
// mid is message id.
// pid is none.
// sid is none.
// uid is none.
#define MM_RQ_SERVER_COMM_HEAD_SIZE 4

// client mmRqMidUInt32_Mulcast mmRqMidUInt32_Unicast 
// mid is message id.
// pid is identity.
// sid is none.
// uid is none.
#define MM_RQ_CLIENT_COMM_HEAD_SIZE 8

// rq status udp internal mid.
enum mmRqUdpMid_t
{
    MM_RQ_UDP_MID_BROKEN = 0x0F001000,
    MM_RQ_UDP_MID_NREADY = 0x0F001001,
    MM_RQ_UDP_MID_FINISH = 0x0F001002,
};

#include "core/mmSuffix.h"

#endif//__mmRqProtocol_h__
