/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqUdp.h"

MM_EXPORT_RQ void mmRqUdpMulcast_Init(struct mmRqUdpMulcast* p)
{
    mmSpinlock_Init(&p->locker, NULL);
    p->timecode_o_packet_number_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->o_packet_number = 0;
    p->o_packet_indexp = 0;
    p->expected_loss = 0;
}
MM_EXPORT_RQ void mmRqUdpMulcast_Destroy(struct mmRqUdpMulcast* p)
{
    mmSpinlock_Destroy(&p->locker);
    p->timecode_o_packet_number_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->o_packet_number = 0;
    p->o_packet_indexp = 0;
    p->expected_loss = 0;
}
MM_EXPORT_RQ void mmRqUdpMulcast_Reset(struct mmRqUdpMulcast* p)
{
    // spinlock not need reset.
    p->timecode_o_packet_number_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->o_packet_number = 0;
    p->o_packet_indexp = 0;
    p->expected_loss = 0;
}
MM_EXPORT_RQ void mmRqUdpMulcast_Lock(struct mmRqUdpMulcast* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_RQ void mmRqUdpMulcast_Unlock(struct mmRqUdpMulcast* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_RQ void mmRqUdp_Init(struct mmRqUdp* p)
{
    mmRqFountain_Init(&p->fountain_unicast);
    mmRqUdpMulcast_Init(&p->udp_mulcast);
}
MM_EXPORT_RQ void mmRqUdp_Destroy(struct mmRqUdp* p)
{
    mmRqFountain_Destroy(&p->fountain_unicast);
    mmRqUdpMulcast_Destroy(&p->udp_mulcast);
}
MM_EXPORT_RQ void mmRqUdp_Reset(struct mmRqUdp* p)
{
    mmRqFountain_Reset(&p->fountain_unicast);
    mmRqUdpMulcast_Reset(&p->udp_mulcast);
}

MM_EXPORT_RQ void mmRqUdp_Lock(struct mmRqUdp* p)
{
    mmRqFountain_Lock(&p->fountain_unicast);
    mmRqUdpMulcast_Lock(&p->udp_mulcast);
}
MM_EXPORT_RQ void mmRqUdp_Unlock(struct mmRqUdp* p)
{
    mmRqUdpMulcast_Unlock(&p->udp_mulcast);
    mmRqFountain_Unlock(&p->fountain_unicast);
}

