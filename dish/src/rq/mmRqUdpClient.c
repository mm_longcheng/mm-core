/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqUdpClient.h"
#include "mmRqProtocol.h"

#include "core/mmLogger.h"

#include "net/mmDefaultHandle.h"
#include "net/mmStreambufPacket.h"
#include "net/mmMessageCoder.h"

struct __mmRqUdpClientPacketIdxArgument
{
    struct mmRqUdpClient* rq_udp_client;
    struct mmRqFountain* fountain;
    struct mmUdp* udp;
};

static void __static_mmRqUdpClient_NetUdpRqBrokenEventUnicast(void* obj);
static void __static_mmRqUdpClient_NetUdpRqNreadyEventUnicast(void* obj);
static void __static_mmRqUdpClient_NetUdpRqFinishEventUnicast(void* obj);

static void __static_mmRqUdpClient_NetUdpRqBrokenEventMulcast(void* obj);
static void __static_mmRqUdpClient_NetUdpRqNreadyEventMulcast(void* obj);
static void __static_mmRqUdpClient_NetUdpRqFinishEventMulcast(void* obj);

static void __static_mmRqUdpClient_NetUdpNtBrokenEventUnicast(void* obj);
static void __static_mmRqUdpClient_NetUdpNtNreadyEventUnicast(void* obj);
static void __static_mmRqUdpClient_NetUdpNtFinishEventUnicast(void* obj);

static void __static_mmRqUdpClient_NetUdpNtBrokenEventMulcast(void* obj);
static void __static_mmRqUdpClient_NetUdpNtNreadyEventMulcast(void* obj);
static void __static_mmRqUdpClient_NetUdpNtFinishEventMulcast(void* obj);

static void __static_mmRqUdpClient_BrokenNNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_NreadyNNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_FinishNNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpClient_BrokenNNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_NreadyNNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_FinishNNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpClient_UnicastNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_MulcastNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_SynNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_AckNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_FinNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_RstNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_SyncAckNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpClient_PingReqNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_PingResNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_LossUpdNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_IdxsUpdNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpClient_UnicastICallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet);
static void __static_mmRqUdpClient_UnicastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpClient_UnicastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpClient_UnicastOCallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet);
static void __static_mmRqUdpClient_UnicastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpClient_UnicastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);

static void __static_mmRqUdpClient_MulcastICallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet);
static void __static_mmRqUdpClient_MulcastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpClient_MulcastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpClient_MulcastOCallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet);
static void __static_mmRqUdpClient_MulcastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpClient_MulcastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);

static void __static_mmRqUdpClient_UnicastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_MulcastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpClient_UnicastQueueRecvPacketHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_MulcastQueueRecvPacketHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpClient_NNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpClient_NNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void
__static_mmRqUdpClient_SendRepeatedMassageUnicast(
    struct mmRqUdpClient* p,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmByteBuffer* buffer,
    struct mmSockaddr* remote);

static void __static_mmRqUdpClient_EmptyHandleForCommonEvent(struct mmRqUdpClient* p);
static void __static_mmRqUdpClient_ValidHandleForCommonEvent(struct mmClientUdp* client_udp);

static void __static_mmRqUdpClient_MsecUpdatePingStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
static void __static_mmRqUdpClient_MsecUpdateLossStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
static void __static_mmRqUdpClient_MsecUpdateIdxsStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);

static void __static_mmRqUdpClient_MsecUpdateMulcastIPersistenceHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);

static void __static_mmRqUdpClient_MulcastIPersistenceHandle(struct mmRqUdpClient* p);

static void __static_mmRqUdpClient_Handle(void* obj, void* u, mmUInt32_t identity, struct mmPacket* pack, struct mmSockaddr* remote)
{

}

MM_EXPORT_RQ void mmRqUdpClientCallback_Init(struct mmRqUdpClientCallback* p)
{
    p->Handle = &__static_mmRqUdpClient_Handle;
    p->obj = NULL;
}
MM_EXPORT_RQ void mmRqUdpClientCallback_Destroy(struct mmRqUdpClientCallback* p)
{
    p->Handle = &__static_mmRqUdpClient_Handle;
    p->obj = NULL;
}

MM_EXPORT_RQ void
mmRqUdpClient_Handledefault(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSockaddr_NativeRemoteString(remote, &udp->socket.ss_native, udp->socket.socket, link_name);
    
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X udp:%s not handle.",
        __FUNCTION__,
        __LINE__,
        pack->phead.mid,
        link_name);
}

MM_EXPORT_RQ void mmRqUdpClient_Init(struct mmRqUdpClient* p)
{
    struct mmClientUdpCallback client_udp_n_unicast_callback;
    struct mmClientUdpCallback client_udp_n_mulcast_callback;
    struct mmPacketQueueCallback queue_recv_tcp_unicast_callback;
    struct mmPacketQueueCallback queue_recv_tcp_mulcast_callback;
    struct mmRqCallback rq_unicast_i_callback;
    struct mmRqCallback rq_unicast_o_callback;
    struct mmRqCallback rq_mulcast_i_callback;
    struct mmRqCallback rq_mulcast_o_callback;

    mmClientUdp_Init(&p->client_udp_unicast);
    mmClientUdp_Init(&p->client_udp_mulcast);

    mmRqFountain_Init(&p->fountain_unicast);
    mmRqFountain_Init(&p->fountain_mulcast);

    mmRqDatabase_Init(&p->database_mulcast_i);

    mmTimer_Init(&p->timer);

    mmRbtreeU32Vpt_Init(&p->rbtree_unicast_q);
    mmRbtreeU32Vpt_Init(&p->rbtree_unicast_n);
    mmRbtreeU32Vpt_Init(&p->rbtree_mulcast_q);
    mmRbtreeU32Vpt_Init(&p->rbtree_mulcast_n);

    mmRqUdpClientCallback_Init(&p->callback_unicast_q);
    mmRqUdpClientCallback_Init(&p->callback_unicast_n);
    mmRqUdpClientCallback_Init(&p->callback_mulcast_q);
    mmRqUdpClientCallback_Init(&p->callback_mulcast_n);

    mmString_Init(&p->mr_multiaddr);
    mmString_Init(&p->mr_interface);

    mmSpinlock_Init(&p->rbtree_locker_unicast_q, NULL);
    mmSpinlock_Init(&p->rbtree_locker_unicast_n, NULL);
    mmSpinlock_Init(&p->rbtree_locker_mulcast_q, NULL);
    mmSpinlock_Init(&p->rbtree_locker_mulcast_n, NULL);

    mmSpinlock_Init(&p->locker, NULL);

    p->msec_update_ping_state = MM_RQ_UDP_CLIENT_MSEC_UPDATE_PING_STATE;
    p->msec_update_loss_state = MM_RQ_UDP_CLIENT_MSEC_UPDATE_LOSS_STATE;
    p->msec_update_idxp_state = MM_RQ_UDP_CLIENT_MSEC_UPDATE_IDXP_STATE;

    p->msec_update_mulcast_i_persistence = MM_RQ_UDP_CLIENT_MSEC_UPDATE_MULCAST_I_PERSISTENCE;

    p->repeated_number = MM_RQ_UDP_CLIENT_COMMON_MESSAGE_REPEATED_NUMBER;

    p->mulcast_max_index = 0;

    p->unicast_timecode_rtt = 0;
    p->unicast_packet_loss = 0;
    p->unicast_expected_loss = 0;

    p->u = NULL;

    queue_recv_tcp_unicast_callback.Handle = &__static_mmRqUdpClient_UnicastQueueRecvPacketHandle;
    queue_recv_tcp_unicast_callback.obj = &p->client_udp_unicast;
    mmPacketQueue_SetQueueCallback(&p->client_udp_unicast.queue_recv, &queue_recv_tcp_unicast_callback);

    queue_recv_tcp_mulcast_callback.Handle = &__static_mmRqUdpClient_MulcastQueueRecvPacketHandle;
    queue_recv_tcp_mulcast_callback.obj = &p->client_udp_mulcast;
    mmPacketQueue_SetQueueCallback(&p->client_udp_mulcast.queue_recv, &queue_recv_tcp_mulcast_callback);

    client_udp_n_unicast_callback.Handle = &__static_mmRqUdpClient_NNetUdpHandleUnicast;
    client_udp_n_unicast_callback.obj = p;
    mmClientUdp_SetNDefaultCallback(&p->client_udp_unicast, &client_udp_n_unicast_callback);

    client_udp_n_mulcast_callback.Handle = &__static_mmRqUdpClient_NNetUdpHandleMulcast;
    client_udp_n_mulcast_callback.obj = p;
    mmClientUdp_SetNDefaultCallback(&p->client_udp_mulcast, &client_udp_n_mulcast_callback);

    mmClientUdp_SetNHandle(&p->client_udp_unicast, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_BrokenNNetUdpHandleUnicast);
    mmClientUdp_SetNHandle(&p->client_udp_unicast, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_NreadyNNetUdpHandleUnicast);
    mmClientUdp_SetNHandle(&p->client_udp_unicast, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_FinishNNetUdpHandleUnicast);

    mmClientUdp_SetNHandle(&p->client_udp_mulcast, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_BrokenNNetUdpHandleMulcast);
    mmClientUdp_SetNHandle(&p->client_udp_mulcast, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_NreadyNNetUdpHandleMulcast);
    mmClientUdp_SetNHandle(&p->client_udp_mulcast, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_FinishNNetUdpHandleMulcast);

    __static_mmRqUdpClient_ValidHandleForCommonEvent(&p->client_udp_unicast);
    __static_mmRqUdpClient_ValidHandleForCommonEvent(&p->client_udp_mulcast);

    mmRqFountain_SetOverheadEncode(&p->fountain_unicast, MM_RQ_UDP_CLIENT_O_OVERHEAD_ENCODE_DEFAULT);
    mmRqFountain_SetOverheadDecode(&p->fountain_unicast, MM_RQ_UDP_CLIENT_I_OVERHEAD_DECODE_DEFAULT);

    mmRqFountain_SetOverheadEncode(&p->fountain_mulcast, MM_RQ_UDP_CLIENT_O_OVERHEAD_ENCODE_DEFAULT);
    mmRqFountain_SetOverheadDecode(&p->fountain_mulcast, MM_RQ_UDP_CLIENT_I_OVERHEAD_DECODE_DEFAULT);

    rq_unicast_i_callback.Handle = &__static_mmRqUdpClient_UnicastICallbackHandle;
    rq_unicast_i_callback.Finish = &__static_mmRqUdpClient_UnicastICallbackFinish;
    rq_unicast_i_callback.Scroll = &__static_mmRqUdpClient_UnicastICallbackScroll;
    rq_unicast_i_callback.obj = p;
    mmRqI_SetCallback(&p->fountain_unicast.rq_i, &rq_unicast_i_callback);

    rq_unicast_o_callback.Handle = &__static_mmRqUdpClient_UnicastOCallbackHandle;
    rq_unicast_o_callback.Finish = &__static_mmRqUdpClient_UnicastOCallbackFinish;
    rq_unicast_o_callback.Scroll = &__static_mmRqUdpClient_UnicastOCallbackScroll;
    rq_unicast_o_callback.obj = p;
    mmRqO_SetCallback(&p->fountain_unicast.rq_o, &rq_unicast_o_callback);

    rq_mulcast_i_callback.Handle = &__static_mmRqUdpClient_MulcastICallbackHandle;
    rq_mulcast_i_callback.Finish = &__static_mmRqUdpClient_MulcastICallbackFinish;
    rq_mulcast_i_callback.Scroll = &__static_mmRqUdpClient_MulcastICallbackScroll;
    rq_mulcast_i_callback.obj = p;
    mmRqI_SetCallback(&p->fountain_mulcast.rq_i, &rq_mulcast_i_callback);

    rq_mulcast_o_callback.Handle = &__static_mmRqUdpClient_MulcastOCallbackHandle;
    rq_mulcast_o_callback.Finish = &__static_mmRqUdpClient_MulcastOCallbackFinish;
    rq_mulcast_o_callback.Scroll = &__static_mmRqUdpClient_MulcastOCallbackScroll;
    rq_mulcast_o_callback.obj = p;
    mmRqO_SetCallback(&p->fountain_mulcast.rq_o, &rq_mulcast_o_callback);

    mmRqI_SetLength(&p->fountain_mulcast.rq_i, MM_RQ_UDP_CLIENT_I_MULCAST_LENGTH_DEFAULT);
    mmRqO_SetLength(&p->fountain_mulcast.rq_o, MM_RQ_UDP_CLIENT_O_MULCAST_LENGTH_DEFAULT);

    mmString_Assigns(&p->mr_multiaddr, MM_RQ_UDP_CLIENT_MR_MULTIADDR_DEFAULT);
    mmString_Assigns(&p->mr_interface, MM_RQ_UDP_CLIENT_MR_INTERFACE_DEFAULT);

    mmTimer_Schedule(
        &p->timer, p->msec_update_ping_state,
        p->msec_update_ping_state,
        &__static_mmRqUdpClient_MsecUpdatePingStateHandle,
        p);
    
    mmTimer_Schedule(
        &p->timer,
        p->msec_update_loss_state,
        p->msec_update_loss_state,
        &__static_mmRqUdpClient_MsecUpdateLossStateHandle,
        p);
    
    mmTimer_Schedule(
        &p->timer,
        p->msec_update_idxp_state,
        p->msec_update_idxp_state,
        &__static_mmRqUdpClient_MsecUpdateIdxsStateHandle,
        p);

    mmTimer_Schedule(
        &p->timer,
        p->msec_update_mulcast_i_persistence,
        p->msec_update_mulcast_i_persistence,
        &__static_mmRqUdpClient_MsecUpdateMulcastIPersistenceHandle,
        p);

    // empty handle for common event.
    __static_mmRqUdpClient_EmptyHandleForCommonEvent(p);
}
MM_EXPORT_RQ void mmRqUdpClient_Destroy(struct mmRqUdpClient* p)
{
    mmRqUdpClient_ClearHandleRbtree(p);
    // empty handle for common event.
    __static_mmRqUdpClient_EmptyHandleForCommonEvent(p);
    //
    mmClientUdp_Destroy(&p->client_udp_unicast);
    mmClientUdp_Destroy(&p->client_udp_mulcast);

    mmRqFountain_Destroy(&p->fountain_unicast);
    mmRqFountain_Destroy(&p->fountain_mulcast);

    mmRqDatabase_Destroy(&p->database_mulcast_i);

    mmTimer_Destroy(&p->timer);

    mmRbtreeU32Vpt_Destroy(&p->rbtree_unicast_q);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_unicast_n);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_mulcast_q);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_mulcast_n);

    mmRqUdpClientCallback_Destroy(&p->callback_unicast_q);
    mmRqUdpClientCallback_Destroy(&p->callback_unicast_n);
    mmRqUdpClientCallback_Destroy(&p->callback_mulcast_q);
    mmRqUdpClientCallback_Destroy(&p->callback_mulcast_n);

    mmString_Destroy(&p->mr_multiaddr);
    mmString_Destroy(&p->mr_interface);

    mmSpinlock_Destroy(&p->rbtree_locker_unicast_q);
    mmSpinlock_Destroy(&p->rbtree_locker_unicast_n);
    mmSpinlock_Destroy(&p->rbtree_locker_mulcast_q);
    mmSpinlock_Destroy(&p->rbtree_locker_mulcast_n);

    mmSpinlock_Destroy(&p->locker);

    p->msec_update_ping_state = MM_RQ_UDP_CLIENT_MSEC_UPDATE_PING_STATE;
    p->msec_update_loss_state = MM_RQ_UDP_CLIENT_MSEC_UPDATE_LOSS_STATE;
    p->msec_update_idxp_state = MM_RQ_UDP_CLIENT_MSEC_UPDATE_IDXP_STATE;

    p->msec_update_mulcast_i_persistence = MM_RQ_UDP_CLIENT_MSEC_UPDATE_MULCAST_I_PERSISTENCE;

    p->repeated_number = MM_RQ_UDP_CLIENT_COMMON_MESSAGE_REPEATED_NUMBER;

    p->mulcast_max_index = 0;

    p->unicast_timecode_rtt = 0;
    p->unicast_packet_loss = 0;
    p->unicast_expected_loss = 0;

    p->u = NULL;
}

MM_EXPORT_RQ void mmRqUdpClient_Lock(struct mmRqUdpClient* p)
{
    mmClientUdp_Lock(&p->client_udp_unicast);
    mmClientUdp_Lock(&p->client_udp_unicast);
    mmRqFountain_Lock(&p->fountain_unicast);
    mmRqFountain_Lock(&p->fountain_mulcast);
    mmTimer_Lock(&p->timer);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_RQ void mmRqUdpClient_Unlock(struct mmRqUdpClient* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmTimer_Unlock(&p->timer);
    mmRqFountain_Unlock(&p->fountain_mulcast);
    mmRqFountain_Unlock(&p->fountain_unicast);
    mmClientUdp_Unlock(&p->client_udp_unicast);
    mmClientUdp_Unlock(&p->client_udp_unicast);
}

MM_EXPORT_RQ void mmRqUdpClient_ResetUnicastContext(struct mmRqUdpClient* p)
{
    mmRqFountain_ResetContext(&p->fountain_unicast);
}
MM_EXPORT_RQ void mmRqUdpClient_ResetMulcastContext(struct mmRqUdpClient* p)
{
    mmRqFountain_ResetContext(&p->fountain_mulcast);
}

MM_EXPORT_RQ void mmRqUdpClient_SetUnicastLength(struct mmRqUdpClient* p, mmUInt32_t length)
{
    mmRqFountain_SetLength(&p->fountain_unicast, length);
}
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastLength(struct mmRqUdpClient* p, mmUInt32_t length)
{
    mmRqFountain_SetLength(&p->fountain_mulcast, length);
}

MM_EXPORT_RQ void mmRqUdpClient_SetDatabaseMulcastIFilepath(struct mmRqUdpClient* p, const char* filepath)
{
    mmRqDatabase_SetFilepath(&p->database_mulcast_i, filepath);
}

MM_EXPORT_RQ void mmRqUdpClient_SetUnicastOverheadEncode(struct mmRqUdpClient* p, mmUInt32_t overhead)
{
    mmRqFountain_SetOverheadEncode(&p->fountain_unicast, overhead);
}
MM_EXPORT_RQ void mmRqUdpClient_SetUnicastOverheadDecode(struct mmRqUdpClient* p, mmUInt32_t overhead)
{
    mmRqFountain_SetOverheadDecode(&p->fountain_unicast, overhead);
}
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastOverheadEncode(struct mmRqUdpClient* p, mmUInt32_t overhead)
{
    mmRqFountain_SetOverheadEncode(&p->fountain_mulcast, overhead);
}
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastOverheadDecode(struct mmRqUdpClient* p, mmUInt32_t overhead)
{
    mmRqFountain_SetOverheadDecode(&p->fountain_mulcast, overhead);
}

MM_EXPORT_RQ void mmRqUdpClient_SetContext(struct mmRqUdpClient* p, void* u)
{
    p->u = u;
    mmClientUdp_SetContext(&p->client_udp_unicast, p->u);
    mmClientUdp_SetContext(&p->client_udp_mulcast, p->u);
}
MM_EXPORT_RQ void mmRqUdpClient_SetRepeatedNumber(struct mmRqUdpClient* p, mmUInt32_t repeated_number)
{
    p->repeated_number = repeated_number;
}
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastMrInterface(struct mmRqUdpClient* p, const char* mr_interface)
{
    mmString_Assigns(&p->mr_interface, mr_interface);
}
MM_EXPORT_RQ void mmRqUdpClient_SetNativeUnicast(struct mmRqUdpClient* p, const char* node, mmUShort_t port)
{
    mmClientUdp_SetNative(&p->client_udp_unicast, node, port);
}
MM_EXPORT_RQ void mmRqUdpClient_SetNativeMulcast(struct mmRqUdpClient* p, const char* node, mmUShort_t port)
{
    mmClientUdp_SetNative(&p->client_udp_mulcast, node, port);
}
MM_EXPORT_RQ void mmRqUdpClient_SetRemoteUnicast(struct mmRqUdpClient* p, const char* node, mmUShort_t port)
{
    mmRqFountain_SetRemote(&p->fountain_unicast, node, port);
}
MM_EXPORT_RQ void mmRqUdpClient_SetRemoteMulcast(struct mmRqUdpClient* p, const char* node, mmUShort_t port)
{
    mmClientUdp_SetRemote(&p->client_udp_unicast, node, port);
    mmClientUdp_SetRemote(&p->client_udp_mulcast, node, port);
    mmRqFountain_SetRemote(&p->fountain_mulcast, node, port);
    mmString_Assigns(&p->mr_multiaddr, node);
}
MM_EXPORT_RQ void mmRqUdpClient_SetQDefaultCallbackUnicast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback)
{
    p->callback_unicast_q = *callback;
}
MM_EXPORT_RQ void mmRqUdpClient_SetNDefaultCallbackUnicast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback)
{
    p->callback_unicast_n = *callback;
}
MM_EXPORT_RQ void mmRqUdpClient_SetQDefaultCallbackMulcast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback)
{
    p->callback_mulcast_q = *callback;
}
MM_EXPORT_RQ void mmRqUdpClient_SetNDefaultCallbackMulcast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback)
{
    p->callback_mulcast_n = *callback;
}
// assign queue callback.at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetQHandleUnicast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker_unicast_q);
    mmRbtreeU32Vpt_Set(&p->rbtree_unicast_q, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast_q);
}
// assign net callback.not at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetNHandleUnicast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker_unicast_n);
    mmRbtreeU32Vpt_Set(&p->rbtree_unicast_n, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast_n);
}
// assign queue callback.at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetQHandleMulcast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker_mulcast_q);
    mmRbtreeU32Vpt_Set(&p->rbtree_mulcast_q, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast_q);
}
// assign net callback.not at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetNHandleMulcast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker_mulcast_n);
    mmRbtreeU32Vpt_Set(&p->rbtree_mulcast_n, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast_n);
}
MM_EXPORT_RQ void mmRqUdpClient_SetStateCheckFlag(struct mmRqUdpClient* p, mmUInt8_t flag)
{
    mmClientUdp_SetStateCheckFlag(&p->client_udp_unicast, flag);
    mmClientUdp_SetStateCheckFlag(&p->client_udp_mulcast, flag);
}
MM_EXPORT_RQ void mmRqUdpClient_SetStateQueueFlag(struct mmRqUdpClient* p, mmUInt8_t flag)
{
    mmClientUdp_SetStateQueueFlag(&p->client_udp_unicast, flag);
    mmClientUdp_SetStateQueueFlag(&p->client_udp_mulcast, flag);
}
MM_EXPORT_RQ void mmRqUdpClient_ClearHandleRbtree(struct mmRqUdpClient* p)
{
    mmClientUdp_ClearHandleRbtree(&p->client_udp_unicast);
    mmClientUdp_ClearHandleRbtree(&p->client_udp_mulcast);

    mmSpinlock_Lock(&p->rbtree_locker_unicast_q);
    mmRbtreeU32Vpt_Clear(&p->rbtree_unicast_q);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast_q);

    mmSpinlock_Lock(&p->rbtree_locker_unicast_n);
    mmRbtreeU32Vpt_Clear(&p->rbtree_unicast_n);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast_n);

    mmSpinlock_Lock(&p->rbtree_locker_mulcast_q);
    mmRbtreeU32Vpt_Clear(&p->rbtree_mulcast_q);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast_q);

    mmSpinlock_Lock(&p->rbtree_locker_mulcast_q);
    mmRbtreeU32Vpt_Clear(&p->rbtree_mulcast_q);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast_q);
}
// fopen socket.
MM_EXPORT_RQ void mmRqUdpClient_FopenSocket(struct mmRqUdpClient* p)
{
    mmClientUdp_FopenSocket(&p->client_udp_unicast);
    mmClientUdp_FopenSocket(&p->client_udp_mulcast);
}
// bind.
MM_EXPORT_RQ void mmRqUdpClient_Bind(struct mmRqUdpClient* p)
{
    mmClientUdp_Bind(&p->client_udp_unicast);
    mmClientUdp_Bind(&p->client_udp_mulcast);
}
// close socket.
MM_EXPORT_RQ void mmRqUdpClient_CloseSocket(struct mmRqUdpClient* p)
{
    mmClientUdp_CloseSocket(&p->client_udp_unicast);
    mmClientUdp_CloseSocket(&p->client_udp_mulcast);
}
MM_EXPORT_RQ void mmRqUdpClient_ShutdownSocket(struct mmRqUdpClient* p)
{
    mmClientUdp_ShutdownSocket(&p->client_udp_unicast);
    mmClientUdp_ShutdownSocket(&p->client_udp_mulcast);
}
MM_EXPORT_RQ void mmRqUdpClient_MulcastConnect(struct mmRqUdpClient* p)
{
    mmUInt64_t usec_current = mmTime_CurrentUSec();

    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct mmPacketHead packet_head;
    struct mmByteBuffer buffer;

    mmPacketHead_Init(&packet_head);
    mmByteBuffer_Init(&buffer);

    mmRqFountain_SLock(fountain_unicast);
    mmRqFountain_SetState(fountain_unicast, MM_RQ_SYN_SEND);
    mmRqFountain_SUnlock(fountain_unicast);

    mmRqFountain_SLock(fountain_mulcast);
    mmRqFountain_SetState(fountain_mulcast, MM_RQ_SYN_SEND);
    mmRqFountain_SUnlock(fountain_mulcast);

    // make sure not NULL.
    buffer.buffer = (mmUInt8_t*)&buffer;
    buffer.offset = (size_t)0;
    buffer.length = (size_t)0;

    // mid is message id.
    // pid is identity.
    // sid is client timecode us.
    // uid is unique_id.
    packet_head.mid = mmRqMidUInt32_SynNone;
    packet_head.pid = 0;
    packet_head.sid = usec_current;
    packet_head.uid = (uintptr_t)(p);

    // ss_remote here is lock free.
    __static_mmRqUdpClient_SendRepeatedMassageUnicast(
        p,
        &packet_head,
        MM_MSG_COMM_HEAD_SIZE,
        &buffer,
        &fountain_unicast->ss_remote);

    mmPacketHead_Destroy(&packet_head);
    mmByteBuffer_Destroy(&buffer);
}
MM_EXPORT_RQ void mmRqUdpClient_MulcastShutdown(struct mmRqUdpClient* p)
{
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct mmPacketHead packet_head;
    struct mmByteBuffer buffer;

    mmPacketHead_Init(&packet_head);
    mmByteBuffer_Init(&buffer);

    mmRqFountain_SLock(fountain_unicast);
    mmRqFountain_SetState(fountain_unicast, MM_RQ_FIN_WAIT_1);
    mmRqFountain_SUnlock(fountain_unicast);

    mmRqFountain_SLock(fountain_mulcast);
    mmRqFountain_SetState(fountain_mulcast, MM_RQ_FIN_WAIT_1);
    mmRqFountain_SUnlock(fountain_mulcast);

    // make sure not NULL.
    buffer.buffer = (mmUInt8_t*)&buffer;
    buffer.offset = (size_t)0;
    buffer.length = (size_t)0;

    // mid is message id.
    // pid is identity.
    // sid is none.
    // uid is none.
    packet_head.mid = mmRqMidUInt32_FinNone;
    packet_head.pid = fountain_unicast->identity;
    packet_head.sid = 0;
    packet_head.uid = 0;

    // ss_remote here is lock free.
    __static_mmRqUdpClient_SendRepeatedMassageUnicast(
        p,
        &packet_head,
        MM_MSG_COMM_HEAD_SIZE,
        &buffer,
        &fountain_unicast->ss_remote);

    mmPacketHead_Destroy(&packet_head);
    mmByteBuffer_Destroy(&buffer);
}
MM_EXPORT_RQ void mmRqUdpClient_MulcastIPersistence(struct mmRqUdpClient* p)
{
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct __mmRqUdpClientPacketIdxArgument _send_argument;

    _send_argument.rq_udp_client = p;
    _send_argument.fountain = fountain_mulcast;
    _send_argument.udp = &p->client_udp_mulcast.net_udp.udp;

    mmRqFountain_ILock(fountain_mulcast);
    mmRqFountain_AssemblyPerfectO(fountain_mulcast, &_send_argument, fountain_mulcast->rq_o_packet_id);
    mmRqFountain_AssemblyPerfectI(fountain_mulcast, &_send_argument, fountain_mulcast->rq_i_packet_id);
    mmRqFountain_IUnlock(fountain_mulcast);

    __static_mmRqUdpClient_MulcastIPersistenceHandle(p);
}
MM_EXPORT_RQ int mmRqUdpClient_MulcastJoin(struct mmRqUdpClient* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmClientUdp_MulcastJoin(&p->client_udp_mulcast, mr_multiaddr, mr_interface);
}
MM_EXPORT_RQ int mmRqUdpClient_MulcastDrop(struct mmRqUdpClient* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmClientUdp_MulcastDrop(&p->client_udp_mulcast, mr_multiaddr, mr_interface);
}
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_RQ void mmRqUdpClient_ThreadHandleRecv(struct mmRqUdpClient* p)
{
    mmClientUdp_ThreadHandleRecv(&p->client_udp_unicast);
    mmClientUdp_ThreadHandleRecv(&p->client_udp_mulcast);
}
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_RQ void mmRqUdpClient_ThreadHandleSend(struct mmRqUdpClient* p)
{
    mmClientUdp_ThreadHandleSend(&p->client_udp_unicast);
    mmClientUdp_ThreadHandleSend(&p->client_udp_mulcast);
}

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushRecvUnicast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    mmClientUdp_PushRecv(&p->client_udp_unicast, obj, pack, remote);
}

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushRecvMulcast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    mmClientUdp_PushRecv(&p->client_udp_mulcast, obj, pack, remote);
}

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushSendUnicast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    mmClientUdp_PushSend(&p->client_udp_unicast, obj, pack, remote);
}

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushSendMulcast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    mmClientUdp_PushSend(&p->client_udp_mulcast, obj, pack, remote);
}

MM_EXPORT_RQ int
mmRqUdpClient_SendBufferUnicast(
    struct mmRqUdpClient* p,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet)
{
    int code = -1;

    if (MM_RQ_ESTABLISHED == p->fountain_unicast.state)
    {
        struct __mmRqUdpClientPacketIdxArgument _send_argument;

        _send_argument.rq_udp_client = p;
        _send_argument.fountain = &p->fountain_unicast;
        _send_argument.udp = &p->client_udp_unicast.net_udp.udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker can not lock.
        //   o_locker need lock.
        mmRqFountain_OLock(&p->fountain_unicast);
        code = mmRqFountain_SendBuffer(&p->fountain_unicast, &_send_argument, packet_head, buffer, hlength, packet);
        mmRqFountain_OUnlock(&p->fountain_unicast);
    }
    else
    {
        void* obj = &p->client_udp_unicast.net_udp.udp;
        // nready event.
        __static_mmRqUdpClient_NetUdpRqNreadyEventUnicast(obj);
    }
    return code;
}

MM_EXPORT_RQ int
mmRqUdpClient_SendBufferMulcast(
    struct mmRqUdpClient* p,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet)
{
    int code = -1;

    if (MM_RQ_ESTABLISHED == p->fountain_mulcast.state)
    {
        struct __mmRqUdpClientPacketIdxArgument _send_argument;

        _send_argument.rq_udp_client = p;
        _send_argument.fountain = &p->fountain_mulcast;
        _send_argument.udp = &p->client_udp_mulcast.net_udp.udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker can not lock.
        //   o_locker need lock.
        mmRqFountain_OLock(&p->fountain_mulcast);
        code = mmRqFountain_SendBuffer(&p->fountain_mulcast, &_send_argument, packet_head, buffer, hlength, packet);
        mmRqFountain_OUnlock(&p->fountain_mulcast);
    }
    else
    {
        void* obj = &p->client_udp_mulcast.net_udp.udp;
        // nready event.
        __static_mmRqUdpClient_NetUdpRqNreadyEventMulcast(obj);
    }
    return code;
}

// send message, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_RQ int
mmRqUdpClient_SendMessageUnicast(
    struct mmRqUdpClient* p,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int code = -1;

    if (MM_RQ_ESTABLISHED == p->fountain_unicast.state)
    {
        struct __mmRqUdpClientPacketIdxArgument _send_argument;

        _send_argument.rq_udp_client = p;
        _send_argument.fountain = &p->fountain_unicast;
        _send_argument.udp = &p->client_udp_unicast.net_udp.udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker can not lock.
        //   o_locker need lock.
        mmRqFountain_OLock(&p->fountain_unicast);
        code = mmRqFountain_SendMessage(&p->fountain_unicast, &_send_argument, coder, packet_head, rq_msg, hlength, rq_pack);
        mmRqFountain_OUnlock(&p->fountain_unicast);
    }
    else
    {
        void* obj = &p->client_udp_unicast.net_udp.udp;
        // nready event.
        __static_mmRqUdpClient_NetUdpRqNreadyEventUnicast(obj);
    }
    return code;
}

// send message, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_RQ int
mmRqUdpClient_SendMessageMulcast(
    struct mmRqUdpClient* p,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int code = -1;

    if (MM_RQ_ESTABLISHED == p->fountain_mulcast.state)
    {
        struct __mmRqUdpClientPacketIdxArgument _send_argument;

        _send_argument.rq_udp_client = p;
        _send_argument.fountain = &p->fountain_mulcast;
        _send_argument.udp = &p->client_udp_mulcast.net_udp.udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker can not lock.
        //   o_locker need lock.
        mmRqFountain_OLock(&p->fountain_mulcast);
        code = mmRqFountain_SendMessage(&p->fountain_mulcast, &_send_argument, coder, packet_head, rq_msg, hlength, rq_pack);
        mmRqFountain_OUnlock(&p->fountain_mulcast);
    }
    else
    {
        void* obj = &p->client_udp_mulcast.net_udp.udp;
        // nready event.
        __static_mmRqUdpClient_NetUdpRqNreadyEventMulcast(obj);
    }
    return code;
}

// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_RQ void mmRqUdpClient_FlushSignalUnicast(struct mmRqUdpClient* p)
{
    mmClientUdp_FlushSignal(&p->client_udp_unicast);
}
// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_RQ void mmRqUdpClient_FlushSignalMulcast(struct mmRqUdpClient* p)
{
    mmClientUdp_FlushSignal(&p->client_udp_mulcast);
}
MM_EXPORT_RQ void mmRqUdpClient_Start(struct mmRqUdpClient* p)
{
    mmRqDatabase_Fopen(&p->database_mulcast_i);
    mmClientUdp_Start(&p->client_udp_unicast);
    mmClientUdp_Start(&p->client_udp_mulcast);
    mmTimer_Start(&p->timer);
}
MM_EXPORT_RQ void mmRqUdpClient_Interrupt(struct mmRqUdpClient* p)
{
    mmClientUdp_Interrupt(&p->client_udp_unicast);
    mmClientUdp_Interrupt(&p->client_udp_mulcast);
    mmTimer_Interrupt(&p->timer);
}
MM_EXPORT_RQ void mmRqUdpClient_Shutdown(struct mmRqUdpClient* p)
{
    mmClientUdp_Shutdown(&p->client_udp_unicast);
    mmClientUdp_Shutdown(&p->client_udp_mulcast);
    mmTimer_Shutdown(&p->timer);
}
MM_EXPORT_RQ void mmRqUdpClient_Join(struct mmRqUdpClient* p)
{
    mmClientUdp_Join(&p->client_udp_unicast);
    mmClientUdp_Join(&p->client_udp_mulcast);
    mmTimer_Join(&p->timer);
    mmRqDatabase_Fclose(&p->database_mulcast_i);
}

static void __static_mmRqUdpClient_NetUdpRqBrokenEventUnicast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpClient_NNetUdpHandleUnicast);
}
static void __static_mmRqUdpClient_NetUdpRqNreadyEventUnicast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpClient_NNetUdpHandleUnicast);
}
static void __static_mmRqUdpClient_NetUdpRqFinishEventUnicast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpClient_NNetUdpHandleUnicast);
}

static void __static_mmRqUdpClient_NetUdpRqBrokenEventMulcast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpClient_NNetUdpHandleMulcast);
}
static void __static_mmRqUdpClient_NetUdpRqNreadyEventMulcast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpClient_NNetUdpHandleMulcast);
}
static void __static_mmRqUdpClient_NetUdpRqFinishEventMulcast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpClient_NNetUdpHandleMulcast);
}
//
static void __static_mmRqUdpClient_NetUdpNtBrokenEventUnicast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_NNetUdpHandleUnicast);
}
static void __static_mmRqUdpClient_NetUdpNtNreadyEventUnicast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_NNetUdpHandleUnicast);
}
static void __static_mmRqUdpClient_NetUdpNtFinishEventUnicast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_NNetUdpHandleUnicast);
}

static void __static_mmRqUdpClient_NetUdpNtBrokenEventMulcast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_NNetUdpHandleMulcast);
}
static void __static_mmRqUdpClient_NetUdpNtNreadyEventMulcast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_NNetUdpHandleMulcast);
}
static void __static_mmRqUdpClient_NetUdpNtFinishEventMulcast(void* obj)
{
    mmNetUdp_MidEventHandle(obj, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_NNetUdpHandleMulcast);
}

static void __static_mmRqUdpClient_BrokenNNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    __static_mmRqUdpClient_NetUdpNtBrokenEventUnicast(obj);
}
static void __static_mmRqUdpClient_NreadyNNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    __static_mmRqUdpClient_NetUdpNtNreadyEventUnicast(obj);
}
static void __static_mmRqUdpClient_FinishNNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);

    mmRqUdpClient_MulcastConnect(p);

    // we need connect event.
    __static_mmRqUdpClient_NetUdpNtFinishEventUnicast(obj);
}
static void __static_mmRqUdpClient_BrokenNNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);

    mmNetUdp_MulcastDrop(net_udp, mmString_CStr(&p->mr_multiaddr), mmString_CStr(&p->mr_interface));

    // mulcast client udp event need fire only nt event.
    __static_mmRqUdpClient_NetUdpNtBrokenEventMulcast(obj);
}
static void __static_mmRqUdpClient_NreadyNNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    // mulcast client udp event need fire only nt event.
    __static_mmRqUdpClient_NetUdpNtNreadyEventMulcast(obj);
}
static void __static_mmRqUdpClient_FinishNNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);

    mmNetUdp_MulcastJoin(net_udp, mmString_CStr(&p->mr_multiaddr), mmString_CStr(&p->mr_interface));

    // mulcast client udp event need fire only nt event.
    __static_mmRqUdpClient_NetUdpNtFinishEventMulcast(obj);
}
static void __static_mmRqUdpClient_UnicastNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        struct __mmRqUdpClientPacketIdxArgument _recv_argument;

        _recv_argument.rq_udp_client = p;
        _recv_argument.fountain = fountain_unicast;
        _recv_argument.udp = udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker need lock.
        //   o_locker can not lock.
        mmRqFountain_ILock(fountain_unicast);
        mmRqFountain_RecvBuffer(fountain_unicast, &_recv_argument, pack, remote);
        mmRqFountain_IUnlock(fountain_unicast);
    }
    else
    {
        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_Lock(fountain_unicast);
        mmRqFountain_ResetContext(fountain_unicast);
        mmRqFountain_Unlock(fountain_unicast);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;

        // mid is message id.
        // pid is identity.
        // sid is none.
        // uid is none.
        packet_head.mid = mmRqMidUInt32_RstNone;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = 0;
        packet_head.uid = 0;

        // rst not need send repeated.
        mmClientUdp_SendBuffer(&p->client_udp_unicast, &packet_head, MM_MSG_COMM_HEAD_SIZE, &buffer, remote);
        mmClientUdp_FlushSignal(&p->client_udp_unicast);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);

        // broken event.
        __static_mmRqUdpClient_NetUdpRqBrokenEventUnicast(obj);
    }
}
static void __static_mmRqUdpClient_MulcastNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_mulcast->state)
    {
        struct __mmRqUdpClientPacketIdxArgument _recv_argument;

        _recv_argument.rq_udp_client = p;
        _recv_argument.fountain = fountain_mulcast;
        _recv_argument.udp = udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker need lock.
        //   o_locker can not lock.
        mmRqFountain_ILock(fountain_mulcast);
        mmRqFountain_RecvBuffer(fountain_mulcast, &_recv_argument, pack, remote);
        mmRqFountain_IUnlock(fountain_mulcast);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_mulcast->state);
    }
}
static void __static_mmRqUdpClient_SynNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmLogger_LogT(gLogger, "%s %d ignore unknow message.", __FUNCTION__, __LINE__);
}
static void __static_mmRqUdpClient_AckNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_LAST_ACK == fountain_unicast->state)
    {
        mmRqFountain_Lock(fountain_unicast);
        mmRqFountain_ResetContext(fountain_unicast);
        mmRqFountain_Unlock(fountain_unicast);

        // broken event.
        __static_mmRqUdpClient_NetUdpRqBrokenEventUnicast(obj);
    }
    else if (MM_RQ_FIN_WAIT_1 == fountain_unicast->state)
    {
        mmRqFountain_SLock(fountain_unicast);
        mmRqFountain_SetState(fountain_unicast, MM_RQ_FIN_WAIT_2);
        mmRqFountain_SUnlock(fountain_unicast);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_FinNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_SLock(fountain_unicast);
        mmRqFountain_SetState(fountain_unicast, MM_RQ_CLOSE_WAIT);
        mmRqFountain_SUnlock(fountain_unicast);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;

        // mid is message id.
        // pid is identity.
        // sid is l(mmUInt32_t) mulcast recv timestamp_initial, r(mmUInt32_t) mulcast timestamp_initial.
        // uid is l(mmUInt32_t) unicast recv timestamp_initial, r(mmUInt32_t) unicast timestamp_initial.
        packet_head.mid = mmRqMidUInt32_AckNone;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = 0;// not need here.
        packet_head.uid = 0;// not need here.

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            remote);

        // mid is message id.
        // pid is identity.
        // sid is none.
        // uid is none.
        packet_head.mid = mmRqMidUInt32_FinNone;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = 0;
        packet_head.uid = 0;

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else if (MM_RQ_FIN_WAIT_2 == fountain_unicast->state)
    {
        mmRqFountain_SLock(fountain_unicast);
        mmRqFountain_SetState(fountain_unicast, MM_RQ_TIME_WAIT);
        mmRqFountain_SUnlock(fountain_unicast);

        // we not need time_wait state.
        mmRqFountain_Lock(fountain_unicast);
        mmRqFountain_ResetContext(fountain_unicast);
        mmRqFountain_Unlock(fountain_unicast);

        // broken event.
        __static_mmRqUdpClient_NetUdpRqBrokenEventUnicast(obj);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_RstNoneNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    mmRqFountain_Lock(fountain_unicast);
    mmRqFountain_ResetContext(fountain_unicast);
    mmRqFountain_Unlock(fountain_unicast);

    // broken event.
    __static_mmRqUdpClient_NetUdpRqBrokenEventUnicast(obj);
    __static_mmRqUdpClient_NetUdpRqBrokenEventMulcast(obj);
}
static void __static_mmRqUdpClient_SyncAckNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_SYN_SEND == fountain_unicast->state)
    {
        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmUInt64_t usec_current = mmTime_CurrentUSec();
        mmUInt64_t usec_current_rq = pack->phead.sid;

        mmUInt32_t identity = pack->phead.pid;

        mmUInt64_t uid = 0;
        mmUInt64_t sid = 0;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_Lock(fountain_unicast);
        mmRqFountain_ResetContext(fountain_unicast);
        fountain_unicast->rq_i.timestamp_initial = mmByte_UInt64L(pack->phead.uid);
        fountain_unicast->rq_o.timestamp_initial = mmByte_UInt64R(pack->phead.uid);
        uid = mmByte_UInt64A(fountain_unicast->rq_o.timestamp_initial, fountain_unicast->rq_i.timestamp_initial);
        mmRqFountain_SetTimecodeCreate(fountain_unicast, usec_current);
        mmRqFountain_SetTimecodeUpdate(fountain_unicast, usec_current_rq);
        mmRqFountain_SetIdentity(fountain_unicast, identity);
        mmRqFountain_SetState(fountain_unicast, MM_RQ_ESTABLISHED);
        mmRqFountain_Unlock(fountain_unicast);

        mmRqFountain_Lock(fountain_mulcast);
        mmRqFountain_ResetContext(fountain_mulcast);
        fountain_mulcast->rq_i.timestamp_initial = mmByte_UInt64L(pack->phead.sid);
        fountain_mulcast->rq_o.timestamp_initial = mmByte_UInt64R(pack->phead.sid);
        sid = mmByte_UInt64A(fountain_mulcast->rq_o.timestamp_initial, fountain_mulcast->rq_i.timestamp_initial);
        mmRqFountain_SetTimecodeCreate(fountain_mulcast, usec_current);
        mmRqFountain_SetTimecodeCreate(fountain_mulcast, usec_current_rq);
        mmRqFountain_SetIdentity(fountain_mulcast, identity);
        mmRqFountain_SetState(fountain_mulcast, MM_RQ_ESTABLISHED);
        mmRqFountain_Unlock(fountain_mulcast);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is l(mmUInt32_t) mulcast recv timestamp_initial, r(mmUInt32_t) mulcast timestamp_initial.
        // uid is l(mmUInt32_t) unicast recv timestamp_initial, r(mmUInt32_t) unicast timestamp_initial.
        packet_head.mid = mmRqMidUInt32_AckNone;
        packet_head.pid = fountain_unicast->identity;
        packet_head.uid = uid;
        packet_head.sid = sid;

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);

        // finish event.
        __static_mmRqUdpClient_NetUdpRqFinishEventUnicast(obj);
        __static_mmRqUdpClient_NetUdpRqFinishEventMulcast(obj);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore repeated message.", __FUNCTION__, __LINE__);
    }
}
static void __static_mmRqUdpClient_PingReqNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is client timecode us.
        // uid is none.
        packet_head.mid = mmRqMidUInt32_PingRes;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = pack->phead.sid;
        packet_head.uid = 0;

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            &fountain_unicast->ss_remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.", __FUNCTION__, __LINE__, fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_PingResNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();

        p->unicast_timecode_rtt = usec_current - pack->phead.sid;

        mmRqFountain_SLock(fountain_unicast);
        mmRqFountain_SetTimecodeRtt(fountain_unicast, p->unicast_timecode_rtt);
        mmRqFountain_SUnlock(fountain_unicast);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_LossUpdNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t timecode_o_packet_number_update = 0;

        mmRqFountain_OLock(fountain_unicast);
        timecode_o_packet_number_update = fountain_unicast->timecode_o_packet_number_update;
        mmRqFountain_OUnlock(fountain_unicast);

        if (timecode_o_packet_number_update < pack->phead.uid)
        {
            mmUInt64_t msec_current_rq = (mmUInt64_t)(pack->phead.uid);

            // most of time, the server mulcast_i_packet_number is nothing.
            // mmUInt32_t mulcast_i_packet_number = mmByte_UInt64L(pack->phead.sid);
            mmUInt32_t unicast_i_packet_number = mmByte_UInt64R(pack->phead.sid);

            mmUInt32_t unicast_o_packet_number = 0;

            float unicast_expected_loss = 0;

            mmRqFountain_OLock(fountain_unicast);
            
            unicast_o_packet_number = (mmUInt32_t)(
                fountain_unicast->rq_o.packet_number -
                fountain_unicast->rq_o_packet_number_interval);
            
            mmRqFountain_SetOPacketNumberUpdate(fountain_unicast, msec_current_rq);
            mmRqFountain_OUnlock(fountain_unicast);

            unicast_expected_loss = mmRqO_ExpectedLoss(unicast_i_packet_number, unicast_o_packet_number);

            mmRqFountain_OLock(fountain_unicast);
            mmRqFountain_SetExpectedLoss(fountain_unicast, unicast_expected_loss);
            mmRqFountain_OUnlock(fountain_unicast);

            p->unicast_packet_loss =
                unicast_o_packet_number > unicast_i_packet_number ?
                unicast_o_packet_number - unicast_i_packet_number : 0;
            
            p->unicast_expected_loss = unicast_expected_loss;
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore repeated message.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_IdxsUpdNNetUdpHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t timecode_o_packet_index_update = 0;

        mmRqFountain_OLock(fountain_unicast);
        timecode_o_packet_index_update = fountain_unicast->timecode_o_packet_indexp_update;
        mmRqFountain_OUnlock(fountain_unicast);

        if (timecode_o_packet_index_update < pack->phead.uid)
        {
            mmUInt64_t msec_current_rq = (mmUInt64_t)(pack->phead.uid);

            // most of time, the server mulcast_index is nothing.
            mmUInt32_t mulcast_index = 0;
            mmUInt32_t unicast_index = 0;

            struct __mmRqUdpClientPacketIdxArgument _send_argument;

            _send_argument.rq_udp_client = p;
            _send_argument.fountain = &p->fountain_unicast;
            _send_argument.udp = udp;

            mulcast_index = mmByte_UInt64L(pack->phead.sid);
            unicast_index = mmByte_UInt64R(pack->phead.sid);

            mmRqFountain_OLock(fountain_unicast);
            // need update timecodde fountain_unicast o_packet_index.
            mmRqFountain_SetOPacketIndexpUpdate(fountain_unicast, msec_current_rq);
            // fountain_unicast repaird fountain_unicast.
            mmRqFountain_GenerateRepairdO(fountain_unicast, &_send_argument, unicast_index);
            mmRqFountain_OUnlock(fountain_unicast);

            mmRqFountain_ILock(fountain_unicast);
            mmRqFountain_AssemblyPerfectO(fountain_unicast, &_send_argument, unicast_index);
            mmRqFountain_AssemblyPerfectI(fountain_unicast, &_send_argument, fountain_unicast->rq_i_packet_id);
            mmRqFountain_IUnlock(fountain_unicast);

            mmRqFountain_ILock(fountain_unicast);
            mmRqFountain_FeedbackPerfectO(fountain_unicast, &_send_argument);
            mmRqFountain_IUnlock(fountain_unicast);

            mmRqFountain_ILock(fountain_unicast);
            mmRqFountain_FeedbackPerfectI(fountain_unicast, &_send_argument);
            mmRqFountain_IUnlock(fountain_unicast);

            _send_argument.rq_udp_client = p;
            _send_argument.fountain = &p->fountain_mulcast;
            _send_argument.udp = udp;

            mmRqFountain_ILock(fountain_mulcast);
            mmRqFountain_AssemblyPerfectO(fountain_mulcast, &_send_argument, mulcast_index);
            mmRqFountain_AssemblyPerfectI(fountain_mulcast, &_send_argument, fountain_mulcast->rq_i_packet_id);
            mmRqFountain_IUnlock(fountain_mulcast);

            mmRqFountain_ILock(fountain_mulcast);
            mmRqFountain_FeedbackPerfectO(fountain_mulcast, &_send_argument);
            mmRqFountain_IUnlock(fountain_mulcast);

            // fountain_mulcast feedback_perfect_i need persistence. 
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore repeated message.", __FUNCTION__, __LINE__);
        }
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}

static void __static_mmRqUdpClient_UnicastICallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet)
{
    // need do nothing.
}

static void __static_mmRqUdpClient_UnicastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    struct mmStreambuf* streambuf = &e->streambuf_buffer;
    struct __mmRqUdpClientPacketIdxArgument* _recv_argument = (struct __mmRqUdpClientPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain_unicast = _recv_argument->fountain;
    struct mmSockaddr* remote = &fountain_unicast->ss_remote;

    size_t gptr = streambuf->gptr;
    
    mmStreambufPacket_HandleUdp(
        streambuf,
        &__static_mmRqUdpClient_UnicastIPacketHandleUdp,
        _recv_argument,
        remote);
    
    mmStreambuf_SetGPtr(streambuf, gptr);

    // note:
    // 1. i_locker is lock outside.
    // 2. locker order is locker -> i_locker -> o_locker.
    // 3. here can not lock locker, because locker order will i_locker -> locker case dead lock.
    // 4. here not need lock locker, because rq_i_packet_id is i_locker data.
    mmRqFountain_IncreaseRqIPacketId(fountain_unicast);
}
static void __static_mmRqUdpClient_UnicastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{

}
static void __static_mmRqUdpClient_UnicastOCallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet)
{
    struct mmRqO* rq_o = (struct mmRqO*)(obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(rq_o->callback.obj);
    struct __mmRqUdpClientPacketIdxArgument* _send_argument = (struct __mmRqUdpClientPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain = _send_argument->fountain;
    struct mmStreambuf* streambuf = &e->streambuf_packet;

    struct mmByteBuffer buffer;
    struct mmPacketHead packet_head;

    mmByteBuffer_Init(&buffer);
    mmPacketHead_Init(&packet_head);

    buffer.buffer = (mmUInt8_t*)streambuf->buff;
    buffer.offset = (size_t)streambuf->gptr;
    buffer.length = (size_t)mmStreambuf_Size(streambuf);

    // mid is message id.
    // pid is identity.
    // sid is none.
    // uid is none.
    packet_head.mid = mmRqMidUInt32_Unicast;
    packet_head.pid = fountain->identity;
    packet_head.sid = 0;
    packet_head.uid = 0;

    // ss_remote here is lock free.
    // note:
    // this process is mmRqO mmRqO_GenerateSourced callback, lock outside.
    // here can not lock o.
    // the fountain memory lock not need lock.
    mmClientUdp_SendBuffer(
        &p->client_udp_unicast,
        &packet_head,
        MM_RQ_CLIENT_COMM_HEAD_SIZE,
        &buffer,
        &fountain->ss_remote);

    mmByteBuffer_Destroy(&buffer);
    mmPacketHead_Destroy(&packet_head);
}
static void __static_mmRqUdpClient_UnicastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}
static void __static_mmRqUdpClient_UnicastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}
static void __static_mmRqUdpClient_MulcastICallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet)
{
    // need do nothing.
}
static void __static_mmRqUdpClient_MulcastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    struct mmStreambuf* streambuf = &e->streambuf_buffer;
    struct __mmRqUdpClientPacketIdxArgument* _recv_argument = (struct __mmRqUdpClientPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain_mulcast = _recv_argument->fountain;
    struct mmSockaddr* remote = &fountain_mulcast->ss_remote;

    size_t gptr = streambuf->gptr;
    
    mmStreambufPacket_HandleUdp(
        streambuf,
        &__static_mmRqUdpClient_MulcastIPacketHandleUdp,
        _recv_argument,
        remote);
    
    mmStreambuf_SetGPtr(streambuf, gptr);

    // note:
    // 1. i_locker is lock outside.
    // 2. locker order is locker -> i_locker -> o_locker.
    // 3. here can not lock locker, because locker order will i_locker -> locker case dead lock.
    // 4. here not need lock locker, because rq_i_packet_id is i_locker data.
    mmRqFountain_IncreaseRqIPacketId(fountain_mulcast);
}
static void __static_mmRqUdpClient_MulcastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    struct mmRqO* rq_o = (struct mmRqO*)(obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(rq_o->callback.obj);
    struct mmStreambuf* streambuf = &e->streambuf_buffer;

    struct mmByteBuffer byte_buffer;

    byte_buffer.buffer = (mmUInt8_t*)streambuf->buff;
    byte_buffer.offset = (size_t)streambuf->gptr;
    byte_buffer.length = (size_t)mmStreambuf_Size(streambuf);

    // lock outside, not need lock here.
    mmRqDatabase_Insert(&p->database_mulcast_i, e->id, &byte_buffer);
}
static void __static_mmRqUdpClient_MulcastOCallbackHandle(void* obj, void* u, void* idx, struct mmRqElem* e, struct mmRqPacket* packet)
{
    struct mmRqO* rq_o = (struct mmRqO*)(obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(rq_o->callback.obj);
    struct __mmRqUdpClientPacketIdxArgument* _send_argument = (struct __mmRqUdpClientPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain = _send_argument->fountain;
    struct mmStreambuf* streambuf = &e->streambuf_packet;

    struct mmByteBuffer buffer;
    struct mmPacketHead packet_head;

    mmByteBuffer_Init(&buffer);
    mmPacketHead_Init(&packet_head);

    buffer.buffer = (mmUInt8_t*)streambuf->buff;
    buffer.offset = (size_t)streambuf->gptr;
    buffer.length = (size_t)mmStreambuf_Size(streambuf);

    // mid is message id.
    // pid is identity.
    // sid is none.
    // uid is none.
    packet_head.mid = mmRqMidUInt32_Mulcast;
    packet_head.pid = fountain->identity;
    packet_head.sid = 0;
    packet_head.uid = 0;

    // ss_remote here is lock free.
    // note:
    // this process is mmRqO mmRqO_GenerateSourced callback, lock outside.
    // here can not lock o.
    // the fountain memory lock not need lock.
    mmClientUdp_SendBuffer(
        &p->client_udp_mulcast,
        &packet_head,
        MM_RQ_CLIENT_COMM_HEAD_SIZE,
        &buffer,
        &fountain->ss_remote);

    mmByteBuffer_Destroy(&buffer);
    mmPacketHead_Destroy(&packet_head);
}
static void __static_mmRqUdpClient_MulcastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}
static void __static_mmRqUdpClient_MulcastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}
static void __static_mmRqUdpClient_UnicastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct __mmRqUdpClientPacketIdxArgument* _recv_argument = (struct __mmRqUdpClientPacketIdxArgument*)(obj);
    struct mmRqUdpClient* p = _recv_argument->rq_udp_client;
    struct mmRqFountain* fountain = _recv_argument->fountain;
    struct mmUdp* udp = _recv_argument->udp;
    struct mmClientUdp* client_udp = &p->client_udp_unicast;

    mmRqUdpClientHandleFunc handle = NULL;
    mmSpinlock_Lock(&p->rbtree_locker_unicast_n);
    handle = (mmRqUdpClientHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree_unicast_n, pack->phead.mid);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast_n);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, p->u, fountain->identity, pack, remote);
    }
    else
    {
        assert(NULL != p->callback_unicast_n.Handle && "p->callback_unicast_n.Handle is a null.");
        (*(p->callback_unicast_n.Handle))(udp, p->u, fountain->identity, pack, remote);
    }
    // push to queue thread.
    if (MM_CLIENT_UDP_QUEUE_ACTIVATE == client_udp->state_queue_flag)
    {
        // push recv api is lock free.
        // note:
        //    we use udp for obj argument.
        mmClientUdp_PushRecv(client_udp, udp, pack, remote);
    }
}
static void __static_mmRqUdpClient_MulcastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct __mmRqUdpClientPacketIdxArgument* _recv_argument = (struct __mmRqUdpClientPacketIdxArgument*)(obj);
    struct mmRqUdpClient* p = _recv_argument->rq_udp_client;
    struct mmRqFountain* fountain = _recv_argument->fountain;
    struct mmUdp* udp = _recv_argument->udp;
    struct mmClientUdp* client_udp = &p->client_udp_mulcast;

    mmRqUdpClientHandleFunc handle = NULL;
    mmSpinlock_Lock(&p->rbtree_locker_mulcast_n);
    handle = (mmRqUdpClientHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree_mulcast_n, pack->phead.mid);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast_n);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, p->u, fountain->identity, pack, remote);
    }
    else
    {
        assert(NULL != p->callback_mulcast_n.Handle && "p->callback_mulcast_n.Handle is a null.");
        (*(p->callback_mulcast_n.Handle))(udp, p->u, fountain->identity, pack, remote);
    }
    // push to queue thread.
    if (MM_CLIENT_UDP_QUEUE_ACTIVATE == client_udp->state_queue_flag)
    {
        // push recv api is lock free.
        // note:
        //    we use udp for obj argument.
        mmClientUdp_PushRecv(client_udp, udp, pack, remote);
    }
}
static void __static_mmRqUdpClient_UnicastQueueRecvPacketHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain = &p->fountain_unicast;

    mmRqUdpClientHandleFunc handle = NULL;
    mmSpinlock_Lock(&p->rbtree_locker_unicast_q);
    handle = (mmRqUdpClientHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree_unicast_q, pack->phead.mid);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast_q);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, p->u, fountain->identity, pack, remote);
    }
    else
    {
        assert(NULL != p->callback_unicast_q.Handle && "p->callback_unicast_q.Handle is a null.");
        (*(p->callback_unicast_q.Handle))(udp, p->u, fountain->identity, pack, remote);
    }
}
static void __static_mmRqUdpClient_MulcastQueueRecvPacketHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain = &p->fountain_mulcast;

    mmRqUdpClientHandleFunc handle = NULL;
    mmSpinlock_Lock(&p->rbtree_locker_mulcast_q);
    handle = (mmRqUdpClientHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree_mulcast_q, pack->phead.mid);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast_q);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, p->u, fountain->identity, pack, remote);
    }
    else
    {
        assert(NULL != p->callback_mulcast_q.Handle && "p->callback_mulcast_q.Handle is a null.");
        (*(p->callback_mulcast_q.Handle))(udp, p->u, fountain->identity, pack, remote);
    }
}
static void __static_mmRqUdpClient_NNetUdpHandleUnicast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain = &p->fountain_unicast;

    struct __mmRqUdpClientPacketIdxArgument _recv_argument;

    _recv_argument.rq_udp_client = p;
    _recv_argument.fountain = fountain;
    _recv_argument.udp = udp;

    __static_mmRqUdpClient_UnicastIPacketHandleUdp(&_recv_argument, pack, remote);
}
static void __static_mmRqUdpClient_NNetUdpHandleMulcast(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmNetUdp* net_udp = (struct mmNetUdp*)(udp->callback.obj);
    struct mmClientUdp* client_udp = (struct mmClientUdp*)(net_udp->callback.obj);
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(client_udp->callback_n.obj);
    struct mmRqFountain* fountain = &p->fountain_mulcast;

    struct __mmRqUdpClientPacketIdxArgument _recv_argument;

    _recv_argument.rq_udp_client = p;
    _recv_argument.fountain = fountain;
    _recv_argument.udp = udp;

    __static_mmRqUdpClient_UnicastIPacketHandleUdp(&_recv_argument, pack, remote);
}

static void
__static_mmRqUdpClient_SendRepeatedMassageUnicast(
    struct mmRqUdpClient* p,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmByteBuffer* buffer,
    struct mmSockaddr* remote)
{
    mmUInt32_t i = 0;
    for (i = 0; i < p->repeated_number; i++)
    {
        mmClientUdp_SendBuffer(&p->client_udp_unicast, packet_head, hlength, buffer, remote);
    }
    mmClientUdp_FlushSignal(&p->client_udp_unicast);
}

static void __static_mmRqUdpClient_EmptyHandleForCommonEvent(struct mmRqUdpClient* p)
{
    mmRqUdpClient_SetQHandleUnicast(p, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleUnicast(p, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleUnicast(p, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);

    mmRqUdpClient_SetNHandleUnicast(p, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleUnicast(p, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleUnicast(p, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);

    mmRqUdpClient_SetQHandleMulcast(p, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleMulcast(p, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleMulcast(p, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);

    mmRqUdpClient_SetNHandleMulcast(p, MM_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleMulcast(p, MM_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleMulcast(p, MM_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);
    //
    mmRqUdpClient_SetQHandleUnicast(p, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleUnicast(p, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleUnicast(p, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);

    mmRqUdpClient_SetNHandleUnicast(p, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleUnicast(p, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleUnicast(p, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);

    mmRqUdpClient_SetQHandleMulcast(p, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleMulcast(p, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetQHandleMulcast(p, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);

    mmRqUdpClient_SetNHandleMulcast(p, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleMulcast(p, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpClient_Handle);
    mmRqUdpClient_SetNHandleMulcast(p, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpClient_Handle);
}
static void __static_mmRqUdpClient_ValidHandleForCommonEvent(struct mmClientUdp* client_udp)
{
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_Unicast, &__static_mmRqUdpClient_UnicastNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_Mulcast, &__static_mmRqUdpClient_MulcastNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_SynNone, &__static_mmRqUdpClient_SynNoneNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_AckNone, &__static_mmRqUdpClient_AckNoneNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_FinNone, &__static_mmRqUdpClient_FinNoneNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_RstNone, &__static_mmRqUdpClient_RstNoneNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_SyncAck, &__static_mmRqUdpClient_SyncAckNNetUdpHandle);

    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_PingReq, &__static_mmRqUdpClient_PingReqNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_PingRes, &__static_mmRqUdpClient_PingResNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_LossUpd, &__static_mmRqUdpClient_LossUpdNNetUdpHandle);
    mmClientUdp_SetNHandle(client_udp, mmRqMidUInt32_IdxsUpd, &__static_mmRqUdpClient_IdxsUpdNNetUdpHandle);
}
static void __static_mmRqUdpClient_MsecUpdatePingStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(entry->callback.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();

        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is client timecode us.
        // uid is none.
        packet_head.mid = mmRqMidUInt32_PingReq;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = usec_current;
        packet_head.uid = 0;

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            &fountain_unicast->ss_remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_MsecUpdateLossStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(entry->callback.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();

        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmUInt32_t mulcast_i_packet_number = 0;
        mmUInt32_t unicast_i_packet_number = 0;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_ILock(fountain_mulcast);
        
        mulcast_i_packet_number = (mmUInt32_t)(
            fountain_mulcast->rq_i.packet_number -
            fountain_mulcast->rq_i_packet_number_interval);
        
        mmRqFountain_SetIPacketNumberUpdate(fountain_mulcast, usec_current);
        mmRqFountain_IUnlock(fountain_mulcast);

        mmRqFountain_ILock(fountain_unicast);
        
        unicast_i_packet_number = (mmUInt32_t)(
            fountain_unicast->rq_i.packet_number -
            fountain_unicast->rq_i_packet_number_interval);
        
        mmRqFountain_SetIPacketNumberUpdate(fountain_unicast, usec_current);
        mmRqFountain_IUnlock(fountain_unicast);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is l(mmUInt32_t) mulcast recv message packet number, r(mmUInt32_t) unicast recv message packet number.
        // uid is timecode us.
        packet_head.mid = mmRqMidUInt32_LossUpd;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = mmByte_UInt64A(mulcast_i_packet_number, unicast_i_packet_number);
        packet_head.uid = usec_current;

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            &fountain_unicast->ss_remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_MsecUpdateIdxsStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(entry->callback.obj);
    struct mmRqFountain* fountain_unicast = &p->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();

        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmUInt32_t mulcast_index = 0;
        mmUInt32_t unicast_index = 0;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_ILock(fountain_mulcast);
        mulcast_index = fountain_mulcast->rq_i_packet_id;
        mmRqFountain_SetIPacketIndexpUpdate(fountain_mulcast, usec_current);
        mmRqFountain_IUnlock(fountain_mulcast);

        mmRqFountain_ILock(fountain_unicast);
        unicast_index = fountain_unicast->rq_i_packet_id;
        mmRqFountain_SetIPacketIndexpUpdate(fountain_unicast, usec_current);
        mmRqFountain_IUnlock(fountain_unicast);

        p->mulcast_max_index = mulcast_index;

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is l(mmUInt32_t) mulcast feedback perfect index, r(mmUInt32_t) unicast feedback perfect index.
        // uid is timecode us.
        packet_head.mid = mmRqMidUInt32_IdxsUpd;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = mmByte_UInt64A(mulcast_index, unicast_index);
        packet_head.uid = usec_current;

        __static_mmRqUdpClient_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            &fountain_unicast->ss_remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpClient_MsecUpdateMulcastIPersistenceHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpClient* p = (struct mmRqUdpClient*)(entry->callback.obj);
    
    __static_mmRqUdpClient_MulcastIPersistenceHandle(p);
}
static void __static_mmRqUdpClient_MulcastIPersistenceHandle(struct mmRqUdpClient* p)
{
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct __mmRqUdpClientPacketIdxArgument _send_argument;

    _send_argument.rq_udp_client = p;
    _send_argument.fountain = fountain_mulcast;
    _send_argument.udp = &p->client_udp_mulcast.net_udp.udp;

    // time cast ~= 10.5 ms.
    mmRqDatabase_Lock(&p->database_mulcast_i);

    mmRqDatabase_TransactionB(&p->database_mulcast_i);

    mmRqFountain_ILock(fountain_mulcast);
    mmRqFountain_FeedbackPerfectI(fountain_mulcast, &_send_argument);
    mmRqFountain_IUnlock(fountain_mulcast);

    mmRqDatabase_TransactionE(&p->database_mulcast_i);

    mmRqDatabase_Fflush(&p->database_mulcast_i);

    mmRqDatabase_Unlock(&p->database_mulcast_i);
}
