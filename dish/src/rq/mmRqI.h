/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqI_h__
#define __mmRqI_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmPoolElement.h"

#include "container/mmVectorVpt.h"

#include "raptorq/mmRaptorqContext.h"

#include "mmRqPacket.h"

#include "mmRqElem.h"
#include "mmRqCallback.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

// logic frequency default design for 20
// time = 128 / 20 = 6.4  seconds
// time = 128 / 60 = 2.13 seconds
#define MM_RQ_I_LENGTH_DEFAULT 128
#define MM_RQ_I_CHUNK_SIZE_DEFAULT 256

#define MM_RQ_I_OVERHEAD_DECODE_DEFAULT 0

struct mmRqI
{
    struct mmRqCallback callback;

    struct mmPoolElement pool_element;
    struct mmVectorVpt vector_vpt;
    // length for sequence
    mmUInt32_t length;

    // duplicate sequence the same number.
    mmUInt64_t duplicate;
    // discard sequence timecode invalid number.
    mmUInt64_t discard;
    // packet number.
    mmUInt64_t packet_number;

    // sampling rate.
    mmUInt32_t sampling_rate;
    // communication frame rate.
    mmUInt32_t frame_rate;

    // timestamp interval interval = sampling_rate/frame_rate relatively fixed.
    mmUInt32_t timestamp_interval;
    // timestamp initial.
    mmUInt32_t timestamp_initial;

    // default MM_RQ_I_OVERHEAD_DECODE_DEFAULT
    mmUInt32_t overhead_decode;

    // finish cursor.
    mmUInt32_t finish_index;
    // scroll cursor.
    mmUInt32_t scroll_index;

    // the next of max cursor.
    mmUInt32_t max_index;

    // user data.
    void* u;
};

MM_EXPORT_RQ void mmRqI_Init(struct mmRqI* p);
MM_EXPORT_RQ void mmRqI_Destroy(struct mmRqI* p);
MM_EXPORT_RQ void mmRqI_Reset(struct mmRqI* p);
MM_EXPORT_RQ void mmRqI_ResetContext(struct mmRqI* p);
MM_EXPORT_RQ void mmRqI_SetRate(struct mmRqI* p, mmUInt32_t sampling_rate, mmUInt32_t frame_rate);
MM_EXPORT_RQ void mmRqI_SetTimestampInitial(struct mmRqI* p, mmUInt32_t timestamp_initial);
MM_EXPORT_RQ void mmRqI_SetLength(struct mmRqI* p, mmUInt32_t length);
MM_EXPORT_RQ void mmRqI_SetChunkSize(struct mmRqI* p, mmUInt32_t chunk_size);
MM_EXPORT_RQ void mmRqI_SetOverhead(struct mmRqI* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqI_SetCallback(struct mmRqI* p, struct mmRqCallback* callback);
MM_EXPORT_RQ void mmRqI_SetContext(struct mmRqI* p, void* u);
MM_EXPORT_RQ void mmRqI_Clear(struct mmRqI* p);

// idx 0xffffffff is notify entire.
MM_EXPORT_RQ void mmRqI_AssemblySourced(struct mmRqI* p, void* idx, struct mmRqPacket* packet);
MM_EXPORT_RQ void mmRqI_FeedbackSourced(struct mmRqI* p, void* idx);
MM_EXPORT_RQ void mmRqI_AssemblyPerfect(struct mmRqI* p, void* idx, mmUInt32_t id);
MM_EXPORT_RQ void mmRqI_FeedbackPerfect(struct mmRqI* p, void* idx);

#include "core/mmSuffix.h"

#endif//__mmRqI_h__
