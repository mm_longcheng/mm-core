/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqPacket.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"
#include "core/mmByteOrder.h"
#include "core/mmBase128Varints.h"

MM_EXPORT_RQ void mmRqPacketHead_Init(struct mmRqPacketHead* p)
{
    p->index_time = 0;
    p->oti_common = 0;
    p->oti_scheme = 0;
    p->payload_id = 0;
}
MM_EXPORT_RQ void mmRqPacketHead_Destroy(struct mmRqPacketHead* p)
{
    p->index_time = 0;
    p->oti_common = 0;
    p->oti_scheme = 0;
    p->payload_id = 0;
}

MM_EXPORT_RQ void mmRqPacketHead_Reset(struct mmRqPacketHead* p)
{
    p->index_time = 0;
    p->oti_common = 0;
    p->oti_scheme = 0;
    p->payload_id = 0;
}

MM_EXPORT_RQ size_t mmRqPacketHead_BufferSize(struct mmRqPacketHead* p)
{
    size_t size = 0;
    size += mmBase128_UInt32Size(p->index_time);
    size += mmBase128_UInt64Size(p->oti_common);
    size += mmBase128_UInt32Size(p->oti_scheme);
    size += mmBase128_UInt32Size(p->payload_id);
    return size;
}

MM_EXPORT_RQ void mmRqPacketHead_Encode(struct mmRqPacketHead* p, struct mmByteBuffer* byte_buffer, size_t* offset)
{
    // "mmRqPacketHead_BufferSize(p) <= byte_buffer->length is invalid, buffer out of range."
    assert(mmRqPacketHead_BufferSize(p) <= byte_buffer->length && "buffer out of range");
    
    mmBase128_UInt32Encode(&p->index_time, byte_buffer, offset);
    mmBase128_UInt64Encode(&p->oti_common, byte_buffer, offset);
    mmBase128_UInt32Encode(&p->oti_scheme, byte_buffer, offset);
    mmBase128_UInt32Encode(&p->payload_id, byte_buffer, offset);
}

MM_EXPORT_RQ void mmRqPacketHead_Decode(struct mmRqPacketHead* p, struct mmByteBuffer* byte_buffer, size_t* offset)
{
    // "mmRqPacketHead_BufferSize(p) <= byte_buffer->length is invalid, buffer out of range."
    assert(mmRqPacketHead_BufferSize(p) <= byte_buffer->length && "buffer out of range");

    mmBase128_UInt32Decode(&p->index_time, byte_buffer, offset);
    mmBase128_UInt64Decode(&p->oti_common, byte_buffer, offset);
    mmBase128_UInt32Decode(&p->oti_scheme, byte_buffer, offset);
    mmBase128_UInt32Decode(&p->payload_id, byte_buffer, offset);
}

MM_EXPORT_RQ void mmRqPacketHead_SetIndexTime(struct mmRqPacketHead* p, mmUInt32_t index_time)
{
    p->index_time = index_time;
}
MM_EXPORT_RQ void mmRqPacketHead_SetPayloadId(struct mmRqPacketHead* p, mmUInt32_t payload_id)
{
    p->payload_id = payload_id;
}
MM_EXPORT_RQ void mmRqPacketHead_SetOtiCommon(struct mmRqPacketHead* p, mmUInt64_t oti_common)
{
    p->oti_common = oti_common;
}
MM_EXPORT_RQ void mmRqPacketHead_SetOtiScheme(struct mmRqPacketHead* p, mmUInt32_t oti_scheme)
{
    p->oti_scheme = oti_scheme;
}

MM_EXPORT_RQ int mmRqPacketHead_Validity(struct mmRqPacketHead* p)
{
    return MM_SUCCESS;
}

MM_EXPORT_RQ void mmRqPacket_Init(struct mmRqPacket* p)
{
    mmRqPacketHead_Init(&p->phead);
    mmByteBuffer_Init(&p->hbuff);
    mmByteBuffer_Init(&p->bbuff);
}
MM_EXPORT_RQ void mmRqPacket_Destroy(struct mmRqPacket* p)
{
    mmRqPacketHead_Destroy(&p->phead);
    mmByteBuffer_Destroy(&p->hbuff);
    mmByteBuffer_Destroy(&p->bbuff);
}
MM_EXPORT_RQ void mmRqPacket_Reset(struct mmRqPacket* p)
{
    mmRqPacketHead_Reset(&p->phead);
    mmByteBuffer_Reset(&p->hbuff);
    mmByteBuffer_Reset(&p->bbuff);
}
MM_EXPORT_RQ size_t mmRqPacket_BufferSize(struct mmRqPacket* p)
{
    return mmRqPacketHead_BufferSize(&p->phead);
}
MM_EXPORT_RQ void mmRqPacket_Encode(struct mmRqPacket* p, struct mmByteBuffer* hbuff, size_t* offset)
{
    mmRqPacketHead_Encode(&p->phead, hbuff, offset);
}
MM_EXPORT_RQ void mmRqPacket_Decode(struct mmRqPacket* p, struct mmByteBuffer* hbuff, size_t* offset)
{
    mmRqPacketHead_Decode(&p->phead, hbuff, offset);
}
MM_EXPORT_RQ void mmRqPacket_CopyWeak(struct mmRqPacket* p, struct mmRqPacket* t)
{
    size_t offset = 0;
    t->hbuff.length = p->hbuff.length;
    t->bbuff.length = p->bbuff.length;
    mmMemcpy(t->hbuff.buffer + t->hbuff.offset, p->hbuff.buffer + p->hbuff.offset, p->hbuff.length);
    mmMemcpy(t->bbuff.buffer + t->bbuff.offset, p->bbuff.buffer + p->bbuff.offset, p->bbuff.length);
    mmRqPacket_Decode(t, &t->hbuff, &offset);
}
MM_EXPORT_RQ int mmRqPacket_Validity(struct mmRqPacket* p)
{
    return mmRqPacketHead_Validity(&p->phead);
}

MM_EXPORT_RQ size_t mmRqPacket_StreambufOverdraft(struct mmStreambuf* p, struct mmRqPacket* pack)
{
    size_t msg_size = pack->hbuff.length + pack->bbuff.length;
    // if the idle put size is lack, we try remove the get buffer.
    mmStreambuf_AlignedMemory(p, msg_size);
    //
    pack->hbuff.buffer = p->buff;
    pack->hbuff.offset = (mmUInt32_t)(p->pptr);
    pack->bbuff.buffer = p->buff;
    pack->bbuff.offset = pack->hbuff.offset + pack->hbuff.length;
    mmStreambuf_Pbump(p, msg_size);
    return msg_size;
}
MM_EXPORT_RQ size_t mmRqPacket_StreambufRepayment(struct mmStreambuf* p, struct mmRqPacket* pack)
{
    size_t msg_size = pack->hbuff.length + pack->bbuff.length;
    mmStreambuf_Gbump(p, msg_size);
    return msg_size;
}

MM_EXPORT_RQ mmUInt16_t mmRqPacket_SymbolsSize(size_t transfer_length)
{
    mmUInt64_t x = 0;
    x = transfer_length;
    mmRoundup64(x);
    x = x > MM_RQ_PACKET_SYMBOLS_SIZE_MIN ? x : MM_RQ_PACKET_SYMBOLS_SIZE_MIN;
    x = x < MM_RQ_PACKET_SYMBOLS_SIZE_MAX ? x : MM_RQ_PACKET_SYMBOLS_SIZE_MAX;
    return (mmUInt16_t)x;
}

