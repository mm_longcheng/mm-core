/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqFountain.h"
#include "mmRqProtocol.h"

#include "core/mmLogger.h"

MM_EXPORT_RQ void mmRqFountain_Init(struct mmRqFountain* p)
{
    mmSockaddr_Init(&p->ss_remote);
    mmStreambuf_Init(&p->streambuf_packet);
    mmRqI_Init(&p->rq_i);
    mmRqO_Init(&p->rq_o);
    mmSpinlock_Init(&p->locker_s, NULL);
    mmSpinlock_Init(&p->locker_i, NULL);
    mmSpinlock_Init(&p->locker_o, NULL);
    p->identity = 0xffffffff;
    p->max_repaird_number = MM_RQ_FOUNTAIN_MAX_REPAIRD_NUMBER_DEFAULT;
    p->max_perfect_number = MM_RQ_FOUNTAIN_MAX_PERFECT_NUMBER_DEFAULT;
    p->rq_i_packet_id = 0;
    p->rq_o_packet_id = 0;
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->timecode_rtt = 0;
    p->timecode_i_packet_number_update = 0;
    p->timecode_o_packet_number_update = 0;
    p->timecode_i_packet_indexp_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->rq_i_packet_number_interval = 0;
    p->rq_o_packet_number_interval = 0;
    p->state = MM_RQ_CLOSED;
}
MM_EXPORT_RQ void mmRqFountain_Destroy(struct mmRqFountain* p)
{
    mmSockaddr_Destroy(&p->ss_remote);
    mmStreambuf_Destroy(&p->streambuf_packet);
    mmRqI_Destroy(&p->rq_i);
    mmRqO_Destroy(&p->rq_o);
    mmSpinlock_Destroy(&p->locker_s);
    mmSpinlock_Destroy(&p->locker_i);
    mmSpinlock_Destroy(&p->locker_o);
    p->identity = 0xffffffff;
    p->max_repaird_number = MM_RQ_FOUNTAIN_MAX_REPAIRD_NUMBER_DEFAULT;
    p->max_perfect_number = MM_RQ_FOUNTAIN_MAX_PERFECT_NUMBER_DEFAULT;
    p->rq_i_packet_id = 0;
    p->rq_o_packet_id = 0;
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->timecode_rtt = 0;
    p->timecode_i_packet_number_update = 0;
    p->timecode_o_packet_number_update = 0;
    p->timecode_i_packet_indexp_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->rq_i_packet_number_interval = 0;
    p->rq_o_packet_number_interval = 0;
    p->state = MM_RQ_CLOSED;
}

MM_EXPORT_RQ void mmRqFountain_Reset(struct mmRqFountain* p)
{
    mmSockaddr_Reset(&p->ss_remote);
    mmStreambuf_Reset(&p->streambuf_packet);
    mmRqI_Reset(&p->rq_i);
    mmRqO_Reset(&p->rq_o);
    // spinlock not need reset.
    p->identity = 0xffffffff;
    p->max_repaird_number = MM_RQ_FOUNTAIN_MAX_REPAIRD_NUMBER_DEFAULT;
    p->max_perfect_number = MM_RQ_FOUNTAIN_MAX_PERFECT_NUMBER_DEFAULT;
    p->rq_i_packet_id = 0;
    p->rq_o_packet_id = 0;
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->timecode_rtt = 0;
    p->timecode_i_packet_number_update = 0;
    p->timecode_o_packet_number_update = 0;
    p->timecode_i_packet_indexp_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->rq_i_packet_number_interval = 0;
    p->rq_o_packet_number_interval = 0;
    p->state = MM_RQ_CLOSED;
}
MM_EXPORT_RQ void mmRqFountain_ResetContext(struct mmRqFountain* p)
{
    mmStreambuf_Reset(&p->streambuf_packet);
    mmRqI_ResetContext(&p->rq_i);
    mmRqO_ResetContext(&p->rq_o);
    // spinlock not need reset.
    // p->identity not need reset.
    // max_repaird_number not need reset.
    // max_perfect_number not need reset.
    p->rq_i_packet_id = 0;
    p->rq_o_packet_id = 0;
    p->timecode_create = 0;
    p->timecode_update = 0;
    p->timecode_rtt = 0;
    p->timecode_i_packet_number_update = 0;
    p->timecode_o_packet_number_update = 0;
    p->timecode_i_packet_indexp_update = 0;
    p->timecode_o_packet_indexp_update = 0;
    p->rq_i_packet_number_interval = 0;
    p->rq_o_packet_number_interval = 0;
    p->state = MM_RQ_CLOSED;
}

MM_EXPORT_RQ void mmRqFountain_Lock(struct mmRqFountain* p)
{
    mmRqFountain_SLock(p);
    mmRqFountain_ILock(p);
    mmRqFountain_OLock(p);
}
MM_EXPORT_RQ void mmRqFountain_Unlock(struct mmRqFountain* p)
{
    mmRqFountain_OUnlock(p);
    mmRqFountain_IUnlock(p);
    mmRqFountain_SUnlock(p);
}
MM_EXPORT_RQ void mmRqFountain_SLock(struct mmRqFountain* p)
{
    mmSpinlock_Lock(&p->locker_s);
}
MM_EXPORT_RQ void mmRqFountain_SUnlock(struct mmRqFountain* p)
{
    mmSpinlock_Unlock(&p->locker_s);
}
MM_EXPORT_RQ void mmRqFountain_ILock(struct mmRqFountain* p)
{
    mmSpinlock_Lock(&p->locker_i);
}
MM_EXPORT_RQ void mmRqFountain_IUnlock(struct mmRqFountain* p)
{
    mmSpinlock_Unlock(&p->locker_i);
}
//
MM_EXPORT_RQ void mmRqFountain_OLock(struct mmRqFountain* p)
{
    mmSpinlock_Lock(&p->locker_o);
}
MM_EXPORT_RQ void mmRqFountain_OUnlock(struct mmRqFountain* p)
{
    mmSpinlock_Unlock(&p->locker_o);
}

MM_EXPORT_RQ void mmRqFountain_SetRemote(struct mmRqFountain* p, const char* node, mmUShort_t port)
{
    mmSockaddr_Assign(&p->ss_remote, node, port);
}
MM_EXPORT_RQ void mmRqFountain_SetRemoteStorage(struct mmRqFountain* p, struct mmSockaddr* remote)
{
    mmSockaddr_CopyFrom(&p->ss_remote, remote);
}
MM_EXPORT_RQ void mmRqFountain_SetRqIPacketId(struct mmRqFountain* p, mmUInt32_t rq_i_packet_id)
{
    p->rq_i_packet_id = rq_i_packet_id;
}
MM_EXPORT_RQ void mmRqFountain_SetRqOPacketId(struct mmRqFountain* p, mmUInt32_t rq_o_packet_id)
{
    p->rq_o_packet_id = rq_o_packet_id;
}
MM_EXPORT_RQ void mmRqFountain_SetTimecodeCreate(struct mmRqFountain* p, mmUInt64_t timecode_create)
{
    p->timecode_create = timecode_create;
}
MM_EXPORT_RQ void mmRqFountain_SetTimecodeUpdate(struct mmRqFountain* p, mmUInt64_t timecode_update)
{
    p->timecode_update = timecode_update;
}
MM_EXPORT_RQ void mmRqFountain_SetTimecodeRtt(struct mmRqFountain* p, mmUInt64_t timecode_rtt)
{
    p->timecode_rtt = timecode_rtt;
}
MM_EXPORT_RQ void mmRqFountain_SetState(struct mmRqFountain* p, mmUInt32_t state)
{
    p->state = state;
}
MM_EXPORT_RQ void mmRqFountain_SetIdentity(struct mmRqFountain* p, mmUInt32_t identity)
{
    p->identity = identity;
}
MM_EXPORT_RQ void mmRqFountain_SetMaxRepairdNumber(struct mmRqFountain* p, mmUInt32_t max_repaird_number)
{
    p->max_repaird_number = max_repaird_number;
}
MM_EXPORT_RQ void mmRqFountain_SetMaxPerfectNumber(struct mmRqFountain* p, mmUInt32_t max_perfect_number)
{
    p->max_perfect_number = max_perfect_number;
}
MM_EXPORT_RQ void mmRqFountain_SetRate(struct mmRqFountain* p, mmUInt32_t sampling_rate, mmUInt32_t frame_rate)
{
    mmRqI_SetRate(&p->rq_i, sampling_rate, frame_rate);
    mmRqO_SetRate(&p->rq_o, sampling_rate, frame_rate);
}
MM_EXPORT_RQ void mmRqFountain_SetTimestampInitial(struct mmRqFountain* p, mmUInt32_t timestamp_initial)
{
    mmRqI_SetTimestampInitial(&p->rq_i, timestamp_initial);
    mmRqO_SetTimestampInitial(&p->rq_o, timestamp_initial);
}
MM_EXPORT_RQ void mmRqFountain_SetLength(struct mmRqFountain* p, mmUInt32_t length)
{
    mmRqI_SetLength(&p->rq_i, length);
    mmRqO_SetLength(&p->rq_o, length);
}
MM_EXPORT_RQ void mmRqFountain_SetChunkSize(struct mmRqFountain* p, mmUInt32_t chunk_size)
{
    mmRqI_SetChunkSize(&p->rq_i, chunk_size);
    mmRqO_SetChunkSize(&p->rq_o, chunk_size);
}
MM_EXPORT_RQ void mmRqFountain_SetOverheadEncode(struct mmRqFountain* p, mmUInt32_t overhead)
{
    mmRqO_SetOverhead(&p->rq_o, overhead);
}
MM_EXPORT_RQ void mmRqFountain_SetOverheadDecode(struct mmRqFountain* p, mmUInt32_t overhead)
{
    mmRqI_SetOverhead(&p->rq_i, overhead);
}
MM_EXPORT_RQ void mmRqFountain_SetPacketAlign(struct mmRqFountain* p, mmUInt8_t packet_align)
{
    mmRqO_SetPacketAlign(&p->rq_o, packet_align);
}
MM_EXPORT_RQ void mmRqFountain_SetExpectedLoss(struct mmRqFountain* p, float expected_loss)
{
    mmRqO_SetExpectedLoss(&p->rq_o, expected_loss);
}

MM_EXPORT_RQ void mmRqFountain_SetIPacketIndexpUpdate(struct mmRqFountain* p, mmUInt64_t timecode)
{
    p->timecode_i_packet_indexp_update = timecode;
}
MM_EXPORT_RQ void mmRqFountain_SetOPacketIndexpUpdate(struct mmRqFountain* p, mmUInt64_t timecode)
{
    p->timecode_o_packet_indexp_update = timecode;
}
MM_EXPORT_RQ void mmRqFountain_SetIPacketNumberUpdate(struct mmRqFountain* p, mmUInt64_t timecode)
{
    p->timecode_i_packet_number_update = timecode;
    p->rq_i_packet_number_interval = p->rq_i.packet_number;
}
MM_EXPORT_RQ void mmRqFountain_SetOPacketNumberUpdate(struct mmRqFountain* p, mmUInt64_t timecode)
{
    p->timecode_o_packet_number_update = timecode;
    p->rq_o_packet_number_interval = p->rq_o.packet_number;
}

MM_EXPORT_RQ void mmRqFountain_IncreaseRqIPacketId(struct mmRqFountain* p)
{
    p->rq_i_packet_id++;
}
MM_EXPORT_RQ void mmRqFountain_IncreaseRqOPacketId(struct mmRqFountain* p)
{
    p->rq_o_packet_id++;
}

MM_EXPORT_RQ int
mmRqFountain_SendMessage(
    struct mmRqFountain* p,
    void* idx,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int code = -1;
    struct mmByteBuffer byte_buffer;
    struct mmStreambuf* streambuf = NULL;

    streambuf = &p->streambuf_packet;

    mmStreambuf_Reset(streambuf);

    code = mmMessageCoder_AppendPacketHead(streambuf, coder, packet_head, rq_msg, hlength, rq_pack);

    byte_buffer.buffer = (mmUInt8_t*)streambuf->buff;
    byte_buffer.offset = (size_t)streambuf->gptr;
    byte_buffer.length = (size_t)mmStreambuf_Size(streambuf);

    mmRqO_GenerateSourced(&p->rq_o, idx, p->rq_o_packet_id, &byte_buffer);

    mmRqFountain_IncreaseRqOPacketId(p);

    return code;
}

MM_EXPORT_RQ int
mmRqFountain_SendBuffer(
    struct mmRqFountain* p,
    void* idx,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet)
{
    int code = -1;
    struct mmByteBuffer byte_buffer;
    struct mmStreambuf* streambuf = NULL;

    streambuf = &p->streambuf_packet;

    mmStreambuf_Reset(streambuf);

    code = mmMessageCoder_EncodeMessageByteBuffer(streambuf, buffer, packet_head, hlength, packet);

    byte_buffer.buffer = (mmUInt8_t*)streambuf->buff;
    byte_buffer.offset = (size_t)streambuf->gptr;
    byte_buffer.length = (size_t)mmStreambuf_Size(streambuf);

    mmRqO_GenerateSourced(&p->rq_o, idx, p->rq_o_packet_id, &byte_buffer);

    mmRqFountain_IncreaseRqOPacketId(p);
    return code;
}

MM_EXPORT_RQ void
mmRqFountain_RecvBuffer(
    struct mmRqFountain* p,
    void* idx,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    size_t offset = 0;
    struct mmRqPacket packet;

    mmRqPacket_Init(&packet);

    mmRqPacket_Decode(&packet, &pack->bbuff, &offset);

    packet.hbuff.buffer = (mmUInt8_t*)(pack->bbuff.buffer);
    packet.hbuff.offset = (size_t)(pack->bbuff.offset);
    packet.hbuff.length = (size_t)(offset);

    packet.bbuff.buffer = (mmUInt8_t*)(pack->bbuff.buffer);
    packet.bbuff.offset = (size_t)(pack->bbuff.offset + offset);
    packet.bbuff.length = (size_t)(pack->bbuff.length - offset);

    mmRqI_AssemblySourced(&p->rq_i, idx, &packet);

    mmRqPacket_Destroy(&packet);
}

MM_EXPORT_RQ void
mmRqFountain_GenerateSymbolsO(
    struct mmRqFountain* p,
    void* idx,
    mmUInt32_t id,
    struct mmByteBuffer* byte_buffer)
{
    mmRqO_GenerateSymbols(&p->rq_o, idx, id, byte_buffer);
}

MM_EXPORT_RQ void mmRqFountain_GenerateRepairdO(struct mmRqFountain* p, void* idx, mmUInt32_t max_index)
{
    mmUInt32_t id = max_index;
    mmUInt32_t n = 0;

    if (max_index < p->rq_o.scroll_index)
    {
        // need repaird from rq file format at outside.
        id = p->rq_o.scroll_index;
        n += p->rq_o.scroll_index - max_index;
    }
    {
        // we need limit the max repaird number.
        while (n < p->max_repaird_number && id < p->rq_o_packet_id)
        {
            mmRqO_GenerateRepaird(&p->rq_o, idx, id);
            //
            id++;
            n++;
        }
    }
}
MM_EXPORT_RQ void mmRqFountain_AssemblyPerfectI(struct mmRqFountain* p, void* idx, mmUInt32_t min_index)
{
    mmUInt32_t id = p->rq_i.scroll_index;
    mmUInt32_t n = 0;

    // we need limit the max perfect number.
    while (n < p->max_perfect_number && id < min_index)
    {
        mmRqI_AssemblyPerfect(&p->rq_i, idx, id);
        //
        id++;
        n++;
    }
}
MM_EXPORT_RQ void mmRqFountain_AssemblyPerfectO(struct mmRqFountain* p, void* idx, mmUInt32_t min_index)
{
    mmUInt32_t id = p->rq_o.scroll_index;
    mmUInt32_t n = 0;

    // we need limit the max perfect number.
    while (n < p->max_perfect_number && id < min_index)
    {
        mmRqO_AssemblyPerfect(&p->rq_o, idx, id);
        //
        id++;
        n++;
    }
}
MM_EXPORT_RQ void mmRqFountain_FeedbackPerfectI(struct mmRqFountain* p, void* idx)
{
    mmRqI_FeedbackPerfect(&p->rq_i, idx);
}
MM_EXPORT_RQ void mmRqFountain_FeedbackPerfectO(struct mmRqFountain* p, void* idx)
{
    mmRqO_FeedbackPerfect(&p->rq_o, idx);
}

MM_EXPORT_RQ void mmRqFountain_PoolElementEventProduce(void* obj, void* u, void* v)
{
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    struct mmRqFountain* e = (struct mmRqFountain*)(v);
    mmRqFountain_Init(e);
}
MM_EXPORT_RQ void mmRqFountain_PoolElementEventRecycle(void* obj, void* u, void* v)
{
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    struct mmRqFountain* e = (struct mmRqFountain*)(v);
    mmRqFountain_Destroy(e);
}

