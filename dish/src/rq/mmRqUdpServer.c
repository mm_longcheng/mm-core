/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqUdpServer.h"
#include "mmRqProtocol.h"

#include "core/mmLogger.h"
#include "core/mmLimit.h"

#include "net/mmDefaultHandle.h"
#include "net/mmStreambufPacket.h"

struct __mmRqUdpServerPacketIdxArgument
{
    struct mmRqUdpServer* rq_udp_server;
    struct mmRqFountain* fountain;
    struct mmUdp* udp;
};
struct __mmRqUdpServerPacketNumberArgument
{
    struct mmRqUdpServer* rq_udp_server;
    mmUInt32_t fountain_number;
    float sampled_summate;
};
struct __mmRqUdpServerPacketIndexpArgument
{
    struct mmRqUdpServer* rq_udp_server;
    mmUInt32_t fountain_number;
    mmUInt32_t min_index;
    mmUInt32_t max_index;
};
struct __mmRqUdpServerPacketDatabaseArgument
{
    struct mmRqUdpServer* rq_udp_server;
    struct mmRqFountain* fountain;
    void* idx;
};

typedef void(*__mmRqUdpServerMidHandleFunc)(
    void* obj,
    void* u,
    struct mmRqFountain* f,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

static void __static_mmRqUdpServer_HeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_HeadsetBroken(void* obj);
static void __static_mmRqUdpServer_HeadsetNready(void* obj);
static void __static_mmRqUdpServer_HeadsetFinish(void* obj);

static void
__static_mmRqUdpServer_HeadsetMidEventHandle(
    void* obj,
    struct mmRqFountain* f,
    mmUInt32_t mid,
    __mmRqUdpServerMidHandleFunc callback);

static void __static_mmRqUdpServer_HeadsetRqBrokenEvent(void* obj, struct mmRqFountain* f);
static void __static_mmRqUdpServer_HeadsetRqNreadyEvent(void* obj, struct mmRqFountain* f);
static void __static_mmRqUdpServer_HeadsetRqFinishEvent(void* obj, struct mmRqFountain* f);

static void __static_mmRqUdpServer_HeadsetNtBrokenEvent(void* obj, struct mmRqFountain* f);
static void __static_mmRqUdpServer_HeadsetNtNreadyEvent(void* obj, struct mmRqFountain* f);
static void __static_mmRqUdpServer_HeadsetNtFinishEvent(void* obj, struct mmRqFountain* f);

static void __static_mmRqUdpServer_UnicastHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_MulcastHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_SynNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_AckNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_FinNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_RstNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_SyncAckHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void __static_mmRqUdpServer_PingReqHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_PingResHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_LossUpdHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_IdxsUpdHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote);

static void
__static_mmRqUdpServer_UnicastICallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet);
static void __static_mmRqUdpServer_UnicastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpServer_UnicastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);

static void
__static_mmRqUdpServer_UnicastOCallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet);
static void __static_mmRqUdpServer_UnicastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpServer_UnicastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);

static void
__static_mmRqUdpServer_MulcastICallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet);
static void __static_mmRqUdpServer_MulcastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpServer_MulcastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);

static void
__static_mmRqUdpServer_MulcastOCallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet);
static void __static_mmRqUdpServer_MulcastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e);
static void __static_mmRqUdpServer_MulcastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e);

static void __static_mmRqUdpServer_UnicastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);
static void __static_mmRqUdpServer_MulcastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote);

static void
__static_mmRqUdpServer_NHeadsetHandle(
    void* obj,
    void* u,
    struct mmRqFountain* f,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

static void
__static_mmRqUdpServer_SendRepeatedMassageUnicast(
    struct mmRqUdpServer* p,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmByteBuffer* buffer,
    struct mmSockaddr* remote);

static void __static_mmRqUdpServer_EmptyHandleForCommonEvent(struct mmRqUdpServer* p);

static void __static_mmRqUdpServer_MsecUpdateStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
static void __static_mmRqUdpServer_MsecUpdateLossNumberHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
static void __static_mmRqUdpServer_MsecUpdateLossIndexpHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);

static void __static_mmRqUdpServer_MsecUpdateLossStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);
static void __static_mmRqUdpServer_MsecUpdateIdxsStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry);

static void
__static_mmRqUdpServer_MsecUpdateMulcastOPersistenceHandle(
    struct mmTimerHeap* timer_heap,
    struct mmTimerEntry* entry);

static void __static_mmRqUdpServer_ShutdownSocketRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v);

static void __static_mmRqUdpServer_UpdateLossStateRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v);
static void __static_mmRqUdpServer_UpdateIdxsStateRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v);

static void __static_mmRqUdpServer_NumberCalculationRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v);
static void __static_mmRqUdpServer_IndexpCalculationRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v);

static void __static_mmRqUdpServer_MulcastOPersistenceHandle(struct mmRqUdpServer* p);

static void
__static_mmRqUdpServer_ORepairdHandle(
    struct mmRqUdpServer* p,
    struct mmRqFountain* fountain,
    void* idx,
    mmUInt32_t max_index);

static void __static_mmRqUdpServer_ORepairdDatabaseSelect(void* obj, mmUInt32_t i, struct mmByteBuffer* byte_buffer, void* v);

static void
__static_mmRqUdpServer_Handle(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{

}

MM_EXPORT_RQ void mmRqUdpServerCallback_Init(struct mmRqUdpServerCallback* p)
{
    p->Handle = &__static_mmRqUdpServer_Handle;
    p->obj = NULL;
}
MM_EXPORT_RQ void mmRqUdpServerCallback_Destroy(struct mmRqUdpServerCallback* p)
{
    p->Handle = &__static_mmRqUdpServer_Handle;
    p->obj = NULL;
}

MM_EXPORT_RQ void
mmRqUdpServer_HandleDefault(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    char link_name[MM_LINK_NAME_LENGTH] = { 0 };
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmLogger* gLogger = mmLogger_Instance();
    mmSockaddr_NativeRemoteString(remote, &udp->socket.ss_native, udp->socket.socket, link_name);
    mmLogger_LogW(gLogger, "%s %d mid:0x%08X udp:%s not handle.", __FUNCTION__, __LINE__, pack->phead.mid, link_name);
}

MM_EXPORT_RQ void mmRqUdpServer_Init(struct mmRqUdpServer* p)
{
    struct mmPoolElementAllocator hAllocator;
    struct mmHeadsetCallback headset_callback;
    struct mmRqCallback rq_mulcast_i_callback;
    struct mmRqCallback rq_mulcast_o_callback;

    mmHeadset_Init(&p->headset);
    mmXoshiro256starstar_Init(&p->random_context);
    mmPoolElement_Init(&p->pool_element);

    mmRbtreeVisibleU32_Init(&p->rbtree_visible);
    mmRbtreeSockaddrVpt_Init(&p->rbtree_sockaddr_vpt);

    mmRqFountain_Init(&p->fountain_mulcast);
    mmUInt32IdGenerater_Init(&p->id_generater);

    mmRqDatabase_Init(&p->database_mulcast_o);

    mmTimer_Init(&p->timer);

    mmRbtreeU32Vpt_Init(&p->rbtree_unicast);
    mmRbtreeU32Vpt_Init(&p->rbtree_mulcast);

    mmRqUdpServerCallback_Init(&p->callback_unicast_n);
    mmRqUdpServerCallback_Init(&p->callback_mulcast_n);

    mmString_Init(&p->mr_multiaddr);
    mmString_Init(&p->mr_interface);

    mmSpinlock_Init(&p->rbtree_locker_unicast, NULL);
    mmSpinlock_Init(&p->rbtree_locker_mulcast, NULL);

    mmSpinlock_Init(&p->instance_locker, NULL);
    mmSpinlock_Init(&p->rbtree_sockaddr_locker, NULL);
    mmSpinlock_Init(&p->id_generater_locker, NULL);

    mmSpinlock_Init(&p->locker, NULL);

    p->msec_timeout_msl = MM_RQ_UDP_SERVER_MSL_DEFAULT;

    p->msec_update_state = MM_RQ_UDP_SERVER_MSEC_UPDATE_STATE;
    p->msec_update_loss_number = MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_NUMBER;
    p->msec_update_loss_indexp = MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_INDEXP;

    p->msec_update_loss_state = MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_STATE;
    p->msec_update_idxp_state = MM_RQ_UDP_SERVER_MSEC_UPDATE_IDXP_STATE;

    p->unicast_length = MM_RQ_UNICAST_LENGTH_DEFAULT;
    p->unicast_overhead_encode = MM_RQ_UDP_SERVER_O_OVERHEAD_ENCODE_DEFAULT;
    p->unicast_overhead_decode = MM_RQ_UDP_SERVER_I_OVERHEAD_DECODE_DEFAULT;

    p->msec_update_mulcast_o_persistence = MM_RQ_UDP_SERVER_MSEC_UPDATE_MULCAST_O_PERSISTENCE;

    p->repeated_number = MM_RQ_UDP_SERVER_COMMON_MESSAGE_REPEATED_NUMBER;

    p->mulcast_io_timestamp_initial = 0;
    p->mulcast_min_index = 0;
    p->mulcast_max_index = 0;
    p->mulcast_expected_loss = MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT;

    p->u = NULL;

    headset_callback.Handle = &__static_mmRqUdpServer_HeadsetHandle;
    headset_callback.Broken = &__static_mmRqUdpServer_HeadsetBroken;
    headset_callback.Nready = &__static_mmRqUdpServer_HeadsetNready;
    headset_callback.Finish = &__static_mmRqUdpServer_HeadsetFinish;
    headset_callback.obj = p;
    mmHeadset_SetHeadsetCallback(&p->headset, &headset_callback);

    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_Unicast, &__static_mmRqUdpServer_UnicastHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_Mulcast, &__static_mmRqUdpServer_MulcastHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_SynNone, &__static_mmRqUdpServer_SynNoneHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_AckNone, &__static_mmRqUdpServer_AckNoneHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_FinNone, &__static_mmRqUdpServer_FinNoneHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_RstNone, &__static_mmRqUdpServer_RstNoneHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_SyncAck, &__static_mmRqUdpServer_SyncAckHeadsetHandle);

    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_PingReq, &__static_mmRqUdpServer_PingReqHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_PingRes, &__static_mmRqUdpServer_PingResHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_LossUpd, &__static_mmRqUdpServer_LossUpdHeadsetHandle);
    mmHeadset_SetHandle(&p->headset, mmRqMidUInt32_IdxsUpd, &__static_mmRqUdpServer_IdxsUpdHeadsetHandle);

    rq_mulcast_i_callback.Handle = &__static_mmRqUdpServer_MulcastICallbackHandle;
    rq_mulcast_i_callback.Finish = &__static_mmRqUdpServer_MulcastICallbackFinish;
    rq_mulcast_i_callback.Scroll = &__static_mmRqUdpServer_MulcastICallbackScroll;
    rq_mulcast_i_callback.obj = p;
    mmRqI_SetCallback(&p->fountain_mulcast.rq_i, &rq_mulcast_i_callback);

    rq_mulcast_o_callback.Handle = &__static_mmRqUdpServer_MulcastOCallbackHandle;
    rq_mulcast_o_callback.Finish = &__static_mmRqUdpServer_MulcastOCallbackFinish;
    rq_mulcast_o_callback.Scroll = &__static_mmRqUdpServer_MulcastOCallbackScroll;
    rq_mulcast_o_callback.obj = p;
    mmRqO_SetCallback(&p->fountain_mulcast.rq_o, &rq_mulcast_o_callback);

    mmRqI_SetLength(&p->fountain_mulcast.rq_i, MM_RQ_UDP_SERVER_I_MULCAST_LENGTH_DEFAULT);
    mmRqO_SetLength(&p->fountain_mulcast.rq_o, MM_RQ_UDP_SERVER_O_MULCAST_LENGTH_DEFAULT);

    hAllocator.Produce = &mmRqUdp_Init;
    hAllocator.Recycle = &mmRqUdp_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmRqUdp));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);

    mmRqUdpServer_SetFountainChunkSize(p, MM_RQ_UDP_SERVER_FOUNTAIN_CHUNK_SIZE_DEFAULT);

    mmString_Assigns(&p->mr_multiaddr, MM_RQ_UDP_SERVER_MR_MULTIADDR_DEFAULT);
    mmString_Assigns(&p->mr_interface, MM_RQ_UDP_SERVER_MR_INTERFACE_DEFAULT);

    // seed init.
    mmXoshiro256starstar_Srand(&p->random_context, (mmUInt64_t)time(NULL));
    //
    // init fountain_mulcast io timestamp_initial.
    mmRqUdpServer_ResetMulcastContext(p);
    mmRqFountain_SetState(&p->fountain_mulcast, MM_RQ_ESTABLISHED);
    mmRqFountain_SetExpectedLoss(&p->fountain_mulcast, MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT);
    mmRqFountain_SetOverheadEncode(&p->fountain_mulcast, MM_RQ_UDP_SERVER_O_OVERHEAD_ENCODE_DEFAULT);
    mmRqFountain_SetOverheadDecode(&p->fountain_mulcast, MM_RQ_UDP_SERVER_I_OVERHEAD_DECODE_DEFAULT);
    //
    mmTimer_Schedule(
        &p->timer,
        p->msec_update_state,
        p->msec_update_state,
        &__static_mmRqUdpServer_MsecUpdateStateHandle,
        p);
    
    mmTimer_Schedule(
        &p->timer,
        p->msec_update_loss_number,
        p->msec_update_loss_number,
        &__static_mmRqUdpServer_MsecUpdateLossNumberHandle,
        p);
    
    mmTimer_Schedule(
        &p->timer,
        p->msec_update_loss_indexp,
        p->msec_update_loss_indexp,
        &__static_mmRqUdpServer_MsecUpdateLossIndexpHandle,
        p);

    mmTimer_Schedule(
        &p->timer,
        p->msec_update_loss_state,
        p->msec_update_loss_state,
        &__static_mmRqUdpServer_MsecUpdateLossStateHandle,
        p);
    
    mmTimer_Schedule(
        &p->timer,
        p->msec_update_idxp_state,
        p->msec_update_idxp_state,
        &__static_mmRqUdpServer_MsecUpdateIdxsStateHandle,
        p);

    mmTimer_Schedule(
        &p->timer,
        p->msec_update_mulcast_o_persistence,
        p->msec_update_mulcast_o_persistence,
        &__static_mmRqUdpServer_MsecUpdateMulcastOPersistenceHandle,
        p);

    // empty handle for common event.
    __static_mmRqUdpServer_EmptyHandleForCommonEvent(p);
}
MM_EXPORT_RQ void mmRqUdpServer_Destroy(struct mmRqUdpServer* p)
{
    mmRqUdpServer_Clear(p);

    mmRqUdpServer_ClearHandleRbtree(p);
    // empty handle for common event.
    __static_mmRqUdpServer_EmptyHandleForCommonEvent(p);
    //
    mmHeadset_Destroy(&p->headset);
    mmXoshiro256starstar_Destroy(&p->random_context);
    mmPoolElement_Destroy(&p->pool_element);

    mmRbtreeVisibleU32_Destroy(&p->rbtree_visible);
    mmRbtreeSockaddrVpt_Destroy(&p->rbtree_sockaddr_vpt);

    mmRqFountain_Destroy(&p->fountain_mulcast);
    mmUInt32IdGenerater_Destroy(&p->id_generater);

    mmRqDatabase_Destroy(&p->database_mulcast_o);

    mmTimer_Destroy(&p->timer);

    mmRbtreeU32Vpt_Destroy(&p->rbtree_unicast);
    mmRbtreeU32Vpt_Destroy(&p->rbtree_mulcast);

    mmRqUdpServerCallback_Destroy(&p->callback_unicast_n);
    mmRqUdpServerCallback_Destroy(&p->callback_mulcast_n);

    mmString_Destroy(&p->mr_multiaddr);
    mmString_Destroy(&p->mr_interface);

    mmSpinlock_Destroy(&p->rbtree_locker_unicast);
    mmSpinlock_Destroy(&p->rbtree_locker_mulcast);

    mmSpinlock_Destroy(&p->instance_locker);
    mmSpinlock_Destroy(&p->rbtree_sockaddr_locker);
    mmSpinlock_Destroy(&p->id_generater_locker);

    mmSpinlock_Destroy(&p->locker);

    p->msec_timeout_msl = MM_RQ_UDP_SERVER_MSL_DEFAULT;

    p->msec_update_state = MM_RQ_UDP_SERVER_MSEC_UPDATE_STATE;
    p->msec_update_loss_number = MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_NUMBER;
    p->msec_update_loss_indexp = MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_INDEXP;

    p->msec_update_loss_state = MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_STATE;
    p->msec_update_idxp_state = MM_RQ_UDP_SERVER_MSEC_UPDATE_IDXP_STATE;

    p->unicast_length = MM_RQ_UNICAST_LENGTH_DEFAULT;
    p->unicast_overhead_encode = MM_RQ_UDP_SERVER_O_OVERHEAD_ENCODE_DEFAULT;
    p->unicast_overhead_decode = MM_RQ_UDP_SERVER_I_OVERHEAD_DECODE_DEFAULT;

    p->repeated_number = MM_RQ_UDP_SERVER_COMMON_MESSAGE_REPEATED_NUMBER;

    p->mulcast_io_timestamp_initial = 0;
    p->mulcast_min_index = 0;
    p->mulcast_max_index = 0;
    p->mulcast_expected_loss = MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT;

    p->u = NULL;
}

MM_EXPORT_RQ void mmRqUdpServer_Lock(struct mmRqUdpServer* p)
{
    mmHeadset_Lock(&p->headset);
    mmPoolElement_Lock(&p->pool_element);
    mmRqFountain_Lock(&p->fountain_mulcast);
    mmTimer_Lock(&p->timer);
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_RQ void mmRqUdpServer_Unlock(struct mmRqUdpServer* p)
{
    mmSpinlock_Unlock(&p->locker);
    mmTimer_Unlock(&p->timer);
    mmRqFountain_Unlock(&p->fountain_mulcast);
    mmPoolElement_Unlock(&p->pool_element);
    mmHeadset_Unlock(&p->headset);
}

MM_EXPORT_RQ void mmRqUdpServer_ResetMulcastContext(struct mmRqUdpServer* p)
{
    mmUInt32_t random_number = 0;
    
    mmRqFountain_ResetContext(&p->fountain_mulcast);

    p->mulcast_io_timestamp_initial = 0;
    p->mulcast_min_index = 0;
    p->mulcast_max_index = 0;
    p->mulcast_expected_loss = MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT;

    random_number = (mmUInt32_t)(mmXoshiro256starstar_Next(&p->random_context) % MM_UINT32_MAX);
    p->fountain_mulcast.rq_i.timestamp_initial = random_number;
    random_number = (mmUInt32_t)(mmXoshiro256starstar_Next(&p->random_context) % MM_UINT32_MAX);
    p->fountain_mulcast.rq_o.timestamp_initial = random_number;
    
    p->mulcast_io_timestamp_initial = mmByte_UInt64A(
        p->fountain_mulcast.rq_o.timestamp_initial,
        p->fountain_mulcast.rq_i.timestamp_initial);
    
    mmRqFountain_SetState(&p->fountain_mulcast, MM_RQ_ESTABLISHED);
}

MM_EXPORT_RQ void mmRqUdpServer_SetFountainChunkSize(struct mmRqUdpServer* p, mmUInt32_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool_element, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}

MM_EXPORT_RQ void mmRqUdpServer_SetUnicastLength(struct mmRqUdpServer* p, mmUInt32_t length)
{
    p->unicast_length = length;
}
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastLength(struct mmRqUdpServer* p, mmUInt32_t length)
{
    mmRqFountain_SetLength(&p->fountain_mulcast, length);
}

MM_EXPORT_RQ void mmRqUdpServer_SetDatabaseMulcastOFilepath(struct mmRqUdpServer* p, const char* filepath)
{
    mmRqDatabase_SetFilepath(&p->database_mulcast_o, filepath);
}

MM_EXPORT_RQ void mmRqUdpServer_SetUnicastOverheadEncode(struct mmRqUdpServer* p, mmUInt32_t overhead)
{
    p->unicast_overhead_encode = overhead;
}
MM_EXPORT_RQ void mmRqUdpServer_SetUnicastOverheadDecode(struct mmRqUdpServer* p, mmUInt32_t overhead)
{
    p->unicast_overhead_decode = overhead;
}
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastOverheadEncode(struct mmRqUdpServer* p, mmUInt32_t overhead)
{
    mmRqFountain_SetOverheadEncode(&p->fountain_mulcast, overhead);
}
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastOverheadDecode(struct mmRqUdpServer* p, mmUInt32_t overhead)
{
    mmRqFountain_SetOverheadDecode(&p->fountain_mulcast, overhead);
}

MM_EXPORT_RQ void mmRqUdpServer_SetContext(struct mmRqUdpServer* p, void* u)
{
    p->u = u;
    mmHeadset_SetContext(&p->headset, p->u);
}
MM_EXPORT_RQ void mmRqUdpServer_SetRepeatedNumber(struct mmRqUdpServer* p, mmUInt32_t repeated_number)
{
    p->repeated_number = repeated_number;
}
MM_EXPORT_RQ void mmRqUdpServer_SetTimeoutMsl(struct mmRqUdpServer* p, mmMSec_t timeout_msl)
{
    p->msec_timeout_msl = timeout_msl;
}
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastMrInterface(struct mmRqUdpServer* p, const char* mr_interface)
{
    mmString_Assigns(&p->mr_interface, mr_interface);
}
MM_EXPORT_RQ void mmRqUdpServer_SetNativeUnicast(struct mmRqUdpServer* p, const char* node, mmUShort_t port)
{
    mmHeadset_SetNative(&p->headset, node, port);
}
MM_EXPORT_RQ void mmRqUdpServer_SetRemoteMulcast(struct mmRqUdpServer* p, const char* node, mmUShort_t port)
{
    mmHeadset_SetRemote(&p->headset, node, port);
    mmRqFountain_SetRemote(&p->fountain_mulcast, node, port);
    mmString_Assigns(&p->mr_multiaddr, node);
}
MM_EXPORT_RQ void mmRqUdpServer_SetThreadNumber(struct mmRqUdpServer* p, mmUInt32_t thread_number)
{
    mmHeadset_SetThreadNumber(&p->headset, thread_number);
}
MM_EXPORT_RQ void mmRqUdpServer_SetNDefaultCallbackUnicast(struct mmRqUdpServer* p, struct mmRqUdpServerCallback* callback)
{
    p->callback_unicast_n = *callback;
}
MM_EXPORT_RQ void mmRqUdpServer_SetNDefaultCallbackMulcast(struct mmRqUdpServer* p, struct mmRqUdpServerCallback* callback)
{
    p->callback_mulcast_n = *callback;
}
MM_EXPORT_RQ void mmRqUdpServer_SetHandleUnicast(struct mmRqUdpServer* p, mmUInt32_t id, mmRqUdpServerHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker_unicast);
    mmRbtreeU32Vpt_Set(&p->rbtree_unicast, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast);
}
MM_EXPORT_RQ void mmRqUdpServer_SetHandleMulcast(struct mmRqUdpServer* p, mmUInt32_t id, mmRqUdpServerHandleFunc callback)
{
    mmSpinlock_Lock(&p->rbtree_locker_mulcast);
    mmRbtreeU32Vpt_Set(&p->rbtree_mulcast, id, (void*)callback);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast);
}
MM_EXPORT_RQ void mmRqUdpServer_ClearHandleRbtree(struct mmRqUdpServer* p)
{
    mmSpinlock_Lock(&p->rbtree_locker_unicast);
    mmRbtreeU32Vpt_Clear(&p->rbtree_unicast);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast);

    mmSpinlock_Lock(&p->rbtree_locker_mulcast);
    mmRbtreeU32Vpt_Clear(&p->rbtree_mulcast);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast);
}
// fopen.
MM_EXPORT_RQ void mmRqUdpServer_FopenSocket(struct mmRqUdpServer* p)
{
    mmHeadset_FopenSocket(&p->headset);
}
// bind.
MM_EXPORT_RQ void mmRqUdpServer_Bind(struct mmRqUdpServer* p)
{
    mmHeadset_Bind(&p->headset);
}
// close socket.
MM_EXPORT_RQ void mmRqUdpServer_CloseSocket(struct mmRqUdpServer* p)
{
    mmHeadset_CloseSocket(&p->headset);
}
// shutdown socket.
MM_EXPORT_RQ void mmRqUdpServer_ShutdownSocket(struct mmRqUdpServer* p)
{
    mmHeadset_ShutdownSocket(&p->headset);
}
MM_EXPORT_RQ void mmRqUdpServer_MulcastShutdown(struct mmRqUdpServer* p)
{
    mmRbtreeVisibleU32_Traver(
        &p->rbtree_visible,
        &__static_mmRqUdpServer_ShutdownSocketRbtreeVisibleU32Handle,
        p);
}
MM_EXPORT_RQ void mmRqUdpServer_MulcastOPersistence(struct mmRqUdpServer* p)
{
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct __mmRqUdpServerPacketIdxArgument _send_argument;

    _send_argument.rq_udp_server = p;
    _send_argument.fountain = fountain_mulcast;
    _send_argument.udp = &p->headset.udp;

    mmRqFountain_ILock(fountain_mulcast);
    mmRqFountain_AssemblyPerfectO(fountain_mulcast, &_send_argument, fountain_mulcast->rq_o_packet_id);
    mmRqFountain_AssemblyPerfectI(fountain_mulcast, &_send_argument, fountain_mulcast->rq_i_packet_id);
    mmRqFountain_IUnlock(fountain_mulcast);

    __static_mmRqUdpServer_MulcastOPersistenceHandle(p);
}
MM_EXPORT_RQ int mmRqUdpServer_MulcastJoin(struct mmRqUdpServer* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmHeadset_MulcastJoin(&p->headset, mr_multiaddr, mr_interface);
}
MM_EXPORT_RQ int mmRqUdpServer_MulcastDrop(struct mmRqUdpServer* p, const char* mr_multiaddr, const char* mr_interface)
{
    return mmHeadset_MulcastDrop(&p->headset, mr_multiaddr, mr_interface);
}

MM_EXPORT_RQ int
mmRqUdpServer_SendBufferUnicast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    mmUInt32_t identity,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet)
{
    int code = -1;

    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;
    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogW(gLogger, "%s %d identity: %u not find.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            struct __mmRqUdpServerPacketIdxArgument _send_argument;

            _send_argument.rq_udp_server = p;
            _send_argument.fountain = fountain_unicast;
            _send_argument.udp = udp;

            // note: 
            //   s_locker not need lock.  
            //   i_locker can not lock.
            //   o_locker need lock.
            mmRqFountain_OLock(fountain_unicast);
            code = mmRqFountain_SendBuffer(fountain_unicast, &_send_argument, packet_head, buffer, hlength, packet);
            mmRqFountain_OUnlock(fountain_unicast);
        }
        else
        {
            void* obj = &p->headset.udp;
            // nready event.
            __static_mmRqUdpServer_HeadsetRqNreadyEvent(obj, fountain_unicast);
        }
    }
    return code;
}

MM_EXPORT_RQ int
mmRqUdpServer_SendBufferMulcast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet)
{
    int code = -1;

    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_mulcast->state)
    {
        struct __mmRqUdpServerPacketIdxArgument _send_argument;

        _send_argument.rq_udp_server = p;
        _send_argument.fountain = fountain_mulcast;
        _send_argument.udp = udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker can not lock.
        //   o_locker need lock.
        mmRqFountain_OLock(fountain_mulcast);
        code = mmRqFountain_SendBuffer(fountain_mulcast, &_send_argument, packet_head, buffer, hlength, packet);
        mmRqFountain_OUnlock(fountain_mulcast);
    }
    else
    {
        void* obj = &p->headset.udp;
        // nready event.
        __static_mmRqUdpServer_HeadsetRqNreadyEvent(obj, fountain_mulcast);
    }
    return code;
}

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_RQ int
mmRqUdpServer_SendMessageUnicast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    mmUInt32_t identity,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int code = -1;

    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;
    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogW(gLogger, "%s %d identity: %u not find.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            struct __mmRqUdpServerPacketIdxArgument _send_argument;

            _send_argument.rq_udp_server = p;
            _send_argument.fountain = fountain_unicast;
            _send_argument.udp = udp;

            // note: 
            //   s_locker not need lock.  
            //   i_locker can not lock.
            //   o_locker need lock.
            mmRqFountain_OLock(fountain_unicast);
            code = mmRqFountain_SendMessage(fountain_unicast, &_send_argument, coder, packet_head, rq_msg, hlength, rq_pack);
            mmRqFountain_OUnlock(fountain_unicast);
        }
        else
        {
            void* obj = &p->headset.udp;
            // nready event.
            __static_mmRqUdpServer_HeadsetRqNreadyEvent(obj, fountain_unicast);
        }
    }
    return code;
}

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_RQ int
mmRqUdpServer_SendMessageMulcast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack)
{
    int code = -1;

    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_mulcast->state)
    {
        struct __mmRqUdpServerPacketIdxArgument _send_argument;

        _send_argument.rq_udp_server = p;
        _send_argument.fountain = fountain_mulcast;
        _send_argument.udp = udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker can not lock.
        //   o_locker need lock.
        mmRqFountain_OLock(fountain_mulcast);
        code = mmRqFountain_SendMessage(fountain_mulcast, &_send_argument, coder, packet_head, rq_msg, hlength, rq_pack);
        mmRqFountain_OUnlock(fountain_mulcast);
    }
    else
    {
        void* obj = &p->headset.udp;
        // nready event.
        __static_mmRqUdpServer_HeadsetRqNreadyEvent(obj, fountain_mulcast);
    }
    return code;
}

// poll size.
MM_EXPORT_RQ size_t mmRqUdpServer_PollSize(struct mmRqUdpServer* p)
{
    return mmRbtreeVisibleU32_Size(&p->rbtree_visible);
}
MM_EXPORT_RQ struct mmRqUdp* mmRqUdpServer_Add(struct mmRqUdpServer* p, mmUInt32_t id)
{
    struct mmRqUdp* u = NULL;
    // only lock for instance create process.
    mmSpinlock_Lock(&p->instance_locker);
    u = mmRqUdpServer_Get(p, id);
    if (NULL == u)
    {
        struct mmRqCallback rq_unicast_i_callback;
        struct mmRqCallback rq_unicast_o_callback;

        struct mmRqFountain* fountain_unicast = NULL;

        // mmPoolElement produce.
        mmPoolElement_Lock(&p->pool_element);
        u = (struct mmRqUdp*)mmPoolElement_Produce(&p->pool_element);
        mmPoolElement_Unlock(&p->pool_element);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmRqUdp_Reset(u);
        // assign
        fountain_unicast = &u->fountain_unicast;
        // assign
        rq_unicast_i_callback.Handle = &__static_mmRqUdpServer_UnicastICallbackHandle;
        rq_unicast_i_callback.Finish = &__static_mmRqUdpServer_UnicastICallbackFinish;
        rq_unicast_i_callback.Scroll = &__static_mmRqUdpServer_UnicastICallbackScroll;
        rq_unicast_i_callback.obj = p;
        mmRqI_SetCallback(&fountain_unicast->rq_i, &rq_unicast_i_callback);

        rq_unicast_o_callback.Handle = &__static_mmRqUdpServer_UnicastOCallbackHandle;
        rq_unicast_o_callback.Finish = &__static_mmRqUdpServer_UnicastOCallbackFinish;
        rq_unicast_o_callback.Scroll = &__static_mmRqUdpServer_UnicastOCallbackScroll;
        rq_unicast_o_callback.obj = p;
        mmRqO_SetCallback(&fountain_unicast->rq_o, &rq_unicast_o_callback);

        mmRqFountain_SetLength(fountain_unicast, p->unicast_length);
        mmRqFountain_SetOverheadEncode(fountain_unicast, p->unicast_overhead_encode);
        mmRqFountain_SetOverheadDecode(fountain_unicast, p->unicast_overhead_decode);

        mmRqFountain_SetIdentity(fountain_unicast, id);
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleU32_Add(&p->rbtree_visible, id, u);
    }
    mmSpinlock_Unlock(&p->instance_locker);
    return u;
}
MM_EXPORT_RQ struct mmRqUdp* mmRqUdpServer_Get(struct mmRqUdpServer* p, mmUInt32_t id)
{
    struct mmRqUdp* u = NULL;
    // note:
    // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
    u = (struct mmRqUdp*)mmRbtreeVisibleU32_Get(&p->rbtree_visible, id);
    return u;
}
MM_EXPORT_RQ struct mmRqUdp* mmRqUdpServer_GetInstance(struct mmRqUdpServer* p, mmUInt32_t id)
{
    struct mmRqUdp* u = mmRqUdpServer_Get(p, id);
    if (NULL == u)
    {
        u = mmRqUdpServer_Add(p, id);
    }
    return u;
}
MM_EXPORT_RQ void mmRqUdpServer_Rmv(struct mmRqUdpServer* p, mmUInt32_t id)
{
    struct mmRqUdp* u = mmRqUdpServer_Get(p, id);
    if (NULL != u)
    {
        mmRqUdp_Lock(u);
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleU32_Rmv(&p->rbtree_visible, id);
        mmRqUdp_Unlock(u);

        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmRqUdp_Reset(u);
        // mmPoolElement recycle.
        mmPoolElement_Lock(&p->pool_element);
        mmPoolElement_Recycle(&p->pool_element, u);
        mmPoolElement_Unlock(&p->pool_element);
    }
}
MM_EXPORT_RQ void mmRqUdpServer_Clear(struct mmRqUdpServer* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmRqUdp* unit = NULL;
    //
    mmSpinlock_Lock(&p->instance_locker);

    // rbtree_m locker is lock inside at mm_nuclear_fd_rmv_poll.
    // here we only need lock the nuclear->cond_locker.
    // mmSpinlock_Lock(&p->rbtree_visible.locker_m);
    n = mmRb_First(&p->rbtree_visible.rbtree_m.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        unit = (struct mmRqUdp*)(it->v);

        mmRqUdp_Lock(unit);
        // note:
        // rbtree_visible will lock inside, can not lock rbtree_visible internal lock outside.
        mmRbtreeVisibleU32_Rmv(&p->rbtree_visible, it->k);
        mmRqUdp_Unlock(unit);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmRqUdp_Reset(unit);
        // pool_element recycle.
        mmPoolElement_Lock(&p->pool_element);
        mmPoolElement_Recycle(&p->pool_element, unit);
        mmPoolElement_Unlock(&p->pool_element);
    }
    // mmSpinlock_Unlock(&p->rbtree_visible.locker_m);

    mmSpinlock_Unlock(&p->instance_locker);
}

MM_EXPORT_RQ void mmRqUdpServer_Start(struct mmRqUdpServer* p)
{
    mmRqDatabase_Fopen(&p->database_mulcast_o);
    mmHeadset_Start(&p->headset);
    mmTimer_Start(&p->timer);
}
MM_EXPORT_RQ void mmRqUdpServer_Interrupt(struct mmRqUdpServer* p)
{
    mmHeadset_Interrupt(&p->headset);
    mmTimer_Interrupt(&p->timer);
}
MM_EXPORT_RQ void mmRqUdpServer_Shutdown(struct mmRqUdpServer* p)
{
    mmHeadset_Shutdown(&p->headset);
    mmTimer_Shutdown(&p->timer);
}
MM_EXPORT_RQ void mmRqUdpServer_Join(struct mmRqUdpServer* p)
{
    mmHeadset_Join(&p->headset);
    mmTimer_Join(&p->timer);
    mmRqDatabase_Fclose(&p->database_mulcast_o);
}

static void __static_mmRqUdpServer_HeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    mmHeadset_HandleDefault(obj, u, pack, remote);
}
static void __static_mmRqUdpServer_HeadsetBroken(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)udp->callback.obj;
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    // rq_udp_server not need mulcast drop.
    // mmHeadset_MulcastDrop(&p->headset, p->mr_multiaddr.s, p->mr_interface.s);

    // mulcast server udp event need fire only nt event.
    __static_mmRqUdpServer_HeadsetNtBrokenEvent(obj, fountain_mulcast);
}
static void __static_mmRqUdpServer_HeadsetNready(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)udp->callback.obj;
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    // mulcast server udp event need fire only nt event.
    __static_mmRqUdpServer_HeadsetNtNreadyEvent(obj, fountain_mulcast);
}
static void __static_mmRqUdpServer_HeadsetFinish(void* obj)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)udp->callback.obj;
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    // rq_udp_server not need mulcast drop.
    // mmHeadset_MulcastJoin(&p->headset, p->mr_multiaddr.s, p->mr_interface.s);

    // mulcast server udp event need fire only nt event.
    __static_mmRqUdpServer_HeadsetNtFinishEvent(obj, fountain_mulcast);
}

static void
__static_mmRqUdpServer_HeadsetMidEventHandle(
    void* obj,
    struct mmRqFountain* f,
    mmUInt32_t mid,
    __mmRqUdpServerMidHandleFunc callback)
{
    struct mmSockaddr remote;
    struct mmPacket pack;
    struct mmPacketHead phead;
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);

    mmSockaddr_Init(&remote);
    mmPacket_Init(&pack);
    mmPacket_HeadBaseZero(&pack);
    pack.hbuff.length = MM_MSG_COMM_HEAD_SIZE;
    pack.bbuff.length = 0;
    mmPacket_ShadowAlloc(&pack);
    pack.phead.mid = mid;
    mmPacket_HeadEncode(&pack, &pack.hbuff, &phead);
    (*(callback))(obj, headset->u, f, &pack, &remote);
    mmPacket_ShadowFree(&pack);
    mmPacket_Destroy(&pack);
    mmSockaddr_Destroy(&remote);
}
static void __static_mmRqUdpServer_HeadsetRqBrokenEvent(void* obj, struct mmRqFountain* f)
{
    __static_mmRqUdpServer_HeadsetMidEventHandle(obj, f, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpServer_NHeadsetHandle);
}
static void __static_mmRqUdpServer_HeadsetRqNreadyEvent(void* obj, struct mmRqFountain* f)
{
    __static_mmRqUdpServer_HeadsetMidEventHandle(obj, f, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpServer_NHeadsetHandle);
}
static void __static_mmRqUdpServer_HeadsetRqFinishEvent(void* obj, struct mmRqFountain* f)
{
    __static_mmRqUdpServer_HeadsetMidEventHandle(obj, f, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpServer_NHeadsetHandle);
}
static void __static_mmRqUdpServer_HeadsetNtBrokenEvent(void* obj, struct mmRqFountain* f)
{
    __static_mmRqUdpServer_HeadsetMidEventHandle(obj, f, MM_UDP_MID_BROKEN, &__static_mmRqUdpServer_NHeadsetHandle);
}
static void __static_mmRqUdpServer_HeadsetNtNreadyEvent(void* obj, struct mmRqFountain* f)
{
    __static_mmRqUdpServer_HeadsetMidEventHandle(obj, f, MM_UDP_MID_NREADY, &__static_mmRqUdpServer_NHeadsetHandle);
}
static void __static_mmRqUdpServer_HeadsetNtFinishEvent(void* obj, struct mmRqFountain* f)
{
    __static_mmRqUdpServer_HeadsetMidEventHandle(obj, f, MM_UDP_MID_FINISH, &__static_mmRqUdpServer_NHeadsetHandle);
}
static void __static_mmRqUdpServer_UnicastHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            struct __mmRqUdpServerPacketIdxArgument _recv_argument;

            _recv_argument.rq_udp_server = p;
            _recv_argument.fountain = fountain_unicast;
            _recv_argument.udp = udp;

            // note: 
            //   s_locker not need lock.  
            //   i_locker need lock.
            //   o_locker can not lock.
            mmRqFountain_ILock(fountain_unicast);
            mmRqFountain_RecvBuffer(fountain_unicast, &_recv_argument, pack, remote);
            mmRqFountain_IUnlock(fountain_unicast);
        }
        else
        {
            struct mmByteBuffer buffer;
            struct mmPacketHead packet_head;

            mmByteBuffer_Init(&buffer);
            mmPacketHead_Init(&packet_head);

            // broken event.
            __static_mmRqUdpServer_HeadsetRqBrokenEvent(udp, fountain_unicast);

            // this api lock inside.
            mmRqUdpServer_Rmv(p, identity);

            mmSpinlock_Lock(&p->id_generater_locker);
            mmUInt32IdGenerater_Recycle(&p->id_generater, identity);
            mmSpinlock_Unlock(&p->id_generater_locker);

            // make sure not NULL.
            buffer.buffer = (mmUInt8_t*)&buffer;
            buffer.offset = (size_t)0;
            buffer.length = (size_t)0;

            // mid is message id.
            // pid is identity.
            // sid is none.
            // uid is none.
            packet_head.mid = mmRqMidUInt32_RstNone;
            packet_head.pid = identity;
            packet_head.sid = 0;
            packet_head.uid = 0;

            mmUdp_OLock(udp);
            mmHeadset_SendBuffer(&p->headset, udp, &packet_head, MM_MSG_COMM_HEAD_SIZE, &buffer, remote);
            mmUdp_OUnlock(udp);

            mmByteBuffer_Destroy(&buffer);
            mmPacketHead_Destroy(&packet_head);
        }
    }
}
static void __static_mmRqUdpServer_MulcastHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_mulcast->state)
    {
        struct __mmRqUdpServerPacketIdxArgument _recv_argument;

        _recv_argument.rq_udp_server = p;
        _recv_argument.fountain = fountain_mulcast;
        _recv_argument.udp = udp;

        // note: 
        //   s_locker not need lock.  
        //   i_locker need lock.
        //   o_locker can not lock.
        mmRqFountain_ILock(fountain_mulcast);
        mmRqFountain_RecvBuffer(fountain_mulcast, &_recv_argument, pack, remote);
        mmRqFountain_IUnlock(fountain_mulcast);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.", __FUNCTION__, __LINE__, fountain_mulcast->state);
    }
}
static void __static_mmRqUdpServer_SynNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;
    struct mmLogger* gLogger = mmLogger_Instance();

    do
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();
        mmUInt64_t usec_current_rq = pack->phead.sid;

        mmUInt32_t identity = 0;

        mmSpinlock_Lock(&p->rbtree_sockaddr_locker);
        rq_udp = (struct mmRqUdp*)mmRbtreeSockaddrVpt_Get(&p->rbtree_sockaddr_vpt, remote);
        mmSpinlock_Unlock(&p->rbtree_sockaddr_locker);

        if (NULL != rq_udp)
        {
            mmLogger_LogT(gLogger, "%s %d ignore repeated message.", __FUNCTION__, __LINE__);
            break;
        }
        {
            char addr_name[MM_ADDR_NAME_LENGTH] = { 0 };

            mmSockaddr_ToString(remote, addr_name);

            mmSpinlock_Lock(&p->id_generater_locker);
            identity = mmUInt32IdGenerater_Produce(&p->id_generater);
            mmSpinlock_Unlock(&p->id_generater_locker);

            rq_udp = mmRqUdpServer_GetInstance(p, identity);

            fountain_unicast = &rq_udp->fountain_unicast;

            mmSpinlock_Lock(&p->rbtree_sockaddr_locker);
            mmRbtreeSockaddrVpt_Set(&p->rbtree_sockaddr_vpt, remote, rq_udp);
            mmSpinlock_Unlock(&p->rbtree_sockaddr_locker);

            mmLogger_LogT(gLogger, "rq_udp_server syn sid: %" PRId64 " uid: %" PRId64 " addr_name: %s identity: %u.",
                pack->phead.sid,
                pack->phead.uid,
                addr_name,
                identity);
        }
        {
            mmUInt64_t uid = 0;
            mmUInt64_t sid = 0;
            mmUInt32_t random_number = 0;

            struct mmByteBuffer buffer;
            struct mmPacketHead packet_head;

            mmByteBuffer_Init(&buffer);
            mmPacketHead_Init(&packet_head);

            mmRqFountain_Lock(fountain_unicast);
            mmRqFountain_ResetContext(fountain_unicast);
            random_number = (mmUInt32_t)(mmXoshiro256starstar_Next(&p->random_context) % MM_UINT32_MAX);
            fountain_unicast->rq_i.timestamp_initial = random_number;
            random_number = (mmUInt32_t)(mmXoshiro256starstar_Next(&p->random_context) % MM_UINT32_MAX);
            fountain_unicast->rq_o.timestamp_initial = random_number;
            
            uid = mmByte_UInt64A(
                fountain_unicast->rq_o.timestamp_initial,
                fountain_unicast->rq_i.timestamp_initial);
            
            mmRqFountain_SetState(fountain_unicast, MM_RQ_SYN_RECV);
            mmRqFountain_SetTimecodeCreate(fountain_unicast, usec_current);
            mmRqFountain_SetTimecodeUpdate(fountain_unicast, usec_current_rq);
            mmRqFountain_SetRemoteStorage(fountain_unicast, remote);
            mmRqFountain_Unlock(fountain_unicast);

            // fountain_mulcast not need reset or assign.
            // here we use cache value.
            sid = p->mulcast_io_timestamp_initial;

            // make sure not NULL.
            buffer.buffer = (mmUInt8_t*)&buffer;
            buffer.offset = (size_t)0;
            buffer.length = (size_t)0;

            // mid is message id.
            // pid is identity.
            // sid is l(mmUInt32_t) mulcast recv timestamp_initial, r(mmUInt32_t) mulcast timestamp_initial.
            // uid is l(mmUInt32_t) unicast recv timestamp_initial, r(mmUInt32_t) unicast timestamp_initial.
            packet_head.mid = mmRqMidUInt32_SyncAck;
            packet_head.pid = identity;
            packet_head.sid = sid;
            packet_head.uid = uid;

            __static_mmRqUdpServer_SendRepeatedMassageUnicast(p, &packet_head, MM_MSG_COMM_HEAD_SIZE, &buffer, remote);

            mmByteBuffer_Destroy(&buffer);
            mmPacketHead_Destroy(&packet_head);
        }
    } while (0);
}
static void __static_mmRqUdpServer_AckNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_SYN_RECV == fountain_unicast->state)
        {
            // remove the sockaddr vpt.
            mmSpinlock_Lock(&p->rbtree_sockaddr_locker);
            mmRbtreeSockaddrVpt_Rmv(&p->rbtree_sockaddr_vpt, remote);
            mmSpinlock_Unlock(&p->rbtree_sockaddr_locker);

            mmRqFountain_SLock(fountain_unicast);
            mmRqFountain_SetState(fountain_unicast, MM_RQ_ESTABLISHED);
            mmRqFountain_SUnlock(fountain_unicast);

            mmSpinlock_Lock(&p->rbtree_sockaddr_locker);
            mmRbtreeSockaddrVpt_Rmv(&p->rbtree_sockaddr_vpt, remote);
            mmSpinlock_Unlock(&p->rbtree_sockaddr_locker);

            // finish event.
            __static_mmRqUdpServer_HeadsetRqFinishEvent(udp, fountain_unicast);
        }
        else if (MM_RQ_FIN_WAIT_1 == fountain_unicast->state)
        {
            mmRqFountain_SLock(fountain_unicast);
            mmRqFountain_SetState(fountain_unicast, MM_RQ_FIN_WAIT_2);
            mmRqFountain_SUnlock(fountain_unicast);
        }
        else if (MM_RQ_LAST_ACK == fountain_unicast->state)
        {
            // broken event.
            __static_mmRqUdpServer_HeadsetRqBrokenEvent(udp, fountain_unicast);

            // this api lock inside.
            mmRqUdpServer_Rmv(p, identity);

            mmSpinlock_Lock(&p->id_generater_locker);
            mmUInt32IdGenerater_Recycle(&p->id_generater, identity);
            mmSpinlock_Unlock(&p->id_generater_locker);
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.", __FUNCTION__, __LINE__, fountain_unicast->state);
        }
    }
}
static void __static_mmRqUdpServer_FinNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            struct mmByteBuffer buffer;
            struct mmPacketHead packet_head;

            mmByteBuffer_Init(&buffer);
            mmPacketHead_Init(&packet_head);

            mmRqFountain_SLock(fountain_unicast);
            mmRqFountain_SetState(fountain_unicast, MM_RQ_CLOSE_WAIT);
            mmRqFountain_SUnlock(fountain_unicast);

            // make sure not NULL.
            buffer.buffer = (mmUInt8_t*)&buffer;
            buffer.offset = (size_t)0;
            buffer.length = (size_t)0;

            // mid is message id.
            // pid is identity.
            // sid is l(mmUInt32_t) mulcast recv timestamp_initial, r(mmUInt32_t) mulcast timestamp_initial.
            // uid is l(mmUInt32_t) unicast recv timestamp_initial, r(mmUInt32_t) unicast timestamp_initial.
            packet_head.mid = mmRqMidUInt32_AckNone;
            packet_head.pid = identity;
            packet_head.sid = 0;// not need here.
            packet_head.uid = 0;// not need here.

            __static_mmRqUdpServer_SendRepeatedMassageUnicast(
                p,
                &packet_head,
                MM_MSG_COMM_HEAD_SIZE,
                &buffer,
                remote);

            // mid is message id.
            // pid is identity.
            // sid is none.
            // uid is none.
            packet_head.mid = mmRqMidUInt32_FinNone;
            packet_head.pid = identity;
            packet_head.sid = 0;
            packet_head.uid = 0;

            __static_mmRqUdpServer_SendRepeatedMassageUnicast(
                p,
                &packet_head,
                MM_MSG_COMM_HEAD_SIZE,
                &buffer,
                remote);

            mmByteBuffer_Destroy(&buffer);
            mmPacketHead_Destroy(&packet_head);
        }
        else if (MM_RQ_FIN_WAIT_2 == fountain_unicast->state)
        {
            mmRqFountain_SLock(fountain_unicast);
            mmRqFountain_SetState(fountain_unicast, MM_RQ_TIME_WAIT);
            mmRqFountain_SUnlock(fountain_unicast);

            // broken event.
            __static_mmRqUdpServer_HeadsetRqBrokenEvent(udp, fountain_unicast);

            // we not need time_wait state.
            mmRqUdpServer_Rmv(p, identity);

            mmSpinlock_Lock(&p->id_generater_locker);
            mmUInt32IdGenerater_Recycle(&p->id_generater, identity);
            mmSpinlock_Unlock(&p->id_generater_locker);
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
                __FUNCTION__,
                __LINE__,
                fountain_unicast->state);
        }
    }
}
static void __static_mmRqUdpServer_RstNoneHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        // broken event.
        __static_mmRqUdpServer_HeadsetRqBrokenEvent(udp, fountain_unicast);

        // this api lock inside.
        mmRqUdpServer_Rmv(p, identity);

        mmSpinlock_Lock(&p->id_generater_locker);
        mmUInt32IdGenerater_Recycle(&p->id_generater, identity);
        mmSpinlock_Unlock(&p->id_generater_locker);
    }
}
static void __static_mmRqUdpServer_SyncAckHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    //
    mmLogger_LogT(gLogger, "%s %d ignore unknow message.", __FUNCTION__, __LINE__);
}
static void __static_mmRqUdpServer_PingReqHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            struct mmByteBuffer buffer;
            struct mmPacketHead packet_head;

            mmByteBuffer_Init(&buffer);
            mmPacketHead_Init(&packet_head);

            // make sure not NULL.
            buffer.buffer = (mmUInt8_t*)&buffer;
            buffer.offset = (size_t)0;
            buffer.length = (size_t)0;

            // mid is message id.
            // pid is identity.
            // sid is client timecode us.
            // uid is none.
            packet_head.mid = mmRqMidUInt32_PingRes;
            packet_head.pid = identity;
            packet_head.sid = pack->phead.sid;
            packet_head.uid = 0;

            __static_mmRqUdpServer_SendRepeatedMassageUnicast(
                p,
                &packet_head,
                MM_MSG_COMM_HEAD_SIZE,
                &buffer,
                remote);

            mmByteBuffer_Destroy(&buffer);
            mmPacketHead_Destroy(&packet_head);
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
                __FUNCTION__,
                __LINE__,
                fountain_unicast->state);
        }
    }
}
static void __static_mmRqUdpServer_PingResHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            mmUInt64_t usec_current = mmTime_CurrentUSec();

            mmRqFountain_SLock(fountain_unicast);
            mmRqFountain_SetTimecodeRtt(fountain_unicast, usec_current - pack->phead.sid);
            mmRqFountain_SUnlock(fountain_unicast);
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
                __FUNCTION__,
                __LINE__,
                fountain_unicast->state);
        }
    }
}
static void __static_mmRqUdpServer_LossUpdHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;
    struct mmRqUdpMulcast* udp_mulcast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;
        udp_mulcast = &rq_udp->udp_mulcast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            mmUInt64_t timecode_o_packet_number_update = 0;

            mmRqFountain_OLock(fountain_unicast);
            timecode_o_packet_number_update = fountain_unicast->timecode_o_packet_number_update;
            mmRqFountain_OUnlock(fountain_unicast);

            if (timecode_o_packet_number_update < pack->phead.uid)
            {
                mmUInt64_t msec_current_rq = (mmUInt64_t)(pack->phead.uid);

                // most of time, the client mulcast_i_packet_number need sampled calculation.
                mmUInt32_t mulcast_i_packet_number = mmByte_UInt64L(pack->phead.sid);
                mmUInt32_t unicast_i_packet_number = mmByte_UInt64R(pack->phead.sid);

                {
                    mmUInt32_t unicast_o_packet_number = 0;

                    float unicast_expected_loss = 0;

                    mmRqFountain_OLock(fountain_unicast);
                    unicast_o_packet_number = (mmUInt32_t)(fountain_unicast->rq_o.packet_number - fountain_unicast->rq_o_packet_number_interval);
                    mmRqFountain_SetOPacketNumberUpdate(fountain_unicast, msec_current_rq);
                    mmRqFountain_OUnlock(fountain_unicast);

                    unicast_expected_loss = mmRqO_ExpectedLoss(unicast_i_packet_number, unicast_o_packet_number);

                    mmRqFountain_OLock(fountain_unicast);
                    mmRqFountain_SetExpectedLoss(fountain_unicast, unicast_expected_loss);
                    mmRqFountain_OUnlock(fountain_unicast);
                }
                {
                    mmUInt64_t o_packet_number = 0;

                    mmUInt32_t mulcast_o_packet_number = 0;

                    float mulcast_expected_loss = 0;

                    mmRqUdpMulcast_Lock(udp_mulcast);
                    o_packet_number = udp_mulcast->o_packet_number;
                    mmRqUdpMulcast_Unlock(udp_mulcast);

                    mmRqFountain_OLock(fountain_mulcast);
                    mulcast_o_packet_number = (mmUInt32_t)(fountain_mulcast->rq_o.packet_number - o_packet_number);
                    o_packet_number = fountain_mulcast->rq_o.packet_number;
                    mmRqFountain_OUnlock(fountain_mulcast);

                    mulcast_expected_loss = mmRqO_ExpectedLoss(mulcast_i_packet_number, mulcast_o_packet_number);

                    mmRqUdpMulcast_Lock(udp_mulcast);
                    udp_mulcast->timecode_o_packet_number_update = msec_current_rq;
                    udp_mulcast->o_packet_number = o_packet_number;
                    udp_mulcast->expected_loss = mulcast_expected_loss;
                    mmRqUdpMulcast_Unlock(udp_mulcast);
                }
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                //
                mmLogger_LogT(gLogger, "%s %d ignore repeated message.", __FUNCTION__, __LINE__);
            }
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
                __FUNCTION__,
                __LINE__,
                fountain_unicast->state);
        }
    }
}
static void __static_mmRqUdpServer_IdxsUpdHeadsetHandle(void* obj, void* u, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)(udp->callback.obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;
    struct mmRqUdp* rq_udp = NULL;
    struct mmRqFountain* fountain_unicast = NULL;
    struct mmRqUdpMulcast* udp_mulcast = NULL;

    mmUInt32_t identity = pack->phead.pid;

    rq_udp = mmRqUdpServer_Get(p, identity);
    if (NULL == rq_udp)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d identity: %u invalid.", __FUNCTION__, __LINE__, identity);
    }
    else
    {
        fountain_unicast = &rq_udp->fountain_unicast;
        udp_mulcast = &rq_udp->udp_mulcast;

        if (MM_RQ_ESTABLISHED == fountain_unicast->state)
        {
            mmUInt64_t timecode_o_packet_index_update = 0;

            mmRqFountain_OLock(fountain_unicast);
            timecode_o_packet_index_update = fountain_unicast->timecode_o_packet_indexp_update;
            mmRqFountain_OUnlock(fountain_unicast);

            if (timecode_o_packet_index_update < pack->phead.uid)
            {
                mmUInt64_t msec_current_rq = (mmUInt64_t)(pack->phead.uid);

                // most of time, the client mulcast_index need index calculation.
                mmUInt32_t mulcast_index = 0;
                mmUInt32_t unicast_index = 0;

                struct __mmRqUdpServerPacketIdxArgument _send_argument;

                _send_argument.rq_udp_server = p;
                _send_argument.fountain = fountain_unicast;
                _send_argument.udp = udp;

                mulcast_index = mmByte_UInt64L(pack->phead.sid);
                unicast_index = mmByte_UInt64R(pack->phead.sid);

                mmRqFountain_OLock(fountain_unicast);
                // need update timecodde fountain_unicast o_packet_index.
                mmRqFountain_SetOPacketIndexpUpdate(fountain_unicast, msec_current_rq);
                // fountain_unicast repaird fountain_unicast.
                mmRqFountain_GenerateRepairdO(fountain_unicast, &_send_argument, unicast_index);
                mmRqFountain_OUnlock(fountain_unicast);

                mmRqFountain_ILock(fountain_unicast);
                mmRqFountain_AssemblyPerfectO(fountain_unicast, &_send_argument, unicast_index);
                mmRqFountain_AssemblyPerfectI(fountain_unicast, &_send_argument, fountain_unicast->rq_i_packet_id);
                mmRqFountain_IUnlock(fountain_unicast);

                mmRqFountain_ILock(fountain_unicast);
                mmRqFountain_FeedbackPerfectO(fountain_unicast, &_send_argument);
                mmRqFountain_IUnlock(fountain_unicast);

                mmRqFountain_ILock(fountain_unicast);
                mmRqFountain_FeedbackPerfectI(fountain_unicast, &_send_argument);
                mmRqFountain_IUnlock(fountain_unicast);

                // not need update timecodde fountain_mulcast o_packet_index.
                // fountain_mulcast repaird fountain_mulcast.
                __static_mmRqUdpServer_ORepairdHandle(p, fountain_mulcast, &_send_argument, mulcast_index);

                mmRqUdpMulcast_Lock(udp_mulcast);
                udp_mulcast->timecode_o_packet_indexp_update = msec_current_rq;
                udp_mulcast->o_packet_indexp = mulcast_index;
                mmRqUdpMulcast_Unlock(udp_mulcast);
            }
            else
            {
                struct mmLogger* gLogger = mmLogger_Instance();
                //
                mmLogger_LogT(gLogger, "%s %d ignore repeated message.", __FUNCTION__, __LINE__);
            }
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            //
            mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
                __FUNCTION__,
                __LINE__,
                fountain_unicast->state);
        }
    }
}

static void
__static_mmRqUdpServer_UnicastICallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet)
{
    // need do nothing.
}

static void __static_mmRqUdpServer_UnicastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    struct mmStreambuf* streambuf = &e->streambuf_buffer;
    struct __mmRqUdpServerPacketIdxArgument* _recv_argument = (struct __mmRqUdpServerPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain_unicast = _recv_argument->fountain;
    struct mmSockaddr* remote = &fountain_unicast->ss_remote;

    size_t gptr = streambuf->gptr;
    
    mmStreambufPacket_HandleUdp(
        streambuf,
        &__static_mmRqUdpServer_UnicastIPacketHandleUdp,
        _recv_argument,
        remote);
    
    mmStreambuf_SetGPtr(streambuf, gptr);

    // note:
    // 1. i_locker is lock outside.
    // 2. locker order is locker -> i_locker -> o_locker.
    // 3. here can not lock locker, because locker order will i_locker -> locker case dead lock.
    // 4. here not need lock locker, because rq_i_packet_id is i_locker data.
    mmRqFountain_IncreaseRqIPacketId(fountain_unicast);
}
static void __static_mmRqUdpServer_UnicastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}

static void
__static_mmRqUdpServer_UnicastOCallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet)
{
    struct mmRqO* rq_o = (struct mmRqO*)(obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(rq_o->callback.obj);
    struct mmHeadset* headset = &p->headset;
    struct __mmRqUdpServerPacketIdxArgument* _send_argument = (struct __mmRqUdpServerPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain = _send_argument->fountain;
    struct mmUdp* udp = _send_argument->udp;
    struct mmStreambuf* streambuf = &e->streambuf_packet;

    struct mmByteBuffer buffer;
    struct mmPacketHead packet_head;

    mmByteBuffer_Init(&buffer);
    mmPacketHead_Init(&packet_head);

    buffer.buffer = (mmUInt8_t*)streambuf->buff;
    buffer.offset = (size_t)streambuf->gptr;
    buffer.length = (size_t)mmStreambuf_Size(streambuf);

    // mid is message id.
    // pid is identity.
    // sid is none.
    // uid is none.
    packet_head.mid = mmRqMidUInt32_Unicast;
    packet_head.pid = fountain->identity;
    packet_head.sid = 0;
    packet_head.uid = 0;

    mmUdp_OLock(udp);
    mmHeadset_SendBuffer(headset, udp, &packet_head, MM_RQ_SERVER_COMM_HEAD_SIZE, &buffer, &fountain->ss_remote);
    mmUdp_OUnlock(udp);

    mmByteBuffer_Destroy(&buffer);
    mmPacketHead_Destroy(&packet_head);
}
static void __static_mmRqUdpServer_UnicastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}
static void __static_mmRqUdpServer_UnicastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}

static void
__static_mmRqUdpServer_MulcastICallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet)
{
    // need do nothing.
}

static void __static_mmRqUdpServer_MulcastICallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    struct mmStreambuf* streambuf = &e->streambuf_buffer;
    struct __mmRqUdpServerPacketIdxArgument* _recv_argument = (struct __mmRqUdpServerPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain_mulcast = _recv_argument->fountain;
    struct mmSockaddr* remote = &fountain_mulcast->ss_remote;

    size_t gptr = streambuf->gptr;
    
    mmStreambufPacket_HandleUdp(
        streambuf,
        &__static_mmRqUdpServer_MulcastIPacketHandleUdp,
        _recv_argument,
        remote);
    
    mmStreambuf_SetGPtr(streambuf, gptr);

    // note:
    // 1. i_locker is lock outside.
    // 2. locker order is locker -> i_locker -> o_locker.
    // 3. here can not lock locker, because locker order will i_locker -> locker case dead lock.
    // 4. here not need lock locker, because rq_i_packet_id is i_locker data.
    mmRqFountain_IncreaseRqIPacketId(fountain_mulcast);
}
static void __static_mmRqUdpServer_MulcastICallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}

static void
__static_mmRqUdpServer_MulcastOCallbackHandle(
    void* obj,
    void* u,
    void* idx,
    struct mmRqElem* e,
    struct mmRqPacket* packet)
{
    struct mmRqO* rq_o = (struct mmRqO*)(obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(rq_o->callback.obj);
    struct mmHeadset* headset = &p->headset;
    struct __mmRqUdpServerPacketIdxArgument* _send_argument = (struct __mmRqUdpServerPacketIdxArgument*)(idx);
    struct mmRqFountain* fountain = _send_argument->fountain;
    struct mmUdp* udp = _send_argument->udp;
    struct mmStreambuf* streambuf = &e->streambuf_packet;

    struct mmByteBuffer buffer;
    struct mmPacketHead packet_head;

    mmByteBuffer_Init(&buffer);
    mmPacketHead_Init(&packet_head);

    buffer.buffer = (mmUInt8_t*)streambuf->buff;
    buffer.offset = (size_t)streambuf->gptr;
    buffer.length = (size_t)mmStreambuf_Size(streambuf);

    // mid is message id.
    // pid is identity.
    // sid is none.
    // uid is none.
    packet_head.mid = mmRqMidUInt32_Mulcast;
    packet_head.pid = fountain->identity;
    packet_head.sid = 0;
    packet_head.uid = 0;

    mmUdp_OLock(udp);
    mmHeadset_SendBuffer(headset, udp, &packet_head, MM_RQ_SERVER_COMM_HEAD_SIZE, &buffer, &fountain->ss_remote);
    mmUdp_OUnlock(udp);

    mmByteBuffer_Destroy(&buffer);
    mmPacketHead_Destroy(&packet_head);
}
static void __static_mmRqUdpServer_MulcastOCallbackFinish(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    // need do nothing.
}
static void __static_mmRqUdpServer_MulcastOCallbackScroll(void* obj, void* u, void* idx, struct mmRqElem* e)
{
    struct mmRqO* rq_o = (struct mmRqO*)(obj);
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(rq_o->callback.obj);
    struct mmStreambuf* streambuf = &e->streambuf_buffer;

    struct mmByteBuffer byte_buffer;

    byte_buffer.buffer = (mmUInt8_t*)streambuf->buff;
    byte_buffer.offset = (size_t)streambuf->gptr;
    byte_buffer.length = (size_t)mmStreambuf_Size(streambuf);

    // lock outside, not need lock here.
    mmRqDatabase_Insert(&p->database_mulcast_o, e->id, &byte_buffer);
}
static void __static_mmRqUdpServer_UnicastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct __mmRqUdpServerPacketIdxArgument* _recv_argument = (struct __mmRqUdpServerPacketIdxArgument*)(obj);
    struct mmRqUdpServer* p = _recv_argument->rq_udp_server;
    struct mmRqFountain* fountain_unicast = _recv_argument->fountain;
    struct mmUdp* udp = _recv_argument->udp;
    struct mmHeadset* headset = &p->headset;

    mmRqUdpServerHandleFunc handle = NULL;
    mmSpinlock_Lock(&p->rbtree_locker_unicast);
    handle = (mmRqUdpServerHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree_unicast, pack->phead.mid);
    mmSpinlock_Unlock(&p->rbtree_locker_unicast);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, headset->u, fountain_unicast->identity, pack, remote);
    }
    else
    {
        assert(NULL != p->callback_unicast_n.Handle && "p->callback_unicast_n.Handle is a null.");
        (*(p->callback_unicast_n.Handle))(udp, headset->u, fountain_unicast->identity, pack, remote);
    }
}
static void __static_mmRqUdpServer_MulcastIPacketHandleUdp(void* obj, struct mmPacket* pack, struct mmSockaddr* remote)
{
    struct __mmRqUdpServerPacketIdxArgument* _recv_argument = (struct __mmRqUdpServerPacketIdxArgument*)(obj);
    struct mmRqUdpServer* p = _recv_argument->rq_udp_server;
    struct mmRqFountain* fountain_mulcast = _recv_argument->fountain;
    struct mmUdp* udp = _recv_argument->udp;
    struct mmHeadset* headset = &p->headset;

    mmRqUdpServerHandleFunc handle = NULL;
    mmSpinlock_Lock(&p->rbtree_locker_mulcast);
    handle = (mmRqUdpServerHandleFunc)mmRbtreeU32Vpt_Get(&p->rbtree_mulcast, pack->phead.mid);
    mmSpinlock_Unlock(&p->rbtree_locker_mulcast);
    if (NULL != handle)
    {
        // fire the handle event.
        (*(handle))(udp, headset->u, fountain_mulcast->identity, pack, remote);
    }
    else
    {
        assert(NULL != p->callback_mulcast_n.Handle && "p->callback_mulcast_n.Handle is a null.");
        (*(p->callback_mulcast_n.Handle))(udp, headset->u, fountain_mulcast->identity, pack, remote);
    }
}

static void
__static_mmRqUdpServer_NHeadsetHandle(
    void* obj,
    void* u,
    struct mmRqFountain* f,
    struct mmPacket* pack,
    struct mmSockaddr* remote)
{
    struct mmUdp* udp = (struct mmUdp*)(obj);
    struct mmHeadset* headset = (struct mmHeadset*)udp->callback.obj;
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(headset->callback.obj);
    struct mmRqFountain* fountain = f;

    struct __mmRqUdpServerPacketIdxArgument _recv_argument;

    _recv_argument.rq_udp_server = p;
    _recv_argument.fountain = fountain;
    _recv_argument.udp = udp;

    __static_mmRqUdpServer_UnicastIPacketHandleUdp(&_recv_argument, pack, remote);
    __static_mmRqUdpServer_MulcastIPacketHandleUdp(&_recv_argument, pack, remote);
}

static void
__static_mmRqUdpServer_SendRepeatedMassageUnicast(
    struct mmRqUdpServer* p,
    struct mmPacketHead* packet_head,
    size_t hlength,
    struct mmByteBuffer* buffer,
    struct mmSockaddr* remote)
{
    struct mmUdp* udp = &p->headset.udp;

    mmUInt32_t i = 0;
    for (i = 0; i < p->repeated_number; i++)
    {
        mmUdp_OLock(udp);
        mmHeadset_SendBuffer(&p->headset, udp, packet_head, MM_MSG_COMM_HEAD_SIZE, buffer, remote);
        mmUdp_OUnlock(udp);
    }
}

static void __static_mmRqUdpServer_EmptyHandleForCommonEvent(struct mmRqUdpServer* p)
{
    mmRqUdpServer_SetHandleUnicast(p, MM_UDP_MID_FINISH, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleUnicast(p, MM_UDP_MID_NREADY, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleUnicast(p, MM_UDP_MID_BROKEN, &__static_mmRqUdpServer_Handle);

    mmRqUdpServer_SetHandleMulcast(p, MM_UDP_MID_FINISH, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleMulcast(p, MM_UDP_MID_NREADY, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleMulcast(p, MM_UDP_MID_BROKEN, &__static_mmRqUdpServer_Handle);
    //
    mmRqUdpServer_SetHandleUnicast(p, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleUnicast(p, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleUnicast(p, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpServer_Handle);

    mmRqUdpServer_SetHandleMulcast(p, MM_RQ_UDP_MID_FINISH, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleMulcast(p, MM_RQ_UDP_MID_NREADY, &__static_mmRqUdpServer_Handle);
    mmRqUdpServer_SetHandleMulcast(p, MM_RQ_UDP_MID_BROKEN, &__static_mmRqUdpServer_Handle);
}
static void __static_mmRqUdpServer_MsecUpdateStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(entry->callback.obj);
    mmUInt64_t usec_current = mmTime_CurrentUSec();

    struct mmRbNode* n = NULL;
    struct mmRbtreeSockaddrVptIterator* it = NULL;
    struct mmRqUdp* u = NULL;
    struct mmRqFountain* fountain_unicast = NULL;
    //
    mmUInt32_t identity = 0;
    mmUInt64_t timecode_create = 0;
    //
    mmSpinlock_Lock(&p->rbtree_sockaddr_locker);
    n = mmRb_First(&p->rbtree_sockaddr_vpt.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeSockaddrVptIterator, n);
        u = (struct mmRqUdp*)(it->v);
        n = mmRb_Next(n);

        fountain_unicast = &u->fountain_unicast;

        mmRqFountain_SLock(fountain_unicast);
        timecode_create = fountain_unicast->timecode_create;
        identity = fountain_unicast->identity;
        mmRqFountain_SUnlock(fountain_unicast);

        if (
            MM_RQ_SYN_RECV == fountain_unicast->state &&
            MM_RQ_UDP_SERVER_MSEC_SYN_RECV_TIMEOUT * MM_MSEC_PER_SEC < usec_current - timecode_create)
        {
            mmSpinlock_Lock(&p->id_generater_locker);
            mmUInt32IdGenerater_Recycle(&p->id_generater, identity);
            mmSpinlock_Unlock(&p->id_generater_locker);

            // this api lock inside.
            mmRqUdpServer_Rmv(p, identity);

            mmRbtreeSockaddrVpt_Erase(&p->rbtree_sockaddr_vpt, it);
        }
    }
    mmSpinlock_Unlock(&p->rbtree_sockaddr_locker);
}
static void __static_mmRqUdpServer_MsecUpdateLossNumberHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(entry->callback.obj);

    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct __mmRqUdpServerPacketNumberArgument _argument;

    mmUInt64_t usec_current = mmTime_CurrentUSec();

    float mulcast_expected_loss = 0;

    mmRqFountain_OLock(fountain_mulcast);
    mmRqFountain_SetOPacketNumberUpdate(fountain_mulcast, usec_current);
    mmRqFountain_OUnlock(fountain_mulcast);

    _argument.rq_udp_server = p;
    _argument.fountain_number = 0;
    _argument.sampled_summate = 0;

    mmRbtreeVisibleU32_Traver(
        &p->rbtree_visible,
        &__static_mmRqUdpServer_NumberCalculationRbtreeVisibleU32Handle,
        &_argument);

    if (0 == _argument.fountain_number)
    {
        mulcast_expected_loss = 0;
    }
    else
    {
        mulcast_expected_loss = _argument.sampled_summate / (float)_argument.fountain_number;
    }
    // packet expected loss max is limit.
    mulcast_expected_loss =
        mulcast_expected_loss < MM_RQ_O_PACKET_EXPECTED_LOSS_MAX ?
        mulcast_expected_loss : MM_RQ_O_PACKET_EXPECTED_LOSS_MAX;

    p->mulcast_expected_loss = mulcast_expected_loss;

    mmRqFountain_OLock(fountain_mulcast);
    mmRqFountain_SetExpectedLoss(fountain_mulcast, p->mulcast_expected_loss);
    mmRqFountain_OUnlock(fountain_mulcast);
}
static void __static_mmRqUdpServer_MsecUpdateLossIndexpHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(entry->callback.obj);

    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct __mmRqUdpServerPacketIndexpArgument _argument;
    struct __mmRqUdpServerPacketIdxArgument _send_argument;

    mmUInt64_t usec_current = 0;

    _argument.rq_udp_server = p;
    _argument.fountain_number = 0;
    _argument.min_index = -1;
    _argument.max_index = 0;

    mmRbtreeVisibleU32_Traver(
        &p->rbtree_visible,
        &__static_mmRqUdpServer_IndexpCalculationRbtreeVisibleU32Handle,
        &_argument);

    if (0 == _argument.fountain_number)
    {
        mmRqFountain_OLock(fountain_mulcast);
        _argument.max_index = fountain_mulcast->rq_o_packet_id;
        _argument.min_index = fountain_mulcast->rq_o_packet_id;
        mmRqFountain_OUnlock(fountain_mulcast);
    }

    usec_current = mmTime_CurrentUSec();

    p->mulcast_min_index = _argument.min_index;
    p->mulcast_max_index = _argument.max_index;

    _send_argument.rq_udp_server = p;
    _send_argument.fountain = fountain_mulcast;
    _send_argument.udp = &p->headset.udp;

    mmRqFountain_OLock(fountain_mulcast);
    // need update timecodde fountain_mulcast o_packet_index.
    mmRqFountain_SetOPacketIndexpUpdate(fountain_mulcast, usec_current);
    mmRqFountain_OUnlock(fountain_mulcast);

    // fountain_mulcast repaird fountain_mulcast.
    __static_mmRqUdpServer_ORepairdHandle(p, fountain_mulcast, &_send_argument, _argument.max_index);

    mmRqFountain_ILock(fountain_mulcast);
    mmRqFountain_AssemblyPerfectO(fountain_mulcast, &_send_argument, _argument.min_index);
    mmRqFountain_AssemblyPerfectI(fountain_mulcast, &_send_argument, fountain_mulcast->rq_i_packet_id);
    mmRqFountain_IUnlock(fountain_mulcast);

    // fountain_mulcast feedback_perfect_o need persistence. 
    mmRqFountain_ILock(fountain_mulcast);
    mmRqFountain_FeedbackPerfectI(fountain_mulcast, &_send_argument);
    mmRqFountain_IUnlock(fountain_mulcast);
}
static void __static_mmRqUdpServer_MsecUpdateLossStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(entry->callback.obj);

    mmRbtreeVisibleU32_Traver(
        &p->rbtree_visible,
        &__static_mmRqUdpServer_UpdateLossStateRbtreeVisibleU32Handle,
        p);
}
static void __static_mmRqUdpServer_MsecUpdateIdxsStateHandle(struct mmTimerHeap* timer_heap, struct mmTimerEntry* entry)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(entry->callback.obj);

    mmRbtreeVisibleU32_Traver(
        &p->rbtree_visible,
        &__static_mmRqUdpServer_UpdateIdxsStateRbtreeVisibleU32Handle,
        p);
}

static void
__static_mmRqUdpServer_MsecUpdateMulcastOPersistenceHandle(
    struct mmTimerHeap* timer_heap,
    struct mmTimerEntry* entry)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(entry->callback.obj);
    
    __static_mmRqUdpServer_MulcastOPersistenceHandle(p);
}

static void __static_mmRqUdpServer_ShutdownSocketRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(u);
    struct mmRqUdp* rq_udp = (struct mmRqUdp*)(v);
    struct mmRqFountain* fountain_unicast = &rq_udp->fountain_unicast;

    struct mmPacketHead packet_head;
    struct mmByteBuffer buffer;

    mmPacketHead_Init(&packet_head);
    mmByteBuffer_Init(&buffer);

    // note:
    // although the v is valid but we can lock v at this function.
    mmRqFountain_SLock(fountain_unicast);
    mmRqFountain_SetState(fountain_unicast, MM_RQ_FIN_WAIT_1);
    mmRqFountain_SUnlock(fountain_unicast);

    // note:
    // rbtree_visible traver callback we lock instance_locker outside, so the v is valid.
    // if at traver callback handle trigger other callback, do not lock the v.
    // if lock v before trigger other callback, some time will case dead lock at other callback inside.
    // although the v is valid but we can lock v at this function, but careful for it.

    // make sure not NULL.
    buffer.buffer = (mmUInt8_t*)&buffer;
    buffer.offset = (size_t)0;
    buffer.length = (size_t)0;

    // mid is message id.
    // pid is identity.
    // sid is none.
    // uid is none.
    packet_head.mid = mmRqMidUInt32_FinNone;
    packet_head.pid = fountain_unicast->identity;
    packet_head.sid = 0;
    packet_head.uid = 0;

    // ss_remote here is lock free.
    __static_mmRqUdpServer_SendRepeatedMassageUnicast(
        p,
        &packet_head,
        MM_MSG_COMM_HEAD_SIZE,
        &buffer,
        &fountain_unicast->ss_remote);

    mmPacketHead_Destroy(&packet_head);
    mmByteBuffer_Destroy(&buffer);
}
static void __static_mmRqUdpServer_UpdateLossStateRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(u);
    struct mmRqUdp* rq_udp = (struct mmRqUdp*)(v);
    struct mmRqFountain* fountain_unicast = &rq_udp->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();

        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmUInt32_t mulcast_i_packet_number = 0;
        mmUInt32_t unicast_i_packet_number = 0;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_ILock(fountain_mulcast);
        
        mulcast_i_packet_number = (mmUInt32_t)(
            fountain_mulcast->rq_i.packet_number -
            fountain_mulcast->rq_i_packet_number_interval);
        
        mmRqFountain_SetIPacketNumberUpdate(fountain_mulcast, usec_current);
        mmRqFountain_IUnlock(fountain_mulcast);

        mmRqFountain_ILock(fountain_unicast);
        
        unicast_i_packet_number = (mmUInt32_t)(
            fountain_unicast->rq_i.packet_number -
            fountain_unicast->rq_i_packet_number_interval);
        
        mmRqFountain_SetIPacketNumberUpdate(fountain_unicast, usec_current);
        mmRqFountain_IUnlock(fountain_unicast);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is l(mmUInt32_t) mulcast recv message packet number, r(mmUInt32_t) unicast recv message packet number.
        // uid is timecode us.
        packet_head.mid = mmRqMidUInt32_LossUpd;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = mmByte_UInt64A(mulcast_i_packet_number, unicast_i_packet_number);
        packet_head.uid = usec_current;

        __static_mmRqUdpServer_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            &fountain_unicast->ss_remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpServer_UpdateIdxsStateRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v)
{
    struct mmRqUdpServer* p = (struct mmRqUdpServer*)(u);
    struct mmRqUdp* rq_udp = (struct mmRqUdp*)(v);
    struct mmRqFountain* fountain_unicast = &rq_udp->fountain_unicast;
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    if (MM_RQ_ESTABLISHED == fountain_unicast->state)
    {
        mmUInt64_t usec_current = mmTime_CurrentUSec();

        struct mmByteBuffer buffer;
        struct mmPacketHead packet_head;

        mmUInt32_t mulcast_index = 0;
        mmUInt32_t unicast_index = 0;

        mmByteBuffer_Init(&buffer);
        mmPacketHead_Init(&packet_head);

        mmRqFountain_ILock(fountain_mulcast);
        mulcast_index = fountain_mulcast->rq_i_packet_id;
        mmRqFountain_SetIPacketIndexpUpdate(fountain_mulcast, usec_current);
        mmRqFountain_IUnlock(fountain_mulcast);

        mmRqFountain_ILock(fountain_unicast);
        unicast_index = fountain_unicast->rq_i_packet_id;
        mmRqFountain_SetIPacketIndexpUpdate(fountain_unicast, usec_current);
        mmRqFountain_IUnlock(fountain_unicast);

        // make sure not NULL.
        buffer.buffer = (mmUInt8_t*)&buffer;
        buffer.offset = (size_t)0;
        buffer.length = (size_t)0;
        //
        // mid is message id.
        // pid is identity.
        // sid is l(mmUInt32_t) mulcast feedback perfect index, r(mmUInt32_t) unicast feedback perfect index.
        // uid is timecode us.
        packet_head.mid = mmRqMidUInt32_IdxsUpd;
        packet_head.pid = fountain_unicast->identity;
        packet_head.sid = mmByte_UInt64A(mulcast_index, unicast_index);
        packet_head.uid = usec_current;

        __static_mmRqUdpServer_SendRepeatedMassageUnicast(
            p,
            &packet_head,
            MM_MSG_COMM_HEAD_SIZE,
            &buffer,
            &fountain_unicast->ss_remote);

        mmByteBuffer_Destroy(&buffer);
        mmPacketHead_Destroy(&packet_head);
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        //
        mmLogger_LogT(gLogger, "%s %d ignore unknow message state: %u.",
            __FUNCTION__,
            __LINE__,
            fountain_unicast->state);
    }
}
static void __static_mmRqUdpServer_NumberCalculationRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v)
{
    struct __mmRqUdpServerPacketNumberArgument* _argument = (struct __mmRqUdpServerPacketNumberArgument*)(u);
    struct mmRqUdp* rq_udp = (struct mmRqUdp*)(v);
    struct mmRqUdpMulcast* udp_mulcast = &rq_udp->udp_mulcast;

    float mulcast_expected_loss = 0;

    mmRqUdpMulcast_Lock(udp_mulcast);
    mulcast_expected_loss = udp_mulcast->expected_loss;
    mmRqUdpMulcast_Unlock(udp_mulcast);

    _argument->fountain_number += 1;
    _argument->sampled_summate += mulcast_expected_loss;
}
static void __static_mmRqUdpServer_IndexpCalculationRbtreeVisibleU32Handle(void* obj, void* u, mmUInt32_t k, void* v)
{
    struct __mmRqUdpServerPacketIndexpArgument* _argument = (struct __mmRqUdpServerPacketIndexpArgument*)(u);
    struct mmRqUdp* rq_udp = (struct mmRqUdp*)(v);
    struct mmRqUdpMulcast* udp_mulcast = &rq_udp->udp_mulcast;

    mmUInt32_t mulcast_index = 0;

    mmRqUdpMulcast_Lock(udp_mulcast);
    mulcast_index = udp_mulcast->o_packet_indexp;
    mmRqUdpMulcast_Unlock(udp_mulcast);

    _argument->fountain_number += 1;
    _argument->max_index = _argument->max_index > mulcast_index ? _argument->max_index : mulcast_index;
    _argument->min_index = _argument->min_index < mulcast_index ? _argument->min_index : mulcast_index;
}
static void __static_mmRqUdpServer_MulcastOPersistenceHandle(struct mmRqUdpServer* p)
{
    struct mmRqFountain* fountain_mulcast = &p->fountain_mulcast;

    struct __mmRqUdpServerPacketIdxArgument _send_argument;

    _send_argument.rq_udp_server = p;
    _send_argument.fountain = fountain_mulcast;
    _send_argument.udp = &p->headset.udp;

    // time cast ~= 6.5 ms.
    mmRqDatabase_Lock(&p->database_mulcast_o);

    mmRqDatabase_TransactionB(&p->database_mulcast_o);

    mmRqFountain_OLock(fountain_mulcast);
    mmRqFountain_FeedbackPerfectO(fountain_mulcast, &_send_argument);
    mmRqFountain_OUnlock(fountain_mulcast);

    mmRqDatabase_TransactionE(&p->database_mulcast_o);

    mmRqDatabase_Fflush(&p->database_mulcast_o);

    mmRqDatabase_Unlock(&p->database_mulcast_o);
}

static void
__static_mmRqUdpServer_ORepairdHandle(
    struct mmRqUdpServer* p,
    struct mmRqFountain* fountain,
    void* idx,
    mmUInt32_t max_index)
{
    mmUInt32_t max_repaird_number = 0;
    mmUInt32_t scroll_index = 0;

    mmRqFountain_OLock(fountain);
    max_repaird_number = fountain->max_repaird_number;
    scroll_index = fountain->rq_o.scroll_index;
    mmRqFountain_OUnlock(fountain);

    if (max_index < scroll_index)
    {
        struct mmRqDatabase* database = &p->database_mulcast_o;

        struct __mmRqUdpServerPacketDatabaseArgument _database_argument;

        mmUInt32_t b = 0;
        mmUInt32_t e = 0;

        _database_argument.rq_udp_server = p;
        _database_argument.fountain = fountain;
        _database_argument.idx = idx;

        b = max_index;
        e = max_index + max_repaird_number;
        e = e < scroll_index ? e : scroll_index;
        e = 0 == e ? 0 : e - 1;

        // time cast ~= (476, 1032) * 85 = (40460, 87720) us ~= (54761, 56727, 67715) us.
        mmRqDatabase_Lock(database);
        mmRqDatabase_TransactionB(database);
        mmRqDatabase_Select(database, b, e, &__static_mmRqUdpServer_ORepairdDatabaseSelect, &_database_argument);
        mmRqDatabase_TransactionE(database);
        mmRqDatabase_Unlock(database);
    }
    mmRqFountain_OLock(fountain);
    mmRqFountain_GenerateRepairdO(fountain, idx, max_index);
    mmRqFountain_OUnlock(fountain);
}

static void __static_mmRqUdpServer_ORepairdDatabaseSelect(void* obj, mmUInt32_t i, struct mmByteBuffer* byte_buffer, void* v)
{
    struct __mmRqUdpServerPacketDatabaseArgument* _database_argument = (struct __mmRqUdpServerPacketDatabaseArgument*)(v);
    struct mmRqFountain* fountain = _database_argument->fountain;
    void* idx = _database_argument->idx;

    // time cast ~= (476, 1032) us.
    mmRqFountain_OLock(fountain);
    mmRqFountain_GenerateSymbolsO(fountain, idx, i, byte_buffer);
    mmRqFountain_OUnlock(fountain);
}
