/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqO_h__
#define __mmRqO_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmPoolElement.h"

#include "container/mmVectorVpt.h"

#include "raptorq/mmRaptorqContext.h"

#include "mmRqPacket.h"

#include "mmRqElem.h"
#include "mmRqCallback.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

#define MM_RQ_O_PACKET_ALIGN_DEFAULT 4

// expected packet loss default 6%.
#define MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT 0.06f
// expected packet loss max     66%.
#define MM_RQ_O_PACKET_EXPECTED_LOSS_MAX 0.66f

// logic frequency default design for 20
// time = 128 / 20 = 6.4  seconds
// time = 128 / 60 = 2.13 seconds
#define MM_RQ_O_LENGTH_DEFAULT 128
#define MM_RQ_O_CHUNK_SIZE_DEFAULT 256

#define MM_RQ_O_OVERHEAD_ENCODE_DEFAULT 3

struct mmRqO
{
    struct mmRqCallback callback;

    struct mmPoolElement pool_element;
    struct mmVectorVpt vector_vpt;
    // length for sequence
    mmUInt32_t length;

    // duplicate sequence the same number.
    mmUInt64_t duplicate;
    // discard sequence timecode invalid number.
    mmUInt64_t discard;
    // packet number.
    mmUInt64_t packet_number;

    // sampling rate.
    mmUInt32_t sampling_rate;
    // communication frame rate.
    mmUInt32_t frame_rate;

    // timestamp interval interval = sampling_rate/frame_rate relatively fixed.
    mmUInt32_t timestamp_interval;
    // timestamp initial.
    mmUInt32_t timestamp_initial;

    // default MM_RQ_O_OVERHEAD_ENCODE_DEFAULT
    mmUInt32_t overhead_encode;

    // finish cursor.
    mmUInt32_t finish_index;
    // scroll cursor.
    mmUInt32_t scroll_index;

    // the next of max cursor.
    mmUInt32_t max_index;

    // default is MM_RQ_O_PACKET_ALIGN_DEFAULT.
    mmUInt8_t packet_align;

    // expected packet loss default MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT.
    float expected_loss;

    // user data.
    void* u;
};

MM_EXPORT_RQ void mmRqO_Init(struct mmRqO* p);
MM_EXPORT_RQ void mmRqO_Destroy(struct mmRqO* p);
MM_EXPORT_RQ void mmRqO_Reset(struct mmRqO* p);
MM_EXPORT_RQ void mmRqO_ResetContext(struct mmRqO* p);
MM_EXPORT_RQ void mmRqO_SetRate(struct mmRqO* p, mmUInt32_t sampling_rate, mmUInt32_t frame_rate);
MM_EXPORT_RQ void mmRqO_SetTimestampInitial(struct mmRqO* p, mmUInt32_t timestamp_initial);
MM_EXPORT_RQ void mmRqO_SetLength(struct mmRqO* p, mmUInt32_t length);
MM_EXPORT_RQ void mmRqO_SetChunkSize(struct mmRqO* p, mmUInt32_t chunk_size);
MM_EXPORT_RQ void mmRqO_SetOverhead(struct mmRqO* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqO_SetCallback(struct mmRqO* p, struct mmRqCallback* callback);
MM_EXPORT_RQ void mmRqO_SetPacketAlign(struct mmRqO* p, mmUInt8_t packet_align);
MM_EXPORT_RQ void mmRqO_SetExpectedLoss(struct mmRqO* p, float expected_loss);
MM_EXPORT_RQ void mmRqO_SetContext(struct mmRqO* p, void* u);
MM_EXPORT_RQ void mmRqO_Clear(struct mmRqO* p);

// idx 0xffffffff is notify entire.
MM_EXPORT_RQ void mmRqO_GenerateSourced(struct mmRqO* p, void* idx, mmUInt32_t id, struct mmByteBuffer* byte_buffer);
MM_EXPORT_RQ void mmRqO_GenerateRepaird(struct mmRqO* p, void* idx, mmUInt32_t id);
MM_EXPORT_RQ void mmRqO_FeedbackSourced(struct mmRqO* p, void* idx);
MM_EXPORT_RQ void mmRqO_AssemblyPerfect(struct mmRqO* p, void* idx, mmUInt32_t id);
MM_EXPORT_RQ void mmRqO_FeedbackPerfect(struct mmRqO* p, void* idx);

MM_EXPORT_RQ void mmRqO_GenerateSymbols(struct mmRqO* p, void* idx, mmUInt32_t id, struct mmByteBuffer* byte_buffer);

MM_EXPORT_RQ float mmRqO_ExpectedLoss(mmUInt32_t i_packet_number, mmUInt32_t o_packet_number);

#include "core/mmSuffix.h"

#endif//__mmRqO_h__
