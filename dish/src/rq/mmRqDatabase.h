/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqDatabase_h__
#define __mmRqDatabase_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmByte.h"
#include "core/mmSpinlock.h"

#include "mmRqExport.h"

#include "track/mmTrackAssets.h"

#include "core/mmPrefix.h"

typedef void(*mmRqDatabaseSelectFunc)(void* obj, mmUInt32_t i, struct mmByteBuffer* byte_buffer, void* v);

struct mmRqDatabase
{
    struct mmTrackAssets track_assets;

    /* External lock. */
    mmAtomic_t locker;
};

MM_EXPORT_RQ void mmRqDatabase_Init(struct mmRqDatabase* p);
MM_EXPORT_RQ void mmRqDatabase_Destroy(struct mmRqDatabase* p);

MM_EXPORT_RQ void mmRqDatabase_Lock(struct mmRqDatabase* p);
MM_EXPORT_RQ void mmRqDatabase_Unlock(struct mmRqDatabase* p);

MM_EXPORT_RQ void mmRqDatabase_SetFilepath(struct mmRqDatabase* p, const char* filepath);

MM_EXPORT_RQ void mmRqDatabase_Fopen(struct mmRqDatabase* p);
MM_EXPORT_RQ void mmRqDatabase_Fflush(struct mmRqDatabase* p);
MM_EXPORT_RQ void mmRqDatabase_Fclose(struct mmRqDatabase* p);

MM_EXPORT_RQ void mmRqDatabase_TransactionB(struct mmRqDatabase* p);
MM_EXPORT_RQ void mmRqDatabase_TransactionE(struct mmRqDatabase* p);

// [b, e]
MM_EXPORT_RQ void
mmRqDatabase_Select(
    struct mmRqDatabase* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmRqDatabaseSelectFunc handle,
    void* v);

MM_EXPORT_RQ void
mmRqDatabase_Insert(
    struct mmRqDatabase* p,
    mmUInt32_t i,
    struct mmByteBuffer* byte_buffer);

#include "core/mmSuffix.h"

#endif//__mmRqDatabase_h__
