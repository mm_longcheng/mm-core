/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqUdpClient_h__
#define __mmRqUdpClient_h__

#include "core/mmCore.h"
#include "core/mmTimer.h"
#include "core/mmSignalTask.h"

#include "net/mmClientUdp.h"
#include "net/mmPacketQueue.h"

#include "mmRqPacket.h"
#include "mmRqElem.h"
#include "mmRqI.h"
#include "mmRqO.h"
#include "mmRqFountain.h"
#include "mmRqDatabase.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"


#define MM_RQ_UDP_CLIENT_COMMON_MESSAGE_REPEATED_NUMBER 3

// milliseconds.
#define MM_RQ_UDP_CLIENT_MSEC_UPDATE_PING_STATE 5000
#define MM_RQ_UDP_CLIENT_MSEC_UPDATE_LOSS_STATE 3000
#define MM_RQ_UDP_CLIENT_MSEC_UPDATE_IDXP_STATE 1000

// milliseconds. mulcast i persistence.
#define MM_RQ_UDP_CLIENT_MSEC_UPDATE_MULCAST_I_PERSISTENCE 3000

// ipv4 "224.0.1.1" "0.0.0.0"
// ipv6 "FF01::1:1" ""
#define MM_RQ_UDP_CLIENT_MR_MULTIADDR_DEFAULT "224.0.1.1"
#define MM_RQ_UDP_CLIENT_MR_INTERFACE_DEFAULT "0.0.0.0"

// overhead
//    0  99%
//    1  99.99%
//    2  99.9999%
//    3  99.999999%
// here we have repaird protocol, overhead = 0 is enough.
#define MM_RQ_UDP_CLIENT_I_OVERHEAD_DECODE_DEFAULT 0
#define MM_RQ_UDP_CLIENT_O_OVERHEAD_ENCODE_DEFAULT 0

#define MM_RQ_UDP_CLIENT_I_MULCAST_LENGTH_DEFAULT 512
#define MM_RQ_UDP_CLIENT_O_MULCAST_LENGTH_DEFAULT 128

typedef void(*mmRqUdpClientHandleFunc)(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

struct mmRqUdpClientCallback
{
    mmRqUdpClientHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};

MM_EXPORT_RQ void mmRqUdpClientCallback_Init(struct mmRqUdpClientCallback* p);
MM_EXPORT_RQ void mmRqUdpClientCallback_Destroy(struct mmRqUdpClientCallback* p);

MM_EXPORT_RQ void
mmRqUdpClient_Handledefault(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

struct mmRqUdpClient
{
    struct mmClientUdp client_udp_unicast;
    struct mmClientUdp client_udp_mulcast;

    struct mmRqFountain fountain_unicast;
    struct mmRqFountain fountain_mulcast;

    struct mmRqDatabase database_mulcast_i;

    struct mmTimer timer;

    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_unicast_q;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_unicast_n;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_mulcast_q;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_mulcast_n;

    struct mmRqUdpClientCallback callback_unicast_q;
    struct mmRqUdpClientCallback callback_unicast_n;
    struct mmRqUdpClientCallback callback_mulcast_q;
    struct mmRqUdpClientCallback callback_mulcast_n;

    struct mmString mr_multiaddr;
    struct mmString mr_interface;

    mmAtomic_t rbtree_locker_unicast_q;
    mmAtomic_t rbtree_locker_unicast_n;
    mmAtomic_t rbtree_locker_mulcast_q;
    mmAtomic_t rbtree_locker_mulcast_n;

    mmAtomic_t locker;

    // milliseconds.default is MM_RQ_UDP_CLIENT_MSEC_UPDATE_PING_STATE.
    mmMSec_t msec_update_ping_state;
    // milliseconds.default is MM_RQ_UDP_CLIENT_MSEC_UPDATE_LOSS_STATE.
    mmMSec_t msec_update_loss_state;
    // milliseconds.default is MM_RQ_UDP_CLIENT_MSEC_UPDATE_IDXP_STATE.
    mmMSec_t msec_update_idxp_state;

    // milliseconds.default is MM_RQ_UDP_CLIENT_MSEC_UPDATE_MULCAST_I_PERSISTENCE.
    mmMSec_t msec_update_mulcast_i_persistence;

    // default is MM_RQ_UDP_CLIENT_COMMON_MESSAGE_REPEATED_NUMBER.
    mmUInt32_t repeated_number;

    // cache for mulcast max_index value.
    mmUInt32_t mulcast_max_index;

    // cache for unicast timecode_rtt value, microseconds.
    mmUInt64_t unicast_timecode_rtt;
    // cache for mulcast packet loss value.
    mmUInt32_t unicast_packet_loss;
    // cache for mulcast expected_loss value.
    float unicast_expected_loss;

    // user data.
    void* u;
};

MM_EXPORT_RQ void mmRqUdpClient_Init(struct mmRqUdpClient* p);
MM_EXPORT_RQ void mmRqUdpClient_Destroy(struct mmRqUdpClient* p);

/* locker order is
*     client_udp_unicast
*     client_udp_mulcast
*     fountain_unicast
*     fountain_mulcast
*     timer
*     locker
*/
MM_EXPORT_RQ void mmRqUdpClient_Lock(struct mmRqUdpClient* p);
MM_EXPORT_RQ void mmRqUdpClient_Unlock(struct mmRqUdpClient* p);

/* reset api no locker. */
MM_EXPORT_RQ void mmRqUdpClient_ResetUnicastContext(struct mmRqUdpClient* p);
MM_EXPORT_RQ void mmRqUdpClient_ResetMulcastContext(struct mmRqUdpClient* p);

/* assign api no locker. */
MM_EXPORT_RQ void mmRqUdpClient_SetUnicastLength(struct mmRqUdpClient* p, mmUInt32_t length);
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastLength(struct mmRqUdpClient* p, mmUInt32_t length);

MM_EXPORT_RQ void mmRqUdpClient_SetDatabaseMulcastIFilepath(struct mmRqUdpClient* p, const char* filepath);

MM_EXPORT_RQ void mmRqUdpClient_SetUnicastOverheadEncode(struct mmRqUdpClient* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqUdpClient_SetUnicastOverheadDecode(struct mmRqUdpClient* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastOverheadEncode(struct mmRqUdpClient* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastOverheadDecode(struct mmRqUdpClient* p, mmUInt32_t overhead);

// assign context handle.
MM_EXPORT_RQ void mmRqUdpClient_SetContext(struct mmRqUdpClient* p, void* u);
MM_EXPORT_RQ void mmRqUdpClient_SetRepeatedNumber(struct mmRqUdpClient* p, mmUInt32_t repeated_number);

// assign mulcast mr_interface.
MM_EXPORT_RQ void mmRqUdpClient_SetMulcastMrInterface(struct mmRqUdpClient* p, const char* mr_interface);

// assign native unicast address.
MM_EXPORT_RQ void mmRqUdpClient_SetNativeUnicast(struct mmRqUdpClient* p, const char* node, mmUShort_t port);
// assign native mulcast address.
MM_EXPORT_RQ void mmRqUdpClient_SetNativeMulcast(struct mmRqUdpClient* p, const char* node, mmUShort_t port);
// assign remote unicast address.
MM_EXPORT_RQ void mmRqUdpClient_SetRemoteUnicast(struct mmRqUdpClient* p, const char* node, mmUShort_t port);
// assign remote mulcast address.
MM_EXPORT_RQ void mmRqUdpClient_SetRemoteMulcast(struct mmRqUdpClient* p, const char* node, mmUShort_t port);

MM_EXPORT_RQ void mmRqUdpClient_SetQDefaultCallbackUnicast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback);
MM_EXPORT_RQ void mmRqUdpClient_SetNDefaultCallbackUnicast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback);
MM_EXPORT_RQ void mmRqUdpClient_SetQDefaultCallbackMulcast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback);
MM_EXPORT_RQ void mmRqUdpClient_SetNDefaultCallbackMulcast(struct mmRqUdpClient* p, struct mmRqUdpClientCallback* callback);
// assign queue callback.at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetQHandleUnicast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback);
// assign net callback.not at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetNHandleUnicast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback);
// assign queue callback.at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetQHandleMulcast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback);
// assign net callback.not at main thread.
MM_EXPORT_RQ void mmRqUdpClient_SetNHandleMulcast(struct mmRqUdpClient* p, mmUInt32_t id, mmRqUdpClientHandleFunc callback);

MM_EXPORT_RQ void mmRqUdpClient_SetStateCheckFlag(struct mmRqUdpClient* p, mmUInt8_t flag);
MM_EXPORT_RQ void mmRqUdpClient_SetStateQueueFlag(struct mmRqUdpClient* p, mmUInt8_t flag);

MM_EXPORT_RQ void mmRqUdpClient_ClearHandleRbtree(struct mmRqUdpClient* p);

// fopen socket.
MM_EXPORT_RQ void mmRqUdpClient_FopenSocket(struct mmRqUdpClient* p);
// bind.
MM_EXPORT_RQ void mmRqUdpClient_Bind(struct mmRqUdpClient* p);
// close socket.
MM_EXPORT_RQ void mmRqUdpClient_CloseSocket(struct mmRqUdpClient* p);
// shutdown socket.
MM_EXPORT_RQ void mmRqUdpClient_ShutdownSocket(struct mmRqUdpClient* p);

MM_EXPORT_RQ void mmRqUdpClient_MulcastConnect(struct mmRqUdpClient* p);
MM_EXPORT_RQ void mmRqUdpClient_MulcastShutdown(struct mmRqUdpClient* p);

MM_EXPORT_RQ void mmRqUdpClient_MulcastIPersistence(struct mmRqUdpClient* p);

MM_EXPORT_RQ int mmRqUdpClient_MulcastJoin(struct mmRqUdpClient* p, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_RQ int mmRqUdpClient_MulcastDrop(struct mmRqUdpClient* p, const char* mr_multiaddr, const char* mr_interface);

// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_RQ void mmRqUdpClient_ThreadHandleRecv(struct mmRqUdpClient* p);
// main thread trigger self thread handle.such as gl thread.
MM_EXPORT_RQ void mmRqUdpClient_ThreadHandleSend(struct mmRqUdpClient* p);

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushRecvUnicast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushRecvMulcast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushSendUnicast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

// push a pack and context tp queue.this function will copy alloc pack.
MM_EXPORT_RQ void
mmRqUdpClient_PushSendMulcast(
    struct mmRqUdpClient* p,
    void* obj,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

// send buffer, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_RQ int
mmRqUdpClient_SendBufferUnicast(
    struct mmRqUdpClient* p,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet);

// send buffer, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_RQ int
mmRqUdpClient_SendBufferMulcast(
    struct mmRqUdpClient* p,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet);

// send message, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_RQ int
mmRqUdpClient_SendMessageUnicast(
    struct mmRqUdpClient* p,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

// send message, this api will push the message to quque wait for asynchronous send.
// this api is lock free.
MM_EXPORT_RQ int
mmRqUdpClient_SendMessageMulcast(
    struct mmRqUdpClient* p,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_RQ void mmRqUdpClient_FlushSignalUnicast(struct mmRqUdpClient* p);
// use for trigger the flush send thread signal.
// is useful for main thread asynchronous send message.
MM_EXPORT_RQ void mmRqUdpClient_FlushSignalMulcast(struct mmRqUdpClient* p);

// start wait thread.
MM_EXPORT_RQ void mmRqUdpClient_Start(struct mmRqUdpClient* p);
// interrupt wait thread.
MM_EXPORT_RQ void mmRqUdpClient_Interrupt(struct mmRqUdpClient* p);
// shutdown wait thread.
MM_EXPORT_RQ void mmRqUdpClient_Shutdown(struct mmRqUdpClient* p);
// join wait thread.
MM_EXPORT_RQ void mmRqUdpClient_Join(struct mmRqUdpClient* p);

#include "core/mmSuffix.h"

#endif//__mmRqUdpClient_h__
