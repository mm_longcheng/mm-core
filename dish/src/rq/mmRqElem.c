/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqElem.h"

MM_EXPORT_RQ void mmRqElem_Init(struct mmRqElem* p)
{
    mmRaptorqContext_Init(&p->rq);
    mmStreambuf_Init(&p->streambuf_buffer);
    mmStreambuf_Init(&p->streambuf_packet);
    mmRbtreeU32U32_Init(&p->esi_num);
    p->id = 0;
    p->overhead = 0;
    p->state = RQ_ELEM_STATE_HANDLE;
}
MM_EXPORT_RQ void mmRqElem_Destroy(struct mmRqElem* p)
{
    mmRaptorqContext_Destroy(&p->rq);
    mmStreambuf_Destroy(&p->streambuf_buffer);
    mmStreambuf_Destroy(&p->streambuf_packet);
    mmRbtreeU32U32_Destroy(&p->esi_num);
    p->id = 0;
    p->overhead = 0;
    p->state = RQ_ELEM_STATE_HANDLE;
}
MM_EXPORT_RQ void mmRqElem_Reset(struct mmRqElem* p)
{
    mmRaptorqContext_Reset(&p->rq);
    mmStreambuf_Reset(&p->streambuf_buffer);
    mmStreambuf_Reset(&p->streambuf_packet);
    mmRqElem_ClearEsiNumber(p);
    p->id = 0;
    p->overhead = 0;
    p->state = RQ_ELEM_STATE_HANDLE;
}
MM_EXPORT_RQ void mmRqElem_SetId(struct mmRqElem* p, mmUInt32_t id)
{
    p->id = id;
}
MM_EXPORT_RQ void mmRqElem_SetOverhead(struct mmRqElem* p, mmUInt32_t overhead)
{
    p->overhead = overhead;
}
MM_EXPORT_RQ void mmRqElem_SetState(struct mmRqElem* p, mmUInt32_t state)
{
    p->state = state;
}
MM_EXPORT_RQ void mmRqElem_SetDecoder(struct mmRqElem* p, mmUInt64_t oti_common, mmUInt32_t oti_scheme)
{
    mmRaptorqContext_DecoderAssign(&p->rq, oti_common, oti_scheme);
}
MM_EXPORT_RQ void mmRqElem_SetEncoder(struct mmRqElem* p, mmUInt64_t F, mmUInt16_t T, mmUInt16_t SS, mmUInt8_t Al, size_t WS)
{
    mmRaptorqContext_EncoderAssign(&p->rq, F, T, SS, Al, WS);
}
MM_EXPORT_RQ int mmRqElem_ContextDecode(struct mmRqElem* p, struct mmByteBuffer* byte_buffer)
{
    return mmRaptorqContext_Decode(&p->rq, byte_buffer);
}
MM_EXPORT_RQ int mmRqElem_ContextEncode(struct mmRqElem* p, struct mmByteBuffer* byte_buffer)
{
    return mmRaptorqContext_Encode(&p->rq, byte_buffer);
}
MM_EXPORT_RQ void mmRqElem_SetEsiNumber(struct mmRqElem* p, mmUInt8_t sbn, mmUInt32_t esi_number)
{
    mmRbtreeU32U32_Set(&p->esi_num, sbn, esi_number);
}
MM_EXPORT_RQ mmUInt32_t mmRqElem_GetEsiNumber(struct mmRqElem* p, mmUInt8_t sbn)
{
    struct mmRbtreeU32U32Iterator* it = mmRbtreeU32U32_GetIterator(&p->esi_num, sbn);
    return NULL == it ? 0 : it->v;
}
MM_EXPORT_RQ void mmRqElem_ClearEsiNumber(struct mmRqElem* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32U32Iterator* it = NULL;
    //
    n = mmRb_First(&p->esi_num.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32U32Iterator, n);
        n = mmRb_Next(n);
        it->v = 0;
    }
}
MM_EXPORT_RQ void mmRqElem_IncreaseEsiNumber(struct mmRqElem* p, mmUInt8_t sbn)
{
    mmUInt32_t n = mmRqElem_GetEsiNumber(p, sbn);
    mmRqElem_SetEsiNumber(p, sbn, n++);
}
MM_EXPORT_RQ void mmRqElem_DecreaseEsiNumber(struct mmRqElem* p, mmUInt8_t sbn)
{
    mmUInt32_t n = mmRqElem_GetEsiNumber(p, sbn);
    mmRqElem_SetEsiNumber(p, sbn, n--);
}

