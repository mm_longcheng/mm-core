/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqFountain_h__
#define __mmRqFountain_h__

#include "core/mmCore.h"
#include "core/mmTime.h"

#include "net/mmSockaddr.h"
#include "net/mmPacket.h"
#include "net/mmMessageCoder.h"

#include "mmRqPacket.h"

#include "mmRqElem.h"

#include "mmRqI.h"
#include "mmRqO.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

// most of time, the net sampling rate is 60, msec_update_idxp default is 1 second.
// repaird 2 second 2 * 60 = 120 is enough.
// 110 * 120 = 13200 ~= 12.8906  kb/s
#define MM_RQ_FOUNTAIN_MAX_REPAIRD_NUMBER_DEFAULT 120
// perfect default 1024.
#define MM_RQ_FOUNTAIN_MAX_PERFECT_NUMBER_DEFAULT 1024

struct mmRqFountain
{
    // remote end point address.
    // this data is lock free.
    struct mmSockaddr ss_remote;

    // use for mmRqO packet.
    // locker_o will lock this data.
    struct mmStreambuf streambuf_packet;

    // locker_i will lock this data.
    struct mmRqI rq_i;

    // locker_o will lock this data.
    struct mmRqO rq_o;

    /* locker order is locker_s -> i_locker -> o_locker. */
    mmAtomic_t locker_s;
    mmAtomic_t locker_i;
    mmAtomic_t locker_o;

    // identity is lock free.
    mmUInt32_t identity;

    // default MM_RQ_FOUNTAIN_MAX_REPAIRD_NUMBER_DEFAULT.
    // max_repaird_number is status data lock free.
    mmUInt32_t max_repaird_number;

    // default MM_RQ_FOUNTAIN_MAX_PERFECT_NUMBER_DEFAULT.
    // max_perfect_number is status data lock free.
    mmUInt32_t max_perfect_number;

    // logic mmRqO packet_id
    // locker_i will lock this data.
    mmUInt32_t rq_i_packet_id;

    // logic mmRqO packet_id
    // locker_o will lock this data.
    mmUInt32_t rq_o_packet_id;

    // microseconds, timecode for context create.
    // locker_s will lock this data.
    mmUInt64_t timecode_create;

    // microseconds, timecode for context update.
    // locker_s will lock this data.
    mmUInt64_t timecode_update;

    // microseconds. rtt.
    // locker_s will lock this data.
    mmUInt64_t timecode_rtt;

    // microseconds. rq_i packet number update timecode.
    // locker_i will lock this data.
    mmUInt64_t timecode_i_packet_number_update;

    // microseconds. rq_o packet number update timecode.
    // locker_o will lock this data.
    mmUInt64_t timecode_o_packet_number_update;

    // microseconds. rq_i packet indexp update timecode.
    // locker_i will lock this data.
    mmUInt64_t timecode_i_packet_indexp_update;

    // microseconds. rq_o packet indexp update timecode.
    // locker_o will lock this data.
    mmUInt64_t timecode_o_packet_indexp_update;

    // rq_i packet number at interval time.
    // locker_i will lock this data.
    mmUInt64_t rq_i_packet_number_interval;

    // rq_i packet number at interval time.
    // locker_o will lock this data.
    mmUInt64_t rq_o_packet_number_interval;

    // locker_s will lock this data.
    mmUInt32_t state;
};
MM_EXPORT_RQ void mmRqFountain_Init(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_Destroy(struct mmRqFountain* p);

MM_EXPORT_RQ void mmRqFountain_Reset(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_ResetContext(struct mmRqFountain* p);

/* locker order is
*     locker_s
*     locker_i
*     locker_o
*/
MM_EXPORT_RQ void mmRqFountain_Lock(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_Unlock(struct mmRqFountain* p);

MM_EXPORT_RQ void mmRqFountain_SLock(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_SUnlock(struct mmRqFountain* p);
//
MM_EXPORT_RQ void mmRqFountain_ILock(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_IUnlock(struct mmRqFountain* p);
//
MM_EXPORT_RQ void mmRqFountain_OLock(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_OUnlock(struct mmRqFountain* p);

MM_EXPORT_RQ void mmRqFountain_SetRemote(struct mmRqFountain* p, const char* node, mmUShort_t port);
MM_EXPORT_RQ void mmRqFountain_SetRemoteStorage(struct mmRqFountain* p, struct mmSockaddr* remote);
MM_EXPORT_RQ void mmRqFountain_SetRqIPacketId(struct mmRqFountain* p, mmUInt32_t rq_i_packet_id);
MM_EXPORT_RQ void mmRqFountain_SetRqOPacketId(struct mmRqFountain* p, mmUInt32_t rq_o_packet_id);
MM_EXPORT_RQ void mmRqFountain_SetTimecodeCreate(struct mmRqFountain* p, mmUInt64_t timecode_create);
MM_EXPORT_RQ void mmRqFountain_SetTimecodeUpdate(struct mmRqFountain* p, mmUInt64_t timecode_update);
MM_EXPORT_RQ void mmRqFountain_SetTimecodeRtt(struct mmRqFountain* p, mmUInt64_t timecode_rtt);
MM_EXPORT_RQ void mmRqFountain_SetState(struct mmRqFountain* p, mmUInt32_t state);
MM_EXPORT_RQ void mmRqFountain_SetIdentity(struct mmRqFountain* p, mmUInt32_t identity);
MM_EXPORT_RQ void mmRqFountain_SetMaxRepairdNumber(struct mmRqFountain* p, mmUInt32_t max_repaird_number);
MM_EXPORT_RQ void mmRqFountain_SetMaxPerfectNumber(struct mmRqFountain* p, mmUInt32_t max_perfect_number);

MM_EXPORT_RQ void mmRqFountain_SetRate(struct mmRqFountain* p, mmUInt32_t sampling_rate, mmUInt32_t frame_rate);
MM_EXPORT_RQ void mmRqFountain_SetTimestampInitial(struct mmRqFountain* p, mmUInt32_t timestamp_initial);
MM_EXPORT_RQ void mmRqFountain_SetLength(struct mmRqFountain* p, mmUInt32_t length);
MM_EXPORT_RQ void mmRqFountain_SetChunkSize(struct mmRqFountain* p, mmUInt32_t chunk_size);
MM_EXPORT_RQ void mmRqFountain_SetOverheadEncode(struct mmRqFountain* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqFountain_SetOverheadDecode(struct mmRqFountain* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqFountain_SetPacketAlign(struct mmRqFountain* p, mmUInt8_t packet_align);
MM_EXPORT_RQ void mmRqFountain_SetExpectedLoss(struct mmRqFountain* p, float expected_loss);

MM_EXPORT_RQ void mmRqFountain_SetIPacketIndexpUpdate(struct mmRqFountain* p, mmUInt64_t timecode);
MM_EXPORT_RQ void mmRqFountain_SetOPacketIndexpUpdate(struct mmRqFountain* p, mmUInt64_t timecode);
MM_EXPORT_RQ void mmRqFountain_SetIPacketNumberUpdate(struct mmRqFountain* p, mmUInt64_t timecode);
MM_EXPORT_RQ void mmRqFountain_SetOPacketNumberUpdate(struct mmRqFountain* p, mmUInt64_t timecode);

MM_EXPORT_RQ void mmRqFountain_IncreaseRqIPacketId(struct mmRqFountain* p);
MM_EXPORT_RQ void mmRqFountain_IncreaseRqOPacketId(struct mmRqFountain* p);

MM_EXPORT_RQ int
mmRqFountain_SendMessage(
    struct mmRqFountain* p,
    void* idx,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

MM_EXPORT_RQ int
mmRqFountain_SendBuffer(
    struct mmRqFountain* p,
    void* idx,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet);
// 
MM_EXPORT_RQ void
mmRqFountain_RecvBuffer(
    struct mmRqFountain* p,
    void* idx,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

MM_EXPORT_RQ void
mmRqFountain_GenerateSymbolsO(
    struct mmRqFountain* p,
    void* idx,
    mmUInt32_t id,
    struct mmByteBuffer* byte_buffer);

MM_EXPORT_RQ void mmRqFountain_GenerateRepairdO(struct mmRqFountain* p, void* idx, mmUInt32_t max_index);
MM_EXPORT_RQ void mmRqFountain_AssemblyPerfectI(struct mmRqFountain* p, void* idx, mmUInt32_t min_index);
MM_EXPORT_RQ void mmRqFountain_AssemblyPerfectO(struct mmRqFountain* p, void* idx, mmUInt32_t min_index);
//
MM_EXPORT_RQ void mmRqFountain_FeedbackPerfectI(struct mmRqFountain* p, void* idx);
MM_EXPORT_RQ void mmRqFountain_FeedbackPerfectO(struct mmRqFountain* p, void* idx);

MM_EXPORT_RQ void mmRqFountain_PoolElementEventProduce(void* obj, void* u, void* v);
MM_EXPORT_RQ void mmRqFountain_PoolElementEventRecycle(void* obj, void* u, void* v);

#include "core/mmSuffix.h"

#endif//__mmRqFountain_h__
