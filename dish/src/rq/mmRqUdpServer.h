/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqUdpServer_h__
#define __mmRqUdpServer_h__

#include "core/mmCore.h"
#include "core/mmTimer.h"

#include "container/mmRbtreeVisibleU32.h"

#include "net/mmHeadset.h"
#include "net/mmRbtreeSockaddr.h"

#include "random/mmXoshiro.h"

#include "dish/mmIdGenerater.h"

#include "mmRqPacket.h"
#include "mmRqElem.h"
#include "mmRqI.h"
#include "mmRqO.h"
#include "mmRqUdp.h"
#include "mmRqDatabase.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

#define MM_RQ_UDP_SERVER_FOUNTAIN_CHUNK_SIZE_DEFAULT 64

#define MM_RQ_UNICAST_LENGTH_DEFAULT 128

#define MM_RQ_UDP_SERVER_COMMON_MESSAGE_REPEATED_NUMBER 3

// milliseconds.
#define MM_RQ_UDP_SERVER_MSEC_UPDATE_STATE 3000
#define MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_NUMBER 5000
#define MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_INDEXP 3000

// milliseconds.
#define MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_STATE 3000
#define MM_RQ_UDP_SERVER_MSEC_UPDATE_IDXP_STATE 1000

// milliseconds. mulcast o persistence.
#define MM_RQ_UDP_SERVER_MSEC_UPDATE_MULCAST_O_PERSISTENCE 3000

// milliseconds.
#define MM_RQ_UDP_SERVER_MSEC_SYN_RECV_TIMEOUT 6000

// ipv4 "224.0.1.1" "0.0.0.0"
// ipv6 "FF01::1:1" ""
#define MM_RQ_UDP_SERVER_MR_MULTIADDR_DEFAULT "224.0.1.1"
#define MM_RQ_UDP_SERVER_MR_INTERFACE_DEFAULT "0.0.0.0"

// overhead
//    0  99%
//    1  99.99%
//    2  99.9999%
//    3  99.999999%
// here we have repaird protocol, overhead = 0 is enough.
#define MM_RQ_UDP_SERVER_I_OVERHEAD_DECODE_DEFAULT 0
#define MM_RQ_UDP_SERVER_O_OVERHEAD_ENCODE_DEFAULT 0

// most of time, the net sampling rate is 60, msec_update_idxp default is 1 second, 1 * 60 = 60 is enough.
#define MM_RQ_UDP_SERVER_MAX_REPAIRD_NUMBER_DEFAULT 60

// milliseconds. Maximum Segment Lifetime.
#define MM_RQ_UDP_SERVER_MSL_DEFAULT 10000

#define MM_RQ_UDP_SERVER_I_MULCAST_LENGTH_DEFAULT 128
#define MM_RQ_UDP_SERVER_O_MULCAST_LENGTH_DEFAULT 512

typedef void(*mmRqUdpServerHandleFunc)(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

struct mmRqUdpServerCallback
{
    mmRqUdpServerHandleFunc Handle;
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_RQ void mmRqUdpServerCallback_Init(struct mmRqUdpServerCallback* p);
MM_EXPORT_RQ void mmRqUdpServerCallback_Destroy(struct mmRqUdpServerCallback* p);

MM_EXPORT_RQ void mmRqUdpServer_HandleDefault(
    void* obj,
    void* u,
    mmUInt32_t identity,
    struct mmPacket* pack,
    struct mmSockaddr* remote);

struct mmRqUdpServer
{
    struct mmHeadset headset;

    struct mmXoshiro256starstar random_context;
    struct mmPoolElement pool_element;

    struct mmRbtreeVisibleU32 rbtree_visible;
    struct mmRbtreeSockaddrVpt rbtree_sockaddr_vpt;

    struct mmRqFountain fountain_mulcast;
    struct mmUInt32IdGenerater id_generater;

    struct mmRqDatabase database_mulcast_o;

    struct mmTimer timer;

    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_unicast;
    // rb tree for msg_id <--> callback.
    struct mmRbtreeU32Vpt rbtree_mulcast;

    struct mmRqUdpServerCallback callback_unicast_n;
    struct mmRqUdpServerCallback callback_mulcast_n;

    struct mmString mr_multiaddr;
    struct mmString mr_interface;

    mmAtomic_t rbtree_locker_unicast;
    mmAtomic_t rbtree_locker_mulcast;

    // the locker for only get instance process thread safe.
    mmAtomic_t instance_locker;
    mmAtomic_t rbtree_sockaddr_locker;
    mmAtomic_t id_generater_locker;

    mmAtomic_t locker;

    // milliseconds.default is MM_RQ_UDP_SERVER_MSL_DEFAULT.
    mmMSec_t msec_timeout_msl;

    // milliseconds.default is MM_RQ_UDP_SERVER_MSEC_UPDATE_STATE.
    mmMSec_t msec_update_state;
    // milliseconds.default is MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_NUMBER.
    mmMSec_t msec_update_loss_number;
    // milliseconds.default is MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_INDEXP.
    mmMSec_t msec_update_loss_indexp;

    // milliseconds.default is MM_RQ_UDP_SERVER_MSEC_UPDATE_LOSS_STATE.
    mmMSec_t msec_update_loss_state;
    // milliseconds.default is MM_RQ_UDP_SERVER_MSEC_UPDATE_IDXP_STATE.
    mmMSec_t msec_update_idxp_state;

    // milliseconds.default is MM_RQ_UDP_SERVER_MSEC_UPDATE_MULCAST_O_PERSISTENCE.
    mmMSec_t msec_update_mulcast_o_persistence;

    // length for sequence
    mmUInt32_t unicast_length;
    // default MM_RQ_UDP_SERVER_O_OVERHEAD_ENCODE_DEFAULT
    mmUInt32_t unicast_overhead_encode;
    // default MM_RQ_UDP_SERVER_I_OVERHEAD_DECODE_DEFAULT
    mmUInt32_t unicast_overhead_decode;

    // default is MM_RQ_UDP_SERVER_COMMON_MESSAGE_REPEATED_NUMBER.
    mmUInt32_t repeated_number;

    // cache for mulcast io timestamp_initial value.
    mmUInt64_t mulcast_io_timestamp_initial;
    // cache for mulcast min_index value.
    mmUInt32_t mulcast_min_index;
    // cache for mulcast max_index value.
    mmUInt32_t mulcast_max_index;
    // cache for mulcast expected_loss value.
    float mulcast_expected_loss;

    // user data.
    void* u;
};

MM_EXPORT_RQ void mmRqUdpServer_Init(struct mmRqUdpServer* p);
MM_EXPORT_RQ void mmRqUdpServer_Destroy(struct mmRqUdpServer* p);

/* locker order is
*     headset
*     pool_element
*     fountain_mulcast
*     timer
*     locker
*/
MM_EXPORT_RQ void mmRqUdpServer_Lock(struct mmRqUdpServer* p);
MM_EXPORT_RQ void mmRqUdpServer_Unlock(struct mmRqUdpServer* p);

/* reset api no locker. */
MM_EXPORT_RQ void mmRqUdpServer_ResetMulcastContext(struct mmRqUdpServer* p);

/* assign api no locker. */
MM_EXPORT_RQ void mmRqUdpServer_SetFountainChunkSize(struct mmRqUdpServer* p, mmUInt32_t chunk_size);

MM_EXPORT_RQ void mmRqUdpServer_SetUnicastLength(struct mmRqUdpServer* p, mmUInt32_t length);
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastLength(struct mmRqUdpServer* p, mmUInt32_t length);

MM_EXPORT_RQ void mmRqUdpServer_SetDatabaseMulcastOFilepath(struct mmRqUdpServer* p, const char* filepath);

MM_EXPORT_RQ void mmRqUdpServer_SetUnicastOverheadEncode(struct mmRqUdpServer* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqUdpServer_SetUnicastOverheadDecode(struct mmRqUdpServer* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastOverheadEncode(struct mmRqUdpServer* p, mmUInt32_t overhead);
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastOverheadDecode(struct mmRqUdpServer* p, mmUInt32_t overhead);

// assign context handle.
MM_EXPORT_RQ void mmRqUdpServer_SetContext(struct mmRqUdpServer* p, void* u);
MM_EXPORT_RQ void mmRqUdpServer_SetRepeatedNumber(struct mmRqUdpServer* p, mmUInt32_t repeated_number);
MM_EXPORT_RQ void mmRqUdpServer_SetTimeoutMsl(struct mmRqUdpServer* p, mmMSec_t timeout_msl);

// assign mulcast mr_interface.
MM_EXPORT_RQ void mmRqUdpServer_SetMulcastMrInterface(struct mmRqUdpServer* p, const char* mr_interface);

// assign native unicast address.
MM_EXPORT_RQ void mmRqUdpServer_SetNativeUnicast(struct mmRqUdpServer* p, const char* node, mmUShort_t port);
// assign remote mulcast address.
MM_EXPORT_RQ void mmRqUdpServer_SetRemoteMulcast(struct mmRqUdpServer* p, const char* node, mmUShort_t port);

// poll thread number.
MM_EXPORT_RQ void mmRqUdpServer_SetThreadNumber(struct mmRqUdpServer* p, mmUInt32_t thread_number);

MM_EXPORT_RQ void mmRqUdpServer_SetNDefaultCallbackUnicast(struct mmRqUdpServer* p, struct mmRqUdpServerCallback* callback);
MM_EXPORT_RQ void mmRqUdpServer_SetNDefaultCallbackMulcast(struct mmRqUdpServer* p, struct mmRqUdpServerCallback* callback);

MM_EXPORT_RQ void mmRqUdpServer_SetHandleUnicast(struct mmRqUdpServer* p, mmUInt32_t id, mmRqUdpServerHandleFunc callback);
MM_EXPORT_RQ void mmRqUdpServer_SetHandleMulcast(struct mmRqUdpServer* p, mmUInt32_t id, mmRqUdpServerHandleFunc callback);

MM_EXPORT_RQ void mmRqUdpServer_ClearHandleRbtree(struct mmRqUdpServer* p);

// fopen.
MM_EXPORT_RQ void mmRqUdpServer_FopenSocket(struct mmRqUdpServer* p);
// bind.
MM_EXPORT_RQ void mmRqUdpServer_Bind(struct mmRqUdpServer* p);
// close socket.
MM_EXPORT_RQ void mmRqUdpServer_CloseSocket(struct mmRqUdpServer* p);
// shutdown socket.
MM_EXPORT_RQ void mmRqUdpServer_ShutdownSocket(struct mmRqUdpServer* p);

MM_EXPORT_RQ void mmRqUdpServer_MulcastShutdown(struct mmRqUdpServer* p);

MM_EXPORT_RQ void mmRqUdpServer_MulcastOPersistence(struct mmRqUdpServer* p);

MM_EXPORT_RQ int mmRqUdpServer_MulcastJoin(struct mmRqUdpServer* p, const char* mr_multiaddr, const char* mr_interface);
MM_EXPORT_RQ int mmRqUdpServer_MulcastDrop(struct mmRqUdpServer* p, const char* mr_multiaddr, const char* mr_interface);

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_RQ int
mmRqUdpServer_SendBufferUnicast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    mmUInt32_t identity,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet);

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_RQ int
mmRqUdpServer_SendBufferMulcast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    struct mmPacketHead* packet_head,
    struct mmByteBuffer* buffer,
    size_t hlength,
    struct mmPacket* packet);

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_RQ int
mmRqUdpServer_SendMessageUnicast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    mmUInt32_t identity,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

// this api will lock udp o locker, do not lock outside.
MM_EXPORT_RQ int
mmRqUdpServer_SendMessageMulcast(
    struct mmRqUdpServer* p,
    struct mmUdp* udp,
    struct mmMessageCoder* coder,
    struct mmPacketHead* packet_head,
    void* rq_msg,
    size_t hlength,
    struct mmPacket* rq_pack);

// poll size.
MM_EXPORT_RQ size_t mmRqUdpServer_PollSize(struct mmRqUdpServer* p);

MM_EXPORT_RQ struct mmRqUdp* mmRqUdpServer_Add(struct mmRqUdpServer* p, mmUInt32_t id);
MM_EXPORT_RQ struct mmRqUdp* mmRqUdpServer_Get(struct mmRqUdpServer* p, mmUInt32_t id);
MM_EXPORT_RQ struct mmRqUdp* mmRqUdpServer_GetInstance(struct mmRqUdpServer* p, mmUInt32_t id);
MM_EXPORT_RQ void mmRqUdpServer_Rmv(struct mmRqUdpServer* p, mmUInt32_t id);
MM_EXPORT_RQ void mmRqUdpServer_Clear(struct mmRqUdpServer* p);

MM_EXPORT_RQ void mmRqUdpServer_Start(struct mmRqUdpServer* p);
MM_EXPORT_RQ void mmRqUdpServer_Interrupt(struct mmRqUdpServer* p);
MM_EXPORT_RQ void mmRqUdpServer_Shutdown(struct mmRqUdpServer* p);
MM_EXPORT_RQ void mmRqUdpServer_Join(struct mmRqUdpServer* p);

#include "core/mmSuffix.h"

#endif//__mmRqUdpServer_h__
