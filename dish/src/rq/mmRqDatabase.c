/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqDatabase.h"

#include "core/mmLogger.h"

struct __mmRqDatabaseChunkSelectArgument
{
    struct mmRqDatabase* db;
    mmRqDatabaseSelectFunc handle;
    void* v;
};

static void __static_mmRqDatabase_TrackChunkSelect(void* obj, mmUInt32_t i, struct mmByteBuffer* byte_buffer, void* v);

MM_EXPORT_RQ void mmRqDatabase_Init(struct mmRqDatabase* p)
{
    mmTrackAssets_Init(&p->track_assets);
    mmSpinlock_Init(&p->locker, NULL);
}
MM_EXPORT_RQ void mmRqDatabase_Destroy(struct mmRqDatabase* p)
{
    mmTrackAssets_Destroy(&p->track_assets);
    mmSpinlock_Destroy(&p->locker);
}

MM_EXPORT_RQ void mmRqDatabase_Lock(struct mmRqDatabase* p)
{
    mmSpinlock_Lock(&p->locker);
}
MM_EXPORT_RQ void mmRqDatabase_Unlock(struct mmRqDatabase* p)
{
    mmSpinlock_Unlock(&p->locker);
}

MM_EXPORT_RQ void mmRqDatabase_SetFilepath(struct mmRqDatabase* p, const char* filepath)
{
    mmTrackAssets_SetFilename(&p->track_assets, filepath);
}

MM_EXPORT_RQ void mmRqDatabase_Fopen(struct mmRqDatabase* p)
{
    mmTrackAssets_Fopen(&p->track_assets);
}
MM_EXPORT_RQ void mmRqDatabase_Fflush(struct mmRqDatabase* p)
{
    mmTrackAssets_Fflush(&p->track_assets);
}
MM_EXPORT_RQ void mmRqDatabase_Fclose(struct mmRqDatabase* p)
{
    mmTrackAssets_Fclose(&p->track_assets);
}

MM_EXPORT_RQ void mmRqDatabase_TransactionB(struct mmRqDatabase* p)
{
    // need do nothing.
}
MM_EXPORT_RQ void mmRqDatabase_TransactionE(struct mmRqDatabase* p)
{
    // need do nothing.
}

MM_EXPORT_RQ void
mmRqDatabase_Select(
    struct mmRqDatabase* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmRqDatabaseSelectFunc handle,
    void* v)
{
    struct __mmRqDatabaseChunkSelectArgument _argument;
    _argument.db = p;
    _argument.handle = handle;
    _argument.v = v;
    mmTrackAssets_Select(&p->track_assets, b, e, &__static_mmRqDatabase_TrackChunkSelect, &_argument);
}

MM_EXPORT_RQ void
mmRqDatabase_Insert(
    struct mmRqDatabase* p,
    mmUInt32_t i,
    struct mmByteBuffer* byte_buffer)
{
    mmUInt8_t* buffer = (mmUInt8_t*)(byte_buffer->buffer + byte_buffer->offset);
    mmUInt32_t length = (mmUInt32_t)(byte_buffer->length);
    
    mmTrackAssets_Insert(&p->track_assets, i, buffer, length);
}

static void __static_mmRqDatabase_TrackChunkSelect(void* obj, mmUInt32_t i, struct mmByteBuffer* byte_buffer, void* v)
{
    struct __mmRqDatabaseChunkSelectArgument* _argument = (struct __mmRqDatabaseChunkSelectArgument*)(v);

    (*(_argument->handle))(_argument->db, i, byte_buffer, _argument->v);
}
