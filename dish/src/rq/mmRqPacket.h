/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRqPacket_h__
#define __mmRqPacket_h__

#include "core/mmCore.h"

#include "core/mmByte.h"
#include "core/mmStreambuf.h"

#include "net/mmSockaddr.h"

#include "mmRqExport.h"

#include "core/mmPrefix.h"

struct mmRqPacketHead
{
    // index time
    mmUInt32_t index_time;
    // rq oti common part
    mmUInt64_t oti_common;
    // rq oti common part
    mmUInt32_t oti_scheme;
    // SBN, Encoding Symbol ID
    mmUInt32_t payload_id;
};

MM_EXPORT_RQ void mmRqPacketHead_Init(struct mmRqPacketHead* p);
MM_EXPORT_RQ void mmRqPacketHead_Destroy(struct mmRqPacketHead* p);

MM_EXPORT_RQ void mmRqPacketHead_Reset(struct mmRqPacketHead* p);

MM_EXPORT_RQ size_t mmRqPacketHead_BufferSize(struct mmRqPacketHead* p);

MM_EXPORT_RQ void mmRqPacketHead_Encode(struct mmRqPacketHead* p, struct mmByteBuffer* byte_buffer, size_t* offset);
MM_EXPORT_RQ void mmRqPacketHead_Decode(struct mmRqPacketHead* p, struct mmByteBuffer* byte_buffer, size_t* offset);

MM_EXPORT_RQ void mmRqPacketHead_SetIndexTime(struct mmRqPacketHead* p, mmUInt32_t index_time);
MM_EXPORT_RQ void mmRqPacketHead_SetPayloadId(struct mmRqPacketHead* p, mmUInt32_t payload_id);
MM_EXPORT_RQ void mmRqPacketHead_SetOtiCommon(struct mmRqPacketHead* p, mmUInt64_t oti_common);
MM_EXPORT_RQ void mmRqPacketHead_SetOtiScheme(struct mmRqPacketHead* p, mmUInt32_t oti_scheme);

// MM_SUCCESS MM_FAILURE
MM_EXPORT_RQ int mmRqPacketHead_Validity(struct mmRqPacketHead* p);

struct mmRqPacket
{
    // packet head.
    struct mmRqPacketHead phead;
    // buffer for head.
    struct mmByteBuffer   hbuff;
    // buffer for body.
    struct mmByteBuffer   bbuff;
};

MM_EXPORT_RQ void mmRqPacket_Init(struct mmRqPacket* p);
MM_EXPORT_RQ void mmRqPacket_Destroy(struct mmRqPacket* p);

MM_EXPORT_RQ void mmRqPacket_Reset(struct mmRqPacket* p);

MM_EXPORT_RQ size_t mmRqPacket_BufferSize(struct mmRqPacket* p);

MM_EXPORT_RQ void mmRqPacket_Encode(struct mmRqPacket* p, struct mmByteBuffer* hbuff, size_t* offset);
MM_EXPORT_RQ void mmRqPacket_Decode(struct mmRqPacket* p, struct mmByteBuffer* hbuff, size_t* offset);

MM_EXPORT_RQ void mmRqPacket_CopyWeak(struct mmRqPacket* p, struct mmRqPacket* t);

// MM_SUCCESS MM_FAILURE
MM_EXPORT_RQ int mmRqPacket_Validity(struct mmRqPacket* p);

MM_EXPORT_RQ size_t mmRqPacket_StreambufOverdraft(struct mmStreambuf* p, struct mmRqPacket* pack);
MM_EXPORT_RQ size_t mmRqPacket_StreambufRepayment(struct mmStreambuf* p, struct mmRqPacket* pack);

// packet handle function type.
typedef void(*mmRqPacketHandleTcpFunc)(void* obj, struct mmRqPacket* pack);
typedef void(*mmRqPacketSignalTcpFunc)(void* obj);
// packet handle function type.
typedef void(*mmRqPacketHandleUdpFunc)(void* obj, struct mmRqPacket* pack, struct mmSockaddr* remote);
typedef void(*mmRqPacketSignalUdpFunc)(void* obj);

// empty payload message bps.
//
// udp net head size     = 4
// rq protocol head size = 8
// rq packet size        = 11 [3, 23]
// symbols size          = 32
// net sampling rate     = 30
//
// 4 + 8 + 11 + 32 = 55
// 55 packet size 32 payload size. 2 * 55 = 110
// 110 * 30 = 3300 ~= 3.2226  kb/s
// 32 / 110 ~= 29.09%

#define MM_RQ_PACKET_SYMBOLS_SIZE_MIN 32
#define MM_RQ_PACKET_SYMBOLS_SIZE_MAX 256

MM_EXPORT_RQ mmUInt16_t mmRqPacket_SymbolsSize(size_t transfer_length);

#include "core/mmSuffix.h"

#endif//__mmRqPacket_h__
