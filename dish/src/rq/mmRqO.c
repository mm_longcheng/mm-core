/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRqO.h"
#include "core/mmAlloc.h"
#include "core/mmLogger.h"

struct __mmRqOIdxArgument
{
    void* idx;
    mmUInt32_t id;
    struct mmByteBuffer* byte_buffer;
    struct mmRqElem* e;
    struct mmRqPacket* packet;
};

typedef void(*__static_mmRqO_GenerateHandle)(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn);

static void
__static_mmRqO_GenerateIdx(
    struct mmRqO* p,
    void* idx,
    mmUInt32_t id,
    struct mmByteBuffer* byte_buffer,
    struct mmRqElem* e,
    __static_mmRqO_GenerateHandle handle);

static void __static_mmRqO_GenerateIdxSbnRepaird(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn);
static void __static_mmRqO_GenerateIdxSbnSourced(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn);
static void __static_mmRqO_GenerateIdxEsi(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn, mmUInt32_t esi);

MM_EXPORT_RQ void mmRqO_Init(struct mmRqO* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmRqCallback_Init(&p->callback);

    mmPoolElement_Init(&p->pool_element);
    mmVectorVpt_Init(&p->vector_vpt);
    p->length = 0;

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->sampling_rate = 1;
    p->frame_rate = 1;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->overhead_encode = MM_RQ_O_OVERHEAD_ENCODE_DEFAULT;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;

    p->packet_align = MM_RQ_O_PACKET_ALIGN_DEFAULT;

    p->expected_loss = MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT;

    p->u = NULL;

    hAllocator.Produce = &mmRqElem_Init;
    hAllocator.Recycle = &mmRqElem_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmRqElem));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);

    mmRqO_SetChunkSize(p, MM_RQ_O_CHUNK_SIZE_DEFAULT);
    mmRqO_SetLength(p, MM_RQ_O_LENGTH_DEFAULT);
}
MM_EXPORT_RQ void mmRqO_Destroy(struct mmRqO* p)
{
    mmRqO_Clear(p);
    //
    mmRqCallback_Destroy(&p->callback);

    mmPoolElement_Destroy(&p->pool_element);
    mmVectorVpt_Destroy(&p->vector_vpt);
    p->length = 0;

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->sampling_rate = 1;
    p->frame_rate = 1;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->overhead_encode = MM_RQ_O_OVERHEAD_ENCODE_DEFAULT;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;

    p->packet_align = MM_RQ_O_PACKET_ALIGN_DEFAULT;

    p->expected_loss = MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT;

    p->u = NULL;
}
MM_EXPORT_RQ void mmRqO_Reset(struct mmRqO* p)
{
    // reset not need clear.
    //
    mmRqCallback_Reset(&p->callback);

    // reset not need handle pool_element.
    // reset not need handle vector_vpt.

    p->length = 0;

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->sampling_rate = 1;
    p->frame_rate = 1;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->overhead_encode = MM_RQ_O_OVERHEAD_ENCODE_DEFAULT;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;

    p->packet_align = MM_RQ_O_PACKET_ALIGN_DEFAULT;

    p->expected_loss = MM_RQ_O_PACKET_EXPECTED_LOSS_DEFAULT;

    p->u = NULL;
}
MM_EXPORT_RQ void mmRqO_ResetContext(struct mmRqO* p)
{
    // reset context not need reset p->length.

    p->duplicate = 0;
    p->discard = 0;
    p->packet_number = 0;

    p->timestamp_interval = 1;
    p->timestamp_initial = 0;

    p->finish_index = 0;
    p->scroll_index = 0;

    p->max_index = 0;
}
MM_EXPORT_RQ void mmRqO_SetRate(struct mmRqO* p, mmUInt32_t sampling_rate, mmUInt32_t frame_rate)
{
    p->sampling_rate = sampling_rate;
    p->frame_rate = frame_rate;
    p->timestamp_interval = p->sampling_rate / p->frame_rate;
}
MM_EXPORT_RQ void mmRqO_SetTimestampInitial(struct mmRqO* p, mmUInt32_t timestamp_initial)
{
    p->timestamp_initial = timestamp_initial;
}
MM_EXPORT_RQ void mmRqO_SetLength(struct mmRqO* p, mmUInt32_t length)
{
    if (length != p->length)
    {
        p->length = length;
        mmVectorVpt_Resize(&p->vector_vpt, p->length);
    }
}
MM_EXPORT_RQ void mmRqO_SetChunkSize(struct mmRqO* p, mmUInt32_t chunk_size)
{
    mmPoolElement_SetChunkSize(&p->pool_element, chunk_size);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_RQ void mmRqO_SetOverhead(struct mmRqO* p, mmUInt32_t overhead)
{
    p->overhead_encode = overhead;
}
MM_EXPORT_RQ void mmRqO_SetCallback(struct mmRqO* p, struct mmRqCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = *callback;
}
MM_EXPORT_RQ void mmRqO_SetPacketAlign(struct mmRqO* p, mmUInt8_t packet_align)
{
    p->packet_align = packet_align;
}
MM_EXPORT_RQ void mmRqO_SetExpectedLoss(struct mmRqO* p, float expected_loss)
{
    assert(0 <= expected_loss && expected_loss < 1.0f && "expected_loss value is invalid.");
    p->expected_loss = expected_loss;
}
MM_EXPORT_RQ void mmRqO_SetContext(struct mmRqO* p, void* u)
{
    p->u = u;
}

MM_EXPORT_RQ void mmRqO_Clear(struct mmRqO* p)
{
    mmPoolElement_ClearChunk(&p->pool_element);
    mmVectorVpt_Clear(&p->vector_vpt);
}

MM_EXPORT_RQ void mmRqO_GenerateSourced(struct mmRqO* p, void* idx, mmUInt32_t id, struct mmByteBuffer* byte_buffer)
{
    __static_mmRqO_GenerateHandle handle = &__static_mmRqO_GenerateIdxSbnSourced;

    struct mmRqElem* nn = NULL;

    mmUInt32_t n = id - p->scroll_index;
    if (n >= p->length)
    {
        mmUInt32_t length = 0 == p->length ? MM_RQ_O_LENGTH_DEFAULT : 2 * p->length;
        mmRqO_SetLength(p, length);
    }

    // we need check n.
    nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, n);
    if (NULL == nn)
    {
        mmUInt16_t packet_size = mmRqPacket_SymbolsSize(byte_buffer->length);

        mmUInt32_t index = n + 1;
        //
        mmUInt64_t transfer_length = byte_buffer->length;

        // determine chunks, symbol size, memory usage from size
        mmUInt16_t ss = packet_size / 2;
        mmUInt32_t ws = packet_size * 100;

        // pool_element produce.
        mmPoolElement_Lock(&p->pool_element);
        nn = (struct mmRqElem*)mmPoolElement_Produce(&p->pool_element);
        mmPoolElement_Unlock(&p->pool_element);
        // note: 
        // pool_element manager init and destroy.
        // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
        mmRqElem_Reset(nn);
        //
        mmVectorVpt_SetIndex(&p->vector_vpt, n, nn);
        p->max_index = p->max_index > index ? p->max_index : index;

        mmRqElem_SetId(nn, id);
        mmRqElem_SetOverhead(nn, p->overhead_encode);
        mmRqElem_SetEncoder(nn, transfer_length, packet_size, ss, p->packet_align, ws);

        mmRqElem_ContextEncode(nn, byte_buffer);
    }
    {
        struct mmStreambuf* streambuf = NULL;

        streambuf = &nn->streambuf_buffer;

        mmStreambuf_Reset(streambuf);
        mmStreambuf_AlignedMemory(streambuf, byte_buffer->length);
        mmStreambuf_Sputn(streambuf, byte_buffer->buffer, byte_buffer->offset, byte_buffer->length);

        __static_mmRqO_GenerateIdx(p, idx, id, byte_buffer, nn, handle);
    }
}
MM_EXPORT_RQ void mmRqO_GenerateRepaird(struct mmRqO* p, void* idx, mmUInt32_t id)
{
    do
    {
        __static_mmRqO_GenerateHandle handle = &__static_mmRqO_GenerateIdxSbnRepaird;

        struct mmRqElem* nn = NULL;

        mmUInt32_t n = id - p->scroll_index;
        if (n >= p->length)
        {
            // can not repaird out of range sourced packet.
            break;
        }
        // we not need check n.
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, n);
        if (NULL == nn)
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogW(gLogger, "%s %d can not repaird no sourced packet idx: %p id: %d.",
                __FUNCTION__,
                __LINE__,
                idx,
                id);
            break;
        }
        {
            struct mmStreambuf* streambuf = &nn->streambuf_buffer;

            struct mmByteBuffer byte_buffer_impl;

            struct mmByteBuffer* byte_buffer = &byte_buffer_impl;

            byte_buffer->buffer = (mmUInt8_t*)streambuf->buff;
            byte_buffer->offset = (size_t)streambuf->gptr;
            byte_buffer->length = (size_t)mmStreambuf_Size(streambuf);

            __static_mmRqO_GenerateIdx(p, idx, id, byte_buffer, nn, handle);
        }
    } while (0);
}
MM_EXPORT_RQ void mmRqO_FeedbackSourced(struct mmRqO* p, void* idx)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t i = 0;
    mmUInt32_t d = p->finish_index - p->scroll_index;
    for (i = d; i < p->length; i++)
    {
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, i);
        if (NULL != nn && RQ_ELEM_STATE_FINISH == nn->state)
        {
            // fire event.
            (*(p->callback.Finish))(p, p->u, idx, nn);
        }
        else
        {
            break;
        }
    }
    if (0 != i)
    {
        // move
        p->finish_index += (i - d);
    }
}
MM_EXPORT_RQ void mmRqO_AssemblyPerfect(struct mmRqO* p, void* idx, mmUInt32_t id)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t n = id - p->scroll_index;

    do
    {
        if (n >= p->length)
        {
            // the package is out of range.
            break;
        }
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, n);
        if (NULL == nn)
        {
            // the package is invalid.
            break;
        }
        mmRqElem_SetState(nn, RQ_ELEM_STATE_SCROLL);
    } while (0);
}
MM_EXPORT_RQ void mmRqO_FeedbackPerfect(struct mmRqO* p, void* idx)
{
    struct mmRqElem* nn = NULL;

    mmUInt32_t i = 0;
    for (i = 0; i < p->length; i++)
    {
        nn = (struct mmRqElem*)mmVectorVpt_GetIndex(&p->vector_vpt, i);
        if (NULL != nn && RQ_ELEM_STATE_SCROLL == nn->state)
        {
            // fire event.
            (*(p->callback.Scroll))(p, p->u, idx, nn);
            // NULL for i.
            mmVectorVpt_SetIndex(&p->vector_vpt, i, NULL);
            // note: 
            // pool_element manager init and destroy.
            // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
            mmRqElem_Reset(nn);
            // pool_element recycle.
            mmPoolElement_Lock(&p->pool_element);
            mmPoolElement_Recycle(&p->pool_element, nn);
            mmPoolElement_Unlock(&p->pool_element);
        }
        else
        {
            break;
        }
    }
    if (0 != i)
    {
        // move
        p->max_index -= i;
        mmMemcpy(p->vector_vpt.arrays, p->vector_vpt.arrays + i, sizeof(struct mm_rq_o_elem*) * (p->max_index));
        mmMemset(p->vector_vpt.arrays + p->max_index, 0, sizeof(struct mm_rq_o_elem*) * (i));
        p->scroll_index += i;
    }
}

MM_EXPORT_RQ void mmRqO_GenerateSymbols(struct mmRqO* p, void* idx, mmUInt32_t id, struct mmByteBuffer* byte_buffer)
{
    __static_mmRqO_GenerateHandle handle = &__static_mmRqO_GenerateIdxSbnSourced;
    //
    struct mmRqElem* nn = NULL;

    mmUInt16_t packet_size = mmRqPacket_SymbolsSize(byte_buffer->length);

    mmUInt64_t transfer_length = byte_buffer->length;

    // determine chunks, symbol size, memory usage from size
    mmUInt16_t ss = packet_size / 2;
    mmUInt32_t ws = packet_size * 100;

    // pool_element produce.
    mmPoolElement_Lock(&p->pool_element);
    nn = (struct mmRqElem*)mmPoolElement_Produce(&p->pool_element);
    mmPoolElement_Unlock(&p->pool_element);
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmRqElem_Reset(nn);

    mmRqElem_SetId(nn, id);
    mmRqElem_SetOverhead(nn, p->overhead_encode);
    mmRqElem_SetEncoder(nn, transfer_length, packet_size, ss, p->packet_align, ws);

    mmRqElem_ContextEncode(nn, byte_buffer);

    __static_mmRqO_GenerateIdx(p, idx, id, byte_buffer, nn, handle);

    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmRqElem_Reset(nn);
    // pool_element recycle.
    mmPoolElement_Lock(&p->pool_element);
    mmPoolElement_Recycle(&p->pool_element, nn);
    mmPoolElement_Unlock(&p->pool_element);
}

MM_EXPORT_RQ float mmRqO_ExpectedLoss(mmUInt32_t i_packet_number, mmUInt32_t o_packet_number)
{
    // [0, MM_RQ_O_PACKET_EXPECTED_LOSS_MAX)

    float expected_loss = 0;

    if (0 == o_packet_number)
    {
        expected_loss = 0;
    }
    else
    {
        // some time o_packet_number < i_packet_number.
        i_packet_number = i_packet_number < o_packet_number ? i_packet_number : o_packet_number;
        expected_loss = 1 - ((float)i_packet_number / (float)o_packet_number);
    }
    // packet expected loss max is limit.
    expected_loss =
        expected_loss < MM_RQ_O_PACKET_EXPECTED_LOSS_MAX ?
        expected_loss : MM_RQ_O_PACKET_EXPECTED_LOSS_MAX;

    return expected_loss;
}

static void
__static_mmRqO_GenerateIdx(
    struct mmRqO* p,
    void* idx,
    mmUInt32_t id,
    struct mmByteBuffer* byte_buffer,
    struct mmRqElem* e,
    __static_mmRqO_GenerateHandle handle)
{
    mmUInt8_t num_sbn = mmRaptorqContext_Blocks(&e->rq);
    mmUInt8_t sbn = 0;

    struct mmRqPacket packet;
    struct __mmRqOIdxArgument argument;

    mmRqPacket_Init(&packet);

    packet.phead.oti_common = mmRaptorqContext_OtiCommon(&e->rq);
    packet.phead.oti_scheme = mmRaptorqContext_OtiSchemeSpecific(&e->rq);

    packet.phead.index_time = p->timestamp_initial + id * p->timestamp_interval;

    argument.idx = idx;
    argument.id = id;
    argument.byte_buffer = byte_buffer;
    argument.e = e;
    argument.packet = &packet;

    for (sbn = 0; sbn < num_sbn; sbn++)
    {
        (*(handle))(p, &argument, sbn);
    }
    //
    mmRqElem_SetState(e, RQ_ELEM_STATE_FINISH);
    mmRqO_FeedbackSourced(p, idx);
    //
    mmRqPacket_Destroy(&packet);
}

static void __static_mmRqO_GenerateIdxSbnRepaird(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn)
{
    mmUInt32_t i = 0;
    mmUInt32_t esi = 0;
    mmUInt32_t num_esi = 0;
    mmUInt32_t packet = 0;
    mmUInt32_t real_packet = 0;
    mmUInt32_t esi_number = 0;

    num_esi = mmRaptorqContext_BlockSymbols(&argument->e->rq, sbn);

    packet = num_esi + argument->e->overhead;

    // we need at last one repaird packet.
    packet = (mmUInt32_t)ceil((packet) * (p->expected_loss));
    packet = packet == 0 ? 1 : packet;

    real_packet = (mmUInt32_t)ceil((packet) / (1 - p->expected_loss));

    esi_number = mmRqElem_GetEsiNumber(argument->e, sbn);

    for (i = 0; i < real_packet; i++)
    {
        esi = i + esi_number;
        __static_mmRqO_GenerateIdxEsi(p, argument, sbn, esi);
    }
    //
    mmRqElem_SetEsiNumber(argument->e, sbn, real_packet + esi_number);
}
static void __static_mmRqO_GenerateIdxSbnSourced(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn)
{
    mmUInt32_t esi = 0;
    mmUInt32_t num_esi = 0;
    mmUInt32_t packet = 0;
    mmUInt32_t real_packet = 0;

    num_esi = mmRaptorqContext_BlockSymbols(&argument->e->rq, sbn);

    packet = num_esi + argument->e->overhead;

    real_packet = (mmUInt32_t)ceil((packet) / (1 - p->expected_loss));

    for (esi = 0; esi < real_packet; esi++)
    {
        __static_mmRqO_GenerateIdxEsi(p, argument, sbn, esi);
    }
    //
    mmRqElem_SetEsiNumber(argument->e, sbn, real_packet);
}
static void __static_mmRqO_GenerateIdxEsi(struct mmRqO* p, struct __mmRqOIdxArgument* argument, mmUInt8_t sbn, mmUInt32_t esi)
{
    int code = MM_FALSE;
    mmUInt32_t payload_id = 0;
    mmUInt16_t symbol_size = 0;
    mmUInt8_t* data = NULL;
    size_t offset = 0;
    struct mmStreambuf* streambuf = &argument->e->streambuf_packet;

    mmUInt16_t packet_size = mmRaptorqContext_SymbolSize(&argument->e->rq);
    mmRaptorqContext_GeneratePayloadId(sbn, esi, &payload_id);

    argument->packet->phead.payload_id = payload_id;

    argument->packet->hbuff.length = (mmUInt32_t)mmRqPacketHead_BufferSize(&argument->packet->phead);
    argument->packet->bbuff.length = packet_size;

    mmRqPacket_StreambufOverdraft(streambuf, argument->packet);

    data = (mmUInt8_t*)(argument->packet->bbuff.buffer + argument->packet->bbuff.offset);

    code = mmRaptorqContext_EncoderGenerateSymbolsBuffer(&argument->e->rq, (mmUInt8_t*)data, esi, sbn, argument->byte_buffer, &symbol_size);
    if (MM_TRUE != code)
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogE(gLogger, "%s %d failed to encode packet data for sbn %d esi %d.",
            __FUNCTION__,
            __LINE__,
            (int)sbn,
            (int)esi);
    }
    else
    {
        mmRqPacket_Encode(argument->packet, &argument->packet->hbuff, &offset);

        // fire event.
        (*(p->callback.Handle))(p, p->u, argument->idx, argument->e, argument->packet);

        p->packet_number++;
    }

    mmRqPacket_StreambufRepayment(streambuf, argument->packet);
}

