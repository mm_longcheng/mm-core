/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqRepair_h__
#define __mmRaptorqRepair_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "mmRaptorqOctetMatrix.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

struct mmRaptorqRepairSym
{
    struct mmOctetMatrix row;
    mmUInt32_t esi;
};
MM_EXPORT_RAPTORQ void mmRaptorqRepairSym_Init(struct mmRaptorqRepairSym* p);
MM_EXPORT_RAPTORQ void mmRaptorqRepairSym_Destroy(struct mmRaptorqRepairSym* p);

struct mmRaptorqRepairVector
{
    struct mmVectorValue v;
};
MM_EXPORT_RAPTORQ void mmRaptorqRepairVector_Init(struct mmRaptorqRepairVector* p);
MM_EXPORT_RAPTORQ void mmRaptorqRepairVector_Destroy(struct mmRaptorqRepairVector* p);
MM_EXPORT_RAPTORQ void mmRaptorqRepairVector_Reset(struct mmRaptorqRepairVector* p);

MM_EXPORT_RAPTORQ void
mmRaptorqRepairVector_PushRepairSym(
    struct mmRaptorqRepairVector* p,
    mmUInt32_t esi,
    mmUInt16_t cols,
    mmUInt8_t* data);

MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqRepairVector_Esi(struct mmRaptorqRepairVector* p, size_t index);
MM_EXPORT_RAPTORQ size_t mmRaptorqRepairVector_Size(struct mmRaptorqRepairVector* p);

#include "core/mmSuffix.h"

#endif//__mmRaptorqRepair_h__
