/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqContext_h__
#define __mmRaptorqContext_h__

#include "core/mmCore.h"
#include "core/mmByte.h"

#include "container/mmBitset.h"
#include "container/mmRbtreeU32.h"

#include "mmRaptorqMath.h"
#include "mmRaptorqDegree.h"
#include "mmRaptorqPrecode.h"
#include "mmRaptorqRepair.h"
#include "mmRaptorqSystematic.h"
#include "mmRaptorqIO.h"
#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

/*
* rfc6330 pg 5
*  o Transfer Length (F): 40-bit unsigned integer. A non-negative
*    integer that is at most 946270874880. This is the transfer length
*    of the object in units of octets.
*/
static const mmUInt64_t MM_RAPTORQ_MAX_TRANSFER = 946270874880ULL; // ~881 GB

/*
* 3.3.1. Mandatory
*  The value of the FEC Encoding ID MUST be 6, as assigned by IANA (see
*  Section 7).
*/
static const mmUInt32_t MM_RAPTORQ_FEC_ENCODING_ID = 6;

/*
* rfc6330 pg 8
*
* 4.3. Example Parameter Derivation Algorithm
*  This section provides recommendations for the derivation of the three
*  transport parameters, T, Z, and N. This recommendation is based on
*  the following input parameters:
*
*  o F: the transfer length of the object, in octets
*  o WS: the maximum size block that is decodable in working memory, in
*    octets
*  o P��: the maximum payload size in octets, which is assumed to be a
*    multiple of Al
*  o Al: the symbol alignment parameter, in octets
*  o SS: a parameter where the desired lower bound on the sub-symbol
*    size is SS*Al
*  o K��_max: the maximum number of source symbols per source block.
*      Note: Section 5.1.2 defines K��_max to be 56403.
*
*  Based on the above inputs, the transport parameters T, Z, and N are
*  calculated as follows:
*
*  Let
*  o T = P��
*  o Kt = ceil(F/T)
*  o N_max = floor(T/(SS*Al))
*  o for all n=1, ..., N_max
*    * KL(n) is the maximum K�� value in Table 2 in Section 5.6 such
*      that
*           K�� <= WS/(Al*(ceil(T/(Al*n))))
*  o Z = ceil(Kt/KL(N_max))
*  o N is the minimum n=1, ..., N_max such that ceil(Kt/Z) <= KL(n)
*
*  It is RECOMMENDED that each packet contains exactly one symbol.
*  However, receivers SHALL support the reception of packets that
*  contain multiple symbols.
*  The value Kt is the total number of symbols required to represent the
*  source data of the object.
*
*  The value Kt is the total number of symbols required to represent the
*  source data of the object.
*
*  The algorithm above and that defined in Section 4.4.1.2 ensure that
*  the sub-symbol sizes are a multiple of the symbol alignment
*  parameter, Al. This is useful because the sum operations used for
*  encoding and decoding are generally performed several octets at a
*  time, for example, at least 4 octets at a time on a 32-bit processor.
*  Thus, the encoding and decoding can be performed faster if the sub-
*  symbol sizes are a multiple of this number of octets.
*
*  The recommended setting for the input parameter Al is 4.
*
*  The parameter WS can be used to generate encoded data that can be
*  decoded efficiently with limited working memory at the decoder. Note
*  that the actual maximum decoder memory requirement for a given value
*  of WS depends on the implementation, but it is possible to implement
*  decoding using working memory only slightly larger than WS.
*/

/*
* 3.3.2. Common
*  The Common FEC Object Transmission Information elements used by this
*  FEC scheme are:
*
*  o Transfer Length (F): 40-bit unsigned integer. A non-negative
*    integer that is at most 946270874880. This is the transfer length
*    of the object in units of octets.
*  o Symbol Size (T): 16-bit unsigned integer. A positive integer that
*    is less than 2^^16. This is the size of a symbol in units of
*    octets.
*/
struct mmRaptorqOtiCommon
{
    mmUInt64_t F;  /* input size in bytes */
    size_t WS;      /* max sub block size decode */
    mmUInt16_t T;  /* the symbol size in octets, which MUST be a multiple of Al */
    mmUInt16_t SS; /* sub symbol size (multiple of alignment) */
};
MM_EXPORT_RAPTORQ void mmRaptorqOtiCommon_Init(struct mmRaptorqOtiCommon* p);
MM_EXPORT_RAPTORQ void mmRaptorqOtiCommon_Destroy(struct mmRaptorqOtiCommon* p);
MM_EXPORT_RAPTORQ void mmRaptorqOtiCommon_Reset(struct mmRaptorqOtiCommon* p);

/*
* 3.3.3. Scheme-Specific
*  The following parameters are carried in the Scheme-Specific FEC
*  Object Transmission Information element for this FEC scheme:
*
*  o The number of source blocks (Z): 8-bit unsigned integer.
*  o The number of sub-blocks (N): 16-bit unsigned integer.
*  o A symbol alignment parameter (Al): 8-bit unsigned integer.
*/
struct mmRaptorqOtiScheme
{
    mmUInt64_t Kt; /* the total number of symbols required to represent input */
    mmUInt16_t Z;  /* number of source blocks */
    mmUInt32_t N;  /* number of sub-blocks in each source block */
    mmUInt16_t Al; /* byte alignment, 0 < Al <= 8, 4 is recommended */
};
MM_EXPORT_RAPTORQ void mmRaptorqOtiScheme_Init(struct mmRaptorqOtiScheme* p);
MM_EXPORT_RAPTORQ void mmRaptorqOtiScheme_Destroy(struct mmRaptorqOtiScheme* p);
MM_EXPORT_RAPTORQ void mmRaptorqOtiScheme_Reset(struct mmRaptorqOtiScheme* p);

struct mmRaptorqPartition
{
    mmUInt16_t IL; /* size of long blocks */
    mmUInt16_t IS; /* size of short blocks*/
    mmUInt16_t JL; /* number of long blocks */
    mmUInt16_t JS; /* number of short blocks */
};
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Init(struct mmRaptorqPartition* p);
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Destroy(struct mmRaptorqPartition* p);
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Reset(struct mmRaptorqPartition* p);

/*
* rfc6330 pg 10
* The function Partition[I,J] derives parameters for partitioning a
* block of size I into J approximately equal-sized blocks. More
* specifically, it partitions I into JL blocks of length IL and JS
* blocks of length IS. The output of Partition[I, J] is the sequence
* (IL, IS, JL, JS), where IL = ceil(I/J), IS = floor(I/J), JL = I - IS
* * J, and JS = J - JL.
*/
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Assign(struct mmRaptorqPartition* p, mmUInt64_t I, mmUInt16_t J);

struct mmRaptorqSourceBlock
{
    struct mmRaptorqPartition part;
    size_t sbloc;
    size_t part_tot;
    mmUInt16_t al;
};

MM_EXPORT_RAPTORQ size_t
mmRaptorqSourceBlock_GetSymbolOffset(
    struct mmRaptorqSourceBlock* p,
    size_t pos,
    mmUInt16_t K,
    mmUInt32_t symbol_id);

struct mmRaptorqEncoderCore
{
    struct mmRaptorqPrecode precode;
    struct mmOctetMatrix symbolmat;
    mmUInt16_t num_symbols;
    mmUInt16_t symbol_size;
    mmUInt8_t sbn;
};
MM_EXPORT_RAPTORQ void mmRaptorqEncoderCore_Init(struct mmRaptorqEncoderCore* p);
MM_EXPORT_RAPTORQ void mmRaptorqEncoderCore_Destroy(struct mmRaptorqEncoderCore* p);
MM_EXPORT_RAPTORQ void mmRaptorqEncoderCore_Reset(struct mmRaptorqEncoderCore* p);

MM_EXPORT_RAPTORQ void
mmRaptorqEncoderCore_Assign(
    struct mmRaptorqEncoderCore* p,
    mmUInt16_t num_symbols,
    mmUInt16_t symbol_size,
    mmUInt8_t sbn);

struct mmRaptorqDecoderCore
{
    struct mmRaptorqPrecode precode;
    struct mmOctetMatrix symbolmat;
    struct mmRaptorqRepairVector repair_bin;
    struct mmBitset mask;
    mmUInt16_t num_symbols;
    mmUInt16_t symbol_size;
    mmUInt8_t sbn;
};
MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_Init(struct mmRaptorqDecoderCore* p);
MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_Destroy(struct mmRaptorqDecoderCore* p);
MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_Reset(struct mmRaptorqDecoderCore* p);

MM_EXPORT_RAPTORQ void
mmRaptorqDecoderCore_Assign(
    struct mmRaptorqDecoderCore* p,
    mmUInt16_t num_symbols,
    mmUInt16_t symbol_size,
    mmUInt8_t sbn);

MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_AssignAlign(struct mmRaptorqDecoderCore* p, mmUInt8_t Al);

struct mmRaptorqContext
{
    struct mmRaptorqOtiCommon common;
    struct mmRaptorqOtiScheme scheme;

    struct mmRaptorqPartition src_part; /* (KL, KS, ZL, ZS) = Partition[  Kt, Z] */
    struct mmRaptorqPartition sub_part; /* (TL, TS, NL, NS) = Partition[T/Al, N] */

    struct mmRbtreeU32Vpt encoders;
    struct mmRbtreeU32Vpt decoders;

    struct mmBitset mask;/* mask for sbn */
};
MM_EXPORT_RAPTORQ void mmRaptorqContext_Init(struct mmRaptorqContext* p);
MM_EXPORT_RAPTORQ void mmRaptorqContext_Destroy(struct mmRaptorqContext* p);

MM_EXPORT_RAPTORQ void mmRaptorqContext_Clear(struct mmRaptorqContext* p);
MM_EXPORT_RAPTORQ void mmRaptorqContext_Reset(struct mmRaptorqContext* p);
MM_EXPORT_RAPTORQ void mmRaptorqContext_MaskReset(struct mmRaptorqContext* p);

MM_EXPORT_RAPTORQ void
mmRaptorqContext_EncoderAssign(
    struct mmRaptorqContext* p,
    mmUInt64_t F,
    mmUInt16_t T,
    mmUInt16_t SS,
    mmUInt8_t Al,
    size_t WS);

MM_EXPORT_RAPTORQ void
mmRaptorqContext_DecoderAssign(
    struct mmRaptorqContext* p,
    mmUInt64_t common,
    mmUInt32_t scheme);

MM_EXPORT_RAPTORQ void mmRaptorqContext_GenerateSchemeSpecific(struct mmRaptorqContext* p);

MM_EXPORT_RAPTORQ void
mmRaptorqContext_SourceBlock(
    struct mmRaptorqContext* p,
    mmUInt8_t sbn,
    mmUInt16_t symbol_size,
    struct mmRaptorqSourceBlock* block);

/*
* The encoded Common FEC Object Transmission Information (OTI) format
*  is shown in Figure 2.
*  0                   1                   2                   3
*  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
*  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*  |                      Transfer Length (F)                      |
*  +               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*  |               |    Reserved   |        Symbol Size (T)        |
*  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*      Figure 2: Encoded Common FEC OTI for RaptorQ FEC Scheme
*
*  NOTE: The limit of 946270874880 on the transfer length is a
*  consequence of the limitation on the symbol size to 2^^16-1, the
*  limitation on the number of symbols in a source block to 56403,
*  and the limitation on the number of source blocks to 2^^8.
*/
MM_EXPORT_RAPTORQ mmUInt64_t mmRaptorqContext_OtiCommon(struct mmRaptorqContext* p);

/*
* These parameters are all positive integers. The encoded Scheme-
*  specific Object Transmission Information is a 4-octet field
*  consisting of the parameters Z, N, and Al as shown in Figure 3.
*  0                   1                   2                   3
*  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
*  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*  |       Z       |              N                |      Al       |
*  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*  Figure 3: Encoded Scheme-Specific FEC Object Transmission Information
*
*  The encoded FEC Object Transmission Information is a 12-octet field
*  consisting of the concatenation of the encoded Common FEC Object
*  Transmission Information and the encoded Scheme-specific FEC Object
*  Transmission Information.
*/
MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_OtiSchemeSpecific(struct mmRaptorqContext* p);

/*
* 3.2. FEC Payload IDs
*  The FEC Payload ID MUST be a 4-octet field defined as follows:
*  0                   1                   2                   3
*  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
*  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*  |      SBN      |              Encoding Symbol ID               |
*  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*                Figure 1: FEC Payload ID Format
*
*  o Source Block Number (SBN): 8-bit unsigned integer. A non-negative
*    integer identifier for the source block that the encoding symbols
*    within the packet relate to.
*  o Encoding Symbol ID (ESI): 24-bit unsigned integer. A non-negative
*    integer identifier for the encoding symbols within the packet.
*
*  The interpretation of the Source Block Number and Encoding Symbol
*  Identifier is defined in Section 4.
*/
MM_EXPORT_RAPTORQ void mmRaptorqContext_GeneratePayloadId(mmUInt8_t sbn, mmUInt32_t esi, mmUInt32_t* payload_id);
MM_EXPORT_RAPTORQ void mmRaptorqContext_AssemblyPayloadId(mmUInt32_t payload_id, mmUInt8_t* sbn, mmUInt32_t* esi);

MM_EXPORT_RAPTORQ mmUInt64_t mmRaptorqContext_TransferLength(struct mmRaptorqContext* p);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqContext_SymbolSize(struct mmRaptorqContext* p);

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqContext_BlockSymbols(struct mmRaptorqContext* p, mmUInt8_t sbn);
MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_MaxRepair(struct mmRaptorqContext* p, mmUInt8_t sbn);
MM_EXPORT_RAPTORQ mmUInt8_t mmRaptorqContext_Blocks(struct mmRaptorqContext* p);
MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_NumMissing(struct mmRaptorqContext* p, mmUInt8_t sbn);
MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_NumRepair(struct mmRaptorqContext* p, mmUInt8_t sbn);

MM_EXPORT_RAPTORQ struct mmRaptorqEncoderCore* mmRaptorqContext_BlockEncoder(struct mmRaptorqContext* p, mmUInt8_t sbn);
MM_EXPORT_RAPTORQ void mmRaptorqContext_EncoderCleanup(struct mmRaptorqContext* p, mmUInt8_t sbn);
MM_EXPORT_RAPTORQ struct mmRaptorqDecoderCore* mmRaptorqContext_BlockDecoder(struct mmRaptorqContext* p, mmUInt8_t sbn);
MM_EXPORT_RAPTORQ void mmRaptorqContext_DecoderCleanup(struct mmRaptorqContext* p, mmUInt8_t sbn);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecoderAssemblySymbols(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    mmUInt16_t* symbol_size);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncoderGenerateSymbols(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    struct mmRaptorqIO* io,
    mmUInt16_t* symbol_size);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncodeBlock(
    struct mmRaptorqContext* p,
    struct mmRaptorqIO* io,
    mmUInt8_t sbn,
    mmUInt64_t* encode_length);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecodeBlock(
    struct mmRaptorqContext* p,
    struct mmRaptorqIO* io,
    mmUInt8_t sbn,
    mmUInt64_t* decode_length);


// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecoderAssemblySymbolsBuffer(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    mmUInt16_t* symbol_size);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncoderGenerateSymbolsBuffer(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    struct mmByteBuffer* byte_buffer,
    mmUInt16_t* symbol_size);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncodeBlockBuffer(
    struct mmRaptorqContext* p,
    struct mmByteBuffer* byte_buffer,
    mmUInt8_t sbn,
    mmUInt64_t* encode_length);

// MM_TRUE is success.
MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecodeBlockBuffer(
    struct mmRaptorqContext* p,
    struct mmByteBuffer* byte_buffer,
    mmUInt8_t sbn,
    mmUInt64_t* decode_length);


// check whether symbol complete. 
// MM_TRUE is complete.
MM_EXPORT_RAPTORQ int mmRaptorqContext_AssemblySymbolComplete(struct mmRaptorqContext* p, mmUInt8_t sbn, mmUInt32_t overhead);
// check whether symbol complete. 
// MM_TRUE is complete.
MM_EXPORT_RAPTORQ int mmRaptorqContext_AssemblySourceComplete(struct mmRaptorqContext* p, mmUInt32_t overhead);
// check whether symbol valuable. 
// MM_TRUE is valuable.
MM_EXPORT_RAPTORQ int mmRaptorqContext_AssemblySymbolValuable(struct mmRaptorqContext* p, mmUInt32_t esi, mmUInt8_t sbn);
// encode byte_buffer.
// MM_TRUE is success.
MM_EXPORT_RAPTORQ int mmRaptorqContext_Encode(struct mmRaptorqContext* p, struct mmByteBuffer* byte_buffer);
// decode byte_buffer.
// MM_TRUE is success.
MM_EXPORT_RAPTORQ int mmRaptorqContext_Decode(struct mmRaptorqContext* p, struct mmByteBuffer* byte_buffer);

#include "core/mmSuffix.h"

#endif//__mmRaptorqContext_h__
