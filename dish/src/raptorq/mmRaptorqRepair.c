/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqRepair.h"
#include "mmRaptorqPair.h"

#include "core/mmAlloc.h"

MM_EXPORT_RAPTORQ void mmRaptorqRepairSym_Init(struct mmRaptorqRepairSym* p)
{
    mmOctetMatrix_Init(&p->row);
    p->esi = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqRepairSym_Destroy(struct mmRaptorqRepairSym* p)
{
    mmOctetMatrix_Destroy(&p->row);
    p->esi = 0;
}

MM_EXPORT_RAPTORQ void mmRaptorqRepairVector_Init(struct mmRaptorqRepairVector* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(&p->v);

    hValueAllocator.Produce = &mmRaptorqRepairSym_Init;
    hValueAllocator.Recycle = &mmRaptorqRepairSym_Destroy;
    mmVectorValue_SetAllocator(&p->v, &hValueAllocator);
    mmVectorValue_SetElement(&p->v, sizeof(struct mmRaptorqRepairSym));
}
MM_EXPORT_RAPTORQ void mmRaptorqRepairVector_Destroy(struct mmRaptorqRepairVector* p)
{
    mmVectorValue_Destroy(&p->v);
}
MM_EXPORT_RAPTORQ void mmRaptorqRepairVector_Reset(struct mmRaptorqRepairVector* p)
{
    mmVectorValue_Clear(&p->v);
}

MM_EXPORT_RAPTORQ void
mmRaptorqRepairVector_PushRepairSym(
    struct mmRaptorqRepairVector* p,
    mmUInt32_t esi,
    mmUInt16_t cols,
    mmUInt8_t* data)
{
    struct mmRaptorqRepairSym* e = NULL;
    size_t index = p->v.size;
    size_t sz = p->v.size + 1;
    sz = sz < p->v.capacity ? p->v.capacity : sz;
    mmVectorValue_Reserve(&p->v, sz);
    p->v.size = index + 1;
    e = (struct mmRaptorqRepairSym*)mmVectorValue_GetIndex(&p->v, index);
    e->esi = esi;
    mmOctetMatrix_Resize(&e->row, 1, cols);
    mmMemcpy(mmOctetMatrix_R(&e->row, 0), data, cols);
}

MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqRepairVector_Esi(struct mmRaptorqRepairVector* p, size_t index)
{
    struct mmRaptorqRepairSym* e = NULL;
    assert(index < p->v.size && "index < p->v.size is invalid.");
    e = (struct mmRaptorqRepairSym*)mmVectorValue_GetIndex(&p->v, index);
    return e->esi;
}
MM_EXPORT_RAPTORQ size_t mmRaptorqRepairVector_Size(struct mmRaptorqRepairVector* p)
{
    return p->v.size;
}
