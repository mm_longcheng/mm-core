/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqOctetBlas.h"
#include "mmRaptorqOctet.h"
#include "mmRaptorqOctetHilo.h"

#include "core/mmAlloc.h"
#include "core/mmInteger.h"

typedef mmUInt8_t octet;

MM_EXPORT_RAPTORQ void mmOctet_Copy(mmUInt8_t* a, mmUInt8_t* b, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k)
{
    size_t aligned_cols_k = mmOctet_AlignedCols(k);

    octet* ap = a + (i * aligned_cols_k);
    octet* bp = b + (j * aligned_cols_k);

    mmMemcpy(ap, bp, sizeof(octet) * k);
}
MM_EXPORT_RAPTORQ void mmOctet_SwapRow(mmUInt8_t* a, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k)
{
    int idx = 0;
    size_t aligned_cols_k = mmOctet_AlignedCols(k);

    octet* ap = NULL;
    octet* bp = NULL;

    mmUInt16_t v_b = 0;
    mmUInt16_t v_a = 0;

    if (i == j)
    {
        return;
    }

    ap = a + (i * aligned_cols_k);
    bp = a + (j * aligned_cols_k);

    v_b = k / 8;
    v_a = k % 8;

    for (idx = 0; idx < v_b; idx++)
    {
        mmInteger_UInt64Swap((mmUInt64_t*)&ap[8 * idx], (mmUInt64_t*)&bp[8 * idx]);
    }
    for (idx = v_b * 8; idx < v_b * 8 + v_a; idx++)
    {
        mmInteger_UInt08Swap((mmUInt8_t*)&ap[idx], (mmUInt8_t*)&bp[idx]);
    }
}
MM_EXPORT_RAPTORQ void mmOctet_SwapCol(mmUInt8_t* a, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k, mmUInt16_t l)
{
    int idx = 0;
    size_t aligned_cols_l = mmOctet_AlignedCols(l);

    octet* ap = NULL;

    if (i == j)
    {
        return;
    }

    ap = a;

    for (idx = 0; idx < k; idx++, ap += aligned_cols_l)
    {
        mmInteger_UInt08Swap(&ap[i], &ap[j]);
    }
}
MM_EXPORT_RAPTORQ void mmOctet_Axpy(mmUInt8_t* a, mmUInt8_t* b, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k, mmUInt8_t u)
{
    int idx = 0;
    size_t aligned_cols_k = mmOctet_AlignedCols(k);

    octet* ap = a + (i * aligned_cols_k);
    octet* bp = b + (j * aligned_cols_k);

    const octet* urow_hi = OCT_MUL_HI[u];
    const octet* urow_lo = OCT_MUL_LO[u];

    if (u == 0)
    {
        return;
    }

    if (u == 1)
    {
        mmOctet_AddRow(a, b, i, j, k);
        return;
    }

    for (idx = 0; idx < k; idx++)
    {
        octet b_lo = bp[idx] & 0x0f;
        octet b_hi = (bp[idx] & 0xf0) >> 4;
        ap[idx] ^= urow_hi[b_hi] ^ urow_lo[b_lo];
    }
}
MM_EXPORT_RAPTORQ void mmOctet_AddRow(mmUInt8_t* a, mmUInt8_t* b, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k)
{
    int idx = 0;
    size_t aligned_cols_k = mmOctet_AlignedCols(k);

    octet* ap = a + (i * aligned_cols_k);
    octet* bp = b + (j * aligned_cols_k);

    mmUInt16_t v_b = 0;
    mmUInt16_t v_a = 0;

    v_b = k / 8;
    v_a = k % 8;

    for (idx = 0; idx < v_b; idx++)
    {
        (*(mmUInt64_t*)&ap[8 * idx]) ^= (*(mmUInt64_t*)&bp[8 * idx]);
    }
    for (idx = v_b * 8; idx < v_b * 8 + v_a; idx++)
    {
        (*(mmUInt8_t*)&ap[idx]) ^= (*(mmUInt8_t*)&bp[idx]);
    }
}
MM_EXPORT_RAPTORQ void mmOctet_Scal(mmUInt8_t* a, mmUInt16_t i, mmUInt16_t k, mmUInt8_t u)
{
    int idx = 0;
    size_t aligned_cols_k = mmOctet_AlignedCols(k);

    octet* ap = a + (i * aligned_cols_k);

    const octet *urow_hi = OCT_MUL_HI[u];
    const octet *urow_lo = OCT_MUL_LO[u];

    if (u == 0)
    {
        return;
    }

    for (idx = 0; idx < k; idx++)
    {
        octet a_lo = ap[idx] & 0x0f;
        octet a_hi = (ap[idx] & 0xf0) >> 4;
        ap[idx] = urow_hi[a_hi] ^ urow_lo[a_lo];
    }
}
MM_EXPORT_RAPTORQ void mmOctet_Zero(mmUInt8_t* a, mmUInt16_t i, size_t k)
{
    size_t aligned_cols_k = mmOctet_AlignedCols(k);

    octet* ap = a + (i * aligned_cols_k);

    mmMemset(ap, 0, sizeof(octet) * k);
}
MM_EXPORT_RAPTORQ void mmOctet_Gemm(mmUInt8_t* a, mmUInt8_t* b, mmUInt8_t* c, mmUInt16_t n, mmUInt16_t k, mmUInt16_t m)
{
    int row = 0;
    int idx = 0;
    size_t aligned_cols_k = mmOctet_AlignedCols(k);
    size_t aligned_cols_m = mmOctet_AlignedCols(m);

    octet *ap, *cp = c;

    for (row = 0; row < n; row++, cp += aligned_cols_m)
    {
        ap = a + (row * aligned_cols_k);

        mmOctet_Zero(cp, 0, m);
        for (idx = 0; idx < k; idx++)
        {
            mmOctet_Axpy(cp, b, 0, idx, m, ap[idx]);
        }
    }
}

