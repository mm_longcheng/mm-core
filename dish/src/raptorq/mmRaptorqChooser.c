/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqChooser.h"
#include "mmRaptorqGraph.h"
#include "mmRaptorqOctetMatrix.h"

#include "core/mmAlloc.h"

MM_EXPORT_RAPTORQ void mmRaptorqChooser_Init(struct mmRaptorqChooser* p)
{
    mmVectorValue_Init(&p->tracking);
    mmVectorValue_Init(&p->r_rows);
    p->only_two_ones = 0;

    mmVectorValue_SetElement(&p->tracking, sizeof(struct mmRaptorqTrackingPair));
    mmVectorValue_SetElement(&p->r_rows, sizeof(struct mmRaptorqUInt16Pair));
}
MM_EXPORT_RAPTORQ void mmRaptorqChooser_Destroy(struct mmRaptorqChooser* p)
{
    mmRaptorqChooser_Clear(p);
    //
    mmVectorValue_Destroy(&p->tracking);
    mmVectorValue_Destroy(&p->r_rows);
    p->only_two_ones = 0;
}

MM_EXPORT_RAPTORQ void mmRaptorqChooser_SetLength(struct mmRaptorqChooser* p, mmUInt16_t length)
{
    mmVectorValue_Reserve(&p->tracking, length);
    mmVectorValue_Reserve(&p->r_rows, length);
}
MM_EXPORT_RAPTORQ void mmRaptorqChooser_Clear(struct mmRaptorqChooser* p)
{
    mmVectorValue_Clear(&p->tracking);
    mmVectorValue_Clear(&p->r_rows);
}
MM_EXPORT_RAPTORQ void mmRaptorqChooser_PushUInt16Pair(struct mmRaptorqChooser* p, mmUInt16_t f, mmUInt16_t s)
{
    struct mmRaptorqUInt16Pair* e = NULL;
    size_t index = p->r_rows.size;
    size_t sz = p->r_rows.size + 1;
    sz = sz < p->r_rows.capacity ? p->r_rows.capacity : sz;
    mmVectorValue_Reserve(&p->r_rows, sz);
    p->r_rows.size = index + 1;
    e = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->r_rows, index);
    e->f = f;
    e->s = s;
}

MM_EXPORT_RAPTORQ void mmRaptorqChooser_PushTrackingPair(struct mmRaptorqChooser* p, size_t row_degree, int is_hdpc)
{
    struct mmRaptorqTrackingPair* e = NULL;
    size_t index = p->tracking.size;
    size_t sz = p->tracking.size + 1;
    sz = sz < p->tracking.capacity ? p->tracking.capacity : sz;
    mmVectorValue_Reserve(&p->tracking, sz);
    p->tracking.size = index + 1;
    e = (struct mmRaptorqTrackingPair*)mmVectorValue_GetIndex(&p->tracking, index);
    e->row_degree = row_degree;
    e->is_hdpc = is_hdpc;
}

MM_EXPORT_RAPTORQ mmUInt16_t
mmRaptorqChooser_NonZero(
    struct mmRaptorqChooser* p,
    struct mmOctetMatrix* A,
    struct mmRaptorqGraph* G,
    mmUInt16_t i,
    mmUInt16_t sub_rows,
    mmUInt16_t sub_cols)
{
    mmUInt16_t row = 0;
    mmUInt16_t col = 0;

    mmUInt16_t non_zero = sub_cols + 1;

    // build graph, get minimum non_zero and track rows that
    // will be needed later

    for (row = 0; row < sub_rows; row++)
    {
        // if the row is NOT HDPC and has two ones,
        // it represents an edge in a graph between the two columns with "1"

        mmUInt16_t non_zero_tmp = 0;
        mmUInt16_t ones = 0;
        mmUInt16_t ones_idx[] = { 0, 0 };

        int next_row = MM_FALSE;

        for (col = 0; col < sub_cols; col++)
        {
            if ((mmUInt8_t)(mmOctetMatrix_A(A, row + i, col + i)) != 0)
            {
                if (++non_zero_tmp > non_zero)
                {
                    next_row = MM_TRUE;
                    break;
                }
            }
            if ((mmUInt8_t)(mmOctetMatrix_A(A, row + i, col + i)) == 1)
            {
                // count the ones and update ones_idx at the same time
                if (++ones <= 2)
                {
                    ones_idx[ones - 1] = col;
                }
            }
        }

        if (next_row || non_zero_tmp == 0)
        {
            continue;
        }
        // now non_zero >= non_zero_tmp, and both > 0

        // rationale & optimization, rfc 6330 pg 34
        // we need to track the rows that have the least number "r"
        // of non-zero elements.
        // if r == 2 and even just one row has the two elements to "1",
        // then we need to track only the rows with "1" in the two
        // non-zero elements.
        if (non_zero == non_zero_tmp)
        {
            // do not add if "only_two_ones && ones != 2"
            if (!p->only_two_ones || ones == 2)
            {
                mmRaptorqChooser_PushUInt16Pair(p, row, ones_idx[0]);
            }
        }
        else
        {
            // non_zero > non_zero_tmp)
            non_zero = non_zero_tmp;
            if (p->only_two_ones && non_zero == 1)
            {
                p->only_two_ones = MM_FALSE;
            }
            mmVectorValue_Clear(&p->r_rows);
            mmRaptorqChooser_PushUInt16Pair(p, row, ones_idx[0]);
        }

        if (ones == 2)
        {
            // track the maximum component in the graph
            if (non_zero == 2)
            {
                struct mmRaptorqTrackingPair* tracking = (struct mmRaptorqTrackingPair*)mmVectorValue_GetIndex(&p->tracking, row + i);
                if (tracking->is_hdpc == 0)
                {
                    mmRaptorqGraph_Link(G, ones_idx[0], ones_idx[1]);
                }
                if (!p->only_two_ones)
                {
                    // must keep only rows with two ones,
                    // so delete the other ones.
                    p->only_two_ones = MM_TRUE;

                    mmVectorValue_Clear(&p->r_rows);
                    mmRaptorqChooser_PushUInt16Pair(p, row, ones_idx[0]);
                }
            }
        }
    }
    return non_zero;
}

MM_EXPORT_RAPTORQ mmUInt16_t
mmRaptorqChooser_Pick(
    struct mmRaptorqChooser* p,
    struct mmRaptorqGraph* G,
    mmUInt16_t i,
    mmUInt16_t sub_rows,
    mmUInt16_t non_zero)
{
    mmUInt16_t chosen = sub_rows;

    // search for r.
    if (non_zero != 2)
    {
        // search for row with minimum original degree.
        // Precedence to non-hdpc

        size_t rp_idx = 0;

        mmUInt16_t min_row = sub_rows;
        mmUInt16_t min_row_hdpc = min_row;
        size_t min_degree = SIZE_MAX;
        size_t min_degree_hdpc = min_degree;

        for (rp_idx = 0; rp_idx < p->r_rows.size; rp_idx++)
        {
            struct mmRaptorqUInt16Pair* rp_a = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->r_rows, rp_idx);
            mmUInt16_t row = rp_a->f;
            struct mmRaptorqTrackingPair* tr_a = (struct mmRaptorqTrackingPair*)mmVectorValue_GetIndex(&p->tracking, row + i);
            if (tr_a->is_hdpc)
            {
                // HDPC
                if (tr_a->row_degree < min_degree_hdpc)
                {
                    min_degree_hdpc = tr_a->row_degree;
                    min_row_hdpc = row;
                }
            }
            else
            {
                // NON-HDPC
                if (tr_a->row_degree < min_degree)
                {
                    min_degree = tr_a->row_degree;
                    min_row = row;
                }
            }
        }
        if (min_row != sub_rows)
        {
            chosen = min_row;
        }
        else
        {
            chosen = min_row_hdpc;
        }
    }
    else
    {
        // non_zero == 2 => graph, else any r
        if (p->only_two_ones)
        {
            size_t rp_idx = 0;

            for (rp_idx = 0; rp_idx < p->r_rows.size; rp_idx++)
            {
                struct mmRaptorqUInt16Pair* rp_a = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->r_rows, rp_idx);
                if (mmRaptorqGraph_IsMax(G, rp_a->s))
                {
                    chosen = rp_a->f;
                    break;
                }
            }
        }
        if (chosen == sub_rows)
        {
            struct mmRaptorqUInt16Pair* rp_a = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->r_rows, 0);
            chosen = rp_a->f;
        }
    }
    return chosen;
}
