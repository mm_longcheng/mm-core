/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqRandom_h__
#define __mmRaptorqRandom_h__

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

/*
*  Rand[y, i, m] denotes a pseudo-random number generator.
*
* 5.3.5.1. Random Number Generator
*  The random number generator Rand[y, i, m] is defined as follows,
*  where y is a non-negative integer, i is a non-negative integer less
*  than 256, and m is a positive integer, and the value produced is an
*  integer between 0 and m-1. Let V0, V1, V2, and V3 be the arrays
*  provided in Section 5.5.
*  Let
*  o x0 = (y + i) mod 2^^8
*  o x1 = (floor(y / 2^^8) + i) mod 2^^8
*  o x2 = (floor(y / 2^^16) + i) mod 2^^8
*  o x3 = (floor(y / 2^^24) + i) mod 2^^8
*  Then
*  Rand[y, i, m] = (V0[x0] ^ V1[x1] ^ V2[x2] ^ V3[x3]) % m
*/
MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorq_Rand(mmUInt32_t y, mmUInt8_t i, mmUInt32_t m);

#include "core/mmSuffix.h"

#endif//__mmRaptorqRandom_h__
