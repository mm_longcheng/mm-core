/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqIO.h"

static size_t __static_mmRaptorqIO_Read(struct mmRaptorqIO* obj, void* buffer, size_t length)
{
    return 0;
}

static size_t __static_mmRaptorqIO_Write(struct mmRaptorqIO* obj, const void* buffer, size_t length)
{
    return 0;
}

static int __static_mmRaptorqIO_Seek(struct mmRaptorqIO* obj, long offset)
{
    return 0;
}

static size_t __static_mmRaptorqIO_Size(struct mmRaptorqIO* obj)
{
    return 0;
}

static long __static_mmRaptorqIO_Tell(struct mmRaptorqIO* obj)
{
    return 0;
}

MM_EXPORT_RAPTORQ void mmRaptorqIO_Init(struct mmRaptorqIO* p)
{
    p->Read = &__static_mmRaptorqIO_Read;
    p->Write = &__static_mmRaptorqIO_Write;
    p->Seek = &__static_mmRaptorqIO_Seek;
    p->Size = &__static_mmRaptorqIO_Size;
    p->Tell = &__static_mmRaptorqIO_Tell;
}
MM_EXPORT_RAPTORQ void mmRaptorqIO_Destroy(struct mmRaptorqIO* p)
{
    p->Read = &__static_mmRaptorqIO_Read;
    p->Write = &__static_mmRaptorqIO_Write;
    p->Seek = &__static_mmRaptorqIO_Seek;
    p->Size = &__static_mmRaptorqIO_Size;
    p->Tell = &__static_mmRaptorqIO_Tell;
}


static size_t __static_mmRaptorqIOFile_Read(struct mmRaptorqIO* obj, void* buffer, size_t length)
{
    struct mmRaptorqIOFile* io_file = (struct mmRaptorqIOFile *)obj;
    return fread(buffer, 1, length, io_file->fp);
}

static size_t __static_mmRaptorqIOFile_Write(struct mmRaptorqIO* obj, const void* buffer, size_t length)
{
    struct mmRaptorqIOFile* io_file = (struct mmRaptorqIOFile *)obj;
    return fwrite(buffer, 1, length, io_file->fp);
}

static int __static_mmRaptorqIOFile_Seek(struct mmRaptorqIO* obj, long offset)
{
    struct mmRaptorqIOFile* io_file = (struct mmRaptorqIOFile *)obj;
    return (fseek(io_file->fp, offset, SEEK_SET) == 0);
}

static size_t __static_mmRaptorqIOFile_Size(struct mmRaptorqIO* obj)
{
    struct mmRaptorqIOFile* io_file = (struct mmRaptorqIOFile *)obj;
    long ret = 0;
    long pos = ftell(io_file->fp);
    fseek(io_file->fp, 0, SEEK_END);
    ret = ftell(io_file->fp);
    fseek(io_file->fp, pos, SEEK_SET);
    return ret;
}

static long __static_mmRaptorqIOFile_Tell(struct mmRaptorqIO* obj)
{
    struct mmRaptorqIOFile* io_file = (struct mmRaptorqIOFile *)obj;
    return ftell(io_file->fp);
}

MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Init(struct mmRaptorqIOFile* p)
{
    mmRaptorqIO_Init(&p->io);
    p->fp = NULL;

    p->io.Read = __static_mmRaptorqIOFile_Read;
    p->io.Write = __static_mmRaptorqIOFile_Write;
    p->io.Seek = __static_mmRaptorqIOFile_Seek;
    p->io.Size = __static_mmRaptorqIOFile_Size;
    p->io.Tell = __static_mmRaptorqIOFile_Tell;
}
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Destroy(struct mmRaptorqIOFile* p)
{
    mmRaptorqIOFile_Fclose(p);
    //
    mmRaptorqIO_Destroy(&p->io);
    p->fp = NULL;
}
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Fopen(struct mmRaptorqIOFile* p, const char* filename, const char* mode)
{
    assert(NULL == p->fp && "NULL == p->fp is invalid, can not fopen twice.");
    p->fp = fopen(filename, mode);
}
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Fclose(struct mmRaptorqIOFile* p)
{
    if (NULL != p->fp)
    {
        fclose(p->fp);
        p->fp = NULL;
    }
}

