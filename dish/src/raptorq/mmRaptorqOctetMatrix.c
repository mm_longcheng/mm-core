/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqOctetMatrix.h"
#include "mmRaptorqOctet.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"

MM_EXPORT_RAPTORQ void mmOctetMatrix_Init(struct mmOctetMatrix* p)
{
    p->data = NULL;
    p->rows = 0;
    p->cols = 0;
    p->cols_align = 0;
    p->max_size = 0;
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Destroy(struct mmOctetMatrix* p)
{
    mmOctetMatrix_Free(p);
    //
    p->data = NULL;
    p->rows = 0;
    p->cols = 0;
    p->cols_align = 0;
    p->max_size = 0;
}

MM_EXPORT_RAPTORQ void mmOctetMatrix_Reset(struct mmOctetMatrix* p)
{
    mmMemset(p->data, 0, p->max_size);
    //
    p->rows = 0;
    p->cols = 0;
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Realloc(struct mmOctetMatrix* p, mmUInt16_t r, mmUInt16_t c)
{
    mmUInt8_t* aligned = NULL;

    size_t max_size = 0;
    mmUInt64_t mem_size = 0;

    p->rows = r;
    p->cols = c;

    p->cols_align = (c / MM_OCTMAT_ALIGN + ((c % MM_OCTMAT_ALIGN) ? 1 : 0)) * MM_OCTMAT_ALIGN;

    max_size = p->rows * p->cols_align;

    if (p->max_size < max_size)
    {
        mem_size = max_size;
        mmRoundup64(mem_size);
        aligned = (mmUInt8_t*)mmAlignedMalloc((size_t)mem_size, MM_OCTMAT_ALIGN);
        assert(NULL != aligned && "NULL != aligned is invalid.");
        mmMemset(aligned, 0, mem_size);
        mmAlignedFree(p->data);
        p->data = aligned;
        p->max_size = (size_t)mem_size;
    }
    else if (max_size < (p->max_size / 4) && 8 < p->max_size)
    {
        mem_size = max_size;
        mmRoundup64(mem_size);
        mem_size <<= 1;
        aligned = (mmUInt8_t*)mmAlignedMalloc((size_t)mem_size, MM_OCTMAT_ALIGN);
        assert(NULL != aligned && "NULL != aligned is invalid.");
        mmMemset(aligned, 0, mem_size);
        mmAlignedFree(p->data);
        p->data = aligned;
        p->max_size = (size_t)mem_size;
    }
    else
    {
        mmMemset(p->data, 0, p->max_size);
    }
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Free(struct mmOctetMatrix* p)
{
    mmAlignedFree(p->data);
    p->data = NULL;
    p->rows = 0;
    p->cols = 0;
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Resize(struct mmOctetMatrix* p, mmUInt16_t r, mmUInt16_t c)
{
    mmOctetMatrix_Realloc(p, r, c);
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Copy(struct mmOctetMatrix* p, struct mmOctetMatrix* q)
{
    p->rows = q->rows;
    p->cols = q->cols;
    p->cols_align = q->cols_align;

    mmOctetMatrix_Resize(p, q->rows, q->cols);

    mmMemcpy(p->data, q->data, q->rows * q->cols_align);
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Clear(struct mmOctetMatrix* p)
{
    p->rows = 0;
    p->cols = 0;
    p->cols_align = 0;
}
MM_EXPORT_RAPTORQ void mmOctetMatrix_Fprintf(struct mmOctetMatrix* p, FILE* stream)
{
    int i = 0;
    int j = 0;
    fprintf(stream, "[%ux%u]\n", p->rows, p->cols);
    for (i = 0; i < p->rows; i++)
    {
        fprintf(stream, "|%3d", mmOctetMatrix_A(p, i, 0));
        for (j = 1; j < p->cols; j++)
        {
            fprintf(stream, ", %3d", mmOctetMatrix_A(p, i, j));
        }
        fprintf(stream, "  |\n");
    }
}
