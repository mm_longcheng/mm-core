/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqMath_h__
#define __mmRaptorqMath_h__

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

#define mmRaptorq_Max(a,b)    (((a) > (b)) ? (a) : (b))
#define mmRaptorq_Min(a,b)    (((a) < (b)) ? (a) : (b))
#define mmRaptorq_Ceil(x)   ceil(x)
#define mmRaptorq_Floor(x)  floor(x)
#define mmRaptorq_DivCeil(a, b) ((a) / (b) + ((a) % (b) ? 1 : 0))
#define mmRaptorq_DivFloor(a, b) ((a) / (b))

MM_EXPORT_RAPTORQ int mmRaptorq_UInt16IsPrime(const mmUInt16_t n);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorq_UInt16NextPrime(mmUInt16_t n);

MM_EXPORT_RAPTORQ int mmRaptorq_UInt32IsPrime(const mmUInt32_t n);
MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorq_UInt32NextPrime(mmUInt32_t n);

#include "core/mmSuffix.h"

#endif//__mmRaptorqMath_h__
