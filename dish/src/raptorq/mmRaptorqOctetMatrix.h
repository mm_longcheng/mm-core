/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqOctetMatrix_h__
#define __mmRaptorqOctetMatrix_h__

// https://github.com/sleepybishop/oblas

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

struct mmOctetMatrix
{
    mmUInt8_t* data;
    mmUInt16_t rows;
    mmUInt16_t cols;
    mmUInt16_t cols_align;
    size_t max_size;
};

MM_EXPORT_RAPTORQ void mmOctetMatrix_Init(struct mmOctetMatrix* p);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Destroy(struct mmOctetMatrix* p);

MM_EXPORT_RAPTORQ void mmOctetMatrix_Reset(struct mmOctetMatrix* p);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Realloc(struct mmOctetMatrix* p, mmUInt16_t r, mmUInt16_t c);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Free(struct mmOctetMatrix* p);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Resize(struct mmOctetMatrix* p, mmUInt16_t r, mmUInt16_t c);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Copy(struct mmOctetMatrix* p, struct mmOctetMatrix* q);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Clear(struct mmOctetMatrix* p);
MM_EXPORT_RAPTORQ void mmOctetMatrix_Fprintf(struct mmOctetMatrix* p, FILE* stream);

#define mmOctetMatrix_R(v, x) ((v)->data + ((x) * (v)->cols_align))
#define mmOctetMatrix_P(v) mmOctetMatrix_R(v, 0)
#define mmOctetMatrix_A(v, x, y) (mmOctetMatrix_R(v, x)[(y)])

#include "core/mmSuffix.h"

#endif//__mmRaptorqOctetMatrix_h__
