/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqContext.h"
#include "mmRaptorqIO.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"

MM_EXPORT_RAPTORQ void mmRaptorqOtiCommon_Init(struct mmRaptorqOtiCommon* p)
{
    p->F = 0;
    p->WS = 0;
    p->T = 0;
    p->SS = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqOtiCommon_Destroy(struct mmRaptorqOtiCommon* p)
{
    p->F = 0;
    p->WS = 0;
    p->T = 0;
    p->SS = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqOtiCommon_Reset(struct mmRaptorqOtiCommon* p)
{
    p->F = 0;
    p->WS = 0;
    p->T = 0;
    p->SS = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqOtiScheme_Init(struct mmRaptorqOtiScheme* p)
{
    p->Kt = 0;
    p->Z = 0;
    p->N = 0;
    p->Al = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqOtiScheme_Destroy(struct mmRaptorqOtiScheme* p)
{
    p->Kt = 0;
    p->Z = 0;
    p->N = 0;
    p->Al = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqOtiScheme_Reset(struct mmRaptorqOtiScheme* p)
{
    p->Kt = 0;
    p->Z = 0;
    p->N = 0;
    p->Al = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Init(struct mmRaptorqPartition* p)
{
    p->IL = 0;
    p->IS = 0;
    p->JL = 0;
    p->JS = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Destroy(struct mmRaptorqPartition* p)
{
    p->IL = 0;
    p->IS = 0;
    p->JL = 0;
    p->JS = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Reset(struct mmRaptorqPartition* p)
{
    p->IL = 0;
    p->IS = 0;
    p->JL = 0;
    p->JS = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqPartition_Assign(struct mmRaptorqPartition* p, mmUInt64_t I, mmUInt16_t J)
{
    if (J == 0)
    {
        p->IL = 0;
        p->IS = 0;
        p->JL = 0;
        p->JS = 0;
    }
    else
    {
        p->IL = (mmUInt16_t)mmRaptorq_Ceil((double)I / J);
        p->IS = (mmUInt16_t)mmRaptorq_Floor((double)I / J);
        p->JL = (mmUInt16_t)I - p->IS * J;
        p->JS = (mmUInt16_t)J - p->JL;

        if (p->JL == 0)
        {
            p->IL = 0;
        }
    }
}

MM_EXPORT_RAPTORQ size_t
mmRaptorqSourceBlock_GetSymbolOffset(
    struct mmRaptorqSourceBlock* p,
    size_t pos,
    mmUInt16_t K,
    mmUInt32_t symbol_id)
{
    size_t i;

    if (pos < p->part_tot)
    {
        size_t sub_blk_id = pos / p->part.IL;
        i = p->sbloc + sub_blk_id * K * p->part.IL + symbol_id * p->part.IL + pos % p->part.IL;
    }
    else
    {
        size_t pos_part2 = pos - p->part_tot;
        size_t sub_blk_id = pos_part2 / p->part.IS;
        i = p->sbloc + (p->part_tot * K) + sub_blk_id * K * p->part.IS + symbol_id * p->part.IS + pos_part2 % p->part.IS;
    }
    return i * p->al;
}

MM_EXPORT_RAPTORQ void mmRaptorqEncoderCore_Init(struct mmRaptorqEncoderCore* p)
{
    mmRaptorqPrecode_Init(&p->precode);
    mmOctetMatrix_Init(&p->symbolmat);
    p->num_symbols = 0;
    p->symbol_size = 0;
    p->sbn = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqEncoderCore_Destroy(struct mmRaptorqEncoderCore* p)
{
    mmRaptorqPrecode_Destroy(&p->precode);
    mmOctetMatrix_Destroy(&p->symbolmat);
    p->num_symbols = 0;
    p->symbol_size = 0;
    p->sbn = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqEncoderCore_Reset(struct mmRaptorqEncoderCore* p)
{
    mmRaptorqPrecode_Reset(&p->precode);
    mmOctetMatrix_Reset(&p->symbolmat);
    p->num_symbols = 0;
    p->symbol_size = 0;
    p->sbn = 0;
}

MM_EXPORT_RAPTORQ void
mmRaptorqEncoderCore_Assign(
    struct mmRaptorqEncoderCore* p,
    mmUInt16_t num_symbols,
    mmUInt16_t symbol_size,
    mmUInt8_t sbn)
{
    p->sbn = sbn;
    p->num_symbols = num_symbols;
    p->symbol_size = symbol_size;
    mmRaptorqPrecode_SetSymbols(&p->precode, p->num_symbols);
}

MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_Init(struct mmRaptorqDecoderCore* p)
{
    mmRaptorqPrecode_Init(&p->precode);
    mmOctetMatrix_Init(&p->symbolmat);
    mmRaptorqRepairVector_Init(&p->repair_bin);
    mmBitset_Init(&p->mask);
    p->num_symbols = 0;
    p->symbol_size = 0;
    p->sbn = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_Destroy(struct mmRaptorqDecoderCore* p)
{
    mmRaptorqPrecode_Destroy(&p->precode);
    mmOctetMatrix_Destroy(&p->symbolmat);
    mmRaptorqRepairVector_Destroy(&p->repair_bin);
    mmBitset_Destroy(&p->mask);
    p->num_symbols = 0;
    p->symbol_size = 0;
    p->sbn = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_Reset(struct mmRaptorqDecoderCore* p)
{
    mmRaptorqPrecode_Reset(&p->precode);
    mmOctetMatrix_Reset(&p->symbolmat);
    mmRaptorqRepairVector_Reset(&p->repair_bin);
    mmBitset_Reset(&p->mask);
    p->num_symbols = 0;
    p->symbol_size = 0;
    p->sbn = 0;
}

MM_EXPORT_RAPTORQ void
mmRaptorqDecoderCore_Assign(
    struct mmRaptorqDecoderCore* p,
    mmUInt16_t num_symbols,
    mmUInt16_t symbol_size,
    mmUInt8_t sbn)
{
    p->sbn = sbn;
    p->num_symbols = num_symbols;
    p->symbol_size = symbol_size;
    mmRaptorqPrecode_SetSymbols(&p->precode, p->num_symbols);
    // default all mask bit is 0.
    mmBitset_Resize(&p->mask, p->num_symbols);
}

MM_EXPORT_RAPTORQ void mmRaptorqDecoderCore_AssignAlign(struct mmRaptorqDecoderCore* p, mmUInt8_t Al)
{
    mmOctetMatrix_Resize(&p->symbolmat, p->num_symbols, p->symbol_size * Al);
}

MM_EXPORT_RAPTORQ void mmRaptorqContext_Init(struct mmRaptorqContext* p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmRaptorqOtiCommon_Init(&p->common);
    mmRaptorqOtiScheme_Init(&p->scheme);
    mmRaptorqPartition_Init(&p->src_part);
    mmRaptorqPartition_Init(&p->sub_part);
    mmRbtreeU32Vpt_Init(&p->encoders);
    mmRbtreeU32Vpt_Init(&p->decoders);
    mmBitset_Init(&p->mask);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->encoders, &_U32VptAllocator);
    mmRbtreeU32Vpt_SetAllocator(&p->decoders, &_U32VptAllocator);
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_Destroy(struct mmRaptorqContext* p)
{
    mmRaptorqContext_Clear(p);
    //
    mmRaptorqOtiCommon_Destroy(&p->common);
    mmRaptorqOtiScheme_Destroy(&p->scheme);
    mmRaptorqPartition_Destroy(&p->src_part);
    mmRaptorqPartition_Destroy(&p->sub_part);
    mmRbtreeU32Vpt_Destroy(&p->encoders);
    mmRbtreeU32Vpt_Destroy(&p->decoders);
    mmBitset_Destroy(&p->mask);
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_Clear(struct mmRaptorqContext* p)
{
    mmUInt8_t sbn = 0;
    mmUInt8_t num_sbn = mmRaptorqContext_Blocks(p);
    for (sbn = 0; sbn < num_sbn; sbn++)
    {
        mmRaptorqContext_EncoderCleanup(p, sbn);
        mmRaptorqContext_DecoderCleanup(p, sbn);
    }
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_Reset(struct mmRaptorqContext* p)
{
    mmUInt8_t sbn = 0;
    mmUInt8_t num_sbn = mmRaptorqContext_Blocks(p);

    struct mmRaptorqEncoderCore* enc = NULL;
    struct mmRaptorqDecoderCore* dec = NULL;

    for (sbn = 0; sbn < num_sbn; sbn++)
    {
        enc = (struct mmRaptorqEncoderCore*)mmRbtreeU32Vpt_Get(&p->encoders, sbn);
        if (NULL != enc)
        {
            mmRaptorqEncoderCore_Reset(enc);
        }
        dec = (struct mmRaptorqDecoderCore*)mmRbtreeU32Vpt_Get(&p->decoders, sbn);
        if (NULL != dec)
        {
            mmRaptorqDecoderCore_Reset(dec);
        }
    }

    mmRaptorqOtiCommon_Reset(&p->common);
    mmRaptorqOtiScheme_Reset(&p->scheme);

    mmRaptorqPartition_Reset(&p->src_part);
    mmRaptorqPartition_Reset(&p->sub_part);

    mmBitset_Reset(&p->mask);
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_MaskReset(struct mmRaptorqContext* p)
{
    mmUInt8_t num_sbn = mmRaptorqContext_Blocks(p);
    mmBitset_Resize(&p->mask, num_sbn);
    mmBitset_Reset(&p->mask);
    mmBitset_FlipAll(&p->mask);
}

MM_EXPORT_RAPTORQ void
mmRaptorqContext_EncoderAssign(
    struct mmRaptorqContext* p,
    mmUInt64_t F,
    mmUInt16_t T,
    mmUInt16_t SS,
    mmUInt8_t Al,
    size_t WS)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    if (T == 0 || Al == 0 || T < Al || T % Al != 0 || SS < Al || (SS % Al) != 0 || SS > T)
    {
        mmLogger_LogE(gLogger, "%s %d mmRaptorqContext encoder assign parameter error.", __FUNCTION__, __LINE__);
        return;
    }
    assert(0 != F && "F transfer length is 0.");

    p->common.F = F;
    p->common.WS = WS;
    p->common.SS = SS;
    p->common.T = T;
    p->scheme.Al = Al;

    mmRaptorqContext_GenerateSchemeSpecific(p);

    assert(p->scheme.Z != 0 && p->scheme.N != 0 &&
           mmRaptorq_DivCeil(p->scheme.Kt, p->scheme.Z) <= mmRaptorq_K_max &&
           "mm_raptorq_oti parameter error.");

    mmRaptorqPartition_Assign(&p->src_part, p->scheme.Kt, p->scheme.Z);
    mmRaptorqPartition_Assign(&p->sub_part, p->common.T / p->scheme.Al, p->scheme.N);

    mmRaptorqContext_MaskReset(p);

    {
        // encoder core reset and assign.
        struct mmRaptorqEncoderCore* enc = NULL;
        mmUInt16_t symbol_size = p->common.T / p->scheme.Al;

        mmUInt8_t sbn = 0;
        mmUInt8_t num_sbn = mmRaptorqContext_Blocks(p);
        for (sbn = 0; sbn < num_sbn; sbn++)
        {
            mmUInt16_t num_symbols = mmRaptorqContext_BlockSymbols(p, sbn);

            enc = (struct mmRaptorqEncoderCore*)mmRbtreeU32Vpt_Get(&p->encoders, sbn);
            if (NULL != enc && num_symbols != 0 && symbol_size != 0)
            {
                mmRaptorqEncoderCore_Reset(enc);

                mmRaptorqEncoderCore_Assign(enc, num_symbols, symbol_size, sbn);
            }
        }
    }

    mmLogger_LogD(gLogger, "%s %d T: %06d SS: %06d AL: %03d WS: %06" PRIuPTR "", __FUNCTION__, __LINE__, p->common.T, p->common.SS, p->scheme.Al, p->common.WS);
    mmLogger_LogD(gLogger, "%s %d Z: %06d N : %06d         Kt: %06" PRIu64 "", __FUNCTION__, __LINE__, p->scheme.Z, p->scheme.N, p->scheme.Kt);
    mmLogger_LogD(gLogger, "%s %d P1 %dx%d P2 %dx%d", __FUNCTION__, __LINE__, p->src_part.JL, p->src_part.IL, p->src_part.JS, p->src_part.IS);
}

MM_EXPORT_RAPTORQ void
mmRaptorqContext_DecoderAssign(
    struct mmRaptorqContext* p,
    mmUInt64_t common,
    mmUInt32_t scheme)
{
    struct mmLogger* gLogger = mmLogger_Instance();

    mmUInt64_t F = common >> 24;
    mmUInt16_t T = common & 0xffff;

    if (F > MM_RAPTORQ_MAX_TRANSFER)
    {
        mmLogger_LogE(gLogger, "%s %d mmRaptorqContext decoder assign parameter error.", __FUNCTION__, __LINE__);
        return;
    }

    p->common.F = F;
    p->common.T = T;

    p->scheme.Z = (scheme >> 24) & 0xff;
    p->scheme.N = (scheme >> 8) & 0xffff;
    p->scheme.Al = scheme & 0xff;
    p->scheme.Kt = mmRaptorq_DivCeil(p->common.F, p->common.T);

    if (p->scheme.Z == 0)
    {
        p->scheme.Z = (UINT8_MAX + 1);
    }
    if (p->scheme.N == 0)
    {
        p->scheme.N = (UINT16_MAX + 1);
    }

    if (p->common.T < p->scheme.Al || p->common.T % p->scheme.Al != 0 ||
        mmRaptorq_DivCeil(mmRaptorq_DivCeil(p->common.F, p->common.T), p->scheme.Z) > mmRaptorq_K_max)
    {
        mmLogger_LogE(gLogger, "%s %d mmRaptorqContext decoder assign parameter error.", __FUNCTION__, __LINE__);
        return;
    }

    mmRaptorqPartition_Assign(&p->src_part, p->scheme.Kt, p->scheme.Z);
    mmRaptorqPartition_Assign(&p->sub_part, p->common.T / p->scheme.Al, p->scheme.N);

    mmRaptorqContext_MaskReset(p);

    {
        // decoder core reset and assign.
        struct mmRaptorqDecoderCore* dec = NULL;
        mmUInt16_t symbol_size = p->common.T / p->scheme.Al;

        mmUInt8_t sbn = 0;
        mmUInt8_t num_sbn = mmRaptorqContext_Blocks(p);
        for (sbn = 0; sbn < num_sbn; sbn++)
        {
            mmUInt16_t num_symbols = mmRaptorqContext_BlockSymbols(p, sbn);

            dec = (struct mmRaptorqDecoderCore*)mmRbtreeU32Vpt_Get(&p->decoders, sbn);
            if (NULL != dec && num_symbols != 0 && symbol_size != 0)
            {
                mmRaptorqDecoderCore_Reset(dec);

                mmRaptorqDecoderCore_Assign(dec, num_symbols, symbol_size, sbn);
                mmRaptorqDecoderCore_AssignAlign(dec, (mmUInt8_t)p->scheme.Al);
            }
        }
    }

    mmLogger_LogD(gLogger, "%s %d T: %06d SS: XXXXXX AL: %03d WS: XXXXXX", __FUNCTION__, __LINE__, p->common.T, p->scheme.Al);
    mmLogger_LogD(gLogger, "%s %d Z: %06d N : %06d         Kt: %06" PRIu64 "", __FUNCTION__, __LINE__, p->scheme.Z, p->scheme.N, p->scheme.Kt);
    mmLogger_LogD(gLogger, "%s %d P1 %dx%d P2 %dx%d", __FUNCTION__, __LINE__, p->src_part.JL, p->src_part.IL, p->src_part.JS, p->src_part.IS);
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_GenerateSchemeSpecific(struct mmRaptorqContext* p)
{
    struct mmVectorU16 KL;

    // o N_max = floor(T/(SS*Al))
    size_t N_max = (size_t)(mmRaptorq_DivFloor(p->common.T, p->common.SS)), n;

    // o Kt = ceil(F/T)
    p->scheme.Kt = mmRaptorq_DivCeil(p->common.F, p->common.T);

    mmVectorU16_Init(&KL);
    mmVectorU16_Reserve(&KL, N_max);

    do
    {
        size_t Z_t = 0;
        mmUInt16_t N_t = 0;

        /*
        * o for all n=1, ..., N_max
        *   * KL(n) is the maximum K�� value in Table 2 in Section 5.6 such
        *     that
        *       K�� <= WS/(Al*(ceil(T/(Al*n))))
        */
        for (n = 1; n <= N_max; n++)
        {
            mmUInt16_t idx;
            size_t KL_max = (size_t)p->common.WS / ((size_t)p->scheme.Al * mmRaptorq_DivCeil(p->common.T, (size_t)(p->scheme.Al * n)));
            if (KL_max > UINT16_MAX)
            {
                KL_max = mmRaptorq_K_max;
            }

            idx = mmRaptorqSystematic_KIndexKl((mmUInt16_t)KL_max);

            mmVectorU16_PushBack(&KL, mmRaptorqSystematic_K(idx == 0 ? 0 : (idx - 1)));
        }
        Z_t = (size_t)(mmRaptorq_DivCeil(p->scheme.Kt, mmVectorU16_GetIndex(&KL, N_max - 1)));
        if (Z_t > (UINT8_MAX + 1))
        {
            break;
        }

        // o Z = ceil(Kt/KL(N_max))
        p->scheme.Z = (mmUInt16_t)Z_t;

        // o N is the minimum n=1, ..., N_max such that ceil(Kt/Z) <= KL(n)
        N_t = (mmUInt16_t)(mmRaptorq_DivCeil(p->scheme.Kt, p->scheme.Z));
        for (n = 0; n < KL.size; n++)
        {
            if (N_t <= mmVectorU16_GetIndex(&KL, n))
            {
                p->scheme.N = (mmUInt16_t)(n + 1);
                break;
            }
        }
    } while (0);

    mmVectorU16_Destroy(&KL);
}

MM_EXPORT_RAPTORQ void
mmRaptorqContext_SourceBlock(
    struct mmRaptorqContext* p,
    mmUInt8_t sbn,
    mmUInt16_t symbol_size,
    struct mmRaptorqSourceBlock* block)
{
    block->part = p->sub_part;
    block->al = p->scheme.Al;
    block->sbloc = 0;
    block->part_tot = p->sub_part.IL * p->sub_part.JL;

    if (sbn < p->src_part.JL)
    {
        block->sbloc = sbn * p->src_part.IL * symbol_size;
    }
    else if (sbn - p->src_part.JL < p->src_part.JS)
    {
        block->sbloc = (p->src_part.IL * p->src_part.JL) * symbol_size + (sbn - p->src_part.JL) * p->src_part.IS * symbol_size;
    }
}

MM_EXPORT_RAPTORQ mmUInt64_t mmRaptorqContext_OtiCommon(struct mmRaptorqContext* p)
{
    mmUInt64_t ret = 0;

    ret = p->common.F << 24; /* transfer length */
    ret |= p->common.T;      /* symbol size */

    return ret;
}

MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_OtiSchemeSpecific(struct mmRaptorqContext* p)
{
    mmUInt32_t ret = 0;

    p->scheme.Z %= (UINT8_MAX + 1);
    p->scheme.N %= (UINT16_MAX + 1);
    ret = p->scheme.Z << 24; /* number of source blocks */
    ret |= p->scheme.N << 8; /* number of sub-blocks */
    ret |= p->scheme.Al;     /* symbol alignment */

    return ret;
}

MM_EXPORT_RAPTORQ void mmRaptorqContext_GeneratePayloadId(mmUInt8_t sbn, mmUInt32_t esi, mmUInt32_t* payload_id)
{
    *payload_id = (mmUInt32_t)(sbn) << 24;
    *payload_id += esi % (mmUInt32_t)(1 << 24);
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_AssemblyPayloadId(mmUInt32_t payload_id, mmUInt8_t* sbn, mmUInt32_t* esi)
{
    *sbn = payload_id >> 24;
    *esi = (payload_id & 0x00ffffff);
}

MM_EXPORT_RAPTORQ mmUInt64_t mmRaptorqContext_TransferLength(struct mmRaptorqContext* p)
{
    return p->common.F;
}

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqContext_SymbolSize(struct mmRaptorqContext* p)
{
    return p->common.T;
}

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqContext_BlockSymbols(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    if (sbn < p->src_part.JL)
    {
        return p->src_part.IL;
    }
    if (sbn - p->src_part.JL < p->src_part.JS)
    {
        return p->src_part.IS;
    }
    return 0;
}

MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_MaxRepair(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    return (mmUInt32_t)((1 << 20) - mmRaptorqContext_BlockSymbols(p, sbn));
}

MM_EXPORT_RAPTORQ mmUInt8_t mmRaptorqContext_Blocks(struct mmRaptorqContext* p)
{
    return (mmUInt8_t)(p->src_part.JL + p->src_part.JS);
}

MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_NumMissing(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    mmUInt16_t num_symbols = mmRaptorqContext_BlockSymbols(p, sbn);
    struct mmRaptorqDecoderCore *dec = mmRaptorqContext_BlockDecoder(p, sbn);
    if (dec == NULL)
    {
        return 0;
    }

    return (mmUInt32_t)mmBitset_Count0(&dec->mask, num_symbols);
}

MM_EXPORT_RAPTORQ mmUInt32_t mmRaptorqContext_NumRepair(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    struct mmRaptorqDecoderCore *dec = mmRaptorqContext_BlockDecoder(p, sbn);
    if (dec == NULL)
    {
        return 0;
    }

    return (mmUInt32_t)dec->repair_bin.v.size;
}
MM_EXPORT_RAPTORQ struct mmRaptorqEncoderCore* mmRaptorqContext_BlockEncoder(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    struct mmRaptorqEncoderCore* enc = NULL;

    mmUInt16_t num_symbols = mmRaptorqContext_BlockSymbols(p, sbn);
    mmUInt16_t symbol_size = p->common.T / p->scheme.Al;

    enc = (struct mmRaptorqEncoderCore*)mmRbtreeU32Vpt_Get(&p->encoders, sbn);
    if (NULL != enc)
    {
        return enc;
    }

    if (num_symbols == 0 || symbol_size == 0)
    {
        return NULL;
    }

    enc = (struct mmRaptorqEncoderCore*)mmMalloc(sizeof(struct mmRaptorqEncoderCore));
    mmRaptorqEncoderCore_Init(enc);
    mmRaptorqEncoderCore_Assign(enc, num_symbols, symbol_size, sbn);
    mmRbtreeU32Vpt_Set(&p->encoders, sbn, enc);
    return enc;
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_EncoderCleanup(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    struct mmRaptorqEncoderCore* enc = (struct mmRaptorqEncoderCore*)mmRbtreeU32Vpt_Get(&p->encoders, sbn);
    if (NULL != enc)
    {
        mmRaptorqEncoderCore_Destroy(enc);
        mmRbtreeU32Vpt_Rmv(&p->encoders, sbn);
        mmFree(enc);
    }
}
MM_EXPORT_RAPTORQ struct mmRaptorqDecoderCore* mmRaptorqContext_BlockDecoder(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    struct mmRaptorqDecoderCore* dec = NULL;

    mmUInt16_t num_symbols = mmRaptorqContext_BlockSymbols(p, sbn);
    mmUInt16_t symbol_size = p->common.T / p->scheme.Al;

    dec = (struct mmRaptorqDecoderCore*)mmRbtreeU32Vpt_Get(&p->decoders, sbn);
    if (NULL != dec)
    {
        return dec;
    }

    if (num_symbols == 0 || symbol_size == 0)
    {
        return NULL;
    }

    dec = (struct mmRaptorqDecoderCore*)mmMalloc(sizeof(struct mmRaptorqDecoderCore));
    mmRaptorqDecoderCore_Init(dec);
    mmRaptorqDecoderCore_Assign(dec, num_symbols, symbol_size, sbn);
    mmRaptorqDecoderCore_AssignAlign(dec, (mmUInt8_t)p->scheme.Al);
    mmRbtreeU32Vpt_Set(&p->decoders, sbn, dec);
    return dec;
}
MM_EXPORT_RAPTORQ void mmRaptorqContext_DecoderCleanup(struct mmRaptorqContext* p, mmUInt8_t sbn)
{
    struct mmRaptorqDecoderCore* dec = (struct mmRaptorqDecoderCore*)mmRbtreeU32Vpt_Get(&p->decoders, sbn);
    if (NULL != dec)
    {
        mmRaptorqDecoderCore_Destroy(dec);
        mmRbtreeU32Vpt_Rmv(&p->decoders, sbn);
        mmFree(dec);
    }
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecoderAssemblySymbols(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    mmUInt16_t* symbol_size)
{
    int code = MM_FALSE;

    struct mmRaptorqDecoderCore* dec = NULL;

    mmUInt16_t cols = 0;

    (*symbol_size) = 0;

    do
    {
        dec = mmRaptorqContext_BlockDecoder(p, sbn);

        if (dec == NULL)
        {
            code = MM_FALSE;
            break;
        }

        cols = dec->symbolmat.cols;

        if (esi >= (1 << 20))
        {
            code = MM_FALSE;
            break;
        }

        // make sure bit set is enough.
        // bit number need more than the mask.n.
        size_t n = dec->mask.n > esi + 1 ? dec->mask.n : esi + 1;
        mmBitset_Resize(&dec->mask, n);

        if (mmBitset_Count0(&dec->mask, dec->num_symbols) == 0)
        {
            // no gaps! no repair needed.
            code = MM_TRUE;
            break;
        }

        if (mmBitset_Get(&dec->mask, esi))
        {
            // already got this esi
            code = MM_TRUE;
            break;
        }

        if (esi < dec->num_symbols)
        {
            mmMemcpy(mmOctetMatrix_R(&dec->symbolmat, esi), data, cols);
        }
        else
        {
            mmRaptorqRepairVector_PushRepairSym(&dec->repair_bin, esi, cols, data);
        }
        mmBitset_Set(&dec->mask, esi, 1);

        (*symbol_size) += cols;

        if ((*symbol_size) != mmRaptorqContext_SymbolSize(p))
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);

    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncoderGenerateSymbols(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    struct mmRaptorqIO* io,
    mmUInt16_t* symbol_size)
{
    int code = MM_FALSE;

    (*symbol_size) = 0;

    do
    {
        struct mmRaptorqEncoderCore* enc = NULL;

        enc = mmRaptorqContext_BlockEncoder(p, sbn);
        if (enc == NULL)
        {
            code = MM_FALSE;
            break;
        }

        if (esi < enc->num_symbols)
        {
            mmUInt16_t i = 0;
            struct mmRaptorqSourceBlock blk;
            mmRaptorqContext_SourceBlock(p, sbn, enc->symbol_size, &blk);
            for (i = 0; i < enc->symbol_size;)
            {
                size_t offset = mmRaptorqSourceBlock_GetSymbolOffset(&blk, i, enc->num_symbols, esi);
                mmUInt16_t sublen = (i < blk.part_tot) ? blk.part.IL : blk.part.IS;
                mmUInt16_t stride = sublen * p->scheme.Al;
                mmUInt8_t* buf = (mmUInt8_t*)mmMalloc(stride);
                size_t got = 0;

                i += sublen;

                if ((*(io->Seek))(io, (long)offset))
                {
                    got = (*(io->Read))(io, buf, stride);
                }
                mmMemcpy(data, buf, got);
                (*symbol_size) += (mmUInt16_t)got;

                mmMemset(data + got, 0, stride - got);
                (*symbol_size) += (mmUInt16_t)(stride - got);

                mmFree(buf);
            }
        }
        else
        {
            // esi is for repair symbol
            struct mmRaptorqPrecode* precode = &enc->precode;
            if (enc->symbolmat.rows == 0)
            {
                mmUInt64_t encode_length = 0;
                int generated = mmRaptorqContext_EncodeBlock(p, io, sbn, &encode_length);
                if (!generated)
                {
                    code = MM_FALSE;
                    break;
                }
            }
            {
                mmUInt8_t* octet = NULL;
                mmUInt16_t i = 0;
                int byte = 0;

                struct mmOctetMatrix t_G;
                mmUInt32_t isi = esi + (precode->Kp - enc->num_symbols);

                mmOctetMatrix_Init(&t_G);
                mmRaptorqPrecode_MatrixEncode(precode, &enc->symbolmat, isi, &t_G);
                octet = mmOctetMatrix_P(&t_G);
                for (i = 0; i < enc->symbol_size; i++)
                {
                    for (byte = 0; byte < p->scheme.Al; byte++)
                    {
                        *(data++) = (octet == NULL) ? 0 : *(octet++);
                        (*symbol_size)++;
                    }
                }
                mmOctetMatrix_Destroy(&t_G);
            }
        }
        if ((*symbol_size) != mmRaptorqContext_SymbolSize(p))
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);
    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncodeBlock(
    struct mmRaptorqContext* p,
    struct mmRaptorqIO* io,
    mmUInt8_t sbn,
    mmUInt64_t* encode_length)
{
    int code = MM_FALSE;

    struct mmRaptorqEncoderCore* enc = NULL;
    struct mmRaptorqPrecode* precode = NULL;

    mmUInt16_t row = 0, col = 0;

    struct mmOctetMatrix A;
    struct mmOctetMatrix D;

    struct mmRaptorqSourceBlock blk;

    (*encode_length) = 0;

    mmOctetMatrix_Init(&A);
    mmOctetMatrix_Init(&D);

    do
    {
        enc = mmRaptorqContext_BlockEncoder(p, sbn);

        if (enc == NULL)
        {
            code = MM_FALSE;
            break;
        }

        if (enc->symbolmat.rows > 0)
        {
            code = MM_TRUE;
            break;
        }

        precode = &enc->precode;
        mmRaptorqPrecode_MatrixGenerate(precode, &A, 0);

        mmOctetMatrix_Resize(&D, precode->Kp + precode->S + precode->H, enc->symbol_size * p->scheme.Al);

        for (row = 0; row < precode->S + precode->H; row++)
        {
            for (col = 0; col < D.cols; col++)
            {
                mmOctetMatrix_A(&D, row, col) = 0;
            }
        }

        mmRaptorqContext_SourceBlock(p, sbn, enc->symbol_size, &blk);
        for (; row < precode->S + precode->H + enc->num_symbols; row++)
        {
            mmUInt16_t i = 0;
            mmUInt32_t symbol_id = row - (precode->S + precode->H);
            col = 0;
            for (i = 0; i < enc->symbol_size;)
            {
                size_t byte = 0;
                size_t offset = mmRaptorqSourceBlock_GetSymbolOffset(&blk, i, enc->num_symbols, symbol_id);
                mmUInt16_t sublen = (i < blk.part_tot) ? blk.part.IL : blk.part.IS;
                mmUInt16_t stride = sublen * p->scheme.Al;
                mmUInt8_t* buf = (mmUInt8_t*)mmMalloc(stride);
                size_t got = 0;

                i += sublen;

                if ((*(io->Seek))(io, (long)offset))
                {
                    got = (*(io->Read))(io, buf, stride);
                }

                (*encode_length) += got;

                for (byte = 0; byte < got; byte++)
                {
                    mmOctetMatrix_A(&D, row, col++) = buf[byte];
                }
                for (byte = got; byte < stride; byte++)
                {
                    mmOctetMatrix_A(&D, row, col++) = 0;
                }
                mmFree(buf);
            }
        }

        for (; row < D.rows; row++)
        {
            for (col = 0; col < D.cols; col++)
            {
                mmOctetMatrix_A(&D, row, col) = 0;
            }
        }

        code = mmRaptorqPrecode_MatrixIntermediate1(precode, &A, &D, &enc->symbolmat);
        if (MM_FALSE == code)
        {
            code = MM_FALSE;
            break;
        }

        code = MM_TRUE;
    } while (0);

    mmOctetMatrix_Destroy(&A);
    mmOctetMatrix_Destroy(&D);

    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecodeBlock(
    struct mmRaptorqContext* p,
    struct mmRaptorqIO* io,
    mmUInt8_t sbn,
    mmUInt64_t* decode_length)
{
    int code = MM_FALSE;

    int success = MM_FALSE;

    mmUInt16_t row = 0, col = 0;
    mmUInt16_t max_esi = 0;

    struct mmRaptorqSourceBlock blk;

    (*decode_length) = 0;

    do
    {
        struct mmRaptorqDecoderCore* dec = mmRaptorqContext_BlockDecoder(p, sbn);
        if (dec == NULL)
        {
            code = MM_FALSE;
            break;
        }
        if (mmBitset_Get(&p->mask, sbn))
        {
            success = mmRaptorqPrecode_MatrixDecode(&dec->precode, &dec->symbolmat, &dec->repair_bin, &dec->mask);
            if (!success)
            {
                code = MM_FALSE;
                break;
            }
            mmBitset_Set(&p->mask, sbn, 0);
        }

        max_esi = dec->symbolmat.rows;
        mmRaptorqContext_SourceBlock(p, sbn, dec->symbol_size, &blk);
        for (; row < max_esi; row++)
        {
            mmUInt16_t i = 0;
            col = 0;
            for (i = 0; i < dec->symbol_size;)
            {
                size_t offset = mmRaptorqSourceBlock_GetSymbolOffset(&blk, i, max_esi, row);
                mmUInt16_t sublen = (i < blk.part_tot) ? blk.part.IL : blk.part.IS;
                mmUInt16_t stride = sublen * p->scheme.Al;

                i += sublen;

                if ((*(io->Seek))(io, (long)offset))
                {
                    mmUInt16_t len = stride;
                    if (offset >= p->common.F)
                    {
                        continue;
                    }
                    if ((offset + stride) >= p->common.F)
                    {
                        len = (int)(p->common.F - offset);
                    }

                    (*decode_length) += (*(io->Write))(io, mmOctetMatrix_R(&dec->symbolmat, row) + col, len);

                    col += stride;
                }
            }
        }
        code = MM_TRUE;
    } while (0);

    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecoderAssemblySymbolsBuffer(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    mmUInt16_t* symbol_size)
{
    return mmRaptorqContext_DecoderAssemblySymbols(p, data, esi, sbn, symbol_size);
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncoderGenerateSymbolsBuffer(
    struct mmRaptorqContext* p,
    mmUInt8_t* data,
    mmUInt32_t esi,
    mmUInt8_t sbn,
    struct mmByteBuffer* byte_buffer,
    mmUInt16_t* symbol_size)
{
    int code = MM_FALSE;

    (*symbol_size) = 0;

    do
    {
        struct mmRaptorqEncoderCore* enc = NULL;

        enc = mmRaptorqContext_BlockEncoder(p, sbn);
        if (enc == NULL)
        {
            code = MM_FALSE;
            break;
        }

        if (esi < enc->num_symbols)
        {
            mmUInt16_t i = 0;
            struct mmRaptorqSourceBlock blk;
            mmRaptorqContext_SourceBlock(p, sbn, enc->symbol_size, &blk);
            for (i = 0; i < enc->symbol_size;)
            {
                size_t offset = mmRaptorqSourceBlock_GetSymbolOffset(&blk, i, enc->num_symbols, esi);
                mmUInt16_t sublen = (i < blk.part_tot) ? blk.part.IL : blk.part.IS;
                mmUInt16_t stride = sublen * p->scheme.Al;
                size_t boundary = 0;
                size_t got = 0;

                i += sublen;

                boundary = offset + stride;
                boundary = boundary < byte_buffer->length ? boundary : byte_buffer->length;
                got = boundary - offset;

                mmMemcpy(data, byte_buffer->buffer + byte_buffer->offset + offset, got);
                (*symbol_size) += (mmUInt16_t)got;

                mmMemset(data + got, 0, stride - got);
                (*symbol_size) += (mmUInt16_t)(stride - got);
            }
        }
        else
        {
            // esi is for repair symbol
            struct mmRaptorqPrecode* precode = &enc->precode;
            if (enc->symbolmat.rows == 0)
            {
                mmUInt64_t encode_length = 0;
                int generated = mmRaptorqContext_EncodeBlockBuffer(p, byte_buffer, sbn, &encode_length);
                if (!generated)
                {
                    code = MM_FALSE;
                    break;
                }
            }
            {
                mmUInt8_t* octet = NULL;
                mmUInt16_t i = 0;
                int byte = 0;

                struct mmOctetMatrix t_G;
                mmUInt32_t isi = esi + (precode->Kp - enc->num_symbols);

                mmOctetMatrix_Init(&t_G);
                mmRaptorqPrecode_MatrixEncode(precode, &enc->symbolmat, isi, &t_G);
                octet = mmOctetMatrix_P(&t_G);
                for (i = 0; i < enc->symbol_size; i++)
                {
                    for (byte = 0; byte < p->scheme.Al; byte++)
                    {
                        *(data++) = (octet == NULL) ? 0 : *(octet++);
                        (*symbol_size)++;
                    }
                }
                mmOctetMatrix_Destroy(&t_G);
            }
        }
        if ((*symbol_size) != mmRaptorqContext_SymbolSize(p))
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);
    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_EncodeBlockBuffer(
    struct mmRaptorqContext* p,
    struct mmByteBuffer* byte_buffer,
    mmUInt8_t sbn,
    mmUInt64_t* encode_length)
{
    int code = MM_FALSE;

    struct mmRaptorqEncoderCore* enc = NULL;
    struct mmRaptorqPrecode* precode = NULL;

    mmUInt16_t row = 0, col = 0;

    struct mmOctetMatrix A;
    struct mmOctetMatrix D;

    struct mmRaptorqSourceBlock blk;

    (*encode_length) = 0;

    mmOctetMatrix_Init(&A);
    mmOctetMatrix_Init(&D);

    do
    {
        enc = mmRaptorqContext_BlockEncoder(p, sbn);

        if (enc == NULL)
        {
            code = MM_FALSE;
            break;
        }

        if (enc->symbolmat.rows > 0)
        {
            code = MM_TRUE;
            break;
        }

        precode = &enc->precode;
        mmRaptorqPrecode_MatrixGenerate(precode, &A, 0);

        mmOctetMatrix_Resize(&D, precode->Kp + precode->S + precode->H, enc->symbol_size * p->scheme.Al);

        for (row = 0; row < precode->S + precode->H; row++)
        {
            for (col = 0; col < D.cols; col++)
            {
                mmOctetMatrix_A(&D, row, col) = 0;
            }
        }

        mmRaptorqContext_SourceBlock(p, sbn, enc->symbol_size, &blk);
        for (; row < precode->S + precode->H + enc->num_symbols; row++)
        {
            mmUInt16_t i = 0;
            mmUInt32_t symbol_id = row - (precode->S + precode->H);
            col = 0;
            for (i = 0; i < enc->symbol_size;)
            {
                size_t byte = 0;
                size_t offset = mmRaptorqSourceBlock_GetSymbolOffset(&blk, i, enc->num_symbols, symbol_id);
                mmUInt16_t sublen = (i < blk.part_tot) ? blk.part.IL : blk.part.IS;
                mmUInt16_t stride = sublen * p->scheme.Al;
                size_t boundary = 0;
                size_t got = 0;

                i += sublen;

                boundary = offset + stride;
                boundary = boundary < byte_buffer->length ? boundary : byte_buffer->length;
                got = boundary - offset;

                (*encode_length) += got;

                for (byte = 0; byte < got; byte++)
                {
                    mmOctetMatrix_A(&D, row, col++) = byte_buffer->buffer[byte_buffer->offset + offset + byte];
                }
                for (byte = got; byte < stride; byte++)
                {
                    mmOctetMatrix_A(&D, row, col++) = 0;
                }
            }
        }

        for (; row < D.rows; row++)
        {
            for (col = 0; col < D.cols; col++)
            {
                mmOctetMatrix_A(&D, row, col) = 0;
            }
        }

        code = mmRaptorqPrecode_MatrixIntermediate1(precode, &A, &D, &enc->symbolmat);
        if (MM_FALSE == code)
        {
            code = MM_FALSE;
            break;
        }

        code = MM_TRUE;
    } while (0);

    mmOctetMatrix_Destroy(&A);
    mmOctetMatrix_Destroy(&D);

    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqContext_DecodeBlockBuffer(
    struct mmRaptorqContext* p,
    struct mmByteBuffer* byte_buffer,
    mmUInt8_t sbn,
    mmUInt64_t* decode_length)
{
    int code = MM_FALSE;

    int success = MM_FALSE;

    mmUInt16_t row = 0, col = 0;
    mmUInt16_t max_esi = 0;

    struct mmRaptorqSourceBlock blk;

    (*decode_length) = 0;

    do
    {
        struct mmRaptorqDecoderCore* dec = mmRaptorqContext_BlockDecoder(p, sbn);
        if (dec == NULL)
        {
            code = MM_FALSE;
            break;
        }
        if (mmBitset_Get(&p->mask, sbn))
        {
            success = mmRaptorqPrecode_MatrixDecode(&dec->precode, &dec->symbolmat, &dec->repair_bin, &dec->mask);
            if (!success)
            {
                code = MM_FALSE;
                break;
            }
            mmBitset_Set(&p->mask, sbn, 0);
        }

        max_esi = dec->symbolmat.rows;
        mmRaptorqContext_SourceBlock(p, sbn, dec->symbol_size, &blk);
        for (; row < max_esi; row++)
        {
            mmUInt16_t i = 0;
            col = 0;
            for (i = 0; i < dec->symbol_size;)
            {
                size_t offset = mmRaptorqSourceBlock_GetSymbolOffset(&blk, i, max_esi, row);
                mmUInt16_t sublen = (i < blk.part_tot) ? blk.part.IL : blk.part.IS;
                mmUInt16_t stride = sublen * p->scheme.Al;

                i += sublen;

                if (offset < byte_buffer->length)
                {
                    mmUInt16_t len = stride;
                    if (offset >= p->common.F)
                    {
                        continue;
                    }
                    if ((offset + stride) >= p->common.F)
                    {
                        len = (int)(p->common.F - offset);
                    }
                    mmMemcpy(byte_buffer->buffer + byte_buffer->offset + offset, mmOctetMatrix_R(&dec->symbolmat, row) + col, len);

                    (*decode_length) += len;

                    col += stride;
                }
            }
        }
        code = MM_TRUE;
    } while (0);

    return code;
}
MM_EXPORT_RAPTORQ int mmRaptorqContext_AssemblySymbolComplete(struct mmRaptorqContext* p, mmUInt8_t sbn, mmUInt32_t overhead)
{
    int code = MM_FALSE;

    do
    {
        struct mmRaptorqDecoderCore* dec = NULL;
        mmUInt16_t num_esi = 0;
        mmUInt16_t bit_esi = 0;

        dec = mmRaptorqContext_BlockDecoder(p, sbn);
        if (dec == NULL)
        {
            code = MM_FALSE;
            break;
        }

        if (mmBitset_Count0(&dec->mask, dec->num_symbols) == 0)
        {
            // no gaps! no repair needed.
            code = MM_TRUE;
            break;
        }

        num_esi = mmRaptorqContext_BlockSymbols(p, sbn);
        bit_esi = (mmUInt16_t)mmBitset_Count(&dec->mask);
        if (bit_esi < num_esi + overhead)
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);
    return code;
}
MM_EXPORT_RAPTORQ int mmRaptorqContext_AssemblySourceComplete(struct mmRaptorqContext* p, mmUInt32_t overhead)
{
    int code = MM_FALSE;

    do
    {
        mmUInt8_t num_sbn = 0;
        int success = MM_TRUE;
        int sbn = 0;

        num_sbn = mmRaptorqContext_Blocks(p);
        if (0 == num_sbn)
        {
            code = MM_FALSE;
            break;
        }
        if (0 == mmBitset_Count1(&p->mask, num_sbn))
        {
            code = MM_TRUE;
            break;
        }
        sbn = (int)mmBitset_FindFirst(&p->mask);
        while (MM_BITSET_NPOS != sbn)
        {
            success = mmRaptorqContext_AssemblySymbolComplete(p, sbn, overhead);
            if (MM_FALSE == success)
            {
                success = MM_FALSE;
                break;
            }
            sbn = (int)mmBitset_FindNext(&p->mask, sbn);
        }

        if (MM_FALSE == success)
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);
    return code;
}
MM_EXPORT_RAPTORQ int mmRaptorqContext_AssemblySymbolValuable(struct mmRaptorqContext* p, mmUInt32_t esi, mmUInt8_t sbn)
{
    int code = MM_FALSE;

    do
    {
        struct mmRaptorqDecoderCore* dec = NULL;

        if (sbn >= mmBitset_Size(&p->mask))
        {
            // the sbn is invalid.
            code = MM_FALSE;
            break;
        }

        if (0 == mmBitset_Get(&p->mask, sbn))
        {
            // the sbn is already complete.
            code = MM_FALSE;
            break;
        }

        dec = mmRaptorqContext_BlockDecoder(p, sbn);
        if (dec == NULL)
        {
            code = MM_FALSE;
            break;
        }

        if (esi < dec->mask.n && 1 == mmBitset_Get(&dec->mask, esi))
        {
            // already got this esi.
            code = MM_FALSE;
            break;
        }

        code = MM_TRUE;
    } while (0);
    return code;
}
MM_EXPORT_RAPTORQ int mmRaptorqContext_Encode(struct mmRaptorqContext* p, struct mmByteBuffer* byte_buffer)
{
    int code = MM_FALSE;

    do
    {
        struct mmRaptorqDecoderCore* dec = NULL;
        mmUInt8_t num_sbn = 0;
        mmUInt64_t encode_length_total = 0;
        mmUInt64_t encode_length = 0;
        mmUInt64_t transfer_length = 0;
        int success = MM_TRUE;
        int sbn = 0;

        num_sbn = mmRaptorqContext_Blocks(p);
        if (0 == num_sbn)
        {
            code = MM_FALSE;
            break;
        }
        for (sbn = 0; sbn < num_sbn; sbn++)
        {
            dec = mmRaptorqContext_BlockDecoder(p, sbn);
            if (dec == NULL)
            {
                success = MM_FALSE;
                break;
            }
            success = mmRaptorqContext_EncodeBlockBuffer(p, byte_buffer, sbn, &encode_length);
            if (MM_FALSE == success)
            {
                success = MM_FALSE;
                break;
            }
            encode_length_total += encode_length;
        }
        if (MM_FALSE == success)
        {
            code = MM_FALSE;
            break;
        }
        transfer_length = mmRaptorqContext_TransferLength(p);
        if (transfer_length != encode_length_total)
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);
    return code;
}
MM_EXPORT_RAPTORQ int mmRaptorqContext_Decode(struct mmRaptorqContext* p, struct mmByteBuffer* byte_buffer)
{
    int code = MM_FALSE;

    do
    {
        struct mmRaptorqDecoderCore* dec = NULL;
        mmUInt8_t num_sbn = 0;
        mmUInt64_t decode_length_total = 0;
        mmUInt64_t decode_length = 0;
        mmUInt64_t transfer_length = 0;
        int success = MM_TRUE;
        int sbn = 0;

        num_sbn = mmRaptorqContext_Blocks(p);
        if (0 == num_sbn)
        {
            code = MM_FALSE;
            break;
        }
        for (sbn = 0; sbn < num_sbn; sbn++)
        {
            dec = mmRaptorqContext_BlockDecoder(p, sbn);
            if (dec == NULL)
            {
                success = MM_FALSE;
                break;
            }
            success = mmRaptorqContext_DecodeBlockBuffer(p, byte_buffer, sbn, &decode_length);
            if (MM_FALSE == success)
            {
                success = MM_FALSE;
                break;
            }
            decode_length_total += decode_length;
        }
        if (MM_FALSE == success)
        {
            code = MM_FALSE;
            break;
        }
        transfer_length = mmRaptorqContext_TransferLength(p);
        if (transfer_length != decode_length_total)
        {
            code = MM_FALSE;
            break;
        }
        code = MM_TRUE;
    } while (0);
    return code;
}
