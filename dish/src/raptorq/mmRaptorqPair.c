/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqPair.h"
#include "core/mmAlloc.h"
#include "core/mmBit.h"
#include "core/mmInteger.h"

MM_EXPORT_RAPTORQ void mmRaptorqUInt16Pair_Assign(struct mmRaptorqUInt16Pair* p, mmUInt16_t f, mmUInt16_t s)
{
    p->f = f;
    p->s = s;
}
MM_EXPORT_RAPTORQ void mmRaptorqTrackingPair_Swap(struct mmRaptorqTrackingPair* u, struct mmRaptorqTrackingPair* v)
{
    mmInteger_IntSwap(&u->is_hdpc, &v->is_hdpc);
    mmInteger_SizeSwap(&u->row_degree, &v->row_degree);
}

