/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqPrecode.h"
#include "mmRaptorqContext.h"
#include "mmRaptorqMath.h"
#include "mmRaptorqDegree.h"
#include "mmRaptorqRandom.h"
#include "mmRaptorqChooser.h"
#include "mmRaptorqRepair.h"
#include "mmRaptorqGraph.h"
#include "mmRaptorqSystematic.h"
#include "mmRaptorqOctet.h"
#include "mmRaptorqOctetBlas.h"

#include "container/mmBitset.h"

#include "core/mmInteger.h"

static void PrecodeMatrix_InitLDPC1(struct mmOctetMatrix* A, mmUInt16_t S, mmUInt16_t B)
{
    // The first LDPC1 submatrix is a SxB matrix of SxS submatrixes
    // (the last submatrix can have less than S columns)
    // each submatrix is full zero, with some exceptions:
    // in the first column positions "0", "i + 1" and "2 * (i+1)" are set
    // to 1. All next columns are all downshifts of the first.
    // which makes each submatrix a circulant matrix)

    // You won't find this directly on the rfc, but you can find it in the book:
    //  Raptor Codes Foundations and Trends in Communications
    //      and Information Theory

    mmUInt16_t row, col;

    assert(S <= A->rows && "S <= A->rows is not invalid, out of range.");
    assert(B <= A->cols && "B <= A->cols is not invalid, out of range..");

    for (row = 0; row < S; row++)
    {
        for (col = 0; col < B; col++)
        {
            mmUInt16_t submtx = col / S;
            if ((row == (col % S)) ||                   /* column 0 & downshifts */
                (row == (col + submtx + 1) % S) ||      /* i + 1 & downshifts    */
                (row == (col + 2 * (submtx + 1)) % S))  /* 2* (i+1) & dshift     */
            {
                mmOctetMatrix_A(A, row, col) = 1;
            }
        }
    }
}

static void PrecodeMatrix_InitLDPC2(struct mmOctetMatrix* A, mmUInt16_t skip, mmUInt16_t rows, mmUInt16_t cols)
{
    // this submatrix has two consecutive "1" in the first row, first two
    // colums, and then every other row is the previous right shifted.

    // You won't find this easily on the rfc, but you can see this in the book:
    //  Raptor Codes Foundations and Trends in Communications
    //  and Information Theory

    mmUInt16_t row, col;

    assert(rows <= A->rows && "rows <= A->rows is not invalid, out of range..");
    assert(cols <= A->cols && "cols <= A->cols is not invalid, out of range..");

    for (row = 0; row < rows; row++)
    {
        mmUInt16_t start = row % cols;
        for (col = 0; col < cols; col++)
        {
            mmUInt8_t val = (col == start || col == (start + 1) % cols) > 0;
            mmOctetMatrix_A(A, row, skip + col) = val;
        }
    }
}

static void PrecodeMatrix_AddIdentity(struct mmOctetMatrix* A, mmUInt16_t size, mmUInt16_t skip_row, mmUInt16_t skip_col)
{
    mmUInt16_t diag = 0;
    for (diag = 0; diag < size; diag++)
    {
        mmOctetMatrix_A(A, skip_row + diag, skip_col + diag) = 1;
    }
}

static void PrecodeMatrix_MakeMT(struct mmOctetMatrix* MT, mmUInt16_t rows, mmUInt16_t cols)
{
    // rfc 6330, pg 24

    mmUInt16_t row, col;

    mmOctetMatrix_Resize(MT, rows, cols);

    for (row = 0; row < MT->rows; row++)
    {
        for (col = 0; col < MT->cols - 1; col++)
        {
            mmUInt32_t r = mmRaptorq_Rand(col + 1, 6, MT->rows);
            if ((row == r) ||
                (row == (r + mmRaptorq_Rand(col + 1, 7, MT->rows - 1) + 1) % MT->rows))
            {
                mmOctetMatrix_A(MT, row, col) = 1;
            }
            else
            {
                mmOctetMatrix_A(MT, row, col) = 0;
            }
        }
        // last column: alpha ^^ i, as in rfc6330
        mmOctetMatrix_A(MT, row, col) = OCT_EXP[row];
    }
}

static void PrecodeMatrix_MakeGAMMA(struct mmOctetMatrix* GAMMA, mmUInt16_t dim)
{
    // rfc 6330, pg 24

    mmUInt16_t row, col;

    mmOctetMatrix_Resize(GAMMA, dim, dim);

    for (row = 0; row < GAMMA->rows; row++)
    {
        for (col = 0; col <= row; col++)
        {
            // alpha ^^ (i-j), as in rfc6330, pg24
            // end of Section 5.7.2: alpha^^i == oct_exp(i)
            //
            // rfc only says "i-j", while the ^^ op. is defined only if
            // the exponent is < 255.
            // we could actually use oct_exp.size() (=>510) since oct_exp
            // actually contains the same values twice. We still use 255
            // so that our implementation is more similar to the other.

            mmOctetMatrix_A(GAMMA, row, col) = OCT_EXP[(row - col) % OCT_EXP_SIZE];
        }
        for (; col < GAMMA->cols; col++)
        {
            mmOctetMatrix_A(GAMMA, row, col) = 0;
        }
    }
}

static void PrecodeMatrix_InitHDPC(struct mmRaptorqPrecode* p, struct mmOctetMatrix* A)
{
    // rfc 6330, pg 25

    size_t row, col;

    mmUInt16_t m = p->H;
    mmUInt16_t n = p->Kp + p->S;

    if (m != 0 && n != 0)
    {
        struct mmOctetMatrix MT;
        struct mmOctetMatrix GAMMA;
        struct mmOctetMatrix MTxGAMMA;

        mmOctetMatrix_Init(&MT);
        mmOctetMatrix_Init(&GAMMA);
        mmOctetMatrix_Init(&MTxGAMMA);

        PrecodeMatrix_MakeMT(&MT, m, n);
        PrecodeMatrix_MakeGAMMA(&GAMMA, n);

        mmOctetMatrix_Resize(&MTxGAMMA, MT.rows, GAMMA.cols);

        mmOctet_Gemm(mmOctetMatrix_P(&MT), mmOctetMatrix_P(&GAMMA), mmOctetMatrix_P(&MTxGAMMA), MT.rows, MT.cols, GAMMA.cols);

        for (col = 0; col < GAMMA.cols; col++)
        {
            for (row = 0; row < MT.rows; row++)
            {
                mmOctetMatrix_A(A, p->S + row, col) = mmOctetMatrix_A(&MTxGAMMA, row, col);
            }
        }

        mmOctetMatrix_Destroy(&MT);
        mmOctetMatrix_Destroy(&GAMMA);
        mmOctetMatrix_Destroy(&MTxGAMMA);
    }
}

static void PrecodeMatrix_AddGENC(struct mmRaptorqPrecode* p, struct mmOctetMatrix* A)
{
    // rfc 6330, pg 26

    mmUInt32_t isi = 0;
    mmUInt16_t idx = 0;
    mmUInt16_t row = 0;

    struct mmVectorU16 idxs;

    mmVectorU16_Init(&idxs);

    for (row = p->S + p->H; row < p->L; row++)
    {
        isi = (row - p->S) - p->H;
        mmRaptorqPrecode_Idxs(p, isi, &idxs);
        for (idx = 0; idx < idxs.size; idx++)
        {
            mmOctetMatrix_A(A, row, mmVectorU16_GetIndex(&idxs, idx)) = 1;
        }
        mmVectorU16_Clear(&idxs);
    }

    mmVectorU16_Destroy(&idxs);
}

static void
Decode_Phase0(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* A,
    struct mmBitset* mask,
    struct mmRaptorqRepairVector* repair_bin,
    mmUInt16_t num_symbols,
    mmUInt16_t overhead)
{
    // D was built as follows:
    // - non-repair esi in their place
    // - for each hole in non-repair esi, put the *first available* repair esi
    //      in its place. they are already ordered anyway
    // - compact remaining repair esis


    // substitute missing symbols in A with appropriate repair line.
    // we substituted some symbols with repair ones (rfc 6330, phase1, pg35),
    // so we need to fix the corresponding rows in A, to say that a
    // repair symbol comes from a set of other symbols.

    int idx = 0;

    mmUInt16_t gap = 0;
    mmUInt16_t row = 0;
    mmUInt16_t col = 0;

    mmUInt16_t rep_row = 0;

    mmUInt16_t num_gaps = 0;
    mmUInt16_t rep_idx = 0;

    mmUInt32_t isi = 0;

    size_t padding = 0;

    struct mmVectorU16 idxs;

    padding = p->Kp - num_symbols;

    mmVectorU16_Init(&idxs);

    num_gaps = (mmUInt16_t)mmBitset_Count0(mask, num_symbols);

    for (gap = 0; gap < p->L && num_gaps > 0; gap++)
    {
        if (mmBitset_Get(mask, gap))
        {
            continue;
        }
        row = gap + p->H + p->S;
        for (col = 0; col < A->cols; col++)
        {
            mmOctetMatrix_A(A, row, col) = 0;
        }
        // now hole_from is the esi hole, and hole_to is our repair sym.
        // put the repair dependancy in the hole row
        isi = mmRaptorqRepairVector_Esi(repair_bin, rep_idx++) + (mmUInt32_t)padding;
        mmRaptorqPrecode_Idxs(p, isi, &idxs);
        for (idx = 0; idx < idxs.size; idx++)
        {
            mmOctetMatrix_A(A, row, mmVectorU16_GetIndex(&idxs, idx)) = 1;
        }
        mmVectorU16_Clear(&idxs);
        num_gaps--;
    }

    // we put the repair symbols in the right places,
    // but we still need to do the same modifications to A also for repair
    // symbols. And those have been compacted.

    rep_row = (mmUInt16_t)(A->rows - overhead);
    for (; rep_row < A->rows; rep_row++)
    {
        // erease the line, mark the dependencies of the repair symbol.
        for (col = 0; col < A->cols; col++)
        {
            mmOctetMatrix_A(A, rep_row, col) = 0;
        }
        isi = mmRaptorqRepairVector_Esi(repair_bin, rep_idx++) + (mmUInt32_t)padding;
        mmRaptorqPrecode_Idxs(p, isi, &idxs);
        for (idx = 0; idx < idxs.size; idx++)
        {
            mmOctetMatrix_A(A, rep_row, mmVectorU16_GetIndex(&idxs, idx)) = 1;
        }
        mmVectorU16_Clear(&idxs);
    }

    mmVectorU16_Destroy(&idxs);
}

static int
Decode_Phase1(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* X,
    struct mmOctetMatrix* D,
    struct mmVectorU16* c,
    uint16_t *i_val,
    uint16_t *u_val)
{
    // rfc6330, page 33

    int code = MM_FALSE;

    mmUInt16_t row = 0;
    mmUInt16_t col = 0;

    mmUInt16_t i = 0;
    mmUInt16_t u = p->P;

    size_t row_degree = 0;

    // optimization: r_rows tracks the rows that can be chosen, and if the row
    // is added to the graph, track also the id of one of the nodes with "1",
    // so that it will be easy to verify it. The row represents an edge
    // between nodes (1) of a maximum component (see rfc 6330, pg 33-34)

    struct mmRaptorqChooser ch;

    struct mmRaptorqGraph G;

    mmRaptorqChooser_Init(&ch);
    mmRaptorqChooser_SetLength(&ch, A->rows);

    mmRaptorqGraph_Init(&G);

    // track hdpc rows and original degree of each row
    for (row = 0; row < A->rows; row++)
    {
        int is_hdpc = (row >= p->S && row < (p->S + p->H));
        row_degree = 0;
        for (col = 0; col < A->cols - u; col++)
        {
            row_degree += (mmUInt8_t)(mmOctetMatrix_A(A, row, col));
        }
        mmRaptorqChooser_PushTrackingPair(&ch, row_degree, is_hdpc);
    }

    do
    {
        while (i + u < p->L)
        {
            mmUInt16_t sub_rows = A->rows - i;
            mmUInt16_t sub_cols = A->cols - i - u;
            mmUInt16_t chosen, non_zero;

            mmUInt16_t row = 0;
            mmUInt16_t col = 0;
            mmUInt16_t swap = 0;

            // reuse G.
            mmRaptorqGraph_SetLength(&G, sub_cols);

            // search for minium "r" (number of nonzero elements in row)
            // build graph, get minimum non_zero and track rows that
            // will be needed later
            non_zero = mmRaptorqChooser_NonZero(&ch, A, &G, i, sub_rows, sub_cols);
            if (non_zero == sub_cols + 1)
            {
                // some error is here.
                code = MM_FALSE;
                break;
            }
            chosen = mmRaptorqChooser_Pick(&ch, &G, i, sub_rows, non_zero);
            // swap chosen row and first V row in A (not just in V)
            if (chosen != 0)
            {
                struct mmRaptorqTrackingPair* u0 = NULL;
                struct mmRaptorqTrackingPair* u1 = NULL;

                mmOctet_SwapRow(mmOctetMatrix_P(A), i, chosen + i, A->cols);
                mmOctet_SwapRow(mmOctetMatrix_P(X), i, chosen + i, X->cols);
                mmOctet_SwapRow(mmOctetMatrix_P(D), i, chosen + i, D->cols);

                u0 = (struct mmRaptorqTrackingPair*)mmVectorValue_GetIndex(&ch.tracking, i);
                u1 = (struct mmRaptorqTrackingPair*)mmVectorValue_GetIndex(&ch.tracking, chosen + i);
                mmRaptorqTrackingPair_Swap(u0, u1);
            }

            // column swap in A. looking at the first V row,
            // the first column must be nonzero, and the other non-zero must be
            // put to the last columns of V.
            if (mmOctetMatrix_A(A, i, i) == 0)
            {
                mmUInt16_t idx = 1;
                for (; idx < sub_cols; idx++)
                {
                    if (mmOctetMatrix_A(A, i, idx + i) != 0)
                    {
                        break;
                    }
                }

                mmOctet_SwapCol(mmOctetMatrix_P(A), i, i + idx, A->rows, A->cols);
                mmOctet_SwapCol(mmOctetMatrix_P(X), i, i + idx, X->rows, X->cols);

                // rfc6330, pg32
                mmInteger_UInt16Swap(
                    mmVectorU16_GetIndexReference(c, i),
                    mmVectorU16_GetIndexReference(c, i + idx));
            }

            col = sub_cols - 1;

            // at most we swapped V(0,0)
            swap = 1;

            // put all the non-zero cols to the last columns.
            for (; col > sub_cols - non_zero; col--)
            {
                if (mmOctetMatrix_A(A, i, col + i) != 0)
                {
                    continue;
                }
                while (swap < col && mmOctetMatrix_A(A, i, swap + i) == 0)
                {
                    swap++;
                }

                if (swap >= col)
                {
                    // line full of zeros, nothing to swap
                    break;
                }

                // now V(0, col) == 0 and V(0, swap != 0. swap them
                mmOctet_SwapCol(mmOctetMatrix_P(A), col + i, swap + i, A->rows, A->cols);
                mmOctet_SwapCol(mmOctetMatrix_P(X), col + i, swap + i, X->rows, X->cols);

                //rfc6330, pg32
                mmInteger_UInt16Swap(
                    mmVectorU16_GetIndexReference(c, col + i),
                    mmVectorU16_GetIndexReference(c, swap + i));
            }

            // now add a multiple of the row V(0) to the other rows of *A* so that
            // the other rows of *V* have a zero first column.
            for (row = 1; row < sub_rows; row++)
            {
                if (mmOctetMatrix_A(A, row + i, i) != 0)
                {
                    mmUInt8_t mnum = mmOctetMatrix_A(A, row + i, i);
                    mmUInt8_t mden = mmOctetMatrix_A(A, i, i);
                    mmUInt8_t multiple = (mnum > 0 && mden > 0) ? mmOctet_Div(mnum, mden) : 0;
                    if (multiple == 0)
                    {
                        continue;
                    }
                    mmOctet_Axpy(mmOctetMatrix_P(A), mmOctetMatrix_P(A), row + i, i, A->cols, multiple);
                    mmOctet_Axpy(mmOctetMatrix_P(D), mmOctetMatrix_P(D), row + i, i, D->cols, multiple);
                }
            }

            // finally increment i by 1, u by (non_zero - 1) and repeat.
            i++;
            u += non_zero - 1;
        }

        code = MM_TRUE;
    } while (0);

    mmRaptorqChooser_Destroy(&ch);
    mmRaptorqGraph_Destroy(&G);

    if (code == MM_TRUE)
    {
        *i_val = i;
        *u_val = u;
    }
    else
    {
        *i_val = 0;
        *u_val = 0;
    }

    return code;
}

static int
Decode_Phase2(
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    mmUInt16_t i,
    mmUInt16_t u,
    mmUInt16_t L)
{
    // rfc 6330, pg 35
    int code = MM_FALSE;

    mmUInt8_t multiple = 0;

    mmUInt16_t row = 0;
    mmUInt16_t del_row = 0;

    // U_Lower parameters (u x u):
    mmUInt16_t row_start = i, row_end = A->rows;
    mmUInt16_t col_start = A->cols - u;

    code = MM_TRUE;

    // try to bring U_Lower to Identity with gaussian elimination.
    // remember that all row swaps affect A as well, not just U_Lower

    for (row = row_start; row < row_end; row++)
    {
        // make sure the considered row has nonzero on the diagonal
        mmUInt16_t row_nonzero = row;
        mmUInt16_t diag = col_start + (row - row_start);
        if (diag >= L)
        {
            break;
        }
        for (; row_nonzero < row_end; row_nonzero++)
        {
            if (mmOctetMatrix_A(A, row_nonzero, diag) != 0)
            {
                break;
            }
        }
        if (row != row_nonzero)
        {
            mmOctet_SwapRow(mmOctetMatrix_P(A), row, row_nonzero, A->cols);
            mmOctet_SwapRow(mmOctetMatrix_P(D), row, row_nonzero, D->cols);
        }

        // U_Lower (row, row) != 0. make it 1.
        if (mmOctetMatrix_A(A, row, diag) > 1)
        {
            multiple = mmOctetMatrix_A(A, row, diag);
            mmOctet_Scal(mmOctetMatrix_P(A), row, A->cols, mmOctet_Div(1, multiple));
            mmOctet_Scal(mmOctetMatrix_P(D), row, D->cols, mmOctet_Div(1, multiple));
        }

        // make U_Lower and identity up to row
        for (del_row = row_start; del_row < row_end; del_row++)
        {
            if (del_row == row)
            {
                continue;
            }

            // subtract row "row" to "del_row" enough times to make
            // row "del_row" start with zero. but row "row" now starts
            // with "1", so this is easy.
            multiple = mmOctetMatrix_A(A, del_row, diag);
            if (multiple == 0)
            {
                continue;
            }
            mmOctet_Axpy(mmOctetMatrix_P(A), mmOctetMatrix_P(A), del_row, row, A->cols, multiple);
            mmOctet_Axpy(mmOctetMatrix_P(D), mmOctetMatrix_P(D), del_row, row, D->cols, multiple);
        }
    }
    // diagonal matrix must all equal 1.
    // A matrix row_start is satisfied, reverse order faster.
    row = 0 == A->cols ? 0 : A->cols - 1;
    for (; row > 0; row--)
    {
        if (1 != mmOctetMatrix_A(A, row, row))
        {
            // A matrix is not solvable.
            code = MM_FALSE;
            break;
        }
    }

    // A should be resized to LxL.
    // we don't really care, as we should not gain that much.
    // A.conservativeResize (params.L, params.L);
    return code;
}

static void
Decode_Phase3(
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* X,
    struct mmOctetMatrix* D,
    mmUInt16_t i)
{
    // rfc 6330, pg 35:
    //  To this end, the matrix X is
    //  multiplied with the submatrix of A consisting of the first i rows of
    //  A. After this operation, the submatrix of A consisting of the
    //  intersection of the first i rows and columns equals to X, whereas the
    //  matrix U_upper is transformed to a sparse form.

    mmUInt16_t row = 0;
    mmUInt16_t col = 0;

    struct mmOctetMatrix Xb;
    struct mmOctetMatrix Ab;
    struct mmOctetMatrix Db;

    mmOctetMatrix_Init(&Xb);
    mmOctetMatrix_Init(&Ab);
    mmOctetMatrix_Init(&Db);

    mmOctetMatrix_Resize(&Xb, i, i);

    for (row = 0; row < i; row++)
    {
        for (col = 0; col < i; col++)
        {
            mmOctetMatrix_A(&Xb, row, col) = mmOctetMatrix_A(X, row, col);
        }
    }

    mmOctetMatrix_Copy(&Ab, A);
    mmOctetMatrix_Copy(&Db, D);

    mmOctet_Gemm(mmOctetMatrix_P(&Xb), mmOctetMatrix_P(&Ab), mmOctetMatrix_P(A), i, i, Ab.cols);
    mmOctet_Gemm(mmOctetMatrix_P(&Xb), mmOctetMatrix_P(&Db), mmOctetMatrix_P(D), i, i, Db.cols);

    mmOctetMatrix_Destroy(&Ab);
    mmOctetMatrix_Destroy(&Xb);
    mmOctetMatrix_Destroy(&Db);
}

static void
Decode_Phase4(
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    mmUInt16_t i,
    mmUInt16_t u)
{
    // rfc 6330, pg 35:
    // For each of the first i rows of U_upper, do the following: if the row
    // has a nonzero entry at position j, and if the value of that nonzero
    // entry is b, then add to this row b times row j of I_u

    // basically: zero out U_upper. we still need to update D each time, though.

    mmUInt16_t row = 0;
    mmUInt16_t col = 0;

    mmUInt8_t multiple = 0;

    mmUInt16_t skip = A->cols - u;

    for (row = 0; row < i; row++)
    {
        for (col = 0; col < u; col++)
        {
            multiple = mmOctetMatrix_A(A, row, col + skip);
            if (multiple == 0)
            {
                continue;
            }
            // U_upper is never read again, so we can avoid some writes
            //U_upper (row, col) = 0;

            // "b times row j of I_u" => row "j" in U_lower.
            // aka: U_upper.rows() + j
            mmOctet_Axpy(mmOctetMatrix_P(D), mmOctetMatrix_P(D), row, i + col, D->cols, multiple);
        }
    }
}

static void
Decode_Phase5(
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    mmUInt16_t i)
{
    // rfc 6330, pg 36
    //
    // For j from 1 to i, perform the following operations:
    //
    // 1.  If A[j,j] is not one, then divide row j of A by A[j,j].
    //
    // 2.  For l from 1 to j-1, if A[j,l] is nonzero, then add A[j,l]
    //     multiplied with row l of A to row j of A.
    //

    mmUInt16_t j = 0;
    mmUInt16_t l = 0;

    mmUInt8_t multiple = 0;

    for (j = 0; j <= i; j++)
    {
        if (mmOctetMatrix_A(A, j, j) != 1)
        {
            // A(j, j) is actually never 0, by construction.
            multiple = mmOctetMatrix_A(A, j, j);
            //mmOctet_Scal(mmOctetMatrix_P(A), j, A->cols, mmOctet_Div(1, multiple));
            mmOctet_Scal(mmOctetMatrix_P(D), j, D->cols, mmOctet_Div(1, multiple));
        }
        for (l = 0; l < j; l++)
        {
            multiple = mmOctetMatrix_A(A, j, l);
            if (multiple == 0)
            {
                continue;
            }
            // this row of A is not read again, so we can avoid making
            // this ADD_MUL on A
            // A.row (j) += A.row (col) * multiple;
            mmOctet_Axpy(mmOctetMatrix_P(A), mmOctetMatrix_P(A), j, l, A->cols, multiple);
            mmOctet_Axpy(mmOctetMatrix_P(D), mmOctetMatrix_P(D), j, l, D->cols, multiple);
        }
    }
}

MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Init(struct mmRaptorqPrecode* p)
{
    p->index_Kp = 0;
    p->Kp = 0;
    p->S = 0;
    p->H = 0;
    p->W = 0;
    p->L = 0;
    p->P = 0;
    p->P1 = 0;
    p->U = 0;
    p->B = 0;
    p->J = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Destroy(struct mmRaptorqPrecode* p)
{
    p->index_Kp = 0;
    p->Kp = 0;
    p->S = 0;
    p->H = 0;
    p->W = 0;
    p->L = 0;
    p->P = 0;
    p->P1 = 0;
    p->U = 0;
    p->B = 0;
    p->J = 0;
}
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Reset(struct mmRaptorqPrecode* p)
{
    p->index_Kp = 0;
    p->Kp = 0;
    p->S = 0;
    p->H = 0;
    p->W = 0;
    p->L = 0;
    p->P = 0;
    p->P1 = 0;
    p->U = 0;
    p->B = 0;
    p->J = 0;
}

MM_EXPORT_RAPTORQ void mmRaptorqPrecode_SetSymbols(struct mmRaptorqPrecode* p, mmUInt16_t symbols)
{
    p->index_Kp = mmRaptorqSystematic_KIndexKp(symbols);

    p->Kp = mmRaptorqSystematic_K(p->index_Kp);

    p->J = mmRaptorqSystematic_J(p->index_Kp);
    p->S = mmRaptorqSystematic_S(p->index_Kp);
    p->H = mmRaptorqSystematic_H(p->index_Kp);
    p->W = mmRaptorqSystematic_W(p->index_Kp);

    p->L = p->Kp + p->S + p->H;
    p->P = p->L - p->W;
    p->U = p->P - p->H;
    p->B = p->W - p->S;

    p->P1 = mmRaptorq_UInt16NextPrime(p->P);
}
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Tuple(struct mmRaptorqPrecode* p, struct mmRaptorqTuple* tuple, mmUInt32_t X)
{
    static const mmUInt64_t pow_2_32 = 4294967296L;
    static const mmUInt32_t pow_2_20 = 1048576L;

    size_t A = 0;
    size_t B = 0;
    mmUInt32_t y = 0;
    mmUInt32_t v = 0;

    A = 53591 + p->J * 997;
    if (0 == A % 2) { A++; }
    B = 10267 * (p->J + 1);
    y = (mmUInt32_t)((B + X * A) % pow_2_32);  // 2^^32
    v = mmRaptorq_Rand(y, 0, pow_2_20);  // 2^^20

    tuple->d = mmRaptorqDegree_Deg(v, p->W);
    tuple->a = 1 + mmRaptorq_Rand(y, 1, p->W - 1);
    tuple->b = mmRaptorq_Rand(y, 2, p->W);

    if (tuple->d < 4) { tuple->d1 = 2 + (mmUInt16_t)mmRaptorq_Rand(X, 3, 2); }
    else { tuple->d1 = 2; }
    tuple->a1 = 1 + (mmUInt16_t)mmRaptorq_Rand(X, 4, p->P1 - 1);
    tuple->b1 = (mmUInt16_t)mmRaptorq_Rand(X, 5, p->P1);
}
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Idxs(struct mmRaptorqPrecode* p, mmUInt32_t X, struct mmVectorU16* idxs)
{
    mmUInt16_t j = 0;

    struct mmRaptorqTuple t;

    mmVectorU16_Clear(idxs);

    mmRaptorqPrecode_Tuple(p, &t, X);

    mmVectorU16_Reserve(idxs, t.d + t.d1);
    mmVectorU16_PushBack(idxs, t.b);

    for (j = 1; j < t.d; j++)
    {
        t.b = (t.b + t.a) % p->W;
        mmVectorU16_PushBack(idxs, t.b);
    }
    while (t.b1 >= p->P)
    {
        t.b1 = (t.b1 + t.a1) % p->P1;
    }
    mmVectorU16_PushBack(idxs, p->W + t.b1);
    for (j = 1; j < t.d1; j++)
    {
        t.b1 = (t.b1 + t.a1) % p->P1;
        while (t.b1 >= p->P)
        {
            t.b1 = (t.b1 + t.a1) % p->P1;
        }
        mmVectorU16_PushBack(idxs, p->W + t.b1);
    }
}

MM_EXPORT_RAPTORQ void
mmRaptorqPrecode_MatrixGenerate(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* A,
    mmUInt16_t overhead)
{
    mmOctetMatrix_Resize(A, p->L + overhead, p->L);

    PrecodeMatrix_InitLDPC1(A, p->S, p->B);
    PrecodeMatrix_AddIdentity(A, p->S, 0, p->B);
    PrecodeMatrix_InitLDPC2(A, p->W, p->S, p->P);
    PrecodeMatrix_InitHDPC(p, A);
    PrecodeMatrix_AddIdentity(A, p->H, p->S, p->L - p->H);
    PrecodeMatrix_AddGENC(p, A);
}

MM_EXPORT_RAPTORQ int
mmRaptorqPrecode_MatrixIntermediate1(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    struct mmOctetMatrix* C)
{
    // rfc 6330, pg 32
    // "c" and "d" are used to track row and columns exchange.
    // since Eigen should track row exchange without actually swapping
    // the data, we can call DenseMtx.row.swap without more overhead
    // than actually having "d". so we're left only with "c",
    // which is needed 'cause D does not have _params.L columns.

    int code = MM_FALSE;

    mmUInt16_t i, u;

    mmUInt16_t l = 0;

    struct mmOctetMatrix X;

    struct mmVectorU16 c;

    mmVectorU16_Init(&c);

    mmOctetMatrix_Init(&X);

    do
    {
        if (p->L == 0 || A == NULL || A->rows == 0 || A->cols == 0)
        {
            // need do nothing.
            code = MM_FALSE;
            break;
        }

        mmOctetMatrix_Copy(&X, A);
        mmVectorU16_Reserve(&c, p->L);
        for (l = 0; l < p->L; l++)
        {
            mmVectorU16_PushBack(&c, l);
        }

        code = Decode_Phase1(p, A, &X, D, &c, &i, &u);
        if (MM_TRUE != code)
        {
            // Decode_Phase1 have error.
            code = MM_FALSE;
            break;
        }

        code = Decode_Phase2(A, D, i, u, p->L);
        if (MM_TRUE != code)
        {
            // Decode_Phase2 have error.
            code = MM_FALSE;
            break;
        }

        // A now should be considered as being LxL from now
        Decode_Phase3(A, &X, D, i);
        Decode_Phase4(A, D, i, u);
        Decode_Phase5(A, D, i);

        mmOctetMatrix_Resize(C, D->rows, D->cols);
        for (l = 0; l < p->L; l++)
        {
            mmOctet_Copy(mmOctetMatrix_P(C), mmOctetMatrix_P(D), mmVectorU16_GetIndex(&c, l), l, C->cols);
        }

        if (C->rows == 0)
        {
            // intermediate1 have error, the rows is zero.
            code = MM_FALSE;
            break;
        }

        code = MM_TRUE;
    } while (0);

    mmVectorU16_Destroy(&c);
    mmOctetMatrix_Destroy(&X);

    return code;
}

MM_EXPORT_RAPTORQ int
mmRaptorqPrecode_MatrixIntermediate2(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* M,
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    struct mmRaptorqRepairVector* repair_bin,
    struct mmBitset* mask,
    mmUInt16_t num_symbols,
    mmUInt16_t overhead)
{
    int code = MM_FALSE;
    mmUInt16_t num_gaps, gap = 0, row = 0;

    struct mmOctetMatrix C;
    struct mmOctetMatrix G;

    mmOctetMatrix_Init(&C);
    mmOctetMatrix_Init(&G);

    do
    {
        if (D->cols == 0)
        {
            code = MM_FALSE;
            break;
        }

        Decode_Phase0(p, A, mask, repair_bin, num_symbols, overhead);

        code = mmRaptorqPrecode_MatrixIntermediate1(p, A, D, &C);
        if (MM_FALSE == code)
        {
            code = MM_FALSE;
            break;
        }

        num_gaps = (mmUInt16_t)mmBitset_Count0(mask, num_symbols);
        mmOctetMatrix_Resize(M, num_gaps, D->cols);
        for (gap = 0; gap < num_symbols && num_gaps > 0; gap++)
        {
            if (mmBitset_Get(mask, gap))
            {
                continue;
            }
            mmRaptorqPrecode_MatrixEncode(p, &C, gap, &G);
            mmOctet_Copy(mmOctetMatrix_P(M), mmOctetMatrix_P(&G), row, 0, M->cols);
            mmOctetMatrix_Clear(&G);
            row++;
            num_gaps--;
        }
        code = MM_TRUE;
    } while (0);

    mmOctetMatrix_Destroy(&C);
    mmOctetMatrix_Destroy(&G);
    return code;
}

MM_EXPORT_RAPTORQ void
mmRaptorqPrecode_MatrixEncode(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* C,
    uint32_t isi,
    struct mmOctetMatrix* G)
{
    mmUInt16_t idx = 0;

    struct mmVectorU16 idxs;

    mmOctetMatrix_Resize(G, 1, C->cols);

    mmVectorU16_Init(&idxs);

    mmRaptorqPrecode_Idxs(p, isi, &idxs);
    for (idx = 0; idx < idxs.size; idx++)
    {
        mmOctet_AddRow(mmOctetMatrix_P(G), mmOctetMatrix_P(C), 0, mmVectorU16_GetIndex(&idxs, idx), G->cols);
    }

    mmVectorU16_Destroy(&idxs);
}

MM_EXPORT_RAPTORQ int
mmRaptorqPrecode_MatrixDecode(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* X,
    struct mmRaptorqRepairVector* repair_bin,
    struct mmBitset* mask)
{
    int code = MM_FALSE;

    int precode_ok = MM_FALSE;

    mmUInt16_t num_symbols = X->rows, rep_idx, num_gaps, num_repair, overhead;

    mmUInt16_t skip = 0;
    mmUInt16_t row = 0;
    mmUInt16_t gap = 0;

    mmUInt16_t miss_row = 0;

    struct mmOctetMatrix A;
    struct mmOctetMatrix D;
    struct mmOctetMatrix M;

    mmOctetMatrix_Init(&A);
    mmOctetMatrix_Init(&D);
    mmOctetMatrix_Init(&M);

    do
    {
        num_repair = (mmUInt16_t)mmRaptorqRepairVector_Size(repair_bin);
        num_gaps = (mmUInt16_t)mmBitset_Count0(mask, num_symbols);

        if (num_gaps == 0)
        {
            code = MM_TRUE;
            break;
        }

        if (num_repair < num_gaps)
        {
            code = MM_FALSE;
            break;
        }

        overhead = num_repair - num_gaps;
        rep_idx = 0;
        mmRaptorqPrecode_MatrixGenerate(p, &A, overhead);

        mmOctetMatrix_Resize(&D, p->S + p->H + p->Kp + overhead, X->cols);

        skip = p->S + p->H;
        for (row = 0; row < X->rows; row++)
        {
            mmOctet_Copy(mmOctetMatrix_P(&D), mmOctetMatrix_P(X), skip + row, row, D.cols);
        }

        for (gap = 0; gap < num_symbols && rep_idx < num_repair; gap++)
        {
            struct mmRaptorqRepairSym* rs = NULL;
            if (mmBitset_Get(mask, gap))
            {
                continue;
            }
            row = skip + gap;
            rs = (struct mmRaptorqRepairSym*)mmVectorValue_GetIndex(&repair_bin->v, rep_idx++);
            mmOctet_Copy(mmOctetMatrix_P(&D), mmOctetMatrix_P(&rs->row), row, 0, D.cols);
        }

        for (row = skip + p->Kp; rep_idx < num_repair; row++)
        {
            struct mmRaptorqRepairSym* rs = NULL;
            rs = (struct mmRaptorqRepairSym*)mmVectorValue_GetIndex(&repair_bin->v, rep_idx++);
            mmOctet_Copy(mmOctetMatrix_P(&D), mmOctetMatrix_P(&rs->row), row, 0, D.cols);
        }

        precode_ok = mmRaptorqPrecode_MatrixIntermediate2(p, &M, &A, &D, repair_bin, mask, num_symbols, overhead);
        if (!precode_ok)
        {
            code = MM_FALSE;
            break;
        }

        miss_row = 0;
        for (row = 0; row < num_symbols && miss_row < M.rows; row++)
        {
            if (mmBitset_Get(mask, row))
            {
                continue;
            }
            miss_row++;

            mmOctet_Copy(mmOctetMatrix_P(X), mmOctetMatrix_P(&M), row, miss_row - 1, X->cols);
            mmBitset_Set(mask, row, 1);
        }
        code = MM_TRUE;
    } while (0);

    mmOctetMatrix_Destroy(&A);
    mmOctetMatrix_Destroy(&D);
    mmOctetMatrix_Destroy(&M);

    return code;
}

