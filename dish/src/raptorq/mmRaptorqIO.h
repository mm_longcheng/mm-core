/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqIO_h__
#define __mmRaptorqIO_h__

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

struct mmRaptorqIO
{
    size_t(*Read)(struct mmRaptorqIO* obj, void* buffer, size_t length);
    size_t(*Write)(struct mmRaptorqIO* obj, const void* buffer, size_t length);
    int(*Seek)(struct mmRaptorqIO* obj, long offset);
    size_t(*Size)(struct mmRaptorqIO* obj);
    long(*Tell)(struct mmRaptorqIO* obj);
};
MM_EXPORT_RAPTORQ void mmRaptorqIO_Init(struct mmRaptorqIO* p);
MM_EXPORT_RAPTORQ void mmRaptorqIO_Destroy(struct mmRaptorqIO* p);

struct mmRaptorqIOFile
{
    struct mmRaptorqIO io;
    FILE* fp;
};
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Init(struct mmRaptorqIOFile* p);
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Destroy(struct mmRaptorqIOFile* p);
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Fopen(struct mmRaptorqIOFile* p, const char* filename, const char* mode);
MM_EXPORT_RAPTORQ void mmRaptorqIOFile_Fclose(struct mmRaptorqIOFile* p);

#include "core/mmSuffix.h"

#endif//__mmRaptorqIO_h__
