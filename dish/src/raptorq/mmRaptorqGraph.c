/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqGraph.h"
#include "core/mmAlloc.h"

MM_EXPORT_RAPTORQ void mmRaptorqGraph_Init(struct mmRaptorqGraph* p)
{
    mmVectorValue_Init(&p->edges);
    p->max_edges = 0;

    mmVectorValue_SetElement(&p->edges, sizeof(struct mmRaptorqUInt16Pair));
}
MM_EXPORT_RAPTORQ void mmRaptorqGraph_Destroy(struct mmRaptorqGraph* p)
{
    mmRaptorqGraph_Clear(p);
    //
    mmVectorValue_Destroy(&p->edges);
    p->max_edges = 0;
}

MM_EXPORT_RAPTORQ void mmRaptorqGraph_SetLength(struct mmRaptorqGraph* p, mmUInt16_t length)
{
    mmUInt16_t i = 0;
    struct mmRaptorqUInt16Pair* e = NULL;

    size_t sz = length > p->edges.capacity ? length : p->edges.capacity;
    mmVectorValue_Realloc(&p->edges, sz);
    p->edges.size = length;

    for (i = 0; i < length; i++)
    {
        e = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, i);
        mmRaptorqUInt16Pair_Assign(e, 1, i);
    }
    p->max_edges = 1;
}
MM_EXPORT_RAPTORQ void mmRaptorqGraph_Clear(struct mmRaptorqGraph* p)
{
    mmVectorValue_Clear(&p->edges);
}

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqGraph_Find(struct mmRaptorqGraph* p, mmUInt16_t id)
{
    mmUInt16_t t = id;
    struct mmRaptorqUInt16Pair* e = NULL;

    e = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, t);

    while (e->s != t)
    {
        t = e->s;
        e = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, t);
    }
    return t;
}

MM_EXPORT_RAPTORQ void mmRaptorqGraph_Link(struct mmRaptorqGraph* p, mmUInt16_t node_a, mmUInt16_t node_b)
{
    mmUInt16_t rep_a = mmRaptorqGraph_Find(p, node_a);
    mmUInt16_t rep_b = mmRaptorqGraph_Find(p, node_b);

    struct mmRaptorqUInt16Pair* e_a = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, rep_a);
    struct mmRaptorqUInt16Pair* e_b = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, rep_b);

    mmUInt16_t s = e_a->f + e_b->f;

    mmRaptorqUInt16Pair_Assign(e_a, s, rep_a);
    mmRaptorqUInt16Pair_Assign(e_b, s, rep_a);

    if (node_a != rep_a)
    {
        struct mmRaptorqUInt16Pair* n_a = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, node_a);
        mmRaptorqUInt16Pair_Assign(n_a, s, rep_a);
    }
    if (node_b != rep_b)
    {
        struct mmRaptorqUInt16Pair* n_b = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, node_b);
        mmRaptorqUInt16Pair_Assign(n_b, s, rep_a);
    }

    if (p->max_edges < e_a->f)
    {
        p->max_edges = e_a->f;
    }
}

MM_EXPORT_RAPTORQ int mmRaptorqGraph_IsMax(struct mmRaptorqGraph* p, mmUInt16_t id)
{
    mmUInt16_t e = mmRaptorqGraph_Find(p, id);
    struct mmRaptorqUInt16Pair* e_a = (struct mmRaptorqUInt16Pair*)mmVectorValue_GetIndex(&p->edges, e);
    return p->max_edges == e_a->f;
}
