/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqOctetBlas_h__
#define __mmRaptorqOctetBlas_h__

// https://github.com/sleepybishop/oblas

// we only use the classic version.

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_RAPTORQ void mmOctet_Copy(mmUInt8_t* a, mmUInt8_t* b, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k);
MM_EXPORT_RAPTORQ void mmOctet_SwapRow(mmUInt8_t* a, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k);
MM_EXPORT_RAPTORQ void mmOctet_SwapCol(mmUInt8_t* a, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k, mmUInt16_t l);
MM_EXPORT_RAPTORQ void mmOctet_Axpy(mmUInt8_t* a, mmUInt8_t* b, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k, mmUInt8_t u);
MM_EXPORT_RAPTORQ void mmOctet_AddRow(mmUInt8_t* a, mmUInt8_t* b, mmUInt16_t i, mmUInt16_t j, mmUInt16_t k);
MM_EXPORT_RAPTORQ void mmOctet_Scal(mmUInt8_t* a, mmUInt16_t i, mmUInt16_t k, mmUInt8_t u);
MM_EXPORT_RAPTORQ void mmOctet_Zero(mmUInt8_t* a, mmUInt16_t i, size_t k);
MM_EXPORT_RAPTORQ void mmOctet_Gemm(mmUInt8_t* a, mmUInt8_t* b, mmUInt8_t* c, mmUInt16_t n, mmUInt16_t k, mmUInt16_t m);

#include "core/mmSuffix.h"

#endif//__mmRaptorqOctetBlas_h__
