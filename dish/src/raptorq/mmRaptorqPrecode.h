/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqPrecode_h__
#define __mmRaptorqPrecode_h__

#include "core/mmCore.h"

#include "container/mmVectorU16.h"
#include "container/mmBitset.h"

#include "mmRaptorqOctetMatrix.h"
#include "mmRaptorqRepair.h"
#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

struct mmRaptorqTuple
{
    mmUInt16_t d;
    mmUInt16_t a;
    mmUInt16_t b;
    mmUInt16_t d1;
    mmUInt16_t a1;
    mmUInt16_t b1;
};

/*
* rfc6330 pg 20
* 5.3.3.3. Pre-Coding Relationships
*/
struct mmRaptorqPrecode
{
    mmUInt16_t index_Kp;// index_Kp

    mmUInt16_t Kp;// K'
    mmUInt16_t S;// S
    mmUInt16_t H;// H
    mmUInt16_t W;// W
    mmUInt16_t L;// L
    mmUInt16_t P;// P
    mmUInt16_t P1;// P1
    mmUInt16_t U;// U
    mmUInt16_t B;// B

    mmUInt16_t J;// J
};

MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Init(struct mmRaptorqPrecode* p);
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Destroy(struct mmRaptorqPrecode* p);
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Reset(struct mmRaptorqPrecode* p);

/*
* 5.3.3.3. Pre-Coding Relationships
*  The pre-coding relationships amongst the L intermediate symbols are
*  defined by requiring that a set of S+H linear combinations of the
*  intermediate symbols evaluate to zero. There are S LDPC and H HDPC
*  symbols, and thus L = K��+S+H. Another partition of the L
*  intermediate symbols is into two sets, one set of W LT symbols and
*  another set of P PI symbols, and thus it is also the case that L =
*  W+P. The P PI symbols are treated differently than the W LT symbols
*  in the encoding process. The P PI symbols consist of the H HDPC
*  symbols together with a set of U = P-H of the other K�� intermediate
*  symbols. The W LT symbols consist of the S LDPC symbols together
*  with W-S of the other K�� intermediate symbols. The values of these
*  parameters are determined from K�� as described below, where H(K��),
*  S(K��), and W(K��) are derived from Table 2 in Section 5.6.
*  Let
*  o S = S(K��)
*  o H = H(K��)
*  o W = W(K��)
*  o L = K�� + S + H
*  o P = L - W
*  o P1 denote the smallest prime number greater than or equal to P.
*  o U = P - H
*  o B = W - S
*/
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_SetSymbols(struct mmRaptorqPrecode* p, mmUInt16_t symbols);

/*
* 5.3.5.4. Tuple Generator
*  The tuple generator Tuple[K��,X] takes the following inputs:
*  o K��: the number of source symbols in the extended source block
*  o X: an ISI
* Let
*  o L be determined from K�� as described in Section 5.3.3.3
*  o J = J(K��) be the systematic index associated with K��, as defined
*  in Table 2 in Section 5.6
*  The output of the tuple generator is a tuple, (d, a, b, d1, a1, b1),
*  determined as follows:
*  o A = 53591 + J*997
*  o if (A % 2 == 0) { A = A + 1 }
*  o B = 10267*(J+1)
*  o y = (B + X*A) % 2^^32
*  o v = Rand[y, 0, 2^^20]
*  o d = Deg[v]
*  o a = 1 + Rand[y, 1, W-1]
*  o b = Rand[y, 2, W]
*  o If (d < 4) { d1 = 2 + Rand[X, 3, 2] } else { d1 = 2 }
*  o a1 = 1 + Rand[X, 4, P1-1]
*  o b1 = Rand[X, 5, P1]
*
* 5.3.3.3. Pre-Coding Relationships
*  o L = K�� + S + H
*/
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Tuple(struct mmRaptorqPrecode* p, struct mmRaptorqTuple* tuple, mmUInt32_t X);

/*
* The encoding symbol generator produces a single encoding symbol as
*  output (referred to as result), according to the following algorithm:
*  o result = C[b]
*  o For j = 1, ..., d-1 do
*  * b = (b + a) % W
*  * result = result + C[b]
*  o While (b1 >= P) do b1 = (b1+a1) % P1
*  o result = result + C[W+b1]
*  o For j = 1, ..., d1-1 do
*  * b1 = (b1 + a1) % P1
*  * While (b1 >= P) do b1 = (b1+a1) % P1
*  * result = result + C[W+b1]
*  o Return result
*/
MM_EXPORT_RAPTORQ void mmRaptorqPrecode_Idxs(struct mmRaptorqPrecode* p, mmUInt32_t X, struct mmVectorU16* idxs);

MM_EXPORT_RAPTORQ void
mmRaptorqPrecode_MatrixGenerate(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* A,
    mmUInt16_t overhead);

MM_EXPORT_RAPTORQ int
mmRaptorqPrecode_MatrixIntermediate1(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    struct mmOctetMatrix* C);

MM_EXPORT_RAPTORQ int
mmRaptorqPrecode_MatrixIntermediate2(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* M,
    struct mmOctetMatrix* A,
    struct mmOctetMatrix* D,
    struct mmRaptorqRepairVector* repair_bin,
    struct mmBitset* mask,
    mmUInt16_t num_symbols,
    mmUInt16_t overhead);

MM_EXPORT_RAPTORQ void
mmRaptorqPrecode_MatrixEncode(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* C,
    uint32_t isi,
    struct mmOctetMatrix* G);

MM_EXPORT_RAPTORQ int
mmRaptorqPrecode_MatrixDecode(
    struct mmRaptorqPrecode* p,
    struct mmOctetMatrix* X,
    struct mmRaptorqRepairVector* repair_bin,
    struct mmBitset* mask);

#include "core/mmSuffix.h"

#endif//__mmRaptorqPrecode_h__
