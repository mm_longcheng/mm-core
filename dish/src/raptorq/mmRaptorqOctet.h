/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqOctet_h__
#define __mmRaptorqOctet_h__

// https://github.com/sleepybishop/oblas

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

#ifndef MM_OCTMAT_ALIGN
#define MM_OCTMAT_ALIGN 16
#endif

/*
* The table OCT_LOG contains 255 non-negative integers. The table is
*  indexed by octets interpreted as integers. The octet corresponding
*  to the zero element, which is represented by the integer 0, is
*  excluded as an index, and thus indexing starts at 1 and ranges up to
*  255, and the entries are the following:
*/
MM_EXPORT_RAPTORQ extern const mmUInt8_t OCT_LOG[256];

/*
* The table OCT_EXP contains 510 octets. The indexing starts at 0 and
*  ranges to 509, and the entries are the octets with the following
*  positive integer representation:
*/
MM_EXPORT_RAPTORQ extern const mmUInt8_t OCT_EXP[510];

static const size_t OCT_LOG_SIZE = 256;
static const size_t OCT_EXP_SIZE = 510;

/*
* The multiplication of two octets is defined with the help of two
*  tables OCT_EXP and OCT_LOG, which are given in Section 5.7.3 and
*  Section 5.7.4, respectively. The table OCT_LOG maps octets (other
*  than the zero element) to non-negative integers, and OCT_EXP maps
*  non-negative integers to octets. For two octets u and v, we define
*  u * v =
*  0, if either u or v are 0,
*  OCT_EXP[OCT_LOG[u] + OCT_LOG[v]] otherwise.
*  Note that the ��+�� on the right-hand side of the above is the usual
*  integer addition, since its arguments are ordinary integers.
*/
MM_EXPORT_RAPTORQ mmUInt8_t mmOctet_Mul(mmUInt8_t u, mmUInt8_t v);

/*
* The division u / v of two octets u and v, and where v != 0, is
*  defined as follows:
*  u / v =
*  0, if u == 0,
*  OCT_EXP[OCT_LOG[u] - OCT_LOG[v] + 255] otherwise.
*/
MM_EXPORT_RAPTORQ mmUInt8_t mmOctet_Div(mmUInt8_t u, mmUInt8_t v);

/*
* The one element (multiplicative identity) is the octet represented by
*  the integer 1. For an octet u that is not the zero element, i.e.,
*  the multiplicative inverse of u is
*  OCT_EXP[255 - OCT_LOG[u]].
*/
MM_EXPORT_RAPTORQ mmUInt8_t mmOctet_Inv(mmUInt8_t u);

/*
* The octet denoted by alpha is the octet with the integer
*  representation 2. If i is a non-negative integer 0 <= i < 256, we
*  have
*  alpha^^i = OCT_EXP[i].
*/
MM_EXPORT_RAPTORQ mmUInt8_t mmOctet_Exp(mmUInt8_t i);

/*
* The octet denoted by alpha is the octet with the integer
*  representation 2. If i is a non-negative integer 0 <= i < 256, we
*  have
*  log2(i) = OCT_LOG[i].
*/
MM_EXPORT_RAPTORQ mmUInt8_t mmOctet_Log2(mmUInt8_t i);

/*
 * aligned cols
 */
MM_EXPORT_RAPTORQ size_t mmOctet_AlignedCols(size_t k);

#include "core/mmSuffix.h"

#endif//__mmRaptorqOctet_h__
