/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqChooser_h__
#define __mmRaptorqChooser_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "mmRaptorqExport.h"
#include "mmRaptorqPair.h"
#include "mmRaptorqOctetMatrix.h"
#include "mmRaptorqGraph.h"

#include "core/mmPrefix.h"

struct mmRaptorqChooser
{
    struct mmVectorValue tracking;
    struct mmVectorValue r_rows;
    int only_two_ones;
};

MM_EXPORT_RAPTORQ void mmRaptorqChooser_Init(struct mmRaptorqChooser* p);
MM_EXPORT_RAPTORQ void mmRaptorqChooser_Destroy(struct mmRaptorqChooser* p);

MM_EXPORT_RAPTORQ void mmRaptorqChooser_SetLength(struct mmRaptorqChooser* p, mmUInt16_t length);
MM_EXPORT_RAPTORQ void mmRaptorqChooser_Clear(struct mmRaptorqChooser* p);

MM_EXPORT_RAPTORQ void mmRaptorqChooser_PushUInt16Pair(struct mmRaptorqChooser* p, mmUInt16_t f, mmUInt16_t s);

MM_EXPORT_RAPTORQ void mmRaptorqChooser_PushTrackingPair(struct mmRaptorqChooser* p, size_t row_degree, int is_hdpc);

MM_EXPORT_RAPTORQ mmUInt16_t
mmRaptorqChooser_NonZero(
    struct mmRaptorqChooser* p,
    struct mmOctetMatrix* A,
    struct mmRaptorqGraph* G,
    mmUInt16_t i,
    mmUInt16_t sub_rows,
    mmUInt16_t sub_cols);

MM_EXPORT_RAPTORQ mmUInt16_t
mmRaptorqChooser_Pick(
    struct mmRaptorqChooser* p,
    struct mmRaptorqGraph* G,
    mmUInt16_t i,
    mmUInt16_t sub_rows,
    mmUInt16_t non_zero);

#include "core/mmSuffix.h"

#endif//__mmRaptorqChooser_h__
