/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRaptorqDegree.h"
#include "mmRaptorqMath.h"

static const mmUInt32_t f[] =
{
          0,
       5243,
     529531,
     704294,
     791675,
     844104,
     879057,
     904023,
     922747,
     937311,
     948962,
     958494,
     966438,
     973160,
     978921,
     983914,
     988283,
     992138,
     995565,
     998631,
    1001391,
    1003887,
    1006157,
    1008229,
    1010129,
    1011876,
    1013490,
    1014983,
    1016370,
    1017662,
    1048576,
};
static const size_t f_size = MM_ARRAY_SIZE(f);

static size_t __static_FInformation_BinarySerachFindFirstLarger(size_t o, size_t l, mmUInt32_t key)
{
    size_t mid = 0;
    size_t left = o;
    size_t right = o + l - 1;
    mmSInt32_t result = 0;

    if (0 == l)
    {
        return (size_t)-1;
    }
    else
    {
        while (left <= right && (size_t)(-1) != right)
        {
            // Avoid overflow problems
            mid = left + (right - left) / 2;

            result = f[mid] - key;

            if (result > 0)
            {
                right = mid - 1;
            }
            else
            {
                left = mid + 1;
            }
        }

        return left;
    }
}

MM_EXPORT_RAPTORQ int mmRaptorqDegree_Deg(unsigned int v, int W)
{
    int value = 0;

    mmUInt16_t d = 0;

    d = (mmUInt16_t)__static_FInformation_BinarySerachFindFirstLarger(0, f_size, v);

    value = ((size_t)(-1) == (size_t)d || (size_t)d >= f_size) ? (mmUInt16_t)0 : ((d < (W - 2)) ? d : (W - 2));

    return value;
}

