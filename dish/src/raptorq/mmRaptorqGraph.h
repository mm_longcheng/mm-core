/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqGraph_h__
#define __mmRaptorqGraph_h__

#include "core/mmCore.h"

#include "container/mmVectorValue.h"

#include "mmRaptorqExport.h"
#include "mmRaptorqPair.h"

#include "core/mmPrefix.h"

struct mmRaptorqGraph
{
    struct mmVectorValue edges;
    mmUInt16_t max_edges;
};

MM_EXPORT_RAPTORQ void mmRaptorqGraph_Init(struct mmRaptorqGraph* p);
MM_EXPORT_RAPTORQ void mmRaptorqGraph_Destroy(struct mmRaptorqGraph* p);

MM_EXPORT_RAPTORQ void mmRaptorqGraph_SetLength(struct mmRaptorqGraph* p, mmUInt16_t length);
MM_EXPORT_RAPTORQ void mmRaptorqGraph_Clear(struct mmRaptorqGraph* p);

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqGraph_Find(struct mmRaptorqGraph* p, mmUInt16_t id);
MM_EXPORT_RAPTORQ void mmRaptorqGraph_Link(struct mmRaptorqGraph* p, mmUInt16_t node_a, mmUInt16_t node_b);
MM_EXPORT_RAPTORQ int mmRaptorqGraph_IsMax(struct mmRaptorqGraph* p, mmUInt16_t id);

#include "core/mmSuffix.h"

#endif//__mmRaptorqGraph_h__
