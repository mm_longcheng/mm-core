/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRaptorqSystematic_h__
#define __mmRaptorqSystematic_h__

#include "core/mmCore.h"

#include "mmRaptorqExport.h"

#include "core/mmPrefix.h"

static const uint16_t mmRaptorq_K_max = 56403;

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_KIndexKp(mmUInt16_t symbols);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_KIndexKl(mmUInt16_t symbols);

MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_K(mmUInt16_t Kp);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_J(mmUInt16_t Kp);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_S(mmUInt16_t Kp);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_H(mmUInt16_t Kp);
MM_EXPORT_RAPTORQ mmUInt16_t mmRaptorqSystematic_W(mmUInt16_t Kp);

#include "core/mmSuffix.h"

#endif//__mmRaptorqSystematic_h__
