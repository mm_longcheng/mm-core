/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmJavaVMEnv.h"
#include "core/mmLogger.h"

static void __static_mmJavaVMEnv_DetachCurrentThread(void* v);
static JNIEnv* __static_mmJavaVMEnv_AttachCurrentThread(struct mmJavaVMEnv* p);
static struct mmJavaVMEnv gJavaVMEnv = { NULL, 0, };

MM_EXPORT_DISH extern struct mmJavaVMEnv* mmJavaVMEnv_Instance()
{
    return &gJavaVMEnv;
}

MM_EXPORT_DISH void mmJavaVMEnv_Init(struct mmJavaVMEnv* p)
{
    p->jvm = NULL;
}
MM_EXPORT_DISH void mmJavaVMEnv_Destroy(struct mmJavaVMEnv* p)
{
    p->jvm = NULL;
}

MM_EXPORT_DISH void mmJavaVMEnv_SetJvm(struct mmJavaVMEnv* p, JavaVM* jvm)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    pthread_t thread_id = pthread_self();
    p->jvm = jvm;
    // mmJavaVMEnv is only use my android.the pthread_t is long int.
    mmLogger_LogI(gLogger,"%s %d jvm: %p thread_id: %ld.",__FUNCTION__,__LINE__,p->jvm,thread_id);
    //
    pthread_key_create(&p->thread_key, __static_mmJavaVMEnv_DetachCurrentThread);
}
MM_EXPORT_DISH JavaVM* mmJavaVMEnv_GetJvm(struct mmJavaVMEnv* p)
{
    struct mmLogger* gLogger = mmLogger_Instance();
    pthread_t thread_id = pthread_self();
    // mmJavaVMEnv is only use my android.the pthread_t is long int.
    mmLogger_LogI(gLogger,"%s %d jvm: %p thread_id: %ld.",__FUNCTION__,__LINE__,p->jvm,thread_id);
    return p->jvm;
}

MM_EXPORT_DISH JNIEnv* mmJavaVMEnv_CurrentThreadJniEnv(struct mmJavaVMEnv* p)
{
    JNIEnv* env = NULL;
    assert(NULL != p->jvm && "p->jvm is a null.");
    env = (JNIEnv*)pthread_getspecific(p->thread_key);
    if (NULL == env)
    {
        env = __static_mmJavaVMEnv_AttachCurrentThread(p);
    }
    return env;
}

MM_EXPORT_DISH void mmJavaVMEnvSetJVM(JavaVM* jvm)
{
    struct mmJavaVMEnv* gJavaVMEnv = mmJavaVMEnv_Instance();
    mmJavaVMEnv_SetJvm(gJavaVMEnv, jvm);
}
MM_EXPORT_DISH JavaVM* mmJavaVMEnvGetJVM(void)
{
    struct mmJavaVMEnv* gJavaVMEnv = mmJavaVMEnv_Instance();
    return mmJavaVMEnv_GetJvm(gJavaVMEnv);
}
MM_EXPORT_DISH JNIEnv* mmJavaVMEnvCurrentThreadJNIEnv(void)
{
    struct mmJavaVMEnv* gJavaVMEnv = mmJavaVMEnv_Instance();
    return mmJavaVMEnv_CurrentThreadJniEnv(gJavaVMEnv);
}

static void __static_mmJavaVMEnv_DetachCurrentThread(void* v) 
{
    struct mmJavaVMEnv* java_vm = mmJavaVMEnv_Instance();
    JavaVM* jvm = mmJavaVMEnv_GetJvm(java_vm);
    (*jvm)->DetachCurrentThread(jvm);
}
static JNIEnv* __static_mmJavaVMEnv_AttachCurrentThread(struct mmJavaVMEnv* p) 
{
    JNIEnv* env = NULL;
    struct mmLogger* gLogger = mmLogger_Instance();
    // get jni environment
    jint ret = (*p->jvm)->GetEnv(p->jvm, (void**)&env, JNI_VERSION_1_4);
    
    switch (ret) 
    {
    case JNI_OK :
        // success.
        pthread_setspecific(p->thread_key, env);
        return env;
    case JNI_EDETACHED :
        // thread not attached
        if (0 > (*p->jvm)->AttachCurrentThread(p->jvm, &env, NULL))
        {
            mmLogger_LogE(gLogger,"%s %d jvm: %p Failed to get the environment using AttachCurrentThread().",__FUNCTION__,__LINE__,p->jvm);
            return NULL;
        } 
        else 
        {
            // success : attached and obtained JNIEnv!
            pthread_setspecific(p->thread_key, env);
            return env;
        }
    case JNI_EVERSION :
        // cannot recover from this error
        mmLogger_LogE(gLogger,"%s %d jvm: %p JNI interface version 1.4 not supported.",__FUNCTION__,__LINE__,p->jvm);
    default :
        mmLogger_LogE(gLogger,"%s %d jvm: %p Failed to get the environment using GetEnv()",__FUNCTION__,__LINE__,p->jvm);
        return NULL;
    }
}
