/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmJavaVMEnvEnv_h__
#define __mmJavaVMEnvEnv_h__

#include "core/mmCore.h"

#include <pthread.h>
#include <jni.h>

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

struct mmJavaVMEnv
{
    JavaVM* jvm;
    pthread_key_t thread_key;
};

MM_EXPORT_DISH extern struct mmJavaVMEnv* mmJavaVMEnv_Instance();

MM_EXPORT_DISH void mmJavaVMEnv_Init(struct mmJavaVMEnv* p);
MM_EXPORT_DISH void mmJavaVMEnv_Destroy(struct mmJavaVMEnv* p);

MM_EXPORT_DISH void mmJavaVMEnv_SetJvm(struct mmJavaVMEnv* p, JavaVM* jvm);
MM_EXPORT_DISH JavaVM* mmJavaVMEnv_GetJvm(struct mmJavaVMEnv* p);

MM_EXPORT_DISH JNIEnv* mmJavaVMEnv_CurrentThreadJniEnv(struct mmJavaVMEnv* p);

MM_EXPORT_DISH void mmJavaVMEnvSetJVM(JavaVM* jvm);
MM_EXPORT_DISH JavaVM* mmJavaVMEnvGetJVM(void);
MM_EXPORT_DISH JNIEnv* mmJavaVMEnvCurrentThreadJNIEnv(void);

#include "core/mmSuffix.h"

#endif//__mmJavaVMEnvEnv_h__