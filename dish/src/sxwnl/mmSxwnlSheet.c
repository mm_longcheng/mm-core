/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlSheet.h"
#include "mmSxwnlConst.h"
#include "mmSxwnlJD.h"
#include "mmSxwnlEph0.h"
#include "mmSxwnlEphB.h"
#include "mmSxwnlEph.h"
#include "mmSxwnlLunar.h"
#include "mmSxwnlLunarData.h"

#include "core/mmString.h"
#include "core/mmStringUtils.h"
#include "core/mmClock.h"
#include "core/mmValueTransform.h"

MM_EXPORT_SXWNL
void
csSheetHX_Init(
    struct csSheetHX* p)
{
    p->vJ = (116.0 + 23.0 / 60.0) / cs_radd;
    p->vW = (39.9               ) / cs_radd;
    p->idx = 1000;
    p->utc = 1;
    p->tz = -8;
    p->lx = 0;
    p->nsn = 0;
    p->n = 10;
    p->dt = 10;
    csEphHXs_Init(&p->HXs);
    csEphHXDatas_Init(&p->HXd);
    csJD_Make(&p->JD, 2008, 8, 1, 10, 22, 12.0);
}

MM_EXPORT_SXWNL
void
csSheetHX_Destroy(
    struct csSheetHX* p)
{
    csJD_Make(&p->JD, 2008, 8, 1, 10, 22, 12.0);
    csEphHXDatas_Destroy(&p->HXd);
    csEphHXs_Destroy(&p->HXs);
    p->dt = 10;
    p->n = 10;
    p->nsn = 0;
    p->lx = 0;
    p->tz = -8;
    p->utc = 1;
    p->idx = 1000;
    p->vW = (39.9) / cs_radd;
    p->vJ = (116.0 + 23.0 / 60.0) / cs_radd;
}

//显示恒星库名称例表
MM_EXPORT_SXWNL
void
csSheetHX_showHXK0(
    struct mmString* s)
{
    int i;
    int idx;
    char str[32] = { 0 };
    const char** R;
    size_t ssz;

    mmString_Reset(s);

    for (i = 0; i < HXKLLength; i++) 
    {
        sprintf(str, "%s%d", "库", i);
        ssz = strlen(str);
        mmString_LAlignAppends(s, str, ssz, 10);
        mmString_Appends(s, " ");
        idx = 1000 + i;
        mmValue_SIntToA(&idx, str);
        ssz = strlen(str);
        mmString_RAlignAppends(s, str, ssz, 2);
        mmString_Appends(s, "\n");
    }

    for (i = 0; i < xz88Length / 5; i ++)
    {
        R = &xz88[i * 5];
        ssz = strlen(R[0]);
        mmString_LAlignAppends(s, R[0], ssz, 10);
        mmString_Appends(s, " ");
        idx = 2000 + i;
        mmValue_SIntToA(&idx, str);
        ssz = strlen(str);
        mmString_RAlignAppends(s, str, ssz, 2);
        mmString_Appends(s, "\n");
    }
}

//更新选中恒星库索引idx
MM_EXPORT_SXWNL
void
csSheetHX_updateHXKIdx(
    struct csSheetHX* p)
{
    int i;

    const char* N;

    switch (p->idx / 1000)
    {
    case 1:
        i = p->idx % 1000;
        idxHXK(i, &p->HXs);
        break;
    case 2:
        i = p->idx % 2000;
        N = xz88[i * 5];
        schHXK(&N[0 + strlen(N) - 3], &p->HXs);
        break;
    default:
        break;
    }
}

//更新选中恒星库索引idx计算数据
MM_EXPORT_SXWNL
void
csSheetHX_updateHXKIdxData(
    struct csSheetHX* p,
    int all)
{
    getHXK(&p->HXs, all, &p->HXd);
}

//显示恒星库
MM_EXPORT_SXWNL
void
csSheetHX_showHXK(
    struct csSheetHX* p,
    struct mmString* s)
{
    struct mmString s1;

    mmString_Assigns(s, "   RA(时分秒)   DEC(度分秒)  自行1   自行2   视差  星等  星名    星座\n");

    mmString_Init(&s1);

    strHXK(&p->HXs, &s1);
    mmString_Append(s, &s1);

    mmString_Destroy(&s1);
}

// 恒星计算
MM_EXPORT_SXWNL
void
csSheetHX_aCalc(
    struct csSheetHX* p,
    struct mmString* s)
{
    int i;
    int Q = p->nsn ? 35 : 0;//小于35天的称短周期项
    int lx = p->lx;//坐标类型
    double L = p->vJ; //地标
    double fa = p->vW;

    struct mmString s1;

    //取屏幕时间
    double jd = csJD_toJD(&p->JD) - cs_J2000;
    if (p->utc) jd += p->tz / 24.0 + dt_T(jd); //转为力学时

    mmString_Init(&s1);

    for (i = 0; i < p->n; i++, jd += p->dt)
    {
        hxCalc(jd / 36525, &p->HXd, Q, lx, L, fa, &s1);
        mmString_Append(s, &s1);
    }

    mmString_Destroy(&s1);
}

// 天象时间格式化输出
MM_EXPORT_SXWNL
void
txFormatT(
    double t,
    struct mmString* s)
{
    char str[32] = { 0 };
    double t1 = t * 36525 + cs_J2000;
    double t2 = t1 - dt_T(t1 - cs_J2000) + 8.0 / 24;
    mmString_Reset(s);
    csJD_JD2str(t1, str); mmString_Appends(s, str);
    mmString_Appends(s, " TD ");
    csJD_JD2str(t2, str); mmString_Appendsn(s, &str[9], 11);
    mmString_Appends(s, " UT ");
}

// n通常取10
MM_EXPORT_SXWNL
void
tianXiang(
    int xm,
    int xm2,
    const struct csJD* dat,
    int n,
    struct mmString* s)
{
    static const char* xxName[] = 
    { 
        "地球", "水星", "金星", "火星", "木星", "土星", "天王星", "海王星", "冥王星", 
    };

    char str[32] = { 0 };
    struct mmString s1;

    int i;
    double re0;
    double re[2];
    double re2[4];
    double jd = csJD_toJD(dat) - cs_J2000;
    jd /= 36525.0;
    
    // 消除可能的未初始化警告
    re0 = 0;

    mmString_Reset(s);

    mmString_Init(&s1);

    if (xm == 1 || xm == 2)
    { 
        //求月亮近远点
        for (i = 0; i < n; i++, jd = re[0] + 27.555 / 36525.0)
        {
            if (xm == 1)
                XL_moonMinR(re, jd, 1);//求近点
            if (xm == 2)
                XL_moonMinR(re, jd, 0);//求远点

            txFormatT(re[0], &s1); mmString_Append(s, &s1);
            toFixed(str, re[1], 2); mmString_Appends(s, str);
            mmString_Appends(s, "千米");
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 3 || xm == 4)
    { 
        //求月亮升降交点
        for (i = 0; i < n; i++, jd = re[0] + 27.555 / 36525.0)
        {
            if (xm == 3)
                XL_moonNode(re, jd, 1);//求升
            if (xm == 4)
                XL_moonNode(re, jd, 0);//求降

            txFormatT(re[0], &s1); mmString_Append(s, &s1);
            rad2str(str, rad2mrad(re[1]), 0); mmString_Appends(s, str);
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 5 || xm == 6)
    { 
        //求地球近远点
        for (i = 0; i < n; i++, jd = re[0] + 365.259636 / 36525.0)
        {
            if (xm == 5)
                XL_earthMinR(re, jd, 1);//求近点
            if (xm == 6)
                XL_earthMinR(re, jd, 0);//求远点

            txFormatT(re[0], &s1); mmString_Append(s, &s1);
            toFixed(str, re[1], 8); mmString_Appends(s, str);
            mmString_Appends(s, " AU");
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 7 || xm == 8)
    { 
        //大距计算
        for (i = 0; i < n; i++, jd = re[0] + 115.8774777586 / 36525.0)
        {
            if (xm == 7)
                daJu(re, 1, jd, 1);//求水星东大距
            if (xm == 8)
                daJu(re, 1, jd, 0);//求水星东西距

            txFormatT(re[0], &s1); mmString_Append(s, &s1);
            toFixed(str, (re[1] / cs_pi * 180), 5); mmString_Appends(s, str);
            mmString_Appends(s, "度");
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 9 || xm == 10)
    { 
        //大距计算
        for (i = 0; i < n; i++, jd = re[0] + 583.9213708245 / 36525.0)
        {
            if (xm == 9)
                daJu(re, 2, jd, 1);//求水星东大距
            if (xm == 10)
                daJu(re, 2, jd, 0);//求水星东西距

            txFormatT(re[0], &s1); mmString_Append(s, &s1);
            toFixed(str, (re[1] / cs_pi * 180), 5); mmString_Appends(s, str);
            mmString_Appends(s, "度");
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 11)
    { 
        //合月计算
        mmString_Assigns(s, "合月时间(TD UT) 星月赤纬差(小于1度可能月掩星,由视差决定)\n");
        for (i = 0; i < n; i++, jd = re2[0] + 28.0 / 36525.0)
        {
            xingHY(re2, xm2, jd);

            txFormatT(re2[0], &s1); mmString_Append(s, &s1);
            toFixed(str, (-re2[1] / cs_pi * 180), 5); mmString_Appends(s, str);
            mmString_Appends(s, "度");
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 12 || xm == 13)
    {
        if (xm == 12)
        {
            mmString_Assigns(s, xxName[xm2]);
            mmString_Appends(s, "合日(地内行星上合)\n");
        }
            
        if (xm == 13)
        {
            mmString_Assigns(s, xxName[xm2]);
            mmString_Appends(s, "冲日(地内行星下合)\n");
        }

        mmString_Appends(s, "黄经合/冲日时间(TD UT) 星日赤纬差\n");

        for (i = 0; i < n; i++, jd = re[0] + cs_xxHH[xm2 - 1] / 36525.0)
        {
            if (xm == 12)
                xingHR(re, xm2, jd, 0);
            if (xm == 13)
                xingHR(re, xm2, jd, 1);

            txFormatT(re[0], &s1); mmString_Append(s, &s1);
            toFixed(str, (-re[1] / cs_pi * 180), 5); mmString_Appends(s, str);
            mmString_Appends(s, "度");
            mmString_Appends(s, "\n");
        }
    }

    if (xm == 14 || xm == 15)
    { 
        //顺留
        if (xm == 14)
        {
            mmString_Assigns(s, xxName[xm2]);
            mmString_Appends(s, "顺留\n");
        }
        
        if (xm == 15)
        {
            mmString_Assigns(s, xxName[xm2]);
            mmString_Appends(s, "逆留\n");
        }

        mmString_Appends(s, "留时间(TD UT)\n");

        for (i = 0; i < n; i++, jd = re0 + cs_xxHH[xm2 - 1] / 36525.0)
        {
            if (xm == 14)
                re0 = xingLiu(xm2, jd, 1);
            if (xm == 15)
                re0 = xingLiu(xm2, jd, 0);

            txFormatT(re0, &s1); mmString_Append(s, &s1);
            mmString_Appends(s, "\n");
        }
    }

    mmString_Destroy(&s1);
}

//
// 通常 utc = 1 n = 10 dt = 1
// 行星星历计算
MM_EXPORT_SXWNL
void
pCalc(
    int xt,
    const struct csJD* dat,
    double vJ, double vW,
    int utc,
    int n,
    int dt,
    struct mmString* s)
{
    int i;
    char str[32] = { 0 };
    struct mmString s1;
    double jd = csJD_toJD(dat) - cs_J2000;//取屏幕时间
    double L = vJ;//地标
    double fa = vW;
    if (utc) jd += -8.0 / 24 + dt_T(jd);//转为力学时

    mmString_Reset(s);

    if (n > 1000)
    {
        // 个数太多了
        return;
    }

    mmString_Init(&s1);

    //求星历
    for (i = 0; i < n; i++, jd += dt)
    {
        double jd2 = jd + 2451545;
        csJD_JD2str(jd2, str); mmString_Appends(s, str);
        mmString_Appends(s, "TD, JED = ");
        toFixed(str, jd2, 7); mmString_Appends(s, str);
        mmString_Appends(s, " ");
        mmString_Appends(s, "\n");

        xingX(&s1, xt, jd, L, fa); mmString_Append(s, &s1);
        mmString_Appends(s, "\n");
    }

    mmString_Destroy(&s1);
}

// 通常 n = 24 jiao = 0
//定朔测试函数
MM_EXPORT_SXWNL
void
suoCalc(
    int y,
    int n,
    int jiao,
    struct mmString* s)
{
    int i;
    int n0;
    double r, T;
    char str[32] = { 0 };
    struct mmString s1;

    y -= 2000;
    if (jiao == -1)
    {
        // 数据有误
        // 请输入角度(0朔,90上弦,180望,270下弦,或其它):
        return;
    }

    mmString_Reset(s);

    mmString_Appends(s, "月-日黄经差");

    toFixed(str, jiao, 0); mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Init(&s1);

    n0 = int2(y * (365.2422 / 29.53058886));//截止当年首经历朔望的个数
    for (i = 0; i < n; i++)
    {
        T = XL_MS_aLon_t((n0 + i + jiao / 360.0) * 2 * cs_pi);//精确时间计算,入口参数是当年各朔望黄经
        r = XL1_calc(2, T, -1);//计算月亮

        //日期转为字串
        csJD_JD2str(T * 36525 + cs_J2000 + 8.0 / 24 - dt_T(T * 36525), str); mmString_Appends(s, str);
        mmString_Appends(s, " ");
        toFixed(str, r, 2); mmString_Appends(s, str);
        mmString_Appends(s, "千米\n");
        if (i % 50 == 0)
        {
            mmString_Append(s, &s1);
            mmString_Assigns(&s1, "");
        }
    }

    mmString_Append(s, &s1);

    mmString_Destroy(&s1);
}

// 通常 n = 24 
// 定气测试函数
MM_EXPORT_SXWNL
void
qiCalc(
    int y,
    int n,
    struct mmString* s)
{
    int i;
    double T;
    char str[32] = { 0 };
    struct mmString s1;
    size_t ssz;

    y -= 2000;

    mmString_Reset(s);

    mmString_Init(&s1);

    for (i = 0; i < n; i++)
    {
        //精确节气时间计算
        T = XL_S_aLon_t((y + i * 15 / 360.0 + 1) * 2 * cs_pi);

        //日期转为字串
        csJD_JD2str(T * 36525 + cs_J2000 + 8.0 / 24 - dt_T(T * 36525), str); mmString_Appends(s, str);
        mmString_Appends(s, csLunarOBB_jqmc[(i + 6) % 24]);

        if (i % 2 == 1)
        {
            mmString_Appends(s, " 视黄经");
            toFixed(str, i * 15, 0);
            ssz = strlen(str);
            mmString_RAlignAppends(s, str, ssz, 3);
            mmString_Appends(s, "\n");
        }
        else
        {
            mmString_Appends(s, " ");
        }
        if (i % 50 == 0)
        {
            mmString_Append(s, &s1);
            mmString_Assigns(&s1, "");
        }
    }

    mmString_Append(s, &s1);

    mmString_Destroy(&s1);
}

// 通常 n = 24 
// 定候测试函数
MM_EXPORT_SXWNL
void
houCalc(
    int y,
    int n,
    struct mmString* s)
{
    int i;
    double T;
    char str[32] = { 0 };
    struct mmString s1;

    y -= 2000;

    mmString_Reset(s);

    mmString_Init(&s1);

    mmString_Assigns(s, "初候                      二候                 三候");

    for (i = 0; i < n * 3; i++)
    {
        T = XL_S_aLon_t((y + i * 5 / 360.0 + 1) * 2 * cs_pi);//精确节气时间计算
        if (i % 3 == 0)
        {
            mmString_Appends(s, "\n");
            mmString_Appends(s, csLunarOBB_jqmc[(i / 3 + 6) % 24]);
        }
        else
        {
            mmString_Appends(s, " ");
        }

        //日期转为字串
        csJD_JD2str(T * 36525 + cs_J2000 + 8.0 / 24.0 - dt_T(T * 36525), str); mmString_Appends(s, str);

        if (i % 50 == 0)
        {
            mmString_Append(s, &s1);
            mmString_Assigns(&s1, "");
        }
    }

    mmString_Append(s, &s1);

    mmString_Destroy(&s1);
}

// 通常 y = 2000 N = 10
// 定气误差测试
MM_EXPORT_SXWNL
void
dingQi_cmp(
    int y,
    int N,
    struct mmString* s)
{
    int i;
    double W, T, maxT = 0;
    char str[32] = { 0 };

    mmString_Reset(s);

    y -= 2000;

    for (i = 0; i < N; i++)
    {
        W = (y + i / 24) * 2 * cs_pi;
        //节气粗算与精算的差异
        T = XL_S_aLon_t2(W) - XL_S_aLon_t(W);
        T = int2(fabs(T * 36525 * 86400));
        if (T > maxT) maxT = T;
    }

    toFixed(str, 2000 + y, 0); mmString_Appends(s, str);
    mmString_Appends(s, "年之后");
    toFixed(str, N, 0); mmString_Appends(s, str);
    mmString_Appends(s, "个朔日粗算与精算的最大差异:");
    toFixed(str, maxT, 0); mmString_Appends(s, str);
    mmString_Appends(s, "秒。");
}

// 通常 y = 2000 N = 10
// 定朔测试函数
MM_EXPORT_SXWNL
void
dingSuo_cmp(
    int y,
    int N,
    struct mmString* s)
{
    int i;
    int n;
    double T, maxT = 0, W;
    char str[32] = { 0 };

    mmString_Reset(s);

    y -= 2000;
    n = int2(y * (365.2422 / 29.53058886));//截止当年首经历朔望的个数
    for (i = 0; i < N; i++)
    {
        W = (n + i / 24.0) * 2 * cs_pi;
        T = XL_MS_aLon_t2(W) - XL_MS_aLon_t(W);//合塑粗算与精算的差异
        T = int2(fabs(T * 36525 * 86400));
        if (T > maxT) maxT = T;
    }

    toFixed(str, 2000 + y, 0); mmString_Appends(s, str);
    mmString_Appends(s, "年之后");
    toFixed(str, N, 0); mmString_Appends(s, str);
    mmString_Appends(s, "个朔日粗算与精算的最大差异:");
    toFixed(str, maxT, 0); mmString_Appends(s, str);
    mmString_Appends(s, "秒。");
}

// 定气计算速度测试
MM_EXPORT_SXWNL
void
dingQi_v(
    struct mmString* s)
{
    int i;
    mmUInt64_t d1, d2, d3;
    mmUInt64_t value;
    struct mmClock clock;
    char str[32] = { 0 };
    size_t ssz;

    mmString_Reset(s);

    mmClock_Init(&clock);

    d1 = mmClock_Microseconds(&clock);
    for (i = 0; i < 1000; i++)
        XL_S_aLon_t(0);
    d2 = mmClock_Microseconds(&clock);
    for (i = 0; i < 1000; i++)
        XL_S_aLon_t2(0);
    d3 = mmClock_Microseconds(&clock);

    mmString_Appends(s, "定气测试\n");
    mmString_Appends(s, "高精度:");
    value = (d2 - d1);
    mmValue_UInt64ToA(&value, str);
    ssz = strlen(str);
    mmString_RAlignAppends(s, str, ssz, 7);
    mmString_Appends(s, "微秒/千个\n");

    mmString_Appends(s, "低精度:");
    value = (d3 - d2);
    mmValue_UInt64ToA(&value, str);
    ssz = strlen(str);
    mmString_RAlignAppends(s, str, ssz, 7);
    mmString_Appends(s, "微秒/千个\n");

    mmClock_Destroy(&clock);
}

// 定朔计算速度测试
MM_EXPORT_SXWNL
void
dingSuo_v(
    struct mmString* s)
{
    int i;
    mmUInt64_t d1, d2, d3;
    mmUInt64_t value;
    struct mmClock clock;
    char str[32] = { 0 };
    size_t ssz;

    mmString_Reset(s);

    mmClock_Init(&clock);

    d1 = mmClock_Microseconds(&clock);
    for (i = 0; i < 1000; i++)
        XL_MS_aLon_t(0);
    d2 = mmClock_Microseconds(&clock);
    for (i = 0; i < 1000; i++)
        XL_MS_aLon_t2(0);
    d3 = mmClock_Microseconds(&clock);

    mmString_Appends(s, "定朔测试\n");
    mmString_Appends(s, "高精度:");
    value = (d2 - d1);
    mmValue_UInt64ToA(&value, str);
    ssz = strlen(str);
    mmString_RAlignAppends(s, str, ssz, 7);
    mmString_Appends(s, "微秒/千个\n");

    mmString_Appends(s, "低精度:");
    value = (d3 - d2);
    mmValue_UInt64ToA(&value, str);
    ssz = strlen(str);
    mmString_RAlignAppends(s, str, ssz, 7);
    mmString_Appends(s, "微秒/千个\n");

    mmClock_Destroy(&clock);
}

// 命理八字表
MM_EXPORT_SXWNL
void
ML_calc(
    const struct csJD* dat,
    double vJ,
    struct mmString* s)
{
    int day;
    struct mmString s1;
    struct csLunarMLBZ ob;
    char str[32] = { 0 };
    double jd = csJD_toJD(dat);

    mmString_Reset(s);

    mmString_Init(&s1);

    //八字计算
    csLunarOBB_mingLiBaZi(jd + (-8.0) / 24 - cs_J2000, vJ, &ob); 
    csLunarOBB_quanTianJiShiBiao(&ob, &s1);

    mmString_Appends(s, "[日标]：");
    mmString_Appends(s, "公历 ");

    mmValue_SIntToA(&dat->Y, str); mmString_Appends(s, str); 
    mmString_Appends(s, "-");
    mmValue_SIntToA(&dat->M, str); mmString_Appends(s, str); 
    mmString_Appends(s, "-");
    mmValue_SIntToA(&dat->D, str); mmString_Appends(s, str);
    
    mmString_Appends(s, " 儒略日数 ");
    day = int2(jd + 0.5);
    mmValue_SIntToA(&day, str); mmString_Appends(s, str);

    mmString_Appends(s, " 距2000年首");
    day = int2(jd + 0.5 - cs_J2000);
    mmValue_SIntToA(&day, str); mmString_Appends(s, str);
    mmString_Appends(s, "日\n");

    mmString_Appends(s, "[八字]：");
    mmString_Appends(s, csLunarOBB_Gan[ob.bz_jn[0]]); mmString_Appends(s, csLunarOBB_Zhi[ob.bz_jn[1]]);
    mmString_Appends(s, "年 ");
    mmString_Appends(s, csLunarOBB_Gan[ob.bz_jy[0]]); mmString_Appends(s, csLunarOBB_Zhi[ob.bz_jy[1]]);
    mmString_Appends(s, "月 ");
    mmString_Appends(s, csLunarOBB_Gan[ob.bz_jr[0]]); mmString_Appends(s, csLunarOBB_Zhi[ob.bz_jr[1]]);
    mmString_Appends(s, "日 ");
    mmString_Appends(s, csLunarOBB_Gan[ob.bz_js[0]]); mmString_Appends(s, csLunarOBB_Zhi[ob.bz_js[1]]);
    mmString_Appends(s, "时");

    mmString_Appends(s, " 真太阳 ");
    csJD_timeStr(ob.bz_zty, str); mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s, "[纪时]：");
    mmString_Append(s, &s1);
    mmString_Appends(s, "\n");

    mmString_Appends(s, "[时标]：");
    mmString_Appends(s, "23　 01　 03　 05　 07　 09　 11　 13　 15　 17　 19　 21　 23");
    mmString_Appends(s, "\n");

    mmString_Destroy(&s1);
}
