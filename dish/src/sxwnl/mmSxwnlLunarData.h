/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlLunarData_h__
#define __mmSxwnlLunarData_h__

#include "core/mmPlatform.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

//以下几个是公有只读数据
//中文数字
MM_EXPORT_SXWNL extern const char* csLunarOBB_numCn[];

//天干
MM_EXPORT_SXWNL extern const char* csLunarOBB_Gan[];

//地支
MM_EXPORT_SXWNL extern const char* csLunarOBB_Zhi[];

//十二生肖
MM_EXPORT_SXWNL extern const char* csLunarOBB_ShX[];

//星座
MM_EXPORT_SXWNL extern const char* csLunarOBB_XiZ[];

//月相名称表
MM_EXPORT_SXWNL extern const char* csLunarOBB_yxmc[];

//月相昵称表
MM_EXPORT_SXWNL extern const char* csLunarOBB_yxnc[];

//二十四节气
MM_EXPORT_SXWNL extern const char* csLunarOBB_jqmc[];

//月名称,建寅
MM_EXPORT_SXWNL extern const char* csLunarOBB_ymc[];

//月昵称,建寅
MM_EXPORT_SXWNL extern const char* csLunarOBB_ync[];

//日名称
MM_EXPORT_SXWNL extern const char* csLunarOBB_rmc[];

//星期名称
MM_EXPORT_SXWNL extern const char* csLunarOBB_xqmc[];

//星期昵称
MM_EXPORT_SXWNL extern const char* csLunarOBB_xqnc[];

//气候名称
MM_EXPORT_SXWNL extern const char* csLunarOBB_qhmc[];

// 气朔数据表
//朔直线拟合参数
MM_EXPORT_SXWNL extern const double csLunarSSQ_suoKB[];
MM_EXPORT_SXWNL extern const int csLunarSSQ_suoKBLength;

//气直线拟合参数
MM_EXPORT_SXWNL extern const double csLunarSSQ_qiKB[];
MM_EXPORT_SXWNL extern const int csLunarSSQ_qiKBLength;

MM_EXPORT_SXWNL extern const char* csLunarSSQ_suoS;
MM_EXPORT_SXWNL extern const char* csLunarSSQ_qiS;

#include "core/mmSuffix.h"

#endif//__mmSxwnlLunarData_h__
