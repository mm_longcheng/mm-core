/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlEph_h__
#define __mmSxwnlEph_h__

#include "core/mmPlatform.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"
#include "container/mmVectorF64.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

struct csJD;

struct csEphSJ
{
    double z;
    double x;
    double s;
    double j;
    double c;
    double h;
    double c2;
    double h2;
    double c3;
    double h3;
    double H0;
    double H;
    double H1;
    double H2;
    double H3;
    double H4;
    struct mmString sm;
};

MM_EXPORT_SXWNL
void
csEphSJ_Init(
    struct csEphSJ* p);

MM_EXPORT_SXWNL
void
csEphSJ_Destroy(
    struct csEphSJ* p);

struct csEphSJS
{
    struct mmString s;
    struct mmString z;
    struct mmString j;
    struct mmString c;
    struct mmString h;
    struct mmString ch;
    struct mmString sj;
    struct mmString Ms;
    struct mmString Mz;
    struct mmString Mj;
};

MM_EXPORT_SXWNL
void
csEphSJS_Init(
    struct csEphSJS* p);

MM_EXPORT_SXWNL
void
csEphSJS_Destroy(
    struct csEphSJS* p);

/****************************************
以下是天文计算部分,包含有：
物件 SZJ   : 用来计算日月的升起、中天、降落
注意，上述函数或物件是纯天文学的，根据实际需要组合使用可以得到所需要的各种日月坐标，计算精度及计算速度也是可以根据需要有效控制的。
*****************************************/

//=========日月升降物件=============

struct csEphSZJ
{
    // vector<struct csEphSJS>
    struct mmVectorValue rts;	//多天的升中降
    int dn;// rts n

    double L;	//站点地理经度,向东测量为正
    double fa;	//站点地理纬度
    double E;	//黄赤交角
    double dt;	//TD-UT
};

MM_EXPORT_SXWNL
void
csEphSZJ_Init(
    struct csEphSZJ* p);

MM_EXPORT_SXWNL
void
csEphSZJ_Destroy(
    struct csEphSZJ* p);

//h地平纬度,w赤纬,返回时角
MM_EXPORT_SXWNL
double 
csEphSZJ_getH(
    struct csEphSZJ* p, 
    double h, 
    double w);

//章动同时影响恒星时和天体坐标,所以不计算章动。返回时角及赤经纬
MM_EXPORT_SXWNL 
void 
csEphSZJ_Mcoord(
    struct csEphSZJ* p, 
    double jd, 
    double H0, 
    struct csEphSJ* r);

//月亮到中升降时刻计算,传入jd含义与St()函数相同
MM_EXPORT_SXWNL
void 
csEphSZJ_Mt(
    struct csEphSZJ* p, 
    double jd, 
    struct csEphSJ* r);

//章动同时影响恒星时和天体坐标,所以不计算章动。返回时角及赤经纬
MM_EXPORT_SXWNL
void 
csEphSZJ_Scoord(
    struct csEphSZJ* p, 
    double jd, 
    int xm, 
    struct csEphSJ* r);

//太阳到中升降时刻计算,传入jd是当地中午12点时间对应的2000年首起算的格林尼治时间UT
MM_EXPORT_SXWNL
void 
csEphSZJ_St(
    struct csEphSZJ* p, 
    double jd, 
    struct csEphSJ* r);

//多天升中降计算,jd是当地起始略日(中午时刻),sq是时区
MM_EXPORT_SXWNL
void
csEphSZJ_calcRTS(
    struct csEphSZJ* p,
    double jd,
    int n,
    double Jdl,
    double Wdl,
    double sq);

/********************
升降计算等
*********************/
//升降字符串
MM_EXPORT_SXWNL
void 
csEphSZJ_RTS1(
    struct csEphSZJ* p, 
    double jd, 
    double vJ, 
    double vW, 
    int tz, 
    struct mmString* s);

//====================升降表===================
//升降表字符串
MM_EXPORT_SXWNL
void 
csEphSZJ_shengjiang(
    struct csEphSZJ* p, 
    int y, 
    int m, 
    double d, 
    double vJ, 
    double vW, 
    struct mmString* s);

//太阳升降快算
MM_EXPORT_SXWNL
void 
csEphSZJ_shengjiang2(
    int y, 
    double vJ, 
    double vW, 
    struct mmString* s);

//年度时差
MM_EXPORT_SXWNL
void 
csEphSZJ_shengjiang3(
    int y, 
    struct mmString* s);

//行星的距角,jing为精度控
MM_EXPORT_SXWNL
double 
xingJJ(
    int xt, 
    double t, 
    int jing);

//大距计算超底速算法, dx=1东大距,t儒略世纪TD
MM_EXPORT_SXWNL
void 
daJu(
    double re[2], 
    int xt, 
    double t, 
    int dx);

//行星的视坐标
MM_EXPORT_SXWNL
void 
xingLiu0(
    double r[3], 
    int xt, 
    double t, 
    int n, 
    double gxs);

//留,sn=1顺留
MM_EXPORT_SXWNL
double 
xingLiu(
    int xt, 
    double t, 
    int sn);

//合月计算
//月亮行星视赤经差
MM_EXPORT_SXWNL
void 
xingMP(
    double re[4], 
    int xt, 
    double t, 
    int n, 
    double E, 
    const double g[4]);

//行星合月(视赤经),t儒略世纪TD
MM_EXPORT_SXWNL
void 
xingHY(
    double re[2], 
    int xt, 
    double t);

//合冲日计算(视黄经合冲)
//行星太阳视黄经差与w0的差
MM_EXPORT_SXWNL
void 
xingSP(
    double re[4], 
    int xt, 
    double t, 
    int n, 
    double w0, 
    double ts, 
    double tp);

//xt星体号,t儒略世纪TD,f=1求冲(或下合)否则求合(或下合)
MM_EXPORT_SXWNL
void 
xingHR(
    double re[2], 
    int xt, 
    double t, 
    int f);

//星历计算
//行星计算,jd力学时
MM_EXPORT_SXWNL
void 
xingX(
    struct mmString* s, 
    int xt, 
    double jd, 
    double L, 
    double fa);

struct csEphCoord
{
    double x;
    double y;
    double z;
    double R1;
    double R2;
    double D;
    double X;
    double J;
    double W;
};

struct csEphNode
{
    double A[3];
    double B[3];
    double R1;
    double R2;
    int n;
};

//========太阳月亮计算类=============
struct csEphMSC
{
    double T;	//TD力学时
    double L;	//地理纬度
    double fa;	//地理经度
    double dt;	//力学-世界时时差
    double jd;	//UT世界时
    double dL;	//黄经章动
    double dE;	//黄纬章动
    double E;	//交角章动
    double gst;	//真恒星时

    double mHJ;	//月球视黄经
    double mHW;	//月球视黄纬
    double mR;	//地月质心距
    double mCJ;	//月球视赤经
    double mCW;	//月球视赤纬
    double mShiJ;	//月球时角

    double mCJ2;	//时差修正后的赤道坐标
    double mCW2;
    double mR2;
    double mDJ;	//高度角
    double mDW;	//方位角
    double mPJ;	//大气折射修正后的高度角
    double mPW;	//大气折射修正后的方位角

    double sHJ;	//太阳视黄经
    double sHW;	//太阳视黄纬
    double sCJ;	//太阳视赤经
    double sCW;	//太阳视赤纬
    double sCJ2;	//时差修正后的赤道坐标
    double sCW2;
    double sR2;
    double sShiJ;	//太阳时角

    double sDJ;	//高度角
    double sDW;	//方位角
    double sR;
    double sPJ;	//方位角
    double sPW;	//高度角
    double sc;	//时差

    double pty;	//平恒星时
    double zty;	//真恒星时
    double mRad;	//月亮视半径
    double sRad;	//太阳视半径
    double e_mRad;	//月亮地心视半径
    double eShadow;	//地本影在月球向径处的半径(角秒)
    double eShadow2;	//地半影在月球向径处的半径(角秒)
    double mIll;	//月面被照亮比例
    double zx_J;	//中心食坐标
    double zx_W;
};

//========日月食计算使用的一些函数=============
//求空间两点连线与地球的交点(靠近点x1的交点)
MM_EXPORT_SXWNL
void 
lineEll(
    struct csEphCoord* p, 
    double x1, double y1, 
    double z1, 
    double x2, double y2, 
    double z2, 
    double e, 
    double r);

//I是贝塞尔坐标参数
MM_EXPORT_SXWNL
void 
lineEar2(
    struct csEphCoord* p, 
    double x1, double y1, 
    double z1, 
    double x2, double y2, 
    double z2, 
    double e, 
    double r, 
    const double I[3]);

//在分点坐标中求空间两点连线与地球的交点(靠近点P的交点),返回地标
MM_EXPORT_SXWNL
void 
lineEar(
    struct csEphCoord* r, 
    const double P[3], 
    const double Q[3], 
    double gst);

//椭圆与圆的交点,R椭圆长半径,R2圆半径,x0,y0圆的圆心
MM_EXPORT_SXWNL
void 
cirOvl(
    struct csEphNode* re, 
    double R, 
    double ba, 
    double R2, 
    double x0, double y0);

MM_EXPORT_SXWNL
void 
lineOvl(
    struct csEphNode* p, 
    double x1, double y1, 
    double dx, double dy, 
    double r, 
    double ba);

//sun_moon类的成员函数。参数：T是力学时,站点经纬L,fa,海拔high(千米)
MM_EXPORT_SXWNL
void 
csEphMSC_calc(
    struct csEphMSC* p, 
    double T, 
    double L, 
    double fa, 
    double high);

MM_EXPORT_SXWNL
void 
csEphMSC_toString(
    const struct csEphMSC* p, 
    int fs, 
    struct mmString* s);

struct csEphYsData
{
    double e_mRad;
    double eShadow;
    double eShadow2;
    double x;
    double y;
    double mr;
    double er;
    double Er;
    double t;
};

//月食快速计算器
struct csEphYsPL
{
    struct mmString LX;
    double lT[7];
    double sf;
};

MM_EXPORT_SXWNL
void
csEphYsPL_Init(
    struct csEphYsPL* p);

MM_EXPORT_SXWNL
void
csEphYsPL_Destroy(
    struct csEphYsPL* p);

//已知t1时刻星体位置、速度，求x*x+y*y=r*r时,t的值
MM_EXPORT_SXWNL
double 
csEphYsPL_lineT(
    struct csEphYsData* G, 
    double v, 
    double u, 
    double r, 
    int n);

//日月黄经纬差转为日面中心直角坐标(用于月食)
MM_EXPORT_SXWNL
void 
csEphYsPL_lecXY(
    double jd, 
    struct csEphYsData* re);

//月食的食甚计算(jd为近朔的力学时,误差几天不要紧)
MM_EXPORT_SXWNL
void 
csEphYsPL_lecMax(
    struct csEphYsPL* p, 
    double jd);

//====================================
/*****
ecFast()函数返回参数说明
r.jdSuo 朔时刻
r.lx    日食类型
*****/
struct csEphRsData
{
    struct mmString lx;
    double jd;
    double jdSuo;
    double ac;
};

MM_EXPORT_SXWNL
void
csEphRsData_Init(
    struct csEphRsData* p);

MM_EXPORT_SXWNL
void
csEphRsData_Destroy(
    struct csEphRsData* p);

//快速日食搜索,jd为朔时间(J2000起算的儒略日数,不必很精确)
MM_EXPORT_SXWNL
void 
ecFast(
    struct csEphRsData* re, 
    double jd);

struct csEphVXY
{
    double vx;
    double vy;
    double Vx;
    double Vy;
    double V;
};

struct csEphRSM
{
    double r1;
    double r2;
    double ar2;
    double sf;
};

struct csEphVecFLAG
{
    struct mmVectorF64 L;
    int f;
    int f2;
};

MM_EXPORT_SXWNL
void
csEphVecFLAG_Init(
    struct csEphVecFLAG* p);

MM_EXPORT_SXWNL
void
csEphVecFLAG_Destroy(
    struct csEphVecFLAG* p);

MM_EXPORT_SXWNL
void
csEphVecFLAG_Reset(
    struct csEphVecFLAG* p);

struct csEphFeature
{
    double jdSuo;
    double dT;
    double ds;
    double vx;
    double vy;
    double ax;
    double ay;
    double v;
    double k;

    double t0;
    double jd;
    double xc;
    double yc;
    double zc;
    double D;
    double d;
    double I[3];
    double gk1[3];
    double gk2[3];
    double gk3[3];
    double gk4[3];
    double gk5[3];
    struct mmString lx;

    double zxJ;
    double zxW;
    double dw;
    double sf;
    double tt;
    double Sdp[3];

    struct mmVectorF64 p1;
    struct mmVectorF64 p2;
    struct mmVectorF64 p3;
    struct mmVectorF64 p4;
    struct mmVectorF64 q1;
    struct mmVectorF64 q2;
    struct mmVectorF64 q3;
    struct mmVectorF64 q4;

    struct csEphVecFLAG L0;//中心线
    struct csEphVecFLAG L1;
    struct csEphVecFLAG L2;
    struct csEphVecFLAG L3;
    struct csEphVecFLAG L4;
    struct csEphVecFLAG L5;//0.5食分线
    struct csEphVecFLAG L6;//0.5食分线
};

MM_EXPORT_SXWNL
void
csEphFeature_Init(
    struct csEphFeature* p);

MM_EXPORT_SXWNL
void
csEphFeature_Destroy(
    struct csEphFeature* p);

MM_EXPORT_SXWNL
void
csEphFeature_ResetVector(
    struct csEphFeature* p);

struct csEphJIEX2
{
    struct mmVectorF64 p1;
    struct mmVectorF64 p2;
    struct mmVectorF64 p3;
};

MM_EXPORT_SXWNL
void
csEphJIEX2_Init(
    struct csEphJIEX2* p);

MM_EXPORT_SXWNL
void
csEphJIEX2_Destroy(
    struct csEphJIEX2* p);

struct csEphRsGS
{
    struct mmVectorF64 Zs; //日月赤道坐标插值表
    double Zdt;            //插值点之间的时间间距
    double Zjd;            //插值表中心时间
    double dT;             //deltatT
    double tanf1;          //半影锥角
    double tanf2;          //本影锥角
    double srad;           //太阳视半径
    double bba;            //贝圆极赤比
    double bhc;            //黄交线与赤交线的夹角简易作图用
    double dyj;            //地月距
};

MM_EXPORT_SXWNL
void
csEphRsGS_Init(
    struct csEphRsGS* p);

MM_EXPORT_SXWNL
void
csEphRsGS_Destroy(
    struct csEphRsGS* p);

//创建插值表(根数表)
MM_EXPORT_SXWNL
void 
csEphRsGS_initialize(
    struct csEphRsGS* p, 
    double jd, 
    int n);

//日月坐标快速计算(贝赛尔插值法),计算第p个根数开始的m个根数
MM_EXPORT_SXWNL
void 
csEphRsGS_chazhi(
    const struct csEphRsGS* p, 
    double jd, 
    int xt, 
    double z[3]);

//传回值可能超过360度
MM_EXPORT_SXWNL
void 
csEphRsGS_sun(
    const struct csEphRsGS* p, 
    double jd, 
    double z[3]);

MM_EXPORT_SXWNL
void 
csEphRsGS_moon(
    const struct csEphRsGS* p, 
    double jd, 
    double z[3]);

MM_EXPORT_SXWNL
void 
csEphRsGS_bse(
    const struct csEphRsGS* p, 
    double jd, 
    double z[3]);

//赤道转贝塞尔坐标
MM_EXPORT_SXWNL
void 
csEphRsGS_cd2bse(
    double r[3], 
    const double z[3], 
    const double I[3]);

//贝塞尔转赤道坐标
MM_EXPORT_SXWNL
void 
csEphRsGS_bse2cd(
    double r[3], 
    const double z[3], 
    const double I[3]);

//贝赛尔转地标(p点到原点连线与地球的交点,z为p点直角坐标),f=1时把地球看成椭球
MM_EXPORT_SXWNL
void 
csEphRsGS_bse2db(
    double r[3], 
    const double z[3], 
    const double I[3], 
    int f);

//贝赛尔转地标(过p点垂直于基面的线与地球的交点,p坐标为(x,y,任意z)),f=1时把地球看成椭球
MM_EXPORT_SXWNL
void 
csEphRsGS_bseXY2db(
    double r[2], 
    double x, double y, 
    const double I[3], 
    int f);

//月亮的贝塞尔坐标
MM_EXPORT_SXWNL
void 
csEphRsGS_bseM(
    const struct csEphRsGS* p, 
    double a[3], 
    double jd);

//以下计算日食总体情况
//地球上一点的速度，用贝塞尔坐标表达，s为贝赤交角
MM_EXPORT_SXWNL
void 
csEphRsGS_Vxy(
    struct csEphVXY* r, 
    double x, double y, 
    double s, 
    double vx, double vy);

//rm,rs单位千米
MM_EXPORT_SXWNL
void 
csEphRsGS_rSM(
    const struct csEphRsGS* p, 
    struct csEphRSM* re, 
    double mR);

//求切入点
MM_EXPORT_SXWNL
void 
csEphRsGS_qrd(
    const struct csEphRsGS* p, 
    double re[3], 
    double jd, 
    double dx, double dy, 
    int fs);

//日食的基本特征
MM_EXPORT_SXWNL
void 
csEphRsGS_feature(
    const struct csEphRsGS* p, 
    struct csEphFeature* re, 
    double jd);

//界线图
MM_EXPORT_SXWNL
void 
csEphRsGS_push(
    const double z[3], 
    struct mmVectorF64* p);

//数据元素复制
MM_EXPORT_SXWNL
void 
csEphRsGS_elmCpy(
    struct mmVectorF64* a, int n, 
    struct mmVectorF64* b, int m);

//vx0,vy0为影足速度(也是整个影子速度),h=1计算北界,h=-1计算南界
MM_EXPORT_SXWNL
void 
csEphRsGS_nanbei(
    double re[4], 
    const double M[3], 
    double vx0, double vy0, 
    double h, 
    double r, 
    const double I[3]);

//vx0,vy0为影足速度(也是整个影子速度),h=1计算北界,h=-1计算南界
MM_EXPORT_SXWNL
void 
csEphRsGS_mQie(
    const struct csEphRsGS* q, 
    const double M[3], 
    double vx0, double vy0, 
    double h, 
    double r, 
    const double I[3], 
    struct csEphVecFLAG* A);

//日出日没食甚
MM_EXPORT_SXWNL
int 
csEphRsGS_mDian(
    const struct csEphRsGS* q, 
    const double M[3], 
    double vx0, double vy0, 
    int AB, 
    double r, 
    const double I[3], 
    struct mmVectorF64* A);

//日出日没的初亏食甚复圆线，南北界线等
MM_EXPORT_SXWNL
void 
csEphRsGS_jieX(
    struct csEphRsGS* q, 
    double jd, 
    struct csEphFeature* re);

//jd力学时
MM_EXPORT_SXWNL
void 
csEphRsGS_jieX2(
    struct csEphRsGS* q, 
    double jd, 
    struct csEphJIEX2* re);

//界线表
MM_EXPORT_SXWNL
void 
csEphRsGS_jieX3(
    const struct csEphRsGS* q,
    double jd, 
    struct csEphFeature* re, 
    struct mmString* s);

struct csEphSECXY
{
    double mCJ;
    double mCW;
    double mR;
    double mCJ2;
    double mCW2;
    double mR2;

    double sCJ;
    double sCW;
    double sR;
    double sCJ2;
    double sCW2;
    double sR2;

    double mr;
    double sr;
    double x;
    double y;
    double t;
};

struct csEphZB
{
    double S[3];
    double M[3];
    double sr;
    double mr;
    double x;
    double y;
    double g;
};

struct csEphGJW
{
    struct mmString c;
    double J;
    double W;
};

MM_EXPORT_SXWNL
void
csEphGJW_Init(
    struct csEphGJW* p);

MM_EXPORT_SXWNL
void
csEphGJW_Destroy(
    struct csEphGJW* p);

struct csEphRsPL
{
    int nasa_r;//为1表示采用NASA的视径比
    double sT[5];//地方日食时间表
    struct mmString LX;
    struct mmString sflx;
    double sf;
    double sf2;//食分(日出食分)
    double sf3;//食分(日没食分)
    double b1;
    double dur;
    double sun_s;
    double sun_j;
    double P1;
    double V1;
    double P2;
    double V2;

    //以下涉及南北界计算
    double A[3];
    double B[3]; //本半影锥顶点坐标
    struct csEphZB P;//t1时刻的日月坐标,g为恒星时
    struct csEphZB Q;//t2时刻的日月坐标
    double V[10];//食界表
    struct mmString Vc;
    struct mmString Vb;  //食中心类型,本影南北距离
};

MM_EXPORT_SXWNL
void
csEphRsPL_Init(
    struct csEphRsPL* p);

MM_EXPORT_SXWNL
void
csEphRsPL_Destroy(
    struct csEphRsPL* p);

//日月xy坐标计算。参数：jd是力学时,站点经纬L,fa,海拔high(千米)
MM_EXPORT_SXWNL
void 
csEphRsPL_secXY(
    struct csEphRsPL* p, 
    const struct csEphRsGS* q, 
    double jd, 
    double L, 
    double fa, 
    double high, 
    struct csEphSECXY* re);

//已知t1时刻星体位置、速度，求x*x+y*y=r*r时,t的值
MM_EXPORT_SXWNL
double 
csEphRsPL_lineT(
    const struct csEphSECXY* G, 
    double v, 
    double u, 
    double r, 
    int n);

//日食的食甚计算(jd为近朔的力学时,误差几天不要紧)
MM_EXPORT_SXWNL
void 
csEphRsPL_secMax(
    struct csEphRsPL* p, 
    struct csEphRsGS* q, 
    double jd, 
    double L, 
    double fa, 
    double high);

MM_EXPORT_SXWNL
void 
csEphRsPL_zb0(
    struct csEphRsPL* p, 
    struct csEphRsGS* q, 
    double jd);

MM_EXPORT_SXWNL
void 
csEphRsPL_zbXY(
    struct csEphZB* p, 
    double L, 
    double fa);

//f取+-1
MM_EXPORT_SXWNL
void 
csEphRsPL_p2p(
    struct csEphRsPL* o, 
    double L, 
    double fa, 
    struct csEphGJW* re, 
    int fAB, 
    int f);

//食中心点计算
MM_EXPORT_SXWNL
void 
csEphRsPL_pp0(
    struct csEphRsPL* o, 
    struct csEphGJW* re);

//南北界计算
MM_EXPORT_SXWNL
void 
csEphRsPL_nbj(
    struct csEphRsPL* p, 
    struct csEphRsGS* q, 
    double jd);

//获取日食类型名字
MM_EXPORT_SXWNL
const char* 
csEphRsPL_GetRSLXName(
    const char* lx);

//日食字符串
MM_EXPORT_SXWNL
void 
csEphRsPL_rysCalc(
    const struct csJD* d, 
    double vJ, double vW, 
    int is_utc, 
    int nasa_r, 
    struct mmString* s);

//查找日食
MM_EXPORT_SXWNL
void 
csEphRsPL_rsSearch(
    int Y, int M, 
    int n, 
    int fs, 
    struct mmString* s);

//日食表字符串
MM_EXPORT_SXWNL
void 
csEphRsPL_rs2Calc(
    int fs, 
    double jd0, 
    double step, 
    int bn, 
    struct mmString* s);

//显示界线表
MM_EXPORT_SXWNL
void 
csEphRsPL_rs2Jxb(
    double jd, 
    struct mmString* s);

#include "core/mmSuffix.h"

#endif//__mmSxwnlEph_h__
