/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlLunarJNB_h__
#define __mmSxwnlLunarJNB_h__

#include "core/mmPlatform.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

struct mmString;

// 纪年数据结构
// 起始公元,使用年数,已用年数,朝代,朝号,皇帝,年号
struct csLunarOBBJN
{
    int qsgy;            // 起始公元
    int syns;            // 使用年数
    int yyns;            // 已用年数
    const char* cd;      // 朝代
    const char* ch;      // 朝号
    const char* hd;      // 皇帝
    const char* nh;      // 年号
};

MM_EXPORT_SXWNL extern const struct csLunarOBBJN csLunarOBB_JNB[];
MM_EXPORT_SXWNL extern const int csLunarOBB_JNBLength;

// 取年号
MM_EXPORT_SXWNL
void 
csLunarOBB_getNH(
    int y, 
    struct mmString* s);

// 取纪年
MM_EXPORT_SXWNL
const struct csLunarOBBJN*
csLunarOBB_getJN(
    int y);


#include "core/mmSuffix.h"

#endif//__mmSxwnlLunarJNB_h__
