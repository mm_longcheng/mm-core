/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlLunar_h__
#define __mmSxwnlLunar_h__

#include "core/mmPlatform.h"
#include "core/mmString.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

struct csJD;

/**************************
寿星万年历中日对象的标准定义
假设日对象的数据结构为ob
·日的公历信息
  ob.d0   2000.0起算儒略日,北京时12:00
  ob.di   所在公历月内日序数
  ob.y    所在公历年,同lun.y
  ob.m    所在公历月,同lun.m
  ob.d    日名称(公历)
  ob.dn   所在公历月的总天数,同lun.d0
  ob.week0所在月的月首的星期,同lun.w0
  ob.week 星期
  ob.weeki在本月中的周序号
  ob.weekN本月的总周数
·日的农历信息
  ob.Ldi  距农历月首的编移量,0对应初一
  ob.Ldc  日名称(农历),即'初一,初二等'
  ob.cur_dz 距冬至的天数
  ob.cur_xz 距夏至的天数
  ob.cur_lq 距立秋的天数
  ob.cur_mz 距芒种的天数
  ob.cur_xs 距小暑的天数
  ob.Lmc  月名称
  ob.Ldn  月大小
  ob.Lleap闰状况(值为'闰'或空串)
  ob.Lmc2 下个月名称,判断除夕时要用到
·日的农历纪年、月、日、时及星座
  ob.Lyear  农历纪年(10进制,1984年起算,分界点可以是立春也可以是春节,在程序中选择一个)
  ob.Lyear2 干支纪年
  ob.Lmonth  纪月处理,1998年12月7日(大雪)开始连续进行节气计数,0为甲子
  ob.Lmonth2 干支纪月
  ob.Lday2  纪日
  ob.Ltime2 纪时
  ob.XiZ 星座
·日的回历信息
  ob.Hyear  年(回历)
  ob.Hmonth 月(回历)
  ob.Hday   日(回历)
·日的其它信息
  ob.yxmc 月相名称
  ob.yxjd 月相时刻(儒略日)
  ob.yxsj 月相时间串
  ob.jqmc 节气名称
  ob.jqjd 节气时刻(儒略日)
  ob.jqsj 节气时间串
*****************************/

/*****************************
寿星万年历中月对象的数据结构定义
假设月对象的名称为lun
  lun.w0  本月第一天的星期
  lun.y   公历年份
  lun.m   公历月分
  lun.d0  月首的J2000.0起算的儒略日数
  lun.dn  本月的天数
  lun.Ly  该年的干支纪年
  lun.ShX 该年的生肖
  lun[0]  该月第1天(日对象)
  lun[1]  该月第2天(日对象)
  ……
*****************************/

/******************************
以下程序严格尊守以上规范，即创建的月(或日)对象，必含以上数据结构内容。寿星万年历通过Lunar类件得到月对象
月或月对象的数据结构的扩充：例如调用oba.getDayName(ob,ob)，可扩展ob对象的“节日信息”，返回值在ob.A、ob.B、ob.C中
根据某些需要，用户还可以自行扩展ob
******************************/


//=============================
//公历基础构件,JD物件的补充
//public中定义的成员可以直接使用
//=============================

struct csLunarOBDay
{
    /* 日的公历信息 */
    int d0;		            // 2000.0起算儒略日,北京时12:00
    int di;		            // 所在公历月内日序数
    int y;                  // 所在公历年,同lun.y
    int m;                  // 所在公历月,同lun.m
    int d; 		            // 日名称(公历)
    int dn;                 // 所在公历月的总天数,同lun.d0
    int week0;              // 所在月的月首的星期,同lun.w0
    int week;  	            // 星期
    int weeki; 	            // 在本月中的周序号
    int weekN; 	            // 本月的总周数
    /* 日的农历信息 */
    int Ldi;	            // 日距农历月首偏移
    struct mmString Ldc;    // 日名称(农历),即'初一,初二等'
    int cur_dz;             // 冬至的天数
    int cur_xz;             // 夏至的天数
    int cur_lq;             // 立秋的天数
    int cur_mz;             // 芒种的天数
    int cur_xs;             // 小暑的天数
    int Lmi;	            // 农历月索引, 与月名称对应
    struct mmString Lmc;    // 月名称
    struct mmString Lmc2;   // 下个月名称,判断除夕时要用到
    int Ldn;   		        // 月大小
    struct mmString Lleap; 	// 闰状况 
    /* 日的农历纪年、月、日、时及星座 */
    int Lyear;		 	    // 农历纪年(10进制,1984年起算,分界点可以是立春也可以是春节,在程序中选择一个)
    int Lyear0;             
    struct mmString Lyear2;	// 干支纪年
    struct mmString Lyear3;	// 干支纪年(春节)
    int Lyear4;			    // 干支纪年(黄帝纪元)
    int Lmonth;			    // 纪月处理,1998年12月7日(大雪)开始连续进行节气计数,0为甲子
    int Lgzjn; 	            // 干支纪年(总数, 实际使用需要手动取模)
    int Lgzjy; 	            // 干支纪月(总数, 实际使用需要手动取模)相对于1998年12月7(大雪)的月数
    int Lgzjr; 	            // 干支纪日(总数, 实际使用需要手动取模)2000年1月7日起算
    int Ljqid;              // 节气编号(-1表示无节气)
    int XiZid;              // 星座编号
    struct mmString Lmonth2;// 干支纪月
    struct mmString Lday2; 	// 纪日
    struct mmString Ltime2;	// 纪时
    struct mmString Ljq;	// 节气
    struct mmString XiZ;   	// 星座
    /* 日的回历信息 */
    int Hyear;	 	        // 年(回历)
    int Hmonth;		        // 月(回历)
    int Hday;	  	        // 日(回历)
    /* 日的其它信息 */
    struct mmString yxmc;	// 月相名称
    double yxjd;   	        // 月相时刻(儒略日)
    struct mmString yxsj;	// 月相时间串
    int yxid;               // 月相索引(-1表示无月相)
    struct mmString jqmc;	// 节气名称
    double jqjd;	        // 节气时刻(儒略日)
    struct mmString jqsj;	// 节气时间串
    int jqid;               // 节气索引(-1表示无节气)
    /* 节日信息 */
    int Fjia;
    struct mmString A;      // #表示放假日
    struct mmString B;      // I表示重要节日或纪念日
    struct mmString C;      // .表示普通节日
};

MM_EXPORT_SXWNL
void
csLunarOBDay_Init(
    struct csLunarOBDay* p);

MM_EXPORT_SXWNL
void
csLunarOBDay_Destroy(
    struct csLunarOBDay* p);

MM_EXPORT_SXWNL
void
csLunarOBDay_Assign(
    struct csLunarOBDay* p,
    const struct csLunarOBDay* q);

MM_EXPORT_SXWNL
void 
csLunarOBA_ResetDayFestival(
    struct csLunarOBDay* r);

MM_EXPORT_SXWNL
void 
csLunarOBA_AppendDayFestival(
    struct csLunarOBDay* r, 
    int type, 
    const char* f);

MM_EXPORT_SXWNL
void
csLunarOBA_FestivalDayName(
    int y, int m, int d,
    const char*** sFtvTable,
    const int* sFtvLength,
    struct csLunarOBDay* r);

// 取某日节日,传入日对象
MM_EXPORT_SXWNL
void 
csLunarOBA_getDayName(
    const struct csLunarOBDay* u, 
    struct csLunarOBDay* r);

//回历计算
MM_EXPORT_SXWNL
void 
csLunarOBA_getHuiLi(
    double d0, 
    struct csLunarOBDay* r);

MM_EXPORT_SXWNL
void
csLunarOBB_FestivalDayName(
    int y, int m, int d,
    const char*** nFtvTable,
    const int* nFtvLength,
    struct csLunarOBDay* r);

//计算农历节日
MM_EXPORT_SXWNL
void 
csLunarOBB_getDayName(
    const struct csLunarOBDay* u, 
    struct csLunarOBDay* r);

struct csLunarMLBZ
{
    int bz_jn[2]; //干支纪年
    int bz_jy[2]; //干支纪月
    int bz_jr[2]; //干支纪日
    int bz_js[2]; //干支纪时
    double bz_zty; //干支真太阳时
    int bz_rs;     //日数, 真太阳时转化的日数
    int bz_sc;     //时辰, 指定时间处于的时辰
};

//命理八字计算。jd为格林尼治UT(J2000起算),J为本地经度(弧度),返回在物件ob中
MM_EXPORT_SXWNL
void 
csLunarOBB_mingLiBaZi(
    double jd, 
    double J, 
    struct csLunarMLBZ* ob);

//全天纪时表
MM_EXPORT_SXWNL
void 
csLunarOBB_quanTianJiShiBiao(
    const struct csLunarMLBZ* ob, 
    struct mmString* s);

//精气
MM_EXPORT_SXWNL
double 
csLunarOBB_qi_accurate(
    double W);

//精朔
MM_EXPORT_SXWNL
double 
csLunarOBB_so_accurate(
    double W);

//精气
MM_EXPORT_SXWNL
double 
csLunarOBB_qi_accurate2(
    double jd);

//精朔
MM_EXPORT_SXWNL
double 
csLunarOBB_so_accurate2(
    double jd);

struct csLunarSSQ
{
    struct mmString SB;//定朔修正表解压
    struct mmString QB;//定气修正表解压

    char ym[14][16];//各月名称

    int leap;  //闰月位置
    int yi[14];//各月索引
    int yv[14];//各月索引, 订正的索引, 与月名称对于
    int ZQ[25];//中气表
    int HS[15];//合朔表
    int dx[14];//各月大小
    int pe[2]; //补算二气
};

MM_EXPORT_SXWNL
void
csLunarSSQ_Init(
    struct csLunarSSQ* p);

MM_EXPORT_SXWNL
void
csLunarSSQ_Destroy(
    struct csLunarSSQ* p);

//低精度定朔计算,在2000年至600，误差在2小时以内(仍比古代日历精准很多)
MM_EXPORT_SXWNL
double 
csLunarSSQ_so_low(
    double W);

//最大误差小于30分钟，平均5分
MM_EXPORT_SXWNL
double 
csLunarSSQ_qi_low(
    double W);

//较高精度气
MM_EXPORT_SXWNL
double 
csLunarSSQ_qi_high(
    double W);

//较高精度朔
MM_EXPORT_SXWNL
double 
csLunarSSQ_so_high(
    double W);

//气朔解压缩
MM_EXPORT_SXWNL
void 
csLunarSSQ_jieya(
    struct mmString* s);

//初使用化
MM_EXPORT_SXWNL
void 
csLunarSSQ_initialize(
    struct csLunarSSQ* p);

enum csQSType
{
    csQSType_Q = 0, // 气 (int)-14600
    csQSType_S = 1, // 朔 (int)-13385
};

//jd应靠近所要取得的气朔日,qs='气'时，算节气的儒略日, 0气1朔.
MM_EXPORT_SXWNL
int 
csLunarSSQ_calc(
    struct csLunarSSQ* p, 
    double jd, 
    enum csQSType qs);

//农历排月序计算,可定出农历,有效范围：两个冬至之间(冬至一 <= d < 冬至二)
MM_EXPORT_SXWNL
void 
csLunarSSQ_calcY(
    struct csLunarSSQ* p, 
    double jd);

/*********************************
以下是月历表的具体实现方法
*********************************/


/*********************************
=====以下是公历、农历、回历综合日历计算类=====

  Lunar：日历计算物件
  使用条件：事先引用eph.js天文历算文件

  实例创建： var lun = new Lunar();
一、 yueLiCalc(By,Bm)方法
·功能：算出该月每日的详信息
·入口参数：
  By是年(公历)
  Bm是月(公历)
·返回：
  lun.w0= (Bd0 + J2000 +1)%7; //本月第一天的星期
  lun.y  公历年份
  lun.m  公历月分
  lun.d0 月首儒略日数
  lun.dn 月天数
  lun.Ly   干支纪年
  lun.ShX  该年对应的生肖
  lun.nianhao 年号纪年
  lun.lun[] 各日信息(对象),日对象尊守此脚本程序开始的注释中定义

二、yueLiHTML(By,Bm)方法
·功能：算出该月每日的详细信息，并给出HTML月历表
·入口参数：
  By是年(公历)
  Bm是月(公历)
·返回：
  yueLiCalc(By,Bm)返回的信息
  lun.pg0 年份信息
  lun.pg1 月历表
  lun.pg2 月相节气表

**********************************/

struct csLunarLun
{
    int w0;			          // 本月第一天的星期
    int y;		 	          // 公历年份
    int m;		 	          // 公历月分
    int d0;			          // 月首的J2000.0起算的儒略日数
    int dn;			          // 本月的天数

    int Lgzjn;                // 干支纪年(所属公历年对应的农历干支纪年)农历年和公历年算法不一样. 这个值其实意义不大.
    struct mmString Ly;       // 干支纪年
    struct mmString ShX;      // 该年对应的生肖

    struct mmString nianhao;  // 年号纪年信息

    struct csLunarOBDay day[31];
};

MM_EXPORT_SXWNL
void
csLunarLun_Init(
    struct csLunarLun* p);

MM_EXPORT_SXWNL
void
csLunarLun_Destroy(
    struct csLunarLun* p);

//返回公历某一个月的'公农回'三合历
MM_EXPORT_SXWNL
void 
csLunarLun_yueLiCalc(
    struct csLunarLun* p, 
    struct csLunarSSQ* SSQ, 
    int By, 
    int Bm);

//String年历生成
MM_EXPORT_SXWNL
void 
csLunarLun_nianLiString(
    struct csLunarSSQ* SSQ, 
    int y, 
    const char* fg, 
    struct mmString* s);

//String年历生成
MM_EXPORT_SXWNL
void 
csLunarLun_nianLi2String(
    struct csLunarSSQ* SSQ, 
    int y, 
    struct mmString* s);

// 月相表
MM_EXPORT_SXWNL
void 
csLunarLun_yxbString(
    struct csLunarLun* p, 
    struct mmString* s);


//用公历年和气候索引求时间，索引取模
MM_EXPORT_SXWNL
void
csQH_JDM(
    int Y,
    int H,
    struct csJD* pJD);

//用公历年和气候索引求时间，索引顺延
MM_EXPORT_SXWNL
void
csQH_JDX(
    int Y,
    int H,
    struct csJD* pJD);

//粗略算某天的气候索引
MM_EXPORT_SXWNL
int
csQH_IndexByJDE(
    struct csJD* pJD);

//获取某天的气候索引, 返回-1表示没有搜索到
MM_EXPORT_SXWNL
int
csQH_IndexByYMD(
    int Y,
    int M,
    int D,
    struct csJD* pJD);

#include "core/mmSuffix.h"

#endif//__mmSxwnlLunar_h__
