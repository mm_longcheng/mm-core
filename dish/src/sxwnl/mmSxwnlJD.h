/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlJD_h__
#define __mmSxwnlJD_h__

#include "core/mmPlatform.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

// 日期元件
struct csJD
{
    int Y;// 年
    int M;// 月
    int D;// 日
    int h;// 时
    int m;// 分
    double s;// 秒
};

MM_EXPORT_SXWNL
void 
csJD_Make(
    struct csJD* p, 
    int Y, int M, int D, 
    int h, int m, double s);

//公历转儒略日
MM_EXPORT_SXWNL
double 
csJD_JD(
    int y, 
    int m, 
    double d);

//儒略日数转公历
MM_EXPORT_SXWNL
void 
csJD_DD(
    struct csJD* r, 
    double jd);

//日期转为串
MM_EXPORT_SXWNL
void 
csJD_DD2str(
    const struct csJD* r, 
    char str[32]);

//JD转为串
MM_EXPORT_SXWNL
void 
csJD_JD2str(
    double jd, 
    char str[32]);

//公历转儒略日
MM_EXPORT_SXWNL
double 
csJD_toJD(
    const struct csJD* p);

//儒略日数转公历
MM_EXPORT_SXWNL
void 
csJD_setFromJD(
    struct csJD* p, 
    double jd);

//提取jd中的时间(去除日期)
MM_EXPORT_SXWNL
void 
csJD_timeStr(
    double jd, 
    char str[32]);

//星期计算
MM_EXPORT_SXWNL
int 
csJD_getWeek(
    double jd);

//求y年m月的第n个星期w的儒略日数
MM_EXPORT_SXWNL
double 
csJD_nnweek(
    int y, 
    int m, 
    int n, 
    int w);

#include "core/mmSuffix.h"

#endif//__mmSxwnlJD_h__
