/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlEph0_h__
#define __mmSxwnlEph0_h__

#include "core/mmPlatform.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

//保留m位小数
MM_EXPORT_SXWNL
void 
toFixed(
    char str[32], 
    double v, 
    int m);

//=================================角度格式化=======================================
//==================================================================================
struct csFormatWord
{
    const char* w1;// 度
    const char* w2;// 分
    const char* w3;// 秒
};

MM_EXPORT_SXWNL extern const struct csFormatWord csFormatWords[];

//将弧度转为字串,ext为小数保留位数
MM_EXPORT_SXWNL
void 
rad2strE(
    char str[32], 
    double d, 
    int tim, 
    int ext);

//将弧度转为字串,保留2位
MM_EXPORT_SXWNL
void 
rad2str(
    char str[32], 
    double d, 
    int tim);

//将弧度转为字串,精确到分
MM_EXPORT_SXWNL
void 
rad2str2(
    char str[32], 
    double d);

//秒转为分秒,fx为小数点位数,fs为1转为"分秒"格式否则转为"角度分秒"格式
MM_EXPORT_SXWNL
void 
m2fm(
    char str[32], 
    double v, 
    int fx, 
    int fs);

//秒转为分秒,fn为分对齐长度,fx为小数点位数,fs格式, 该函数会补空格用以对齐
MM_EXPORT_SXWNL
void
m2fmv2(
    char str[32],
    double v,
    int fn,
    int fx,
    int fs);

//串转弧度, f=1表示输入的s为时分秒
MM_EXPORT_SXWNL
double 
str2rad(
    const char* s, 
    int f);

//=================================数学工具=========================================
//==================================================================================
//对超过0-2PI的角度转为0-2PI
MM_EXPORT_SXWNL
double 
rad2mrad(
    double v);

//对超过-PI到PI的角度转为-PI到PI
MM_EXPORT_SXWNL
double 
rad2rrad(
    double v);

//临界余数(a与最近的整倍数b相差的距离)
MM_EXPORT_SXWNL
double 
mod2(
    double a, 
    double b);

//球面转直角坐标
MM_EXPORT_SXWNL
void 
llr2xyz(
    double r[3], 
    const double JW[3]);

//直角坐标转球
MM_EXPORT_SXWNL
void 
xyz2llr(
    double r[3], 
    const double xyz[3]);

//球面坐标旋转
MM_EXPORT_SXWNL
void 
llrConv(
    double r[3], 
    const double JW[3], 
    double E);

//赤道坐标转为地平坐标
MM_EXPORT_SXWNL
void 
CD2DP(double a[3], 
    const double z[3], 
    double L, 
    double fa, 
    double gst);

//求角度差
MM_EXPORT_SXWNL
double 
j1_j2(
    double J1, 
    double W1, 
    double J2, 
    double W2);

//日心球面转地心球面,Z星体球面坐标,A地球球面坐标
MM_EXPORT_SXWNL
void 
h2g(
    double r[3], 
    const double z[3], 
    const double a[3]);

//视差角(不是视差)
MM_EXPORT_SXWNL
double 
shiChaJ(
    double gst, 
    double L, 
    double fa, 
    double J, 
    double W);

// TD - UT1 计算表
MM_EXPORT_SXWNL extern const double dt_at[];

//二次曲线外推
MM_EXPORT_SXWNL
double 
dt_ext(
    double y, 
    double jsd);

//计算世界时与原子时之差,传入年
MM_EXPORT_SXWNL
double 
dt_calc(
    double y);

//传入儒略日(J2000起算),计算TD-UT(单位:日)
MM_EXPORT_SXWNL
double 
dt_T(
    double t);

//=================================岁差计算=========================================
//==================================================================================
// IAU1976岁差表
MM_EXPORT_SXWNL extern const double preceTab_IAU1976[];

// IAU2000岁差表
MM_EXPORT_SXWNL extern const double preceTab_IAU2000[];

MM_EXPORT_SXWNL extern const double preceTab_P03[];

enum csPreceMX
{
    csPMX_IAU1976 = 0,
    csPMX_IAU2000 = 1,
    csPMX_P03     = 2,
};

enum csPreceSC
{
    csPSC_fi =  0,
    csPSC_w  =  1,
    csPSC_P  =  2,
    csPSC_Q  =  3,
    csPSC_E  =  4,
    csPSC_x  =  5,
    csPSC_pi =  6,
    csPSC_II =  7,
    csPSC_p  =  8,
    csPSC_th =  9,
    csPSC_Z  = 10,
    csPSC_z  = 11,
};

//t是儒略世纪数,sc是岁差量名称,mx是模型
MM_EXPORT_SXWNL
double 
prece(
    double t, 
    enum csPreceSC sc, 
    enum csPreceMX mx);

//返回P03黄赤交角,t是世纪数
MM_EXPORT_SXWNL
double 
hcjj(
    double t);

//=================================岁差旋转=========================================
//==================================================================================

//J2000赤道转Date赤道
MM_EXPORT_SXWNL
void 
CDllr_J2D(
    double r[3], 
    double t, 
    const double llr[3], 
    enum csPreceMX mx);

//Date赤道转J2000赤道
MM_EXPORT_SXWNL
void 
CDllr_D2J(
    double r[3], 
    double t, 
    const double llr[3], 
    enum csPreceMX mx);

//黄道球面坐标_J2000转Date分点,t为儒略世纪数
MM_EXPORT_SXWNL
void 
HDllr_J2D(
    double r[3], 
    double t, 
    const double llr[3], 
    enum csPreceMX mx);

//黄道球面坐标_Date分点转J2000,t为儒略世纪数
MM_EXPORT_SXWNL
void 
HDllr_D2J(
    double r[3], 
    double t, 
    const double llr[3], 
    enum csPreceMX mx);

//=================================章动计算=========================================
//==================================================================================
// IAU2000B章动序列
MM_EXPORT_SXWNL extern const int nuTab[];

//章动计算,t是J2000.0起算的儒略世纪数,zq表示只计算周期大于zq(天)的项
MM_EXPORT_SXWNL
void 
nutation(
    double r[2], 
    double t, 
    int zq);

//本函数计算赤经章动及赤纬章动
MM_EXPORT_SXWNL
void 
CDnutation(
    double r[3], 
    const double z[3], 
    double E, 
    double dL, 
    double dE);

// 中精度章动计算表
MM_EXPORT_SXWNL extern const double nutB[];

//中精度章动计算,t是世纪数
MM_EXPORT_SXWNL
void 
nutation2(
    double r[2], 
    double t);

//只计算黄经章动
MM_EXPORT_SXWNL
double 
nutationLon2(
    double t);

//=================================蒙气改正=========================================
//==================================================================================

//大气折射,h是真高度
MM_EXPORT_SXWNL
double 
MQC(
    double h);

//大气折射,ho是视高度
MM_EXPORT_SXWNL
double 
MQC2(
    double ho);

//=================================视差改正=========================================
//==================================================================================

//视差修正
MM_EXPORT_SXWNL
void 
parallax(
    double s[3], 
    const double z[3], 
    double H, 
    double fa, 
    double high);

//xt星体,zn坐标号,t儒略世纪数,n计算项数
MM_EXPORT_SXWNL
double 
XL0_calc(
    int xt, 
    int zn, 
    double t, 
    int n);

//返回冥王星J2000直角坐标
MM_EXPORT_SXWNL
void 
pluto_coord(
    double r[3], 
    double t);

//xt星体,T儒略世纪数,TD
MM_EXPORT_SXWNL
void 
p_coord(
    double z[3], 
    int xt, 
    double t, 
    int n1, 
    int n2, 
    int n3);

//返回地球坐标,t为世纪数
MM_EXPORT_SXWNL
void 
e_coord(
    double re[3], 
    double t, 
    int n1, 
    int n2, 
    int n3);

//=================================月亮星历--=======================================
//==================================================================================

//计算月亮
MM_EXPORT_SXWNL
double 
XL1_calc(
    int zn, 
    double t, 
    int n);

//返回月球坐标,n1,n2,n3为各坐标所取的项数
MM_EXPORT_SXWNL
void 
m_coord(
    double re[3], 
    double t, 
    int n1, 
    int n2, 
    int n3);

//=============================一些天文基本问题=====================================
//==================================================================================
//返回朔日的编号,jd应在朔日附近，允许误差数天
MM_EXPORT_SXWNL
double 
suoN(
    double jd);

//太阳光行差,t是世纪数
MM_EXPORT_SXWNL
double 
gxc_sunLon(
    double t);

//黄纬光行差
MM_EXPORT_SXWNL
double 
gxc_sunLat(
    double t);

//月球经度光行差,误差0.07"
MM_EXPORT_SXWNL
double 
gxc_moonLon(
    double t);

//月球纬度光行差,误差0.006"
MM_EXPORT_SXWNL
double 
gxc_moonLat(
    double t);

//传入T是2000年首起算的日数(UT),dt是deltatT(日),精度要求不高时dt可取值为0
MM_EXPORT_SXWNL
double 
pGST(
    double T, 
    double dt);

//传入力学时J2000起算日数，返回平恒星时
MM_EXPORT_SXWNL
double 
pGST2(
    double jd);

//太阳升降计算。jd儒略日(须接近L当地平午UT)，L地理经度，fa地理纬度，sj=-1升,sj=1降
MM_EXPORT_SXWNL
double 
sunShengJ(
    double jd, 
    double L, 
    double fa, 
    int sj);

//时差计算(高精度),t力学时儒略世纪数
MM_EXPORT_SXWNL
double 
pty_zty(
    double t);

//时差计算(低精度),误差约在1秒以内,t力学时儒略世纪数
MM_EXPORT_SXWNL
double 
pty_zty2(
    double t);


//物件XL : 日月黄道平分点坐标、视坐标、速度、已知经度反求时间等方面的计算

//地球经度计算,返回Date分点黄经,传入世纪数、取项数
MM_EXPORT_SXWNL
double 
XL_E_Lon(
    double t, 
    int n);

//月球经度计算,返回Date分点黄经,传入世纪数,n是项数比例
MM_EXPORT_SXWNL
double 
XL_M_Lon(
    double t, 
    int n);

//=========================
//地球速度,t是世纪数,误差小于万分3
MM_EXPORT_SXWNL
double 
XL_E_v(
    double t);

//月球速度计算,传入世经数
MM_EXPORT_SXWNL
double 
XL_M_v(
    double t);

//=========================
//月日视黄经的差值
MM_EXPORT_SXWNL
double 
XL_MS_aLon(
    double t, 
    int Mn, 
    int Sn);

//太阳视黄经
MM_EXPORT_SXWNL
double 
XL_S_aLon(
    double t, 
    int n);

//=========================
//已知地球真黄经求时间
MM_EXPORT_SXWNL
double 
XL_E_Lon_t(
    double W);

//已知真月球黄经求时间
MM_EXPORT_SXWNL
double 
XL_M_Lon_t(
    double W);

//已知月日视黄经差求时间
MM_EXPORT_SXWNL
double 
XL_MS_aLon_t(
    double W);

//已知太阳视黄经反求时间
MM_EXPORT_SXWNL
double 
XL_S_aLon_t(
    double W);

//已知月日视黄经差求时间,高速低精度,误差不超过600秒(只验算了几千年)
MM_EXPORT_SXWNL
double 
XL_MS_aLon_t2(
    double W);

//已知太阳视黄经反求时间,高速低精度,最大误差不超过600秒
MM_EXPORT_SXWNL
double 
XL_S_aLon_t2(
    double W);

//月亮被照亮部分的比例
MM_EXPORT_SXWNL
double 
XL_moonIll(
    double t);

//转入地平纬度及地月质心距离,返回站心视半径(角秒)
MM_EXPORT_SXWNL
double 
XL_moonRad(
    double r, 
    double h);

//求月亮近点时间和距离,t为儒略世纪数力学时
MM_EXPORT_SXWNL
void 
XL_moonMinR(
    double re[2], 
    double t, 
    int min);

//月亮升交点
MM_EXPORT_SXWNL
void 
XL_moonNode(
    double re[2], 
    double t, 
    double asc);

//地球近远点
MM_EXPORT_SXWNL
void 
XL_earthMinR(
    double re[2], 
    double t, 
    int min);

#include "core/mmSuffix.h"

#endif//__mmSxwnlEph0_h__
