/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlLunar.h"
#include "mmSxwnlLunarData.h"
#include "mmSxwnlLunarJNB.h"
#include "mmSxwnlLunarFTV.h"
#include "mmSxwnlConst.h"
#include "mmSxwnlJD.h"
#include "mmSxwnlEph0.h"

#include "core/mmAlloc.h"
#include "core/mmValueTransform.h"
#include "core/mmStringUtils.h"
#include "core/mmUTFConvert.h"

MM_EXPORT_SXWNL
void
csLunarOBDay_Init(
    struct csLunarOBDay* p)
{
    p->d0 = 0;
    p->di = 0;
    p->y = 0;
    p->m = 0;
    p->d = 0;
    p->dn = 0;
    p->week0 = 0;
    p->week = 0;
    p->weeki = 0;
    p->weekN = 0;

    p->Ldi = 0;
    mmString_Init(&p->Ldc);
    p->cur_dz = 0;
    p->cur_xz = 0;
    p->cur_lq = 0;
    p->cur_mz = 0;
    p->cur_xs = 0;
    p->Lmi = 0;
    mmString_Init(&p->Lmc);
    mmString_Init(&p->Lmc2);
    p->Ldn = 0;
    mmString_Init(&p->Lleap);

    p->Lyear = 0;
    p->Lyear0 = 0;
    mmString_Init(&p->Lyear2);
    mmString_Init(&p->Lyear3);
    p->Lyear4 = 0;
    p->Lmonth = 0;
    p->Lgzjn = 0;
    p->Lgzjy = 0;
    p->Lgzjr = 0;
    p->Ljqid = -1;
    p->XiZid = 0;
    mmString_Init(&p->Lmonth2);
    mmString_Init(&p->Lday2);
    mmString_Init(&p->Ltime2);
    mmString_Init(&p->Ljq);
    mmString_Init(&p->XiZ);

    p->Hyear = 0;
    p->Hmonth = 0;
    p->Hday = 0;

    mmString_Init(&p->yxmc);
    p->yxjd = 0;
    mmString_Init(&p->yxsj);
    p->yxid = -1;
    mmString_Init(&p->jqmc);
    p->jqjd = 0;
    mmString_Init(&p->jqsj);
    p->jqid = -1;

    p->Fjia = 0;
    mmString_Init(&p->A);
    mmString_Init(&p->B);
    mmString_Init(&p->C);
}

MM_EXPORT_SXWNL
void
csLunarOBDay_Destroy(
    struct csLunarOBDay* p)
{
    mmString_Destroy(&p->C);
    mmString_Destroy(&p->B);
    mmString_Destroy(&p->A);
    p->Fjia = 0;

    p->jqid = -1;
    mmString_Destroy(&p->jqsj);
    p->jqjd = 0;
    mmString_Destroy(&p->jqmc);
    p->yxid = -1;
    mmString_Destroy(&p->yxsj);
    p->yxjd = 0;
    mmString_Destroy(&p->yxmc);

    p->Hday = 0;
    p->Hmonth = 0;
    p->Hyear = 0;

    mmString_Destroy(&p->XiZ);
    mmString_Destroy(&p->Ljq);
    mmString_Destroy(&p->Ltime2);
    mmString_Destroy(&p->Lday2);
    mmString_Destroy(&p->Lmonth2);
    p->XiZid = 0;
    p->Ljqid = -1;
    p->Lgzjr = 0;
    p->Lgzjy = 0;
    p->Lgzjn = 0;
    p->Lmonth = 0;
    p->Lyear4 = 0;
    mmString_Destroy(&p->Lyear3);
    mmString_Destroy(&p->Lyear2);
    p->Lyear0 = 0;
    p->Lyear = 0;

    mmString_Destroy(&p->Lleap);
    p->Ldn = 0;
    mmString_Destroy(&p->Lmc2);
    mmString_Destroy(&p->Lmc);
    p->Lmi = 0;
    p->cur_xs = 0;
    p->cur_mz = 0;
    p->cur_lq = 0;
    p->cur_xz = 0;
    p->cur_dz = 0;
    mmString_Destroy(&p->Ldc);
    p->Ldi = 0;

    p->weekN = 0;
    p->weeki = 0;
    p->week = 0;
    p->week0 = 0;
    p->dn = 0;
    p->d = 0;
    p->m = 0;
    p->y = 0;
    p->di = 0;
    p->d0 = 0;
}

MM_EXPORT_SXWNL
void
csLunarOBDay_Assign(
    struct csLunarOBDay* p,
    const struct csLunarOBDay* q)
{
    p->d0 = q->d0;
    p->di = q->di;
    p->y = q->y;
    p->m = q->m;
    p->d = q->d;
    p->dn = q->dn;
    p->week0 = q->week0;
    p->week = q->week;
    p->weeki = q->weeki;
    p->weekN = q->weekN;

    p->Ldi = q->Ldi;
    mmString_Assign(&p->Ldc, &q->Ldc);
    p->cur_dz = q->cur_dz;
    p->cur_xz = q->cur_xz;
    p->cur_lq = q->cur_lq;
    p->cur_mz = q->cur_mz;
    p->cur_xs = q->cur_xs;
    p->Lmi = q->Lmi;
    mmString_Assign(&p->Lmc, &q->Lmc);
    mmString_Assign(&p->Lmc2, &q->Lmc2);
    p->Ldn = q->Ldn;
    mmString_Assign(&p->Lleap, &q->Lleap);

    p->Lyear = q->Lyear;
    p->Lyear0 = q->Lyear0;
    mmString_Assign(&p->Lyear2, &q->Lyear2);
    mmString_Assign(&p->Lyear3, &q->Lyear3);
    p->Lyear4 = q->Lyear4;
    p->Lmonth = q->Lmonth;
    p->Lgzjn = q->Lgzjn;
    p->Lgzjy = q->Lgzjy;
    p->Lgzjr = q->Lgzjr;
    p->Ljqid = q->Ljqid;
    p->XiZid = q->XiZid;
    mmString_Assign(&p->Lmonth2, &q->Lmonth2);
    mmString_Assign(&p->Lday2, &q->Lday2);
    mmString_Assign(&p->Ltime2, &q->Ltime2);
    mmString_Assign(&p->Ljq, &q->Ljq);
    mmString_Assign(&p->XiZ, &q->XiZ);

    p->Hyear = q->Hyear;
    p->Hmonth = q->Hmonth;
    p->Hday = q->Hday;

    mmString_Assign(&p->yxmc, &q->yxmc);
    p->yxjd = q->yxjd;
    mmString_Assign(&p->yxsj, &q->yxsj);
    p->yxid = q->yxid;
    mmString_Assign(&p->jqmc, &q->jqmc);
    p->jqjd = q->jqjd;
    mmString_Assign(&p->jqsj, &q->jqsj);
    p->jqid = q->jqid;

    p->Fjia = q->Fjia;
    mmString_Assign(&p->A, &q->A);
    mmString_Assign(&p->B, &q->B);
    mmString_Assign(&p->C, &q->C);
}

MM_EXPORT_SXWNL
void
csLunarOBA_ResetDayFestival(
    struct csLunarOBDay* r)
{
    mmString_Reset(&r->A);
    mmString_Reset(&r->B);
    mmString_Reset(&r->C);
    r->Fjia = 0;
}

MM_EXPORT_SXWNL
void
csLunarOBA_AppendDayFestival(
    struct csLunarOBDay* r,
    int type,
    const char* f)
{
    switch (type)
    {
    case '#':
        // 放假的节日
        mmString_Appends(&r->A, f);
        mmString_Appends(&r->A, " ");
        r->Fjia = 1;
        break;
    case 'I':
        // 主要
        mmString_Appends(&r->B, f);
        mmString_Appends(&r->B, " ");
        break;
    case '.':
        // 其它
        mmString_Appends(&r->C, f);
        mmString_Appends(&r->C, " ");
        break;
    }
}

MM_EXPORT_SXWNL
void
csLunarOBA_FestivalDayName(
    int y, int m, int d,
    const char*** sFtvTable,
    const int* sFtvLength,
    struct csLunarOBDay* r)
{
    int i;
    char d0[8] = { 0 };
    const char** sFtvm;
    const char* s1;
    const char* s2;
    char type;
    size_t slen;

    sprintf(d0, "%02d", d);

    sFtvm = sFtvTable[m - 1];

    // 遍历本月节日表
    for (i = 0; i < sFtvLength[m - 1]; i++) 
    {
        s1 = sFtvm[i];
        slen = strlen(s1);
        if (0 != mmMemcmp(s1, d0, 2)) continue;
        type = s1[2];

        if (7 < slen && s1[7] == '-')
        {
            // "01#1889-9999劳动节",
            // 有年限的
            int l, r;
            char sl[8] = { 0 };
            char sr[8] = { 0 };
            mmMemcpy(sl, s1 + 3, 4);
            mmMemcpy(sr, s1 + 8, 4);
            mmValue_AToSInt(sl, &l);
            mmValue_AToSInt(sr, &r);
            if (y < l || r < y)
            {
                continue;
            }
            else
            {
                s2 = s1 + 12;
            }
        }
        else
        {
            // "01#元旦",
            if (y < 1850)
            {
                continue;
            }
            else
            {
                s2 = s1 + 3;
            }
        }
        csLunarOBA_AppendDayFestival(r, type, s2);
    }
}

// 取某日节日,传入日对象
MM_EXPORT_SXWNL
void
csLunarOBA_getDayName(
    const struct csLunarOBDay* u,
    struct csLunarOBDay* r)
{
     /****************
      节日名称生成
      传入日物件u
      返回某日节日信息
      r.A 重要喜庆日子名称(可将日子名称置红)
      r.B 重要日子名称
      r.C 各种日子名称(连成一大串)
      r.Fjia 放假日子(可用于日期数字置红)
     *****************/
    int i;
    int w1, w2;
    char m0[8] = { 0 };
    char sw1[8] = { 0 };
    char sw2[8] = { 0 };
    const char* s1;
    const char* s2;
    char type;

    sprintf(m0, "%02d", u->m);

    // 星期日或星期六放假
    if (u->week == 0 || u->week == 6) r->Fjia = 1;

    // 按公历日期查找
    csLunarOBA_FestivalDayName(
        u->y, u->m, u->d,
        csLunarOBA_sFtv,
        csLunarOBA_sFtvLength,
        r);

    // 按周查找
    w1 = u->weeki; if (u->week  >= u->week0    ) w1 += 1;
    w2 =       w1; if (u->weeki == u->weekN - 1) w2 = 5;
    // d日在本月的第几个星期某
    sprintf(sw1, "%s%d%d", m0, w1, u->week);
    sprintf(sw2, "%s%d%d", m0, w2, u->week);

    for (i = 0; i < csLunarOBA_wFtvLength; i++)
    {
        int wm;
        char sw[8] = { 0 };
        // "0520.国际母亲节",
        s1 = csLunarOBA_wFtv[i];
        mmMemcpy(sw, s1, 4);
        mmValue_AToSInt(sw, &wm);
        if (0 != mmMemcmp(sw1, sw, 4) && 0 != mmMemcmp(sw2, sw, 4)) continue;
        type = s1[4];
        s2 = s1 + 5;

        csLunarOBA_AppendDayFestival(r, type, s2);
    }
}

//回历计算
MM_EXPORT_SXWNL
void
csLunarOBA_getHuiLi(
    double d0,
    struct csLunarOBDay* r)
{
    //以下算法使用Excel测试得到,测试时主要关心年临界与月临界
    int z, y, m;
    double d;

    //10631为一周期(30年)
    d  = d0 + 503105;           z = int2(d / 10631);       
    //加0.5的作用是保证闰年正确(一周中的闰年是第2,5,7,10,13,16,18,21,24,26,29年)
    d -= z * 10631;             y = int2((d + 0.5) / 354.366); 
    //分子加0.11,分母加0.01的作用是第354或355天的的月分保持为12月(m=11)
    d -= int2(y*354.366 + 0.5); m = int2((d + 0.11) / 29.51);  

    d -= int2(m*29.5 + 0.5);
    r->Hyear = z * 30 + y + 1;
    r->Hmonth = m + 1;
    r->Hday = int2(d + 1);
}

MM_EXPORT_SXWNL
void
csLunarOBB_FestivalDayName(
    int y, int m, int d,
    const char*** nFtvTable,
    const int* nFtvLength,
    struct csLunarOBDay* r)
{
    int i;
    char d0[8] = { 0 };
    const char** sFtvm;
    const char* s1;
    const char* s2;
    char type;
    size_t slen;

    // 农历月份索引对应月名加一
    sprintf(d0, "%02d", d + 1);

    // 农历月份
    // 冬 十一月 
    // 腊 十二月
    // 正 岁首月 不一定是一月, 要看月建和当时规定
    // 一是一月名称不是正月, 冰是腊月别名不是腊月
    // 冬 腊 正 二 三 四 五 六 七 八 九 十 一 冰
    // 00 01 02 03 04 05 06 07 08 09 10 11 12 13
    sFtvm = nFtvTable[m];

    // 遍历本月节日表
    for (i = 0; i < nFtvLength[m]; i++)
    {
        s1 = sFtvm[i];
        slen = strlen(s1);
        if (0 != mmMemcmp(s1, d0, 2)) continue;
        type = s1[2];

        if (7 < slen && s1[7] == '-')
        {
            // "01#1889-9999劳动节",
            // 有年限的
            int l, r;
            char sl[8] = { 0 };
            char sr[8] = { 0 };
            mmMemcpy(sl, s1 + 3, 4);
            mmMemcpy(sr, s1 + 8, 4);
            mmValue_AToSInt(sl, &l);
            mmValue_AToSInt(sr, &r);
            if (y < l || r < y)
            {
                continue;
            }
            else
            {
                s2 = s1 + 12;
            }
        }
        else
        {
            // "15#元宵节",
            if (y < 0)
            {
                // y是干支纪年(黄帝纪元)
                // 纪元之前就不算农历节日了吧
                continue;
            }
            else
            {
                s2 = s1 + 3;
            }
        }
        csLunarOBA_AppendDayFestival(r, type, s2);
    }
}

//计算农历节日
MM_EXPORT_SXWNL
void
csLunarOBB_getDayName(
    const struct csLunarOBDay* u,
    struct csLunarOBDay* r)
{
     //按农历日期查找重量点节假日
    const char* w;
    if (0 != mmString_CompareCStr(&u->Lleap, "闰"))
    {
        // 按农历日期查找
        csLunarOBB_FestivalDayName(
            u->Lyear4, u->Lmi, u->Ldi,
            csLunarOBB_nFtv,
            csLunarOBB_nFtvLength,
            r);
    }

    if (0 == mmString_CompareCStr(&u->Lmc2, "正"))
    {
        // 除夕要看大小月, 单独判断
        // 注意这里先判断了下个月是正月
        if ((1 == u->Lmi && 29 == u->Ldi && u->Ldn == 30) ||
            (1 == u->Lmi && 28 == u->Ldi && u->Ldn == 29))
        {
            csLunarOBA_AppendDayFestival(r, '#', "除夕");
        }
    }

    if (0 != mmString_Size(&u->Ljq))
    {
        if (0 == mmString_CompareCStr(&u->Ljq, "清明"))
        {
            csLunarOBA_AppendDayFestival(r, '#', mmString_CStr(&u->Ljq));
        }
        else
        {
            csLunarOBA_AppendDayFestival(r, 'I', mmString_CStr(&u->Ljq));
        }
    }

    //农历杂节
    if (u->cur_dz >= 0 && u->cur_dz < 81) 
    { 
        //数九
        w = csLunarOBB_numCn[(int)floor(u->cur_dz / 9) + 1];
        if (u->cur_dz % 9 == 0)
        {
            char str[16] = { 0 };
            sprintf(str, "%s%s%s%s", "『", w, "九", "』");
            csLunarOBA_AppendDayFestival(r, 'I', str);
        }
        else
        {
            char str[16] = { 0 };
            sprintf(str, "%s%s%d%s", w, "九第", (u->cur_dz % 9 + 1), "天");
            csLunarOBA_AppendDayFestival(r, '.', str);
        }
    }

    //ob.Lday2  纪日
    w = mmString_CStr(&u->Lday2);
    if (u->cur_xz >= 20 && u->cur_xz < 30 && 0 == mmMemcmp(w    , "庚", 3))
    {
        csLunarOBA_AppendDayFestival(r, 'I', "初伏");
    }
    if (u->cur_xz >= 30 && u->cur_xz < 40 && 0 == mmMemcmp(w    , "庚", 3))
    {
        csLunarOBA_AppendDayFestival(r, 'I', "中伏");
    }
    if (u->cur_lq >=  0 && u->cur_lq < 10 && 0 == mmMemcmp(w    , "庚", 3))
    {
        csLunarOBA_AppendDayFestival(r, 'I', "末伏");
    }
    if (u->cur_mz >=  0 && u->cur_mz < 10 && 0 == mmMemcmp(w    , "丙", 3))
    {
        csLunarOBA_AppendDayFestival(r, 'I', "入梅");
    }
    if (u->cur_xs >=  0 && u->cur_xs < 12 && 0 == mmMemcmp(w + 3, "未", 3))
    {
        csLunarOBA_AppendDayFestival(r, 'I', "出梅");
    }
}

//命理八字计算。jd为格林尼治UT(J2000起算),J为本地经度(弧度),返回在物件ob中
MM_EXPORT_SXWNL
void
csLunarOBB_mingLiBaZi(
    double jd,
    double J,
    struct csLunarMLBZ* ob)
{
    int v;
    int D, SC;
    double jd2 = jd + dt_T(jd); //力学时
    double w = XL_S_aLon(jd2 / 36525, -1); //此刻太阳视黄经
    int k = int2((w / cs_pi2 * 360 + 45 + 15 * 360) / 30); //1984年立春起算的节气数(不含中气)
    jd += pty_zty2(jd2 / 36525) + J / cs_pi / 2; //本地真太阳时(使用低精度算法计算时差)
    ob->bz_zty = jd;

    jd += 13.0 / 24; //转为前一日23点起算(原jd为本日中午12点起算)
    D = (int)floor(jd); SC = int2((jd - D) * 12); //日数与时辰
    // 这里 k/12 虽然是int相除, 但是结果做了取整截断,所以结果是一样的. 不用特意转double.
    v =      int2(k / 12 +  6000000); ob->bz_jn[0] = v % 10; ob->bz_jn[1] = v % 12;
    v =            k + 2 + 60000000 ; ob->bz_jy[0] = v % 10; ob->bz_jy[1] = v % 12; 
    v =            D - 6 +  9000000 ; ob->bz_jr[0] = v % 10; ob->bz_jr[1] = v % 12; 
    v = (D - 1) * 12 + 90000000 + SC; ob->bz_js[0] = v % 10; ob->bz_js[1] = v % 12;

    ob->bz_rs = D;
    ob->bz_sc = SC;
}

//全天纪时表
MM_EXPORT_SXWNL
void
csLunarOBB_quanTianJiShiBiao(
    const struct csLunarMLBZ* ob,
    struct mmString* s)
{
    int i;
    int v;
    int D = ob->bz_rs;
    int SC = ob->bz_sc;

    v = (D - 1) * 12 + 90000000 + SC;

    v -= SC;

    //全天纪时表
    mmString_Reset(s);

    //一天中包含有13个纪时
    for (i = 0; i < 13; i++)
    {
        //间隔符号
        mmString_Appendnc(s, (i ? 1 : 0), ' ');
        //各时辰的八字
        mmString_Appends(s, csLunarOBB_Gan[(v + i) % 10]);
        mmString_Appends(s, csLunarOBB_Zhi[(v + i) % 12]);
    }
}

//精气
MM_EXPORT_SXWNL
double
csLunarOBB_qi_accurate(
    double W)
{
    double t = XL_S_aLon_t(W) * 36525;  
    return t - dt_T(t) + 8.0 / 24;
} 

//精朔
MM_EXPORT_SXWNL
double
csLunarOBB_so_accurate(
    double W)
{
    double t = XL_MS_aLon_t(W) * 36525; 
    return t - dt_T(t) + 8.0 / 24; 
} 

//精气
MM_EXPORT_SXWNL
double
csLunarOBB_qi_accurate2(
    double jd)
{
    double d = cs_pi / 12;
    double w = floor((jd + 293) / 365.2422 * 24) * d;
    double a = csLunarOBB_qi_accurate(w);
    if (a - jd >  5) return csLunarOBB_qi_accurate(w - d);
    if (a - jd < -5) return csLunarOBB_qi_accurate(w + d);
    return a;
}

//精朔
MM_EXPORT_SXWNL
double
csLunarOBB_so_accurate2(
    double jd)
{
    return csLunarOBB_so_accurate(floor((jd + 8) / 29.5306) * cs_pi * 2);
} 

/************************
  实气实朔计算器
  适用范围 -722年2月22日——1959年12月
  平气平朔计算使用古历参数进行计算
  定朔、定气计算使用开普勒椭圆轨道计算，同时考虑了光行差和力学时与UT1的时间差
  古代历算仅在晚期才使用开普勒方法计算，此前多采用一些修正表并插值得到，精度很低，与本程序中
的开普勒方法存在误差，造成朔日计算错误1千多个，这些错误使用一个修正表进行订正。同样，定气部分
也使用了相同的方法时行订正。
  平气朔表的算法(线性拟合)：
  气朔日期计算公式：D = k*n + b  , 式中n=0,1,2,3,...,N-1, N为该式适用的范围
  h表示k不变b允许的误差,如果b不变则k许可误差为h/N
  每行第1个参数为k,第2参数为b
  public中定义的成员可以直接使用
*************************/

MM_EXPORT_SXWNL
void
csLunarSSQ_Init(
    struct csLunarSSQ* p)
{
    mmString_Init(&p->SB);
    mmString_Init(&p->QB);

    mmMemset(p->ym, 0, sizeof(p->ym));
    p->leap = 0;
    mmMemset(p->yi, 0, sizeof(p->yi));
    mmMemset(p->yv, 0, sizeof(p->yv));
    mmMemset(p->ZQ, 0, sizeof(p->ZQ));
    mmMemset(p->HS, 0, sizeof(p->HS));
    mmMemset(p->dx, 0, sizeof(p->dx));
    mmMemset(p->pe, 0, sizeof(p->pe));
}

MM_EXPORT_SXWNL
void
csLunarSSQ_Destroy(
    struct csLunarSSQ* p)
{
    mmMemset(p->pe, 0, sizeof(p->pe));
    mmMemset(p->dx, 0, sizeof(p->dx));
    mmMemset(p->HS, 0, sizeof(p->HS));
    mmMemset(p->ZQ, 0, sizeof(p->ZQ));
    mmMemset(p->yv, 0, sizeof(p->yv));
    mmMemset(p->yi, 0, sizeof(p->yi));
    p->leap = 0;
    mmMemset(p->ym, 0, sizeof(p->ym));

    mmString_Destroy(&p->QB);
    mmString_Destroy(&p->SB);
}

//低精度定朔计算,在2000年至600，误差在2小时以内(仍比古代日历精准很多)
MM_EXPORT_SXWNL
double
csLunarSSQ_so_low(
    double W)
{
    double v = 7771.37714500204;
    double t = (W + 1.08472) / v;
    t -= (-0.0000331*t*t
        + 0.10976 *cos(0.785 + 8328.6914*t)
        + 0.02224 *cos(0.187 + 7214.0629*t)
        - 0.03342 *cos(4.669 + 628.3076*t)) / v
        + (32 * (t + 1.8)*(t + 1.8) - 20) / 86400 / 36525;
    return t * 36525 + 8.0 / 24;
}

//最大误差小于30分钟，平均5分
MM_EXPORT_SXWNL
double
csLunarSSQ_qi_low(
    double W)
{
    double t, L, v = 628.3319653318;
    t = (W - 4.895062166) / v; //第一次估算,误差2天以内
    t -= (53 * t*t + 334116 * cos(4.67 + 628.307585*t) + 2061 * cos(2.678 + 628.3076*t)*t) / v / 10000000; //第二次估算,误差2小时以内

    L = 48950621.66 + 6283319653.318*t + 53 * t*t    //平黄经
        + 334166 * cos(4.669257 +  628.307585*t)     //地球椭圆轨道级数展开
        +   3489 * cos(4.6261   + 1256.61517*t)      //地球椭圆轨道级数展开
        + 2060.6 * cos(2.67823  +  628.307585*t) * t //一次泊松项
        - 994 - 834 * sin(2.1824 - 33.75705*t);      //光行差与章动修正

    t -= (L / 10000000 - W) / 628.332 + (32 * (t + 1.8)*(t + 1.8) - 20) / 86400 / 36525;
    return t * 36525 + 8.0 / 24;
}

//较高精度气
MM_EXPORT_SXWNL
double
csLunarSSQ_qi_high(
    double W)
{
    double t = XL_S_aLon_t2(W) * 36525;
    t = t - dt_T(t) + 8.0 / 24;
    double v = fmod(t + 0.5, 1.0) * 86400;
    if (v < 1200 || v >86400 - 1200) t = XL_S_aLon_t(W) * 36525 - dt_T(t) + 8.0 / 24;
    return  t;
}

//较高精度朔
MM_EXPORT_SXWNL
double
csLunarSSQ_so_high(
    double W)
{
    double t = XL_MS_aLon_t2(W) * 36525;
    t = t - dt_T(t) + 8.0 / 24;
    double v = fmod(t + 0.5, 1.0) * 86400;
    if (v < 1800 || v >86400 - 1800) t = XL_MS_aLon_t(W) * 36525 - dt_T(t) + 8.0 / 24;
    return  t;
}

//气朔解压缩
MM_EXPORT_SXWNL
void
csLunarSSQ_jieya(
    struct mmString* s)
{
    const char* o1 = "0000000000";
    const char* o2 = "00000000000000000000";
    const char* o3 = "000000000000000000000000000000";
    const char* o4 = "0000000000000000000000000000000000000000";
    const char* o5 = "00000000000000000000000000000000000000000000000000";
    const char* o6 = "000000000000000000000000000000000000000000000000000000000000";

    mmString_ReplaceCStr(s, "J", "00");
    mmString_ReplaceCStr(s, "I", "000");
    mmString_ReplaceCStr(s, "H", "0000");
    mmString_ReplaceCStr(s, "G", "00000");
    mmString_ReplaceCStr(s, "t", "02");
    mmString_ReplaceCStr(s, "s", "002");
    mmString_ReplaceCStr(s, "r", "0002");
    mmString_ReplaceCStr(s, "q", "00002");
    mmString_ReplaceCStr(s, "p", "000002");
    mmString_ReplaceCStr(s, "o", "0000002");
    mmString_ReplaceCStr(s, "n", "00000002");
    mmString_ReplaceCStr(s, "m", "000000002");
    mmString_ReplaceCStr(s, "l", "0000000002");
    mmString_ReplaceCStr(s, "k", "01");
    mmString_ReplaceCStr(s, "j", "0101");
    mmString_ReplaceCStr(s, "i", "001");
    mmString_ReplaceCStr(s, "h", "001001");
    mmString_ReplaceCStr(s, "g", "0001");
    mmString_ReplaceCStr(s, "f", "00001");
    mmString_ReplaceCStr(s, "e", "000001");
    mmString_ReplaceCStr(s, "d", "0000001");
    mmString_ReplaceCStr(s, "c", "00000001");
    mmString_ReplaceCStr(s, "b", "000000001");
    mmString_ReplaceCStr(s, "a", "0000000001");
    mmString_ReplaceCStr(s, "A", o6);
    mmString_ReplaceCStr(s, "B", o5);
    mmString_ReplaceCStr(s, "C", o4);
    mmString_ReplaceCStr(s, "D", o3);
    mmString_ReplaceCStr(s, "E", o2);
    mmString_ReplaceCStr(s, "F", o1);
}

//初使用化
MM_EXPORT_SXWNL
void
csLunarSSQ_initialize(
    struct csLunarSSQ* p)
{
    mmString_Reserve(&p->SB, 16598);// 默认自增: 32513 预设扩容: 16609
    mmString_Reserve(&p->QB,  7567);// 默认自增: 14337 预设扩容:  7569

    mmString_Assigns(&p->SB, csLunarSSQ_suoS);
    mmString_Assigns(&p->QB, csLunarSSQ_qiS);

    csLunarSSQ_jieya(&p->SB);
    csLunarSSQ_jieya(&p->QB);
}

//jd应靠近所要取得的气朔日,qs='气'时，算节气的儒略日, 0气1朔.
MM_EXPORT_SXWNL
int
csLunarSSQ_calc(
    struct csLunarSSQ* p,
    double jd,
    enum csQSType qs)
{
    int i;
    double D;
    char n;
    const double* B;
    double pc; 
    int L;
    const char* SB = mmString_CStr(&p->SB);
    const char* QB = mmString_CStr(&p->QB);

    if (qs == csQSType_Q)
    {
        // '气'
        B = csLunarSSQ_qiKB;
        pc = 7;
        L = csLunarSSQ_qiKBLength;
    }
    else
    {
        // '朔'
        B = csLunarSSQ_suoKB;
        pc = 14;
        L = csLunarSSQ_suoKBLength;
    }
    jd += 2451545;

    double f1 = B[0] - pc, f2 = B[L - 1] - pc, f3 = 2436935;

    if (jd < f1 || jd >= f3) 
    { 
        //平气朔表中首个之前，使用现代天文算法。1960.1.1以后，使用现代天文算法 (这一部分调用了qi_high和so_high,所以需星历表支持)
        if (qs == csQSType_Q) return (int)floor(csLunarSSQ_qi_high(floor((jd + pc - 2451259) / 365.2422 * 24) * cs_pi / 12) + 0.5); //2451259是1999.3.21,太阳视黄经为0,春分.定气计算
        else                  return (int)floor(csLunarSSQ_so_high(floor((jd + pc - 2451551) / 29.5306      ) * cs_pi *  2) + 0.5); //2451551是2000.1.7的那个朔日,黄经差为0.定朔计算
    }

    if (jd >= f1 && jd < f2) { //平气或平朔
        for (i = 0; i < L; i += 2)  if (jd + pc < B[i + 2]) break;
        D = B[i] + B[i + 1] * floor((jd + pc - B[i]) / B[i + 1]);
        D = floor(D + 0.5);
        if (D == 1683460) D++; //如果使用太初历计算-103年1月24日的朔日,结果得到的是23日,这里修正为24日(实历)。修正后仍不影响-103的无中置闰。如果使用秦汉历，得到的是24日，本行D不会被执行。
        return (int)(D - 2451545);
    }

    if (jd >= f2 && jd < f3) { //定气或定朔
        if (qs == csQSType_Q) {
            D = floor(csLunarSSQ_qi_low(floor((jd + pc - 2451259) / 365.2422 * 24) * cs_pi / 12) + 0.5); //2451259是1999.3.21,太阳视黄经为0,春分.定气计算
            n = QB[(int)floor((jd - f2) / 365.2422 * 24)]; //找定气修正值
        }
        else {
            D = floor(csLunarSSQ_so_low(floor((jd + pc - 2451551) /  29.5306     ) * cs_pi *  2) + 0.5); //2451551是2000.1.7的那个朔日,黄经差为0.定朔计算
            n = SB[(int)floor((jd - f2) / 29.5306      )]; //找定朔修正值
        }
        if (n == '1') return (int)(D + 1);
        if (n == '2') return (int)(D - 1);
        return (int)(D);
    }
    return 0;
}

//农历排月序计算,可定出农历,有效范围：两个冬至之间(冬至一 <= d < 冬至二)
MM_EXPORT_SXWNL
void
csLunarSSQ_calcY(
    struct csLunarSSQ* p,
    double jd)
{
    int* A = p->ZQ;
    int* B = p->HS;  //中气表,日月合朔表(整日)
    int i;
    double W, w;
    int* yi = p->yi;
    int YY;

    //该年的气
    W = int2((jd - 355 + 183) / 365.2422)*365.2422 + 355;  //355是2000.12冬至,得到较靠近jd的冬至估计值
    if (csLunarSSQ_calc(p, W, csQSType_Q) > jd) W -= 365.2422;
    for (i = 0; i < 25; i++) A[i] = csLunarSSQ_calc(p, W + 15.2184*i, csQSType_Q); //25个节气时刻(北京时间),从冬至开始到下一个冬至以后
    p->pe[0] = csLunarSSQ_calc(p, W - 15.2, csQSType_Q); p->pe[1] = csLunarSSQ_calc(p, W - 30.4, csQSType_Q); //补算二气,确保一年中所有月份的“气”全部被计算在内

    //今年"首朔"的日月黄经差w
    w = csLunarSSQ_calc(p, A[0], csQSType_S); //求较靠近冬至的朔日
    if (w > A[0]) w -= 29.53;

    //该年所有朔,包含14个月的始末
    for (i = 0; i < 15; i++) B[i] = csLunarSSQ_calc(p, w + 29.5306*i, csQSType_S);

    //月大小
    p->leap = 0;
    for (i = 0; i < 14; i++) {
        p->dx[i] = p->HS[i + 1] - p->HS[i]; //月大小
        yi[i] = i;  //月序初始化
    }

    //-721年至-104年的后九月及月建问题,与朔有关，与气无关
    YY = int2((p->ZQ[0] + 10 + 180) / 365.2422) + 2000; //确定年份
    if (YY >= -721 && YY <= -104) {
        int ns[9] = { 0 };
        int yy;
        static const char *str[] = { "十三", "后九" };
        for (i = 0; i < 3; i++) {
            yy = YY + i - 1;
            //颁行历年首, 闰月名称, 月建
            if (yy >= -721)
            {
                //春秋历,ly为-722.12.17
                ns[i] = csLunarSSQ_calc(p, 1457698 - cs_J2000 + int2(0.342 + (yy + 721)*12.368422)*29.5306, csQSType_S);
                ns[i + 3] = 0; ns[i + 6] = 2;
            }
            if (yy >= -479)
            {
                //战国历,ly为-480.12.11
                ns[i] = csLunarSSQ_calc(p, 1546083 - cs_J2000 + int2(0.500 + (yy + 479)*12.368422)*29.5306, csQSType_S);
                ns[i + 3] = 0; ns[i + 6] = 2;
            }
            if (yy >= -220)
            {
                //秦汉历,ly为-221.10.31
                ns[i] = csLunarSSQ_calc(p, 1640641 - cs_J2000 + int2(0.866 + (yy + 220)*12.369000)*29.5306, csQSType_S);
                ns[i + 3] = 1; ns[i + 6] = 11;
            }
        }
        int nn, f1;
        for (i = 0; i < 14; i++) {
            for (nn = 2; nn >= 0; nn--) if (p->HS[i] >= ns[nn]) break;
            f1 = int2((p->HS[i] - ns[nn] + 15) / 29.5306); //该月积数
            if (f1 < 12)
            {
                const char* ymc = csLunarOBB_ymc[(f1 + ns[nn + 6]) % 12];
                mmMemcpy(p->ym[i], ymc, strlen(ymc));
            }
            else
            {
                const char* ymc = str[ns[nn + 3]];
                mmMemcpy(p->ym[i], ymc, strlen(ymc));
            }
        }
        return;
    }


    //无中气置闰法确定闰月,(气朔结合法,数据源需有冬至开始的的气和朔)
    if (B[13] <= A[24]) { //第13月的月末没有超过冬至(不含冬至),说明今年含有13个月
        for (i = 1; B[i + 1] > A[2 * i] && i < 13; i++); //在13个月中找第1个没有中气的月份
        p->leap = i;
        for (; i < 14; i++) yi[i]--;
    }

    //名称转换(月建别名)
    for (i = 0; i < 14; i++) {
        double Dm = p->HS[i] + cs_J2000; //Dm初一的儒略日
        int v2 = yi[i]; //v2为月建序号
        //月建对应的默认月名称：建子十一,建丑十二,建寅为正……
        const char* mc = csLunarOBB_ymc[v2 % 12]; 

        // 2是正月
        p->yv[i] = v2 % 12;

        if      (Dm >= 1724360 && Dm <= 1729794) 
        {
            //  8.01.15至 23.12.02 建子为十二,其它顺推
            mc = csLunarOBB_ymc[(v2 + 1) % 12]; 
            p->yv[i] = (v2 + 1) % 12;
        }
        else if (Dm >= 1807724 && Dm <= 1808699)
        {
            //237.04.12至239.12.13 建子为十二,其它顺推
            mc = csLunarOBB_ymc[(v2 + 1) % 12]; 
            p->yv[i] = (v2 + 1) % 12;
        }
        else if (Dm >= 1999349 && Dm <= 1999467)
        {
            //761.12.02至762.03.30 建子为正月,其它顺推
            mc = csLunarOBB_ymc[(v2 + 2) % 12]; 
            p->yv[i] = (v2 + 2) % 12;
        }
        else if (Dm >= 1973067 && Dm <= 1977052) 
        { 
            //689.12.18至700.11.15 建子为正月,建寅为一月,其它不变
            if (v2 % 12 == 0) { mc = "正"; p->yv[i] =  2; }
            if (v2      == 2) { mc = "一"; p->yv[i] = 12; }
        } 

        if (Dm == 1729794 || Dm == 1808699)
        {
            //239.12.13及23.12.02均为十二月,为避免两个连续十二月，此处改名
            mc = "拾贰"; 
            p->yv[i] = 13;
        }
        mmMemcpy(p->ym[i], mc, strlen(mc));
    }
}

MM_EXPORT_SXWNL
void
csLunarLun_Init(
    struct csLunarLun* p)
{
    int i;

    p->w0 = 0;
    p->y = 0;
    p->m = 0;
    p->d0 = 0;
    p->dn = 0;

    p->Lgzjn = 0;
    mmString_Init(&p->Ly);
    mmString_Init(&p->ShX);

    mmString_Init(&p->nianhao);

    for (i = 0; i < 31; i++)
    {
        csLunarOBDay_Init(&p->day[i]);
    }
}

MM_EXPORT_SXWNL
void
csLunarLun_Destroy(
    struct csLunarLun* p)
{
    int i;

    for (i = 0; i < 31; i++)
    {
        csLunarOBDay_Destroy(&p->day[i]);
    }

    mmString_Destroy(&p->nianhao);

    mmString_Destroy(&p->ShX);
    mmString_Destroy(&p->Ly);
    p->Lgzjn = 0;

    p->dn = 0;
    p->d0 = 0;
    p->m = 0;
    p->y = 0;
    p->w0 = 0;
}

//返回公历某一个月的'公农回'三合历
MM_EXPORT_SXWNL
void
csLunarLun_yueLiCalc(
    struct csLunarLun* p,
    struct csLunarSSQ* SSQ,
    int By,
    int Bm)
{
    double w;
    int D;
    int i, j, c, Bd0, Bdn;
    //日历物件初始化
    struct csJD JD;//  = { By, Bm, 1, 12, 0, 0.1 };
    struct csLunarOBDay* ob, *ob2;

    JD.h = 12; JD.m = 0; JD.s = 0.1;
    JD.Y = By; JD.M = Bm; JD.D = 1;
    Bd0 = int2(csJD_toJD(&JD)) - cs_J2000;       //公历月首,中午
    
    JD.M++; if (JD.M > 12) { JD.Y++; JD.M = 1; }
    Bdn = int2(csJD_toJD(&JD)) - cs_J2000 - Bd0; //本月天数(公历)

    p->w0 = (Bd0 + cs_J2000 + 1 + 7000000) % 7; //本月第一天的星期
    p->y = By; //公历年份
    p->m = Bm; //公历月分
    p->d0 = Bd0;
    p->dn = Bdn;

    //所属公历年对应的农历干支纪年
    c = By - 1984 + 12000;
    p->Lgzjn = c;
    mmString_Assigns(&p->Ly, csLunarOBB_Gan[c % 10]); mmString_Appends(&p->Ly, csLunarOBB_Zhi[c % 12]);//干支纪年
    mmString_Assigns(&p->ShX, csLunarOBB_ShX[c % 12]);//该年对应的生肖
    csLunarOBB_getNH(By, &p->nianhao);

    //提取各日信息

    for (i = 0, j = 0; i < Bdn; i++) {
        ob = &p->day[i];
        ob->d0 = Bd0 + i; //儒略日,北京时12:00
        ob->di = i;       //公历月内日序数
        ob->y  = By;      //公历年
        ob->m  = Bm;      //公历月
        ob->dn = Bdn;     //公历月天数
        ob->week0 = p->w0;                            //月首的星期
        ob->week = (p->w0 + i) % 7;                   //当前日的星期
        ob->weeki = int2((p->w0 + i) / 7);            //本日所在的周序号
        ob->weekN = int2((p->w0 + Bdn - 1) / 7) + 1;  //本月的总周数
        csJD_setFromJD(&JD, ob->d0 + cs_J2000); ob->d = JD.D; //公历日名称

        //农历月历
        if (ob->d0 < SSQ->ZQ[0] || ob->d0 >= SSQ->ZQ[24]) //如果d0已在计算农历范围内则不再计算
            csLunarSSQ_calcY(SSQ, ob->d0);
        int mk = int2((ob->d0 - SSQ->HS[0]) / 30);  if (mk < 13 && SSQ->HS[mk + 1] <= ob->d0) mk++; //农历所在月的序数

        ob->Ldi = ob->d0 - SSQ->HS[mk];                     //距农历月首的编移量,0对应初一
        mmString_Assigns(&ob->Ldc, csLunarOBB_rmc[ob->Ldi]);//农历日名称
        ob->cur_dz = ob->d0 - SSQ->ZQ[ 0];                  //距冬至的天数
        ob->cur_xz = ob->d0 - SSQ->ZQ[12];                  //距夏至的天数
        ob->cur_lq = ob->d0 - SSQ->ZQ[15];                  //距立秋的天数
        ob->cur_mz = ob->d0 - SSQ->ZQ[11];                  //距芒种的天数
        ob->cur_xs = ob->d0 - SSQ->ZQ[13];                  //距小暑的天数
        if (ob->d0 == SSQ->HS[mk] || ob->d0 == Bd0) { //月的信息
            ob->Lmi = SSQ->yv[mk];
            mmString_Assigns(&ob->Lmc, SSQ->ym[mk]);//月名称
            ob->Ldn = SSQ->dx[mk];//月大小
            mmString_Assigns(&ob->Lleap, (SSQ->leap&&SSQ->leap == mk) ? "闰" : "");//闰状况
            mmString_Assigns(&ob->Lmc2, mk < 13 ? SSQ->ym[mk + 1] : "未知");//下个月名称,判断除夕时要用到
        }
        else {
            ob2 = &p->day[i - 1];
            ob->Lmi = ob2->Lmi;
            mmString_Assign(&ob->Lmc, &ob2->Lmc);
            ob->Ldn = ob2->Ldn;
            mmString_Assign(&ob->Lleap, &ob2->Lleap);
            mmString_Assign(&ob->Lmc2, &ob2->Lmc2);
        }
        int qk = int2((ob->d0 - SSQ->ZQ[0] - 7) / 15.2184); if (qk < 23 && ob->d0 >= SSQ->ZQ[qk + 1]) qk++; //节气的取值范围是0-23
        if (ob->d0 == SSQ->ZQ[qk]) { mmString_Assigns(&ob->Ljq, csLunarOBB_jqmc[qk]); ob->Ljqid = qk; }
        else                       { mmString_Assigns(&ob->Ljq,                  ""); ob->Ljqid = -1; }

        //月相名称,月相时刻(儒略日),月相时间串
        mmString_Assigns(&ob->yxmc, "");
        ob->yxjd = 0;
        mmString_Assigns(&ob->yxsj, "");

        //定气名称,节气时刻(儒略日),节气时间串
        mmString_Assigns(&ob->jqmc, "");
        ob->jqjd = 0;
        mmString_Assigns(&ob->jqsj, "");

        //干支纪年处理
        //以立春为界定年首
        D = SSQ->ZQ[3] + (ob->d0 < SSQ->ZQ[3] ? -365 : 0) + (int)(365.25 * 16) - 35; //以立春为界定纪年
        ob->Lyear = (int)floor(D / 365.2422 + 0.5); //农历纪年(10进制,1984年起算)
        //以下几行以正月初一定年首
        D = SSQ->HS[2]; //一般第3个月为春节
        for (j = 0; j < 14; j++) { //找春节
            if (0 != strcmp(SSQ->ym[j], "正") || (SSQ->leap == j && j)) continue;
            D = SSQ->HS[j];
            if (ob->d0 < D) { D -= 365; break; } //无需再找下一个正月
        }
        D = D + 5810;  //计算该年春节与1984年平均春节(立春附近)相差天数估计
        ob->Lyear0 = (int)floor(D / 365.2422 + 0.5); //农历纪年(10进制,1984年起算)

        D = ob->Lyear  + 12000; mmString_Assigns(&ob->Lyear2, csLunarOBB_Gan[D % 10]); mmString_Appends(&ob->Lyear2, csLunarOBB_Zhi[D % 12]); //干支纪年(立春)
        D = ob->Lyear0 + 12000; mmString_Assigns(&ob->Lyear3, csLunarOBB_Gan[D % 10]); mmString_Appends(&ob->Lyear3, csLunarOBB_Zhi[D % 12]); //干支纪年(正月)
        ob->Lyear4 = ob->Lyear0 + 1984 + 2698; //黄帝纪年

        // 干支纪年
        // 这里与命理八字里的方式不一样但是结果是一样的
        ob->Lgzjn = ob->Lyear + 6000000;

        //纪月处理,1998年12月7(大雪)开始连续进行节气计数,0为甲子
        mk = int2((ob->d0 - SSQ->ZQ[0]) / 30.43685);  if (mk < 12 && ob->d0 >= SSQ->ZQ[2 * mk + 1]) mk++;  //相对大雪的月数计算,mk的取值范围0-12

        D = mk + int2((SSQ->ZQ[12] + 390) / 365.2422) * 12 + 900000; //相对于1998年12月7(大雪)的月数,900000为正数基数
        ob->Lmonth = D % 12;
        mmString_Assigns(&ob->Lmonth2, csLunarOBB_Gan[D % 10]); mmString_Appends(&ob->Lmonth2, csLunarOBB_Zhi[D % 12]);
        ob->Lgzjy = D;

        //纪日,2000年1月7日起算
        D = ob->d0 - 6 + 9000000;
        mmString_Assigns(&ob->Lday2, csLunarOBB_Gan[D % 10]); mmString_Appends(&ob->Lday2, csLunarOBB_Zhi[D % 12]);
        ob->Lgzjr = D;

        //星座
        mk = int2((ob->d0 - SSQ->ZQ[0] - 15) / 30.43685);  if (mk < 11 && ob->d0 >= SSQ->ZQ[2 * mk + 2]) mk++; //星座所在月的序数,(如果j=13,ob.d0不会超过第14号中气)
        mmString_Assigns(&ob->XiZ, csLunarOBB_XiZ[(mk + 12) % 12]); mmString_Appends(&ob->XiZ, "座");
        ob->XiZid = (mk + 12) % 12;

        //回历
        csLunarOBA_getHuiLi(ob->d0, ob);
        //节日
        mmString_Assigns(&ob->A, "");
        mmString_Assigns(&ob->B, "");
        mmString_Assigns(&ob->C, "");
        ob->Fjia = 0;
        csLunarOBA_getDayName(ob, ob); //公历
        csLunarOBB_getDayName(ob, ob); //农历
    }

    //以下是月相与节气的处理
    int xn;
    double d, jd2 = Bd0 + dt_T(Bd0) - 8.0 / 24;
    //月相查找
    w = XL_MS_aLon(jd2 / 36525, 10, 3);
    w = int2((w - 0.78) / cs_pi * 2) * cs_pi / 2;
    do {
        char str[32] = { 0 };
        d = csLunarOBB_so_accurate(w);
        D = int2(d + 0.5);
        xn = int2(w / cs_pi2 * 4 + 4000000.01) % 4;
        w += cs_pi2 / 4;
        if (D >= Bd0 + Bdn) break;
        if (D < Bd0) continue;
        ob = &p->day[D - Bd0];
        mmString_Assigns(&ob->yxmc, csLunarOBB_yxmc[xn]);//取得月相名称
        ob->yxjd = d;
        csJD_timeStr(d, str);
        mmString_Assigns(&ob->yxsj, str);//取得月相名称
        ob->yxid = xn;
    } while (D + 5 < Bd0 + Bdn);

    //节气查找
    w = XL_S_aLon(jd2 / 36525, 3);
    w = int2((w - 0.13) / cs_pi2 * 24) *cs_pi2 / 24;
    do {
        char str[32] = { 0 };
        d = csLunarOBB_qi_accurate(w);
        D = int2(d + 0.5);
        xn = int2(w / cs_pi2 * 24 + 24000006.01) % 24;
        w += cs_pi2 / 24;
        if (D >= Bd0 + Bdn) break;
        if (D < Bd0) continue;
        ob = &p->day[D - Bd0];
        mmString_Assigns(&ob->jqmc, csLunarOBB_jqmc[xn]);//取得节气名称
        ob->jqjd = d;
        csJD_timeStr(d, str);
        mmString_Assigns(&ob->jqsj, str);
        ob->jqid = xn;
    } while (D + 12 < Bd0 + Bdn);
};

//String年历生成
MM_EXPORT_SXWNL
void
csLunarLun_nianLiString(
    struct csLunarSSQ* SSQ,
    int y,
    const char* fg,
    struct mmString* s)
{
    int i, j;
    double v, qi;
    char str[32] = { 0 };
    struct mmString s1, s2;
    
    // 消除可能的未初始化警告
    qi = 0;

    mmString_Assigns(s, "");

    mmString_Init(&s1);
    mmString_Init(&s2);
    csLunarSSQ_calcY(SSQ, int2((y - 2000)*365.2422 + 180));
    for (i = 0; i < 14; i++) {
        if (SSQ->HS[i + 1] > SSQ->ZQ[24]) break; //已包含下一年的冬至
        if (SSQ->leap && i == SSQ->leap) mmString_Assigns(&s1, "闰"); else mmString_Assigns(&s1, "·");
        mmString_Appends(&s1, SSQ->ym[i]); if(mmUTF_GetUTF8StringLength((const mmUTF8*)mmString_CStr(&s1)) < 3) mmString_Appends(&s1, "月");
        mmString_Appends(&s1, SSQ->dx[i] > 29 ? "大" : "小");

        csJD_JD2str(SSQ->HS[i] + cs_J2000, str);
        mmString_Appends(&s1, " ");
        mmString_Appendsn(&s1, &str[6], 5);

        v = csLunarOBB_so_accurate2(SSQ->HS[i]);
        csJD_JD2str(v + cs_J2000, str);
        if (int2(v + 0.5) != SSQ->HS[i])
        {
            // s2 = '<font color=red>' + s2 + '</font>';
            mmString_Assigns(&s2, "[");
            mmString_Appendsn(&s2, &str[9], 11);
            mmString_Appends(&s2, "]");
        }
        else
        {
            mmString_Assigns(&s2, "(");
            mmString_Appendsn(&s2, &str[9], 11);
            mmString_Appends(&s2, ")");
        }
        //v=(v+0.5+J2000)%1; if(v>0.5) v=1-v; if(v<8/1440) s2 = '<u>'+s2+'</u>'; //对靠近0点的加注
        mmString_Append(&s1, &s2);
        mmString_Appends(&s1, fg);

        for (j = -2; j < 24; j++) {
            if (j >=  0) qi = SSQ->ZQ[j];
            if (j == -1) qi = SSQ->pe[0];
            if (j == -2) qi = SSQ->pe[1];

            if (qi < SSQ->HS[i] || qi >= SSQ->HS[i + 1]) continue;
            csJD_JD2str(qi + cs_J2000, str);
            mmString_Appends(&s1, " ");
            mmString_Appends(&s1, csLunarOBB_jqmc[(j + 24) % 24]);
            mmString_Appends(&s1, " ");
            mmString_Appendsn(&s1, &str[6], 5);

            v = csLunarOBB_qi_accurate2(qi);
            csJD_JD2str(v + cs_J2000, str);
            if (int2(v + 0.5) != qi)
            {
                // s2 = '<font color=red>' + s2 + '</font>';
                mmString_Assigns(&s2, "[");
                mmString_Appendsn(&s2, &str[9], 11);
                mmString_Appends(&s2, "]");
            }
            else
            {
                mmString_Assigns(&s2, "(");
                mmString_Appendsn(&s2, &str[9], 11);
                mmString_Appends(&s2, ")");
            }
            //v=(v+0.5+J2000)%1; if(v>0.5) v=1-v; if(v<8/1440) s2 = '<u>'+s2+'</u>'; //对靠近0点的加注
            mmString_Append(&s1, &s2);
            mmString_Appends(&s1, fg);
        }
        mmString_Append(s, &s1);
        mmString_Appends(s, "\n");
    }
    mmString_Destroy(&s2);
    mmString_Destroy(&s1);
}

//String年历生成
MM_EXPORT_SXWNL
void
csLunarLun_nianLi2String(
    struct csLunarSSQ* SSQ,
    int y,
    struct mmString* s)
{
    int i, j;
    double v, qi;
    double v2;
    char str[32] = { 0 };
    struct mmString s1, s2;
    
    // 消除可能的未初始化警告
    qi = 0;

    mmString_Assigns(s, "");

    mmString_Init(&s1);
    mmString_Init(&s2);
    csLunarSSQ_calcY(SSQ, int2((y - 2000)*365.2422 + 180));
    for (i = 0; i < 14; i++) {
        if (SSQ->HS[i + 1] > SSQ->ZQ[24]) break; //已包含下一年的冬至

        if (SSQ->leap && i == SSQ->leap) mmString_Assigns(&s1, "闰"); else mmString_Assigns(&s1, "·");
        mmString_Appends(&s1, SSQ->ym[i]); if (mmUTF_GetUTF8StringLength((const mmUTF8*)mmString_CStr(&s1)) < 3) mmString_Appends(&s1, "月");
        mmString_Appends(&s1, SSQ->dx[i] > 29 ? "大" : "小");

        v = SSQ->HS[i] + cs_J2000;

        mmString_Appends(&s1, " ");
        mmString_Appends(&s1, csLunarOBB_Gan[(int)fmod((v + 9), 10)]);
        mmString_Appends(&s1, csLunarOBB_Zhi[(int)fmod((v + 1), 12)]);

        csJD_JD2str(v, str);
        mmString_Appends(&s1, " ");
        mmString_Appendsn(&s1, &str[6], 5);

        for (j = -2; j < 24; j++) {
            if (j >=  0) qi = SSQ->ZQ[j];
            if (j == -1) qi = SSQ->pe[0];
            if (j == -2) qi = SSQ->pe[1];

            if (qi < SSQ->HS[i] || qi >= SSQ->HS[i + 1]) continue;
            v2 = qi + cs_J2000;

            mmString_Appends(&s1, " ");
            mmString_Appends(&s1, csLunarOBB_rmc[(int)floor(v2 - v)]);
            mmString_Appends(&s1, csLunarOBB_Gan[(int)fmod((v2 + 9), 10)]);
            mmString_Appends(&s1, csLunarOBB_Zhi[(int)fmod((v2 + 1), 12)]);

            csJD_JD2str(qi + cs_J2000, str);
            mmString_Appends(&s1, csLunarOBB_jqmc[(int)fmod((j + 24), 24)]);
            mmString_Appendsn(&s1, &str[6], 5);
        }
        mmString_Append(s, &s1);
        mmString_Appends(s, "\n");
    }
    mmString_Destroy(&s2);
    mmString_Destroy(&s1);
}

// 月相表
MM_EXPORT_SXWNL
void
csLunarLun_yxbString(
    struct csLunarLun* p,
    struct mmString* s)
{
    // 14日 02:37:36望月  29日 01:55:00朔月  
    //  7日 10:14:07上弦  20日 22:18:36下弦  
    //  7日 10:37:59小暑  23日 04:06:58大暑  

    int i;
    int n;
    struct csLunarOBDay* ob;
    struct mmString b2, b3, b4;
    char str[32];
    size_t ssz;

    mmString_Reset(s);

    mmString_Init(&b2);
    mmString_Init(&b3);
    mmString_Init(&b4);

    for (i = 0; i < p->dn; i++) 
    {
        ob = &p->day[i];
        n = i + 1; 

        if (0 == mmString_CompareCStr(&ob->yxmc, "朔") || 
            0 == mmString_CompareCStr(&ob->yxmc, "望"))
        {
            mmValue_SIntToA(&n, str);
            ssz = strlen(str);
            mmString_RAlignAppends(&b2, str, ssz, 2);
            mmString_Appends(&b2, "日 ");
            mmString_Append(&b2, &ob->yxsj);
            mmString_Append(&b2, &ob->yxmc);
            mmString_Appends(&b2, "月  ");
        }

        if (0 == mmString_CompareCStr(&ob->yxmc, "上弦") || 
            0 == mmString_CompareCStr(&ob->yxmc, "下弦"))
        {
            mmValue_SIntToA(&n, str);
            ssz = strlen(str);
            mmString_RAlignAppends(&b3, str, ssz, 2);
            mmString_Appends(&b3, "日 ");
            mmString_Append(&b3, &ob->yxsj);
            mmString_Append(&b3, &ob->yxmc);
            mmString_Appends(&b3, "  ");
        }

        if (!mmString_Empty(&ob->jqmc))
        {
            mmValue_SIntToA(&n, str);
            ssz = strlen(str);
            mmString_RAlignAppends(&b4, str, ssz, 2);
            mmString_Appends(&b4, "日 ");
            mmString_Append(&b4, &ob->jqsj);
            mmString_Append(&b4, &ob->jqmc);
            mmString_Appends(&b4, "  ");
        }
    }

    mmString_Append(s, &b2); 
    mmString_Appends(s, "\n");
    mmString_Append(s, &b3);
    mmString_Appends(s, "\n");
    mmString_Append(s, &b4);

    mmString_Destroy(&b4);
    mmString_Destroy(&b3);
    mmString_Destroy(&b2);
}

//用公历年和气候索引求时间，索引取模
MM_EXPORT_SXWNL
void
csQH_JDM(
    int Y,
    int H,
    struct csJD* pJD)
{
    int h = (72 + H - 18) % 72;
    double T = XL_S_aLon_t(((Y - 2000) + h * 5 / 360.0 + 1) * 2 * cs_pi);
    double t = T * 36525 + cs_J2000 + 8.0 / 24 - dt_T(T * 36525);
    csJD_DD(pJD, t);
}

//用公历年和气候索引求时间，索引顺延
MM_EXPORT_SXWNL
void
csQH_JDX(
    int Y,
    int H,
    struct csJD* pJD)
{
    int h = H - 18;
    double T = XL_S_aLon_t(((Y - 2000) + h * 5 / 360.0 + 1) * 2 * cs_pi);
    double t = T * 36525 + cs_J2000 + 8.0 / 24 - dt_T(T * 36525);
    csJD_DD(pJD, t);
}

//粗略算某天的气候索引
MM_EXPORT_SXWNL
int
csQH_IndexByJDE(
    struct csJD* pJD)
{
    // vx由[-9999, +9999]数值v2拟合曲线得出
    // 一次函数 F(x) = 0.00009630265341427202*x+0.1519034294816384
    // 拟合方差 2.5878858713422115e-05
    // 拟合误差 0.9999162862221544
    double jd = csJD_toJD(pJD);
    double t = jd - cs_J2000 - 8.0 / 24.0 + dt_T(jd);
    double T = t / 36525;
    double w = XL_S_aLon(T, 3);
    double vx = 0.00009630265341427202 * (pJD->Y + 4000) - 0.1519034294816384;
    double v0 = w / cs_pi2;
    double v1 = v0 - (pJD->Y - 2000) - 1;
    double v2 = v1 * 72;
    double v3 = v2 - vx + 18;
    double v4 = fmod(v3 - 72 * floor(v3 / 72), 72);
    return (int)floor(v4);
}

//获取某天的气候索引, 返回-1表示没有搜索到
MM_EXPORT_SXWNL
int
csQH_IndexByYMD(
    int Y,
    int M,
    int D,
    struct csJD* pJD)
{
    // 气候直接按天切换, 即按每天的末尾判定全天的气候标志
    int H;
    double dt;
    double jdv;
    double jd1;
    double jd2;
    struct csJD JD0;
    struct csJD JD1;
    struct csJD JD2;
    struct csJD JDV = { Y, M, D, 23, 59, 60 - 1e-6 };
    H = csQH_IndexByJDE(&JDV);

    csQH_JDX(Y - 1, 72 + H, &JD1);
    jdv = csJD_toJD(&JDV);
    jd1 = csJD_toJD(&JD1);

    dt = jd1 - jdv;
    if (-10 < dt && dt < +10)
    {
        // (+10, +10);
        // 没有跨年就在附近
        csQH_JDX(Y - 1, 71 + H, &JD0);
        csQH_JDX(Y - 1, 73 + H, &JD2);
        jd2 = csJD_toJD(&JD2);
    }
    else if (dt >= +10.0)
    {
        // [+10, +inf];
        // 跨年, 后退一年
        csQH_JDX(Y - 2, 71 + H, &JD0);
        csQH_JDX(Y - 2, 72 + H, &JD1);
        csQH_JDX(Y - 2, 73 + H, &JD2);
        jdv = csJD_toJD(&JDV);
        jd1 = csJD_toJD(&JD1);
        jd2 = csJD_toJD(&JD2);
    }
    else
    {
        // (-inf, -10];
        // 跨年, 前进一年
        csQH_JDX(Y, 71 + H, &JD0);
        csQH_JDX(Y, 72 + H, &JD1);
        csQH_JDX(Y, 73 + H, &JD2);
        jdv = csJD_toJD(&JDV);
        jd1 = csJD_toJD(&JD1);
        jd2 = csJD_toJD(&JD2);
    }

    if (jdv < jd1)
    {
        (*pJD) = JD0;
        return H - 1;
    }

    if (jd1 <= jdv && jdv < jd2)
    {
        (*pJD) = JD1;
        return H;
    }

    if (jd2 <= jdv)
    {
        (*pJD) = JD2;
        return H + 1;
    }

    return -1;
}
