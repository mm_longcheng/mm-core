/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlSheet_h__
#define __mmSxwnlSheet_h__

#include "core/mmPlatform.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "sxwnl/mmSxwnlJD.h"
#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

struct csSheetHX
{
    double vJ;               // 选择的位置经度(弧度), 默认(116.0 + 23.0 / 60.0) / cs_radd
    double vW;               // 选择的位置维度(弧度), 默认(39.9               ) / cs_radd;
    int idx;                 // 恒星库当前选中的索引, 默认1000(库0)
    int utc;                 // 当前时间是否是utc单位, 默认1 UTC/TD 标志位
    int tz;                  // 当前位置的时区, 默认-8 timezone
    int lx;                  // 类型 0 视位置 1 站坐标 2 平位置, 默认0
    int nsn;                 // 略短章标志位, 默认0, 小于35天的称短周期项
    int n;                   // 个数, 默认10
    int dt;                  // 间隔(天), 默认10
    struct mmVectorValue HXs;// 恒星库当前选中的列表
    struct mmVectorValue HXd;// 恒星库数据列表
    struct csJD JD;          // 选择的时间, 默认 { 2008, 8, 1, 10, 22, 12.0 }
};

MM_EXPORT_SXWNL
void
csSheetHX_Init(
    struct csSheetHX* p);

MM_EXPORT_SXWNL
void
csSheetHX_Destroy(
    struct csSheetHX* p);

//显示恒星库名称例表
MM_EXPORT_SXWNL
void 
csSheetHX_showHXK0(
    struct mmString* s);

//更新选中恒星库索引idx
MM_EXPORT_SXWNL
void 
csSheetHX_updateHXKIdx(
    struct csSheetHX* p);

//更新选中恒星库索引idx计算数据
MM_EXPORT_SXWNL
void 
csSheetHX_updateHXKIdxData(
    struct csSheetHX* p, 
    int all);

//显示恒星库
MM_EXPORT_SXWNL
void 
csSheetHX_showHXK(
    struct csSheetHX* p, 
    struct mmString* s);

// 恒星计算
MM_EXPORT_SXWNL
void 
csSheetHX_aCalc(
    struct csSheetHX* p, 
    struct mmString* s);

// 天象时间格式化输出
MM_EXPORT_SXWNL
void 
txFormatT(
    double t, 
    struct mmString* s);

// tianXiang( 1,0) 月亮近点 tianXiang( 2,0) 月亮远点
// tianXiang( 3,0) 月亮升交 tianXiang( 4,0) 月亮降交
// tianXiang( 5,0) 地球近日 tianXiang( 6,0) 地球远日
// tianXiang( 7,0) 水东大距 tianXiang( 8,0) 水西大距
// tianXiang( 9,0) 金东大距 tianXiang(10,0) 金西大距
//
// [赤经合月] 
// tianXiang(11,1) 水 
// tianXiang(11,2) 金       tianXiang(11,3) 火 
// tianXiang(11,4) 木       tianXiang(11,5) 土 
// tianXiang(11,6) 天       tianXiang(11,7) 海 
//
// [黄经合日] 
// tianXiang(12,1) 水上 
// tianXiang(12,2) 金上     tianXiang(12,3) 火 
// tianXiang(12,4) 木       tianXiang(12,5) 土 
// tianXiang(12,6) 天       tianXiang(12,7) 海 
//
// [行星顺留] 
// tianXiang(14,1) 水 
// tianXiang(14,2) 金       tianXiang(14,3) 火 
// tianXiang(14,4) 木       tianXiang(14,5) 土 
// tianXiang(14,6) 天       tianXiang(14,7) 海 
//
// [黄经冲日] 
// tianXiang(13,1) 水下 
// tianXiang(13,2) 金下     tianXiang(13,3) 火 
// tianXiang(13,4) 木       tianXiang(13,5) 土 
// tianXiang(13,6) 天       tianXiang(13,7) 海 
//
// [行星逆留] 
// tianXiang(15,1) 水 
// tianXiang(15,2) 金       tianXiang(15,3) 火 
// tianXiang(15,4) 木       tianXiang(15,5) 土 
// tianXiang(15,6) 天       tianXiang(15,7) 海 
//
// 通常 n = 10
MM_EXPORT_SXWNL
void 
tianXiang(
    int xm, 
    int xm2, 
    const struct csJD* dat, 
    int n, 
    struct mmString* s);

//
// 通常 utc = 1 n = 10 dt = 1
// 行星星历计算
MM_EXPORT_SXWNL
void 
pCalc(
    int xt, 
    const struct csJD* dat, 
    double vJ, double vW, 
    int utc, 
    int n, 
    int dt, 
    struct mmString* s);

// 通常 n = 24 jiao = 0
//定朔测试函数
MM_EXPORT_SXWNL
void 
suoCalc(
    int y, 
    int n, 
    int jiao, 
    struct mmString* s);

// 通常 n = 24 
// 定气测试函数
MM_EXPORT_SXWNL
void 
qiCalc(
    int y, 
    int n, 
    struct mmString* s);

// 通常 n = 24 
// 定候测试函数
MM_EXPORT_SXWNL
void 
houCalc(
    int y, 
    int n, 
    struct mmString* s);

// 通常 y = 2000 N = 10
// 定气误差测试
MM_EXPORT_SXWNL
void 
dingQi_cmp(
    int y, 
    int N, 
    struct mmString* s);

// 通常 y = 2000 N = 10
// 定朔测试函数
MM_EXPORT_SXWNL
void 
dingSuo_cmp(
    int y, 
    int N, 
    struct mmString* s);

// 定气计算速度测试
MM_EXPORT_SXWNL
void 
dingQi_v(
    struct mmString* s);

// 定朔计算速度测试
MM_EXPORT_SXWNL
void 
dingSuo_v(
    struct mmString* s);

// 命理八字表
MM_EXPORT_SXWNL
void 
ML_calc(
    const struct csJD* dat, 
    double vJ, 
    struct mmString* s);

#include "core/mmSuffix.h"

#endif//__mmSxwnlSheet_h__
