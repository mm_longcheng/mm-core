/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlEphB.h"
#include "mmSxwnlConst.h"
#include "mmSxwnlEph0.h"
#include "mmSxwnlJD.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmValueTransform.h"

//=================================地球SSB位置和速度================================
//==================================================================================
/****************************
算法取自《天文算法》
****************************/

//地球的SSB速度计算表
MM_EXPORT_SXWNL const int evTab[] =
{ 
 1,1,  0,0, 0,0,-1719914,  -25,   25,1578089,   10,684185, //01
 2,1,  0,0, 0,0,    6434,28007,25697,  -5904,11141, -2559,
 1,3,  0,0, 0,0,     715,    0,    6,   -657,  -15,  -282,
 1,7,  0,0, 0,0,     715,    0,    0,   -656,    0,  -285,
 3,1,  0,0, 0,0,     486, -236, -216,   -446,  -94,  -193,
 1,4,  0,0, 0,0,     159,    0,    2,   -147,   -6,   -61, //06
 1,10, 0,0, 0,0,  0,  0,  0, 26,  0,-59,
 1,7,  1,9, 0,0, 39,  0,  0,-36,  0,-16,
 2,3,  0,0, 0,0, 33,-10, -9,-30, -5,-13,
 2,1, -1,3, 0,0, 31,  1,  1,-28,  0,-12,
 3,1, -8,2, 3,3,  8,-28, 25,  8, 11,  3, //11
 5,1, -8,2, 3,3,  8,-28,-25, -8,-11, -3,
 2,0, -1,1, 0,0, 21,  0,  0,-19,  0, -8,
 1,0,  0,0, 0,0,-19,  0,  0, 17,  0,  8,
 1,5,  0,0, 0,0, 17,  0,  0,-16,  0, -7,
 1,1, -2,3, 0,0, 16,  0,  0, 15,  1,  7, //16
 1,6,  0,0, 0,0, 16,  0,  1,-15, -3, -6,
 1,1,  1,3, 0,0, 11, -1, -1,-10, -1, -5,
 2,0, -2,1, 0,0,  0,-11,-10,  0, -4,  0,
 1,1, -1,3, 0,0,-11, -2, -2,  9, -1,  4,
 4,1,  0,0, 0,0, -7, -8, -8,  6, -3,  3, //21
 3,1, -2,3, 0,0,-10,  0,  0,  9,  0,  4,
 1,0, -2,1, 0,0, -9,  0,  0, -9,  0, -4,
 2,0, -3,1, 0,0, -9,  0,  0, -8,  0, -4,
 2,4,  0,0, 0,0,  0, -9, -8,  0, -3,  0,
 2,0, -4,1, 0,0,  0, -9,  8,  0,  3,  0, //26
 3,1, -2,2, 0,0,  8,  0,  0, -8,  0, -3,
 1,7,  2,8,-1,9,  8,  0,  0, -7,  0, -3,
 8,0,-12,1, 0,0, -4, -7, -6,  4, -3,  2,
 8,0,-14,1, 0,0, -4, -7,  6, -4,  3, -2,
 2,2,  0,0, 0,0, -6, -5, -4,  5, -2,  2, //31
 3,0, -4,1, 0,0, -1, -1, -2, -7,  1, -4,
 2,1, -2,3, 0,0,  4, -6, -5, -4, -2, -2,
 3,0, -3,1, 0,0,  0, -7, -6,  0, -3,  0,
 2,1, -2,2, 0,0,  5, -5, -4, -5, -2, -2,
 1,7, -2,8, 0,0,  5,  0,  0, -5,  0, -2, //36

 1,1,  0,0, 0,0, -2,   0,-13, 156, 32,-358, //本行开始为泊松项
 2,1,  0,0, 0,0,141,-107,-95,-130,-48,-55,
 3,1,  0,0, 0,0, -5,  -4, -4,   5,  0,  0
};

//地球的SSB速度, t儒略世纪, 速度单位AU/世纪, 平均误差4*10^-8AU/日
MM_EXPORT_SXWNL
void
evSSB(
    double v[3],
    double t)
{
    const double J[] = 
    {
      3.1761467 + 1021.3285546*t, //金
      1.7534703 +  628.3075849*t, //地
      6.2034809 +  334.0612431*t, //火
      0.5995465 +   52.9690965*t, //木
      0.8740168 +   21.3299095*t, //土
      5.4812939 +    7.4781599*t, //天
      5.3118863 +    3.8133036*t, //海
      3.8103444 + 8399.6847337*t, //月 L'
      5.1984667 + 7771.3771486*t, //月 D
      2.3555559 + 8328.6914289*t, //月 M'
      1.6279052 + 8433.4661601*t  //月 F
    };
    int i, k;
    double c, S, C, Tn = 1;
    const int* F = evTab;
    v[0] = 0; v[1] = 0; v[2] = 0;
    for (i = 0; i < 39; i++) {
        if (i >= 36) Tn = t;
        k = i * 12;
        c = F[k] * J[F[k + 1]] + F[k + 2] * J[F[k + 3]] + F[k + 4] * J[F[k + 5]];
        S = sin(c); C = cos(c);
        v[0] += (F[k +  6] * S + F[k + 7] * C) * Tn;
        v[1] += (F[k +  8] * S + F[k + 9] * C) * Tn;
        v[2] += (F[k + 10] * S + F[k + 11] * C)* Tn;
    }
    v[0] *= 0.00036525; v[1] *= 0.00036525; v[2] *= 0.00036525;
}

MM_EXPORT_SXWNL const double epTab[] =
{
 999829, 1.753486, 6283.07585,999892, 0.182659, 6283.07585,
   8353, 1.710345,12566.1517,  24427, 3.141593,    0,
   5611, 0,           0,        8353, 0.139529,12566.1517,
    105, 1.667226,18849.22755,   105, 0.096417,18849.22755,

 31, 0.668752,83996.8473181, 31, 5.381141,83996.8473181,
 26, 0.583102,  529.6909651, 26, 5.30104,   529.6909651,
 21, 1.092352, 1577.3435424, 21, 2.662535, 1577.3435424,
 17, 0.495402, 6279.5527316, 17, 5.207804, 6279.5527316,
 17, 6.153155, 6286.5989683, 17, 4.582329, 6286.5989683,
 14, 3.472728, 2352.8661538, 14, 1.900682, 2352.8661538,
 11, 3.689848, 5223.6939198, 11, 5.273134, 5223.6939198,
  9, 6.073899,12036.4607349,  9, 4.503012,12036.4607349,
  9, 3.17572, 10213.2855462,  9, 1.605633,10213.2855462,
  6, 2.15262,  1059.3819302,  6, 0.581422, 1059.3819302,
  7, 1.30699,  5753.3848849,  7, 2.807289,  398.1490034,
  7, 4.355002,  398.1490034,  6, 6.029239, 5753.3848849,
  7, 2.218215, 4705.7323075,  7, 0.647296, 4705.7323075,
  6, 5.384792, 6812.7668151,  6, 3.813815, 6812.7668151,
  5, 6.087683, 5884.9268466,  5, 4.527856, 5884.9268466,
  5, 1.279337, 6256.7775302,  5, 5.991672, 6256.7775302,

  -5196635 / 1048, 0.599451, 529.6909651,  -5195200 / 1048, 5.312032, 529.6909651, //木星
  -9516383 / 3504, 0.874414, 213.2990954,  -9529869 / 3504, 5.586006, 213.2990954, //土星
 -30058900 / 19434,5.312113,  38.1330356, -30060560 / 19434,3.740863,  38.1330356, //海王
 -19173710 / 22927,5.481334,  74.7815986, -19165180 / 22927,3.910457,  74.7815986  //天王
};

//地心SSB坐标,t儒略世纪数,4位有效数字
MM_EXPORT_SXWNL
void
epSSB(
    double v[3],
    double t)
{
    int i;
    double x = 0, y = 0, z = 0, E;
    const double* p = epTab;
    t /= 10;
    for (i = 0; i < csArraySize(epTab); i += 6) {
        x += p[i + 0] * cos(p[i + 1] + p[i + 2] * t);
        y += p[i + 3] * cos(p[i + 4] + p[i + 5] * t);
    }
    x += t * (1234 + 515 * cos(6.002663 + 12566.1517*t) + 13 * cos(5.959431 + 18849.22755*t) + 11 * cos(2.015542 + 6283.07585*t));
    y += t * ( 930 + 515 * cos(4.431805 + 12566.1517*t) + 13 * cos(4.388605 + 18849.22755*t));
    z += t * ( 54 + 2278 * cos(3.413725 + 6283.07585*t) + 19 * cos(3.370613 + 12566.15170*t));
    x /= 1000000; y /= 1000000; z /= 1000000;

    E = -84381.448 / cs_rad;

    v[0] = x;
    v[1] = z * sin(E) + y * cos(E);
    v[2] = z * cos(E) - y * sin(E);
}

//=================================引力偏转=========================================
//==================================================================================
//引力偏转,z天体赤道坐标,a太阳赤道坐标
MM_EXPORT_SXWNL
void
ylpz(
    double r[3],
    const double z[3],
    const double a[3])
{
    double d, D;
    r[0] = z[0]; r[1] = z[1]; r[2] = z[2];
    d = z[0] - a[0];
    D = sin(z[1])*sin(a[1]) + cos(z[1])*cos(a[1])*cos(d); //角度差的余弦
    D = 0.00407 * (1 / (1 - D) + D / 2) / cs_rad;
    r[0] += D * (cos(a[1]) * sin(d) / cos(z[1]));
    r[1] += D * (sin(z[1]) * cos(a[1])*cos(d) - sin(a[1])*cos(z[1]));
    r[0] = rad2mrad(r[0]);
}

//=================================低精度周年光行差=================================
//==================================================================================
/***********
function getGxcConst(t){ //取恒星光行差计算相关的常数
 var t2=t*t, t3=t2*t, t4=t3*t, t5=t4*t, r=new Object();
 r.K=20.49552/cs_rad;        //光行差常数
 r.L= (280.4664567 +36000.76982779*t +0.0003032028*t2 +1/49931000*t3 -1/153000000*t4 -5e-12*t5)/180*Math.PI; //太阳的几何平黄经
 r.p= (102.93735 +1.71946*t +0.00046*t2)/180*Math.PI; //近点
 r.e=0.016708634 -0.000042037*t -0.0000001267*t2;
 return r;
}
function HDzngxc(t,a){ //光行差黄道修正(恒星周年光行差),精度一般
 var r=getGxcConst(t); //取光行差计算的常数或系数
 var dL=r.L-a[0], dP=r.p-a[0];
 var b = new Array();
 b[0] = a[0] - r.K*(cos(dL)-r.e*cos(dP)) / cos(a[1]);
 b[1] = a[1] - r.K*sin(a[1]) * (sin(dL)-r.e*sin(dP));
 b[2] = a[2];
 return b;
}
function CDzngxc(t,a){ //计算周年光行差对赤道坐标的影响值,t儒略世纪数(J2000起算),精度一般
 //先把周日运动看圆周运动
 var r=getGxcConst(t);            //取光行差计算的常数或系数
 var E=prece(t,'E','P03');  //黄赤交角,取0.409也可以,对结果影响不很大
 var sinL=sin(r.L), cosL=cos(r.L);
 var cosE=cos(E), tanE=tan(E);
 var sinR=sin(a[0]),cosR=cos(a[0]);
 var sinD=sin(a[1]),cosD=cos(a[1]);
 var tcss=tanE*cosD-sinR*sinD;
 var b = new Array();
 b[0] = a[0] - r.K*(cosR*cosE*cosL+sinR*sinL)/cosD;  //赤经周年光行差修正值
 b[1] = a[1] - r.K*(cosL*cosE*tcss+cosR*sinD*sinL);  //赤纬的
 //e项修正(考虑非正圆运动的误差)
 var sinp=sin(r.p), cosp=cos(r.p);
 b[0] += r.e*r.K*(cosR*cosE*cosp+sinR*sinp)/cosD;
 b[1] += r.e*r.K*(cosp*cosE*tcss+cosR*sinD*sinp);
 b[2] = a[2];
 return b;
}
***********/

//=================================周年视差或光行差=================================
//==================================================================================
//严格的恒星视差或光行差改正
MM_EXPORT_SXWNL
void
scGxc(
    double r[3],
    const double z[3],
    const double v[3],
    int f)
{
    //z为某时刻天体赤道坐标, 球面坐标, z含自行但不含章动和光行差
    //v为同是刻地球赤道坐标, 直角坐标
    //f=0 进行光行差改正, v须为SSB速度, 本函数返回z+v向量(z向径为光速)
    //f=1 做周年视差改正, v须为SSB位置, 本函数返回z-v向量(z向径为距离)
    //z和v应统一使用J2000赤道坐标系
    double c;
    double sinJ, cosJ, sinW, cosW;
    r[0] = z[0]; r[1] = z[1]; r[2] = z[2];
    c = cs_GS / cs_AU * 86400 * 36525; //光速,AU每儒略世纪
    if (f) c = -z[2]; //求视差

    sinJ = sin(z[0]); cosJ = cos(z[0]);
    sinW = sin(z[1]); cosW = cos(z[1]);
    r[0] += rad2mrad((v[1] * cosJ - v[0] * sinJ) / cosW / c);
    r[1] += (v[2] * cosW - (v[0] * cosJ + v[1] * sinJ) * sinW) / c;
}

//=================================太阳J2000球面坐标================================
//==================================================================================
MM_EXPORT_SXWNL
void
sun2000(
    double a[3],
    double t,
    int n)
{
    e_coord(a, t, n, n, n);
    a[0] += cs_pi; a[1] = -a[1]; //太阳Date黄道坐标
    HDllr_D2J(a, t, a, csPMX_P03);  //转到J2000坐标
}

//88星座表。结构：汉语+缩写,面积(平方度),赤经 (时分),赤纬(度分),象限角 族 星座英文名
MM_EXPORT_SXWNL const char* xz88[] =
{
 "仙女座And", " 722.278", " 0 48.46", " 37 25.91", "NQ1 英仙 Andromeda",    //01
 "唧筒座Ant", " 238.901", "10 16.43", "-32 29.01", "SQ2 拉卡伊 Antlia", 
 "天燕座APS", " 206.327", "16 08.65", "-75 18.00", "SQ3 拜耳 Apus", 
 "宝瓶座Aqr", " 979.854", "22 17.38", "-10 47.35", "SQ4 黄道 Aquarius", 
 "天鹰座Aql", " 652.473", "19 40.02", "  3 24.65", "NQ4 武仙 Aquila", 
 "天坛座Ara", " 237.057", "17 22.49", "-56 35.30", "SQ3 武仙 Ara", 
 "白羊座Ari", " 441.395", " 2 38.16", " 20 47.54", "NQ1 黄道 Aries", 
 "御夫座Aur", " 657.438", " 6 04.42", " 42 01.68", "NQ2 英仙 Auriga", 
 "牧夫座Boo", " 906.831", "14 42.64", " 31 12.16", "NQ3 大熊 Bootes", 
 "雕具座Cae", " 124.865", " 4 42.27", "-37 52.90", "SQ1 拉卡伊 Caelum",     //10
 "鹿豹座Cam", " 756.828", " 8 51.37", " 69 22.89", "NQ2 大熊 Camelopardalis", 
 "巨蟹座Cnc", " 505.872", " 8 38.96", " 19 48.35", "NQ2 黄道 Cancer", 
 "猎犬座CVn", " 465.194", "13 06.96", " 40 06.11", "NQ3 大熊 Canes Venatici", 
 "大犬座CMa", " 380.118", " 6 49.74", "-22 08.42", "SQ2 猎户 Canis Major", 
 "小犬座CMi", " 183.367", " 7 39.17", "  6 25.63", "NQ2 猎户 Canis Minor", 
 "摩羯座CAP", " 413.947", "21 02.93", "-18 01.39", "SQ4 黄道 Capricornus", 
 "船底座Car", " 494.184", " 8 41.70", "-63 13.16", "SQ2 幻之水 Carina", 
 "仙后座Cas", " 598.407", " 1 19.16", " 62 11.04", "NQ1 英仙 Cassiopeia", 
 "半人马Cen", "1060.422", "13 04.27", "-47 20.72", "SQ3 武仙 Centaurus", 
 "仙王座Cep", " 587.787", "22 00.00", " 71 00.51", "NQ4 英仙 Cepheus",      //20
 "鲸鱼座Cet", "1231.411", " 1 40.10", " -7 10.76", "SQ1 英仙 Cetus", 
 "堰蜒座Cha", " 131.592", "10 41.53", "-79 12.30", "SQ2 拜耳 Chamaeleon", 
 "圆规座Cir", "  93.353", "14 34.54", "-63 01.82", "SQ3 拉卡伊 Circinus", 
 "天鸽座Col", " 270.184", " 5 51.76", "-35 05.67", "SQ1 幻之水 Columba", 
 "后发座Com", " 386.475", "12 47.27", " 23 18.34", "NQ3 大熊 Coma Berenices", 
 "南冕座CrA", " 127.696", "18 38.79", "-41 08.85", "SQ4 武仙 Corona Australis", 
 "北冕座CrB", " 178.710", "15 50.59", " 32 37.49", "NQ3 大熊 Corona Borealis", 
 "乌鸦座Crv", " 183.801", "12 26.52", "-18 26.20", "SQ3 武仙 Corvus", 
 "巨爵座Crt", " 282.398", "11 23.75", "-15 55.74", "SQ2 武仙 Crater", 
 "南十字Cru", "  68.447", "12 26.99", "-60 11.19", "SQ3 武仙 Crux",         //30
 "天鹅座Cyg", " 803.983", "20 35.28", " 44 32.70", "NQ4 武仙 Cygnus", 
 "海豚座Del", " 188.549", "20 41.61", " 11 40.26", "NQ4 幻之水 Delphinus", 
 "剑鱼座Dor", " 179.173", " 5 14.51", "-59 23.22", "SQ1 拜耳 Dorado", 
 "天龙座Dra", "1082.952", "15 08.64", " 67 00.40", "NQ3 大熊 Draco", 
 "小马座Equ", "  71.641", "21 11.26", "  7 45.49", "NQ4 幻之水 Equuleus", 
 "波江座Eri", "1137.919", " 3 18.02", "-28 45.37", "SQ1 幻之水 Eridanus", 
 "天炉座For", " 397.502", " 2 47.88", "-31 38.07", "SQ1 拉卡伊 Fornax", 
 "双子座Gem", " 513.761", " 7 04.24", " 22 36.01", "NQ2 黄道 Gemini", 
 "天鹤座Gru", " 365.513", "22 27.39", "-46 21.11", "SQ4 拜耳 Grus", 
 "武仙座Her", "1225.148", "17 23.16", " 27 29.93", "NQ3 武仙 Hercules",     //40
 "时钟座Hor", " 248.885", " 3 16.56", "-53 20.18", "SQ1 拉卡伊 Horologium", 
 "长蛇座Hya", "1302.844", "11 36.73", "-14 31.91", "SQ2 武仙 Hydra", 
 "水蛇座Hyi", " 243.035", " 2 20.65", "-69 57.39", "SQ1 拜耳 Hydrus", 
 "印第安Ind", " 294.006", "21 58.33", "-59 42.40", "SQ4 拜耳 Indus", 
 "蝎虎座Lac", " 200.688", "22 27.68", " 46 02.51", "NQ4 英仙 Lacerta", 
 "狮子座Leo", " 946.964", "10 40.03", " 13 08.32", "NQ2 黄道 Leo", 
 "小狮座LMi", " 231.956", "10 14.72", " 32 08.08", "NQ2 大熊 Leo Minor", 
 "天兔座Lep", " 290.291", " 5 33.95", "-19 02.78", "SQ1 猎户 Lepus", 
 "天秤座Lib", " 538.052", "15 11.96", "-15 14.08", "SQ3 黄道 Libra", 
 "豺狼座Lup", " 333.683", "15 13.21", "-42 42.53", "SQ3 武仙 Lupus",        //50
 "天猫座Lyn", " 545.386", " 7 59.53", " 47 28.00", "NQ2 大熊 Lynx", 
 "天琴座Lyr", " 286.476", "18 51.17", " 36 41.36", "NQ4 武仙 Lyra", 
 "山案座Men", " 153.484", " 5 24.90", "-77 30.24", "SQ1 拉卡伊 Mensa", 
 "显微镜Mic", " 209.513", "20 57.88", "-36 16.49", "SQ4 拉卡伊 Microscopium", 
 "麒麟座Mon", " 481.569", " 7 03.63", "  0 16.93", "NQ2 猎户 Monoceros", 
 "苍蝇座Mus", " 138.355", "12 35.28", "-70 09.66", "SQ3 拜耳 Musca", 
 "矩尺座Nor", " 165.290", "15 54.18", "-51 21.09", "SQ3 拉卡伊 Norma", 
 "南极座Oct", " 291.045", "23 00.00", "-82 09.12", "SQ4 拉卡伊 Octans", 
 "蛇夫座Oph", " 948.340", "17 23.69", " -7 54.74", "SQ3 武仙 Ophiuchus", 
 "猎户座Ori", " 594.120", " 5 34.59", "  5 56.94", "NQ1 猎户 Orion",        //60
 "孔雀座Pav", " 377.666", "19 36.71", "-65 46.89", "SQ4 拜耳 Pavo", 
 "飞马座Peg", "1120.794", "22 41.84", " 19 27.98", "NQ4 英仙 Pegasus", 
 "英仙座Per", " 614.997", " 3 10.50", " 45 00.79", "NQ1 英仙 Perseus", 
 "凤凰座Phe", " 469.319", " 0 55.91", "-48 34.84", "SQ1 拜耳 Phoenix", 
 "绘架座Pic", " 246.739", " 5 42.46", "-53 28.45", "SQ1 拉卡伊 Pictor", 
 "双鱼座Psc", " 889.417", " 0 28.97", " 13 41.23", "NQ1 黄道 Pisces", 
 "南鱼座PsA", " 245.375", "22 17.07", "-30 38.53", "SQ4 幻之水 Piscis Austrinus", 
 "船尾座Pup", " 673.434", " 7 15.48", "-31 10.64", "SQ2 幻之水 Puppis", 
 "罗盘座Pyx", " 220.833", " 8 57.16", "-27 21.10", "SQ2 幻之水 Pyxis", 
 "网罟座Ret", " 113.936", " 3 55.27", "-59 59.85", "SQ1 拉卡伊 Reticulum",  //70
 "天箭座Sge", "  79.932", "19 39.05", " 18 51.68", "NQ4 武仙 Sagitta", 
 "人马座Sgr", " 867.432", "19 05.94", "-28 28.61", "SQ4 黄道 Sagittarius", 
 "天蝎座Sco", " 496.783", "16 53.24", "-27 01.89", "SQ3 黄道 Scorpius", 
 "玉夫座Scl", " 474.764", " 0 26.28", "-32 05.30", "SQ1 拉卡伊 Sculptor", 
 "盾牌座Sct", " 109.114", "18 40.39", " -9 53.32", "SQ4 武仙 Scutum", 
 "巨蛇座Ser", " 636.928", "16 57.04", "  6 07.32", "NQ3 武仙 Serpens", 
 "六分仪Sex", " 313.515", "10 16.29", " -2 36.88", "SQ2 武仙 Sextans", 
 "金牛座Tau", " 797.249", " 4 42.13", " 14 52.63", "NQ1 黄道 Taurus", 
 "望远镜Tel", " 251.512", "19 19.54", "-51 02.21", "SQ4 拉卡伊 Telescopium", 
 "三角座Tri", " 131.847", " 2 11.07", " 31 28.56", "NQ1 英仙 Triangulum",   //80
 "南三角TrA", " 109.978", "16 04.95", "-65 23.28", "SQ3 武仙 Triangulum Australe", 
 "杜鹃座Tuc", " 294.557", "23 46.64", "-65 49.80", "SQ4 拜耳 Tucana", 
 "大熊座UMa", "1279.660", "11 18.76", " 50 43.27", "NQ2 大熊 Ursa Major", 
 "小熊座UMi", " 255.864", "15 00.00", " 77 41.99", "NQ3 大熊 Ursa Minor", 
 "船帆座Vel", " 499.649", " 9 34.64", "-47 10.03", "SQ2 幻之水 Vela", 
 "室女座Vir", "1294.428", "13 24.39", " -4 09.51", "SQ3 黄道 Virgo", 
 "飞鱼座Vol", " 141.354", " 7 47.73", "-69 48.07", "SQ2 拜耳 Volans", 
 "狐狸座Vul", " 268.165", "20 13.88", " 24 26.56", "NQ4 武仙 Vulpecula",
};

MM_EXPORT_SXWNL const int xz88Length = csArraySize(xz88);

//恒星库
// RA(时分秒)   DEC(度分秒)   自行1  自行2  视差  星等  星名  星座#
const char* HXK0[] =
{
    // "库0#"
    "* 0 01 57.620", "- 6 00 50.68", " 0.0031", " -0.041", " 0.008", " 4.37 ", "星1630 ", "Psc 30 M3#",
    "* 0 03 44.391", "-17 20 09.58", " 0.0020", " -0.007", " 0.014", " 4.55 ", "星905  ", "Cet 2  B9#",
    "* 0 05 20.142", "- 5 42 27.45", "-0.0009", "  0.089", " 0.025", " 4.61 ", "星1002 ", "Psc 33 K1#",
    "* 0 08 23.260", " 29 05 25.54", " 0.0104", " -0.163", " 0.034", " 2.07 ", "星1    ", "And α B9#",
    "* 0 09 10.686", " 59 08 59.19", " 0.0681", " -0.180", " 0.060", " 2.28 ", "星2    ", "Cas β F2#",
    "* 0 10 19.247", " 46 04 20.17", " 0.0005", "  0.001", " 0.003", " 5.01 ", "星4    ", "And 22 F2#",
    "* 0 11 34.421", "-27 47 59.06", " 0.0003", "  0.016", " 0.006", " 5.41 ", "星5    ", "Scl κ2 K2#",
    "* 0 11 44.010", "-35 07 59.24", " 0.0138", "  0.115", " 0.046", " 5.24 ", "星6    ", "Scl θ F3#",
    "* 0 13 14.154", " 15 11 00.93", " 0.0003", " -0.008", " 0.010", " 2.83 ", "星7    ", "Peg γ B2#",
    "* 0 14 36.165", " 20 12 24.12", " 0.0064", " -0.001", " 0.010", " 4.79 ", "星1004 ", "Peg χ M2#",
    "* 0 17 05.500", " 38 40 53.87", "-0.0046", " -0.013", " 0.013", " 4.61 ", "N30    ", "And θ A2#",
    "* 0 18 19.658", " 36 47 06.79", "-0.0055", " -0.042", " 0.023", " 4.51 ", "星1005 ", "And σ A2#",
    "* 0 18 38.258", " 31 31 02.01", " 0.0044", " -0.004", " 0.006", " 5.88 ", "星1006 ", "Pi 0h38 A0#",
    "* 0 19 25.676", "- 8 49 26.14", "-0.0010", " -0.037", " 0.011", " 3.56 ", "星9    ", "Cet ι K2#",
    "* 0 20 35.863", "  8 11 24.96", "-0.0003", "  0.010", " 0.008", " 5.38 ", "星1008 ", "Psc 41 K3#",
    "* 0 21 07.270", " 37 58 06.95", " 0.0049", " -0.039", " 0.020", " 5.16 ", "星1009 ", "And ρ F5#",
    "* 0 24 47.506", " 61 49 51.80", " 0.0018", " -0.002", " 0.004", " 5.38 ", "GC     ", "Cas 12 B9#",
    "* 0 25 24.210", "  1 56 22.87", "-0.0010", " -0.013", " 0.006", " 5.77 ", "星1010 ", "Psc 44 G5#",
    "* 0 25 45.092", "-77 15 15.30", " 0.6689", "  0.323", " 0.134", " 2.82 ", "星11   ", "Hyi β G2#",
    "* 0 26 17.052", "-42 18 21.55", " 0.0210", " -0.354", " 0.042", " 2.40 ", "星12   ", "Phe α K0#",
};
const char* HXK1[] =
{
    // "库1#'
    "* 0 48 22.978", "  5 16 50.19", "  0.0507", "-1.141", "0.134", " 5.74", "星1019", "G.Psc 96 K2#",
    "* 0 26 17.052", "-42 18 21.55", "  0.0210", "-0.354", "0.042", " 2.40", "星12  ", "Phe α   K0#",
    "* 2 36 00.049", "- 7 49 53.77", " -0.0022", "-0.060", "0.006", " 5.53", "星1074", "Cet 80   M0#",
    "* 2 35 52.472", "  5 35 35.67", " -0.0019", "-0.024", "0.009", " 4.87", "星1072", "Cet υ   G8#",

    "*18 36 27.834", "  9 07 20.98", " -0.0001", "-0.132", "0.026", " 5.38", "星1484", "Oph 9  F5#",
    "*18 36 56.338", " 38 47 01.29", "  0.0172", " 0.288", "0.129", " 0.03", "星699", " Lyr α A0#",
    "*18 37 54.426", "-21 23 51.81", " -0.0001", "-0.066", "0.010", " 5.93", "星1485", "Sgr 83 A5#",
    "*18 42 16.428", "- 9 03 09.14", "  0.0005", "-0.002", "0.017", " 4.70", "星1486", "Sct δ F2#",
    "*18 43 31.254", "- 8 16 30.76", "  0.0013", " 0.008", "0.006", " 4.88", "星702", " Sct ε G8#",
};
MM_EXPORT_SXWNL const char** HXK[] =
{
    HXK0, HXK1,
};

MM_EXPORT_SXWNL const int HXKLength[] =
{
    csArraySize(HXK0),
    csArraySize(HXK1),
};

MM_EXPORT_SXWNL const int HXKLLength = csArraySize(HXKLength);

MM_EXPORT_SXWNL
void
csEphHX_Init(
    struct csEphHX* p)
{
    mmString_Init(&p->XH);
    mmString_Init(&p->RA);
    mmString_Init(&p->DEC);
    mmString_Init(&p->zx1);
    mmString_Init(&p->zx2);
    mmString_Init(&p->sc);
    mmString_Init(&p->xd);
    mmString_Init(&p->xm);
    mmString_Init(&p->xz);
}

MM_EXPORT_SXWNL
void
csEphHX_Destroy(
    struct csEphHX* p)
{
    mmString_Destroy(&p->xz);
    mmString_Destroy(&p->xm);
    mmString_Destroy(&p->xd);
    mmString_Destroy(&p->sc);
    mmString_Destroy(&p->zx2);
    mmString_Destroy(&p->zx1);
    mmString_Destroy(&p->DEC);
    mmString_Destroy(&p->RA);
    mmString_Destroy(&p->XH);
}

MM_EXPORT_SXWNL
void
csEphHXs_Init(
    struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &csEphHX_Init;
    hValueAllocator.Recycle = &csEphHX_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct csEphHX));
}

MM_EXPORT_SXWNL
void
csEphHXs_Destroy(
    struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

//整个星库引用
MM_EXPORT_SXWNL
void
idxHXK(
    int i,
    struct mmVectorValue* r)
{
    int j;

    const char** K;
    const char** R;

    struct csEphHX* HXs;
    struct csEphHX* u;

    size_t length = HXKLength[i] / 8;
    mmVectorValue_Clear(r);
    mmVectorValue_AllocatorMemory(r, length);
    HXs = (struct csEphHX*)r->arrays;

    K = HXK[i];

    for (j = 0; j < HXKLength[i] / 8; j++)
    {
        R = &K[j * 8];
        u = &HXs[j];

        mmString_Assignsn(&u->XH, R[0], 1);
        mmString_Assignsn(&u->RA, &R[0][1], strlen(R[0]) - 1);
        mmString_Assigns(&u->DEC, R[1]);
        mmString_Assigns(&u->zx1, R[2]);
        mmString_Assigns(&u->zx2, R[3]);
        mmString_Assigns(&u->sc, R[4]);
        mmString_Assigns(&u->xd, R[5]);
        mmString_Assigns(&u->xm, R[6]);
        mmString_Assignsn(&u->xz, R[7], strlen(R[7]) - 1);
    }
}

//星库检索
MM_EXPORT_SXWNL
void
schHXK(
    const char* key,
    struct mmVectorValue* r)
{
    int i, j;

    const char** K;
    const char** R;

    struct csEphHX* u;

    mmVectorValue_Clear(r);

    //遍历星座表
    for (i = 0; i < xz88Length / 5; i++)
    {
        R = &xz88[i * 5];
        if (0 == mmMemcmp(R[0] + strlen(R[0]) - 3, key, 3))
        {
            // "仙女座And", " 722.278", " 0 48.46", " 37 25.91", "NQ1 英仙 Andromeda",    //01

            const char* a = R[2]; // " 0 48.46"
            const char* b = R[3]; // " 37 25.91"
            char as[16] = { 0 };
            char bs[16] = { 0 };

            mmVectorValue_AllocatorMemory(r, r->size + 1);
            u = (struct csEphHX*)mmVectorValue_At(r, r->size - 1);

            mmMemcpy(as, a, 5); as[5] = ' '; toFixed(&as[6], atoi(&a[6])*0.6, 1);
            mmMemcpy(bs, b, 6); bs[6] = ' '; toFixed(&bs[7], atoi(&b[7])*0.6, 1);

            mmString_Assigns(&u->XH, "*");
            mmString_Assigns(&u->RA, as);
            mmString_Assigns(&u->DEC, bs);
            mmString_Assigns(&u->zx1, "0");
            mmString_Assigns(&u->zx2, "0");
            mmString_Assigns(&u->sc, "0");
            mmString_Assigns(&u->xd, "0.0");

            mmString_Assigns(&u->xm, "中心"); 
            mmString_Appends(&u->xm, R[1]);
            mmString_Appends(&u->xm, "方");

            mmString_Assigns(&u->xz, R[4]);
            break;
        }
    }

    //遍历所有子库
    for (i = 0; i < csArraySize(HXK); i++) 
    {
        K = HXK[i];

        for (j = 0; j < HXKLength[i] / 8; j++)
        {
            R = &K[j * 8];
            if (0 == mmMemcmp(R[7], key, 3))
            {
                mmVectorValue_AllocatorMemory(r, r->size + 1);
                u = (struct csEphHX*)mmVectorValue_At(r, r->size - 1);

                mmString_Assignsn(&u->XH, R[0], 1);
                mmString_Assignsn(&u->RA, &R[0][1], strlen(R[0]) - 1);
                mmString_Assigns(&u->DEC, R[1]);
                mmString_Assigns(&u->zx1, R[2]);
                mmString_Assigns(&u->zx2, R[3]);
                mmString_Assigns(&u->sc, R[4]);
                mmString_Assigns(&u->xd, R[5]);
                mmString_Assigns(&u->xm, R[6]);
                mmString_Assignsn(&u->xz, R[7], strlen(R[7]) - 1);
            }
        }
    }
}

MM_EXPORT_SXWNL
void
strHXK(
    const struct mmVectorValue* r,
    struct mmString* s)
{
    size_t i;
    struct csEphHX* HXs;
    struct csEphHX* u;

    mmString_Reset(s);

    HXs = (struct csEphHX*)r->arrays;
    for (i = 0; i < r->size; i++)
    {
        u = &HXs[i];

        mmString_Append(s, &u->XH);
        mmString_Append(s, &u->RA); mmString_Appends(s, ",");
        mmString_Append(s, &u->DEC); mmString_Appends(s, ",");
        mmString_Append(s, &u->zx1); mmString_Appends(s, ",");
        mmString_Append(s, &u->zx2); mmString_Appends(s, ",");
        mmString_Append(s, &u->sc); mmString_Appends(s, ",");
        mmString_Append(s, &u->xd); mmString_Appends(s, ",");
        mmString_Append(s, &u->xm); mmString_Appends(s, ",");
        mmString_Append(s, &u->xz); mmString_Appends(s, "\n");
    }
}

MM_EXPORT_SXWNL
void
csEphHXData_Init(
    struct csEphHXData* p)
{
    p->cj = 0;
    p->cw = 0;
    p->cjzx = 0;
    p->cwzx = 0;
    p->sc = 0;
    p->xd = 0;
    mmString_Init(&p->xz);
    mmString_Init(&p->gp);
    mmString_Init(&p->sxd);
}

MM_EXPORT_SXWNL
void
csEphHXData_Destroy(
    struct csEphHXData* p)
{
    mmString_Destroy(&p->sxd);
    mmString_Destroy(&p->gp);
    mmString_Destroy(&p->xz);
    p->xd = 0;
    p->sc = 0;
    p->cwzx = 0;
    p->cjzx = 0;
    p->cw = 0;
    p->cj = 0;
}

MM_EXPORT_SXWNL
void
csEphHXDatas_Init(
    struct mmVectorValue* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(p);

    hValueAllocator.Produce = &csEphHXData_Init;
    hValueAllocator.Recycle = &csEphHXData_Destroy;
    mmVectorValue_SetAllocator(p, &hValueAllocator);
    mmVectorValue_SetElement(p, sizeof(struct csEphHXData));
}

MM_EXPORT_SXWNL
void
csEphHXDatas_Destroy(
    struct mmVectorValue* p)
{
    mmVectorValue_Destroy(p);
}

//提取并格式化恒星库(把度分秒、角分秒转为弧度),all=1表示全部取出
MM_EXPORT_SXWNL
void
getHXK(
    const struct mmVectorValue* s,
    int all,
    struct mmVectorValue* r)
{
    size_t i, k;

    double value;

    struct csEphHX* u;
    struct csEphHXData* d;

    mmVectorValue_Clear(r);
    mmVectorValue_AllocatorMemory(r, s->size);

    k = 0;
    for (i = 0; i < s->size; i++)
    {
        u = (struct csEphHX*)mmVectorValue_At(s, i);
        d = (struct csEphHXData*)mmVectorValue_At(r, k);
        if (mmString_Size(&u->RA) < 5) continue;
        if (mmString_CompareCStr(&u->XH, "*") != 0 && !all) continue;

        // 0 赤经
        d->cj = str2rad(mmString_CStr(&u->RA), 1);

        // 1 赤纬
        d->cw = str2rad(mmString_CStr(&u->DEC), 0);

        // 2 赤经世纪自行
        mmValue_AToDouble(mmString_CStr(&u->zx1), &value); 
        d->cjzx = value / cs_rad * 15;       
        
        // 3 赤纬纪纪自行
        mmValue_AToDouble(mmString_CStr(&u->zx2), &value);
        d->cwzx = value / cs_rad;

        // 4 视差
        mmValue_AToDouble(mmString_CStr(&u->sc), &value);
        d->sc = value / cs_rad;

        // 5 星等
        mmValue_AToDouble(mmString_CStr(&u->xd), &value);
        d->xd = value;
        mmString_Assign(&d->sxd, &u->xd);

        // 6 星座等信息
        mmString_Assign(&d->xz, &u->xm);

        // 7 星座光谱
        mmString_Assign(&d->gp, &u->xz);

        k++;
    }
}

//多颗同时计算,t儒略世纪TD,只算章动周期在于Q天(为0不限制)
MM_EXPORT_SXWNL
void
hxCalc(
    double t,
    const struct mmVectorValue* F,
    int Q,
    int lx,
    double L,
    double fa,
    struct mmString* s)
{
    static const char* lxn[] = 
    {
        "视赤经 视赤纬",
        "站心坐标",
        "平赤经 平赤纬",
    };

    int i;
    const char* s0;
    double d[2];
    double z[3];
    double v[3], p[3], a[3];
    double gstP, gst;
    double E;
    char str[32] = { 0 };
    struct csEphHXData* e;
    
    // 消除可能的未初始化警告
    E = 0; gst = 0;

    assert(0 <= lx && lx < csArraySize(lxn) && "lx is invalid.");
    s0 = lxn[lx];

    mmString_Reset(s);

    csJD_JD2str(t * 36525 + cs_J2000, str);
    mmString_Appends(s, str);
    mmString_Appends(s, " TD ");
    mmString_Appends(s, s0);
    mmString_Appends(s, "\n");

    if (lx == 0 || lx == 1) {
        nutation(d, t, Q); //章动
        E = hcjj(t);  //黄赤交角
        evSSB(v, t); //地球SSB直角速度(光行差使用的)
        epSSB(p, t); //地球SSB直角位置(视差修正使用的)
        sun2000(a, t, 20); //太阳J2000球面坐标(引力偏转用的)
        llrConv(a, a, 84381.406 / cs_rad); //太阳赤道坐标
        gstP = pGST2(t * 36525); //平恒星时
        gst = gstP + d[0] * cos(E); //真恒星时
    }
    for (i = 0; i < F->size; i ++) {
        e = (struct csEphHXData*)mmVectorValue_At(F, i);

        mmString_Append(s, &e->xz);
        mmString_Appends(s, " ");
        mmString_Append(s, &e->gp);
        mmString_Appends(s, " ");
        mmString_Append(s, &e->sxd);
        mmString_Appends(s, " ");

        z[0] = e->cj + e->cjzx * t * 100; //J2000赤经含自行
        z[1] = e->cw + e->cwzx * t * 100; //J2000赤纬含自行
        z[2] = 1.0 / e->sc;
        z[0] = rad2mrad(z[0]);
        if (!z[2]) z[2] = 1e11;

        if (lx == 0 || lx == 1) {
            ylpz(z, z, a);    //引力偏转修正
            scGxc(z, z, p, 1); //周年视差修正
            scGxc(z, z, v, 0); //光行差修正
            CDllr_J2D(z, t, z, csPMX_P03); //转到当日赤道(岁差修正)
            CDnutation(z, z, E, d[0], d[1]); //章动修正
            if (lx == 1) { //站心坐标
                // double sj = rad2rrad(gst + L - z[0]); //得到此刻天体时角
                z[0] += cs_pi / 2 - gst - L;       //转到相对于地平赤道分点的赤道坐标
                llrConv(z, z, cs_pi / 2 - fa);    //恒星地平坐标
                z[0] = rad2mrad(cs_pi / 2 - z[0]); //方位角,高度角
                if (z[1] > 0) z[1] += MQC(z[1]);      //大气折射修正
            }
        }
        if (lx == 2) {
            CDllr_J2D(z, t, z, csPMX_P03); //转到当日赤道(岁差修正)
        }
        if (lx == 0 || lx == 2)
        {
            //视位置或平位置
            rad2strE(str, z[0], 1, 3); mmString_Appends(s, str);
            mmString_Appends(s, " ");
            rad2strE(str, z[1], 0, 2); mmString_Appends(s, str);
            mmString_Appends(s, "\n");
        }
        else
        {
            rad2strE(str, z[0], 0, 2); mmString_Appends(s, str);
            mmString_Appends(s, " ");
            rad2strE(str, z[1], 0, 2); mmString_Appends(s, str);
            mmString_Appends(s, "\n");
        }
    }

    mmString_Appends(s, "\n");
}
