/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSxwnlEphB_h__
#define __mmSxwnlEphB_h__

#include "core/mmPlatform.h"
#include "core/mmString.h"

#include "container/mmVectorValue.h"

#include "sxwnl/mmSxwnlExport.h"

#include "core/mmPrefix.h"

//地球的SSB速度计算表
MM_EXPORT_SXWNL extern const int evTab[];

//地球的SSB速度, t儒略世纪, 速度单位AU/世纪, 平均误差4*10^-8AU/日
MM_EXPORT_SXWNL
void 
evSSB(
    double v[3], 
    double t);

MM_EXPORT_SXWNL extern const double epTab[];

//地心SSB坐标,t儒略世纪数,4位有效数字
MM_EXPORT_SXWNL
void 
epSSB(
    double v[3], 
    double t);

//引力偏转,z天体赤道坐标,a太阳赤道坐标
MM_EXPORT_SXWNL
void 
ylpz(
    double r[3], 
    const double z[3], 
    const double a[3]);

//=================================周年视差或光行差=================================
//==================================================================================
//严格的恒星视差或光行差改正
MM_EXPORT_SXWNL
void 
scGxc(
    double r[3], 
    const double z[3], 
    const double v[3], 
    int f);

//=================================太阳J2000球面坐标================================
//==================================================================================
MM_EXPORT_SXWNL
void 
sun2000(
    double a[3], 
    double t, 
    int n);

//88星座表。结构：汉语+缩写,面积(平方度),赤经 (时分),赤纬(度分),象限角 族 星座英文名
//"仙女座And", " 722.278", " 0 48.46", " 37 25.91", "NQ1 英仙 Andromeda",    //01
MM_EXPORT_SXWNL extern const char* xz88[];
MM_EXPORT_SXWNL extern const int xz88Length;

//恒星库
// RA(时分秒)   DEC(度分秒)   自行1  自行2  视差  星等  星名  星座#
//"* 0 01 57.620", "- 6 00 50.68", " 0.0031", " -0.041", " 0.008", " 4.37 ", "星1630 ", "Psc 30 M3#",
MM_EXPORT_SXWNL extern const char** HXK[];
MM_EXPORT_SXWNL extern const int HXKLength[];
MM_EXPORT_SXWNL extern const int HXKLLength;

struct csEphHX
{
    struct mmString XH; // * 星号标志
    struct mmString RA; // 0 RA(时分秒)
    struct mmString DEC;// 1 DEC(度分秒)
    struct mmString zx1;// 2 自行1
    struct mmString zx2;// 3 自行2
    struct mmString sc; // 4 视差
    struct mmString xd; // 5 星等
    struct mmString xm; // 6 星名
    struct mmString xz; // 7 星座
};

MM_EXPORT_SXWNL
void
csEphHX_Init(
    struct csEphHX* p);

MM_EXPORT_SXWNL
void
csEphHX_Destroy(
    struct csEphHX* p);

MM_EXPORT_SXWNL
void
csEphHXs_Init(
    struct mmVectorValue* p);

MM_EXPORT_SXWNL
void
csEphHXs_Destroy(
    struct mmVectorValue* p);

//整个星库引用
MM_EXPORT_SXWNL
void 
idxHXK(
    int i, 
    struct mmVectorValue* r);

//星库检索
MM_EXPORT_SXWNL
void 
schHXK(
    const char* key, 
    struct mmVectorValue* r);

//星库字符串表
MM_EXPORT_SXWNL
void 
strHXK(
    const struct mmVectorValue* r, 
    struct mmString* s);

struct csEphHXData
{
    double cj;          // 0 赤经
    double cw;          // 1 赤纬
    double cjzx;        // 2 赤经世纪自行
    double cwzx;        // 3 赤纬纪纪自行
    double sc;          // 4 视差
    double xd;          // 5 星等
    struct mmString xz; // 6 星座等信息
    struct mmString gp; // 7 星座光谱

    struct mmString sxd;// 5 星等 原始字符串
};

MM_EXPORT_SXWNL
void
csEphHXData_Init(
    struct csEphHXData* p);

MM_EXPORT_SXWNL
void
csEphHXData_Destroy(
    struct csEphHXData* p);

MM_EXPORT_SXWNL
void
csEphHXDatas_Init(
    struct mmVectorValue* p);

MM_EXPORT_SXWNL
void
csEphHXDatas_Destroy(
    struct mmVectorValue* p);

//提取并格式化恒星库(把度分秒、角分秒转为弧度),all=1表示全部取出
MM_EXPORT_SXWNL
void 
getHXK(
    const struct mmVectorValue* s, 
    int all, 
    struct mmVectorValue* r);

//多颗同时计算,t儒略世纪TD,只算章动周期在于Q天(为0不限制)
MM_EXPORT_SXWNL
void 
hxCalc(
    double t, 
    const struct mmVectorValue* F, 
    int Q, 
    int lx, 
    double L, 
    double fa, 
    struct mmString* s);

#include "core/mmSuffix.h"

#endif//__mmSxwnlEphB_h__
