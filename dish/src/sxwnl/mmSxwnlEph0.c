/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlEph0.h"
#include "mmSxwnlConst.h"
#include "mmSxwnlEphData.h"

#include "core/mmAlloc.h"
#include "core/mmStringUtils.h"
#include "core/mmValueTransform.h"

#include <stdio.h>
#include <string.h>

//保留m位小数
MM_EXPORT_SXWNL
void
toFixed(
    char str[32],
    double v,
    int m)
{
    char fmt[16] = { 0 };
    sprintf(fmt, "%%.%df", m);
    sprintf(str, fmt, v);
};

//=================================角度格式化=======================================
//==================================================================================
MM_EXPORT_SXWNL const struct csFormatWord csFormatWords[] =
{
    { "°", "\'", "\"" },
    { "度", "分", "秒" },
    {  "h",  "m",  "s" },
};

//将弧度转为字串,ext为小数保留位数
MM_EXPORT_SXWNL
void
rad2strE(
    char str[32],
    double d,
    int tim,
    int ext)
{
    //tim=0输出格式示例: -23°59' 48.23"
    //tim=1输出格式示例:  18h 29m 44.52s
    const char* s = " ";
    const char* w1;
    const char* w2;
    const char* w3;
    if (d < 0) { d = -d; s = "-"; }
    if (tim) { d *=  12 / cs_pi; w1 = "h"; w2 =  "m"; w3 =  "s"; }
    else     { d *= 180 / cs_pi; w1 = "°"; w2 = "\'"; w3 = "\""; }
    int a = (int)floor(d); d = (d - a) * 60;
    int b = (int)floor(d); d = (d - b) * 60;
    int c = (int)floor(d);

    double Q = pow(10, ext);

    d = floor((d - c)*Q + 0.5);
    if (d >=  Q) { d -=  Q; c++; }
    if (c >= 60) { c -= 60; b++; }
    if (b >= 60) { b -= 60; a++; }

    char A[16] = { 0 };
    char B[16] = { 0 };
    char C[16] = { 0 };
    char D[16] = { 0 };
    sprintf(A, "   %d", a);
    sprintf(B, "0%d", b);
    sprintf(C, "0%d", c);
    sprintf(D, "00000%d", (int)d);

    if (ext)
    {
        //            - 57°17' 44.81"
        sprintf(str, "%s%s%s%s%s%s.%s%s",
            s,
            A + strlen(A) - 3, w1,
            B + strlen(B) - 2, w2,
            C + strlen(C) - 2,
            D + strlen(D) - ext,
            w3);
    }
    else
    {
        //            - 57°17' 44"
        sprintf(str, "%s%s%s%s%s%s%s",
            s,
            A + strlen(A) - 3, w1,
            B + strlen(B) - 2, w2,
            C + strlen(C) - 2, w3);
    }
}

//将弧度转为字串,保留2位
MM_EXPORT_SXWNL
void
rad2str(
    char str[32],
    double d,
    int tim)
{
    rad2strE(str, d, tim, 2); 
} 

//将弧度转为字串,精确到分
MM_EXPORT_SXWNL
void
rad2str2(
    char str[32],
    double d)
{
    //输出格式示例: -23°59'
    const char* s = "+";
    const char* w1 = "°";
    const char* w2 = "'";
    if (d < 0) { d = -d; s = "-"; }
    d *= 180 / cs_pi;
    int a = (int)floor(d);
    int b = (int)floor((d - a) * 60 + 0.5);
    if (b >= 60) { b -= 60; a++; }
    char A[16] = { 0 };
    char B[16] = { 0 };
    sprintf(A, "   %d", a);
    sprintf(B, "0%d", b);

    //            - 57°17' 
    sprintf(str, "%s%s%s%s%s",
        s,
        A + strlen(A) - 3, w1,
        B + strlen(B) - 2, w2);
}

//秒转为分秒,fx为小数点位数,fs为1转为"分秒"格式否则转为"角度分秒"格式
MM_EXPORT_SXWNL
void
m2fm(
    char str[32],
    double v,
    int fx,
    int fs)
{
    int f;
    double m;
    char str1[32];
    const struct csFormatWord* w;
    const char* gn;
    if (v < 0) { v = -v; gn = "-"; } 
    else       {         gn =  ""; }
    f = (int)floor(v / 60);
    m = v - f * 60;

    assert(0 <= fs && fs < csArraySize(csFormatWords) && "fs is invalid.");
    w = &csFormatWords[fs];

    toFixed(str1, m, fx);
    sprintf(str, "%s%d%s%s%s", gn, f, w->w2, str1, w->w3);
}

//秒转为分秒,fn为分对齐长度,fx为小数点位数,fs格式, 该函数会补空格用以对齐
MM_EXPORT_SXWNL
void
m2fmv2(
    char str[32],
    double v,
    int fn,
    int fx,
    int fs)
{
    int f;
    double m;
    char str0[32];
    char str1[32];
    char str2[32];
    const struct csFormatWord* w;
    const char* gn;
    size_t ssz;
    if (v < 0) { v = -v; gn = "-"; }
    else       {         gn = " "; }
    f = (int)floor(v / 60);
    m = v - f * 60;

    assert(0 <= fs && fs < csArraySize(csFormatWords) && "fs is invalid.");
    w = &csFormatWords[fs];

    mmValue_SIntToA(&f, str2);
    ssz = strlen(str2);
    mmCStrRAlignAppends(str0, str2, ssz, fn);
    toFixed(str2, m, fx);
    ssz = strlen(str2);
    mmCStrRAlignAppends(str1, str2, ssz, fx + 3);
    sprintf(str, "%s%s%s%s%s", gn, str0, w->w2, str1, w->w3);
}

//串转弧度, f=1表示输入的s为时分秒
MM_EXPORT_SXWNL
double
str2rad(
    const char* s,
    int f)
{
    int fh = 1;
    double v[3] = { 0 };
    struct mmString str;
    size_t len = strlen(s);
    size_t idx = mmCStringFindNextOf(s, '-', 0, len);
    f = f ? 15 : 1;
    if (mmStringNpos != idx) fh = -1;
    mmString_Init(&str);
    mmString_Assigns(&str, s);
    mmString_ReplaceChar(&str, '-', ' ');
    mmString_ReplaceChar(&str, 'h', ' ');
    mmString_ReplaceChar(&str, 'm', ' ');
    mmString_ReplaceChar(&str, 's', ' ');
    mmString_ReplaceCStr(&str, "°", " ");
    mmString_ReplaceChar(&str, '\'', ' ');
    mmString_ReplaceChar(&str, '\"', ' ');
    mmValueSplitStringToTypeArray(&str, v, sizeof(double), ' ', 3, &mmValue_AToDouble);
    mmString_Destroy(&str);
    return fh * (v[0] * 3600 + v[1] * 60 + v[2] * 1) / cs_rad * f;
}

//=================================数学工具=========================================
//==================================================================================
//对超过0-2PI的角度转为0-2PI
MM_EXPORT_SXWNL
double
rad2mrad(
    double v)
{
    v = fmod(v, (2 * cs_pi));
    if (v < 0) return v + 2 * cs_pi;
    return v;
}

//对超过-PI到PI的角度转为-PI到PI
MM_EXPORT_SXWNL
double
rad2rrad(
    double v)
{
    v = fmod(v, (2 * cs_pi));
    if (v <= -cs_pi) return v + 2 * cs_pi;
    if (v >   cs_pi) return v - 2 * cs_pi;
    return v;
}

//临界余数(a与最近的整倍数b相差的距离)
MM_EXPORT_SXWNL
double
mod2(
    double a,
    double b)
{
    double c = a / b;
    c -= floor(c);
    if (c > 0.5) c -= 1;
    return c * b;
}

//球面转直角坐标
MM_EXPORT_SXWNL
void
llr2xyz(
    double r[3],
    const double JW[3])
{
    double J = JW[0], W = JW[1], R = JW[2];
    r[0] = R * cos(W)*cos(J);
    r[1] = R * cos(W)*sin(J);
    r[2] = R * sin(W);
}

//直角坐标转球
MM_EXPORT_SXWNL
void
xyz2llr(
    double r[3],
    const double xyz[3])
{
    double x = xyz[0], y = xyz[1], z = xyz[2];
    r[2] = sqrt(x*x + y * y + z * z);
    r[1] = asin(z / r[2]);
    r[0] = rad2mrad(atan2(y, x));
}

//球面坐标旋转
MM_EXPORT_SXWNL
void
llrConv(
    double r[3],
    const double JW[3],
    double E)
{
    //黄道赤道坐标变换,赤到黄E取负
    double J = JW[0], W = JW[1];
    r[0] = atan2(sin(J)*cos(E) - tan(W)*sin(E), cos(J));
    r[1] = asin(cos(E)*sin(W) + sin(E)*cos(W)*sin(J));
    r[2] = JW[2];
    r[0] = rad2mrad(r[0]);
}

//赤道坐标转为地平坐标
MM_EXPORT_SXWNL
void
CD2DP(double a[3],
    const double z[3],
    double L,
    double fa,
    double gst)
{
    double JW[3] = { z[0] + cs_pi / 2 - gst - L, z[1], z[2] };
    //转到相对于地平赤道分点的赤道坐标
    llrConv(a, JW, cs_pi / 2 - fa);
    a[0] = rad2mrad(cs_pi / 2 - a[0]);
}

//求角度差
MM_EXPORT_SXWNL
double
j1_j2(
    double J1,
    double W1,
    double J2,
    double W2)
{
    double dJ = rad2rrad(J1 - J2), dW = W1 - W2;
    if (fabs(dJ) < 1 / 1000.0 && fabs(dW) < 1 / 1000.0) {
        dJ *= cos((W1 + W2) / 2);
        return sqrt(dJ*dJ + dW * dW);
    }
    return acos(sin(W1)*sin(W2) + cos(W1)*cos(W2)*cos(dJ));
}

//日心球面转地心球面,Z星体球面坐标,A地球球面坐标
MM_EXPORT_SXWNL
void
h2g(
    double r[3],
    const double z[3],
    const double a[3])
{
    //本含数是通用的球面坐标中心平移函数,行星计算中将反复使用
    double ra[3];
    double rz[3];
    llr2xyz(ra, a); //地球
    llr2xyz(rz, z); //星体
    rz[0] -= ra[0]; rz[1] -= ra[1]; rz[2] -= ra[2];
    xyz2llr(r, rz);
}

//视差角(不是视差)
MM_EXPORT_SXWNL
double
shiChaJ(
    double gst,
    double L,
    double fa,
    double J,
    double W)
{
    double H = gst + L - J; //天体的时角
    return rad2mrad(atan2(sin(H), tan(fa)*cos(W) - sin(W)*cos(H)));
}

// TD - UT1 计算表
MM_EXPORT_SXWNL const double dt_at[] =
{ 
-4000,108371.7,-13036.80,392.000, 0.0000,
 -500, 17201.0,  -627.82, 16.170,-0.3413,
 -150, 12200.6,  -346.41,  5.403,-0.1593,
  150,  9113.8,  -328.13, -1.647, 0.0377,
  500,  5707.5,  -391.41,  0.915, 0.3145,
  900,  2203.4,  -283.45, 13.034,-0.1778,
 1300,   490.1,   -57.35,  2.085,-0.0072,
 1600,   120.0,    -9.81, -1.532, 0.1403,
 1700,    10.2,    -0.91,  0.510,-0.0370,
 1800,    13.4,    -0.72,  0.202,-0.0193,
 1830,     7.8,    -1.81,  0.416,-0.0247,
 1860,     8.3,    -0.13, -0.406, 0.0292,
 1880,    -5.4,     0.32, -0.183, 0.0173,
 1900,    -2.3,     2.06,  0.169,-0.0135,
 1920,    21.2,     1.69, -0.304, 0.0167,
 1940,    24.2,     1.22, -0.064, 0.0031,
 1960,    33.2,     0.51,  0.231,-0.0109,
 1980,    51.0,     1.29, -0.026, 0.0032,
 2000,    63.87,    0.1,   0,     0,
 2005,    64.7,     0.21,  0,     0,
 2012,    66.8,     0.22,  0,     0,
 2018,    69.0,     0.36,  0,     0,
 2028,    72.6
};

//二次曲线外推
MM_EXPORT_SXWNL
double
dt_ext(
    double y,
    double jsd)
{
    double dy = (y - 1820) / 100; return -20 + jsd * dy*dy; 
} 

//计算世界时与原子时之差,传入年
MM_EXPORT_SXWNL
double
dt_calc(
    double y)
{
    int length = csArraySize(dt_at);
    double y0 = dt_at[length - 2]; //表中最后一年
    double t0 = dt_at[length - 1]; //表中最后一年的deltatT
    if (y >= y0) {
        double jsd = 31; //sjd是y1年之后的加速度估计。瑞士星历表jsd=31,NASA网站jsd=32,skmap的jsd=29
        if (y > y0 + 100) return dt_ext(y, jsd);
        double  v = dt_ext( y, jsd);       //二次曲线外推
        double dv = dt_ext(y0, jsd) - t0;  //ye年的二次外推与te的差
        return v - dv * (y0 + 100 - y) / 100;
    }
    int i;
    const double* d = dt_at;
    for (i = 0; i < length; i += 5) if (y < d[i + 5]) break;
    double t1 = (y - d[i]) / (d[i + 5] - d[i]) * 10, t2 = t1 * t1, t3 = t2 * t1;
    return d[i + 1] + d[i + 2] * t1 + d[i + 3] * t2 + d[i + 4] * t3;
}

//传入儒略日(J2000起算),计算TD-UT(单位:日)
MM_EXPORT_SXWNL
double
dt_T(
    double t)
{
    return dt_calc(t / 365.2425 + 2000) / 86400.0; 
}  

//=================================岁差计算=========================================
//==================================================================================
// IAU1976岁差表
MM_EXPORT_SXWNL const double preceTab_IAU1976[] =
{ 
     0,    5038.7784,   -1.07259, -0.001147, //fi
 84381.448,   0,        +0.05127, -0.007726, //w
     0,     +4.1976,    +0.19447, -0.000179, //P
     0,    -46.8150,    +0.05059, +0.000344, //Q
 84381.448,-46.8150,    -0.00059, +0.001813, //E
     0,    +10.5526,    -2.38064, -0.001125, //x
     0,      47.0028,   -0.03301, +0.000057, //pi(自导出),根据P=sin(pi)*sin(II)
629554.886,-869.8192,   +0.03666, -0.001504, //II(自导出),根据Q=sin(pi)*cos(II)
     0,     5029.0966,  +1.11113, +0.000006, //p
     0,     2004.3109,  -0.42665, -0.041833, //th
     0,     2306.2181,  +0.30188, +0.017998, //Z
     0,     2306.2181,  +1.09468, +0.018203  //z	
};

// IAU2000岁差表
MM_EXPORT_SXWNL const double preceTab_IAU2000[] =
{ 
     0,    5038.478750, -1.07259, -0.001147,0,0, //fi
 84381.448,  -0.025240, +0.05127, -0.007726,0,0, //w,
     0,      +4.1976,   +0.19447, -0.000179,0,0, //P,
     0,     -46.8150,   +0.05059, +0.000344,0,0, //Q,
 84381.448, -46.84024,  -0.00059, +0.001813,0,0, //E,
     0,     +10.5526,   -2.38064, -0.001125,0,0, //x
      0,      47.0028,  -0.03301, +0.000057,0,0, //pi(自导出)
629554.886,-869.8192,  +0.03666,  -0.001504,0,0, //II(自导出)
     0,    5028.79695, +1.11113,  +0.000006,0,0, //p
     0,        2004.1917476, -0.4269353, -0.0418251, -0.0000601, -0.0000001, //th
    +2.5976176,2306.0809506, +0.3019015, +0.0179663, -0.0000327, -0.0000002, //Z
    -2.5976176,2306.0803226, +1.0947790, +0.0182273, +0.0000470, -0.0000003  //z
};

MM_EXPORT_SXWNL const double preceTab_P03[] =
{
     0,        5038.481507, -1.0790069, -0.00114045, +0.000132851, -9.51e-8,  //fi
 84381.406000,   -0.025754, +0.0512623, -0.00772503, -4.67e-7,     +3.337e-7, //w
     0,           4.199094, +0.1939873, -0.00022466, -9.12e-7,     +1.20e-8,  //P
     0,         -46.811015, +0.0510283, +0.00052413, -6.46e-7,     -1.72e-8,  //Q
 84381.406000,  -46.836769, -0.0001831, +0.00200340, -5.76e-7,     -4.34e-8,  //E
     0,          10.556403, -2.3814292, -0.00121197, +0.000170663, -5.60e-8,  //x
     0,          46.998973, -0.0334926, -0.00012559, +1.13e-7,     -2.2e-9,   //pi
629546.7936,   -867.95758,  +0.157992,  -0.0005371,  -0.00004797,  +7.2e-8,   //II
     0,        5028.796195, +1.1054348, +0.00007964, -0.000023857, +3.83e-8,  //p
     0,        2004.191903, -0.4294934, -0.04182264, -7.089e-6,    -1.274e-7, //th
     2.650545, 2306.083227, +0.2988499, +0.01801828, -5.971e-6,    -3.173e-7, //Z
    -2.650545, 2306.077181, +1.0927348, +0.01826837, -0.000028596, -2.904e-7  //z
};

//t是儒略世纪数,sc是岁差量名称,mx是模型
MM_EXPORT_SXWNL
double
prece(
    double t,
    enum csPreceSC sc,
    enum csPreceMX mx)
{
    int i, n;
    const double* p;
    double tn = 1, c = 0;
    switch (mx)
    {
    case csPMX_IAU1976:
        n = 4; p = preceTab_IAU1976;
        break;
    case csPMX_IAU2000:
        n = 6; p = preceTab_IAU2000;
        break;
    case csPMX_P03:
        n = 6; p = preceTab_P03;
        break;
    default:
        n = 0; p = NULL;
        break;
    }
    for (i = 0; i < n; i++, tn *= t) c += p[sc*n + i] * tn;
    return c / cs_rad;
}

//返回P03黄赤交角,t是世纪数
MM_EXPORT_SXWNL
double
hcjj(
    double t)
{
    double t2 = t * t, t3 = t2 * t, t4 = t3 * t, t5 = t4 * t;
    return (84381.4060 - 46.836769*t - 0.0001831*t2 + 0.00200340*t3 - 5.76e-7*t4 - 4.34e-8*t5) / cs_rad;
}

//=================================岁差旋转=========================================
//==================================================================================
//J2000赤道转Date赤道
MM_EXPORT_SXWNL
void
CDllr_J2D(
    double r[3],
    double t,
    const double llr[3],
    enum csPreceMX mx)
{
    double  Z = prece(t, csPSC_Z , mx) + llr[0];
    double  z = prece(t, csPSC_z , mx);
    double th = prece(t, csPSC_th, mx);
    double cosW = cos(llr[1]), cosH = cos(th);
    double sinW = sin(llr[1]), sinH = sin(th);
    double A = cosW * sin(Z);
    double B = cosH * cosW*cos(Z) - sinH * sinW;
    double C = sinH * cosW*cos(Z) + cosH * sinW;
    r[0] = rad2mrad(atan2(A, B) + z);
    r[1] = asin(C);
    r[2] = llr[2];
}

//Date赤道转J2000赤道
MM_EXPORT_SXWNL
void
CDllr_D2J(
    double r[3],
    double t,
    const double llr[3],
    enum csPreceMX mx)
{
    double Z  = -prece(t, csPSC_z , mx) + llr[0];
    double z  = -prece(t, csPSC_Z , mx);
    double th = -prece(t, csPSC_th, mx);
    double cosW = cos(llr[1]), cosH = cos(th);
    double sinW = sin(llr[1]), sinH = sin(th);
    double A = cosW * sin(Z);
    double B = cosH * cosW*cos(Z) - sinH * sinW;
    double C = sinH * cosW*cos(Z) + cosH * sinW;
    r[0] = rad2mrad(atan2(A, B) + z);
    r[1] = asin(C);
    r[2] = llr[2];
}

//黄道球面坐标_J2000转Date分点,t为儒略世纪数
MM_EXPORT_SXWNL
void
HDllr_J2D(
    double r[3],
    double t,
    const double llr[3],
    enum csPreceMX mx)
{
    //J2000黄道旋转到Date黄道(球面对球面),也可直接由利用球面旋转函数计算,但交角接近为0时精度很低
    r[0] = llr[0]; r[1] = llr[1]; r[2] = llr[2];
    r[0] += prece(t, csPSC_fi, mx); llrConv(r, r,  prece(t, csPSC_w, mx));
    r[0] -= prece(t, csPSC_x , mx); llrConv(r, r, -prece(t, csPSC_E, mx));
}

//黄道球面坐标_Date分点转J2000,t为儒略世纪数
MM_EXPORT_SXWNL
void
HDllr_D2J(
    double r[3],
    double t,
    const double llr[3],
    enum csPreceMX mx)
{
    r[0] = llr[0]; r[1] = llr[1]; r[2] = llr[2];
    llrConv(r, r,  prece(t, csPSC_E, mx));  r[0] += prece(t, csPSC_x , mx);
    llrConv(r, r, -prece(t, csPSC_w, mx));  r[0] -= prece(t, csPSC_fi, mx);
    r[0] = rad2mrad(r[0]);
}

//=================================章动计算=========================================
//==================================================================================
// IAU2000B章动序列
MM_EXPORT_SXWNL const int nuTab[] =
{ 
//l  l' F  D  Om         A        A'      A"        B      B'     B"
  0, 0, 0, 0, 1, -172064161, -174666,  33386, 92052331,  9086, 15377,
  0, 0, 2,-2, 2,  -13170906,   -1675, -13696,  5730336, -3015, -4587,
  0, 0, 2, 0, 2,   -2276413,    -234,   2796,   978459,  -485,  1374,
  0, 0, 0, 0, 2,    2074554,     207,   -698,  -897492,   470,  -291,
  0, 1, 0, 0, 0,    1475877,   -3633,  11817,    73871,  -184, -1924,
  0, 1, 2,-2, 2,    -516821,    1226,   -524,   224386,  -677,  -174,
  1, 0, 0, 0, 0,     711159,      73,   -872,    -6750,     0,   358,
  0, 0, 2, 0, 1,    -387298,    -367,    380,   200728,    18,   318,
  1, 0, 2, 0, 2,    -301461,     -36,    816,   129025,   -63,   367,
  0,-1, 2,-2, 2,     215829,    -494,    111,   -95929,   299,   132,

  0, 0, 2,-2, 1,     128227,     137,    181,   -68982,    -9,    39,
 -1, 0, 2, 0, 2,     123457,      11,     19,   -53311,    32,    -4,
 -1, 0, 0, 2, 0,     156994,      10,   -168,    -1235,     0,    82,
  1, 0, 0, 0, 1,      63110,      63,     27,   -33228,     0,    -9,
 -1, 0, 0, 0, 1,     -57976,     -63,   -189,    31429,     0,   -75,
 -1, 0, 2, 2, 2,     -59641,     -11,    149,    25543,   -11,    66,
  1, 0, 2, 0, 1,     -51613,     -42,    129,    26366,     0,    78,
 -2, 0, 2, 0, 1,      45893,      50,     31,   -24236,   -10,    20,
  0, 0, 0, 2, 0,      63384,      11,   -150,    -1220,     0,    29,
  0, 0, 2, 2, 2,     -38571,      -1,    158,    16452,   -11,    68,

  0,-2, 2,-2, 2,      32481,       0,      0,   -13870,     0,     0,
 -2, 0, 0, 2, 0,     -47722,       0,    -18,      477,     0,   -25,
  2, 0, 2, 0, 2,     -31046,      -1,    131,    13238,   -11,    59,
  1, 0, 2,-2, 2,      28593,       0,     -1,   -12338,    10,    -3,
 -1, 0, 2, 0, 1,      20441,      21,     10,   -10758,     0,    -3,
  2, 0, 0, 0, 0,      29243,       0,    -74,     -609,     0,    13,
  0, 0, 2, 0, 0,      25887,       0,    -66,     -550,     0,    11,
  0, 1, 0, 0, 1,     -14053,     -25,     79,     8551,    -2,   -45,
 -1, 0, 0, 2, 1,      15164,      10,     11,    -8001,     0,    -1,
  0, 2, 2,-2, 2,     -15794,      72,    -16,     6850,   -42,    -5,

  0, 0,-2, 2, 0,      21783,       0,     13,     -167,     0,    13,
  1, 0, 0,-2, 1,     -12873,     -10,    -37,     6953,     0,   -14,
  0,-1, 0, 0, 1,     -12654,      11,     63,     6415,     0,    26,
 -1, 0, 2, 2, 1,     -10204,       0,     25,     5222,     0,    15,
  0, 2, 0, 0, 0,      16707,     -85,    -10,      168,    -1,    10,
  1, 0, 2, 2, 2,      -7691,       0,     44,     3268,     0,    19,
 -2, 0, 2, 0, 0,     -11024,       0,    -14,      104,     0,     2,
  0, 1, 2, 0, 2,       7566,     -21,    -11,    -3250,     0,    -5,
  0, 0, 2, 2, 1,      -6637,     -11,     25,     3353,     0,    14,
  0,-1, 2, 0, 2,      -7141,      21,      8,     3070,     0,     4,

  0, 0, 0, 2, 1,      -6302,     -11,      2,     3272,     0,     4,
  1, 0, 2,-2, 1,       5800,      10,      2,    -3045,     0,    -1,
  2, 0, 2,-2, 2,       6443,       0,     -7,    -2768,     0,    -4,
 -2, 0, 0, 2, 1,      -5774,     -11,    -15,     3041,     0,    -5,
  2, 0, 2, 0, 1,      -5350,       0,     21,     2695,     0,    12,
  0,-1, 2,-2, 1,      -4752,     -11,     -3,     2719,     0,    -3,
  0, 0, 0,-2, 1,      -4940,     -11,    -21,     2720,     0,    -9,
 -1,-1, 0, 2, 0,       7350,       0,     -8,      -51,     0,     4,
  2, 0, 0,-2, 1,       4065,       0,      6,    -2206,     0,     1,
  1, 0, 0, 2, 0,       6579,       0,    -24,     -199,     0,     2,

  0, 1, 2,-2, 1,       3579,       0,      5,    -1900,     0,     1,
  1,-1, 0, 0, 0,       4725,       0,     -6,      -41,     0,     3,
 -2, 0, 2, 0, 2,      -3075,       0,     -2,     1313,     0,    -1,
  3, 0, 2, 0, 2,      -2904,       0,     15,     1233,     0,     7,
  0,-1, 0, 2, 0,       4348,       0,    -10,      -81,     0,     2,
  1,-1, 2, 0, 2,      -2878,       0,      8,     1232,     0,     4,
  0, 0, 0, 1, 0,      -4230,       0,      5,      -20,     0,    -2,
 -1,-1, 2, 2, 2,      -2819,       0,      7,     1207,     0,     3,
 -1, 0, 2, 0, 0,      -4056,       0,      5,       40,     0,    -2,
  0,-1, 2, 2, 2,      -2647,       0,     11,     1129,     0,     5,

 -2, 0, 0, 0, 1,      -2294,       0,    -10,     1266,     0,    -4,
  1, 1, 2, 0, 2,       2481,       0,     -7,    -1062,     0,    -3,
  2, 0, 0, 0, 1,       2179,       0,     -2,    -1129,     0,    -2,
 -1, 1, 0, 1, 0,       3276,       0,      1,       -9,     0,     0,
  1, 1, 0, 0, 0,      -3389,       0,      5,       35,     0,    -2,
  1, 0, 2, 0, 0,       3339,       0,    -13,     -107,     0,     1,
 -1, 0, 2,-2, 1,      -1987,       0,     -6,     1073,     0,    -2,
  1, 0, 0, 0, 2,      -1981,       0,      0,      854,     0,     0,
 -1, 0, 0, 1, 0,       4026,       0,   -353,     -553,     0,  -139,
  0, 0, 2, 1, 2,       1660,       0,     -5,     -710,     0,    -2,

 -1, 0, 2, 4, 2,      -1521,       0,      9,      647,     0,     4,
 -1, 1, 0, 1, 1,       1314,       0,      0,     -700,     0,     0,
  0,-2, 2,-2, 1,      -1283,       0,      0,      672,     0,     0,
  1, 0, 2, 2, 1,      -1331,       0,      8,      663,     0,     4,
 -2, 0, 2, 2, 2,       1383,       0,     -2,     -594,     0,    -2,
 -1, 0, 0, 0, 2,       1405,       0,      4,     -610,     0,     2,
  1, 1, 2,-2, 2,       1290,       0,      0,     -556,     0,     0
};

//章动计算,t是J2000.0起算的儒略世纪数,zq表示只计算周期大于zq(天)的项
MM_EXPORT_SXWNL
void
nutation(
    double r[2],
    double t,
    int zq)
{
    double t2 = t * t, t3 = t2 * t, t4 = t3 * t; //t的二、三、四次方
    double l = 485868.249036 + 1717915923.2178*t + 31.8792*t2 + 0.051635*t3 - 0.00024470*t4;
    double l1=1287104.79305  +  129596581.0481*t -  0.5532*t2 + 0.000136*t3 - 0.00001149*t4;
    double F = 335779.526232 + 1739527262.8478*t - 12.7512*t2 - 0.001037*t3 + 0.00000417*t4;
    double D =1072260.70369  + 1602961601.2090*t -  6.3706*t2 + 0.006593*t3 - 0.00003169*t4;
    double Om= 450160.398036 -    6962890.5431*t +  7.4722*t2 + 0.007702*t3 - 0.00005939*t4;
    double dL = 0, dE = 0;
    double c, q;
    int i;
    const int* B = nuTab;
    for (i = 0; i < 77 * 11; i += 11) { //周期项取和计算
        c = (B[i] * l + B[i + 1] * l1 + B[i + 2] * F + B[i + 3] * D + B[i + 4] * Om) / cs_rad;
        if (zq) { //只算周期大于zq天的项
            q = 36526 * 2 * cs_pi*cs_rad / (1717915923.2178*B[i] + 129596581.0481*B[i + 1] + 1739527262.8478*B[i + 2] + 1602961601.2090*B[i + 3] + 6962890.5431*B[i + 4]);
            if (q < zq) continue;
        }
        dL += (B[i + 5] + B[i + 6] * t) * sin(c) + B[i +  7] * cos(c);
        dE += (B[i + 8] + B[i + 9] * t) * cos(c) + B[i + 10] * sin(c);
    }
    // 返回IAU2000B章动值, dL是黄经章动,dE是交角章动
    r[0] = dL /= 10000000 * cs_rad;
    r[1] = dE /= 10000000 * cs_rad;
}

//本函数计算赤经章动及赤纬章动
MM_EXPORT_SXWNL
void
CDnutation(
    double r[3],
    const double z[3],
    double E,
    double dL,
    double dE)
{
    r[0] = z[0]; r[1] = z[1]; r[2] = z[2];
    r[0] += (cos(E) + sin(E)*sin(z[0])*tan(z[1]))*dL - cos(z[0])*tan(z[1])*dE; //赤经章动
    r[1] += sin(E)*cos(z[0])*dL + sin(z[0])*dE;                                //赤纬章动
    r[0] = rad2mrad(r[0]);
}

// 中精度章动计算表
MM_EXPORT_SXWNL const double nutB[] =
{
  2.1824,  -33.75705, 36e-6,-1720,920,
  3.5069, 1256.66393, 11e-6,-132, 57,
  1.3375,16799.4182, -51e-6, -23, 10,
  4.3649,  -67.5141,  72e-6,  21, -9,
  0.04,   -628.302,   0,     -14,  0,
  2.36,   8328.691,   0,       7,  0,
  3.46,   1884.966,   0,      -5,  2,
  5.44,  16833.175,   0,      -4,  2,
  3.69,  25128.110,   0,      -3,  0,
  3.55,    628.362,   0,       2,  0,
};

//中精度章动计算,t是世纪数
MM_EXPORT_SXWNL
void
nutation2(
    double r[2],
    double t)
{
    int i;
    double c, a;
    double t2 = t * t, dL = 0, dE = 0;
    const double* B = nutB;
    for (i = 0; i < csArraySize(nutB); i += 5) {
        c = B[i] + B[i + 1] * t + B[i + 2] * t2;
        if (i == 0) a = -1.742*t; else a = 0;
        dL += (B[i + 3] + a)*sin(c);
        dE += B[i + 4] * cos(c);
    }
    // 黄经章动,交角章动
    r[0] = dL / 100 / cs_rad;
    r[1] = dE / 100 / cs_rad;
}

//只计算黄经章动
MM_EXPORT_SXWNL
double
nutationLon2(
    double t)
{
    int i;
    double a;
    double t2 = t * t, dL = 0;
    const double* B = nutB;
    for (i = 0; i < csArraySize(nutB); i += 5) {
        if (i == 0) a = -1.742*t; else a = 0;
        dL += (B[i + 3] + a) * sin(B[i] + B[i + 1] * t + B[i + 2] * t2);
    }
    return dL / 100 / cs_rad;
}

//=================================蒙气改正=========================================
//==================================================================================
//大气折射,h是真高度
MM_EXPORT_SXWNL
double
MQC(
    double h)
{
    return  0.0002967 / tan(h  + 0.003138 / (h  + 0.08919)); 
} 
//大气折射,ho是视高度
MM_EXPORT_SXWNL
double
MQC2(
    double ho)
{
    return -0.0002909 / tan(ho + 0.002227 / (ho + 0.07679)); 
} 

//=================================视差改正=========================================
//==================================================================================
//视差修正
MM_EXPORT_SXWNL
void
parallax(
    double s[3],
    const double z[3],
    double H,
    double fa,
    double high)
{
    //z赤道坐标,fa地理纬度,H时角,high海拔(千米)
    s[0] = z[0]; s[1] = z[1]; s[2] = z[2];
    double dw = 1; if (s[2] < 500) dw = cs_AU;
    s[2] *= dw;
    double r0, x0, y0, z0, f = cs_ba, u = atan(f*tan(fa)), g = s[0] + H;
    r0 = cs_rEar * cos(u)   + high * cos(fa); //站点与地地心向径的赤道投影长度
    z0 = cs_rEar * sin(u)*f + high * sin(fa); //站点与地地心向径的轴向投影长度
    x0 = r0 * cos(g);
    y0 = r0 * sin(g);

    llr2xyz(s, s);
    s[0] -= x0; s[1] -= y0; s[2] -= z0;
    xyz2llr(s, s);
    s[0] = s[0]; s[1] = s[1]; s[2] = s[2] / dw;
}

//xt星体,zn坐标号,t儒略世纪数,n计算项数
MM_EXPORT_SXWNL
double
XL0_calc(
    int xt,
    int zn,
    double t,
    int n)
{
    int i, j;
    double v = 0, tn = 1, c;
    const double* F = XL0[xt];
    int n1, n2, N;
    int n0, pn = zn * 6 + 1, N0 = (int)(F[pn + 1] - F[pn]); //N0序列总数
    t /= 10; //转为儒略千年数
    for (i = 0; i < 6; i++, tn *= t) {
        n1 = (int)(F[pn + i]); n2 = (int)(F[pn + 1 + i]); n0 = n2 - n1;
        if (!n0) continue;
        if (n < 0) N = n2;  //确定项数
        else { N = int2(3 * n*n0 / N0 + 0.5) + n1;  if (i) N += 3;  if (N > n2) N = n2; }
        for (j = n1, c = 0; j < N; j += 3) c += F[j] * cos(F[j + 1] + t * F[j + 2]);
        v += c * tn;
    }
    v /= F[0];
    if (xt == 0) { //地球
        double t2 = t * t, t3 = t2 * t; //千年数的各次方
        if (zn == 0) v += (-0.0728 - 2.7702*t - 1.1019*t2 - 0.0996*t3) / cs_rad;
        if (zn == 1) v += (+0.0000 + 0.0004*t + 0.0004*t2 - 0.0026*t3) / cs_rad;
        if (zn == 2) v += (-0.0020 + 0.0044*t + 0.0213*t2 - 0.0250*t3) / 1000000;
    }
    else { //其它行星
        double dv = XL0_xzb[(xt - 1) * 3 + zn];
        if (zn == 0) v += -3 * t / cs_rad;
        if (zn == 2) v += dv / 1000000;
        else         v += dv / cs_rad;
    }
    return v;
}

//返回冥王星J2000直角坐标
MM_EXPORT_SXWNL
void
pluto_coord(
    double r[3],
    double t)
{
    int i, j;
    double c0 = cs_pi / 180 / 100000;
    double x = -1 + 2 * (t * 36525 + 1825394.5) / 2185000;
    double T = t / 100000000;
    r[0] = 0; r[1] = 0; r[2] = 0;
    for (i = 0; i < 9; i++) {
        const double* ob = XL0Pluto[i], N = XL0PlutoLength[i];
        double v = 0;
        for (j = 0; j < N; j += 3) v += ob[j] * sin(ob[j + 1] * T + ob[j + 2] * c0);
        if (i % 3 == 1) v *= x;
        if (i % 3 == 2) v *= x * x;
        r[int2(i / 3)] += v / 100000000;
    }
    r[0] +=  9.922274 + 0.154154*x;
    r[1] += 10.016090 + 0.064073*x;
    r[2] += -3.947474 - 0.042746*x;
}

//xt星体,T儒略世纪数,TD
MM_EXPORT_SXWNL
void
p_coord(
    double z[3],
    int xt,
    double t,
    int n1,
    int n2,
    int n3)
{
    if (xt < 8) {
        z[0] = XL0_calc(xt, 0, t, n1);
        z[1] = XL0_calc(xt, 1, t, n2);
        z[2] = XL0_calc(xt, 2, t, n3);
    }
    if (xt == 8) { //冥王星
        pluto_coord(z, t);
        xyz2llr(z, z);
        HDllr_J2D(z, t, z, csPMX_P03);
    }
    if (xt == 9) { //太阳
        z[0] = 0; z[1] = 0; z[2] = 0;
    }
}

//返回地球坐标,t为世纪数
MM_EXPORT_SXWNL
void
e_coord(
    double re[3],
    double t,
    int n1,
    int n2,
    int n3)
{
    re[0] = XL0_calc(0, 0, t, n1);
    re[1] = XL0_calc(0, 1, t, n2);
    re[2] = XL0_calc(0, 2, t, n3);
}

//=================================月亮星历--=======================================
//==================================================================================

//计算月亮
MM_EXPORT_SXWNL
double
XL1_calc(
    int zn,
    double t,
    int n)
{
    int i, j, N;
    int FLength;
    int ob0Length = XL1MLength[zn][0];
    const double** ob = XL1[zn];
    const double* F;
    double v = 0, tn = 1, c;
    double t2 = t * t, t3 = t2 * t, t4 = t3 * t, t5 = t4 * t, tx = t - 10;
    if (zn == 0) {
        v += (3.81034409 + 8399.684730072*t - 3.319e-05*t2 + 3.11e-08*t3 - 2.033e-10*t4)*cs_rad; //月球平黄经(弧度)
        v += 5028.792262*t + 1.1124406*t2 + 0.00007699*t3 - 0.000023479*t4 - 0.0000000178*t5   ; //岁差(角秒)
        if (tx > 0) v += -0.866 + 1.43*tx + 0.054*tx*tx; //对公元3000年至公元5000年的拟合,最大误差小于10角秒
    }
    t2 /= 1e4; t3 /= 1e8; t4 /= 1e8;
    n *= 6; if (n < 0) n = ob0Length;
    for (i = 0; i < XL1Length[zn]; i++, tn *= t) {
        FLength = XL1MLength[zn][i];
        F = ob[i];
        N = int2(n*FLength / ob0Length + 0.5);  if (i) N += 6;  if (N >= FLength) N = FLength;
        for (j = 0, c = 0; j < N; j += 6) c += F[j] * cos(F[j + 1] + t * F[j + 2] + t2 * F[j + 3] + t3 * F[j + 4] + t4 * F[j + 5]);
        v += c * tn;
    }
    if (zn != 2) v /= cs_rad;
    return v;
};

//返回月球坐标,n1,n2,n3为各坐标所取的项数
MM_EXPORT_SXWNL
void
m_coord(
    double re[3],
    double t,
    int n1,
    int n2,
    int n3)
{
    re[0] = XL1_calc(0, t, n1);
    re[1] = XL1_calc(1, t, n2);
    re[2] = XL1_calc(2, t, n3);
}

//=============================一些天文基本问题=====================================
//==================================================================================
//返回朔日的编号,jd应在朔日附近，允许误差数天
MM_EXPORT_SXWNL
double
suoN(
    double jd)
{
    return floor((jd + 8) / 29.5306); 
} 

//太阳光行差,t是世纪数
MM_EXPORT_SXWNL
double
gxc_sunLon(
    double t)
{
    double v = -0.043126 + 628.301955*t - 0.000002732*t*t; //平近点角
    double e = 0.016708634 - 0.000042037*t - 0.0000001267*t*t;
    return (-20.49552 * (1 + e * cos(v))) / cs_rad; //黄经光行差
}

//黄纬光行差
MM_EXPORT_SXWNL
double
gxc_sunLat(
    double t)
{
    return 0;
}

//月球经度光行差,误差0.07"
MM_EXPORT_SXWNL
double
gxc_moonLon(
    double t)
{
    return -3.4E-6; 
} 

//月球纬度光行差,误差0.006"
MM_EXPORT_SXWNL
double
gxc_moonLat(
    double t)
{
    return 0.063*sin(0.057 + 8433.4662*t + 0.000064*t*t) / cs_rad; 
} 

//传入T是2000年首起算的日数(UT),dt是deltatT(日),精度要求不高时dt可取值为0
MM_EXPORT_SXWNL
double
pGST(
    double T,
    double dt)
{
    //返回格林尼治平恒星时(不含赤经章动及非多项式部分),即格林尼治子午圈的平春风点起算的赤经
    double t = (T + dt) / 36525, t2 = t * t, t3 = t2 * t, t4 = t3 * t;
    return cs_pi2 * (0.7790572732640 + 1.00273781191135448*T) //T是UT,下一行的t是力学时(世纪数)
        + (0.014506 + 4612.15739966*t + 1.39667721*t2 - 0.00009344*t3 + 0.00001882*t4) / cs_rad;
}
//传入力学时J2000起算日数，返回平恒星时
MM_EXPORT_SXWNL
double
pGST2(
    double jd)
{
    double dt = dt_T(jd);
    return pGST(jd - dt, dt);
}

//太阳升降计算。jd儒略日(须接近L当地平午UT)，L地理经度，fa地理纬度，sj=-1升,sj=1降
MM_EXPORT_SXWNL
double
sunShengJ(
    double jd,
    double L,
    double fa,
    int sj)
{
    int i;
    jd = floor(jd + 0.5) - L / cs_pi2;
    for (i = 0; i < 2; i++) {
        double T = jd / 36525, E = (84381.4060 - 46.836769*T) / cs_rad; //黄赤交角
        double t = T + (32 * (T + 1.8)*(T + 1.8) - 20) / 86400 / 36525;  //儒略世纪年数,力学时
        double J = (48950621.66 + 6283319653.318*t + 53 * t*t - 994
            + 334166 * cos(4.669257 + 628.307585*t)
            + 3489 * cos(4.6261 + 1256.61517*t)
            + 2060.6 * cos(2.67823 + 628.307585*t) * t) / 10000000;
        double sinJ = sin(J), cosJ = cos(J); //太阳黄经以及它的正余弦值
        double gst = (0.7790572732640 + 1.00273781191135448*jd)*cs_pi2 + (0.014506 + 4612.15739966*T + 1.39667721*T*T) / cs_rad; //恒星时(子午圈位置)
        double A = atan2(sinJ*cos(E), cosJ);  //太阳赤经
        double D = asin(sin(E)*sinJ);        //太阳赤纬
        double cosH0 = (sin(-50 * 60 / cs_rad) - sin(fa)*sin(D)) / (cos(fa)*cos(D)); if (fabs(cosH0) >= 1) return 0; //太阳在地平线上的cos(时角)计算
        jd += rad2rrad(sj*acos(cosH0) - (gst + L - A)) / 6.28; //(升降时角-太阳时角)/太阳速度
    }
    return jd; //反回格林尼治UT
}

//时差计算(高精度),t力学时儒略世纪数
MM_EXPORT_SXWNL
double
pty_zty(
    double t)
{
    double t2 = t * t, t3 = t2 * t, t4 = t3 * t, t5 = t4 * t;
    double L = (1753470142 + 628331965331.8*t + 5296.74*t2 + 0.432*t3 - 0.1124*t4 - 0.00009*t5) / 1000000000 + cs_pi - 20.5 / cs_rad;

    double E, dE, dL;
    double z[3] = { 0 };
    dL = -17.2*sin(2.1824 - 33.75705*t) / cs_rad; //黄经章
    dE =   9.2*cos(2.1824 - 33.75705*t) / cs_rad; //交角章
    E = hcjj(t) + dE; //真黄赤交角

    //地球坐标
    z[0] = XL0_calc(0, 0, t, 50) + cs_pi + gxc_sunLon(t) + dL;
    z[1] = -(2796 * cos(3.1987 + 8433.46616*t) + 1016 * cos(5.4225 + 550.75532*t) + 804 * cos(3.88 + 522.3694*t)) / 1000000000;

    llrConv(z, z, E); //z太阳地心赤道坐标
    z[0] -= dL * cos(E);

    L = rad2rrad(L - z[0]);
    return L / cs_pi2; //单位是周(天)
}

//时差计算(低精度),误差约在1秒以内,t力学时儒略世纪数
MM_EXPORT_SXWNL
double
pty_zty2(
    double t)
{
    double L = (1753470142 + 628331965331.8*t + 5296.74*t*t) / 1000000000 + cs_pi;
    double z[3] = { 0 };
    double E = (84381.4088 - 46.836051*t) / cs_rad;
    z[0] = XL0_calc(0, 0, t, 5) + cs_pi; z[1] = 0; //地球坐标
    llrConv(z, z, E); //z太阳地心赤道坐标
    L = rad2rrad(L - z[0]);
    return L / cs_pi2; //单位是周(天)
}


//物件XL : 日月黄道平分点坐标、视坐标、速度、已知经度反求时间等方面的计算

//日月星历基本函数类

//星历函数(日月球面坐标计算)

//地球经度计算,返回Date分点黄经,传入世纪数、取项数
MM_EXPORT_SXWNL
double
XL_E_Lon(
    double t,
    int n)
{
    return XL0_calc(0, 0, t, n); 
} 

//月球经度计算,返回Date分点黄经,传入世纪数,n是项数比例
MM_EXPORT_SXWNL
double
XL_M_Lon(
    double t,
    int n)
{
    return XL1_calc(0, t, n); 
}    

//=========================
//地球速度,t是世纪数,误差小于万分3
MM_EXPORT_SXWNL
double
XL_E_v(
    double t)
{
    double f = 628.307585*t;
    return 628.332 + 21 * sin(1.527 + f) + 0.44 *sin(1.48 + f * 2)
        + 0.129*sin(5.82 + f)*t + 0.00055*sin(4.21 + f)*t*t;
}

//月球速度计算,传入世经数
MM_EXPORT_SXWNL
double
XL_M_v(
    double t)
{
    double v = 8399.71 - 914 * sin(0.7848 + 8328.691425*t + 0.0001523*t*t); //误差小于5%
    v -= 179 * sin(2.543  + 15542.7543*t)  //误差小于0.3%
       + 160 * sin(0.1874 +  7214.0629*t)
       +  62 * sin(3.14   + 16657.3828*t)
       +  34 * sin(4.827  + 16866.9323*t)
       +  22 * sin(4.9    + 23871.4457*t)
       +  12 * sin(2.59   + 14914.4523*t)
       +   7 * sin(0.23   +  6585.7609*t)
       +   5 * sin(0.9    + 25195.624 *t)
       +   5 * sin(2.32   -  7700.3895*t)
       +   5 * sin(3.88   +  8956.9934*t)
       +   5 * sin(0.49   +  7771.3771*t);
    return v;
}

//=========================
//月日视黄经的差值
MM_EXPORT_SXWNL
double
XL_MS_aLon(
    double t,
    int Mn,
    int Sn)
{
    return XL_M_Lon(t, Mn) + gxc_moonLon(t) - (XL_E_Lon(t, Sn) + gxc_sunLon(t) + cs_pi);
}

//太阳视黄经
MM_EXPORT_SXWNL
double
XL_S_aLon(
    double t,
    int n)
{
    return XL_E_Lon(t, n) + nutationLon2(t) + gxc_sunLon(t) + cs_pi; //注意，这里的章动计算很耗时
}

//=========================

//已知地球真黄经求时间
MM_EXPORT_SXWNL
double
XL_E_Lon_t(
    double W)
{
    double t, v = 628.3319653318;
    t  = (W -         1.75347) / v; v = XL_E_v(t);   //v的精度0.03%，详见原文
    t += (W - XL_E_Lon(t, 10)) / v; v = XL_E_v(t);   //再算一次v有助于提高精度,不算也可以
    t += (W - XL_E_Lon(t, -1)) / v;
    return t;
}

//已知真月球黄经求时间
MM_EXPORT_SXWNL
double
XL_M_Lon_t(
    double W)
{
    double t, v = 8399.70911033384;
    t  = (W -         3.81034) / v;
    t += (W - XL_M_Lon(t,  3)) / v; v = XL_M_v(t);  //v的精度0.5%，详见原文
    t += (W - XL_M_Lon(t, 20)) / v;
    t += (W - XL_M_Lon(t, -1)) / v;
    return t;
}

//已知月日视黄经差求时间
MM_EXPORT_SXWNL
double
XL_MS_aLon_t(
    double W)
{
    double t, v = 7771.37714500204;
    t  = (W +               1.08472) / v;
    t += (W - XL_MS_aLon(t,  3,  3)) / v; v = XL_M_v(t) - XL_E_v(t);  //v的精度0.5%，详见原文
    t += (W - XL_MS_aLon(t, 20, 10)) / v;
    t += (W - XL_MS_aLon(t, -1, 60)) / v;
    return t;
}

//已知太阳视黄经反求时间
MM_EXPORT_SXWNL
double
XL_S_aLon_t(
    double W)
{
    double t, v = 628.3319653318;
    t  = (W -  1.75347 - cs_pi) / v; v = XL_E_v(t); //v的精度0.03%，详见原文
    t += (W - XL_S_aLon(t, 10)) / v; v = XL_E_v(t); //再算一次v有助于提高精度,不算也可以
    t += (W - XL_S_aLon(t, -1)) / v;
    return t;
}

//已知月日视黄经差求时间,高速低精度,误差不超过600秒(只验算了几千年)
MM_EXPORT_SXWNL
double
XL_MS_aLon_t2(
    double W)
{
    double t, v = 7771.37714500204;
    t = (W + 1.08472) / v;
    double L, t2 = t * t;
    t -= (-0.00003309*t2 
        + 0.10976*cos(0.784758 + 8328.6914246*t + 0.000152292*t2) 
        + 0.02224*cos(0.18740  + 7214.0628654*t - 0.00021848 *t2) 
        - 0.03342*cos(4.669257 +  628.307585 *t)) / v;
    L = XL_M_Lon(t, 20) - (4.8950632 + 628.3319653318*t + 0.000005297*t*t 
        + 0.0334166*cos(4.669257 +  628.307585*t) 
        + 0.0002061*cos(2.67823  +  628.307585*t)*t 
        + 0.000349 *cos(4.6261   + 1256.61517 *t) - 20.5 / cs_rad);
    v = 7771.38 
        - 914 * sin(0.7848 +  8328.691425*t + 0.0001523*t*t) 
        - 179 * sin(2.543  + 15542.7543  *t) 
        - 160 * sin(0.1874 +  7214.0629  *t);
    t += (W - L) / v;
    return t;
}

//已知太阳视黄经反求时间,高速低精度,最大误差不超过600秒
MM_EXPORT_SXWNL
double
XL_S_aLon_t2(
    double W)
{
    double t, v = 628.3319653318;
    t = (W - 1.75347 - cs_pi) / v;
    t -= (0.000005297*t*t 
        + 0.0334166 * cos(4.669257 + 628.307585*t) 
        + 0.0002061 * cos(2.67823  + 628.307585*t)*t) / v;
    t += (W - XL_E_Lon(t, 8) - cs_pi + (20.5 + 17.2*sin(2.1824 - 33.75705*t)) / cs_rad) / v;
    return t;
}

//月亮被照亮部分的比例
MM_EXPORT_SXWNL
double
XL_moonIll(
    double t)
{
    double t2 = t * t, t3 = t2 * t, t4 = t3 * t;
    double D, M, m, a, dm = cs_pi / 180;
    D = (297.8502042 + 445267.1115168*t - 0.0016300*t2 + t3 / 545868 - t4 / 113065000)*dm; //日月平距角
    M = (357.5291092 +  35999.0502909*t - 0.0001536*t2 + t3 /                24490000)*dm; //太阳平近点
    m = (134.9634114 + 477198.8676313*t + 0.0089970*t2 + t3 /  69699 - t4 /  14712000)*dm; //月亮平近点
    a = cs_pi - D + (-6.289*sin(m) + 2.100*sin(M) - 1.274*sin(D * 2 - m) - 0.658*sin(D * 2) - 0.214*sin(m * 2) - 0.110*sin(D))*dm;
    return (1 + cos(a)) / 2;
}

//转入地平纬度及地月质心距离,返回站心视半径(角秒)
MM_EXPORT_SXWNL
double
XL_moonRad(
    double r,
    double h)
{
    return cs_sMoon / r * (1 + sin(h)*cs_rEar / r);
}

//求月亮近点时间和距离,t为儒略世纪数力学时
MM_EXPORT_SXWNL
void
XL_moonMinR(
    double re[2],
    double t,
    int min)
{
    double a = 27.55454988 / 36525, b;
    if (min) b = -10.3302 / 36525; else b = 3.4471 / 36525;
    t = b + a * int2((t - b) / a + 0.5); //平近(远)点时间
    double r1, r2, r3, dt;
    //初算二次
    dt = 2.0 / 36525;
    r1 = XL1_calc(2, t - dt, 10);
    r2 = XL1_calc(2, t     , 10);
    r3 = XL1_calc(2, t + dt, 10);
    t += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2;
    dt = 0.5 / 36525;
    r1 = XL1_calc(2, t - dt, 20);
    r2 = XL1_calc(2, t     , 20);
    r3 = XL1_calc(2, t + dt, 20);
    t += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2;
    //精算
    dt = 1200.0 / 86400 / 36525;
    r1 = XL1_calc(2, t - dt, -1);
    r2 = XL1_calc(2, t     , -1);
    r3 = XL1_calc(2, t + dt, -1);
    t  += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2;
    r2 += (r1 - r3) / (r1 + r3 - 2 * r2)*(r3 - r1) / 8;
    re[0] = t;
    re[1] = r2;
}

//月亮升交点
MM_EXPORT_SXWNL
void
XL_moonNode(
    double re[2],
    double t,
    double asc)
{
    double a = 27.21222082 / 36525, b;
    if (asc) b = 21.0 / 36525; else b = 35.0 / 36525;
    t = b + a * int2((t - b) / a + 0.5); //平升(降)交点时间
    double w, v, w2, dt;
    dt = 0.5  / 36525; w = XL1_calc(1, t, 10); w2 = XL1_calc(1, t + dt, 10); v = (w2 - w) / dt; t -= w / v;
    dt = 0.05 / 36525; w = XL1_calc(1, t, 40); w2 = XL1_calc(1, t + dt, 40); v = (w2 - w) / dt; t -= w / v;
    w = XL1_calc(1, t, -1);  t -= w / v;
    re[0] = t;
    re[1] = XL1_calc(0, t, -1);
}

//地球近远点
MM_EXPORT_SXWNL
void
XL_earthMinR(
    double re[2],
    double t,
    int min)
{
    double a = 365.25963586 / 36525, b;
    if (min) b = 1.7 / 36525; else b = 184.5 / 36525;
    t = b + a * int2((t - b) / a + 0.5); //平近(远)点时间
    double r1, r2, r3, dt;
    //初算二次
    dt = 3.0 / 36525;
    r1 = XL0_calc(0, 2, t - dt, 10);
    r2 = XL0_calc(0, 2, t     , 10);
    r3 = XL0_calc(0, 2, t + dt, 10);
    t += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2; //误差几个小时
    dt = 0.2 / 36525;
    r1 = XL0_calc(0, 2, t - dt, 80);
    r2 = XL0_calc(0, 2, t     , 80);
    r3 = XL0_calc(0, 2, t + dt, 80);
    t += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2; //误差几分钟
    //精算
    dt = 0.01 / 36525;
    r1 = XL0_calc(0, 2, t - dt, -1);
    r2 = XL0_calc(0, 2, t     , -1);
    r3 = XL0_calc(0, 2, t + dt, -1);
    t  += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2; //误差小于秒
    r2 += (r1 - r3) / (r1 + r3 - 2 * r2)*(r3 - r1) / 8;
    re[0] = t;
    re[1] = r2;
}
