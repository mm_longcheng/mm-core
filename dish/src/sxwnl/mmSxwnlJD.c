/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlJD.h"

#include "mmSxwnlConst.h"

#include "core/mmAlloc.h"

#include <stdio.h>

MM_EXPORT_SXWNL
void
csJD_Make(
    struct csJD* p,
    int Y, int M, int D,
    int h, int m, double s)
{
    p->Y = Y;
    p->M = M;
    p->D = D;
    p->h = h;
    p->m = m;
    p->s = s;
}

//公历转儒略日
MM_EXPORT_SXWNL
double
csJD_JD(
    int y,
    int m,
    double d)
{
    int n = 0, G = 0;
    if (y * 372 + m * 31 + int2(d) >= 588829) G = 1;       //判断是否为格里高利历日1582*372+10*31+15
    if (m <= 2) { m += 12; y--; }
    if (G) { n = int2(y / 100); n = 2 - n + int2(n / 4); } //加百年闰
    return int2(365.25*(y + 4716)) + int2(30.6001*(m + 1)) + d + n - 1524.5;
}

//儒略日数转公历
MM_EXPORT_SXWNL
void
csJD_DD(
    struct csJD* r,
    double jd)
{
    int c;  
    int D = int2(jd + 0.5);
    //取得日数的整数部份A及小数部分F
    double F = jd + 0.5 - D;
    if (D >= 2299161) { c = int2((D - 1867216.25) / 36524.25); D += 1 + c - int2(c / 4); }
    D += 1524;              r->Y = int2((D - 122.1) / 365.25);//年数
    D -= int2(365.25*r->Y); r->M = int2(D / 30.601);          //月数
    D -= int2(30.601*r->M); r->D = D;                         //日数
    if (r->M > 13) { r->M -= 13; r->Y -= 4715; }
    else           { r->M -=  1; r->Y -= 4716; }
    //日的小数转为时分秒
    F *= 24; r->h = int2(F); F -= r->h;
    F *= 60; r->m = int2(F); F -= r->m;
    F *= 60; r->s = F;
}

//日期转为串
MM_EXPORT_SXWNL
void
csJD_DD2str(
    const struct csJD* r,
    char str[32])
{
    int h, m, s;

    h = r->h;
    m = r->m;
    s = int2(r->s + 0.5);

    if (s >= 60) { s -= 60; m++; }
    if (m >= 60) { m -= 60; h++; }

    sprintf(str, "%5d-%02d-%02d %02d:%02d:%02d",
        r->Y, r->M, r->D, h, m, s);
}

//JD转为串
MM_EXPORT_SXWNL
void
csJD_JD2str(
    double jd,
    char str[32])
{
    struct csJD r;
    csJD_DD(&r, jd);
    csJD_DD2str(&r, str);
}

//公历转儒略日
MM_EXPORT_SXWNL
double
csJD_toJD(
    const struct csJD* p)
{
    return csJD_JD(p->Y, p->M, p->D + ((p->s / 60 + p->m) / 60 + p->h) / 24); 
} 

//儒略日数转公历
MM_EXPORT_SXWNL
void
csJD_setFromJD(
    struct csJD* p,
    double jd)
{
    csJD_DD(p, jd);
} 

//提取jd中的时间(去除日期)
MM_EXPORT_SXWNL
void
csJD_timeStr(
    double jd,
    char str[32])
{
    int h, m, s;

    jd += 0.5; jd = (jd - int2(jd));

    s = int2(jd * 86400 + 0.5);
    h = int2(s / 3600); s -= h * 3600;
    m = int2(s / 60);   s -= m * 60;
    sprintf(str, "%02d:%02d:%02d", h, m, s);
}

//星期计算
MM_EXPORT_SXWNL
int
csJD_getWeek(
    double jd)
{
    return int2(jd + 1.5 + 7000000) % 7; 
} 

//求y年m月的第n个星期w的儒略日数
MM_EXPORT_SXWNL
double
csJD_nnweek(
    int y,
    int m,
    int n,
    int w)
{
    double jd = csJD_JD(y, m, 1.5);          //月首儒略日
    double w0 = int2(jd + 1 + 7000000) % 7;  //月首的星期
    double r = jd - w0 + 7 * n + w;          //jd-w0+7*n是和n个星期0,起算下本月第一行的星期日(可能落在上一月)。加w后为第n个星期w
    if (w >= w0) r -= 7;                     //第1个星期w可能落在上个月,造成多算1周,所以考虑减1周
    if (n == 5) 
    {
        m++; if (m > 12) { m = 1; y++; }     //下个月
        if (r >= csJD_JD(y, m, 1.5)) r -= 7; //r跑到下个月则减1周
    }
    return r;
}
