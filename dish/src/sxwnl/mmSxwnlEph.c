/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlEph.h"
#include "mmSxwnlConst.h"
#include "mmSxwnlEph0.h"
#include "mmSxwnlJD.h"

#include "core/mmAlloc.h"
#include "core/mmString.h"
#include "core/mmStringUtils.h"

MM_EXPORT_SXWNL
void
csEphSJ_Init(
    struct csEphSJ* p)
{
    mmMemset(p, 0, sizeof(struct csEphSJ));
    mmString_Init(&p->sm);
}

MM_EXPORT_SXWNL
void
csEphSJ_Destroy(
    struct csEphSJ* p)
{
    mmString_Destroy(&p->sm);
    mmMemset(p, 0, sizeof(struct csEphSJ));
}

MM_EXPORT_SXWNL
void
csEphSJS_Init(
    struct csEphSJS* p)
{
    mmString_Init(&p->s);
    mmString_Init(&p->z);
    mmString_Init(&p->j);
    mmString_Init(&p->c);
    mmString_Init(&p->h);
    mmString_Init(&p->ch);
    mmString_Init(&p->sj);
    mmString_Init(&p->Ms);
    mmString_Init(&p->Mz);
    mmString_Init(&p->Mj);
}

MM_EXPORT_SXWNL
void
csEphSJS_Destroy(
    struct csEphSJS* p)
{
    mmString_Destroy(&p->Mj);
    mmString_Destroy(&p->Mz);
    mmString_Destroy(&p->Ms);
    mmString_Destroy(&p->sj);
    mmString_Destroy(&p->ch);
    mmString_Destroy(&p->h);
    mmString_Destroy(&p->c);
    mmString_Destroy(&p->j);
    mmString_Destroy(&p->z);
    mmString_Destroy(&p->s);
}

MM_EXPORT_SXWNL
void
csEphSZJ_Init(
    struct csEphSZJ* p)
{
    struct mmVectorValueAllocator hValueAllocator;

    mmVectorValue_Init(&p->rts);
    p->dn = 0;
    p->L = 0;
    p->fa = 0;
    p->E = 0.409092614;
    p->dt = 0;

    hValueAllocator.Produce = &csEphSJS_Init;
    hValueAllocator.Recycle = &csEphSJS_Destroy;
    mmVectorValue_SetAllocator(&p->rts, &hValueAllocator);
    mmVectorValue_SetElement(&p->rts, sizeof(struct csEphSJS));
}

MM_EXPORT_SXWNL
void
csEphSZJ_Destroy(
    struct csEphSZJ* p)
{
    p->dt = 0;
    p->E = 0.409092614;
    p->fa = 0;
    p->L = 0;
    p->dn = 0;
    mmVectorValue_Destroy(&p->rts);
}

//h地平纬度,w赤纬,返回时角
MM_EXPORT_SXWNL
double
csEphSZJ_getH(
    struct csEphSZJ* p,
    double h,
    double w)
{
    double c = (sin(h) - sin(p->fa)*sin(w)) / cos(p->fa) / cos(w);
    if (fabs(c) > 1) return cs_pi;
    return acos(c);
}

//章动同时影响恒星时和天体坐标,所以不计算章动。返回时角及赤经纬
MM_EXPORT_SXWNL
void
csEphSZJ_Mcoord(
    struct csEphSZJ* p,
    double jd,
    double H0,
    struct csEphSJ* r)
{
    double z[3];
    m_coord(z, (jd + p->dt) / 36525, 40, 30, 8); //低精度月亮赤经纬
    llrConv(z, z, p->E); //转为赤道坐标
    r->H = rad2rrad(pGST(jd, p->dt) + p->L - z[0]); //得到此刻天体时角

    if (H0) r->H0 = csEphSZJ_getH(p, 0.7275*cs_rEar / z[2] - 34 * 60 / cs_rad, z[1]); //升起对应的时角
}

//月亮到中升降时刻计算,传入jd含义与St()函数相同
MM_EXPORT_SXWNL
void
csEphSZJ_Mt(
    struct csEphSZJ* p,
    double jd,
    struct csEphSJ* r)
{
    p->dt = dt_T(jd);
    p->E = hcjj(jd / 36525);
    jd -= mod2(0.1726222 + 0.966136808032357*jd - 0.0366*p->dt + p->L / cs_pi2, 1); //查找最靠近当日中午的月上中天,mod2的第1参数为本地时角近似值

    double sv = cs_pi2 * 0.966;
    r->z = r->x = r->s = r->j = r->c = r->h = jd;
    csEphSZJ_Mcoord(p, jd, 1, r); //月亮坐标
    r->s += (-r->H0 - r->H) / sv;
    r->j += (r->H0 - r->H) / sv;
    r->z += (0 - r->H) / sv;
    r->x += (cs_pi - r->H) / sv;
    csEphSZJ_Mcoord(p, r->s, 1, r);  r->s += rad2rrad(-r->H0 - r->H) / sv;
    csEphSZJ_Mcoord(p, r->j, 1, r);  r->j += rad2rrad(+r->H0 - r->H) / sv;
    csEphSZJ_Mcoord(p, r->z, 0, r);  r->z += rad2rrad(0 - r->H) / sv;
    csEphSZJ_Mcoord(p, r->x, 0, r);  r->x += rad2rrad(cs_pi - r->H) / sv;
}

//章动同时影响恒星时和天体坐标,所以不计算章动。返回时角及赤经纬
MM_EXPORT_SXWNL
void
csEphSZJ_Scoord(
    struct csEphSZJ* p,
    double jd,
    int xm,
    struct csEphSJ* r)
{
    //太阳坐标(修正了光行差)
    double z[3] = { XL_E_Lon((jd + p->dt) / 36525, 5) + cs_pi - 20.5 / cs_rad, 0, 1 };
    llrConv(z, z, p->E); //转为赤道坐标
    r->H = rad2rrad(pGST(jd, p->dt) + p->L - z[0]); //得到此刻天体时角

    if (xm == 10 || xm == 1) r->H1 = csEphSZJ_getH(p, -50 *   60 / cs_rad, z[1]); //地平以下50分
    if (xm == 10 || xm == 2) r->H2 = csEphSZJ_getH(p, - 6 * 3600 / cs_rad, z[1]); //地平以下6度
    if (xm == 10 || xm == 3) r->H3 = csEphSZJ_getH(p, -12 * 3600 / cs_rad, z[1]); //地平以下12度
    if (xm == 10 || xm == 4) r->H4 = csEphSZJ_getH(p, -18 * 3600 / cs_rad, z[1]); //地平以下18度
}

//太阳到中升降时刻计算,传入jd是当地中午12点时间对应的2000年首起算的格林尼治时间UT
MM_EXPORT_SXWNL
void
csEphSZJ_St(
    struct csEphSZJ* p,
    double jd,
    struct csEphSJ* r)
{
    p->dt = dt_T(jd);
    p->E = hcjj(jd / 36525);
    jd -= mod2(jd + p->L / cs_pi2, 1); //查找最靠近当日中午的日上中天,mod2的第1参数为本地时角近似值

    double sv = cs_pi2;
    r->z = r->x = r->s = r->j = r->c = r->h = r->c2 = r->h2 = r->c3 = r->h3 = jd; 
    mmString_Assigns(&r->sm, "");
    csEphSZJ_Scoord(p, jd, 10, r); //太阳坐标
    r->s  += (-r->H1 - r->H) / sv; //升起
    r->j  += ( r->H1 - r->H) / sv; //降落

    r->c  += (-r->H2 - r->H) / sv; //民用晨
    r->h  += ( r->H2 - r->H) / sv; //民用昏
    r->c2 += (-r->H3 - r->H) / sv; //航海晨
    r->h2 += ( r->H3 - r->H) / sv; //航海昏
    r->c3 += (-r->H4 - r->H) / sv; //天文晨
    r->h3 += ( r->H4 - r->H) / sv; //天文昏

    r->z  += (0      - r->H) / sv; //中天
    r->x  += (cs_pi  - r->H) / sv; //下中天
    csEphSZJ_Scoord(p, r->s , 1, r); r->s  += rad2rrad(-r->H1 - r->H) / sv; if (r->H1 == cs_pi) mmString_Appends(&r->sm, "无升起.");
    csEphSZJ_Scoord(p, r->j , 1, r); r->j  += rad2rrad(+r->H1 - r->H) / sv; if (r->H1 == cs_pi) mmString_Appends(&r->sm, "无降落.");

    csEphSZJ_Scoord(p, r->c , 2, r); r->c  += rad2rrad(-r->H2 - r->H) / sv; if (r->H2 == cs_pi) mmString_Appends(&r->sm, "无民用晨.");
    csEphSZJ_Scoord(p, r->h , 2, r); r->h  += rad2rrad(+r->H2 - r->H) / sv; if (r->H2 == cs_pi) mmString_Appends(&r->sm, "无民用昏.");
    csEphSZJ_Scoord(p, r->c2, 3, r); r->c2 += rad2rrad(-r->H3 - r->H) / sv; if (r->H3 == cs_pi) mmString_Appends(&r->sm, "无航海晨.");
    csEphSZJ_Scoord(p, r->h2, 3, r); r->h2 += rad2rrad(+r->H3 - r->H) / sv; if (r->H3 == cs_pi) mmString_Appends(&r->sm, "无航海昏.");
    csEphSZJ_Scoord(p, r->c3, 4, r); r->c3 += rad2rrad(-r->H4 - r->H) / sv; if (r->H4 == cs_pi) mmString_Appends(&r->sm, "无天文晨.");
    csEphSZJ_Scoord(p, r->h3, 4, r); r->h3 += rad2rrad(+r->H4 - r->H) / sv; if (r->H4 == cs_pi) mmString_Appends(&r->sm, "无天文昏.");

    csEphSZJ_Scoord(p, r->z, 0, r);  r->z += (0 - r->H) / sv;
    csEphSZJ_Scoord(p, r->x, 0, r);  r->x += rad2rrad(cs_pi - r->H) / sv;
}

//多天升中降计算,jd是当地起始略日(中午时刻),sq是时区
MM_EXPORT_SXWNL
void
csEphSZJ_calcRTS(
    struct csEphSZJ* p,
    double jd,
    int n,
    double Jdl,
    double Wdl,
    double sq)
{
    char str[32];
    int i, c;
    struct csEphSJ r;
    struct csEphSJS* rr;
    struct csEphSJS* rts;
    mmVectorValue_AllocatorMemory(&p->rts, 31);
    rts = (struct csEphSJS*)p->rts.arrays;
    p->L = Jdl; p->fa = Wdl; sq /= 24; //设置站点参数
    for (i = 0; i < n; i++) 
    { 
        rr = &rts[i];
        mmString_Assigns(&rr->Ms, "--:--:--");
        mmString_Assigns(&rr->Mz, "--:--:--");
        mmString_Assigns(&rr->Mj, "--:--:--");
    }
    for (i = -1; i <= n; i++) {
        if (i >= 0 && i < n) { //太阳
            rr = &rts[i];
            csEphSZJ_St(p, jd + i + sq, &r);
            csJD_timeStr(r.s - sq, str); mmString_Assigns(&rr->s, str); //升
            csJD_timeStr(r.z - sq, str); mmString_Assigns(&rr->z, str); //中
            csJD_timeStr(r.j - sq, str); mmString_Assigns(&rr->j, str); //降
            csJD_timeStr(r.c - sq, str); mmString_Assigns(&rr->c, str); //晨
            csJD_timeStr(r.h - sq, str); mmString_Assigns(&rr->h, str); //昏
            csJD_timeStr(r.h - r.c - 0.5, str); mmString_Assigns(&rr->ch, str); //光照时间,timeStr()内部+0.5,所以这里补上-0.5
            csJD_timeStr(r.j - r.s - 0.5, str); mmString_Assigns(&rr->sj, str); //昼长
        }
        csEphSZJ_Mt(p, jd + i + sq, &r); //月亮
        c = (int)(int2(r.s - sq + 0.5) - jd); if (c >= 0 && c < n) { csJD_timeStr(r.s - sq, str); mmString_Assigns(&rts[c].Ms, str); }
        c = (int)(int2(r.z - sq + 0.5) - jd); if (c >= 0 && c < n) { csJD_timeStr(r.z - sq, str); mmString_Assigns(&rts[c].Mz, str); }
        c = (int)(int2(r.j - sq + 0.5) - jd); if (c >= 0 && c < n) { csJD_timeStr(r.j - sq, str); mmString_Assigns(&rts[c].Mj, str); }
    }
    p->dn = n;
}

/********************
升降计算等
*********************/
//升降字符串
MM_EXPORT_SXWNL
void
csEphSZJ_RTS1(
    struct csEphSZJ* p,
    double jd,
    double vJ,
    double vW,
    int tz,
    struct mmString* s)
{
    struct csEphSJS* ob;
    //升降计算,使用北时时间,tz=-8指东8区,jd+tz应在当地正午左右(误差数小时不要紧)
    csEphSZJ_calcRTS(p, jd, 1, vJ, vW, (double)tz); 
    ob = (struct csEphSJS*)mmVectorValue_At(&p->rts, 0);
    // JD.setFromJD(jd+J2000);
    mmString_Reset(s);

    mmString_Appends(s,  "日出 "); mmString_Append(s, &ob->s);
    mmString_Appends(s, " 日落 "); mmString_Append(s, &ob->j);
    mmString_Appends(s, " 中天 "); mmString_Append(s, &ob->z);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "月出 "); mmString_Append(s, &ob->Ms);
    mmString_Appends(s, " 月落 "); mmString_Append(s, &ob->Mj);
    mmString_Appends(s, " 月中 "); mmString_Append(s, &ob->Mz);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "晨起天亮 "); mmString_Append(s, &ob->c);
    mmString_Appends(s, " 晚上天黑 "); mmString_Append(s, &ob->h);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "日照时间 "); mmString_Append(s, &ob->sj);
    mmString_Appends(s, " 白天时间 "); mmString_Append(s, &ob->ch);
    mmString_Appends(s, "\n");
}

//====================升降表===================
//升降表字符串
MM_EXPORT_SXWNL
void
csEphSZJ_shengjiang(
    struct csEphSZJ* p,
    int y,
    int m,
    double d,
    double vJ,
    double vW,
    struct mmString* s)
{
    struct csEphSJ r;
    char str[32] = { 0 };
    double c;
    double jd = csJD_JD(y, m, d + 0.5) - cs_J2000;
    double sq = vJ / cs_pi2 * 24;

    //设置站点参数
    p->L  = vJ; 
    p->fa = vW;

    csEphSJ_Init(&r);

    mmString_Reset(s);

    mmString_Assigns(s, "北京时间(转为格林尼治时间请减8小时)：\n");

    c = cs_J2000 + 8.0 / 24;
    csEphSZJ_St(p, jd - sq / 24, &r);

    mmString_Appends(s,  "太阳升起 "); csJD_JD2str(r.s + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 太阳降落 "); csJD_JD2str(r.j + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "日上中天 "); csJD_JD2str(r.z + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 日下中天 "); csJD_JD2str(r.x + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "民用天亮 "); csJD_JD2str(r.c + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 民用天黑 "); csJD_JD2str(r.h + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "航海天亮 "); csJD_JD2str(r.c2 + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 航海天黑 "); csJD_JD2str(r.h2 + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "日照长度 "); csJD_timeStr(r.j - r.s - 0.5, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 日光长度 "); csJD_timeStr(r.h - r.c - 0.5, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    if (!mmString_Empty(&r.sm))
    {
        mmString_Appends(s, "注：");
        mmString_Append(s, &r.sm);
        mmString_Appends(s, "\n");
    }

    csEphSZJ_Mt(p, jd - sq / 24, &r);

    mmString_Appends(s,  "月亮升起 "); csJD_JD2str(r.s + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 月亮降落 "); csJD_JD2str(r.j + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s,  "月上中天 "); csJD_JD2str(r.z + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, " 月下中天 "); csJD_JD2str(r.x + c, str);  mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    csEphSJ_Destroy(&r);
}

//太阳升降快算
MM_EXPORT_SXWNL
void
csEphSZJ_shengjiang2(
    int y,
    double vJ,
    double vW,
    struct mmString* s)
{
    int i;
    double t;
    double L = vJ; //设置站点参数
    double fa = vW;
    double jd = csJD_JD(y, 1, 1.5) - cs_J2000;
    char str[32] = { 0 };
    mmString_Reset(s);
    mmString_Reserve(s, 12289);

    mmString_Appends(s, "太阳年度升降表\n");
    for (i = 0; i < 368; i++) {
        t = sunShengJ(jd + i, L, fa, -1) + cs_J2000 + 8.0 / 24; 
        csJD_JD2str(t, str);
        mmString_Appendsn(s, &str[6], 14); mmString_Appends(s, ", ");

        t = sunShengJ(jd + i, L, fa,  1) + cs_J2000 + 8.0 / 24; 
        csJD_timeStr(t, str);
        mmString_Appends(s, str); mmString_Appends(s, "\n");
    }
}

//年度时差
MM_EXPORT_SXWNL
void
csEphSZJ_shengjiang3(
    int y,
    struct mmString* s)
{
    int i;
    double D, t;
    double jd = csJD_JD(y, 1, 1.5);
    char str[32] = { 0 };

    mmString_Reset(s);
    mmString_Reserve(s, 12289);

    mmString_Appends(s, "太阳时差表(所用时间为北京时间每日12点)\n");

    for (i = 0; i < 368; i++) {
        D = jd + i - 8.0 / 24 - cs_J2000; D += dt_T(D);
        t = pty_zty(D / 36525); 
        csJD_JD2str(jd + i, str);
        mmString_Appendsn(s, &str[0], 11);
        mmString_Appends(s, " ");
        m2fmv2(str, t * 86400, 2, 2, 2);
        mmString_Appends(s, str);
        mmString_Appends(s, "\n");
    }
}

//========行星天象及星历=============

//大距计算
//行星的距角,jing为精度控
MM_EXPORT_SXWNL
double
xingJJ(
    int xt,
    double t,
    int jing)
{
    double a[3], z[3];
    p_coord(a,  0, t, 10, 10, 10);  //地球
    p_coord(z, xt, t, 10, 10, 10);  //行星
    h2g(z, z, a); //转到地心
    if (jing == 0) {}; //低精度
    if (jing == 1) { //中精度
        p_coord(a,  0, t, 60, 60, 60);  //地球
        p_coord(z, xt, t, 60, 60, 60);  //行星
        h2g(z, z, a); //转到地心
    }
    if (jing >= 2) { //高精度(补光行时)
        p_coord(a,  0, t - a[2] * cs_Agx, -1, -1, -1); //地球
        p_coord(z, xt, t - z[2] * cs_Agx, -1, -1, -1); //行星
        h2g(z, z, a); //转到地心
    }
    a[0] += cs_pi; a[1] = -a[1]; //太阳
    return j1_j2(z[0], z[1], a[0], a[1]);
}

//大距计算超底速算法, dx=1东大距,t儒略世纪TD
MM_EXPORT_SXWNL
void
daJu(
    double re[2],
    int xt,
    double t,
    int dx)
{
    static const double csx[5] = { 2, 0.2, 0.01,  46,  87 };//水星
    static const double cjx[5] = { 4, 0.2, 0.01, 382, 521 };//金星
    int i;
    double dt, r1, r2, r3;
    double a, b, c[5];
    // 消除可能的未初始化警告
    a = 0; r1 = 0; r2 = 0; r3 = 0;
    if (xt == 1) { a = 115.8774777586 / 36525; mmMemcpy(c, csx, sizeof(c)); } //水星
    if (xt == 2) { a = 583.9213708245 / 36525; mmMemcpy(c, cjx, sizeof(c)); } //金星
    if (dx) b = c[3] / 36525;
    else    b = c[4] / 36525;
    t = b + a * int2((t - b) / a + 0.5); //大距平时间
    for (i = 0; i < 3; i++) {
        dt = c[i] / 36525;
        r1 = xingJJ(xt, t - dt, i);
        r2 = xingJJ(xt, t, i);
        r3 = xingJJ(xt, t + dt, i);
        t += (r1 - r3) / (r1 + r3 - 2 * r2)*dt / 2;
    }
    r2 += (r1 - r3) / (r1 + r3 - 2 * r2)*(r3 - r1) / 8;
    re[0] = t;
    re[1] = r2;
}

//行星的视坐标
MM_EXPORT_SXWNL
void
xingLiu0(
    double r[3],
    int xt,
    double t,
    int n,
    double gxs)
{
    double a[3], z[3];
    double zd[2];
    double E = hcjj(t);
    p_coord(a,  0, t - gxs, n, n, n);  //地球
    p_coord(z, xt, t - gxs, n, n, n);  //行星
    h2g(z, z, a); //转到地心
    if (gxs) { //如果计算了光行时，那么也计算章动
        nutation2(zd, t);  //章动计算
        z[0] += zd[0];
        E += zd[1];
    }
    llrConv(r, z, E);
}

//留,sn=1顺留
MM_EXPORT_SXWNL
double
xingLiu(
    int xt,
    double t,
    int sn)
{
    int i, n;
    double g;
    double y1[3], y2[3], y3[3];
    //先求冲(下合)
    double hh = cs_xxHH[xt - 1] / 36525; //会合周期
    double v = cs_pi2 / hh; if (xt > 2) v = -v; //行星相对地球的黄经平速度
    for (i = 0; i < 6; i++) t -= rad2rrad(XL0_calc(xt, 0, t, 8) - XL0_calc(0, 0, t, 8)) / v;  //水星的平速度与真速度相差较多,所以多算几次
    double dt;
    static const double tt[] = { 5.0 / 36525, 1.0 / 36525, 0.5 / 36525, 2e-6, 2e-6 };
    static const double tc[] = { 17.4, 28, 52, 82, 86, 88, 89, 90 };
    double tx = tc[xt - 1] / 36525;

    if (sn) { if (xt > 2) t -= tx; else t += tx; } //顺留
    else    { if (xt > 2) t += tx; else t -= tx; } //逆留
    for (i = 0; i < 4; i++) {
        dt = tt[i]; n = 8; g = 0;
        if (i >= 3) {
            g = y2[2] * cs_Agx;
            n = -1;
        }
        xingLiu0(y1, xt, t - dt, n, g);
        xingLiu0(y2, xt, t, n, g);
        xingLiu0(y3, xt, t + dt, n, g);
        t += (y1[0] - y3[0]) / (y1[0] + y3[0] - 2 * y2[0])*dt / 2;
    }
    return t;
}

//合月计算
//月亮行星视赤经差
MM_EXPORT_SXWNL
void
xingMP(
    double re[4],
    int xt,
    double t,
    int n,
    double E,
    const double g[4])
{
    double a[3], p[3], m[3];
    p_coord(a,  0, t - g[1], n, n, n); //地球
    p_coord(p, xt, t - g[1], n, n, n); //行星
    m_coord(m,     t - g[0], n, n, n); //月亮
    h2g(p, p, a);
    m[0] += g[2];  p[0] += g[2];
    llrConv(m, m, E + g[3]);
    llrConv(p, p, E + g[3]);

    re[0] = rad2rrad(m[0] - p[0]);
    re[1] = m[1] - p[1];
    re[2] = m[2] / cs_GS / 86400 / 36525;
    re[3] = p[2] / cs_GS / 86400 / 36525 * cs_AU;
}

//行星合月(视赤经),t儒略世纪TD
MM_EXPORT_SXWNL
void
xingHY(
    double re[2],
    int xt,
    double t)
{
    double i, v, E;
    double g[4] = { 0 };
    double d[4], d2[4];
    double zd[2];
    for (i = 0; i < 3; i++) {
        xingMP(d, xt, t, 8, 0.4091, g);
        t -= d[0] / 8192;
    }
    E = hcjj(t);
    nutation2(zd, t);
    //光行时,章动
    g[0] = d[2]; g[1] = d[3]; g[2] = zd[0]; g[3] = zd[1];

    xingMP( d, xt, t       , 8, E, g);
    xingMP(d2, xt, t + 1e-6, 8, E, g);
    v = (d2[0] - d[0]) / 1e-6; //速度

    xingMP(d, xt, t, 30, E, g); t -= d[0] / v;
    xingMP(d, xt, t, -1, E, g); t -= d[0] / v;

    re[0] = t;
    re[1] = d[1];
}

//合冲日计算(视黄经合冲)
//行星太阳视黄经差与w0的差
MM_EXPORT_SXWNL
void
xingSP(
    double re[4],
    int xt,
    double t,
    int n,
    double w0,
    double ts,
    double tp)
{
    double a[3], p[3], s[3];
    p_coord(a,  0, t - tp, n, n, n); //地球
    p_coord(p, xt, t - tp, n, n, n); //行星
    p_coord(s,  0, t - ts, n, n, n); s[0] += cs_pi; s[1] = -s[1]; //太阳
    h2g(p, p, a);
    re[0] = rad2rrad(p[0] - s[0] - w0);
    re[1] = p[1] - s[1];
    re[2] = s[2] * cs_Agx;
    re[3] = p[2] * cs_Agx;
}

//xt星体号,t儒略世纪TD,f=1求冲(或下合)否则求合(或下合)
MM_EXPORT_SXWNL
void
xingHR(
    double re[2],
    int xt,
    double t,
    int f)
{
    double a[4], b[4];
    double i, v, dt = 2e-5;
    double w0 = cs_pi, w1 = 0; //合(或上合)时,日心黄经差为180，地心黄经差为0
    if (f) { //求冲(或下合)
        w0 = 0; //日心黄经差
        if (xt > 2) w1 = cs_pi; //地心黄经差(冲)
    }
    v = cs_pi2 / cs_xxHH[xt - 1] * 36525; if (xt > 2) v = -v; //行星相对地球的黄经平速度
    for (i = 0; i < 6; i++) t -= rad2rrad(XL0_calc(xt, 0, t, 8) - XL0_calc(0, 0, t, 8) - w0) / v;  //水星的平速度与真速度相差较多,所以多算几次
    //严格计算
    xingSP(a, xt, t, 8, w1, 0, 0);
    xingSP(b, xt, t + dt, 8, w1, 0, 0);
    v = (b[0] - a[0]) / dt;
    xingSP(a, xt, t, 40, w1, a[2], a[3]); t -= a[0] / v;
    xingSP(a, xt, t, -1, w1, a[2], a[3]); t -= a[0] / v;

    re[0] = t;
    re[1] = a[1];
}

//星历计算

//行星计算,jd力学时
MM_EXPORT_SXWNL
void
xingX(
    struct mmString* s,
    int xt,
    double jd,
    double L,
    double fa)
{
     //基本参数计算
    double zd[2];
    double T = jd / 36525;
    nutation2(zd, T);
    double dL = zd[0], dE = zd[1];      //章动
    double E = hcjj(T) + dE;            //真黄赤交角
    double gstPing = pGST2(jd);         //平恒星时
    double gst = gstPing + dL * cos(E); //真恒星时(不考虑非多项式部分)

    double z[3], a[3], z2[3], a2[3];
    mmString_Assigns(s, "");
    double ra, rb, rc;
    double sj;
    int rfn = 8;

    char str[32];
    
    // 消除可能的未初始化警告
    rc = 0;

    if (xt == 10) { //月亮
        rfn = 2;
        //求光行时并精确求出地月距
        e_coord(a, T, 15, 15, 15); //地球
        m_coord(z, T,  1,  1, -1); ra = z[2]; //月亮

        T -= ra * cs_Agx / cs_AU; //光行时计算

        //求视坐标
        e_coord(a2, T, 15, 15, 15);//地球
        m_coord( z, T, -1, -1, -1); rc = z[2]; //月亮

        //求光行距
        h2g(a2, a, a2); a2[2] *= cs_AU;
        h2g(z2, z, a2); rb = z2[2];

        //地心黄道及地心赤道
        z[0] = rad2mrad(z[0] + dL);

        rad2str(str, z[0], 0); mmString_Appends(s,  "视黄经 "); mmString_Appends(s, str);
        rad2str(str, z[1], 0); mmString_Appends(s, " 视黄纬 "); mmString_Appends(s, str);
        toFixed(str, ra, rfn); mmString_Appends(s, " 地心距 "); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        //转到赤道坐标
        llrConv(z, z, E); 

        rad2str(str, z[0], 0); mmString_Appends(s,  "视赤经 "); mmString_Appends(s, str);
        rad2str(str, z[1], 0); mmString_Appends(s, " 视赤纬 "); mmString_Appends(s, str);
        toFixed(str, rb, rfn); mmString_Appends(s, " 光行距 "); mmString_Appends(s, str);
        mmString_Appends(s, "\n");
    }
    if (xt < 10) { //行星和太阳
        p_coord(a,  0, T, -1, -1, -1); //地球
        p_coord(z, xt, T, -1, -1, -1); //行星
        z[0] = rad2mrad(z[0]);
        rad2str(str, z[0],   0); mmString_Appends(s,  "黄经一 "); mmString_Appends(s, str);
        rad2str(str, z[1],   0); mmString_Appends(s, " 黄纬一 "); mmString_Appends(s, str);
        toFixed(str, z[2], rfn); mmString_Appends(s, " 向径一 "); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        //地心黄道
        h2g(z, z, a); ra = z[2];  //ra地心距
        T -= ra * cs_Agx; //光行时

        //重算坐标
        p_coord(a2,  0, T, -1, -1, -1); //地球
        p_coord(z2, xt, T, -1, -1, -1); //行星
        h2g(z, z2, a);  rb = z[2]; //rb光行距(在惯性系中看)
        h2g(z, z2, a2); rc = z[2]; //rc视距
        z[0] = rad2mrad(z[0] + dL); //补章动

        rad2str(str, z[0], 0); mmString_Appends(s,  "视黄经 "); mmString_Appends(s, str);
        rad2str(str, z[1], 0); mmString_Appends(s, " 视黄纬 "); mmString_Appends(s, str);
        toFixed(str, ra, rfn); mmString_Appends(s, " 地心距 "); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        //转到赤道坐标
        llrConv(z, z, E); 

        rad2str(str, z[0], 1); mmString_Appends(s,  "视赤经 "); mmString_Appends(s, str);
        rad2str(str, z[1], 0); mmString_Appends(s, " 视赤纬 "); mmString_Appends(s, str);
        toFixed(str, rb, rfn); mmString_Appends(s, " 光行距 "); mmString_Appends(s, str);
        mmString_Appends(s, "\n");
    }
    sj = rad2rrad(gst + L - z[0]); //得到天体时角
    parallax(z, z, sj, fa, 0); //视差修正
    rad2str(str, z[0], 1); mmString_Appends(s,  "站赤经 "); mmString_Appends(s, str);
    rad2str(str, z[1], 0); mmString_Appends(s, " 站赤纬 "); mmString_Appends(s, str);
    toFixed(str, rc, rfn); mmString_Appends(s, " 视距离 "); mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    z[0] += cs_pi / 2 - gst - L;  //修正了视差的赤道坐标
    llrConv(z, z, cs_pi / 2 - fa); //转到时角坐标转到地平坐标
    z[0] = rad2mrad(cs_pi / 2 - z[0]);

    if (z[1] > 0) z[1] += MQC(z[1]); //大气折射修正
    rad2str(str, z[0], 0); mmString_Appends(s,  "方位角 "); mmString_Appends(s, str);
    rad2str(str, z[1], 0); mmString_Appends(s, " 高度角 "); mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    mmString_Appends(s, "恒星时 ");
    rad2str(str, rad2mrad(gstPing), 1); mmString_Appends(s, str); mmString_Appends(s, "(平) ");
    rad2str(str, rad2mrad(gst    ), 1); mmString_Appends(s, str); mmString_Appends(s, "(真)");
    mmString_Appends(s, "\n");
}

//========日月食计算使用的一些函数=============

//求空间两点连线与地球的交点(靠近点x1的交点)
MM_EXPORT_SXWNL
void
lineEll(
    struct csEphCoord* p,
    double x1, double y1,
    double z1,
    double x2, double y2,
    double z2,
    double e,
    double r)
{
    double dx = x2 - x1, dy = y2 - y1, dz = z2 - z1, e2 = e * e, A, B, C, D, R, t;
    A = dx * dx + dy * dy + dz * dz / e2;
    B = x1 * dx + y1 * dy + z1 * dz / e2;
    C = x1 * x1 + y1 * y1 + z1 * z1 / e2 - r * r;
    p->D = B * B - A * C; if (p->D < 0) return;   //判别式小于0无解
    D = sqrt(p->D); if (B < 0) D = -D;            //只求靠近x1的交点
    t = (-B + D) / A;
    p->x = x1 + dx * t; p->y = y1 + dy * t; p->z = z1 + dz * t;
    R = sqrt(dx*dx + dy * dy + dz * dz);
    p->R1 = R * fabs(t); p->R2 = R * fabs(t - 1); //R1,R2分别为x1,x2到交点的距离
}

//I是贝塞尔坐标参数
MM_EXPORT_SXWNL
void
lineEar2(
    struct csEphCoord* p,
    double x1, double y1,
    double z1,
    double x2, double y2,
    double z2,
    double e,
    double r,
    const double I[3])
{
    double P = cos(I[1]), Q = sin(I[1]);
    double X1 = x1, Y1 = P * y1 - Q * z1, Z1 = Q * y1 + P * z1;
    double X2 = x2, Y2 = P * y2 - Q * z2, Z2 = Q * y2 + P * z2;
    lineEll(p, X1, Y1, Z1, X2, Y2, Z2, e, r);
    p->J = p->W = 100;
    if (p->D < 0) return;
    p->J = rad2rrad(atan2(p->y, p->x) + I[0] - I[2]);
    p->W = atan(p->z / e / e / sqrt(p->x*p->x + p->y*p->y));
}

//在分点坐标中求空间两点连线与地球的交点(靠近点P的交点),返回地标
MM_EXPORT_SXWNL
void
lineEar(
    struct csEphCoord* r,
    const double P[3],
    const double Q[3],
    double gst)
{
    double p[3], q[3];
    llr2xyz(p, P); llr2xyz(q, Q);
    lineEll(r, p[0], p[1], p[2], q[0], q[1], q[2], cs_ba, cs_rEar);
    if (r->D < 0) { r->J = r->W = 100; return; } //反回100表示无解
    r->W = atan(r->z / cs_ba2 / sqrt(r->x*r->x + r->y*r->y));
    r->J = rad2rrad(atan2(r->y, r->x) - gst);
}

//椭圆与圆的交点,R椭圆长半径,R2圆半径,x0,y0圆的圆心
MM_EXPORT_SXWNL
void
cirOvl(
    struct csEphNode* re,
    double R,
    double ba,
    double R2,
    double x0, double y0)
{
    double d = sqrt(x0*x0 + y0 * y0);
    double sinB = y0 / d, cosB = x0 / d;
    double cosA = (R*R + d * d - R2 * R2) / (2 * d*R);
    if (fabs(cosA) > 1) { re->n = 0; return; } //无解
    double sinA = sqrt(1 - cosA * cosA);

    int k;
    double g, ba2 = ba * ba, C, S;
    for (k = -1; k < 2; k += 2) {
        S = cosA * sinB + sinA * cosB*k;
        g = R - S * S*(1 / ba2 - 1) / 2;
        cosA = (g*g + d * d - R2 * R2) / (2 * d*g);
        if (fabs(cosA) > 1) { re->n = 0; return; } //无解
        sinA = sqrt(1 - cosA * cosA);
        C = cosA * cosB - sinA * sinB*k;
        S = cosA * sinB + sinA * cosB*k;
        if (k == 1) 
        { 
            re->A[0] = g * C;
            re->A[1] = g * S;
        }
        else 
        { 
            re->B[0] = g * C;
            re->B[1] = g * S;
        }
    }
    re->n = 2;
}

MM_EXPORT_SXWNL
void
lineOvl(
    struct csEphNode* p,
    double x1, double y1,
    double dx, double dy,
    double r,
    double ba)
{
    double A, B, C, D, L, t1, t2;
    double f = ba * ba;
    A = dx * dx + dy * dy / f;
    B = x1 * dx + y1 * dy / f;
    C = x1 * x1 + y1 * y1 / f - r * r;
    D = B * B - A * C; if (D < 0) { p->n = 0; return; }//判别式小于0无解
    if (!D) p->n = 1; else p->n = 2;
    D = sqrt(D);
    t1 = (-B + D) / A; t2 = (-B - D) / A;
    p->A[0] = x1 + dx * t1; p->A[1] = y1 + dy * t1;
    p->B[0] = x1 + dx * t2; p->B[1] = y1 + dy * t2;
    L = sqrt(dx*dx + dy * dy);
    p->R1 = L * fabs(t1); //x1到交点1的距离
    p->R2 = L * fabs(t2); //x1到交点2的距离
}

//sun_moon类的成员函数。参数：T是力学时,站点经纬L,fa,海拔high(千米)
MM_EXPORT_SXWNL
void
csEphMSC_calc(
    struct csEphMSC* p,
    double T,
    double L,
    double fa,
    double high)
{
    double zd[2];
    double z[3];
    //基本参数计算
    p->T = T; p->L = L; p->fa = fa;
    p->dt = dt_T(T); //TD-UT
    p->jd = T - p->dt;    //UT
    T /= 36525;
    nutation2(zd, T);
    p->dL = zd[0];   //黄经章
    p->dE = zd[1];   //交角章动
    p->E = hcjj(T) + p->dE; //真黄赤交角
    p->gst = pGST(p->jd, p->dt) + p->dL*cos(p->E); //真恒星时(不考虑非多项式部分)

    //=======月亮========
    //月亮黄道坐标
    m_coord(z, T, -1, -1, -1); //月球坐标
    z[0] = rad2mrad(z[0] + gxc_moonLon(T) + p->dL);  z[1] += gxc_moonLat(T);  //补上月球光行差及章动
    p->mHJ = z[0]; p->mHW = z[1]; p->mR = z[2]; //月球视黄经,视黄纬,地月质心距

    //月球赤道坐标
    llrConv(z, z, p->E); //转为赤道坐标
    p->mCJ = z[0]; p->mCW = z[1]; //月球视赤经,月球赤纬

    //月亮时角计算
    p->mShiJ = rad2mrad(p->gst + L - z[0]); //得到此刻天体时角
    if (p->mShiJ > cs_pi) p->mShiJ -= cs_pi2;

    //修正了视差的赤道坐标
    parallax(z, z, p->mShiJ, fa, high); //视差修正
    p->mCJ2 = z[0]; p->mCW2 = z[1]; p->mR2 = z[2];

    //月亮时角坐标
    z[0] += cs_pi / 2 - p->gst - L;  //转到相对于地平赤道分点的赤道坐标(时角坐标)

    //月亮地平坐标
    llrConv(z, z, cs_pi / 2 - fa);    //转到地平坐标(只改经纬度)
    z[0] = rad2mrad(cs_pi / 2 - z[0]);
    p->mDJ = z[0]; p->mDW = z[1]; //方位角,高度角
    if (z[1] > 0) z[1] += MQC(z[1]); //大气折射修正
    p->mPJ = z[0]; p->mPW = z[1]; //方位角,高度角

    //=======太阳========
    //太阳黄道坐标
    e_coord(z, T, -1, -1, -1);   //地球坐标
    z[0] = rad2mrad(z[0] + cs_pi + gxc_sunLon(T) + p->dL);  //补上太阳光行差及章动
    z[1] = -z[1] + gxc_sunLat(T); //z数组为太阳地心黄道视坐标
    p->sHJ = z[0]; p->sHW = z[1]; p->sR = z[2]; //太阳视黄经,视黄纬,日地质心距

    //太阳赤道坐标
    llrConv(z, z, p->E); //转为赤道坐标
    p->sCJ = z[0]; p->sCW = z[1]; //太阳视赤经,视赤纬

    //太阳时角计算
    p->sShiJ = rad2mrad(p->gst + L - z[0]); //得到此刻天体时角
    if (p->sShiJ > cs_pi) p->sShiJ -= cs_pi2;

    //修正了视差的赤道坐标
    parallax(z, z, p->sShiJ, fa, high); //视差修正
    p->sCJ2 = z[0]; p->sCW2 = z[1]; p->sR2 = z[2];

    //太阳时角坐标
    z[0] += cs_pi / 2 - p->gst - L;  //转到相对于地平赤道分点的赤道坐标

    //太阳地平坐标
    llrConv(z, z, cs_pi / 2 - fa);
    z[0] = rad2mrad(cs_pi / 2 - z[0]);
    //z[1] -= 8.794/cs_rad/z[2]*cos(z[1]); //直接在地平坐标中视差修正(这里把地球看为球形,精度比parallax()稍差一些)
    p->sDJ = z[0]; p->sDW = z[1]; //方位角,高度角

    if (z[1] > 0) z[1] += MQC(z[1]); //大气折射修正
    p->sPJ = z[0]; p->sPW = z[1]; //方位角,高度角

    //=======其它========
    //时差计算
    double t = T / 10, t2 = t * t, t3 = t2 * t, t4 = t3 * t, t5 = t4 * t;
    double Lon = (1753470142 + 6283319653318 * t + 529674 * t2 + 432 * t3 - 1124 * t4 - 9 * t5) / 1000000000 + cs_pi - 20.5 / cs_rad; //修正了光行差的太阳平黄经
    Lon = rad2mrad(Lon - (p->sCJ - p->dL*cos(p->E))); //(修正了光行差的平黄经)-(不含dL*cos(E)的视赤经)
    if (Lon > cs_pi) Lon -= cs_pi2; //得到时差,单位是弧度
    p->sc = Lon / cs_pi2;   //时差(单位:日)

    //真太阳与平太阳
    p->pty = p->jd + L / cs_pi2;  //平太阳时
    p->zty = p->jd + L / cs_pi2 + p->sc; //真太阳时

    //视半径
    //p->mRad = XL.moonRad(p->mR,p->mDW);  //月亮视半径(角秒)
    p->mRad = cs_sMoon / p->mR2; //月亮视半径(角秒)
    p->sRad = 959.63 / p->sR2; //太阳视半径(角秒)
    p->e_mRad = cs_sMoon / p->mR; //月亮地心视半径(角秒)
    p->eShadow  = (cs_rEarA / p->mR*cs_rad - (959.63 - 8.794) / p->sR) * 51 / 50; //地本影在月球向径处的半径(角秒),式中51/50是大气厚度补偿
    p->eShadow2 = (cs_rEarA / p->mR*cs_rad + (959.63 + 8.794) / p->sR) * 51 / 50; //地半影在月球向径处的半径(角秒),式中51/50是大气厚度补偿
    p->mIll = XL_moonIll(T); //月亮被照面比例

    //中心食计算
    if (fabs(rad2rrad(p->mCJ - p->sCJ)) < 50.0 / 180 * cs_pi) {
        double P[3] = { p->mCJ, p->mCW, p->mR };
        double Q[3] = { p->sCJ, p->sCW, p->sR*cs_AU };
        struct csEphCoord pp;
        lineEar(&pp, P, Q, p->gst);
        p->zx_J = pp.J;
        p->zx_W = pp.W; //无解返回值是100
    }
    else p->zx_J = p->zx_W = 100;
}

MM_EXPORT_SXWNL
void
csEphMSC_toString(
    const struct csEphMSC* p,
    int fs,
    struct mmString* s)
{
    char str[32];

    mmString_Clear(s);
    mmString_Appends(s, "-------------------------------------------\n");
    mmString_Appends(s,  "平太阳  "); csJD_timeStr(p->pty, str); mmString_Appends(s, str);
    mmString_Appends(s, " 真太阳 " ); csJD_timeStr(p->zty, str); mmString_Appends(s, str);
    mmString_Appends(s, "\n");
    mmString_Appends(s,  "时差 "); m2fm(str, p->sc * 86400, 2, 1); mmString_Appends(s, str);
    mmString_Appends(s, " 月亮被照亮 "); toFixed(str, p->mIll * 100, 2); mmString_Appends(s, str);
    mmString_Appends(s, "% "); mmString_Appends(s, "\n");

    mmString_Appends(s, "-------------------------------------------\n");
    mmString_Appends(s, "表一         月亮            太阳\n");
    mmString_Appends(s, "视黄经 "); rad2str(str, p->mHJ, 0); mmString_Appends(s, str); 
    mmString_Appends(s, "  "); rad2str(str, p->sHJ, 0); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "视黄纬 "); rad2str(str, p->mHW, 0); mmString_Appends(s, str);
    mmString_Appends(s, "  "); rad2str(str, p->sHW, 0); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "视赤经 "); rad2str(str, p->mCJ, 1); mmString_Appends(s, str);
    mmString_Appends(s, "  "); rad2str(str, p->sCJ, 1); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "视赤纬 "); rad2str(str, p->mCW, 0); mmString_Appends(s, str);
    mmString_Appends(s, "  "); rad2str(str, p->sCW, 0); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "距离    "); toFixed(str, p->mR, 2); mmString_Appends(s, str);
    mmString_Appends(s, "千米    "); toFixed(str, p->sR, 8); mmString_Appends(s, str); mmString_Appends(s, "AU");
    mmString_Appends(s, "\n");

    mmString_Appends(s, "-------------------------------------------\n");
    mmString_Appends(s, "表二         月亮            太阳\n");
    mmString_Appends(s, "方位角 "); rad2str(str, p->mPJ, 0); mmString_Appends(s, str);
    mmString_Appends(s, "  "); rad2str(str, p->sPJ, 0); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "高度角 "); rad2str(str, p->mPW, 0); mmString_Appends(s, str);
    mmString_Appends(s, "  "); rad2str(str, p->sPW, 0); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "时角   "); rad2str(str, p->mShiJ, 0); mmString_Appends(s, str);
    mmString_Appends(s, "  "); rad2str(str, p->sShiJ, 0); mmString_Appends(s, str); mmString_Appends(s, "\n");
    mmString_Appends(s, "视半径(观测点) "); m2fm(str, p->mRad, 2, 0); mmString_Appends(s, str);
    mmString_Appends(s, "    "); m2fm(str, p->sRad, 2, 0); mmString_Appends(s, str);
    mmString_Appends(s, "\n");

    if (fs)
    {
        mmString_Appends(s, "-------------------------------------------\n");
        mmString_Appends(s, "力学时 "); csJD_JD2str(p->T + cs_J2000, str); mmString_Appends(s, str);
        mmString_Appends(s, "\n");
        mmString_Appends(s, "黄经章 "); toFixed(str, p->dL / cs_pi2 * 360 * 3600, 2); mmString_Appends(s, str);
        mmString_Appends(s, "\" ");
        mmString_Appends(s, "交角章 "); toFixed(str, p->dE / cs_pi2 * 360 * 3600, 2); mmString_Appends(s, str);
        mmString_Appends(s, "\" ");
        mmString_Appends(s, "\n");
        mmString_Appends(s, "ΔT= "); toFixed(str, p->dt * 86400, 1); mmString_Appends(s, str);
        mmString_Appends(s, "秒"); 
        mmString_Appends(s, " ε= "); rad2str(str, p->E, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\n");
        mmString_Appends(s, "-------------------------------------------\n");
    }
}

MM_EXPORT_SXWNL
void
csEphYsPL_Init(
    struct csEphYsPL* p)
{
    mmMemset(p, 0, sizeof(struct csEphYsPL));

    mmString_Init(&p->LX);
}

MM_EXPORT_SXWNL
void
csEphYsPL_Destroy(
    struct csEphYsPL* p)
{
    mmString_Destroy(&p->LX);

    mmMemset(p, 0, sizeof(struct csEphYsPL));
}

//已知t1时刻星体位置、速度，求x*x+y*y=r*r时,t的值
MM_EXPORT_SXWNL
double
csEphYsPL_lineT(
    struct csEphYsData* G,
    double v,
    double u,
    double r,
    int n)
{
    double b = G->y*v - G->x*u, A = u * u + v * v, B = u * b, C = b * b - r * r*v*v, D = B * B - A * C;
    if (D < 0) return 0;
    D = sqrt(D); if (!n) D = -D;
    return G->t + ((-B + D) / A - G->x) / v;
}

//日月黄经纬差转为日面中心直角坐标(用于月食)
MM_EXPORT_SXWNL
void
csEphYsPL_lecXY(
    double jd,
    struct csEphYsData* re)
{
    double T = jd / 36525;
    double zm[3] = { 0 }, zs[3] = { 0 };

    //=======太阳月亮黄道坐标========
    e_coord(zs, T, -1, -1, -1);   //地球坐标
    zs[0] = rad2mrad(zs[0] + cs_pi + gxc_sunLon(T));  zs[1] = -zs[1] + gxc_sunLat(T); //补上太阳光行差
    m_coord(zm, T, -1, -1, -1);   //月球坐标
    zm[0] = rad2mrad(zm[0] + gxc_moonLon(T));  zm[1] += gxc_moonLat(T);  //补上月球光行差就可以了

    //=======视半径=======
    re->e_mRad = cs_sMoon / zm[2]; //月亮地心视半径(角秒)
    re->eShadow  = (cs_rEarA / zm[2] * cs_rad - (959.63 - 8.794) / zs[2]) * 51 / 50; //地本影在月球向径处的半径(角秒),式中51/50是大气厚度补偿
    re->eShadow2 = (cs_rEarA / zm[2] * cs_rad + (959.63 + 8.794) / zs[2]) * 51 / 50; //地半影在月球向径处的半径(角秒),式中51/50是大气厚度补偿

    re->x = rad2rrad(zm[0] + cs_pi - zs[0]) * cos((zm[1] - zs[1]) / 2);
    re->y = zm[1] + zs[1];
    re->mr = re->e_mRad / cs_rad; re->er = re->eShadow / cs_rad; re->Er = re->eShadow2 / cs_rad;
    re->t = jd;
}

//月食的食甚计算(jd为近朔的力学时,误差几天不要紧)
MM_EXPORT_SXWNL
void
csEphYsPL_lecMax(
    struct csEphYsPL* p,
    double jd)
{
     //分别是:食甚,初亏,复圆,半影食始,半影食终,食既,生光
    mmMemset(p->lT, 0, sizeof(p->lT));
    p->sf = 0;
    mmString_Assigns(&p->LX, "");

    jd = XL_MS_aLon_t2(floor((jd - 4) / 29.5306)*cs_pi * 2 + cs_pi) * 36525; //低精度的朔(误差10分钟),与食甚相差10分钟左右

    struct csEphYsData g = { 0 }, G = { 0 };
    double u, v;

    //求极值(平均误差数秒)
    u = -18461 * sin(0.057109 + 0.23089571958*jd)*0.23090 / cs_rad; //月日黄纬速度差
    v = (XL_M_v(jd / 36525) - XL_E_v(jd / 36525)) / 36525; //月日黄经速度差
    csEphYsPL_lecXY(jd, &G);
    jd -= (G.y*u + G.x*v) / (u*u + v * v); //极值时间

    //精密求极值
    double dt = 60.0 / 86400.0;
    csEphYsPL_lecXY(jd, &G); csEphYsPL_lecXY(jd + dt, &g); //精密差分得速度,再求食甚
    u = (g.y - G.y) / dt;
    v = (g.x - G.x) / dt;
    dt = -(G.y*u + G.x*v) / (u*u + v * v); jd += dt; //极值时间

    //求直线到影子中心的最小值
    double x = G.x + dt * v, y = G.y + dt * u, rmin = sqrt(x*x + y * y);
    //注意,以上计算得到了极值及最小距rmin,但没有再次计算极值时刻的半径,对以下的判断造成一定的风险,必要的话可以再算一次。
    //不过必要性不很大，因为第一次极值计算已经很准确了,误差只有几秒

    //求月球与影子的位置关系
    if (rmin <= G.mr + G.er) { //食计算
        p->lT[1] = jd; //食甚
        mmString_Assigns(&p->LX, "偏");
        p->sf = (G.mr + G.er - rmin) / G.mr / 2; //食分

        p->lT[0] = csEphYsPL_lineT(&G, v, u, G.mr + G.er, 0); //初亏
        csEphYsPL_lecXY(p->lT[0], &g);
        p->lT[0] = csEphYsPL_lineT(&g, v, u, g.mr + g.er, 0); //初亏再算一次

        p->lT[2] = csEphYsPL_lineT(&G, v, u, G.mr + G.er, 1); //复圆
        csEphYsPL_lecXY(p->lT[2], &g);
        p->lT[2] = csEphYsPL_lineT(&g, v, u, g.mr + g.er, 1); //复圆再算一次
    }
    if (rmin <= G.mr + G.Er) { //半影食计算
        p->lT[3] = csEphYsPL_lineT(&G, v, u, G.mr + G.Er, 0); //半影食始
        csEphYsPL_lecXY(p->lT[3], &g);
        p->lT[3] = csEphYsPL_lineT(&g, v, u, g.mr + g.Er, 0); //半影食始再算一次

        p->lT[4] = csEphYsPL_lineT(&G, v, u, G.mr + G.Er, 1); //半影食终
        csEphYsPL_lecXY(p->lT[4], &g);
        p->lT[4] = csEphYsPL_lineT(&g, v, u, g.mr + g.Er, 1); //半影食终再算一次
    }
    if (rmin <= G.er - G.mr) { //全食计算
        mmString_Assigns(&p->LX, "全");
        p->lT[5] = csEphYsPL_lineT(&G, v, u, G.er - G.mr, 0); //食既
        csEphYsPL_lecXY(p->lT[5], &g);
        p->lT[5] = csEphYsPL_lineT(&g, v, u, g.er - g.mr, 0); //食既再算一次

        p->lT[6] = csEphYsPL_lineT(&G, v, u, G.er - G.mr, 1); //生光
        csEphYsPL_lecXY(p->lT[6], &g);
        p->lT[6] = csEphYsPL_lineT(&g, v, u, g.er - g.mr, 1); //生光再算一次
    }
}

MM_EXPORT_SXWNL
void
csEphRsData_Init(
    struct csEphRsData* p)
{
    mmMemset(p, 0, sizeof(struct csEphRsData));

    mmString_Init(&p->lx);
}

MM_EXPORT_SXWNL
void
csEphRsData_Destroy(
    struct csEphRsData* p)
{
    mmString_Destroy(&p->lx);

    mmMemset(p, 0, sizeof(struct csEphRsData));
}

MM_EXPORT_SXWNL
void
csEphVecFLAG_Init(
    struct csEphVecFLAG* p)
{
    mmVectorF64_Init(&p->L);
    p->f = 0;
    p->f2 = 0;
}

MM_EXPORT_SXWNL
void
csEphVecFLAG_Destroy(
    struct csEphVecFLAG* p)
{
    p->f2 = 0;
    p->f = 0;
    mmVectorF64_Destroy(&p->L);
}

MM_EXPORT_SXWNL
void
csEphVecFLAG_Reset(
    struct csEphVecFLAG* p)
{
    mmVectorF64_Reset(&p->L);
    p->f = 0;
    p->f2 = 0;
}

//快速日食搜索,jd为朔时间(J2000起算的儒略日数,不必很精确)
MM_EXPORT_SXWNL
void
ecFast(
    struct csEphRsData* re,
    double jd)
{
    double t, t2, t3, t4;
    double L, mB, mR, sR, vL, vB, vR;
    double W = floor((jd + 8) / 29.5306)*cs_pi * 2; //合朔时的日月黄经差

    //合朔时间计算,2000前+-4000年误差1小时以内，+-2000年小于10分钟
    t = (W + 1.08472) / 7771.37714500204; //平朔时间
    re->jd = re->jdSuo = t * 36525;

    t2 = t * t; t3 = t2 * t; t4 = t3 * t;
    L = (93.2720993 + 483202.0175273*t - 0.0034029*t2 - t3 / 3526000 + t4 / 863310000) / 180 * cs_pi;
    re->ac = 1; mmString_Assigns(&re->lx, "N");
    if (fabs(sin(L)) > 0.4) return; //一般大于21度已不可能

    t -= (-0.0000331*t*t + 0.10976 *cos(0.785 + 8328.6914*t)) / 7771;
    t2 = t * t;
    L = -1.084719 + 7771.377145013*t - 0.0000331*t2 +
        (22640 * cos(0.785 +  8328.6914*t + 0.000152*t2)
        + 4586 * cos(0.19  +  7214.063*t  - 0.000218*t2)
        + 2370 * cos(2.54  + 15542.754*t  - 0.000070*t2)
        +  769 * cos(3.1   + 16657.383*t)
        +  666 * cos(1.5   +   628.302*t)
        +  412 * cos(4.8   + 16866.93*t)
        +  212 * cos(4.1   -  1114.63*t)
        +  205 * cos(0.2   +  6585.76*t)
        +  192 * cos(4.9   + 23871.45*t)
        +  165 * cos(2.6   + 14914.45*t)
        +  147 * cos(5.5   -  7700.39*t)
        +  125 * cos(0.5   +  7771.38*t)
        +  109 * cos(3.9   +  8956.99*t)
        +   55 * cos(5.6   -  1324.18*t)
        +   45 * cos(0.9   + 25195.62*t)
        +   40 * cos(3.8   -  8538.24*t)
        +   38 * cos(4.3   + 22756.82*t)
        +   36 * cos(5.5   + 24986.07*t)
        - 6893 * cos(4.669257 +  628.3076*t)
        -   72 * cos(4.6261   + 1256.62*t)
        -   43 * cos(2.67823  +  628.31*t)*t
        +   21) / cs_rad;
    t += (W - L) / (7771.38
        - 914 * sin(0.7848 +  8328.691425*t + 0.0001523*t2)
        - 179 * sin(2.543  + 15542.7543*t)
        - 160 * sin(0.1874 +  7214.0629*t));
    re->jd = re->jdSuo = jd = t * 36525; //朔时刻

    //纬 52,15 (角秒)
    t2 = t * t / 10000; t3 = t2 * t / 10000;
    mB =
         18461 * cos(0.0571 +  8433.46616*t - 0.640*t2 -  1 * t3)
        + 1010 * cos(2.413  + 16762.1576 *t + 0.88 *t2 + 25 * t3)
        + 1000 * cos(5.440  -   104.7747 *t + 2.16 *t2 + 26 * t3)
        +  624 * cos(0.915  +  7109.2881 *t + 0    *t2 +  7 * t3)
        +  199 * cos(1.82   + 15647.529  *t - 2.8  *t2 - 19 * t3)
        +  167 * cos(4.84   -  1219.403  *t - 1.5  *t2 - 18 * t3)
        +  117 * cos(4.17   + 23976.220  *t - 1.3  *t2 +  6 * t3)
        +   62 * cos(4.8    + 25090.849  *t + 2    *t2 + 50 * t3)
        +   33 * cos(3.3    + 15437.980  *t + 2    *t2 + 32 * t3)
        +   32 * cos(1.5    +  8223.917  *t + 4    *t2 + 51 * t3)
        +   30 * cos(1.0    +  6480.986  *t + 0    *t2 +  7 * t3)
        +   16 * cos(2.5    -  9548.095  *t - 3    *t2 - 43 * t3)
        +   15 * cos(0.2    + 32304.912  *t + 0    *t2 + 31 * t3)
        +   12 * cos(4.0    +  7737.590  *t)
        +    9 * cos(1.9    + 15019.227  *t)
        +    8 * cos(5.4    +  8399.709  *t)
        +    8 * cos(4.2    + 23347.918  *t)
        +    7 * cos(4.9    -  1847.705  *t)
        +    7 * cos(3.8    - 16133.856  *t)
        +    7 * cos(2.7    + 14323.351  *t);
    mB /= cs_rad;

    //距 106, 23 (千米)
    mR = 385001
        + 20905 * cos(5.4971 +  8328.691425*t + 1.52 *t2 + 25 * t3)
        +  3699 * cos(4.900  +  7214.06287 *t - 2.18 *t2 - 19 * t3)
        +  2956 * cos(0.972  + 15542.75429 *t - 0.66 *t2 +  6 * t3)
        +   570 * cos(1.57   + 16657.3828  *t + 3.0  *t2 + 50 * t3)
        +   246 * cos(5.69   -  1114.6286  *t - 3.7  *t2 - 44 * t3)
        +   205 * cos(1.02   + 14914.4523  *t - 1    *t2 +  6 * t3)
        +   171 * cos(3.33   + 23871.4457  *t + 1    *t2 + 31 * t3)
        +   152 * cos(4.94   +  6585.761   *t - 2    *t2 - 19 * t3)
        +   130 * cos(0.74   -  7700.389   *t - 2    *t2 - 25 * t3)
        +   109 * cos(5.20   +  7771.377   *t)
        +   105 * cos(2.31   +  8956.993   *t + 1    *t2 + 25 * t3)
        +    80 * cos(5.38   -  8538.241   *t + 2.8  *t2 + 26 * t3)
        +    49 * cos(6.24   +   628.302   *t)
        +    35 * cos(2.7    + 22756.817   *t - 3    *t2 - 13 * t3)
        +    31 * cos(4.1    + 16171.056   *t - 1    *t2 +  6 * t3)
        +    24 * cos(1.7    +  7842.365   *t - 2    *t2 - 19 * t3)
        +    23 * cos(3.9    + 24986.074   *t + 5    *t2 + 75 * t3)
        +    22 * cos(0.4    + 14428.126   *t - 4    *t2 - 38 * t3)
        +    17 * cos(2.0    +  8399.679   *t);
    mR /= 6378.1366;

    t = jd / 365250; t2 = t * t; t3 = t2 * t;
    //误0.0002AU
    sR = 10001399 //日地距离
        + 167070 * cos(3.098464 +  6283.07585*t)
        +   1396 * cos(3.0552   + 12566.1517 *t)
        +  10302 * cos(1.10749  +  6283.07585*t)*t
        +    172 * cos(1.064    + 12566.152  *t)*t
        +    436 * cos(5.785    +  6283.076  *t)*t2
        +     14 * cos(4.27     +  6283.08   *t)*t3;
    sR *= 1.49597870691 / 6378.1366 * 10;

    //经纬速度
    t = jd / 36525;
    vL = 7771 //月日黄经差速度
        - 914 * sin(0.785 +  8328.6914*t)
        - 179 * sin(2.543 + 15542.7543*t)
        - 160 * sin(0.187 +  7214.0629*t);
    vB = -755 * sin(0.057 +  8433.4662*t) //月亮黄纬速度
        -  82 * sin(2.413 + 16762.1576*t);
    vR = -27299 * sin(5.497 +  8328.691425*t)
        -  4184 * sin(4.900 +  7214.06287*t)
        -  7204 * sin(0.972 + 15542.75429*t);
    vL /= 36525; vB /= 36525; vR /= 36525; //每日速度


    double gm = mR * sin(mB)*vL / sqrt(vB*vB + vL * vL), smR = sR - mR; //gm伽马值,smR日月距
    double mk = 0.2725076, sk = 109.1222;
    double f1 = (sk + mk) / smR, r1 = mk + f1 * mR; //tanf1半影锥角, r1半影半径
    double f2 = (sk - mk) / smR, r2 = mk - f2 * mR; //tanf2本影锥角, r2本影半径
    double b = 0.9972, Agm = fabs(gm), Ar2 = fabs(r2);
    double fh2 = mR - mk / f2, h = Agm < 1 ? sqrt(1 - gm * gm) : 0; //fh2本影顶点的z坐标
    double ls1, ls2, ls3, ls4;

    if (fh2 < h) mmString_Assigns(&re->lx, "T");
    else         mmString_Assigns(&re->lx, "A");

    ls1 = Agm - (b +  r1); if (fabs(ls1) < 0.016) re->ac = 0; //无食分界
    ls2 = Agm - (b + Ar2); if (fabs(ls2) < 0.016) re->ac = 0; //偏食分界
    ls3 = Agm - (b      ); if (fabs(ls3) < 0.016) re->ac = 0; //无中心食分界
    ls4 = Agm - (b - Ar2); if (fabs(ls4) < 0.016) re->ac = 0; //有中心食分界(但本影未全部进入)

    if      (ls1 > 0) mmString_Assigns(&re->lx, "N"); //无日食
    else if (ls2 > 0) mmString_Assigns(&re->lx, "P"); //偏食
    else if (ls3 > 0) mmString_Appends(&re->lx, "0"); //无中心
    else if (ls4 > 0) mmString_Appends(&re->lx, "1"); //有中心(本影未全部进入)
    else { //本影全进入
        if (fabs(fh2 - h) < 0.019) re->ac = 0;
        if (fabs(fh2) < h) {
            double dr = vR * h / vL / mR;
            double H1 = mR - dr - mk / f2;  //入点影锥z坐标
            double H2 = mR + dr - mk / f2;  //出点影锥z坐标
            if (H1 > 0) mmString_Assigns(&re->lx, "H3");      //环全全
            if (H2 > 0) mmString_Assigns(&re->lx, "H2");      //全全环
            if (H1 > 0 && H2 > 0) mmString_Assigns(&re->lx, "H"); //环全环
            if (fabs(H1) < 0.019) re->ac = 0;
            if (fabs(H2) < 0.019) re->ac = 0;
        }
    }
}

MM_EXPORT_SXWNL
void
csEphFeature_Init(
    struct csEphFeature* p)
{
    mmMemset(p, 0, sizeof(struct csEphFeature));

    mmString_Init(&p->lx);

    mmVectorF64_Init(&p->p1);
    mmVectorF64_Init(&p->p2);
    mmVectorF64_Init(&p->p3);
    mmVectorF64_Init(&p->p4);
    mmVectorF64_Init(&p->q1);
    mmVectorF64_Init(&p->q2);
    mmVectorF64_Init(&p->q3);
    mmVectorF64_Init(&p->q4);

    csEphVecFLAG_Init(&p->L0);
    csEphVecFLAG_Init(&p->L1);
    csEphVecFLAG_Init(&p->L2);
    csEphVecFLAG_Init(&p->L3);
    csEphVecFLAG_Init(&p->L4);
    csEphVecFLAG_Init(&p->L5);
    csEphVecFLAG_Init(&p->L6);
}

MM_EXPORT_SXWNL
void
csEphFeature_Destroy(
    struct csEphFeature* p)
{
    csEphVecFLAG_Destroy(&p->L6);
    csEphVecFLAG_Destroy(&p->L5);
    csEphVecFLAG_Destroy(&p->L4);
    csEphVecFLAG_Destroy(&p->L3);
    csEphVecFLAG_Destroy(&p->L2);
    csEphVecFLAG_Destroy(&p->L1);
    csEphVecFLAG_Destroy(&p->L0);

    mmVectorF64_Destroy(&p->q4);
    mmVectorF64_Destroy(&p->q3);
    mmVectorF64_Destroy(&p->q2);
    mmVectorF64_Destroy(&p->q1);
    mmVectorF64_Destroy(&p->p4);
    mmVectorF64_Destroy(&p->p3);
    mmVectorF64_Destroy(&p->p2);
    mmVectorF64_Destroy(&p->p1);

    mmString_Destroy(&p->lx);

    mmMemset(p, 0, sizeof(struct csEphFeature));
}

MM_EXPORT_SXWNL
void
csEphFeature_ResetVector(
    struct csEphFeature* p)
{
    mmVectorF64_Reset(&p->p1);
    mmVectorF64_Reset(&p->p2);
    mmVectorF64_Reset(&p->p3);
    mmVectorF64_Reset(&p->p4);
    mmVectorF64_Reset(&p->q1);
    mmVectorF64_Reset(&p->q2);
    mmVectorF64_Reset(&p->q3);
    mmVectorF64_Reset(&p->q4);

    csEphVecFLAG_Reset(&p->L0);
    csEphVecFLAG_Reset(&p->L1);
    csEphVecFLAG_Reset(&p->L2);
    csEphVecFLAG_Reset(&p->L3);
    csEphVecFLAG_Reset(&p->L4);
    csEphVecFLAG_Reset(&p->L5);
    csEphVecFLAG_Reset(&p->L6);
}

MM_EXPORT_SXWNL
void
csEphJIEX2_Init(
    struct csEphJIEX2* p)
{
    mmVectorF64_Init(&p->p1);
    mmVectorF64_Init(&p->p2);
    mmVectorF64_Init(&p->p3);
}

MM_EXPORT_SXWNL
void
csEphJIEX2_Destroy(
    struct csEphJIEX2* p)
{
    mmVectorF64_Destroy(&p->p3);
    mmVectorF64_Destroy(&p->p2);
    mmVectorF64_Destroy(&p->p1);
}

MM_EXPORT_SXWNL
void
csEphRsGS_Init(
    struct csEphRsGS* p)
{
    mmVectorF64_Init(&p->Zs);
    p->Zdt = 0.04;    //插值点之间的时间间距
    p->Zjd = 0;       //插值表中心时间
    p->dT = 0;        //deltatT
    p->tanf1 = 0.0046;//半影锥角
    p->tanf2 = 0.0045;//本影锥角
    p->srad = 0.0046; //太阳视半径
    p->bba = 1;       //贝圆极赤比
    p->bhc = 0;       //黄交线与赤交线的夹角简易作图用
    p->dyj = 23500;   //地月距
}

MM_EXPORT_SXWNL
void
csEphRsGS_Destroy(
    struct csEphRsGS* p)
{
    p->dyj = 23500;   //地月距
    p->bhc = 0;       //黄交线与赤交线的夹角简易作图用
    p->bba = 1;       //贝圆极赤比
    p->srad = 0.0046; //太阳视半径
    p->tanf2 = 0.0045;//本影锥角
    p->tanf1 = 0.0046;//半影锥角
    p->dT = 0;        //deltatT
    p->Zjd = 0;       //插值表中心时间
    p->Zdt = 0.04;    //插值点之间的时间间距
    mmVectorF64_Destroy(&p->Zs);
}

//创建插值表(根数表)
MM_EXPORT_SXWNL
void
csEphRsGS_initialize(
    struct csEphRsGS* p,
    double jd,
    int n)
{
    double zd[2];
    double E;
    double T;
    double S[3], M[3], B[3];
    double xyz[3];
    double* a;
    int i, k;
    int d;
    if (suoN(jd) == suoN(p->Zjd) && p->Zs.size == n * 9) return;
    p->Zs.size = 0;
    p->Zjd = jd = XL_MS_aLon_t2(suoN(jd)*cs_pi * 2) * 36525; //低精度的朔(误差10分钟)
    p->dT = dt_T(jd); //deltat T

    nutation2(zd, jd / 36525); //章动
    E = hcjj(jd / 36525) + zd[1]; //黄赤交角

    mmVectorF64_AllocatorMemory(&p->Zs, n * 9);
    p->Zs.size = n * 9;
    a = p->Zs.arrays;
    for (i = 0; i < n; i++) { //插值点范围不要超过360度(约1个月)
        T = (p->Zjd + (i - n / 2.0 + 0.5)*p->Zdt) / 36525;

        if (n == 7) { e_coord(S, T, -1, -1, -1); m_coord(M, T, -1,  -1,  -1); } //地球坐标及月球坐标,全精度
        if (n == 3) { e_coord(S, T, 65, 65, 65); m_coord(M, T, -1, 150, 150); } //中精度
        if (n == 2) { e_coord(S, T, 20, 20, 20); m_coord(M, T, 30,  30,  30); } //低精度


        S[0] = S[0] + zd[0] + gxc_sunLon(T) + cs_pi;  S[1] = -S[1] + gxc_sunLat(T);  //补上太阳光行差及章动
        M[0] = M[0] + zd[0] + gxc_moonLon(T);         M[1] =  M[1] + gxc_moonLat(T); //补上月球光行差及章动
        llrConv(S, S, E);  llrConv(M, M, E); S[2] *= cs_AU; //转为赤道坐标
        if (i && S[0] < a[0]) S[0] += cs_pi2;  //确保插值数据连续
        if (i && M[0] < a[3]) M[0] += cs_pi2;  //确保插值数据连续

        k = i * 9;
        a[k + 0] = S[0]; a[k + 1] = S[1]; a[k + 2] = S[2]; //存入插值表
        a[k + 3] = M[0]; a[k + 4] = M[1]; a[k + 5] = M[2];

        //贝塞尔坐标的z轴坐标计算,得到a[k+6,7,8]交点赤经,贝赤交角,真恒星时
        llr2xyz(S, S); llr2xyz(M, M);
        xyz[0] = S[0] - M[0]; xyz[1] = S[1] - M[1]; xyz[2] = S[2] - M[2];
        xyz2llr(B, xyz);
        B[0] = cs_pi / 2 + B[0];
        B[1] = cs_pi / 2 - B[1];
        if (i && B[0] < a[6]) B[0] += cs_pi2; //确保插值数据连续

        a[k + 6] = B[0]; a[k + 7] = B[1]; a[k + 8] = pGST(T * 36525 - p->dT, p->dT) + zd[0] * cos(E); //真恒星时
    }
    //一些辅助参数的计算
    d = (int)(p->Zs.size - 9);
    p->dyj = (a[2] + a[d + 2] - a[5] - a[d + 5]) / 2 / cs_rEar; //地月平均距离
    p->tanf1 = (cs_k0 + cs_k ) / p->dyj; //tanf1半影锥角
    p->tanf2 = (cs_k0 - cs_k2) / p->dyj; //tanf2本影锥角
    p->srad = cs_k0 / ((a[2] + a[d + 2]) / 2 / cs_rEar);
    p->bba = sin((a[1] + a[d + 1]) / 2);
    p->bba = cs_ba * (1 + (1 - cs_ba2)*p->bba*p->bba / 2);
    p->bhc = -atan(tan(E)*sin((a[6] + a[d + 6]) / 2)); //黄交线与赤交线的夹角
}

//日月坐标快速计算(贝赛尔插值法),计算第p个根数开始的m个根数
MM_EXPORT_SXWNL
void
csEphRsGS_chazhi(
    const struct csEphRsGS* p,
    double jd,
    int xt,
    double z[3])
{
    int d = xt * 3, m = 3; //计算第p个根数开始的m个根数
    int i, N = (int)p->Zs.size / 9;
    double* B = p->Zs.arrays;
    int w = (int)(p->Zs.size / N); //每节点个数
    double t = (jd - p->Zjd) / p->Zdt + N / 2.0 - 0.5; //相对于第一点的时间距离

    if (N == 2) { for (i = 0; i < m; i++, d++) z[i] = B[d] + (B[d + w] - B[d])*t; return; }
    int c = (int)floor(t + 0.5); if (c <= 0) c = 1; if (c > N - 2) c = N - 2; //确定c,并对超出范围的处理
    t -= c; d += c * w; //c插值中心,t为插值因子,t再转为插值中心在数据中的位置
    for (i = 0; i < m; i++, d++)
        z[i] = B[d] + (B[d + w] - B[d - w] + (B[d + w] + B[d - w] - B[d] * 2)*t) * t / 2;
}

//传回值可能超过360度
MM_EXPORT_SXWNL
void
csEphRsGS_sun(
    const struct csEphRsGS* p,
    double jd,
    double z[3])
{
    csEphRsGS_chazhi(p, jd, 0, z); 
} 

MM_EXPORT_SXWNL
void
csEphRsGS_moon(
    const struct csEphRsGS* p,
    double jd,
    double z[3])
{
    csEphRsGS_chazhi(p, jd, 1, z); 
}

MM_EXPORT_SXWNL
void
csEphRsGS_bse(
    const struct csEphRsGS* p,
    double jd,
    double z[3])
{
    csEphRsGS_chazhi(p, jd, 2, z); 
}

//赤道转贝塞尔坐标
MM_EXPORT_SXWNL
void
csEphRsGS_cd2bse(
    double r[3],
    const double z[3],
    const double I[3])
{
    r[0] = z[0] - I[0]; r[1] = z[1]; r[2] = z[2];
    llrConv(r, r, -I[1]);
    llr2xyz(r, r);
}

//贝塞尔转赤道坐标
MM_EXPORT_SXWNL
void
csEphRsGS_bse2cd(
    double r[3],
    const double z[3],
    const double I[3])
{
    xyz2llr(r, z);
    llrConv(r, r, I[1]);
    r[0] = rad2mrad(r[0] + I[0]);
}

//贝赛尔转地标(p点到原点连线与地球的交点,z为p点直角坐标),f=1时把地球看成椭球
MM_EXPORT_SXWNL
void
csEphRsGS_bse2db(
    double r[3],
    const double z[3],
    const double I[3],
    int f)
{
    xyz2llr(r, z);
    llrConv(r, r, I[1]);
    r[0] = rad2rrad(r[0] + I[0] - I[2]);
    if (f) r[1] = atan(tan(r[1]) / cs_ba2);
}

//贝赛尔转地标(过p点垂直于基面的线与地球的交点,p坐标为(x,y,任意z)),f=1时把地球看成椭球
MM_EXPORT_SXWNL
void
csEphRsGS_bseXY2db(
    double r[2],
    double x, double y,
    const double I[3],
    int f)
{
    struct csEphCoord F;
    double b = f ? cs_ba : 1;
    lineEar2(&F, x, y, 2, x, y, 0, b, 1, I);//求中心对应的地标
    r[0] = F.J;
    r[1] = F.W;
}

//月亮的贝塞尔坐标
MM_EXPORT_SXWNL
void
csEphRsGS_bseM(
    const struct csEphRsGS* p,
    double a[3],
    double jd)
{
    double z1[3], z2[3];
    csEphRsGS_chazhi(p, jd, 1, z1);
    csEphRsGS_chazhi(p, jd, 2, z2);
    csEphRsGS_cd2bse(a, z1, z2);
    a[0] /= cs_rEar; a[1] /= cs_rEar; a[2] /= cs_rEar;
}

//以下计算日食总体情况

//地球上一点的速度，用贝塞尔坐标表达，s为贝赤交角
MM_EXPORT_SXWNL
void
csEphRsGS_Vxy(
    struct csEphVXY* r,
    double x, double y,
    double s,
    double vx, double vy)
{
    double h = 1 - x * x - y * y;
    if (h < 0) h = 0;  //越界置0，使速度场连续，置零有助于迭代时单向收敛
    else       h = sqrt(h);
    r->vx = cs_pi2 * (sin(s)*h - cos(s)*y);
    r->vy = cs_pi2 * x*cos(s);
    r->Vx = vx - r->vx;
    r->Vy = vy - r->vy;
    r->V = sqrt(r->Vx*r->Vx + r->Vy*r->Vy);
}

//rm,rs单位千米
MM_EXPORT_SXWNL
void
csEphRsGS_rSM(
    const struct csEphRsGS* p,
    struct csEphRSM* re,
    double mR)
{
    re->r1 = cs_k  + p->tanf1*mR; //半影半径
    re->r2 = cs_k2 - p->tanf2*mR; //本影半径
    re->ar2 = fabs(re->r2);
    re->sf = cs_k2 / mR / cs_k0 * (p->dyj + mR); //食分
}

//求切入点
MM_EXPORT_SXWNL
void
csEphRsGS_qrd(
    const struct csEphRsGS* p,
    double re[3],
    double jd,
    double dx, double dy,
    int fs)
{
    struct csEphRSM B;
    double M[3];
    double z[3], v[3];
    double ba2 = p->bba*p->bba;
    csEphRsGS_bseM(p, M, jd);
    double x = M[0], y = M[1];
    csEphRsGS_rSM(p, &B, M[2]);
    double r = 0; if (fs == 1) r = B.r1;
    double d = 1 - (1 / ba2 - 1)*y*y / (x*x + y * y) / 2 + r;
    double t = (d*d - x * x - y * y) / (dx*x + dy * y) / 2;
    x += t * dx; y += t * dy; jd += t;

    double c = (1 - ba2)*r*x*y / d / d / d;
    x += c * y;
    y -= c * x;
    csEphRsGS_bse(p, jd, z);
    v[0] = x / d; v[1] = y / d; v[2] = 0;
    csEphRsGS_bse2db(re, v, z, 1);
    //re[0] +=0.275/radd; //转为deltatT为66秒的历书经度
    re[2] = jd;
}

//日食的基本特征
MM_EXPORT_SXWNL
void
csEphRsGS_feature(
    const struct csEphRsGS* p,
    struct csEphFeature* re,
    double jd)
{
    // 存疑: 这里舍弃了传入参数有点奇怪, 
    //       但看使用情况其实用之前已经提前init初始化了p->Zjd
    //       可能这个jd参数确实无用
    jd = p->Zjd; //低精度的朔(误差10分钟)

    struct csEphCoord F;
    struct csEphRSM Bc, Bp, B2, B3;
    double dt, t2, t3, t4, t5, t6;

    double d[3];
    double lls[3];
    double a[3], b[3], c[3];
    double tg = 0.04;
    double ls;

    csEphRsGS_bseM(p, a, jd - tg);
    csEphRsGS_bseM(p, b, jd);
    csEphRsGS_bseM(p, c, jd + tg);
    double vx = (c[0] - a[0]) / tg / 2;
    double vy = (c[1] - a[1]) / tg / 2;
    double vz = (c[2] - a[2]) / tg / 2;
    double ax = (c[0] + a[0] - 2 * b[0]) / tg / tg;
    double ay = (c[1] + a[1] - 2 * b[1]) / tg / tg;
    double v = sqrt(vx*vx + vy * vy), v2 = v * v;

    //影轴在贝塞尔面扫线的特征参数
    re->jdSuo = jd;    //朔
    re->dT = p->dT;    //deltat T
    re->ds = p->bhc;   //黄交线与赤交线的夹角
    re->vx = vx;       //影速x
    re->vy = vy;       //影速y
    re->ax = ax;
    re->ay = ay;
    re->v = v;
    re->k = vy / vx;   //斜率

    double t0 = -(b[0] * vx + b[1] * vy) / v2;
    re->jd = jd + t0;                      //中点时间
    re->xc = b[0] + vx * t0;               //中点坐标x
    re->yc = b[1] + vy * t0;               //中点坐标y
    re->zc = b[2] + vz * t0 - 1.37*t0*t0;  //中点坐标z
    re->D = (vx*b[1] - vy * b[0]) / v;
    re->d = fabs(re->D);                   //直线到圆心的距离
    csEphRsGS_bse(p, re->jd, re->I);       //中心点的贝塞尔z轴的赤道坐标及恒星时，(J,W,g)

    //影轴交点判断
    lineEar2(&F, re->xc, re->yc, 2, re->xc, re->yc, 0, cs_ba, 1, re->I);//求中心对应的地标
    //四个关键点的影子半径计算
    csEphRsGS_rSM(p, &B3, re->zc);
    Bc = Bp = B2 = B3; //中点处的影子半径
    if (F.W != 100)  csEphRsGS_rSM(p, &Bp, re->zc - F.R2);
    if (re->d < 1) {
        dt = sqrt(1 - re->d*re->d) / v;  t2 = t0 - dt; t3 = t0 + dt; //中心始终参数
        csEphRsGS_rSM(p, &B2, t2*vz + b[2] - 1.37*t2*t2);   //中心线始影半径
        csEphRsGS_rSM(p, &B3, t3*vz + b[2] - 1.37*t3*t3);   //中心线终影半径
    }
    
    ls = 1;         dt = 0; if (re->d < ls) dt = sqrt(ls*ls - re->d*re->d) / v;
    t2 = t0 - dt; t3 = t0 + dt; //偏食始终参数,t2,t3
    
    ls = 1 + Bc.r1; dt = 0; if (re->d < ls) dt = sqrt(ls*ls - re->d*re->d) / v;
    t4 = t0 - dt; t5 = t0 + dt; //偏食始终参数,t4,t5
    
    t6 = -b[0] / vx; //视午参数l6
    if (re->d < 1) {
        csEphRsGS_qrd(p, re->gk1, t2 + jd, vx, vy, 0); //中心始
        csEphRsGS_qrd(p, re->gk2, t3 + jd, vx, vy, 0); //中心终
    }
    else {
        re->gk1[0] = 0; re->gk1[1] = 0; re->gk1[2] = 0;
        re->gk2[0] = 0; re->gk2[1] = 0; re->gk2[2] = 0;
    }
    csEphRsGS_qrd(p, re->gk3, t4 + jd, vx, vy, 1); //偏食始
    csEphRsGS_qrd(p, re->gk4, t5 + jd, vx, vy, 1); //偏食终

    csEphRsGS_bse(p, t6 + jd, d);
    csEphRsGS_bseXY2db(re->gk5, t6*vx + b[0], t6*vy + b[1], d, 1);  re->gk5[2] = t6 + jd; //地方视午日食

    //日食类型、最大食地标、食分、太阳地平坐标
    if (F.W == 100) { //无中心线
        //最大食地标及时分
        d[0] = re->xc; d[0] = re->yc; d[0] = 0;
        csEphRsGS_bse2db(lls, d, re->I, 0); re->zxJ = lls[0]; re->zxW = lls[1]; //最大食地标
        re->sf = (Bc.r1 - (re->d - 0.9972)) / (Bc.r1 - Bc.r2); //0.9969是南北极区的平半径
        //类型判断
        if      (re->d > 0.9972 + Bc.r1 ) { mmString_Assigns(&re->lx, "N"); } //无食,半影没有进入
        else if (re->d > 0.9972 + Bc.ar2) { mmString_Assigns(&re->lx, "P"); } //偏食,本影没有进入
        else { if (Bc.sf < 1) mmString_Assigns(&re->lx, "A0"); else mmString_Assigns(&re->lx, "T0"); } //中心线未进入,本影部分进入(无中心，所以只是部分地入)
    }
    else { //有中心线
        //最大食地标及时分
        re->zxJ = F.J; re->zxW = F.W;  //最大食地标
        re->sf = Bp.sf; //食分
        //类型判断
        if (re->d > 0.9966 - Bp.ar2) { if (Bp.sf < 1) mmString_Assigns(&re->lx, "A1"); else mmString_Assigns(&re->lx, "T1"); } //中心进入,但本影没有完全进入
        else { //本影全进入有中心日食
            if (Bp.sf >= 1) {
                mmString_Assigns(&re->lx, "H");
                if (B2.sf > 1) mmString_Assigns(&re->lx, "H2"); //全环食,全始
                if (B3.sf > 1) mmString_Assigns(&re->lx, "H3"); //全环食,全终
                if (B2.sf > 1 && B3.sf > 1) mmString_Assigns(&re->lx, "T"); //全食
            }
            else mmString_Assigns(&re->lx, "A"); //环食
        }
    }
    csEphRsGS_sun(p, re->jd, d);
    CD2DP(re->Sdp, d, re->zxJ, re->zxW, re->I[2]);  //太阳在中心点的地平坐标

    //食带宽度和时延
    if (F.W != 100) {
        struct csEphVXY llls;
        re->dw = fabs(2 * Bp.r2*cs_rEar) / sin(re->Sdp[1]); //食带宽度
        csEphRsGS_Vxy(&llls, re->xc, re->yc, re->I[1], re->vx, re->vy); //求地表影速
        re->tt = 2 * fabs(Bp.r2) / llls.V; //时延
    }
    else re->dw = re->tt = 0;
}

//界线图
MM_EXPORT_SXWNL
void
csEphRsGS_push(
    const double z[3],
    struct mmVectorF64* p)
{
    //经度改为东经为正,所以有个负号
    mmVectorF64_PushBack(p, z[0]);
    mmVectorF64_PushBack(p, z[1]);
} 

//数据元素复制
MM_EXPORT_SXWNL
void
csEphRsGS_elmCpy(
    struct mmVectorF64* a, int n,
    struct mmVectorF64* b, int m)
{
    if (!b->size) return;
    if (n == -2) n = (int)a->size;
    if (m == -2) m = (int)b->size;
    if (n == -1) n = (int)a->size - 2;
    if (m == -1) m = (int)b->size - 2;
    a[n] = b[m]; a[n + 1] = b[m + 1];
}

//vx0,vy0为影足速度(也是整个影子速度),h=1计算北界,h=-1计算南界
MM_EXPORT_SXWNL
void
csEphRsGS_nanbei(
    double re[4],
    const double M[3],
    double vx0, double vy0,
    double h,
    double r,
    const double I[3])
{
    struct csEphCoord p;
    double x = M[0] - vy0 / vx0 * r*h, y = M[1] + h * r, z, i;
    double vx, vy, v, sinA, cosA, js = 0;
    double X, Y;
    // 消除可能的未初始化警告
    sinA = 0; cosA = 0;
    for (i = 0; i < 3; i++) {
        z = 1 - x * x - y * y;
        if (z < 0) { if (js) break;  z = 0; js++; } //z小于0则置0，如果两次小于0，可能不收敛造成的，故不再迭代了
        z = sqrt(z);
        x -= (x - M[0])*z / M[2];
        y -= (y - M[1])*z / M[2];
        vx = vx0 - cs_pi2 * (sin(I[1])*z - cos(I[1])*y);
        vy = vy0 - cs_pi2 * cos(I[1])*x;
        v = sqrt(vx*vx + vy * vy);
        sinA = h * vy / v; cosA = h * vx / v;
        x = M[0] - r * sinA; y = M[1] + r * cosA;
    }
    X = M[0] - cs_k * sinA; Y = M[1] + cs_k * cosA;
    lineEar2(&p, X, Y, M[2], x, y, 0, cs_ba, 1, I);
    re[0] = p.J;
    re[1] = p.W;
    re[2] = x;
    re[3] = y;
}

//vx0,vy0为影足速度(也是整个影子速度),h=1计算北界,h=-1计算南界
MM_EXPORT_SXWNL
void
csEphRsGS_mQie(
    const struct csEphRsGS* q,
    const double M[3],
    double vx0, double vy0,
    double h,
    double r,
    const double I[3],
    struct csEphVecFLAG* A)
{
    double p[4];
    csEphRsGS_nanbei(p, M, vx0, vy0, h, r, I);
    if (!A->f2) A->f2 = 0;   A->f = p[1] == 100 ? 0 : 1; //记录有无解
    if (A->f2 != A->f) { //补线头线尾
        struct csEphNode g;
        lineOvl(&g, p[2], p[3], vx0, vy0, 1, q->bba);
        double dj;
        double F[3];
        if (g.n) {
            double d[3];
            if (A->f)
            {
                dj = g.R2;
                F[0] = g.B[0]; F[1] = g.B[1]; F[2] = g.B[2];
            }
            else
            {
                dj = g.R1;
                F[0] = g.A[0]; F[1] = g.A[1]; F[2] = g.A[2];
            }
            F[2] = 0;
            double I2[3] = { I[0], I[1], I[2] - dj / sqrt(vx0*vx0 + vy0 * vy0)*6.28 };  //也可以不重算计算恒星时，直接用I[2]代替，但线头不会严格落在日出日没食甚线上
            csEphRsGS_bse2db(d, F, I2, 1);
            csEphRsGS_push(d, &A->L);//有解补线头
        }
    }
    A->f2 = A->f; //记录上次有无解

    if (p[1] != 100) csEphRsGS_push(p, &A->L);
}

//日出日没食甚
MM_EXPORT_SXWNL
int
csEphRsGS_mDian(
    const struct csEphRsGS* q,
    const double M[3],
    double vx0, double vy0,
    int AB,
    double r,
    const double I[3],
    struct mmVectorF64* A)
{
    double R;
    struct csEphNode p;
    struct csEphVXY c;
    double a[3] = { M[0], M[1], M[2] };
    int i;
    // 消除可能的未初始化警告
    R = 0;
    for (i = 0; i < 2; i++) { //迭代求交点
        csEphRsGS_Vxy(&c, a[0], a[1], I[1], vx0, vy0);
        lineOvl(&p, M[0], M[1], c.Vy, -c.Vx, 1, q->bba);
        if (!p.n) break;
        if (AB) 
        {
            a[0] = p.A[0]; a[1] = p.A[1]; a[2] = p.A[2];
            R = p.R1;
        }
        else
        {
            a[0] = p.B[0]; a[1] = p.B[1]; a[2] = p.B[2];
            R = p.R2;
        }
    }
    if (p.n && R <= r) { //有交点
        double d[3] = { a[0], a[1], 0 };
        csEphRsGS_bse2db(a, d, I, 1); //转为地标
        csEphRsGS_push(a, A); //保存第一食甚线A或B根
        return 1;
    }
    return 0;
}

//日出日没的初亏食甚复圆线，南北界线等
MM_EXPORT_SXWNL
void
csEphRsGS_jieX(
    struct csEphRsGS* q,
    double jd,
    struct csEphFeature* re)
{
    const double Zero[3] = { 0 };
    int i;
    double M[3], I[3];
    double pp[2];
    double lls[3];
    double d[3];
    struct csEphRSM B;
    struct csEphNode p;
    struct csEphNode ls;
    //var i, p, ls;
    csEphRsGS_feature(q, re, jd);  //求特征参数

    csEphFeature_ResetVector(re);
    //re.p1 = new Array(), re.p2 = new Array(), re.p3 = new Array(), re.p4 = new Array();
    //re.q1 = new Array(), re.q2 = new Array(), re.q3 = new Array(), re.q4 = new Array();
    //re.L1 = new Array(), re.L2 = new Array(), re.L3 = new Array(), re.L4 = new Array();
    //re.L5 = new Array(), re.L6 = new Array(); //0.5食分线
    //re.L0 = new Array(); //中心线

    double T = 1.7*1.7 - re->d*re->d; if (T < 0) T = 0; T = sqrt(T) / re->v + 0.01;
    double t = re->jd - T, N = 400, dt = 2 * T / N;

    int n1 = 0, n4 = 0; //n1切入时序

    //对日出日没食甚线预置一个点
    struct mmVectorF64* Ua = &re->q1;
    struct mmVectorF64* Ub = &re->q2;
    csEphRsGS_push(Zero, &re->q2); csEphRsGS_push(Zero, &re->q3); csEphRsGS_push(Zero, &re->q4);

    for (i = 0; i <= N; i++, t += dt) {
        double vx = re->vx + re->ax*(t - re->jdSuo);
        double vy = re->vy + re->ay*(t - re->jdSuo);
        csEphRsGS_bseM(q, M, t);    //此刻月亮贝塞尔坐标(其x和y正是影足)
        csEphRsGS_rSM(q, &B, M[2]);  //本半影等
        double r = B.r1;            //半影半径
        csEphRsGS_bse(q, t, I);     //贝塞尔坐标参数

        cirOvl(&p, 1, q->bba, r, M[0], M[1]); //求椭圆与圆交点
        if (n1 % 2) { if (!p.n) n1++; }
        else { if (p.n) n1++; }
        if (p.n) { //有交点
            p.A[2] = p.B[2] = 0; csEphRsGS_bse2db(p.A, p.A, I, 1); csEphRsGS_bse2db(p.B, p.B, I, 1); //转为地标
            if (n1 == 1) { csEphRsGS_push(p.A, &re->p1); csEphRsGS_push(p.B, &re->p2); }//保存第一亏圆界线
            if (n1 == 3) { csEphRsGS_push(p.A, &re->p3); csEphRsGS_push(p.B, &re->p4); }//保存第二亏圆界线
        }

        //日出日没食甚线
        if (!csEphRsGS_mDian(q, M, vx, vy, 0, r, I, Ua)) { if (Ua->size > 0) Ua = &re->q3; };
        if (!csEphRsGS_mDian(q, M, vx, vy, 1, r, I, Ub)) { if (Ub->size > 2) Ub = &re->q4; };
        if (t > re->jd) {
            if (Ua->size == 0) Ua = &re->q3;
            if (Ub->size == 2) Ub = &re->q4;
        }

        //求中心线
        csEphRsGS_bseXY2db(pp, M[0], M[1], I, 1);
        if ((pp[1] != 100 && n4 == 0) || (pp[1] == 100 && n4 == 1)) { //从无交点跳到有交点或反之
           lineOvl(&ls, M[0], M[1], vx, vy, 1, q->bba);
           double dj;
           if (n4 == 0)
           {
               dj = ls.R2;
               lls[0] = ls.B[0]; lls[1] = ls.B[1]; lls[2] = ls.B[2]; //首坐标
           }
           else
           {
               dj = ls.R1;
               lls[0] = ls.A[0]; lls[1] = ls.A[1]; lls[2] = ls.A[2]; //末坐标
           }
            lls[2] = 0;
            double I2[3] = { I[0], I[1], I[2] - dj / sqrt(vx*vx + vy * vy)*6.28 };  //也可以不重算计算恒星时，直接用I[2]代替，但线头不会严格落在日出日没食甚线上
            csEphRsGS_bse2db(d, lls, I2, 1);
            csEphRsGS_push(d, &re->L0.L);
            n4++;
        }
        if (pp[1] != 100) csEphRsGS_push(pp, &re->L0.L); //保存中心线

        //南北界
        csEphRsGS_mQie(q, M, vx, vy, +1, r, I, &re->L1); //半影北界
        csEphRsGS_mQie(q, M, vx, vy, -1, r, I, &re->L2); //半影南界
        csEphRsGS_mQie(q, M, vx, vy, +1, B.r2, I, &re->L3); //本影北界
        csEphRsGS_mQie(q, M, vx, vy, -1, B.r2, I, &re->L4); //本影南界
        csEphRsGS_mQie(q, M, vx, vy, +1, (r + B.r2) / 2, I, &re->L5); //0.5半影北界
        csEphRsGS_mQie(q, M, vx, vy, -1, (r + B.r2) / 2, I, &re->L6); //0.5半影南界
    }

    //日出日没食甚线的线头连接
    csEphRsGS_elmCpy(&re->q3, 0, &re->q1, -1); //连接q1和a3,单边界必须
    csEphRsGS_elmCpy(&re->q4, 0, &re->q2, -1); //连接q2和a4,单边界必须

    csEphRsGS_elmCpy(&re->q1, -2, &re->L1.L, 0); //半影北界线西端
    csEphRsGS_elmCpy(&re->q2, -2, &re->L2.L, 0); //半影南界线西端
    csEphRsGS_elmCpy(&re->q3, 0, &re->L1.L, -1); //半影北界线东端
    csEphRsGS_elmCpy(&re->q4, 0, &re->L2.L, -1); //半影南界线东端

    csEphRsGS_elmCpy(&re->q2, 0, &re->q1, 0);
    csEphRsGS_elmCpy(&re->q3, -2, &re->q4, -1);
}

//jd力学时
MM_EXPORT_SXWNL
void
csEphRsGS_jieX2(
    struct csEphRsGS* q,
    double jd,
    struct csEphJIEX2* re)
{
    double S[3], M[3], I[3];
    double Z;
    double s, x, y, X, Y;
    double cosS, sinS;
    double d[3], v[3];
    double pp[3];
    struct csEphRSM B;
    struct csEphCoord p;
    int i;

    struct mmVectorF64* p1 = &re->p1;
    struct mmVectorF64* p2 = &re->p2;
    struct mmVectorF64* p3 = &re->p3;

    if (fabs(jd - q->Zjd) > 0.5) return;

    //var i, s, p, x, y, X, Y;
    csEphRsGS_sun(q, jd, S);    //此刻太阳赤道坐标
    csEphRsGS_bseM(q, M, jd);   //此刻月亮
    csEphRsGS_rSM(q, &B, M[2]); //本半影等
    csEphRsGS_bse(q, jd, I);    //贝塞尔坐标参数
    Z = M[2];                   //月亮的坐标的z量

    double a0 = M[0] * M[0] + M[1] * M[1];
    double a1 = a0 - B.r2*B.r2;
    double a2 = a0 - B.r1*B.r1;
    double N = 200;
    for (i = 0; i < N; i++) {//第0和第N点是同一点，可形成一个环，但不必计算，因为第0点可能在界外而无效
        s = i / N * cs_pi2;
        cosS = cos(s); sinS = sin(s);
        X = M[0] + cs_k * cosS; Y = M[1] + cs_k * sinS;
        //本影
        x = M[0] + B.r2*cosS; y = M[1] + B.r2*sinS;
        lineEar2(&p, X, Y, Z, x, y, 0, cs_ba, 1, I);
        if (p.W != 100)
        {
            d[0] = p.J; d[1] = p.W;
            csEphRsGS_push(d, p1);
        }
        else 
        { 
            if (sqrt(x*x + y * y) > a1)
            {
                v[0] = x; v[1] = y; v[2] = 0;
                csEphRsGS_bse2db(d, v, I, 1);
                csEphRsGS_push(d, p1);
            }
        }
        //半影
        x = M[0] + B.r1*cosS; y = M[1] + B.r1*sinS;
        lineEar2(&p, X, Y, Z, x, y, 0, cs_ba, 1, I);
        if (p.W != 100)
        {
            d[0] = p.J; d[1] = p.W;
            csEphRsGS_push(d, p2);
        }
        else 
        { 
            if (sqrt(x*x + y * y) > a2)
            {
                v[0] = x; v[1] = y; v[2] = 0;
                csEphRsGS_bse2db(d, v, I, 1);
                csEphRsGS_push(d, p2);
            }
        }
        //晨昏圈
        v[0] = s; v[1] = 0; v[2] = 0;
        llrConv(pp, v, cs_pi_2 - S[1]);
        pp[0] = rad2rrad(pp[0] + S[0] + cs_pi_2 - I[2]);
        csEphRsGS_push(pp, p3);
    }
    mmVectorF64_PushBack(p1, p1->arrays[0]); mmVectorF64_PushBack(p1, p1->arrays[1]);
    mmVectorF64_PushBack(p2, p2->arrays[0]); mmVectorF64_PushBack(p2, p2->arrays[1]);
    mmVectorF64_PushBack(p3, p3->arrays[0]); mmVectorF64_PushBack(p3, p3->arrays[1]);
    //p1[p1.length] = p1[0], p1[p1.length] = p1[1];
    //p2[p2.length] = p2[0], p2[p2.length] = p2[1];
    //p3[p3.length] = p3[0], p3[p3.length] = p3[1];

    // re.p1 = p1, re.p2 = p2, re.p3 = p3;
}

//界线表
MM_EXPORT_SXWNL
void
csEphRsGS_jieX3(
    const struct csEphRsGS* q,
    double jd,
    struct csEphFeature* re,
    struct mmString* s)
{
    char str[32];
    int i;
    double M[3], I[3];
    double k;
    double p[4];
    struct csEphRSM B;
    struct mmString s1;
    struct mmString s2;
    mmString_Init(&s1);
    mmString_Init(&s2);
    csEphRsGS_feature(q, re, jd);  //求特征参数

    double t = floor(re->jd * 1440) / 1440 - 3.0 / 24;
    double N = 360, dt = 1.0 / 1440;
    mmString_Assigns(s, "");

    for (i = 0; i < N; i++, t += dt) {
        double vx = re->vx + re->ax*(t - re->jdSuo);
        double vy = re->vy + re->ay*(t - re->jdSuo);
        csEphRsGS_bseM(q, M, t);    //此刻月亮贝塞尔坐标(其x和y正是影足)
        csEphRsGS_rSM(q, &B, M[2]); //本半影等
        double r = B.r1;            //半影半径
        csEphRsGS_bse(q, t, I);     //贝塞尔坐标参数
        csJD_JD2str(t + cs_J2000, str);
        mmString_Assigns(&s2, str);
        mmString_Appends(&s2, " ");
        k = 0;
        //南北界
        csEphRsGS_nanbei(p, M, vx, vy, +1, r, I); 
        if (p[1] != 100)
        {
            mmString_Appends(&s2, " ");
            rad2str2(str, p[0]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            rad2str2(str, p[1]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            mmString_Appends(&s2, "|");
            k++;
        }
        else
        {
            //半影北界
            mmString_Appends(&s2, "-------------------|");
        }
        
        csEphRsGS_nanbei(p, M, vx, vy, +1, B.r2, I); 
        if (p[1] != 100)
        {
            mmString_Appends(&s2, " ");
            rad2str2(str, p[0]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            rad2str2(str, p[1]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            mmString_Appends(&s2, "|");
            k++;
        }
        else
        {
            //本影北界
            mmString_Appends(&s2, "-------------------|");
        }
        
        csEphRsGS_bseXY2db(p, M[0], M[1], I, 1);       
        if (p[1] != 100)
        {
            mmString_Appends(&s2, " ");
            rad2str2(str, p[0]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            rad2str2(str, p[1]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            mmString_Appends(&s2, "|");
            k++;
        }
        else
        {
            //中心线
            mmString_Appends(&s2, "-------------------|");
        }

        csEphRsGS_nanbei(p, M, vx, vy, -1, B.r2, I); 
        if (p[1] != 100)
        {
            mmString_Appends(&s2, " ");
            rad2str2(str, p[0]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            rad2str2(str, p[1]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            mmString_Appends(&s2, "|");
            k++;
        }
        else
        {
            //本影南界
            mmString_Appends(&s2, "-------------------|");
        }

        csEphRsGS_nanbei(p, M, vx, vy, -1, r, I); 
        if (p[1] != 100)
        {
            mmString_Appends(&s2, " ");
            rad2str2(str, p[0]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            rad2str2(str, p[1]); mmString_Appends(&s2, str); 
            mmString_Appends(&s2, " ");
            mmString_Appends(&s2, " ");
            k++;
        }
        else
        {
            //半影南界
            mmString_Appends(&s2, "------------------- ");
        }
        
        if (k)
        {
            mmString_Append(&s1, &s2);
            mmString_Appends(&s1, "\n");
        }
    }

    mmString_Appends(s, "    时间(力学时)     ");
    mmString_Appends(s, "     半影北界限     ");
    mmString_Appends(s, "     本影北界线     ");
    mmString_Appends(s, "       中心线       ");
    mmString_Appends(s, "     本影南界线     ");
    mmString_Appends(s, "半影南界线，(伪本影南北界应互换)");
    mmString_Appends(s, "\n");

    mmString_Append(s, &s1);

    mmString_Destroy(&s2);
    mmString_Destroy(&s1);
}

MM_EXPORT_SXWNL
void
csEphGJW_Init(
    struct csEphGJW* p)
{
    mmString_Init(&p->c);
    p->J = 0;
    p->W = 0;
}

MM_EXPORT_SXWNL
void
csEphGJW_Destroy(
    struct csEphGJW* p)
{
    p->W = 0;
    p->J = 0;
    mmString_Destroy(&p->c);
}

MM_EXPORT_SXWNL
void
csEphRsPL_Init(
    struct csEphRsPL* p)
{
    p->nasa_r = 0;
    mmMemset(p->sT, 0, sizeof(p->sT));
    mmString_Init(&p->LX);
    mmString_Init(&p->sflx);
    p->sf = 0;
    p->sf2 = 0;
    p->sf3 = 0;
    p->b1 = 0;
    p->dur = 0;
    p->sun_s = 0;
    p->sun_j = 0;
    p->P1 = 0;
    p->V1 = 0;
    p->P2 = 0;
    p->V2 = 0;

    mmMemset(p->A, 0, sizeof(p->A));
    mmMemset(p->B, 0, sizeof(p->B));

    mmMemset(&p->P, 0, sizeof(p->P));
    mmMemset(&p->Q, 0, sizeof(p->Q));

    mmMemset(p->V, 0, sizeof(p->V));

    mmString_Init(&p->Vc);
    mmString_Init(&p->Vb);
}

MM_EXPORT_SXWNL
void
csEphRsPL_Destroy(
    struct csEphRsPL* p)
{
    mmString_Destroy(&p->Vb);
    mmString_Destroy(&p->Vc);

    mmMemset(p->V, 0, sizeof(p->V));

    mmMemset(&p->Q, 0, sizeof(p->Q));
    mmMemset(&p->P, 0, sizeof(p->P));

    mmMemset(p->B, 0, sizeof(p->B));
    mmMemset(p->A, 0, sizeof(p->A));

    p->V2 = 0;
    p->P2 = 0;
    p->V1 = 0;
    p->P1 = 0;
    p->sun_j = 0;
    p->sun_s = 0;
    p->dur = 0;
    p->b1 = 0;
    p->sf3 = 0;
    p->sf2 = 0;
    p->sf = 0;
    mmString_Destroy(&p->sflx);
    mmString_Destroy(&p->LX);
    mmMemset(p->sT, 0, sizeof(p->sT));
    p->nasa_r = 0;
}

//日月xy坐标计算。参数：jd是力学时,站点经纬L,fa,海拔high(千米)
MM_EXPORT_SXWNL
void
csEphRsPL_secXY(
    struct csEphRsPL* p,
    const struct csEphRsGS* q,
    double jd,
    double L,
    double fa,
    double high,
    struct csEphSECXY* re)
{
    //基本参数计算
    double zd[2];
    double deltat = dt_T(jd); //TD-UT
    nutation2(zd, jd / 36525);
    double gst = pGST(jd - deltat, deltat) + zd[0] * cos(hcjj(jd / 36525) + zd[1]); //真恒星时(不考虑非多项式部分)

    double z[3];
    //=======月亮========
    csEphRsGS_moon(q, jd, z); re->mCJ = z[0]; re->mCW = z[1]; re->mR = z[2]; //月亮视赤经,月球赤纬
    double mShiJ = rad2rrad(gst + L - z[0]); //得到此刻月亮时角
    parallax(z, z, mShiJ, fa, high); re->mCJ2 = z[0]; re->mCW2 = z[1]; re->mR2 = z[2]; //修正了视差的赤道坐标

    //=======太阳========
    csEphRsGS_sun(q, jd, z); re->sCJ = z[0]; re->sCW = z[1]; re->sR = z[2]; //太阳视赤经,太阳赤纬
    double sShiJ = rad2rrad(gst + L - z[0]); //得到此刻太阳时角
    parallax(z, z, sShiJ, fa, high); re->sCJ2 = z[0]; re->sCW2 = z[1]; re->sR2 = z[2]; //修正了视差的赤道坐标

    //=======视半径========
    re->mr = cs_sMoon / re->mR2 / cs_rad;
    re->sr = 959.63 / re->sR2 / cs_rad * cs_AU;
    if (p->nasa_r) re->mr *= cs_sMoon2 / cs_sMoon; //0.99925;
    //=======日月赤经纬差转为日面中心直角坐标(用于日食)==============
    re->x = rad2rrad(re->mCJ2 - re->sCJ2) * cos((re->mCW2 + re->sCW2) / 2);
    re->y = re->mCW2 - re->sCW2;
    re->t = jd;
}

//已知t1时刻星体位置、速度，求x*x+y*y=r*r时,t的值
MM_EXPORT_SXWNL
double
csEphRsPL_lineT(
    const struct csEphSECXY* G,
    double v,
    double u,
    double r,
    int n)
{
    double b = G->y*v - G->x*u, A = u * u + v * v, B = u * b, C = b * b - r * r*v*v, D = B * B - A * C;
    if (D < 0) return 0;
    D = sqrt(D); if (!n) D = -D;
    return G->t + ((-B + D) / A - G->x) / v;
}

//日食的食甚计算(jd为近朔的力学时,误差几天不要紧)
MM_EXPORT_SXWNL
void
csEphRsPL_secMax(
    struct csEphRsPL* p,
    struct csEphRsGS* q,
    double jd,
    double L,
    double fa,
    double high)
{
    int i;
    double tt;
    struct csEphSECXY G, g;
    //分别是:食甚,初亏,复圆,食既,生光
    mmMemset(p->sT, 0, sizeof(p->sT));
    mmString_Assigns(&p->LX, "");//类型
    p->sf = 0;  //食分
    p->sf2 = 0; //食分(日出食分)
    p->sf3 = 0; //食分(日没食分)
    mmString_Assigns(&p->sflx, " ");//食分类型
    p->b1 = 1;  //月日半径比(食甚时刻)
    p->dur = 0; //持续时间
    p->P1 = p->V1 = 0;  //初亏方位,P北点起算,V顶点起算
    p->P2 = p->V2 = 0;  //复圆方位,P北点起算,V顶点起算
    p->sun_s = p->sun_j = 0; //日出日没

    csEphRsGS_initialize(q, jd, 7);
    jd = q->Zjd; //食甚初始估值为插值表中心时刻(粗朔)

    csEphRsPL_secXY(p, q, jd, L, fa, high, &G);
    jd -= G.x / 0.2128; //与食甚的误差在20分钟以内

    double u, v, dt = 60.0 / 86400, dt2;
    // 消除可能的未初始化警告
    u = 0; v = 0;
    for (i = 0; i < 2; i++) {
        csEphRsPL_secXY(p, q, jd, L, fa, high, &G);
        csEphRsPL_secXY(p, q, jd + dt, L, fa, high, &g);
        u = (g.y - G.y) / dt;
        v = (g.x - G.x) / dt;
        dt2 = -(G.y*u + G.x*v) / (u*u + v * v);
        jd += dt2; //极值时间
    }

    //求直线到太阳中心的最小值
    double maxsf = 0, maxjd = jd, rmin, ls;
    for (i = -30; i < 30; i += 6) {
        tt = jd + i / 86400.0;
        csEphRsPL_secXY(p, q, tt, L, fa, high, &g);
        ls = (g.mr + g.sr - sqrt(g.x * g.x + g.y * g.y)) / g.sr / 2;
        if (ls > maxsf) { maxsf = ls; maxjd = tt; }
    }
    jd = maxjd;
    for (i = -5; i < 5; i += 1) {
        tt = jd + i / 86400.0;
        csEphRsPL_secXY(p, q, tt, L, fa, high, &g);
        ls = (g.mr + g.sr - sqrt(g.x * g.x + g.y * g.y)) / g.sr / 2;
        if (ls > maxsf) { maxsf = ls; maxjd = tt; }
    }
    jd = maxjd;
    csEphRsPL_secXY(p, q, jd, L, fa, high, &G);
    rmin = sqrt(G.x * G.x + G.y * G.y);

    p->sun_s = sunShengJ(jd - dt_T(jd) + L / cs_pi2, L, fa, -1) + dt_T(jd); //日出,统一用力学时
    p->sun_j = sunShengJ(jd - dt_T(jd) + L / cs_pi2, L, fa, 1) + dt_T(jd); //日没,统一用力学时


    if (rmin <= G.mr + G.sr) { //食计算
        p->sT[1] = jd; //食甚
        mmString_Assigns(&p->LX, "偏");
        p->sf = (G.mr + G.sr - rmin) / G.sr / 2; //食分
        p->b1 = G.mr / G.sr;

        csEphRsPL_secXY(p, q, p->sun_s, L, fa, high, &g); //日出食分
        p->sf2 = (g.mr + g.sr - sqrt(g.x*g.x + g.y*g.y)) / g.sr / 2; //日出食分
        if (p->sf2 < 0) p->sf2 = 0;

        csEphRsPL_secXY(p, q, p->sun_j, L, fa, high, &g); //日没食分
        p->sf3 = (g.mr + g.sr - sqrt(g.x*g.x + g.y*g.y)) / g.sr / 2; //日没食分
        if (p->sf3 < 0) p->sf3 = 0;

        p->sT[0] = csEphRsPL_lineT(&G, v, u, G.mr + G.sr, 0); //初亏
        for (i = 0; i < 3; i++) { //初亏再算3次
            csEphRsPL_secXY(p, q, p->sT[0], L, fa, high, &g);
            p->sT[0] = csEphRsPL_lineT(&g, v, u, g.mr + g.sr, 0);
        }

        p->P1 = rad2mrad(atan2(g.x, g.y)); //初亏位置角
        p->V1 = rad2mrad(p->P1 - shiChaJ(pGST2(p->sT[0]), L, fa, g.sCJ, g.sCW)); //这里g.sCJ与g.sCW对应的时间与sT[0]还差了一点，所以有一小点误差，不采用真恒星时也误差一点

        p->sT[2] = csEphRsPL_lineT(&G, v, u, G.mr + G.sr, 1); //复圆
        for (i = 0; i < 3; i++) { //复圆再算3次
            csEphRsPL_secXY(p, q, p->sT[2], L, fa, high, &g);
            p->sT[2] = csEphRsPL_lineT(&g, v, u, g.mr + g.sr, 1);
        }
        p->P2 = rad2mrad(atan2(g.x, g.y));
        p->V2 = rad2mrad(p->P2 - shiChaJ(pGST2(p->sT[2]), L, fa, g.sCJ, g.sCW)); //这里g.sCJ与g.sCW对应的时间与sT[2]还差了一点，所以有一小点误差，不采用真恒星时也误差一点
    }
    if (rmin <= G.mr - G.sr) { //全食计算
        mmString_Assigns(&p->LX, "全");
        p->sT[3] = csEphRsPL_lineT(&G, v, u, G.mr - G.sr, 0); //食既
        csEphRsPL_secXY(p, q, p->sT[3], L, fa, high, &g);
        p->sT[3] = csEphRsPL_lineT(&g, v, u, g.mr - g.sr, 0); //食既再算1次

        p->sT[4] = csEphRsPL_lineT(&G, v, u, G.mr - G.sr, 1); //生光
        csEphRsPL_secXY(p, q, p->sT[4], L, fa, high, &g);
        p->sT[4] = csEphRsPL_lineT(&g, v, u, g.mr - g.sr, 1); //生光再算1次
        p->dur = p->sT[4] - p->sT[3];
    }
    if (rmin <= G.sr - G.mr) { //环食计算
        mmString_Assigns(&p->LX, "环");
        p->sT[3] = csEphRsPL_lineT(&G, v, u, G.sr - G.mr, 0); //食既
        csEphRsPL_secXY(p, q, p->sT[3], L, fa, high, &g);
        p->sT[3] = csEphRsPL_lineT(&g, v, u, g.sr - g.mr, 0); //食既再算1次

        p->sT[4] = csEphRsPL_lineT(&G, v, u, G.sr - G.mr, 1); //生光
        csEphRsPL_secXY(p, q, p->sT[4], L, fa, high, &g);
        p->sT[4] = csEphRsPL_lineT(&g, v, u, g.sr - g.mr, 1); //生光再算1次
        p->dur = p->sT[4] - p->sT[3];
    }
    if (p->sT[1] < p->sun_s && p->sf2 > 0) { p->sf = p->sf2; mmString_Assigns(&p->sflx, "#"); } //食甚在日出前，取日出食分
    if (p->sT[1] > p->sun_j && p->sf3 > 0) { p->sf = p->sf3; mmString_Assigns(&p->sflx, "*"); } //食甚在日没后，取日没食分

    for (i = 0; i < 5; i++) {
        if (p->sT[i]<p->sun_s || p->sT[i]>p->sun_j) p->sT[i] = 0; //升降时间之外的日食算值无效，因为地球不是透明的
    }

    p->sun_s -= dt_T(jd);
    p->sun_j -= dt_T(jd);
}

MM_EXPORT_SXWNL
void
csEphRsPL_zb0(
    struct csEphRsPL* p,
    struct csEphRsGS* q,
    double jd)
{
    double zd[2];
    //基本参数计算
    double deltat = dt_T(jd); //TD-UT
    double E = hcjj(jd / 36525);
    nutation2(zd, jd / 36525);

    p->P.g = pGST(jd - deltat, deltat) + zd[0] * cos(E + zd[1]); //真恒星时(不考虑非多项式部分)
    csEphRsGS_sun(q, jd, p->P.S);
    csEphRsGS_moon(q, jd, p->P.M);

    double t2 = jd + 60.0 / 86400;
    p->Q.g = pGST(t2 - deltat, deltat) + zd[0] * cos(E + zd[1]);
    csEphRsGS_sun(q, t2, p->Q.S);
    csEphRsGS_moon(q, t2, p->Q.M);

    //转为直角坐标
    double z1[3], z2[3];
    //var z1 = new Array(), z2 = new Array();
    llr2xyz(z1, p->P.S);
    llr2xyz(z2, p->P.M);

    double k = 959.63 / cs_sMoon * cs_AU; //k为日月半径比
    //本影锥顶点坐标计算
    double FA[3] = {
    (z1[0] - z2[0]) / (1 - k) + z2[0],
    (z1[1] - z2[1]) / (1 - k) + z2[1],
    (z1[2] - z2[2]) / (1 - k) + z2[2] };
    xyz2llr(p->A, FA);
    //半影锥顶点坐标计算
    double FB[3] = {
    (z1[0] - z2[0]) / (1 + k) + z2[0],
    (z1[1] - z2[1]) / (1 + k) + z2[1],
    (z1[2] - z2[2]) / (1 + k) + z2[2] };
    xyz2llr(p->B, FB);
}

MM_EXPORT_SXWNL
void
csEphRsPL_zbXY(
    struct csEphZB* p,
    double L,
    double fa)
{
    double s[3] = { p->S[0], p->S[1], p->S[2] };
    double m[3] = { p->M[0], p->M[1], p->M[2] };
    parallax(s, s, p->g + L - p->S[0], fa, 0); //修正了视差的赤道坐标
    parallax(m, m, p->g + L - p->M[0], fa, 0); //修正了视差的赤道坐标
    //=======视半径========
    p->mr = cs_sMoon / m[2] / cs_rad;
    p->sr = 959.63 / s[2] / cs_rad * cs_AU;
    //=======日月赤经纬差转为日面中心直角坐标(用于日食)==============
    p->x = rad2rrad(m[0] - s[0]) * cos((m[1] + s[1]) / 2);
    p->y = m[1] - s[1];
}

//f取+-1
MM_EXPORT_SXWNL
void
csEphRsPL_p2p(
    struct csEphRsPL* o,
    double L,
    double fa,
    struct csEphGJW* re,
    int fAB,
    int f)
{
    struct csEphZB* p = &o->P;
    struct csEphZB* q = &o->Q;
    csEphRsPL_zbXY(&o->P, L, fa);
    csEphRsPL_zbXY(&o->Q, L, fa);

    double u = q->y - p->y, v = q->x - p->x, a = sqrt(u*u + v * v), r = 959.63 / p->S[2] / cs_rad * cs_AU;

    double W = p->S[1] + f * r*v / a, J = p->S[0] - f * r*u / a / cos((W + p->S[1]) / 2), R = p->S[2];

    double* A = fAB ? o->A : o->B;

    struct csEphCoord pp;
    double d[3] = { J, W, R };
    lineEar(&pp, d, A, p->g);
    re->J = pp.J;
    re->W = pp.W;
}

//食中心点计算
MM_EXPORT_SXWNL
void
csEphRsPL_pp0(
    struct csEphRsPL* o,
    struct csEphGJW* re)
{
    struct csEphCoord pp;
    struct csEphZB* p = &o->P;
    lineEar(&pp, p->M, p->S, p->g);
    re->J = pp.J;
    re->W = pp.W; //无解返回值是100

    if (re->W == 100) { mmString_Assigns(&re->c, ""); return; }
    mmString_Assigns(&re->c, "全");
    csEphRsPL_zbXY(p, re->J, re->W);
    if (p->sr > p->mr) mmString_Assigns(&re->c, "环");
}

//南北界计算
MM_EXPORT_SXWNL
void
csEphRsPL_nbj(
    struct csEphRsPL* p,
    struct csEphRsGS* q,
    double jd)
{
    csEphRsGS_initialize(q, jd, 7);
    int i;
    struct csEphGJW G;
    csEphGJW_Init(&G);
    double* V = p->V;
    for (i = 0; i < 10; i++) V[i] = 100; 
    //返回初始化,纬度值为100表示无解,经度100也是无解,但在以下程序中经度会被转为-PI到+PI
    mmString_Assigns(&p->Vc, ""); mmString_Assigns(&p->Vb, "");

    csEphRsPL_zb0(p, q, jd);
    csEphRsPL_pp0(p, &G);
    V[0] = G.J; V[1] = G.W; p->Vc = G.c; //食中心

    G.J = G.W = 0; for (i = 0; i < 2; i++) csEphRsPL_p2p(p, G.J, G.W, &G, 1,  1);
    V[2] = G.J; V[3] = G.W; //本影北界,环食为南界(本影区之内,变差u,v基本不变,所以计算两次足够)
    
    G.J = G.W = 0; for (i = 0; i < 2; i++) csEphRsPL_p2p(p, G.J, G.W, &G, 1, -1);
    V[4] = G.J; V[5] = G.W; //本影南界,环食为北界
    
    G.J = G.W = 0; for (i = 0; i < 3; i++) csEphRsPL_p2p(p, G.J, G.W, &G, 0, -1);
    V[6] = G.J; V[7] = G.W; //半影北界
    
    G.J = G.W = 0; for (i = 0; i < 3; i++) csEphRsPL_p2p(p, G.J, G.W, &G, 0,  1);
    V[8] = G.J; V[9] = G.W; //半影南界

    if (V[3] != 100 && V[5] != 100) { //粗算本影南北距离
        double x = (V[2] - V[4])*cos((V[3] + V[5]) / 2), y = V[3] - V[5];
        double v = cs_rEarA * sqrt(x*x + y * y);
        char str[32];
        toFixed(str, v, 0); mmString_Appends(&p->Vb, str); mmString_Appends(&p->Vb, "千米");
    }
    csEphGJW_Destroy(&G);
}

static const char* lxb[][2] =
{
    {  "N", "无日食"               },
    {  "T", "全食"                 },
    {  "A", "环食"                 },
    {  "P", "偏食"                 },
    { "T0", "无中心全食"           },
    { "T1", "部分本影有中心全食"   },
    { "A0", "无中心环食"           },
    { "A1", "部分伪本影有中心全食" },
    {  "H", "全环全"               },
    { "H2", "全全环"               },
    { "H3", "环全全"               },
};

//获取日食类型名字
MM_EXPORT_SXWNL
const char*
csEphRsPL_GetRSLXName(
    const char* lx)
{
    int i;
    for (i = 0; i < csArraySize(lxb); i++)
    {
        if (0 == strcmp(lxb[i][0], lx))
        {
            return lxb[i][1];
        }
    }
    // "无日食"
    return lxb[0][1];
}

//日食字符串
MM_EXPORT_SXWNL
void
csEphRsPL_rysCalc(
    const struct csJD* d,
    double vJ, double vW,
    int is_utc,
    int nasa_r,
    struct mmString* s)
{
    char str[32] = { 0 };
    struct mmString s2;
    struct csEphMSC msc;
    struct csEphRsPL rsPL;
    struct csEphRsGS rsGS;
    struct csEphYsPL ysPL;
    double jd = csJD_toJD(d) - cs_J2000;
    if (is_utc) 
    {
        jd += -8 / 24.0 + dt_T(jd);
    }
    csEphMSC_calc(&msc, jd, vJ, vW, 0);

    double J1, W1, J2, W2;
    double sr, mr, er, Er, d0, d1, d2;
    double msHJ = rad2mrad(msc.mHJ - msc.sHJ);
    int i;

    mmString_Reset(s);

    mmString_Init(&s2);
    csEphRsPL_Init(&rsPL);
    csEphRsGS_Init(&rsGS);
    csEphYsPL_Init(&ysPL);

    if (msHJ < 3 / cs_radd || msHJ > 357 / cs_radd)
    {
        static const char* mc1[] =
        {
            "食中心点", "本影北界", "本影南界", "半影北界", "半影南界",
        };

        static const char* mc2[] =
        {
            "初亏", "食甚", "复圆", "食既", "生光",
        };

        static const char* mc3[] =
        {
            "初亏", "食甚", "复圆", "环食始", "环食始",
        };

        // 显示食甚等时间
        const char* td = " TD";

        const char** mc4;

        // 日食图表放大计算
        J1 = msc.mCJ2; W1 = msc.mCW2; J2 = msc.sCJ2; W2 = msc.sCW2;	// 用未做大气折射的来计算日食
        sr = msc.sRad; mr = msc.mRad;
        d1 = j1_j2(J1, W1, J2, W2) * cs_rad; d0 = mr + sr;
        if (msc.zx_W != 100)
        {
            mmString_Appends(&s2, "食中心地标：经 ");
            toFixed(str, msc.zx_J / cs_pi * 180, 5);
            mmString_Appends(&s2, str);

            mmString_Appends(&s2, " 纬 ");
            toFixed(str, msc.zx_W / cs_pi * 180, 5);
            mmString_Appends(&s2, str);
        }
        else
        {
            mmString_Assigns(&s2, "此刻月亮本影中心线不经过地球。");
        }

        mmString_Appends(s, "日月站心视半径 ");
        m2fm(str, sr, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, "及");
        m2fm(str, mr, 2, 0); mmString_Appends(s, str);

        mmString_Appends(s, " \n");
        mmString_Append(s, &s2);
        mmString_Appends(s, " \n");

        mmString_Appends(s, "日月中心视距 ");
        m2fm(str, d1, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, " 日月半径和 ");
        m2fm(str, d0, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "半径差 ");
        m2fm(str, sr - mr, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\t");
        mmString_Appends(s, "距外切 ");
        m2fm(str, d1 - d0, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        // 显示南北界数据
        rsPL.nasa_r = nasa_r;		// 视径选择

        mmString_Appends(s, "--------------------------------------\n");
        csJD_JD2str(jd + cs_J2000, str); mmString_Appends(s, str);
        mmString_Appends(s, " TD\n--------------------------------------\n");
        mmString_Appends(s, "南北界点：经度　　　　纬度\n");

        csEphRsPL_nbj(&rsPL, &rsGS, jd);
        for (i = 0; i < 5; i++)
        {
            mmString_Appends(s, mc1[i]);
            mmString_Appends(s, "：");

            //s += mc[i] + "：";
            if (rsPL.V[i * 2 + 1] == 100)
            {
                mmString_Appends(s, "无　　　　　无\n");
                continue;
            }
            toFixed(str, rsPL.V[i * 2    ] * cs_radd, 5); mmString_Appends(s, str);
            mmString_Appends(s, "　");
            toFixed(str, rsPL.V[i * 2 + 1] * cs_radd, 5); mmString_Appends(s, str);
            mmString_Appends(s, "\n");
        }
        mmString_Appends(s, "中心类型：");
        mmString_Append(s, &rsPL.Vc);
        mmString_Appends(s, "食\n");

        mmString_Appends(s, "本影南北界距约");
        mmString_Append(s, &rsPL.Vb);

        csEphRsPL_secMax(&rsPL, &rsGS, jd, vJ, vW, 0);
        if (0 == mmString_CompareCStr(&rsPL.LX, "环"))
        {
            // 环食没有食既和生光
            mc4 = mc3;
            //mc[3] = "环食始", mc[4] = "环食终";	
        }
        else
        {
            mc4 = mc2;
        }

        mmString_Appends(s, "\n--------------------------------------\n");
        mmString_Appends(s, "时间表 (日");
        mmString_Append(s, &rsPL.LX);
        mmString_Appends(s, "食)\n");

        for (i = 0; i < 5; i++)
        {
            jd = rsPL.sT[i];
            if (!jd)
                continue;
            if (is_utc)
            {
                // 转为UTC(本地时间)
                jd -= -8 / 24.0 + dt_T(jd); td = " UTC";
            }

            mmString_Appends(s, mc4[i]);
            mmString_Appends(s, ":");
            csJD_JD2str(jd + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, td); mmString_Appends(s, "\n");
        }

        mmString_Appends(s, "时长: ");
        m2fm(str, rsPL.dur * 86400, 1, 1); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "食分: ");
        toFixed(str, rsPL.sf, 5); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "月日视径比: ");
        toFixed(str, rsPL.b1, 5); mmString_Appends(s, str);
        mmString_Appends(s, "(全或环食分)\n");

        mmString_Appends(s, "是否NASA径比(1是,0否): ");
        toFixed(str, (double)rsPL.nasa_r, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "食分指日面直径被遮比例");
    }

    if (msHJ > 170 / cs_radd && msHJ < 190 / cs_radd)
    {
        static const char* mc1[] =
        {
            "初亏", "食甚", "复圆", "半影食始", "半影食终", "食既", "生光",
        };

        const char* td = " TD";

        // 月食图表放大计算
        J1 = msc.mCJ; W1 = msc.mCW; J2 = msc.sCJ + cs_pi; W2 = -msc.sCW;
        er = msc.eShadow; Er = msc.eShadow2; mr = msc.e_mRad;	// 用未做大气折射的来计算日食
        d1 = j1_j2(J1, W1, J2, W2) * cs_rad; d0 = mr + er; d2 = mr + Er;

        mmString_Appends(s, "本影半径 ");
        m2fm(str, er, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, " 半影半径 ");
        m2fm(str, Er, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, " 月亮地心视半径 ");
        m2fm(str, mr, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "影月中心距 ");
        m2fm(str, d1, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, " 影月半径和 ");
        m2fm(str, d0, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "距相切 ");
        m2fm(str, d1 - d0, 2, 0); mmString_Appends(s, str);
        mmString_Appends(s, " 距第二相切 ");
        m2fm(str, d1 - d2, 2, 0); mmString_Appends(s, str);

        csEphYsPL_lecMax(&ysPL, jd);

        mmString_Appends(s, "\n\n时间表(月");
        mmString_Append(s, &ysPL.LX);
        mmString_Appends(s, "食)\n");

        for (i = 0; i < 7; i++)
        {
            jd = ysPL.lT[i];
            if (!jd)
                continue;
            if (is_utc)
            {
                // 转为UTC(本地时间)
                jd -= -8 / 24.0 + dt_T(jd); td = " UTC";
            }

            mmString_Appends(s, mc1[i]);
            mmString_Appends(s, ":");
            csJD_JD2str(jd + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, td);
            mmString_Appends(s, "\n");
        }
        mmString_Appends(s, "食分:");
        toFixed(str, ysPL.sf, 5); mmString_Appends(s, str);
        mmString_Appends(s, "\n");

        mmString_Appends(s, "食分指月面直径被遮比例");
    }

    csEphYsPL_Destroy(&ysPL);
    csEphRsGS_Destroy(&rsGS);
    csEphRsPL_Destroy(&rsPL);
    mmString_Destroy(&s2);
}

//查找日食
MM_EXPORT_SXWNL
void
csEphRsPL_rsSearch(
    int Y, int M,
    int n,
    int fs,
    struct mmString* s)
{
    int i, k;
    struct mmString s1;
    struct csEphRsData r;
    struct csEphFeature rr;
    struct csEphRsGS rsGS;
    char str[32] = { 0 };
    struct csJD JD = { Y, M, 1, 0, 0, 0.0 };
    double jd = csJD_toJD(&JD) - cs_J2000;  //取屏幕时间
    jd = XL_MS_aLon_t2(int2((jd + 8) / 29.5306)*cs_pi * 2) * 36525; //定朔

    mmString_Reset(s);

    mmString_Init(&s1);
    csEphRsData_Init(&r);
    csEphFeature_Init(&rr);
    csEphRsGS_Init(&rsGS);

    for (i = 0, k = 0; i < n; i++)
    {
        ecFast(&r, jd); //低精度高速搜索
        if (0 == mmString_CompareCStr(&r.lx, "NN")) 
        { 
            //排除不可能的情况，加速计算
            jd += 29.5306; 
            continue; 
        } 
        if (!r.ac)
        {
            if (fs == 0) csEphRsGS_initialize(&rsGS, jd, 2); //低精度
            if (fs == 1) csEphRsGS_initialize(&rsGS, jd, 7); //高精度
            csEphRsGS_feature(&rsGS, &rr, jd);

            // 拷贝数据
            mmString_Assign(&r.lx, &rr.lx);
            r.jd = rr.jd;
            r.jdSuo = rr.jdSuo;
            r.ac = r.ac;
        }
        if (0 != mmString_CompareCStr(&r.lx, "N"))
        {
            csJD_JD2str(r.jd + cs_J2000, str);
            mmString_Appendsn(&s1, &str[0], 11);
            mmString_LAlignAppends(&s1, mmString_CStr(&r.lx), mmString_Size(&r.lx), 2);
            k++;
            if (k %   5 == 0) { mmString_Appends(&s1, "\n"); }
            if (k % 100 == 0) { mmString_Append(s, &s1); mmString_Assigns(&s1, ""); }
        }
        jd = r.jd + 29.5306;
    }

    mmString_Append(s, &s1);

    csEphRsGS_Destroy(&rsGS);
    csEphFeature_Destroy(&rr);
    csEphRsData_Destroy(&r);
    mmString_Destroy(&s1);
}

//日食表字符串
MM_EXPORT_SXWNL
void
csEphRsPL_rs2Calc(
    int fs,
    double jd0,
    double step,
    int bn,
    struct mmString* s)
{
    if (fs == 0) return;

    //默认时间 2008-08-01 10:14:30
    double jd = 2454679.926741 - cs_J2000; 
    if (fs == 1) jd = jd0;
    // if(fs==2) ; //保持时间不变
    if (fs == 3) jd -= step;
    if (fs == 4) jd += step;
    jd = XL_MS_aLon_t2(int2((jd + 8) / 29.5306)*cs_pi * 2) * 36525; //归朔

    mmString_Assigns(s, "");
    //计算单个日食
    if (fs == 1 || fs == 2 || fs == 3 || fs == 4)
    {
        char str[32] = { 0 };
        struct csEphFeature r;
        struct csEphRsGS rsGS;
        csEphFeature_Init(&r);
        csEphRsGS_Init(&rsGS);
        csEphRsGS_initialize(&rsGS, jd, 7);
        //特征计算
        csEphRsGS_feature(&rsGS, &r, jd);

        if (0 == mmString_CompareCStr(&r.lx, "N"))
        {
            mmString_Assigns(s, "无日食");
        }
        else
        {
            mmString_Assigns(s, "本次日食概述(力学时)\n");

            mmString_Appends(s, "偏食始："); csJD_JD2str(r.gk3[2] + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, " "); rad2str2(str, r.gk3[0]); mmString_Appends(s, str);
            mmString_Appends(s, ","); rad2str2(str, r.gk3[1]); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            mmString_Appends(s, "中心始："); csJD_JD2str(r.gk1[2] + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, " "); rad2str2(str, r.gk1[0]); mmString_Appends(s, str);
            mmString_Appends(s, ","); rad2str2(str, r.gk1[1]); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            if (r.gk5[1] != 100)
            {
                mmString_Appends(s, "视午食："); csJD_JD2str(r.gk5[2] + cs_J2000, str); mmString_Appends(s, str);
                mmString_Appends(s, " "); rad2str2(str, r.gk5[0]); mmString_Appends(s, str);
                mmString_Appends(s, ","); rad2str2(str, r.gk5[1]); mmString_Appends(s, str);
                mmString_Appends(s, "\n");
            }

            mmString_Appends(s, "中心终："); csJD_JD2str(r.gk2[2] + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, " "); rad2str2(str, r.gk2[0]); mmString_Appends(s, str);
            mmString_Appends(s, ","); rad2str2(str, r.gk2[1]); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            mmString_Appends(s, "偏食终："); csJD_JD2str(r.gk4[2] + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, " "); rad2str2(str, r.gk4[0]); mmString_Appends(s, str);
            mmString_Appends(s, ","); rad2str2(str, r.gk4[1]); mmString_Appends(s, str);
            mmString_Appends(s, "\n");


            mmString_Appends(s, "中心点特征\n");

            mmString_Appends(s, "影轴地心距 γ = "); toFixed(str, r.D, 4); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            mmString_Appends(s, "中心地标 (经,纬) = "); toFixed(str, (r.zxJ*cs_radd), 2); mmString_Appends(s, str);
            mmString_Appends(s,                   ","); toFixed(str, (r.zxW*cs_radd), 2); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            mmString_Appends(s, "中心时刻 tm = "); csJD_JD2str(r.jd + cs_J2000, str); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            mmString_Appends(s, "太阳方位 (经,纬) = "); toFixed(str, (r.Sdp[0] * cs_radd), 0); mmString_Appends(s, str);
            mmString_Appends(s, ","); toFixed(str, (r.Sdp[1] * cs_radd), 0); mmString_Appends(s, str);
            mmString_Appends(s, "\n");

            mmString_Appends(s, "日食类型 LX = "); mmString_Append(s, &r.lx);
            mmString_Appends(s, " "); mmString_Appends(s, csEphRsPL_GetRSLXName(mmString_CStr(&r.lx)));
            mmString_Appends(s, "\n");

            mmString_Appends(s, "食分="); toFixed(str, r.sf, 4); mmString_Appends(s, str);
            mmString_Appends(s, ", 食延="); m2fm(str, r.tt * 86400, 0, 2); mmString_Appends(s, str);
            mmString_Appends(s, ", 食带="); toFixed(str, r.dw, 0); mmString_Appends(s, str); mmString_Appends(s, "km");
            mmString_Appends(s, "\n");
        }
        csEphRsGS_Destroy(&rsGS);
        csEphFeature_Destroy(&r);
        return;
    }

    //计算多个日食
    if (fs == 5)
    {
        int i;
        char str[32] = { 0 };
        size_t ssz;
        struct csEphFeature r;
        struct csEphRsGS rsGS;
        csEphFeature_Init(&r);
        csEphRsGS_Init(&rsGS);

        mmString_Assigns(s, "       力学时             γ    型       中心地标        方位角     食分  食带  食延\n");

        for (i = 0; i < bn; i++, jd += step)
        {
            //中精度计算
            csEphRsGS_initialize(&rsGS, jd, 3);

            csEphRsGS_feature(&rsGS, &r, jd);
            if (0 == mmString_CompareCStr(&r.lx, "N"))
            {
                continue;
            }
            else
            {
                //      力学时                Y   型    中心地标      方位角    食分  食带  食延   
                //2008-08-01 10:22:12     0.8307  T    72.30, 65.66   55, 34   1.0394  239  02m27s
                //2009-01-26 07:59:45    -0.2820  A    70.23,-34.08  157, 73   0.9282  281  07m54s

                // 力学时
                csJD_JD2str(r.jd + cs_J2000, str);
                mmString_Appends(s, str);
                mmString_Appends(s, "  ");

                // γ
                toFixed(str, r.D, 4);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 7);
                mmString_Appends(s, "  ");

                // 型
                ssz = mmString_Size(&r.lx);
                mmString_RAlignAppends(s, mmString_CStr(&r.lx), ssz, 2);
                mmString_Appends(s, "  ");

                // 中心地标
                mmString_Appends(s, "(");
                toFixed(str, (r.zxJ*cs_radd), 2);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 7);
                mmString_Appends(s, ",");
                toFixed(str, (r.zxW*cs_radd), 2);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 7);
                mmString_Appends(s, ")");
                mmString_Appends(s, "  ");

                // 方位角
                mmString_Appends(s, "(");
                toFixed(str, (r.Sdp[0] * cs_radd), 0);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 3);
                mmString_Appends(s, ",");
                toFixed(str, (r.Sdp[1] * cs_radd), 0);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 3);
                mmString_Appends(s, ")");
                mmString_Appends(s, "  ");

                // 食分
                toFixed(str, r.sf, 4);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 6);
                mmString_Appends(s, "  ");
                // 食带
                toFixed(str, r.dw, 0);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 3);
                mmString_Appends(s, "  ");
                // 食延
                m2fm(str, r.tt * 86400, 0, 2);
                ssz = strlen(str);
                mmString_RAlignAppends(s, str, ssz, 6);

                mmString_Appends(s, "\n");
            }
        }
        csEphRsGS_Destroy(&rsGS);
        csEphFeature_Destroy(&r);
    }
}

//显示界线表
MM_EXPORT_SXWNL
void
csEphRsPL_rs2Jxb(
    double jd,
    struct mmString* s)
{
    struct csEphFeature r;
    struct csEphRsGS rsGS;
    jd = XL_MS_aLon_t2(int2((jd + 8) / 29.5306)*cs_pi * 2) * 36525; //归朔
    csEphFeature_Init(&r);
    csEphRsGS_Init(&rsGS);
    csEphRsGS_initialize(&rsGS, jd, 7);
    csEphRsGS_jieX3(&rsGS, jd, &r, s);
    csEphRsGS_Destroy(&rsGS);
    csEphFeature_Destroy(&r);
}
