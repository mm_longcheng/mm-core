/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmSxwnlLunarFTV.h"
#include "mmSxwnlConst.h"

//某月的第几个星期几,如第2个星期一指从月首开始顺序找到第2个“星期一”
//一月的最后一个星期日（月倒数第一个星期日）
MM_EXPORT_SXWNL const char* csLunarOBA_wFtv[] =
{
    "0150I世界麻风日", 
    "0520.国际母亲节",
    "0530I全国助残日",
    "0630.父亲节",
    "0730.被奴役国家周",
    "0932I国际和平日",
    "0940.国际聋人节 世界儿童日",
    "0950I世界海事日",
    "1011.国际住房日",
    "1013I国际减轻自然灾害日(减灾日)",
    "1144I感恩节",
};

MM_EXPORT_SXWNL const int csLunarOBA_wFtvLength = csArraySize(csLunarOBA_wFtv);

//国历节日,#表示放假日,I表示重要节日或纪念日

//1月
const char* sFtv_01[] =
{
    "01#元旦",
};

//2月
const char* sFtv_02[] =
{
    "02I世界湿地日", 
    "10.国际气象节", 
    "14I情人节",
};

//3月
const char* sFtv_03[] =
{
    "01.国际海豹日", 
    "03.全国爱耳日", 
    "05.1963-9999学雷锋纪念日", 
    "08I妇女节", 
    "12I植树节", 
    "12.1925-9999孙中山逝世纪念日", 
    "14.国际警察日", 
    "15I1983-9999消费者权益日", 
    "17.中国国医节", 
    "17.国际航海日", 
    "21.世界森林日", 
    "21.消除种族歧视国际日", 
    "21.世界儿歌日", 
    "22I世界水日", 
    "23I世界气象日", 
    "24.1982-9999世界防治结核病日", 
    "25.全国中小学生安全教育日", 
    "30.巴勒斯坦国土日",
};

//4月
const char* sFtv_04[] =
{
    "01I1564-9999愚人节",
    "01.全国爱国卫生运动月(四月)",
    "01.税收宣传月(四月)",
    "07I世界卫生日",
    "22I世界地球日",
    "23.世界图书和版权日",
    "24.亚非新闻工作者日",
};

//5月
const char* sFtv_05[] =
{
    "01#1889-9999劳动节",
    "04I青年节",
    "05.碘缺乏病防治日",
    "08.世界红十字日",
    "12I国际护士节",
    "15I国际家庭日",
    "17.国际电信日",
    "18.国际博物馆日",
    "20.全国学生营养日",
    "23.国际牛奶日",
    "31I世界无烟日",
};

//6月
const char* sFtv_06[] =
{
    "01I1925-9999国际儿童节",
    "05.世界环境保护日",
    "06.全国爱眼日",
    "17.防治荒漠化和干旱日",
    "23.国际奥林匹克日",
    "25.全国土地日",
    "26I国际禁毒日",
};

//7月
const char* sFtv_07[] =
{
    "01I1997-9999香港回归纪念日", 
    "01I1921-9999中共诞辰", 
    "01.世界建筑日", 
    "02.国际体育记者日", 
    "07I1937-9999抗日战争纪念日", 
    "11I世界人口日", 
    "30.非洲妇女日",
};

//8月
const char* sFtv_08[] =
{
    "01I1927-9999建军节",
    "08.中国男子节(爸爸节)",
};

//9月
const char* sFtv_09[] =
{
    "03I1945-9999抗日战争胜利纪念", 
    "08.1966-9999国际扫盲日", 
    "08.国际新闻工作者日", 
    "09.毛泽东逝世纪念", 
    "10I中国教师节", 
    "14.世界清洁地球日", 
    "16.国际臭氧层保护日", 
    "18I九·一八事变纪念日", 
    "20.国际爱牙日", 
    "27.世界旅游日", 
    "28I孔子诞辰",
};

//10月
const char* sFtv_10[] =
{
    "01#1949-9999国庆节", 
    "01.世界音乐日", 
    "01.国际老人节", 
    "02#1949-9999国庆节假日", 
    "02.国际和平与民主自由斗争日", 
    "03#1949-9999国庆节假日",
    "04.世界动物日", 
    "06.老人节", 
    "08.全国高血压日", 
    "08.世界视觉日", 
    "09.世界邮政日", 
    "09.万国邮联日", 
    "10I辛亥革命纪念日", 
    "10.世界精神卫生日",
    "13.世界保健日", 
    "13.国际教师节", 
    "14.世界标准日", 
    "15.国际盲人节(白手杖节)", 
    "16.世界粮食日", 
    "17.世界消除贫困日", 
    "22.世界传统医药日", 
    "24.联合国日", 
    "31.世界勤俭日",
};

//11月
const char* sFtv_11[] =
{
    "07.1917-9999十月社会主义革命纪念日", 
    "08.中国记者日", 
    "09.全国消防安全宣传教育日", 
    "10.世界青年节", 
    "11.国际科学与和平周(本日所属的一周)", 
    "12.孙中山诞辰纪念日", 
    "14.世界糖尿病日", 
    "17.国际大学生节", 
    "17.世界学生节", 
    "20.彝族年", 
    "21.彝族年", 
    "21.世界问候日", 
    "21.世界电视日", 
    "22.彝族年",
    "29.国际声援巴勒斯坦人民国际日",
};

//12月
const char* sFtv_12[] =
{
    "01I1988-9999世界艾滋病日", 
    "03.世界残疾人日", 
    "05.国际经济和社会发展志愿人员日", 
    "08.国际儿童电视日", 
    "09.世界足球日", 
    "10.世界人权日", 
    "12I西安事变纪念日", 
    "13I南京大屠杀(1937年)纪念日", 
    "20.澳门回归纪念", 
    "21.国际篮球日", 
    "24I平安夜", 
    "25I圣诞节", 
    "26.毛泽东诞辰纪念",
};

MM_EXPORT_SXWNL const char** csLunarOBA_sFtv[] =
{
    sFtv_01, sFtv_02, sFtv_03, sFtv_04,
    sFtv_05, sFtv_06, sFtv_07, sFtv_08,
    sFtv_09, sFtv_10, sFtv_11, sFtv_12,
};

MM_EXPORT_SXWNL const int csLunarOBA_sFtvLength[] =
{
    csArraySize(sFtv_01),
    csArraySize(sFtv_02),
    csArraySize(sFtv_03),
    csArraySize(sFtv_04),
    csArraySize(sFtv_05),
    csArraySize(sFtv_06),
    csArraySize(sFtv_07),
    csArraySize(sFtv_08),
    csArraySize(sFtv_09),
    csArraySize(sFtv_10),
    csArraySize(sFtv_11),
    csArraySize(sFtv_12),
};

//冬月
const char* nFtv_01[] =
{
    "",
};

//腊月, 注意除夕要看大小月所以不在这张表
const char* nFtv_02[] =
{
    "08I腊八节",
    "23I北方小年",
    "24I南方小年",
};

//正月, 注意正月不是一月
const char* nFtv_03[] =
{
    "01#春节",
    "02#大年初二",
    "15#元宵节",
    "15I上元节",
    "15.壮族歌墟节 苗族踩山节 达斡尔族卡钦",
    "16.侗族芦笙节(至正月二十)",
    "25.填仓节",
    "29.送穷日",
};

//二月
const char* nFtv_04[] =
{
    "01.瑶族忌鸟节",
    "02I春龙节(龙抬头)",
    "02.畲族会亲节",
    "08.傈傈族刀杆节",
};

//三月
const char* nFtv_05[] =
{
    "03I北帝诞",
    "03.苗族黎族歌墟节",
};

//四月
const char* nFtv_06[] =
{
    "08I牛王诞",
    "18.锡伯族西迁节",
};

//五月
const char* nFtv_07[] =
{
    "05#端午节",
    "13I关帝诞",
    "13.阿昌族泼水节",
    "22.鄂温克族米阔鲁节",
    "29.瑶族达努节",
};

//六月
const char* nFtv_08[] =
{
    "06I姑姑节 天贶节",
    "06.壮族祭田节 瑶族尝新节",
    "24.火把节、星回节(彝、白、佤、阿昌、纳西、基诺族 )",
};

//七月
const char* nFtv_09[] =
{
    "07I七夕(中国情人节,乞巧节,女儿节)",
    "13.侗族吃新节",
    "15.中元节 鬼节",
};

//八月
const char* nFtv_10[] =
{
    "15#中秋节",
};

//九月
const char* nFtv_11[] =
{
    "09I重阳节",
};

//十月
const char* nFtv_12[] =
{
    "01I祭祖节(十月朝)",
    "15I下元节",
    "16.瑶族盘王节",
};

//一月, 注意一月不是正月
const char* nFtv_13[] =
{
    "",
};

//冰月, 十二月的别称
const char* nFtv_14[] =
{
    "",
};

MM_EXPORT_SXWNL const char** csLunarOBB_nFtv[] =
{
    nFtv_01, nFtv_02, nFtv_03, nFtv_04,
    nFtv_05, nFtv_06, nFtv_07, nFtv_08,
    nFtv_09, nFtv_10, nFtv_11, nFtv_12,
    nFtv_13,nFtv_14,
};

MM_EXPORT_SXWNL const int csLunarOBB_nFtvLength[] =
{
    csArraySize(nFtv_01),
    csArraySize(nFtv_02),
    csArraySize(nFtv_03),
    csArraySize(nFtv_04),
    csArraySize(nFtv_05),
    csArraySize(nFtv_06),
    csArraySize(nFtv_07),
    csArraySize(nFtv_08),
    csArraySize(nFtv_09),
    csArraySize(nFtv_10),
    csArraySize(nFtv_11),
    csArraySize(nFtv_12),
    csArraySize(nFtv_13),
    csArraySize(nFtv_14),
};

