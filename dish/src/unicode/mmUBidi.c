/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmUBidi.h"
#include "mmUString.h"
#include "mmUBidiType.h"
#include "mmUBidiTypeLookup.h"

U_STABLE UBiDiDirection U_EXPORT2
mmUBidi_GetBaseDirection(const UChar* text, int32_t length)
{
    int32_t i;
    UChar32 uchar;
    UBiDiDirection dir;

    if(text == NULL || length < -1)
    {
        return UBIDI_NEUTRAL;
    }

    if(length == -1)
    {
        length = mmUString_Strlen(text);
    }

    for(i = 0;i < length;)
    {
        /* i is incremented by U16_NEXT */
        U16_NEXT(text, i, length, uchar);
        dir = mmUBidi_LookupBidiType(uchar);
        if(dir == mmBidiTypeL)
        {
            return UBIDI_LTR;
        }
        if(dir == mmBidiTypeR || dir == mmBidiTypeAL)
        {
            return UBIDI_RTL;
        }
    }
    return UBIDI_NEUTRAL;
}
