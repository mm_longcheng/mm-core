/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUBidiType_h__
#define __mmUBidiType_h__

#include "core/mmCore.h"

#include "core/mmPrefix.h"

/**
 * Constants that specify the bidirectional types of a character.
 */
enum
{
    mmBidiTypeNil = 0x00,

    mmBidiTypeL   = 0x01,   /**< Strong: Left-to-Right */
    mmBidiTypeR   = 0x02,   /**< Strong: Right-to-Left */
    mmBidiTypeAL  = 0x03,   /**< Strong: Right-to-Left Arabic */

    mmBidiTypeBN  = 0x04,   /**< Weak: Boundary Neutral */
    mmBidiTypeNSM = 0x05,   /**< Weak: Non-Spacing Mark */
    mmBidiTypeAN  = 0x06,   /**< Weak: Arabic Number */
    mmBidiTypeEN  = 0x07,   /**< Weak: European Number */
    mmBidiTypeET  = 0x08,   /**< Weak: European Number Terminator */
    mmBidiTypeES  = 0x09,   /**< Weak: European Number Separator */
    mmBidiTypeCS  = 0x0A,   /**< Weak: Common Number Separator */

    mmBidiTypeWS  = 0x0B,   /**< Neutral: White Space */
    mmBidiTypeS   = 0x0C,   /**< Neutral: Segment Separator */
    mmBidiTypeB   = 0x0D,   /**< Neutral: Paragraph Separator */
    mmBidiTypeON  = 0x0E,   /**< Neutral: Other Neutral */

    mmBidiTypeLRI = 0x0F,   /**< Format: Left-to-Right Isolate */
    mmBidiTypeRLI = 0x10,   /**< Format: Right-to-Left Isolate */
    mmBidiTypeFSI = 0x11,   /**< Format: First Strong Isolate */
    mmBidiTypePDI = 0x12,   /**< Format: Pop Directional Isolate */
    mmBidiTypeLRE = 0x13,   /**< Format: Left-to-Right Embedding */
    mmBidiTypeRLE = 0x14,   /**< Format: Right-to-Left Embedding */
    mmBidiTypeLRO = 0x15,   /**< Format: Left-to-Right Override */
    mmBidiTypeRLO = 0x16,   /**< Format: Right-to-Left Override */
    mmBidiTypePDF = 0x17    /**< Format: Pop Directional Formatting */
};

/**
 * <code>UBiDiDirection</code> values indicate the text direction.
 * @stable ICU 2.0
 */
enum UBiDiDirection
{
    /** Left-to-right text. This is a 0 value.
     * <ul>
     * <li>As return value for <code>ubidi_getDirection()</code>, it means
     *     that the source string contains no right-to-left characters, or
     *     that the source string is empty and the paragraph level is even.
     * <li> As return value for <code>ubidi_getBaseDirection()</code>, it
     *      means that the first strong character of the source string has
     *      a left-to-right direction.
     * </ul>
     * @stable ICU 2.0
     */
    UBIDI_LTR,
    /** Right-to-left text. This is a 1 value.
     * <ul>
     * <li>As return value for <code>ubidi_getDirection()</code>, it means
     *     that the source string contains no left-to-right characters, or
     *     that the source string is empty and the paragraph level is odd.
     * <li> As return value for <code>ubidi_getBaseDirection()</code>, it
     *      means that the first strong character of the source string has
     *      a right-to-left direction.
     * </ul>
     * @stable ICU 2.0
     */
    UBIDI_RTL,
    /** Mixed-directional text.
     * <p>As return value for <code>ubidi_getDirection()</code>, it means
     *    that the source string contains both left-to-right and
     *    right-to-left characters.
     * @stable ICU 2.0
     */
    UBIDI_MIXED,
    /** No strongly directional text.
     * <p>As return value for <code>ubidi_getBaseDirection()</code>, it means
     *    that the source string is missing or empty, or contains neither left-to-right
     *    nor right-to-left characters.
     * @stable ICU 4.6
     */
    UBIDI_NEUTRAL,
};

/** @stable ICU 2.0 */
typedef enum UBiDiDirection UBiDiDirection;

#include "core/mmSuffix.h"

#endif//__mmUBidiType_h__
