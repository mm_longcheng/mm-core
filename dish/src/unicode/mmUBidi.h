/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUBidi_h__
#define __mmUBidi_h__

#include "core/mmCore.h"

#include "unicode/mmUMachine.h"
#include "unicode/mmUBidiType.h"

#include "core/mmPrefix.h"

/**
 * Gets the base direction of the text provided according
 * to the Unicode Bidirectional Algorithm. The base direction
 * is derived from the first character in the string with bidirectional
 * character type L, R, or AL. If the first such character has type L,
 * <code>UBIDI_LTR</code> is returned. If the first such character has
 * type R or AL, <code>UBIDI_RTL</code> is returned. If the string does
 * not contain any character of these types, then
 * <code>UBIDI_NEUTRAL</code> is returned.
 *
 * This is a lightweight function for use when only the base direction
 * is needed and no further bidi processing of the text is needed.
 *
 * @param text is a pointer to the text whose base
 *             direction is needed.
 * Note: the text must be (at least) @c length long.
 *
 * @param length is the length of the text;
 *               if <code>length==-1</code> then the text
 *               must be zero-terminated.
 *
 * @return  <code>UBIDI_LTR</code>, <code>UBIDI_RTL</code>,
 *          <code>UBIDI_NEUTRAL</code>
 *
 * @see UBiDiDirection
 * @stable ICU 4.6
 */
U_STABLE UBiDiDirection U_EXPORT2
mmUBidi_GetBaseDirection(const UChar* text, int32_t length);

#include "core/mmSuffix.h"

#endif//__mmUBidi_h__
