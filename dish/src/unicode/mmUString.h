/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUString_h__
#define __mmUString_h__

#include "core/mmCore.h"

#include "unicode/mmUMachine.h"

#include "unicode/mmUTF.h"
#include "unicode/mmUTF8.h"
#include "unicode/mmUTF16.h"

#include "core/mmPrefix.h"

// const UChar* empty.
U_STABLE extern const UChar* U_EXPORT2
mmUStringEmpty;

/**
 * Determine the length of an array of UChar.
 *
 * @param s The array of UChars, NULL (U+0000) terminated.
 * @return The number of UChars in <code>chars</code>, minus the terminator.
 * @stable ICU 2.0
 */
U_STABLE int32_t U_EXPORT2
mmUString_Strlen(const UChar* s);

/**
 * Compare two Unicode strings for bitwise equality (code unit order).
 *
 * @param s1 A string to compare.
 * @param s2 A string to compare.
 * @return 0 if <code>s1</code> and <code>s2</code> are bitwise equal; a negative
 * value if <code>s1</code> is bitwise less than <code>s2,</code>; a positive
 * value if <code>s1</code> is bitwise greater than <code>s2</code>.
 * @stable ICU 2.0
 */
U_STABLE int32_t U_EXPORT2
mmUString_Strcmp(const UChar* s1, const UChar* s2);

/**
 * Compare two ustrings for bitwise equality.
 * Compares at most <code>n</code> characters.
 *
 * @param s1 A string to compare (can be NULL/invalid if n<=0).
 * @param s2 A string to compare (can be NULL/invalid if n<=0).
 * @param n The maximum number of characters to compare; always returns 0 if n<=0.
 * @return 0 if <code>s1</code> and <code>s2</code> are bitwise equal; a negative
 * value if <code>s1</code> is bitwise less than <code>s2</code>; a positive
 * value if <code>s1</code> is bitwise greater than <code>s2</code>.
 * @stable ICU 2.0
 */
U_STABLE int32_t U_EXPORT2
mmUString_Strncmp(const UChar* s1, const UChar* s2, int32_t n);

/**
 * Copy a ustring. Adds a null terminator.
 *
 * @param dst The destination string.
 * @param src The source string.
 * @return A pointer to <code>dst</code>.
 * @stable ICU 2.0
 */
U_STABLE UChar* U_EXPORT2
mmUString_Strcpy(UChar* dst, const UChar* src);

/**
 * Copy a ustring.
 * Copies at most <code>n</code> characters.  The result will be null terminated
 * if the length of <code>src</code> is less than <code>n</code>.
 *
 * @param dst The destination string.
 * @param src The source string (can be NULL/invalid if n<=0).
 * @param n The maximum number of characters to copy; no-op if <=0.
 * @return A pointer to <code>dst</code>.
 * @stable ICU 2.0
 */
U_STABLE UChar* U_EXPORT2
mmUString_Strncpy(UChar* dst, const UChar* src, int32_t n);

/**
 * Count Unicode code points in the length UChar code units of the string.
 * A code point may occupy either one or two UChar code units.
 * Counting code points involves reading all code units.
 *
 * This functions is basically the inverse of the U16_FWD_N() macro (see utf.h).
 *
 * @param s The input string.
 * @param length The number of UChar code units to be checked, or -1 to count all
 *               code points before the first NUL (U+0000).
 * @return The number of code points in the specified code units.
 * @stable ICU 2.0
 */
U_STABLE int32_t U_EXPORT2
mmUString_CountChar32(const UChar* s, int32_t length);

#include "core/mmSuffix.h"

#endif//__mmUString_h__
