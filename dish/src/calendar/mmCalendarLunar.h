/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCalendarLunar_h__
#define __mmCalendarLunar_h__

#include "core/mmPlatform.h"

#include "calendar/mmCalendarType.h"
#include "calendar/mmCalendarVector.h"

#include "calendar/mmCalendarExport.h"

#include "core/mmPrefix.h"

//年月日
struct cs_lunar_nyr
{
    int y;//年
    int m;//月
    int d;//日
};
//月
struct cs_lunar_yue
{
    int d0;     //月首的J2000.0起算的儒略日数
    int dn;     //本月的天数
    int week0;  //月首的星期
    int weeki;  //本日所在的周序号
    int weekN;  //本月的总周数
};
//天干地支
struct cs_lunar_tgdz
{
    char g;// 干
    char z;// 支
};
//干支四柱
struct cs_lunar_gzsz
{
    struct cs_lunar_tgdz gz_jn; //干支纪年
    struct cs_lunar_tgdz gz_jy; //干支纪月
    struct cs_lunar_tgdz gz_jr; //干支纪日
    struct cs_lunar_tgdz gz_js; //干支纪时
    cs_real gz_zty;             //干支真太阳时
};

//命理八字计算。jd为格林尼治UT(J2000起算),J为本地经度,返回在物件re中
MM_EXPORT_CALENDAR void cs_lunar_mingLiBaZi(cs_real jd, cs_real J, struct cs_lunar_gzsz* re);
//回历计算,J2000.0起算的儒略日数
MM_EXPORT_CALENDAR void cs_lunar_getHuiLi(cs_real d0, struct cs_lunar_nyr* re);
//精气
MM_EXPORT_CALENDAR cs_real cs_lunar_qi_accurate(cs_real W);
//精朔
MM_EXPORT_CALENDAR cs_real cs_lunar_so_accurate(cs_real W);
//精气
MM_EXPORT_CALENDAR cs_real cs_lunar_qi_accurate2(cs_real jd);
//精朔
MM_EXPORT_CALENDAR cs_real cs_lunar_so_accurate2(cs_real jd);

#include "core/mmSuffix.h"

#endif//__mmCalendarLunar_h__
