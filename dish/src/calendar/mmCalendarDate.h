/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCalendarDate_h__
#define __mmCalendarDate_h__

#include "core/mmPlatform.h"
#include "core/mmString.h"

#include "calendar/mmCalendarType.h"

#include "calendar/mmCalendarExport.h"

#include "core/mmPrefix.h"

//=================================日期计算--=======================================
//==================================================================================
// we not use int but prefer cs_real, because we have some like floor(D/30.601) algorithm.
struct cs_date
{
    cs_real Y;// 年
    cs_real M;// 月
    cs_real D;// 日
    cs_real h;// 时
    cs_real m;// 分
    cs_real s;// 秒
};
MM_EXPORT_CALENDAR void cs_date_init(struct cs_date* p);
MM_EXPORT_CALENDAR void cs_date_init_arg(struct cs_date* p, cs_real _Y, cs_real _M, cs_real _D, cs_real _h, cs_real _m, cs_real _s);
//公历转儒略日
MM_EXPORT_CALENDAR cs_real cs_date_toJD(struct cs_date* p);
//儒略日数转公历
MM_EXPORT_CALENDAR void cs_date_setFromJD(struct cs_date* p, cs_real jd);
//日期转为串
MM_EXPORT_CALENDAR void cs_date_DD2str(struct cs_date* p, struct mmString* str);
//////////////////////////////////////////////////////////////////////////
//提取jd中的时间(去除日期)
MM_EXPORT_CALENDAR void cs_date_timeStr(cs_real jd, struct mmString* str);
//星期计算
MM_EXPORT_CALENDAR int cs_date_getWeek(cs_real jd);
//求y年m月的第n个星期w的儒略日数
MM_EXPORT_CALENDAR cs_real cs_date_nnweek(cs_real y, cs_real m, cs_real n, cs_real w);
//公历转儒略日
MM_EXPORT_CALENDAR cs_real cs_date_JD(cs_real y, cs_real m, cs_real d);
//JD转为串
MM_EXPORT_CALENDAR void cs_date_JD2str(cs_real jd, struct mmString* str);

#include "core/mmSuffix.h"

#endif//__mmCalendarDate_h__
