/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCalendarI2s_h__
#define __mmCalendarI2s_h__

#include "core/mmPlatform.h"

#include "calendar/mmCalendarVector.h"

#include "calendar/mmCalendarExport.h"

#include "core/mmPrefix.h"

// calendar int to string
// common name mapping.
// const string.
// 行星名
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_xx_name;
// 星期
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_Week;
// 中文数字
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_Number;
// 干
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_Gan;
// 支
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_Zhi;
// 生肖
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_ShX;
// 星座
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_XiZ;
// 月相名称表
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_yxmc;
// 节气名称表
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_jqmc;
// 月名称,建寅
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_ymc;
// 日名称,初一
MM_EXPORT_CALENDAR extern const struct cs_arrs cs_rmc;

#include "core/mmSuffix.h"

#endif//__mmCalendarI2s_h__
