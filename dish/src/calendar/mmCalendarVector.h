/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCalendarVector_h__
#define __mmCalendarVector_h__

#include "core/mmPlatform.h"

#include "calendar/mmCalendarType.h"

#include "calendar/mmCalendarExport.h"

#include "core/mmPrefix.h"

struct cs_vector3
{
    cs_real x;
    cs_real y;
    cs_real z;
};
MM_INLINE static cs_real* cs_vector3_at(struct cs_vector3* p, unsigned int i)
{
    return &p->x + i;
}
struct cs_vector4
{
    cs_real x;
    cs_real y;
    cs_real z;
    cs_real w;
};
// cs_real array.
struct cs_arr1
{
    size_t len;
    const cs_real* d;
};
// struct cs_arr1 array.
struct cs_arr2
{
    size_t len;
    const struct cs_arr1* d;
};
// struct cs_arr2 array.
struct cs_arr3
{
    size_t len;
    const struct cs_arr2* d;
};
// const char* array.
struct cs_arrs
{
    size_t len;
    const char** d;
};
// const int array.
struct cs_arri
{
    size_t len;
    const int* d;
};
//get count from array like int arr[].
#define cs_array_size( a )  ( sizeof( (a) ) / sizeof( (a[0]) ) )

#include "core/mmSuffix.h"

#endif//__mmCalendarVector_h__
