/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIntegerLog2_h__
#define __mmIntegerLog2_h__

/*
 *  come from boost::integer_log2.
 *  https://www.boost.org
 *  less depend.
*/

#include <limits>

namespace mm
{
    namespace detail
    {
        template<typename T>
        int integer_log2_impl(T x, int n)
        {
            int result = 0;

            while (x != 1)
            {
                const T t = static_cast<T>(x >> n);
                if (t)
                {
                    result += n;
                    x = t;
                }
                n /= 2;

            }
            return result;
        }

        // helper to find the maximum power of two
        // less than p (more involved than necessary,
        // to avoid PTS)
        //
        template<int p, int n>
        struct max_pow2_less
        {
            enum { c = 2 * n < p };
            static const int value = (c ? (max_pow2_less< c*p, 2 * c*n>::value) : n);
        };

        template<>
        struct max_pow2_less<0, 0>
        {
            static const int value = (0);
        };

        // this template is here just for Borland :(
        // we could simply rely on numeric_limits but sometimes
        // Borland tries to use numeric_limits<const T>, because
        // of its usual const-related problems in argument deduction
        // - gps
        template<typename T>
        struct width
        {
#ifdef __BORLANDC__
            static const int value = (sizeof(T) * CHAR_BIT);
#else
            static const int value = (std::numeric_limits<T>::digits);
#endif
        };
    }
    // ---------
    // integer_log2
    // ---------------
    //
    template<typename T>
    int integer_log2(T x)
    {

        assert(x > 0);

        const int n = detail::max_pow2_less<
            detail::width<T> ::value, 4
        > ::value;

        return detail::integer_log2_impl(x, n);
    }
}

#endif//__mmIntegerLog2_h__
