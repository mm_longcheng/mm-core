/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDynamicBitset_h__
#define __mmDynamicBitset_h__

/*
 *  come from boost::dynamic_bitset.
 *  https://www.boost.org
 *  less depend.
 *
 *  I change is_subset_of is_proper_subset_of api. not assert the size.
 *  mm_dynamic_bitset<uintptr_t> x0(32, 0xFF);
 *  mm_dynamic_bitset<uintptr_t> x1(16, 0x0F);
 *  bool value = x1.is_subset_of(x0);// value = true.
*/

#include "mmDynamicBitsetImpl.h"
#include "mmMakeNonConst.h"
#include "mmLowestBit.h"

#include <assert.h>

#include <algorithm>
#include <stdexcept>
#include <vector>
#include <ios>

// MM_NO_CXX11_ALLOCATOR
// MM_NO_INTRINSIC_WCHAR_T
// MM_NO_LONG_LONG

namespace mm
{
    template<
        typename Block = unsigned long,
        typename Allocator = std::allocator<Block> >
    class mm_dynamic_bitset;

    template<typename Block, typename Allocator>
    class mm_dynamic_bitset
    {
    public:
        typedef std::vector<Block, Allocator> buffer_type;
    public:
        typedef Block block_type;
        typedef Allocator allocator_type;
        typedef std::size_t size_type;
        typedef typename buffer_type::size_type block_width_type;

        static const int bits_per_block = (std::numeric_limits<Block>::digits);
        static const size_type npos = static_cast<size_type>(-1);
    public:
        // A proxy class to simulate lvalues of bit type.
        //
        class reference
        {
            friend class mm_dynamic_bitset<Block, Allocator>;


            // the one and only non-copy ctor
            reference(block_type & b, block_width_type pos)
                : m_block(b)
                , m_mask((assert(pos < bits_per_block), block_type(1) << pos))
            {

            }

            void operator&(); // left undefined

        public:

            // copy constructor: compiler generated

            operator bool() const { return (m_block & m_mask) != 0; }
            bool operator~() const { return (m_block & m_mask) == 0; }

            reference& flip() { do_flip(); return *this; }

            reference& operator=(bool x) { do_assign(x);   return *this; } // for b[i] = x
            reference& operator=(const reference& rhs) { do_assign(rhs); return *this; } // for b[i] = b[j]

            reference& operator|=(bool x) { if (x) do_set();   return *this; }
            reference& operator&=(bool x) { if (!x) do_reset(); return *this; }
            reference& operator^=(bool x) { if (x) do_flip();  return *this; }
            reference& operator-=(bool x) { if (x) do_reset(); return *this; }

        private:
            block_type & m_block;
            const block_type m_mask;

            void do_set() { m_block |= m_mask; }
            void do_reset() { m_block &= ~m_mask; }
            void do_flip() { m_block ^= m_mask; }
            void do_assign(bool x) { x ? do_set() : do_reset(); }
        };

        typedef bool const_reference;
    private:
        buffer_type m_bits;
        size_type   m_num_bits;
    public:
        // constructors, etc.
        explicit mm_dynamic_bitset(const Allocator& alloc = Allocator());

        explicit mm_dynamic_bitset(size_type num_bits, unsigned long value = 0, const Allocator& alloc = Allocator());

        // WARNING: you should avoid using this constructor.
        //
        //  A conversion from string is, in most cases, formatting,
        //  and should be performed by using operator>>.
        //
        // NOTE:
        //  Leave the parentheses around std::basic_string<CharT, Traits, Alloc>::npos.
        //  g++ 3.2 requires them and probably the standard will - see core issue 325
        // NOTE 2:
        //  split into two constructors because of bugs in MSVC 6.0sp5 with STLport

        template<typename CharT, typename Traits, typename Alloc>
        mm_dynamic_bitset(const std::basic_string<CharT, Traits, Alloc>& s,
            typename std::basic_string<CharT, Traits, Alloc>::size_type pos,
            typename std::basic_string<CharT, Traits, Alloc>::size_type n,
            size_type num_bits = npos,
            const Allocator& alloc = Allocator())
            : m_bits(alloc)
            , m_num_bits(0)
        {
            init_from_string(s, pos, n, num_bits);
        }

        template<typename CharT, typename Traits, typename Alloc>
        explicit mm_dynamic_bitset(const std::basic_string<CharT, Traits, Alloc>& s,
            typename std::basic_string<CharT, Traits, Alloc>::size_type pos = 0)
            : m_bits(Allocator())
            , m_num_bits(0)
        {
            init_from_string(s, pos, (std::basic_string<CharT, Traits, Alloc>::npos),
                npos);
        }

        // The first bit in *first is the least significant bit, and the
        // last bit in the block just before *last is the most significant bit.
        template<typename BlockInputIterator>
        mm_dynamic_bitset(BlockInputIterator first, BlockInputIterator last,
            const Allocator& alloc = Allocator())
            : m_bits(alloc)
            , m_num_bits(0)
        {
            using detail::dynamic_bitset_impl::value_to_type;
            using detail::dynamic_bitset_impl::is_numeric;

            const value_to_type<is_numeric<BlockInputIterator>::value> selector;

            dispatch_init(first, last, selector);
        }

        template<typename T>
        void dispatch_init(T num_bits, unsigned long value, detail::dynamic_bitset_impl::value_to_type<true>)
        {
            init_from_unsigned_long(static_cast<size_type>(num_bits), value);
        }

        template<typename T>
        void dispatch_init(T first, T last, detail::dynamic_bitset_impl::value_to_type<false>)
        {
            init_from_block_range(first, last);
        }

        template<typename BlockIter>
        void init_from_block_range(BlockIter first, BlockIter last)
        {
            assert(m_bits.size() == 0);
            m_bits.insert(m_bits.end(), first, last);
            m_num_bits = m_bits.size() * bits_per_block;
        }

        // copy constructor
        mm_dynamic_bitset(const mm_dynamic_bitset& b);

        ~mm_dynamic_bitset();
    public:
        void swap(mm_dynamic_bitset& b);
        mm_dynamic_bitset& operator=(const mm_dynamic_bitset& b);

        allocator_type get_allocator() const;

        // size changing operations
        void resize(size_type num_bits, bool value = false);
        void clear();
        void push_back(bool bit);
        void pop_back();
        void append(Block block);
    public:
        // bitset operations
        mm_dynamic_bitset & operator&=(const mm_dynamic_bitset& b);
        mm_dynamic_bitset& operator|=(const mm_dynamic_bitset& b);
        mm_dynamic_bitset& operator^=(const mm_dynamic_bitset& b);
        mm_dynamic_bitset& operator-=(const mm_dynamic_bitset& b);
        mm_dynamic_bitset& operator<<=(size_type n);
        mm_dynamic_bitset& operator>>=(size_type n);
        mm_dynamic_bitset operator<<(size_type n) const;
        mm_dynamic_bitset operator>>(size_type n) const;

        // basic bit operations
        mm_dynamic_bitset& set(size_type n, bool val = true);
        mm_dynamic_bitset& set();
        mm_dynamic_bitset& reset(size_type n);
        mm_dynamic_bitset& reset();
        mm_dynamic_bitset& flip(size_type n);
        mm_dynamic_bitset& flip();
        bool test(size_type n) const;
        bool test_set(size_type n, bool val = true);
        bool all() const;
        bool any() const;
        bool none() const;
        mm_dynamic_bitset operator~() const;
        size_type count() const noexcept;

        // subscript
        reference operator[](size_type pos)
        {
            return reference(m_bits[block_index(pos)], bit_index(pos));
        }
        bool operator[](size_type pos) const { return test(pos); }

        unsigned long to_ulong() const;

        size_type size() const noexcept;
        size_type num_blocks() const noexcept;
        size_type max_size() const noexcept;
        bool empty() const noexcept;
        size_type capacity() const noexcept;
        void reserve(size_type num_bits);
        void shrink_to_fit();

        bool is_subset_of(const mm_dynamic_bitset& a) const;
        bool is_proper_subset_of(const mm_dynamic_bitset& a) const;
        bool intersects(const mm_dynamic_bitset& a) const;

        // lookup
        size_type find_first() const;
        size_type find_next(size_type pos) const;
    public:
        // lexicographical comparison
        template<typename B, typename A>
        friend bool operator==(const mm_dynamic_bitset<B, A>& a, const mm_dynamic_bitset<B, A>& b);

        template<typename B, typename A>
        friend bool operator<(const mm_dynamic_bitset<B, A>& a, const mm_dynamic_bitset<B, A>& b);

        template<typename B, typename A>
        friend bool oplessthan(const mm_dynamic_bitset<B, A>& a, const mm_dynamic_bitset<B, A>& b);

        template<typename B, typename A, typename BlockOutputIterator>
        friend void to_block_range(const mm_dynamic_bitset<B, A>& b, BlockOutputIterator result);

        template<typename BlockIterator, typename B, typename A>
        friend void from_block_range(BlockIterator first, BlockIterator last, mm_dynamic_bitset<B, A>& result);


        template<typename CharT, typename Traits, typename B, typename A>
        friend std::basic_istream<CharT, Traits>& operator>>(std::basic_istream<CharT, Traits>& is, mm_dynamic_bitset<B, A>& b);

        template<typename B, typename A, typename stringT>
        friend void to_string_helper(const mm_dynamic_bitset<B, A> & b, stringT & s, bool dump_all);
    private:
        void m_zero_unused_bits();
        bool m_check_invariants() const;

        size_type m_do_find_from(size_type first_block) const;

        block_width_type count_extra_bits() const noexcept { return bit_index(size()); }
        static size_type block_index(size_type pos) noexcept { return pos / bits_per_block; }
        static block_width_type bit_index(size_type pos) noexcept { return static_cast<block_width_type>(pos % bits_per_block); }
        static Block bit_mask(size_type pos) noexcept { return Block(1) << bit_index(pos); }

        template<typename CharT, typename Traits, typename Alloc>
        void init_from_string(const std::basic_string<CharT, Traits, Alloc>& s,
            typename std::basic_string<CharT, Traits, Alloc>::size_type pos,
            typename std::basic_string<CharT, Traits, Alloc>::size_type n,
            size_type num_bits)
        {
            assert(pos <= s.size());

            typedef typename std::basic_string<CharT, Traits, Alloc> StrT;
            typedef typename StrT::traits_type Tr;

            const typename StrT::size_type rlen = (std::min)(n, s.size() - pos);
            const size_type sz = (num_bits != npos ? num_bits : rlen);
            m_bits.resize(calc_num_blocks(sz));
            m_num_bits = sz;


            const CharT one = '1';

            const size_type m = num_bits < rlen ? num_bits : rlen;
            typename StrT::size_type i = 0;
            for (; i < m; ++i)
            {

                const CharT c = s[(pos + m - 1) - i];

                assert(Tr::eq(c, one) || Tr::eq(c, '0'));

                if (Tr::eq(c, one))
                    set(i);

            }
        }

        void init_from_unsigned_long(size_type num_bits, unsigned long value/*, const Allocator& alloc*/)
        {

            assert(m_bits.size() == 0);

            m_bits.resize(calc_num_blocks(num_bits));
            m_num_bits = num_bits;

            typedef unsigned long num_type;
            typedef detail::dynamic_bitset_impl::shifter<num_type, bits_per_block, ulong_width> shifter;

            //if (num_bits == 0)
            //    return;

            // zero out all bits at pos >= num_bits, if any;
            // note that: num_bits == 0 implies value == 0
            if (num_bits < static_cast<size_type>(ulong_width))
            {
                const num_type mask = (num_type(1) << num_bits) - 1;
                value &= mask;
            }

            typename buffer_type::iterator it = m_bits.begin();
            for (; value; shifter::left_shift(value), ++it)
            {
                *it = static_cast<block_type>(value);
            }
        }
    private:
        static const block_width_type ulong_width = (std::numeric_limits<unsigned long>::digits);

        bool m_unchecked_test(size_type pos) const;
        static size_type calc_num_blocks(size_type num_bits);

        Block&        m_highest_block();
        const Block&  m_highest_block() const;
    };
    //=============================================================================
    // mm_dynamic_bitset implementation
    //-----------------------------------------------------------------------------
    // constructors, etc.

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>::mm_dynamic_bitset(const Allocator& alloc)
        : m_bits(alloc)
        , m_num_bits(0)
    {

    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>::mm_dynamic_bitset(size_type num_bits, unsigned long value, const Allocator& alloc)
        : m_bits(alloc)
        , m_num_bits(0)
    {
        init_from_unsigned_long(num_bits, value);
    }

    // copy constructor
    template<typename Block, typename Allocator>
    inline mm_dynamic_bitset<Block, Allocator>::mm_dynamic_bitset(const mm_dynamic_bitset& b)
        : m_bits(b.m_bits)
        , m_num_bits(b.m_num_bits)
    {

    }

    template<typename Block, typename Allocator>
    inline mm_dynamic_bitset<Block, Allocator>::~mm_dynamic_bitset()
    {
        assert(m_check_invariants());
    }

    template<typename Block, typename Allocator>
    inline void mm_dynamic_bitset<Block, Allocator>::swap(mm_dynamic_bitset<Block, Allocator>& b) // no throw
    {
        std::swap(m_bits, b.m_bits);
        std::swap(m_num_bits, b.m_num_bits);
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::operator=(const mm_dynamic_bitset<Block, Allocator>& b)
    {
        m_bits = b.m_bits;
        m_num_bits = b.m_num_bits;
        return *this;
    }
    template<typename Block, typename Allocator>
    inline typename mm_dynamic_bitset<Block, Allocator>::allocator_type mm_dynamic_bitset<Block, Allocator>::get_allocator() const
    {
        return m_bits.get_allocator();
    }

    //-----------------------------------------------------------------------------
    // size changing operations

    template<typename Block, typename Allocator>
    void mm_dynamic_bitset<Block, Allocator>::resize(size_type num_bits, bool value) // strong guarantee
    {
        const size_type old_num_blocks = num_blocks();
        const size_type required_blocks = calc_num_blocks(num_bits);

        const block_type v = value ? ~Block(0) : Block(0);

        if (required_blocks != old_num_blocks)
        {
            m_bits.resize(required_blocks, v); // s.g. (copy)
        }

        // At this point:
        //
        //  - if the buffer was shrunk, we have nothing more to do,
        //    except a call to m_zero_unused_bits()
        //
        //  - if it was enlarged, all the (used) bits in the new blocks have
        //    the correct value, but we have not yet touched those bits, if
        //    any, that were 'unused bits' before enlarging: if value == true,
        //    they must be set.

        if (value && (num_bits > m_num_bits))
        {
            const block_width_type extra_bits = count_extra_bits();
            if (extra_bits)
            {
                assert(old_num_blocks >= 1 && old_num_blocks <= m_bits.size());

                // Set them.
                m_bits[old_num_blocks - 1] |= (v << extra_bits);
            }
        }

        m_num_bits = num_bits;
        m_zero_unused_bits();
    }

    template<typename Block, typename Allocator>
    void mm_dynamic_bitset<Block, Allocator>::clear() // no throw
    {
        m_bits.clear();
        m_num_bits = 0;
    }


    template<typename Block, typename Allocator>
    void mm_dynamic_bitset<Block, Allocator>::push_back(bool bit)
    {
        const size_type sz = size();
        resize(sz + 1);
        set(sz, bit);
    }

    template<typename Block, typename Allocator>
    void mm_dynamic_bitset<Block, Allocator>::pop_back()
    {
        const size_type old_num_blocks = num_blocks();
        const size_type required_blocks = calc_num_blocks(m_num_bits - 1);

        if (required_blocks != old_num_blocks)
        {
            m_bits.pop_back();
        }

        --m_num_bits;
        m_zero_unused_bits();
    }


    template<typename Block, typename Allocator>
    void mm_dynamic_bitset<Block, Allocator>::append(Block value) // strong guarantee
    {
        const block_width_type r = count_extra_bits();

        if (r == 0)
        {
            // the buffer is empty, or all blocks are filled
            m_bits.push_back(value);
        }
        else
        {
            m_bits.push_back(value >> (bits_per_block - r));
            m_bits[m_bits.size() - 2] |= (value << r); // m_bits.size() >= 2
        }

        m_num_bits += bits_per_block;
        assert(m_check_invariants());
    }

    //-----------------------------------------------------------------------------
    // bitset operations
    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::operator&=(const mm_dynamic_bitset& rhs)
    {
        assert(size() == rhs.size());
        for (size_type i = 0; i < num_blocks(); ++i)
            m_bits[i] &= rhs.m_bits[i];
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::operator|=(const mm_dynamic_bitset& rhs)
    {
        assert(size() == rhs.size());
        for (size_type i = 0; i < num_blocks(); ++i)
            m_bits[i] |= rhs.m_bits[i];
        //m_zero_unused_bits();
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::operator^=(const mm_dynamic_bitset& rhs)
    {
        assert(size() == rhs.size());
        for (size_type i = 0; i < this->num_blocks(); ++i)
            m_bits[i] ^= rhs.m_bits[i];
        //m_zero_unused_bits();
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::operator-=(const mm_dynamic_bitset& rhs)
    {
        assert(size() == rhs.size());
        for (size_type i = 0; i < num_blocks(); ++i)
            m_bits[i] &= ~rhs.m_bits[i];
        //m_zero_unused_bits();
        return *this;
    }

    //
    // NOTE:
    //  Note that the 'if (r != 0)' is crucial to avoid undefined
    //  behavior when the left hand operand of >> isn't promoted to a
    //  wider type (because rs would be too large).
    //
    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::operator<<=(size_type n)
    {
        if (n >= m_num_bits)
            return reset();
        //else
        if (n > 0)
        {

            size_type    const last = num_blocks() - 1;  // num_blocks() is >= 1
            size_type    const div = n / bits_per_block; // div is <= last
            block_width_type const r = bit_index(n);
            block_type * const b = &m_bits[0];

            if (r != 0)
            {

                block_width_type const rs = bits_per_block - r;

                for (size_type i = last - div; i > 0; --i)
                {
                    b[i + div] = (b[i] << r) | (b[i - 1] >> rs);
                }
                b[div] = b[0] << r;

            }
            else
            {
                for (size_type i = last - div; i > 0; --i)
                {
                    b[i + div] = b[i];
                }
                b[div] = b[0];
            }

            // zero out div blocks at the less significant end
            std::fill_n(m_bits.begin(), div, static_cast<block_type>(0));

            // zero out any 1 bit that flowed into the unused part
            m_zero_unused_bits(); // thanks to Lester Gong

        }

        return *this;
    }


    //
    // NOTE:
    //  see the comments to operator <<=
    //
    template<typename B, typename A>
    mm_dynamic_bitset<B, A> & mm_dynamic_bitset<B, A>::operator>>=(size_type n)
    {
        if (n >= m_num_bits)
        {
            return reset();
        }
        //else
        if (n > 0)
        {
            size_type  const last = num_blocks() - 1; // num_blocks() is >= 1
            size_type  const div = n / bits_per_block;   // div is <= last
            block_width_type const r = bit_index(n);
            block_type * const b = &m_bits[0];


            if (r != 0)
            {

                block_width_type const ls = bits_per_block - r;

                for (size_type i = div; i < last; ++i)
                {
                    b[i - div] = (b[i] >> r) | (b[i + 1] << ls);
                }
                // r bits go to zero
                b[last - div] = b[last] >> r;
            }
            else
            {
                for (size_type i = div; i <= last; ++i)
                {
                    b[i - div] = b[i];
                }
                // note the '<=': the last iteration 'absorbs'
                // b[last-div] = b[last] >> 0;
            }
            // div blocks are zero filled at the most significant end
            std::fill_n(m_bits.begin() + (num_blocks() - div), div, static_cast<block_type>(0));
        }

        return *this;
    }


    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator> mm_dynamic_bitset<Block, Allocator>::operator<<(size_type n) const
    {
        mm_dynamic_bitset r(*this);
        return r <<= n;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator> mm_dynamic_bitset<Block, Allocator>::operator>>(size_type n) const
    {
        mm_dynamic_bitset r(*this);
        return r >>= n;
    }


    //-----------------------------------------------------------------------------
    // basic bit operations

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::set(size_type pos, bool val)
    {
        assert(pos < m_num_bits);

        if (val)
            m_bits[block_index(pos)] |= bit_mask(pos);
        else
            reset(pos);

        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::set()
    {
        std::fill(m_bits.begin(), m_bits.end(), static_cast<Block>(~0));
        m_zero_unused_bits();
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::reset(size_type pos)
    {
        assert(pos < m_num_bits);
#if defined __MWERKS__ && (__MWERKS__<= 0x3003) // 8.x
        // CodeWarrior 8 generates incorrect code when the &=~ is compiled,
        // use the |^ variation instead.. <grafik>
        m_bits[block_index(pos)] |= bit_mask(pos);
        m_bits[block_index(pos)] ^= bit_mask(pos);
#else
        m_bits[block_index(pos)] &= ~bit_mask(pos);
#endif
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::reset()
    {
        std::fill(m_bits.begin(), m_bits.end(), Block(0));
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::flip(size_type pos)
    {
        assert(pos < m_num_bits);
        m_bits[block_index(pos)] ^= bit_mask(pos);
        return *this;
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator>& mm_dynamic_bitset<Block, Allocator>::flip()
    {
        for (size_type i = 0; i < num_blocks(); ++i)
            m_bits[i] = ~m_bits[i];
        m_zero_unused_bits();
        return *this;
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::m_unchecked_test(size_type pos) const
    {
        return (m_bits[block_index(pos)] & bit_mask(pos)) != 0;
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::test(size_type pos) const
    {
        assert(pos < m_num_bits);
        return m_unchecked_test(pos);
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::test_set(size_type pos, bool val)
    {
        bool const b = test(pos);
        if (b != val)
        {
            set(pos, val);
        }
        return b;
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::all() const
    {
        if (empty())
        {
            return true;
        }

        const block_width_type extra_bits = count_extra_bits();
        block_type const all_ones = static_cast<Block>(~0);

        if (extra_bits == 0)
        {
            for (size_type i = 0, e = num_blocks(); i < e; ++i)
            {
                if (m_bits[i] != all_ones)
                {
                    return false;
                }
            }
        }
        else
        {
            for (size_type i = 0, e = num_blocks() - 1; i < e; ++i)
            {
                if (m_bits[i] != all_ones)
                {
                    return false;
                }
            }
            const block_type mask = (block_type(1) << extra_bits) - 1;
            if (m_highest_block() != mask)
            {
                return false;
            }
        }
        return true;
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::any() const
    {
        for (size_type i = 0; i < num_blocks(); ++i)
            if (m_bits[i])
                return true;
        return false;
    }

    template<typename Block, typename Allocator>
    inline bool mm_dynamic_bitset<Block, Allocator>::none() const
    {
        return !any();
    }

    template<typename Block, typename Allocator>
    mm_dynamic_bitset<Block, Allocator> mm_dynamic_bitset<Block, Allocator>::operator~() const
    {
        mm_dynamic_bitset b(*this);
        b.flip();
        return b;
    }

    template<typename Block, typename Allocator>
    typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::count() const noexcept
    {
        using detail::dynamic_bitset_impl::table_width;
        using detail::dynamic_bitset_impl::access_by_bytes;
        using detail::dynamic_bitset_impl::access_by_blocks;
        using detail::dynamic_bitset_impl::value_to_type;

#if (__GNUC__ == 4) && (__GNUC_MINOR__ == 3) && (__GNUC_PATCHLEVEL__ == 3)
        // NOTE: Explicit qualification of "bits_per_block"
        //       breaks compilation on gcc 4.3.3
        enum { no_padding = bits_per_block == CHAR_BIT * sizeof(Block) };
#else
        // NOTE: Explicitly qualifying "bits_per_block" to workaround
        //       regressions of gcc 3.4.x
        enum
        {
            no_padding = mm_dynamic_bitset<Block, Allocator>::bits_per_block == CHAR_BIT * sizeof(Block)
        };
#endif

        enum { enough_table_width = table_width >= CHAR_BIT };

        enum
        {
            mode = (no_padding && enough_table_width) ? access_by_bytes : access_by_blocks
        };

        return do_count(m_bits.begin(), num_blocks(), Block(0), static_cast<value_to_type<(bool)mode> *>(0));
    }
    template<typename Block, typename Allocator>
    unsigned long mm_dynamic_bitset<Block, Allocator>::to_ulong() const
    {

        if (m_num_bits == 0)
            return 0; // convention

                      // Check for overflows. This may be a performance burden on very
                      // large bitsets but is required by the specification, sorry
        if (find_next(ulong_width - 1) != npos)
            throw (std::overflow_error("mm_dynamic_bitset::to_ulong overflow"));

        // Ok, from now on we can be sure there's no "on" bit
        // beyond the "allowed" positions
        typedef unsigned long result_type;

        const size_type maximum_size = (std::min)(m_num_bits, static_cast<size_type>(ulong_width));

        const size_type last_block = block_index(maximum_size - 1);

        assert((last_block * bits_per_block) < static_cast<size_type>(ulong_width));

        result_type result = 0;
        for (size_type i = 0; i <= last_block; ++i)
        {
            const size_type offset = i * bits_per_block;
            result |= (static_cast<result_type>(m_bits[i]) << offset);
        }

        return result;
    }

    template<typename Block, typename Allocator>
    inline typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::size() const noexcept
    {
        return m_num_bits;
    }

    template<typename Block, typename Allocator>
    inline typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::num_blocks() const noexcept
    {
        return m_bits.size();
    }

    template<typename Block, typename Allocator>
    inline typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::max_size() const noexcept
    {
        // Semantics of vector<>::max_size() aren't very clear
        // (see lib issue 197) and many library implementations
        // simply return dummy values, _unrelated_ to the underlying
        // allocator.
        //
        // Given these problems, I was tempted to not provide this
        // function at all but the user could need it if he provides
        // his own allocator.
        //

        const size_type m = detail::dynamic_bitset_impl::vector_max_size_workaround(m_bits);

        return m <= (size_type(-1) / bits_per_block) ?
            m * bits_per_block :
            size_type(-1);
    }

    template<typename Block, typename Allocator>
    inline bool mm_dynamic_bitset<Block, Allocator>::empty() const noexcept
    {
        return size() == 0;
    }

    template<typename Block, typename Allocator>
    inline typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::capacity() const noexcept
    {
        return m_bits.capacity() * bits_per_block;
    }

    template<typename Block, typename Allocator>
    inline void mm_dynamic_bitset<Block, Allocator>::reserve(size_type num_bits)
    {
        m_bits.reserve(calc_num_blocks(num_bits));
    }

    template<typename Block, typename Allocator>
    void mm_dynamic_bitset<Block, Allocator>::shrink_to_fit()
    {
        if (m_bits.size() < m_bits.capacity())
        {
            buffer_type(m_bits).swap(m_bits);
        }
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::is_subset_of(const mm_dynamic_bitset<Block, Allocator>& a) const
    {
        //assert(size() == a.size());
        //for (size_type i = 0; i < num_blocks(); ++i)
        //  if (m_bits[i] & ~a.m_bits[i])
        //      return false;

        if (size() > a.size())
        {
            return false;
        }
        else
        {
            for (size_type i = 0; i < num_blocks(); ++i)
                if (m_bits[i] & ~a.m_bits[i])
                    return false;
        }
        return true;
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::is_proper_subset_of(const mm_dynamic_bitset<Block, Allocator>& a) const
    {
        //assert(size() == a.size());
        //assert(num_blocks() == a.num_blocks());

        //bool proper = false;
        //for (size_type i = 0; i < num_blocks(); ++i) 
        //{
        //  const Block & bt = m_bits[i];
        //  const Block & ba = a.m_bits[i];

        //  if (bt & ~ba)
        //      return false; // not a subset at all
        //  if (ba & ~bt)
        //      proper = true;
        //}
        //return proper;

        bool proper = false;
        if (size() > a.size())
        {
            return false;
        }
        else
        {
            for (size_type i = 0; i < num_blocks(); ++i)
            {
                const Block & bt = m_bits[i];
                const Block & ba = a.m_bits[i];

                if (bt & ~ba)
                    return false; // not a subset at all
                if (ba & ~bt)
                    proper = true;
            }
        }
        return proper;
    }

    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::intersects(const mm_dynamic_bitset & b) const
    {
        size_type common_blocks = num_blocks() < b.num_blocks() ? num_blocks() : b.num_blocks();

        for (size_type i = 0; i < common_blocks; ++i)
        {
            if (m_bits[i] & b.m_bits[i])
                return true;
        }
        return false;
    }

    template<typename Block, typename Allocator>
    typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::find_first() const
    {
        return m_do_find_from(0);
    }


    template<typename Block, typename Allocator>
    typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::find_next(size_type pos) const
    {

        const size_type sz = size();
        if (pos >= (sz - 1) || sz == 0)
            return npos;

        ++pos;

        const size_type blk = block_index(pos);
        const block_width_type ind = bit_index(pos);

        // shift bits upto one immediately after current
        const Block fore = m_bits[blk] >> ind;

        return fore ?
            pos + static_cast<size_type>(lowest_bit(fore))
            :
            m_do_find_from(blk + 1);

    }

    // If size() is not a multiple of bits_per_block
    // then not all the bits in the last block are used.
    // This function resets the unused bits (convenient
    // for the implementation of many member functions)
    //
    template<typename Block, typename Allocator>
    inline void mm_dynamic_bitset<Block, Allocator>::m_zero_unused_bits()
    {
        assert(num_blocks() == calc_num_blocks(m_num_bits));

        // if != 0 this is the number of bits used in the last block
        const block_width_type extra_bits = count_extra_bits();

        if (extra_bits != 0)
            m_highest_block() &= (Block(1) << extra_bits) - 1;
    }

    // check class invariants
    template<typename Block, typename Allocator>
    bool mm_dynamic_bitset<Block, Allocator>::m_check_invariants() const
    {
        const block_width_type extra_bits = count_extra_bits();
        if (extra_bits > 0)
        {
            const block_type mask = block_type(~0) << extra_bits;
            if ((m_highest_block() & mask) != 0)
                return false;
        }
        if (m_bits.size() > m_bits.capacity() || num_blocks() != calc_num_blocks(size()))
            return false;

        return true;

    }

    // look for the first bit "on", starting
    // from the block with index first_block
    //
    template<typename Block, typename Allocator>
    typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::m_do_find_from(size_type first_block) const
    {
        size_type i = first_block;

        // skip null blocks
        while (i < num_blocks() && m_bits[i] == 0)
            ++i;

        if (i >= num_blocks())
            return npos; // not found

        return i * bits_per_block + static_cast<size_type>(lowest_bit(m_bits[i]));

    }

    template<typename Block, typename Allocator>
    inline typename mm_dynamic_bitset<Block, Allocator>::size_type mm_dynamic_bitset<Block, Allocator>::calc_num_blocks(size_type num_bits)
    {
        return num_bits / bits_per_block
            + static_cast<size_type>(num_bits % bits_per_block != 0);
    }

    // gives a reference to the highest block
    //
    template<typename Block, typename Allocator>
    inline Block& mm_dynamic_bitset<Block, Allocator>::m_highest_block()
    {
        return const_cast<Block &>(static_cast<const mm_dynamic_bitset *>(this)->m_highest_block());
    }

    // gives a const-reference to the highest block
    //
    template<typename Block, typename Allocator>
    inline const Block& mm_dynamic_bitset<Block, Allocator>::m_highest_block() const
    {
        assert(size() > 0 && num_blocks() > 0);
        return m_bits.back();
    }


    //-----------------------------------------------------------------------------
    // comparison
    template<typename Block, typename Allocator>
    bool operator==(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        return (a.m_num_bits == b.m_num_bits)
            && (a.m_bits == b.m_bits);
    }

    template<typename Block, typename Allocator>
    inline bool operator!=(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        return !(a == b);
    }

    template<typename Block, typename Allocator>
    bool operator<(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        //    assert(a.size() == b.size());
        typedef typename mm_dynamic_bitset<Block, Allocator>::size_type size_type;

        size_type asize(a.size());
        size_type bsize(b.size());

        if (!bsize)
        {
            return false;
        }
        else if (!asize)
        {
            return true;
        }
        else if (asize == bsize)
        {
            for (size_type ii = a.num_blocks(); ii > 0; --ii)
            {
                size_type i = ii - 1;
                if (a.m_bits[i] < b.m_bits[i])
                    return true;
                else if (a.m_bits[i] > b.m_bits[i])
                    return false;
            }
            return false;
        }
        else
        {

            size_type leqsize(std::min(asize, bsize));

            for (size_type ii = 0; ii < leqsize; ++ii, --asize, --bsize)
            {
                size_type i = asize - 1;
                size_type j = bsize - 1;
                if (a[i] < b[j])
                    return true;
                else if (a[i] > b[j])
                    return false;
            }
            return (a.size() < b.size());
        }
    }

    template<typename Block, typename Allocator>
    bool oplessthan(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        //    assert(a.size() == b.size());
        typedef typename mm_dynamic_bitset<Block, Allocator>::size_type size_type;

        size_type asize(a.num_blocks());
        size_type bsize(b.num_blocks());
        assert(asize == 3);
        assert(bsize == 4);

        if (!bsize)
        {
            return false;
        }
        else if (!asize)
        {
            return true;
        }
        else
        {

            size_type leqsize(std::min(asize, bsize));
            assert(leqsize == 3);

            //if (a.size() == 0)
            //  return false;

            // Since we are storing the most significant bit
            // at pos == size() - 1, we need to do the comparisons in reverse.
            //
            for (size_type ii = 0; ii < leqsize; ++ii, --asize, --bsize)
            {
                size_type i = asize - 1;
                size_type j = bsize - 1;
                if (a.m_bits[i] < b.m_bits[j])
                    return true;
                else if (a.m_bits[i] > b.m_bits[j])
                    return false;
            }
            return (a.num_blocks() < b.num_blocks());
        }
    }
    template<typename Block, typename Allocator, typename BlockOutputIterator>
    inline void to_block_range(const mm_dynamic_bitset<Block, Allocator>& b, BlockOutputIterator result)
    {
        // note how this copies *all* bits, including the
        // unused ones in the last block (which are zero)
        std::copy(b.m_bits.begin(), b.m_bits.end(), result);
    }

    template<typename BlockIterator, typename B, typename A>
    inline void from_block_range(BlockIterator first, BlockIterator last, mm_dynamic_bitset<B, A>& result)
    {
        // PRE: distance(first, last) <= numblocks()
        std::copy(first, last, result.m_bits.begin());
    }

    template<typename Ch, typename Tr, typename Block, typename Alloc>
    std::basic_istream<Ch, Tr>& operator>>(std::basic_istream<Ch, Tr>& is, mm_dynamic_bitset<Block, Alloc>& b)
    {
        using namespace std;

        typedef mm_dynamic_bitset<Block, Alloc> bitset_type;
        typedef typename mm_dynamic_bitset<Block, Alloc>::size_type size_type;

        const streamsize w = is.width();
        const size_type limit = 0 < w && static_cast<size_type>(w) < b.max_size() ? static_cast<size_type>(w) : b.max_size();

        ios_base::iostate err = ios_base::goodbit;
        typename basic_istream<Ch, Tr>::sentry cerberos(is); // skips whitespaces
        if (cerberos)
        {

            // in accordance with prop. resol. of lib DR 303 [last checked 4 Feb 2004]
            const Ch zero = '0';
            const Ch one = '1';

            b.clear();
            try
            {
                typename bitset_type::bit_appender appender(b);
                basic_streambuf <Ch, Tr> * buf = is.rdbuf();
                typename Tr::int_type c = buf->sgetc();
                for (; appender.get_count() < limit; c = buf->snextc())
                {
                    if (Tr::eq_int_type(Tr::eof(), c))
                    {
                        err |= ios_base::eofbit;
                        break;
                    }
                    else
                    {
                        const Ch to_c = Tr::to_char_type(c);
                        const bool is_one = Tr::eq(to_c, one);

                        if (!is_one && !Tr::eq(to_c, zero))
                            break; // non digit character

                        appender.do_append(is_one);
                    }

                } // for
            }
            catch (...)
            {
                // catches from stream buf, or from vector:
                //
                // bits_stored bits have been extracted and stored, and
                // either no further character is extractable or we can't
                // append to the underlying vector (out of memory)

                bool rethrow = false;   // see std 27.6.1.1/4
                try { is.setstate(ios_base::badbit); }
                catch (...) { rethrow = true; }

                if (rethrow)
                    throw;
            }
        }

        is.width(0);
        if (b.size() == 0 /*|| !cerberos*/)
            err |= ios_base::failbit;
        if (err != ios_base::goodbit)
            is.setstate(err); // may throw

        return is;

    }

    template<typename B, typename A, typename stringT>
    void to_string_helper(const mm_dynamic_bitset<B, A> & b, stringT & s, bool dump_all)
    {
        typedef typename stringT::traits_type Tr;
        typedef typename stringT::value_type  Ch;

        const Ch zero = '0';
        const Ch one = '1';

        // Note that this function may access (when
        // dump_all == true) bits beyond position size() - 1

        typedef typename mm_dynamic_bitset<B, A>::size_type size_type;

        const size_type len = dump_all ? mm_dynamic_bitset<B, A>::bits_per_block * b.num_blocks() : b.size();
        s.assign(len, zero);

        for (size_type i = 0; i < len; ++i)
        {
            if (b.m_unchecked_test(i))
                Tr::assign(s[len - 1 - i], one);
        }

    }

    template<typename Block, typename Allocator>
    inline bool operator<=(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        return !(a > b);
    }

    template<typename Block, typename Allocator>
    inline bool operator>(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        return b < a;
    }

    template<typename Block, typename Allocator>
    inline bool operator>=(const mm_dynamic_bitset<Block, Allocator>& a, const mm_dynamic_bitset<Block, Allocator>& b)
    {
        return !(a < b);
    }

    template<typename Ch, typename Tr, typename Block, typename Alloc>
    std::basic_ostream<Ch, Tr>& operator<<(std::basic_ostream<Ch, Tr>& os, const mm_dynamic_bitset<Block, Alloc>& b)
    {
        using namespace std;

        const ios_base::iostate ok = ios_base::goodbit;
        ios_base::iostate err = ok;

        typename basic_ostream<Ch, Tr>::sentry cerberos(os);
        if (cerberos)
        {

            const Ch zero = '0';
            const Ch one = '1';

            try
            {
                typedef typename mm_dynamic_bitset<Block, Alloc>::size_type bitset_size_type;
                typedef basic_streambuf<Ch, Tr> buffer_type;

                buffer_type * buf = os.rdbuf();
                // careful: os.width() is signed (and can be < 0)
                const bitset_size_type width = (os.width() <= 0) ? 0 : static_cast<bitset_size_type>(os.width());
                streamsize npad = (width <= b.size()) ? 0 : width - b.size();

                const Ch fill_char = os.fill();
                const ios_base::fmtflags adjustfield = os.flags() & ios_base::adjustfield;

                // if needed fill at left; pad is decreased along the way
                if (adjustfield != ios_base::left)
                {
                    for (; 0 < npad; --npad)
                        if (Tr::eq_int_type(Tr::eof(), buf->sputc(fill_char)))
                        {
                            err |= ios_base::failbit;
                            break;
                        }
                }

                if (err == ok)
                {
                    // output the bitset
                    for (bitset_size_type i = b.size(); 0 < i; --i)
                    {
                        typename buffer_type::int_type ret = buf->sputc(b.test(i - 1) ? one : zero);
                        if (Tr::eq_int_type(Tr::eof(), ret))
                        {
                            err |= ios_base::failbit;
                            break;
                        }
                    }
                }

                if (err == ok)
                {
                    // if needed fill at right
                    for (; 0 < npad; --npad)
                    {
                        if (Tr::eq_int_type(Tr::eof(), buf->sputc(fill_char)))
                        {
                            err |= ios_base::failbit;
                            break;
                        }
                    }
                }

                os.width(0);

            }
            catch (...)
            { // see std 27.6.1.1/4
                bool rethrow = false;
                try { os.setstate(ios_base::failbit); }
                catch (...) { rethrow = true; }

                if (rethrow)
                    throw;
            }
        }

        if (err != ok)
            os.setstate(err); // may throw exception
        return os;
    }
}
#endif//__mmDynamicBitset_h__
