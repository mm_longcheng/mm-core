/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmMRuntimeState.h"

namespace mm
{
    //
    //  struct implementation for mmMRuntimeState   //
    mmMRuntimeState::~mmMRuntimeState(void)
    {

    }
    mmMRuntimeState::mmMRuntimeState(void)
        :unique_id(0)
        , pid(0)
        , node_i("")
        , bind_i("")
        , port_i(0)
        , workers_i(0)
        , node_e("")
        , bind_e("")
        , port_e(0)
        , workers_e(0)
        , lavg_01(0)
        , lavg_05(0)
        , lavg_15(0)
        , cpu_pro(0)
        , mem_pro(0)
        , weights(0)
        , logger_lvl(0)
        , logger_dir("")
        , updatetime(0)
        , key("")
    {

    }
    void mmMRuntimeState::encode(mmOArchive& archive) const
    {
        archive << unique_id;
        archive << pid;
        archive << node_i;
        archive << bind_i;
        archive << port_i;
        archive << workers_i;
        archive << node_e;
        archive << bind_e;
        archive << port_e;
        archive << workers_e;
        archive << lavg_01;
        archive << lavg_05;
        archive << lavg_15;
        archive << cpu_pro;
        archive << mem_pro;
        archive << weights;
        archive << logger_lvl;
        archive << logger_dir;
        archive << updatetime;
        archive << key;
    }
    void mmMRuntimeState::decode(const mmIArchive& archive)
    {
        archive >> unique_id;
        archive >> pid;
        archive >> node_i;
        archive >> bind_i;
        archive >> port_i;
        archive >> workers_i;
        archive >> node_e;
        archive >> bind_e;
        archive >> port_e;
        archive >> workers_e;
        archive >> lavg_01;
        archive >> lavg_05;
        archive >> lavg_15;
        archive >> cpu_pro;
        archive >> mem_pro;
        archive >> weights;
        archive >> logger_lvl;
        archive >> logger_dir;
        archive >> updatetime;
        archive >> key;
    }

    //
    //  struct implementation for mmMConfigModule   //
    mmMConfigModule::~mmMConfigModule(void)
    {

    }
    mmMConfigModule::mmMConfigModule(void)
        :unique_id(0)
        , mid_l(0)
        , mid_r(0)
        , shard_size(0)
        , depth_size(0)
        , desc("")
    {

    }
    void mmMConfigModule::encode(mmOArchive& archive) const
    {
        archive << unique_id;
        archive << mid_l;
        archive << mid_r;
        archive << shard_size;
        archive << depth_size;
        archive << desc;
    }
    void mmMConfigModule::decode(const mmIArchive& archive)
    {
        archive >> unique_id;
        archive >> mid_l;
        archive >> mid_r;
        archive >> shard_size;
        archive >> depth_size;
        archive >> desc;
    }

}
