/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConfigParse_h__
#define __mmConfigParse_h__

#include "dish/mmConfigValue.h"

#include <string>

#include "dish/mmDishExport.h"

namespace mm
{
    class MM_EXPORT_DISH mmConfigParse
    {
    private:
        mmConfigValue& d_config_value;
    public:
        explicit mmConfigParse(mmConfigValue& config_value);
        ~mmConfigParse(void);
    public:
        static bool IsPrintf(char _c);
        static std::string RejectEmpty(const std::string& _key);
    public:
        void LoadFile(const char* _file_name);
        void LoadBuffer(const char* _buffer, size_t _size);
    public:
        const mmConfigValue& GetConfigValue(void) const;
        mmConfigValue& GetConfigValueRef(void);
    private:
        void LoadLine(const std::string& _line);
    };
}
#endif//__mmConfigParse_h__
