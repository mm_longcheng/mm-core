/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmArchive_h__
#define __mmArchive_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"

#include "dish/mmDishExport.h"

#include <list>
#include <vector>
#include <stack>
#include <string>

namespace mm
{
    //
    //  Input stream.
    //
    // mmUInt32_t for std::string and container length,
    class MM_EXPORT_DISH mmIArchive
    {
    public:
        mmIArchive(struct mmStreambuf& _streambuf);

        size_t size(void) const
        {
            return mmStreambuf_Size(&this->streambuf);
        }

        const struct mmStreambuf& GetStreambuf(void) const
        {
            return this->streambuf;
        }

        struct mmStreambuf& GetStreambufRef(void)
        {
            return this->streambuf;
        }
    public:
        struct mmStreambuf& streambuf;
    };
    // compatible if have coder interface.
    template<typename ComplexType>
    const mmIArchive& operator >> (const mmIArchive& archive, ComplexType& val)
    {
        val.Decode(archive);
        return (archive);
    }
    //list vector decode will not trigger the value_type's copy construction ,
    //because list vector insert return the value reference.
    template<
        typename ValueType,
        typename AllocType>
    const mmIArchive& operator >> (const mmIArchive& archive, std::list<ValueType, AllocType>& val)
    {
        typedef std::list<ValueType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::iterator iterator;
        mmUInt32_t size = 0;
        (archive) >> size;
        // do not use resize.std list vector resize(size_t n, ValueType())
        // if value_type contain shared pointer will case bug.
        // std list not need reserve.
        for (mmUInt32_t _index = 0; _index < size; ++_index)
        {
            // mmOArchive is begin ==> end.
            iterator it = val.insert(val.end(), ValueType());
            (archive) >> (*it);
        }
        return (archive);
    }
    template<
        typename ValueType,
        typename AllocType>
    const mmIArchive& operator >> (const mmIArchive& archive, std::vector<ValueType, AllocType>& val)
    {
        typedef std::vector<ValueType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::iterator iterator;
        mmUInt32_t size = 0;
        (archive) >> size;
        // do not use resize.std list vector resize(size_t n, ValueType())
        // if value_type contain shared pointer will case bug.
        // std vector need reserve.
        val.reserve(size);
        for (mmUInt32_t _index = 0; _index < size; ++_index)
        {
            // mmOArchive is begin ==> end.
            iterator it = val.insert(val.end(), ValueType());
            (archive) >> (*it);
        }
        return (archive);
    }
    //pair
    template<
        typename Type1,
        typename Type2>
    const mmIArchive& operator >> (const mmIArchive& archive, std::pair<Type1, Type2>& val)
    {
        (archive) >> val.first;
        (archive) >> val.second;
        return (archive);
    }
    //map multimap decode will not trigger the mapped_type's copy construction ,
    //because map multimap insert return the value reference.
    template<
        typename KeyType,
        typename ValueType,
        typename CompareType,
        typename AllocType,
        template<class, class, class, class> class ComplexContainer>
    const mmIArchive& operator >> (const mmIArchive& archive, ComplexContainer<KeyType, ValueType, CompareType, AllocType>& val)
    {
        typedef ComplexContainer<KeyType, ValueType, CompareType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::mapped_type mapped_type;
        typedef typename ComplexContainerType::iterator iterator;
        typedef typename std::pair<iterator, bool> result_type;        
        mmUInt32_t size = 0;
        (archive) >> size;
        val.clear();
        while (0 < size)
        {
            size--;
            KeyType k;
            (archive) >> k;
            result_type _result = val.insert(value_type(k, mapped_type()));
            (archive) >> (_result.first->second);
        }
        return (archive);
    }
    //set multiset decode will trigger the key_type copy construction ,
    //because set multiset insert return the value reference but not assignable.
    //Can't modify a set's members in place.
    //http://stackoverflow.com/questions/16299125/return-iterator-from-stdsetinsert-is-const
    template<
        typename KeyType,
        typename CompareType,
        typename AllocType,
        template<class, class, class> class ComplexContainer>
    const mmIArchive& operator >> (const mmIArchive& archive, ComplexContainer<KeyType, CompareType, AllocType>& val)
    {
        mmUInt32_t size = 0;
        (archive) >> size;
        val.clear();
        while (0 < size)
        {
            size--;
            KeyType k;
            (archive) >> k;
            val.insert(k);
        }
        return (archive);
    }
    //stack decode will trigger a copy construction ,
    //because stack push not return the value reference, and only only value.
    template<
        typename ValueType,
        typename Container>
    const mmIArchive& operator >> (const mmIArchive& archive, std::stack<ValueType, Container>& val)
    {
        mmUInt32_t size = 0;
        (archive) >> size;
        while (!val.empty())
        {
            val.pop();
        }
        while (0 < size)
        {
            size--;
            ValueType v;
            (archive) >> v;
            val.push(v);
        }
        return (archive);
    }

    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmSChar_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmUChar_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmSShort_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmUShort_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmSInt_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmUInt_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmSLong_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmULong_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmSLLong_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, mmULLong_t& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, float& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, double& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, bool& val);
    MM_EXPORT_DISH const mmIArchive& operator >> (const mmIArchive& archive, std::string& val);

    //
    //  Output stream.
    //
    // mmUInt32_t for std::string and container length,
    class MM_EXPORT_DISH mmOArchive
    {
    public:
        mmOArchive(struct mmStreambuf& _streambuf);

        size_t size(void) const
        {
            return mmStreambuf_Size(&this->streambuf);
        }

        const struct mmStreambuf& GetStreambuf(void)const
        {
            return this->streambuf;
        }

        struct mmStreambuf& GetStreambufRef(void)
        {
            return this->streambuf;
        }
    public:
        struct mmStreambuf& streambuf;
    };
    // compatible if have coder interface.
    template<typename ComplexType>
    mmOArchive& operator << (mmOArchive& archive, const ComplexType& val)
    {
        val.Encode(archive);
        return (archive);
    }
    //list vector
    template<
        typename ValueType,
        typename AllocType,
        template<class, class> class ComplexContainer>
    mmOArchive& operator << (mmOArchive& archive, const ComplexContainer<ValueType, AllocType>& val)
    {
        typedef ComplexContainer<ValueType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            for (const_iterator it = val.begin();
                it != val.end(); it++)
            {
                (archive) << (*it);
            }
        }
        return (archive);
    }
    //pair
    template<
        typename Type1,
        typename Type2>
    mmOArchive& operator << (mmOArchive& archive, const std::pair<Type1, Type2>& val)
    {
        (archive) << val.first;
        (archive) << val.second;
        return (archive);
    }
    //map
    template<
        typename KeyType,
        typename ValueType,
        typename CompareType,
        typename AllocType,
        template<class, class, class, class> class ComplexContainer>
    mmOArchive& operator << (mmOArchive& archive, const ComplexContainer<KeyType, ValueType, CompareType, AllocType>& val)
    {
        typedef ComplexContainer<KeyType, ValueType, CompareType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            for (const_iterator it = val.begin();
                it != val.end(); it++)
            {
                (archive) << it->first;
                (archive) << it->second;
            }
        }
        return (archive);
    }
    //set
    template<
        typename KeyType,
        typename CompareType,
        typename AllocType,
        template<class, class, class> class ComplexContainer>
    mmOArchive& operator << (mmOArchive& archive, const ComplexContainer<KeyType, CompareType, AllocType>& val)
    {
        typedef ComplexContainer<KeyType, CompareType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            for (const_iterator it = val.begin();
                it != val.end(); it++)
            {
                (archive) << (*it);
            }
        }
        return (archive);
    }
    //stack
    template<
        typename ValueType,
        typename Container>
    mmOArchive& operator << (mmOArchive& archive, const std::stack<ValueType, Container>& val)
    {
        typedef typename std::stack<ValueType, Container> ComplexContainerType;
        typedef typename ComplexContainerType::container_type container_type;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            const container_type& cval = val._Get_container();
            for (const_iterator it = cval.begin();
                it != cval.end(); it++)
            {
                (archive) << (*it);
            }
        }
        return (archive);
    }
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmSChar_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmUChar_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmSShort_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmUShort_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmSInt_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmUInt_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmSLong_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmULong_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmSLLong_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const mmULLong_t& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const float& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const double& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const bool& val);
    MM_EXPORT_DISH mmOArchive& operator << (mmOArchive& archive, const std::string& val);
}
#endif//__mmArchive_h__
