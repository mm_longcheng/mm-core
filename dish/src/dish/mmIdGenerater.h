/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmIdGenerater_h__
#define __mmIdGenerater_h__

#include "core/mmConfig.h"

#include "container/mmRbtsetU32.h"
#include "container/mmRbtsetU64.h"

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

#define MM_ID_GENERATER_RATE_TRIM_BORDER_DEFAULT 0.75
#define MM_ID_GENERATER_RATE_TRIM_NUMBER_DEFAULT 0.25

struct mmUInt32IdGenerater
{
    // weak ref.
    struct mmRbtsetU32 idle;
    mmUInt32_t id_counter;
    float rate_trim_border;
    float rate_trim_number;
};

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_Init(
    struct mmUInt32IdGenerater*                    p);

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_Destroy(
    struct mmUInt32IdGenerater*                    p);

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_SetRateTrimBorder(
    struct mmUInt32IdGenerater*                    p, 
    float                                          rate_trim_border);

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_SetRateTrimNumber(
    struct mmUInt32IdGenerater*                    p, 
    float                                          rate_trim_number);

MM_EXPORT_DISH 
mmUInt32_t 
mmUInt32IdGenerater_Produce(
    struct mmUInt32IdGenerater*                    p);

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_Recycle(
    struct mmUInt32IdGenerater*                    p, 
    mmUInt32_t                                     id);

MM_EXPORT_DISH 
mmUInt32_t 
mmUInt32IdGenerater_MaxIdleId(
    struct mmUInt32IdGenerater*                    p);

struct mmUInt64IdGenerater
{
    // weak ref.
    struct mmRbtsetU64 idle;
    mmUInt64_t id_counter;
    float rate_trim_border;
    float rate_trim_number;
};

MM_EXPORT_DISH 
void
mmUInt64IdGenerater_Init(
    struct mmUInt64IdGenerater*                    p);

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_Destroy(
    struct mmUInt64IdGenerater*                    p);

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_SetRateTrimBorder(
    struct mmUInt64IdGenerater*                    p, 
    float                                          rate_trim_border);

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_SetRateTrimNumber(
    struct mmUInt64IdGenerater*                    p, 
    float                                          rate_trim_number);

MM_EXPORT_DISH 
mmUInt64_t 
mmUInt64IdGenerater_Produce(
    struct mmUInt64IdGenerater*                    p);

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_Recycle(
    struct mmUInt64IdGenerater*                    p, 
    mmUInt64_t                                     id);

MM_EXPORT_DISH 
mmUInt64_t 
mmUInt64IdGenerater_MaxIdleId(
    struct mmUInt64IdGenerater*                    p);

#include "core/mmSuffix.h"

#endif//__mmIdGenerater_h__
