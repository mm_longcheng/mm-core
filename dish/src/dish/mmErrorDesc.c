/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmErrorDesc.h"
#include "core/mmAlloc.h"
#include "core/mmString.h"

MM_EXPORT_DISH 
void 
mmErrorDesc_Init(
    struct mmErrorDesc*                            p)
{
    struct mmRbtreeU32VptAllocator _U32VptAllocator;

    mmRbtreeU32Vpt_Init(&p->rbtree);

    _U32VptAllocator.Produce = &mmRbtreeU32Vpt_WeakProduce;
    _U32VptAllocator.Recycle = &mmRbtreeU32Vpt_WeakRecycle;
    mmRbtreeU32Vpt_SetAllocator(&p->rbtree, &_U32VptAllocator);
}

MM_EXPORT_DISH 
void 
mmErrorDesc_Destroy(
    struct mmErrorDesc*                            p)
{
    // clear first.
    mmErrorDesc_Clear(p);
    //
    mmRbtreeU32Vpt_Destroy(&p->rbtree);
}

MM_EXPORT_DISH 
void 
mmErrorDesc_Clear(
    struct mmErrorDesc*                            p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeU32VptIterator* it = NULL;
    struct mmString* e = NULL;
    n = mmRb_First(&p->rbtree.rbt);
    while (n)
    {
        it = mmRb_Entry(n, struct mmRbtreeU32VptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmString*)(it->v);
        mmString_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
const char* 
mmErrorDesc_CodeString(
    struct mmErrorDesc*                            p, 
    int                                            code)
{
    struct mmString* desc_string = (struct mmString*)mmRbtreeU32Vpt_Get(&p->rbtree, code);
    if (NULL == desc_string)
    {
        return mmErrorDesc_Unknown();
    }
    else
    {
        return mmString_CStr(desc_string);
    }
}

// use the code cache current code -> desc.
MM_EXPORT_DISH 
void 
mmErrorDesc_CacheResult(
    struct mmErrorDesc*                            p, 
    int                                            code)
{
    p->code = code;
    mmString_Assigns(&p->desc, mmErrorDesc_CodeString(p, p->code));
}

MM_EXPORT_DISH 
void 
mmErrorDesc_SetCodeDesc(
    struct mmErrorDesc*                            p, 
    int                                            code, 
    const char*                                    desc)
{
    struct mmString* desc_string = (struct mmString*)mmRbtreeU32Vpt_Get(&p->rbtree, code);
    if (NULL == desc_string)
    {
        desc_string = (struct mmString*)mmMalloc(sizeof(struct mmString));
        mmString_Init(desc_string);
        mmString_Assigns(desc_string, desc);
        mmRbtreeU32Vpt_Set(&p->rbtree, code, desc_string);
    }
    else
    {
        mmString_Assigns(desc_string, desc);
    }
}

MM_EXPORT_DISH 
const char* 
mmErrorDesc_Unknown(void)
{
    return "unknown error.";
}

MM_EXPORT_DISH 
void 
mmErrorDesc_SetCore(
    struct mmErrorDesc*                            p)
{
    mmErrorDesc_SetCodeDesc(p, MM_ERR_CORE_UNKNOWN, "unknown error.");
    mmErrorDesc_SetCodeDesc(p, MM_ERR_CORE_SUCCESS, "");
}

