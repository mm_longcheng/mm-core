/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmRandomNumber_h__
#define __mmRandomNumber_h__

#include "core/mmConfig.h"

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

// come form  * @author  Takayuki HARUKI (University of Toyama, Japan)

MM_EXPORT_DISH extern const char MM_RANDOM_NUMBER_CHAR_L;
MM_EXPORT_DISH extern const char MM_RANDOM_NUMBER_CHAR_R;
MM_EXPORT_DISH extern const int  MM_RANDOM_NUMBER_CHAR_N;

#define MM_RANDOM_NUMBER_N 624
#define MM_RANDOM_NUMBER_M 397

struct mmRandomNumber
{
    unsigned long   mt[MM_RANDOM_NUMBER_N]; //!< array for the state vector
    int             mti;                    //!< mti==N+1 means mt[N] is not initialized
};

MM_EXPORT_DISH 
void 
mmRandomNumber_Init(
    struct mmRandomNumber*                         p);

MM_EXPORT_DISH 
void 
mmRandomNumber_Destroy(
    struct mmRandomNumber*                         p);

MM_EXPORT_DISH 
double 
mmRandomNumber_GenrandReal1(
    struct mmRandomNumber*                         p);

MM_EXPORT_DISH 
double 
mmRandomNumber_GenrandReal2(
    struct mmRandomNumber*                         p);

MM_EXPORT_DISH 
double 
mmRandomNumber_GenrandReal3(
    struct mmRandomNumber*                         p);

// [0, 0xffffffff]
MM_EXPORT_DISH 
unsigned long 
mmRandomNumber_GenrandUInt32(
    struct mmRandomNumber*                         p);

MM_EXPORT_DISH 
void 
mmRandomNumber_ChangeSeed(
    struct mmRandomNumber*                         p, 
    unsigned long                                  seed);

// random buffer.
MM_EXPORT_DISH 
void 
mmRandomNumber_RandBuffer(
    struct mmRandomNumber*                         p, 
    unsigned char*                                 buffer, 
    unsigned int                                   offset, 
    unsigned int                                   length);

#include "core/mmSuffix.h"

#endif//__mmRandomNumber_h__
