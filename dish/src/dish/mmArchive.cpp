/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmArchive.h"

namespace mm
{
    mmIArchive::mmIArchive(struct mmStreambuf& _streambuf)
        : streambuf(_streambuf)
    {

    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmSChar_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSChar_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmUChar_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUChar_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmSShort_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSShort_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmUShort_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUShort_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmSInt_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSInt_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmUInt_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUInt_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmSLong_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSLong_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmULong_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmULong_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmSLLong_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSLLong_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, mmULLong_t& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmULLong_t));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, float& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(float));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, double& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(double));
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, std::string& val)
    {
        mmUInt32_t size = 0;
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&size, 0, sizeof(mmUInt32_t));
        if (0 != size)
        {
            val.resize(size);
            mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)val.data(), 0, size);
        }
        return archive;
    }

    const mmIArchive& operator >> (const mmIArchive& archive, bool& val)
    {
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(bool));
        return archive;
    }
    
    mmOArchive::mmOArchive(struct mmStreambuf& _streambuf)
        : streambuf(_streambuf)
    {

    }

    mmOArchive& operator << (mmOArchive& archive, const mmSChar_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSChar_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmUChar_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUChar_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmSShort_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSShort_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmUShort_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUShort_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmSInt_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSInt_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmUInt_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUInt_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmSLong_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSLong_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmULong_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmULong_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmSLLong_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSLLong_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const mmULLong_t& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmULLong_t));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const float& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(float));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const double& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(double));
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const std::string& val)
    {
        mmUInt32_t size = (mmUInt32_t)val.size();
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&size, 0, sizeof(mmUInt32_t));
        if (0 != size)
        {
            mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)val.c_str(), 0, size);
        }
        return archive;
    }

    mmOArchive& operator << (mmOArchive& archive, const bool& val)
    {
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(bool));
        return archive;
    }
}
