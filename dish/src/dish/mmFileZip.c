/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFileZip.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmFileSystem.h"
#include "core/mmFilePath.h"

#include "minizip/unzip.h"

MM_EXPORT_DISH 
void 
mmZipEntry_Init(
    struct mmZipEntry*                             p)
{
    p->compr = 0;
    p->csize = 0;
    p->dsize = 0;
    p->attri = _A_NORMAL;
    mmString_Init(&p->fullname);
    mmString_Init(&p->basename);
    mmString_Init(&p->pathname);
    p->pos = 0;
    p->num = 0;
    //
    mmString_Assigns(&p->fullname, "");
    mmString_Assigns(&p->basename, "");
    mmString_Assigns(&p->pathname, "");
}

MM_EXPORT_DISH 
void 
mmZipEntry_Destroy(
    struct mmZipEntry*                             p)
{
    p->compr = 0;
    p->csize = 0;
    p->dsize = 0;
    p->attri = _A_NORMAL;
    mmString_Destroy(&p->fullname);
    mmString_Destroy(&p->basename);
    mmString_Destroy(&p->pathname);
    p->pos = 0;
    p->num = 0;
}

MM_EXPORT_DISH 
void 
mmZipEntry_SetIsDirectory(
    struct mmZipEntry*                             p,
    int                                            hIsDirectory)
{
    if (0 == hIsDirectory)
    {
        p->attri = p->attri & (~_A_SUBDIR);
    }
    else
    {
        p->attri = p->attri | _A_SUBDIR;
    }
}

MM_EXPORT_DISH 
int 
mmZipEntry_GetIsDirectory(
    struct mmZipEntry*                             p)
{
    return (0 != (p->attri & _A_SUBDIR)) ? 1 : 0;
}

MM_EXPORT_DISH 
void 
mmZipEntry_UpdateAttri(
    struct mmZipEntry*                             p)
{
    size_t fl = mmString_Size(&p->fullname);
    size_t bl = mmString_Size(&p->basename);
    size_t pl = mmString_Size(&p->pathname);
    int d = (0 != fl && 0 == bl && 0 != pl) ? 1 : 0;
    mmZipEntry_SetIsDirectory(p, d);
}

MM_EXPORT_DISH 
void 
mmZipDirectory_Init(
    struct mmZipDirectory*                         p)
{
    struct mmRbtreeStringVptAllocator _StringVptAllocator;
    //
    mmString_Init(&p->pathname);
    mmRbtreeStringVpt_Init(&p->entrys);
    //
    _StringVptAllocator.Produce = &mmRbtreeStringVpt_WeakProduce;;
    _StringVptAllocator.Recycle = &mmRbtreeStringVpt_WeakRecycle;;
    mmRbtreeStringVpt_SetAllocator(&p->entrys, &_StringVptAllocator);
}

MM_EXPORT_DISH 
void 
mmZipDirectory_Destroy(
    struct mmZipDirectory*                         p)
{
    mmZipDirectory_Clear(p);
    //
    mmString_Destroy(&p->pathname);
    mmRbtreeStringVpt_Destroy(&p->entrys);
}

MM_EXPORT_DISH 
struct mmZipEntry* 
mmZipDirectory_Add(
    struct mmZipDirectory*                         p,
    struct mmString*                               fullname)
{
    struct mmZipEntry* e = mmZipDirectory_Get(p, fullname);
    if (NULL == e)
    {
        e = (struct mmZipEntry*)mmMalloc(sizeof(struct mmZipEntry));
        mmZipEntry_Init(e);
        // cache the entry info.
        mmRbtreeStringVpt_Set(&p->entrys, fullname, e);
    }
    return e;
}

MM_EXPORT_DISH 
struct mmZipEntry* 
mmZipDirectory_Get(
    struct mmZipDirectory*                         p,
    struct mmString*                               fullname)
{
    return (struct mmZipEntry*)mmRbtreeStringVpt_Get(&p->entrys, fullname);
}

MM_EXPORT_DISH 
struct mmZipEntry* 
mmZipDirectory_GetInstance(
    struct mmZipDirectory*                         p,
    struct mmString*                               fullname)
{
    struct mmZipEntry* e = mmZipDirectory_Get(p, fullname);
    if (NULL == e)
    {
        e = mmZipDirectory_Add(p, fullname);
    }
    return e;
}

MM_EXPORT_DISH 
void 
mmZipDirectory_Rmv(
    struct mmZipDirectory*                         p,
    struct mmString*                               fullname)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmZipEntry* e = NULL;

    it = mmRbtreeStringVpt_GetIterator(&p->entrys, fullname);
    if (NULL != it)
    {
        e = (struct mmZipEntry*)(it->v);
        mmRbtreeStringVpt_Erase(&p->entrys, it);
        mmZipEntry_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmZipDirectory_Clear(
    struct mmZipDirectory*                         p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmZipEntry* e = NULL;
    n = mmRb_First(&p->entrys.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmZipEntry*)(it->v);
        mmRbtreeStringVpt_Erase(&p->entrys, it);
        mmZipEntry_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmFileZip_Init(
    struct mmFileZip*                              p)
{
    struct mmRbtreeStringVptAllocator _StringVptAllocator;
    //
    mmString_Init(&p->filename);
    mmString_Init(&p->filter);
    mmRbtreeStringVpt_Init(&p->directorys);
    p->unzipFile = NULL;
    //
    _StringVptAllocator.Produce = &mmRbtreeStringVpt_WeakProduce;;
    _StringVptAllocator.Recycle = &mmRbtreeStringVpt_WeakRecycle;;
    mmRbtreeStringVpt_SetAllocator(&p->directorys, &_StringVptAllocator);
}

MM_EXPORT_DISH 
void 
mmFileZip_Destroy(
    struct mmFileZip*                              p)
{
    mmFileZip_Clear(p);
    mmFileZip_Close(p);
    //
    mmString_Destroy(&p->filename);
    mmString_Destroy(&p->filter);
    mmRbtreeStringVpt_Destroy(&p->directorys);
    p->unzipFile = NULL;
}

MM_EXPORT_DISH 
void 
mmFileZip_SetFilename(
    struct mmFileZip*                              p,
    const char*                                    filename)
{
    mmString_Assigns(&p->filename, filename);
}

MM_EXPORT_DISH 
void 
mmFileZip_SetFilter(
    struct mmFileZip*                              p,
    const char*                                    filter)
{
    if (0 == mmStrcmp(filter, ".") || 0 == mmStrcmp(filter, "./"))
    {
        mmString_Assigns(&p->filter, "");
    }
    else
    {
        mmString_Assigns(&p->filter, filter);
    }
}

MM_EXPORT_DISH 
void 
mmFileZip_Fopen(
    struct mmFileZip*                              p)
{
    if (!mmString_Empty(&p->filename))
    {
        // try close the current zip file.
        mmFileZip_Close(p);
        p->unzipFile = (void*)unzOpen64(mmString_CStr(&p->filename));
    }
}

MM_EXPORT_DISH 
void 
mmFileZip_Close(
    struct mmFileZip*                              p)
{
    if (NULL != p->unzipFile)
    {
        unzClose((unzFile)p->unzipFile);
        p->unzipFile = NULL;
    }
}

MM_EXPORT_DISH 
void 
mmFileZip_AcquireAnalysis(
    struct mmFileZip*                              p)
{
    mmFileZip_Clear(p);
    //
    if (NULL != p->unzipFile)
    {
        unz_global_info unzipGlobalInfo;
        unz_file_info64 unzipFileInfo;
        unz64_file_pos file_pos;
        uLong i = 0;
        char szZipFName[PATH_MAX];
        unzFile unzipFile = (unzFile)p->unzipFile;
        
        struct mmString qualified_name;
        struct mmString pathname;
        struct mmString basename;
        struct mmString path_substr;
        struct mmZipDirectory* zip_directory = NULL;
        struct mmZipEntry* zip_entry = NULL;
        mmString_Init(&qualified_name);
        mmString_Init(&pathname);
        mmString_Init(&basename);
        mmString_Init(&path_substr);
        // cache names
        do
        {
            if (UNZ_OK != unzGetGlobalInfo(unzipFile, &unzipGlobalInfo))
            {
                break;
            }
            
            for (i = 0; i < unzipGlobalInfo.number_entry; i++)
            {
                if (UNZ_OK != unzGetCurrentFileInfo64(
                    unzipFile, 
                    &unzipFileInfo, 
                    szZipFName, 
                    sizeof(szZipFName), 
                    NULL, 
                    0, 
                    NULL, 
                    0))
                {
                    break;
                }

                mmString_Assigns(&qualified_name, szZipFName);
                mmString_Substr(&qualified_name, &path_substr, 0, mmString_Size(&p->filter));
                if (0 == mmString_Size(&p->filter) || 0 == mmString_Compare(&path_substr, &p->filter))
                {
                    mmPathSplitFileName(&qualified_name, &basename, &pathname);
                    // directory
                    zip_directory = mmFileZip_Add(p, &pathname);
                    // entry
                    zip_entry = mmZipDirectory_GetInstance(zip_directory, &qualified_name);
                    
                    zip_entry->compr = unzipFileInfo.compression_method;
                    zip_entry->csize = unzipFileInfo.compressed_size;
                    zip_entry->dsize = unzipFileInfo.uncompressed_size;
                    mmString_Assign(&zip_entry->fullname, &qualified_name);
                    mmString_Assign(&zip_entry->basename, &basename);
                    mmString_Assign(&zip_entry->pathname, &pathname);
                    mmZipEntry_UpdateAttri(zip_entry);
                    
                    unzGetFilePos64(unzipFile, &file_pos);
                    zip_entry->pos = file_pos.pos_in_zip_directory;
                    zip_entry->num = file_pos.num_of_file;

                    // add into parent path.
                    mmDirectoryNoneSuffix(&qualified_name, mmString_CStr(&pathname));
                    mmPathSplitFileName(&qualified_name, &basename, &pathname);

                    // directory
                    zip_directory = mmFileZip_Add(p, &pathname);
                    // entry
                    zip_entry = mmZipDirectory_GetInstance(zip_directory, &qualified_name);

                    zip_entry->compr = 0;
                    zip_entry->csize = 0;
                    zip_entry->dsize = 0;
                    mmString_Assign(&zip_entry->fullname, &qualified_name);
                    mmString_Assign(&zip_entry->basename, &basename);
                    mmString_Assign(&zip_entry->pathname, &pathname);
                    mmZipEntry_SetIsDirectory(zip_entry, 1);
                }
                
                // Next file
                if ((i + 1) < unzipGlobalInfo.number_entry)
                {
                    if (UNZ_OK != unzGoToNextFile(unzipFile))
                    {
                        break;
                    }
                }
            }
        }while (0);
        
        mmString_Destroy(&qualified_name);
        mmString_Destroy(&pathname);
        mmString_Destroy(&basename);
        mmString_Destroy(&path_substr);
    }
}

MM_EXPORT_DISH 
void
mmFileZip_ReleaseAnalysis(
    struct mmFileZip*                              p)
{
    mmFileZip_Clear(p);
}

// acquire file buffer.
MM_EXPORT_DISH 
void 
mmFileZip_AcquireFileByteBuffer(
    const struct mmFileZip*                        p,
    const char*                                    fullname, 
    struct mmByteBuffer*                           pByteBuffer)
{
    do
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        struct mmZipEntry* entry = NULL;
        unz64_file_pos file_pos;
        voidp buffer = NULL;
        unsigned length = 0;
        unzFile unzipFile = (unzFile)p->unzipFile;
        const char* pFileName = mmString_CStr(&p->filename);

        // reset byte buffer.
        mmByteBuffer_Reset(pByteBuffer);

        if (NULL == p->unzipFile)
        {
            // need do nothing.
            break;
        }

        entry = mmFileZip_GetEntry(p, fullname);
        if (NULL == entry)
        {
            mmLogger_LogE(gLogger, "%s %d can not find file:%s %s.", __FUNCTION__, __LINE__, pFileName, fullname);
            break;
        }
        file_pos.pos_in_zip_directory = entry->pos;
        file_pos.num_of_file = entry->num;
        if(UNZ_OK != unzGoToFilePos64(unzipFile, &file_pos))
        {
            mmLogger_LogE(gLogger, "%s %d can not goto file:%s %s.", __FUNCTION__, __LINE__, pFileName, fullname);
            break;
        }

        // format not used here (always binary)
        // try if we find the file
        if (UNZ_OK != unzOpenCurrentFile(unzipFile))
        {
            mmLogger_LogE(gLogger, "%s %d can not open file:%s %s.", __FUNCTION__, __LINE__, pFileName, fullname);
            break;
        }

        // malloc buffer.
        mmByteBuffer_Malloc(pByteBuffer, entry->dsize + 1);
        pByteBuffer->length = (size_t)entry->dsize;
        pByteBuffer->buffer[pByteBuffer->length] = 0;
        buffer = (voidp)(pByteBuffer->buffer + pByteBuffer->offset);
        length = (unsigned)pByteBuffer->length;

        // read current file buffer.
        if (0 > unzReadCurrentFile(unzipFile, buffer, length))
        {
            mmLogger_LogE(gLogger, "%s %d can not read file:%s %s.", __FUNCTION__, __LINE__, pFileName, fullname);
            // free buffer.
            mmFree(pByteBuffer->buffer);
            mmByteBuffer_Reset(pByteBuffer);
        }

        // close current file.
        unzCloseCurrentFile(unzipFile);
    } while (0);
}

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileZip_ReleaseFileByteBuffer(
    const struct mmFileZip*                        p,
    struct mmByteBuffer*                           pByteBuffer)
{
    mmByteBuffer_Free(pByteBuffer);
}

// check file name exists.1 exists 0 not exists.
MM_EXPORT_DISH 
int
mmFileZip_Exists(
    const struct mmFileZip*                        p,
    const char*                                    fullname)
{
    int code = 0;
    if (NULL == p->unzipFile)
    {
        // need do nothing.
        code = 0;
    }
    else
    {
        struct mmZipEntry* entry = mmFileZip_GetEntry(p, fullname);
        code = (NULL == entry) ? 0 : 1;
    }
    return code;
}

// get entry for file name.
MM_EXPORT_DISH 
struct mmZipEntry* 
mmFileZip_GetEntry(
    const struct mmFileZip*                        p,
    const char*                                    fullname)
{
    struct mmString qualified_name;
    struct mmString pathname;
    struct mmString basename;
    struct mmZipDirectory* zip_directory = NULL;
    struct mmZipEntry* zip_entry = NULL;

    mmString_MakeWeaks(&qualified_name, fullname);

    mmString_Init(&pathname);
    mmString_Init(&basename);

    mmPathSplitFileName(&qualified_name, &basename, &pathname);

    // directory
    zip_directory = mmFileZip_Get(p, &pathname);
    if (NULL == zip_directory)
    {
        // can not find the directory.
        zip_entry = NULL;
    }
    else
    {
        // entry
        zip_entry = mmZipDirectory_Get(zip_directory, &qualified_name);
    }

    mmString_Destroy(&pathname);
    mmString_Destroy(&basename);
    //
    return zip_entry;
}

MM_EXPORT_DISH 
struct mmZipDirectory* 
mmFileZip_Add(
    struct mmFileZip*                              p,
    struct mmString*                               pathname)
{
    struct mmZipDirectory* e = mmFileZip_Get(p, pathname);
    if (NULL == e)
    {
        e = (struct mmZipDirectory*)mmMalloc(sizeof(struct mmZipDirectory));
        mmZipDirectory_Init(e);
        // cache the entry info.
        mmRbtreeStringVpt_Set(&p->directorys, pathname, e);
    }
    return e;
}

MM_EXPORT_DISH 
struct mmZipDirectory* 
mmFileZip_Get(
    const struct mmFileZip*                        p,
    struct mmString*                               pathname)
{
    struct mmZipDirectory* e = NULL;

    if (0 == mmMemcmp(mmString_Data(pathname), "./", 2))
    {
        struct mmString real_pathname;
        mmString_Init(&real_pathname);
        mmString_Substr(pathname, &real_pathname, 2, mmString_Size(pathname) - 2);
        e = (struct mmZipDirectory*)mmRbtreeStringVpt_Get(&p->directorys, &real_pathname);
        mmString_Destroy(&real_pathname);
    }
    else
    {
        e = (struct mmZipDirectory*)mmRbtreeStringVpt_Get(&p->directorys, pathname);
    }
    return e;
}

MM_EXPORT_DISH 
void 
mmFileZip_Rmv(
    struct mmFileZip*                              p,
    struct mmString*                               pathname)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmZipDirectory* e = NULL;

    it = mmRbtreeStringVpt_GetIterator(&p->directorys, pathname);
    if (NULL != it)
    {
        e = (struct mmZipDirectory*)(it->v);
        mmRbtreeStringVpt_Erase(&p->directorys, it);
        mmZipDirectory_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmFileZip_Clear(
    struct mmFileZip*                              p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmZipDirectory* e = NULL;
    n = mmRb_First(&p->directorys.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmZipDirectory*)(it->v);
        mmRbtreeStringVpt_Erase(&p->directorys, it);
        mmZipDirectory_Destroy(e);
        mmFree(e);
    }
}

