/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConfigValue_h__
#define __mmConfigValue_h__

#include "core/mmCore.h"

#include "dish/mmValue.h"

#include <string>
#include <map>

#include "dish/mmDishExport.h"

namespace mm
{
    class MM_EXPORT_DISH mmConfigValue
    {
    public:
        typedef std::map<std::string, mmValue> ValueMapType;
    private:
        ValueMapType d_value_map;
    public:
        mmConfigValue(void);
        virtual ~mmConfigValue(void);
    public:
        const ValueMapType& GetValueMap(void)const;
        ValueMapType& GetValueMapRef(void);
    public:
        template<typename ValueType>
        void GetValue(const std::string& _key, ValueType& _value)const
        {
            ValueMapType::const_iterator it = this->d_value_map.find(_key);
            if (it != this->d_value_map.end())
            {
                const mmValue& _v = it->second;
                _v.GetValue(_value);
            }
        }
        template<typename ValueType>
        void SetValue(const std::string& _key, const ValueType& _value)
        {
            ValueMapType::iterator it = this->d_value_map.find(_key);
            if (it != this->d_value_map.end())
            {
                mmValue& _v = it->second;
                _v.SetValue(_value);
            }
            else
            {
                // add mmValue unit.
                mmValue& _v = this->d_value_map[_key];
                _v.SetValue(_value);
            }
        }
    };
}
#endif//__mmConfigValue_h__
