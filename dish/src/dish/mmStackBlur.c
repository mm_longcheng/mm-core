/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmStackBlur.h"

#include "core/mmThread.h"

#include <pthread.h>

// http://vitiy.info/Code/stackblur.cpp

// The Stack Blur Algorithm was invented by Mario Klingemann,
// mario@quasimondo.com and described here:
// http://incubator.quasimondo.com/processing/fast_blur_deluxe.php

// This is C++ RGBA (32 bit color) multi-threaded version
// by Victor Laskin (victor.laskin@gmail.com)
// More details: http://vitiy.info/stackblur-algorithm-multi-threaded-blur-for-cpp

// This code is using MVThread class from my cross-platform framework
// You can exchange it with any thread implementation you like


// -------------------------------------- stackblur ----------------------------------------->

static unsigned short const stackblur_mul[255] =
{
    512,512,456,512,328,456,335,512,405,328,271,456,388,335,292,512,
    454,405,364,328,298,271,496,456,420,388,360,335,312,292,273,512,
    482,454,428,405,383,364,345,328,312,298,284,271,259,496,475,456,
    437,420,404,388,374,360,347,335,323,312,302,292,282,273,265,512,
    497,482,468,454,441,428,417,405,394,383,373,364,354,345,337,328,
    320,312,305,298,291,284,278,271,265,259,507,496,485,475,465,456,
    446,437,428,420,412,404,396,388,381,374,367,360,354,347,341,335,
    329,323,318,312,307,302,297,292,287,282,278,273,269,265,261,512,
    505,497,489,482,475,468,461,454,447,441,435,428,422,417,411,405,
    399,394,389,383,378,373,368,364,359,354,350,345,341,337,332,328,
    324,320,316,312,309,305,301,298,294,291,287,284,281,278,274,271,
    268,265,262,259,257,507,501,496,491,485,480,475,470,465,460,456,
    451,446,442,437,433,428,424,420,416,412,408,404,400,396,392,388,
    385,381,377,374,370,367,363,360,357,354,350,347,344,341,338,335,
    332,329,326,323,320,318,315,312,310,307,304,302,299,297,294,292,
    289,287,285,282,280,278,275,273,271,269,267,265,263,261,259,
};

static unsigned char const stackblur_shr[255] =
{
     9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17,
    17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19,
    19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20,
    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21,
    21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
    21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22,
    22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22,
    22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23,
    23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
    23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
    23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
    23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
    24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
    24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
    24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
    24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
};

// Stackblur algorithm body
MM_EXPORT_DISH 
void 
mmStackBlur_Job(
    unsigned char*                                 src,
    unsigned int                                   w,
    unsigned int                                   h,
    unsigned int                                   radius,
    int                                            cores,
    int                                            core,
    int                                            step,
    unsigned char*                                 stack)
{
    unsigned int x, y, xp, yp, i;
    unsigned int sp;
    unsigned int stack_start;
    unsigned char* stack_ptr;

    unsigned char* src_ptr;
    unsigned char* dst_ptr;

    unsigned long sum_r;
    unsigned long sum_g;
    unsigned long sum_b;
    unsigned long sum_a;
    unsigned long sum_in_r;
    unsigned long sum_in_g;
    unsigned long sum_in_b;
    unsigned long sum_in_a;
    unsigned long sum_out_r;
    unsigned long sum_out_g;
    unsigned long sum_out_b;
    unsigned long sum_out_a;

    unsigned int wm = w - 1;
    unsigned int hm = h - 1;
    unsigned int w4 = w * 4;
    unsigned int div = (radius * 2) + 1;
    unsigned int mul_sum = stackblur_mul[radius];
    unsigned char shr_sum = stackblur_shr[radius];


    if (step == 1)
    {
        unsigned int minY = core * h / cores;
        unsigned int maxY = (core + 1) * h / cores;

        for(y = minY; y < maxY; y++)
        {
            sum_r = sum_g = sum_b = sum_a =
            sum_in_r = sum_in_g = sum_in_b = sum_in_a =
            sum_out_r = sum_out_g = sum_out_b = sum_out_a = 0;

            // start of line (0, y)
            src_ptr = src + w4 * y;

            for(i = 0; i <= radius; i++)
            {
                stack_ptr    = &stack[ 4 * i ];
                stack_ptr[0] = src_ptr[0];
                stack_ptr[1] = src_ptr[1];
                stack_ptr[2] = src_ptr[2];
                stack_ptr[3] = src_ptr[3];
                sum_r += src_ptr[0] * (i + 1);
                sum_g += src_ptr[1] * (i + 1);
                sum_b += src_ptr[2] * (i + 1);
                sum_a += src_ptr[3] * (i + 1);
                sum_out_r += src_ptr[0];
                sum_out_g += src_ptr[1];
                sum_out_b += src_ptr[2];
                sum_out_a += src_ptr[3];
            }

            for(i = 1; i <= radius; i++)
            {
                if (i <= wm) src_ptr += 4;
                stack_ptr = &stack[ 4 * (i + radius) ];
                stack_ptr[0] = src_ptr[0];
                stack_ptr[1] = src_ptr[1];
                stack_ptr[2] = src_ptr[2];
                stack_ptr[3] = src_ptr[3];
                sum_r += src_ptr[0] * (radius + 1 - i);
                sum_g += src_ptr[1] * (radius + 1 - i);
                sum_b += src_ptr[2] * (radius + 1 - i);
                sum_a += src_ptr[3] * (radius + 1 - i);
                sum_in_r += src_ptr[0];
                sum_in_g += src_ptr[1];
                sum_in_b += src_ptr[2];
                sum_in_a += src_ptr[3];
            }

            sp = radius;
            xp = radius;
            if (xp > wm) xp = wm;
            // img.pix_ptr(xp, y);
            src_ptr = src + 4 * (xp + y * w);
            // img.pix_ptr(0, y);
            dst_ptr = src + y * w4; 
            for(x = 0; x < w; x++)
            {
                dst_ptr[0] = (unsigned char)((sum_r * mul_sum) >> shr_sum);
                dst_ptr[1] = (unsigned char)((sum_g * mul_sum) >> shr_sum);
                dst_ptr[2] = (unsigned char)((sum_b * mul_sum) >> shr_sum);
                dst_ptr[3] = (unsigned char)((sum_a * mul_sum) >> shr_sum);
                dst_ptr += 4;

                sum_r -= sum_out_r;
                sum_g -= sum_out_g;
                sum_b -= sum_out_b;
                sum_a -= sum_out_a;

                stack_start = sp + div - radius;
                if (stack_start >= div) stack_start -= div;
                stack_ptr = &stack[4 * stack_start];

                sum_out_r -= stack_ptr[0];
                sum_out_g -= stack_ptr[1];
                sum_out_b -= stack_ptr[2];
                sum_out_a -= stack_ptr[3];

                if(xp < wm)
                {
                    src_ptr += 4;
                    ++xp;
                }

                stack_ptr[0] = src_ptr[0];
                stack_ptr[1] = src_ptr[1];
                stack_ptr[2] = src_ptr[2];
                stack_ptr[3] = src_ptr[3];

                sum_in_r += src_ptr[0];
                sum_in_g += src_ptr[1];
                sum_in_b += src_ptr[2];
                sum_in_a += src_ptr[3];
                sum_r    += sum_in_r;
                sum_g    += sum_in_g;
                sum_b    += sum_in_b;
                sum_a    += sum_in_a;

                ++sp;
                if (sp >= div) sp = 0;
                stack_ptr = &stack[sp*4];

                sum_out_r += stack_ptr[0];
                sum_out_g += stack_ptr[1];
                sum_out_b += stack_ptr[2];
                sum_out_a += stack_ptr[3];
                sum_in_r  -= stack_ptr[0];
                sum_in_g  -= stack_ptr[1];
                sum_in_b  -= stack_ptr[2];
                sum_in_a  -= stack_ptr[3];
            }
        }
    }

    // step 2
    if (step == 2)
    {
        unsigned int minX = core * w / cores;
        unsigned int maxX = (core + 1) * w / cores;

        for(x = minX; x < maxX; x++)
        {
            sum_r =    sum_g =    sum_b =    sum_a =
            sum_in_r = sum_in_g = sum_in_b = sum_in_a =
            sum_out_r = sum_out_g = sum_out_b = sum_out_a = 0;

            // x, 0
            src_ptr = src + 4 * x;
            for(i = 0; i <= radius; i++)
            {
                stack_ptr    = &stack[i * 4];
                stack_ptr[0] = src_ptr[0];
                stack_ptr[1] = src_ptr[1];
                stack_ptr[2] = src_ptr[2];
                stack_ptr[3] = src_ptr[3];
                sum_r           += src_ptr[0] * (i + 1);
                sum_g           += src_ptr[1] * (i + 1);
                sum_b           += src_ptr[2] * (i + 1);
                sum_a           += src_ptr[3] * (i + 1);
                sum_out_r       += src_ptr[0];
                sum_out_g       += src_ptr[1];
                sum_out_b       += src_ptr[2];
                sum_out_a       += src_ptr[3];
            }
            for(i = 1; i <= radius; i++)
            {
                // +stride
                if(i <= hm) src_ptr += w4;

                stack_ptr = &stack[4 * (i + radius)];
                stack_ptr[0] = src_ptr[0];
                stack_ptr[1] = src_ptr[1];
                stack_ptr[2] = src_ptr[2];
                stack_ptr[3] = src_ptr[3];
                sum_r += src_ptr[0] * (radius + 1 - i);
                sum_g += src_ptr[1] * (radius + 1 - i);
                sum_b += src_ptr[2] * (radius + 1 - i);
                sum_a += src_ptr[3] * (radius + 1 - i);
                sum_in_r += src_ptr[0];
                sum_in_g += src_ptr[1];
                sum_in_b += src_ptr[2];
                sum_in_a += src_ptr[3];
            }

            sp = radius;
            yp = radius;
            if (yp > hm) yp = hm;
            // img.pix_ptr(x, yp);
            src_ptr = src + 4 * (x + yp * w);
            // img.pix_ptr(x, 0);
            dst_ptr = src + 4 * x;
            for(y = 0; y < h; y++)
            {
                dst_ptr[0] = (unsigned char)((sum_r * mul_sum) >> shr_sum);
                dst_ptr[1] = (unsigned char)((sum_g * mul_sum) >> shr_sum);
                dst_ptr[2] = (unsigned char)((sum_b * mul_sum) >> shr_sum);
                dst_ptr[3] = (unsigned char)((sum_a * mul_sum) >> shr_sum);
                dst_ptr += w4;

                sum_r -= sum_out_r;
                sum_g -= sum_out_g;
                sum_b -= sum_out_b;
                sum_a -= sum_out_a;

                stack_start = sp + div - radius;
                if(stack_start >= div) stack_start -= div;
                stack_ptr = &stack[4 * stack_start];

                sum_out_r -= stack_ptr[0];
                sum_out_g -= stack_ptr[1];
                sum_out_b -= stack_ptr[2];
                sum_out_a -= stack_ptr[3];

                if(yp < hm)
                {
                    // stride
                    src_ptr += w4;
                    ++yp;
                }

                stack_ptr[0] = src_ptr[0];
                stack_ptr[1] = src_ptr[1];
                stack_ptr[2] = src_ptr[2];
                stack_ptr[3] = src_ptr[3];

                sum_in_r += src_ptr[0];
                sum_in_g += src_ptr[1];
                sum_in_b += src_ptr[2];
                sum_in_a += src_ptr[3];
                sum_r    += sum_in_r;
                sum_g    += sum_in_g;
                sum_b    += sum_in_b;
                sum_a    += sum_in_a;

                ++sp;
                if (sp >= div) sp = 0;
                stack_ptr = &stack[sp*4];

                sum_out_r += stack_ptr[0];
                sum_out_g += stack_ptr[1];
                sum_out_b += stack_ptr[2];
                sum_out_a += stack_ptr[3];
                sum_in_r  -= stack_ptr[0];
                sum_in_g  -= stack_ptr[1];
                sum_in_b  -= stack_ptr[2];
                sum_in_a  -= stack_ptr[3];
            }
        }
    }
}

static 
void* 
__static_mmStackBlurWorker_PollTaskThread(
    void*                                          pArgs);

struct mmStackBlurWorker
{
    unsigned char* src;
    unsigned int w;
    unsigned int h;
    unsigned int radius;
    int cores;
    int core;
    int step;
    unsigned char* stack;

    int complete;

    pthread_t thread;
    pthread_mutex_t mutex;
    pthread_cond_t cond;

    // mmThreadState_t, default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

void 
mmStackBlurWorker_Init(
    struct mmStackBlurWorker*                      p)
{
    p->src = NULL;
    p->w = 0;
    p->h = 0;
    p->radius = 0;
    p->cores = 1;
    p->core = 0;
    p->step = 0;
    p->stack = NULL;
    
    p->complete = 0;
    
    pthread_mutex_init(&p->mutex, NULL);
    pthread_cond_init(&p->cond, NULL);
    p->state = MM_TS_CLOSED;
}

void 
mmStackBlurWorker_Destroy(
    struct mmStackBlurWorker*                      p)
{
    p->src = NULL;
    p->w = 0;
    p->h = 0;
    p->radius = 0;
    p->cores = 1;
    p->core = 0;
    p->step = 0;
    p->stack = NULL;
    
    p->complete = 0;
    
    pthread_mutex_destroy(&p->mutex);
    pthread_cond_destroy(&p->cond);
    p->state = MM_TS_CLOSED;
}

void 
mmStackBlurWorker_PollTask(
    struct mmStackBlurWorker*                      p)
{
    while (MM_TS_MOTION == p->state)
    {
        if(1 == p->complete)
        {
            mmStackBlur_Job(p->src, p->w, p->h, p->radius, p->cores, p->core, p->step, p->stack);
            
            p->complete = 0;
            
            pthread_mutex_lock(&p->mutex);
            pthread_cond_signal(&p->cond);
            pthread_mutex_unlock(&p->mutex);
        }
        else
        {
            pthread_mutex_lock(&p->mutex);
            pthread_cond_wait(&p->cond, &p->mutex);
            pthread_mutex_unlock(&p->mutex);
        }
    }
}

void 
mmStackBlurWorker_Start(
    struct mmStackBlurWorker*                      p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->thread, NULL, &__static_mmStackBlurWorker_PollTaskThread, p);
}

void 
mmStackBlurWorker_Interrupt(
    struct mmStackBlurWorker*                      p)
{
    p->state = MM_TS_CLOSED;
    pthread_mutex_lock(&p->mutex);
    pthread_cond_signal(&p->cond);
    pthread_mutex_unlock(&p->mutex);
}

void 
mmStackBlurWorker_Shutdown(
    struct mmStackBlurWorker*                      p)
{
    p->state = MM_TS_FINISH;
    pthread_mutex_lock(&p->mutex);
    pthread_cond_signal(&p->cond);
    pthread_mutex_unlock(&p->mutex);
}

void 
mmStackBlurWorker_Join(
    struct mmStackBlurWorker*                      p)
{
    pthread_join(p->thread, NULL);
}

void 
mmStackBlurWorker_Wake(
    struct mmStackBlurWorker*                      p)
{
    p->complete = 1;
    
    pthread_mutex_lock(&p->mutex);
    pthread_cond_signal(&p->cond);
    pthread_mutex_unlock(&p->mutex);
}

void 
mmStackBlurWorker_Wait(
    struct mmStackBlurWorker*                      p)
{
    if(1 == p->complete)
    {
        pthread_mutex_lock(&p->mutex);
        pthread_cond_wait(&p->cond, &p->mutex);
        pthread_mutex_unlock(&p->mutex);
    }
}

static 
void* 
__static_mmStackBlurWorker_PollTaskThread(
    void*                                          pArgs)
{
    struct mmStackBlurWorker* p = (struct mmStackBlurWorker*)(pArgs);
    mmStackBlurWorker_PollTask(p);
    return NULL;
}

MM_EXPORT_DISH 
void 
mmStackBlur_SingleThread(
    unsigned char*                                 src,
    unsigned int                                   w,
    unsigned int                                   h,
    unsigned int                                   radius)
{
    if (radius > 254)
    {
        return;
    }
    if (radius < 2)
    {
        return;
    }
    
    {
        int cores = 1;
        unsigned int div = (radius * 2) + 1;
        unsigned char* stack = (unsigned char*)malloc(sizeof(unsigned char) * div * 4 * cores);
        
        mmStackBlur_Job(src, w, h, radius, 1, 0, 1, stack);
        mmStackBlur_Job(src, w, h, radius, 1, 0, 2, stack);
        
        free(stack);
    }
}

// Stackblur algorithm by Mario Klingemann
// Details here:
// http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
// C++ implemenation base from:
// https://gist.github.com/benjamin9999/3809142
// http://www.antigrain.com/__code/include/agg_blur.h.html
// This version works only with RGBA color
MM_EXPORT_DISH 
void 
mmStackBlur_MultiThread(
    unsigned char*                                 src,
    unsigned int                                   w,
    unsigned int                                   h,
    unsigned int                                   radius,
    int                                            cores)
{
    if (radius > 254)
    {
        return;
    }
    if (radius < 2)
    {
        return;
    }

    {
        unsigned int div = (radius * 2) + 1;
        unsigned char* stack = (unsigned char*)malloc(sizeof(unsigned char) * div * 4 * cores);

        if (cores == 1)
        {
            // no multithreading
            mmStackBlur_Job(src, w, h, radius, 1, 0, 1, stack);
            mmStackBlur_Job(src, w, h, radius, 1, 0, 2, stack);
        }
        else
        {
            int i = 0;
            struct mmStackBlurWorker** workers = (struct mmStackBlurWorker**)malloc(sizeof(struct mmStackBlurWorker) * cores);
            for (i = 0; i < cores; i++)
            {
                struct mmStackBlurWorker* worker = (struct mmStackBlurWorker*)malloc(sizeof(struct mmStackBlurWorker));
                mmStackBlurWorker_Init(worker);
                worker->src = src;
                worker->w = w;
                worker->h = h;
                worker->radius = radius;
                worker->cores = cores;
                worker->core = i;
                worker->step = 1;
                worker->stack = stack + div * 4 * i;
                mmStackBlurWorker_Start(worker);
                mmStackBlurWorker_Wake(worker);
                workers[i] = worker;
            }

            for (i = 0; i < cores; i++)
            {
                struct mmStackBlurWorker* worker = workers[i];
                mmStackBlurWorker_Wait(worker);
            }
            
            for (i = 0; i < cores; i++)
            {
                struct mmStackBlurWorker* worker = workers[i];
                worker->step = 2;
                mmStackBlurWorker_Wake(worker);
            }

            for (i = 0; i < cores; i++)
            {
                struct mmStackBlurWorker* worker = workers[i];
                mmStackBlurWorker_Wait(worker);
                
                mmStackBlurWorker_Shutdown(worker);
                mmStackBlurWorker_Join(worker);
                mmStackBlurWorker_Destroy(worker);
                free(worker);
            }

            free(workers);
        }

        free(stack);
    }
}
