/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmErrorDesc_h__
#define __mmErrorDesc_h__

#include "core/mmCore.h"

#include "container/mmRbtreeU32.h"

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

struct mmErrorDesc
{
    struct mmRbtreeU32Vpt rbtree;
    // cache for error desc.
    struct mmString desc;
    // cache for error code.
    mmUInt32_t code;
};

MM_EXPORT_DISH 
void 
mmErrorDesc_Init(
    struct mmErrorDesc*                            p);

MM_EXPORT_DISH 
void mmErrorDesc_Destroy(
    struct mmErrorDesc*                            p);

MM_EXPORT_DISH 
void 
mmErrorDesc_Clear(
    struct mmErrorDesc*                            p);

MM_EXPORT_DISH 
const char* 
mmErrorDesc_CodeString(
    struct mmErrorDesc*                            p, 
    int                                            code);

// use the code cache current code -> desc.
MM_EXPORT_DISH 
void 
mmErrorDesc_CacheResult(
    struct mmErrorDesc*                            p, 
    int                                            code);

MM_EXPORT_DISH 
void 
mmErrorDesc_SetCodeDesc(
    struct mmErrorDesc*                            p, 
    int                                            code, 
    const char*                                    desc);

MM_EXPORT_DISH 
const char* 
mmErrorDesc_Unknown(void);

enum mmErrCore_t
{
    MM_ERR_CORE_UNKNOWN = -1,//(-1)unknown
    MM_ERR_CORE_SUCCESS =  0,//( 0)success 
};

MM_EXPORT_DISH 
void 
mmErrorDesc_SetCore(
    struct mmErrorDesc*                            p);

#include "core/mmSuffix.h"

#endif//__mmErrorDesc_h__
