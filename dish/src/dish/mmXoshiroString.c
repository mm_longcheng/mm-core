/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmXoshiroString.h"

MM_EXPORT_DISH const char MM_XOSHIRO_RANDOM_NUMBER_CHAR_L = '!';
MM_EXPORT_DISH const char MM_XOSHIRO_RANDOM_NUMBER_CHAR_R = '~';
MM_EXPORT_DISH const int  MM_XOSHIRO_RANDOM_NUMBER_CHAR_N = 94;

MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomBuffer(
    struct mmXoshiro256starstar*                   r,
    unsigned char*                                 buffer,
    unsigned int                                   offset,
    size_t                                         length)
{
    size_t i = 0;
    mmUInt64_t rn = 0;
    int val = 0;

    for (i = 0; i < length; ++i)
    {
        rn = mmXoshiro256starstar_Next(r);
        val = (int)rn % MM_XOSHIRO_RANDOM_NUMBER_CHAR_N;
        val += MM_XOSHIRO_RANDOM_NUMBER_CHAR_L;
        buffer[offset + i] = (char)val;
    }
}

MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length)
{
    char* d = NULL;
    mmString_Resize(v, length + 1);
    d = (char*)mmString_Data(v);
    d[length] = '\0';
    mmXoshiro256starstar_RandomBuffer(
        r, (unsigned char*)d, 
        0, (unsigned int)length);
}

MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomTableBuffer(
    struct mmXoshiro256starstar*                   r,
    unsigned char*                                 buffer,
    unsigned int                                   offset,
    size_t                                         length,
    const char*                                    t,
    size_t                                         n)
{
    size_t i = 0;
    mmUInt64_t rn = 0;
    int val = 0;

    for (i = 0; i < length; ++i)
    {
        rn = mmXoshiro256starstar_Next(r);
        val = (int)rn % n;
        buffer[offset + i] = (char)t[val];
    }
}

MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomTableString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length,
    const char*                                    t,
    size_t                                         n)
{
    char* d = NULL;
    mmString_Resize(v, length + 1);
    d = (char*)mmString_Data(v);
    d[length] = '\0';
    mmXoshiro256starstar_RandomTableBuffer(
        r, (unsigned char*)d, 
        0, (unsigned int)length, t, n);
}


MM_EXPORT_DISH const char   MM_XOSHIRO_RANDOM_TABLE_A[62] =
{
    "0123456789"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
};
MM_EXPORT_DISH const size_t MM_XOSHIRO_RANDOM_TABLE_N0 = 62;
MM_EXPORT_DISH const size_t MM_XOSHIRO_RANDOM_TABLE_N1 = 36;

// [0, 9] [A, z]
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomCapableNumberCaseString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length)
{
    mmXoshiro256starstar_RandomTableString(
        r, v, length, 
        MM_XOSHIRO_RANDOM_TABLE_A, 
        MM_XOSHIRO_RANDOM_TABLE_N0);
}

// [0, 9] [A, Z]
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomCapableNumberString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length)
{
    mmXoshiro256starstar_RandomTableString(
        r, v, length, 
        MM_XOSHIRO_RANDOM_TABLE_A, 
        MM_XOSHIRO_RANDOM_TABLE_N1);
}
