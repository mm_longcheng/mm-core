/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTerminal.h"

#include "core/mmTime.h"
#include "core/mmErrno.h"
#include "core/mmLogger.h"

#include <stdio.h>

static void* __static_mmTerminal_ThreadPoll(void* _arg);

MM_EXPORT_DISH void mmTerminalCallback_Init(struct mmTerminalCallback* p)
{
    p->Handle = NULL;
    p->obj = NULL;
}
MM_EXPORT_DISH void mmTerminalCallback_Destroy(struct mmTerminalCallback* p)
{
    p->Handle = NULL;
    p->obj = NULL;
}

MM_EXPORT_DISH void mmTerminal_Init(struct mmTerminal* p)
{
    mmString_Init(&p->prompt);
    mmString_Assigns(&p->prompt, ">");
    mmTerminalCallback_Init(&p->callback);
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DISH void mmTerminal_Destroy(struct mmTerminal* p)
{
    mmString_Destroy(&p->prompt);
    mmTerminalCallback_Destroy(&p->callback);
    p->state = MM_TS_CLOSED;
}

MM_EXPORT_DISH void mmTerminal_Start(struct mmTerminal* p)
{
    p->state = MM_TS_FINISH == p->state ? MM_TS_CLOSED : MM_TS_MOTION;
    pthread_create(&p->thread_stdin, NULL, &__static_mmTerminal_ThreadPoll, p);
}
MM_EXPORT_DISH void mmTerminal_Interrupt(struct mmTerminal* p)
{
    p->state = MM_TS_CLOSED;
}
MM_EXPORT_DISH void mmTerminal_Shutdown(struct mmTerminal* p)
{
    p->state = MM_TS_FINISH;
}
MM_EXPORT_DISH void mmTerminal_Join(struct mmTerminal* p)
{
    pthread_join(p->thread_stdin, NULL);
}
MM_EXPORT_DISH void mmTerminal_SetPrompt(struct mmTerminal* p, const char* prompt)
{
    mmString_Assigns(&p->prompt, prompt);
}
MM_EXPORT_DISH void mmTerminal_SetCallback(struct mmTerminal* p, struct mmTerminalCallback* callback)
{
    assert(NULL != callback && "you can not assign null callback.");
    p->callback = *callback;
}
MM_EXPORT_DISH void mmTerminal_Loop(struct mmTerminal* p)
{
    char buf[MM_CMD_STDIN_MAX_LINE];
    size_t len = 0;
    assert(p->callback.Handle && "p->callback.Handle is a null.");

    while (MM_TS_MOTION == p->state)
    {
        mmPrintf("%s", mmString_CStr(&p->prompt));
        fflush(stdout);
        if (fgets(buf, MM_CMD_STDIN_MAX_LINE, stdin) == NULL)
        {
            mmErr_t errcode = mmErrno;
            mmPrintf("errno:(%d) %s\n", errcode, strerror(errcode));
            break;
        }
        len = strlen(buf);
        while (len && (buf[len - 1] == '\n' || buf[len - 1] == '\r'))
        {
            len--;
            buf[len] = '\0';
        }
        if (len > 0)
        {
            (*(p->callback.Handle))(p, buf, len);
        }
    }
}
MM_EXPORT_DISH void mmTerminal_FlushPrompt(struct mmTerminal* p)
{
    mmPrintf("%s", mmString_CStr(&p->prompt));
    fflush(stdout);
}

static void* __static_mmTerminal_ThreadPoll(void* _arg)
{
    struct mmTerminal* p = (struct mmTerminal*)(_arg);
    mmTerminal_Loop(p);
    return NULL;
}
