/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEvent.h"
#include <algorithm>

namespace mm
{
    class mmEventHandlerCompare
    {
    private:
        const mmEventHandler& hHandler;
    public:
        mmEventHandlerCompare(const mmEventHandler& s)
            : hHandler(s)
        {

        }

        bool operator()(std::pair<mmEvent::GroupType, mmEventHandler> e) const
        {
            return e.second == this->hHandler;
        }

    private:
        void operator=(const mmEventHandlerCompare&) {}
    };
    
    bool mmEventHandler::operator==(const mmEventHandler& rhs) const
    {
        mmEventHandlerBase* l = this->hHandler.get();
        mmEventHandlerBase* r = rhs.hHandler.get();
        if (NULL == l && NULL == r)
        {
            return true;
        }
        if (NULL != l && NULL != r)
        {
            return *(l) == *(r);
        }
        else
        {
            return false;
        }
    }
    
    void mmEvent::operator()(mmEventArgs& args)
    {
        HandlerMapType::iterator iter = this->hHandlerMap.begin();
        const HandlerMapType::const_iterator end_iter = this->hHandlerMap.end();

        // execute all subscribers, updating the 'handled' state as we go
        for (; iter != end_iter; ++iter)
        {
            if ((*(iter->second.hHandler))(args))
            {
                ++args.hHandled;
            }
        }
    }

    mmEventHandler mmEvent::Subscribe(const mmEventHandler& handler)
    {
        return this->Subscribe(static_cast<GroupType>(-1), handler);
    }

    mmEventHandler mmEvent::Subscribe(GroupType group, const mmEventHandler& handler)
    {
        this->hHandlerMap.insert(HandlerMapType::value_type(group, handler));
        return handler;
    }

    void mmEvent::Unsubscribe(const mmEventHandler& handler)
    {
        // try to find the slot in our collection
        HandlerMapType::iterator l = this->hHandlerMap.begin();
        HandlerMapType::iterator r = this->hHandlerMap.end();
        HandlerMapType::iterator c = std::find_if(l, r, mmEventHandlerCompare(handler));

        // erase our reference to the slot, if we had one.
        if (c != this->hHandlerMap.end())
        {
            this->hHandlerMap.erase(c);
        }
    }

    mmEventSet::EventSharedPtr mmEventSet::AddEvent(const std::string& name)
    {
        EventSharedPtr e = this->GetEvent(name);
        if (NULL == e)
        {
            e = EventSharedPtr(new mmEvent);
            e->SetName(name);
            this->hEventMap.insert(EventMapType::value_type(name, e));
        }
        return e;
    }

    mmEventSet::EventSharedPtr mmEventSet::GetEvent(const std::string& name) const
    {
        EventSharedPtr e = NULL;
        EventMapType::const_iterator it = this->hEventMap.find(name);
        if (it != this->hEventMap.end())
        {
            e = it->second;
        }
        return e;
    }

    mmEventSet::EventSharedPtr mmEventSet::GetEventInstance(const std::string& name)
    {
        EventSharedPtr e = this->GetEvent(name);
        if (NULL == e)
        {
            e = this->AddEvent(name);
        }
        return e;
    }

    void mmEventSet::RmvEvent(const std::string& name)
    {
        EventMapType::const_iterator it = this->hEventMap.find(name);
        if (it != this->hEventMap.end())
        {
            EventSharedPtr e = it->second;
            e = EventSharedPtr(NULL);
            this->hEventMap.erase(it);
        }
    }

    void mmEventSet::ClearEvent(void)
    {
        EventMapType::const_iterator it = this->hEventMap.begin();
        while (it != this->hEventMap.end())
        {
            EventSharedPtr e = it->second;
            e = EventSharedPtr(NULL);
            this->hEventMap.erase(it++);
        }
    }

    void mmEventSet::UnsubscribeEvent(const std::string& name, const mmEventHandler& handler)
    {
        EventSharedPtr e = this->GetEvent(name);
        if (NULL != e)
        {
            e->Unsubscribe(handler);
        }
    }

    void mmEventSet::FireEvent(const std::string& name, mmEventArgs& args)
    {
        EventSharedPtr e = this->GetEvent(name);
        if ((NULL != e) && (false == this->hIsMuted))
        {
            (*e)(args);
        }
    }
}

