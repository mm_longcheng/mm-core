/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmConfigParse.h"

#include "core/mmString.h"

#include <assert.h>

namespace mm
{
    mmConfigParse::mmConfigParse(mmConfigValue& _config)
        :d_config_value(_config)
    {

    }

    mmConfigParse::~mmConfigParse(void)
    {

    }

    std::string mmConfigParse::RejectEmpty(const std::string& _key)
    {
        std::string k1;
        std::string k2;
        bool find_empty(false);
        for (std::string::const_reverse_iterator it = _key.rbegin();
            it != _key.rend(); it++)
        {
            char c = *it;
            if (find_empty || c != ' ')
            {
                k1.push_back(c);
                find_empty = true;
            }
        }
        find_empty = false;
        for (std::string::const_reverse_iterator it = k1.rbegin();
            it != k1.rend(); it++)
        {
            char c = *it;
            if (find_empty || c != ' ')
            {
                k2.push_back(c);
                find_empty = true;
            }
        }
        return k2;
    }

    void mmConfigParse::LoadFile(const char* _file_name)
    {
        assert(_file_name);
        FILE* fdrb = fopen(_file_name, "rb");
        assert(fdrb);
        fseek(fdrb, 0, SEEK_END);
        long fdsize = ftell(fdrb);
        fseek(fdrb, 0, SEEK_SET);
        char* fdbf = (char*)malloc(sizeof(char)*fdsize);
        fread(fdbf, sizeof(char), fdsize, fdrb);
        this->LoadBuffer(fdbf, fdsize);
        free(fdbf);
    }

    void mmConfigParse::LoadBuffer(const char* _buffer, size_t _size)
    {
        std::string line;
        bool _is_note = false;
        
        assert(_buffer);

        for (size_t i = 0; i < _size; i++)
        {
            if (_buffer[i] == '/'&&_buffer[i + 1] == '/')
            {
                _is_note = true;
                i++;
            }
            if (_buffer[i] == '\n' ||
                _buffer[i] == '\r')
            {
                if (!line.empty())
                {
                    this->LoadLine(line);
                    line.clear();
                }
                _is_note = false;
            }
            else
            {
                if (!_is_note)
                {
                    line.push_back(_buffer[i]);
                }
            }
        }
        if (!line.empty())
        {
            this->LoadLine(line);
            line.clear();
        }
    }

    void mmConfigParse::LoadLine(const std::string& _line)
    {
        std::string key;
        std::string val;
        bool equal_sign = false;
        for (std::string::const_iterator it = _line.begin();
            it != _line.end(); it++)
        {
            char c = *it;
            if (c == '=')
            {
                equal_sign = true;
            }
            else
            {
                if (equal_sign)
                {
                    if (mmConfigParse::IsPrintf(c))
                    {
                        val.push_back(c);
                    }
                }
                else
                {
                    if (mmConfigParse::IsPrintf(c))
                    {
                        key.push_back(c);
                    }
                }
            }
        }
        this->d_config_value.SetValue(mmConfigParse::RejectEmpty(key), mmConfigParse::RejectEmpty(val));
    }

    bool mmConfigParse::IsPrintf(char _c)
    {
        //lazy judge use to Chinese.
        if (_c < 0)
        {
            return true;
        }
        return isprint(_c) != 0;
    }

    const mmConfigValue& mmConfigParse::GetConfigValue(void) const
    {
        return this->d_config_value;
    }

    mmConfigValue& mmConfigParse::GetConfigValueRef(void)
    {
        return this->d_config_value;
    }

}

