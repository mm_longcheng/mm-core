/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileContext_h__
#define __mmFileContext_h__

#include "core/mmCore.h"
#include "core/mmList.h"
#include "core/mmByte.h"
#include "core/mmFileSystem.h"

#include "container/mmRbtreeString.h"

#include "mmFileZip.h"

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

struct mmStream;

struct mmIFileAssets
{
    //int
    //(*AssetsType)(
    //    const struct mmIFileAssets*                p);
    void* AssetsType;

    //int
    //(*IsDataExists)(
    //    const struct mmIFileAssets*                p,
    //    const char*                                pRealName);
    void* IsDataExists;

    //void
    //(*FileInfoName)(
    //    const struct mmIFileAssets*                p,
    //    struct mmString*                           pInfoName,
    //    const char*                                pFileName);
    void* FileInfoName;

    //void
    //(*PathNameToRealName)(
    //    const struct mmIFileAssets*                p,
    //    struct mmString*                           pRealName,
    //    const char*                                pPathName);
    void* PathNameToRealName;

    //void
    //(*RealNameToPathName)(
    //    const struct mmIFileAssets*                p,
    //    struct mmString*                           pPathName,
    //    const char*                                pRealName);
    void* RealNameToPathName;

    //void
    //(*AcquireFileByteBuffer)(
    //    const struct mmIFileAssets*                p,
    //    const char*                                pRealName,
    //    struct mmByteBuffer*                       pByteBuffer);
    void* AcquireFileByteBuffer;

    //void
    //(*ReleaseFileByteBuffer)(
    //    const struct mmIFileAssets*                p,
    //    struct mmByteBuffer*                       pByteBuffer);
    void* ReleaseFileByteBuffer;

    //void
    //(*AcquireFindFiles)(
    //    const struct mmIFileAssets*                p,
    //    const char*                                pDirectory,
    //    const char*                                pPattern,
    //    int                                        hRecursive,
    //    int                                        hDirs,
    //    int                                        hIgnoreHidden,
    //    struct mmRbtreeStringVpt*                  pRbtree);
    void* AcquireFindFiles;

    //void
    //(*ReleaseFindFiles)(
    //    const struct mmIFileAssets*                p,
    //    struct mmRbtreeStringVpt*                  pRbtree);
    void* ReleaseFindFiles;
};

// ../../resources/assets
// path ../../resources
// base assets
struct mmFileAssetsFolder
{
    const struct mmIFileAssets* metadata;

    // name for assets.
    struct mmString name;
    // path for assets.
    struct mmString path;
    // base for assets.
    struct mmString base;
};

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_Init(
    struct mmFileAssetsFolder*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_Destroy(
    struct mmFileAssetsFolder*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_SetAssets(
    struct mmFileAssetsFolder*                     p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_AcquireEntry(
    struct mmFileAssetsFolder*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_ReleaseEntry(
    struct mmFileAssetsFolder*                     p);

MM_EXPORT_DISH
int
mmFileAssetsFolder_AssetsType(
    const struct mmFileAssetsFolder*               p);

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH 
int 
mmFileAssetsFolder_IsDataExists(
    const struct mmFileAssetsFolder*               p,
    const char*                                    pRealName);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_FileInfoName(
    const struct mmFileAssetsFolder*               p,
    struct mmString*                               pInfoName,
    const char*                                    pFileName);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_PathNameToRealName(
    const struct mmFileAssetsFolder*               p,
    struct mmString*                               pRealName,
    const char*                                    pPathName);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_RealNameToPathName(
    const struct mmFileAssetsFolder*               p,
    struct mmString*                               pPathName,
    const char*                                    pRealName);

// acquire file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsFolder_AcquireFileByteBuffer(
    const struct mmFileAssetsFolder*               p,
    const char*                                    pRealName,
    struct mmByteBuffer*                           pByteBuffer);

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsFolder_ReleaseFileByteBuffer(
    const struct mmFileAssetsFolder*               p,
    struct mmByteBuffer*                           pByteBuffer);
    
MM_EXPORT_DISH
void
mmFileAssetsFolder_AcquireFindFiles(
    const struct mmFileAssetsFolder*               p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree);

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_ReleaseFindFiles(
    const struct mmFileAssetsFolder*               p,
    struct mmRbtreeStringVpt*                      rbtree);

// ../../resources/assets.zip
// path ../../resources/assets.zip
// base assets (zip file inside )
struct mmFileAssetsSource
{
    const struct mmIFileAssets* metadata;

    // name for assets.
    struct mmString name;
    // path for assets.
    struct mmString path;
    // base for assets.
    struct mmString base;

    // zip file.
    struct mmFileZip zip;
};

MM_EXPORT_DISH 
void 
mmFileAssetsSource_Init(
    struct mmFileAssetsSource*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_Destroy(
    struct mmFileAssetsSource*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_SetAssets(
    struct mmFileAssetsSource*                     p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_AcquireEntry(
    struct mmFileAssetsSource*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_ReleaseEntry(
    struct mmFileAssetsSource*                     p);

MM_EXPORT_DISH
int
mmFileAssetsSource_AssetsType(
    const  struct mmFileAssetsSource*              p);

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH 
int 
mmFileAssetsSource_IsDataExists(
    const struct mmFileAssetsSource*               p,
    const char*                                    pRealName);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_FileInfoName(
    const struct mmFileAssetsSource*               p,
    struct mmString*                               pInfoName,
    const char*                                    pFileName);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_PathNameToRealName(
    const struct mmFileAssetsSource*               p,
    struct mmString*                               pRealName,
    const char*                                    pPathName);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_RealNameToPathName(
    const struct mmFileAssetsSource*               p,
    struct mmString*                               pPathName,
    const char*                                    pRealName);

// acquire file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsSource_AcquireFileByteBuffer(
    const struct mmFileAssetsSource*               p,
    const char*                                    pRealName,
    struct mmByteBuffer*                           pByteBuffer);

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsSource_ReleaseFileByteBuffer(
    const struct mmFileAssetsSource*               p,
    struct mmByteBuffer*                           pByteBuffer);

MM_EXPORT_DISH
void
mmFileAssetsSource_AcquireFindFiles(
    const struct mmFileAssetsSource*               p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree);

MM_EXPORT_DISH 
void 
mmFileAssetsSource_ReleaseFindFiles(
    const struct mmFileAssetsSource*               p,
    struct mmRbtreeStringVpt*                      pRbtree);

enum mmFileAssetsType
{
    MM_FILE_ASSETS_TYPE_FOLDER = 0,
    MM_FILE_ASSETS_TYPE_SOURCE = 1,
};

struct mmFileAssetsCacher
{
    int type;
    struct mmIFileAssets** file;
};

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_Init(
    struct mmFileAssetsCacher*                     p);

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_Destroy(
    struct mmFileAssetsCacher*                     p);

// copy q to p.
MM_EXPORT_DISH 
void 
mmFileAssetsCacher_Copy(
    struct mmFileAssetsCacher*                     p, 
    const struct mmFileAssetsCacher*               q);

MM_EXPORT_DISH 
int
mmFileAssetsCacher_AssetsType(
    const struct mmFileAssetsCacher*               p);

MM_EXPORT_DISH
int
mmFileAssetsCacher_IsDataExists(
    const struct mmFileAssetsCacher*               p,
    const char*                                    pRealName);

MM_EXPORT_DISH
void
mmFileAssetsCacher_FileInfoName(
    const struct mmFileAssetsCacher*               p,
    struct mmString*                               pInfoName,
    const char*                                    pFileName);

MM_EXPORT_DISH
void
mmFileAssetsCacher_PathNameToRealName(
    const struct mmFileAssetsCacher*               p,
    struct mmString*                               pRealName,
    const char*                                    pPathName);

MM_EXPORT_DISH
void
mmFileAssetsCacher_RealNameToPathName(
    const struct mmFileAssetsCacher*               p,
    struct mmString*                               pPathName,
    const char*                                    pRealName);

MM_EXPORT_DISH
void
mmFileAssetsCacher_AcquireFileByteBuffer(
    const struct mmFileAssetsCacher*               p,
    const char*                                    pRealName,
    struct mmByteBuffer*                           pByteBuffer);

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_ReleaseFileByteBuffer(
    const struct mmFileAssetsCacher*               p,
    struct mmByteBuffer*                           pByteBuffer);

MM_EXPORT_DISH
void
mmFileAssetsCacher_AcquireFindFiles(
    const struct mmFileAssetsCacher*               p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree);

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_ReleaseFindFiles(
    const struct mmFileAssetsCacher*               p,
    struct mmRbtreeStringVpt*                      pRbtree);

struct mmFileAssetsInfo
{
    struct mmFileAssetsCacher cacher;
    // the file's fully qualified name
    struct mmString filename;
    // base filename
    struct mmString basename;
    // path name; separated by '/' and ending with '/'
    struct mmString pathname;
    // compressed size
    size_t csize;
    // uncompressed size
    size_t dsize;
};

MM_EXPORT_DISH 
void 
mmFileAssetsInfo_Init(
    struct mmFileAssetsInfo*                       p);

MM_EXPORT_DISH 
void 
mmFileAssetsInfo_Destroy(
    struct mmFileAssetsInfo*                       p);

struct mmFileContext
{
    struct mmString folder_path;
    struct mmString folder_base;
    struct mmString source_path;
    struct mmString source_base;

    struct mmRbtreeStringVpt folders;
    struct mmRbtreeStringVpt sources;
    struct mmRbtreeStringVpt cachers;
};

MM_EXPORT_DISH 
void 
mmFileContext_Init(
    struct mmFileContext*                          p);

MM_EXPORT_DISH 
void 
mmFileContext_Destroy(
    struct mmFileContext*                          p);

MM_EXPORT_DISH 
void 
mmFileContext_SetAssetsRootFolder(
    struct mmFileContext*                          p, 
    const char*                                    pRootFolder, 
    const char*                                    pRootBase);

MM_EXPORT_DISH 
void 
mmFileContext_SetAssetsRootSource(
    struct mmFileContext*                          p, 
    const char*                                    pRootSource,
    const char*                                    pRootBase);

MM_EXPORT_DISH 
struct mmFileAssetsFolder* 
mmFileContext_AddAssetsFolder(
    struct mmFileContext*                          p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base);

MM_EXPORT_DISH 
struct mmFileAssetsFolder* 
mmFileContext_GetAssetsFolder(
    struct mmFileContext*                          p, 
    const char*                                    name);

MM_EXPORT_DISH 
void 
mmFileContext_RmvAssetsFolder(
    struct mmFileContext*                          p, 
    const char*                                    name);

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsFolder(
    struct mmFileContext*                          p);

MM_EXPORT_DISH 
struct mmFileAssetsSource* 
mmFileContext_AddAssetsSource(
    struct mmFileContext*                          p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base);

MM_EXPORT_DISH 
struct mmFileAssetsSource* 
mmFileContext_GetAssetsSource(
    struct mmFileContext*                          p, 
    const char*                                    name);

MM_EXPORT_DISH 
void 
mmFileContext_RmvAssetsSource(
    struct mmFileContext*                          p, 
    const char*                                    name);

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsSource(
    struct mmFileContext*                          p);

MM_EXPORT_DISH 
struct mmFileAssetsCacher* 
mmFileContext_AddAssetsCacher(
    struct mmFileContext*                          p, 
    const char*                                    pRealName);

MM_EXPORT_DISH 
struct mmFileAssetsCacher* 
mmFileContext_GetAssetsCacher(
    struct mmFileContext*                          p, 
    const char*                                    pRealName);

MM_EXPORT_DISH 
void 
mmFileContext_RmvAssetsCacher(
    struct mmFileContext*                          p, 
    const char*                                    pRealName);

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsCacher(
    struct mmFileContext*                          p);

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsCacherByPointer(
    struct mmFileContext*                          p, 
    void*                                          file);

// Get file assets type.
// pPathName is assets logic absolute.
// return value
//     == -1 not exists.
//     >=  0 exists assets type.
MM_EXPORT_DISH
int
mmFileContext_GetFileAssetsType(
    struct mmFileContext*                          p,
    const char*                                    pPathName);

// Get file assets real path name.
// pPathName is assets logic absolute.
// return value
//     == -1 not exists.
//     >=  0 exists assets type.
MM_EXPORT_DISH
int
mmFileContext_GetFileAssetsPath(
    struct mmFileContext*                          p,
    const char*                                    pPathName,
    struct mmString*                               pRealName);

// check file exists. 
// pPathName is assets logic absolute.
// return value
//     1 exists 
//     0 not exists.
MM_EXPORT_DISH 
int 
mmFileContext_IsFileExists(
    struct mmFileContext*                          p, 
    const char*                                    pPathName);

// acquire file buffer.
// priority 0 folder 1 source, note: we will first acquire folder file byte buffer.
MM_EXPORT_DISH 
void 
mmFileContext_AcquireFileByteBuffer(
    struct mmFileContext*                          p, 
    const char*                                    pPathName,
    struct mmByteBuffer*                           pByteBuffer);

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileContext_ReleaseFileByteBuffer(
    struct mmFileContext*                          p, 
    struct mmByteBuffer*                           pByteBuffer);

// acquire file stream.
// priority 0 folder 1 source, note: we will first acquire folder file byte buffer.
MM_EXPORT_DISH
void
mmFileContext_AcquireStream(
    struct mmFileContext*                          p,
    const char*                                    pPathName,
    const char*                                    pMode,
    struct mmStream*                               pStream);

// release file stream.
MM_EXPORT_DISH
void
mmFileContext_ReleaseStream(
    struct mmFileContext*                          p,
    struct mmStream*                               pStream);

MM_EXPORT_DISH 
void 
mmFileContext_AcquireFindFiles(
    struct mmFileContext*                          p, 
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree);

MM_EXPORT_DISH 
void 
mmFileContext_ReleaseFindFiles(
    struct mmFileContext*                          p, 
    struct mmRbtreeStringVpt*                      pRbtree);

#include "core/mmSuffix.h"

#endif//__mmFileContext_h__
