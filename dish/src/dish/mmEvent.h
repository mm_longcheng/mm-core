/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEvent_h__
#define __mmEvent_h__

#include "core/mmCore.h"

#include "dish/mmEventArgs.h"

#include <map>
#include <memory>
#include <string>

#include "dish/mmDishExport.h"

// CEGUI: http://cegui.org.uk/
// Port for CEGUI::EventSet

namespace mm
{
    // Event handler base class.
    class MM_EXPORT_DISH mmEventHandlerBase
    {
    public:
        virtual ~mmEventHandlerBase(void) {};
        virtual bool operator()(const mmEventArgs& args) = 0;
        virtual bool operator==(const mmEventHandlerBase& rhs) const = 0;
    };

    // Object function event handler.
    // It is useful for c like API pass by `void*` object.
    class MM_EXPORT_DISH mmEventHandlerObject : public mmEventHandlerBase
    {
    public:
        typedef bool(*FunctionObjectType)(void* obj, const mmEventArgs& args);

        FunctionObjectType hFunction;
        void* pObject;
    public:
        mmEventHandlerObject(FunctionObjectType func, void* obj)
            : hFunction(func)
            , pObject(obj)
        {

        }

        virtual bool operator()(const mmEventArgs& args)
        {
            return (*(this->hFunction))(this->pObject, args);
        }

        virtual bool operator==(const mmEventHandlerBase& rhs) const
        {
            // there is no static information if Functor holds a member function
            // or a free function; this is the main difference to tr1::function
            if (typeid(*this) != typeid(rhs))
            {
                // cannot be equal
                return false;
            }
            else
            {
                const mmEventHandlerObject& eho = static_cast<const mmEventHandlerObject&>(rhs);
                // if this line gives a compiler error, you are using a function object.
                // you need to implement bool MyFnObj::operator == (const MyFnObj&) const;
                return (this->hFunction == eho.hFunction) && (this->pObject == eho.pObject);
            }
        }
    };

    // Static function event handler.
    // It is useful for c like API pass by `nothing`.
    class MM_EXPORT_DISH mmEventHandlerStatic : public mmEventHandlerBase
    {
    public:
        typedef bool(*FunctionStaticType)(const mmEventArgs& args);

        FunctionStaticType hFunction;
    public:
        mmEventHandlerStatic(FunctionStaticType func)
            : hFunction(func)
        {

        }
        
        virtual bool operator()(const mmEventArgs& args)
        {
            return (*(this->hFunction))(args);
        }
        
        virtual bool operator==(const mmEventHandlerBase& rhs) const
        {
            // there is no static information if Functor holds a member function 
            // or a free function; this is the main difference to tr1::function
            if (typeid(*this) != typeid(rhs))
            {
                // cannot be equal
                return false;
            }
            else
            {
                const mmEventHandlerStatic& ehs = static_cast<const mmEventHandlerStatic&>(rhs);
                // if this line gives a compiler error, you are using a function object.
                // you need to implement bool MyFnObj::operator == (const MyFnObj&) const;
                return (this->hFunction == ehs.hFunction);
            }
        }
    };

    // Member function event handler.
    // It is useful for c++ like API pass by `this` object.
    //
    // Although this method is more flexible.
    // But recommended the mmEventHandlerObject method whenever possible.
    // Template instantiation will make a bigger code size.
    template<typename T>
    class mmEventHandlerMember : public mmEventHandlerBase
    {
    public:
        typedef bool(T::*FunctionMemberType)(const mmEventArgs& args);

        FunctionMemberType hFunction;
        T* pObject;
    public:
        mmEventHandlerMember(FunctionMemberType func, T* obj)
            : hFunction(func)
            , pObject(obj)
        {

        }
        
        virtual bool operator()(const mmEventArgs& args)
        {
            return ((this->pObject)->*(this->hFunction))(args);
        }
        
        virtual bool operator==(const mmEventHandlerBase& rhs) const
        {
            if (typeid(*this) != typeid(rhs))
            {
                // cannot be equal
                return false;
            }
            else
            {
                const mmEventHandlerMember& ehm = static_cast<const mmEventHandlerMember&>(rhs);
                // if this line gives a compiler error, you are using a function object.
                // you need to implement bool MyFnObj::operator == (const MyFnObj&) const;
                return (this->pObject == ehm.pObject) && (this->hFunction == ehm.hFunction);
            }
        }
    };

    // Function event handler.
    class MM_EXPORT_DISH mmEventHandler
    {
    public:
        typedef std::shared_ptr<mmEventHandlerBase> HandlerType;

        HandlerType hHandler;
    public:
        mmEventHandler(void)
            : hHandler(NULL)
        {

        }
        
        // Note: Pure function type here is better.
        mmEventHandler(bool(*func)(void*, const mmEventArgs&), void* obj)
            : hHandler(new mmEventHandlerObject(func, obj))
        {

        }
        
        // Note: Pure function type here is better.
        mmEventHandler(bool(*func)(const mmEventArgs&))
            : hHandler(new mmEventHandlerStatic(func))
        {

        }
        
        // Note:
        //     We can only use the pure function type.
        //     If we use typename mmEventHandlerMember<T>::FunctionMemberType for type.
        //     We will get a Compile Error: Member pointer refers into non-class type 'void'.
        //     It is interesting for compiler template instantiation.
        template<typename T>
        mmEventHandler(bool(T::*func)(const mmEventArgs&), T* obj)
            : hHandler(new mmEventHandlerMember<T>(func, obj))
        {

        }
        
        virtual ~mmEventHandler(void)
        {

        }
        
        bool operator()(const mmEventArgs& args)
        {
            return (*(this->hHandler))(args);
        }
        
        bool operator==(const mmEventHandler& rhs) const;
        
        // Note: Pure function type here is better.
        void SetFunction(bool(*func)(void*, const mmEventArgs&), void* obj)
        {
            this->hHandler = HandlerType(new mmEventHandlerObject(func, obj));
        }
        
        // Note: Pure function type here is better.
        void SetFunction(bool(*func)(const mmEventArgs&))
        {
            this->hHandler = HandlerType(new mmEventHandlerStatic(func));
        }
        
        // Note:
        //     We can only use the pure function type.
        //     If we use typename mmEventHandlerMember<T>::FunctionMemberType for type.
        //     We will get a Compile Error: Member pointer refers into non-class type 'void'.
        //     It is interesting for compiler template instantiation.
        template<typename T>
        void SetFunction(bool(T::*func)(const mmEventArgs&), T* obj)
        {
            this->hHandler = HandlerType(new mmEventHandlerMember<T>(func, obj));
        }
    };

    // Event for subscribe handler.
    class MM_EXPORT_DISH mmEvent
    {
    public:
        typedef mmUInt32_t GroupType;
        
        typedef std::multimap<GroupType, mmEventHandler> HandlerMapType;
        HandlerMapType hHandlerMap;
        std::string hName;
    public:
        mmEvent(void)
        {

        }
        
        virtual ~mmEvent(void)
        {

        }
        
        const std::string& GetName(void) const
        {
            return this->hName;
        }
        
        void SetName(const std::string& v)
        {
            this->hName = v;
        }
        
        // Note: the mmEventArgs not const.
        void operator()(mmEventArgs& args);
        
        mmEventHandler Subscribe(const mmEventHandler& handler);
        mmEventHandler Subscribe(GroupType group, const mmEventHandler& handler);
        void Unsubscribe(const mmEventHandler& handler);
    };

    // Event set.
    class MM_EXPORT_DISH mmEventSet
    {
    public:
        typedef std::shared_ptr<mmEvent> EventSharedPtr;
        typedef std::map<std::string, EventSharedPtr> EventMapType;
        EventMapType hEventMap;
        bool hIsMuted;
    public:
        mmEventSet(void)
            : hEventMap()
            , hIsMuted(false)
        {

        }
        
        virtual ~mmEventSet(void)
        {
            this->ClearEvent();
        }
        
        void SetIsMuted(bool muted)
        {
            this->hIsMuted = muted;
        }
        
        bool GetIsMuted(void) const
        {
            return this->hIsMuted;
        }
        
        EventSharedPtr AddEvent(const std::string& name);
        EventSharedPtr GetEvent(const std::string& name) const;
        EventSharedPtr GetEventInstance(const std::string& name);
        void RmvEvent(const std::string& name);
        void ClearEvent(void);
        
        mmEventHandler SubscribeEvent(const std::string& name, mmEvent::GroupType group, mmEventHandler handler)
        {
            EventSharedPtr e = this->GetEventInstance(name);
            return e->Subscribe(group, handler);
        }
        
        mmEventHandler SubscribeEvent(const std::string& name, mmEventHandler handler)
        {
            EventSharedPtr e = this->GetEventInstance(name);
            return e->Subscribe(handler);
        }
        
        // Note: Pure function type here is better.
        mmEventHandler SubscribeEvent(const std::string& name, mmEvent::GroupType group, bool(*func)(void*, const mmEventArgs&), void* obj)
        {
            return this->SubscribeEvent(name, group, mmEventHandler(func, obj));
        }

        // Note: Pure function type here is better.
        mmEventHandler SubscribeEvent(const std::string& name, bool(*func)(void*, const mmEventArgs&), void* obj)
        {
            return this->SubscribeEvent(name, mmEventHandler(func, obj));
        }
        
        // Note: Pure function type here is better.
        mmEventHandler SubscribeEvent(const std::string& name, mmEvent::GroupType group, bool(*func)(const mmEventArgs&))
        {
            return this->SubscribeEvent(name, group, mmEventHandler(func));
        }
        
        // Note: Pure function type here is better.
        mmEventHandler SubscribeEvent(const std::string& name, bool(*func)(const mmEventArgs&))
        {
            return this->SubscribeEvent(name, mmEventHandler(func));
        }
        
        // Note:
        //     We can only use the pure function type.
        //     If we use typename mmEventHandlerMember<T>::FunctionMemberType for type.
        //     We will get a Compile Error: Member pointer refers into non-class type 'void'.
        //     It is interesting for compiler template instantiation.
        template<typename T>
        mmEventHandler SubscribeEvent(const std::string& name, mmEvent::GroupType group, bool(T::*func)(const mmEventArgs&), T* obj)
        {
            return this->SubscribeEvent(name, group, mmEventHandler(func, obj));
        }
        
        // Note:
        //     We can only use the pure function type.
        //     If we use typename mmEventHandlerMember<T>::FunctionMemberType for type.
        //     We will get a Compile Error: Member pointer refers into non-class type 'void'.
        //     It is interesting for compiler template instantiation.
        template<typename T>
        mmEventHandler SubscribeEvent(const std::string& name, bool(T::*func)(const mmEventArgs&), T* obj)
        {
            return this->SubscribeEvent(name, mmEventHandler(func, obj));
        }
        
        void UnsubscribeEvent(const std::string& name, const mmEventHandler& handler);
        
        // Note: Pure function type here is better.
        void UnsubscribeEvent(const std::string& name, bool(*func)(void*, const mmEventArgs&), void* obj)
        {
            this->UnsubscribeEvent(name, mmEventHandler(func, obj));
        }
        
        // Note: Pure function type here is better.
        void UnsubscribeEvent(const std::string& name, bool(*func)(const mmEventArgs&))
        {
            this->UnsubscribeEvent(name, mmEventHandler(func));
        }
        
        // Note:
        //     We can only use the pure function type.
        //     If we use typename mmEventHandlerMember<T>::FunctionMemberType for type.
        //     We will get a Compile Error: Member pointer refers into non-class type 'void'.
        //     It is interesting for compiler template instantiation.
        template<typename T>
        void UnsubscribeEvent(const std::string& name, bool(T::*func)(const mmEventArgs&), T* obj)
        {
            this->UnsubscribeEvent(name, mmEventHandler(func, obj));
        }
        
        // Note: the mmEventArgs not const.
        void FireEvent(const std::string& name, mmEventArgs& args);
    };
}
#endif//__mmEvent_h__
