/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2021-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmChinaIdentityCard.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

/**
 *  https://zh.wikipedia.org/zh-cn/%E4%B8%AD%E5%8D%8E%E4%BA%BA%E6%B0%91%E5%85%B1%E5%92%8C%E5%9B%BD%E8%A1%8C%E6%94%BF%E5%8C%BA%E5%88%92%E4%BB%A3%E7%A0%81
 * <pre>
 * 省、直辖市代码表：
 *     11 : 北京  12 : 天津  13 : 河北       14 : 山西  15 : 内蒙古
 *     21 : 辽宁  22 : 吉林  23 : 黑龙江
 *     31 : 上海  32 : 江苏  33 : 浙江       34 : 安徽  35 : 福建      36 : 江西  37 : 山东
 *     41 : 河南  42 : 湖北  43 : 湖南       44 : 广东  45 : 广西      46 : 海南
 *     50 : 重庆  51 : 四川  52 : 贵州       53 : 云南  54 : 西藏
 *     61 : 陕西  62 : 甘肃  63 : 青海       64 : 宁夏  65 : 新疆
 *     71 : 台湾
 *     81 : 香港  82 : 澳门
 *     91 : 国外  83 : 台湾地区
 *
 *     [0]中华人民共和国声称拥有台湾地区的主权，然而并未实际管治，所以实际上代码只在全国省级行政区排序上使用，详见台湾问题。
 *     [1]在港澳台居民居住证的公民身份号码中，前6位地址码为“830000”，与分配的数字代码不同，与国务院规定的行政区划序列相同[5][6]。
 *     [2]华东地区包括上海、江苏、浙江、安徽、福建、江西、山东、台湾，共七省一市，台湾因特殊性，一般单独列出，统计资料时也常不包含在内
 *     [3]在港澳台居民居住证的公民身份号码中，前6位地址码为“830000”
 * </pre>
 */
// 必须有序
static const char* mmChinaIdentityCityCodeTable[36] =
{
    "11", "12", "13", "14", "15",
    "21", "22", "23",
    "31", "32", "33", "34", "35", "36", "37",
    "41", "42", "43", "44", "45", "46",
    "50", "51", "52", "53", "54",
    "61", "62", "63", "64", "65",
    "71",
    "81", "82", "83",
    "91",
};

static const size_t mmChinaIdentityCityCodeSize = MM_ARRAY_SIZE(mmChinaIdentityCityCodeTable);

static size_t mmChinaIdentity_CityCodeBinarySerach(size_t o, size_t l, const char* city)
{
    size_t mid = 0;
    size_t left = o;
    size_t right = o + l - 1;
    int result = 0;

    if (0 == l)
    {
        return (size_t)-1;
    }
    else
    {
        while (left <= right && (size_t)(-1) != right)
        {
            // Avoid overflow problems
            mid = left + (right - left) / 2;

            result = memcmp(mmChinaIdentityCityCodeTable[mid], city, 2);

            if (result > 0)
            {
                right = mid - 1;
            }
            else if (result < 0)
            {
                left = mid + 1;
            }
            else
            {
                return mid;
            }
        }

        return -1;
    }
}
// 校验省份代码
static int mmChinaIdentity_CheckCityCode(const char* city)
{
    size_t index = mmChinaIdentity_CityCodeBinarySerach(0, mmChinaIdentityCityCodeSize, city);
    return ((size_t)(-1) != index && index < mmChinaIdentityCityCodeSize);
}

// 校验出生日期 日期格式 YYYYMMDD 如 "19870912"
static int mmChinaIdentity_IsLeapYear(int year)
{
    return (((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)) ? 1 : 0);
}
static int mmChinaIdentity_TimeIsValid(int y, int m, int d)
{
    static const int month[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, };
    if ((1900 <= y && y < 9999) && (0 < m && m <= 12))
    {
        int v1 = month[m - 1];
        int v2 = (2 == m) ? (v1 + (mmChinaIdentity_IsLeapYear(y) ? 1 : 0)) : v1;
        return (0 < d && d <= v2);
    }
    else
    {
        return 0;
    }
}

/**
 * 身份证号码是由15位数字组成的，它们分别表示：
 *（1）前1、2位数字表示：所在省份的代码；
 *（2）第3、4位数字表示：所在城市的代码；
 *（3）第5、6位数字表示：所在区县的代码；
 *（4）第7~12位数字表示：出生年、月、日；
 *（5）第13、14位数字表示：所在地的派出所的代码；
 *（6）第15位数字表示性别：奇数表示男性，偶数表示女性；
*/
static int mmChinaIdentityCard_15Verification(const char* sPaperId)
{
    char ys[4] = { 0 };
    char ms[4] = { 0 };
    char ds[4] = { 0 };
    int y, m, d;
    int i = 0;
    // 检验长度
    if(15 != strlen(sPaperId))
    {
        return -1;
    }
    // 校验数字
    for (i = 0; i < 15; i++)
    {
        if (!isdigit(sPaperId[i]))
        {
            return -2;
        }
    }
    // 校验省份代码
    if (!mmChinaIdentity_CheckCityCode(sPaperId))
    {
        return -3;
    }
    // 校验出生日期
    memcpy(ys, &sPaperId[ 6], 2);
    memcpy(ms, &sPaperId[ 8], 2);
    memcpy(ds, &sPaperId[10], 2);
    y = atoi(ys) + 1900;
    m = atoi(ms);
    d = atoi(ds);
    if (!mmChinaIdentity_TimeIsValid(y, m, d))
    {
        return -4;
    }

    return 0;
}

/**
 * 身份证15位转18位原理：身份证中的年份补全，即：第六、七位之间增加“1”“9”（目前大多数是20世纪出身的），现在身份证号码位数是17位。
 * 第18位确定：将身份证号码17位数分别乘以不同系数，为7-9-10-5-8-4-2-1-6-3-7-9-10-5-8-4-2。
 * 将这17位数字相加除以11，得到余数。
 * 余数只可能为0-1-2-3-4-5-6-7-8-9-10这11个数字。其分别对应的数为1-0-X-9-8-7-6-5-4-3-2。而这个数就是最后一位身份证号码。
*/
/**
 * 函 数 名: mmChinaIdentityCardVerification
 *
 * 函数功能: 校验18位身份证号码,15位的号码需补齐18位
 *
 * 输入参数:  pID  身份证号
 *
 * 输出参数:
 *
 * 返回值:   0           成功
 *        其他     失败
 */

/**
 * 身份证号码是由18位数字组成的，它们分别表示：
 *（1）前1、2位数字表示：所在省份的代码；
 *（2）第3、4位数字表示：所在城市的代码；
 *（3）第5、6位数字表示：所在区县的代码；
 *（4）第7~14位数字表示：出生年、月、日；
 *（5）第15、16位数字表示：所在地的派出所的代码；
 *（6）第17位数字表示性别：奇数表示男性，偶数表示女性；
 *（7）第18位数字是校检码：校检码可以是0~9的数字，有时也用x表示。
 */

static int mmChinaIdentityCard_18Verification(const char* sPaperId)
{
    // 加权因子
    static const int WeightingFactor[17] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, };
    // 校验码
    static const char CheckCode[11] = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2', };
    
    char ys[8] = { 0 };
    char ms[4] = { 0 };
    char ds[4] = { 0 };
    int y, m, d;
    int i = 0;
    long lSumQT = 0;
    // 检验长度
    if(18 != strlen(sPaperId))
    {
        return -1;
    }
    // 校验数字
    for (i = 0; i < 18; i++)
    {
        if (!isdigit(sPaperId[i]) && !(('X' == toupper(sPaperId[i])) && 17 == i))
        {
            return -2;
        }
    }
    // 校验省份代码
    if (!mmChinaIdentity_CheckCityCode(sPaperId))
    {
        return -3;
    }
    // 校验出生日期
    memcpy(ys, &sPaperId[ 6], 4);
    memcpy(ms, &sPaperId[10], 2);
    memcpy(ds, &sPaperId[12], 2);
    y = atoi(ys);
    m = atoi(ms);
    d = atoi(ds);
    if (!mmChinaIdentity_TimeIsValid(y, m, d))
    {
        return -4;
    }
    // 验证最末的校验码
    for (i = 0;i <= 16; i++)
    {
        lSumQT += (sPaperId[i] - 48) * WeightingFactor[i];
    }
    if (CheckCode[lSumQT % 11] != toupper(sPaperId[17]))
    {
        return -5;
    }

    return 0;
}

MM_EXPORT_DISH int mmChinaIdentityCard_Verification(const char* sPaperId)
{
    if (18 == strlen(sPaperId))
    {
        return mmChinaIdentityCard_18Verification(sPaperId);
    }
    else
    {
        return mmChinaIdentityCard_15Verification(sPaperId);
    }
}
