/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmArchiveEndian_h__
#define __mmArchiveEndian_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmByteswap.h"

#include "dish/mmDishExport.h"

#include <list>
#include <vector>
#include <stack>
#include <string>

namespace mm
{
    class mmArchiveByteOrderLittle
    {
    public:
        static mmInline mmUInt16_t EndianEncode16(const mmUInt16_t& v) { return mmHtol16(v); }
        static mmInline mmUInt16_t EndianDecode16(const mmUInt16_t& v) { return mmLtoh16(v); }
        static mmInline mmUInt32_t EndianEncode32(const mmUInt32_t& v) { return mmHtol32(v); }
        static mmInline mmUInt32_t EndianDecode32(const mmUInt32_t& v) { return mmLtoh32(v); }
        static mmInline mmUInt64_t EndianEncode64(const mmUInt64_t& v) { return mmHtol64(v); }
        static mmInline mmUInt64_t EndianDecode64(const mmUInt64_t& v) { return mmLtoh64(v); }
    };
    class mmArchiveByteOrderBigger
    {
    public:
        static mmInline mmUInt16_t EndianEncode16(const mmUInt16_t& v) { return mmHtob16(v); }
        static mmInline mmUInt16_t EndianDecode16(const mmUInt16_t& v) { return mmBtoh16(v); }
        static mmInline mmUInt32_t EndianEncode32(const mmUInt32_t& v) { return mmHtob32(v); }
        static mmInline mmUInt32_t EndianDecode32(const mmUInt32_t& v) { return mmBtoh32(v); }
        static mmInline mmUInt64_t EndianEncode64(const mmUInt64_t& v) { return mmHtob64(v); }
        static mmInline mmUInt64_t EndianDecode64(const mmUInt64_t& v) { return mmBtoh64(v); }
    };

    // mmIArchive mmIArchiveEndian are different.
    // mmUInt32_t for std::string and container length,
    // do not coder none cross platform type:size_t long int.
    template<typename ArchiveByteOrder = mmArchiveByteOrderLittle>
    class mmIArchiveEndian
    {
    public:
        struct mmStreambuf& streambuf;
    public:
        mmIArchiveEndian(struct mmStreambuf& _streambuf)
            : streambuf(_streambuf)
        {

        }
    };
    typedef mmIArchiveEndian<mmArchiveByteOrderLittle> mmIArchiveLittle;
    typedef mmIArchiveEndian<mmArchiveByteOrderBigger> mmIArchiveBigger;

    // compatible if have coder interface.
    template<
        typename ArchiveByteOrder,
        typename ComplexType>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, ComplexType& val)
    {
        val.decode(archive);
        return (archive);
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmUInt8_t& val)
    {
        // mmUInt8_t is benchmark unit.
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUInt8_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmSInt8_t& val)
    {
        // mmSInt8_t is benchmark unit.
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSInt8_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmUInt16_t& val)
    {
        mmUInt16_t v = mmUInt16_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt16_t));
        val = ArchiveByteOrder::EndianDecode16(v);
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmSInt16_t& val)
    {
        mmSInt16_t v = mmSInt16_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmSInt16_t));
        val = ArchiveByteOrder::EndianDecode16(v);
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmUInt32_t& val)
    {
        mmUInt32_t v = mmUInt32_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt32_t));
        val = ArchiveByteOrder::EndianDecode32(v);
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmSInt32_t& val)
    {
        mmSInt32_t v = mmSInt32_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmSInt32_t));
        val = ArchiveByteOrder::EndianDecode32(v);
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmUInt64_t& val)
    {
        mmUInt64_t v = mmUInt64_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt64_t));
        val = ArchiveByteOrder::EndianDecode64(v);
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, mmSInt64_t& val)
    {
        mmSInt64_t v = mmSInt64_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmSInt64_t));
        val = ArchiveByteOrder::EndianDecode64(v);
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, float& val)
    {
        // float is IEEE754
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(float));
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, double& val)
    {
        // double is IEEE754
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(double));
        return archive;
    }
    template<typename ArchiveByteOrder>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, std::string& val)
    {
        mmUInt32_t v = mmUInt32_t();
        mmUInt32_t size = mmUInt32_t();
        mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt32_t));
        size = ArchiveByteOrder::EndianDecode32(v);
        if (0 != size)
        {
            val.resize(size);
            mmStreambuf_Sgetn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)val.data(), 0, size);
        }
        return archive;
    }
    //list vector decode will not trigger the value_type's copy construction ,
    //because list vector insert return the value reference.
    template<
        typename ArchiveByteOrder,
        typename ValueType,
        typename AllocType>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, std::list<ValueType, AllocType>& val)
    {
        typedef std::list<ValueType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::iterator iterator;
        mmUInt32_t size = 0;
        (archive) >> size;
        // do not use resize.std list vector resize(size_t n, ValueType())
        // if value_type contain shared pointer will case bug.
        // std list not need reserve.
        for (mmUInt32_t _index = 0; _index < size; ++_index)
        {
            // mmOArchiveEndian is begin ==> end.
            iterator it = val.insert(val.end(), ValueType());
            (archive) >> (*it);
        }
        return (archive);
    }
    template<
        typename ArchiveByteOrder,
        typename ValueType,
        typename AllocType>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, std::vector<ValueType, AllocType>& val)
    {
        typedef std::vector<ValueType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::iterator iterator;
        mmUInt32_t size = 0;
        (archive) >> size;
        // do not use resize.std list vector resize(size_t n, ValueType())
        // if value_type contain shared pointer will case bug.
        // std vector need reserve.
        val.reserve(size);
        for (mmUInt32_t _index = 0; _index < size; ++_index)
        {
            // mmOArchiveEndian is begin ==> end.
            iterator it = val.insert(val.end(), ValueType());
            (archive) >> (*it);
        }
        return (archive);
    }
    //pair
    template<
        typename ArchiveByteOrder,
        typename Type1,
        typename Type2>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, std::pair<Type1, Type2>& val)
    {
        (archive) >> val.first;
        (archive) >> val.second;
        return (archive);
    }
    //map multimap decode will not trigger the mapped_type's copy construction ,
    //because map multimap insert return the value reference.
    template<
        typename ArchiveByteOrder,
        typename KeyType,
        typename ValueType,
        typename CompareType,
        typename AllocType,
        template<class, class, class, class> class ComplexContainer>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, ComplexContainer<KeyType, ValueType, CompareType, AllocType>& val)
    {
        typedef ComplexContainer<KeyType, ValueType, CompareType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::mapped_type mapped_type;
        typedef typename ComplexContainerType::iterator iterator;
        typedef typename std::pair<iterator, bool> result_type;
        mmUInt32_t size = 0;
        (archive) >> size;
        val.clear();
        while (0 < size)
        {
            size--;
            KeyType k;
            (archive) >> k;
            result_type _result = val.insert(value_type(k, mapped_type()));
            (archive) >> (_result.first->second);
        }
        return (archive);
    }
    //set multiset decode will trigger the key_type copy construction ,
    //because set multiset insert return the value reference but not assignable.
    //Can't modify a set's members in place.
    //http://stackoverflow.com/questions/16299125/return-iterator-from-stdsetinsert-is-const
    template<
        typename ArchiveByteOrder,
        typename KeyType,
        typename CompareType,
        typename AllocType,
        template<class, class, class> class ComplexContainer>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, ComplexContainer<KeyType, CompareType, AllocType>& val)
    {
        mmUInt32_t size = 0;
        (archive) >> size;
        val.clear();
        while (0 < size)
        {
            size--;
            KeyType k;
            (archive) >> k;
            val.insert(k);
        }
        return (archive);
    }
    //stack decode will trigger a copy construction ,
    //because stack push not return the value reference, and only only value.
    template<
        typename ArchiveByteOrder,
        typename ValueType,
        typename Container>
    const mmIArchiveEndian<ArchiveByteOrder>& operator >> (const mmIArchiveEndian<ArchiveByteOrder>& archive, std::stack<ValueType, Container>& val)
    {
        mmUInt32_t size = 0;
        (archive) >> size;
        while (!val.empty())
        {
            val.pop();
        }
        while (0 < size)
        {
            size--;
            ValueType v;
            (archive) >> v;
            val.push(v);
        }
        return (archive);
    }

    // mmOArchive mmOArchiveEndian are different.
    // mmUInt32_t for std::string and container length,
    // do not coder none cross platform type:size_t long int.
    template<typename ArchiveByteOrder = mmArchiveByteOrderLittle>
    class mmOArchiveEndian
    {
    public:
        struct mmStreambuf& streambuf;
    public:
        mmOArchiveEndian(struct mmStreambuf& _streambuf)
            : streambuf(_streambuf)
        {

        }
    };
    typedef mmOArchiveEndian<mmArchiveByteOrderLittle> mmOArchiveLittle;
    typedef mmOArchiveEndian<mmArchiveByteOrderBigger> mmOArchiveBigger;

    // compatible if have coder interface.
    template<
        typename ArchiveByteOrder,
        typename ComplexType>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const ComplexType& val)
    {
        val.encode(archive);
        return (archive);
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmUInt8_t& val)
    {
        // mmUInt8_t is benchmark unit.
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmUInt8_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmSInt8_t& val)
    {
        // mmSInt8_t is benchmark unit.
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(mmSInt8_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmUInt16_t& val)
    {
        mmUInt16_t v = ArchiveByteOrder::EndianEncode16(val);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt16_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmSInt16_t& val)
    {
        mmSInt16_t v = ArchiveByteOrder::EndianEncode16(val);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmSInt16_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmUInt32_t& val)
    {
        mmUInt32_t v = ArchiveByteOrder::EndianEncode32(val);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt32_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmSInt32_t& val)
    {
        mmSInt32_t v = ArchiveByteOrder::EndianEncode32(val);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmSInt32_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmUInt64_t& val)
    {
        mmUInt64_t v = ArchiveByteOrder::EndianEncode64(val);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt64_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const mmSInt64_t& val)
    {
        mmSInt64_t v = ArchiveByteOrder::EndianEncode64(val);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmSInt64_t));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const float& val)
    {
        // float is IEEE754
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(float));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const double& val)
    {
        // double is IEEE754
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&val, 0, sizeof(double));
        return archive;
    }
    template<typename ArchiveByteOrder>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const std::string& val)
    {
        mmUInt32_t size = (mmUInt32_t)val.size();
        mmUInt32_t v = ArchiveByteOrder::EndianEncode32(size);
        mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)&v, 0, sizeof(mmUInt32_t));
        if (0 != size)
        {
            mmStreambuf_Sputn((struct mmStreambuf*)&archive.streambuf, (mmUInt8_t*)val.c_str(), 0, size);
        }
        return archive;
    }
    //list vector
    template<
        typename ArchiveByteOrder,
        typename ValueType,
        typename AllocType,
        template<class, class> class ComplexContainer>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const ComplexContainer<ValueType, AllocType>& val)
    {
        typedef ComplexContainer<ValueType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            for (const_iterator it = val.begin();
                it != val.end(); it++)
            {
                (archive) << (*it);
            }
        }
        return (archive);
    }
    //pair
    template<
        typename ArchiveByteOrder,
        typename Type1,
        typename Type2>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const std::pair<Type1, Type2>& val)
    {
        (archive) << val.first;
        (archive) << val.second;
        return (archive);
    }
    //map
    template<
        typename ArchiveByteOrder,
        typename KeyType,
        typename ValueType,
        typename CompareType,
        typename AllocType,
        template<class, class, class, class> class ComplexContainer>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const ComplexContainer<KeyType, ValueType, CompareType, AllocType>& val)
    {
        typedef ComplexContainer<KeyType, ValueType, CompareType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            for (const_iterator it = val.begin();
                it != val.end(); it++)
            {
                (archive) << it->first;
                (archive) << it->second;
            }
        }
        return (archive);
    }
    //set
    template<
        typename ArchiveByteOrder,
        typename KeyType,
        typename CompareType,
        typename AllocType,
        template<class, class, class> class ComplexContainer>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const ComplexContainer<KeyType, CompareType, AllocType>& val)
    {
        typedef ComplexContainer<KeyType, CompareType, AllocType> ComplexContainerType;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            for (const_iterator it = val.begin();
                it != val.end(); it++)
            {
                (archive) << (*it);
            }
        }
        return (archive);
    }
    //stack
    template<
        typename ArchiveByteOrder,
        typename ValueType,
        typename Container>
    mmOArchiveEndian<ArchiveByteOrder>& operator << (mmOArchiveEndian<ArchiveByteOrder>& archive, const std::stack<ValueType, Container>& val)
    {
        typedef typename std::stack<ValueType, Container> ComplexContainerType;
        typedef typename ComplexContainerType::container_type container_type;
        typedef typename ComplexContainerType::const_iterator const_iterator;
        mmUInt32_t size = (mmUInt32_t)val.size();
        (archive) << size;
        if (0 < size)
        {
            const container_type& cval = val._Get_container();
            for (const_iterator it = cval.begin();
                it != cval.end(); it++)
            {
                (archive) << (*it);
            }
        }
        return (archive);
    }
}

#endif//__mmArchiveEndian_h__
