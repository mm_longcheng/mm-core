/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmPoolType_h__
#define __mmPoolType_h__

#include "core/mmConfig.h"
#include "core/mmPoolElement.h"

#include "dish/mmDishExport.h"

namespace mm
{
    class mmPoolTypeBase
    {
    public:
        mmPoolTypeBase(void) {}
        virtual ~mmPoolTypeBase(void) {}
    public:
        virtual void* Produce(void) = 0;
        virtual void Recycle(void* v) = 0;
    };

    // note: the component_type must have default constructor. 
    template<typename T>
    class mmPoolType : public mmPoolTypeBase
    {
    public:
        typedef T data_type;
    public:
        struct mmPoolElement d_pool_element;
    public:
        mmPoolType(void);
        virtual ~mmPoolType(void);
    public:
        virtual size_t Capacity(void) const;
    public:
        virtual void SetChunkSize(size_t chunk_size);
    public:
        virtual void* Produce(void);
        virtual void Recycle(void* v);
    public:
        static void __static_PoolElementEventProduce(void* v);
        static void __static_PoolElementEventRecycle(void* v);
    };

    // implement
    template<typename T>
    mmPoolType<T>::mmPoolType(void)
    {
        struct mmPoolElementAllocator hAllocator;

        hAllocator.Produce = (void*)&__static_PoolElementEventProduce;
        hAllocator.Recycle = (void*)&__static_PoolElementEventRecycle;
        hAllocator.obj = this;

        mmPoolElement_Init(&this->d_pool_element);
        mmPoolElement_SetElementSize(&this->d_pool_element, sizeof(data_type));
        mmPoolElement_SetAllocator(&this->d_pool_element, &hAllocator);
        mmPoolElement_CalculationBucketSize(&this->d_pool_element);
    }
    template<typename T>
    mmPoolType<T>::~mmPoolType(void)
    {
        mmPoolElement_Destroy(&this->d_pool_element);
    }
    template<typename T>
    void mmPoolType<T>::SetChunkSize(size_t chunk_size)
    {
        mmPoolElement_SetChunkSize(&this->d_pool_element, chunk_size);
        mmPoolElement_CalculationBucketSize(&this->d_pool_element);
    }
    template<typename T>
    size_t mmPoolType<T>::Capacity(void) const
    {
        return mmPoolElement_Capacity((struct mmPoolElement*)&this->d_pool_element);
    }
    template<typename T>
    void* mmPoolType<T>::Produce(void)
    {
        void* v = NULL;
        mmPoolElement_Lock(&this->d_pool_element);
        v = mmPoolElement_Produce(&this->d_pool_element);
        mmPoolElement_Unlock(&this->d_pool_element);
        return v;
    }
    template<typename T>
    void mmPoolType<T>::Recycle(void* v)
    {
        mmPoolElement_Lock(&this->d_pool_element);
        mmPoolElement_Recycle(&this->d_pool_element, v);
        mmPoolElement_Unlock(&this->d_pool_element);
    }
    template<typename T>
    void mmPoolType<T>::__static_PoolElementEventProduce(void* v)
    {
        data_type* d = (data_type*)(v);
        new (d) data_type();
    }
    template<typename T>
    void mmPoolType<T>::__static_PoolElementEventRecycle(void* v)
    {
        data_type* d = (data_type*)(v);
        d->~data_type();
    }
}
#endif//__mmPoolType_h__
