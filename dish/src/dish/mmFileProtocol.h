/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileProtocol_h__
#define __mmFileProtocol_h__

#include "core/mmCore.h"
#include "core/mmByte.h"

#include "dish/mmArchive.h"
#include "dish/mmPackage.h"

#include <assert.h>

#include "dish/mmDishExport.h"

namespace mm
{
    class MM_EXPORT_DISH mmFileProtocol : public mmPackage
    {
    public:
        struct mmStreambuf d_streambuf;
    public:
        mmFileProtocol(void);
        virtual ~mmFileProtocol(void);
    public:
        // get file buffer.
        void AcquireFileByteBuffer(const char* file_name, struct mmByteBuffer* byte_buffer);
        // free file buffer.
        void ReleaseFileByteBuffer(struct mmByteBuffer* byte_buffer);
        
        // save.
        void Save(const char* file_name);

        // load.
        void Load(const char* file_name);
        
        void LoadBuffer(const char* buffer, size_t size);
    };
}

#endif//__mmFileProtocol_h__
