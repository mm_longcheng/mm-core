/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFileContext.h"

#include "core/mmAlloc.h"
#include "core/mmLogger.h"
#include "core/mmFilePath.h"
#include "core/mmStream.h"


static const char* gAssetsRootFolderName = "__folder__";
static const char* gAssetsRootSourceName = "__source__";

const struct mmIFileAssets mmFileAssetsMetadataFolder = 
{
    &mmFileAssetsFolder_AssetsType,
    &mmFileAssetsFolder_IsDataExists,
    &mmFileAssetsFolder_FileInfoName,
    &mmFileAssetsFolder_PathNameToRealName,
    &mmFileAssetsFolder_RealNameToPathName,
    &mmFileAssetsFolder_AcquireFileByteBuffer,
    &mmFileAssetsFolder_ReleaseFileByteBuffer,
    &mmFileAssetsFolder_AcquireFindFiles,
    &mmFileAssetsFolder_ReleaseFindFiles,
};

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_Init(
    struct mmFileAssetsFolder*                     p)
{
    p->metadata = &mmFileAssetsMetadataFolder;

    mmString_Init(&p->name);
    mmString_Init(&p->path);
    mmString_Init(&p->base);
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_Destroy(
    struct mmFileAssetsFolder*                     p)
{
    mmFileAssetsFolder_ReleaseEntry(p);

    mmString_Destroy(&p->base);
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->name);

    p->metadata = &mmFileAssetsMetadataFolder;
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_SetAssets(
    struct mmFileAssetsFolder*                     p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base)
{
    mmString_Assigns(&p->name, name);
    mmString_Assigns(&p->path, path);
    mmString_Assigns(&p->base, base);
    mmDirectoryNoneSuffix(&p->path, mmString_CStr(&p->path));
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_AcquireEntry(
    struct mmFileAssetsFolder*                     p)
{
    // need do nothing.
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_ReleaseEntry(
    struct mmFileAssetsFolder*                     p)
{
    // need do nothing.
}

MM_EXPORT_DISH
int
mmFileAssetsFolder_AssetsType(
    const struct mmFileAssetsFolder*               p)
{
    return MM_FILE_ASSETS_TYPE_FOLDER;
}

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH 
int 
mmFileAssetsFolder_IsDataExists(
    const struct mmFileAssetsFolder*               p,
    const char*                                    pRealName)
{
    return mmAbsolutePathIsDataExists(pRealName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_FileInfoName(
    const struct mmFileAssetsFolder*               p,
    struct mmString*                               pInfoName,
    const char*                                    pFileName)
{
    mmString_Assigns(pInfoName, "folder");
    mmString_Appends(pInfoName, " ");
    mmString_Append(pInfoName, &p->name);
    mmString_Appends(pInfoName, " ");
    mmString_Appends(pInfoName, pFileName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_PathNameToRealName(
    const struct mmFileAssetsFolder*               p,
    struct mmString*                               pRealName,
    const char*                                    pPathName)
{
    struct mmString path;
    mmString_Init(&path);

    if (!((0 == mmString_Size(&p->path)) || 0 == mmString_CompareCStr(&p->path, ".")))
    {
        mmString_Append(&path, &p->path);
        mmString_Appends(&path, "/");
    }
    if (!((0 == mmString_Size(&p->base)) || 0 == mmString_CompareCStr(&p->base, ".")))
    {
        mmString_Append(&path, &p->base);
        mmString_Appends(&path, "/");
    }
    mmString_Appends(&path, pPathName);

    mmCleanPath(&path, mmString_CStr(&path));
    mmString_Assign(pRealName, &path);
    mmString_Destroy(&path);
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_RealNameToPathName(
    const struct mmFileAssetsFolder*               p,
    struct mmString*                               pPathName,
    const char*                                    pRealName)
{
    struct mmString prefix;
    struct mmString path;
    mmString_Init(&prefix);
    mmString_Init(&path);
    mmString_Assigns(&path, pRealName);

    if (!((0 == mmString_Size(&p->path)) || 0 == mmString_CompareCStr(&p->path, ".")))
    {
        mmString_Assign(&prefix, &p->path);
        mmString_Appends(&prefix, "/");
        mmString_Substr(
            &path, 
            &path, 
            mmString_Size(&prefix), 
            mmString_Size(&path) - mmString_Size(&prefix));
    }
    if (!((0 == mmString_Size(&p->base)) || 0 == mmString_CompareCStr(&p->base, ".")))
    {
        mmString_Assign(&prefix, &p->base);
        mmString_Appends(&prefix, "/");
        mmString_Substr(
            &path, 
            &path, 
            mmString_Size(&prefix), 
            mmString_Size(&path) - mmString_Size(&prefix));
    }

    mmCleanPath(&path, mmString_CStr(&path));
    mmString_Assign(pPathName, &path);
    mmString_Destroy(&prefix);
    mmString_Destroy(&path);
}

// acquire file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsFolder_AcquireFileByteBuffer(
    const struct mmFileAssetsFolder*               p,
    const char*                                    pRealName,
    struct mmByteBuffer*                           pByteBuffer)
{
    mmAbsolutePathAcquireFileByteBuffer(pRealName, pByteBuffer);
}

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsFolder_ReleaseFileByteBuffer(
    const struct mmFileAssetsFolder*               p,
    struct mmByteBuffer*                           pByteBuffer)
{
    mmByteBuffer_Free(pByteBuffer);
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_AcquireFindFiles(
    const struct mmFileAssetsFolder*               p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree)
{
    size_t pos1 = 0;
    size_t pos2 = 0;
    struct mmFileAssetsInfo* fi = NULL;
    struct mmString hUnitName;
    struct mmString hPatternString;
    struct mmString hDirectoryString;
    struct mmString hFullPattern;
    struct mmString hFileInfoName;

    intptr_t hDataHandle, res;
    struct _finddata_t hFindData;

    mmString_Init(&hUnitName);
    mmString_Init(&hPatternString);
    mmString_Init(&hDirectoryString);
    mmString_Init(&hFullPattern);
    mmString_Init(&hFileInfoName);

    mmString_Assigns(&hPatternString, pPattern);
    mmFileAssetsFolder_PathNameToRealName(p, &hUnitName, pDirectory);

    // pattern can contain a directory name, separate it from mask
    pos1 = mmString_FindLastOf(&hPatternString, '/');
    pos2 = mmString_FindLastOf(&hPatternString, '\\');
    if (mmStringNpos == pos1 || ((mmStringNpos != pos2) && (pos1 < pos2)))
    {
        pos1 = pos2;
    }
    if (mmStringNpos != pos1)
    {
        mmString_Substr(&hPatternString, &hDirectoryString, 0, pos1 + 1);
    }
    // concatenate
    mmConcatenatePath(&hFullPattern, mmString_CStr(&hUnitName), pPattern);
    //
    hDataHandle = mmFindFirst(mmString_CStr(&hFullPattern), &hFindData);
    res = 0;
    while (hDataHandle != -1 && res != -1)
    {
        if ((hDirs == ((hFindData.attrib & _A_SUBDIR) != 0)) &&
            (0 == hIgnoreHidden || (hFindData.attrib & _A_HIDDEN) == 0) &&
            (0 == hDirs || 0 == mmIsReservedDirectory(hFindData.name)))
        {
            fi = (struct mmFileAssetsInfo*)mmMalloc(sizeof(struct mmFileAssetsInfo));
            mmFileAssetsInfo_Init(fi);
            mmString_Assign(&fi->filename, &hDirectoryString);
            mmString_Appends(&fi->filename, hFindData.name);
            mmString_Assigns(&fi->basename, hFindData.name);
            mmString_Assign(&fi->pathname, &hDirectoryString);
            fi->csize = hFindData.size;
            fi->dsize = hFindData.size;
            fi->cacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
            fi->cacher.file = (struct mmIFileAssets**)p;
            mmFileAssetsFolder_FileInfoName(p, &hFileInfoName, mmString_CStr(&fi->filename));
            mmRbtreeStringVpt_Set(pRbtree, &hFileInfoName, fi);
        }
        res = mmFindNext(hDataHandle, &hFindData);
    }
    // Close if we found any files
    if (hDataHandle != -1)
    {
        mmFindClose(hDataHandle);
    }

    // Now find directories
    if (0 != hRecursive)
    {
        size_t substr_o = 0;
        size_t substr_l = 0;
        struct mmString hBaseDir;
        struct mmString hMask;
        struct mmString hPatternSubstr;
        mmString_Init(&hBaseDir);
        mmString_Init(&hMask);
        mmString_Init(&hPatternSubstr);

        mmString_Assigns(
            &hBaseDir,
            0 == mmString_Size(&hUnitName) ? "." : mmString_CStr(&hUnitName));

        if (!mmString_Empty(&hDirectoryString))
        {
            size_t vl = 0;
            char* vs = NULL;

            mmConcatenatePath(
                &hBaseDir,
                mmString_CStr(&hBaseDir),
                mmString_CStr(&hDirectoryString));

            // Remove the last '/'
            vl = mmString_Size(&hBaseDir);
            vs = (char*)mmString_Data(&hBaseDir);
            if ('/' == vs[vl - 1] || '\\' == vs[vl - 1])
            {
                vs[vl - 1] = 0;
                mmString_SetSizeValue(&hBaseDir, vl - 1);
            }
        }
        mmString_Appends(&hBaseDir, "/*");
        //
        // Remove directory name from pattern
        mmString_Assigns(&hMask, "/");
        if (mmStringNpos != pos1)
        {
            substr_o = pos1 + 1;
            substr_l = mmString_Size(&hPatternString) - substr_o;
            mmString_Substr(&hPatternString, &hPatternSubstr, substr_o, substr_l);
            mmString_Append(&hMask, &hPatternSubstr);
        }
        else
        {
            mmString_Appends(&hMask, pPattern);
        }

        hDataHandle = mmFindFirst(mmString_CStr(&hBaseDir), &hFindData);
        res = 0;
        while (hDataHandle != -1 && res != -1)
        {
            if ((hFindData.attrib & _A_SUBDIR) &&
                (0 == hIgnoreHidden || (hFindData.attrib & _A_HIDDEN) == 0) &&
                (0 == mmIsReservedDirectory(hFindData.name)))
            {
                // recurse
                mmString_Assign(&hBaseDir, &hDirectoryString);
                mmString_Appends(&hBaseDir, hFindData.name);
                mmString_Append(&hBaseDir, &hMask);

                mmFileAssetsFolder_AcquireFindFiles(
                    p, 
                    pDirectory, 
                    mmString_CStr(&hBaseDir),
                    hRecursive, 
                    hDirs,
                    hIgnoreHidden,
                    pRbtree);
            }
            res = mmFindNext(hDataHandle, &hFindData);
        }
        // Close if we found any files
        if (hDataHandle != -1)
        {
            mmFindClose(hDataHandle);
        }
        mmString_Destroy(&hPatternSubstr);
        mmString_Destroy(&hMask);
        mmString_Destroy(&hBaseDir);
    }
    mmString_Destroy(&hFileInfoName);
    mmString_Destroy(&hFullPattern);
    mmString_Destroy(&hDirectoryString);
    mmString_Destroy(&hPatternString);
    mmString_Destroy(&hUnitName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsFolder_ReleaseFindFiles(
    const struct mmFileAssetsFolder*               p,
    struct mmRbtreeStringVpt*                      rbtree)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFileAssetsInfo* e = NULL;
    n = mmRb_First(&rbtree->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsInfo*)(it->v);
        mmRbtreeStringVpt_Erase(rbtree, it);
        mmFileAssetsInfo_Destroy(e);
        mmFree(e);
    }
}

const struct mmIFileAssets mmFileAssetsMetadataSource =
{
    &mmFileAssetsSource_AssetsType,
    &mmFileAssetsSource_IsDataExists,
    &mmFileAssetsSource_FileInfoName,
    &mmFileAssetsSource_PathNameToRealName,
    &mmFileAssetsSource_RealNameToPathName,
    &mmFileAssetsSource_AcquireFileByteBuffer,
    &mmFileAssetsSource_ReleaseFileByteBuffer,
    &mmFileAssetsSource_AcquireFindFiles,
    &mmFileAssetsSource_ReleaseFindFiles,
};

MM_EXPORT_DISH 
void 
mmFileAssetsSource_Init(
    struct mmFileAssetsSource*                     p)
{
    p->metadata = &mmFileAssetsMetadataSource;

    mmString_Init(&p->name);
    mmString_Init(&p->path);
    mmString_Init(&p->base);
    mmFileZip_Init(&p->zip);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_Destroy(
    struct mmFileAssetsSource*                     p)
{
    mmFileAssetsSource_ReleaseEntry(p);

    mmFileZip_Destroy(&p->zip);
    mmString_Destroy(&p->base);
    mmString_Destroy(&p->path);
    mmString_Destroy(&p->name);

    p->metadata = &mmFileAssetsMetadataSource;
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_SetAssets(
    struct mmFileAssetsSource*                     p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base)
{
    mmString_Assigns(&p->name, name);
    mmString_Assigns(&p->path, path);
    mmString_Assigns(&p->base, base);
    mmDirectoryNoneSuffix(&p->path, mmString_CStr(&p->path));

    mmFileZip_SetFilename(&p->zip, path);
    mmFileZip_SetFilter(&p->zip, base);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_AcquireEntry(
    struct mmFileAssetsSource*                     p)
{
    mmFileZip_Fopen(&p->zip);
    mmFileZip_AcquireAnalysis(&p->zip);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_ReleaseEntry(
    struct mmFileAssetsSource*                     p)
{
    mmFileZip_ReleaseAnalysis(&p->zip);
    mmFileZip_Close(&p->zip);
}

MM_EXPORT_DISH
int
mmFileAssetsSource_AssetsType(
    const struct mmFileAssetsSource*               p)
{
    return MM_FILE_ASSETS_TYPE_SOURCE;
}

// check file name exists.1 exists 0 not exists.
// file_name is assets logic absolute.
MM_EXPORT_DISH 
int 
mmFileAssetsSource_IsDataExists(
    const struct mmFileAssetsSource*               p,
    const char*                                    pRealName)
{
    return mmFileZip_Exists(&p->zip, pRealName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_FileInfoName(
    const struct mmFileAssetsSource*               p,
    struct mmString*                               pInfoName,
    const char*                                    pFileName)
{
    mmString_Assigns(pInfoName, "source");
    mmString_Appends(pInfoName, " ");
    mmString_Append(pInfoName, &p->name);
    mmString_Appends(pInfoName, " ");
    mmString_Appends(pInfoName, pFileName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_PathNameToRealName(
    const struct mmFileAssetsSource*               p,
    struct mmString*                               pRealName,
    const char*                                    pPathName)
{
    struct mmString path;
    mmString_Init(&path);

    if (!((0 == mmString_Size(&p->base)) || 0 == mmString_CompareCStr(&p->base, ".")))
    {
        mmString_Append(&path, &p->base);
        mmString_Appends(&path, "/");
    }
    mmString_Appends(&path, pPathName);

    mmCleanPath(&path, mmString_CStr(&path));
    mmString_Assign(pRealName, &path);
    mmString_Destroy(&path);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_RealNameToPathName(
    const struct mmFileAssetsSource*               p,
    struct mmString*                               pPathName,
    const char*                                    pRealName)
{
    struct mmString path;
    struct mmString prefix;
    mmString_Init(&path);
    mmString_Init(&prefix);
    mmString_Assigns(&path, pRealName);
    if (!((0 == mmString_Size(&p->base)) || 0 == mmString_CompareCStr(&p->base, ".")))
    {
        mmString_Assign(&prefix, &p->base);
        mmString_Appends(&prefix, "/");
        mmString_Substr(
            &path, 
            &path, 
            mmString_Size(&prefix), 
            mmString_Size(&path) - mmString_Size(&prefix));
    }
    mmCleanPath(&path, mmString_CStr(&path));
    mmString_Assign(pPathName, &path);
    mmString_Destroy(&path);
    mmString_Destroy(&prefix);
}

// acquire file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsSource_AcquireFileByteBuffer(
    const struct mmFileAssetsSource*               p,
    const char*                                    pRealName,
    struct mmByteBuffer*                           pByteBuffer)
{
    if (NULL != pRealName)
    {
        mmFileZip_AcquireFileByteBuffer(&p->zip, pRealName, pByteBuffer);
    }
    else
    {
        mmByteBuffer_Reset(pByteBuffer);
    }
}

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileAssetsSource_ReleaseFileByteBuffer(
    const struct mmFileAssetsSource*               p,
    struct mmByteBuffer*                           pByteBuffer)
{
    mmByteBuffer_Free(pByteBuffer);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_AcquireFindFiles(
    const struct mmFileAssetsSource*               p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree)
{
    size_t pos1 = 0;
    size_t pos2 = 0;
    struct mmFileAssetsInfo* fi = NULL;

    struct mmString hPath;
    struct mmString hPathSubstr;
    struct mmString hBaseName;
    struct mmString hPathName;

    struct mmString hUnitName;
    struct mmString hPatternString;
    struct mmString hDirectoryString;
    struct mmString hFullPattern;
    struct mmString hFileInfoName;

    struct mmZipDirectory* pZipDirectory = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmZipEntry* e = NULL;
    int hIsLocalDirectory = 0;

    mmString_Init(&hPath);
    mmString_Init(&hPathSubstr);
    mmString_Init(&hBaseName);
    mmString_Init(&hPathName);

    mmString_Init(&hUnitName);
    mmString_Init(&hPatternString);
    mmString_Init(&hDirectoryString);
    mmString_Init(&hFullPattern);
    mmString_Init(&hFileInfoName);

    mmString_Assigns(&hPatternString, pPattern);
    mmFileAssetsSource_PathNameToRealName(p, &hUnitName, pDirectory);

    // pattern can contain a directory name, separate it from mask
    pos1 = mmString_FindLastOf(&hPatternString, '/');
    pos2 = mmString_FindLastOf(&hPatternString, '\\');
    if (mmStringNpos == pos1 || ((mmStringNpos != pos2) && (pos1 < pos2)))
    {
        pos1 = pos2;
    }
    if (mmStringNpos != pos1)
    {
        mmString_Substr(&hPatternString, &hDirectoryString, 0, pos1 + 1);
    }
    // concatenate
    mmConcatenatePath(&hFullPattern, mmString_CStr(&hUnitName), pPattern);

    do
    {
        mmPathSplitFileName(&hFullPattern, &hBaseName, &hPathName);

        mmDirectoryNoneSuffix(&hPathName, mmString_CStr(&hPathName));
        mmString_Appends(&hPathName, "/");

        pZipDirectory = mmFileZip_Get(&p->zip, &hPathName);
        if (NULL == pZipDirectory)
        {
            // zip directory is not exist.
            break;
        }

        if (0 == mmString_CompareCStr(&hUnitName, "."))
        {
            hIsLocalDirectory = 1;
            // remove the "./"
            // hFullPattern = hUnitName + "/" + pPattern
            // if hUnitName[0] = "." the lenght(hFullPattern) must >= 2. 
            // substr(hFullPattern, 2, lenght - 2) is safe.
            mmString_Substr(&hFullPattern, &hFullPattern, 2, mmString_Size(&hFullPattern) - 2);
        }

        n = mmRb_First(&pZipDirectory->entrys.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            e = (struct mmZipEntry*)(it->v);
            mmString_Assign(&hPath, &e->fullname);
            mmString_Substr(&hPath, &hPathSubstr, 0, mmString_Size(&hUnitName));
            if ((!mmString_Empty(&e->fullname)) &&
                (0 == mmString_Compare(&hPathSubstr, &hUnitName) || 1 == hIsLocalDirectory) &&
                (mmPathMatch(mmString_CStr(&e->fullname), mmString_CStr(&hFullPattern))) &&
                (hDirs == (0 != mmZipEntry_GetIsDirectory(e))) &&
                (0 == hDirs || 0 == mmIsReservedDirectory(mmString_CStr(&e->fullname))))
            {
                fi = (struct mmFileAssetsInfo*)mmMalloc(sizeof(struct mmFileAssetsInfo));
                mmFileAssetsInfo_Init(fi);
                mmFileAssetsSource_RealNameToPathName(p, &fi->filename, mmString_CStr(&e->fullname));
                mmDirectoryRemovePrefixPath(&fi->filename, mmString_CStr(&fi->filename), pDirectory);
                mmPathSplitFileName(&fi->filename, &fi->basename, &fi->pathname);
                fi->csize = e->csize;
                fi->dsize = e->dsize;
                fi->cacher.type = MM_FILE_ASSETS_TYPE_SOURCE;
                fi->cacher.file = (struct mmIFileAssets**)p;
                mmFileAssetsSource_FileInfoName(p, &hFileInfoName, mmString_CStr(&fi->filename));
                mmRbtreeStringVpt_Set(pRbtree, &hFileInfoName, fi);
            }
        }
        // Now find directories
        if (0 != hRecursive)
        {
            size_t substr_o = 0;
            size_t substr_l = 0;
            struct mmString hBaseDir;
            struct mmString hMask;
            struct mmString hPatternSubstr;
            mmString_Init(&hBaseDir);
            mmString_Init(&hMask);
            mmString_Init(&hPatternSubstr);

            mmString_Assigns(
                &hBaseDir,
                0 == mmString_Size(&hUnitName) ? "." : mmString_CStr(&hUnitName));

            if (!mmString_Empty(&hDirectoryString))
            {
                size_t vl = 0;
                char* vs = NULL;

                mmConcatenatePath(
                    &hBaseDir,
                    mmString_CStr(&hBaseDir),
                    mmString_CStr(&hDirectoryString));

                // Remove the last '/'
                vl = mmString_Size(&hBaseDir);
                vs = (char*)mmString_Data(&hBaseDir);
                if ('/' == vs[vl - 1] || '\\' == vs[vl - 1])
                {
                    vs[vl - 1] = 0;
                    mmString_SetSizeValue(&hBaseDir, vl - 1);
                }
            }
            mmString_Appends(&hBaseDir, "/*");
            //
            // Remove directory name from pattern
            mmString_Assigns(&hMask, "/");
            if (mmStringNpos != pos1)
            {
                substr_o = pos1 + 1;
                substr_l = mmString_Size(&hPatternString) - substr_o;
                mmString_Substr(&hPatternString, &hPatternSubstr, substr_o, substr_l);
                mmString_Append(&hMask, &hPatternSubstr);
            }
            else
            {
                mmString_Appends(&hMask, pPattern);
            }

            n = mmRb_First(&pZipDirectory->entrys.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmZipEntry*)(it->v);
                mmString_Assigns(&hPath, mmString_CStr(&e->fullname));
                mmString_Substr(&hPath, &hPathSubstr, 0, mmString_Size(&hUnitName));
                if ((0 == mmString_Compare(&hPathSubstr, &hUnitName) || 1 == hIsLocalDirectory) &&
                    (0 != mmZipEntry_GetIsDirectory(e)) &&
                    (0 == mmIsReservedDirectory(mmString_CStr(&e->fullname))) &&
                    (!mmString_Empty(&e->basename)))
                {
                    // recurse
                    mmString_Assign(&hBaseDir, &hDirectoryString);
                    mmString_Append(&hBaseDir, &e->basename);
                    mmString_Append(&hBaseDir, &hMask);

                    mmFileAssetsSource_AcquireFindFiles(
                        p, 
                        pDirectory, 
                        mmString_CStr(&hBaseDir),
                        hRecursive,
                        hDirs,
                        hIgnoreHidden,
                        pRbtree);
                }
            }

            mmString_Destroy(&hPatternSubstr);
            mmString_Destroy(&hMask);
            mmString_Destroy(&hBaseDir);
        }
    } while (0);

    mmString_Destroy(&hFileInfoName);
    mmString_Destroy(&hFullPattern);
    mmString_Destroy(&hDirectoryString);
    mmString_Destroy(&hPatternString);
    mmString_Destroy(&hUnitName);

    mmString_Destroy(&hPathName);
    mmString_Destroy(&hBaseName);
    mmString_Destroy(&hPathSubstr);
    mmString_Destroy(&hPath);
}

MM_EXPORT_DISH 
void 
mmFileAssetsSource_ReleaseFindFiles(
    const struct mmFileAssetsSource*               p,
    struct mmRbtreeStringVpt*                      rbtree)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFileAssetsInfo* e = NULL;
    n = mmRb_First(&rbtree->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsInfo*)(it->v);
        mmRbtreeStringVpt_Erase(rbtree, it);
        mmFileAssetsInfo_Destroy(e);
        mmFree(e);
    }
}

static 
const struct mmFileAssetsFolder 
gFileAssetsFolderAbsolutePath =
{
    &mmFileAssetsMetadataFolder,
    mmString_Make(""),
    mmString_Make(""),
    mmString_Make(""),
};

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_Init(
    struct mmFileAssetsCacher*                     p)
{
    p->type = MM_FILE_ASSETS_TYPE_FOLDER;
    p->file = NULL;
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_Destroy(
    struct mmFileAssetsCacher*                     p)
{
    p->file = NULL;
    p->type = MM_FILE_ASSETS_TYPE_FOLDER;
}

// copy q to p.
MM_EXPORT_DISH 
void 
mmFileAssetsCacher_Copy(
    struct mmFileAssetsCacher*                     p, 
    const struct mmFileAssetsCacher*               q)
{
    p->type = q->type;
    p->file = q->file;
}

MM_EXPORT_DISH 
int
mmFileAssetsCacher_AssetsType(
    const struct mmFileAssetsCacher*               p)
{
    typedef int
    (*AssetsType)(
        const void*                                p);

    return (*((AssetsType)(*p->file)->AssetsType))(p->file);
}

MM_EXPORT_DISH 
int 
mmFileAssetsCacher_IsDataExists(
    const struct mmFileAssetsCacher*               p,
    const char*                                    pRealName)
{
    typedef int
    (*IsDataExists)(
        const void*                                p,
        const char*                                pRealName);

    return (*((IsDataExists)(*p->file)->IsDataExists))(p->file, pRealName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_FileInfoName(
    const struct mmFileAssetsCacher*               p,
    struct mmString*                               pInfoName,
    const char*                                    pFileName)
{
    typedef void
    (*FileInfoName)(
        const void*                                p,
        struct mmString*                           pInfoName,
        const char*                                pFileName);

    (*((FileInfoName)(*p->file)->FileInfoName))(p->file, pInfoName, pFileName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_PathNameToRealName(
    const struct mmFileAssetsCacher*               p,
    struct mmString*                               pRealName,
    const char*                                    pPathName)
{
    typedef void
    (*PathNameToRealName)(
        const void*                                p,
        struct mmString*                           pRealName,
        const char*                                pPathName);

    (*((PathNameToRealName)(*p->file)->PathNameToRealName))(p->file, pRealName, pPathName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_RealNameToPathName(
    const struct mmFileAssetsCacher*               p,
    struct mmString*                               pPathName,
    const char*                                    pRealName)
{
    typedef void
    (*RealNameToPathName)(
        const void*                                p,
        struct mmString*                           pPathName,
        const char*                                pRealName);

    (*((RealNameToPathName)(*p->file)->RealNameToPathName))(p->file, pPathName, pRealName);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_AcquireFileByteBuffer(
    const struct mmFileAssetsCacher*               p,
    const char*                                    pRealName,
    struct mmByteBuffer*                           pByteBuffer)
{
    typedef void
    (*AcquireFileByteBuffer)(
        const void*                                p,
        const char*                                pRealName,
        struct mmByteBuffer*                       pByteBuffer);

    (*((AcquireFileByteBuffer)(*p->file)->AcquireFileByteBuffer))(p->file, pRealName, pByteBuffer);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_ReleaseFileByteBuffer(
    const struct mmFileAssetsCacher*               p,
    struct mmByteBuffer*                           pByteBuffer)
{
    typedef void
    (*ReleaseFileByteBuffer)(
        const void*                                p,
        struct mmByteBuffer*                       pByteBuffer);

    (*((ReleaseFileByteBuffer)(*p->file)->AcquireFileByteBuffer))(p->file, pByteBuffer);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_AcquireFindFiles(
    const struct mmFileAssetsCacher*               p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree)
{
    typedef void
    (*AcquireFindFiles)(
        const void*                                p,
        const char*                                pDirectory,
        const char*                                pPattern,
        int                                        hRecursive,
        int                                        hDirs,
        int                                        hIgnoreHidden,
        struct mmRbtreeStringVpt*                  pRbtree);

    (*((AcquireFindFiles)(*p->file)->AcquireFindFiles))(
        p->file, 
        pDirectory,
        pPattern,
        hRecursive,
        hDirs,
        hIgnoreHidden,
        pRbtree);
}

MM_EXPORT_DISH 
void 
mmFileAssetsCacher_ReleaseFindFiles(
    const struct mmFileAssetsCacher*               p,
    struct mmRbtreeStringVpt*                      pRbtree)
{
    typedef void
    (*ReleaseFindFiles)(
        const void*                                p,
        struct mmRbtreeStringVpt*                  pRbtree);

    (*((ReleaseFindFiles)(*p->file)->AcquireFindFiles))(p->file, pRbtree);
}

MM_EXPORT_DISH 
void 
mmFileAssetsInfo_Init(
    struct mmFileAssetsInfo*                       p)
{
    mmFileAssetsCacher_Init(&p->cacher);
    mmString_Init(&p->filename);
    mmString_Init(&p->basename);
    mmString_Init(&p->pathname);
    p->csize = 0;
    p->dsize = 0;
}

MM_EXPORT_DISH 
void 
mmFileAssetsInfo_Destroy(
    struct mmFileAssetsInfo*                       p)
{
    p->dsize = 0;
    p->csize = 0;
    mmString_Destroy(&p->pathname);
    mmString_Destroy(&p->basename);
    mmString_Destroy(&p->filename);
    mmFileAssetsCacher_Destroy(&p->cacher);
}

MM_EXPORT_DISH 
void 
mmFileContext_Init(
    struct mmFileContext*                          p)
{
    struct mmRbtreeStringVptAllocator hStringVptAllocator;

    mmString_Init(&p->folder_path);
    mmString_Init(&p->folder_base);
    mmString_Init(&p->source_path);
    mmString_Init(&p->source_base);
    mmRbtreeStringVpt_Init(&p->folders);
    mmRbtreeStringVpt_Init(&p->sources);
    mmRbtreeStringVpt_Init(&p->cachers);

    mmString_Assigns(&p->folder_path, ".");
    mmString_Assigns(&p->source_path, "");

    hStringVptAllocator.Produce = &mmRbtreeStringVpt_WeakProduce;;
    hStringVptAllocator.Recycle = &mmRbtreeStringVpt_WeakRecycle;;
    mmRbtreeStringVpt_SetAllocator(&p->folders, &hStringVptAllocator);
    mmRbtreeStringVpt_SetAllocator(&p->sources, &hStringVptAllocator);
    mmRbtreeStringVpt_SetAllocator(&p->cachers, &hStringVptAllocator);
}

MM_EXPORT_DISH 
void 
mmFileContext_Destroy(
    struct mmFileContext*                          p)
{
    mmFileContext_ClearAssetsCacher(p);
    mmFileContext_ClearAssetsFolder(p);
    mmFileContext_ClearAssetsSource(p);

    mmRbtreeStringVpt_Destroy(&p->cachers);
    mmRbtreeStringVpt_Destroy(&p->sources);
    mmRbtreeStringVpt_Destroy(&p->folders);
    mmString_Destroy(&p->source_base);
    mmString_Destroy(&p->source_path);
    mmString_Destroy(&p->folder_base);
    mmString_Destroy(&p->folder_path);
}

MM_EXPORT_DISH 
void 
mmFileContext_SetAssetsRootFolder(
    struct mmFileContext*                          p, 
    const char*                                    pRootFolder,
    const char*                                    pRootBase)
{
    mmString_Assigns(&p->folder_path, pRootFolder);
    mmDirectoryNoneSuffix(&p->folder_path, mmString_CStr(&p->folder_path));

    mmString_Assigns(&p->folder_base, pRootBase);
    mmDirectoryNoneSuffix(&p->folder_base, mmString_CStr(&p->folder_base));

    mmFileContext_RmvAssetsFolder(p, gAssetsRootFolderName);
    mmFileContext_AddAssetsFolder(p, gAssetsRootFolderName, pRootFolder, pRootBase);
}

MM_EXPORT_DISH 
void 
mmFileContext_SetAssetsRootSource(
    struct mmFileContext*                          p, 
    const char*                                    pRootSource, 
    const char*                                    pRootBase)
{
    mmString_Assigns(&p->source_path, pRootSource);
    mmDirectoryNoneSuffix(&p->source_path, mmString_CStr(&p->source_path));

    mmString_Assigns(&p->source_base, pRootBase);
    mmDirectoryNoneSuffix(&p->source_base, mmString_CStr(&p->source_base));

    mmFileContext_RmvAssetsSource(p, gAssetsRootSourceName);
    mmFileContext_AddAssetsSource(p, gAssetsRootSourceName, pRootSource, pRootBase);
}

MM_EXPORT_DISH 
struct mmFileAssetsFolder* 
mmFileContext_AddAssetsFolder(
    struct mmFileContext*                          p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base)
{
    struct mmFileAssetsFolder* assets = mmFileContext_GetAssetsFolder(p, name);
    if (NULL == assets)
    {
        struct mmString k;
        assets = (struct mmFileAssetsFolder*)mmMalloc(sizeof(struct mmFileAssetsFolder));
        mmString_MakeWeaks(&k, name);
        mmFileAssetsFolder_Init(assets);
        mmFileAssetsFolder_SetAssets(assets, name, path, base);
        mmFileAssetsFolder_AcquireEntry(assets);
        mmRbtreeStringVpt_Set(&p->folders, &k, assets);
    }
    return assets;
}

MM_EXPORT_DISH 
struct mmFileAssetsFolder* 
mmFileContext_GetAssetsFolder(
    struct mmFileContext*                          p, 
    const char*                                    name)
{
    struct mmString k;
    mmString_MakeWeaks(&k, name);
    return (struct mmFileAssetsFolder*)mmRbtreeStringVpt_Get(&p->folders, &k);
}

MM_EXPORT_DISH 
void 
mmFileContext_RmvAssetsFolder(
    struct mmFileContext*                          p, 
    const char*                                    name)
{
    struct mmFileAssetsFolder* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString k;
    mmString_MakeWeaks(&k, name);
    it = mmRbtreeStringVpt_GetIterator(&p->folders, &k);
    if (NULL != it)
    {
        e = (struct mmFileAssetsFolder*)(it->v);
        mmRbtreeStringVpt_Erase(&p->folders, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsFolder_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsFolder(
    struct mmFileContext*                          p)
{
    struct mmFileAssetsFolder* e = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->folders.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsFolder*)(it->v);
        mmRbtreeStringVpt_Erase(&p->folders, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsFolder_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
struct mmFileAssetsSource* 
mmFileContext_AddAssetsSource(
    struct mmFileContext*                          p, 
    const char*                                    name, 
    const char*                                    path, 
    const char*                                    base)
{
    struct mmFileAssetsSource* assets = mmFileContext_GetAssetsSource(p, name);
    if (NULL == assets)
    {
        struct mmString k;
        assets = (struct mmFileAssetsSource*)mmMalloc(sizeof(struct mmFileAssetsSource));
        mmString_MakeWeaks(&k, name);
        mmFileAssetsSource_Init(assets);
        mmFileAssetsSource_SetAssets(assets, name, path, base);
        mmFileAssetsSource_AcquireEntry(assets);
        mmRbtreeStringVpt_Set(&p->sources, &k, assets);
    }
    return assets;
}

MM_EXPORT_DISH 
struct mmFileAssetsSource* 
mmFileContext_GetAssetsSource(
    struct mmFileContext*                          p, 
    const char*                                    name)
{
    struct mmString k;
    mmString_MakeWeaks(&k, name);
    return (struct mmFileAssetsSource*)mmRbtreeStringVpt_Get(&p->sources, &k);
}

MM_EXPORT_DISH 
void 
mmFileContext_RmvAssetsSource(
    struct mmFileContext*                          p, 
    const char*                                    name)
{
    struct mmFileAssetsSource* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString k;
    mmString_MakeWeaks(&k, name);
    it = mmRbtreeStringVpt_GetIterator(&p->sources, &k);
    if (NULL != it)
    {
        e = (struct mmFileAssetsSource*)(it->v);
        mmRbtreeStringVpt_Erase(&p->sources, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsSource_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsSource(
    struct mmFileContext*                          p)
{
    struct mmFileAssetsSource* e = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->sources.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsSource*)(it->v);
        mmRbtreeStringVpt_Erase(&p->sources, it);

        mmFileContext_ClearAssetsCacherByPointer(p, e);

        mmFileAssetsSource_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
struct mmFileAssetsCacher* 
mmFileContext_AddAssetsCacher(
    struct mmFileContext*                          p, 
    const char*                                    pRealName)
{
    struct mmFileAssetsCacher* assets = mmFileContext_GetAssetsCacher(p, pRealName);
    if (NULL == assets)
    {
        struct mmString k;
        assets = (struct mmFileAssetsCacher*)mmMalloc(sizeof(struct mmFileAssetsCacher));
        mmString_MakeWeaks(&k, pRealName);
        mmFileAssetsCacher_Init(assets);
        mmRbtreeStringVpt_Set(&p->cachers, &k, assets);
    }
    return assets;
}

MM_EXPORT_DISH 
struct mmFileAssetsCacher* 
mmFileContext_GetAssetsCacher(
    struct mmFileContext*                          p, 
    const char*                                    pRealName)
{
    struct mmString k;
    mmString_MakeWeaks(&k, pRealName);
    return (struct mmFileAssetsCacher*)mmRbtreeStringVpt_Get(&p->cachers, &k);
}

MM_EXPORT_DISH 
void 
mmFileContext_RmvAssetsCacher(
    struct mmFileContext*                          p, 
    const char*                                    pRealName)
{
    struct mmFileAssetsCacher* e = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmString k;
    mmString_MakeWeaks(&k, pRealName);
    it = mmRbtreeStringVpt_GetIterator(&p->cachers, &k);
    if (NULL != it)
    {
        e = (struct mmFileAssetsCacher*)(it->v);
        mmRbtreeStringVpt_Erase(&p->cachers, it);
        mmFileAssetsCacher_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsCacher(
    struct mmFileContext*                          p)
{
    struct mmFileAssetsCacher* e = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->cachers.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsCacher*)(it->v);
        mmRbtreeStringVpt_Erase(&p->cachers, it);
        mmFileAssetsCacher_Destroy(e);
        mmFree(e);
    }
}

MM_EXPORT_DISH 
void 
mmFileContext_ClearAssetsCacherByPointer(
    struct mmFileContext*                          p, 
    void*                                          file)
{
    struct mmFileAssetsCacher* e = NULL;
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    n = mmRb_First(&p->cachers.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsCacher*)(it->v);
        if (file == e->file)
        {
            mmRbtreeStringVpt_Erase(&p->cachers, it);
            mmFileAssetsCacher_Destroy(e);
            mmFree(e);
        }
    }
}

// Get file assets type.
// pPathName is assets logic absolute.
// return value
//     == -1 not exists.
//     >=  0 exists assets type.
MM_EXPORT_DISH
int
mmFileContext_GetFileAssetsType(
    struct mmFileContext*                          p,
    const char*                                    pPathName)
{
    int type = -1;
    int code = 0;
    struct mmString hUnitName;
    struct mmString hRealName;
    struct mmFileAssetsCacher hFileCacher;
    const char* pUnitName = NULL;
    mmString_Init(&hUnitName);
    mmString_Init(&hRealName);
    mmFileAssetsCacher_Init(&hFileCacher);
    mmCleanPath(&hUnitName, pPathName);
    pUnitName = mmString_CStr(&hUnitName);
    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmRbNode* n = NULL;
        {
            struct mmFileAssetsCacher* e = mmFileContext_GetAssetsCacher(p, pUnitName);
            if (NULL != e)
            {
                hFileCacher.type = e->type;
                hFileCacher.file = e->file;
                type = e->type;
            }
            else
            {
                type = -1;
            }
        }
        if (-1 != type) { break; }
        if (0 != mmIsAbsolutePath(pUnitName))
        {
            code = mmAbsolutePathIsDataExists(pUnitName);
            if (0 != code)
            {
                hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                hFileCacher.file = (struct mmIFileAssets**)&gFileAssetsFolderAbsolutePath;
                type = MM_FILE_ASSETS_TYPE_FOLDER;
            }
            else
            {
                type = -1;
            }
        }
        if (-1 != type) { break; }
        if (0 != p->folders.size)
        {
            struct mmFileAssetsFolder* e = NULL;
            n = mmRb_First(&p->folders.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsFolder*)(it->v);
                mmFileAssetsFolder_PathNameToRealName(e, &hRealName, pUnitName);
                code = mmFileAssetsFolder_IsDataExists(e, mmString_CStr(&hRealName));
                if (0 != code)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    type = MM_FILE_ASSETS_TYPE_FOLDER;
                    break;
                }
                else
                {
                    type = -1;
                }
            }
        }
        if (-1 != type) { break; }
        if (0 != p->sources.size)
        {
            struct mmFileAssetsSource* e = NULL;
            n = mmRb_First(&p->sources.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsSource*)(it->v);
                mmFileAssetsSource_PathNameToRealName(e, &hRealName, pUnitName);
                code = mmFileAssetsSource_IsDataExists(e, mmString_CStr(&hRealName));
                if (0 != code)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_SOURCE;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    type = MM_FILE_ASSETS_TYPE_SOURCE;
                    break;
                }
                else
                {
                    type = -1;
                }
            }
        }
    } while (0);
    if (-1 != type)
    {
        struct mmFileAssetsCacher* e = mmFileContext_AddAssetsCacher(p, pUnitName);
        e->type = hFileCacher.type;
        e->file = hFileCacher.file;
    }
    else
    {
        mmFileContext_RmvAssetsCacher(p, mmString_CStr(&hRealName));
    }
    mmFileAssetsCacher_Destroy(&hFileCacher);
    mmString_Destroy(&hRealName);
    mmString_Destroy(&hUnitName);
    return type;
}

// Get file assets real path name.
// pPathName is assets logic absolute.
// return value
//     == -1 not exists.
//     >=  0 exists assets type.
MM_EXPORT_DISH
int
mmFileContext_GetFileAssetsPath(
    struct mmFileContext*                          p,
    const char*                                    pPathName,
    struct mmString*                               pRealName)
{
    int type = -1;
    int code = 0;
    struct mmString hUnitName;
    struct mmString hRealName;
    struct mmFileAssetsCacher hFileCacher;
    const char* pUnitName = NULL;
    mmString_Init(&hUnitName);
    mmString_Init(&hRealName);
    mmFileAssetsCacher_Init(&hFileCacher);
    mmCleanPath(&hUnitName, pPathName);
    pUnitName = mmString_CStr(&hUnitName);
    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmRbNode* n = NULL;
        {
            struct mmFileAssetsCacher* e = mmFileContext_GetAssetsCacher(p, pUnitName);
            if (NULL != e)
            {
                hFileCacher.type = e->type;
                hFileCacher.file = e->file;
                type = e->type;
                mmFileAssetsCacher_PathNameToRealName(e, pRealName, pUnitName);
            }
            else
            {
                type = -1;
            }
        }
        if (-1 != type) { break; }
        if (0 != mmIsAbsolutePath(pUnitName))
        {
            code = mmAbsolutePathIsDataExists(pUnitName);
            if (0 != code)
            {
                hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                hFileCacher.file = (struct mmIFileAssets**)&gFileAssetsFolderAbsolutePath;
                type = MM_FILE_ASSETS_TYPE_FOLDER;
                mmString_Assign(pRealName, &hUnitName);
            }
            else
            {
                type = -1;
            }
        }
        if (-1 != type) { break; }
        if (0 != p->folders.size)
        {
            struct mmFileAssetsFolder* e = NULL;
            n = mmRb_First(&p->folders.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsFolder*)(it->v);
                mmFileAssetsFolder_PathNameToRealName(e, &hRealName, pUnitName);
                code = mmFileAssetsFolder_IsDataExists(e, mmString_CStr(&hRealName));
                if (0 != code)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    type = MM_FILE_ASSETS_TYPE_FOLDER;
                    mmString_Assign(pRealName, &hRealName);
                    break;
                }
                else
                {
                    type = -1;
                }
            }
        }
        if (-1 != type) { break; }
        if (0 != p->sources.size)
        {
            struct mmFileAssetsSource* e = NULL;
            n = mmRb_First(&p->sources.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsSource*)(it->v);
                mmFileAssetsSource_PathNameToRealName(e, &hRealName, pUnitName);
                code = mmFileAssetsSource_IsDataExists(e, mmString_CStr(&hRealName));
                if (0 != code)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_SOURCE;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    type = MM_FILE_ASSETS_TYPE_SOURCE;
                    mmString_Assign(pRealName, &hRealName);
                    break;
                }
                else
                {
                    type = -1;
                }
            }
        }
    } while (0);
    if (-1 != type)
    {
        struct mmFileAssetsCacher* e = mmFileContext_AddAssetsCacher(p, pUnitName);
        e->type = hFileCacher.type;
        e->file = hFileCacher.file;
    }
    else
    {
        mmFileContext_RmvAssetsCacher(p, mmString_CStr(&hRealName));
        mmString_Assigns(pRealName, "");
    }
    mmFileAssetsCacher_Destroy(&hFileCacher);
    mmString_Destroy(&hRealName);
    mmString_Destroy(&hUnitName);
    return type;
}

// check file exists. 
// pPathName is assets logic absolute.
// return value
//     1 exists 
//     0 not exists.
MM_EXPORT_DISH
int 
mmFileContext_IsFileExists(
    struct mmFileContext*                          p, 
    const char*                                    pPathName)
{
    int code = 0;
    struct mmString hUnitName;
    struct mmString hRealName;
    struct mmFileAssetsCacher hFileCacher;
    const char* pUnitName = NULL;
    mmString_Init(&hUnitName);
    mmString_Init(&hRealName);
    mmFileAssetsCacher_Init(&hFileCacher);
    mmCleanPath(&hUnitName, pPathName);
    pUnitName = mmString_CStr(&hUnitName);
    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmRbNode* n = NULL;
        {
            struct mmFileAssetsCacher* e = mmFileContext_GetAssetsCacher(p, pUnitName);
            code = NULL != e ? 1 : 0;
            if (NULL != e)
            {
                hFileCacher.type = e->type;
                hFileCacher.file = e->file;
            }
        }
        if (0 != code) { break; }
        if (0 != mmIsAbsolutePath(pUnitName))
        {
            code = mmAbsolutePathIsDataExists(pUnitName);
            if (0 != code)
            {
                hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                hFileCacher.file = (struct mmIFileAssets**)&gFileAssetsFolderAbsolutePath;
            }
        }
        if (0 != code) { break; }
        if (0 != p->folders.size)
        {
            struct mmFileAssetsFolder* e = NULL;
            n = mmRb_First(&p->folders.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsFolder*)(it->v);
                mmFileAssetsFolder_PathNameToRealName(e, &hRealName, pUnitName);
                code = mmFileAssetsFolder_IsDataExists(e, mmString_CStr(&hRealName));
                if (0 != code)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    break;
                }
            }
        }
        if (0 != code) { break; }
        if (0 != p->sources.size)
        {
            struct mmFileAssetsSource* e = NULL;
            n = mmRb_First(&p->sources.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsSource*)(it->v);
                mmFileAssetsSource_PathNameToRealName(e, &hRealName, pUnitName);
                code = mmFileAssetsSource_IsDataExists(e, mmString_CStr(&hRealName));
                if (0 != code)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_SOURCE;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    break;
                }
            }
        }
    } while (0);
    if (0 != code)
    {
        struct mmFileAssetsCacher* e = mmFileContext_AddAssetsCacher(p, pUnitName);
        e->type = hFileCacher.type;
        e->file = hFileCacher.file;
    }
    else
    {
        mmFileContext_RmvAssetsCacher(p, mmString_CStr(&hRealName));
    }
    mmFileAssetsCacher_Destroy(&hFileCacher);
    mmString_Destroy(&hRealName);
    mmString_Destroy(&hUnitName);
    return code;
}

// acquire file buffer.
// priority 0 folder 1 source, note: we will first acquire folder file byte buffer.
MM_EXPORT_DISH 
void 
mmFileContext_AcquireFileByteBuffer(
    struct mmFileContext*                          p,
    const char*                                    pPathName,
    struct mmByteBuffer*                           pByteBuffer)
{
    struct mmString hUnitName;
    struct mmString hRealName;
    struct mmFileAssetsCacher hFileCacher;
    const char* pUnitName = NULL;
    mmString_Init(&hUnitName);
    mmString_Init(&hRealName);
    mmFileAssetsCacher_Init(&hFileCacher);
    mmCleanPath(&hUnitName, pPathName);
    mmByteBuffer_Reset(pByteBuffer);
    pUnitName = mmString_CStr(&hUnitName);
    do
    {
        struct mmRbtreeStringVptIterator* it = NULL;
        struct mmRbNode* n = NULL;
        {
            struct mmFileAssetsCacher* e = mmFileContext_GetAssetsCacher(p, pUnitName);
            if (NULL != e)
            {
                mmFileAssetsCacher_PathNameToRealName(e, &hRealName, pUnitName);
                mmFileAssetsCacher_AcquireFileByteBuffer(e, mmString_CStr(&hRealName), pByteBuffer);
                hFileCacher.type = e->type;
                hFileCacher.file = e->file;
            }
        }
        if (0 != pByteBuffer->length) { break; }
        if (0 != mmIsAbsolutePath(pUnitName))
        {
            mmAbsolutePathAcquireFileByteBuffer(pUnitName, pByteBuffer);
            if (0 != pByteBuffer->length)
            {
                hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                hFileCacher.file = (struct mmIFileAssets**)&gFileAssetsFolderAbsolutePath;
            }
        }
        if (0 != pByteBuffer->length) { break; }
        if (0 != p->folders.size)
        {
            struct mmFileAssetsFolder* e = NULL;
            n = mmRb_First(&p->folders.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsFolder*)(it->v);
                mmFileAssetsFolder_PathNameToRealName(e, &hRealName, pUnitName);
                mmFileAssetsFolder_AcquireFileByteBuffer(e, mmString_CStr(&hRealName), pByteBuffer);
                if (0 != pByteBuffer->length)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_FOLDER;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    break;
                }
            }
        }
        if (0 != pByteBuffer->length) { break; }
        if (0 != p->sources.size)
        {
            struct mmFileAssetsSource* e = NULL;
            n = mmRb_First(&p->sources.rbt);
            while (NULL != n)
            {
                it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
                n = mmRb_Next(n);
                e = (struct mmFileAssetsSource*)(it->v);
                mmFileAssetsSource_PathNameToRealName(e, &hRealName, pUnitName);
                mmFileAssetsSource_AcquireFileByteBuffer(e, mmString_CStr(&hRealName), pByteBuffer);
                if (0 != pByteBuffer->length)
                {
                    hFileCacher.type = MM_FILE_ASSETS_TYPE_SOURCE;
                    hFileCacher.file = (struct mmIFileAssets**)e;
                    break;
                }
            }
        }
    } while (0);
    if (0 != pByteBuffer->length)
    {
        struct mmFileAssetsCacher* e = mmFileContext_AddAssetsCacher(p, pUnitName);
        e->type = hFileCacher.type;
        e->file = hFileCacher.file;
    }
    else
    {
        struct mmLogger* gLogger = mmLogger_Instance();
        mmLogger_LogT(gLogger, "%s %d acquire path_name:%s failure.", __FUNCTION__, __LINE__, pPathName);
        mmFileContext_RmvAssetsCacher(p, mmString_CStr(&hRealName));
    }
    mmFileAssetsCacher_Destroy(&hFileCacher);
    mmString_Destroy(&hRealName);
    mmString_Destroy(&hUnitName);
}

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileContext_ReleaseFileByteBuffer(
    struct mmFileContext*                          p,
    struct mmByteBuffer*                           pByteBuffer)
{
    mmByteBuffer_Free(pByteBuffer);
}

// acquire file stream.
// priority 0 folder 1 source, note: we will first acquire folder file byte buffer.
MM_EXPORT_DISH
void
mmFileContext_AcquireStream(
    struct mmFileContext*                          p,
    const char*                                    pPathName,
    const char*                                    pMode,
    struct mmStream*                               pStream)
{
    struct mmString hRealName;
    int AssetsType;
    
    assert(NULL == pStream->s && "assets is not destroy complete.");
    
    mmString_Init(&hRealName);
    
    AssetsType = mmFileContext_GetFileAssetsPath(p, pPathName, &hRealName);
    
    switch (AssetsType)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        const char* pRealName = mmString_CStr(&hRealName);
        FILE* s = fopen(pRealName, pMode);
        pStream->i = &mmStreamFILE;
        pStream->s = (void*)s;
        pStream->t = AssetsType;
    }
    break;
    
    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        struct mmByteBuffer* s = mmMalloc(sizeof(struct mmByteBuffer));
        mmFileContext_AcquireFileByteBuffer(p, pPathName, s);
        pStream->i = &mmStreamByteBuffer;
        pStream->s = (void*)s;
        pStream->t = AssetsType;
    }
    break;
    
    default:
    {
        // Not support assets type.
    }
    break;
    }
    
    mmString_Destroy(&hRealName);
}

// release file stream.
MM_EXPORT_DISH
void
mmFileContext_ReleaseStream(
    struct mmFileContext*                          p,
    struct mmStream*                               pStream)
{
    switch (pStream->t)
    {
    case MM_FILE_ASSETS_TYPE_FOLDER:
    {
        if (NULL != pStream->s)
        {
            FILE* s = (FILE*)(pStream->s);
            fclose(s);
        }
        pStream->i = NULL;
        pStream->s = NULL;
        pStream->t = -1;
    }
    break;
    
    case MM_FILE_ASSETS_TYPE_SOURCE:
    {
        if (NULL != pStream->s)
        {
            struct mmByteBuffer* s = (struct mmByteBuffer*)(pStream->s);
            mmFileContext_ReleaseFileByteBuffer(p, s);
            mmFree(s);
        }
        pStream->i = NULL;
        pStream->s = NULL;
        pStream->t = -1;
    }
    break;
    
    default:
    {
        // Not support assets type.
    }
    break;
    }
}

MM_EXPORT_DISH 
void 
mmFileContext_AcquireFindFiles(
    struct mmFileContext*                          p,
    const char*                                    pDirectory,
    const char*                                    pPattern,
    int                                            hRecursive,
    int                                            hDirs,
    int                                            hIgnoreHidden,
    struct mmRbtreeStringVpt*                      pRbtree)
{
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmRbNode* n = NULL;
    if (0 != p->folders.size)
    {
        struct mmFileAssetsFolder* e = NULL;
        n = mmRb_First(&p->folders.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            e = (struct mmFileAssetsFolder*)(it->v);

            mmFileAssetsFolder_AcquireFindFiles(
                e, 
                pDirectory, 
                pPattern, 
                hRecursive, 
                hDirs, 
                hIgnoreHidden, 
                pRbtree);
        }
    }
    if (0 != p->sources.size)
    {
        struct mmFileAssetsSource* e = NULL;
        n = mmRb_First(&p->sources.rbt);
        while (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
            n = mmRb_Next(n);
            e = (struct mmFileAssetsSource*)(it->v);

            mmFileAssetsSource_AcquireFindFiles(
                e, 
                pDirectory, 
                pPattern, 
                hRecursive, 
                hDirs, 
                hIgnoreHidden, 
                pRbtree);
        }
    }
}

MM_EXPORT_DISH 
void 
mmFileContext_ReleaseFindFiles(
    struct mmFileContext*                          p,
    struct mmRbtreeStringVpt*                      pRbtree)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeStringVptIterator* it = NULL;
    struct mmFileAssetsInfo* e = NULL;
    n = mmRb_First(&pRbtree->rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeStringVptIterator, n);
        n = mmRb_Next(n);
        e = (struct mmFileAssetsInfo*)(it->v);
        mmRbtreeStringVpt_Erase(pRbtree, it);
        mmFileAssetsInfo_Destroy(e);
        mmFree(e);
    }
}
