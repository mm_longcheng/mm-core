/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTerminal_h__
#define __mmTerminal_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmThread.h"

#include "dish/mmDishExport.h"

#include <pthread.h>

#include "core/mmPrefix.h"

#define MM_CMD_STDIN_MAX_LINE 4096

struct mmTerminalCallback
{
    void(*Handle)(void* obj, const char* buff, size_t size);
    // weak ref. user data for callback.
    void* obj;
};
MM_EXPORT_DISH void mmTerminalCallback_Init(struct mmTerminalCallback* p);
MM_EXPORT_DISH void mmTerminalCallback_Destroy(struct mmTerminalCallback* p);

struct mmTerminal
{
    // default is ">"
    struct mmString prompt;

    struct mmTerminalCallback callback;
    
    // stdin thread.
    pthread_t thread_stdin;
    
    // mm_thread_state_t,default is MM_TS_CLOSED(0)
    mmSInt8_t state;
};

MM_EXPORT_DISH void mmTerminal_Init(struct mmTerminal* p);
MM_EXPORT_DISH void mmTerminal_Destroy(struct mmTerminal* p);

MM_EXPORT_DISH void mmTerminal_Start(struct mmTerminal* p);
MM_EXPORT_DISH void mmTerminal_Interrupt(struct mmTerminal* p);
MM_EXPORT_DISH void mmTerminal_Shutdown(struct mmTerminal* p);
MM_EXPORT_DISH void mmTerminal_Join(struct mmTerminal* p);

MM_EXPORT_DISH void mmTerminal_SetPrompt(struct mmTerminal* p, const char* prompt);
MM_EXPORT_DISH void mmTerminal_SetCallback(struct mmTerminal* p, struct mmTerminalCallback* callback);
MM_EXPORT_DISH void mmTerminal_Loop(struct mmTerminal* p);
MM_EXPORT_DISH void mmTerminal_FlushPrompt(struct mmTerminal* p);

#include "core/mmSuffix.h"

#endif//__mmTerminal_h__
