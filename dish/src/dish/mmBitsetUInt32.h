/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmBitsetUInt32_h__
#define __mmBitsetUInt32_h__

#include "core/mmConfig.h"

#include "container/mmBitset.h"

#include <algorithm>
#include <ostream>

#include "dish/mmDishExport.h"

namespace mm
{
    // use for port mm_dynamic_bitset api.
    class MM_EXPORT_DISH mmBitsetUInt32
    {
    public:
        typedef mmUInt32_t block_type;
        typedef size_t size_type;
        typedef size_type block_width_type;
    public:
        static const int bits_per_block = (std::numeric_limits<block_type>::digits);
        static const size_type npos = static_cast<size_type>(-1);
    public:
        // A proxy class to simulate lvalues of bit type.
        //
        class reference
        {
            friend class mmBitsetUInt32;

            // Possible misuse of comma operator here, we need conversion to void here.
            // the one and only non-copy ctor
            reference(block_type & b, block_width_type pos)
                : m_block(b)
                , m_mask(((void)assert(pos < bits_per_block), block_type(1) << pos))
            {

            }

            void operator&(); // left undefined

        public:

            // copy constructor: compiler generated

            operator bool() const { return (m_block & m_mask) != 0; }
            bool operator~() const { return (m_block & m_mask) == 0; }

            reference& flip() { do_flip(); return *this; }

            reference& operator=(bool x) { do_assign(x);   return *this; } // for b[i] = x
            reference& operator=(const reference& rhs) { do_assign(rhs); return *this; } // for b[i] = b[j]

            reference& operator|=(bool x) { if (x) do_set();   return *this; }
            reference& operator&=(bool x) { if (!x) do_reset(); return *this; }
            reference& operator^=(bool x) { if (x) do_flip();  return *this; }
            reference& operator-=(bool x) { if (x) do_reset(); return *this; }

        private:
            block_type & m_block;
            const block_type m_mask;

            void do_set() { m_block |= m_mask; }
            void do_reset() { m_block &= ~m_mask; }
            void do_flip() { m_block ^= m_mask; }
            void do_assign(bool x) { x ? do_set() : do_reset(); }
        };
    public:
        struct mmBitset d_bitset;
    public:
        mmBitsetUInt32();
        ~mmBitsetUInt32();
    public:
        mmBitsetUInt32(const mmBitsetUInt32& b);
        explicit mmBitsetUInt32(size_type bit_number);
    public:
        void swap(mmBitsetUInt32& b);
        mmBitsetUInt32& operator=(const mmBitsetUInt32& b);
        mmBitsetUInt32 operator~() const;
    public:
        void resize(size_t bit_number);
        void clear();

        size_t size();
        size_t block_number();
        bool empty();
        size_t capacity();
        void reserve(size_t bit_number);
        void shrink_to_fit();
    public:
        // bitset operations
        mmBitsetUInt32 & operator&=(const mmBitsetUInt32& b);
        mmBitsetUInt32& operator|=(const mmBitsetUInt32& b);
        mmBitsetUInt32& operator^=(const mmBitsetUInt32& b);
        mmBitsetUInt32& operator-=(const mmBitsetUInt32& b);
        mmBitsetUInt32& operator<<=(size_t n);
        mmBitsetUInt32& operator>>=(size_t n);
        mmBitsetUInt32 operator<<(size_t n) const;
        mmBitsetUInt32 operator>>(size_t n) const;
    public:
        void set(size_t n, int v);
        int get(size_t n) const;
        mmBitsetUInt32& flip(size_t n);
        mmBitsetUInt32& flip();
    public:
        int all();
        int any();
        int none();
        size_t count();
        void append(mmUInt32_t value);

        // subscript
        reference operator[](size_t n);
        bool operator[](size_t n) const;
    public:
        bool is_subset_of(const mmBitsetUInt32& a) const;
        bool is_proper_subset_of(const mmBitsetUInt32& a) const;
        bool intersects(const mmBitsetUInt32& a) const;

        // lookup
        size_t find_first() const;
        size_t find_next(size_t n) const;
    public:
        void update_size(size_t n);
    public:
        // lexicographical comparison
        friend bool operator==(const mmBitsetUInt32& a, const mmBitsetUInt32& b);
        friend bool operator!=(const mmBitsetUInt32& a, const mmBitsetUInt32& b);
        friend bool operator<(const mmBitsetUInt32& a, const mmBitsetUInt32& b);
    };
    //-----------------------------------------------------------------------------
    // comparison
    inline bool operator==(const mmBitsetUInt32& a, const mmBitsetUInt32& b)
    {
        return mmBitset_OperatorEqual((struct mmBitset*)&a.d_bitset, (struct mmBitset*)&b.d_bitset);
    }

    inline bool operator!=(const mmBitsetUInt32& a, const mmBitsetUInt32& b)
    {
        return mmBitset_OperatorNotEqual((struct mmBitset*)&a.d_bitset, (struct mmBitset*)&b.d_bitset);
    }

    inline bool operator<(const mmBitsetUInt32& a, const mmBitsetUInt32& b)
    {
        return mmBitset_OperatorLess((struct mmBitset*)&a.d_bitset, (struct mmBitset*)&b.d_bitset);
    }
    inline bool operator<=(const mmBitsetUInt32& a, const mmBitsetUInt32& b)
    {
        return mmBitset_OperatorLessEqual((struct mmBitset*)&a.d_bitset, (struct mmBitset*)&b.d_bitset);
    }
    inline bool operator>(const mmBitsetUInt32& a, const mmBitsetUInt32& b)
    {
        return mmBitset_OperatorGreater((struct mmBitset*)&a.d_bitset, (struct mmBitset*)&b.d_bitset);
    }
    inline bool operator>=(const mmBitsetUInt32& a, const mmBitsetUInt32& b)
    {
        return mmBitset_OperatorGreaterEqual((struct mmBitset*)&a.d_bitset, (struct mmBitset*)&b.d_bitset);
    }
    template<typename Ch, typename Tr>
    std::basic_ostream<Ch, Tr>& operator<<(std::basic_ostream<Ch, Tr>& os, const mmBitsetUInt32& b)
    {
        struct mmString s;
        mmString_Init(&s);
        mmBitset_ToString((struct mmBitset*)&b.d_bitset, &s);
        os << mmString_CStr(&s);
        mmString_Destroy(&s);
        return os;
    }
}

#endif//__mmBitsetUInt32_h__
