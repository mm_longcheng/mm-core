/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmValue.h"

namespace mm
{
    mmValue::mmValue(void)
        :d_value("")
    {
        
    }

    mmValue::mmValue(const mmValue& _val)
        :d_value(_val.d_value)
    {

    }

    mmValue::mmValue(const std::string& _val)
        : d_value(_val)
    {

    }

    mmValue::mmValue(const char* _val)
    {
        this->d_value = (NULL == _val) ? "" : _val;
    }

    mmValue::~mmValue(void)
    {

    }

    const mmValue::value_type& mmValue::GetValueType(void) const
    {
        return this->d_value;
    }

    mmValue::value_type& mmValue::GetValueTypeRef(void)
    {
        return this->d_value;
    }

    mmSChar_t mmValue::ToSChar(const std::string& _value)
    {
        mmSChar_t val = mmSChar_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmUChar_t mmValue::ToUChar(const std::string& _value)
    {
        mmUChar_t val = mmUChar_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmSShort_t mmValue::ToSShort(const std::string& _value)
    {
        mmSShort_t val = mmSShort_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmUShort_t mmValue::ToUShort(const std::string& _value)
    {
        mmUShort_t val = mmUShort_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmSInt_t mmValue::ToSInt(const std::string& _value)
    {
        mmSInt_t val = mmSInt_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmUInt_t mmValue::ToUInt(const std::string& _value)
    {
        mmUInt_t val = mmUInt_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmSLong_t mmValue::ToSLong(const std::string& _value)
    {
        mmSLong_t val = mmSLong_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmULong_t mmValue::ToULong(const std::string& _value)
    {
        mmULong_t val = mmULong_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmSLLong_t mmValue::ToSLLong(const std::string& _value)
    {
        mmSLLong_t val = mmSLLong_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmULLong_t mmValue::ToULLong(const std::string& _value)
    {
        mmULLong_t val = mmULLong_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmSInt32_t mmValue::ToSInt32(const std::string& _value)
    {
        mmSInt32_t val = mmSInt32_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmUInt32_t mmValue::ToUInt32(const std::string& _value)
    {
        mmUInt32_t val = mmUInt32_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmSInt64_t mmValue::ToSInt64(const std::string& _value)
    {
        mmSInt64_t val = mmSInt64_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    mmUInt64_t mmValue::ToUInt64(const std::string& _value)
    {
        mmUInt64_t val = mmUInt64_t();
        mmValue::ToValue(_value, val);
        return val;
    }
    float mmValue::ToFloat(const std::string& _value)
    {
        float val = float();
        mmValue::ToValue(_value, val);
        return val;
    }
    double mmValue::ToDouble(const std::string& _value)
    {
        double val = double();
        mmValue::ToValue(_value, val);
        return val;
    }
    bool mmValue::ToBool(const std::string& _value)
    {
        bool val = bool();
        mmValue::ToValue(_value, val);
        return val;
    }

    void mmValue::ToValue(const std::string& _value, mmSChar_t& _type)
    {
        sscanf(_value.c_str(), "%" SCNd8, &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmUChar_t& _type)
    {
        sscanf(_value.c_str(), "%" SCNu8, &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmSShort_t& _type)
    {
        sscanf(_value.c_str(), "%" SCNd16, &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmUShort_t& _type)
    {
        sscanf(_value.c_str(), "%" SCNu16, &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmSInt_t& _type)
    {
        sscanf(_value.c_str(), "%d", &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmUInt_t& _type)
    {
        sscanf(_value.c_str(), "%u", &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmSLong_t& _type)
    {
        sscanf(_value.c_str(), "%ld", &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmULong_t& _type)
    {
        sscanf(_value.c_str(), "%lu", &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmSLLong_t& _type)
    {
        sscanf(_value.c_str(), "%lld", &_type);
    }
    void mmValue::ToValue(const std::string& _value, mmULLong_t& _type)
    {
        sscanf(_value.c_str(), "%llu", &_type);
    }
    void mmValue::ToValue(const std::string& _value, float& _type)
    {
        sscanf(_value.c_str(), "%f", &_type);
    }
    void mmValue::ToValue(const std::string& _value, double& _type)
    {
        sscanf(_value.c_str(), "%lf", &_type);
    }
    void mmValue::ToValue(const std::string& _value, bool& _type)
    {
        _type = (_value == "true" ? true : false);
    }
    void mmValue::ToValue(const std::string& _value, std::string& _type)
    {
        _type = _value;
    }

    std::string mmValue::ToString(const mmSChar_t& _value)
    {
        char ch[8] = { 0 };
        sprintf(ch, "%" PRId8, _value);
        return ch;
    }
    std::string mmValue::ToString(const mmUChar_t& _value)
    {
        char ch[8] = { 0 };
        sprintf(ch, "%" PRIu8, _value);
        return ch;
    }
    std::string mmValue::ToString(const mmSShort_t& _value)
    {
        char ch[8] = { 0 };
        sprintf(ch, "%" PRId16, _value);
        return ch;
    }
    std::string mmValue::ToString(const mmUShort_t& _value)
    {
        char ch[8] = { 0 };
        sprintf(ch, "%" PRIu16, _value);
        return ch;
    }
    std::string mmValue::ToString(const mmSInt_t& _value)
    {
        char ch[16] = { 0 };
        sprintf(ch, "%d", _value);
        return ch;
    }
    std::string mmValue::ToString(const mmUInt_t& _value)
    {
        char ch[16] = { 0 };
        sprintf(ch, "%u", _value);
        return ch;
    }
    std::string mmValue::ToString(const mmSLong_t& _value)
    {
        char ch[32] = { 0 };
        sprintf(ch, "%ld", _value);
        return ch;
    }
    std::string mmValue::ToString(const mmULong_t& _value)
    {
        char ch[32] = { 0 };
        sprintf(ch, "%lu", _value);
        return ch;
    }
    std::string mmValue::ToString(const mmSLLong_t& _value)
    {
        char ch[32] = { 0 };
        sprintf(ch, "%lld", _value);
        return ch;
    }
    std::string mmValue::ToString(const mmULLong_t& _value)
    {
        char ch[32] = { 0 };
        sprintf(ch, "%llu", _value);
        return ch;
    }
    std::string mmValue::ToString(const float& _value)
    {
        char ch[16] = { 0 };
        sprintf(ch, "%f", _value);
        return ch;
    }
    std::string mmValue::ToString(const double& _value)
    {
        char ch[32] = { 0 };
        sprintf(ch, "%lf", _value);
        return ch;
    }
    std::string mmValue::ToString(const bool& _value)
    {
        return _value ? "true" : "false";
    }
    std::string mmValue::ToString(const char* _value)
    {
        return (NULL == _value) ? "" : _value;
    }
    std::string mmValue::ToString(const std::string& _value)
    {
        return _value;
    }


    void mmValue::ToString(std::string& _value, const mmSChar_t& _type)
    {
        int n = 0;
        _value.resize(8);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%" PRId8, _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmUChar_t& _type)
    {
        int n = 0;
        _value.resize(8);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%" PRIu8, _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmSShort_t& _type)
    {
        int n = 0;
        _value.resize(8);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%" PRId16, _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmUShort_t& _type)
    {
        int n = 0;
        _value.resize(8);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%" PRIu16, _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmSInt_t& _type)
    {
        int n = 0;
        _value.resize(16);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%d", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmUInt_t& _type)
    {
        int n = 0;
        _value.resize(16);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%u", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmSLong_t& _type)
    {
        int n = 0;
        _value.resize(32);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%ld", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmULong_t& _type)
    {
        int n = 0;
        _value.resize(32);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%lu", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmSLLong_t& _type)
    {
        int n = 0;
        _value.resize(32);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%lld", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const mmULLong_t& _type)
    {
        int n = 0;
        _value.resize(32);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%llu", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const float& _type)
    {
        int n = 0;
        _value.resize(16);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%f", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const double& _type)
    {
        int n = 0;
        _value.resize(32);
        memset((void*)_value.data(), 0, _value.size());
        n = sprintf((char*)_value.data(), "%lf", _type);
        _value.resize(n < 0 ? 0 : n);
    }
    void mmValue::ToString(std::string& _value, const bool& _type)
    {
        _value = (_type ? "true" : "false");
    }
    void mmValue::ToString(std::string& _value, const char* _type)
    {
        _value = (NULL == _type) ? "" : _type;
    }
    void mmValue::ToString(std::string& _value, const std::string& _type)
    {
        _value = _type;
    }
}

