/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFileZip_h__
#define __mmFileZip_h__

#include "core/mmCore.h"
#include "core/mmString.h"
#include "core/mmByte.h"

#include "container/mmRbtreeString.h"

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

struct mmZipEntry
{
    /* compression method */
    mmUInt64_t      compr;
    /* compressed size */
    mmUInt64_t      csize;
    /* file size / decompressed size */
    mmUInt64_t      dsize;
    /* file attib */
    int             attri;
    /* file name / strdupped name */
    struct mmString fullname;
    /* base name / strdupped name */
    struct mmString basename;
    /* path name / strdupped name */
    struct mmString pathname;
    
    /* file pos internal use */
    /* offset in zip file directory */
    mmUInt64_t pos;
    
    /* file pos internal use */
    /* # of file */
    mmUInt64_t num;
};

MM_EXPORT_DISH 
void 
mmZipEntry_Init(
    struct mmZipEntry*                             p);

MM_EXPORT_DISH 
void 
mmZipEntry_Destroy(
    struct mmZipEntry*                             p);

//
// check file name whether is directory.1 is directory 0 not.
MM_EXPORT_DISH 
void 
mmZipEntry_SetIsDirectory(
    struct mmZipEntry*                             p,
    int                                            hIsDirectory);

MM_EXPORT_DISH 
int 
mmZipEntry_GetIsDirectory(
    struct mmZipEntry*                             p);

MM_EXPORT_DISH 
void 
mmZipEntry_UpdateAttri(
    struct mmZipEntry*                             p);

struct mmZipDirectory
{
    struct mmString pathname;
    struct mmRbtreeStringVpt entrys;
};

MM_EXPORT_DISH 
void 
mmZipDirectory_Init(
    struct mmZipDirectory*                         p);

MM_EXPORT_DISH 
void 
mmZipDirectory_Destroy(
    struct mmZipDirectory*                         p);

MM_EXPORT_DISH 
struct mmZipEntry* 
mmZipDirectory_Add(
    struct mmZipDirectory*                         p, 
    struct mmString*                               fullname);

MM_EXPORT_DISH 
struct mmZipEntry* 
mmZipDirectory_Get(
    struct mmZipDirectory*                         p, 
    struct mmString*                               fullname);

MM_EXPORT_DISH 
struct mmZipEntry* 
mmZipDirectory_GetInstance(
    struct mmZipDirectory*                         p, 
    struct mmString*                               fullname);

MM_EXPORT_DISH 
void 
mmZipDirectory_Rmv(
    struct mmZipDirectory*                         p, 
    struct mmString*                               fullname);

MM_EXPORT_DISH 
void 
mmZipDirectory_Clear(
    struct mmZipDirectory*                         p);

struct mmFileZip
{
    struct mmString filename;
    struct mmString filter;
    struct mmRbtreeStringVpt directorys;
    
    // Internal implement.
    void* unzipFile;
};

MM_EXPORT_DISH 
void 
mmFileZip_Init(
    struct mmFileZip*                              p);

MM_EXPORT_DISH 
void 
mmFileZip_Destroy(
    struct mmFileZip*                              p);

MM_EXPORT_DISH 
void 
mmFileZip_SetFilename(
    struct mmFileZip*                              p, 
    const char*                                    filename);

MM_EXPORT_DISH 
void 
mmFileZip_SetFilter(
    struct mmFileZip*                              p, 
    const char*                                    filter);

MM_EXPORT_DISH 
void 
mmFileZip_Fopen(
    struct mmFileZip*                              p);

MM_EXPORT_DISH 
void 
mmFileZip_Close(
    struct mmFileZip*                              p);

MM_EXPORT_DISH 
void 
mmFileZip_AcquireAnalysis(
    struct mmFileZip*                              p);

MM_EXPORT_DISH 
void 
mmFileZip_ReleaseAnalysis(
    struct mmFileZip*                              p);

// acquire file buffer.
MM_EXPORT_DISH 
void 
mmFileZip_AcquireFileByteBuffer(
    const struct mmFileZip*                        p,
    const char*                                    fullname, 
    struct mmByteBuffer*                           pByteBuffer);

// release file buffer.
MM_EXPORT_DISH 
void 
mmFileZip_ReleaseFileByteBuffer(
    const  struct mmFileZip*                       p,
    struct mmByteBuffer*                           pByteBuffer);

// check file name exists.1 exists 0 not exists.
MM_EXPORT_DISH 
int 
mmFileZip_Exists(
    const struct mmFileZip*                        p,
    const char*                                    fullname);

// get entry for file name.
MM_EXPORT_DISH 
struct mmZipEntry* 
mmFileZip_GetEntry(
    const struct mmFileZip*                        p,
    const char*                                    fullname);


MM_EXPORT_DISH 
struct mmZipDirectory* 
mmFileZip_Add(
    struct mmFileZip*                              p, 
    struct mmString*                               pathname);

MM_EXPORT_DISH 
struct mmZipDirectory* 
mmFileZip_Get(
    const struct mmFileZip*                        p,
    struct mmString*                               pathname);

MM_EXPORT_DISH 
void 
mmFileZip_Rmv(
    struct mmFileZip*                              p, 
    struct mmString*                               pathname);

MM_EXPORT_DISH 
void 
mmFileZip_Clear(
    struct mmFileZip*                              p);

#include "core/mmSuffix.h"

#endif//__mmFileZip_h__
