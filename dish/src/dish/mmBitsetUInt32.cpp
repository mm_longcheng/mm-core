/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmBitsetUInt32.h"

namespace mm
{
    mmBitsetUInt32::mmBitsetUInt32()
    {
        mmBitset_Init(&this->d_bitset);
    }
    mmBitsetUInt32::~mmBitsetUInt32()
    {
        mmBitset_Destroy(&this->d_bitset);
    }
    mmBitsetUInt32::mmBitsetUInt32(const mmBitsetUInt32& b)
    {
        mmBitset_Init(&this->d_bitset);
        mmBitset_Assign(&this->d_bitset, (struct mmBitset*)&b.d_bitset);
    }
    mmBitsetUInt32::mmBitsetUInt32(size_type n)
    {
        mmBitset_Init(&this->d_bitset);
        mmBitset_Resize(&this->d_bitset, n);
    }
    void mmBitsetUInt32::swap(mmBitsetUInt32& b)
    {
        size_t s = mmUtf32String_Size(&this->d_bitset.v);
        mmUtf32String_Resize(&this->d_bitset.v, s);
        mmUtf32String_Assign(&b.d_bitset.v, &this->d_bitset.v);
        mmUtf32String_Reset(&this->d_bitset.v);
        mmUtf32String_Resize(&this->d_bitset.v, s);
        mmUtf32String_Assign(&this->d_bitset.v, &b.d_bitset.v);
        std::swap(this->d_bitset.n, b.d_bitset.n);
    }
    mmBitsetUInt32& mmBitsetUInt32::operator=(const mmBitsetUInt32& b)
    {
        if (this != &b)
        {
            mmBitset_Assign(&this->d_bitset, (struct mmBitset*)&b.d_bitset);
        }
        return *this;
    }
    mmBitsetUInt32 mmBitsetUInt32::operator~() const
    {
        mmBitsetUInt32 b(*this);
        b.flip();
        return b;
    }
    void mmBitsetUInt32::resize(size_t n)
    {
        mmBitset_Resize(&this->d_bitset, n);
    }
    void mmBitsetUInt32::clear()
    {
        mmBitset_Clear(&this->d_bitset);
    }

    size_t mmBitsetUInt32::size()
    {
        return mmBitset_Size(&this->d_bitset);
    }
    size_t mmBitsetUInt32::block_number()
    {
        return mmBitset_BlockNumber(&this->d_bitset);
    }
    bool mmBitsetUInt32::empty()
    {
        return mmBitset_Empty(&this->d_bitset);
    }
    size_t mmBitsetUInt32::capacity()
    {
        return mmBitset_Capacity(&this->d_bitset);
    }
    void mmBitsetUInt32::reserve(size_t n)
    {
        mmBitset_Reserve(&this->d_bitset, n);
    }
    void mmBitsetUInt32::shrink_to_fit()
    {
        size_t s = mmUtf32String_Size(&this->d_bitset.v);
        size_t c = mmUtf32String_Capacity(&this->d_bitset.v);
        if (s < c)
        {
            struct mmUtf32String v;
            mmUtf32String_Init(&v);
            mmUtf32String_Resize(&this->d_bitset.v, s);
            mmUtf32String_Assign(&v, &this->d_bitset.v);
            mmUtf32String_Reset(&this->d_bitset.v);
            mmUtf32String_Resize(&this->d_bitset.v, s);
            mmUtf32String_Assign(&this->d_bitset.v, &v);
            mmUtf32String_Destroy(&v);
        }
    }
    // bitset operations
    mmBitsetUInt32& mmBitsetUInt32::operator&=(const mmBitsetUInt32& b)
    {
        mmBitset_BitAnd((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&b.d_bitset);
        return *this;
    }
    mmBitsetUInt32& mmBitsetUInt32::operator|=(const mmBitsetUInt32& b)
    {
        mmBitset_BitOr((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&b.d_bitset);
        return *this;
    }
    mmBitsetUInt32& mmBitsetUInt32::operator^=(const mmBitsetUInt32& b)
    {
        mmBitset_BitNot((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&b.d_bitset);
        return *this;
    }
    mmBitsetUInt32& mmBitsetUInt32::operator-=(const mmBitsetUInt32& b)
    {
        mmBitset_BitSub((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&b.d_bitset);
        return *this;
    }
    mmBitsetUInt32& mmBitsetUInt32::operator<<=(size_t n)
    {
        mmBitset_BitLShift((struct mmBitset*)&this->d_bitset, n);
        return *this;
    }
    mmBitsetUInt32& mmBitsetUInt32::operator>>=(size_t n)
    {
        mmBitset_BitRShift((struct mmBitset*)&this->d_bitset, n);
        return *this;
    }
    mmBitsetUInt32 mmBitsetUInt32::operator<<(size_t n) const
    {
        mmBitsetUInt32 r(*this);
        mmBitset_BitLShift((struct mmBitset*)&r.d_bitset, n);
        return *this;
    }
    mmBitsetUInt32 mmBitsetUInt32::operator>>(size_t n) const
    {
        mmBitsetUInt32 r(*this);
        mmBitset_BitRShift((struct mmBitset*)&r.d_bitset, n);
        return *this;
    }
    void mmBitsetUInt32::set(size_t n, int v)
    {
        mmBitset_Set(&this->d_bitset, n, v);
    }
    int mmBitsetUInt32::get(size_t n) const
    {
        return mmBitset_Get((struct mmBitset*)&this->d_bitset, n);
    }
    mmBitsetUInt32& mmBitsetUInt32::flip(size_t n)
    {
        mmBitset_Flip(&this->d_bitset, n);
        return *this;
    }
    mmBitsetUInt32& mmBitsetUInt32::flip()
    {
        mmBitset_FlipAll(&this->d_bitset);
        return *this;
    }
    int mmBitsetUInt32::all()
    {
        return mmBitset_All(&this->d_bitset);
    }
    int mmBitsetUInt32::any()
    {
        return mmBitset_Any(&this->d_bitset);
    }
    int mmBitsetUInt32::none()
    {
        return mmBitset_None(&this->d_bitset);
    }
    size_t mmBitsetUInt32::count()
    {
        return mmBitset_Count(&this->d_bitset);
    }
    void mmBitsetUInt32::append(mmUInt32_t value)
    {
        mmBitset_Append(&this->d_bitset, value);
    }
    mmBitsetUInt32::reference mmBitsetUInt32::operator[](size_t n)
    {
        size_t block_index = mmBitset_CalculateBlockIndex(n);
        size_t bit_index = mmBitset_CalculateBitIndex(n);
        mmUInt32_t* arrays = (mmUInt32_t*)mmUtf32String_Data(&this->d_bitset.v);
        return reference(arrays[block_index], bit_index);
    }
    bool mmBitsetUInt32::operator[](size_t n) const
    {
        return this->get(n);
    }
    bool mmBitsetUInt32::is_subset_of(const mmBitsetUInt32& a) const
    {
        return mmBitset_IsSubsetOf((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&a.d_bitset);
    }
    bool mmBitsetUInt32::is_proper_subset_of(const mmBitsetUInt32& a) const
    {
        return mmBitset_IsProperSubsetOf((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&a.d_bitset);
    }
    bool mmBitsetUInt32::intersects(const mmBitsetUInt32& a) const
    {
        return mmBitset_Intersects((struct mmBitset*)&this->d_bitset, (struct mmBitset*)&a.d_bitset);
    }

    // lookup
    size_t mmBitsetUInt32::find_first() const
    {
        return mmBitset_FindFirst((struct mmBitset*)&this->d_bitset);
    }
    size_t mmBitsetUInt32::find_next(size_t n) const
    {
        return mmBitset_FindNext((struct mmBitset*)&this->d_bitset, n);
    }
    void mmBitsetUInt32::update_size(size_t n)
    {
        mmBitset_UpdateSize(&this->d_bitset, n);
    }
}
