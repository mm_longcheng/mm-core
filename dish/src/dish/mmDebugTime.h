/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmDebugTime_h__
#define __mmDebugTime_h__

#include "core/mmTime.h"
#include "core/mmLoggerManager.h"

#include "dish/mmDishExport.h"

namespace mm
{
    class MM_EXPORT_DISH mmDebugTime
    {
    public:
        explicit mmDebugTime(const char* _flag)
            : flag(_flag)
        {
            mmGettimeofday(&bt, NULL);
        }
        ~mmDebugTime(void)
        {
            unsigned int usec = 0;
            struct timeval et;
            struct mmLogger* gLogger = mmLogger_Instance();
            mmGettimeofday(&et, NULL);
            usec = (unsigned int)((et.tv_sec - bt.tv_sec) * MM_USEC_PER_SEC + et.tv_usec - bt.tv_usec);
            mmLogger_LogI(gLogger, "%s time period %u usec", flag, usec);
        }
    private:
        // weak ref for this flag.
        const char* flag;
        struct timeval bt;
    };
}

#ifdef MM_NOT_DEBUGTIME
#define MM_DEBUGTIME_FUNC()
#else
#define MM_DEBUGTIME_FUNC() mm::mmDebugTime _debug_time(__FUNCTION__)
#endif // _DEBUG

#endif//__mmDebugTime_h__
