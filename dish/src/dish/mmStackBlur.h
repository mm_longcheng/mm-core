/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2020-2021 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmStackBlur_h__
#define __mmStackBlur_h__

#include "core/mmCore.h"

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

/*
 * @param src     < input image data
 * @param w       < image width
 * @param h       < image height
 * @param radius  < blur intensity (should be in 2..254 range)
 * @param cores   < total number of working threads
 * @param core    < current thread number
 * @param step    < step of processing (1,2)
 * @param stack   < stack buffer
 */
MM_EXPORT_DISH 
void 
mmStackBlur_Job(
    unsigned char*                                 src,
    unsigned int                                   w,
    unsigned int                                   h,
    unsigned int                                   radius,
    int                                            cores,
    int                                            core,
    int                                            step,
    unsigned char*                                 stack);

/*
 * @param src     < input image data
 * @param w       < image width
 * @param h       < image height
 * @param radius  < blur intensity (should be in 2..254 range)
 */
MM_EXPORT_DISH 
void 
mmStackBlur_SingleThread(
    unsigned char*                                 src,
    unsigned int                                   w,
    unsigned int                                   h,
    unsigned int                                   radius);

/*
 * @param src     < input image data
 * @param w       < image width
 * @param h       < image height
 * @param radius  < blur intensity (should be in 2..254 range)
 * @param cores   < number of threads (1 - normal single thread)
 */
MM_EXPORT_DISH 
void 
mmStackBlur_MultiThread(
    unsigned char*                                 src,
    unsigned int                                   w,
    unsigned int                                   h,
    unsigned int                                   radius,
    int                                            cores);

#include "core/mmSuffix.h"

#endif//__mmStackBlur_h__
