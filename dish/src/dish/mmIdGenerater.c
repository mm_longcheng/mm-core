/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmIdGenerater.h"

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_Init(
    struct mmUInt32IdGenerater*                    p)
{
    mmRbtsetU32_Init(&p->idle);
    p->id_counter = 0;
    p->rate_trim_border = MM_ID_GENERATER_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_ID_GENERATER_RATE_TRIM_NUMBER_DEFAULT;
}

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_Destroy(
    struct mmUInt32IdGenerater*                    p)
{
    mmRbtsetU32_Destroy(&p->idle);
    p->id_counter = 0;
    p->rate_trim_border = MM_ID_GENERATER_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_ID_GENERATER_RATE_TRIM_NUMBER_DEFAULT;
}

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_SetRateTrimBorder(
    struct mmUInt32IdGenerater*                    p, 
    float                                          rate_trim_border)
{
    p->rate_trim_border = rate_trim_border;
}

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_SetRateTrimNumber(
    struct mmUInt32IdGenerater*                    p, 
    float                                          rate_trim_number)
{
    p->rate_trim_number = rate_trim_number;
}

MM_EXPORT_DISH 
mmUInt32_t 
mmUInt32IdGenerater_Produce(
    struct mmUInt32IdGenerater*                    p)
{
    mmUInt32_t id = -1;
    if (0 == p->idle.size)
    {
        id = p->id_counter++;
    }
    else
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetU32Iterator* it = NULL;
        // min.
        n = mmRb_First(&p->idle.rbt);
        if (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
            id = it->k;
            // rmv id to idle set.
            mmRbtsetU32_Rmv(&p->idle, id);
        }
    }
    return id;
}

MM_EXPORT_DISH 
void 
mmUInt32IdGenerater_Recycle(
    struct mmUInt32IdGenerater*                    p, 
    mmUInt32_t                                     id)
{
    mmUInt32_t max_id = 0;
    float idle_sz = 0;
    float memb_sz = 0;
    int trim_number = 0;

    // add id to idle set.
    mmRbtsetU32_Add(&p->idle, id);

    idle_sz = (float)p->idle.size;
    memb_sz = (float)p->id_counter;
    trim_number = (int)(memb_sz * p->rate_trim_number);

    while (0 != trim_number && idle_sz / memb_sz > p->rate_trim_border)
    {
        int i = 0;
        // 
        struct mmRbNode* n = NULL;
        struct mmRbtsetU32Iterator* it = NULL;

        idle_sz = (float)p->idle.size;
        memb_sz = (float)p->id_counter;
        trim_number = (int)(memb_sz * p->rate_trim_number);

        // max
        n = mmRb_Last(&p->idle.rbt);
        while (NULL != n && i < trim_number)
        {
            it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
            max_id = it->k;
            n = mmRb_Prev(n);
            if (max_id == p->id_counter - 1)
            {
                // rmv id to idle set.
                mmRbtsetU32_Erase(&p->idle, it);
                p->id_counter--;
            }
            else
            {
                trim_number = 0;
                break;
            }
            i++;
        }
    }
}

MM_EXPORT_DISH 
mmUInt32_t 
mmUInt32IdGenerater_MaxIdleId(
    struct mmUInt32IdGenerater*                    p)
{
    mmUInt32_t max_id = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU32Iterator* it = NULL;
    // max
    n = mmRb_Last(&p->idle.rbt);
    if (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU32Iterator, n);
        max_id = it->k;
    }
    return max_id;
}

MM_EXPORT_DISH 
void
mmUInt64IdGenerater_Init(
    struct mmUInt64IdGenerater*                    p)
{
    mmRbtsetU64_Init(&p->idle);
    p->id_counter = 0;
    p->rate_trim_border = MM_ID_GENERATER_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_ID_GENERATER_RATE_TRIM_NUMBER_DEFAULT;
}

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_Destroy(
    struct mmUInt64IdGenerater*                    p)
{
    mmRbtsetU64_Destroy(&p->idle);
    p->id_counter = 0;
    p->rate_trim_border = MM_ID_GENERATER_RATE_TRIM_BORDER_DEFAULT;
    p->rate_trim_number = MM_ID_GENERATER_RATE_TRIM_NUMBER_DEFAULT;
}

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_SetRateTrimBorder(
    struct mmUInt64IdGenerater*                    p, 
    float                                          rate_trim_border)
{
    p->rate_trim_border = rate_trim_border;
}

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_SetRateTrimNumber(
    struct mmUInt64IdGenerater*                    p, 
    float                                          rate_trim_number)
{
    p->rate_trim_number = rate_trim_number;
}

MM_EXPORT_DISH 
mmUInt64_t 
mmUInt64IdGenerater_Produce(
    struct mmUInt64IdGenerater*                    p)
{
    mmUInt64_t id = -1;
    if (0 == p->idle.size)
    {
        id = p->id_counter++;
    }
    else
    {
        struct mmRbNode* n = NULL;
        struct mmRbtsetU64Iterator* it = NULL;
        // min.
        n = mmRb_First(&p->idle.rbt);
        if (NULL != n)
        {
            it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
            id = it->k;
            // rmv id to idle set.
            mmRbtsetU64_Rmv(&p->idle, id);
        }
    }
    return id;
}

MM_EXPORT_DISH 
void 
mmUInt64IdGenerater_Recycle(
    struct mmUInt64IdGenerater*                    p, 
    mmUInt64_t                                     id)
{
    mmUInt64_t max_id = 0;
    float idle_sz = 0;
    float memb_sz = 0;
    int trim_number = 0;

    // add id to idle set.
    mmRbtsetU64_Add(&p->idle, id);

    idle_sz = (float)p->idle.size;
    memb_sz = (float)p->id_counter;
    trim_number = (int)(memb_sz * p->rate_trim_number);

    while (0 != trim_number && idle_sz / memb_sz > p->rate_trim_border)
    {
        int i = 0;
        // 
        struct mmRbNode* n = NULL;
        struct mmRbtsetU64Iterator* it = NULL;

        idle_sz = (float)p->idle.size;
        memb_sz = (float)p->id_counter;
        trim_number = (int)(memb_sz * p->rate_trim_number);

        // max
        n = mmRb_Last(&p->idle.rbt);
        while (NULL != n && i < trim_number)
        {
            it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
            max_id = it->k;
            n = mmRb_Prev(n);
            if (max_id == p->id_counter - 1)
            {
                // rmv id to idle set.
                mmRbtsetU64_Erase(&p->idle, it);
                p->id_counter--;
            }
            else
            {
                trim_number = 0;
                break;
            }
            i++;
        }
    }
}

MM_EXPORT_DISH 
mmUInt64_t 
mmUInt64IdGenerater_MaxIdleId(
    struct mmUInt64IdGenerater*                    p)
{
    mmUInt64_t max_id = 0;
    struct mmRbNode* n = NULL;
    struct mmRbtsetU64Iterator* it = NULL;
    // max
    n = mmRb_Last(&p->idle.rbt);
    if (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtsetU64Iterator, n);
        max_id = it->k;
    }
    return max_id;
}
