/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmMRuntimeState_h__
#define __mmMRuntimeState_h__

#include "core/mmCore.h"

#include "dish/mmPackage.h"

#include <string>

#include "dish/mmDishExport.h"

namespace mm
{
    //
    //  struct definition for mmMRuntimeState   //
    struct MM_EXPORT_DISH mmMRuntimeState : public mmPackage
    {
    public:
        // unique_id for server.
        mmUInt32_t unique_id;
        // process id.
        mmUInt64_t pid;
        // node internal.
        std::string node_i;
        // bind internal.
        std::string bind_i;
        // port internal.
        mmUInt32_t port_i;
        // workers internal.
        mmUInt32_t workers_i;
        // node external.
        std::string node_e;
        // bind external.
        std::string bind_e;
        // port external.
        mmUInt32_t port_e;
        // workers external.
        mmUInt32_t workers_e;
        // 01 lavg.
        mmFloat32_t lavg_01;
        // 05 lavg.
        mmFloat32_t lavg_05;
        // 15 lavg.
        mmFloat32_t lavg_15;
        // cpu percentage.
        mmFloat32_t cpu_pro;
        // mem percentage.
        mmFloat32_t mem_pro;
        // overload percentage.
        mmFloat32_t weights;
        // logger level.
        mmUInt32_t logger_lvl;
        // logger path.
        std::string logger_dir;
        // updatetime unix time.
        mmUInt32_t updatetime;
        // public key.
        std::string key;
    public:
        virtual ~mmMRuntimeState(void);
    public:
        mmMRuntimeState(void);
        virtual void encode(mmOArchive& archive) const;
        virtual void decode(const mmIArchive& archive);
    };

    //
    //  struct definition for mmMConfigModule   //
    struct MM_EXPORT_DISH mmMConfigModule : public mmPackage
    {
    public:
        // unique_id for module.
        mmUInt32_t unique_id;
        // message id l.
        mmUInt32_t mid_l;
        // message id r.
        mmUInt32_t mid_r;
        // shard size.
        mmUInt32_t shard_size;
        // depth size.
        mmUInt32_t depth_size;
        // description.
        std::string desc;
    public:
        virtual ~mmMConfigModule(void);
    public:
        mmMConfigModule(void);
        virtual void encode(mmOArchive& archive) const;
        virtual void decode(const mmIArchive& archive);
    };

}
#endif//__mmMRuntimeState_h__
