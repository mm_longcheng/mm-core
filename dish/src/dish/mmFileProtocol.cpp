/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFileProtocol.h"

#include "core/mmLogger.h"

namespace mm
{
    mmFileProtocol::mmFileProtocol(void)
    {
        mmStreambuf_Init(&this->d_streambuf);
    }

    mmFileProtocol::~mmFileProtocol(void)
    {
        mmStreambuf_Destroy(&this->d_streambuf);
    }

    // get file buffer.
    void mmFileProtocol::AcquireFileByteBuffer(const char* file_name, struct mmByteBuffer* byte_buffer)
    {
        mmStreambuf_Reset(&this->d_streambuf);
        mmOArchive os(this->d_streambuf);
        os << (*this);
        mmByteBuffer_Malloc(byte_buffer, (mmUInt32_t)mmStreambuf_Size(&this->d_streambuf));
        memcpy(byte_buffer->buffer, (const char*)(this->d_streambuf.buff + this->d_streambuf.gptr), byte_buffer->length);
    }
    // free file buffer.
    void mmFileProtocol::ReleaseFileByteBuffer(struct mmByteBuffer* byte_buffer)
    {
        mmByteBuffer_Free(byte_buffer);
    }

    void mmFileProtocol::Save(const char* file_name)
    {
        assert(NULL != file_name && "save file_name is a null.");
        mmStreambuf_Reset(&this->d_streambuf);
        mmOArchive os(this->d_streambuf);
        os << (*this);
        size_t size = mmStreambuf_Size(&this->d_streambuf);
        FILE* fdwb = fopen(file_name, "wb");
        fwrite((const char*)(this->d_streambuf.buff + this->d_streambuf.gptr), sizeof(char), size, fdwb);
        fclose(fdwb);
    }

    void mmFileProtocol::Load(const char* file_name)
    {
        assert(NULL != file_name && "load file_name is a null.");
        FILE* fdrb = fopen(file_name, "rb");
        assert(fdrb);
        fseek(fdrb, 0, SEEK_END);
        long fdsize = ftell(fdrb);
        fseek(fdrb, 0, SEEK_SET);
        char* fdbf = (char*)malloc(sizeof(char)*fdsize);
        fread(fdbf, sizeof(char), fdsize, fdrb);
        this->LoadBuffer(fdbf, fdsize);
        free(fdbf);
        fclose(fdrb);
    }

    void mmFileProtocol::LoadBuffer(const char* buffer, size_t size)
    {
        if (NULL != buffer && 0 < size)
        {
            mmStreambuf_Reset(&this->d_streambuf);
            mmStreambuf_Sputn(&this->d_streambuf, (mmUInt8_t*)buffer, 0, size);
            mmIArchive is1(this->d_streambuf);
            is1 >> (*this);
        }
        else
        {
            struct mmLogger* gLogger = mmLogger_Instance();
            mmLogger_LogI(gLogger, "%s %d loadBuffer buffer is a null.", __FUNCTION__, __LINE__);
        }
    }

}

