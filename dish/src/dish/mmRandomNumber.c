/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmRandomNumber.h"
#include <time.h>
#include "core/mmAlloc.h"

MM_EXPORT_DISH const char MM_RANDOM_NUMBER_CHAR_L = '!';
MM_EXPORT_DISH const char MM_RANDOM_NUMBER_CHAR_R = '~';
// MM_RANDOM_NUMBER_CHAR_R - MM_RANDOM_NUMBER_CHAR_L + 1 = 94
MM_EXPORT_DISH const int  MM_RANDOM_NUMBER_CHAR_N = 94;

//!< constant vector a
#define MM_RANDOM_NUMBER_MATRIX_A   0x9908b0dfUL
//!< most significant w-r bits
#define MM_RANDOM_NUMBER_UPPER_MASK 0x80000000UL
//!< least significant r bits
#define MM_RANDOM_NUMBER_LOWER_MASK 0x7fffffffUL

MM_EXPORT_DISH 
void 
mmRandomNumber_Init(
    struct mmRandomNumber*                         p)
{
    mmMemset(p->mt, 0, sizeof(unsigned long) * MM_RANDOM_NUMBER_N);
    p->mti = MM_RANDOM_NUMBER_N + 1;

    mmRandomNumber_ChangeSeed(p, (unsigned)time(NULL));
}

MM_EXPORT_DISH 
void 
mmRandomNumber_Destroy(
    struct mmRandomNumber*                         p)
{
    mmMemset(p->mt, 0, sizeof(unsigned long) * MM_RANDOM_NUMBER_N);
    p->mti = MM_RANDOM_NUMBER_N + 1;
}

MM_EXPORT_DISH double 
mmRandomNumber_GenrandReal1(
    struct mmRandomNumber*                         p)
{
    return mmRandomNumber_GenrandUInt32(p) * (1.0 / 4294967295.0);
}

MM_EXPORT_DISH 
double 
mmRandomNumber_GenrandReal2(
    struct mmRandomNumber*                         p)
{
    return mmRandomNumber_GenrandUInt32(p) * (1.0 / 4294967296.0);
}

MM_EXPORT_DISH 
double 
mmRandomNumber_GenrandReal3(
    struct mmRandomNumber*                         p)
{
    return (mmRandomNumber_GenrandUInt32(p) + 0.5) * (1.0 / 4294967296.0);
}

// [0, 0xffffffff]
MM_EXPORT_DISH 
unsigned long 
mmRandomNumber_GenrandUInt32(
    struct mmRandomNumber*                         p)
{
    unsigned long y;
    static unsigned long mag01[2] = { 0x0UL, MM_RANDOM_NUMBER_MATRIX_A };
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (p->mti >= MM_RANDOM_NUMBER_N)
    {
        /* generate N words at one time */
        int kk;

        if (p->mti == MM_RANDOM_NUMBER_N + 1)   /* if init_genrand() has not been called, */
        {
            mmRandomNumber_ChangeSeed(p, 5489UL);
        }

        for (kk = 0; kk < MM_RANDOM_NUMBER_N - MM_RANDOM_NUMBER_M; kk++)
        {
            y = (p->mt[kk] & MM_RANDOM_NUMBER_UPPER_MASK) | (p->mt[kk + 1] & MM_RANDOM_NUMBER_LOWER_MASK);
            p->mt[kk] = p->mt[kk + MM_RANDOM_NUMBER_M] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }

        for (; kk < MM_RANDOM_NUMBER_N - 1; kk++)
        {
            y = (p->mt[kk] & MM_RANDOM_NUMBER_UPPER_MASK) | (p->mt[kk + 1] & MM_RANDOM_NUMBER_LOWER_MASK);
            p->mt[kk] = p->mt[kk + (MM_RANDOM_NUMBER_M - MM_RANDOM_NUMBER_N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
        }

        y = (p->mt[MM_RANDOM_NUMBER_N - 1] & MM_RANDOM_NUMBER_UPPER_MASK) | (p->mt[0] & MM_RANDOM_NUMBER_LOWER_MASK);
        p->mt[MM_RANDOM_NUMBER_N - 1] = p->mt[MM_RANDOM_NUMBER_M - 1] ^ (y >> 1) ^ mag01[y & 0x1UL];

        p->mti = 0;
    }

    y = p->mt[p->mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
}

MM_EXPORT_DISH 
void 
mmRandomNumber_ChangeSeed(
    struct mmRandomNumber*                         p, 
    unsigned long                                  seed)
{
    p->mt[0] = seed & 0xffffffffUL;

    for (p->mti = 1; p->mti < MM_RANDOM_NUMBER_N; p->mti++)
    {
        p->mt[p->mti] = (1812433253UL * (p->mt[p->mti - 1] ^ (p->mt[p->mti - 1] >> 30)) + p->mti);
        /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
        /* In the previous versions, MSBs of the seed affect   */
        /* only MSBs of the array mt[].                        */
        /* 2002/01/09 modified by Makoto Matsumoto             */
        p->mt[p->mti] &= 0xffffffffUL;
        /* for >32 bit machines */
    }
}

// random buffer.
MM_EXPORT_DISH 
void 
mmRandomNumber_RandBuffer(
    struct mmRandomNumber*                         p, 
    unsigned char*                                 buffer, 
    unsigned int                                   offset, 
    unsigned int                                   length)
{
    size_t i = 0;
    unsigned long rn = 0;
    int val = 0;

    for (i = 0; i < length; ++i)
    {
        rn = mmRandomNumber_GenrandUInt32(p);
        val = (MM_RANDOM_NUMBER_CHAR_N > 0) ? (rn % MM_RANDOM_NUMBER_CHAR_N) : 0;
        val += MM_RANDOM_NUMBER_CHAR_L;
        buffer[offset + i] = (char)val;
    }
}
