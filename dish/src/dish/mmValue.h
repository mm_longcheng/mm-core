/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmValue_h__
#define __mmValue_h__

#include "core/mmCore.h"

#include <string>

#include "dish/mmDishExport.h"

namespace mm
{
    class MM_EXPORT_DISH mmValue
    {
    public:
        typedef std::string value_type;
    public:
        mmValue(void);
        mmValue(const mmValue& _val);
        mmValue(const std::string& _val);
        mmValue(const char* _val);
        virtual ~mmValue(void);
    public:
        const value_type& GetValueType(void) const;
        value_type& GetValueTypeRef(void);
    public:
        template<typename T>
        void GetValue(const T& _value)
        {
            mmValue::ToValue(this->d_value, _value);
        }
        //
        template<typename T>
        void SetValue(const T& _value)
        {
            mmValue::ToString(this->d_value, _value);
        }
    public:
        static mmSChar_t ToSChar(const std::string& _value);
        static mmUChar_t ToUChar(const std::string& _value);
        static mmSShort_t ToSShort(const std::string& _value);
        static mmUShort_t ToUShort(const std::string& _value);
        static mmSInt_t ToSInt(const std::string& _value);
        static mmUInt_t ToUInt(const std::string& _value);
        static mmSLong_t ToSLong(const std::string& _value);
        static mmULong_t ToULong(const std::string& _value);
        static mmSLLong_t ToSLLong(const std::string& _value);
        static mmULLong_t ToULLong(const std::string& _value);
        static mmSInt32_t ToSInt32(const std::string& _value);
        static mmUInt32_t ToUInt32(const std::string& _value);
        static mmSInt64_t ToSInt64(const std::string& _value);
        static mmUInt64_t ToUInt64(const std::string& _value);
        static float ToFloat(const std::string& _value);
        static double ToDouble(const std::string& _value);
        static bool ToBool(const std::string& _value);
        //
        static void ToValue(const std::string& _value, mmSChar_t& _type);
        static void ToValue(const std::string& _value, mmUChar_t& _type);
        static void ToValue(const std::string& _value, mmSShort_t& _type);
        static void ToValue(const std::string& _value, mmUShort_t& _type);
        static void ToValue(const std::string& _value, mmSInt_t& _type);
        static void ToValue(const std::string& _value, mmUInt_t& _type);
        static void ToValue(const std::string& _value, mmSLong_t& _type);
        static void ToValue(const std::string& _value, mmULong_t& _type);
        static void ToValue(const std::string& _value, mmSLLong_t& _type);
        static void ToValue(const std::string& _value, mmULLong_t& _type);
        static void ToValue(const std::string& _value, float& _type);
        static void ToValue(const std::string& _value, double& _type);
        static void ToValue(const std::string& _value, bool& _type);
        static void ToValue(const std::string& _value, std::string& _type);
        //
        static std::string ToString(const mmSChar_t& _value);
        static std::string ToString(const mmUChar_t& _value);
        static std::string ToString(const mmSShort_t& _value);
        static std::string ToString(const mmUShort_t& _value);
        static std::string ToString(const mmSInt_t& _value);
        static std::string ToString(const mmUInt_t& _value);
        static std::string ToString(const mmSLong_t& _value);
        static std::string ToString(const mmULong_t& _value);
        static std::string ToString(const mmSLLong_t& _value);
        static std::string ToString(const mmULLong_t& _value);
        static std::string ToString(const float& _value);
        static std::string ToString(const double& _value);
        static std::string ToString(const bool& _value);
        static std::string ToString(const char* _value);
        static std::string ToString(const std::string& _value);
        //
        static void ToString(std::string& _value, const mmSChar_t& _type);
        static void ToString(std::string& _value, const mmUChar_t& _type);
        static void ToString(std::string& _value, const mmSShort_t& _type);
        static void ToString(std::string& _value, const mmUShort_t& _type);
        static void ToString(std::string& _value, const mmSInt_t& _type);
        static void ToString(std::string& _value, const mmUInt_t& _type);
        static void ToString(std::string& _value, const mmSLong_t& _type);
        static void ToString(std::string& _value, const mmULong_t& _type);
        static void ToString(std::string& _value, const mmSLLong_t& _type);
        static void ToString(std::string& _value, const mmULLong_t& _type);
        static void ToString(std::string& _value, const float& _type);
        static void ToString(std::string& _value, const double& _type);
        static void ToString(std::string& _value, const bool& _type);
        static void ToString(std::string& _value, const char* _type);
        static void ToString(std::string& _value, const std::string& _type);
    private:
        value_type d_value;
    };
}
#endif//__mmValue_h__
