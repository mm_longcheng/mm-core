/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmXoshiroString_h__
#define __mmXoshiroString_h__

#include "core/mmConfig.h"
#include "core/mmTypes.h"
#include "core/mmString.h"

#include "random/mmXoshiro.h"

#include "dish/mmDishExport.h"

#include "core/mmPrefix.h"

MM_EXPORT_DISH extern const char MM_XOSHIRO_RANDOM_NUMBER_CHAR_L;
MM_EXPORT_DISH extern const char MM_XOSHIRO_RANDOM_NUMBER_CHAR_R;

// N = '~' - '!' + 1
MM_EXPORT_DISH extern const int  MM_XOSHIRO_RANDOM_NUMBER_CHAR_N;

// random buffer
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomBuffer(
    struct mmXoshiro256starstar*                   r,
    unsigned char*                                 buffer,
    unsigned int                                   offset,
    size_t                                         length);

// random string
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length);

// random table buffer
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomTableBuffer(
    struct mmXoshiro256starstar*                   r,
    unsigned char*                                 buffer,
    unsigned int                                   offset,
    size_t                                         length,
    const char*                                    t,
    size_t                                         n);

// random table string
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomTableString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length,
    const char*                                    t,
    size_t                                         n);

// [0, 9] [A, Z] [a, z]
MM_EXPORT_DISH extern const char   MM_XOSHIRO_RANDOM_TABLE_A[62];
MM_EXPORT_DISH extern const size_t MM_XOSHIRO_RANDOM_TABLE_N0;
MM_EXPORT_DISH extern const size_t MM_XOSHIRO_RANDOM_TABLE_N1;

// [0, 9] [A, z]
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomCapableNumberCaseString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length);

// [0, 9] [A, Z]
MM_EXPORT_DISH 
void
mmXoshiro256starstar_RandomCapableNumberString(
    struct mmXoshiro256starstar*                   r,
    struct mmString*                               v,
    size_t                                         length);

#include "core/mmSuffix.h"

#endif//__mmXoshiroString_h__
