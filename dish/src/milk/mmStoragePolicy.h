/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmStoragePolicy_h__
#define __mmStoragePolicy_h__

namespace mm
{
    ////////////////////////////////////////////////////////////////////////////////
    ///  \class HeapStorage
    ///
    ///  \ingroup  SmartPointerStorageGroup
    ///  Implementation of the StoragePolicy used by SharedPtr.  Uses explicit call
    ///   to T's destructor followed by call to free.
    ////////////////////////////////////////////////////////////////////////////////


    template<class T>
    class HeapStorage
    {
    public:
        typedef T* StoredType;      /// the type of the pointee_ object
        typedef T* InitPointerType; /// type used to declare OwnershipPolicy type.
        typedef T* PointerType;     /// type returned by operator->
        typedef T& ReferenceType;   /// type returned by operator*

        HeapStorage() : pointee_(Default())
        {}

        // The storage policy doesn't initialize the stored pointer
        //     which will be initialized by the OwnershipPolicy's Clone fn
        HeapStorage(const HeapStorage&) : pointee_(0)
        {}

        template<class U>
        HeapStorage(const HeapStorage<U>&) : pointee_(0)
        {}

        HeapStorage(const StoredType& p) : pointee_(p) {}

        PointerType operator->() const { return pointee_; }

        ReferenceType operator*() const { return *pointee_; }

        void Swap(HeapStorage& rhs)
        {
            std::swap(pointee_, rhs.pointee_);
        }

        // Accessors
        template<class F>
        friend typename HeapStorage<F>::PointerType GetImpl(const HeapStorage<F>& sp);

        template<class F>
        friend const typename HeapStorage<F>::StoredType& GetImplRef(const HeapStorage<F>& sp);

        template<class F>
        friend typename HeapStorage<F>::StoredType& GetImplRef(HeapStorage<F>& sp);

    protected:
        // Destroys the data stored
        // (Destruction might be taken over by the OwnershipPolicy)
        void Destroy()
        {
            if (0 != pointee_)
            {
                pointee_->~T();
                ::free(pointee_);
            }
        }

        // Default value to initialize the pointer
        static StoredType Default()
        {
            return 0;
        }

    private:
        // Data
        StoredType pointee_;
    };

    template<class T>
    inline typename HeapStorage<T>::PointerType GetImpl(const HeapStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline const typename HeapStorage<T>::StoredType& GetImplRef(const HeapStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline typename HeapStorage<T>::StoredType& GetImplRef(HeapStorage<T>& sp)
    {
        return sp.pointee_;
    }


    ////////////////////////////////////////////////////////////////////////////////
    ///  \class DefaultSPStorage
    ///
    ///  \ingroup  SmartPointerStorageGroup
    ///  Implementation of the StoragePolicy used by SharedPtr
    ////////////////////////////////////////////////////////////////////////////////


    template<class T>
    class DefaultSPStorage
    {
    public:
        typedef T* StoredType;    // the type of the pointee_ object
        typedef T* InitPointerType; /// type used to declare OwnershipPolicy type.
        typedef T* PointerType;   // type returned by operator->
        typedef T& ReferenceType; // type returned by operator*

        DefaultSPStorage() : pointee_(Default())
        {}

        // The storage policy doesn't initialize the stored pointer
        //     which will be initialized by the OwnershipPolicy's Clone fn
        DefaultSPStorage(const DefaultSPStorage&) : pointee_(0)
        {}

        template<class U>
        DefaultSPStorage(const DefaultSPStorage<U>&) : pointee_(0)
        {}

        DefaultSPStorage(const StoredType& p) : pointee_(p) {}

        PointerType operator->() const { return pointee_; }

        ReferenceType operator*() const { return *pointee_; }

        void Swap(DefaultSPStorage& rhs)
        {
            std::swap(pointee_, rhs.pointee_);
        }

        // Accessors
        template<class F>
        friend typename DefaultSPStorage<F>::PointerType GetImpl(const DefaultSPStorage<F>& sp);

        template<class F>
        friend const typename DefaultSPStorage<F>::StoredType& GetImplRef(const DefaultSPStorage<F>& sp);

        template<class F>
        friend typename DefaultSPStorage<F>::StoredType& GetImplRef(DefaultSPStorage<F>& sp);

    protected:
        // Destroys the data stored
        // (Destruction might be taken over by the OwnershipPolicy)
        //
        // If your compiler gives you a warning in this area while
        // compiling the tests, it is on purpose, please ignore it.
        void Destroy()
        {
            delete pointee_;
        }

        // Default value to initialize the pointer
        static StoredType Default()
        {
            return 0;
        }

    private:
        // Data
        StoredType pointee_;
    };

    template<class T>
    inline typename DefaultSPStorage<T>::PointerType GetImpl(const DefaultSPStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline const typename DefaultSPStorage<T>::StoredType& GetImplRef(const DefaultSPStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline typename DefaultSPStorage<T>::StoredType& GetImplRef(DefaultSPStorage<T>& sp)
    {
        return sp.pointee_;
    }


    ////////////////////////////////////////////////////////////////////////////////
    ///  \class LockedStorage
    ///
    ///  \ingroup  SmartPointerStorageGroup
    ///  Implementation of the StoragePolicy used by SharedPtr.
    ///
    ///  Each call to operator-> locks the object for the duration of a call to a
    ///  member function of T.
    ///
    ///  \par How It Works
    ///  LockedStorage has a helper class called Locker, which acts as a smart
    ///  pointer with limited abilities.  LockedStorage::operator-> returns an
    ///  unnamed temporary of type Locker<T> that exists for the duration of the
    ///  call to a member function of T.  The unnamed temporary locks the object
    ///  when it is constructed by operator-> and unlocks the object when it is
    ///  destructed.
    ///
    ///  \note This storage policy requires class T to have member functions Lock
    ///  and Unlock.  If your class does not have Lock or Unlock functions, you may
    ///  either make a child class which does, or make a policy class similar to
    ///  LockedStorage which calls other functions to lock the object.
    ////////////////////////////////////////////////////////////////////////////////

    template<class T>
    class Locker
    {
    public:
        Locker(const T * p) : pointee_(const_cast<T *>(p))
        {
            if (pointee_ != 0)
                pointee_->Lock();
        }

        ~Locker(void)
        {
            if (pointee_ != 0)
                pointee_->Unlock();
        }

        operator T * ()
        {
            return pointee_;
        }

        T * operator->()
        {
            return pointee_;
        }

    private:
        Locker(void);
        Locker & operator = (const Locker &);
        T * pointee_;
    };

    template<class T>
    class LockedStorage
    {
    public:

        typedef T* StoredType;           /// the type of the pointee_ object
        typedef T* InitPointerType;      /// type used to declare OwnershipPolicy type.
        typedef Locker< T > PointerType; /// type returned by operator->
        typedef T& ReferenceType;        /// type returned by operator*

        LockedStorage() : pointee_(Default()) {}

        ~LockedStorage(void) {}

        LockedStorage(const LockedStorage&) : pointee_(0) {}

        LockedStorage(const StoredType & p) : pointee_(p) {}

        PointerType operator->()
        {
            return Locker< T >(pointee_);
        }

        void Swap(LockedStorage& rhs)
        {
            std::swap(pointee_, rhs.pointee_);
        }

        // Accessors
        template<class F>
        friend typename LockedStorage<F>::InitPointerType GetImpl(const LockedStorage<F>& sp);

        template<class F>
        friend const typename LockedStorage<F>::StoredType& GetImplRef(const LockedStorage<F>& sp);

        template<class F>
        friend typename LockedStorage<F>::StoredType& GetImplRef(LockedStorage<F>& sp);

    protected:
        // Destroys the data stored
        // (Destruction might be taken over by the OwnershipPolicy)
        void Destroy()
        {
            delete pointee_;
        }

        // Default value to initialize the pointer
        static StoredType Default()
        {
            return 0;
        }

    private:
        /// Dereference operator is not implemented.
        ReferenceType operator*();

        // Data
        StoredType pointee_;
    };

    template<class T>
    inline typename LockedStorage<T>::InitPointerType GetImpl(const LockedStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline const typename LockedStorage<T>::StoredType& GetImplRef(const LockedStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline typename LockedStorage<T>::StoredType& GetImplRef(LockedStorage<T>& sp)
    {
        return sp.pointee_;
    }


    ////////////////////////////////////////////////////////////////////////////////
    ///  \class ArrayStorage
    ///
    ///  \ingroup  SmartPointerStorageGroup
    ///  Implementation of the ArrayStorage used by SharedPtr
    ////////////////////////////////////////////////////////////////////////////////


    template<class T>
    class ArrayStorage
    {
    public:
        typedef T* StoredType;    // the type of the pointee_ object
        typedef T* InitPointerType; /// type used to declare OwnershipPolicy type.
        typedef T* PointerType;   // type returned by operator->
        typedef T& ReferenceType; // type returned by operator*

        ArrayStorage() : pointee_(Default())
        {}

        // The storage policy doesn't initialize the stored pointer
        //     which will be initialized by the OwnershipPolicy's Clone fn
        ArrayStorage(const ArrayStorage&) : pointee_(0)
        {}

        template<class U>
        ArrayStorage(const ArrayStorage<U>&) : pointee_(0)
        {}

        ArrayStorage(const StoredType& p) : pointee_(p) {}

        PointerType operator->() const { return pointee_; }

        ReferenceType operator*() const { return *pointee_; }

        void Swap(ArrayStorage& rhs)
        {
            std::swap(pointee_, rhs.pointee_);
        }

        // Accessors
        template<class F>
        friend typename ArrayStorage<F>::PointerType GetImpl(const ArrayStorage<F>& sp);

        template<class F>
        friend const typename ArrayStorage<F>::StoredType& GetImplRef(const ArrayStorage<F>& sp);

        template<class F>
        friend typename ArrayStorage<F>::StoredType& GetImplRef(ArrayStorage<F>& sp);

    protected:
        // Destroys the data stored
        // (Destruction might be taken over by the OwnershipPolicy)
        void Destroy()
        {
            delete[] pointee_;
        }

        // Default value to initialize the pointer
        static StoredType Default()
        {
            return 0;
        }

    private:
        // Data
        StoredType pointee_;
    };

    template<class T>
    inline typename ArrayStorage<T>::PointerType GetImpl(const ArrayStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline const typename ArrayStorage<T>::StoredType& GetImplRef(const ArrayStorage<T>& sp)
    {
        return sp.pointee_;
    }

    template<class T>
    inline typename ArrayStorage<T>::StoredType& GetImplRef(ArrayStorage<T>& sp)
    {
        return sp.pointee_;
    }
}
#endif//__mmStoragePolicy_h__
