/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFactory_h__
#define __mmFactory_h__

//come from loki Factory.
// $Id: Factory.h 788 2006-11-24 22:30:54Z clitte_bbt $


#include "mmTypeInfo.h"
#include "mmFunctor.h"
#include "mmAssocVector.h"

/**
 * \defgroup    FactoriesGroup Factories
 * \defgroup    FactoryGroup Factory
 * \ingroup     FactoriesGroup
 * \brief       Implements a generic object factory.
 *
 * <i>The Factory Method pattern is an object-oriented design pattern.
 * Like other creational patterns, it deals with the problem of creating objects
 * (products) without specifying the exact class of object that will be created.
 * Factory Method, one of the patterns from the Design Patterns book, handles
 * this problem by defining a separate method for creating the objects, which
 * subclasses can then override to specify the derived type of product that will
 * be created.
 * <br>
 * More generally, the term Factory Method is often used to refer to any method
 * whose main purpose is creation of objects.</i>
 * <div ALIGN="RIGHT"><a href="http://en.wikipedia.org/wiki/Factory_method_pattern">
 * Wikipedia</a></div>
 *
 * Loki proposes a generic version of the Factory. Here is a typical use.<br>
 * <code><br>
 * 1. Factory< AbstractProduct, int > aFactory;<br>
 * 2. aFactory.Register( 1, createProductNull );<br>
 * 3. aFactory.CreateObject( 1 ); <br>
 * </code><br>
 * <br>
 * - 1. The declaration<br>
 * You want a Factory that produces AbstractProduct.<br>
 * The client will refer to a creation method through an int.<br>
 * - 2.The registration<br>
 * The code that will contribute to the Factory will now need to declare its
 * ProductCreator by registering them into the Factory.<br>
 * A ProductCreator is a just a function that will return the right object. ie <br>
 * <code>
 * Product* createProductNull()<br>
 * {<br>
 *     return new Product<br>
 * }<br>
 * </code><br>
 * - 3. The use<br>
 * Now the client can create object by calling the Factory's CreateObject method
 * with the right identifier. If the ProductCreator were to have arguments
 * (<i>ie :Product* createProductParm( int a, int b )</i>)
 */

namespace mm
{

    /**
     * \defgroup    FactoryErrorPoliciesGroup Factory Error Policies
     * \ingroup     FactoryGroup
     * \brief       Manages the "Unknown Type" error in an object factory
     */

    /**
     * \class DefaultFactoryError
     * \ingroup     FactoryErrorPoliciesGroup
     * \brief       Default policy that throws an exception
     *
     */

    template<typename IdentifierType, class AbstractProduct>
    struct DefaultFactoryError
    {
        struct Exception : public std::exception
        {
            const char* what() const throw() { return "Unknown Type"; }
        };

        static AbstractProduct* OnUnknownType(IdentifierType)
        {
            throw Exception();
        }
    };


#define ENABLE_NEW_FACTORY_CODE
#ifdef ENABLE_NEW_FACTORY_CODE


    ////////////////////////////////////////////////////////////////////////////////
    // class template FunctorImpl
    ////////////////////////////////////////////////////////////////////////////////

    struct FactoryImplBase
    {
        typedef EmptyType Parm1;
        typedef EmptyType Parm2;
    };

    template<typename AP, typename Id, typename TList>
    struct FactoryImpl;

    template<typename AP, typename Id>
    struct FactoryImpl<AP, Id, NullType>
        : public FactoryImplBase
    {
        typedef AP AbstractProduct;
        typedef Id IdentifierType;

        virtual ~FactoryImpl() {}
        virtual AbstractProduct* CreateObject(const IdentifierType & id) = 0;
    };
    template<typename AP, typename Id, typename P1>
    struct FactoryImpl<AP, Id, Seq<P1> >
        : public FactoryImplBase
    {
        typedef AP AbstractProduct;
        typedef Id IdentifierType;

        typedef P1 Parm1;
        virtual ~FactoryImpl() {}
        virtual AbstractProduct* CreateObject(const IdentifierType& id, Parm1) = 0;
    };

    template<typename AP, typename Id, typename P1, typename P2>
    struct FactoryImpl<AP, Id, Seq<P1, P2> >
        : public FactoryImplBase
    {
        typedef AP AbstractProduct;
        typedef Id IdentifierType;

        typedef P1 Parm1;
        typedef P2 Parm2;
        virtual ~FactoryImpl() {}
        virtual AbstractProduct* CreateObject(const IdentifierType& id, Parm1, Parm2) = 0;
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class Factory
    ///
    ///  \ingroup FactoryGroup
    ///  Implements a generic object factory.
    ///
    ///  Create functions can have up to 15 parameters.
    ///
    ///  \par Singleton lifetime when used with Loki::SingletonHolder
    ///  Because Factory uses internally Functors which inherits from
    ///  SmallObject you must use the singleton lifetime
    ///  \code Loki::LongevityLifetime::DieAsSmallObjectChild \endcode
    ///  Alternatively you could suppress for Functor the inheritance
    ///  from SmallObject by defining the macro:
    /// \code LOKI_FUNCTOR_IS_NOT_A_SMALLOBJECT \endcode
    ////////////////////////////////////////////////////////////////////////////////
    template<
        class AbstractProduct_,
        typename IdentifierType_,
        typename CreatorParmTList_ = NullType,
        template<typename, class> class FactoryErrorPolicy = DefaultFactoryError>
    class Factory : public FactoryErrorPolicy<IdentifierType_, AbstractProduct_>
    {
    public:
        typedef AbstractProduct_ AbstractProduct;
        typedef IdentifierType_ IdentifierType;
        typedef CreatorParmTList_ CreatorParmTList;

        typedef FactoryImpl< AbstractProduct, IdentifierType, CreatorParmTList > Impl;

        typedef typename Impl::Parm1 Parm1;
        typedef typename Impl::Parm2 Parm2;

        typedef Functor<AbstractProduct*, CreatorParmTList> ProductCreator;

        typedef AssocVector<IdentifierType, ProductCreator> IdToProductMap;
    private:
        IdToProductMap associations_;
    public:

        Factory()
            : associations_()
        {
        }

        ~Factory()
        {
            associations_.erase(associations_.begin(), associations_.end());
        }

        bool Register(const IdentifierType& id, ProductCreator creator)
        {
            return associations_.insert(
                typename IdToProductMap::value_type(id, creator)).second != 0;
        }

        template<class PtrObj, typename CreaFn>
        bool Register(const IdentifierType& id, const PtrObj& p, CreaFn fn)
        {
            ProductCreator creator(p, fn);
            return associations_.insert(
                typename IdToProductMap::value_type(id, creator)).second != 0;
        }

        bool Unregister(const IdentifierType& id)
        {
            return associations_.erase(id) != 0;
        }

        std::vector<IdentifierType> RegisteredIds()
        {
            std::vector<IdentifierType> ids;
            for (typename IdToProductMap::iterator it = associations_.begin();
                it != associations_.end(); ++it)
            {
                ids.push_back(it->first);
            }
            return ids;
        }

        AbstractProduct* CreateObject(const IdentifierType& id)
        {
            typename IdToProductMap::iterator i = associations_.find(id);
            if (i != associations_.end())
                return (i->second)();
            return this->OnUnknownType(id);
        }

        AbstractProduct* CreateObject(const IdentifierType& id,
            Parm1 p1)
        {
            typename IdToProductMap::iterator i = associations_.find(id);
            if (i != associations_.end())
                return (i->second)(p1);
            return this->OnUnknownType(id);
        }

        AbstractProduct* CreateObject(const IdentifierType& id,
            Parm1 p1, Parm2 p2)
        {
            typename IdToProductMap::iterator i = associations_.find(id);
            if (i != associations_.end())
                return (i->second)(p1, p2);
            return this->OnUnknownType(id);
        }

    };

#else

    template<
        class AbstractProduct,
        typename IdentifierType,
        typename ProductCreator = AbstractProduct * (*)(),
        template<typename, class> class FactoryErrorPolicy = DefaultFactoryError>
    class Factory
        : public FactoryErrorPolicy<IdentifierType, AbstractProduct>
    {
    public:
        bool Register(const IdentifierType& id, ProductCreator creator)
        {
            return associations_.insert(
                typename IdToProductMap::value_type(id, creator)).second != 0;
        }

        bool Unregister(const IdentifierType& id)
        {
            return associations_.erase(id) != 0;
        }

        AbstractProduct* CreateObject(const IdentifierType& id)
        {
            typename IdToProductMap::iterator i = associations_.find(id);
            if (i != associations_.end())
            {
                return (i->second)();
            }
            return this->OnUnknownType(id);
        }

    private:
        typedef AssocVector<IdentifierType, ProductCreator> IdToProductMap;
        IdToProductMap associations_;
    };

#endif//ENABLE_NEW_FACTORY_CODE

    /**
     *   \defgroup  CloneFactoryGroup Clone Factory
     *   \ingroup   FactoriesGroup
     *   \brief     Creates a copy from a polymorphic object.
     */
     
    /**
     *
     *   \class     CloneFactory
     *   \ingroup   CloneFactoryGroup
     *   \brief     Creates a copy from a polymorphic object.
     */

    template<
        class AbstractProduct,
        class ProductCreator =
        AbstractProduct * (*)(const AbstractProduct*),
        template<typename, class> class FactoryErrorPolicy = DefaultFactoryError>
    class CloneFactory
        : public FactoryErrorPolicy<TypeInfo, AbstractProduct>
    {
    public:
        bool Register(const TypeInfo& ti, ProductCreator creator)
        {
            return associations_.insert(
                typename IdToProductMap::value_type(ti, creator)).second != 0;
        }

        bool Unregister(const TypeInfo& id)
        {
            return associations_.erase(id) != 0;
        }

        AbstractProduct* CreateObject(const AbstractProduct* model)
        {
            if (model == NULL)
            {
                return NULL;
            }

            typename IdToProductMap::iterator i =
                associations_.find(typeid(*model));

            if (i != associations_.end())
            {
                return (i->second)(model);
            }
            return this->OnUnknownType(typeid(*model));
        }

    private:
        typedef AssocVector<TypeInfo, ProductCreator> IdToProductMap;
        IdToProductMap associations_;
    };

}//namespace mm

#endif//__mmFactory_h__

