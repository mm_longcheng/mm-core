/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUnImplCreatePolicy_h__
#define __mmUnImplCreatePolicy_h__

#include "mmSharedPtr.h"

namespace mm
{
    template<typename T>
    struct UnImplCreatePolicy
    {
        static T* Create();
        static void Destroy(T* p);
    };

    template<typename T>
    class UnImplType
    {
    public:
        typedef T value_type;
        typedef T& ReferenceType;
        typedef const T& ConstReferenceType;
        typedef T* PointerType;
        typedef const T* ConstPointerType;
    public:
        UnImplType()
        {
            mPointer = UnImplCreatePolicy<value_type>::Create();
        }
        virtual ~UnImplType()
        {
            UnImplCreatePolicy<value_type>::Destroy(mPointer);
        }
    public:
        PointerType operator->()
        {
            return mPointer;
        }

        ConstPointerType operator->() const
        {
            return mPointer;
        }

        ReferenceType operator*()
        {
            return (*mPointer);
        }

        ConstReferenceType operator*() const
        {
            return (*mPointer);
        }

        PointerType get()
        {
            return mPointer;
        }

        ConstPointerType get()const
        {
            return mPointer;
        }
    private:
        PointerType mPointer;
    };

}
#endif//__mmUnImplCreatePolicy_h__
