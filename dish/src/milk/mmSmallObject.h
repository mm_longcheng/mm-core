/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSmallObject_h__
#define __mmSmallObject_h__

#include <cstddef>
#include <new>

namespace mm
{
    template<typename T = void>
    class SmallObject
    {
    public:

        /// Throwing single-object new throws bad_alloc when allocation fails.
#ifdef _MSC_VER
        /// @note MSVC complains about non-empty exception specification lists.
        static void * operator new (std::size_t size)
#else
        static void * operator new (std::size_t size) throw (std::bad_alloc)
#endif
        {
            return malloc(size);
            //typename MyThreadingModel::Lock lock;
            //(void)lock; // get rid of warning
            //return MyAllocatorSingleton::Instance().Allocate( size, true );
        }

        /// Non-throwing single-object new returns NULL if allocation fails.
        static void * operator new (std::size_t size, const std::nothrow_t &) throw ()
        {
            return malloc(size);
            //typename MyThreadingModel::Lock lock;
            // (void)lock; // get rid of warning
            // return MyAllocatorSingleton::Instance().Allocate( size, false );
        }

        /// Placement single-object new merely calls global placement new.
        inline static void * operator new (std::size_t size, void * place)
        {
            return ::operator new(size, place);
        }

        /// Single-object delete.
        static void operator delete (void * p, std::size_t size) throw ()
        {
            free(p);
            //typename MyThreadingModel::Lock lock;
            //(void)lock; // get rid of warning
            //MyAllocatorSingleton::Instance().Deallocate( p, size );
        }

        /** Non-throwing single-object delete is only called when nothrow
         new operator is used, and the constructor throws an exception.
         */
        static void operator delete (void * p, const std::nothrow_t &) throw()
        {
            free(p);
            //typename MyThreadingModel::Lock lock;
            // (void)lock; // get rid of warning
            // MyAllocatorSingleton::Instance().Deallocate( p );
        }

        /// Placement single-object delete merely calls global placement delete.
        inline static void operator delete (void * p, void * place)
        {
            ::operator delete (p, place);
        }

#ifdef LOKI_SMALL_OBJECT_USE_NEW_ARRAY

        /// Throwing array-object new throws bad_alloc when allocation fails.
#ifdef _MSC_VER
        /// @note MSVC complains about non-empty exception specification lists.
        static void * operator new[](std::size_t size)
#else
        static void * operator new [](std::size_t size)
            throw (std::bad_alloc)
#endif
        {
            typename MyThreadingModel::Lock lock;
            (void)lock; // get rid of warning
            return MyAllocatorSingleton::Instance().Allocate(size, true);
        }

        /// Non-throwing array-object new returns NULL if allocation fails.
        static void * operator new [](std::size_t size,
            const std::nothrow_t &) throw ()
        {
            typename MyThreadingModel::Lock lock;
            (void)lock; // get rid of warning
            return MyAllocatorSingleton::Instance().Allocate(size, false);
        }

        /// Placement array-object new merely calls global placement new.
        inline static void * operator new [](std::size_t size, void * place)
        {
            return ::operator new(size, place);
        }

        /// Array-object delete.
        static void operator delete [](void * p, std::size_t size) throw ()
        {
            typename MyThreadingModel::Lock lock;
            (void)lock; // get rid of warning
            MyAllocatorSingleton::Instance().Deallocate(p, size);
        }

        /** Non-throwing array-object delete is only called when nothrow
            new operator is used, and the constructor throws an exception.
            */
        static void operator delete [](void * p,
            const std::nothrow_t &) throw()
        {
            typename MyThreadingModel::Lock lock;
            (void)lock; // get rid of warning
            MyAllocatorSingleton::Instance().Deallocate(p);
        }

        /// Placement array-object delete merely calls global placement delete.
        inline static void operator delete [](void * p, void * place)
        {
            ::operator delete (p, place);
        }
#endif//LOKI_SMALL_OBJECT_USE_NEW_ARRAY
    };
}
#endif//__mmSmallObject_h__
