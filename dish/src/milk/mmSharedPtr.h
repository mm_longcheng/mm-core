/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSharedPtr_h__
#define __mmSharedPtr_h__

#include "mmTypeManip.h"
#include "mmRefToValue.h"
#include "mmStoragePolicy.h"
#include "mmOwnershipPolicy.h"
#include "mmConstPolicy.h"
#include "mmConversionPolicy.h"
#include "mmCheckingPolicy.h"

#include <assert.h>
#include <iostream>

//from Loki shared ptr.
#if defined(_MSC_VER) || defined(__GNUC__)
// GCC>=4.1 must use -ffriend-injection due to a bug in GCC
#define MILK_ENABLE_FRIEND_TEMPLATE_TEMPLATE_PARAMETER_WORKAROUND
#endif

namespace mm
{
    struct _Static_tag {};
    struct _Const_tag {};
    struct _Dynamic_tag {};

    ////////////////////////////////////////////////////////////////////////////////
    // class template SharedPtr (declaration)
    // The reason for all the fuss above
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OwnershipPolicy = RefCounted,
        class ConversionPolicy = DisallowConversion,
        template<class> class CheckingPolicy = AssertCheck,
        template<class> class StoragePolicy = DefaultSPStorage,
        template<class> class ConstnessPolicy = DontPropagateConst>
    class SharedPtr;

    ////////////////////////////////////////////////////////////////////////////////
    // class template SharedPtrDef (definition)
    // this class added to unify the usage of SharedPtr
    // instead of writing SharedPtr<T,OP,CP,KP,SP> write SharedPtrDef<T,OP,CP,KP,SP>::type
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OwnershipPolicy = RefCounted,
        class ConversionPolicy = DisallowConversion,
        template<class> class CheckingPolicy = AssertCheck,
        template<class> class StoragePolicy = DefaultSPStorage,
        template<class> class ConstnessPolicy = DontPropagateConst>
    struct SharedPtrDef
    {
        typedef SharedPtr<
            T,
            OwnershipPolicy,
            ConversionPolicy,
            CheckingPolicy,
            StoragePolicy,
            ConstnessPolicy>
            type;
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class SharedPtr
    ///
    ///  \ingroup SmartPointerGroup
    ///
    ///  \param OwnershipPolicy  default =  RefCounted,
    ///  \param ConversionPolicy default = DisallowConversion,
    ///  \param CheckingPolicy default = AssertCheck,
    ///  \param StoragePolicy default = DefaultSPStorage
    ///  \param ConstnessPolicy default = DontPropagateConst
    ///
    ///  \par IMPORTANT NOTE
    ///  Due to threading issues, the OwnershipPolicy has been changed as follows:
    ///
    ///     - Release() returns a boolean saying if that was the last release
    ///        so the pointer can be deleted by the StoragePolicy
    ///     - IsUnique() was removed
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OwnershipPolicy,
        class ConversionPolicy,
        template<class> class CheckingPolicy,
        template<class> class StoragePolicy,
        template<class> class ConstnessPolicy>
    class SharedPtr
        : public StoragePolicy<T>
        , public OwnershipPolicy<typename StoragePolicy<T>::InitPointerType>
        , public CheckingPolicy<typename StoragePolicy<T>::StoredType>
        , public ConversionPolicy
    {
    public:
        typedef StoragePolicy<T> SP;
        typedef OwnershipPolicy<typename StoragePolicy<T>::InitPointerType> OP;
        typedef CheckingPolicy<typename StoragePolicy<T>::StoredType> KP;
        typedef ConversionPolicy CP;

    public:
        typedef typename ConstnessPolicy<T>::Type* ConstPointerType;
        typedef typename ConstnessPolicy<T>::Type& ConstReferenceType;

        typedef typename SP::PointerType PointerType;
        typedef typename SP::StoredType StoredType;
        typedef typename SP::ReferenceType ReferenceType;

        typedef typename Select<OP::destructiveCopy, SharedPtr, const SharedPtr>::Result CopyArg;

    private:
        struct NeverMatched {};

#ifdef MILK_SHAREDPTR_CONVERSION_CONSTRUCTOR_POLICY
        typedef typename Select< CP::allow, const StoredType&, NeverMatched>::Result ImplicitArg;
        typedef typename Select<!CP::allow, const StoredType&, NeverMatched>::Result ExplicitArg;
#else
        typedef const StoredType& ImplicitArg;
        typedef typename Select<false, const StoredType&, NeverMatched>::Result ExplicitArg;
#endif

    public:

        SharedPtr()
        {
            KP::OnDefault(GetImpl(*this));
        }

        explicit SharedPtr(ExplicitArg p) : SP(p)
        {
            KP::OnInit(GetImpl(*this));
        }

        SharedPtr(ImplicitArg p) : SP(p)
        {
            KP::OnInit(GetImpl(*this));
        }

        SharedPtr(CopyArg& rhs) : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            GetImplRef(*this) = OP::Clone(GetImplRef(rhs));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            GetImplRef(*this) = OP::Clone(GetImplRef(rhs));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            GetImplRef(*this) = OP::Clone(GetImplRef(rhs));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs, const _Static_tag&)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            typedef SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 > _T1Type;
            GetImplRef(*this) = static_cast<PointerType>((_T1Type(rhs)).Clone(GetImplRef(rhs)));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs, const _Static_tag&)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            GetImplRef(*this) = static_cast<PointerType>(rhs.Clone(GetImplRef(rhs)));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs, const _Const_tag&)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            typedef SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 > _T1Type;
            GetImplRef(*this) = const_cast<PointerType>((_T1Type(rhs)).Clone(GetImplRef(rhs)));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs, const _Const_tag&)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            GetImplRef(*this) = const_cast<PointerType>(rhs.Clone(GetImplRef(rhs)));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs, const _Dynamic_tag&)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            typedef SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 > _T1Type;
            GetImplRef(*this) = dynamic_cast<PointerType>((_T1Type(rhs)).Clone(GetImplRef(rhs)));
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs, const _Dynamic_tag&)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {
            GetImplRef(*this) = dynamic_cast<PointerType>(rhs.Clone(GetImplRef(rhs)));
        }

        SharedPtr(RefToValue<SharedPtr> rhs)
            : SP(rhs), OP(rhs), KP(rhs), CP(rhs)
        {}

        operator RefToValue<SharedPtr>()
        {
            return RefToValue<SharedPtr>(*this);
        }

        SharedPtr& operator=(CopyArg& rhs)
        {
            SharedPtr temp(rhs);
            temp.Swap(*this);
            return *this;
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr& operator=(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs)
        {
            SharedPtr temp(rhs);
            temp.Swap(*this);
            return *this;
        }

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        SharedPtr& operator=(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs)
        {
            SharedPtr temp(rhs);
            temp.Swap(*this);
            return *this;
        }

        void Swap(SharedPtr& rhs)
        {
            OP::Swap(rhs);
            CP::Swap(rhs);
            KP::Swap(rhs);
            SP::Swap(rhs);
        }

        ~SharedPtr()
        {
            if (OP::Release(GetImpl(*static_cast<SP*>(this))))
            {
                SP::Destroy();
            }
        }

#ifdef MILK_ENABLE_FRIEND_TEMPLATE_TEMPLATE_PARAMETER_WORKAROUND

        // old non standard in class definition of friends
        friend inline void Release(SharedPtr& sp, typename SP::StoredType& p)
        {
            p = GetImplRef(sp);
            GetImplRef(sp) = SP::Default();
        }

        friend inline void Reset(SharedPtr& sp, typename SP::StoredType p)
        {
            SharedPtr(p).Swap(sp);
        }

#else

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        friend void Release(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1>& sp,
                typename SP1<T1>::StoredType& p);

        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        friend void Reset(SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1>& sp,
                typename SP1<T1>::StoredType p);
#endif


        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        bool Merge(SharedPtr< T1, OP1, CP1, KP1, SP1, CNP1 > & rhs)
        {
            if (GetImpl(*this) != GetImpl(rhs))
            {
                return false;
            }
            return OP::template Merge(rhs);
        }

        PointerType operator->()
        {
            KP::OnDereference(GetImplRef(*this));
            return SP::operator->();
        }

        ConstPointerType operator->() const
        {
            KP::OnDereference(GetImplRef(*this));
            return SP::operator->();
        }

        ReferenceType operator*()
        {
            KP::OnDereference(GetImplRef(*this));
            return SP::operator*();
        }

        ConstReferenceType operator*() const
        {
            KP::OnDereference(GetImplRef(*this));
            return SP::operator*();
        }

        bool operator!() const // Enables "if (!sp) ..."
        {
            return GetImpl(*this) == 0;
        }

        static inline T * GetPointer(const SharedPtr & sp)
        {
            return GetImpl(sp);
        }

        // Ambiguity buster
        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        bool operator==(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs) const
        {
            return GetImpl(*this) == GetImpl(rhs);
        }

        // Ambiguity buster
        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        bool operator!=(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs) const
        {
            return !(*this == rhs);
        }

        // Ambiguity buster
        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        bool operator<(const SharedPtr<T1, OP1, CP1, KP1, SP1, CNP1 >& rhs) const
        {
            return GetImpl(*this) < GetImpl(rhs);
        }

        // Ambiguity buster
        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        inline bool operator > (const SharedPtr< T1, OP1, CP1, KP1, SP1, CNP1 > & rhs)
        {
            return (GetImpl(rhs) < GetImpl(*this));
        }

        // Ambiguity buster
        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        inline bool operator <= (const SharedPtr< T1, OP1, CP1, KP1, SP1, CNP1 > & rhs)
        {
            return !(GetImpl(rhs) < GetImpl(*this));
        }

        // Ambiguity buster
        template<
            typename T1,
            template<class> class OP1,
            class CP1,
            template<class> class KP1,
            template<class> class SP1,
            template<class> class CNP1>
        inline bool operator >= (const SharedPtr< T1, OP1, CP1, KP1, SP1, CNP1 > & rhs)
        {
            return !(GetImpl(*this) < GetImpl(rhs));
        }

    private:
        // Helper for enabling 'if (sp)'
        struct Tester
        {
            Tester(int) {}
            void dummy() {}
        };

        typedef void (Tester::*unspecified_boolean_type_)();

        typedef typename Select<CP::allow, Tester, unspecified_boolean_type_>::Result
            unspecified_boolean_type;

    public:
        // enable 'if (sp)'
        operator unspecified_boolean_type() const
        {
            return !*this ? 0 : &Tester::dummy;
        }

    private:
        // Helper for disallowing automatic conversion
        struct Insipid
        {
            Insipid(PointerType) {}
        };

        typedef typename Select<CP::allow, PointerType, Insipid>::Result
            AutomaticConversionResult;

    public:
        operator AutomaticConversionResult() const
        {
            return GetImpl(*this);
        }
    };


    ////////////////////////////////////////////////////////////////////////////////
    // friends
    ////////////////////////////////////////////////////////////////////////////////

#ifndef MILK_ENABLE_FRIEND_TEMPLATE_TEMPLATE_PARAMETER_WORKAROUND

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP>
    inline void Release(SharedPtr<T, OP, CP, KP, SP, CNP>& sp,
            typename SP<T>::StoredType& p)
    {
        p = GetImplRef(sp);
        GetImplRef(sp) = SP<T>::Default();
    }

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP>
    inline void Reset(SharedPtr<T, OP, CP, KP, SP, CNP>& sp,
            typename SP<T>::StoredType p)
    {
        SharedPtr<T, OP, CP, KP, SP, CNP>(p).Swap(sp);
    }

#endif

    ////////////////////////////////////////////////////////////////////////////////
    // free comparison operators for class template SharedPtr
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator== for lhs = SharedPtr, rhs = raw pointer
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP1,
        typename U>
    inline bool operator==(const SharedPtr<T, OP, CP, KP, SP, CNP1 >& lhs,
            U* rhs)
    {
        return GetImpl(lhs) == rhs;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator== for lhs = raw pointer, rhs = SharedPtr
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP1,
        typename U>
    inline bool operator==(U* lhs,
            const SharedPtr<T, OP, CP, KP, SP, CNP1 >& rhs)
    {
        return rhs == lhs;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator!= for lhs = SharedPtr, rhs = raw pointer
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator!=(const SharedPtr<T, OP, CP, KP, SP, CNP >& lhs,
            U* rhs)
    {
        return !(lhs == rhs);
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator!= for lhs = raw pointer, rhs = SharedPtr
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator!=(U* lhs,
            const SharedPtr<T, OP, CP, KP, SP, CNP >& rhs)
    {
        return rhs != lhs;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator< for lhs = SharedPtr, rhs = raw pointer
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator<(const SharedPtr<T, OP, CP, KP, SP, CNP >& lhs,
            U* rhs)
    {
        return (GetImpl(lhs) < rhs);
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator< for lhs = raw pointer, rhs = SharedPtr
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator<(U* lhs,
            const SharedPtr<T, OP, CP, KP, SP, CNP >& rhs)
    {
        return (GetImpl(rhs) < lhs);
    }

    ////////////////////////////////////////////////////////////////////////////////
    //  operator> for lhs = SharedPtr, rhs = raw pointer
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator>(const SharedPtr<T, OP, CP, KP, SP, CNP >& lhs,
            U* rhs)
    {
        return rhs < lhs;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator> for lhs = raw pointer, rhs = SharedPtr
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator>(U* lhs,
            const SharedPtr<T, OP, CP, KP, SP, CNP >& rhs)
    {
        return rhs < lhs;
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator<= for lhs = SharedPtr, rhs = raw pointer
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator<=(const SharedPtr<T, OP, CP, KP, SP, CNP >& lhs,
            U* rhs)
    {
        return !(rhs < lhs);
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator<= for lhs = raw pointer, rhs = SharedPtr
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator<=(U* lhs,
            const SharedPtr<T, OP, CP, KP, SP, CNP >& rhs)
    {
        return !(rhs < lhs);
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator>= for lhs = SharedPtr, rhs = raw pointer
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator>=(const SharedPtr<T, OP, CP, KP, SP, CNP >& lhs,
            U* rhs)
    {
        return !(lhs < rhs);
    }

    ////////////////////////////////////////////////////////////////////////////////
    ///  operator>= for lhs = raw pointer, rhs = SharedPtr
    ///  \ingroup SmartPointerGroup
    ////////////////////////////////////////////////////////////////////////////////

    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP,
        typename U>
    inline bool operator>=(U* lhs,
            const SharedPtr<T, OP, CP, KP, SP, CNP >& rhs)
    {
        return !(lhs < rhs);
    }

    template<
        class _Ty1,
        class _Ty2>
    SharedPtr<_Ty1> static_pointer_cast(const SharedPtr<_Ty2>& _Other)
    {   // return shared_ptr object holding static_cast<_Ty1 *>(_Other.get())
        return (SharedPtr<_Ty1>(_Other, _Static_tag()));
    }

    template<
        class _Ty1,
        class _Ty2>
    SharedPtr<_Ty1> const_pointer_cast(const SharedPtr<_Ty2>& _Other)
    {   // return shared_ptr object holding const_cast<_Ty1 *>(_Other.get())
        return (SharedPtr<_Ty1>(_Other, _Const_tag()));
    }

    template<
        class _Ty1,
        class _Ty2>
    SharedPtr<_Ty1> dynamic_pointer_cast(const SharedPtr<_Ty2>& _Other)
    {   // return shared_ptr object holding dynamic_cast<_Ty1 *>(_Other.get())
        return (SharedPtr<_Ty1>(_Other, _Dynamic_tag()));
    }
} // namespace mm

////////////////////////////////////////////////////////////////////////////////
///  specialization of std::less for SharedPtr
///  \ingroup SmartPointerGroup
////////////////////////////////////////////////////////////////////////////////

namespace std
{
    template<
        typename T,
        template<class> class OP,
        class CP,
        template<class> class KP,
        template<class> class SP,
        template<class> class CNP>
    struct less< mm::SharedPtr<T, OP, CP, KP, SP, CNP > >
        : public binary_function<mm::SharedPtr<T, OP, CP, KP, SP, CNP >,
        mm::SharedPtr<T, OP, CP, KP, SP, CNP >, bool>
    {
        bool operator()(const mm::SharedPtr<T, OP, CP, KP, SP, CNP >& lhs,
            const mm::SharedPtr<T, OP, CP, KP, SP, CNP >& rhs) const
        {
            return less<T*>()(GetImpl(lhs), GetImpl(rhs));
        }
    };
}
#endif//__mmSharedPtr_h__
