/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmCheckingPolicy_h__
#define __mmCheckingPolicy_h__

#include "mmStaticCheck.h"

#include <stdexcept>
#include <string>

namespace mm
{
    ////////////////////////////////////////////////////////////////////////////////
    ///  \class NoCheck
    ///
    ///  \ingroup  SmartPointerCheckingGroup
    ///  Implementation of the CheckingPolicy used by SharedPtr
    ///  Well, it's clear what it does :o)
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct NoCheck
    {
        NoCheck()
        {}

        template<class P1>
        NoCheck(const NoCheck<P1>&)
        {}

        static void OnDefault(const P&)
        {}

        static void OnInit(const P&)
        {}

        static void OnDereference(const P&)
        {}

        static void Swap(NoCheck&)
        {}
    };


    ////////////////////////////////////////////////////////////////////////////////
    ///  \class AssertCheck
    ///
    ///  \ingroup  SmartPointerCheckingGroup
    ///  Implementation of the CheckingPolicy used by SharedPtr
    ///  Checks the pointer before dereference
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct AssertCheck
    {
        AssertCheck()
        {}

        template<class P1>
        AssertCheck(const AssertCheck<P1>&)
        {}

        template<class P1>
        AssertCheck(const NoCheck<P1>&)
        {}

        static void OnDefault(const P&)
        {}

        static void OnInit(const P&)
        {}

        static void OnDereference(P val)
        {
            assert(val); (void)val;
        }

        static void Swap(AssertCheck&)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class AssertCheckStrict
    ///
    ///  \ingroup  SmartPointerCheckingGroup
    ///  Implementation of the CheckingPolicy used by SharedPtr
    ///  Checks the pointer against zero upon initialization and before dereference
    ///  You can initialize an AssertCheckStrict with an AssertCheck
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct AssertCheckStrict
    {
        AssertCheckStrict()
        {}

        template<class U>
        AssertCheckStrict(const AssertCheckStrict<U>&)
        {}

        template<class U>
        AssertCheckStrict(const AssertCheck<U>&)
        {}

        template<class P1>
        AssertCheckStrict(const NoCheck<P1>&)
        {}

        static void OnDefault(P val)
        {
            assert(val);
        }

        static void OnInit(P val)
        {
            assert(val);
        }

        static void OnDereference(P val)
        {
            assert(val);
        }

        static void Swap(AssertCheckStrict&)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \struct NullPointerException
    ///
    ///  \ingroup SmartPointerGroup
    ///  Used by some implementations of the CheckingPolicy used by SharedPtr
    ////////////////////////////////////////////////////////////////////////////////

    struct NullPointerException : public std::runtime_error
    {
        NullPointerException() : std::runtime_error(std::string(""))
        { }
        const char* what() const throw()
        {
            return "Null Pointer Exception";
        }
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class RejectNullStatic
    ///
    ///  \ingroup  SmartPointerCheckingGroup
    ///  Implementation of the CheckingPolicy used by SharedPtr
    ///  Checks the pointer upon initialization and before dereference
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct RejectNullStatic
    {
        RejectNullStatic()
        {}

        template<class P1>
        RejectNullStatic(const RejectNullStatic<P1>&)
        {}

        template<class P1>
        RejectNullStatic(const NoCheck<P1>&)
        {}

        template<class P1>
        RejectNullStatic(const AssertCheck<P1>&)
        {}

        template<class P1>
        RejectNullStatic(const AssertCheckStrict<P1>&)
        {}

        static void OnDefault(const P&)
        {
            // Make it depended on template parameter
            static const bool DependedFalse = sizeof(P*) == 0;

            MILK_STATIC_CHECK(DependedFalse, ERROR_This_Policy_Does_Not_Allow_Default_Initialization);
        }

        static void OnInit(const P& val)
        {
            if (!val) throw NullPointerException();
        }

        static void OnDereference(const P& val)
        {
            if (!val) throw NullPointerException();
        }

        static void Swap(RejectNullStatic&)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class RejectNull
    ///
    ///  \ingroup  SmartPointerCheckingGroup
    ///  Implementation of the CheckingPolicy used by SharedPtr
    ///  Checks the pointer before dereference
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct RejectNull
    {
        RejectNull()
        {}

        template<class P1>
        RejectNull(const RejectNull<P1>&)
        {}

        static void OnInit(P)
        {}

        static void OnDefault(P)
        {}

        void OnDereference(P val)
        {
            if (!val) throw NullPointerException();
        }

        void OnDereference(P val) const
        {
            if (!val) throw NullPointerException();
        }

        void Swap(RejectNull&)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class RejectNullStrict
    ///
    ///  \ingroup  SmartPointerCheckingGroup
    ///  Implementation of the CheckingPolicy used by SharedPtr
    ///  Checks the pointer upon initialization and before dereference
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct RejectNullStrict
    {
        RejectNullStrict()
        {}

        template<class P1>
        RejectNullStrict(const RejectNullStrict<P1>&)
        {}

        template<class P1>
        RejectNullStrict(const RejectNull<P1>&)
        {}

        static void OnInit(P val)
        {
            if (!val) throw NullPointerException();
        }

        void OnDereference(P val)
        {
            OnInit(val);
        }

        void OnDereference(P val) const
        {
            OnInit(val);
        }

        void Swap(RejectNullStrict&)
        {}
    };
}
#endif//__mmCheckingPolicy_h__
