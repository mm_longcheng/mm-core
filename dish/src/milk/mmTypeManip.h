/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTypeManip_h__
#define __mmTypeManip_h__

//form loki TypeManip.h

namespace mm
{
    template<int v>
    struct Int2Type
    {
        enum { value = v };
    };

    ////////////////////////////////////////////////////////////////////////////////
    // class template Type2Type
    // Converts each type into a unique, insipid type
    // Invocation Type2Type<T> where T is a type
    // Defines the type OriginalType which maps back to T
    ////////////////////////////////////////////////////////////////////////////////

    template<typename T>
    struct Type2Type
    {
        typedef T OriginalType;
    };

    ////////////////////////////////////////////////////////////////////////////////
    // class template Select
    // Selects one of two types based upon a boolean constant
    // Invocation: Select<flag, T, U>::Result
    // where:
    // flag is a compile-time boolean constant
    // T and U are types
    // Result evaluates to T if flag is true, and to U otherwise.
    ////////////////////////////////////////////////////////////////////////////////

    template<bool flag, typename T, typename U>
    struct Select
    {
        typedef T Result;
    };
    template<typename T, typename U>
    struct Select<false, T, U>
    {
        typedef U Result;
    };
}
#endif//__mmTypeManip_h__
