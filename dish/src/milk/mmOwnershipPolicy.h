/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmOwnershipPolicy_h__
#define __mmOwnershipPolicy_h__

#include "mmSmallObject.h"
#include "mmStaticCheck.h"

#include <assert.h>

//#define LOKI_EXPORT

namespace mm
{
    ////////////////////////////////////////////////////////////////////////////////
    ///  \class RefCounted
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Provides a classic external reference counting implementation
    ////////////////////////////////////////////////////////////////////////////////
    template<class P>
    class RefCountedSmallObject
    {
    public:
        RefCountedSmallObject()
            : pCount_(static_cast<uintptr_t*>(
                SmallObject<>::operator new(sizeof(uintptr_t))))
        {
            assert(pCount_ != 0);
            *pCount_ = 1;
        }

        RefCountedSmallObject(const RefCountedSmallObject& rhs)
            : pCount_(rhs.pCount_)
        {}

        // MWCW lacks template friends, hence the following kludge
        template<typename P1>
        RefCountedSmallObject(const RefCountedSmallObject<P1>& rhs)
            : pCount_(reinterpret_cast<const RefCountedSmallObject&>(rhs).pCount_)
        {}

        P Clone(const P& val)
        {
            ++*pCount_;
            return val;
        }

        bool Release(const P&)
        {
            if (!--*pCount_)
            {
                SmallObject<>::operator delete(pCount_, sizeof(uintptr_t));
                pCount_ = NULL;
                return true;
            }
            return false;
        }

        void Swap(RefCountedSmallObject& rhs)
        {
            std::swap(pCount_, rhs.pCount_);
        }

        enum { destructiveCopy = false };

    private:
        // Data
        uintptr_t * pCount_;
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class RefCounted
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Provides a classic external reference counting implementation
    ////////////////////////////////////////////////////////////////////////////////
    template<class P>
    class RefCounted
    {
    public:
        RefCounted()
            :pCount_(new uintptr_t)
        {
            assert(pCount_ != 0);
            *pCount_ = 1;
        }

        RefCounted(const RefCounted& rhs)
            : pCount_(rhs.pCount_)
        {}

        // MWCW lacks template friends, hence the following kludge
        template<typename P1>
        RefCounted(const RefCounted<P1>& rhs)
            : pCount_(reinterpret_cast<const RefCounted&>(rhs).pCount_)
        {}

        P Clone(const P& val)
        {
            ++*pCount_;
            return val;
        }

        bool Release(const P&)
        {
            if (!--*pCount_)
            {
                delete pCount_;
                pCount_ = NULL;
                return true;
            }
            return false;
        }

        void Swap(RefCounted& rhs)
        {
            std::swap(pCount_, rhs.pCount_);
        }

        enum { destructiveCopy = false };

    private:
        // Data
        uintptr_t * pCount_;
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \struct RefCountedMT
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Implements external reference counting for multithreaded programs
    ///  Policy Usage: RefCountedMTAdj<ThreadingModel>::RefCountedMT
    ///
    ///  \par Warning
    ///  There could be a race condition, see bug "Race condition in RefCountedMTAdj::Release"
    ///  http://sourceforge.net/tracker/index.php?func=detail&aid=1408845&group_id=29557&atid=396644
    ///  As stated in bug 1408845, the Release function is not thread safe if a
    ///  SharedPtr copy-constructor tries to copy the last pointer to an object in
    ///  one thread, while the destructor is acting on the last pointer in another
    ///  thread.  The existence of a race between a copy-constructor and destructor
    ///  implies a design flaw at a higher level.  That race condition must be
    ///  fixed at a higher design level, and no change to this class could fix it.
    ////////////////////////////////////////////////////////////////////////////////

        //template<template<class, class> class ThreadingModel,
        //          class MX = LOKI_DEFAULT_MUTEX >
        //struct RefCountedMTAdj
        //{
        //    template<class P>
        //    class RefCountedMT : public ThreadingModel< RefCountedMT<P>, MX >
        //    {
        //        typedef ThreadingModel< RefCountedMT<P>, MX > base_type;
        //        typedef typename base_type::IntType       CountType;
        //        typedef volatile CountType               *CountPtrType;

        //    public:
        //        RefCountedMT()
        //        {
        //            pCount_ = static_cast<CountPtrType>(
        //                SmallObject<LOKI_DEFAULT_THREADING_NO_OBJ_LEVEL>::operator new(
        //                    sizeof(*pCount_)));
        //            assert(pCount_);
        //            //*pCount_ = 1;
        //            ThreadingModel<RefCountedMT, MX>::AtomicAssign(*pCount_, 1);
        //        }

        //        RefCountedMT(const RefCountedMT& rhs)
        //        : pCount_(rhs.pCount_)
        //        {}

        //        //MWCW lacks template friends, hence the following kludge
        //        template<typename P1>
        //        RefCountedMT(const RefCountedMT<P1>& rhs)
        //        : pCount_(reinterpret_cast<const RefCountedMT<P>&>(rhs).pCount_)
        //        {}

        //        P Clone(const P& val)
        //        {
        //            ThreadingModel<RefCountedMT, MX>::AtomicIncrement(*pCount_);
        //            return val;
        //        }

        //        bool Release(const P&)
        //        {
        //            bool isZero = false;
        //            ThreadingModel< RefCountedMT, MX >::AtomicDecrement( *pCount_, 0, isZero );
        //            if ( isZero )
        //            {
        //                SmallObject<LOKI_DEFAULT_THREADING_NO_OBJ_LEVEL>::operator delete(
        //                    const_cast<CountType *>(pCount_),
        //                    sizeof(*pCount_));
        //                return true;
        //            }
        //            return false;
        //        }

        //        void Swap(RefCountedMT& rhs)
        //        { std::swap(pCount_, rhs.pCount_); }

        //        enum { destructiveCopy = false };

        //    private:
        //        // Data
        //        CountPtrType pCount_;
        //    };
        //};

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class COMRefCounted
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Adapts COM intrusive reference counting to OwnershipPolicy-specific syntax
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    class COMRefCounted
    {
    public:
        COMRefCounted()
        {}

        template<class U>
        COMRefCounted(const COMRefCounted<U>&)
        {}

        static P Clone(const P& val)
        {
            if (val != 0)
                val->AddRef();
            return val;
        }

        static bool Release(const P& val)
        {
            if (val != 0)
                val->Release();
            return false;
        }

        enum { destructiveCopy = false };

        static void Swap(COMRefCounted&)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class DeepCopy
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Implements deep copy semantics, assumes existence of a Clone() member
    ///  function of the pointee type
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    struct DeepCopy
    {
        DeepCopy()
        {}

        template<class P1>
        DeepCopy(const DeepCopy<P1>&)
        {}

        static P Clone(const P& val)
        {
            return val->Clone();
        }

        static bool Release(const P&)
        {
            return true;
        }

        static void Swap(DeepCopy&)
        {}

        enum { destructiveCopy = false };
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class RefLinked
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Implements reference linking
    ////////////////////////////////////////////////////////////////////////////////

        //namespace Private
        //{
        //    class LOKI_EXPORT RefLinkedBase
        //    {
        //    public:
        //        RefLinkedBase()
        //        { prev_ = next_ = this; }

        //        RefLinkedBase(const RefLinkedBase& rhs);

        //        bool Release();

        //        void Swap(RefLinkedBase& rhs);

        //        bool Merge( RefLinkedBase & rhs );

        //        enum { destructiveCopy = false };

        //    private:
        //        static unsigned int CountPrevCycle( const RefLinkedBase * pThis );
        //        static unsigned int CountNextCycle( const RefLinkedBase * pThis );
        //        bool HasPrevNode( const RefLinkedBase * p ) const;
        //        bool HasNextNode( const RefLinkedBase * p ) const;

        //        mutable const RefLinkedBase* prev_;
        //        mutable const RefLinkedBase* next_;
        //    };
        //}

        //template<class P>
        //class RefLinked : public Private::RefLinkedBase
        //{
        //public:
        //    RefLinked()
        //    {}

        //    template<class P1>
        //    RefLinked(const RefLinked<P1>& rhs)
        //    : Private::RefLinkedBase(rhs)
        //    {}

        //    static P Clone(const P& val)
        //    { return val; }

        //    bool Release(const P&)
        //    { return Private::RefLinkedBase::Release(); }

        //    template< class P1 >
        //    bool Merge( RefLinked< P1 > & rhs )
        //    {
        //        return Private::RefLinkedBase::Merge( rhs );
        //    }
        //};

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class DestructiveCopy
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Implements destructive copy semantics (a la std::auto_ptr)
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    class DestructiveCopy
    {
    public:
        DestructiveCopy()
        {}

        template<class P1>
        DestructiveCopy(const DestructiveCopy<P1>&)
        {}

        template<class P1>
        static P Clone(P1& val)
        {
            P result(val);
            val = P1();
            return result;
        }

        static bool Release(const P&)
        {
            return true;
        }

        static void Swap(DestructiveCopy&)
        {}

        enum { destructiveCopy = true };
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class NoCopy
    ///
    ///  \ingroup  SmartPointerOwnershipGroup
    ///  Implementation of the OwnershipPolicy used by SharedPtr
    ///  Implements a policy that doesn't allow copying objects
    ////////////////////////////////////////////////////////////////////////////////

    template<class P>
    class NoCopy
    {
    public:
        NoCopy()
        {}

        template<class P1>
        NoCopy(const NoCopy<P1>&)
        {}

        static P Clone(const P&)
        {
            // Make it depended on template parameter
            static const bool DependedFalse = sizeof(P*) == 0;

            MILK_STATIC_CHECK(DependedFalse, This_Policy_Disallows_Value_Copying);
        }

        static bool Release(const P&)
        {
            return true;
        }

        static void Swap(NoCopy&)
        {}

        enum { destructiveCopy = false };
    };
}
#endif//__mmOwnershipPolicy_h__
