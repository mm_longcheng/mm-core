/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmSingletonHolder_h__
#define __mmSingletonHolder_h__

#include "core/mmPlatform.h"

#include <cassert>
#include <stdexcept>
#include <cstdlib>

#ifdef _MSC_VER
#define C_CALLING_CONVENTION_QUALIFIER __cdecl 
#else
#define C_CALLING_CONVENTION_QUALIFIER 
#endif//_MSC_VER

namespace mm
{
    ////////////////////////////////////////////////////////////////////////////////
    ///  \class CreateUsingNew
    ///
    ///  \ingroup CreationGroup
    ///  Implementation of the CreationPolicy used by SingletonHolder
    ///  Creates objects using a straight call to the new operator 
    ////////////////////////////////////////////////////////////////////////////////
    template<class T> struct CreateUsingNew
    {
        static T* Create()
        {
            return new T;
        }

        static void Destroy(T* p)
        {
            delete p;
        }
    };

    typedef void (C_CALLING_CONVENTION_QUALIFIER *atexit_pfn_t)();

    ////////////////////////////////////////////////////////////////////////////////
    ///  \class DefaultLifetime
    ///
    ///  \ingroup LifetimeGroup
    ///  Implementation of the LifetimePolicy used by SingletonHolder
    ///  Schedules an object's destruction as per C++ rules
    ///  Forwards to std::atexit
    ////////////////////////////////////////////////////////////////////////////////
    template<class T>
    struct DefaultLifetime
    {
        static void ScheduleDestruction(T*, atexit_pfn_t pFun)
        {
            std::atexit(pFun);
        }

        static void OnDeadReference()
        {
            throw std::logic_error("Dead Reference Detected");
        }
    };
    template
    <
        typename T,
        template<class> class CreationPolicy = CreateUsingNew,
        template<class> class LifetimePolicy = DefaultLifetime
    >
    class SingletonHolder
    {
    public:
        typedef T ObjectType;
        static T& Instance();
    protected:
    private:
        // Helpers
        static void MakeInstance();
        static void C_CALLING_CONVENTION_QUALIFIER DestroySingleton();

        // Protection
        SingletonHolder();

        // Data
        typedef T* PtrInstanceType;
        static PtrInstanceType pInstance_;
        static bool destroyed_;
    };
    //  // we not open this default.
    //#ifdef MILK_USE_STATIC_LIB
        ////////////////////////////////////////////////////////////////////////////////
        // SingletonHolder::pInstance_
        ////////////////////////////////////////////////////////////////////////////////
    template
    <
        typename T,
        template<class> class C,
        template<class> class L
    >
    typename SingletonHolder<T, C, L>::PtrInstanceType
    SingletonHolder<T, C, L>::pInstance_ = 0;
    ////////////////////////////////////////////////////////////////////////////////
    // SingletonHolder::destroyed_
    ////////////////////////////////////////////////////////////////////////////////
    template
    <
        typename T,
        template<class> class C,
        template<class> class L
    >
    bool SingletonHolder<T, C, L>::destroyed_ = false;
    //#endif
        ////////////////////////////////////////////////////////////////////////////////
        // SingletonHolder::Instance
        ////////////////////////////////////////////////////////////////////////////////

    template
    <
        typename T,
        template<class> class C,
        template<class> class L
    >
    inline T& SingletonHolder<T, C, L>::Instance()
    {
        if (!pInstance_)
        {
            MakeInstance();
        }
        return *pInstance_;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // SingletonHolder::MakeInstance (helper for Instance)
    ////////////////////////////////////////////////////////////////////////////////

    template
    <
        typename T,
        template<class> class CreationPolicy,
        template<class> class LifetimePolicy
    >
    void SingletonHolder<T, CreationPolicy, LifetimePolicy>::MakeInstance()
    {
        if (!pInstance_)
        {
            if (destroyed_)
            {
                destroyed_ = false;
                LifetimePolicy<T>::OnDeadReference();
            }
            pInstance_ = CreationPolicy<T>::Create();
            LifetimePolicy<T>::ScheduleDestruction(pInstance_, &DestroySingleton);
        }
    }

    template
    <
        typename T,
        template<class> class CreationPolicy,
        template<class> class L
    >
    void C_CALLING_CONVENTION_QUALIFIER
        SingletonHolder<T, CreationPolicy, L>::DestroySingleton()
    {
        assert(!destroyed_);
        CreationPolicy<T>::Destroy(pInstance_);
        pInstance_ = 0;
        destroyed_ = true;
    }
    template<class T>
    class MM_EXPORT_DISH Singleton
    {
    public:
        static T& Instance();
    };
}

/// \def MM_SINGLETON_INSTANCE_DEFINITION(SHOLDER)
/// Convenience macro for the definition of the static Instance member function
/// Put this macro called with a SingletonHolder typedef into your cpp file.

#define MILK_SINGLETON_INSTANCE_DEFINITION(SHOLDER)                     \
namespace mm                                                          \
{                                                                       \
    template<>                                                          \
    SHOLDER::ObjectType& Singleton<SHOLDER::ObjectType>::Instance()     \
    {                                                                   \
        return SHOLDER::Instance();                                     \
    }                                                                   \
}

#endif//__mmSingletonHolder_h__
