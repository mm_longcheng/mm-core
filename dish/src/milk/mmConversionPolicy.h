/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmConversionPolicy_h__
#define __mmConversionPolicy_h__

namespace mm
{
    ////////////////////////////////////////////////////////////////////////////////
    ///  \struct AllowConversion
    ///
    ///  \ingroup  SmartPointerConversionGroup
    ///  Implementation of the ConversionPolicy used by SharedPtr
    ///  Allows implicit conversion from SharedPtr to the pointee type
    ////////////////////////////////////////////////////////////////////////////////

    struct AllowConversion
    {
        enum { allow = true };

        void Swap(AllowConversion&)
        {}
    };

    ////////////////////////////////////////////////////////////////////////////////
    ///  \struct DisallowConversion
    ///
    ///  \ingroup  SmartPointerConversionGroup
    ///  Implementation of the ConversionPolicy used by SharedPtr
    ///  Does not allow implicit conversion from SharedPtr to the pointee type
    ///  You can initialize a DisallowConversion with an AllowConversion
    ////////////////////////////////////////////////////////////////////////////////

    struct DisallowConversion
    {
        DisallowConversion()
        {}

        DisallowConversion(const AllowConversion&)
        {}

        enum { allow = false };

        void Swap(DisallowConversion&)
        {}
    };
}
#endif//__mmConversionPolicy_h__
