/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFunctor_h__
#define __mmFunctor_h__

#include <stdexcept>
#include <memory>
#include <list>
#include <assert.h>
#include <typeinfo>

#include "mmTypeSeq.h"

//Simplify from Loki,a temp code.Parameter must less than 2!!
//You can use it by one to be in Parameter and one to be out Parameter.
//Why only two parameter?
//You can define a event struct contain lost of other parameter.
//Special: 
//  If you want the in Parameter is const and out Parameter is none const.
//
//Note:No type traits here!

namespace mm
{
    namespace Private
    {
        template<typename R>
        struct FunctorImplBase
        {
            typedef R ResultType;
            typedef FunctorImplBase<R> FunctorImplBaseType;

            typedef EmptyType Parm1;
            typedef EmptyType Parm2;


            virtual ~FunctorImplBase()
            {}

            virtual FunctorImplBase* DoClone() const = 0;

            template<class U>
            static U* Clone(U* pObj)
            {
                if (!pObj) return 0;
                U* pClone = static_cast<U*>(pObj->DoClone());
                assert(typeid(*pClone) == typeid(*pObj));
                return pClone;
            }

            virtual bool operator==(const FunctorImplBase&) const = 0;

        };
    }
#define DEFINE_CLONE_FUNCTORIMPL(Cls) \
    virtual Cls* DoClone() const { return new Cls(*this); }

    template<typename R, class TList>
    class FunctorImpl;

    template<typename R>
    class FunctorImpl<R, NullType>
        : public Private::FunctorImplBase<R>
    {
    public:
        typedef R ResultType;
        virtual R operator()() = 0;
    };

    template<typename R, typename P1>
    class FunctorImpl<R, Seq<P1> >
        : public Private::FunctorImplBase<R>
    {
    public:
        typedef R ResultType;
        typedef P1 Parm1;
        virtual R operator()(Parm1) = 0;
    };

    template<typename R, typename P1, typename P2>
    class FunctorImpl<R, Seq<P1, P2> >
        : public Private::FunctorImplBase<R>
    {
    public:
        typedef R ResultType;
        typedef P1 Parm1;
        typedef P2 Parm2;
        virtual R operator()(Parm1, Parm2) = 0;
    };

    template<class ParentFunctor, typename Fun>
    class FunctorHandler
        : public ParentFunctor::Impl
    {
        typedef typename ParentFunctor::Impl Base;
    public:
        typedef typename Base::ResultType ResultType;
        typedef typename Base::Parm1 Parm1;
        typedef typename Base::Parm2 Parm2;
        //
        FunctorHandler(const Fun& fun) : f_(fun) {}

        DEFINE_CLONE_FUNCTORIMPL(FunctorHandler);

        bool operator==(const typename Base::FunctorImplBaseType& rhs) const
        {
            // there is no static information if Functor holds a member function 
            // or a free function; this is the main difference to tr1::function
            if (typeid(*this) != typeid(rhs))
                return false; // cannot be equal

            const FunctorHandler& fh = static_cast<const FunctorHandler&>(rhs);
            // if this line gives a compiler error, you are using a function object.
            // you need to implement bool MyFnObj::operator == (const MyFnObj&) const;
            return  f_ == fh.f_;
        }
        //
        ResultType operator()()
        {
            return f_();
        }

        ResultType operator()(Parm1 p1)
        {
            return f_(p1);
        }

        ResultType operator()(Parm1 p1, Parm2 p2)
        {
            return f_(p1, p2);
        }
    private:
        Fun f_;
    };
    template<class ParentFunctor, typename PointerToObj,
        typename PointerToMemFn>
        class MemFunHandler : public ParentFunctor::Impl
    {
        typedef typename ParentFunctor::Impl Base;
    public:
        typedef typename Base::ResultType ResultType;
        typedef typename Base::Parm1 Parm1;
        typedef typename Base::Parm2 Parm2;

        MemFunHandler(const PointerToObj& pObj, PointerToMemFn pMemFn)
            : pObj_(pObj), pMemFn_(pMemFn)
        {}

        DEFINE_CLONE_FUNCTORIMPL(MemFunHandler);

        bool operator==(const typename Base::FunctorImplBaseType& rhs) const
        {
            if (typeid(*this) != typeid(rhs))
                return false; // cannot be equal 

            const MemFunHandler& mfh = static_cast<const MemFunHandler&>(rhs);
            // if this line gives a compiler error, you are using a function object.
            // you need to implement bool MyFnObj::operator == (const MyFnObj&) const;
            return  pObj_ == mfh.pObj_ && pMemFn_ == mfh.pMemFn_;
        }

        ResultType operator()()
        {
            return ((*pObj_).*pMemFn_)();
        }

        ResultType operator()(Parm1 p1)
        {
            return ((*pObj_).*pMemFn_)(p1);
        }

        ResultType operator()(Parm1 p1, Parm2 p2)
        {
            return ((*pObj_).*pMemFn_)(p1, p2);
        }
    private:
        PointerToObj pObj_;
        PointerToMemFn pMemFn_;
    };

    class bad_function_call : public std::runtime_error
    {
    public:
        bad_function_call() : std::runtime_error("bad_function_call in Functor")
        {}
    };

#define FUNCTION_THROW_BAD_FUNCTION_CALL if(empty()) throw bad_function_call();

    template<typename R = void, class TList = NullType >
    class Functor
    {
    public:
        // Handy type definitions for the body type
        typedef FunctorImpl<R, TList> Impl;
        typedef R ResultType;
        typedef TList ParmList;
        typedef typename Impl::Parm1 Parm1;
        typedef typename Impl::Parm2 Parm2;

        // Member functions

        Functor() : spImpl_(0)
        {}

        Functor(const Functor& rhs) : spImpl_(Impl::Clone(rhs.spImpl_.get()))
        {}

        Functor(std::auto_ptr<Impl> spImpl) : spImpl_(spImpl)
        {}

        template<typename Fun>
        Functor(Fun fun)
            : spImpl_(new FunctorHandler<Functor, Fun>(fun))
        {}

        template<class PtrObj, typename MemFn>
        Functor(const PtrObj& p, MemFn memFn)
            : spImpl_(new MemFunHandler<Functor, PtrObj, MemFn>(p, memFn))
        {}

        typedef Impl * (std::auto_ptr<Impl>::*unspecified_bool_type)() const;

        operator unspecified_bool_type() const
        {
            return spImpl_.get() ? &std::auto_ptr<Impl>::get : 0;
        }

        Functor& operator=(const Functor& rhs)
        {
            Functor copy(rhs);
            // swap auto_ptrs by hand
            Impl* p = spImpl_.release();
            spImpl_.reset(copy.spImpl_.release());
            copy.spImpl_.reset(p);
            return *this;
        }

        bool empty() const
        {
            return spImpl_.get() == 0;
        }

        void clear()
        {
            spImpl_.reset(0);
        }

        bool operator==(const Functor& rhs) const
        {
            if (spImpl_.get() == 0 && rhs.spImpl_.get() == 0)
                return true;
            if (spImpl_.get() != 0 && rhs.spImpl_.get() != 0)
                return *spImpl_.get() == *rhs.spImpl_.get();
            else
                return false;
        }

        bool operator!=(const Functor& rhs) const
        {
            return !(*this == rhs);
        }

        // operator() implementations for up to 2 arguments

        ResultType operator()() const
        {
            FUNCTION_THROW_BAD_FUNCTION_CALL
                return (*spImpl_)();
        }

        ResultType operator()(Parm1 p1) const
        {
            FUNCTION_THROW_BAD_FUNCTION_CALL
                return (*spImpl_)(p1);
        }

        ResultType operator()(Parm1 p1, Parm2 p2) const
        {
            FUNCTION_THROW_BAD_FUNCTION_CALL
                return (*spImpl_)(p1, p2);
        }
    private:
        std::auto_ptr<Impl> spImpl_;
    };
    ////////////////////////////////////////////////////////////////////////////////
    ///  \class Function
    ///
    ///  \ingroup FunctorGroup
    ///  Allows a boost/TR1 like usage of Functor.
    /// 
    ///  \par Usage
    ///
    ///      - free functions: e.g.  \code Function<int(int,int)> f(&freeFunction);
    ///                              \endcode
    ///      - member functions: e.g \code Function<int()> f(&object,&ObjectType::memberFunction); 
    ///                              \endcode
    ///
    ///  see also test/Function/FunctionTest.cpp (the modified test program from boost)
    ////////////////////////////////////////////////////////////////////////////////

    template<class R = void()>
    struct Function;


    template<class R>
    struct Function<R()> : public Functor<R>
    {
        typedef Functor<R> FBase;

        Function() : FBase() {}

        Function(const Function& func) : FBase()
        {
            if (!func.empty())
                FBase::operator=(func);
        }

        // test on emptiness
        template<class R2>
        Function(Function<R2()> func) : FBase()
        {
            if (!func.empty())
                FBase::operator=(func);
        }

        // clear  by '= 0'
        Function(const int i) : FBase()
        {
            if (i == 0)
                FBase::clear();
            else
                throw std::runtime_error("mm::Function(const int i): i!=0");
        }

        template<class Func>
        Function(Func func) : FBase(func) {}

        template<class Host, class Func>
        Function(const Host& host, const Func& func) : FBase(host, func) {}

    };


    ////////////////////////////////////////////////////////////////////////////////
    // macros for the repetitions
    ////////////////////////////////////////////////////////////////////////////////

#define FUNCTION_BODY                                                       \
                                                                            \
    Function() : FBase() {}                                                 \
                                                                            \
    Function(const Function& func) : FBase()                                \
    {                                                                       \
        if( !func.empty())                                                  \
            FBase::operator=(func);                                         \
    }                                                                       \
                                                                            \
    Function(const int i) : FBase()                                         \
    {                                                                       \
        if(i==0)                                                            \
            FBase::clear();                                                 \
        else                                                                \
            throw std::runtime_error("mm::Function(const int i): i!=0");  \
    }                                                                       \
                                                                            \
    template<class Func>                                                    \
    Function(Func func) : FBase(func) {}                                    \
                                                                            \
    template<class Host, class Func>                                        \
    Function(const Host& host, const Func& func): FBase(host,func) {}


#define FUNCTION_R2_CTOR_BODY           \
    : FBase()                           \
    {                                   \
        if(!func.empty())               \
            FBase::operator=(func);     \
    }


    ////////////////////////////////////////////////////////////////////////////////
    // repetitions
    ////////////////////////////////////////////////////////////////////////////////

    template<>
    struct Function<>
        : public Functor<>
    {
        typedef Functor<> FBase;

        template<class R2>
        Function(Function<R2()> func)
            FUNCTION_R2_CTOR_BODY

            FUNCTION_BODY // if compilation breaks here then 
            // Function.h was not included before
            // Functor.h, check your include order
            // or define Milk_ENABLE_FUNCTION 
    };

    template<class R, class P01>
    struct Function<R(P01)>
        : public Functor<R, Seq<P01> >
    {
        typedef Functor<R, Seq<P01> > FBase;

        template<class R2, class Q01>
        Function(Function<R2(Q01)> func)
            FUNCTION_R2_CTOR_BODY

            FUNCTION_BODY
    };

    template<class R, class P01, class P02>
    struct Function<R(P01, P02)>
        : public Functor<R, Seq<P01, P02> >
    {
        typedef Functor<R, Seq<P01, P02> > FBase;

        template<class R2, class Q01, class Q02>
        Function(Function<R2(Q01, Q02)> func)
            FUNCTION_R2_CTOR_BODY

            FUNCTION_BODY
    };
#undef FUNCTION_R2_CTOR_BODY
#undef FUNCTION_BODY
    ////////////////////////////////////////////////////////////////////////////////
    ///  \class FunctorList
    ///
    ///  \ingroup FunctorGroup
    ///  Allows a boost/TR1 like usage of Functor.
    /// 
    ///  \par Usage
    ///
    ///      - free functions: e.g.  \code FunctorList<int(int,int)> f.push_pack(&freeFunction);
    ///                              \endcode
    ///      - member functions: e.g \code FunctorList<int()> f.push_pack(&object,&ObjectType::memberFunction); 
    ///                              \endcode
    ///
    ////////////////////////////////////////////////////////////////////////////////
    /// \You can remove some function unit at function call.
    /// \But could not delete current FunctorList immediately!!!
    /// \You must be careful to use call back methods.
    template<class R, class TList = NullType>
    class FunctorList
    {
    public:
        // Handy type definitions for the body type
        typedef Functor<R, TList > Impl;
        typedef R ResultType;
        typedef TList ParmList;
        typedef typename Impl::Parm1 Parm1;
        typedef typename Impl::Parm2 Parm2;

        bool empty() const
        {
            return fList.empty();
        }

        //push_back use Functor
        void push_back(const Impl& f)
        {
            fList.push_back(f);
        }
        //push_back use static fun
        template<typename Fun>
        void push_back(Fun fun)
        {
            fList.push_back(Impl(fun));
        }
        //push_back use member fun
        template<class PtrObj, typename MemFn>
        void push_back(const PtrObj& p, MemFn memFn)
        {
            fList.push_back(Impl(p, memFn));
        }

        //push_front use Functor
        void push_front(const Impl& f)
        {
            fList.push_front(f);
        }
        //push_front use static fun
        template<typename Fun>
        void push_front(Fun fun)
        {
            fList.push_front(Impl(fun));
        }
        //push_front use member fun
        template<class PtrObj, typename MemFn>
        void push_front(const PtrObj& p, MemFn memFn)
        {
            fList.push_front(Impl(p, memFn));
        }

        //remove use Functor
        void remove(const Impl& f)
        {
            for (typename FunList::iterator it = fList.begin(); it != fList.end(); it++)
            {
                if (f == *it)
                {
                    it->clear();
                }
            }
        }
        //remove use static fun
        template<typename Fun>
        void remove(Fun fun)
        {
            Impl f(fun);
            for (typename FunList::iterator it = fList.begin(); it != fList.end(); it++)
            {
                if (f == *it)
                {
                    it->clear();
                }
            }
        }
        //remove use member fun
        template<class PtrObj, typename MemFn>
        void remove(const PtrObj& p, MemFn memFn)
        {
            Impl f(p, memFn);
            for (typename FunList::iterator it = fList.begin(); it != fList.end(); it++)
            {
                if (f == *it)
                {
                    it->clear();
                }
            }
        }

        void clear()
        {
            fList.clear();
        }

        // operator() implementations for up to 2 arguments

        ResultType operator()()
        {
            typename FunList::iterator it = fList.begin();
            while (it != fList.end())
            {
                if (it->empty())
                {
                    fList.erase(it++);
                    continue;
                }
                (*it)();
                it++;
            }
        }

        ResultType operator()(Parm1 p1)
        {
            typename FunList::iterator it = fList.begin();
            while (it != fList.end())
            {
                if (it->empty())
                {
                    fList.erase(it++);
                    continue;
                }
                (*it)(p1);
                it++;
            }
        }

        ResultType operator()(Parm1 p1, Parm2 p2)
        {
            typename FunList::iterator it = fList.begin();
            while (it != fList.end())
            {
                if (it->empty())
                {
                    fList.erase(it++);
                    continue;
                }
                (*it)(p1, p2);
                it++;
            }
        }
    private:
        typedef std::list<Impl> FunList;
        FunList fList;
    };
    //
    template< class R = void() >
    struct FunctionList;

    template< class R >
    struct FunctionList<R()> : public FunctorList<R> {};

    template< class R, class P01>
    struct FunctionList<R(P01)> : public FunctorList< R, Seq<P01> > {};

    template< class R, class P01, class P02>
    struct FunctionList<R(P01, P02)> : public FunctorList< R, Seq<P01, P02> > {};
}
#endif//__mmFunctor_h__
