/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmUnitFactory_h__
#define __mmUnitFactory_h__

#include "mmFactory.h"

namespace mm
{
    //////////////////////////////////////////////////////////////////////////
    template<typename _T>
    struct DumyCreatorType
    {
        typedef _T ElemType;
        template<typename T>
        static T* CreateUnit()
        {
            assert(false && "Call a DumyCreatorType create function!");
            return NULL;
        }
        static void DestroyUnit(ElemType* _obj)
        {
            assert(false && "Call a DumyCreatorType destroy function!");
        }
    };
    template<typename _T>
    struct UseNewCreatorType
    {
        typedef _T UnitType;
        template<typename T>
        static T* CreateUnit()
        {
            return new T;
        }
        static void DestroyUnit(UnitType* _obj)
        {
            delete _obj;
        }
    };
    //////////////////////////////////////////////////////////////////////////
    template<
        typename id_type_,
        typename UnitType_,
        typename CreationPolicy_ = UseNewCreatorType<UnitType_>,
        template<typename, class> class FactoryError_ = DefaultFactoryError>
    class UnitFactory : public FactoryImpl<UnitType_, id_type_, NullType>
    {
    public:
        typedef UnitType_ UnitType;
        typedef id_type_ IdType;
        typedef CreationPolicy_ CreationPolicy;
        typedef NullType TypeList;

        typedef Factory<UnitType, IdType, TypeList, FactoryError_> FactoryType;
    public:
        UnitFactory()
        {

        }
        virtual ~UnitFactory()
        {

        }
        FactoryType* GetFactory() { return &mFactory; }
        //
        virtual UnitType* CreateObject(const IdType& id)
        {
            return mFactory->CreateObject(id);
        }
        virtual void DestroyObject(UnitType* _obj)
        {
            CreationPolicy::DestroyUnit(_obj);
        }
        template<typename T>
        void Register(const IdType& id)
        {
            typedef Function<T*(void)> CreatorType;
            mFactory->Register(id, CreatorType(&CreationPolicy::template CreateUnit<T>));
        }
        void Unregister(const IdType& id)
        {
            mFactory->Unregister(id);
        }
    private:
        FactoryType mFactory;//strong ref.
    };
}
#endif//__mmUnitFactory_h__
