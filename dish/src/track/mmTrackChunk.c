/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTrackChunk.h"
#include "mmTrackAssets.h"

#include "core/mmByteOrder.h"
#include "core/mmByteswap.h"
#include "core/mmLimit.h"

static void __static_mmTrackChunk_ByteOrderFileLittleDecode(struct mmTrackChunk* p, long* offset, mmUInt32_t* v);
static void __static_mmTrackChunk_ByteOrderFileLittleEncode(struct mmTrackChunk* p, long* offset, mmUInt32_t* v);

static void __static_mmTrackChunk_InsertDynamic(struct mmTrackChunk* p, mmUInt32_t index, mmUInt8_t* buffer, mmUInt32_t length);
static void __static_mmTrackChunk_InsertRegular(struct mmTrackChunk* p, mmUInt32_t index, mmUInt8_t* buffer, mmUInt32_t length);
static void __static_mmTrackChunk_SelectDynamic(struct mmTrackChunk* p, mmUInt32_t b, mmUInt32_t e, mmTrackChunkSelectFunc handle, void* v);
static void __static_mmTrackChunk_SelectRegular(struct mmTrackChunk* p, mmUInt32_t b, mmUInt32_t e, mmTrackChunkSelectFunc handle, void* v);

MM_EXPORT_TRACK void mmTrackChunkHeader_Init(struct mmTrackChunkHeader* p)
{
    p->chunk_id = 0;
    p->index = 0;
    p->number = 0;
}
MM_EXPORT_TRACK void mmTrackChunkHeader_Destroy(struct mmTrackChunkHeader* p)
{
    p->chunk_id = 0;
    p->index = 0;
    p->number = 0;
}
MM_EXPORT_TRACK void mmTrackChunkHeader_Reset(struct mmTrackChunkHeader* p)
{
    p->chunk_id = 0;
    p->index = 0;
    p->number = 0;
}

MM_EXPORT_TRACK void mmTrackChunk_Init(struct mmTrackChunk* p)
{
    mmTrackChunkHeader_Init(&p->header);

    p->assets = NULL;

    p->pptr_head = 0;
    p->pptr_body = p->pptr_head + MM_TRACK_CHUNK_HEAD_SIZE;
    p->pptr_last = p->pptr_head + MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT;
    p->pptr_idxe = p->pptr_head + MM_TRACK_CHUNK_OFFSET_INDEX_E;

    p->offset_prev = 0;
    p->free_space = (mmUInt32_t)(p->pptr_last - p->pptr_body);

    p->insert = &__static_mmTrackChunk_InsertDynamic;
    p->select = &__static_mmTrackChunk_SelectDynamic;
}
MM_EXPORT_TRACK void mmTrackChunk_Destroy(struct mmTrackChunk* p)
{
    mmTrackChunkHeader_Destroy(&p->header);

    p->assets = NULL;

    p->pptr_head = 0;
    p->pptr_body = p->pptr_head + MM_TRACK_CHUNK_HEAD_SIZE;
    p->pptr_last = p->pptr_head + MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT;
    p->pptr_idxe = p->pptr_head + MM_TRACK_CHUNK_OFFSET_INDEX_E;

    p->offset_prev = 0;
    p->free_space = (mmUInt32_t)(p->pptr_last - p->pptr_body);

    p->insert = &__static_mmTrackChunk_InsertDynamic;
    p->select = &__static_mmTrackChunk_SelectDynamic;
}

MM_EXPORT_TRACK void mmTrackChunk_Reset(struct mmTrackChunk* p)
{
    mmTrackChunkHeader_Reset(&p->header);

    p->assets = NULL;

    p->pptr_head = 0;
    p->pptr_body = p->pptr_head + MM_TRACK_CHUNK_HEAD_SIZE;
    p->pptr_last = p->pptr_head + MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT;
    p->pptr_idxe = p->pptr_head + MM_TRACK_CHUNK_OFFSET_INDEX_E;

    p->offset_prev = 0;
    p->free_space = (mmUInt32_t)(p->pptr_last - p->pptr_body);

    p->insert = &__static_mmTrackChunk_InsertDynamic;
    p->select = &__static_mmTrackChunk_SelectDynamic;
}

MM_EXPORT_TRACK void mmTrackChunk_SetAssets(struct mmTrackChunk* p, struct mmTrackAssets* assets)
{
    p->assets = assets;
}
MM_EXPORT_TRACK void mmTrackChunk_SetChunkId(struct mmTrackChunk* p, mmUInt32_t chunk_id)
{
    p->header.chunk_id = chunk_id;
}
MM_EXPORT_TRACK void mmTrackChunk_SetIndex(struct mmTrackChunk* p, mmUInt32_t index)
{
    p->header.index = index;
}
MM_EXPORT_TRACK void mmTrackChunk_SetNumber(struct mmTrackChunk* p, mmUInt32_t number)
{
    p->header.number = number;
}

MM_EXPORT_TRACK void mmTrackChunk_EncodeHeader(struct mmTrackChunk* p)
{
    struct mmTrackAssets* assets = p->assets;

    struct mmTrackChunkHeader* header = &p->header;

    long offset = p->pptr_head;

    mmByteOrder_FileLittleEncodeU32(assets->fd, &offset, &header->chunk_id);
    mmByteOrder_FileLittleEncodeU32(assets->fd, &offset, &header->index);
    mmByteOrder_FileLittleEncodeU32(assets->fd, &offset, &header->number);
}
MM_EXPORT_TRACK void mmTrackChunk_DecodeHeader(struct mmTrackChunk* p)
{
    struct mmTrackAssets* assets = p->assets;

    struct mmTrackChunkHeader* header = &p->header;

    long offset = p->pptr_head;

    mmByteOrder_FileLittleDecodeU32(assets->fd, &offset, &header->chunk_id);
    mmByteOrder_FileLittleDecodeU32(assets->fd, &offset, &header->index);
    mmByteOrder_FileLittleDecodeU32(assets->fd, &offset, &header->number);
}

MM_EXPORT_TRACK mmBool_t mmTrackChunk_Contain(struct mmTrackChunk* p, mmUInt32_t b, mmUInt32_t e)
{
    struct mmTrackChunkHeader* header_chunk = &p->header;

    mmUInt32_t n_b = header_chunk->index;
    mmUInt32_t n_e = header_chunk->index + header_chunk->number - 1;

    mmBool_t b0 = header_chunk->number > 0;
    mmBool_t b1 = n_b <= b && b <= n_e;
    mmBool_t b2 = n_b <= e && e <= n_e;

    return b0 && (b1 || b2);
}

MM_EXPORT_TRACK void mmTrackChunk_CalculatePptrValue(struct mmTrackChunk* p)
{
    struct mmTrackAssets* assets = p->assets;
    struct mmTrackAssetsHeader* header_assets = &assets->header;

    p->pptr_head = MM_TRACK_ASSETS_FHEAD_SIZE_DEFAULT + p->header.chunk_id * assets->header.chunk_size;
    p->pptr_body = p->pptr_head + MM_TRACK_CHUNK_HEAD_SIZE;
    p->pptr_last = p->pptr_head + assets->header.chunk_size;
    p->pptr_idxe = p->pptr_head + MM_TRACK_CHUNK_OFFSET_INDEX_E;

    p->insert = (0 == header_assets->regular_length) ? &__static_mmTrackChunk_InsertDynamic : &__static_mmTrackChunk_InsertRegular;
    p->select = (0 == header_assets->regular_length) ? &__static_mmTrackChunk_SelectDynamic : &__static_mmTrackChunk_SelectRegular;
}
MM_EXPORT_TRACK void mmTrackChunk_CalculateFreeSpace(struct mmTrackChunk* p)
{
    struct mmTrackAssets* assets = p->assets;

    mmUInt32_t idx_e = p->header.number;

    // size is empty.
    if (0 == idx_e)
    {
        p->offset_prev = 0;
        p->free_space = (mmUInt32_t)(p->pptr_last - p->pptr_body);
    }
    else
    {
        long pptr_e = (long)(p->pptr_last - assets->offset_size * idx_e);

        mmUInt32_t prev_pptr = 0;
        mmUInt32_t body_space = (mmUInt32_t)(p->pptr_last - p->pptr_body);

        __static_mmTrackChunk_ByteOrderFileLittleDecode(p, &pptr_e, &prev_pptr);

        p->offset_prev = prev_pptr;
        p->free_space = body_space - (assets->offset_size * idx_e) - prev_pptr;
    }
}

MM_EXPORT_TRACK void mmTrackChunk_Insert(struct mmTrackChunk* p, mmUInt32_t index, mmUInt8_t* buffer, mmUInt32_t length)
{
    (*(p->insert))(p, index, buffer, length);
}
MM_EXPORT_TRACK void mmTrackChunk_Select(struct mmTrackChunk* p, mmUInt32_t b, mmUInt32_t e, mmTrackChunkSelectFunc handle, void* v)
{
    (*(p->select))(p, b, e, handle, v);
}

static void __static_mmTrackChunk_ByteOrderFileLittleDecode(struct mmTrackChunk* p, long* offset, mmUInt32_t* v)
{
    struct mmTrackAssets* assets = p->assets;

    if (sizeof(mmUInt32_t) == assets->offset_size)
    {
        mmByteOrder_FileLittleDecodeU32(assets->fd, offset, v);
    }
    else
    {
        mmUInt16_t u16 = 0;
        mmByteOrder_FileLittleDecodeU16(assets->fd, offset, &u16);
        (*v) = (mmUInt32_t)u16;
    }
}
static void __static_mmTrackChunk_ByteOrderFileLittleEncode(struct mmTrackChunk* p, long* offset, mmUInt32_t* v)
{
    struct mmTrackAssets* assets = p->assets;

    if (sizeof(mmUInt32_t) == assets->offset_size)
    {
        mmByteOrder_FileLittleEncodeU32(assets->fd, offset, v);
    }
    else
    {
        mmUInt16_t u16 = (mmUInt16_t)(*v);
        mmByteOrder_FileLittleEncodeU16(assets->fd, offset, &u16);
    }
}

static void
__static_mmTrackChunk_InsertDynamic(
    struct mmTrackChunk* p,
    mmUInt32_t index,
    mmUInt8_t* buffer,
    mmUInt32_t length)
{
    struct mmTrackAssets* assets = p->assets;

    struct mmTrackChunkHeader* header_chunk = &p->header;

    mmUInt32_t idx_curr = index - header_chunk->index;
    mmUInt32_t idx_e = header_chunk->index + header_chunk->number;

    long pptr_data = 0;
    long pptr_prev = 0;
    long pptr_idxe = p->pptr_idxe;

    mmUInt32_t offset_curr = 0;
    mmUInt32_t offset_prev = 0;

    do
    {
        if (index != idx_e)
        {
            // index is out of range.
            break;
        }

        if (length + assets->offset_size > p->free_space)
        {
            // not enough free space.
            break;
        }

        header_chunk->number++;

        pptr_prev = (long)(p->pptr_last - assets->offset_size * (idx_curr + 1));

        offset_prev = p->offset_prev;

        pptr_data = (long)(p->pptr_body + offset_prev);
        offset_curr = offset_prev + length;

        p->free_space = (mmUInt32_t)(pptr_prev - (pptr_data + length));
        p->offset_prev = offset_curr;

        mmByteOrder_FileLittleEncodeU32(assets->fd, &pptr_idxe, &header_chunk->number);

        mmByteOrder_FileLittleEncodeBin(assets->fd, &pptr_data, buffer, length);

        __static_mmTrackChunk_ByteOrderFileLittleEncode(p, &pptr_prev, &offset_curr);
    } while (0);
}

static void
__static_mmTrackChunk_InsertRegular(
    struct mmTrackChunk* p,
    mmUInt32_t index,
    mmUInt8_t* buffer,
    mmUInt32_t length)
{
    struct mmTrackAssets* assets = p->assets;

    struct mmTrackAssetsHeader* header_assets = &assets->header;
    struct mmTrackChunkHeader* header_chunk = &p->header;

    mmUInt32_t idx_curr = index - header_chunk->index;
    mmUInt32_t idx_e = header_chunk->index + header_chunk->number;

    long pptr_data = 0;
    long pptr_idxe = p->pptr_idxe;

    do
    {
        if (index != idx_e)
        {
            // index is out of range.
            break;
        }

        if (length + assets->offset_size > p->free_space)
        {
            // not enough free space.
            break;
        }

        header_chunk->number++;

        pptr_data = (long)(p->pptr_body + (idx_curr * header_assets->regular_length));

        p->free_space = (mmUInt32_t)(p->pptr_last - (pptr_data + length));

        mmByteOrder_FileLittleEncodeU32(assets->fd, &pptr_idxe, &header_chunk->number);

        mmByteOrder_FileLittleEncodeBin(assets->fd, &pptr_data, buffer, length);
    } while (0);
}

static void
__static_mmTrackChunk_SelectDynamic(
    struct mmTrackChunk* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmTrackChunkSelectFunc handle,
    void* v)
{
    struct mmTrackAssets* assets = p->assets;

    struct mmTrackChunkHeader* header_chunk = &p->header;

    if (0 < header_chunk->number)
    {
        mmUInt32_t i_b = 0;
        mmUInt32_t i_e = 0;

        // [b, e]
        mmUInt32_t i = 0;
        mmUInt32_t idx_b = 0;
        mmUInt32_t length = 0;

        mmUInt32_t n_b = header_chunk->index;
        mmUInt32_t n_e = header_chunk->index + header_chunk->number - 1;

        struct mmByteBuffer byte_buffer;

        long pptr_data = 0;
        long pptr_idxi = 0;
        
        long pptr_e = 0;

        mmUInt32_t offset_idxi = 0;
        mmUInt32_t offset_prev = 0;

        mmUInt32_t idx_i = 0;

        i_b = n_b > b ? n_b : b;
        i_e = n_e < e ? n_e : e;

        idx_b = i_b - header_chunk->index;

        if (0 == idx_b)
        {
            offset_prev = 0;
        }
        else
        {
            pptr_e = (long)(p->pptr_last - assets->offset_size * idx_b);

            __static_mmTrackChunk_ByteOrderFileLittleDecode(p, &pptr_e, &offset_prev);
        }

        for (i = i_b; i <= i_e; i++)
        {
            idx_i = i - header_chunk->index;

            pptr_data = (long)(p->pptr_body + offset_prev);

            pptr_idxi = p->pptr_last - assets->offset_size * (idx_i + 1);

            __static_mmTrackChunk_ByteOrderFileLittleDecode(p, &pptr_idxi, &offset_idxi);

            length = offset_idxi - offset_prev;

            mmStreambuf_Reset(&assets->streambuf);
            mmStreambuf_AlignedMemory(&assets->streambuf, length);

            byte_buffer.buffer = assets->streambuf.buff;
            byte_buffer.offset = 0;
            byte_buffer.length = length;

            mmByteOrder_FileLittleDecodeBin(assets->fd, &pptr_data, assets->streambuf.buff, length);

            (*(handle))(p, i, &byte_buffer, v);

            offset_prev = offset_idxi;
        }
    }
}

static void
__static_mmTrackChunk_SelectRegular(
    struct mmTrackChunk* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmTrackChunkSelectFunc handle,
    void* v)
{
    struct mmTrackAssets* assets = p->assets;

    struct mmTrackAssetsHeader* header_assets = &assets->header;
    struct mmTrackChunkHeader* header_chunk = &p->header;

    if (0 < header_chunk->number)
    {
        mmUInt32_t i_b = 0;
        mmUInt32_t i_e = 0;

        // [b, e]
        mmUInt32_t i = 0;
        mmUInt32_t length = 0;

        mmUInt32_t n_b = header_chunk->index;
        mmUInt32_t n_e = header_chunk->index + header_chunk->number - 1;

        struct mmByteBuffer byte_buffer;

        long pptr_data = 0;

        mmUInt32_t idx_i = 0;

        i_b = n_b > b ? n_b : b;
        i_e = n_e < e ? n_e : e;

        for (i = i_b; i <= i_e; i++)
        {
            idx_i = i - header_chunk->index;

            pptr_data = (long)(p->pptr_body + (idx_i * header_assets->regular_length));

            length = header_assets->regular_length;

            mmStreambuf_Reset(&assets->streambuf);
            mmStreambuf_AlignedMemory(&assets->streambuf, length);

            byte_buffer.buffer = assets->streambuf.buff;
            byte_buffer.offset = 0;
            byte_buffer.length = length;

            mmByteOrder_FileLittleDecodeBin(assets->fd, &pptr_data, assets->streambuf.buff, length);

            (*(handle))(p, i, &byte_buffer, v);
        }
    }
}
