/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTrackChunk_h__
#define __mmTrackChunk_h__

#include "core/mmCore.h"
#include "core/mmByte.h"
#include "core/mmStreambuf.h"

#include "container/mmVectorU32.h"

#include "mmTrackExport.h"

#include "core/mmPrefix.h"

typedef void(*mmTrackChunkSelectFunc)(void* obj, mmUInt32_t i, struct mmByteBuffer* byte_buffer, void* v);

// uint32 * 3
#define MM_TRACK_CHUNK_HEAD_SIZE 12

#define MM_TRACK_CHUNK_OFFSET_INDEX_E 8

/*
 *   track chunk dynamic page struct      track chunk regular page struct
 *   little endian                        little endian
 *
 *    ------------------------------      ------------------------------
 *   |        (chunk_size)          |    |      (chunk_size)            |
 *    ------------------------------      ------------------------------
 *   |  u32    chunk_id             |    |  u32    chunk_id             |
 *   |  u32    index                |    |  u32    index                |
 *   |  u32    number               |    |  u32    number               |
 *    ------------------------------      ------------------------------
 *   |  byte*  buffer_0             |    |  byte*  buffer_0             |
 *   |  byte*  buffer_1             |    |  byte*  buffer_1             |
 *   |         ......               |    |         ......               |
 *   |  byte*  buffer_i             |    |  byte*  buffer_i             |
 *    ------------------------------      ------------------------------
 *   |         <free space>         |    |         <free space>         |
 *    ------------------------------     |                              |
 *   |  u32    offset_i             |    |                              |
 *   |         ......               |    |                              |
 *   |  u32    offset_1             |    |                              |
 *   |  u32    offset_0             |    |                              |
 *    ------------------------------      ------------------------------
 */

struct mmTrackChunkHeader
{
    mmUInt32_t chunk_id;
    mmUInt32_t index;
    mmUInt32_t number;
};
MM_EXPORT_TRACK void mmTrackChunkHeader_Init(struct mmTrackChunkHeader* p);
MM_EXPORT_TRACK void mmTrackChunkHeader_Destroy(struct mmTrackChunkHeader* p);
MM_EXPORT_TRACK void mmTrackChunkHeader_Reset(struct mmTrackChunkHeader* p);

struct mmTrackChunk;

typedef void(*mmTrackChunkProcessInsertFunc)(
    struct mmTrackChunk* p,
    mmUInt32_t index,
    mmUInt8_t* buffer,
    mmUInt32_t length);

typedef void(*mmTrackChunkProcessSelectFunc)(
    struct mmTrackChunk* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmTrackChunkSelectFunc handle,
    void* v);

struct mmTrackChunk
{
    struct mmTrackChunkHeader header;

    // weak ref.
    struct mmTrackAssets* assets;

    // cache for chunk head pptr.
    long pptr_head;
    // cache for chunk body pptr
    long pptr_body;
    // cache for chunk last pptr.
    long pptr_last;
    // cache for chunk idxe pptr.
    long pptr_idxe;

    // cache for chunk offset prev.
    mmUInt32_t offset_prev;
    // cache for body free space
    mmUInt32_t free_space;

    mmTrackChunkProcessInsertFunc insert;
    mmTrackChunkProcessSelectFunc select;
};
MM_EXPORT_TRACK void mmTrackChunk_Init(struct mmTrackChunk* p);
MM_EXPORT_TRACK void mmTrackChunk_Destroy(struct mmTrackChunk* p);

MM_EXPORT_TRACK void mmTrackChunk_Reset(struct mmTrackChunk* p);

MM_EXPORT_TRACK void mmTrackChunk_SetAssets(struct mmTrackChunk* p, struct mmTrackAssets* assets);
MM_EXPORT_TRACK void mmTrackChunk_SetChunkId(struct mmTrackChunk* p, mmUInt32_t chunk_id);
MM_EXPORT_TRACK void mmTrackChunk_SetIndex(struct mmTrackChunk* p, mmUInt32_t index);
MM_EXPORT_TRACK void mmTrackChunk_SetNumber(struct mmTrackChunk* p, mmUInt32_t number);

MM_EXPORT_TRACK void mmTrackChunk_EncodeHeader(struct mmTrackChunk* p);
MM_EXPORT_TRACK void mmTrackChunk_DecodeHeader(struct mmTrackChunk* p);

MM_EXPORT_TRACK mmBool_t mmTrackChunk_Contain(struct mmTrackChunk* p, mmUInt32_t b, mmUInt32_t e);

MM_EXPORT_TRACK void mmTrackChunk_CalculatePptrValue(struct mmTrackChunk* p);
MM_EXPORT_TRACK void mmTrackChunk_CalculateFreeSpace(struct mmTrackChunk* p);

MM_EXPORT_TRACK void
mmTrackChunk_Insert(
    struct mmTrackChunk* p,
    mmUInt32_t index,
    mmUInt8_t* buffer,
    mmUInt32_t length);

MM_EXPORT_TRACK void
mmTrackChunk_Select(
    struct mmTrackChunk* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmTrackChunkSelectFunc handle,
    void* v);

#include "core/mmSuffix.h"

#endif//__mmTrackChunk_h__
