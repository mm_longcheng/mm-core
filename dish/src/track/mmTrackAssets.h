/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmTrackAssets_h__
#define __mmTrackAssets_h__

#include "core/mmCore.h"
#include "core/mmStreambuf.h"
#include "core/mmPoolElement.h"
#include "core/mmString.h"

#include "container/mmRbtreeIntervalU32.h"

#include "mmTrackChunk.h"

#include "mmTrackExport.h"

#include "core/mmPrefix.h"

struct mmTrackChunk;

#define MM_TRACK_ASSETS_VERSION 0
// page size 4K.
#define MM_TRACK_ASSETS_FHEAD_SIZE_DEFAULT 4096
#define MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT 4096

#define MM_TRACK_ASSETS_OFFSET_CHUNK_NUMBER  24

/*
 *   track assets page struct
 *   little endian
 *
 *    ------------------------------
 *   |         (head_size) 4K       |
 *    ------------------------------
 *   |  u8[12] ID                   |
 *   |  u32    version              |
 *   |  u32    regular_length       |
 *   |  u32    chunk_size           |
 *   |  u32    chunk_number         |
 *    ------------------------------
 *   | (chunk_size) chunk 0         |
 *    ------------------------------
 *   | (chunk_size) chunk 1         |
 *    ------------------------------
 *   |         ......               |
 *    ------------------------------
 *   | (chunk_size) chunk i         |
 *    ------------------------------
 */

struct mmTrackAssetsHeader
{
    mmUInt8_t ID[16];
    mmUInt32_t version;
    mmUInt32_t regular_length;
    mmUInt32_t chunk_size;
    mmUInt32_t chunk_number;
};
MM_EXPORT_TRACK void mmTrackAssetsHeader_Init(struct mmTrackAssetsHeader* p);
MM_EXPORT_TRACK void mmTrackAssetsHeader_Destroy(struct mmTrackAssetsHeader* p);

struct mmTrackAssets
{
    struct mmTrackAssetsHeader header;

    struct mmPoolElement pool_element;
    struct mmRbtreeIntervalU32 rbtree;

    struct mmStreambuf streambuf;
    struct mmStreambuf information;

    struct mmString filename;

    mmUInt32_t offset_size;
    mmUInt32_t index_current;

    mmBool_t format_valid;
    FILE* fd;
    struct mmTrackChunk* current_chunk;
};
MM_EXPORT_TRACK void mmTrackAssets_Init(struct mmTrackAssets* p);
MM_EXPORT_TRACK void mmTrackAssets_Destroy(struct mmTrackAssets* p);

MM_EXPORT_TRACK void mmTrackAssets_SetFilename(struct mmTrackAssets* p, const char* filename);
MM_EXPORT_TRACK void mmTrackAssets_SetChunkSize(struct mmTrackAssets* p, mmUInt32_t chunk_size);
MM_EXPORT_TRACK void mmTrackAssets_SetRegularLength(struct mmTrackAssets* p, mmUInt32_t regular_length);

MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_EncodeHeader(struct mmTrackAssets* p);
MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_DecodeHeader(struct mmTrackAssets* p);

MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_Fopen(struct mmTrackAssets* p);
MM_EXPORT_TRACK void mmTrackAssets_Fflush(struct mmTrackAssets* p);
MM_EXPORT_TRACK void mmTrackAssets_Fclose(struct mmTrackAssets* p);

MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_ClearDatabase(struct mmTrackAssets* p);

MM_EXPORT_TRACK mmUInt32_t
mmTrackAssets_Insert(struct mmTrackAssets* p,
    mmUInt32_t index,
    mmUInt8_t* buffer,
    mmUInt32_t length);

MM_EXPORT_TRACK mmUInt32_t
mmTrackAssets_Select(
    struct mmTrackAssets* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmTrackChunkSelectFunc handle,
    void* v);

MM_EXPORT_TRACK struct mmTrackChunk* mmTrackAssets_ChunkProduce(struct mmTrackAssets* p);
MM_EXPORT_TRACK void mmTrackAssets_ChunkRecycle(struct mmTrackAssets* p, struct mmTrackChunk* chunk);
MM_EXPORT_TRACK void mmTrackAssets_ChunkClear(struct mmTrackAssets* p);

#include "core/mmSuffix.h"

#endif//__mmTrackAssets_h__
