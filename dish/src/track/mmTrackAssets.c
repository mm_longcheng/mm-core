/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmTrackAssets.h"

#include "core/mmAlloc.h"
#include "core/mmByteswap.h"
#include "core/mmByteOrder.h"

#include "algorithm/mmCRC32.h"

static const char g_ID[16] = { "mm_track" };

static void __static_mmTrackAssets_CommitChunkNumber(struct mmTrackAssets* p);

MM_EXPORT_TRACK void mmTrackAssetsHeader_Init(struct mmTrackAssetsHeader* p)
{
    mmMemset(p->ID, 0, sizeof(mmChar_t) * 16);
    p->version = 0;
    p->regular_length = 0;
    p->chunk_size = MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT;
    p->chunk_number = 0;
    //
    mmMemcpy(p->ID, g_ID, 12);
}
MM_EXPORT_TRACK void mmTrackAssetsHeader_Destroy(struct mmTrackAssetsHeader* p)
{
    mmMemset(p->ID, 0, sizeof(mmChar_t) * 12);
    p->version = 0;
    p->regular_length = 0;
    p->chunk_size = MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT;
    p->chunk_number = 0;
}

MM_EXPORT_TRACK void mmTrackAssets_Init(struct mmTrackAssets* p)
{
    struct mmPoolElementAllocator hAllocator;

    mmPoolElement_Init(&p->pool_element);
    mmRbtreeIntervalU32_Init(&p->rbtree);
    mmStreambuf_Init(&p->streambuf);
    mmStreambuf_Init(&p->information);
    mmString_Init(&p->filename);
    mmTrackAssetsHeader_Init(&p->header);
    p->offset_size = sizeof(mmUInt16_t);
    p->index_current = -1;
    p->format_valid = MM_FALSE;
    p->fd = NULL;
    p->current_chunk = NULL;
    //
    hAllocator.Produce = &mmTrackChunk_Init;
    hAllocator.Recycle = &mmTrackChunk_Destroy;
    hAllocator.obj = p;
    mmPoolElement_SetElementSize(&p->pool_element, sizeof(struct mmTrackChunk));
    mmPoolElement_SetAllocator(&p->pool_element, &hAllocator);
    mmPoolElement_CalculationBucketSize(&p->pool_element);
}
MM_EXPORT_TRACK void mmTrackAssets_Destroy(struct mmTrackAssets* p)
{
    mmTrackAssets_ChunkClear(p);
    mmTrackAssets_Fflush(p);
    mmTrackAssets_Fclose(p);

    mmPoolElement_Destroy(&p->pool_element);
    mmRbtreeIntervalU32_Destroy(&p->rbtree);
    mmStreambuf_Destroy(&p->streambuf);
    mmStreambuf_Destroy(&p->information);
    mmString_Destroy(&p->filename);
    mmTrackAssetsHeader_Destroy(&p->header);
    p->offset_size = sizeof(mmUInt16_t);
    p->index_current = -1;
    p->format_valid = MM_FALSE;
    p->fd = NULL;
    p->current_chunk = NULL;
}

MM_EXPORT_TRACK void mmTrackAssets_SetFilename(struct mmTrackAssets* p, const char* filename)
{
    mmString_Assigns(&p->filename, filename);
}
MM_EXPORT_TRACK void mmTrackAssets_SetChunkSize(struct mmTrackAssets* p, mmUInt32_t chunk_size)
{
    p->header.chunk_size = chunk_size;
    assert(chunk_size % MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT == 0 && "chunk_size only support page multiple.");
}
MM_EXPORT_TRACK void mmTrackAssets_SetRegularLength(struct mmTrackAssets* p, mmUInt32_t regular_length)
{
    p->header.regular_length = regular_length;
}

MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_EncodeHeader(struct mmTrackAssets* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        struct mmTrackAssetsHeader* header = &p->header;

        long offset = 0;

        mmUInt8_t* buffer = NULL;
        mmUInt32_t lenght = 0;

        if (NULL == p->fd)
        {
            // FILE is not fopen.
            code = MM_FAILURE;
            break;
        }

        // common
        mmByteOrder_FileLittleEncodeBin(p->fd, &offset, &header->ID[0], 12);
        mmByteOrder_FileLittleEncodeU32(p->fd, &offset, &header->version);
        mmByteOrder_FileLittleEncodeU32(p->fd, &offset, &header->regular_length);
        mmByteOrder_FileLittleEncodeU32(p->fd, &offset, &header->chunk_size);
        mmByteOrder_FileLittleEncodeU32(p->fd, &offset, &header->chunk_number);
        // information
        buffer = p->information.buff + p->information.pptr;
        lenght = (mmUInt32_t)mmStreambuf_Size(&p->information);
        mmByteOrder_FileLittleEncodeU32(p->fd, &offset, &lenght);
        mmByteOrder_FileLittleEncodeBin(p->fd, &offset, buffer, lenght);

        code = MM_SUCCESS;
    } while (0);

    return code;
}
MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_DecodeHeader(struct mmTrackAssets* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        struct mmTrackAssetsHeader* header = &p->header;

        long offset = 0;

        mmUInt8_t* buffer = NULL;
        mmUInt32_t lenght = 0;

        if (NULL == p->fd)
        {
            // FILE is not fopen.
            code = MM_FAILURE;
            break;
        }

        mmMemset(&header->ID[0], 0, 12);

        // common
        mmByteOrder_FileLittleDecodeBin(p->fd, &offset, &header->ID[0], 12);
        mmByteOrder_FileLittleDecodeU32(p->fd, &offset, &header->version);
        mmByteOrder_FileLittleDecodeU32(p->fd, &offset, &header->regular_length);
        mmByteOrder_FileLittleDecodeU32(p->fd, &offset, &header->chunk_size);
        mmByteOrder_FileLittleDecodeU32(p->fd, &offset, &header->chunk_number);

        p->format_valid = (0 == mmMemcmp(p->header.ID, g_ID, 12));

        if (MM_FALSE == p->format_valid)
        {
            // FILE format error.
            code = MM_FAILURE;
            break;
        }

        if (0 != (header->chunk_size % MM_TRACK_ASSETS_CHUNK_SIZE_DEFAULT))
        {
            // FILE format error.
            code = MM_FAILURE;
            break;
        }

        if (MM_TRACK_ASSETS_VERSION < header->version)
        {
            // FILE version not support.
            code = MM_FAILURE;
            break;
        }

        // information
        mmByteOrder_FileLittleDecodeU32(p->fd, &offset, &lenght);
        mmStreambuf_AlignedMemory(&p->information, lenght);
        buffer = p->information.buff + p->information.pptr;
        mmByteOrder_FileLittleDecodeBin(p->fd, &offset, buffer, lenght);
        mmStreambuf_Pbump(&p->information, lenght);

        code = MM_SUCCESS;
    } while (0);

    return code;
}

MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_Fopen(struct mmTrackAssets* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        const char* hFileName = mmString_CStr(&p->filename);
        if (NULL != p->fd)
        {
            // need do nothing.
            code = MM_SUCCESS;
            break;
        }

        if (mmString_Empty(&p->filename))
        {
            // FILE name is empty.
            code = MM_FAILURE;
            break;
        }

        p->fd = fopen(hFileName, "rb+");
        if (NULL == p->fd)
        {
            // new file.
            p->fd = fopen(hFileName, "wb+");

            code = mmTrackAssets_EncodeHeader(p);

            if (MM_SUCCESS == code)
            {
                p->header.chunk_number = 0;

                p->index_current = -1;
                p->format_valid = MM_TRUE;
                p->current_chunk = NULL;

                mmTrackAssets_Fflush(p);

                // 64k
                p->offset_size = p->header.chunk_size > 65536 ? sizeof(mmUInt32_t) : sizeof(mmUInt16_t);
            }
        }
        else
        {
            code = mmTrackAssets_DecodeHeader(p);

            if (MM_SUCCESS == code)
            {
                mmUInt32_t i = 0;

                struct mmTrackChunk* chunk = NULL;

                // 64k
                p->offset_size = p->header.chunk_size > 65536 ? sizeof(mmUInt32_t) : sizeof(mmUInt16_t);

                for (i = 0; i < p->header.chunk_number; i++)
                {
                    if (NULL != p->current_chunk)
                    {
                        struct mmTrackChunkHeader* header = &p->current_chunk->header;

                        mmUInt32_t l = header->index;
                        mmUInt32_t r = header->index + header->number - 1;

                        assert(0 < header->number && "chunk number is invalid.");

                        mmRbtreeIntervalU32_Set(&p->rbtree, l, r, p->current_chunk);
                        p->current_chunk = NULL;
                    }

                    chunk = mmTrackAssets_ChunkProduce(p);

                    mmTrackChunk_SetAssets(chunk, p);
                    mmTrackChunk_SetChunkId(chunk, i);

                    mmTrackChunk_CalculatePptrValue(chunk);
                    mmTrackChunk_DecodeHeader(chunk);
                    mmTrackChunk_CalculateFreeSpace(chunk);

                    p->current_chunk = chunk;

                    p->index_current = chunk->header.index + chunk->header.number - 1;
                }
            }
            else
            {
                mmTrackAssets_Fclose(p);
            }
        }

    } while (0);

    return code;
}
MM_EXPORT_TRACK void mmTrackAssets_Fflush(struct mmTrackAssets* p)
{
    if (NULL != p->fd)
    {
        fflush(p->fd);
    }
}
MM_EXPORT_TRACK void mmTrackAssets_Fclose(struct mmTrackAssets* p)
{
    if (NULL != p->fd)
    {
        fclose(p->fd);
        p->fd = NULL;
    }
}

MM_EXPORT_TRACK mmUInt32_t mmTrackAssets_ClearDatabase(struct mmTrackAssets* p)
{
    mmUInt32_t code = MM_UNKNOWN;

    mmTrackAssets_ChunkClear(p);
    mmTrackAssets_Fclose(p);

    p->fd = fopen(mmString_CStr(&p->filename), "wb+");

    code = mmTrackAssets_EncodeHeader(p);

    if (NULL != p->fd)
    {
        p->header.chunk_number = 0;

        p->index_current = -1;
        p->format_valid = MM_TRUE;
        p->current_chunk = NULL;

        mmTrackAssets_Fflush(p);

        // 64k
        p->offset_size = p->header.chunk_size > 65536 ? sizeof(mmUInt32_t) : sizeof(mmUInt16_t);
    }

    return code;
}

MM_EXPORT_TRACK mmUInt32_t
mmTrackAssets_Insert(
    struct mmTrackAssets* p,
    mmUInt32_t index,
    mmUInt8_t* buffer,
    mmUInt32_t length)
{
    mmUInt32_t code = MM_UNKNOWN;

    do
    {
        if (NULL == p->fd)
        {
            // FILE is not fopen.
            code = MM_FAILURE;
            break;
        }

        if (p->header.chunk_size - MM_TRACK_CHUNK_HEAD_SIZE <= length)
        {
            // buffer length must be less than free space.
            code = MM_FAILURE;
            break;
        }

        if (index != p->index_current + 1)
        {
            // index must be append from index_current.
            code = MM_FAILURE;
            break;
        }

        if (NULL != p->current_chunk && length + sizeof(mmUInt32_t) > p->current_chunk->free_space)
        {
            struct mmTrackChunkHeader* header = &p->current_chunk->header;

            mmUInt32_t l = header->index;
            mmUInt32_t r = header->index + header->number - 1;

            assert(0 < header->number && "chunk number is invalid.");

            mmRbtreeIntervalU32_Set(&p->rbtree, l, r, p->current_chunk);
            p->current_chunk = NULL;
        }
        if (NULL == p->current_chunk)
        {
            struct mmTrackChunk* chunk = mmTrackAssets_ChunkProduce(p);

            mmTrackChunk_SetAssets(chunk, p);
            mmTrackChunk_SetChunkId(chunk, p->header.chunk_number);
            mmTrackChunk_SetIndex(chunk, p->index_current + 1);
            mmTrackChunk_SetNumber(chunk, 0);

            mmTrackChunk_CalculatePptrValue(chunk);
            mmTrackChunk_EncodeHeader(chunk);
            mmTrackChunk_CalculateFreeSpace(chunk);

            mmTrackAssets_Fflush(p);

            p->current_chunk = chunk;

            p->header.chunk_number++;

            __static_mmTrackAssets_CommitChunkNumber(p);
        }

        // insert into current chunk.
        mmTrackChunk_Insert(p->current_chunk, index, buffer, length);

        p->index_current++;

        code = MM_SUCCESS;
    } while (0);

    return code;
}

MM_EXPORT_TRACK mmUInt32_t
mmTrackAssets_Select(
    struct mmTrackAssets* p,
    mmUInt32_t b,
    mmUInt32_t e,
    mmTrackChunkSelectFunc handle,
    void* v)
{
    mmUInt32_t code = 0;

    do
    {
        struct mmTrackChunk* chunk = NULL;

        if (NULL == p->fd)
        {
            // FILE is not fopen.
            code = MM_FAILURE;
            break;
        }

        if (b > e)
        {
            // index must began <= ended.
            code = MM_FAILURE;
            break;
        }

        if (0 < p->rbtree.size)
        {
            struct mmRbNode* n = NULL;
            struct mmRbtreeIntervalU32Iterator* it = NULL;

            it = mmRbtreeIntervalU32_GetIterator(&p->rbtree, b, b);
            if (NULL != it)
            {
                n = &it->n;

                while (NULL != n)
                {
                    it = mmRb_Entry(n, struct mmRbtreeIntervalU32Iterator, n);
                    n = mmRb_Prev(n);

                    chunk = (struct mmTrackChunk*)it->v;

                    if (mmTrackChunk_Contain(chunk, b, e))
                    {
                        mmTrackChunk_Select(chunk, b, e, handle, v);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        if (NULL != p->current_chunk)
        {
            chunk = p->current_chunk;

            if (mmTrackChunk_Contain(chunk, b, e))
            {
                mmTrackChunk_Select(chunk, b, e, handle, v);
            }
        }

        code = MM_SUCCESS;
    } while (0);

    return code;
}

MM_EXPORT_TRACK struct mmTrackChunk* mmTrackAssets_ChunkProduce(struct mmTrackAssets* p)
{
    struct mmTrackChunk* chunk = NULL;

    // pool_element produce.
    mmPoolElement_Lock(&p->pool_element);
    chunk = (struct mmTrackChunk*)mmPoolElement_Produce(&p->pool_element);
    mmPoolElement_Unlock(&p->pool_element);
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmTrackChunk_Reset(chunk);

    return chunk;
}
MM_EXPORT_TRACK void mmTrackAssets_ChunkRecycle(struct mmTrackAssets* p, struct mmTrackChunk* chunk)
{
    // note: 
    // pool_element manager init and destroy.
    // when mmPoolElement_Produce and mmPoolElement_Recycle only need reset.
    mmTrackChunk_Reset(chunk);
    // pool_element recycle.
    mmPoolElement_Lock(&p->pool_element);
    mmPoolElement_Recycle(&p->pool_element, chunk);
    mmPoolElement_Unlock(&p->pool_element);
}

MM_EXPORT_TRACK void mmTrackAssets_ChunkClear(struct mmTrackAssets* p)
{
    struct mmRbNode* n = NULL;
    struct mmRbtreeIntervalU32Iterator* it = NULL;
    struct mmTrackChunk* chunk = NULL;
    //
    n = mmRb_First(&p->rbtree.rbt);
    while (NULL != n)
    {
        it = mmRb_Entry(n, struct mmRbtreeIntervalU32Iterator, n);
        n = mmRb_Next(n);
        chunk = (struct mmTrackChunk*)it->v;
        mmRbtreeIntervalU32_Erase(&p->rbtree, it);

        mmTrackAssets_ChunkRecycle(p, chunk);
    }
}

static void __static_mmTrackAssets_CommitChunkNumber(struct mmTrackAssets* p)
{
    long offset = MM_TRACK_ASSETS_OFFSET_CHUNK_NUMBER;
    mmByteOrder_FileLittleEncodeU32(p->fd, &offset, &p->header.chunk_number);
}

