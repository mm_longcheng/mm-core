/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityComponentManager.h"

namespace mm
{
    mmEntityComponentManager::mmEntityComponentManager()
        : d_chunk_size(MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT)
    {

    }
    mmEntityComponentManager::~mmEntityComponentManager()
    {
        this->Clear();
    }
    void mmEntityComponentManager::SetChunkSize(size_t chunk_size)
    {
        this->d_chunk_size = chunk_size;
    }
    mmPoolTypeBase* mmEntityComponentManager::Get(size_t id) const
    {
        mmPoolTypeBase* e = NULL;
        ComponentPoolMapType::const_iterator it = this->d_pool_component.find(id);
        if (it != this->d_pool_component.end())
        {
            e = it->second;
        }
        return e;
    }

    void mmEntityComponentManager::Rmv(size_t id)
    {
        mmPoolTypeBase* e = NULL;
        ComponentPoolMapType::iterator it = this->d_pool_component.find(id);
        if (it != this->d_pool_component.end())
        {
            e = it->second;
            this->d_pool_component.erase(it);
            delete e;
        }
    }
    void mmEntityComponentManager::Clear()
    {
        mmPoolTypeBase* e = NULL;
        ComponentPoolMapType::iterator it = this->d_pool_component.begin();
        while (it != this->d_pool_component.end())
        {
            e = it->second;
            this->d_pool_component.erase(it++);
            delete e;
        }
    }
    void mmEntityComponentManager::Recycle(void* v, size_t id)
    {
        mmPoolTypeBase* e = this->Get(id);
        assert(NULL != e && "can not find valid pool type.");
        e->Recycle(v);
    }
    // cancellation name -> data_type pool.
    void mmEntityComponentManager::Cancellation(const char* name)
    {
        NameToIdMapType::iterator it = this->d_name_to_id_map.find(name);
        if (it != this->d_name_to_id_map.end())
        {
            size_t id = it->second;
            this->d_id_to_name_map.erase(id);
            this->d_name_to_id_map.erase(name);
        }
    }
    size_t mmEntityComponentManager::NameToId(const char* name) const
    {
        size_t id = 0;
        NameToIdMapType::const_iterator it = this->d_name_to_id_map.find(name);
        if (it != this->d_name_to_id_map.end())
        {
            id = it->second;
        }
        return id;
    }
    const char* mmEntityComponentManager::IdToName(size_t id) const
    {
        const char* name = "";
        IdToNameMapType::const_iterator it = this->d_id_to_name_map.find(id);
        if (it != this->d_id_to_name_map.end())
        {
            name = it->second.c_str();
        }
        return name;
    }
    void* mmEntityComponentManager::Produce(size_t id)
    {
        void* v = NULL;
        mmPoolTypeBase* e = this->Get(id);
        assert(NULL != e && "not find type at registration map.");
        v = e->Produce();
        assert(NULL != v && "memory not enough, can not alloc data.");
        return v;
    }
    void* mmEntityComponentManager::Produce(const char* name)
    {
        void* v = NULL;
        size_t id = this->NameToId(name);
        v = this->Produce(id);
        return v;
    }
}
