/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityComponentManager_h__
#define __mmEntityComponentManager_h__

#include "core/mmConfig.h"

#include "dish/mmPoolType.h"

#include "entity/mmEntityComponent.h"

#include <map>

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntityComponentManager
    {
    public:
        typedef std::map<size_t, mmPoolTypeBase*> ComponentPoolMapType;
        typedef std::map<size_t, std::string> IdToNameMapType;
        typedef std::map<std::string, size_t> NameToIdMapType;
    public:
        ComponentPoolMapType d_pool_component;
        IdToNameMapType d_id_to_name_map;
        NameToIdMapType d_name_to_id_map;
        size_t d_chunk_size;
    public:
        mmEntityComponentManager();
        ~mmEntityComponentManager();
    public:
        void SetChunkSize(size_t chunk_size);
    public:
        // add data_type pool.
        template<typename DataType>
        mmPoolTypeBase* Add(size_t id);

        // get data_type pool.
        mmPoolTypeBase* Get(size_t id) const;

        // get_instance data_type pool.
        template<typename DataType>
        mmPoolTypeBase* GetInstance(size_t id);

        // rmv data_type pool.
        void Rmv(size_t id);

        // clear data_type pool.
        void Clear();
    public:
        // produce data type struct.
        template<typename DataType>
        void* Produce();

        // recycle data type struct.
        template<typename DataType>
        void Recycle(void* v);

        // recycle data type struct.
        void Recycle(void* v, size_t id);
    public:
        // registration name -> data_type pool.
        template<typename DataType>
        void Registration(const char* name);

        // cancellation name -> data_type pool.
        void Cancellation(const char* name);

        // get name by id.
        size_t NameToId(const char* name) const;

        // get id by name.
        const char* IdToName(size_t id) const;

        // produce data type struct, by data type id.
        // note: you must registration the data type before use this api.
        void* Produce(size_t id);

        // produce data type struct, by data type name.
        // note: you must registration the data type before use this api.
        void* Produce(const char* name);
    };

    // implement
    template<typename DataType>
    mmPoolTypeBase* mmEntityComponentManager::Add(size_t id)
    {
        typedef mmPoolType<DataType> mmPoolTypeDataType;
        mmPoolTypeBase* e = this->Get(id);
        if (NULL == e)
        {
            mmPoolTypeDataType* pool = new mmPoolTypeDataType;
            pool->SetChunkSize(this->d_chunk_size);
            e = pool;
            this->d_pool_component.insert(ComponentPoolMapType::value_type(id, e));
        }
        return e;
    }
    template<typename DataType>
    mmPoolTypeBase* mmEntityComponentManager::GetInstance(size_t id)
    {
        mmPoolTypeBase* e = this->Get(id);
        if (NULL == e)
        {
            e = this->Add<DataType>(id);
        }
        return e;
    }
    template<typename DataType>
    void* mmEntityComponentManager::Produce()
    {
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::Family();
        void* v = NULL;
        mmPoolTypeBase* e = this->GetInstance<DataType>(id);
        assert(NULL != e && "memory not enough, can not alloc pool.");
        v = e->Produce();
        assert(NULL != v && "memory not enough, can not alloc data.");
        return v;
    }
    template<typename DataType>
    void mmEntityComponentManager::Recycle(void* v)
    {
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::Family();
        this->Recycle(v, id);
    }
    // registration name -> data_type pool.
    template<typename DataType>
    void mmEntityComponentManager::Registration(const char* name)
    {
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::Family();
        this->d_id_to_name_map[id] = name;
        this->d_name_to_id_map[name] = id;
        this->Add<DataType>(id);
    }
}
#endif//__mmEntityComponentManager_h__
