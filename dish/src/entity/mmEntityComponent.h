/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityComponent_h__
#define __mmEntityComponent_h__

#include "core/mmConfig.h"

#include "core/mmPoolElement.h"

#include "entity/mmEntityMask.h"

#include "entity/mmEntityExport.h"

namespace mm
{
    class mmEntity;
    class mmEntityWorld;

    class MM_EXPORT_ENTITY mmEntityComponentBase
    {
    public:
        static size_t d_family_counter;
    public:
        mmEntityComponentBase();
        virtual ~mmEntityComponentBase();
    public:
        // attach entity event.
        virtual void OnAttachEntity(mmEntityWorld* entity_world, mmEntity* entity);

        // detach entity event.
        virtual void OnDetachEntity(mmEntityWorld* entity_world, mmEntity* entity);
    public:
        static size_t FamilyCounter();
    };

    template<typename T>
    class mmEntityComponent : public mmEntityComponentBase
    {
    public:
        typedef T ComponentType;
    public:
        mmEntityComponent() {}
        virtual ~mmEntityComponent() {}
    public:
        static size_t Family();
    };

    // implement
    template<typename C>
    size_t mmEntityComponent<C>::Family()
    {
        static size_t family = mmEntityComponentBase::d_family_counter++;
        return family;
    }
}

#endif//__mmEntityComponent_h__
