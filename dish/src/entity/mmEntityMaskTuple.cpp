/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityMaskTuple.h"

namespace mm
{
    void mmEntityMaskTuple::AddComponentMaskTupleComponent(const mmEntityMask& mask, size_t n)
    {
        MaskSetType& mask_set = this->d_component_mask_tuple[n];
        mask_set.insert(mask);
    }
    void mmEntityMaskTuple::RmvComponentMaskTupleComponent(const mmEntityMask& mask, size_t n)
    {
        MaskSetType& mask_set = this->d_component_mask_tuple[n];
        mask_set.erase(mask);
    }
    mmEntityMaskTuple::EntityTupleType* mmEntityMaskTuple::AttachComponentMaskTuple(const mmEntityMask& mask)
    {
        typedef std::pair<EntityMaskTupleType::iterator, bool> ResultType;
        ResultType result = this->d_entity_mask_tuple.insert(EntityMaskTupleType::value_type(mask, EntityTupleType()));

        size_t n = 0;
        n = mask.find_first();
        while (mmEntityMask::npos != n)
        {
            this->AddComponentMaskTupleComponent(mask, n);
            n = mask.find_next(n);
        }

        return &result.first->second;
    }
    void mmEntityMaskTuple::DetachComponentMaskTuple(const mmEntityMask& mask)
    {
        this->d_entity_mask_tuple.erase(mask);

        size_t n = 0;
        n = mask.find_first();
        while (mmEntityMask::npos != n)
        {
            this->RmvComponentMaskTupleComponent(mask, n);
            n = mask.find_next(n);
        }
    }
    void mmEntityMaskTuple::AddEntityMaskTupleComponent(mmEntity* entity, size_t n)
    {
        const mmEntityMask& mask = entity->d_mask;

        MaskSetType& mask_set = this->d_component_mask_tuple[n];
        for (MaskSetType::iterator it = mask_set.begin();
            it != mask_set.end(); ++it)
        {
            const mmEntityMask& mask_tuple = *it;
            if (mask_tuple.is_subset_of(mask))
            {
                EntityTupleType& entity_tuple = this->d_entity_mask_tuple[mask_tuple];
                mmEntityTuple& tuple = entity_tuple[entity->d_id];
                tuple.Set(n, entity->d_tuple.Get(n));
            }
        }
    }
    void mmEntityMaskTuple::RmvEntityMaskTupleComponent(mmEntity* entity, size_t n)
    {
        const mmEntityMask& mask = entity->d_mask;

        MaskSetType& mask_set = this->d_component_mask_tuple[n];
        for (MaskSetType::iterator it = mask_set.begin();
            it != mask_set.end(); ++it)
        {
            const mmEntityMask& mask_tuple = *it;
            if (!mask_tuple.is_subset_of(mask))
            {
                EntityTupleType& entity_tuple = this->d_entity_mask_tuple[mask_tuple];
                entity_tuple.erase(entity->d_id);
            }
        }
    }

    void mmEntityMaskTuple::AttachEntityMaskTuple(mmEntity* entity)
    {
        const mmEntityMask& mask = entity->d_mask;

        size_t n = 0;
        n = mask.find_first();
        while (mmEntityMask::npos != n)
        {
            this->AddEntityMaskTupleComponent(entity, n);
            n = mask.find_next(n);
        }
    }
    void mmEntityMaskTuple::DetachEntityMaskTuple(mmEntity* entity)
    {
        const mmEntityMask& mask = entity->d_mask;

        size_t n = 0;
        n = mask.find_first();
        while (mmEntityMask::npos != n)
        {
            this->RmvEntityMaskTupleComponent(entity, n);
            n = mask.find_next(n);
        }
    }
}
