/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityMaskTuple_h__
#define __mmEntityMaskTuple_h__

#include "core/mmConfig.h"

#include "dish/mmIdGenerater.h"

#include "entity/mmEntity.h"
#include "entity/mmEntityMask.h"
#include "entity/mmEntityTuple.h"

#include <map>
#include <set>

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntityMaskTuple
    {
    public:
        typedef mmEntity::IdType IdType;
        typedef std::map<IdType, mmEntityTuple> EntityTupleType;
        typedef std::map<mmEntityMask, EntityTupleType> EntityMaskTupleType;
        typedef std::set<mmEntityMask> MaskSetType;
        typedef std::map<size_t, MaskSetType> ComponentMaskTupleType;
    public:
        // mask -> entity tuple.
        EntityMaskTupleType d_entity_mask_tuple;
        // id -> mask set.
        ComponentMaskTupleType d_component_mask_tuple;
    public:
        mmEntityMaskTuple() {}
        ~mmEntityMaskTuple() {}
    public:
        // add component to component_mask_tuple immediately.
        void AddComponentMaskTupleComponent(const mmEntityMask& mask, size_t n);

        // rmv component to component_mask_tuple immediately.
        void RmvComponentMaskTupleComponent(const mmEntityMask& mask, size_t n);

        // attach mask.
        EntityTupleType* AttachComponentMaskTuple(const mmEntityMask& mask);

        // detach mask.
        void DetachComponentMaskTuple(const mmEntityMask& mask);
    public:
        // add entity a component to entity mask.
        void AddEntityMaskTupleComponent(mmEntity* entity, size_t n);

        // rmv entity a component to entity mask.
        void RmvEntityMaskTupleComponent(mmEntity* entity, size_t n);

        // attach entity mask to mask tuple.
        void AttachEntityMaskTuple(mmEntity* entity);

        // detach entity mask to mask tuple.
        void DetachEntityMaskTuple(mmEntity* entity);
    };
}

#endif//__mmEntityMaskTuple_h__
