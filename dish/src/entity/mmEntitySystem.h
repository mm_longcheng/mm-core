/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntitySystem_h__
#define __mmEntitySystem_h__

#include "core/mmConfig.h"

#include "entity/mmEntityMask.h"
#include "entity/mmEntityMaskTuple.h"

#include "entity/mmEntityExport.h"

namespace mm
{
    class mmEntityWorld;

    class MM_EXPORT_ENTITY mmEntitySystemBase
    {
    public:
        typedef mmEntityMaskTuple::EntityTupleType EntityTupleType;
    public:
        static size_t d_family_counter;
    public:
        mmEntityMask d_mask;
        const EntityTupleType* d_entity_tuple;// weak ref.
    public:
        mmEntitySystemBase();
        virtual ~mmEntitySystemBase();
    public:
        virtual void OnAttachWorld(mmEntityWorld* entity_world);
        virtual void OnDetachWorld(mmEntityWorld* entity_world);
    public:
        static size_t FamilyCounter();
    public:
        virtual void AttachSystemMaskTuple();
        virtual void DetachSystemMaskTuple();
    public:
        void SetMask(const mmEntityMask& mask);
        const mmEntityMask& GetMask() const;
    public:
        void SetEntityTuple(const EntityTupleType* entity_tuple);
        const EntityTupleType* GetEntityTuple() const;
    public:
        template<typename data_type>
        void AddEntityComponent();

        void AddEntityComponent(size_t id);

        template<typename data_type>
        void RmvEntityComponent();

        void RmvEntityComponent(size_t id);
    };

    template<typename T>
    class mmEntitySystem : public mmEntitySystemBase
    {
    public:
        typedef T system_type;
    public:
        mmEntitySystem() {}
        virtual ~mmEntitySystem() {}
    public:
        static size_t Family();
    };

    // implement
    template<typename DataType>
    void mmEntitySystemBase::AddEntityComponent()
    {
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::Family();
        this->AddEntityComponent(id);
    }
    template<typename DataType>
    void mmEntitySystemBase::RmvEntityComponent()
    {
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::Family();
        this->RmvEntityComponent(id);
    }
    template<typename C>
    size_t mmEntitySystem<C>::Family()
    {
        static size_t family = mmEntitySystemBase::d_family_counter++;
        return family;
    }
}

#endif//__mmEntitySystem_h__
