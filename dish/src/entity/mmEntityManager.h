/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityManager_h__
#define __mmEntityManager_h__

#include "core/mmConfig.h"

#include "dish/mmIdGenerater.h"

#include "entity/mmEntity.h"
#include "entity/mmEntityMask.h"
#include "entity/mmEntityTuple.h"
#include "entity/mmEntityMaskTuple.h"
#include "entity/mmEntityComponentManager.h"

#include <vector>

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntityManager
    {
    public:
        typedef mmEntity::IdType IdType;
        typedef mmPoolType<mmEntity> EntityPoolType;
        typedef std::vector<mmEntity*> VectorEntityType;
    public:
        struct mmUInt32IdGenerater d_id_generater;
        EntityPoolType d_entity_pool;
        VectorEntityType d_entity;
    public:
        mmEntityManager();
        virtual ~mmEntityManager();
    public:
        void SetChunkSize(size_t chunk_size);

        mmEntity* Produce();
        void Recycle(mmEntity* entity);

        mmEntity* Get(IdType e) const;
        void Clear();
        const VectorEntityType& GetEntityVector() const;
    public:
        void AttachEntityComponent(IdType e, size_t id, void* d);
        void* DetachEntityComponent(IdType e, size_t id);
    };
}

#endif//__mmEntityManager_h__
