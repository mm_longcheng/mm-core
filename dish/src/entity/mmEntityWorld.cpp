/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityWorld.h"

namespace mm
{
    mmEntityWorld::mmEntityWorld()
        : d_world_entity(NULL)
        , d_context(NULL)
    {

    }
    mmEntityWorld::~mmEntityWorld()
    {

    }

    void mmEntityWorld::SetEntityChunkSize(size_t chunk_size)
    {
        this->d_entity_manager.SetChunkSize(chunk_size);
    }

    void mmEntityWorld::SetComponentChunkSize(size_t chunk_size)
    {
        this->d_component_manager.SetChunkSize(chunk_size);
    }

    void mmEntityWorld::SetContext(void* context)
    {
        this->d_context = context;
    }

    void* mmEntityWorld::GetContext() const
    {
        return this->d_context;
    }

    void mmEntityWorld::Launching()
    {
        this->ProduceWorldEntity();
    }

    void mmEntityWorld::Terminate()
    {
        this->RecycleWorldEntity();
    }

    void mmEntityWorld::ProduceWorldEntity()
    {
        this->d_world_entity = this->Produce();
    }

    void mmEntityWorld::RecycleWorldEntity()
    {
        this->Recycle(this->d_world_entity);
        this->d_world_entity = NULL;
    }

    mmEntity* mmEntityWorld::GetWorldEntity() const
    {
        return this->d_world_entity;
    }

    void mmEntityWorld::ClearEntityComponent(mmEntity* entity)
    {
        const mmEntityMask& mask = entity->d_mask;

        size_t n = 0;
        n = mask.find_first();
        while (mmEntityMask::npos != n)
        {
            this->RmvEntityComponent(entity, n);
            n = mask.find_next(n);
        }
    }

    void mmEntityWorld::ClearEntity()
    {
        typedef mmEntityManager::VectorEntityType VectorEntityType;

        mmEntity* entity = NULL;

        const VectorEntityType& vector_entity = this->d_entity_manager.GetEntityVector();
        for (VectorEntityType::const_iterator it = vector_entity.begin();
            it != vector_entity.end(); ++it)
        {
            entity = *it;
            if (NULL != entity)
            {
                this->ClearEntityComponent(entity);
            }
        }

        this->d_entity_manager.Clear();

        // world entity at entity manager.
        this->d_world_entity = NULL;
    }

    void* mmEntityWorld::GetEntitySystem(size_t id) const
    {
        return this->d_system_manager.GetEntitySystem(id);
    }

    mmEntity* mmEntityWorld::Produce()
    {
        return this->d_entity_manager.Produce();
    }

    void mmEntityWorld::Recycle(mmEntity* entity)
    {
        this->ClearEntityComponent(entity);
        this->d_entity_manager.Recycle(entity);
    }

    mmEntity* mmEntityWorld::GetEntity(IdType e) const
    {
        return this->d_entity_manager.Get(e);
    }

    void mmEntityWorld::RmvEntityComponent(IdType e, size_t id)
    {
        mmEntity* entity = this->d_entity_manager.Get(e);
        this->RmvEntityComponent(entity, id);
    }

    void mmEntityWorld::RmvEntityComponent(mmEntity* entity, size_t id)
    {
        IdType e = entity->d_id;
        void* d = this->d_entity_manager.DetachEntityComponent(e, id);
        this->d_component_manager.Recycle(d, id);
        this->RmvEntityMaskTupleComponent(entity, id);
    }

    void* mmEntityWorld::GetEntityComponent(IdType e, size_t id) const
    {
        mmEntity* entity = this->d_entity_manager.Get(e);
        return this->GetEntityComponent(entity, id);
    }

    void* mmEntityWorld::GetEntityComponent(mmEntity* entity, size_t id) const
    {
        return entity->GetComponent(id);
    }

    void mmEntityWorld::ComponentCancellation(const char* name)
    {
        this->d_component_manager.Cancellation(name);
    }

    size_t mmEntityWorld::ComponentNameToId(const char* name) const
    {
        return this->d_component_manager.NameToId(name);
    }

    const char* mmEntityWorld::ComponentIdToName(size_t id) const
    {
        return this->d_component_manager.IdToName(id);
    }

    void* mmEntityWorld::AddEntityComponent(IdType e, size_t id)
    {
        void* d = this->d_component_manager.Produce(id);
        this->d_entity_manager.AttachEntityComponent(e, id, d);
        this->AttachEntityComponentEvent(std::true_type(), e, (mmEntityComponentBase*)d);
        return d;
    }

    void* mmEntityWorld::AddEntityComponent(mmEntity* entity, size_t id)
    {
        return this->AddEntityComponent(entity->d_id, id);
    }

    void* mmEntityWorld::AddEntityComponent(IdType e, const char* name)
    {
        size_t id = this->d_component_manager.NameToId(name);
        return this->AddEntityComponent(e, id);
    }

    void* mmEntityWorld::AddEntityComponent(mmEntity* entity, const char* name)
    {
        return this->AddEntityComponent(entity->d_id, name);
    }

    void mmEntityWorld::RmvEntityComponent(IdType e, const char* name)
    {
        mmEntity* entity = this->d_entity_manager.Get(e);
        size_t id = this->d_component_manager.NameToId(name);
        this->RmvEntityComponent(entity, id);
    }

    void mmEntityWorld::RmvEntityComponent(mmEntity* entity, const char* name)
    {
        size_t id = this->d_component_manager.NameToId(name);
        this->RmvEntityComponent(entity, id);
    }

    void* mmEntityWorld::GetEntityComponent(IdType e, const char* name) const
    {
        size_t id = this->d_component_manager.NameToId(name);
        return this->GetEntityComponent(e, id);
    }

    void mmEntityWorld::SystemCancellation(const char* name)
    {
        this->d_system_manager.Cancellation(name);
    }

    size_t mmEntityWorld::SystemNameToId(const char* name) const
    {
        return this->d_system_manager.NameToId(name);
    }

    const char* mmEntityWorld::SystemIdToName(size_t id) const
    {
        return this->d_system_manager.IdToName(id);
    }

    void* mmEntityWorld::AddEntitySystem(size_t id)
    {
        void* system = this->d_system_manager.AddEntitySystem(id);
        this->AttachEntitySystemEvent(std::true_type(), (mmEntitySystemBase*)system);
        return system;
    }

    void* mmEntityWorld::AddEntitySystem(const char* name)
    {
        size_t id = this->d_system_manager.NameToId(name);
        return this->AddEntitySystem(id);
    }

    void mmEntityWorld::RmvEntitySystem(size_t id)
    {
        void* system = this->d_system_manager.GetEntitySystem(id);
        this->DetachEntitySystemEvent(std::true_type(), (mmEntitySystemBase*)system);
        this->d_system_manager.RmvEntitySystem(id);
    }

    void mmEntityWorld::RmvEntitySystem(const char* name)
    {
        size_t id = this->d_system_manager.NameToId(name);
        return this->RmvEntitySystem(id);
    }

    void* mmEntityWorld::GetEntitySystem(const char* name) const
    {
        size_t id = this->d_system_manager.NameToId(name);
        return this->GetEntitySystem(id);
    }

    void mmEntityWorld::AddEntityMaskTupleComponent(mmEntity* entity, size_t n)
    {
        this->d_mask_tuple.AddEntityMaskTupleComponent(entity, n);
    }

    void mmEntityWorld::RmvEntityMaskTupleComponent(mmEntity* entity, size_t n)
    {
        this->d_mask_tuple.RmvEntityMaskTupleComponent(entity, n);
    }

    void mmEntityWorld::AttachEntityMaskTuple(mmEntity* entity)
    {
        this->d_mask_tuple.AttachEntityMaskTuple(entity);
    }

    void mmEntityWorld::DetachEntityMaskTuple(mmEntity* entity)
    {
        this->d_mask_tuple.DetachEntityMaskTuple(entity);
    }

    void mmEntityWorld::AttachEntityComponentEvent(std::true_type, IdType e, mmEntityComponentBase* d)
    {
        mmEntity* entity = this->d_entity_manager.Get(e);
        d->OnAttachEntity(this, entity);
    }

    void mmEntityWorld::DetachEntityComponentEvent(std::true_type, IdType e, mmEntityComponentBase* d)
    {
        mmEntity* entity = this->d_entity_manager.Get(e);
        d->OnDetachEntity(this, entity);
    }

    void mmEntityWorld::AttachEntitySystemEvent(std::true_type, mmEntitySystemBase* system)
    {
        EntityTupleType* entity_tuple = NULL;
        system->AttachSystemMaskTuple();
        entity_tuple = this->d_mask_tuple.AttachComponentMaskTuple(system->GetMask());
        system->SetEntityTuple(entity_tuple);
        system->OnAttachWorld(this);
    }

    void mmEntityWorld::DetachEntitySystemEvent(std::true_type, mmEntitySystemBase* system)
    {
        system->OnDetachWorld(this);
        system->SetEntityTuple(NULL);
        this->d_mask_tuple.DetachComponentMaskTuple(system->GetMask());
        system->DetachSystemMaskTuple();
    }
}
