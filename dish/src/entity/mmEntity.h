/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntity_h__
#define __mmEntity_h__

#include "core/mmConfig.h"

#include "entity/mmEntityMask.h"
#include "entity/mmEntityTuple.h"

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntity
    {
    public:
        typedef mmUInt32_t IdType;
    public:
        static const IdType INVALID_ID = static_cast<IdType>(-1);
    public:
        IdType d_id;
        mmEntityMask d_mask;
        mmEntityTuple d_tuple;
    public:
        mmEntity();
        virtual ~mmEntity();
    public:
        void SetId(const IdType id);
        IdType GetId() const;
    public:
        void AttachComponent(size_t id, void* d);
        void* DetachComponent(size_t id);
    public:
        template<typename DataType>
        DataType* GetComponent() const;

        void* GetComponent(size_t id) const;
    };

    // implement
    template<typename DataType>
    DataType* mmEntity::GetComponent() const
    {
        return this->d_tuple.GetComponent<DataType>();
    }
}

#endif//__mmEntity_h__
