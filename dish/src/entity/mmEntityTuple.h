/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityTuple_h__
#define __mmEntityTuple_h__

#include "core/mmConfig.h"

#include "entity/mmEntityMask.h"
#include "entity/mmEntityComponent.h"

#include <map>

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntityTuple
    {
    public:
        typedef std::map<size_t, void*> ComponentMapType;
    public:
        ComponentMapType d_component_map;
    public:
        mmEntityTuple() {}
        virtual ~mmEntityTuple() {}
    public:
        void Set(size_t id, void* d);
        void* Rmv(size_t id);
        void* Get(size_t id) const;
        void Clear();
    public:
        template<typename DataType>
        DataType* GetComponent() const;

        void* GetComponent(size_t id) const;
    };

    // implement
    template<typename DataType>
    DataType* mmEntityTuple::GetComponent() const
    {
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::Family();
        return (DataType*)this->Get(id);
    }
}

#endif//__mmEntityTuple_h__
