/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntity.h"

namespace mm
{
    mmEntity::mmEntity()
        : d_id(INVALID_ID)
    {

    }
    mmEntity::~mmEntity()
    {

    }
    void mmEntity::SetId(const IdType id)
    {
        this->d_id = id;
    }
    mmEntity::IdType mmEntity::GetId() const
    {
        return this->d_id;
    }
    void mmEntity::AttachComponent(size_t id, void* d)
    {
        this->d_mask.update_size(id);
        this->d_mask.set(id, 1);
        this->d_tuple.Set(id, d);
    }
    void* mmEntity::DetachComponent(size_t id)
    {
        this->d_mask.update_size(id);
        this->d_mask.set(id, 0);
        return this->d_tuple.Rmv(id);
    }

    void* mmEntity::GetComponent(size_t id) const
    {
        return this->d_tuple.Get(id);
    }
}
