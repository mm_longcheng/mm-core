/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntitySystemManager.h"

namespace mm
{
    mmEntitySystemManager::mmEntitySystemManager()
    {

    }
    mmEntitySystemManager::~mmEntitySystemManager()
    {
        this->ClearEntitySystem();
        this->Clear();
    }
    mmEntityPoolSystemBase* mmEntitySystemManager::Get(size_t id) const
    {
        mmEntityPoolSystemBase* e = NULL;
        SystemPoolMapType::const_iterator it = this->d_pool_system.find(id);
        if (it != this->d_pool_system.end())
        {
            e = it->second;
        }
        return e;
    }
    void mmEntitySystemManager::Rmv(size_t id)
    {
        mmEntityPoolSystemBase* e = NULL;
        SystemPoolMapType::iterator it = this->d_pool_system.find(id);
        if (it != this->d_pool_system.end())
        {
            e = it->second;
            this->d_pool_system.erase(it);
            delete e;
        }
    }
    void mmEntitySystemManager::Clear()
    {
        mmEntityPoolSystemBase* e = NULL;
        SystemPoolMapType::iterator it = this->d_pool_system.begin();
        while (it != this->d_pool_system.end())
        {
            e = it->second;
            this->d_pool_system.erase(it++);
            delete e;
        }
    }
    void mmEntitySystemManager::Recycle(void* v, size_t id)
    {
        mmEntityPoolSystemBase* e = this->Get(id);
        assert(NULL != e && "can not find valid pool type.");
        e->Recycle(v);
    }
    void* mmEntitySystemManager::GetEntitySystem(size_t id) const
    {
        void* system = NULL;
        EntitySystemMapType::const_iterator it = this->d_entity_system.find(id);
        if (it != this->d_entity_system.end())
        {
            system = it->second;
        }
        return system;
    }
    void* mmEntitySystemManager::GetEntitySystem(const char* name) const
    {
        size_t id = this->NameToId(name);
        return this->GetEntitySystem(id);
    }
    void* mmEntitySystemManager::AddEntitySystem(size_t id)
    {
        void* system = this->GetEntitySystem(id);
        if (NULL == system)
        {
            system = this->Produce(id);
            this->AttachEntitySystem(id, system);
        }
        return system;
    }
    void* mmEntitySystemManager::AddEntitySystem(const char* name)
    {
        size_t id = this->NameToId(name);
        return this->AddEntitySystem(id);
    }
    void mmEntitySystemManager::RmvEntitySystem(size_t id)
    {
        void* system = this->DetachEntitySystem(id);
        this->Recycle(system, id);
    }
    void mmEntitySystemManager::RmvEntitySystem(const char* name)
    {
        size_t id = this->NameToId(name);
        this->RmvEntitySystem(id);
    }
    void mmEntitySystemManager::AttachEntitySystem(size_t id, void* system)
    {
        this->d_entity_system.insert(EntitySystemMapType::value_type(id, system));
    }
    void* mmEntitySystemManager::DetachEntitySystem(size_t id)
    {
        void* system = NULL;
        EntitySystemMapType::iterator it = this->d_entity_system.find(id);
        if (it != this->d_entity_system.end())
        {
            system = it->second;
            this->d_entity_system.erase(it);
        }
        return system;
    }
    void mmEntitySystemManager::ClearEntitySystem()
    {
        size_t id = 0;
        void* system = NULL;
        EntitySystemMapType::iterator it = this->d_entity_system.begin();
        while (it != this->d_entity_system.end())
        {
            id = it->first;
            system = it->second;
            this->d_entity_system.erase(it++);
            this->Recycle(system, id);
        }
    }
    void mmEntitySystemManager::Cancellation(const char* name)
    {
        NameToIdMapType::iterator it = this->d_name_to_id_map.find(name);
        if (it != this->d_name_to_id_map.end())
        {
            size_t id = it->second;
            this->d_id_to_name_map.erase(id);
            this->d_name_to_id_map.erase(name);
        }
    }
    // get name by id.
    size_t mmEntitySystemManager::NameToId(const char* name) const
    {
        size_t id = 0;
        NameToIdMapType::const_iterator it = this->d_name_to_id_map.find(name);
        if (it != this->d_name_to_id_map.end())
        {
            id = it->second;
        }
        return id;
    }
    // get id by name.
    const char* mmEntitySystemManager::IdToName(size_t id) const
    {
        const char* name = "";
        IdToNameMapType::const_iterator it = this->d_id_to_name_map.find(id);
        if (it != this->d_id_to_name_map.end())
        {
            name = it->second.c_str();
        }
        return name;
    }
    void* mmEntitySystemManager::Produce(size_t id)
    {
        void* v = NULL;
        mmEntityPoolSystemBase* e = this->Get(id);
        assert(NULL != e && "not find type at registration map.");
        v = e->Produce();
        assert(NULL != v && "memory not enough, can not alloc data.");
        return v;
    }
    // add entity system struct, by entity system name.
    // note: you must registration the entity system before use this api.
    void* mmEntitySystemManager::Produce(const char* name)
    {
        void* v = NULL;
        size_t id = this->NameToId(name);
        v = this->Produce(id);
        return v;
    }
}
