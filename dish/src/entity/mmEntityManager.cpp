/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityManager.h"

namespace mm
{
    mmEntityManager::mmEntityManager()
    {
        mmUInt32IdGenerater_Init(&this->d_id_generater);
    }
    mmEntityManager::~mmEntityManager()
    {
        this->Clear();
        mmUInt32IdGenerater_Destroy(&this->d_id_generater);
    }
    void mmEntityManager::SetChunkSize(size_t chunk_size)
    {
        this->d_entity_pool.SetChunkSize(chunk_size);
    }
    mmEntity* mmEntityManager::Produce()
    {
        mmEntity* entity = NULL;
        IdType e = mmUInt32IdGenerater_Produce(&this->d_id_generater);
        if (e < this->d_entity.size())
        {
            entity = (mmEntity*)this->d_entity_pool.Produce();
            this->d_entity[e] = entity;
            entity->SetId(e);
        }
        else
        {
            entity = (mmEntity*)this->d_entity_pool.Produce();
            size_t capacity = this->d_entity_pool.Capacity();
            this->d_entity.resize(capacity);
            this->d_entity[e] = entity;
            entity->SetId(e);
        }
        return entity;
    }
    void mmEntityManager::Recycle(mmEntity* entity)
    {
        IdType e = entity->GetId();
        mmUInt32IdGenerater_Recycle(&this->d_id_generater, e);
        this->d_entity_pool.Recycle(entity);
        mmUInt32_t id_counter = this->d_id_generater.id_counter;
        size_t capacity = this->d_entity_pool.Capacity();
        size_t size = this->d_entity.size();
        size_t shrink_size = capacity > id_counter ? capacity : id_counter;
        if (shrink_size < size)
        {
            this->d_entity.resize(shrink_size);
            VectorEntityType(this->d_entity).swap(this->d_entity);
        }
        else
        {
            this->d_entity[e] = NULL;
        }
    }
    mmEntity* mmEntityManager::Get(IdType e) const
    {
        assert(e < this->d_entity.size() && "e < this->d_entity.size() is invalid.");
        return this->d_entity[e];
    }
    void mmEntityManager::Clear()
    {
        mmEntity* entity = NULL;
        VectorEntityType::iterator it = this->d_entity.begin();
        while (it != this->d_entity.end())
        {
            entity = *it;
            ++it;
            if (NULL != entity)
            {
                this->Recycle(entity);
            }
        }
    }
    const mmEntityManager::VectorEntityType& mmEntityManager::GetEntityVector() const
    {
        return this->d_entity;
    }
    void mmEntityManager::AttachEntityComponent(IdType e, size_t id, void* d)
    {
        assert(e < this->d_entity.size() && "e < this->d_entity.size() is invalid.");
        mmEntity* entity = this->d_entity[e];
        entity->AttachComponent(id, d);
    }
    void* mmEntityManager::DetachEntityComponent(IdType e, size_t id)
    {
        void* d = NULL;
        mmEntity* entity = this->d_entity[e];
        d = entity->DetachComponent(id);
        return d;
    }
}
