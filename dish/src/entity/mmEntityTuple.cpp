/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntityTuple.h"

namespace mm
{
    void mmEntityTuple::Set(size_t id, void* d)
    {
        this->d_component_map.insert(ComponentMapType::value_type(id, d));
    }
    void* mmEntityTuple::Rmv(size_t id)
    {
        void* d = NULL;
        ComponentMapType::iterator it = this->d_component_map.find(id);
        if (it != this->d_component_map.end())
        {
            d = it->second;
            this->d_component_map.erase(it);
        }
        return d;
    }
    void* mmEntityTuple::Get(size_t id) const
    {
        void* d = NULL;
        ComponentMapType::const_iterator it = this->d_component_map.find(id);
        if (it != this->d_component_map.end())
        {
            d = it->second;
        }
        return d;
    }
    void mmEntityTuple::Clear()
    {
        this->d_component_map.clear();
    }

    void* mmEntityTuple::GetComponent(size_t id) const
    {
        return this->Get(id);
    }
}
