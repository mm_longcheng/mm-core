/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntitySystemManager_h__
#define __mmEntitySystemManager_h__

#include "core/mmConfig.h"

#include "entity/mmEntitySystem.h"

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntityPoolSystemBase
    {
    public:
        mmEntityPoolSystemBase() {}
        virtual ~mmEntityPoolSystemBase() {}
    public:
        virtual void* Produce() = 0;
        virtual void Recycle(void* v) = 0;
    };

    template<typename T>
    class mmEntityPoolSystem : public mmEntityPoolSystemBase
    {
    public:
        typedef T SystemType;
    public:
        mmEntityPoolSystem() {}
        virtual ~mmEntityPoolSystem() {}
    public:
        virtual void* Produce();
        virtual void Recycle(void* v);
    };

    class MM_EXPORT_ENTITY mmEntitySystemManager
    {
    public:
        typedef std::map<size_t, mmEntityPoolSystemBase*> SystemPoolMapType;
        typedef std::map<size_t, void*> EntitySystemMapType;
        typedef std::map<size_t, std::string> IdToNameMapType;
        typedef std::map<std::string, size_t> NameToIdMapType;
    public:
        SystemPoolMapType d_pool_system;
        EntitySystemMapType d_entity_system;
        IdToNameMapType d_id_to_name_map;
        NameToIdMapType d_name_to_id_map;
    public:
        mmEntitySystemManager();
        virtual ~mmEntitySystemManager();
    public:
        // add entity_system pool.
        template<typename SystemType>
        mmEntityPoolSystemBase* Add(size_t id);

        // get entity_system pool.
        mmEntityPoolSystemBase* Get(size_t id) const;

        // get_instance entity_system pool.
        template<typename SystemType>
        mmEntityPoolSystemBase* GetInstance(size_t id);

        // rmv entity_system pool.
        void Rmv(size_t id);

        // clear entity_system pool.
        void Clear();
    public:
        // produce data type struct.
        template<typename SystemType>
        void* Produce();

        // recycle data type struct.
        template<typename SystemType>
        void Recycle(void* v);

        // recycle data type struct.
        void Recycle(void* v, size_t id);
    public:
        // add system by type.
        template<typename SystemType>
        SystemType* AddEntitySystem();

        // get system by type.
        template<typename SystemType>
        SystemType* GetEntitySystem() const;

        // rmv system by type.
        template<typename SystemType>
        void RmvEntitySystem();

        // get system by type id.
        void* GetEntitySystem(size_t id) const;

        // get system by type name.
        void* GetEntitySystem(const char* name) const;

        // add system by type id.
        // note: you must registration the entity system before use this api.
        void* AddEntitySystem(size_t id);

        // add system by type name.
        // note: you must registration the entity system before use this api.
        void* AddEntitySystem(const char* name);

        // rmv system by type id.
        // note: you must registration the entity system before use this api.
        void RmvEntitySystem(size_t id);

        // rmv system by type name.
        // note: you must registration the entity system before use this api.
        void RmvEntitySystem(const char* name);
    public:
        void AttachEntitySystem(size_t id, void* system);
        void* DetachEntitySystem(size_t id);
    public:
        void ClearEntitySystem();
    public:
        // registration name -> data_type pool.
        template<typename SystemType>
        void Registration(const char* name);

        // cancellation name -> data_type pool.
        void Cancellation(const char* name);

        // get name by id.
        size_t NameToId(const char* name) const;

        // get id by name.
        const char* IdToName(size_t id) const;

        // add entity system struct, by entity system id.
        // note: you must registration the entity system before use this api.
        void* Produce(size_t id);

        // add entity system struct, by entity system name.
        // note: you must registration the entity system before use this api.
        void* Produce(const char* name);
    };

    // implement
    template<typename T>
    void* mmEntityPoolSystem<T>::Produce()
    {
        T* system = new T();
        return system;
    }
    template<typename T>
    void mmEntityPoolSystem<T>::Recycle(void* v)
    {
        T* system = (T*)(v);
        delete system;
    }

    template<typename SystemType>
    mmEntityPoolSystemBase* mmEntitySystemManager::Add(size_t id)
    {
        typedef mmEntityPoolSystem<SystemType> mmEntityPoolSystemType;
        mmEntityPoolSystemBase* e = this->Get(id);
        if (NULL == e)
        {
            mmEntityPoolSystemType* pool = new mmEntityPoolSystemType;
            e = pool;
            this->d_pool_system.insert(SystemPoolMapType::value_type(id, e));
        }
        return e;
    }
    template<typename SystemType>
    mmEntityPoolSystemBase* mmEntitySystemManager::GetInstance(size_t id)
    {
        mmEntityPoolSystemBase* e = this->Get(id);
        if (NULL == e)
        {
            e = this->Add<SystemType>(id);
        }
        return e;
    }
    // produce data type struct.
    template<typename SystemType>
    void* mmEntitySystemManager::Produce()
    {
        typedef mmEntitySystem<SystemType> EntitySystemType;
        size_t id = EntitySystemType::Family();
        void* v = NULL;
        mmEntityPoolSystemBase* e = this->GetInstance<SystemType>(id);
        assert(NULL != e && "memory not enough, can not alloc pool.");
        v = e->Produce();
        assert(NULL != v && "memory not enough, can not alloc data.");
        return v;
    }
    // recycle data type struct.
    template<typename SystemType>
    void mmEntitySystemManager::Recycle(void* v)
    {
        typedef mmEntityComponent<SystemType> EntitySystemType;
        size_t id = EntitySystemType::Family();
        this->Recycle(v, id);
    }
    template<typename SystemType>
    SystemType* mmEntitySystemManager::AddEntitySystem()
    {
        typedef mmEntitySystem<SystemType> EntitySystemType;
        size_t id = EntitySystemType::Family();
        void* system = this->GetEntitySystem(id);
        if (NULL == system)
        {
            system = this->Produce<SystemType>();
            this->AttachEntitySystem(id, system);
        }
        return (SystemType*)system;
    }
    template<typename SystemType>
    SystemType* mmEntitySystemManager::GetEntitySystem() const
    {
        typedef mmEntitySystem<SystemType> EntitySystemType;
        size_t id = EntitySystemType::Family();
        void* system = this->GetEntitySystem(id);
        return (SystemType*)system;
    }
    template<typename SystemType>
    void mmEntitySystemManager::RmvEntitySystem()
    {
        typedef mmEntitySystem<SystemType> EntitySystemType;
        size_t id = EntitySystemType::Family();
        this->RmvEntitySystem(id);
    }
    template<typename SystemType>
    void mmEntitySystemManager::Registration(const char* name)
    {
        typedef mmEntitySystem<SystemType> EntitySystemType;
        size_t id = EntitySystemType::Family();
        this->d_id_to_name_map[id] = name;
        this->d_name_to_id_map[name] = id;
        this->Add<SystemType>(id);
    }
}

#endif//__mmEntitySystemManager_h__
