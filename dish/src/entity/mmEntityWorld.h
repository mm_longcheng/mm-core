/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmEntityWorld_h__
#define __mmEntityWorld_h__

#include "core/mmConfig.h"

#include "entity/mmEntityManager.h"
#include "entity/mmEntitySystemManager.h"
#include "entity/mmEntityComponentManager.h"

#include <vector>

#include "entity/mmEntityExport.h"

namespace mm
{
    class MM_EXPORT_ENTITY mmEntityWorld
    {
    public:
        typedef mmEntity::IdType IdType;
        typedef mmEntityMaskTuple::EntityTupleType EntityTupleType;
    public:
        mmEntityManager d_entity_manager;
        mmEntityComponentManager d_component_manager;
        mmEntitySystemManager d_system_manager;
        mmEntityMaskTuple d_mask_tuple;
        mmEntity* d_world_entity;
        void* d_context;// weak ref.
    public:
        mmEntityWorld();
        virtual ~mmEntityWorld();
    public:
        // assign entity chunk size. default is MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT.
        void SetEntityChunkSize(size_t chunk_size);

        // assign component chunk size. default is MM_POOL_ELEMENT_CHUNK_SIZE_DEFAULT.
        void SetComponentChunkSize(size_t chunk_size);
    public:
        void SetContext(void* context);
        void* GetContext() const;
    public:
        // launching the system.
        void Launching();

        // terminate the system.
        void Terminate();
    public:
        // entity world is a entity, we need the entity for hold such as singleton component.
        // produce world entity.
        void ProduceWorldEntity();

        // recycle world entity.
        void RecycleWorldEntity();

        // get world entity.
        mmEntity* GetWorldEntity() const;
    public:
        // clear entity component.
        // note: this api will rmv the component data, and rmv the mask tuple component.
        void ClearEntityComponent(mmEntity* entity);

        // clear all world entity.
        void ClearEntity();
    public:
        // add entity system to world.
        template<typename SystemType>
        SystemType* AddEntitySystem();

        // get entity system to world.
        template<typename SystemType>
        SystemType* GetEntitySystem() const;

        // get entity system to world.
        void* GetEntitySystem(size_t id) const;

        // rmv entity system to world.
        template<typename SystemType>
        void RmvEntitySystem();
    public:
        // produce entity. 
        mmEntity* Produce();

        // recycle entity. 
        void Recycle(mmEntity* entity);

        // get entity by entity id. 
        mmEntity* GetEntity(IdType e) const;

        // add entity component by entity id and data type.
        // note: this api only add the component data, not add the mask tuple component.
        template<typename DataType>
        DataType* AddEntityComponent(IdType e);

        // add entity component by entity id and data type.
        // note: this api only add the component data, not add the mask tuple component.
        template<typename DataType>
        DataType* AddEntityComponent(mmEntity* entity);

        // rmv entity component by entity id and data type.
        // note: this api only rmv the component data, not rmv the mask tuple component.
        template<typename DataType>
        void RmvEntityComponent(IdType e);

        // rmv entity component by entity id and data type.
        // note: this api only rmv the component data, not rmv the mask tuple component.
        template<typename DataType>
        void RmvEntityComponent(mmEntity* entity);

        // rmv entity component by entity id and data type id.
        // note: this api only rmv the component data, not rmv the mask tuple component.
        void RmvEntityComponent(IdType e, size_t id);

        // rmv entity component by entity and data type id.
        // note: this api will rmv the component data, and rmv the mask tuple component.
        void RmvEntityComponent(mmEntity* entity, size_t id);

        // get entity component by data type id.
        void* GetEntityComponent(IdType e, size_t id) const;

        // get entity component by data type id.
        void* GetEntityComponent(mmEntity* entity, size_t id) const;
    public:
        // component registration name -> data_type pool.
        template<typename DataType>
        void ComponentRegistration(const char* name);

        // component cancellation name -> data_type pool.
        void ComponentCancellation(const char* name);

        // component get name by id.
        size_t ComponentNameToId(const char* name) const;

        // component get id by name.
        const char* ComponentIdToName(size_t id) const;

        // add entity component by entity id and data type id.
        // note: this api only add the component data, not add the mask tuple component.
        //       you must registration the data type before use this api.
        //       the data type must base of mmEntityComponentBase.
        void* AddEntityComponent(IdType e, size_t id);

        // add entity component by entity id and data type id.
        // note: this api only add the component data, not add the mask tuple component.
        //       you must registration the data type before use this api.
        //       the data type must base of mmEntityComponentBase.
        void* AddEntityComponent(mmEntity* entity, size_t id);

        // add entity component by entity id and data type name.
        // note: this api only add the component data, not add the mask tuple component.
        //       you must registration the data type before use this api.
        //       the data type must base of mmEntityComponentBase.
        void* AddEntityComponent(IdType e, const char* name);

        // add entity component by entity id and data type name.
        // note: this api only add the component data, not add the mask tuple component.
        //       you must registration the data type before use this api.
        //       the data type must base of mmEntityComponentBase.
        void* AddEntityComponent(mmEntity* entity, const char* name);

        // rmv entity component by entity id and data type name.
        // note: this api only add the component data, not add the mask tuple component.
        //       you must registration the data type before use this api.
        //       the data type must base of mmEntityComponentBase.
        void RmvEntityComponent(IdType e, const char* name);

        // rmv entity component by entity id and data type name.
        // note: this api only add the component data, not add the mask tuple component.
        //       you must registration the data type before use this api.
        //       the data type must base of mmEntityComponentBase.
        void RmvEntityComponent(mmEntity* entity, const char* name);

        // get entity component by data type name.
        void* GetEntityComponent(IdType e, const char* name) const;
    public:
        // registration name -> data_type pool.
        template<typename SystemType>
        void SystemRegistration(const char* name);

        // cancellation name -> data_type pool.
        void SystemCancellation(const char* name);

        // get name by id.
        size_t SystemNameToId(const char* name) const;

        // get id by name.
        const char* SystemIdToName(size_t id) const;

        // add system by type id.
        // note: you must registration the entity system before use this api.
        //       you must registration the system type before use this api.
        //       the data type must base of mmEntitySystemBase.
        void* AddEntitySystem(size_t id);

        // add system by type name.
        // note: you must registration the entity system before use this api.
        //       you must registration the system type before use this api.
        //       the data type must base of mmEntitySystemBase.
        void* AddEntitySystem(const char* name);

        // rmv system by type id.
        // note: you must registration the entity system before use this api.
        //       you must registration the system type before use this api.
        //       the data type must base of mmEntitySystemBase.
        void RmvEntitySystem(size_t id);

        // rmv system by type name.
        // note: you must registration the entity system before use this api.
        //       you must registration the system type before use this api.
        //       the data type must base of mmEntitySystemBase.
        void RmvEntitySystem(const char* name);

        // get system by type name.
        void* GetEntitySystem(const char* name) const;
    public:
        // add data type id to mask tuple component.
        void AddEntityMaskTupleComponent(mmEntity* entity, size_t n);

        // rmv data type id to mask tuple component.
        void RmvEntityMaskTupleComponent(mmEntity* entity, size_t n);

        // attach all data type id by the entity mask to mask tuple component.
        void AttachEntityMaskTuple(mmEntity* entity);

        // detach all data type id by the entity mask to mask tuple component.
        void DetachEntityMaskTuple(mmEntity* entity);
    public:
        // attach entity component event.
        template<typename DataType>
        void AttachEntityComponentEvent(std::false_type, IdType e, DataType* d);

        // attach entity component event. specialization mmEntityComponentBase.
        void AttachEntityComponentEvent(std::true_type, IdType e, mmEntityComponentBase* d);

        // detach entity component event.
        template<typename DataType>
        void DetachEntityComponentEvent(std::false_type, IdType e, DataType* d);

        // detach entity component event. specialization mmEntityComponentBase.
        void DetachEntityComponentEvent(std::true_type, IdType e, mmEntityComponentBase* d);
    public:
        // attach entity system event.
        template<typename SystemType>
        void AttachEntitySystemEvent(std::false_type, SystemType* system);

        // attach entity system event. specialization mmEntitySystemBase.
        void AttachEntitySystemEvent(std::true_type, mmEntitySystemBase* system);

        // detach entity system event.
        template<typename SystemType>
        void DetachEntitySystemEvent(std::false_type, SystemType* system);

        // detach entity system event. specialization mmEntitySystemBase.
        void DetachEntitySystemEvent(std::true_type, mmEntitySystemBase* system);
    };

    // implement
    template<typename SystemType>
    SystemType* mmEntityWorld::AddEntitySystem()
    {
        typedef std::integral_constant< bool, std::is_base_of<mmEntitySystemBase, SystemType>::value > BoolType;
        SystemType* system = this->d_system_manager.AddEntitySystem<SystemType>();
        this->AttachEntitySystemEvent(BoolType(), system);
        return system;
    }
    template<typename SystemType>
    SystemType* mmEntityWorld::GetEntitySystem() const
    {
        return this->d_system_manager.GetEntitySystem<SystemType>();
    }
    template<typename SystemType>
    void mmEntityWorld::RmvEntitySystem()
    {
        typedef std::integral_constant< bool, std::is_base_of<mmEntitySystemBase, SystemType>::value > BoolType;
        typedef mmEntitySystem<SystemType> EntitySystemType;
        size_t id = EntitySystemType::family();
        void* v = this->d_system_manager.GetEntitySystem(id);
        mmEntitySystemBase* system = (mmEntitySystemBase*)(v);
        this->DetachEntitySystemEvent(BoolType(), system);
        this->d_system_manager.RmvEntitySystem(id);
    }
    template<typename DataType>
    DataType* mmEntityWorld::AddEntityComponent(IdType e)
    {
        typedef std::integral_constant< bool, std::is_base_of<mmEntityComponentBase, DataType>::value > BoolType;
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::family();
        void* d = this->d_component_manager.Produce<DataType>();
        this->d_entity_manager.AttachEntityComponent(e, id, d);
        this->AttachEntityComponentEvent(BoolType(), e, (DataType*)d);
        return (DataType*)d;
    }
    template<typename DataType>
    DataType* mmEntityWorld::AddEntityComponent(mmEntity* entity)
    {
        return this->AddEntityComponent<DataType>(entity->d_id);
    }
    template<typename DataType>
    void mmEntityWorld::RmvEntityComponent(IdType e)
    {
        typedef std::integral_constant< bool, std::is_base_of<mmEntityComponentBase, DataType>::value > BoolType;
        typedef mmEntityComponent<DataType> ComponentType;
        size_t id = ComponentType::family();
        void* d = this->d_entity_manager.DetachEntityComponent(e, id);
        this->DetachEntityComponentEvent(BoolType(), e, (DataType*)d);
        this->d_component_manager.Recycle(d, id);
    }
    template<typename DataType>
    void mmEntityWorld::RmvEntityComponent(mmEntity* entity)
    {
        return this->RmvEntityComponent<DataType>(entity->d_id);
    }
    template<typename DataType>
    void mmEntityWorld::ComponentRegistration(const char* name)
    {
        this->d_component_manager.Registration<DataType>(name);
    }
    template<typename SystemType>
    void mmEntityWorld::SystemRegistration(const char* name)
    {
        this->d_system_manager.Registration<SystemType>(name);
    }
    template<typename DataType>
    void mmEntityWorld::AttachEntityComponentEvent(std::false_type, IdType e, DataType* d)
    {
        // do nothing here.
    }
    template<typename DataType>
    void mmEntityWorld::DetachEntityComponentEvent(std::false_type, IdType e, DataType* d)
    {
        // do nothing here.
    }
    template<typename SystemType>
    void mmEntityWorld::AttachEntitySystemEvent(std::false_type, SystemType* system)
    {
        // do nothing here.
    }
    template<typename SystemType>
    void mmEntityWorld::DetachEntitySystemEvent(std::false_type, SystemType* system)
    {
        // do nothing here.
    }

}

#endif//__mmEntityWorld_h__
