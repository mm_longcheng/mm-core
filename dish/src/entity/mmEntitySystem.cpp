/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmEntitySystem.h"

namespace mm
{
    size_t mmEntitySystemBase::d_family_counter = 0;

    mmEntitySystemBase::mmEntitySystemBase()
        : d_entity_tuple(NULL)
    {

    }

    mmEntitySystemBase::~mmEntitySystemBase()
    {

    }

    void mmEntitySystemBase::OnAttachWorld(mmEntityWorld* entity_world)
    {

    }

    void mmEntitySystemBase::OnDetachWorld(mmEntityWorld* entity_world)
    {

    }

    size_t mmEntitySystemBase::FamilyCounter()
    {
        return mmEntitySystemBase::d_family_counter;
    }

    void mmEntitySystemBase::AttachSystemMaskTuple()
    {

    }

    void mmEntitySystemBase::DetachSystemMaskTuple()
    {

    }

    void mmEntitySystemBase::SetMask(const mmEntityMask& mask)
    {
        this->d_mask = mask;
    }

    const mmEntityMask& mmEntitySystemBase::GetMask() const
    {
        return this->d_mask;
    }

    void mmEntitySystemBase::SetEntityTuple(const EntityTupleType* entity_tuple)
    {
        this->d_entity_tuple = entity_tuple;
    }

    const mmEntitySystemBase::EntityTupleType* mmEntitySystemBase::GetEntityTuple() const
    {
        return this->d_entity_tuple;
    }

    void mmEntitySystemBase::AddEntityComponent(size_t id)
    {
        this->d_mask.update_size(id);
        this->d_mask.set(id, 1);
    }
    void mmEntitySystemBase::RmvEntityComponent(size_t id)
    {
        this->d_mask.update_size(id);
        this->d_mask.set(id, 0);
    }
}
