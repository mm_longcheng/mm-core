/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFix32_h__
#define __mmFix32_h__

#include "core/mmCore.h"
#include "core/mmValueTransform.h"

#include "math/mmMathFix32.h"

#include <ostream>
#include <string>

#include "fix32/mmFix32Export.h"

namespace mm
{
    class MM_EXPORT_FIX32 mmFix32
    {
    public:
        static const mmFix32 MM_PI;
        static const mmFix32 MM_PI_DIV_2;
        static const mmFix32 MM_PI_DIV_4;
        static const mmFix32 MM_1_DIV_PI;
        static const mmFix32 MM_2_DIV_PI;
        static const mmFix32 MM_2_MUL_PI;

        static const mmFix32 MM_PI_DIV_180;
        static const mmFix32 MM_180_DIV_PI;

        static const mmFix32 MM_E;
    public:
        mmFix32_t d_value;
    public:
        mmFix32() : d_value(0) {}
        mmFix32(const mmFix32& inValue) : d_value(inValue.d_value) {}
        mmFix32(const mmFix32_t inValue) : d_value(inValue) {}
        mmFix32(const float inValue) : d_value(mmFix32_Dbl2Fix(inValue)) {}
        mmFix32(const double inValue) : d_value(mmFix32_Dbl2Fix(inValue)) {}
        mmFix32(const mmSInt16_t inValue) : d_value(mmFix32_Int2Fix((int)inValue)) {}
        mmFix32(const mmSInt32_t inValue) : d_value(mmFix32_Int2Fix((int)inValue)) {}

        operator mmFix32_t() const { return this->d_value; }
        operator double()  const { return mmFix32_Fix2Dbl(this->d_value); }
        operator float()   const { return (float)mmFix32_Fix2Dbl(this->d_value); }
        operator mmSInt16_t() const { return (mmSInt16_t)mmFix32_Fix2Int(this->d_value); }
        operator mmSInt32_t() const { return (mmSInt32_t)mmFix32_Fix2Int(this->d_value); }

        mmFix32& operator=(const mmFix32& rhs) { this->d_value = rhs.d_value;             return *this; }
        mmFix32& operator=(const mmFix32_t rhs) { this->d_value = rhs;                   return *this; }
        mmFix32& operator=(const double rhs) { this->d_value = mmFix32_Dbl2Fix(rhs);   return *this; }
        mmFix32& operator=(const float rhs) { this->d_value = mmFix32_Dbl2Fix(rhs); return *this; }
        mmFix32& operator=(const mmSInt16_t rhs) { this->d_value = mmFix32_Int2Fix((int)rhs);   return *this; }
        mmFix32& operator=(const mmSInt32_t rhs) { this->d_value = mmFix32_Int2Fix((int)rhs);   return *this; }

        mmFix32& operator+=(const mmFix32& rhs) { this->d_value += rhs.d_value;             return *this; }
        mmFix32& operator+=(const mmFix32_t rhs) { this->d_value += rhs;                   return *this; }
        mmFix32& operator+=(const double rhs) { this->d_value += mmFix32_Dbl2Fix(rhs);   return *this; }
        mmFix32& operator+=(const float rhs) { this->d_value += mmFix32_Dbl2Fix(rhs); return *this; }
        mmFix32& operator+=(const mmSInt16_t rhs) { this->d_value += mmFix32_Int2Fix((int)rhs);   return *this; }
        mmFix32& operator+=(const mmSInt32_t rhs) { this->d_value += mmFix32_Int2Fix((int)rhs);   return *this; }

        mmFix32& operator-=(const mmFix32& rhs) { this->d_value -= rhs.d_value; return *this; }
        mmFix32& operator-=(const mmFix32_t rhs) { this->d_value -= rhs; return *this; }
        mmFix32& operator-=(const double rhs) { this->d_value -= mmFix32_Dbl2Fix(rhs); return *this; }
        mmFix32& operator-=(const float rhs) { this->d_value -= mmFix32_Dbl2Fix(rhs); return *this; }
        mmFix32& operator-=(const mmSInt16_t rhs) { this->d_value -= mmFix32_Int2Fix((int)rhs); return *this; }
        mmFix32& operator-=(const mmSInt32_t rhs) { this->d_value -= mmFix32_Int2Fix((int)rhs); return *this; }

        mmFix32& operator*=(const mmFix32& rhs) { this->d_value = mmFix32_Mul(this->d_value, rhs.d_value); return *this; }
        mmFix32& operator*=(const mmFix32_t rhs) { this->d_value = mmFix32_Mul(this->d_value, rhs); return *this; }
        mmFix32& operator*=(const double rhs) { this->d_value = mmFix32_Mul(this->d_value, mmFix32_Dbl2Fix(rhs)); return *this; }
        mmFix32& operator*=(const float rhs) { this->d_value = mmFix32_Mul(this->d_value, mmFix32_Dbl2Fix(rhs)); return *this; }
        mmFix32& operator*=(const mmSInt16_t rhs) { this->d_value = mmFix32_Mul(this->d_value, mmFix32_Int2Fix((int)rhs)); return *this; }
        mmFix32& operator*=(const mmSInt32_t rhs) { this->d_value = mmFix32_Mul(this->d_value, mmFix32_Int2Fix((int)rhs)); return *this; }

        mmFix32& operator/=(const mmFix32& rhs) { this->d_value = mmFix32_Div(this->d_value, rhs.d_value); return *this; }
        mmFix32& operator/=(const mmFix32_t rhs) { this->d_value = mmFix32_Div(this->d_value, rhs); return *this; }
        mmFix32& operator/=(const double rhs) { this->d_value = mmFix32_Div(this->d_value, mmFix32_Dbl2Fix(rhs)); return *this; }
        mmFix32& operator/=(const float rhs) { this->d_value = mmFix32_Div(this->d_value, mmFix32_Dbl2Fix(rhs)); return *this; }
        mmFix32& operator/=(const mmSInt16_t rhs) { this->d_value = mmFix32_Div(this->d_value, mmFix32_Int2Fix((int)rhs)); return *this; }
        mmFix32& operator/=(const mmSInt32_t rhs) { this->d_value = mmFix32_Div(this->d_value, mmFix32_Int2Fix((int)rhs)); return *this; }

        mmFix32& operator%=(const mmFix32& rhs) { this->d_value = mmFix32_Fmod(this->d_value, rhs.d_value); return *this; }
        mmFix32& operator%=(const mmFix32_t rhs) { this->d_value = mmFix32_Fmod(this->d_value, rhs); return *this; }
        mmFix32& operator%=(const double rhs) { this->d_value = mmFix32_Fmod(this->d_value, mmFix32_Dbl2Fix(rhs)); return *this; }
        mmFix32& operator%=(const float rhs) { this->d_value = mmFix32_Fmod(this->d_value, mmFix32_Dbl2Fix(rhs)); return *this; }
        mmFix32& operator%=(const mmSInt16_t rhs) { this->d_value = mmFix32_Fmod(this->d_value, mmFix32_Int2Fix((int)rhs)); return *this; }
        mmFix32& operator%=(const mmSInt32_t rhs) { this->d_value = mmFix32_Fmod(this->d_value, mmFix32_Int2Fix((int)rhs)); return *this; }

        const int operator==(const mmFix32& other)  const { return (this->d_value == other.d_value); }
        const int operator==(const mmFix32_t other) const { return (this->d_value == other); }
        const int operator==(const double other)  const { return (this->d_value == mmFix32_Dbl2Fix(other)); }
        const int operator==(const float other)   const { return (this->d_value == mmFix32_Dbl2Fix(other)); }
        const int operator==(const mmSInt16_t other) const { return (this->d_value == mmFix32_Int2Fix((int)other)); }
        const int operator==(const mmSInt32_t other) const { return (this->d_value == mmFix32_Int2Fix((int)other)); }

        const int operator!=(const mmFix32& other)  const { return (this->d_value != other.d_value); }
        const int operator!=(const mmFix32_t other) const { return (this->d_value != other); }
        const int operator!=(const double other)  const { return (this->d_value != mmFix32_Dbl2Fix(other)); }
        const int operator!=(const float other)   const { return (this->d_value != mmFix32_Dbl2Fix(other)); }
        const int operator!=(const mmSInt16_t other) const { return (this->d_value != mmFix32_Int2Fix((int)other)); }
        const int operator!=(const mmSInt32_t other) const { return (this->d_value != mmFix32_Int2Fix((int)other)); }

        const int operator<=(const mmFix32& other)  const { return (this->d_value <= other.d_value); }
        const int operator<=(const mmFix32_t other) const { return (this->d_value <= other); }
        const int operator<=(const double other)  const { return (this->d_value <= mmFix32_Dbl2Fix(other)); }
        const int operator<=(const float other)   const { return (this->d_value <= mmFix32_Dbl2Fix(other)); }
        const int operator<=(const mmSInt16_t other) const { return (this->d_value <= mmFix32_Int2Fix((int)other)); }
        const int operator<=(const mmSInt32_t other) const { return (this->d_value <= mmFix32_Int2Fix((int)other)); }

        const int operator>=(const mmFix32& other)  const { return (this->d_value >= other.d_value); }
        const int operator>=(const mmFix32_t other) const { return (this->d_value >= other); }
        const int operator>=(const double other)  const { return (this->d_value >= mmFix32_Dbl2Fix(other)); }
        const int operator>=(const float other)   const { return (this->d_value >= mmFix32_Dbl2Fix(other)); }
        const int operator>=(const mmSInt16_t other) const { return (this->d_value >= mmFix32_Int2Fix((int)other)); }
        const int operator>=(const mmSInt32_t other) const { return (this->d_value >= mmFix32_Int2Fix((int)other)); }

        const int operator< (const mmFix32& other)  const { return (this->d_value < other.d_value); }
        const int operator< (const mmFix32_t other) const { return (this->d_value < other); }
        const int operator< (const double other)  const { return (this->d_value < mmFix32_Dbl2Fix(other)); }
        const int operator< (const float other)   const { return (this->d_value < mmFix32_Dbl2Fix(other)); }
        const int operator< (const mmSInt16_t other) const { return (this->d_value < mmFix32_Int2Fix((int)other)); }
        const int operator< (const mmSInt32_t other) const { return (this->d_value < mmFix32_Int2Fix((int)other)); }

        const int operator> (const mmFix32& other)  const { return (this->d_value > other.d_value); }
        const int operator> (const mmFix32_t other) const { return (this->d_value > other); }
        const int operator> (const double other)  const { return (this->d_value > mmFix32_Dbl2Fix(other)); }
        const int operator> (const float other)   const { return (this->d_value > mmFix32_Dbl2Fix(other)); }
        const int operator> (const mmSInt16_t other) const { return (this->d_value > mmFix32_Int2Fix((int)other)); }
        const int operator> (const mmSInt32_t other) const { return (this->d_value > mmFix32_Int2Fix((int)other)); }

        const mmFix32& operator + () const { return *this; }
        mmFix32 operator - () const { return -this->d_value; }
    public:
        inline friend mmFix32 operator * (mmFix32 lhs, mmFix32 rhs) { return mmFix32_Mul(lhs.d_value, rhs.d_value); }
        inline friend mmFix32 operator * (mmFix32_t lhs, mmFix32 rhs) { return mmFix32_Mul(lhs, rhs.d_value); }
        inline friend mmFix32 operator * (mmFix32 lhs, mmFix32_t rhs) { return mmFix32_Mul(lhs.d_value, rhs); }
        inline friend mmFix32 operator * (double lhs, mmFix32 rhs) { return mmFix32_Mul(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator * (mmFix32 lhs, double rhs) { return mmFix32_Mul(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator * (float lhs, mmFix32 rhs) { return mmFix32_Mul(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator * (mmFix32 lhs, float rhs) { return mmFix32_Mul(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator * (mmSInt16_t lhs, mmFix32 rhs) { return mmFix32_Mul(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator * (mmFix32 lhs, mmSInt16_t rhs) { return mmFix32_Mul(lhs, mmFix32_Int2Fix(rhs)); }
        inline friend mmFix32 operator * (mmSInt32_t lhs, mmFix32 rhs) { return mmFix32_Mul(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator * (mmFix32 lhs, mmSInt32_t rhs) { return mmFix32_Mul(lhs, mmFix32_Int2Fix(rhs)); }

        inline friend mmFix32 operator / (mmFix32 lhs, mmFix32 rhs) { return mmFix32_Div(lhs.d_value, rhs.d_value); }
        inline friend mmFix32 operator / (mmFix32_t lhs, mmFix32 rhs) { return mmFix32_Div(lhs, rhs.d_value); }
        inline friend mmFix32 operator / (mmFix32 lhs, mmFix32_t rhs) { return mmFix32_Div(lhs.d_value, rhs); }
        inline friend mmFix32 operator / (double lhs, mmFix32 rhs) { return mmFix32_Div(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator / (mmFix32 lhs, double rhs) { return mmFix32_Div(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator / (float lhs, mmFix32 rhs) { return mmFix32_Div(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator / (mmFix32 lhs, float rhs) { return mmFix32_Div(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator / (mmSInt16_t lhs, mmFix32 rhs) { return mmFix32_Div(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator / (mmFix32 lhs, mmSInt16_t rhs) { return mmFix32_Div(lhs, mmFix32_Int2Fix(rhs)); }
        inline friend mmFix32 operator / (mmSInt32_t lhs, mmFix32 rhs) { return mmFix32_Div(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator / (mmFix32 lhs, mmSInt32_t rhs) { return mmFix32_Div(lhs, mmFix32_Int2Fix(rhs)); }

        inline friend mmFix32 operator + (mmFix32 lhs, mmFix32 rhs) { return mmFix32_Add(lhs.d_value, rhs.d_value); }
        inline friend mmFix32 operator + (mmFix32_t lhs, mmFix32 rhs) { return mmFix32_Add(lhs, rhs.d_value); }
        inline friend mmFix32 operator + (mmFix32 lhs, mmFix32_t rhs) { return mmFix32_Add(lhs.d_value, rhs); }
        inline friend mmFix32 operator + (double lhs, mmFix32 rhs) { return mmFix32_Add(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator + (mmFix32 lhs, double rhs) { return mmFix32_Add(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator + (float lhs, mmFix32 rhs) { return mmFix32_Add(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator + (mmFix32 lhs, float rhs) { return mmFix32_Add(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator + (mmSInt16_t lhs, mmFix32 rhs) { return mmFix32_Add(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator + (mmFix32 lhs, mmSInt16_t rhs) { return mmFix32_Add(lhs, mmFix32_Int2Fix(rhs)); }
        inline friend mmFix32 operator + (mmSInt32_t lhs, mmFix32 rhs) { return mmFix32_Add(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator + (mmFix32 lhs, mmSInt32_t rhs) { return mmFix32_Add(lhs, mmFix32_Int2Fix(rhs)); }

        inline friend mmFix32 operator - (mmFix32 lhs, mmFix32 rhs) { return mmFix32_Sub(lhs.d_value, rhs.d_value); }
        inline friend mmFix32 operator - (mmFix32_t lhs, mmFix32 rhs) { return mmFix32_Sub(lhs, rhs.d_value); }
        inline friend mmFix32 operator - (mmFix32 lhs, mmFix32_t rhs) { return mmFix32_Sub(lhs.d_value, rhs); }
        inline friend mmFix32 operator - (double lhs, mmFix32 rhs) { return mmFix32_Sub(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator - (mmFix32 lhs, double rhs) { return mmFix32_Sub(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator - (float lhs, mmFix32 rhs) { return mmFix32_Sub(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator - (mmFix32 lhs, float rhs) { return mmFix32_Sub(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator - (mmSInt16_t lhs, mmFix32 rhs) { return mmFix32_Sub(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator - (mmFix32 lhs, mmSInt16_t rhs) { return mmFix32_Sub(lhs, mmFix32_Int2Fix(rhs)); }
        inline friend mmFix32 operator - (mmSInt32_t lhs, mmFix32 rhs) { return mmFix32_Sub(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator - (mmFix32 lhs, mmSInt32_t rhs) { return mmFix32_Sub(lhs, mmFix32_Int2Fix(rhs)); }

        inline friend mmFix32 operator % (mmFix32 lhs, mmFix32 rhs) { return mmFix32_Fmod(lhs.d_value, rhs.d_value); }
        inline friend mmFix32 operator % (mmFix32_t lhs, mmFix32 rhs) { return mmFix32_Fmod(lhs, rhs.d_value); }
        inline friend mmFix32 operator % (mmFix32 lhs, mmFix32_t rhs) { return mmFix32_Fmod(lhs.d_value, rhs); }
        inline friend mmFix32 operator % (double lhs, mmFix32 rhs) { return mmFix32_Fmod(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator % (mmFix32 lhs, double rhs) { return mmFix32_Fmod(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator % (float lhs, mmFix32 rhs) { return mmFix32_Fmod(mmFix32_Dbl2Fix(lhs), rhs); }
        inline friend mmFix32 operator % (mmFix32 lhs, float rhs) { return mmFix32_Fmod(lhs, mmFix32_Dbl2Fix(rhs)); }
        inline friend mmFix32 operator % (mmSInt16_t lhs, mmFix32 rhs) { return mmFix32_Fmod(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator % (mmFix32 lhs, mmSInt16_t rhs) { return mmFix32_Fmod(lhs, mmFix32_Int2Fix(rhs)); }
        inline friend mmFix32 operator % (mmSInt32_t lhs, mmFix32 rhs) { return mmFix32_Fmod(mmFix32_Int2Fix(lhs), rhs); }
        inline friend mmFix32 operator % (mmFix32 lhs, mmSInt32_t rhs) { return mmFix32_Fmod(lhs, mmFix32_Int2Fix(rhs)); }
    public:
        std::string ToString() const
        {
            char val[64] = { 0 };
            double d = mmFix32_Fix2Dbl(this->d_value);
            mmValue_DoubleToA(&d, val);
            return std::string(val);
        }

        friend std::ostream& operator<<(std::ostream &stream, const mmFix32 & v)
        {
            stream << (double)v;
            return stream;
        }
    };
}
// elementary function
static mmInline mm::mmFix32 cos(const mm::mmFix32 &x) { return mmFix32_Cos(x.d_value); }
static mmInline mm::mmFix32 sin(const mm::mmFix32 &x) { return mmFix32_Sin(x.d_value); }
static mmInline mm::mmFix32 tan(const mm::mmFix32 &x) { return mmFix32_Tan(x.d_value); }
static mmInline mm::mmFix32 sec(const mm::mmFix32 &x) { return mmFix32_Sec(x.d_value); }
static mmInline mm::mmFix32 csc(const mm::mmFix32 &x) { return mmFix32_Csc(x.d_value); }
static mmInline mm::mmFix32 cot(const mm::mmFix32 &x) { return mmFix32_Cot(x.d_value); }
static mmInline mm::mmFix32 acos(const mm::mmFix32 &x) { return mmFix32_Acos(x.d_value); }
static mmInline mm::mmFix32 asin(const mm::mmFix32 &x) { return mmFix32_Asin(x.d_value); }
static mmInline mm::mmFix32 atan(const mm::mmFix32 &x) { return mmFix32_Atan(x.d_value); }
static mmInline mm::mmFix32 atan2(const mm::mmFix32 &y, const mm::mmFix32 &x) { return mmFix32_Atan2(y.d_value, x.d_value); }
static mmInline mm::mmFix32 cosh(const mm::mmFix32 &x) { return mmFix32_Cosh(x.d_value); }
static mmInline mm::mmFix32 sinh(const mm::mmFix32 &x) { return mmFix32_Sinh(x.d_value); }
static mmInline mm::mmFix32 tanh(const mm::mmFix32 &x) { return mmFix32_Tanh(x.d_value); }
static mmInline mm::mmFix32 sech(const mm::mmFix32 &x) { return mmFix32_Sech(x.d_value); }
static mmInline mm::mmFix32 csch(const mm::mmFix32 &x) { return mmFix32_Csch(x.d_value); }
static mmInline mm::mmFix32 coth(const mm::mmFix32 &x) { return mmFix32_Coth(x.d_value); }
static mmInline mm::mmFix32 exp(const mm::mmFix32 &x) { return mmFix32_Exp(x.d_value); }
static mmInline mm::mmFix32 log(const mm::mmFix32 &x) { return mmFix32_Log(x.d_value); }
static mmInline mm::mmFix32 log2(const mm::mmFix32 &x) { return mmFix32_Log2(x.d_value); }
static mmInline mm::mmFix32 ln(const mm::mmFix32 &x) { return mmFix32_Ln(x.d_value); }
static mmInline mm::mmFix32 inv(const mm::mmFix32 &x) { return mmFix32_Inv(x.d_value); }
static mmInline mm::mmFix32 pow(const mm::mmFix32 &x, const mm::mmFix32 &y) { return mmFix32_Pow(x.d_value, y.d_value); }
static mmInline mm::mmFix32 sqrt(const mm::mmFix32 &x) { return mmFix32_Sqrt(x.d_value); }
static mmInline mm::mmFix32 floor(const mm::mmFix32 &x) { return mmFix32_Floor(x.d_value); }
static mmInline mm::mmFix32 ceil(const mm::mmFix32 &x) { return mmFix32_Ceil(x.d_value); }
static mmInline mm::mmFix32 round(const mm::mmFix32 &x) { return mmFix32_Round(x.d_value); }
static mmInline mm::mmFix32 fabs(const mm::mmFix32 &x) { return mmFix32_Fabs(x.d_value); }
static mmInline mm::mmFix32 fmod(const mm::mmFix32 &x, const mm::mmFix32 &y) { return mmFix32_Fmod(x.d_value, y.d_value); }
static mmInline mm::mmFix32 clamp(const mm::mmFix32 &x, const mm::mmFix32 &min_v, const mm::mmFix32 &max_v)
{ return mmFix32_Clamp(x.d_value, min_v.d_value, max_v.d_value); }
// ext
static mmInline mm::mmFix32 inv_sqrt(const mm::mmFix32 &x) { return 1.0 / mmFix32_Sqrt(x.d_value); }
static mmInline mm::mmFix32 degrees_2_radians(const mm::mmFix32 &x) { return x * mm::mmFix32::MM_PI_DIV_180; }
static mmInline mm::mmFix32 radians_2_degrees(const mm::mmFix32 &x) { return x * mm::mmFix32::MM_180_DIV_PI; }
static mmInline mm::mmFix32 squared(const mm::mmFix32 &x) { return x * x; }

#endif//__mmFix32_h__
