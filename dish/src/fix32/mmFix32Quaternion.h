/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFix32Quaternion_h__
#define __mmFix32Quaternion_h__

#include "core/mmCore.h"

#include "fix32/mmFix32.h"

#include "fix32/mmFix32Matrix3x3.h"

#include <assert.h>

#include "fix32/mmFix32Export.h"

// https://www.ogre3d.org/

namespace mm
{
    struct MM_EXPORT_FIX32 mmFix32Quaternion
    {
    public:
        // Cutoff for sine near zero
        static const mmFix32 EPSILON;

        // special values
        static const mmFix32Quaternion ZERO;
        static const mmFix32Quaternion IDENTITY;
    public:
        // Cutoff for sine near zero
        static const mmFix32 msEpsilon;
    public:
        mmFix32 x, y, z, w;
    public:
        inline mmFix32Quaternion()
            : x(0)
            , y(0)
            , z(0)
            , w(0)
        {

        }
        inline mmFix32Quaternion(const mmFix32Quaternion& v)
            : x(v.x)
            , y(v.y)
            , z(v.z)
            , w(v.w)
        {

        }
        inline explicit mmFix32Quaternion(double scalar)
            : x(scalar)
            , y(scalar)
            , z(scalar)
            , w(scalar)
        {

        }
        inline explicit mmFix32Quaternion(double dx, double dy, double dz, double dw)
            : x(dx)
            , y(dy)
            , z(dz)
            , w(dw)
        {

        }
        inline explicit mmFix32Quaternion(mmFix32 scalar)
            : x(scalar)
            , y(scalar)
            , z(scalar)
            , w(scalar)
        {

        }
        inline explicit mmFix32Quaternion(mmFix32 fx, mmFix32 fy, mmFix32 fz, mmFix32 fw)
            : x(fx)
            , y(fy)
            , z(fz)
            , w(fw)
        {

        }
        /// Construct a quaternion from a rotation matrix
        inline mmFix32Quaternion(const mmFix32Matrix3x3& rot)
        {
            this->FromRotationMatrix(rot);
        }
    public:
        inline void Swap(mmFix32Quaternion& other)
        {
            std::swap(this->x, other.x);
            std::swap(this->y, other.y);
            std::swap(this->z, other.z);
            std::swap(this->w, other.w);
        }
    public:
        inline mmFix32 operator [] (const size_t i) const
        {
            assert(i < 4);
            return *(&this->x + i);
        }

        inline mmFix32& operator [] (const size_t i)
        {
            assert(i < 4);
            return *(&this->x + i);
        }
    public:
        inline mmFix32Quaternion& operator = (const mmFix32Quaternion& rkQ)
        {
            this->x = rkQ.x;
            this->y = rkQ.y;
            this->z = rkQ.z;
            this->w = rkQ.w;
            return *this;
        }
    public:
        inline bool operator == (const mmFix32Quaternion& rhs) const
        {
            return
                (rhs.x == x) && (rhs.y == y) &&
                (rhs.z == z) && (rhs.w == w);
        }
        inline bool operator != (const mmFix32Quaternion& rhs) const
        {
            return !operator==(rhs);
        }
    public:
        mmFix32Quaternion operator + (const mmFix32Quaternion& rkQ) const;
        mmFix32Quaternion operator - (const mmFix32Quaternion& rkQ) const;
        mmFix32Quaternion operator * (const mmFix32Quaternion& rkQ) const;
        // Rotation of a vector by a quaternion
        mmFix32Vector3 operator * (const mmFix32Vector3& rkVector) const;
    public:
        friend mmFix32Quaternion operator * (mmFix32 fScalar, const mmFix32Quaternion& rkQ);
        friend mmFix32Quaternion operator * (const mmFix32Quaternion& rkQ, mmFix32 fScalar);
    public:
        inline const mmFix32Quaternion& operator + () const
        {
            return *this;
        }

        inline mmFix32Quaternion operator - () const
        {
            return mmFix32Quaternion(-this->x, -this->y, -this->z, -this->w);
        }
    public:
        inline mmFix32 DotProduct(const mmFix32Quaternion& rkQ) const
        {
            return w * rkQ.w + x * rkQ.x + y * rkQ.y + z * rkQ.z;
        }
    public:
        // Normalises this quaternion, and returns the previous length
        mmFix32 Normalise();
    public:
        // Apply to non-zero quaternion
        mmFix32Quaternion Inverse() const;
        // Apply to unit-length quaternion
        mmFix32Quaternion UnitInverse() const;
        mmFix32Quaternion QuaternionExp() const;
        mmFix32Quaternion QuaternionLog() const;
    public:
        void FromRotationMatrix(const mmFix32Matrix3x3& kRot);
        void ToRotationMatrix(mmFix32Matrix3x3& kRot) const;

        /** Setups the quaternion using the supplied vector, and "roll" around
         *  that vector by the specified radians.
         */
        void FromAngleAxis(const mmFix32& rfAngle, const mmFix32Vector3& rkAxis);
        void ToAngleAxis(mmFix32& rfAngle, mmFix32Vector3& rkAxis) const;

        /** Constructs the quaternion using 3 axes, the axes are assumed to be orthonormal
         *  @see FromAxes
         */
        void FromAxes(const mmFix32Vector3* akAxis);
        void FromAxes(const mmFix32Vector3& xAxis, const mmFix32Vector3& yAxis, const mmFix32Vector3& zAxis);
        /** Gets the 3 orthonormal axes defining the quaternion. @see FromAxes */
        void ToAxes(mmFix32Vector3* akAxis) const;
        void ToAxes(mmFix32Vector3& xAxis, mmFix32Vector3& yAxis, mmFix32Vector3& zAxis) const;

        /** Returns the X orthonormal axis defining the quaternion. Same as doing
         *  xAxis = Vector3::UNIT_X * this. Also called the local X-axis
         */
        mmFix32Vector3 XAxis(void) const;

        /** Returns the Y orthonormal axis defining the quaternion. Same as doing
         *  yAxis = Vector3::UNIT_Y * this. Also called the local Y-axis
         */
        mmFix32Vector3 YAxis(void) const;

        /** Returns the Z orthonormal axis defining the quaternion. Same as doing
         *  zAxis = Vector3::UNIT_Z * this. Also called the local Z-axis
         */
        mmFix32Vector3 ZAxis(void) const;

        /** Calculate the local roll element of this quaternion.
         * @param reprojectAxis By default the method returns the 'intuitive' result
         *      that is, if you projected the local X of the quaternion onto the XY plane,
         *      the angle between it and global X is returned. The co-domain of the returned
         *      value is from -180 to 180 degrees. If set to false though, the result is
         *      the rotation around Z axis that could be used to implement the quaternion
         *      using some non-intuitive order of rotations. This behavior is preserved for
         *      backward compatibility, to decompose quaternion into yaw, pitch and roll use
         *      q.ToRotationMatrix().ToEulerAnglesYXZ(yaw, pitch, roll) instead.
         */
        mmFix32 GetRotateX(bool reprojectAxis = true) const;

        /** Calculate the local pitch element of this quaternion
         * @param reprojectAxis By default the method returns the 'intuitive' result
         *      that is, if you projected the local Y of the quaternion onto the YZ plane,
         *      the angle between it and global Y is returned. The co-domain of the returned
         *      value is from -180 to 180 degrees. If set to false though, the result is
         *      the rotation around X axis that could be used to implement the quaternion
         *      using some non-intuitive order of rotations. This behavior is preserved for
         *      backward compatibility, to decompose quaternion into yaw, pitch and roll use
         *      q.ToRotationMatrix().ToEulerAnglesYXZ(yaw, pitch, roll) instead.
         */
        mmFix32 GetRotateY(bool reprojectAxis = true) const;

        /** Calculate the local yaw element of this quaternion
         * @param reprojectAxis By default the method returns the 'intuitive' result
         *      that is, if you projected the local Z of the quaternion onto the ZX plane,
         *      the angle between it and global Z is returned. The co-domain of the returned
         *      value is from -180 to 180 degrees. If set to false though, the result is
         *      the rotation around Y axis that could be used to implement the quaternion
         *      using some non-intuitive order of rotations. This behavior is preserved for
         *      backward compatibility, to decompose quaternion into yaw, pitch and roll use
         *      q.ToRotationMatrix().ToEulerAnglesYXZ(yaw, pitch, roll) instead.
         */
        mmFix32 GetRotateZ(bool reprojectAxis = true) const;
        
        /** Equality with tolerance (tolerance is max angle difference)
         *  @remarks Both equals() and orientationEquals() measure the exact same thing.
         *      One measures the difference by angle, the other by a different, non-linear metric.
         */
        bool Equals(const mmFix32Quaternion& rhs, const mmFix32& tolerance) const;

        /** Compare two quaternions which are assumed to be used as orientations.
         *  @remarks Both equals() and orientationEquals() measure the exact same thing.
         *      One measures the difference by angle, the other by a different, non-linear metric.
         *  @return true if the two orientations are the same or very close, relative to the given tolerance.
         *      Slerp ( 0.75f, A, B ) != Slerp ( 0.25f, B, A );
         *      therefore be careful if your code relies in the order of the operands.
         *      This is specially important in IK animation.
         */
        inline bool OrientationEquals(const mmFix32Quaternion& other, mmFix32 tolerance = 1e-3) const
        {
            mmFix32 d = this->DotProduct(other);
            return 1 - d * d < tolerance;
        }
    public:
        /** Gets the shortest arc quaternion to rotate this vector to the destination
         *  vector.
         *  @remarks
         *      If you call this with a dest vector that is close to the inverse
         *      of this vector, we will rotate 180 degrees around the 'fallbackAxis'
         *      (if specified, or a generated axis if not) since in this case
         *      ANY axis of rotation is valid.
         */
        static mmFix32Quaternion GetRotationTo(const mmFix32Vector3& rkVector, const mmFix32Vector3& dest, const mmFix32Vector3& fallbackAxis = mmFix32Vector3::ZERO);
    public:
        /** Performs Spherical linear interpolation between two quaternions, and returns the result.
         * Slerp ( 0.0f, A, B ) = A
         * Slerp ( 1.0f, A, B ) = B
         * @return Interpolated quaternion
         * @remarks
         *      Slerp has the proprieties of performing the interpolation at constant
         *      velocity, and being torque-minimal (unless shortestPath=false).
         *      However, it's NOT commutative, which means
         *      Slerp ( 0.75f, A, B ) != Slerp ( 0.25f, B, A );
         *      therefore be careful if your code relies in the order of the operands.
         *      This is specially important in IK animation.
         */
        static mmFix32Quaternion Slerp(
            mmFix32 fT,
            const mmFix32Quaternion& rkP,
            const mmFix32Quaternion& rkQ,
            bool shortestPath = false);

        /** @see Slerp. It adds extra "spins" (i.e. rotates several times) specified
         *      by parameter 'iExtraSpins' while interpolating before arriving to the
         *      final values
         */
        static mmFix32Quaternion SlerpExtraSpins(
            mmFix32 fT,
            const mmFix32Quaternion& rkP,
            const mmFix32Quaternion& rkQ,
            int iExtraSpins);

        // Setup for spherical quadratic interpolation
        static void Intermediate(
            const mmFix32Quaternion& rkQ0,
            const mmFix32Quaternion& rkQ1,
            const mmFix32Quaternion& rkQ2,
            mmFix32Quaternion& rkA,
            mmFix32Quaternion& rkB);

        // Spherical quadratic interpolation
        static mmFix32Quaternion Squad(
            mmFix32 fT,
            const mmFix32Quaternion& rkP,
            const mmFix32Quaternion& rkA,
            const mmFix32Quaternion& rkB,
            const mmFix32Quaternion& rkQ,
            bool shortestPath = false);

        /** Performs Normalised linear interpolation between two quaternions, and returns the result.
         * nlerp ( 0.0f, A, B ) = A
         * nlerp ( 1.0f, A, B ) = B
         * @remarks
         *      Nlerp is faster than Slerp.
         *      Nlerp has the proprieties of being commutative (@see Slerp;
         *      commutativity is desired in certain places, like IK animation), and
         *      being torque-minimal (unless shortestPath=false). However, it's performing
         *      the interpolation at non-constant velocity; sometimes this is desired,
         *      sometimes it is not. Having a non-constant velocity can produce a more
         *      natural rotation feeling without the need of tweaking the weights; however
         *      if your scene relies on the timing of the rotation or assumes it will point
         *      at a specific angle at a specific weight value, Slerp is a better choice.
         */
        static mmFix32Quaternion Nlerp(
            mmFix32 fT,
            const mmFix32Quaternion& rkP,
            const mmFix32Quaternion& rkQ,
            bool shortestPath = false);
    public:
        MM_EXPORT_FIX32 friend std::ostream& operator << (std::ostream& o, const mmFix32Quaternion& q);
    };
}

#endif//__mmFix32Quaternion_h__
