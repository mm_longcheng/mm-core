/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFix32Matrix3x3_h__
#define __mmFix32Matrix3x3_h__

#include "core/mmCore.h"
#include "core/mmAlloc.h"

#include "fix32/mmFix32.h"

#include "fix32/mmFix32Vector3.h"

#include <assert.h>

#include "fix32/mmFix32Export.h"

// https://www.ogre3d.org/

// NB All code adapted from Wild Magic 0.2 Matrix math (free source code)
// http://www.geometrictools.com/

// NOTE.  The (x,y,z) coordinate system is assumed to be right-handed.
// Coordinate axis rotation matrices are of the form
//   RX =    1       0       0
//           0     cos(t) -sin(t)
//           0     sin(t)  cos(t)
// where t > 0 indicates a counterclockwise rotation in the yz-plane
//   RY =  cos(t)    0     sin(t)
//           0       1       0
//        -sin(t)    0     cos(t)
// where t > 0 indicates a counterclockwise rotation in the zx-plane
//   RZ =  cos(t) -sin(t)    0
//         sin(t)  cos(t)    0
//           0       0       1
// where t > 0 indicates a counterclockwise rotation in the xy-plane.

namespace mm
{
    /** \addtogroup Core
     *  @{
     */
    /** \addtogroup Math
     *  @{
     */
    /** A 3x3 matrix which can represent rotations around axes.
     *  @note
     *      <b>All the code is adapted from the Wild Magic 0.2 Matrix
     *      library (http://www.geometrictools.com/).</b>
     *  @par
     *      The coordinate system is assumed to be <b>right-handed</b>.
     */
    struct MM_EXPORT_FIX32 mmFix32Matrix3x3
    {
    public:
        static const mmFix32 EPSILON;
        static const mmFix32Matrix3x3 ZERO;
        static const mmFix32Matrix3x3 IDENTITY;
    public:
        static const mmFix32 msSvdEpsilon;
        static const unsigned int msSvdMaxIterations;
    public:
        mmFix32 m[3][3];
    public:
        inline mmFix32Matrix3x3()
        {
            mmMemset(this->m, 0, sizeof(mmFix32) * 9);
        }
        /// Assignment and comparison
        inline mmFix32Matrix3x3(const mmFix32Matrix3x3& v)
        {
            mmMemcpy(this->m, v.m, sizeof(mmFix32) * 9);
        }

        mmFix32Matrix3x3(
            mmFix32 fEntry00, mmFix32 fEntry01, mmFix32 fEntry02,
            mmFix32 fEntry10, mmFix32 fEntry11, mmFix32 fEntry12,
            mmFix32 fEntry20, mmFix32 fEntry21, mmFix32 fEntry22);
    public:
        /** Exchange the contents of this matrix with another.
        */
        void Swap(mmFix32Matrix3x3& other);
    public:
        /// Member access, allows use of construct mat[r][c]
        inline const mmFix32* operator [] (size_t iRow) const
        {
            assert(iRow < 3);
            return m[iRow];
        }

        inline mmFix32* operator [] (size_t iRow)
        {
            assert(iRow < 3);
            return m[iRow];
        }
    public:
        inline mmFix32Matrix3x3& operator = (const mmFix32Matrix3x3& rkMatrix)
        {
            mmMemcpy(m, rkMatrix.m, 9 * sizeof(mmFix32));
            return *this;
        }
    public:
        /** Tests 2 matrices for equality.
        */
        bool operator == (const mmFix32Matrix3x3& m2) const;

        /** Tests 2 matrices for inequality.
        */
        inline bool operator != (const mmFix32Matrix3x3& m2) const
        {
            return !operator==(m2);
        }
    public:
        // arithmetic operations
        /** Matrix addition.
        */
        mmFix32Matrix3x3 operator + (const mmFix32Matrix3x3& m2) const;

        /** Matrix subtraction.
        */
        mmFix32Matrix3x3 operator - (const mmFix32Matrix3x3& m2) const;
        /** Matrix concatenation using '*'.
        */
        mmFix32Matrix3x3 operator * (const mmFix32Matrix3x3& m2) const;
        // matrix3x3 * vector3x1 [3x3 * 3x1 = 3x1]
        friend mmFix32Vector3 operator * (const mmFix32Matrix3x3& rkMatrix, const mmFix32Vector3& rkVector);
        // vector1x3 * matrix3x3 [1x3 * 3x3 = 1x3]
        friend mmFix32Vector3 operator * (const mmFix32Vector3& rkVector, const mmFix32Matrix3x3& rkMatrix);
        // Matrix * scalar
        friend mmFix32Matrix3x3 operator * (const mmFix32Matrix3x3& rkMatrix, mmFix32 fScalar);
        // Scalar * matrix
        friend mmFix32Matrix3x3 operator * (mmFix32 fScalar, const mmFix32Matrix3x3& rkMatrix);

        mmFix32Matrix3x3 operator - () const;
    public:
        mmFix32Matrix3x3 Transpose() const;
        mmFix32Matrix3x3 Inverse() const;
        mmFix32 Determinant() const;
    public:
        // Singular value decomposition
        void SingularValueDecomposition(mmFix32Matrix3x3& kL, mmFix32Vector3& kS, mmFix32Matrix3x3& kR) const;
        void SingularValueComposition(const mmFix32Matrix3x3& rkL, const mmFix32Vector3& rkS, const mmFix32Matrix3x3& rkR);

        // Gram-Schmidt orthonormalization (applied to columns of rotation matrix)
        void Orthonormalize();
        // Orthogonal Q, diagonal D, upper triangular U stored as (u01,u02,u12)
        void QDUDecomposition(mmFix32Matrix3x3& kQ, mmFix32Vector3& kD, mmFix32Vector3& kU) const;

        mmFix32 SpectralNorm() const;

        // Note: Matrix must be orthonormal
        void ToAngleAxis(mmFix32Vector3& rkAxis, mmFix32& rfRadians) const;
        void FromAngleAxis(const mmFix32Vector3& rkAxis, const mmFix32& fRadians);

        /** The matrix must be orthonormal.  The decomposition is yaw*pitch*roll
         *  where yaw is rotation about the Up vector, pitch is rotation about the
         *  Right axis, and roll is rotation about the Direction axis.
         */
        bool ToEulerAnglesXYZ(mmFix32& rfYAngle, mmFix32& rfPAngle, mmFix32& rfRAngle) const;
        void FromEulerAnglesXYZ(const mmFix32& fYAngle, const mmFix32& fPAngle, const mmFix32& fRAngle);

        // Eigensolver, matrix must be symmetric
        void EigenSolveSymmetric(mmFix32 afEigenvalue[3], mmFix32Vector3 akEigenvector[3]) const;
    public:
        static void TensorProduct(const mmFix32Vector3& rkU, const mmFix32Vector3& rkV, mmFix32Matrix3x3& rkProduct);
    public:
        /** Function for writing to a stream.
        */
        MM_EXPORT_FIX32 friend std::ostream& operator << (std::ostream& o, const mmFix32Matrix3x3& mat);
    public:
        static void Bidiagonalize(mmFix32Matrix3x3& kA, mmFix32Matrix3x3& kL, mmFix32Matrix3x3& kR);
        static void GolubKahanStep(mmFix32Matrix3x3& kA, mmFix32Matrix3x3& kL, mmFix32Matrix3x3& kR);
        // support for spectral norm
        static mmFix32 MaxCubicRoot(mmFix32 afCoeff[3]);
        // support for eigensolver
        void Tridiagonal(mmFix32 afDiag[3], mmFix32 afSubDiag[3]);
        bool QLAlgorithm(mmFix32 afDiag[3], mmFix32 afSubDiag[3]);
    };
    /** @} */
    /** @} */
}

#endif//__mmFix32Matrix3x3_h__
