/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFix32Vector4_h__
#define __mmFix32Vector4_h__

#include "core/mmCore.h"

#include "fix32/mmFix32.h"

#include <assert.h>

#include "fix32/mmFix32Export.h"

// https://www.ogre3d.org/

namespace mm
{
    /** \addtogroup Core
     *  @{
     */
    /** \addtogroup Math
     *  @{
     */
    /** 4-dimensional homogeneous vector.
    */
    struct MM_EXPORT_FIX32 mmFix32Vector4
    {
    public:
        static const mmFix32Vector4 ZERO;
    public:
        mmFix32 x, y, z, w;
    public:
        inline mmFix32Vector4()
            : x(0)
            , y(0)
            , z(0)
            , w(0)
        {

        }
        inline mmFix32Vector4(const mmFix32Vector4& v)
            : x(v.x)
            , y(v.y)
            , z(v.z)
            , w(v.w)
        {

        }
        inline explicit mmFix32Vector4(double scalar)
            : x(scalar)
            , y(scalar)
            , z(scalar)
            , w(scalar)
        {

        }
        inline explicit mmFix32Vector4(double dx, double dy, double dz, double dw)
            : x(dx)
            , y(dy)
            , z(dz)
            , w(dw)
        {

        }
        inline explicit mmFix32Vector4(mmFix32 scalar)
            : x(scalar)
            , y(scalar)
            , z(scalar)
            , w(scalar)
        {

        }
        inline explicit mmFix32Vector4(mmFix32 fx, mmFix32 fy, mmFix32 fz, mmFix32 fw)
            : x(fx)
            , y(fy)
            , z(fz)
            , w(fw)
        {

        }
    public:
        /** Exchange the contents of this vector with another.
        */
        inline void Swap(mmFix32Vector4& other)
        {
            std::swap(this->x, other.x);
            std::swap(this->y, other.y);
            std::swap(this->z, other.z);
            std::swap(this->w, other.w);
        }
    public:
        inline mmFix32 operator [] (const size_t i) const
        {
            assert(i < 4);
            return *(&this->x + i);
        }

        inline mmFix32& operator [] (const size_t i)
        {
            assert(i < 4);
            return *(&this->x + i);
        }
        /// Pointer accessor for direct copying
        inline mmFix32* Ptr()
        {
            return &this->x;
        }
        /// Pointer accessor for direct copying
        inline const mmFix32* Ptr() const
        {
            return &this->x;
        }
    public:
        /** Assigns the value of the other vector.
         *  @param
         *      rkVector The other vector
         */
        inline mmFix32Vector4& operator = (const mmFix32Vector4& rkVector)
        {
            this->x = rkVector.x;
            this->y = rkVector.y;
            this->z = rkVector.z;
            this->w = rkVector.w;
            return *this;
        }

        inline mmFix32Vector4& operator = (const mmFix32 fScalar)
        {
            this->x = fScalar;
            this->y = fScalar;
            this->z = fScalar;
            this->w = fScalar;
            return *this;
        }
    public:
        inline bool operator == (const mmFix32Vector4& rkVector) const
        {
            return
                this->x == rkVector.x &&
                this->y == rkVector.y &&
                this->z == rkVector.z &&
                this->w == rkVector.w;
        }

        inline bool operator != (const mmFix32Vector4& rkVector) const
        {
            return
                this->x != rkVector.x ||
                this->y != rkVector.y ||
                this->z != rkVector.z ||
                this->w != rkVector.w;
        }
    public:
        inline mmFix32Vector4 operator + (const mmFix32Vector4& rkVector) const
        {
            return mmFix32Vector4(
                this->x + rkVector.x,
                this->y + rkVector.y,
                this->z + rkVector.z,
                this->w + rkVector.w);
        }

        inline mmFix32Vector4 operator - (const mmFix32Vector4& rkVector) const
        {
            return mmFix32Vector4(
                this->x - rkVector.x,
                this->y - rkVector.y,
                this->z - rkVector.z,
                this->w - rkVector.w);
        }

        inline mmFix32Vector4 operator * (const mmFix32 fScalar) const
        {
            return mmFix32Vector4(
                this->x * fScalar,
                this->y * fScalar,
                this->z * fScalar,
                this->w * fScalar);
        }

        inline mmFix32Vector4 operator * (const mmFix32Vector4& rhs) const
        {
            return mmFix32Vector4(
                rhs.x * this->x,
                rhs.y * this->y,
                rhs.z * this->z,
                rhs.w * this->w);
        }

        inline mmFix32Vector4 operator / (const mmFix32 fScalar) const
        {
            assert(fScalar != 0.0);
            mmFix32 fInv = mmFix32(1.0f) / fScalar;
            return mmFix32Vector4(
                this->x * fInv,
                this->y * fInv,
                this->z * fInv,
                this->w * fInv);
        }

        inline mmFix32Vector4 operator / (const mmFix32Vector4& rhs) const
        {
            return mmFix32Vector4(
                this->x / rhs.x,
                this->y / rhs.y,
                this->z / rhs.z,
                this->w / rhs.w);
        }

        inline const mmFix32Vector4& operator + () const
        {
            return *this;
        }

        inline mmFix32Vector4 operator - () const
        {
            return mmFix32Vector4(-this->x, -this->y, -this->z, -this->w);
        }

        inline friend mmFix32Vector4 operator * (const mmFix32 fScalar, const mmFix32Vector4& rkVector)
        {
            return mmFix32Vector4(
                fScalar * rkVector.x,
                fScalar * rkVector.y,
                fScalar * rkVector.z,
                fScalar * rkVector.w);
        }

        inline friend mmFix32Vector4 operator / (const mmFix32 fScalar, const mmFix32Vector4& rkVector)
        {
            return mmFix32Vector4(
                fScalar / rkVector.x,
                fScalar / rkVector.y,
                fScalar / rkVector.z,
                fScalar / rkVector.w);
        }

        inline friend mmFix32Vector4 operator + (const mmFix32Vector4& lhs, const mmFix32 rhs)
        {
            return mmFix32Vector4(
                lhs.x + rhs,
                lhs.y + rhs,
                lhs.z + rhs,
                lhs.w + rhs);
        }

        inline friend mmFix32Vector4 operator + (const mmFix32 lhs, const mmFix32Vector4& rhs)
        {
            return mmFix32Vector4(
                lhs + rhs.x,
                lhs + rhs.y,
                lhs + rhs.z,
                lhs + rhs.w);
        }

        inline friend mmFix32Vector4 operator - (const mmFix32Vector4& lhs, mmFix32 rhs)
        {
            return mmFix32Vector4(
                lhs.x - rhs,
                lhs.y - rhs,
                lhs.z - rhs,
                lhs.w - rhs);
        }

        inline friend mmFix32Vector4 operator - (const mmFix32 lhs, const mmFix32Vector4& rhs)
        {
            return mmFix32Vector4(
                lhs - rhs.x,
                lhs - rhs.y,
                lhs - rhs.z,
                lhs - rhs.w);
        }

        inline mmFix32Vector4& operator += (const mmFix32Vector4& rkVector)
        {
            this->x += rkVector.x;
            this->y += rkVector.y;
            this->z += rkVector.z;
            this->w += rkVector.w;
            return *this;
        }

        inline mmFix32Vector4& operator -= (const mmFix32Vector4& rkVector)
        {
            this->x -= rkVector.x;
            this->y -= rkVector.y;
            this->z -= rkVector.z;
            this->w -= rkVector.w;
            return *this;
        }

        inline mmFix32Vector4& operator *= (const mmFix32 fScalar)
        {
            this->x *= fScalar;
            this->y *= fScalar;
            this->z *= fScalar;
            this->w *= fScalar;
            return *this;
        }

        inline mmFix32Vector4& operator += (const mmFix32 fScalar)
        {
            this->x += fScalar;
            this->y += fScalar;
            this->z += fScalar;
            this->w += fScalar;
            return *this;
        }

        inline mmFix32Vector4& operator -= (const mmFix32 fScalar)
        {
            this->x -= fScalar;
            this->y -= fScalar;
            this->z -= fScalar;
            this->w -= fScalar;
            return *this;
        }

        inline mmFix32Vector4& operator *= (const mmFix32Vector4& rkVector)
        {
            this->x *= rkVector.x;
            this->y *= rkVector.y;
            this->z *= rkVector.z;
            this->w *= rkVector.w;
            return *this;
        }

        inline mmFix32Vector4& operator /= (const mmFix32 fScalar)
        {
            assert(fScalar != 0.0);
            mmFix32 fInv = mmFix32(1.0f) / fScalar;
            this->x *= fInv;
            this->y *= fInv;
            this->z *= fInv;
            this->w *= fInv;
            return *this;
        }

        inline mmFix32Vector4& operator /= (const mmFix32Vector4& rkVector)
        {
            this->x /= rkVector.x;
            this->y /= rkVector.y;
            this->z /= rkVector.z;
            this->w /= rkVector.w;
            return *this;
        }
    public:
        /** Calculates the dot (scalar) product of this vector with another.
         *  @param
         *      vec Vector with which to calculate the dot product (together
         *      with this one).
         *  @return
         *      A float representing the dot product value.
         */
        inline mmFix32 DotProduct(const mmFix32Vector4& vec) const
        {
            return this->x * vec.x + this->y * vec.y + this->z * vec.z + this->w * vec.w;
        }
        // High dimensional( > 3d) vector, cross product is not a vector. Less used now.
    public:
        /** Function for writing to a stream.
        */
        MM_EXPORT_FIX32 friend std::ostream& operator << (std::ostream& o, const mmFix32Vector4& v);
    };
    /** @} */
    /** @} */
}

#endif//__mmFix32Vector3_h__
