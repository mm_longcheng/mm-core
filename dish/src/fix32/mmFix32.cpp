/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFix32.h"

namespace mm
{
    const mmFix32 mmFix32::MM_PI = MM_MATH_FIX32_PI;
    const mmFix32 mmFix32::MM_PI_DIV_2 = MM_MATH_FIX32_PI_DIV_2;
    const mmFix32 mmFix32::MM_PI_DIV_4 = MM_MATH_FIX32_PI_DIV_4;
    const mmFix32 mmFix32::MM_1_DIV_PI = MM_MATH_FIX32_1_DIV_PI;
    const mmFix32 mmFix32::MM_2_DIV_PI = MM_MATH_FIX32_2_DIV_PI;
    const mmFix32 mmFix32::MM_2_MUL_PI = MM_MATH_FIX32_2_MUL_PI;

    const mmFix32 mmFix32::MM_PI_DIV_180 = MM_MATH_FIX32_PI_DIV_180;
    const mmFix32 mmFix32::MM_180_DIV_PI = MM_MATH_FIX32_180_DIV_PI;

    const mmFix32 mmFix32::MM_E = MM_MATH_FIX32_E;
}

