/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFix32Quaternion.h"

namespace mm
{
    const mmFix32 mmFix32Quaternion::EPSILON = 1e-03;
    const mmFix32Quaternion mmFix32Quaternion::ZERO(0, 0, 0, 0);
    const mmFix32Quaternion mmFix32Quaternion::IDENTITY(1, 0, 0, 0);

    const mmFix32 mmFix32Quaternion::msEpsilon = 1e-03;

    mmFix32Quaternion mmFix32Quaternion::operator + (const mmFix32Quaternion& rkQ) const
    {
        return mmFix32Quaternion(
            this->x + rkQ.x,
            this->y + rkQ.y,
            this->z + rkQ.z,
            this->w + rkQ.w);
    }
    mmFix32Quaternion mmFix32Quaternion::operator - (const mmFix32Quaternion& rkQ) const
    {
        return mmFix32Quaternion(
            this->x - rkQ.x,
            this->y - rkQ.y,
            this->z - rkQ.z,
            this->w - rkQ.w);
    }
    mmFix32Quaternion mmFix32Quaternion::operator * (const mmFix32Quaternion& rkQ) const
    {
        // NOTE:  Multiplication is not generally commutative, so in most
        // cases p*q != q*p.
        return mmFix32Quaternion
        (
            w * rkQ.x + x * rkQ.w + y * rkQ.z - z * rkQ.y,
            w * rkQ.y + y * rkQ.w + z * rkQ.x - x * rkQ.z,
            w * rkQ.z + z * rkQ.w + x * rkQ.y - y * rkQ.x,
            w * rkQ.w - x * rkQ.x - y * rkQ.y - z * rkQ.z
        );
    }

    mmFix32Vector3 mmFix32Quaternion::operator * (const mmFix32Vector3& v) const
    {
        // nVidia SDK implementation
        mmFix32Vector3 uv, uuv;
        mmFix32Vector3 qvec(x, y, z);
        uv = qvec.CrossProduct(v);
        uuv = qvec.CrossProduct(uv);
        uv *= (2.0f * w);
        uuv *= 2.0f;

        return v + uv + uuv;
    }

    mmFix32Quaternion operator * (mmFix32 fScalar, const mmFix32Quaternion& rkQ)
    {
        return mmFix32Quaternion(
            fScalar * rkQ.x,
            fScalar * rkQ.y,
            fScalar * rkQ.z,
            fScalar * rkQ.w);
    }
    mmFix32Quaternion operator * (const mmFix32Quaternion& rkQ, mmFix32 fScalar)
    {
        return mmFix32Quaternion(
            fScalar * rkQ.x,
            fScalar * rkQ.y,
            fScalar * rkQ.z,
            fScalar * rkQ.w);
    }

    mmFix32 mmFix32Quaternion::Normalise()
    {
        mmFix32 fLength = sqrt(x * x + y * y + z * z);

        // Will also work for zero-sized vectors, but will change nothing
        // We're not using epsilons because we don't need to.
        if (fLength > mmFix32(0.0f))
        {
            mmFix32 fInvLength = mmFix32(1.0f) / fLength;
            this->x *= fInvLength;
            this->y *= fInvLength;
            this->z *= fInvLength;
            this->w *= fInvLength;
        }

        return fLength;
    }

    mmFix32Quaternion mmFix32Quaternion::Inverse() const
    {
        mmFix32 fNorm = w * w + x * x + y * y + z * z;
        if (fNorm > 0.0)
        {
            mmFix32 fInvNorm = 1.0f / fNorm;
            return mmFix32Quaternion(-x * fInvNorm, -y * fInvNorm, -z * fInvNorm, w*fInvNorm);
        }
        else
        {
            // return an invalid result to flag the error
            return ZERO;
        }
    }

    mmFix32Quaternion mmFix32Quaternion::UnitInverse() const
    {
        // assert:  'this' is unit length
        return mmFix32Quaternion(w, -x, -y, -z);
    }

    mmFix32Quaternion mmFix32Quaternion::QuaternionExp() const
    {
        // If q = A*(x*i+y*j+z*k) where (x,y,z) is unit length, then
        // exp(q) = e^w(cos(A)+sin(A)*(x*i+y*j+z*k)).  If sin(A) is near zero,
        // use exp(q) = e^w(cos(A)+(x*i+y*j+z*k)) since sin(A)/A has limit 1.

        mmFix32 fAngle(sqrt(x*x + y * y + z * z));
        mmFix32 fSin = sin(fAngle);
        mmFix32 fExpW = exp(w);

        mmFix32Quaternion kResult;
        kResult.w = fExpW * cos(fAngle);

        if (fabs(fAngle) >= msEpsilon)
        {
            mmFix32 fCoeff = fExpW * (fSin / (fAngle));
            kResult.x = fCoeff * x;
            kResult.y = fCoeff * y;
            kResult.z = fCoeff * z;
        }
        else
        {
            kResult.x = fExpW * x;
            kResult.y = fExpW * y;
            kResult.z = fExpW * z;
        }

        return kResult;
    }

    mmFix32Quaternion mmFix32Quaternion::QuaternionLog() const
    {
        // If q = cos(A)+sin(A)*(x*i+y*j+z*k) where (x,y,z) is unit length, then
        // log(q) = (A/sin(A))*(x*i+y*j+z*k).  If sin(A) is near zero, use
        // log(q) = (x*i+y*j+z*k) since A/sin(A) has limit 1.

        mmFix32Quaternion kResult;
        kResult.w = 0.0;

        if (fabs(w) < 1.0)
        {
            // According to Neil Dantam, atan2 has the best stability.
            // http://www.neil.dantam.name/note/dantam-quaternion.pdf
            mmFix32 fNormV = sqrt(x*x + y * y + z * z);
            mmFix32 fAngle(atan2(fNormV, w));

            mmFix32 fSin = sin(fAngle);
            if (fabs(fSin) >= msEpsilon)
            {
                mmFix32 fCoeff = fAngle / fSin;
                kResult.x = fCoeff * x;
                kResult.y = fCoeff * y;
                kResult.z = fCoeff * z;
                return kResult;
            }
        }

        kResult.x = x;
        kResult.y = y;
        kResult.z = z;

        return kResult;
    }

    void mmFix32Quaternion::FromRotationMatrix(const mmFix32Matrix3x3& kRot)
    {
        // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
        // article "Quaternion Calculus and Fast Animation".

        mmFix32 fTrace = kRot[0][0] + kRot[1][1] + kRot[2][2];
        mmFix32 fRoot;

        if (fTrace > 0.0)
        {
            // |w| > 1/2, may as well choose w > 1/2
            fRoot = sqrt(fTrace + 1.0f);  // 2w
            w = 0.5f*fRoot;
            fRoot = 0.5f / fRoot;  // 1/(4w)
            x = (kRot[2][1] - kRot[1][2])*fRoot;
            y = (kRot[0][2] - kRot[2][0])*fRoot;
            z = (kRot[1][0] - kRot[0][1])*fRoot;
        }
        else
        {
            // |w| <= 1/2
            static size_t s_iNext[3] = { 1, 2, 0 };
            size_t i = 0;
            if (kRot[1][1] > kRot[0][0])
                i = 1;
            if (kRot[2][2] > kRot[i][i])
                i = 2;
            size_t j = s_iNext[i];
            size_t k = s_iNext[j];

            fRoot = sqrt(kRot[i][i] - kRot[j][j] - kRot[k][k] + 1.0f);
            mmFix32* apkQuat[3] = { &x, &y, &z };
            *apkQuat[i] = 0.5f*fRoot;
            fRoot = 0.5f / fRoot;
            w = (kRot[k][j] - kRot[j][k])*fRoot;
            *apkQuat[j] = (kRot[j][i] + kRot[i][j])*fRoot;
            *apkQuat[k] = (kRot[k][i] + kRot[i][k])*fRoot;
        }
    }

    void mmFix32Quaternion::ToRotationMatrix(mmFix32Matrix3x3& kRot) const
    {
        mmFix32 fTx = x + x;
        mmFix32 fTy = y + y;
        mmFix32 fTz = z + z;
        mmFix32 fTwx = fTx * w;
        mmFix32 fTwy = fTy * w;
        mmFix32 fTwz = fTz * w;
        mmFix32 fTxx = fTx * x;
        mmFix32 fTxy = fTy * x;
        mmFix32 fTxz = fTz * x;
        mmFix32 fTyy = fTy * y;
        mmFix32 fTyz = fTz * y;
        mmFix32 fTzz = fTz * z;

        kRot[0][0] = 1.0f - (fTyy + fTzz);
        kRot[0][1] = fTxy - fTwz;
        kRot[0][2] = fTxz + fTwy;
        kRot[1][0] = fTxy + fTwz;
        kRot[1][1] = 1.0f - (fTxx + fTzz);
        kRot[1][2] = fTyz - fTwx;
        kRot[2][0] = fTxz - fTwy;
        kRot[2][1] = fTyz + fTwx;
        kRot[2][2] = 1.0f - (fTxx + fTyy);
    }

    void mmFix32Quaternion::FromAngleAxis(const mmFix32& rfAngle, const mmFix32Vector3& rkAxis)
    {
        // assert:  axis[] is unit length
        //
        // The quaternion representing the rotation is
        //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

        mmFix32 fHalfAngle(0.5*rfAngle);
        mmFix32 fSin = sin(fHalfAngle);
        w = cos(fHalfAngle);
        x = fSin * rkAxis.x;
        y = fSin * rkAxis.y;
        z = fSin * rkAxis.z;
    }

    void mmFix32Quaternion::ToAngleAxis(mmFix32& rfAngle, mmFix32Vector3& rkAxis) const
    {
        // The quaternion representing the rotation is
        //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

        mmFix32 fSqrLength = x * x + y * y + z * z;
        if (fSqrLength > 0.0)
        {
            rfAngle = 2.0*acos(w);
            mmFix32 fInvLength = inv_sqrt(fSqrLength);
            rkAxis.x = x * fInvLength;
            rkAxis.y = y * fInvLength;
            rkAxis.z = z * fInvLength;
        }
        else
        {
            // angle is 0 (mod 2*pi), so any axis will do
            rfAngle = mmFix32(0.0);
            rkAxis.x = 1.0;
            rkAxis.y = 0.0;
            rkAxis.z = 0.0;
        }
    }

    void mmFix32Quaternion::FromAxes(const mmFix32Vector3* akAxis)
    {
        mmFix32Matrix3x3 kRot;

        for (size_t iCol = 0; iCol < 3; iCol++)
        {
            kRot[0][iCol] = akAxis[iCol].x;
            kRot[1][iCol] = akAxis[iCol].y;
            kRot[2][iCol] = akAxis[iCol].z;
        }

        this->FromRotationMatrix(kRot);
    }

    void mmFix32Quaternion::FromAxes(const mmFix32Vector3& xaxis, const mmFix32Vector3& yaxis, const mmFix32Vector3& zaxis)
    {
        mmFix32Matrix3x3 kRot;

        kRot[0][0] = xaxis.x;
        kRot[1][0] = xaxis.y;
        kRot[2][0] = xaxis.z;

        kRot[0][1] = yaxis.x;
        kRot[1][1] = yaxis.y;
        kRot[2][1] = yaxis.z;

        kRot[0][2] = zaxis.x;
        kRot[1][2] = zaxis.y;
        kRot[2][2] = zaxis.z;

        this->FromRotationMatrix(kRot);
    }

    void mmFix32Quaternion::ToAxes(mmFix32Vector3* akAxis) const
    {
        mmFix32Matrix3x3 kRot;

        this->ToRotationMatrix(kRot);

        for (size_t iCol = 0; iCol < 3; iCol++)
        {
            akAxis[iCol].x = kRot[0][iCol];
            akAxis[iCol].y = kRot[1][iCol];
            akAxis[iCol].z = kRot[2][iCol];
        }
    }

    void mmFix32Quaternion::ToAxes(mmFix32Vector3& xaxis, mmFix32Vector3& yaxis, mmFix32Vector3& zaxis) const
    {
        mmFix32Matrix3x3 kRot;

        this->ToRotationMatrix(kRot);

        xaxis.x = kRot[0][0];
        xaxis.y = kRot[1][0];
        xaxis.z = kRot[2][0];

        yaxis.x = kRot[0][1];
        yaxis.y = kRot[1][1];
        yaxis.z = kRot[2][1];

        zaxis.x = kRot[0][2];
        zaxis.y = kRot[1][2];
        zaxis.z = kRot[2][2];
    }

    mmFix32Vector3 mmFix32Quaternion::XAxis(void) const
    {
        //Real fTx  = 2.0*x;
        mmFix32 fTy = 2.0f*y;
        mmFix32 fTz = 2.0f*z;
        mmFix32 fTwy = fTy * w;
        mmFix32 fTwz = fTz * w;
        mmFix32 fTxy = fTy * x;
        mmFix32 fTxz = fTz * x;
        mmFix32 fTyy = fTy * y;
        mmFix32 fTzz = fTz * z;

        return mmFix32Vector3(1.0f - (fTyy + fTzz), fTxy + fTwz, fTxz - fTwy);
    }

    mmFix32Vector3 mmFix32Quaternion::YAxis(void) const
    {
        mmFix32 fTx = 2.0f*x;
        mmFix32 fTy = 2.0f*y;
        mmFix32 fTz = 2.0f*z;
        mmFix32 fTwx = fTx * w;
        mmFix32 fTwz = fTz * w;
        mmFix32 fTxx = fTx * x;
        mmFix32 fTxy = fTy * x;
        mmFix32 fTyz = fTz * y;
        mmFix32 fTzz = fTz * z;

        return mmFix32Vector3(fTxy - fTwz, 1.0f - (fTxx + fTzz), fTyz + fTwx);
    }

    mmFix32Vector3 mmFix32Quaternion::ZAxis(void) const
    {
        mmFix32 fTx = 2.0f*x;
        mmFix32 fTy = 2.0f*y;
        mmFix32 fTz = 2.0f*z;
        mmFix32 fTwx = fTx * w;
        mmFix32 fTwy = fTy * w;
        mmFix32 fTxx = fTx * x;
        mmFix32 fTxz = fTz * x;
        mmFix32 fTyy = fTy * y;
        mmFix32 fTyz = fTz * y;

        return mmFix32Vector3(fTxz + fTwy, fTyz - fTwx, 1.0f - (fTxx + fTyy));
    }

    mmFix32 mmFix32Quaternion::GetRotateX(bool reprojectAxis) const
    {
        if (reprojectAxis)
        {
            // roll = atan2(localx.y, localx.x)
            // pick parts of xAxis() implementation that we need
            //          Real fTx  = 2.0*x;
            mmFix32 fTy = 2.0f*y;
            mmFix32 fTz = 2.0f*z;
            mmFix32 fTwz = fTz * w;
            mmFix32 fTxy = fTy * x;
            mmFix32 fTyy = fTy * y;
            mmFix32 fTzz = fTz * z;

            // Vector3(1.0-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);

            return mmFix32(atan2(fTxy + fTwz, 1.0f - (fTyy + fTzz)));

        }
        else
        {
            return mmFix32(atan2(2 * (x*y + w * z), w*w + x * x - y * y - z * z));
        }
    }

    mmFix32 mmFix32Quaternion::GetRotateY(bool reprojectAxis) const
    {
        if (reprojectAxis)
        {
            // pitch = atan2(localy.z, localy.y)
            // pick parts of yAxis() implementation that we need
            mmFix32 fTx = 2.0f*x;
            //          Real fTy  = 2.0f*y;
            mmFix32 fTz = 2.0f*z;
            mmFix32 fTwx = fTx * w;
            mmFix32 fTxx = fTx * x;
            mmFix32 fTyz = fTz * y;
            mmFix32 fTzz = fTz * z;

            // Vector3(fTxy-fTwz, 1.0-(fTxx+fTzz), fTyz+fTwx);
            return mmFix32(atan2(fTyz + fTwx, 1.0f - (fTxx + fTzz)));
        }
        else
        {
            // internal version
            return mmFix32(atan2(2 * (y*z + w * x), w*w - x * x - y * y + z * z));
        }
    }

    mmFix32 mmFix32Quaternion::GetRotateZ(bool reprojectAxis) const
    {
        if (reprojectAxis)
        {
            // yaw = atan2(localz.x, localz.z)
            // pick parts of zAxis() implementation that we need
            mmFix32 fTx = 2.0f*x;
            mmFix32 fTy = 2.0f*y;
            mmFix32 fTz = 2.0f*z;
            mmFix32 fTwy = fTy * w;
            mmFix32 fTxx = fTx * x;
            mmFix32 fTxz = fTz * x;
            mmFix32 fTyy = fTy * y;

            // Vector3(fTxz+fTwy, fTyz-fTwx, 1.0-(fTxx+fTyy));

            return mmFix32(atan2(fTxz + fTwy, 1.0f - (fTxx + fTyy)));

        }
        else
        {
            // internal version
            return mmFix32(asin(-2 * (x*z - w * y)));
        }
    }

    bool mmFix32Quaternion::Equals(const mmFix32Quaternion& rhs, const mmFix32& tolerance) const
    {
        mmFix32 d = this->DotProduct(rhs);
        mmFix32 angle = acos(2.0f * d*d - 1.0f);

        return fabs(angle) <= tolerance;
    }

    mmFix32Quaternion mmFix32Quaternion::GetRotationTo(const mmFix32Vector3& rkVector, const mmFix32Vector3& dest, const mmFix32Vector3& fallbackAxis /*= mmFix32Vector3::ZERO*/)
    {
        // From Sam Hocevar's article "Quaternion from two vectors:
        // the final version"
        mmFix32 a = sqrt(rkVector.SquaredLength() * dest.SquaredLength());
        mmFix32 b = a + rkVector.DotProduct(dest);
        mmFix32Vector3 axis;

        if (b < (mmFix32)1e-06 * a)
        {
            b = (mmFix32)0.0;
            axis = fallbackAxis != mmFix32Vector3::ZERO ? fallbackAxis
                : fabs(rkVector.x) > fabs(rkVector.z) ? mmFix32Vector3(-rkVector.y, rkVector.x, (mmFix32)0.0)
                : mmFix32Vector3((mmFix32)0.0, -rkVector.z, rkVector.y);
        }
        else
        {
            axis = rkVector.CrossProduct(dest);
        }

        mmFix32Quaternion q(b, axis.x, axis.y, axis.z);
        q.Normalise();
        return q;
    }

    mmFix32Quaternion mmFix32Quaternion::Slerp(
        mmFix32 fT,
        const mmFix32Quaternion& rkP,
        const mmFix32Quaternion& rkQ,
        bool shortestPath/*= false*/)
    {
        mmFix32 fCos = rkP.DotProduct(rkQ);
        mmFix32Quaternion rkT;

        // Do we need to invert rotation?
        if (fCos < 0.0f && shortestPath)
        {
            fCos = -fCos;
            rkT = -rkQ;
        }
        else
        {
            rkT = rkQ;
        }

        if (fabs(fCos) < 1 - EPSILON)
        {
            // Standard case (slerp)
            mmFix32 fSin = sqrt(1 - squared(fCos));
            mmFix32 fAngle = atan2(fSin, fCos);
            mmFix32 fInvSin = 1.0f / fSin;
            mmFix32 fCoeff0 = sin((1.0f - fT) * fAngle) * fInvSin;
            mmFix32 fCoeff1 = sin(fT * fAngle) * fInvSin;
            return fCoeff0 * rkP + fCoeff1 * rkT;
        }
        else
        {
            // There are two situations:
            // 1. "rkP" and "rkQ" are very close (fCos ~= +1), so we can do a linear
            //    interpolation safely.
            // 2. "rkP" and "rkQ" are almost inverse of each other (fCos ~= -1), there
            //    are an infinite number of possibilities interpolation. but we haven't
            //    have method to fix this case, so just use linear interpolation here.
            mmFix32Quaternion t = (1.0f - fT) * rkP + fT * rkT;
            // taking the complement requires renormalisation
            t.Normalise();
            return t;
        }
    }

    mmFix32Quaternion mmFix32Quaternion::SlerpExtraSpins(
        mmFix32 fT,
        const mmFix32Quaternion& rkP,
        const mmFix32Quaternion& rkQ,
        int iExtraSpins)
    {
        mmFix32 fCos = rkP.DotProduct(rkQ);
        mmFix32 fAngle(acos(fCos));

        if (fabs(fAngle) < msEpsilon)
            return rkP;

        mmFix32 fSin = sin(fAngle);
        mmFix32 fPhase(mmFix32::MM_PI*iExtraSpins*fT);
        mmFix32 fInvSin = 1.0f / fSin;
        mmFix32 fCoeff0 = sin((1.0f - fT)*fAngle - fPhase)*fInvSin;
        mmFix32 fCoeff1 = sin(fT*fAngle + fPhase)*fInvSin;
        return fCoeff0 * rkP + fCoeff1 * rkQ;
    }

    void mmFix32Quaternion::Intermediate(
        const mmFix32Quaternion& rkQ0,
        const mmFix32Quaternion& rkQ1,
        const mmFix32Quaternion& rkQ2,
        mmFix32Quaternion& rkA,
        mmFix32Quaternion& rkB)
    {
        // assert:  q0, q1, q2 are unit quaternions

        mmFix32Quaternion kQ0inv = rkQ0.UnitInverse();
        mmFix32Quaternion kQ1inv = rkQ1.UnitInverse();
        mmFix32Quaternion rkP0 = kQ0inv * rkQ1;
        mmFix32Quaternion rkP1 = kQ1inv * rkQ2;
        mmFix32Quaternion kArg = 0.25*(rkP0.QuaternionLog() - rkP1.QuaternionLog());
        mmFix32Quaternion kMinusArg = -kArg;

        rkA = rkQ1 * kArg.QuaternionExp();
        rkB = rkQ1 * kMinusArg.QuaternionExp();
    }

    mmFix32Quaternion mmFix32Quaternion::Squad(
        mmFix32 fT,
        const mmFix32Quaternion& rkP,
        const mmFix32Quaternion& rkA,
        const mmFix32Quaternion& rkB,
        const mmFix32Quaternion& rkQ,
        bool shortestPath/*= false*/)
    {
        mmFix32 fSlerpT = 2.0f*fT*(1.0f - fT);
        mmFix32Quaternion kSlerpP = Slerp(fT, rkP, rkQ, shortestPath);
        mmFix32Quaternion kSlerpQ = Slerp(fT, rkA, rkB);
        return Slerp(fSlerpT, kSlerpP, kSlerpQ);
    }

    mmFix32Quaternion mmFix32Quaternion::Nlerp(
        mmFix32 fT,
        const mmFix32Quaternion& rkP,
        const mmFix32Quaternion& rkQ,
        bool shortestPath /*= false*/)
    {
        mmFix32Quaternion result;
        mmFix32 fCos = rkP.DotProduct(rkQ);
        if (fCos < 0.0f && shortestPath)
        {
            result = rkP + fT * ((-rkQ) - rkP);
        }
        else
        {
            result = rkP + fT * (rkQ - rkP);
        }
        result.Normalise();
        return result;
    }

    MM_EXPORT_FIX32 std::ostream& operator << (std::ostream& o, const mmFix32Quaternion& q)
    {
        o << "Quaternion(" << q.w << ", " << q.x << ", " << q.y << ", " << q.z << ")";
        return o;
    }
}
