/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFix32Vector3.h"

namespace mm
{
    const mmFix32Vector3 mmFix32Vector3::ZERO(0, 0, 0);

    const mmFix32Vector3 mmFix32Vector3::UNIT_X(1, 0, 0);
    const mmFix32Vector3 mmFix32Vector3::UNIT_Y(0, 1, 0);
    const mmFix32Vector3 mmFix32Vector3::UNIT_Z(0, 0, 1);
    const mmFix32Vector3 mmFix32Vector3::NEGATIVE_UNIT_X(-1, 0, 0);
    const mmFix32Vector3 mmFix32Vector3::NEGATIVE_UNIT_Y(0, -1, 0);
    const mmFix32Vector3 mmFix32Vector3::NEGATIVE_UNIT_Z(0, 0, -1);
    const mmFix32Vector3 mmFix32Vector3::UNIT_SCALE(1, 1, 1);

    mmFix32Vector3 mmFix32Vector3::Perpendicular(void) const
    {
        // From Sam Hocevar's article "On picking an orthogonal
        // vector (and combing coconuts)"
        mmFix32Vector3 perp = fabs(x) > fabs(z)
            ? mmFix32Vector3(-y, x, (mmFix32)0.0) : mmFix32Vector3((mmFix32)0.0, -z, y);

        perp.Normalise();

        return perp;
    }

    mmFix32 mmFix32Vector3::Normalise()
    {
        mmFix32 fLength = sqrt(x * x + y * y + z * z);

        // Will also work for zero-sized vectors, but will change nothing
        // We're not using epsilons because we don't need to.
        if (fLength > mmFix32(0.0f))
        {
            mmFix32 fInvLength = mmFix32(1.0f) / fLength;
            this->x *= fInvLength;
            this->y *= fInvLength;
            this->z *= fInvLength;
        }

        return fLength;
    }

    mmFix32 mmFix32Vector3::AngleBetween(const mmFix32Vector3& dest) const
    {
        mmFix32 lenProduct = this->Length() * dest.Length();

        // Divide by zero check
        if (lenProduct < 1e-6f)
            lenProduct = 1e-6f;

        mmFix32 f = this->DotProduct(dest) / lenProduct;

        f = clamp(f, (mmFix32)-1.0, (mmFix32)1.0);
        return acos(f);

    }

    MM_EXPORT_FIX32 std::ostream& operator << (std::ostream& o, const mmFix32Vector3& v)
    {
        o << "Vector3(" << v.x << ", " << v.y << ", " << v.z << ")";
        return o;
    }
}
