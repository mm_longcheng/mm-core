/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFix32Matrix4x4_h__
#define __mmFix32Matrix4x4_h__

#include "core/mmCore.h"

#include "fix32/mmFix32.h"

#include "fix32/mmFix32Vector4.h"
#include "fix32/mmFix32Quaternion.h"
#include "fix32/mmFix32Matrix3x3.h"

#include <assert.h>

#include "fix32/mmFix32Export.h"

// https://www.ogre3d.org/

namespace mm
{
    /** \addtogroup Core
     *  @{
     */
    /** \addtogroup Math
     *  @{
     */
    /** Class encapsulating a standard 4x4 homogeneous matrix.
     *  @remarks
     *      OGRE uses column vectors when applying matrix multiplications,
     *      This means a vector is represented as a single column, 4-row
     *      matrix. This has the effect that the transformations implemented
     *      by the matrices happens right-to-left e.g. if vector V is to be
     *      transformed by M1 then M2 then M3, the calculation would be
     *      M3 * M2 * M1 * V. The order that matrices are concatenated is
     *      vital since matrix multiplication is not commutative, i.e. you
     *      can get a different result if you concatenate in the wrong order.
     *  @par
     *      The use of column vectors and right-to-left ordering is the
     *      standard in most mathematical texts, and is the same as used in
     *      OpenGL. It is, however, the opposite of Direct3D, which has
     *      inexplicably chosen to differ from the accepted standard and uses
     *      row vectors and left-to-right matrix multiplication.
     *  @par
     *      OGRE deals with the differences between D3D and OpenGL etc.
     *      internally when operating through different render systems. OGRE
     *      users only need to conform to standard maths conventions, i.e.
     *      right-to-left matrix multiplication, (OGRE transposes matrices it
     *      passes to D3D to compensate).
     *  @par
     *      The generic form M * V which shows the layout of the matrix
     *      entries is shown below:
     *      <pre>
     *          [ m[0][0]  m[0][1]  m[0][2]  m[0][3] ]   {x}
     *          | m[1][0]  m[1][1]  m[1][2]  m[1][3] | * {y}
     *          | m[2][0]  m[2][1]  m[2][2]  m[2][3] |   {z}
     *          [ m[3][0]  m[3][1]  m[3][2]  m[3][3] ]   {1}
     *      </pre>
     */
    struct MM_EXPORT_FIX32 mmFix32Matrix4x4
    {
    public:
        static const mmFix32Matrix4x4 ZERO;
        static const mmFix32Matrix4x4 ZEROAFFINE;
        static const mmFix32Matrix4x4 IDENTITY;
    public:
        /// The matrix entries, indexed by [row][col].
        mmFix32 m[4][4];
    public:
        inline mmFix32Matrix4x4()
        {
            mmMemset(this->m, 0, sizeof(mmFix32) * 16);
        }

        inline mmFix32Matrix4x4(const mmFix32Matrix4x4& v)
        {
            mmMemcpy(this->m, v.m, sizeof(mmFix32) * 16);
        }

        mmFix32Matrix4x4(
            mmFix32 m00, mmFix32 m01, mmFix32 m02, mmFix32 m03,
            mmFix32 m10, mmFix32 m11, mmFix32 m12, mmFix32 m13,
            mmFix32 m20, mmFix32 m21, mmFix32 m22, mmFix32 m23,
            mmFix32 m30, mmFix32 m31, mmFix32 m32, mmFix32 m33);

        /** Creates a standard 4x4 transformation matrix with a zero translation part from a rotation/scaling 3x3 matrix.
        */
        inline mmFix32Matrix4x4(const mmFix32Matrix3x3& m3x3)
        {
            operator=(IDENTITY);
            operator=(m3x3);
        }

        /** Creates a standard 4x4 transformation matrix with a zero translation part from a rotation/scaling Quaternion.
        */
        inline mmFix32Matrix4x4(const mmFix32Quaternion& rot)
        {
            mmFix32Matrix3x3 m3x3;
            rot.ToRotationMatrix(m3x3);
            operator=(IDENTITY);
            operator=(m3x3);
        }
    public:
        inline mmFix32Matrix4x4& operator = (const mmFix32Matrix4x4& rkMatrix)
        {
            mmMemcpy(m, rkMatrix.m, 16 * sizeof(mmFix32));
            return *this;
        }

        /** Assignment from 3x3 matrix.
        */
        void operator = (const mmFix32Matrix3x3& mat3);
    public:
        /** Exchange the contents of this matrix with another.
        */
        void Swap(mmFix32Matrix4x4& other);
    public:
        inline mmFix32* operator [] (size_t iRow)
        {
            assert(iRow < 4);
            return m[iRow];
        }

        inline const mmFix32 *operator [] (size_t iRow) const
        {
            assert(iRow < 4);
            return m[iRow];
        }
    public:
        mmFix32Matrix4x4 Concatenate(const mmFix32Matrix4x4 &m2) const;

        /** Matrix concatenation using '*'.
        */
        inline mmFix32Matrix4x4 operator * (const mmFix32Matrix4x4 &m2) const
        {
            return this->Concatenate(m2);
        }

        /** Vector transformation using '*'.
         *  @remarks
         *      Transforms the given 3-D vector by the matrix, projecting the
         *      result back into <i>w</i> = 1.
         *  @note
         *      This means that the initial <i>w</i> is considered to be 1.0,
         *      and then all the tree elements of the resulting 3-D vector are
         *      divided by the resulting <i>w</i>.
         */
        mmFix32Matrix4x4 operator * (mmFix32 scalar) const;

        /** Removed from Vector4 and made a non-member here because otherwise
         *  OgreMatrix4.h and OgreVector4.h have to try to include and inline each
         *  other, which frankly doesn't work ;)
         */
        friend mmFix32Vector4 operator * (const mmFix32Vector4& v, const mmFix32Matrix4x4& mat);
    public:
        /** Matrix addition.
        */
        mmFix32Matrix4x4 operator + (const mmFix32Matrix4x4 &m2) const;

        /** Matrix subtraction.
        */
        mmFix32Matrix4x4 operator - (const mmFix32Matrix4x4 &m2) const;

        /** Tests 2 matrices for equality.
        */
        bool operator == (const mmFix32Matrix4x4& m2) const;

        /** Tests 2 matrices for inequality.
        */
        inline bool operator != (const mmFix32Matrix4x4& m2) const
        {
            return !(operator ==(m2));
        }
    public:
        mmFix32Matrix4x4 Transpose(void) const;
        mmFix32Matrix4x4 Adjoint() const;
        mmFix32 Determinant() const;
        mmFix32Matrix4x4 Inverse() const;
    public:
        /*
        -----------------------------------------------------------------------
        Translation Transformation
        -----------------------------------------------------------------------
        */
        /** Sets the translation transformation part of the matrix.
        */
        inline void SetTranslation(const mmFix32Vector3& v)
        {
            m[0][3] = v.x;
            m[1][3] = v.y;
            m[2][3] = v.z;
        }

        /** Extracts the translation transformation part of the matrix.
        */
        inline mmFix32Vector3 GetTranslation() const
        {
            return mmFix32Vector3(m[0][3], m[1][3], m[2][3]);
        }


        /** Builds a translation matrix
        */
        void MakeTranslation(const mmFix32Vector3& v);

        void MakeTranslation(mmFix32 tx, mmFix32 ty, mmFix32 tz);

        /** Gets a translation matrix.
        */
        static mmFix32Matrix4x4 GetTranslation(const mmFix32Vector3& v);

        /** Gets a translation matrix - variation for not using a vector.
        */
        static mmFix32Matrix4x4 GetTranslation(mmFix32 t_x, mmFix32 t_y, mmFix32 t_z);
    public:
        /*
        -----------------------------------------------------------------------
        Scale Transformation
        -----------------------------------------------------------------------
        */
        /** Sets the scale part of the matrix.
        */
        inline void SetScale(const mmFix32Vector3& v)
        {
            m[0][0] = v.x;
            m[1][1] = v.y;
            m[2][2] = v.z;
        }

        /** Gets a scale matrix.
        */
        static mmFix32Matrix4x4 GetScale(const mmFix32Vector3& v);

        /** Gets a scale matrix - variation for not using a vector.
        */
        static mmFix32Matrix4x4 GetScale(mmFix32 s_x, mmFix32 s_y, mmFix32 s_z);

        /** Extracts the rotation / scaling part of the Matrix as a 3x3 matrix.
        @param m3x3 Destination Matrix3
        */
        void Extract3x3Matrix(mmFix32Matrix3x3& m3x3) const;

        /** Determines if this matrix involves a negative scaling. */
        inline bool HasNegativeScale() const
        {
            return this->Determinant() < 0;
        }

        /** Extracts the rotation / scaling part as a quaternion from the Matrix.
        */
        inline mmFix32Quaternion ExtractQuaternion() const
        {
            mmFix32Matrix3x3 m3x3;
            this->Extract3x3Matrix(m3x3);
            return mmFix32Quaternion(m3x3);
        }
    public:
        /** Building a Matrix4 from orientation / scale / position.
         *  @remarks
         *      Transform is performed in the order scale, rotate, translation, i.e. translation is independent
         *      of orientation axes, scale does not affect size of translation, rotation and scaling are always
         *      centered on the origin.
         */
        void MakeTransform(const mmFix32Vector3& position, const mmFix32Vector3& scale, const mmFix32Quaternion& orientation);

        /** Building an inverse Matrix4 from orientation / scale / position.
         *  @remarks
         *      As makeTransform except it build the inverse given the same data as makeTransform, so
         *      performing -translation, -rotate, 1/scale in that order.
         */
        void MakeInverseTransform(const mmFix32Vector3& position, const mmFix32Vector3& scale, const mmFix32Quaternion& orientation);

        /** Decompose a Matrix4 to orientation / scale / position.
        */
        void Decomposition(mmFix32Vector3& position, mmFix32Vector3& scale, mmFix32Quaternion& orientation) const;
    public:
        /** Check whether or not the matrix is affine matrix.
         *  @remarks
         *      An affine matrix is a 4x4 matrix with row 3 equal to (0, 0, 0, 1),
         *      e.g. no projective coefficients.
         */
        inline bool IsAffine(void) const
        {
            return m[3][0] == 0 && m[3][1] == 0 && m[3][2] == 0 && m[3][3] == 1;
        }

        /** Returns the inverse of the affine matrix.
         *  @note
         *      The matrix must be an affine matrix. @see Matrix4::isAffine.
         */
        mmFix32Matrix4x4 InverseAffine(void) const;

        /** Concatenate two affine matrices.
         *  @note
         *      The matrices must be affine matrix. @see Matrix4::isAffine.
         */
        mmFix32Matrix4x4 ConcatenateAffine(const mmFix32Matrix4x4 &m2) const;

        /** 3-D Vector transformation specially for an affine matrix.
         *  @remarks
         *      Transforms the given 3-D vector by the 3x3 submatrix, without
         *      adding translation, as should be transformed directions and normals.
         *  @note
         *      The matrix must be an affine matrix. @see Matrix4::isAffine.
         */
        mmFix32Vector3 TransformDirectionAffine(const mmFix32Vector3& v) const;

        /** 3-D Vector transformation specially for an affine matrix.
         *  @remarks
         *      Transforms the given 3-D vector by the matrix, projecting the
         *      result back into <i>w</i> = 1.
         *  @note
         *      The matrix must be an affine matrix. @see Matrix4::isAffine.
         */
        mmFix32Vector3 TransformAffine(const mmFix32Vector3& v) const;

        /** 4-D Vector transformation specially for an affine matrix.
         *  @note
         *      The matrix must be an affine matrix. @see Matrix4::isAffine.
         */
        mmFix32Vector4 TransformAffine(const mmFix32Vector4& v) const;
    public:
        MM_EXPORT_FIX32 friend std::ostream& operator << (std::ostream& o, const mmFix32Matrix4x4& mat);
    };
    /** @} */
    /** @} */
}

#endif//__mmFix32Matrix4x4_h__
