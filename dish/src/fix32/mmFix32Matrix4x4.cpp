/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "mmFix32Matrix4x4.h"

namespace mm
{
    const mmFix32Matrix4x4 mmFix32Matrix4x4::ZERO
    (
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    );

    const mmFix32Matrix4x4 mmFix32Matrix4x4::ZEROAFFINE
    (
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 1
    );

    const mmFix32Matrix4x4 mmFix32Matrix4x4::IDENTITY
    (
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    );

    inline static mmFix32 MINOR(
        const mmFix32Matrix4x4& m,
        const size_t r0, const size_t r1, const size_t r2,
        const size_t c0, const size_t c1, const size_t c2)
    {
        return
            m[r0][c0] * (m[r1][c1] * m[r2][c2] - m[r2][c1] * m[r1][c2]) -
            m[r0][c1] * (m[r1][c0] * m[r2][c2] - m[r2][c0] * m[r1][c2]) +
            m[r0][c2] * (m[r1][c0] * m[r2][c1] - m[r2][c0] * m[r1][c1]);
    }

    mmFix32Matrix4x4::mmFix32Matrix4x4(
        mmFix32 m00, mmFix32 m01, mmFix32 m02, mmFix32 m03,
        mmFix32 m10, mmFix32 m11, mmFix32 m12, mmFix32 m13,
        mmFix32 m20, mmFix32 m21, mmFix32 m22, mmFix32 m23,
        mmFix32 m30, mmFix32 m31, mmFix32 m32, mmFix32 m33)
    {
        m[0][0] = m00;
        m[0][1] = m01;
        m[0][2] = m02;
        m[0][3] = m03;
        m[1][0] = m10;
        m[1][1] = m11;
        m[1][2] = m12;
        m[1][3] = m13;
        m[2][0] = m20;
        m[2][1] = m21;
        m[2][2] = m22;
        m[2][3] = m23;
        m[3][0] = m30;
        m[3][1] = m31;
        m[3][2] = m32;
        m[3][3] = m33;
    }

    void mmFix32Matrix4x4::operator = (const mmFix32Matrix3x3& mat3)
    {
        m[0][0] = mat3.m[0][0]; m[0][1] = mat3.m[0][1]; m[0][2] = mat3.m[0][2];
        m[1][0] = mat3.m[1][0]; m[1][1] = mat3.m[1][1]; m[1][2] = mat3.m[1][2];
        m[2][0] = mat3.m[2][0]; m[2][1] = mat3.m[2][1]; m[2][2] = mat3.m[2][2];
    }

    void mmFix32Matrix4x4::Swap(mmFix32Matrix4x4& other)
    {
        std::swap(m[0][0], other.m[0][0]);
        std::swap(m[0][1], other.m[0][1]);
        std::swap(m[0][2], other.m[0][2]);
        std::swap(m[0][3], other.m[0][3]);
        std::swap(m[1][0], other.m[1][0]);
        std::swap(m[1][1], other.m[1][1]);
        std::swap(m[1][2], other.m[1][2]);
        std::swap(m[1][3], other.m[1][3]);
        std::swap(m[2][0], other.m[2][0]);
        std::swap(m[2][1], other.m[2][1]);
        std::swap(m[2][2], other.m[2][2]);
        std::swap(m[2][3], other.m[2][3]);
        std::swap(m[3][0], other.m[3][0]);
        std::swap(m[3][1], other.m[3][1]);
        std::swap(m[3][2], other.m[3][2]);
        std::swap(m[3][3], other.m[3][3]);
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::Concatenate(const mmFix32Matrix4x4 &m2) const
    {
        mmFix32Matrix4x4 r;
        r.m[0][0] = m[0][0] * m2.m[0][0] + m[0][1] * m2.m[1][0] + m[0][2] * m2.m[2][0] + m[0][3] * m2.m[3][0];
        r.m[0][1] = m[0][0] * m2.m[0][1] + m[0][1] * m2.m[1][1] + m[0][2] * m2.m[2][1] + m[0][3] * m2.m[3][1];
        r.m[0][2] = m[0][0] * m2.m[0][2] + m[0][1] * m2.m[1][2] + m[0][2] * m2.m[2][2] + m[0][3] * m2.m[3][2];
        r.m[0][3] = m[0][0] * m2.m[0][3] + m[0][1] * m2.m[1][3] + m[0][2] * m2.m[2][3] + m[0][3] * m2.m[3][3];

        r.m[1][0] = m[1][0] * m2.m[0][0] + m[1][1] * m2.m[1][0] + m[1][2] * m2.m[2][0] + m[1][3] * m2.m[3][0];
        r.m[1][1] = m[1][0] * m2.m[0][1] + m[1][1] * m2.m[1][1] + m[1][2] * m2.m[2][1] + m[1][3] * m2.m[3][1];
        r.m[1][2] = m[1][0] * m2.m[0][2] + m[1][1] * m2.m[1][2] + m[1][2] * m2.m[2][2] + m[1][3] * m2.m[3][2];
        r.m[1][3] = m[1][0] * m2.m[0][3] + m[1][1] * m2.m[1][3] + m[1][2] * m2.m[2][3] + m[1][3] * m2.m[3][3];

        r.m[2][0] = m[2][0] * m2.m[0][0] + m[2][1] * m2.m[1][0] + m[2][2] * m2.m[2][0] + m[2][3] * m2.m[3][0];
        r.m[2][1] = m[2][0] * m2.m[0][1] + m[2][1] * m2.m[1][1] + m[2][2] * m2.m[2][1] + m[2][3] * m2.m[3][1];
        r.m[2][2] = m[2][0] * m2.m[0][2] + m[2][1] * m2.m[1][2] + m[2][2] * m2.m[2][2] + m[2][3] * m2.m[3][2];
        r.m[2][3] = m[2][0] * m2.m[0][3] + m[2][1] * m2.m[1][3] + m[2][2] * m2.m[2][3] + m[2][3] * m2.m[3][3];

        r.m[3][0] = m[3][0] * m2.m[0][0] + m[3][1] * m2.m[1][0] + m[3][2] * m2.m[2][0] + m[3][3] * m2.m[3][0];
        r.m[3][1] = m[3][0] * m2.m[0][1] + m[3][1] * m2.m[1][1] + m[3][2] * m2.m[2][1] + m[3][3] * m2.m[3][1];
        r.m[3][2] = m[3][0] * m2.m[0][2] + m[3][1] * m2.m[1][2] + m[3][2] * m2.m[2][2] + m[3][3] * m2.m[3][2];
        r.m[3][3] = m[3][0] * m2.m[0][3] + m[3][1] * m2.m[1][3] + m[3][2] * m2.m[2][3] + m[3][3] * m2.m[3][3];

        return r;
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::operator * (mmFix32 scalar) const
    {
        return mmFix32Matrix4x4(
            scalar*m[0][0], scalar*m[0][1], scalar*m[0][2], scalar*m[0][3],
            scalar*m[1][0], scalar*m[1][1], scalar*m[1][2], scalar*m[1][3],
            scalar*m[2][0], scalar*m[2][1], scalar*m[2][2], scalar*m[2][3],
            scalar*m[3][0], scalar*m[3][1], scalar*m[3][2], scalar*m[3][3]);
    }

    mmFix32Vector4 operator * (const mmFix32Vector4& v, const mmFix32Matrix4x4& mat)
    {
        return mmFix32Vector4(
            v.x*mat[0][0] + v.y*mat[1][0] + v.z*mat[2][0] + v.w*mat[3][0],
            v.x*mat[0][1] + v.y*mat[1][1] + v.z*mat[2][1] + v.w*mat[3][1],
            v.x*mat[0][2] + v.y*mat[1][2] + v.z*mat[2][2] + v.w*mat[3][2],
            v.x*mat[0][3] + v.y*mat[1][3] + v.z*mat[2][3] + v.w*mat[3][3]
        );
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::operator + (const mmFix32Matrix4x4 &m2) const
    {
        mmFix32Matrix4x4 r;

        r.m[0][0] = m[0][0] + m2.m[0][0];
        r.m[0][1] = m[0][1] + m2.m[0][1];
        r.m[0][2] = m[0][2] + m2.m[0][2];
        r.m[0][3] = m[0][3] + m2.m[0][3];

        r.m[1][0] = m[1][0] + m2.m[1][0];
        r.m[1][1] = m[1][1] + m2.m[1][1];
        r.m[1][2] = m[1][2] + m2.m[1][2];
        r.m[1][3] = m[1][3] + m2.m[1][3];

        r.m[2][0] = m[2][0] + m2.m[2][0];
        r.m[2][1] = m[2][1] + m2.m[2][1];
        r.m[2][2] = m[2][2] + m2.m[2][2];
        r.m[2][3] = m[2][3] + m2.m[2][3];

        r.m[3][0] = m[3][0] + m2.m[3][0];
        r.m[3][1] = m[3][1] + m2.m[3][1];
        r.m[3][2] = m[3][2] + m2.m[3][2];
        r.m[3][3] = m[3][3] + m2.m[3][3];

        return r;
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::operator - (const mmFix32Matrix4x4 &m2) const
    {
        mmFix32Matrix4x4 r;

        r.m[0][0] = m[0][0] - m2.m[0][0];
        r.m[0][1] = m[0][1] - m2.m[0][1];
        r.m[0][2] = m[0][2] - m2.m[0][2];
        r.m[0][3] = m[0][3] - m2.m[0][3];

        r.m[1][0] = m[1][0] - m2.m[1][0];
        r.m[1][1] = m[1][1] - m2.m[1][1];
        r.m[1][2] = m[1][2] - m2.m[1][2];
        r.m[1][3] = m[1][3] - m2.m[1][3];

        r.m[2][0] = m[2][0] - m2.m[2][0];
        r.m[2][1] = m[2][1] - m2.m[2][1];
        r.m[2][2] = m[2][2] - m2.m[2][2];
        r.m[2][3] = m[2][3] - m2.m[2][3];

        r.m[3][0] = m[3][0] - m2.m[3][0];
        r.m[3][1] = m[3][1] - m2.m[3][1];
        r.m[3][2] = m[3][2] - m2.m[3][2];
        r.m[3][3] = m[3][3] - m2.m[3][3];

        return r;
    }

    bool mmFix32Matrix4x4::operator == (const mmFix32Matrix4x4& m2) const
    {
        return
            m[0][0] != m2.m[0][0] || m[0][1] != m2.m[0][1] || m[0][2] != m2.m[0][2] || m[0][3] != m2.m[0][3] ||
            m[1][0] != m2.m[1][0] || m[1][1] != m2.m[1][1] || m[1][2] != m2.m[1][2] || m[1][3] != m2.m[1][3] ||
            m[2][0] != m2.m[2][0] || m[2][1] != m2.m[2][1] || m[2][2] != m2.m[2][2] || m[2][3] != m2.m[2][3] ||
            m[3][0] != m2.m[3][0] || m[3][1] != m2.m[3][1] || m[3][2] != m2.m[3][2] || m[3][3] != m2.m[3][3];
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::Transpose(void) const
    {
        return mmFix32Matrix4x4(
            m[0][0], m[1][0], m[2][0], m[3][0],
            m[0][1], m[1][1], m[2][1], m[3][1],
            m[0][2], m[1][2], m[2][2], m[3][2],
            m[0][3], m[1][3], m[2][3], m[3][3]);
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::Adjoint() const
    {
        return mmFix32Matrix4x4(
            MINOR(*this, 1, 2, 3, 1, 2, 3),
            -MINOR(*this, 0, 2, 3, 1, 2, 3),
            MINOR(*this, 0, 1, 3, 1, 2, 3),
            -MINOR(*this, 0, 1, 2, 1, 2, 3),

            -MINOR(*this, 1, 2, 3, 0, 2, 3),
            MINOR(*this, 0, 2, 3, 0, 2, 3),
            -MINOR(*this, 0, 1, 3, 0, 2, 3),
            MINOR(*this, 0, 1, 2, 0, 2, 3),

            MINOR(*this, 1, 2, 3, 0, 1, 3),
            -MINOR(*this, 0, 2, 3, 0, 1, 3),
            MINOR(*this, 0, 1, 3, 0, 1, 3),
            -MINOR(*this, 0, 1, 2, 0, 1, 3),

            -MINOR(*this, 1, 2, 3, 0, 1, 2),
            MINOR(*this, 0, 2, 3, 0, 1, 2),
            -MINOR(*this, 0, 1, 3, 0, 1, 2),
            MINOR(*this, 0, 1, 2, 0, 1, 2));
    }
    mmFix32 mmFix32Matrix4x4::Determinant() const
    {
        return
            m[0][0] * MINOR(*this, 1, 2, 3, 1, 2, 3) -
            m[0][1] * MINOR(*this, 1, 2, 3, 0, 2, 3) +
            m[0][2] * MINOR(*this, 1, 2, 3, 0, 1, 3) -
            m[0][3] * MINOR(*this, 1, 2, 3, 0, 1, 2);
    }
    mmFix32Matrix4x4 mmFix32Matrix4x4::Inverse() const
    {
        mmFix32 m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
        mmFix32 m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
        mmFix32 m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
        mmFix32 m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

        mmFix32 v0 = m20 * m31 - m21 * m30;
        mmFix32 v1 = m20 * m32 - m22 * m30;
        mmFix32 v2 = m20 * m33 - m23 * m30;
        mmFix32 v3 = m21 * m32 - m22 * m31;
        mmFix32 v4 = m21 * m33 - m23 * m31;
        mmFix32 v5 = m22 * m33 - m23 * m32;

        mmFix32 t00 = +(v5 * m11 - v4 * m12 + v3 * m13);
        mmFix32 t10 = -(v5 * m10 - v2 * m12 + v1 * m13);
        mmFix32 t20 = +(v4 * m10 - v2 * m11 + v0 * m13);
        mmFix32 t30 = -(v3 * m10 - v1 * m11 + v0 * m12);

        mmFix32 invDet = 1 / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

        mmFix32 d00 = t00 * invDet;
        mmFix32 d10 = t10 * invDet;
        mmFix32 d20 = t20 * invDet;
        mmFix32 d30 = t30 * invDet;

        mmFix32 d01 = -(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
        mmFix32 d11 = +(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
        mmFix32 d21 = -(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
        mmFix32 d31 = +(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

        v0 = m10 * m31 - m11 * m30;
        v1 = m10 * m32 - m12 * m30;
        v2 = m10 * m33 - m13 * m30;
        v3 = m11 * m32 - m12 * m31;
        v4 = m11 * m33 - m13 * m31;
        v5 = m12 * m33 - m13 * m32;

        mmFix32 d02 = +(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
        mmFix32 d12 = -(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
        mmFix32 d22 = +(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
        mmFix32 d32 = -(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

        v0 = m21 * m10 - m20 * m11;
        v1 = m22 * m10 - m20 * m12;
        v2 = m23 * m10 - m20 * m13;
        v3 = m22 * m11 - m21 * m12;
        v4 = m23 * m11 - m21 * m13;
        v5 = m23 * m12 - m22 * m13;

        mmFix32 d03 = -(v5 * m01 - v4 * m02 + v3 * m03) * invDet;
        mmFix32 d13 = +(v5 * m00 - v2 * m02 + v1 * m03) * invDet;
        mmFix32 d23 = -(v4 * m00 - v2 * m01 + v0 * m03) * invDet;
        mmFix32 d33 = +(v3 * m00 - v1 * m01 + v0 * m02) * invDet;

        return mmFix32Matrix4x4(
            d00, d01, d02, d03,
            d10, d11, d12, d13,
            d20, d21, d22, d23,
            d30, d31, d32, d33);
    }

    void mmFix32Matrix4x4::MakeTranslation(const mmFix32Vector3& v)
    {
        m[0][0] = 1.0; m[0][1] = 0.0; m[0][2] = 0.0; m[0][3] = v.x;
        m[1][0] = 0.0; m[1][1] = 1.0; m[1][2] = 0.0; m[1][3] = v.y;
        m[2][0] = 0.0; m[2][1] = 0.0; m[2][2] = 1.0; m[2][3] = v.z;
        m[3][0] = 0.0; m[3][1] = 0.0; m[3][2] = 0.0; m[3][3] = 1.0;
    }

    void mmFix32Matrix4x4::MakeTranslation(mmFix32 tx, mmFix32 ty, mmFix32 tz)
    {
        m[0][0] = 1.0; m[0][1] = 0.0; m[0][2] = 0.0; m[0][3] = tx;
        m[1][0] = 0.0; m[1][1] = 1.0; m[1][2] = 0.0; m[1][3] = ty;
        m[2][0] = 0.0; m[2][1] = 0.0; m[2][2] = 1.0; m[2][3] = tz;
        m[3][0] = 0.0; m[3][1] = 0.0; m[3][2] = 0.0; m[3][3] = 1.0;
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::GetTranslation(const mmFix32Vector3& v)
    {
        mmFix32Matrix4x4 r;

        r.m[0][0] = 1.0; r.m[0][1] = 0.0; r.m[0][2] = 0.0; r.m[0][3] = v.x;
        r.m[1][0] = 0.0; r.m[1][1] = 1.0; r.m[1][2] = 0.0; r.m[1][3] = v.y;
        r.m[2][0] = 0.0; r.m[2][1] = 0.0; r.m[2][2] = 1.0; r.m[2][3] = v.z;
        r.m[3][0] = 0.0; r.m[3][1] = 0.0; r.m[3][2] = 0.0; r.m[3][3] = 1.0;

        return r;
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::GetTranslation(mmFix32 t_x, mmFix32 t_y, mmFix32 t_z)
    {
        mmFix32Matrix4x4 r;

        r.m[0][0] = 1.0; r.m[0][1] = 0.0; r.m[0][2] = 0.0; r.m[0][3] = t_x;
        r.m[1][0] = 0.0; r.m[1][1] = 1.0; r.m[1][2] = 0.0; r.m[1][3] = t_y;
        r.m[2][0] = 0.0; r.m[2][1] = 0.0; r.m[2][2] = 1.0; r.m[2][3] = t_z;
        r.m[3][0] = 0.0; r.m[3][1] = 0.0; r.m[3][2] = 0.0; r.m[3][3] = 1.0;

        return r;
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::GetScale(const mmFix32Vector3& v)
    {
        mmFix32Matrix4x4 r;
        r.m[0][0] = v.x; r.m[0][1] = 0.0; r.m[0][2] = 0.0; r.m[0][3] = 0.0;
        r.m[1][0] = 0.0; r.m[1][1] = v.y; r.m[1][2] = 0.0; r.m[1][3] = 0.0;
        r.m[2][0] = 0.0; r.m[2][1] = 0.0; r.m[2][2] = v.z; r.m[2][3] = 0.0;
        r.m[3][0] = 0.0; r.m[3][1] = 0.0; r.m[3][2] = 0.0; r.m[3][3] = 1.0;

        return r;
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::GetScale(mmFix32 s_x, mmFix32 s_y, mmFix32 s_z)
    {
        mmFix32Matrix4x4 r;
        r.m[0][0] = s_x; r.m[0][1] = 0.0; r.m[0][2] = 0.0; r.m[0][3] = 0.0;
        r.m[1][0] = 0.0; r.m[1][1] = s_y; r.m[1][2] = 0.0; r.m[1][3] = 0.0;
        r.m[2][0] = 0.0; r.m[2][1] = 0.0; r.m[2][2] = s_z; r.m[2][3] = 0.0;
        r.m[3][0] = 0.0; r.m[3][1] = 0.0; r.m[3][2] = 0.0; r.m[3][3] = 1.0;

        return r;
    }

    void mmFix32Matrix4x4::Extract3x3Matrix(mmFix32Matrix3x3& m3x3) const
    {
        m3x3.m[0][0] = m[0][0];
        m3x3.m[0][1] = m[0][1];
        m3x3.m[0][2] = m[0][2];
        m3x3.m[1][0] = m[1][0];
        m3x3.m[1][1] = m[1][1];
        m3x3.m[1][2] = m[1][2];
        m3x3.m[2][0] = m[2][0];
        m3x3.m[2][1] = m[2][1];
        m3x3.m[2][2] = m[2][2];
    }

    void mmFix32Matrix4x4::MakeTransform(const mmFix32Vector3& position, const mmFix32Vector3& scale, const mmFix32Quaternion& orientation)
    {
        // Ordering:
        //    1. Scale
        //    2. Rotate
        //    3. Translate

        mmFix32Matrix3x3 rot3x3;
        orientation.ToRotationMatrix(rot3x3);

        // Set up final matrix with scale, rotation and translation
        m[0][0] = scale.x * rot3x3[0][0]; m[0][1] = scale.y * rot3x3[0][1]; m[0][2] = scale.z * rot3x3[0][2]; m[0][3] = position.x;
        m[1][0] = scale.x * rot3x3[1][0]; m[1][1] = scale.y * rot3x3[1][1]; m[1][2] = scale.z * rot3x3[1][2]; m[1][3] = position.y;
        m[2][0] = scale.x * rot3x3[2][0]; m[2][1] = scale.y * rot3x3[2][1]; m[2][2] = scale.z * rot3x3[2][2]; m[2][3] = position.z;

        // No projection term
        m[3][0] = 0; m[3][1] = 0; m[3][2] = 0; m[3][3] = 1;
    }

    void mmFix32Matrix4x4::MakeInverseTransform(const mmFix32Vector3& position, const mmFix32Vector3& scale, const mmFix32Quaternion& orientation)
    {
        // Invert the parameters
        mmFix32Vector3 invTranslate = -position;
        mmFix32Vector3 invScale(1 / scale.x, 1 / scale.y, 1 / scale.z);
        mmFix32Quaternion invRot = orientation.Inverse();

        // Because we're inverting, order is translation, rotation, scale
        // So make translation relative to scale & rotation
        invTranslate = invRot * invTranslate; // rotate
        invTranslate *= invScale; // scale

                                  // Next, make a 3x3 rotation matrix
        mmFix32Matrix3x3 rot3x3;
        invRot.ToRotationMatrix(rot3x3);

        // Set up final matrix with scale, rotation and translation
        m[0][0] = invScale.x * rot3x3[0][0]; m[0][1] = invScale.x * rot3x3[0][1]; m[0][2] = invScale.x * rot3x3[0][2]; m[0][3] = invTranslate.x;
        m[1][0] = invScale.y * rot3x3[1][0]; m[1][1] = invScale.y * rot3x3[1][1]; m[1][2] = invScale.y * rot3x3[1][2]; m[1][3] = invTranslate.y;
        m[2][0] = invScale.z * rot3x3[2][0]; m[2][1] = invScale.z * rot3x3[2][1]; m[2][2] = invScale.z * rot3x3[2][2]; m[2][3] = invTranslate.z;

        // No projection term
        m[3][0] = 0; m[3][1] = 0; m[3][2] = 0; m[3][3] = 1;
    }

    void mmFix32Matrix4x4::Decomposition(mmFix32Vector3& position, mmFix32Vector3& scale, mmFix32Quaternion& orientation) const
    {
        assert(this->IsAffine());

        mmFix32Matrix3x3 m3x3;
        this->Extract3x3Matrix(m3x3);

        mmFix32Matrix3x3 matQ;
        mmFix32Vector3 vecU;
        m3x3.QDUDecomposition(matQ, scale, vecU);

        orientation = mmFix32Quaternion(matQ);
        position = mmFix32Vector3(m[0][3], m[1][3], m[2][3]);
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::InverseAffine(void) const
    {
        assert(this->IsAffine());

        mmFix32 m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
        mmFix32 m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

        mmFix32 t00 = m22 * m11 - m21 * m12;
        mmFix32 t10 = m20 * m12 - m22 * m10;
        mmFix32 t20 = m21 * m10 - m20 * m11;

        mmFix32 m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];

        mmFix32 invDet = 1 / (m00 * t00 + m01 * t10 + m02 * t20);

        t00 *= invDet; t10 *= invDet; t20 *= invDet;

        m00 *= invDet; m01 *= invDet; m02 *= invDet;

        mmFix32 r00 = t00;
        mmFix32 r01 = m02 * m21 - m01 * m22;
        mmFix32 r02 = m01 * m12 - m02 * m11;

        mmFix32 r10 = t10;
        mmFix32 r11 = m00 * m22 - m02 * m20;
        mmFix32 r12 = m02 * m10 - m00 * m12;

        mmFix32 r20 = t20;
        mmFix32 r21 = m01 * m20 - m00 * m21;
        mmFix32 r22 = m00 * m11 - m01 * m10;

        mmFix32 m03 = m[0][3], m13 = m[1][3], m23 = m[2][3];

        mmFix32 r03 = -(r00 * m03 + r01 * m13 + r02 * m23);
        mmFix32 r13 = -(r10 * m03 + r11 * m13 + r12 * m23);
        mmFix32 r23 = -(r20 * m03 + r21 * m13 + r22 * m23);

        return mmFix32Matrix4x4(
            r00, r01, r02, r03,
            r10, r11, r12, r13,
            r20, r21, r22, r23,
            0, 0, 0, 1);
    }

    mmFix32Matrix4x4 mmFix32Matrix4x4::ConcatenateAffine(const mmFix32Matrix4x4 &m2) const
    {
        assert(this->IsAffine() && m2.IsAffine());

        return mmFix32Matrix4x4(
            m[0][0] * m2.m[0][0] + m[0][1] * m2.m[1][0] + m[0][2] * m2.m[2][0],
            m[0][0] * m2.m[0][1] + m[0][1] * m2.m[1][1] + m[0][2] * m2.m[2][1],
            m[0][0] * m2.m[0][2] + m[0][1] * m2.m[1][2] + m[0][2] * m2.m[2][2],
            m[0][0] * m2.m[0][3] + m[0][1] * m2.m[1][3] + m[0][2] * m2.m[2][3] + m[0][3],

            m[1][0] * m2.m[0][0] + m[1][1] * m2.m[1][0] + m[1][2] * m2.m[2][0],
            m[1][0] * m2.m[0][1] + m[1][1] * m2.m[1][1] + m[1][2] * m2.m[2][1],
            m[1][0] * m2.m[0][2] + m[1][1] * m2.m[1][2] + m[1][2] * m2.m[2][2],
            m[1][0] * m2.m[0][3] + m[1][1] * m2.m[1][3] + m[1][2] * m2.m[2][3] + m[1][3],

            m[2][0] * m2.m[0][0] + m[2][1] * m2.m[1][0] + m[2][2] * m2.m[2][0],
            m[2][0] * m2.m[0][1] + m[2][1] * m2.m[1][1] + m[2][2] * m2.m[2][1],
            m[2][0] * m2.m[0][2] + m[2][1] * m2.m[1][2] + m[2][2] * m2.m[2][2],
            m[2][0] * m2.m[0][3] + m[2][1] * m2.m[1][3] + m[2][2] * m2.m[2][3] + m[2][3],

            0, 0, 0, 1);
    }

    mmFix32Vector3 mmFix32Matrix4x4::TransformDirectionAffine(const mmFix32Vector3& v) const
    {
        assert(this->IsAffine());

        return mmFix32Vector3(
            m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z,
            m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z,
            m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z);
    }

    mmFix32Vector3 mmFix32Matrix4x4::TransformAffine(const mmFix32Vector3& v) const
    {
        assert(this->IsAffine());

        return mmFix32Vector3(
            m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z + m[0][3],
            m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z + m[1][3],
            m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z + m[2][3]);
    }

    mmFix32Vector4 mmFix32Matrix4x4::TransformAffine(const mmFix32Vector4& v) const
    {
        assert(this->IsAffine());

        return mmFix32Vector4(
            m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z + m[0][3] * v.w,
            m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z + m[1][3] * v.w,
            m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z + m[2][3] * v.w,
            v.w);
    }

    MM_EXPORT_FIX32 std::ostream& operator << (std::ostream& o, const mmFix32Matrix4x4& mat)
    {
        o << "Matrix4x4(";
        for (size_t i = 0; i < 4; ++i)
        {
            o << " row" << (unsigned)i << "{";
            for (size_t j = 0; j < 4; ++j)
            {
                o << mat[i][j] << " ";
            }
            o << "}";
        }
        o << ")";
        return o;
    }

}
