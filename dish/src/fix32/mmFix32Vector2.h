/*
-----------------------------------------------------------------------------
MIT License

Copyright (c) 2017-2020 mm_longcheng@icloud.com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __mmFix32Vector2_h__
#define __mmFix32Vector2_h__

#include "core/mmCore.h"

#include "fix32/mmFix32.h"

#include <assert.h>
#include <sstream>

#include "fix32/mmFix32Export.h"

// https://www.ogre3d.org/

namespace mm
{
    /** \addtogroup Core
     *  @{
     */
    /** \addtogroup Math
     *  @{
     */
    /** Standard 2-dimensional vector.
     *  @remarks
     *      A direction in 2D space represented as distances along the 2
     *      orthogonal axes (x, y). Note that positions, directions and
     *      scaling factors can be represented by a vector, depending on how
     *      you interpret the values.
     */
    struct MM_EXPORT_FIX32 mmFix32Vector2
    {
    public:
        static const mmFix32Vector2 ZERO;
        static const mmFix32Vector2 UNIT_X;
        static const mmFix32Vector2 UNIT_Y;
        static const mmFix32Vector2 NEGATIVE_UNIT_X;
        static const mmFix32Vector2 NEGATIVE_UNIT_Y;
        static const mmFix32Vector2 UNIT_SCALE;
    public:
        mmFix32 x, y;
    public:
        inline mmFix32Vector2()
            : x(0)
            , y(0)
        {

        }
        inline mmFix32Vector2(const mmFix32Vector2& v)
            : x(v.x)
            , y(v.y)
        {

        }
        inline explicit mmFix32Vector2(double scalar)
            : x(scalar)
            , y(scalar)
        {

        }
        inline explicit mmFix32Vector2(double dx, double dy)
            : x(dx)
            , y(dy)
        {

        }
        inline explicit mmFix32Vector2(mmFix32 scalar)
            : x(scalar)
            , y(scalar)
        {

        }
        inline explicit mmFix32Vector2(mmFix32 fx, mmFix32 fy)
            : x(fx)
            , y(fy)
        {

        }
    public:
        /** Exchange the contents of this vector with another.
        */
        inline void Swap(mmFix32Vector2& other)
        {
            std::swap(this->x, other.x);
            std::swap(this->y, other.y);
        }
    public:
        inline mmFix32 operator [] (const size_t i) const
        {
            assert(i < 2);
            return *(&this->x + i);
        }

        inline mmFix32& operator [] (const size_t i)
        {
            assert(i < 2);
            return *(&this->x + i);
        }
        /// Pointer accessor for direct copying
        inline mmFix32* Ptr()
        {
            return &this->x;
        }
        /// Pointer accessor for direct copying
        inline const mmFix32* Ptr() const
        {
            return &this->x;
        }
    public:
        /** Assigns the value of the other vector.
         *  @param
         *      rkVector The other vector
         */
        inline mmFix32Vector2& operator = (const mmFix32Vector2& rkVector)
        {
            this->x = rkVector.x;
            this->y = rkVector.y;
            return *this;
        }

        inline mmFix32Vector2& operator = (const mmFix32 fScalar)
        {
            this->x = fScalar;
            this->y = fScalar;
            return *this;
        }
    public:
        inline bool operator == (const mmFix32Vector2& rkVector) const
        {
            return (this->x == rkVector.x && this->y == rkVector.y);
        }

        inline bool operator != (const mmFix32Vector2& rkVector) const
        {
            return (this->x != rkVector.x || this->y != rkVector.y);
        }
    public:
        inline mmFix32Vector2 operator + (const mmFix32Vector2& rkVector) const
        {
            return mmFix32Vector2(
                this->x + rkVector.x,
                this->y + rkVector.y);
        }

        inline mmFix32Vector2 operator - (const mmFix32Vector2& rkVector) const
        {
            return mmFix32Vector2(
                this->x - rkVector.x,
                this->y - rkVector.y);
        }

        inline mmFix32Vector2 operator * (const mmFix32 fScalar) const
        {
            return mmFix32Vector2(
                this->x * fScalar,
                this->y * fScalar);
        }

        inline mmFix32Vector2 operator * (const mmFix32Vector2& rhs) const
        {
            return mmFix32Vector2(
                this->x * rhs.x,
                this->y * rhs.y);
        }

        inline mmFix32Vector2 operator / (const mmFix32 fScalar) const
        {
            assert(fScalar != 0.0);
            mmFix32 fInv = mmFix32(1.0f) / fScalar;
            return mmFix32Vector2(
                x * fInv,
                y * fInv);
        }

        inline mmFix32Vector2 operator / (const mmFix32Vector2& rhs) const
        {
            return mmFix32Vector2(
                this->x / rhs.x,
                this->y / rhs.y);
        }

        inline const mmFix32Vector2& operator + () const
        {
            return *this;
        }

        inline mmFix32Vector2 operator - () const
        {
            return mmFix32Vector2(-this->x, -this->y);
        }

        inline friend mmFix32Vector2 operator * (const mmFix32 fScalar, const mmFix32Vector2& rkVector)
        {
            return mmFix32Vector2(
                fScalar * rkVector.x,
                fScalar * rkVector.y);
        }

        inline friend mmFix32Vector2 operator / (const mmFix32 fScalar, const mmFix32Vector2& rkVector)
        {
            return mmFix32Vector2(
                fScalar / rkVector.x,
                fScalar / rkVector.y);
        }

        inline friend mmFix32Vector2 operator + (const mmFix32Vector2& lhs, const mmFix32 rhs)
        {
            return mmFix32Vector2(
                lhs.x + rhs,
                lhs.y + rhs);
        }

        inline friend mmFix32Vector2 operator + (const mmFix32 lhs, const mmFix32Vector2& rhs)
        {
            return mmFix32Vector2(
                lhs + rhs.x,
                lhs + rhs.y);
        }

        inline friend mmFix32Vector2 operator - (const mmFix32Vector2& lhs, const mmFix32 rhs)
        {
            return mmFix32Vector2(
                lhs.x - rhs,
                lhs.y - rhs);
        }

        inline friend mmFix32Vector2 operator - (const mmFix32 lhs, const mmFix32Vector2& rhs)
        {
            return mmFix32Vector2(
                lhs - rhs.x,
                lhs - rhs.y);
        }
    public:
        inline mmFix32Vector2& operator += (const mmFix32 fScaler)
        {
            this->x += fScaler;
            this->y += fScaler;
            return *this;
        }
        inline mmFix32Vector2& operator +=(const mmFix32Vector2& v)
        {
            this->x += v.x;
            this->y += v.y;
            return *this;
        }
        inline mmFix32Vector2& operator -= (const mmFix32 fScaler)
        {
            this->x -= fScaler;
            this->y -= fScaler;
            return *this;
        }
        inline mmFix32Vector2& operator -=(const mmFix32Vector2& v)
        {
            this->x -= v.x;
            this->y -= v.y;
            return *this;
        }
        inline mmFix32Vector2& operator *= (const mmFix32 fScalar)
        {
            assert(fScalar != 0.0);
            mmFix32 fInv = mmFix32(1.0f) / fScalar;
            x *= fInv;
            y *= fInv;
            return *this;
        }
        inline mmFix32Vector2& operator *=(const mmFix32Vector2& v)
        {
            this->x *= v.x;
            this->y *= v.y;
            return *this;
        }
        inline mmFix32Vector2& operator /= (const mmFix32 fScaler)
        {
            this->x /= fScaler;
            this->y /= fScaler;
            return *this;
        }
        inline mmFix32Vector2& operator /=(const mmFix32Vector2& v)
        {
            this->x /= v.x;
            this->y /= v.y;
            return *this;
        }
    public:
        /// @copydoc vector2::length
        inline mmFix32 Length() const
        {
            return sqrt(this->x * this->x + this->y * this->y);
        }

        /// @copydoc vector2::squared_length
        inline mmFix32 SquaredLength() const
        {
            return this->x * this->x + this->y * this->y;
        }

        /// @copydoc vector2::distance
        inline mmFix32 Distance(const mmFix32Vector2& rhs) const
        {
            return (*this - rhs).Length();
        }

        /// @copydoc vector2::squared_distance
        inline mmFix32 SquaredDistance(const mmFix32Vector2& rhs) const
        {
            return (*this - rhs).SquaredLength();
        }
    public:
        /** Generates a vector perpendicular to this vector (eg an 'up' vector).
         *  @remarks
         *      This method will return a vector which is perpendicular to this
         *      vector. There are an infinite number of possibilities but this
         *      method will guarantee to generate one of them. If you need more
         *      control you should use the Quaternion class.
         */
        inline mmFix32Vector2 Perpendicular(void) const
        {
            return mmFix32Vector2(-y, x);
        }

        /// @copydoc vector2::dot_product
        inline mmFix32 DotProduct(const mmFix32Vector2& vec) const
        {
            return this->x * vec.x + this->y * vec.y;
        }

        /** Calculates the 2 dimensional cross-product of 2 vectors, which results
         *  in a single floating point value which is 2 times the area of the triangle.
         */
        inline mmFix32 CrossProduct(const mmFix32Vector2& rkVector) const
        {
            return this->x * rkVector.y - this->y * rkVector.x;
        }
    public:
        /// @copydoc vector2::normalise
        inline mmFix32 Normalise();
    public:
        /** Returns true if the vector's scalar components are all greater
         *  that the ones of the vector it is compared against.
         */
        inline bool operator < (const mmFix32Vector2& rhs) const
        {
            return this->x < rhs.x && this->y < rhs.y;
        }

        /** Returns true if the vector's scalar components are all smaller
         *  that the ones of the vector it is compared against.
         */
        inline bool operator > (const mmFix32Vector2& rhs) const
        {
            return this->x > rhs.x && this->y > rhs.y;
        }
    public:
        /** Calculates a reflection vector to the plane with the given normal .
         *  @remarks NB assumes 'this' is pointing AWAY FROM the plane, invert if it is not.
         */
        inline mmFix32Vector2 Reflect(const mmFix32Vector2& normal) const
        {
            return mmFix32Vector2(*this - (mmFix32(2) * this->DotProduct(normal) * normal));
        }

        /** Gets the angle between 2 vectors.
         *  @remarks
         *      Vectors do not have to be unit-length but must represent directions.
         */
        mmFix32 AngleBetween(const mmFix32Vector2& other) const;

        /** Gets the oriented angle between 2 vectors.
         *  @remarks
         *      Vectors do not have to be unit-length but must represent directions.
         *      The angle is comprised between 0 and 2 PI.
         */
        mmFix32 AngleTo(const mmFix32Vector2& other) const;
    public:
        /** Function for writing to a stream.
        */
        MM_EXPORT_FIX32 friend std::ostream& operator << (std::ostream& o, const mmFix32Vector2& v);
    };
    /** @} */
    /** @} */
}

#endif//__mmFix32Vector3_h__
